package cn.com.yusys.yusp.web.server.xdxt0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0010.req.Xdxt0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.resp.Xdxt0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxt0010.Xdxt0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 接口处理类:查询客户经理名单，包括工号、姓名
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0010:查询客户经理名单，包括工号、姓名")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0010Resource.class);

    @Autowired
    private Xdxt0010Service xdxt0010Service;
    /**
     * 交易码：xdxt0010
     * 交易描述：查询客户经理名单，包括工号、姓名
     *
     * @param xdxt0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户经理名单，包括工号、姓名")
    @PostMapping("/xdxt0010")
    protected @ResponseBody
    ResultDto<Xdxt0010DataRespDto> xdxt0010(@Validated @RequestBody Xdxt0010DataReqDto xdxt0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataReqDto));
        Xdxt0010DataRespDto xdxt0010DataRespDto = new Xdxt0010DataRespDto();// 响应Dto:查询客户经理名单，包括工号、姓名
        ResultDto<Xdxt0010DataRespDto> xdxt0010DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0010DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataReqDto));
            xdxt0010DataRespDto = xdxt0010Service.getXdxt0010(xdxt0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataRespDto));
            // 封装xdxt0010DataResultDto中正确的返回码和返回信息
            xdxt0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, e.getMessage());
            // 封装xdxt0010DataResultDto中异常返回码和返回信息
            xdxt0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0010DataRespDto到xdxt0010DataResultDto中
        xdxt0010DataResultDto.setData(xdxt0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataResultDto));
        return xdxt0010DataResultDto;
    }
}
