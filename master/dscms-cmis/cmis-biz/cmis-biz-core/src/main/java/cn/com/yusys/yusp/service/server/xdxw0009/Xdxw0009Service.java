package cn.com.yusys.yusp.service.server.xdxw0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0009.resp.Xdxw0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.xdypgyrcx.XdypgyrcxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class Xdxw0009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0009Service.class);
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Resource
    private XdypgyrcxService xdypgyrcxService ;
    @Resource
    private ICmisCfgClientService iCmisCfgClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AdminSmOrgService AdminSmOrgService;

    /**
     * 交易描述：借款担保合同签订/支用
     *
     * @param loanContNo
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdxw0009DataRespDto getXdxw0009(String loanContNo) throws Exception{
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(loanContNo));
        //返回对象
        Xdxw0009DataRespDto xdxw0009DataRespDto = new Xdxw0009DataRespDto();
        try{
            //请求参数必输校验
            if (StringUtils.isBlank(loanContNo)) {
                throw new Exception("借款合同号不能为空！");
            }

            /******第一步：根据借款合同号，查询项下的押品编号、调查流水号*******/
            logger.info("****XDXW0009**第一步：根据借款合同号，查询项下的押品编号、调查流水号****START****");
            String guaranty_id = grtGuarBizRstRelMapper.selectGuarantyIdByContNo(loanContNo);
            String survey_serno = ctrLoanContMapper.querySurveySernoByContNo(loanContNo);

            /******第二步：根据押品编号，查询客户经理信息*******/
            logger.info("****XDXW0009**第二步：根据押品编号，查询客户经理信息****START****");
            String manager_id = guarBaseInfoMapper.queryManagerIdByContNo(guaranty_id);//客户经理工号
            ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(manager_id);
            String manager_name = adminSmUserDtoResultDto.getData().getUserName();//客户经理名称
            String manager_org = adminSmUserDtoResultDto.getData().getOrgId();//所属机构
            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = AdminSmOrgService.getByOrgCode(manager_org);
            String manager_org_name= adminSmOrgDtoResultDto.getData().getOrgName();//所属机构名称

            /******第三步：根据押品编号，查询担保人人数*******/
            logger.info("****XDXW0009**第三步：根据押品编号，查询担保人人数****START****");
            XdypgyrcxReqDto xdypgyrcxReqDto = new XdypgyrcxReqDto();
            xdypgyrcxReqDto.setGuaranty_id(guaranty_id);
            XdypgyrcxRespDto xdypgyrcxRespDto = xdypgyrcxService.xdypgyrcx(xdypgyrcxReqDto);
            int count = 0;
            if (CollectionUtils.nonNull(xdypgyrcxRespDto)) {
                count = xdypgyrcxRespDto.getList().size();
            }
            if(count > 2){
                throw new Exception("该房产对应的最高额担保合同中担保人人数大于3，请走线下优抵贷！");
            }
            /******第四步：获取对应机构、机构名称及调查流水号、机构签章编码*******/
            logger.info("****XDXW0009**第四步：获取对应机构、机构名称及调查流水号、机构签章编码****START****");
            String query_organno="";//用于章查询的机构号
            if((!"".equals(manager_org)) && manager_org.startsWith("01")){
                //默认取016000的章
                query_organno="016000";
            }else if("127000".equals(manager_org) || "127100".equals(manager_org) || "107000".equals(manager_org) || "097000".equals(manager_org)){
                //苏州地区异地机构取对应机构的用章编码
                query_organno=manager_org;
            }else{
                throw new Exception("该业务非苏州大市范围内业务！");
            }
            //获取对应的章编码
            logger.info("**********XDXW0009**获取对应的章编码开始,查询参数为:{}", JSON.toJSONString(query_organno));
            ResultDto<CfgOrgElecSeal> resultDto = iCmisCfgClientService.selectFromCfgByOrgNo(query_organno);
            String code = resultDto.getCode();//返回结果
            CfgOrgElecSeal cfgOrgElecSeal = new CfgOrgElecSeal();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                cfgOrgElecSeal =  Optional.ofNullable(resultDto.getData()).orElse(new CfgOrgElecSeal());
            }
            logger.info("**********XDXW0009**获取对应的章编码结束,返回参数为:{}", JSON.toJSONString(cfgOrgElecSeal));
            String seal_code = cfgOrgElecSeal.getSealCode();//合同章编码
            String dy_seal_code = cfgOrgElecSeal.getGaurSealCode();//抵押担保合同章编码
            if("".equals(seal_code)||"".equals(dy_seal_code)){
                throw new Exception("请到信贷系统维护合同用章编码、抵押用章编码！");
            }

            /******第五步：接口返回值*******/
            logger.info("**********XDXW0009**第五步：接口返回值****START****,回值参数为:{}", JSON.toJSONString(xdxw0009DataRespDto));
            xdxw0009DataRespDto.setIndgtSerno(survey_serno);//调查流水号
            xdxw0009DataRespDto.setGageId(guaranty_id);//押品编号
            xdxw0009DataRespDto.setManagerName(manager_name);//客户经理名称
            xdxw0009DataRespDto.setOrgNo(manager_org);//机构号
            xdxw0009DataRespDto.setOrgName(manager_org_name);//机构名称
            xdxw0009DataRespDto.setContSealCode(seal_code);//合同用章编码
            xdxw0009DataRespDto.setPldSealCode(dy_seal_code);//抵押用章编码
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataRespDto));
        return xdxw0009DataRespDto;
    }

}
