package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportBasicInfo
 * @类描述: lmt_survey_report_basic_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-10 22:16:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyReportBasicInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 客户姓名 **/
	private String cusName;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 电话号码 **/
	private String phone;
	
	/** 工作单位 **/
	private String workUnit;
	
	/** 婚姻状况 **/
	private String marStatus;
	
	/** 配偶姓名 **/
	private String spouseName;
	
	/** 配偶证件号码 **/
	private String spouseCertCode;
	
	/** 配偶电话 **/
	private String spousePhone;
	
	/** 有无子女 **/
	private String haveChildren;
	
	/** 学历 **/
	private String edu;
	
	/** 居住年限 **/
	private String resiYears;
	
	/** 居住地址 **/
	private String livingAddr;
	
	/** 是否线上抵押 **/
	private String isOnlinePld;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;

	/** 是否提前申贷 **/
	private String isTqsd;

	/** 申贷类型 **/
	private String appLoanWay;

	/** 申贷类型 **/
	private String isCwhb;

	/** 无还本模型利率 **/
	private java.math.BigDecimal whbModelRate;

	/** 无还本模型金额（元） **/
	private java.math.BigDecimal whbModelAmt;

	/** 担保方式 **/
	private String guarMode;
	
	/** 模型建议金额 **/
	private java.math.BigDecimal modelAdviceAmt;
	
	/** 模型建议利率 **/
	private java.math.BigDecimal modelAdviceRate;
	
	/** 参考利率 **/
	private java.math.BigDecimal refRate;
	
	/** 建议金额 **/
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	private java.math.BigDecimal adviceRate;
	
	/** 配偶客户编号 **/
	private String spouseCusId;
	
	/** 建议期限 **/
	private String adviceTerm;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 是否续贷 **/
	private String isRenewLoan;
	
	/** 是否现场勘验 **/
	private String isSceneInquest;
	
	/** 模型初步结果 **/
	private String modelFstResult;
	
	/** 贷款用途 **/
	private String loanPurp;
	
	/** 是否农户 **/
	private String isAgri;
	
	/** 是否新员工 **/
	private String isNewEmployee;
	
	/** 新员工名称 **/
	private String newEmployeeName;
	
	/** 新员工电话 **/
	private String newEmployeePhone;
	
	/** 原借据号 **/
	private String oldBillNo;
	
	/** 原借据金额 **/
	private java.math.BigDecimal oldBillAmt;
	
	/** 原借据余额 **/
	private java.math.BigDecimal oldBillBalance;
	
	/** 原借据利率 **/
	private java.math.BigDecimal oldBillLoanRate;
	
	/** 借款人负债较上期增加是否超50% **/
	private String debtFlag;
	
	/** 借款人对外是否提供过多担保或大量资产被抵押 **/
	private String guarFlag;
	
	/** 借款人及其家庭是否发生意外 **/
	private String accidentFlag;
	
	/** 抵/质押物是否异常 **/
	private String guaranteeFlag;
	
	/** 借款人经营活动是否正常 **/
	private String activityFlag;
	
	/** 借款人经营所有权是否发生重大变化 **/
	private String managementBelongFlag;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 营销人工号 **/
	private String marketingId;


	public String getIsTqsd() {
		return isTqsd;
	}

	public void setIsTqsd(String isTqsd) {
		this.isTqsd = isTqsd;
	}

	public String getAppLoanWay() {
		return appLoanWay;
	}

	public void setAppLoanWay(String appLoanWay) {
		this.appLoanWay = appLoanWay;
	}

	public String getIsCwhb() {
		return isCwhb;
	}

	public void setIsCwhb(String isCwhb) {
		this.isCwhb = isCwhb;
	}

	public BigDecimal getWhbModelRate() {
		return whbModelRate;
	}

	public void setWhbModelRate(BigDecimal whbModelRate) {
		this.whbModelRate = whbModelRate;
	}

	public BigDecimal getWhbModelAmt() {
		return whbModelAmt;
	}

	public void setWhbModelAmt(BigDecimal whbModelAmt) {
		this.whbModelAmt = whbModelAmt;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit == null ? null : workUnit.trim();
	}
	
    /**
     * @return WorkUnit
     */	
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus == null ? null : marStatus.trim();
	}
	
    /**
     * @return MarStatus
     */	
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName == null ? null : spouseName.trim();
	}
	
    /**
     * @return SpouseName
     */	
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode == null ? null : spouseCertCode.trim();
	}
	
    /**
     * @return SpouseCertCode
     */	
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone == null ? null : spousePhone.trim();
	}
	
    /**
     * @return SpousePhone
     */	
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param haveChildren
	 */
	public void setHaveChildren(String haveChildren) {
		this.haveChildren = haveChildren == null ? null : haveChildren.trim();
	}
	
    /**
     * @return HaveChildren
     */	
	public String getHaveChildren() {
		return this.haveChildren;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu == null ? null : edu.trim();
	}
	
    /**
     * @return Edu
     */	
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param resiYears
	 */
	public void setResiYears(String resiYears) {
		this.resiYears = resiYears == null ? null : resiYears.trim();
	}
	
    /**
     * @return ResiYears
     */	
	public String getResiYears() {
		return this.resiYears;
	}
	
	/**
	 * @param livingAddr
	 */
	public void setLivingAddr(String livingAddr) {
		this.livingAddr = livingAddr == null ? null : livingAddr.trim();
	}
	
    /**
     * @return LivingAddr
     */	
	public String getLivingAddr() {
		return this.livingAddr;
	}
	
	/**
	 * @param isOnlinePld
	 */
	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld == null ? null : isOnlinePld.trim();
	}
	
    /**
     * @return IsOnlinePld
     */	
	public String getIsOnlinePld() {
		return this.isOnlinePld;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param modelAdviceAmt
	 */
	public void setModelAdviceAmt(java.math.BigDecimal modelAdviceAmt) {
		this.modelAdviceAmt = modelAdviceAmt;
	}
	
    /**
     * @return ModelAdviceAmt
     */	
	public java.math.BigDecimal getModelAdviceAmt() {
		return this.modelAdviceAmt;
	}
	
	/**
	 * @param modelAdviceRate
	 */
	public void setModelAdviceRate(java.math.BigDecimal modelAdviceRate) {
		this.modelAdviceRate = modelAdviceRate;
	}
	
    /**
     * @return ModelAdviceRate
     */	
	public java.math.BigDecimal getModelAdviceRate() {
		return this.modelAdviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return RefRate
     */	
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return AdviceAmt
     */	
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return AdviceRate
     */	
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId == null ? null : spouseCusId.trim();
	}
	
    /**
     * @return SpouseCusId
     */	
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm == null ? null : adviceTerm.trim();
	}
	
    /**
     * @return AdviceTerm
     */	
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param isRenewLoan
	 */
	public void setIsRenewLoan(String isRenewLoan) {
		this.isRenewLoan = isRenewLoan == null ? null : isRenewLoan.trim();
	}
	
    /**
     * @return IsRenewLoan
     */	
	public String getIsRenewLoan() {
		return this.isRenewLoan;
	}
	
	/**
	 * @param isSceneInquest
	 */
	public void setIsSceneInquest(String isSceneInquest) {
		this.isSceneInquest = isSceneInquest == null ? null : isSceneInquest.trim();
	}
	
    /**
     * @return IsSceneInquest
     */	
	public String getIsSceneInquest() {
		return this.isSceneInquest;
	}
	
	/**
	 * @param modelFstResult
	 */
	public void setModelFstResult(String modelFstResult) {
		this.modelFstResult = modelFstResult == null ? null : modelFstResult.trim();
	}
	
    /**
     * @return ModelFstResult
     */	
	public String getModelFstResult() {
		return this.modelFstResult;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}
	
    /**
     * @return LoanPurp
     */	
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param isAgri
	 */
	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri == null ? null : isAgri.trim();
	}
	
    /**
     * @return IsAgri
     */	
	public String getIsAgri() {
		return this.isAgri;
	}
	
	/**
	 * @param isNewEmployee
	 */
	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee == null ? null : isNewEmployee.trim();
	}
	
    /**
     * @return IsNewEmployee
     */	
	public String getIsNewEmployee() {
		return this.isNewEmployee;
	}
	
	/**
	 * @param newEmployeeName
	 */
	public void setNewEmployeeName(String newEmployeeName) {
		this.newEmployeeName = newEmployeeName == null ? null : newEmployeeName.trim();
	}
	
    /**
     * @return NewEmployeeName
     */	
	public String getNewEmployeeName() {
		return this.newEmployeeName;
	}
	
	/**
	 * @param newEmployeePhone
	 */
	public void setNewEmployeePhone(String newEmployeePhone) {
		this.newEmployeePhone = newEmployeePhone == null ? null : newEmployeePhone.trim();
	}
	
    /**
     * @return NewEmployeePhone
     */	
	public String getNewEmployeePhone() {
		return this.newEmployeePhone;
	}
	
	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo == null ? null : oldBillNo.trim();
	}
	
    /**
     * @return OldBillNo
     */	
	public String getOldBillNo() {
		return this.oldBillNo;
	}
	
	/**
	 * @param oldBillAmt
	 */
	public void setOldBillAmt(java.math.BigDecimal oldBillAmt) {
		this.oldBillAmt = oldBillAmt;
	}
	
    /**
     * @return OldBillAmt
     */	
	public java.math.BigDecimal getOldBillAmt() {
		return this.oldBillAmt;
	}
	
	/**
	 * @param oldBillBalance
	 */
	public void setOldBillBalance(java.math.BigDecimal oldBillBalance) {
		this.oldBillBalance = oldBillBalance;
	}
	
    /**
     * @return OldBillBalance
     */	
	public java.math.BigDecimal getOldBillBalance() {
		return this.oldBillBalance;
	}
	
	/**
	 * @param oldBillLoanRate
	 */
	public void setOldBillLoanRate(java.math.BigDecimal oldBillLoanRate) {
		this.oldBillLoanRate = oldBillLoanRate;
	}
	
    /**
     * @return OldBillLoanRate
     */	
	public java.math.BigDecimal getOldBillLoanRate() {
		return this.oldBillLoanRate;
	}
	
	/**
	 * @param debtFlag
	 */
	public void setDebtFlag(String debtFlag) {
		this.debtFlag = debtFlag == null ? null : debtFlag.trim();
	}
	
    /**
     * @return DebtFlag
     */	
	public String getDebtFlag() {
		return this.debtFlag;
	}
	
	/**
	 * @param guarFlag
	 */
	public void setGuarFlag(String guarFlag) {
		this.guarFlag = guarFlag == null ? null : guarFlag.trim();
	}
	
    /**
     * @return GuarFlag
     */	
	public String getGuarFlag() {
		return this.guarFlag;
	}
	
	/**
	 * @param accidentFlag
	 */
	public void setAccidentFlag(String accidentFlag) {
		this.accidentFlag = accidentFlag == null ? null : accidentFlag.trim();
	}
	
    /**
     * @return AccidentFlag
     */	
	public String getAccidentFlag() {
		return this.accidentFlag;
	}
	
	/**
	 * @param guaranteeFlag
	 */
	public void setGuaranteeFlag(String guaranteeFlag) {
		this.guaranteeFlag = guaranteeFlag == null ? null : guaranteeFlag.trim();
	}
	
    /**
     * @return GuaranteeFlag
     */	
	public String getGuaranteeFlag() {
		return this.guaranteeFlag;
	}
	
	/**
	 * @param activityFlag
	 */
	public void setActivityFlag(String activityFlag) {
		this.activityFlag = activityFlag == null ? null : activityFlag.trim();
	}
	
    /**
     * @return ActivityFlag
     */	
	public String getActivityFlag() {
		return this.activityFlag;
	}
	
	/**
	 * @param managementBelongFlag
	 */
	public void setManagementBelongFlag(String managementBelongFlag) {
		this.managementBelongFlag = managementBelongFlag == null ? null : managementBelongFlag.trim();
	}
	
    /**
     * @return ManagementBelongFlag
     */	
	public String getManagementBelongFlag() {
		return this.managementBelongFlag;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param marketingId
	 */
	public void setMarketingId(String marketingId) {
		this.marketingId = marketingId == null ? null : marketingId.trim();
	}
	
    /**
     * @return MarketingId
     */	
	public String getMarketingId() {
		return this.marketingId;
	}


}