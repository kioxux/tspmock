/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarWarrantInfo;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 17:13:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarwarrantinfo")
public class GuarWarrantInfoResource {
    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarWarrantInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarWarrantInfo> list = guarWarrantInfoService.selectAll(queryModel);
        return new ResultDto<List<GuarWarrantInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<GuarWarrantInfo>> index(@RequestBody QueryModel queryModel) {
        List<GuarWarrantInfo> list;
        //权证状态
        String certiState = (String) queryModel.getCondition().get("certiState");

        if (CmisBizConstants.STD_ZB_CERTI_STATE_04.equals(certiState) || CmisBizConstants.STD_ZB_CERTI_STATE_07.equals(certiState)){
            //做权证出库及续借时，由于老信贷有些权证是集中作业人员帮客户经理做的，且没有担保合同，这些权证在数据迁移时会将managerId赋值为空
            list = guarWarrantInfoService.selectWarrantInInfoByModel(queryModel);
        }else{
            list = guarWarrantInfoService.selectByModel(queryModel);
        }

        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<GuarWarrantInfo> show(@PathVariable("pkId") String pkId) {
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<GuarWarrantInfo>(guarWarrantInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarWarrantInfo> create(@RequestBody GuarWarrantInfo guarWarrantInfo) throws URISyntaxException {
        guarWarrantInfoService.insert(guarWarrantInfo);
        return new ResultDto<GuarWarrantInfo>(guarWarrantInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarWarrantInfo guarWarrantInfo) throws URISyntaxException {
        int result = guarWarrantInfoService.update(guarWarrantInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = guarWarrantInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarWarrantInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据核心担保编号查询权证是否电子权证
     * @函数名称:selectIsElectronicByCoreGuarantyNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/selectisewarrantbycoreguarantyno/{coreGuarantyNo}")
    protected ResultDto<String> selectIsEWarrantByCoreGuarantyNo(@PathVariable("coreGuarantyNo") String coreGuarantyNo){
        String result = guarWarrantInfoService.selectIsEWarrantByCoreGuarantyNo(coreGuarantyNo);
        return new ResultDto<>(result);
    }

    /**
     * 根据核心担保编号查询实时权证状态
     * @函数名称:selectCurCertiStateByCoreGuarantyNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/selectcurcertistatebycoreguarantyno/{coreGuarantyNo}")
    protected ResultDto<String> selectCurCertiStateByCoreGuarantyNo(@PathVariable("coreGuarantyNo") String coreGuarantyNo){
        String curCertiState = guarWarrantInfoService.selectCurCertiStateByCoreGuarantyNo(coreGuarantyNo);
        return new ResultDto<>(curCertiState);
    }
}
