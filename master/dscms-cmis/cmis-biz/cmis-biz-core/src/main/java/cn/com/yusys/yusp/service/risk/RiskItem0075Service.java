package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.GuarWarrantInfo;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.repository.mapper.LmtReplyMapper;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0075Service
 * @类描述: 批复日期及征信报告拦截校验
 * @功能描述: 批复日期及征信报告拦截校验
 * @创建人: wangqing
 * @创建时间: 2021-08-04 10:18:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0075Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0075Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private LmtReplyMapper lmtReplyMapper;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    /**
     * @方法名称: riskItem0075
     * @方法描述: 批复日期及征信报告拦截校验
     * @参数与返回说明: 1、借据起始日超过授信批复日30天（含）以上，校验是否引入人行征信报告及苏州地方征信报告（重新查征信），若未引入则拦截，
     *               2、若已引入同时判断报告生成日期是否在放款起始日30日（含）内，若超过则拦截，未超过则通过
     * @算法描述:
     * @创建人: wangqing
     * @创建时间: 2021-08-04 10:18:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0075(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("批复日期及征信报告拦截校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        if (pvpLoanApp != null) {
            String prdTypeProp = pvpLoanApp.getPrdTypeProp();
            if ("P010".equals(prdTypeProp)) {
                String replyNo = pvpLoanApp.getReplyNo();
                LmtReply lmtReply = lmtReplyMapper.getOriginReply(replyNo);
                // 授信复批日
                Date replyInureDate = DateUtils.parseDate(lmtReply.getReplyInureDate(), "yyyy-MM-dd");
                if (StringUtils.isEmpty(pvpLoanApp.getLoanStartDate()) || StringUtils.isEmpty(lmtReply.getReplyInureDate())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04513);
                    return riskResultDto;
                }
                // 贷款起始日
                Date loanStartDate = DateUtils.parseDate(pvpLoanApp.getLoanStartDate(), "yyyy-MM-dd");
                if (loanStartDate.compareTo(DateUtils.addDay(replyInureDate,30)) > 0) {
                    String pvpSerno = pvpLoanApp.getPvpSerno();
                    // 人行征信记录
                    QueryModel model = new QueryModel();
                    model.getCondition().put("bizSerno", pvpSerno);
                    List<CreditReportQryLstDto> creditReportQryLst = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(model);
                    if (CollectionUtils.nonEmpty(creditReportQryLst)) {
                        for (CreditReportQryLstDto dto : creditReportQryLst) {
                            if (StringUtils.isEmpty(dto.getReportCreateTime())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04515);
                                return riskResultDto;
                            }
                            // 报告生成时间
                            Date getReportCreateTime = DateUtils.parseDate(dto.getReportCreateTime(), "yyyy-MM-dd");
                            if (loanStartDate.compareTo(DateUtils.addDay(getReportCreateTime,30)) > 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04516);
                                return riskResultDto;
                            }
                        }
                        // 苏州地方征信记录
                        model = new QueryModel();
                        model.getCondition().put("bizSerno", pvpSerno);
                        model.getCondition().put("qryCls", "3");
                        creditReportQryLst = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(model);
                        if (CollectionUtils.nonEmpty(creditReportQryLst)) {
                            for (CreditReportQryLstDto dto : creditReportQryLst) {
                                if (StringUtils.isEmpty(dto.getReportCreateTime())) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04515);
                                    return riskResultDto;
                                }
                                // 报告生成时间
                                Date getReportCreateTime = DateUtils.parseDate(dto.getReportCreateTime(), "yyyy-MM-dd");
                                if (loanStartDate.compareTo(DateUtils.addDay(getReportCreateTime, 30)) > 0) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04516);
                                    return riskResultDto;
                                }
                            }
                        } else {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04514);
                            return riskResultDto;
                        }
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04514);
                        return riskResultDto;
                    }
                }
            }else{
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
                return riskResultDto;
            }
        } else {
            //数据获取失败 RISK_ERROR_0005
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
