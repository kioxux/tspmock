package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.req.Fb1174ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.resp.Fb1174RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.VisaXdRiskMapper;
import cn.com.yusys.yusp.service.Dscms2CircpClientService;
import cn.com.yusys.yusp.service.SfResultInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 房抵e点贷尽调结果录入流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX07BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX07BizService.class);//定义log


    @Autowired
    private SfResultInfoService sfResultInfoService;
    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;
    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 房贷尽调结果录入
        if ("YX015".equals(bizType)){
            sfResultInfoBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 房贷尽调结果录入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sfResultInfoBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            SfResultInfo sfResultInfo = sfResultInfoService.selectByPrimaryKey(serno);
            log.info("房抵e点贷尽调结果录入:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("房抵e点贷尽调结果录入:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("房抵e点贷尽调结果录入:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("房抵e点贷尽调结果录入:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("房抵e点贷尽调结果录入:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("房抵e点贷尽调结果录入:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("房抵e点贷尽调结果录入:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_997);
                //审批通过业务处理
                String is_admin = "1";
                //尽调结果发风控
                String flag = this.sendfb1174(sfResultInfo,is_admin);
                if(!"success".equals(flag)){
                    throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "风控返回："+flag);
                }
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("房抵e点贷尽调结果录入:"+serno+"流程否决，参数："+ instanceInfo.toString());
                String is_admin = "0";
                //尽调结果发风控
                String flag = this.sendfb1174(sfResultInfo,is_admin);
                if(!"success".equals(flag)){
                    throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "风控返回："+flag);
                }
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(sfResultInfo, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("房抵e点贷尽调结果录入:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 尽调结果录入审批通过业务处理
     * sfResultInfo 业务申请信息
     * */
    public String sendfb1174(SfResultInfo record,String is_admin){
        /*
         *************************************************************************
         * 调用风控fb1174房抵e点贷尽调结果通知
         * *************************************************************************/
        // 定义是否准入
        String is_zr = "";
        log.info("获取面签基本信息,流水号【{}】", record.getSerno());
        // 根据业务流水获取面签信息
        VisaXdRisk visaXdRisk = visaXdRiskMapper.selectByPrimaryKey(record.getSerno());
        log.info("获取面签基本信息：", visaXdRisk);
        // 调用风控fb1174接口
        log.info("调用房抵e点贷尽调结果通知推送风控开始,流水号【{}】", record.getSerno());
        Fb1174ReqDto fb1174ReqDto = new Fb1174ReqDto();
        if("1".equals(is_admin)){
            is_zr = "Y";
        }else if("0".equals(is_admin)){
            is_zr = "N";
        }
        fb1174ReqDto.setIs_admint(is_zr); //是否予以准入
        fb1174ReqDto.setApp_no(visaXdRisk.getCrpSerno()); //业务流水
        fb1174ReqDto.setCust_core_id (record.getCusId()); //核心客户号
        fb1174ReqDto.setCust_name(record.getCusName()); //客户名称
        fb1174ReqDto.setCert_code(record.getCertCode()); //证件号
        log.info("调用房抵e点贷尽调结果通知推送风控请求：" + fb1174ReqDto);
        ResultDto<Fb1174RespDto> fb1174RespResultDto = dscms2CircpClientService.fb1174(fb1174ReqDto);
        log.info("调用房抵e点贷尽调结果通知推送风控返回：" + fb1174RespResultDto);
        String fb1174Code = Optional.ofNullable(fb1174RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String fb1174Meesage = Optional.ofNullable(fb1174RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        log.info("调用房抵e点贷尽调结果通知推送风控返回信息：" + fb1174Meesage);
        if (Objects.equals(fb1174Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取相关的值并解析
            return "success";
        } else {
            return fb1174Meesage;
        }
    }

    /***
     * 流程审批状态更新
     * sfResultInfo 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (SfResultInfo sfResultInfo, String approveStatus){
        sfResultInfo.setApproveStatus(approveStatus);
        sfResultInfoService.updateSelective(sfResultInfo);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX07).equals(flowCode);
    }
}
