/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditQryBizReal;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditQryBizRealMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CreditQryBizRealMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CreditQryBizReal selectByPrimaryKey(@Param("cqbrSerno") String cqbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CreditQryBizReal> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CreditQryBizReal record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CreditQryBizReal record);

    /**
     * 批量插入
     * @param creditBizRelList
     * @return
     */
    int insertCreditBizRelList(@Param("list")List<CreditQryBizReal> creditBizRelList);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CreditQryBizReal record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CreditQryBizReal record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("cqbrSerno") String cqbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 查询征信业务流水号
     * @param xdzx0001DataReqDto
     * @return
     */
    Xdzx0001DataRespDto getZxSerno(Xdzx0001DataReqDto xdzx0001DataReqDto);

    /**
     * 根据流水号查询征信报告关联信息
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0033.resp.List> getCreditBizInfoBySurvey(@Param("indgtSerno") String indgtSerno);
    /**
     * 根据条件删除征信报告关联信息
     * @return
     */
    int deleteByCondition(Map params);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CreditQryBizReal> selectBySerno(String serno);

    /**
     * 根据业务流水号和征信流水号，查询征信关联信息
     * @return
     */
    CreditQryBizReal selectByBizSernoAndCreSerno(@Param("bizSerno") String bizSerno, @Param("crqlSerno") String crqlSerno);

    /**
     * 根据业务流水号和征信流水号，查询征信关联信息
     * @return
     */
    CreditQryBizReal selectByBizSernoAndCertCode(@Param("bizSerno") String bizSerno, @Param("certCode") String certCode);
}