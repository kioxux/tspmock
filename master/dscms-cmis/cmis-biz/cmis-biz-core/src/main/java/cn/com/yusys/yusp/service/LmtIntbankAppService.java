/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.req.CmisLmt0037ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.resp.CmisLmt0037RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 19:09:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankAppService extends BizInvestCommonService{
    private static final Logger log = LoggerFactory.getLogger(LmtIntbankAppService.class);

    @Autowired
    private LmtIntbankAppMapper lmtIntbankAppMapper;

    @Autowired
    private LmtIntbankAppSubMapper lmtIntbankAppSubMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankAppSubService lmtIntbankAppSubService;

    @Autowired
    private LmtIntbankApprService lmtIntbankApprService ;

    @Autowired
    private LmtIntbankReplyService lmtIntbankReplyService ;

    @Autowired
    private LmtIntbankReplyMapper lmtIntbankReplyMapper ;

    @Autowired
    private LmtIntbankAccService lmtIntbankAccService ;

    @Autowired
    private LmtIntbankAccMapper lmtIntbankAccMapper;

    @Autowired
    private LmtIntbankReplySubService lmtIntbankReplySubService;

    @Autowired
    private LmtIntbankReplySubMapper lmtIntbankReplySubMapper ;

    @Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService ;

    @Autowired
    private LmtIntbankAccSubMapper lmtIntbankAccSubMapper ;

    @Autowired
    private LmtIntbankApprSubService lmtIntbankApprSubService ;

    @Autowired
    private LmtChgDetailMapper lmtChgDetailMapper;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    @Autowired
    private LmtAppRelGrpLimitService lmtAppRelGrpLimitService;

    @Autowired
    private LmtIntbankAccService lmtIntbankAccervice;

    @Autowired
    private LmtRediDetailService lmtRediDetailService;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService ;
    @Autowired
    private ICusClientService iCusClientService;
    // 系统参数配置服务
    @Resource
    private AdminSmPropService adminSmPropService;

    @Autowired
    private LmtReconsideDetailService lmtReconsideDetailService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private LmtReplyAccLoanCondService lmtReplyAccLoanCondService;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtIntbankApp selectByPrimaryKey(String pkId) {
        return lmtIntbankAppMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankApp> selectAll(QueryModel model) {
        List<LmtIntbankApp> records = (List<LmtIntbankApp>) lmtIntbankAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtIntbankApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankApp> list = lmtIntbankAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map valilmttype(QueryModel model) {
        Map rtnData = new HashMap();

        List<LmtIntbankAcc> LmtIntbankAccList= lmtIntbankAccMapper.selectByModel(model);
        if (CollectionUtils.nonEmpty(LmtIntbankAccList)) {
            rtnData.put("lmtType", CmisBizConstants.STD_SX_LMT_TYPE_03);
        } else {
            rtnData.put("lmtType", CmisBizConstants.STD_SX_LMT_TYPE_01);
        }
        return rtnData;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtIntbankApp record) {
        return lmtIntbankAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankApp record) {
        return lmtIntbankAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtIntbankApp record) {
        return lmtIntbankAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtIntbankApp record) {
        return lmtIntbankAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: onReconside
     * @方法描述: 新增同业客户授信复议申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: lixy
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onReconside(@RequestBody LmtIntbankApp lmtIntbankApp) throws BizException {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String origSerno = "";
        String origReplySerno = "";
        LmtIntbankApp lmtIntbankAppNew = new LmtIntbankApp();
        LmtIntbankReply lmtIntbankReply = new LmtIntbankReply();
        String cusId = "";
        try {
            cusId = lmtIntbankApp.getCusId();
            //由于传送数据不全，这里根据主键重新获取
            lmtIntbankApp = this.selectByPrimaryKey(lmtIntbankApp.getPkId());

            //校验是否存在在途申请
            boolean exit = judgeIsExistOnWayApp(cusId);
            //存在在途申请，不允许发起复议
            if(exit){
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                throw BizException.error(null, EcbEnum.ECB010002.key, EcbEnum.ECB010002.value);
            }
            //判断当前客户是否存在正在流转的主体授信
            List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppService.selectRunningInvestListByCusId(cusId);
            if (lmtSigInvestApps!=null && lmtSigInvestApps.size()>0){
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                throw BizException.error(null, EcbEnum.ECB020002.key, EcbEnum.ECB020002.value);
            }
            //审批状态
            String approveStatus = lmtIntbankApp.getApproveStatus();

            /**
             * 校验选中要复议的这笔申请，是否是当前客户最新的批复对应的申请
             */
            origSerno = lmtIntbankApp.getSerno();
            HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
            lmtReplyQueryMap.put("serno", origSerno);
            BizInvestCommonService.checkParamsIsNull("origSerno",origSerno);
            lmtReplyQueryMap.put("status", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtIntbankReply> list  = lmtIntbankReplyService.queryLmtReplyDataByParams(lmtReplyQueryMap);
            if(list != null && list.size() == 1){
                lmtIntbankReply = list.get(0);
            }else{
                rtnCode = EcbEnum.ECB020009.key;
                rtnMsg = EcbEnum.ECB020009.value;
                throw BizException.error(null, EcbEnum.ECB020009.key, EcbEnum.ECB020009.value);
            }

            //如果不是否决数据，进行判断该批复是否存在有效占用关系，如果存在不允许复议
            if(CmisFlowConstants.WF_STATUS_997.equals(approveStatus)){
                //申请流水号
                serno = lmtIntbankApp.getSerno() ;
                //根据申请流水号查询批复台账信息
                LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.selectBySerno(serno) ;

                if(lmtIntbankAcc == null ){
                    rtnCode = EcbEnum.ECB020012.key;
                    rtnMsg = EcbEnum.ECB020012.value;
                    throw BizException.error(null, EcbEnum.ECB020012.key, EcbEnum.ECB020012.value);
                }

                //获取批复台账信息
                String accNo = lmtIntbankAcc.getAccNo() ;
                CmisLmt0037ReqDto cmisLmt0037reqDto = new CmisLmt0037ReqDto() ;
                //查询类型：1 批复台账编号查询
                cmisLmt0037reqDto.setQueryType(CmisBizConstants.FLOW_QUERY_TYPE_1);
                cmisLmt0037reqDto.setAccNo(accNo);
                //调用接口，查询批复向下是否存在有效占用关系
                ResultDto<CmisLmt0037RespDto> cmisLmt0037RespDtoResultDto  = cmisLmtClientService.cmislmt0037(cmisLmt0037reqDto) ;
                //接口结果处理
                if(cmisLmt0037RespDtoResultDto.getCode().equals(SuccessEnum.CMIS_SUCCSESS.key)){
                    CmisLmt0037RespDto cmisLmt0037RespDto = cmisLmt0037RespDtoResultDto.getData() ;
                    if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(cmisLmt0037RespDto.getIsExists())){
                        rtnCode = EcbEnum.ECB020005.key;
                        rtnMsg = EcbEnum.ECB020005.value;
                        throw BizException.error(null, EcbEnum.ECB020005.key, EcbEnum.ECB020005.value);
                    }
                }
            }

            origReplySerno = lmtIntbankReply.getReplySerno();

            //判断复议申请是否超过3次
            checkChangeIsExceedTimes(lmtIntbankApp.getSerno(),lmtIntbankReply);

            copyProperties(lmtIntbankReply, lmtIntbankAppNew);
            lmtIntbankAppNew.setOrigiLmtReplySerno(lmtIntbankReply.getReplySerno());
            lmtIntbankAppNew.setOrigiLmtTerm(lmtIntbankReply.getLmtTerm());
            //添加原授信金额
            lmtIntbankAppNew.setOrigiLmtAmt(lmtIntbankReply.getLmtAmt());
            serno = generateSerno(SeqConstant.INTBANK_LMT_SEQ);
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("保存授信复议申请数据,生成流水号%s", serno));

            //添加企业基本信息（lmt_app_rel_cus_info）
            lmtAppRelCusInfoService.insertCusInfoApp(serno, cusId, lmtIntbankReply.getCurType(), CmisBizConstants.STD_ZB_CUS_CATALOG_3);

            //添加集团额度信息（lmt_app_rel_grp_limit）
            lmtAppRelGrpLimitService.insertCusLmtInfo(serno, cusId, lmtIntbankReply.getCurType(), CmisBizConstants.STD_ZB_CUS_CATALOG_3);

            // 申请流水号
            lmtIntbankAppNew.setSerno(serno);
            // 数据操作标志为新增
            lmtIntbankAppNew.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //  记录申请类型为复议
            lmtIntbankAppNew.setLmtType(CmisCommonConstants.LMT_TYPE_05);
            // 流程状态
            lmtIntbankAppNew.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                lmtIntbankAppNew.setPkId(generatePkId());
                lmtIntbankAppNew.setInputId(userInfo.getLoginCode());
                lmtIntbankAppNew.setInputBrId(userInfo.getOrg().getCode());
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                lmtIntbankAppNew.setInputDate(openday);
                lmtIntbankAppNew.setUpdId(userInfo.getLoginCode());
                lmtIntbankAppNew.setUpdBrId(userInfo.getOrg().getCode());
                lmtIntbankAppNew.setUpdDate(openday);
                lmtIntbankAppNew.setManagerId(userInfo.getLoginCode());
                lmtIntbankAppNew.setManagerBrId(userInfo.getOrg().getCode());
                lmtIntbankAppNew.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtIntbankAppNew.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            //申请类型为授信复议，初始化复议申请表数据
            LmtReconsideDetail lmtReconsideDetail = new LmtReconsideDetail();
            lmtReconsideDetail.setLmtSerno(serno);
            initInsertDomainProperties(lmtReconsideDetail);
            lmtReconsideDetailService.insert(lmtReconsideDetail);

            log.info(String.format("保存单一客户授信复议数据,流水号%s", serno));

            initMustCheckItem(lmtIntbankAppNew);

            int count = lmtIntbankAppMapper.insert(lmtIntbankAppNew);
            boolean result = lmtIntbankReplySubService.copyToLmtAppSub(origReplySerno,serno,approveStatus);
            if (count != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成复议申请失败！");
            }
        } catch (BizException e) {
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch(Exception e){
            throw BizException.error(null, "", e.getMessage());
        } finally {
            rtnData.put("lmtIntbankApp", lmtIntbankAppNew);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: judgeIsExistOnWayApp
     * @方法描述: 根据客户号查询客户是否存在在途的授信申请
     *  同业客户 需要同时判断同业客户综合授信中是否存在审核中的数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun1
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public boolean judgeIsExistOnWayApp(String cusId) {
        HashMap paramMap = new HashMap<String, Object>();
        paramMap.put("cusId", cusId);
        BizInvestCommonService.checkParamsIsNull("cusId",cusId);
        paramMap.put("oprType", CmisCommonConstants.ADD_OPR);
        paramMap.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
        List<LmtIntbankApp> lmtAppList = lmtIntbankAppMapper.selectByParams(paramMap);
        if (lmtAppList.size() > 0) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断复议申请是否超过
     * @param origSerno
     * @param origiLmtIntbankReply
     * @return
     */
    public void checkChangeIsExceedTimes(String origSerno, LmtIntbankReply origiLmtIntbankReply) throws BizException, ParseException {
        //授信复议次数
        int fuyiCount = 0;
        //授信复议默认3天
        int allowFuyiCount = 3;


        String fuiyiCount = "1" ; //getSysParameterByName("FY_COUNT");
        if (!StringUtils.isBlank(fuiyiCount)) {
            allowFuyiCount = Integer.parseInt(fuiyiCount);
        }
        //授信复议可发起天数
        int allowDelayDays = 0;
        String allowDelayDaysSys = getSysParameterByName("FY_DAYS");
        if (!StringUtils.isBlank(allowDelayDaysSys)) {
            allowDelayDays = Integer.parseInt(allowDelayDaysSys);
            //0天 不允许发起复议
            if (allowDelayDays == 0){
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000022.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000022.value);
            }
            String startDate = origiLmtIntbankReply.getStartDate();

            //6.授信复议不能超过天数计算
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = DateUtils.addDay(simpleDateFormat.parse(startDate),allowDelayDays);

            //当前日期(采用openDay)
            Date nowDate = DateUtils.parseDate(getCurrrentDateStr(),"yyyy-MM-dd");

            log.info("【"+origSerno+"】发起复议，allowDelayDays【"+allowDelayDays+"】  startDate【"+startDate+"】 " +
                    "curDate【"+simpleDateFormat.format(nowDate)+"】 deadline【"+simpleDateFormat.format(date)+"】");

            if(nowDate.after(date)){
                log.info("当前日期已超过当前数据复议的最大天数限制:"+allowDelayDays);
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000023.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000023.value + allowDelayDays + "天！");
            }
        }

        //获取当前申请发起的复议记录
        String realOriginReplySerno = findRealOriginLmtSigInvestRst(origiLmtIntbankReply.getReplySerno());
        fuyiCount = getFyCount(realOriginReplySerno);
        log.info("origSerno【{}】复议次数：{}", origSerno, fuyiCount);
        if (fuyiCount >= allowFuyiCount) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_FUYI.key, EclEnum.LMT_SIG_INVESTAPP_FUYI.value + allowFuyiCount + "次！");
        }

    }

    /**
     * 获取复议次数 包含审批中的数据
     *
     * @param replySerno
     * @return
     */
    public Integer getFyCount(String replySerno) {
        int count = 0;
        //获取审批完成的复议次数
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("origiLmtReplySerno", replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        queryModel.addCondition("appType",CmisBizConstants.STD_SX_LMT_TYPE_05);
        List<LmtIntbankReply> lmtIntbankReplies = lmtIntbankReplyMapper.selectByModel(queryModel);
        //获取审批中的复议次数
        queryModel.addCondition("approveStatusList", "000,111,990,991,992,993");
        List<LmtIntbankApp> lmtIntbankApps = selectAll(queryModel);
        if (CollectionUtils.nonEmpty(lmtIntbankApps)) {
            List<String> collect = lmtIntbankApps.stream().map(LmtIntbankApp::getSerno).collect(Collectors.toList());
            log.info("origiLmtReplySerno【" + replySerno + "】 == > app ==> " + collect.toString());
            count += lmtIntbankApps.size();
        }
        if (CollectionUtils.nonEmpty(lmtIntbankReplies)) {
            List<String> collect = lmtIntbankReplies.stream().map(LmtIntbankReply::getSerno).collect(Collectors.toList());
            log.info("origiLmtReplySerno【" + replySerno + "】 == > rst ==> " + collect.toString());
            log.info("==========================");
            count += lmtIntbankReplies.size();
            for (LmtIntbankReply lmtIntbankReply : lmtIntbankReplies) {
                //获取当前批复记录的复议次数
                count += getFyCount(lmtIntbankReply.getReplySerno());
            }
        }
        return count;
    }

    /**
     * 获取当前复议的初始记录
     *
     * @param replySerno
     * @return
     */
    private String findRealOriginLmtSigInvestRst(String replySerno) {
        log.info("findRealOriginLmtSigInvestRst ===> " + replySerno);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno", replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtIntbankReply> lmtIntbankReplies = lmtIntbankReplyMapper.selectByModel(queryModel);
        if (lmtIntbankReplies != null && lmtIntbankReplies.size() == 1) {
            LmtIntbankReply lmtIntbankReply = lmtIntbankReplies.get(0);
            if (CmisCommonConstants.LMT_TYPE_05.equals(lmtIntbankReply.getLmtType())) {
                return findRealOriginLmtSigInvestRst(lmtIntbankReply.getOrigiLmtReplySerno());
            } else {
                return lmtIntbankReply.getReplySerno();
            }
        } else {
            throw BizException.error(null, "99999", "授信批复查询异常!");
        }
    }

    /**
     * 获取发起复议次数
     * add by zhangjw 20210724
     */
    public Integer getFyCount2(String serno){
        Integer count = 0;
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.selectBySerno(serno);
        if (lmtIntbankReply != null) {
            String lmtType = lmtIntbankReply.getLmtType();
            if(CmisBizConstants.STD_SX_LMT_TYPE_05.equals(lmtType)){
                count ++ ;

                String origiLmtReplySerno = lmtIntbankReply.getOrigiLmtReplySerno();
                if(!StringUtils.isBlank(origiLmtReplySerno)){
                    LmtIntbankReply newLmtIntbankReply = lmtIntbankReplyService.selectByReplySerno(origiLmtReplySerno);
                    if(newLmtIntbankReply!=null){
                        if(CmisBizConstants.STD_SX_LMT_TYPE_05.equals(newLmtIntbankReply.getLmtType())){
                            count = count + this.getFyCount(newLmtIntbankReply.getSerno());
                        }
                    }
                }
            }
        }
        return count;
    }


    /**
     * 添加（同业授信、主体授信）记录
     * @param info
     */
    @Transactional
    public Map addProjectBasicInfo(Map info) {
        int result = 0;
        try {
            Map dataInfo = (Map) info.get("map");
            String cusId = (String) dataInfo.get("cusId");
            String cusType = (String) dataInfo.get("cusType");
            String cusCatalog = (String) dataInfo.get("cusCatalog");

            //校验前台pkId 和serno 是否生成
            if (StringUtils.isBlank(String.valueOf(dataInfo.get("newSerno")))
                    || StringUtils.isBlank(String.valueOf(dataInfo.get("newPkId")))){
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
            }

            //生成申请流水号 从前台获取 防止重复提交
            String serno = (String) dataInfo.get("newSerno");

            log.info("同业授信生成申请流水号【{}】", serno);
            //添加企业基本信息（lmt_app_rel_cus_info）
            lmtAppRelCusInfoService.insertCusInfoApp(serno, cusId, cusType, cusCatalog);

            //添加集团额度信息（lmt_app_rel_grp_limit）
            lmtAppRelGrpLimitService.insertCusLmtInfo(serno, cusId, cusType, cusCatalog);

            boolean isExistOnWayApp = judgeIsExistOnWayApp(cusId);
            if (isExistOnWayApp){
                throw BizException.error(null, EcbEnum.ECB010002.key, EcbEnum.ECB010002.value);
            }

            //判断当前客户是否存在正在流转的产品授信
            List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppService.selectRunningInvestListByCusId(cusId);
            if (CollectionUtils.nonEmpty(lmtSigInvestApps)){
                throw BizException.error(null, EcbEnum.ECB020002.key, EcbEnum.ECB020002.value);
            }

            //添加授信主表数据
            result = addOrUpdateLmtIntbankApp(serno, dataInfo, false);
            //更新授信金额
            updateLmtAmt(serno);
            if (result == 1){
                //添加授信从表数据
//                result = lmtIntbankAppSubService.addOrUpdateLmtIntbankAppSub(serno,dataInfo,false);
            }
            dataInfo.put("serno", serno);
        } catch (Exception e) {
            log.error("同业授信新增失败",e);
            if (e instanceof BizException){
                throw e;
            }
            result = 0;
        }
        if (result == 0){
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_INSERT_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_INSERT_FAILD.value);
        }
        return info;
    }

    /**
     * 添加或修改授信主表数据
     * @param serno
     * @param info
     * @param isUpdate
     */
    private int addOrUpdateLmtIntbankApp(String serno, Map info,boolean isUpdate){
        LmtIntbankApp app = null;
        //修改前保存原pkid
        String origiPkId = null;
        if (isUpdate){
            app = selectBySerno(serno);
            if (app == null){
                return 0;
            }else{
                origiPkId = app.getPkId();
            }
        }else{
            app = new LmtIntbankApp();
        }
        //修改String为BigDecimal
        changeValueToBigDecimal(info,"lmtAmt");
        changeValueToBigDecimal(info,"intendActualIssuedScale");
        info.remove("updateTime");
        info.remove("createTime");
        mapToBean(info,app);
        //同业授信 默认币种为 人民币(CNY)
        app.setCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
        //增加默认值0
        app.setLmtAmt(new BigDecimal(0));

        if (CmisBizConstants.STD_SX_LMT_TYPE_03.equals(app.getLmtType())) {
            String accNo = "";
            if (info.get("accNo") == null){
                throw BizException.error(null,"99999","台账编号获取失败！");
            }else{
                accNo = info.get("accNo").toString();
            }
            //根据cusId获取用户存在的授信额度
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("accNo",accNo);
            BizInvestCommonService.checkParamsIsNull("accNo",accNo);
            List<LmtIntbankAcc> LmtIntbankAccList= lmtIntbankAccMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtIntbankAccList)) {
                LmtIntbankAcc lmtIntbankAcc = LmtIntbankAccList.get(0);

                LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.selectBySerno(lmtIntbankAcc.getSerno());
                if (lmtIntbankReply!=null){
                    //copy基本信息(从批复表中获取)
                    copyProperties(lmtIntbankReply,app);
                    //设置申请类型为授信续作
                    app.setLmtType(CmisBizConstants.STD_SX_LMT_TYPE_03);
                }

                app.setSerno(serno);
                app.setOrigiLmtReplySerno(lmtIntbankAcc.getReplySerno());
                app.setOrigiLmtAmt(lmtIntbankAcc.getLmtAmt());
                app.setOrigiLmtTerm(lmtIntbankAcc.getTerm());

                //copy授信分项信息
                QueryModel model = new QueryModel();
                model.addCondition("replySerno", lmtIntbankAcc.getReplySerno());
                BizInvestCommonService.checkParamsIsNull("replySerno",lmtIntbankAcc.getReplySerno());
                model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                List<LmtIntbankAccSub> lmtIntbankAccSubList= lmtIntbankAccSubService.selectAll(model);
                if (lmtIntbankAccSubList!=null && lmtIntbankAccSubList.size()>0){
                    for (LmtIntbankAccSub lmtIntbankAccSub : lmtIntbankAccSubList) {
                        LmtIntbankAppSub newLmtIntbankAppSub = new LmtIntbankAppSub();
                        //原期限信息
                        int oldTerm = lmtIntbankAccSub.getLmtTerm() ;
                        //原分享金额
                        BigDecimal oldLmtAmt = lmtIntbankAccSub.getLmtAmt() ;
                        //原分享编号
                        String oldSubNo = lmtIntbankAccSub.getAccSubNo() ;
                        copyProperties(lmtIntbankAccSub,newLmtIntbankAppSub);
                        newLmtIntbankAppSub.setSerno(serno);
                        //处理原数据信息
                        newLmtIntbankAppSub.setOrigiLmtAccSubTerm(oldTerm);
                        newLmtIntbankAppSub.setOrigiLmtAccSubAmt(oldLmtAmt);
                        newLmtIntbankAppSub.setOrigiLmtAccSubNo(oldSubNo);
                        newLmtIntbankAppSub.setTerm(oldTerm);
                        //初始化(新增)通用domain信息
                        initInsertDomainProperties(newLmtIntbankAppSub);
                        lmtIntbankAppSubMapper.insert(newLmtIntbankAppSub) ;
                    }
                }
            }
        }
        app.setSerno(serno);
        app.setManagerBrId( SessionUtils.getUserInformation().getOrg().getCode());
        app.setManagerId( SessionUtils.getUserInformation().getLoginCode());

        if (isUpdate){
            app.setPkId(origiPkId);
            //初始化（更新）通用domain信息
            initUpdateDomainProperties(app);
            return lmtIntbankAppMapper.updateByPrimaryKey(app);
        }else{
            //初始化(新增)通用domain信息
            initInsertDomainProperties(app);
            //流程状态为未发起
            app.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);

            initMustCheckItem(app);

            //从前台获取防止重复提交
            String newPkId = (String) info.get("newPkId");
            app.setPkId(newPkId);

            return lmtIntbankAppMapper.insert(app);
        }
    }

    /**
     * 初始化 必填信息校验页面
     * @param app
     */
    private void initMustCheckItem(LmtIntbankApp app) {
        //判断是否未 授信复议
        if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(app.getLmtType())){
            insertMustCheckItem(app.getSerno(),"fysqb","复议申请表",app.getLmtType(),CmisBizConstants.STD_ZB_YES_NO_N);
            return;
        }

        //判断是否未 授信变更
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(app.getLmtType())){
            insertMustCheckItem(app.getSerno(),"bgsqb","变更申请书",app.getLmtType(),CmisBizConstants.STD_ZB_YES_NO_N);
            return;
        }

        //添加必填信息校验初始化
        insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getLmtType(),CmisBizConstants.STD_ZB_YES_NO_N);
    }

    /**
     * 根据申请流水号获取
     * @param serno
     * @return
     */
    public LmtIntbankApp selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtIntbankApp> lmtIntbankApps = lmtIntbankAppMapper.selectByModel(queryModel);
        if (lmtIntbankApps!=null && lmtIntbankApps.size()>0){
            return lmtIntbankApps.get(0);
        }
        return null;
    }

    /**
     * 判断大额授信 -同业综合授信<br/>
     * 同业银行类<br/>
     *     1.授信主体类别：<br/>
     *              国有商业银行(同业机构类型 : 1C1201  大型商业银行)、<br/>
     *              全国性股份行(同业机构类型 : 1C1202  股份制商业银行)、<br/>
     *              省内农商行（江苏省内）(同业机构类型 : 1C1204 并且 同业客户信息中注册地行政区划 是在江苏省范围内（320开头）)<br/>
     *          大额授信判断额度：5亿<br/>
     *     2.授信主体类别：其他(除上述三种，其他银行类 同业机构类型 以 1C1 开头的，不包含上述三种情况的)<br/>
     *          大额授信判断额度：3亿<br/>
     * 同业非银类<br/>
     *      授信主体类别：同业非银类(除上述四种情况均为同业非银类)<br/>
     *          大额授信判断额度：2亿<br/>
     *
     * @param lmtIntbankApp
     * @return
     */
    public Map<String,String> checkIsLargeLmt(LmtIntbankApp lmtIntbankApp) {

        String serno = lmtIntbankApp.getSerno();
        //是否大额授信
        String isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_N;
        //是否报备董事长
        String isReportChairman = CmisBizConstants.STD_ZB_YES_NO_N;
        Map<String,String> resultMap = new HashMap<>();
        resultMap.put("isLargeLmt",isLargeLmt);
        resultMap.put("isReportChairman",isReportChairman);

        //授信金额
        BigDecimal lmtAmt = changeValueToBigDecimal(lmtIntbankApp.getLmtAmt());
        log.info("【"+serno+"】判断是否大额授信：1-------------->"+lmtAmt);
        /**add by zhangjw 同业综合授信在判断是否在信贷管理部负责人权限内的话，要扣减掉同业管理类额度*/
        List<LmtIntbankAppSub> lmtIntbankAppSubs = lmtIntbankAppSubService.selectSubListBySerno(lmtIntbankApp.getSerno());
        if(lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
            //modify by zhangjw 20210822 增加判断如果同业授信只有同业管理类额度，则审批权限默认为信贷管理部负责人权限内
            if (lmtIntbankAppSubs.size() == 1
                    && CmisBizConstants.LMT_INTBANK_BIZ_TYPE_3006.equals(lmtIntbankAppSubs.get(0).getLmtBizType())) {
                resultMap.put("isLargeLmt", isLargeLmt);
                resultMap.put("isReportChairman", isLargeLmt);
                log.info("【" + serno + "】判断是否大额授信：-------------->仅有同业管理类额度，默认为否");
                return resultMap;
            }
            for(LmtIntbankAppSub lmtIntbankAppSub : lmtIntbankAppSubs ){
                if(CmisBizConstants.LMT_INTBANK_BIZ_TYPE_3006.equals(lmtIntbankAppSub.getLmtBizType())){
                    BigDecimal lmtsubAmt = lmtIntbankAppSub.getLmtAmt();
                    lmtAmt = lmtAmt.subtract(lmtsubAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
                    break;
                }
            }
            log.info("【"+serno+"】判断是否大额授信：扣除掉同业管理类额度后lmtAmt-------------->"+lmtAmt);
        }
        log.info("【"+serno+"】判断是否大额授信：2【{}】【{}】-------------->",lmtIntbankApp.getSerno(),lmtIntbankApp.getCusId());
        //获取客户基本信息
        LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(lmtIntbankApp.getSerno(), lmtIntbankApp.getCusId());
        if (lmtAppRelCusInfo == null) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000024.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000024.value);
        }
        //同业机构类型
        String intbankOrgType = lmtAppRelCusInfo.getIntbankOrgType();
        log.info("【"+lmtIntbankApp.getSerno()+"】判断是否大额授信：3---------->intbankOrgType【{}】" , intbankOrgType);
        //授信主体类别：国有商业银行(C12111、C12141)、全国性股份行C12112
        if (CmisBizConstants.STD_ZB_INTBANK_TYPE_C12111.equals(intbankOrgType)
                || CmisBizConstants.STD_ZB_INTBANK_TYPE_C12141.equals(intbankOrgType)
                || CmisBizConstants.STD_ZB_INTBANK_TYPE_C12112.equals(intbankOrgType)) {
            log.info("【"+lmtIntbankApp.getSerno()+"】判断是否大额授信：4---------->");
            //大额授信判断额度：5亿
            if (new BigDecimal(500000000L).compareTo(lmtAmt) <= 0) {
                isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_Y;
                resultMap.put("isLargeLmt",isLargeLmt);
                //判断是否超两倍大额值
                if(new BigDecimal(1000000000L).compareTo(lmtAmt) <= 0){
                    isReportChairman = CmisBizConstants.STD_ZB_YES_NO_Y;
                    resultMap.put("isReportChairman",isReportChairman);
                }
            }
            return resultMap;
        }
        //授信主体类别：省内农商行（江苏省内）C121321  (1C1204:废弃) 并且 同业客户信息中注册地行政区划 是在江苏省范围内（320开头）
        if (CmisBizConstants.STD_ZB_INTBANK_TYPE_C121321.equals(intbankOrgType)) {
            //注册地行政区划
//            String regiAreaCode = lmtAppRelCusInfo.getRegiAreaCode();
            log.info("【"+lmtIntbankApp.getSerno()+"】判断是否大额授信：5 intbankOrgType【{}】---------->",intbankOrgType);
            //大额授信判断额度：5亿
            if (new BigDecimal(500000000L).compareTo(lmtAmt) <= 0) {
                isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_Y;
                resultMap.put("isLargeLmt",isLargeLmt);
                //判断是否超两倍大额值
                if(new BigDecimal(1000000000L).compareTo(lmtAmt) <= 0){
                    isReportChairman = CmisBizConstants.STD_ZB_YES_NO_Y;
                    resultMap.put("isReportChairman",isReportChairman);
                }
            }
            return resultMap;
        }

        //授信主体类别：其他同业银行类 -- 同业机构类型 以 C 开头的，不包含上述三种情况的
        if (intbankOrgType.startsWith("C")) {
            log.info("【"+lmtIntbankApp.getSerno()+"】判断是否大额授信：6 ---------->");
            //大额授信判断额度：3亿
            if (new BigDecimal(300000000L).compareTo(lmtAmt) <= 0) {
                isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_Y;
                resultMap.put("isLargeLmt",isLargeLmt);
                //判断是否超两倍大额值
                if(new BigDecimal(600000000L).compareTo(lmtAmt) <= 0){
                    isReportChairman = CmisBizConstants.STD_ZB_YES_NO_Y;
                    resultMap.put("isReportChairman",isReportChairman);
                }
            }
            return resultMap;
        }
        //授信主体类别：同业非银类  大额授信判断额度：2亿
        if (BigDecimal.valueOf(200000000L).compareTo(lmtAmt) <= 0) {
            log.info("【"+lmtIntbankApp.getSerno()+"】判断是否大额授信：7 ---------->");
            isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_Y;
            resultMap.put("isLargeLmt",isLargeLmt);
            //判断是否超两倍大额值
            if(new BigDecimal(400000000L).compareTo(lmtAmt) <= 0){
                isReportChairman = CmisBizConstants.STD_ZB_YES_NO_Y;
                resultMap.put("isReportChairman",isReportChairman);
            }
        }
        return resultMap;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * 1.判断当前申请状态是否为未发起 逻辑删除
     * 2.判断当前申请状态是否为打回，修改为自行退出
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int logicDelete(String pkId) {
        log.info("同业授信申请删除开始。。。。");
        LmtIntbankApp lmtIntbankApp = selectByPrimaryKey(pkId);
        if (lmtIntbankApp!= null){
            //1.判断当前申请状态是否为未发起 逻辑删除
            if (CmisBizConstants.APPLY_STATE_TODO.equals(lmtIntbankApp.getApproveStatus())){
                lmtIntbankApp.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                return update(lmtIntbankApp);
            }
            //2.判断当前申请状态是否为打回，修改为自行退出
            if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtIntbankApp.getApproveStatus())){
                log.info("流程删除===》BizId：{}",lmtIntbankApp.getSerno());
                workflowCoreClient.deleteByBizId(lmtIntbankApp.getSerno());

                lmtIntbankApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                return update(lmtIntbankApp);
            }
        }else{
            throw BizException.error(null,EclEnum.INTBANK_ERROR_000005.key,EclEnum.INTBANK_ERROR_000005.value);
        }
        return 0;
    }

    /**
     * @方法名称: onModify
     * @方法描述: 新增授信变更申请
     * @参数与返回说明:
     * @算法描述:
     */
    public Map onModify( Map params) throws URISyntaxException {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        //从前台获取 防止重复提交
        String serno = (String) params.get("newSerno");
        //从前台获取 防止重复提交
        String pkId = (String) params.get("newPkId");
        LmtIntbankApp lmtIntbankApp = new LmtIntbankApp();
        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccervice.selectByAccNo((String) params.get("accNo"));
        String cusId = "";
        String replySerno = "";
        // TODO 保存前校验 待补充
        try {

            cusId = lmtIntbankAcc.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if(exit){
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                throw BizException.error(null, EcbEnum.ECB010002.key, EcbEnum.ECB010002.value);
            }

            //判断当前客户是否存在正在流转的产品授信
            List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppService.selectRunningInvestListByCusId(cusId);
            if (lmtSigInvestApps!=null && lmtSigInvestApps.size()>0){
                throw BizException.error(null, EcbEnum.ECB020002.key, EcbEnum.ECB020002.value);
            }

            replySerno = lmtIntbankAcc.getReplySerno();
            LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.queryLmtReplyBySerno(replySerno);
            if(lmtIntbankReply == null){
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("保存授信变更申请数据,生成流水号%s", serno));

            //添加企业基本信息（lmt_app_rel_cus_info）
            lmtAppRelCusInfoService.insertCusInfoApp(serno, cusId, lmtIntbankAcc.getCurType(), CmisBizConstants.STD_ZB_CUS_CATALOG_3);

            //添加集团额度信息（lmt_app_rel_grp_limit）
            lmtAppRelGrpLimitService.insertCusLmtInfo(serno, cusId, lmtIntbankAcc.getCurType(), CmisBizConstants.STD_ZB_CUS_CATALOG_3);

            copyProperties(lmtIntbankReply, lmtIntbankApp);

            lmtIntbankApp.setPkId(pkId);
            // 申请流水号
            lmtIntbankApp.setSerno(serno);
            // 数据操作标志为新增
            lmtIntbankApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            lmtIntbankApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                lmtIntbankApp.setLmtType(CmisCommonConstants.LMT_TYPE_02);//授信类型为变更
                lmtIntbankApp.setOrigiLmtTerm(lmtIntbankReply.getLmtTerm());
                lmtIntbankApp.setOrigiLmtReplySerno(replySerno);
                lmtIntbankApp.setOrigiLmtAmt(lmtIntbankReply.getLmtAmt());
                lmtIntbankApp.setInputId(userInfo.getLoginCode());
                lmtIntbankApp.setInputBrId(userInfo.getOrg().getCode());

                lmtIntbankApp.setInputDate(openday);
                lmtIntbankApp.setUpdId(userInfo.getLoginCode());
                lmtIntbankApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtIntbankApp.setUpdDate(openday);
                lmtIntbankApp.setManagerId(userInfo.getLoginCode());
                lmtIntbankApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtIntbankApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtIntbankApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            //生成授信变更申请从表信息
            LmtChgDetail lmtChgDetail = new LmtChgDetail();
            lmtChgDetail.setLmtSerno(serno);
            lmtChgDetail.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtChgDetail.setInputId(userInfo.getLoginCode());
            lmtChgDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setInputDate(openday);
            lmtChgDetail.setUpdId(userInfo.getLoginCode());
            lmtChgDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setUpdDate(openday);
            lmtChgDetail.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtChgDetail.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info(String.format("新增客户授信变更数据,流水号%s", serno));

            //初始化 必填信息校验页面
            initMustCheckItem(lmtIntbankApp);

            int count = lmtIntbankAppMapper.insert(lmtIntbankApp);
            log.info(String.format("新增客户授信变更从表数据,流水号%s", serno));
            int countDetail = lmtChgDetailMapper.insert(lmtChgDetail);
            boolean result = lmtIntbankReplySubService.copyToLmtAppSub(replySerno,serno,CmisCommonConstants.WF_STATUS_000);
            if (count != 1 || countDetail != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
        } catch (BizException e) {
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } finally {
            rtnData.put("lmtIntbankApp", lmtIntbankApp);
            rtnData.put("pkId", pkId);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateLmtAmt
     * @方法描述: 更新授信金额
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateLmtAmt(String serno) {
        int result = 0;
        //分项向下额度总额
        String lmtAmt = lmtIntbankAppSubMapper.selectLmtAmtByModel(serno);
        lmtAmt = StringUtils.isBlank(lmtAmt) ? "0" : lmtAmt;
        //分项向下最大期限
        String maxTerm = lmtIntbankAppSubMapper.selectMaxTermBySerno(serno);
        maxTerm = StringUtils.isBlank(maxTerm) ? "0" : maxTerm;

        try {
            LmtIntbankApp lmtIntbankApp = selectBySerno(serno);
            if (lmtIntbankApp != null) {
                //更新授信金额
                lmtIntbankApp.setLmtTerm(Integer.parseInt(maxTerm));
                lmtIntbankApp.setLmtAmt(new BigDecimal(lmtAmt));

                Map<String, String> stringStringMap = checkIsLargeLmt(lmtIntbankApp);
                //大额授信权限权限、是否报备董事长权限
                String isLargeLmt =  stringStringMap.get("isLargeLmt");
                String isReportChairman = stringStringMap.get("isReportChairman");

                log.info("isLargeLmt==>{}", isLargeLmt);
                log.info("isReportChairman==>{}", isReportChairman);
                lmtIntbankApp.setIsLargeLmt(isLargeLmt);
                //判断是否报备董事长
                lmtIntbankApp.setIsReportChairman(isReportChairman);

                /**
                 * 审批权限
                 *   判断这笔申请数据是不是正在信贷管理部负责人权限内，若是，则审批模式为51-信贷管理部负责人权限；
                 *   判断申请数据有没有在信贷管理部分管行长权限内，若在，则审批模式为52-信贷管理部分管行长权限
                 *   若不在，则为53-投委会权限;
                 */
                lmtAppRelCusInfoService.setTmpLmtAmt(new BigDecimal(lmtAmt));
                //流程路由3：是否信贷管理部负责人权限范围内
                int rightXDGL = lmtAppRelCusInfoService.rightJudgeXDGL(serno,lmtIntbankApp.getCusId());

                //流程路由5：是否分管行长权限
                int rightFHZ = 0;
                if(rightXDGL == 0){
                    rightFHZ = lmtAppRelCusInfoService.rightJudgeFHZ(serno,lmtIntbankApp.getCusId());
                }

                //审批权限
                String approveMode = "";
                if(rightXDGL == 1 ){
                    approveMode = CmisBizConstants.STD_APPR_MODE_51;
                }else if(rightFHZ == 1 ){
                    approveMode = CmisBizConstants.STD_APPR_MODE_52;
                }else{
                    approveMode = CmisBizConstants.STD_APPR_MODE_53;
                }
                lmtIntbankApp.setApprMode(approveMode);
                log.info("申请流水号【{}】，对应的审批权限是【{}】",lmtIntbankApp.getSerno(),approveMode);

                return lmtIntbankAppMapper.updateByPrimaryKey(lmtIntbankApp);
            }else{
                result = 0;
            }
        } catch (NumberFormatException e) {
            log.error("更新授信金额和大额授信失败，申请流水号【{}】，报错信息{}", serno, e);
        }
        if (result == 0){
            throw BizException.error(null,EclEnum.ECL070050.key,EclEnum.ECL070050.value);
        }
        return 0;
    }

    /**
     * 同业授信申请(通过、否决 录入批复表中)
     * @param lmtIntbankApp
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleAfterEnd(LmtIntbankApp lmtIntbankApp, String currentUserId, String currentOrgId, String approveStatus, ResultInstanceDto resultInstanceDto) throws Exception {
        if(CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
            this.flowPassHandleAfterEnd(lmtIntbankApp, currentUserId, currentOrgId) ;
            //推送首页提醒事项 add by zhangjw 20210630
            this.sendWbMsgNotice(lmtIntbankApp,CmisBizConstants.STD_WB_NOTICE_TYPE_2,
                    resultInstanceDto.getComment().getUserComment(),resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"通过");
        }else{
            this.flowRefuseHandleAfterEnd(lmtIntbankApp, currentUserId, currentOrgId) ;
            //推送首页提醒事项 add by zhangjw 20210630
            this.sendWbMsgNotice(lmtIntbankApp,CmisBizConstants.STD_WB_NOTICE_TYPE_3,
                    null,resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"否决");
        }
        //更新审批状态
        lmtIntbankApp.setApproveStatus(approveStatus);
        this.update(lmtIntbankApp);

    }


    /**
     * 同业授信申请(同意 录入批复表台账表中)
     * @param lmtIntbankApp
     * @param currentUserId
     * @param currentOrgId
     */
    @Transactional(rollbackFor=Exception.class)
    public void flowPassHandleAfterEnd(LmtIntbankApp lmtIntbankApp, String currentUserId, String currentOrgId) throws Exception {
        List<LmtIntbankAccSub> lmtIntbankAccSubs = new ArrayList<LmtIntbankAccSub>() ;
        //获取本次申请类型
        //01 授信新增 02 授信变更 03 授信续作 05 授信复议
        String lmtType = lmtIntbankApp.getLmtType() ;

        //如果是授信复议，则获取真正的授信类型
        if(CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(lmtType)){
            lmtType = bizCommonService.getLmtIntbankRelLmtType(lmtIntbankApp);
        }

        //授信复议，授信续作，原协议对象的台账信息更新为失效已结清
        if(CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(lmtType)){
            //根据原批复编号，查找对应的台账主表信息以及台账分项信息，并且更改其状态
            QueryModel model = new QueryModel();
            model.addCondition("cusId",lmtIntbankApp.getCusId());
            BizInvestCommonService.checkParamsIsNull("cusId",lmtIntbankApp.getCusId());
            model.addCondition("accStatus",CmisBizConstants.STD_REPLY_STATUS_01);
            model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
            List<LmtIntbankAcc> lmtIntbankAccs = lmtIntbankAccService.selectByModel(model);
            log.info("续作失效老批复信息：【{}】", JSON.toJSONString(lmtIntbankAccs));
            if(lmtIntbankAccs!=null && lmtIntbankAccs.size()>0){
                LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccs.get(0);
                lmtIntbankAcc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_02);
                lmtIntbankAccMapper.updateByPrimaryKey(lmtIntbankAcc) ;
            }
        }

        //获取生申请流水号
        String serno = lmtIntbankApp.getSerno() ;
        //根据申请流水号查询最新的
        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.selectLmtIntBankApprApprBySerno(serno) ;

        /**从同业授信申请审批表 中产生批复表和台账表
         * 根据同业授信申请审批表 生成 同业授信批复表（lmt_intbank_reply） 同业授信台账表（lmt_intbank_acc） 信息**/
        if(lmtIntbankAppr ==null){
            throw new Exception(EclEnum.ECL070077.value) ;
        }

        //生成批复主表信息
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.initLmtIntBankRelyInfo(lmtIntbankAppr) ;
        lmtIntbankReplyMapper.insert(lmtIntbankReply) ;

        //add by zhangjw 20210725 增加失效原批复信息
        LmtIntbankReply origiLmtIntbankReply = lmtIntbankReplyService.selectByReplySerno(lmtIntbankApp.getOrigiLmtReplySerno());
        if(origiLmtIntbankReply!=null){
            origiLmtIntbankReply.setReplyStatus(CmisBizConstants.STD_REPLY_STATUS_02);
            lmtIntbankReplyService.update(origiLmtIntbankReply);
        }


        //生成同业台账主表信息  -- 如果是复议，则使用真正的申请类型
        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.initLmtIntbankAccInfo(lmtIntbankAppr,lmtType) ;
        //批复编号
        String replySerno = lmtIntbankReply.getReplySerno() ;
        //台账表中更新批复编号
        lmtIntbankAcc.setReplySerno(replySerno);
        if(CmisLmtConstants.STD_SX_LMT_TYPE_02.equals(lmtType)){
            lmtIntbankAccMapper.updateByPrimaryKey(lmtIntbankAcc) ;
        }else{
            lmtIntbankAccMapper.insert(lmtIntbankAcc) ;

            //增加原台帐失效
            LmtIntbankAcc origiLmtIntbankAcc = lmtIntbankAccService.selectByReplyNo(lmtIntbankApp.getOrigiLmtReplySerno());
            if(origiLmtIntbankAcc!=null){
                origiLmtIntbankAcc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_02);
                lmtIntbankAccService.update(origiLmtIntbankAcc);
            }
        }

        /** 根据盛情流水号获取同业授信分项信息，同时生成批复分项（lmt_intbank_reply_sub）和 台账分项信息 （lmt_intbank_acc_sub）**/
        //台账编号
        String accNo = lmtIntbankAcc.getAccNo() ;
        //根据审批流水号获取分项信息
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("approveSerno",lmtIntbankAppr.getApproveSerno());
        BizInvestCommonService.checkParamsIsNull("approveSerno",lmtIntbankAppr.getApproveSerno());
        //删除的处理不了
        //queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtIntbankApprSub> lmtIntbankApprSubs = lmtIntbankApprSubService.selectByModel(queryModel) ;

        if (lmtIntbankApprSubs!=null && lmtIntbankApprSubs.size()>0){
            for (LmtIntbankApprSub lmtIntbankApprSub : lmtIntbankApprSubs) {
                LmtIntbankReplySub lmtIntbankReplySub = lmtIntbankReplySubService.initLmtIntbankReplySubInfo(lmtIntbankApprSub,lmtIntbankReply) ;
                lmtIntbankReplySub.setReplySerno(replySerno);
                lmtIntbankReplySubMapper.insert(lmtIntbankReplySub) ;

                /**************处理台账分项信息*****************/
                //操作类型
                String oprType = lmtIntbankApprSub.getOprType() ;
                //原批复分项编号
                String origiLmtAccSubNo = lmtIntbankApprSub.getOrigiLmtAccSubNo() ;
                //针对申请中，新增了分项信息又删除了该分项信息的数据，我们不拷贝进入台账分项表中
                if(CmisLmtConstants.OPR_TYPE_DELETE.equals(oprType) && StringUtils.isBlank(origiLmtAccSubNo)){
                    continue ;
                }
                LmtIntbankAccSub lmtIntbankAccSub = lmtIntbankAccSubService.initLmtIntbankAccSubInfo(lmtIntbankApprSub, lmtType) ;
                //台账编号
                lmtIntbankAccSub.setAccNo(accNo);
                //批复流水号
                lmtIntbankAccSub.setReplySerno(replySerno);
                //批复分项流水号
                lmtIntbankAccSub.setReplySubSerno(lmtIntbankReplySub.getReplySubSerno());
                //新增操作，变更操作新增处理
                if(StringUtils.nonBlank(origiLmtAccSubNo) && CmisLmtConstants.STD_SX_LMT_TYPE_02.equals(lmtType)){
                    lmtIntbankAccSubMapper.updateByPrimaryKey(lmtIntbankAccSub) ;
                }else{
                    lmtIntbankAccSubMapper.insert(lmtIntbankAccSub) ;
                }
                lmtIntbankAccSubs.add(lmtIntbankAccSub) ;
            }
        }

        /**
         * 拷贝用信条件
         */
        List<LmtApprLoanCond> lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByApprSerno(lmtIntbankAppr.getApproveSerno());
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtApprLoanConds)){
            for (LmtApprLoanCond lmtApprLoanCond : lmtApprLoanConds) {
                //生成批复信息
                LmtReplyLoanCond lmtReplyLoanCond = new LmtReplyLoanCond();
                BeanUtils.copyProperties(lmtApprLoanCond, lmtReplyLoanCond);
                lmtReplyLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyLoanCond.setReplySerno(replySerno);
                lmtReplyLoanCondService.insert(lmtReplyLoanCond);

                //生成台账信息
                LmtReplyAccLoanCond lmtReplyAccLoanCond = new LmtReplyAccLoanCond();
                BeanUtils.copyProperties(lmtReplyLoanCond, lmtReplyAccLoanCond);
                lmtReplyAccLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyAccLoanCond.setAccNo(accNo);
                lmtReplyAccLoanCondService.insert(lmtReplyAccLoanCond);

                log.info("生成其他条件信息:" + lmtReplyLoanCond.toString());
            }
        }

        lmtIntbankAccService.lmtIntbankSendCmisLmt0003(serno,lmtType);
    }

    /**
     * 同业授信申请(否决 录入批复表中)
     * @param lmtIntbankApp
     * @param currentUserId
     * @param currentOrgId
     */
    @Transactional(rollbackFor=Exception.class)
    public void flowRefuseHandleAfterEnd(LmtIntbankApp lmtIntbankApp, String currentUserId, String currentOrgId) throws Exception {
        //获取生申请流水号
        String serno = lmtIntbankApp.getSerno() ;
        //根据申请流水号查询最新的
        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.selectLmtIntBankApprApprBySerno(serno) ;
        Object entityBean = null ;

        /**同业授信申请审批表 则证明该申请中无 审批表信息， 数据从申请表中产生
         * 根据同业授信申请审批表 生成 同业授信批复表（lmt_intbank_reply） 同业授信台账表（lmt_intbank_acc） 信息**/
        if(lmtIntbankAppr == null){
            throw new Exception(EclEnum.ECL070077.value) ;
        }
        //生成批复主表信息
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.initLmtIntBankRelyInfo(lmtIntbankAppr) ;
        lmtIntbankReplyMapper.insert(lmtIntbankReply) ;

        /** 根据流水号获取同业授信分项信息，同时生成批复分项（lmt_intbank_reply_sub）和 台账分项信息 （lmt_intbank_acc_sub）**/
        //根据流水号获取分项信息
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtIntbankApprSub> lmtIntbankApprSubs = lmtIntbankApprSubService.selectByModel(queryModel) ;
        if(lmtIntbankApprSubs != null && lmtIntbankApprSubs.size()>0){
            //便利审批表中的分项信息，更具分项信息生成批复信息
            for (LmtIntbankApprSub lmtIntbankApprSub : lmtIntbankApprSubs) {
                LmtIntbankReplySub lmtIntbankReplySub = lmtIntbankReplySubService.initLmtIntbankReplySubInfo(lmtIntbankApprSub,lmtIntbankReply) ;
                lmtIntbankReplySubMapper.insert(lmtIntbankReplySub) ;
            }
        }

    }


    /**
     * 流程开始，将数据拷贝申请数据拷贝到审批表中
     * @param lmtIntbankApp
     * @param varParam -审批路由结果
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleAfterStart(LmtIntbankApp lmtIntbankApp, String issueReportType, Map<String,Object> varParam) throws Exception {
        //数据拷贝，将申请数据拷贝到审批表内中
        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.initLmtIntbankApprInfo(lmtIntbankApp) ;
        //出具报告类型
        lmtIntbankAppr.setIssueReportType(issueReportType);
        //终身机构 -- 授信终审机构类型 03 总行
        lmtIntbankAppr.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);

         /**
         *  判断这笔申请数据是不是正在信贷管理部负责人权限内，若是，则审批模式为51-信贷管理部负责人权限；
         *                  *  判断申请数据有没有在信贷管理部分管行长权限内，若在，则审批模式为52-信贷管理部分管行长权限
         *                  *  若不在，则为53-投委会权限;
         */
        String approveMode = "";
        Integer rightXDGL = (Integer)varParam.get("rightXDGL");
        Integer rightFHZ = (Integer)varParam.get("rightFHZ");

        if(rightXDGL == 1 ){
            approveMode = CmisBizConstants.STD_APPR_MODE_51;
        }else if(rightFHZ == 1 ){
            approveMode = CmisBizConstants.STD_APPR_MODE_52;
        }else{
            approveMode = CmisBizConstants.STD_APPR_MODE_53;
        }
        lmtIntbankAppr.setApprMode(approveMode);
        log.info("申请流水号【{}】，对应的审批权限是【{}】",lmtIntbankApp.getSerno(),approveMode);

        //是否大额授信
        String isLargeLmt = (String)varParam.get("isLargeLmt");
        lmtIntbankAppr.setIsLargeLmt(isLargeLmt);

        lmtIntbankApprService.insert(lmtIntbankAppr) ;

        //客户编号
        String cusId = lmtIntbankAppr.getCusId() ;
        //客户名称
        String cusName = lmtIntbankAppr.getCusName() ;
        //审批流水号
        String approveSerno = lmtIntbankAppr.getApproveSerno() ;
        //处理分项表数据
        List<LmtIntbankAppSub> lmtIntbankApprSubList = lmtIntbankAppSubService.selectUnOptTypeBySerno(lmtIntbankApp.getSerno()) ;
        if (lmtIntbankApprSubList != null && lmtIntbankApprSubList.size()>0){
            //遍历分项申请信息，生成分项批复信息
            for (LmtIntbankAppSub lmtIntbankAppSub : lmtIntbankApprSubList) {
                LmtIntbankApprSub lmtIntbankApprSub = lmtIntbankApprSubService.initLmtIntbankApprSubInfo(lmtIntbankAppSub) ;
                //申请分项表中无客户号客户名称，此处客户号客户名称从审批中表中获取赋值
                lmtIntbankApprSub.setCusId(cusId);
                lmtIntbankApprSub.setCusName(cusName);
                //审批流水号
                lmtIntbankApprSub.setApproveSerno(approveSerno);
                //数据落地
                lmtIntbankApprSubService.insert(lmtIntbankApprSub) ;
            }
        }
        lmtIntbankApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
        this.update(lmtIntbankApp);
    }

    /**
     * 审批打回至客户经理处，将审批表数据copy至申请表
     * @param lmtIntbankApp
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterCallBack(LmtIntbankApp lmtIntbankApp, String currentUserId, String currentOrgId, String approveStatus) throws Exception{

        /**
         * 20210720张静雯修改：打回至客户经理处，不将审批表数据更新至申请表数据中
         */
        //获取生申请流水号
//        String serno = lmtIntbankApp.getSerno() ;
//        //根据申请流水号查询最新的
//        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.selectLmtIntBankApprApprBySerno(serno) ;
//
//        /**从同业授信申请审批表 中产生批复表和台账表
//         * 根据同业授信申请审批表 生成 同业授信批复表（lmt_intbank_reply） 同业授信台账表（lmt_intbank_acc） 信息**/
//        if(lmtIntbankAppr ==null){
//            throw new Exception(EclEnum.ECL070077.value) ;
//        }
//
//        copyProperties(lmtIntbankAppr,lmtIntbankApp,"pkId");
//        //初始化（更新）通用domain信息
//        initUpdateDomainProperties(lmtIntbankApp);


        //打回，更新申请表信息，审批状态更新为【打回-992】 状态
        lmtIntbankApp.setApproveStatus(approveStatus);
        update(lmtIntbankApp);

//        //删除申请表原子项信息
//        List<LmtIntbankAppSub> lmtIntbankApprSubList = lmtIntbankAppSubService.selectSubListBySerno(lmtIntbankApp.getSerno()) ;
//        if (lmtIntbankApprSubList != null && lmtIntbankApprSubList.size()>0){
//            for (LmtIntbankAppSub lmtIntbankAppSub : lmtIntbankApprSubList) {
//                //逻辑删除
//                lmtIntbankAppSub.setOprType(CmisBizConstants.OPR_TYPE_02);
//                lmtIntbankAppSubService.update(lmtIntbankAppSub);
//            }
//        }
//
//        //从审批表子项中重新获取最新的
//        //根据流水号获取分项信息
//        QueryModel queryModel = new QueryModel() ;
//        queryModel.addCondition("serno",serno);
//        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
//        queryModel.addCondition("approveSerno",lmtIntbankAppr.getApproveSerno());
//        List<LmtIntbankApprSub> lmtIntbankApprSubs = lmtIntbankApprSubService.selectByModel(queryModel);
//        if(lmtIntbankApprSubs != null && lmtIntbankApprSubs.size()>0){
//            for (LmtIntbankApprSub lmtIntbankApprSub : lmtIntbankApprSubs) {
//                LmtIntbankAppSub lmtIntbankAppSub = new LmtIntbankAppSub();
//                copyProperties(lmtIntbankApprSub,lmtIntbankAppSub);
//                //初始化(新增)通用domain信息
//                initInsertDomainProperties(lmtIntbankAppSub);
//                lmtIntbankAppSubService.insert(lmtIntbankAppSub);
//            }
//        }
    }




    /**
     * 推送首页提醒事项
     * @param lmtIntbankApp,messageType,comment,inputId,inputBrId
     * add by zhangjw 20210630
     */
    @Transactional(rollbackFor=Exception.class)
    public void sendWbMsgNotice(LmtIntbankApp lmtIntbankApp,String messageType,String comment,String inputId,String inputBrId,String result ) throws Exception {

        log.info("同业授信申报审批流程申请启用授信申报审批流程【{}】，流程打回或退回操作，推送首页提升事项", lmtIntbankApp.getSerno());

        Map<String, String> map = new HashMap<>();
        map.put("cusName", lmtIntbankApp.getCusName());
        map.put("prdName", "同业授信申报");
        map.put("result", result);
        ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(lmtIntbankApp.getManagerId());
        sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1",lmtIntbankApp.getManagerId(),byLoginCode.getData().getUserMobilephone());
    }

    /**
     * @方法名称: getRouterMapResult
     * @方法描述: 获取路由结果集-同业授信申报审批流程
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: zhangjw
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getRouterMapResult(ResultInstanceDto resultInstanceDto,String serno) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        LmtIntbankApp lmtIntbankApp = this.selectBySerno(serno);

        //流程路由1：发起人岗位判断（不需要）

        //流程路由2：金融市场总部风险合规部负责人是否等于金融市场总部总裁
        String isEqualJRSCBZC = commonService.getIsEqualJRSCBZC();
        resultMap.put("isEqualJRSCBZC", isEqualJRSCBZC);

        //流程路由3：是否信贷管理部负责人权限范围内
        int rightXDGL = lmtAppRelCusInfoService.rightJudgeXDGL(resultInstanceDto.getBizId(),resultInstanceDto.getBizUserId());
        resultMap.put("rightXDGL",rightXDGL);

        //流程路由4：是否上调权限
        resultMap.put("isUpAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.selectLastBySerno(serno);
        if(lmtIntbankAppr!=null){
            String isUpAppr = lmtIntbankAppr.getIsUpperApprAuth() == null ? "0" : lmtIntbankAppr.getIsUpperApprAuth();
            resultMap.put("isUpAppr", isUpAppr);
        }

        //流程路由5：是否分管行长权限
        int rightFHZ = 0;
        if(rightXDGL == 0){
            rightFHZ = lmtAppRelCusInfoService.rightJudgeFHZ(resultInstanceDto.getBizId(),resultInstanceDto.getBizUserId());
        }
        resultMap.put("rightFHZ",rightFHZ);

        //流程路由6：是否大额授信
        resultMap.put("isLargeLmt", CmisCommonConstants.STD_ZB_YES_NO_0);
        Map<String,String> isLargeLmtMap = this.checkIsLargeLmt(lmtIntbankApp);
        resultMap.put("isLargeLmt", isLargeLmtMap.get("isLargeLmt"));

        //流程路由7：是否关联方交易  获取系统参数配置，我行资本净额  (参考LmtAppService.java  2288行)
        // 是否关联审批
        // 是否关联交易
        String isGLAppr = bizCommonService.checkCusIsGlf(lmtIntbankApp.getCusId());
        resultMap.put("isGLAppr", isGLAppr);

        // 如果是关联交易
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGLAppr)){
            Map<String,Object> gljyMap = bizCommonService.calGljyType(lmtIntbankApp.getCusId(),lmtIntbankApp.getLmtAmt(),BigDecimal.ZERO,BigDecimal.ZERO,CmisBizConstants.STD_ZB_INSTU_CDE_ZJG);

            BigDecimal newLmtRelRate = (BigDecimal)gljyMap.get("newLmtRelRate");
            BigDecimal allLmtRelRate = (BigDecimal)gljyMap.get("allLmtRelRate");
            String isSupGLFLmtAmt = (String)gljyMap.get("isSupGLFLmtAmt");

            resultMap.put("newLmtRelRate",newLmtRelRate);//单笔拟申报金额比例
            resultMap.put("allLmtRelRate",allLmtRelRate);//关联方贷款余额（存量敞口）+拟申报授信金额之和占我行资本净额
            resultMap.put("isSupGLFLmtAmt",isSupGLFLmtAmt);//是否超过关联方预计额度
            //关联交易类型
            String gLType = bizCommonService.getGLType(gljyMap);
            resultMap.put("gLType",gLType);
        }

        //流程路由8：是否超两倍大额标准值（是否报备董事长）
        resultMap.put("isReportChairman", isLargeLmtMap.get("isReportChairman"));

        //流程路由9：是否同时开展信贷业务  同业客户不会存在信贷业务
        resultMap.put("isHasLoanBuss", CmisCommonConstants.STD_ZB_YES_NO_0);

        return resultMap;
    }

    /**
     * @方法名称: findRealLmtType
     * @方法描述: 根据授信批复查询原始批复的授信类型
     * @参数与返回说明:
     * @算法描述: 递归查询最近一笔授信类型不为复议的数据
     * @创建人: zhangjw
     * @创建时间: 2021-07-22
     * @修改记录: 修改时间    修改人员    修改原因
     *
     */
    public String findRealLmtType(LmtIntbankReply lmtIntbankReply) throws Exception {
        LmtIntbankReply reply = lmtIntbankReplyService.selectByReplySerno(lmtIntbankReply.getOrigiLmtReplySerno());
        if(reply != null){
            if(CmisCommonConstants.LMT_TYPE_05.equals(reply.getLmtType())){
                LmtIntbankApp lmtIntbankApp = this.selectBySerno(reply.getSerno());
                if(CmisBizConstants.APPLY_STATE_PASS.equals(lmtIntbankApp.getApproveStatus())){
                    return CmisCommonConstants.LMT_TYPE_02;
                }else{
                    findRealLmtType(reply);
                }
            }else{
                return reply.getLmtType();
            }
        }else{
            throw new Exception("授信批复查询异常!");
        }
        return null;
    }

    /**
     *
     * @param serno
     * @return
     */
    public Map getLargeLmtInfo(String serno) {

        return null;
    }

    /**
     * 更新图片路径
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        LmtIntbankApp lmtIntbankApp = new LmtIntbankApp();
        lmtIntbankApp.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/" + getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)) {
            picAbsolutePath = getFileAbsolutePath(fileId, serverPath, relativePath, fanruanFileTemplate);
        }
        setValByKey(lmtIntbankApp, key, picAbsolutePath);
        updateSelective(lmtIntbankApp);
        return picAbsolutePath;
    }

    /**
     * 更新主体分析
     * @param lmtIntbankApp
     * @return
     */
    public int updateZtfx(LmtIntbankApp lmtIntbankApp) {
        String serno = lmtIntbankApp.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"ztfx");
        return updateSelective(lmtIntbankApp);
    }

    /**
     * 获取客户在途的同业授信金额
     * @return
     */
    public BigDecimal getIntbankZtAmt(String cusId){
        BigDecimal ztInvestLmtAmt = lmtIntbankAppMapper.getIntbankZtAmt(cusId);
        return ztInvestLmtAmt;
    }
}
