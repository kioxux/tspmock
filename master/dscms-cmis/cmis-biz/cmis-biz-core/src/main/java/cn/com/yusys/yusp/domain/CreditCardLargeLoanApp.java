/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardLargeLoanApp
 * @类描述: credit_card_large_loan_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-31 21:16:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_card_large_loan_app")
public class CreditCardLargeLoanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 5)
	private String appChnl;
	
	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 卡号 **/
	@Column(name = "CARD_NO", unique = false, nullable = true, length = 40)
	private String cardNo;
	
	/** 分期计划 **/
	@Column(name = "LOAN_PLAN", unique = false, nullable = true, length = 5)
	private String loanPlan;
	
	/** 分期金额 **/
	@Column(name = "LOAN_AMOUNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmount;
	
	/** 分期期数 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 5)
	private String loanTerm;
	
	/** 放款方式 **/
	@Column(name = "SEND_MODE", unique = false, nullable = true, length = 5)
	private String sendMode;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 分期手续费收取方式 **/
	@Column(name = "LOAN_FEE_METHOD", unique = false, nullable = true, length = 5)
	private String loanFeeMethod;
	
	/** 分期本金分配方式 **/
	@Column(name = "LOAN_PRIN_DIST_METHOD", unique = false, nullable = true, length = 5)
	private String loanPrinDistMethod;
	
	/** 分期手续费计算方式 **/
	@Column(name = "LOAN_FEE_CALC_METHOD", unique = false, nullable = true, length = 5)
	private String loanFeeCalcMethod;
	
	/** 分期手续费比例 **/
	@Column(name = "LOAN_FEE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanFeeRate;
	
	/** 分期放款账户对公/对私标识 **/
	@Column(name = "LOANR_TARGET", unique = false, nullable = true, length = 5)
	private String loanrTarget;
	
	/** 分期放款开户行号 **/
	@Column(name = "DD_BANK_BRANCH", unique = false, nullable = true, length = 40)
	private String ddBankBranch;
	
	/** 分期放款银行名称 **/
	@Column(name = "DD_BANK_NAME", unique = false, nullable = true, length = 80)
	private String ddBankName;
	
	/** 分期放款账号 **/
	@Column(name = "DD_BANK_ACC_NO", unique = false, nullable = true, length = 40)
	private String ddBankAccNo;
	
	/** 分期放款账户姓名 **/
	@Column(name = "DD_BANK_ACC_NAME", unique = false, nullable = true, length = 80)
	private String ddBankAccName;
	
	/** 放款账户移动电话 **/
	@Column(name = "DISB_ACCT_PHONE", unique = false, nullable = true, length = 20)
	private String disbAcctPhone;
	
	/** 放款账户证件类型 **/
	@Column(name = "DISB_ACCT_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String disbAcctCertType;
	
	/** 放款账户证件号码 **/
	@Column(name = "DISB_ACCT_CERT_CODE", unique = false, nullable = true, length = 20)
	private String disbAcctCertCode;
	
	/** 资金用途 **/
	@Column(name = "PAYMENT_PURPOSE", unique = false, nullable = true, length = 5)
	private String paymentPurpose;
	
	/** 分期折算近似年化利率 **/
	@Column(name = "YEAR_INTEREST_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearInterestRate;
	
	/** 分期营销客户经理号 **/
	@Column(name = "SALES_MAN_NO", unique = false, nullable = true, length = 20)
	private String salesManNo;
	
	/** 分期营销人员姓名 **/
	@Column(name = "SALES_MAN", unique = false, nullable = true, length = 80)
	private String salesMan;
	
	/** 分期营销人员手机号 **/
	@Column(name = "SALES_MAN_PHONE", unique = false, nullable = true, length = 20)
	private String salesManPhone;
	
	/** 分期营销人员所属支行 **/
	@Column(name = "SALES_MAN_OWING_BRANCH", unique = false, nullable = true, length = 40)
	private String salesManOwingBranch;
	
	/** 征信授权日期 **/
	@Column(name = "CREDIT_AUTH_DATE", unique = false, nullable = true, length = 20)
	private String creditAuthDate;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 分期申请号 **/
	@Column(name = "REGISTER_ID", unique = false, nullable = true, length = 40)
	private String registerId;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 80)
	private String imageNo;
	
	/** 试算状态 **/
	@Column(name = "SHISUAN_STATUS", unique = false, nullable = true, length = 5)
	private String shisuanStatus;
	
	/** 征信状态 **/
	@Column(name = "ZHENGXIN_STATUS", unique = false, nullable = true, length = 5)
	private String zhengxinStatus;
	
	/** 推荐人工号 **/
	@Column(name = "RECOM_ID", unique = false, nullable = true, length = 20)
	private String recomId;
	
	/** 推荐人名称 **/
	@Column(name = "RECOM_NAME", unique = false, nullable = true, length = 80)
	private String recomName;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 管护客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 管护机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cardNo
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
    /**
     * @return cardNo
     */
	public String getCardNo() {
		return this.cardNo;
	}
	
	/**
	 * @param loanPlan
	 */
	public void setLoanPlan(String loanPlan) {
		this.loanPlan = loanPlan;
	}
	
    /**
     * @return loanPlan
     */
	public String getLoanPlan() {
		return this.loanPlan;
	}
	
	/**
	 * @param loanAmount
	 */
	public void setLoanAmount(java.math.BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}
	
    /**
     * @return loanAmount
     */
	public java.math.BigDecimal getLoanAmount() {
		return this.loanAmount;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param sendMode
	 */
	public void setSendMode(String sendMode) {
		this.sendMode = sendMode;
	}
	
    /**
     * @return sendMode
     */
	public String getSendMode() {
		return this.sendMode;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param loanFeeMethod
	 */
	public void setLoanFeeMethod(String loanFeeMethod) {
		this.loanFeeMethod = loanFeeMethod;
	}
	
    /**
     * @return loanFeeMethod
     */
	public String getLoanFeeMethod() {
		return this.loanFeeMethod;
	}
	
	/**
	 * @param loanPrinDistMethod
	 */
	public void setLoanPrinDistMethod(String loanPrinDistMethod) {
		this.loanPrinDistMethod = loanPrinDistMethod;
	}
	
    /**
     * @return loanPrinDistMethod
     */
	public String getLoanPrinDistMethod() {
		return this.loanPrinDistMethod;
	}
	
	/**
	 * @param loanFeeCalcMethod
	 */
	public void setLoanFeeCalcMethod(String loanFeeCalcMethod) {
		this.loanFeeCalcMethod = loanFeeCalcMethod;
	}
	
    /**
     * @return loanFeeCalcMethod
     */
	public String getLoanFeeCalcMethod() {
		return this.loanFeeCalcMethod;
	}
	
	/**
	 * @param loanFeeRate
	 */
	public void setLoanFeeRate(java.math.BigDecimal loanFeeRate) {
		this.loanFeeRate = loanFeeRate;
	}
	
    /**
     * @return loanFeeRate
     */
	public java.math.BigDecimal getLoanFeeRate() {
		return this.loanFeeRate;
	}
	
	/**
	 * @param loanrTarget
	 */
	public void setLoanrTarget(String loanrTarget) {
		this.loanrTarget = loanrTarget;
	}
	
    /**
     * @return loanrTarget
     */
	public String getLoanrTarget() {
		return this.loanrTarget;
	}
	
	/**
	 * @param ddBankBranch
	 */
	public void setDdBankBranch(String ddBankBranch) {
		this.ddBankBranch = ddBankBranch;
	}
	
    /**
     * @return ddBankBranch
     */
	public String getDdBankBranch() {
		return this.ddBankBranch;
	}
	
	/**
	 * @param ddBankName
	 */
	public void setDdBankName(String ddBankName) {
		this.ddBankName = ddBankName;
	}
	
    /**
     * @return ddBankName
     */
	public String getDdBankName() {
		return this.ddBankName;
	}
	
	/**
	 * @param ddBankAccNo
	 */
	public void setDdBankAccNo(String ddBankAccNo) {
		this.ddBankAccNo = ddBankAccNo;
	}
	
    /**
     * @return ddBankAccNo
     */
	public String getDdBankAccNo() {
		return this.ddBankAccNo;
	}
	
	/**
	 * @param ddBankAccName
	 */
	public void setDdBankAccName(String ddBankAccName) {
		this.ddBankAccName = ddBankAccName;
	}
	
    /**
     * @return ddBankAccName
     */
	public String getDdBankAccName() {
		return this.ddBankAccName;
	}
	
	/**
	 * @param disbAcctPhone
	 */
	public void setDisbAcctPhone(String disbAcctPhone) {
		this.disbAcctPhone = disbAcctPhone;
	}
	
    /**
     * @return disbAcctPhone
     */
	public String getDisbAcctPhone() {
		return this.disbAcctPhone;
	}
	
	/**
	 * @param disbAcctCertType
	 */
	public void setDisbAcctCertType(String disbAcctCertType) {
		this.disbAcctCertType = disbAcctCertType;
	}
	
    /**
     * @return disbAcctCertType
     */
	public String getDisbAcctCertType() {
		return this.disbAcctCertType;
	}
	
	/**
	 * @param disbAcctCertCode
	 */
	public void setDisbAcctCertCode(String disbAcctCertCode) {
		this.disbAcctCertCode = disbAcctCertCode;
	}
	
    /**
     * @return disbAcctCertCode
     */
	public String getDisbAcctCertCode() {
		return this.disbAcctCertCode;
	}
	
	/**
	 * @param paymentPurpose
	 */
	public void setPaymentPurpose(String paymentPurpose) {
		this.paymentPurpose = paymentPurpose;
	}
	
    /**
     * @return paymentPurpose
     */
	public String getPaymentPurpose() {
		return this.paymentPurpose;
	}
	
	/**
	 * @param yearInterestRate
	 */
	public void setYearInterestRate(java.math.BigDecimal yearInterestRate) {
		this.yearInterestRate = yearInterestRate;
	}
	
    /**
     * @return yearInterestRate
     */
	public java.math.BigDecimal getYearInterestRate() {
		return this.yearInterestRate;
	}
	
	/**
	 * @param salesManNo
	 */
	public void setSalesManNo(String salesManNo) {
		this.salesManNo = salesManNo;
	}
	
    /**
     * @return salesManNo
     */
	public String getSalesManNo() {
		return this.salesManNo;
	}
	
	/**
	 * @param salesMan
	 */
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	
    /**
     * @return salesMan
     */
	public String getSalesMan() {
		return this.salesMan;
	}
	
	/**
	 * @param salesManPhone
	 */
	public void setSalesManPhone(String salesManPhone) {
		this.salesManPhone = salesManPhone;
	}
	
    /**
     * @return salesManPhone
     */
	public String getSalesManPhone() {
		return this.salesManPhone;
	}
	
	/**
	 * @param salesManOwingBranch
	 */
	public void setSalesManOwingBranch(String salesManOwingBranch) {
		this.salesManOwingBranch = salesManOwingBranch;
	}
	
    /**
     * @return salesManOwingBranch
     */
	public String getSalesManOwingBranch() {
		return this.salesManOwingBranch;
	}
	
	/**
	 * @param creditAuthDate
	 */
	public void setCreditAuthDate(String creditAuthDate) {
		this.creditAuthDate = creditAuthDate;
	}
	
    /**
     * @return creditAuthDate
     */
	public String getCreditAuthDate() {
		return this.creditAuthDate;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param registerId
	 */
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	
    /**
     * @return registerId
     */
	public String getRegisterId() {
		return this.registerId;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param shisuanStatus
	 */
	public void setShisuanStatus(String shisuanStatus) {
		this.shisuanStatus = shisuanStatus;
	}
	
    /**
     * @return shisuanStatus
     */
	public String getShisuanStatus() {
		return this.shisuanStatus;
	}
	
	/**
	 * @param zhengxinStatus
	 */
	public void setZhengxinStatus(String zhengxinStatus) {
		this.zhengxinStatus = zhengxinStatus;
	}
	
    /**
     * @return zhengxinStatus
     */
	public String getZhengxinStatus() {
		return this.zhengxinStatus;
	}
	
	/**
	 * @param recomId
	 */
	public void setRecomId(String recomId) {
		this.recomId = recomId;
	}
	
    /**
     * @return recomId
     */
	public String getRecomId() {
		return this.recomId;
	}
	
	/**
	 * @param recomName
	 */
	public void setRecomName(String recomName) {
		this.recomName = recomName;
	}
	
    /**
     * @return recomName
     */
	public String getRecomName() {
		return this.recomName;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}