/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.dto.BusContInfoDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.resp.Xdsx0023DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrCvrgContMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 16:08:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrCvrgContMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无selectByCvrgSernoKey
     */
    
    CtrCvrgCont selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrCvrgCont> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrCvrgCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrCvrgCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrCvrgCont record);

    /**
     * 注销
     * @param record
     * @return
     */
    int updateByPrimaryKeylogout(CtrCvrgCont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrCvrgCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByCvrgSernoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrCvrgCont selectByCvrgSernoKey(@Param("serno") String serno);

    /**
     * 查询保函合同列表
     * @param param
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0004.resp.List> getContInfoByCusIdAndBizType(Map param);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrCvrgCont> selectByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);
    /**
     * @方法名称: selectByContNo
     * @方法描述: 根据合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CtrCvrgCont selectByContNo(@Param("contNo") String contNo);


    /**
     * 保函协议查询
     * @param queryMap
     * @return
     */
    Xdsx0023DataRespDto getCvrgContDetail(Map queryMap);

    /**
     * 合同金额查询
     * @param queryMap
     * @return
     */
    BigDecimal getLmtAmt(Map queryMap);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * 查询保函合同列表
     * @param param
     * @return
     */
    List<CtrCvrgCont> getCvrgContByCusIdAndBizType(Map param);

    /**
     * 查询保函合同列表
     * @param param
     * @return
     */
    CtrCvrgCont getCvrgContByParam(Map param);

    /**
     * 根据的合同编号查询生效的合同
     * @param queryMap
     * @return
     */
    CtrCvrgCont getCvrgContDetailByContNo(HashMap queryMap);

    /**
     * 根据合同号更新lmtAccNo
     * @param record
     * @return
     */
    int updateLmtAccNoByContNo(CtrCvrgCont record);

    /**
     * 根据额度编号查询合同金额总和
     *
     * @param lmtAccNo
     * @return
     */
    BigDecimal getSumContAmt(String lmtAccNo);

    /**
     * 查询所有类型合同信息
     * @param cusId
     * @return
     */
    List<BusContInfoDto> getAllBusContInfo(String cusId);

    Integer isContImage(Map queryMap);
}