/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituBsLowRisk
 * @类描述: rpt_fnc_situ_bs_low_risk数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-26 21:05:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_fnc_situ_bs_low_risk")
public class RptFncSituBsLowRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 40)
	private String proName;
	
	/** 项目类型 **/
	@Column(name = "PRO_TYPE", unique = false, nullable = true, length = 20)
	private String proType;
	
	/** 最近第二年 **/
	@Column(name = "LAST_TWO_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYear;
	
	/** 最近第一年 **/
	@Column(name = "LAST_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYear;
	
	/** 当年月末 **/
	@Column(name = "CUR_YEAR_LAST_MONTH", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearLastMonth;
	
	/** 当年月末资产总额 **/
	@Column(name = "ASS_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assTotal;
	
	/** 当年月末净资产 **/
	@Column(name = "PURE_ASSET_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pureAssetTotal;
	
	/** 当年月末资产负债率 **/
	@Column(name = "DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtRate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType;
	}
	
    /**
     * @return proType
     */
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param lastTwoYear
	 */
	public void setLastTwoYear(java.math.BigDecimal lastTwoYear) {
		this.lastTwoYear = lastTwoYear;
	}
	
    /**
     * @return lastTwoYear
     */
	public java.math.BigDecimal getLastTwoYear() {
		return this.lastTwoYear;
	}
	
	/**
	 * @param lastYear
	 */
	public void setLastYear(java.math.BigDecimal lastYear) {
		this.lastYear = lastYear;
	}
	
    /**
     * @return lastYear
     */
	public java.math.BigDecimal getLastYear() {
		return this.lastYear;
	}
	
	/**
	 * @param curYearLastMonth
	 */
	public void setCurYearLastMonth(java.math.BigDecimal curYearLastMonth) {
		this.curYearLastMonth = curYearLastMonth;
	}
	
    /**
     * @return curYearLastMonth
     */
	public java.math.BigDecimal getCurYearLastMonth() {
		return this.curYearLastMonth;
	}
	
	/**
	 * @param assTotal
	 */
	public void setAssTotal(java.math.BigDecimal assTotal) {
		this.assTotal = assTotal;
	}
	
    /**
     * @return assTotal
     */
	public java.math.BigDecimal getAssTotal() {
		return this.assTotal;
	}
	
	/**
	 * @param pureAssetTotal
	 */
	public void setPureAssetTotal(java.math.BigDecimal pureAssetTotal) {
		this.pureAssetTotal = pureAssetTotal;
	}
	
    /**
     * @return pureAssetTotal
     */
	public java.math.BigDecimal getPureAssetTotal() {
		return this.pureAssetTotal;
	}
	
	/**
	 * @param debtRate
	 */
	public void setDebtRate(java.math.BigDecimal debtRate) {
		this.debtRate = debtRate;
	}
	
    /**
     * @return debtRate
     */
	public java.math.BigDecimal getDebtRate() {
		return this.debtRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}