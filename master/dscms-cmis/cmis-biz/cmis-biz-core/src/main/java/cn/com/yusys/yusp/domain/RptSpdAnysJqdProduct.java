/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJqdProduct
 * @类描述: rpt_spd_anys_jqd_product数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jqd_product")
public class RptSpdAnysJqdProduct extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 产品种类 **/
	@Column(name = "PRODUCT_TYPE", unique = false, nullable = true, length = 5)
	private String productType;
	
	/** 售价 **/
	@Column(name = "SELL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sellAmt;
	
	/** 根据成本结构计算成本 **/
	@Column(name = "PRODUCT_COST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal productCost;
	
	/** 占总销售比例 **/
	@Column(name = "PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prec;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param productType
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
    /**
     * @return productType
     */
	public String getProductType() {
		return this.productType;
	}
	
	/**
	 * @param sellAmt
	 */
	public void setSellAmt(java.math.BigDecimal sellAmt) {
		this.sellAmt = sellAmt;
	}
	
    /**
     * @return sellAmt
     */
	public java.math.BigDecimal getSellAmt() {
		return this.sellAmt;
	}
	
	/**
	 * @param productCost
	 */
	public void setProductCost(java.math.BigDecimal productCost) {
		this.productCost = productCost;
	}
	
    /**
     * @return productCost
     */
	public java.math.BigDecimal getProductCost() {
		return this.productCost;
	}
	
	/**
	 * @param prec
	 */
	public void setPrec(java.math.BigDecimal prec) {
		this.prec = prec;
	}
	
    /**
     * @return prec
     */
	public java.math.BigDecimal getPrec() {
		return this.prec;
	}


}