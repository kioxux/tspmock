/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCobInfo
 * @类描述: lmt_cob_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-04 09:27:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_cob_info")
public class LmtCobInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 共借人姓名 **/
	@Column(name = "COMMON_DEBIT_NAME", unique = false, nullable = true, length = 80)
	private String commonDebitName;
	
	/** 共借人性别 **/
	@Column(name = "COMMON_DEBIT_SEX", unique = false, nullable = true, length = 5)
	private String commonDebitSex;
	
	/** 共借人证件类型 **/
	@Column(name = "COMMON_DEBIT_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String commonDebitCertType;
	
	/** 共借人证件号码 **/
	@Column(name = "COMMON_DEBIT_CERT_CODE", unique = false, nullable = true, length = 40)
	private String commonDebitCertCode;
	
	/** 共借人婚姻状况 **/
	@Column(name = "COMMON_DEBIT_MAR_STATUS", unique = false, nullable = true, length = 5)
	private String commonDebitMarStatus;
	
	/** 工作单位 **/
	@Column(name = "COMMON_DEBIT_CPRT", unique = false, nullable = true, length = 120)
	private String commonDebitCprt;
	
	/** 共借人手机 **/
	@Column(name = "COMMON_DEBIT_PHONE", unique = false, nullable = true, length = 11)
	private String commonDebitPhone;
	
	/** 与共借人关系 **/
	@Column(name = "COMMON_DEBIT_CORRE", unique = false, nullable = true, length = 6)
	private String commonDebitCorre;
	
	/** 描述 **/
	@Column(name = "DESCRIPTION", unique = false, nullable = true, length = 500)
	private String description;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 共借人地址 **/
	@Column(name = "COMMON_DEBIT_ADDR", unique = false, nullable = true, length = 500)
	private String commonDebitAddr;
	
	/** 共借人数据来源 **/
	@Column(name = "COB_DATA_SOURCE", unique = false, nullable = true, length = 5)
	private String cobDataSource;
	
	/** 签约状态 **/
	@Column(name = "SIGN_STATE", unique = false, nullable = true, length = 5)
	private String signState;
	
	/** 影像流水号1 **/
	@Column(name = "IMAGE_CODE1", unique = false, nullable = true, length = 80)
	private String imageCode1;
	
	/** 影像流水号2 **/
	@Column(name = "IMAGE_CODE2", unique = false, nullable = true, length = 80)
	private String imageCode2;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param commonDebitName
	 */
	public void setCommonDebitName(String commonDebitName) {
		this.commonDebitName = commonDebitName;
	}
	
    /**
     * @return commonDebitName
     */
	public String getCommonDebitName() {
		return this.commonDebitName;
	}
	
	/**
	 * @param commonDebitSex
	 */
	public void setCommonDebitSex(String commonDebitSex) {
		this.commonDebitSex = commonDebitSex;
	}
	
    /**
     * @return commonDebitSex
     */
	public String getCommonDebitSex() {
		return this.commonDebitSex;
	}
	
	/**
	 * @param commonDebitCertType
	 */
	public void setCommonDebitCertType(String commonDebitCertType) {
		this.commonDebitCertType = commonDebitCertType;
	}
	
    /**
     * @return commonDebitCertType
     */
	public String getCommonDebitCertType() {
		return this.commonDebitCertType;
	}
	
	/**
	 * @param commonDebitCertCode
	 */
	public void setCommonDebitCertCode(String commonDebitCertCode) {
		this.commonDebitCertCode = commonDebitCertCode;
	}
	
    /**
     * @return commonDebitCertCode
     */
	public String getCommonDebitCertCode() {
		return this.commonDebitCertCode;
	}
	
	/**
	 * @param commonDebitMarStatus
	 */
	public void setCommonDebitMarStatus(String commonDebitMarStatus) {
		this.commonDebitMarStatus = commonDebitMarStatus;
	}
	
    /**
     * @return commonDebitMarStatus
     */
	public String getCommonDebitMarStatus() {
		return this.commonDebitMarStatus;
	}
	
	/**
	 * @param commonDebitCprt
	 */
	public void setCommonDebitCprt(String commonDebitCprt) {
		this.commonDebitCprt = commonDebitCprt;
	}
	
    /**
     * @return commonDebitCprt
     */
	public String getCommonDebitCprt() {
		return this.commonDebitCprt;
	}
	
	/**
	 * @param commonDebitPhone
	 */
	public void setCommonDebitPhone(String commonDebitPhone) {
		this.commonDebitPhone = commonDebitPhone;
	}
	
    /**
     * @return commonDebitPhone
     */
	public String getCommonDebitPhone() {
		return this.commonDebitPhone;
	}
	
	/**
	 * @param commonDebitCorre
	 */
	public void setCommonDebitCorre(String commonDebitCorre) {
		this.commonDebitCorre = commonDebitCorre;
	}
	
    /**
     * @return commonDebitCorre
     */
	public String getCommonDebitCorre() {
		return this.commonDebitCorre;
	}
	
	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
    /**
     * @return description
     */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param commonDebitAddr
	 */
	public void setCommonDebitAddr(String commonDebitAddr) {
		this.commonDebitAddr = commonDebitAddr;
	}
	
    /**
     * @return commonDebitAddr
     */
	public String getCommonDebitAddr() {
		return this.commonDebitAddr;
	}
	
	/**
	 * @param cobDataSource
	 */
	public void setCobDataSource(String cobDataSource) {
		this.cobDataSource = cobDataSource;
	}
	
    /**
     * @return cobDataSource
     */
	public String getCobDataSource() {
		return this.cobDataSource;
	}
	
	/**
	 * @param signState
	 */
	public void setSignState(String signState) {
		this.signState = signState;
	}
	
    /**
     * @return signState
     */
	public String getSignState() {
		return this.signState;
	}
	
	/**
	 * @param imageCode1
	 */
	public void setImageCode1(String imageCode1) {
		this.imageCode1 = imageCode1;
	}
	
    /**
     * @return imageCode1
     */
	public String getImageCode1() {
		return this.imageCode1;
	}
	
	/**
	 * @param imageCode2
	 */
	public void setImageCode2(String imageCode2) {
		this.imageCode2 = imageCode2;
	}
	
    /**
     * @return imageCode2
     */
	public String getImageCode2() {
		return this.imageCode2;
	}


}