package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0057Service;
import cn.com.yusys.yusp.service.risk.RiskItem0113Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0113Resource
 * @类描述: 小企业无还本续贷业务限额校验
 * @功能描述: 小企业无还本续贷业务限额10亿
 * @创建人: quwen
 * @创建时间: 2021年9月8日13:41:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0113小企业无还本续贷业务限额校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0113")
public class RiskItem0113Resource {

    @Autowired
    private RiskItem0113Service riskItem0113Service;

    @ApiOperation(value = "小企业无还本续贷业务限额校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0113(@RequestBody QueryModel queryModel) {
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0113Service.riskItem0113(queryModel));
    }
}
