/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtIntbankAppr;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaInfo;
import cn.com.yusys.yusp.service.LmtIntbankApprService;
import cn.com.yusys.yusp.service.LmtSigInvestRelFinaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppRelCusInfo;
import cn.com.yusys.yusp.service.LmtAppRelCusInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-16 13:38:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapprelcusinfo")
public class LmtAppRelCusInfoResource {
    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    @Autowired
    private LmtSigInvestRelFinaInfoService lmtSigInvestRelFinaInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppRelCusInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppRelCusInfo> list = lmtAppRelCusInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtAppRelCusInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAppRelCusInfo>> index(QueryModel queryModel) {
        List<LmtAppRelCusInfo> list = lmtAppRelCusInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppRelCusInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppRelCusInfo> show(@PathVariable("pkId") String pkId) {
        LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppRelCusInfo>(lmtAppRelCusInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppRelCusInfo> create(@RequestBody LmtAppRelCusInfo lmtAppRelCusInfo) throws URISyntaxException {
        lmtAppRelCusInfoService.insert(lmtAppRelCusInfo);
        return new ResultDto<LmtAppRelCusInfo>(lmtAppRelCusInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppRelCusInfo lmtAppRelCusInfo) throws URISyntaxException {
        int result = lmtAppRelCusInfoService.update(lmtAppRelCusInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述: 根据主键更新 - 只更新非空字段
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtAppRelCusInfo lmtAppRelCusInfo) throws URISyntaxException {
        int result = lmtAppRelCusInfoService.updateSelective(lmtAppRelCusInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppRelCusInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppRelCusInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectBySerno
     * @函数描述:根据serno获取企业基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtAppRelCusInfo> selectBySerno(@RequestBody Map map) {
        LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(map);
        return new ResultDto<LmtAppRelCusInfo>(lmtAppRelCusInfo);
    }

    /**
     * 根据cusId获取同业客户详情
     * @param condition
     * @return
     */
    @PostMapping("/selectCusInfo")
    protected ResultDto<Map> selectCusInfo(@RequestBody Map condition){
        String cusId = (String) condition.get("cusId");
        String cusCatalog = (String) condition.get("cusCatalog");
        Map result = lmtAppRelCusInfoService.selectCusInfo(cusId, cusCatalog);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:updateForCusInfo
     * @函数描述:更新企业信息和（同业授信/准入）信息
     * @参数与返回说明:
     * @算法描述: 根据主键更新 - 只更新非空字段
     */
    @PostMapping("/updateForCusInfo")
    protected ResultDto<Integer> updateForCusInfo(@RequestBody Map lmtAppRelCusInfo) throws URISyntaxException {
        lmtAppRelCusInfoService.updateMustCheckStatus((String)lmtAppRelCusInfo.get("serno"),"qyxx");
        int result = lmtAppRelCusInfoService.updateForCusInfo(lmtAppRelCusInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述: 根据主键更新 - 只更新非空字段
     */
    @PostMapping("/selectForCusInfo")
    protected ResultDto<List<Map>> selectForCusInfo(@RequestBody Map condition) throws URISyntaxException {
        HashMap newMap = new HashMap();
        //获取客户基本信息
        LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(condition);
        if(lmtAppRelCusInfo!=null){
            Map<String, Object> resultMap = (Map<String, Object>) BeanUtils.beanToMap(lmtAppRelCusInfo);
            resultMap.forEach((key,val)->{
                newMap.put(key,val);
            });
        }
        //（同业授信/准入）信息
        String serno = (String) condition.get("serno");
        String cusId = (String) condition.get("cusId");
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = lmtSigInvestRelFinaInfoService.selectBySerno(serno, cusId);
        if (lmtSigInvestRelFinaInfo!=null){
            newMap.put("intbankLmtAdmit",lmtSigInvestRelFinaInfo.getIntbankLmtAdmit());
        }

        List<Map> list = new ArrayList<>();
        list.add(newMap);

        return new ResultDto<List<Map>>(list);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateQyxx")
    protected ResultDto<Integer> updateQyxx(@RequestBody LmtAppRelCusInfo lmtAppRelCusInfo) throws URISyntaxException {
        int result = lmtAppRelCusInfoService.updateQyxx(lmtAppRelCusInfo);
        return new ResultDto<Integer>(result);
    }
}
