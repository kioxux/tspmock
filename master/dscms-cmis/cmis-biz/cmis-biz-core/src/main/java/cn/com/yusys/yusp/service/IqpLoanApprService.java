/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanAppr;
import cn.com.yusys.yusp.repository.mapper.IqpLoanApprMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-23 20:06:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanApprService {

    private static final Logger log = LoggerFactory.getLogger(IqpLoanApprService.class);
    @Autowired
    private IqpLoanApprMapper iqpLoanApprMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpLoanAppr selectByPrimaryKey(String pkId, String iqpSerno) {
        return iqpLoanApprMapper.selectByPrimaryKey(pkId, iqpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanAppr> selectAll(QueryModel model) {
        List<IqpLoanAppr> records = (List<IqpLoanAppr>) iqpLoanApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpLoanAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanAppr> list = iqpLoanApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpLoanAppr record) {
        return iqpLoanApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanAppr record) {
        return iqpLoanApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpLoanAppr record) {
        return iqpLoanApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanAppr record) {
        return iqpLoanApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String iqpSerno) {
        return iqpLoanApprMapper.deleteByPrimaryKey(pkId, iqpSerno);
    }

    /**
     * @param iqpLoanAppr
     * @return
     * @author wzy
     * @date 2021/8/23 16:43
     * @version 1.0.0
     * @desc 根据节点与流水查询审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpLoanAppr queryBySernoAndNode(IqpLoanAppr iqpLoanAppr) {
        Map map = new HashMap();
        map.put("iqpSerno",iqpLoanAppr.getIqpSerno());
        IqpLoanAppr result = iqpLoanApprMapper.queryBySernoAndNode(map);
        return result;
    }

    /**
     * @param iqpLoanAppr
     * @return
     * @author wzy
     * @date 2021/8/23 16:43
     * @version 1.0.0
     * @desc 新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void saveOrUpdate(IqpLoanAppr iqpLoanAppr) {
        try{
            int result;
            String iqpSerno = iqpLoanAppr.getIqpSerno();
            String pkId = iqpLoanAppr.getPkId();
            IqpLoanAppr returnInfo = iqpLoanApprMapper.selectByPrimaryKey(pkId,iqpSerno);
            if (returnInfo ==null) {
                iqpLoanAppr.setPkId(UUID.randomUUID().toString());
                result = insertSelective(iqpLoanAppr);
                if(result !=1){
                    throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "审批信息新增异常！");
                }
            }else{
                result = updateSelective(iqpLoanAppr);
                if(result !=1){
                    throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "审批信息更新异常！");
                }
            }
        }catch (Exception e){
            log.info("业务流水："+iqpLoanAppr.getIqpSerno()+",异常："+e.getMessage());
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, e);
        }
    }
}
