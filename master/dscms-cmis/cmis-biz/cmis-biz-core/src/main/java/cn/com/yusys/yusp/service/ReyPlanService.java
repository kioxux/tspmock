/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.ReyPlan;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.ReyPlanMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 还款计划变更service
 */
@Service
public class ReyPlanService {
    private static final Logger log = LoggerFactory.getLogger(ReyPlanService.class);
    @Autowired
    private ReyPlanMapper reyPlanMapper;
    @Autowired
    private AccLoanService accLoanService;//借据服务
    @Autowired
    private BizCorreManagerInfoService bizCorreManagerInfoService;//办理人员服务
    @Autowired
    private CtrLmtRelService ctrLmtRelService;//合同与授信关系
    @Autowired
    private CtrLoanContService ctrLoanContService;//合同服务
    @Autowired
    private IqpAcctService iqpAcctService;//账户信息
    @Autowired
    private IqpLoanAppApprModeService iqpLoanAppApprModeService;//审批模式服务
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;//授权信息
    @Autowired
    private PvpTruPayInfoService pvpTruPayInfoService;//受托支付
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务
	

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ReyPlan record) {
        return reyPlanMapper.insertSelective(record);
    }


    /**
     * 新增页面保存数据
     * @param reyPlan
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public int saveReyPlan(ReyPlan reyPlan) {
        int result = 0;
        try {
            log.info("根据借据编号新增还款计划变更开始【{}】", JSONObject.toJSON(reyPlan));
            reyPlan.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            reyPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            result = reyPlanMapper.insertSelective(reyPlan);
            if (result == 0) {
                throw new YuspException(EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
            }
            log.info("根据借据编号新增还款计划变更结束【{}】", JSONObject.toJSON(reyPlan));
            return result;
        } catch (YuspException e) {
            log.error("还款计划变更申请新增失败！", e);
            return -1;
        }
    }

    /**
     * 根据借据编号查询是否存在在途的变更业务
     * @param reyPlan
     * @return
     */
    public int checkIsExistWFByBillNo(ReyPlan reyPlan) {
        try {
            String billNo = reyPlan.getBillNo();
            log.info("根据借据编号查询在途的还款方式变更申请业务【{}】", JSONObject.toJSON(reyPlan));
            int result = 0;
            result = reyPlanMapper.checkIsExistWFByBillNo(billNo);
            if (result > 0 ) {
                throw new YuspException(EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.key, EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.value);
            }
            return result;
        } catch (YuspException e) {
            log.error("还款方式变更申请校验其他变更业务失败！", e);
            return -1;
        }
    }

    /**
     *根据主键查询数据
     * @param iqpSerno
     * @return
     */
    public ReyPlan selectByPrimaryKey(String iqpSerno) {
        ReyPlan reyPlan = reyPlanMapper.selectByPrimaryKey(iqpSerno);
        return reyPlan;
    }

    public int updateApproveStatusByIqpSerno(String iqpSerno, String wfStatus111) {
        return reyPlanMapper.updateApproveStatusByIqpSerno(iqpSerno,wfStatus111);
    }

    /**
     * 流程审批通过，更新这笔借据的还款方式
     * @param iqpSerno
     */
    public void updateRepayWayAfterWfAppr(String iqpSerno) {
        ReyPlan reyPlan = new ReyPlan();
        reyPlan = selectByPrimaryKey(iqpSerno);
        updateAccLoanRepayWayByBillNo(reyPlan);
    }

    /**
     * 通过借据号修改贷款台账中借据的还款方式
     * @reyPlan
     * @return
     */
    public void updateAccLoanRepayWayByBillNo(ReyPlan reyPlan) {
        try{
            //想iqprepayplan表中插入数据
            /*AccLoan accLoan = reyPlanMapper.selectPvpLoanAppByBillNo(reyPlan.getBillNo());
            String pvpSerno = accLoan.getPvpSerno();
            reyPlanMapper.insertIntoIqpRepayPlan(reyPlan);*/
            System.out.println("审批结束后向借据表反插数据");
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("更新贷款台账还款方式信息处理发生异常！",e);
            throw new YuspException(EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.key, EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.value);
        }
    }

    /**
     * 更新申请状态
     * @param reyPlan
     * @return
     */
    public int updateByPrimaryKeySelective(ReyPlan reyPlan) {
        return reyPlanMapper.updateByPrimaryKeySelective(reyPlan);
    }
}
