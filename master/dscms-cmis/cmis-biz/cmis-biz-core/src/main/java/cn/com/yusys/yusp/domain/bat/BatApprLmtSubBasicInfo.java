/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatApprLmtSubBasicInfo
 * @类描述: appr_lmt_sub_basic_info数据实体类
 * @功能描述: 
 * @创建人: fengjj
 * @创建时间: 2021-11-12 10:05:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BatApprLmtSubBasicInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;

	/** FK_PKID **/
	private String fkPkid;
	
	/** 批复分项编号 **/
	private String apprSubSerno;

	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 额度分项编号 **/
	private String limitSubNo;
	
	/** 额度分项名称 **/
	private String limitSubName;
	
	/** 授信品种类型属性 **/
	private String lmtBizTypeProp;
	
	/** 父节点 **/
	private String parentId;
	
	/** 是否低风险授信 **/
	private String isLriskLmt;
	
	/** 授信分项类型 **/
	private String lmtSubType;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 额度类型 **/
	private String limitType;
	
	/** 适用担保方式 **/
	private String suitGuarWay;
	
	/** 额度有效期 **/
	private String lmtDate;
	
	/** 额度宽限期（月) **/
	private Integer lmtGraper;
	
	/** 授信总额 **/
	private java.math.BigDecimal avlAmt;
	
	/** 已用总额 **/
	private java.math.BigDecimal outstndAmt;
	
	/** 敞口金额 **/
	private java.math.BigDecimal spacAmt;
	
	/** 敞口已用额度 **/
	private java.math.BigDecimal spacOutstndAmt;
	
	/** 用信余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 状态 **/
	private String status;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 资产编号 **/
	private String assetNo;
	
	/** 币种 **/
	private String curType;
	
	/** 保证金预留比例 **/
	private java.math.BigDecimal bailPreRate;
	
	/** 年利率 **/
	private java.math.BigDecimal rateYear;
	
	/** 授信总额累加 **/
	private java.math.BigDecimal lmtAmtAdd;
	
	/** 已出帐金额 **/
	private java.math.BigDecimal pvpOutstndAmt;
	
	/** 可出账金额 **/
	private java.math.BigDecimal avlOutstndAmt;
	
	/** 是否预授信标识 **/
	private String isPreCrd;
	
	/** 是否涉及货币基金 **/
	private String isIvlMf;
	
	/** 单只货币基金授信额度 **/
	private java.math.BigDecimal lmtSingleMfAmt;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 期限 **/
	private Integer term;
	
	/** 用信敞口余额 **/
	private java.math.BigDecimal loanSpacBalance;
	
	/** 投资类型 **/
	private String investType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;
	

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid;
	}
	
    /**
     * @return fkPkid
     */
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}
	
    /**
     * @return grpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName;
	}
	
    /**
     * @return limitSubName
     */
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param lmtBizTypeProp
	 */
	public void setLmtBizTypeProp(String lmtBizTypeProp) {
		this.lmtBizTypeProp = lmtBizTypeProp;
	}
	
    /**
     * @return lmtBizTypeProp
     */
	public String getLmtBizTypeProp() {
		return this.lmtBizTypeProp;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
    /**
     * @return parentId
     */
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param isLriskLmt
	 */
	public void setIsLriskLmt(String isLriskLmt) {
		this.isLriskLmt = isLriskLmt;
	}
	
    /**
     * @return isLriskLmt
     */
	public String getIsLriskLmt() {
		return this.isLriskLmt;
	}
	
	/**
	 * @param lmtSubType
	 */
	public void setLmtSubType(String lmtSubType) {
		this.lmtSubType = lmtSubType;
	}
	
    /**
     * @return lmtSubType
     */
	public String getLmtSubType() {
		return this.lmtSubType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	
    /**
     * @return limitType
     */
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param suitGuarWay
	 */
	public void setSuitGuarWay(String suitGuarWay) {
		this.suitGuarWay = suitGuarWay;
	}
	
    /**
     * @return suitGuarWay
     */
	public String getSuitGuarWay() {
		return this.suitGuarWay;
	}
	
	/**
	 * @param lmtDate
	 */
	public void setLmtDate(String lmtDate) {
		this.lmtDate = lmtDate;
	}
	
    /**
     * @return lmtDate
     */
	public String getLmtDate() {
		return this.lmtDate;
	}
	
	/**
	 * @param lmtGraper
	 */
	public void setLmtGraper(Integer lmtGraper) {
		this.lmtGraper = lmtGraper;
	}
	
    /**
     * @return lmtGraper
     */
	public Integer getLmtGraper() {
		return this.lmtGraper;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(java.math.BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return avlAmt
     */
	public java.math.BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(java.math.BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return spacAmt
     */
	public java.math.BigDecimal getSpacAmt() {
		return this.spacAmt;
	}
	
	/**
	 * @param spacOutstndAmt
	 */
	public void setSpacOutstndAmt(java.math.BigDecimal spacOutstndAmt) {
		this.spacOutstndAmt = spacOutstndAmt;
	}
	
    /**
     * @return spacOutstndAmt
     */
	public java.math.BigDecimal getSpacOutstndAmt() {
		return this.spacOutstndAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailPreRate
	 */
	public void setBailPreRate(java.math.BigDecimal bailPreRate) {
		this.bailPreRate = bailPreRate;
	}
	
    /**
     * @return bailPreRate
     */
	public java.math.BigDecimal getBailPreRate() {
		return this.bailPreRate;
	}
	
	/**
	 * @param rateYear
	 */
	public void setRateYear(java.math.BigDecimal rateYear) {
		this.rateYear = rateYear;
	}
	
    /**
     * @return rateYear
     */
	public java.math.BigDecimal getRateYear() {
		return this.rateYear;
	}
	
	/**
	 * @param lmtAmtAdd
	 */
	public void setLmtAmtAdd(java.math.BigDecimal lmtAmtAdd) {
		this.lmtAmtAdd = lmtAmtAdd;
	}
	
    /**
     * @return lmtAmtAdd
     */
	public java.math.BigDecimal getLmtAmtAdd() {
		return this.lmtAmtAdd;
	}
	
	/**
	 * @param pvpOutstndAmt
	 */
	public void setPvpOutstndAmt(java.math.BigDecimal pvpOutstndAmt) {
		this.pvpOutstndAmt = pvpOutstndAmt;
	}
	
    /**
     * @return pvpOutstndAmt
     */
	public java.math.BigDecimal getPvpOutstndAmt() {
		return this.pvpOutstndAmt;
	}
	
	/**
	 * @param avlOutstndAmt
	 */
	public void setAvlOutstndAmt(java.math.BigDecimal avlOutstndAmt) {
		this.avlOutstndAmt = avlOutstndAmt;
	}
	
    /**
     * @return avlOutstndAmt
     */
	public java.math.BigDecimal getAvlOutstndAmt() {
		return this.avlOutstndAmt;
	}
	
	/**
	 * @param isPreCrd
	 */
	public void setIsPreCrd(String isPreCrd) {
		this.isPreCrd = isPreCrd;
	}
	
    /**
     * @return isPreCrd
     */
	public String getIsPreCrd() {
		return this.isPreCrd;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf;
	}
	
    /**
     * @return isIvlMf
     */
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(java.math.BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return lmtSingleMfAmt
     */
	public java.math.BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param loanSpacBalance
	 */
	public void setLoanSpacBalance(java.math.BigDecimal loanSpacBalance) {
		this.loanSpacBalance = loanSpacBalance;
	}
	
    /**
     * @return loanSpacBalance
     */
	public java.math.BigDecimal getLoanSpacBalance() {
		return this.loanSpacBalance;
	}
	
	/**
	 * @param investType
	 */
	public void setInvestType(String investType) {
		this.investType = investType;
	}
	
    /**
     * @return investType
     */
	public String getInvestType() {
		return this.investType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}