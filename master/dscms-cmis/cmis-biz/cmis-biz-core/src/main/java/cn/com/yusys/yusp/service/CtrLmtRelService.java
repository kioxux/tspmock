/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrLmtRel;
import cn.com.yusys.yusp.repository.mapper.CtrLmtRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLmtRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-28 19:37:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CtrLmtRelService {
    private static final Logger log = LoggerFactory.getLogger(CtrLmtRelService.class);
    @Autowired
    private CtrLmtRelMapper ctrLmtRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CtrLmtRel selectByPrimaryKey(String pkId) {
        return ctrLmtRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CtrLmtRel> selectAll(QueryModel model) {
        List<CtrLmtRel> records = (List<CtrLmtRel>) ctrLmtRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CtrLmtRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLmtRel> list = ctrLmtRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CtrLmtRel record) {
        return ctrLmtRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CtrLmtRel record) {
        return ctrLmtRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CtrLmtRel record) {
        return ctrLmtRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CtrLmtRel record) {
        return ctrLmtRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return ctrLmtRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrLmtRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByParamsKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLmtRel> selectByParamsKey(String serno) {
        return ctrLmtRelMapper.selectByParamsKey(serno);
    }

    /**
     * 通过合同编号获取合同与授信、第三方额度关系数据
     * @param contNo
     * @return
     */
    public List<CtrLmtRel> selectCtrLmtRelByContNo(String contNo) {
        return ctrLmtRelMapper.selectCtrLmtRelByContNo(contNo);
    }

    /**
     * 通过额度编号更新额度编号数据
     * @param updateMap
     * @return
     */
    public int updateLmtLimitNo(Map updateMap) {
        return ctrLmtRelMapper.updateLmtLimitNo(updateMap);
    }

    /**
     * 注销时校验合同状态
     * @param map
     * @return
     */
    public List<String> getContnoByLmtCtrNo(Map map) {
        //个人授信协议注销--合同状态  500-中止600-注销700-撤回800-作废
        map.put("contStatuss", CmisCommonConstants.LMT_CTR_CANCEL_CONT_STATUS);
        // 00-自有额度
        map.put("limitType", "00");
        return ctrLmtRelMapper.getContnoByLmtCtrNo(map);
    }
}
