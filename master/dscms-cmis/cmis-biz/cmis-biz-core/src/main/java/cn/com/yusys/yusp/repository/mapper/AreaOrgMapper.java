/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.QueryAreaInfoDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.AreaList;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.AreaOrg;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaOrgMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-26 14:03:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AreaOrgMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AreaOrg selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AreaOrg> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AreaOrg record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AreaOrg record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AreaOrg record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AreaOrg record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /** 
     * @author zlf
     * @date 2021/4/26 14:10
     * @version 1.0.0
     * @desc    根据区域编号删除
     * @修改历史: 修改时间    修改人员    修改原因
    */
    int deleteByAreaNo(@Param("areaNo") String areaNo);
    /** 
     * @author zlf
     * @date 2021/4/26 14:05
     * @version 1.0.0
     * @desc    根据区域编号查询
     * @修改历史: 修改时间    修改人员    修改原因
    */
    List<AreaOrg> selectByAreaNo(@Param("areaNo") String areaNo);

    /**
     * @author zlf
     * @date 2021/4/26 14:05
     * @version 1.0.0
     * @desc    根据区域编号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<AreaOrg> selectByFbCode(@Param("fbCode") String fbCode);

    /**
     * @author zlf
     * @date 2021/4/26 14:05
     * @version 1.0.0
     * @desc    根据机构号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<AreaOrg> selectByOrgId(@Param("orgId") String orgId);


    /**
     * 根据机构号查询区域编号
     * @param orgNo
     * @return
     */
    List<AreaList> selectAreaNameByOrgNo(@Param("orgNo") String orgNo);

    /**
     * 根据机构号查询区域名称和用户名称
     * @param orgNo
     * @return
     */
    QueryAreaInfoDto getAreaInfoByOrgId(@Param("orgNo") String orgNo);
}