/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardLargeJudgInifo;
import cn.com.yusys.yusp.service.CreditCardLargeJudgInifoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardLargeJudgInifoResource
 * @类描述: #大额分期申请审批模块
 * @功能描述: 
 * @创建人: ZSM
 * @创建时间: 2021-05-25 21:08:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditcardlargejudginifo")
public class CreditCardLargeJudgInifoResource {
    @Autowired
    private CreditCardLargeJudgInifoService creditCardLargeJudgInifoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardLargeJudgInifo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardLargeJudgInifo> list = creditCardLargeJudgInifoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardLargeJudgInifo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditCardLargeJudgInifo>> index(QueryModel queryModel) {
        List<CreditCardLargeJudgInifo> list = creditCardLargeJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardLargeJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardLargeJudgInifo> show(@PathVariable("pkId") String pkId) {
        CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardLargeJudgInifo>(creditCardLargeJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CreditCardLargeJudgInifo> create(@RequestBody CreditCardLargeJudgInifo creditCardLargeJudgInifo) throws URISyntaxException {
        creditCardLargeJudgInifoService.insert(creditCardLargeJudgInifo);
        return new ResultDto<CreditCardLargeJudgInifo>(creditCardLargeJudgInifo);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<CreditCardLargeJudgInifo> save(@RequestBody CreditCardLargeJudgInifo creditCardLargeJudgInifo) throws URISyntaxException {
        creditCardLargeJudgInifoService.save(creditCardLargeJudgInifo);
        return new ResultDto<CreditCardLargeJudgInifo>(creditCardLargeJudgInifo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardLargeJudgInifo creditCardLargeJudgInifo) throws URISyntaxException {
        int result = creditCardLargeJudgInifoService.update(creditCardLargeJudgInifo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardLargeJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cut
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/cut")
    protected ResultDto<Integer> cut(@RequestBody String pkId) {
        int result = creditCardLargeJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardLargeJudgInifoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期申请审批")
    @PostMapping("/querymodel")
    protected ResultDto<List<CreditCardLargeJudgInifo>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditCardLargeJudgInifo> list = creditCardLargeJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardLargeJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键大额分期申请审批")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardLargeJudgInifo> showPost(@RequestBody CreditCardLargeJudgInifo creditCardLargeJudgInifos) {
        CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectByPrimaryKey(creditCardLargeJudgInifos.getPkId());
        return new ResultDto<CreditCardLargeJudgInifo>(creditCardLargeJudgInifo);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水查询最新的审批信息")
    @PostMapping("/selectfinalinfo")
    protected ResultDto<CreditCardLargeJudgInifo> selectFinalInfo(@RequestBody CreditCardLargeJudgInifo creditCardLargeJudgInifos) {
        CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectFinalInfo(creditCardLargeJudgInifos.getSerno());
        return new ResultDto<CreditCardLargeJudgInifo>(creditCardLargeJudgInifo);
    }
}
