package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitCreditInfo
 * @类描述: LMT_DEBIT_CREDIT_INFO数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-15 21:45:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtDebitCreditInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查编号 **/
	private String surveyNo;
	
	/** 借款人未结清贷款笔数 **/
	private String loanUnpaidNums;
	
	/** 借款人未销户信用卡授信总额 **/
	private java.math.BigDecimal creditCrdamtClose;
	
	/** 借款人未结清贷款余额 **/
	private java.math.BigDecimal loanBalUnpaid;
	
	/** 借款人近6个月信用卡平均支用比例 **/
	private java.math.BigDecimal credit6mAveDefrayPerc;
	
	/** 借款人近2年贷款审批查询机构数 **/
	private String loan2yQryorgNums;
	
	/** 借款人未结清消费金融公司贷款笔数 **/
	private String loanConsumecomUnpaidNums;
	
	/** 借款人未结清对外担保贷款笔数 **/
	private String loanOutguarUnpaidNums;
	
	/** 企业未结清业务机构数 **/
	private String comUnpaidBusiOrg;
	
	/** 企业未结清信贷余额(元) **/
	private java.math.BigDecimal loanBalComUnpaid;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo == null ? null : surveyNo.trim();
	}
	
    /**
     * @return SurveyNo
     */	
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param loanUnpaidNums
	 */
	public void setLoanUnpaidNums(String loanUnpaidNums) {
		this.loanUnpaidNums = loanUnpaidNums == null ? null : loanUnpaidNums.trim();
	}
	
    /**
     * @return LoanUnpaidNums
     */	
	public String getLoanUnpaidNums() {
		return this.loanUnpaidNums;
	}
	
	/**
	 * @param creditCrdamtClose
	 */
	public void setCreditCrdamtClose(java.math.BigDecimal creditCrdamtClose) {
		this.creditCrdamtClose = creditCrdamtClose;
	}
	
    /**
     * @return CreditCrdamtClose
     */	
	public java.math.BigDecimal getCreditCrdamtClose() {
		return this.creditCrdamtClose;
	}
	
	/**
	 * @param loanBalUnpaid
	 */
	public void setLoanBalUnpaid(java.math.BigDecimal loanBalUnpaid) {
		this.loanBalUnpaid = loanBalUnpaid;
	}
	
    /**
     * @return LoanBalUnpaid
     */	
	public java.math.BigDecimal getLoanBalUnpaid() {
		return this.loanBalUnpaid;
	}
	
	/**
	 * @param credit6mAveDefrayPerc
	 */
	public void setCredit6mAveDefrayPerc(java.math.BigDecimal credit6mAveDefrayPerc) {
		this.credit6mAveDefrayPerc = credit6mAveDefrayPerc;
	}
	
    /**
     * @return Credit6mAveDefrayPerc
     */	
	public java.math.BigDecimal getCredit6mAveDefrayPerc() {
		return this.credit6mAveDefrayPerc;
	}
	
	/**
	 * @param loan2yQryorgNums
	 */
	public void setLoan2yQryorgNums(String loan2yQryorgNums) {
		this.loan2yQryorgNums = loan2yQryorgNums == null ? null : loan2yQryorgNums.trim();
	}
	
    /**
     * @return Loan2yQryorgNums
     */	
	public String getLoan2yQryorgNums() {
		return this.loan2yQryorgNums;
	}
	
	/**
	 * @param loanConsumecomUnpaidNums
	 */
	public void setLoanConsumecomUnpaidNums(String loanConsumecomUnpaidNums) {
		this.loanConsumecomUnpaidNums = loanConsumecomUnpaidNums == null ? null : loanConsumecomUnpaidNums.trim();
	}
	
    /**
     * @return LoanConsumecomUnpaidNums
     */	
	public String getLoanConsumecomUnpaidNums() {
		return this.loanConsumecomUnpaidNums;
	}
	
	/**
	 * @param loanOutguarUnpaidNums
	 */
	public void setLoanOutguarUnpaidNums(String loanOutguarUnpaidNums) {
		this.loanOutguarUnpaidNums = loanOutguarUnpaidNums == null ? null : loanOutguarUnpaidNums.trim();
	}
	
    /**
     * @return LoanOutguarUnpaidNums
     */	
	public String getLoanOutguarUnpaidNums() {
		return this.loanOutguarUnpaidNums;
	}
	
	/**
	 * @param comUnpaidBusiOrg
	 */
	public void setComUnpaidBusiOrg(String comUnpaidBusiOrg) {
		this.comUnpaidBusiOrg = comUnpaidBusiOrg == null ? null : comUnpaidBusiOrg.trim();
	}
	
    /**
     * @return ComUnpaidBusiOrg
     */	
	public String getComUnpaidBusiOrg() {
		return this.comUnpaidBusiOrg;
	}
	
	/**
	 * @param loanBalComUnpaid
	 */
	public void setLoanBalComUnpaid(java.math.BigDecimal loanBalComUnpaid) {
		this.loanBalComUnpaid = loanBalComUnpaid;
	}
	
    /**
     * @return LoanBalComUnpaid
     */	
	public java.math.BigDecimal getLoanBalComUnpaid() {
		return this.loanBalComUnpaid;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}