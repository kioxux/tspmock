/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IndustryBsinsIncomeTop
 * @类描述: industry_bsins_income_top数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 19:31:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "industry_bsins_income_top")
public class IndustryBsinsIncomeTop extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = false, length = 5)
	private String tradeClass;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;
	
	/** 总资产 **/
	@Column(name = "TOTAL_RESSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalRessetAmt;
	
	/** 净资产 **/
	@Column(name = "TOTAL_PURE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalPureAmt;
	
	/** 营业收入 **/
	@Column(name = "BSINS_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bsinsIncome;
	
	/** 净利润 **/
	@Column(name = "PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal profit;
	
	/** 资产负债率 **/
	@Column(name = "ASSET_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetDebtRate;
	
	/** 营业融资总额 **/
	@Column(name = "TOTAL_OPER_FINANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalOperFinance;
	
	/** 销贷比 **/
	@Column(name = "LOAN_SOLD_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanSoldRate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param totalRessetAmt
	 */
	public void setTotalRessetAmt(java.math.BigDecimal totalRessetAmt) {
		this.totalRessetAmt = totalRessetAmt;
	}
	
    /**
     * @return totalRessetAmt
     */
	public java.math.BigDecimal getTotalRessetAmt() {
		return this.totalRessetAmt;
	}
	
	/**
	 * @param totalPureAmt
	 */
	public void setTotalPureAmt(java.math.BigDecimal totalPureAmt) {
		this.totalPureAmt = totalPureAmt;
	}
	
    /**
     * @return totalPureAmt
     */
	public java.math.BigDecimal getTotalPureAmt() {
		return this.totalPureAmt;
	}
	
	/**
	 * @param bsinsIncome
	 */
	public void setBsinsIncome(java.math.BigDecimal bsinsIncome) {
		this.bsinsIncome = bsinsIncome;
	}
	
    /**
     * @return bsinsIncome
     */
	public java.math.BigDecimal getBsinsIncome() {
		return this.bsinsIncome;
	}
	
	/**
	 * @param profit
	 */
	public void setProfit(java.math.BigDecimal profit) {
		this.profit = profit;
	}
	
    /**
     * @return profit
     */
	public java.math.BigDecimal getProfit() {
		return this.profit;
	}
	
	/**
	 * @param assetDebtRate
	 */
	public void setAssetDebtRate(java.math.BigDecimal assetDebtRate) {
		this.assetDebtRate = assetDebtRate;
	}
	
    /**
     * @return assetDebtRate
     */
	public java.math.BigDecimal getAssetDebtRate() {
		return this.assetDebtRate;
	}
	
	/**
	 * @param totalOperFinance
	 */
	public void setTotalOperFinance(java.math.BigDecimal totalOperFinance) {
		this.totalOperFinance = totalOperFinance;
	}
	
    /**
     * @return totalOperFinance
     */
	public java.math.BigDecimal getTotalOperFinance() {
		return this.totalOperFinance;
	}
	
	/**
	 * @param loanSoldRate
	 */
	public void setLoanSoldRate(java.math.BigDecimal loanSoldRate) {
		this.loanSoldRate = loanSoldRate;
	}
	
    /**
     * @return loanSoldRate
     */
	public java.math.BigDecimal getLoanSoldRate() {
		return this.loanSoldRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}