package cn.com.yusys.yusp.service.server.xdca0002;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.dto.server.xdca0002.req.Xdca0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0002.resp.Xdca0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2TonglianClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0002Service
 * @类描述: #服务类 大额分期申请（试算）接口
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdca0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0002Service.class);

    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;

    /**
     * 大额分期申请（试算）接口
     *
     * @param xdca0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0002DataRespDto xdca0002(Xdca0002DataReqDto xdca0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value);
        Xdca0002DataRespDto xdca0002DataRespDto = new Xdca0002DataRespDto();
        try {
            //请求字段
            String appChnl = xdca0002DataReqDto.getAppChnl();//申请渠道
            String cusName = xdca0002DataReqDto.getCusName();//客户姓名
            String certType = xdca0002DataReqDto.getCertType();//证件类型
            String certCode = xdca0002DataReqDto.getCertCode();//证件号码
            String cardNo = xdca0002DataReqDto.getCardNo();//卡号
            String crcycd = xdca0002DataReqDto.getCrcycd();//币种
            String loanPlan = xdca0002DataReqDto.getLoanPlan();//分期计划
            BigDecimal loanAmount = Optional.ofNullable(xdca0002DataReqDto.getLoanAmount()).orElse(BigDecimal.ZERO);//分期金额
            Integer loanTerm = Optional.ofNullable(xdca0002DataReqDto.getLoanTerm()).orElse(0);//分期期数
            String sendMode = xdca0002DataReqDto.getSendMode();//放款方式
            String guarMode = xdca0002DataReqDto.getGuarMode();//担保方式
            String loanFeeMethod = xdca0002DataReqDto.getLoanFeeMethod();//分期手续费收取方式
            String loanPrinDistMethod = xdca0002DataReqDto.getLoanPrinDistMethod();//分期本金分配方式
            String loanFeeCalcMethod = xdca0002DataReqDto.getLoanFeeCalcMethod();//分期手续费计算方式
            BigDecimal loanFeeRate = Optional.ofNullable(xdca0002DataReqDto.getLoanFeeRate()).orElse(BigDecimal.ZERO);;//分期手续费比例
            String optcod = xdca0002DataReqDto.getOptcod();//操作类型
            String loanrTarget = xdca0002DataReqDto.getLoanrTarget();//分期放款账户对公/对私标识
            String ddBankBranch = xdca0002DataReqDto.getDdBankBranch();//分期放款开户行号
            String ddBankName = xdca0002DataReqDto.getDdBankName();//分期放款银行名称
            String ddBankAccNo = xdca0002DataReqDto.getDdBankAccNo();//分期放款账号
            String ddBankAccName = xdca0002DataReqDto.getDdBankAccName();//分期放款账户姓名
            String disbAcctPhone = xdca0002DataReqDto.getDisbAcctPhone();//放款账户移动电话
            String disbAcctCertType = xdca0002DataReqDto.getDisbAcctCertType();//放款账户证件类型
            String disbAcctCertCode = xdca0002DataReqDto.getDisbAcctCertCode();//放款账户证件号码
            String paymentPurpose = xdca0002DataReqDto.getPaymentPurpose();//资金用途
            BigDecimal yearInterestRate = xdca0002DataReqDto.getYearInterestRate();//分期折算近似年化利率
            String salesManNo = xdca0002DataReqDto.getSalesManNo();//分期营销客户经理号
            String salesMan = xdca0002DataReqDto.getSalesMan();//分期营销人员姓名
            String salesManPhone = xdca0002DataReqDto.getSalesManPhone();//分期营销人员手机号
            String salesManOwingBranch = xdca0002DataReqDto.getSalesManOwingBranch();//分期营销人员所属支行
            String recomId = xdca0002DataReqDto.getRecomId();//推荐人工号
            String recomName = xdca0002DataReqDto.getRecomName();//推荐人名称
            String inputId = xdca0002DataReqDto.getInputId();//登记人
            String inputBrId = xdca0002DataReqDto.getInputBrId();//登记机构
            String inputDate = xdca0002DataReqDto.getInputDate();//登记日期
            BigDecimal loanAmt = xdca0002DataReqDto.getLoanAmt();//分期手续费固定金额

            //调用通联的试算接口
            //请求字段
            D13160ReqDto d13160ReqDto = new D13160ReqDto();
            d13160ReqDto.setCardno(cardNo); //卡号
            d13160ReqDto.setCrcycd("156"); //币种
            d13160ReqDto.setLoannt(loanAmount); //分期金额
            d13160ReqDto.setLoanterm(Optional.ofNullable(loanTerm.toString()).orElse(StringUtils.EMPTY)); //分期期数
            d13160ReqDto.setLnfemd(loanFeeMethod); //分期手续费收取方式;非必输项;F：一次性收取,E：分期收取
            d13160ReqDto.setLoanmethod(loanPrinDistMethod); //分期本金分配方式;此项为非必输项;F：按月平分,L：全部本金放在末期
            d13160ReqDto.setLothod(loanFeeCalcMethod); //分期手续费计算方式;此项为非必输项;R：附加金额按比例计算,A：附加金额为固定金额
            if("R".equals(loanFeeCalcMethod)){//【分期手续费计算方式】的取值为“R：附加金额按比例计算” ，此项必须输入
                d13160ReqDto.setLoanrate(Optional.ofNullable(loanFeeRate.toString()).orElse(StringUtils.EMPTY)); //分期手续费比例
            }
            if("A".equals(loanFeeCalcMethod)){//【分期手续费计算方式】的取值为“A：附加金额为固定金额”，此项必须输入
                d13160ReqDto.setLoanamt(Optional.ofNullable(loanAmt.toString()).orElse(StringUtils.EMPTY));//  分期手续费固定金额
            }
            d13160ReqDto.setLoanind("Y");
            d13160ReqDto.setOptcod(optcod);// 0：试算; 1：申请
            d13160ReqDto.setSendmode(sendMode);//分期放款方式;此项为非必输项;O:实时放款,B:批量放款
            d13160ReqDto.setDbnknm(ddBankName);//此四项为非必输项;输入放款账户信息时， 这四项信息必须同时输入，
            d13160ReqDto.setDbkbch(ddBankBranch);//此四项为非必输项;输入放款账户信息时， 这四项信息必须同时输入，
            d13160ReqDto.setBkctnm(ddBankAccName);//此四项为非必输项;输入放款账户信息时， 这四项信息必须同时输入
            d13160ReqDto.setDbkact(ddBankAccNo);//此四项为非必输项;输入放款账户信息时， 这四项信息必须同时输入
            d13160ReqDto.setSeflag("N");//  非本人账户放款标识，此项为非必输项，如果此项未输入，默认为’N’取值含义：Y-非本人账户放款；N-本人账户放款
            d13160ReqDto.setPapose(paymentPurpose);//此项为非必输项
            d13160ReqDto.setLoanet(loanrTarget);//分期放款账户对公/对私标识;取值含义如下：TP：对公;TS：对私;空： 申请分期时未指定放款账户的对公/对私属性
            d13160ReqDto.setSalman(salesMan);
            d13160ReqDto.setSaleno(salesManNo);
            d13160ReqDto.setSalech(salesManOwingBranch);
            d13160ReqDto.setEappno(appChnl);//  外部申请编号
            d13160ReqDto.setLocode(loanPlan);//  分期计划代码;此项为非必输项

            logger.info("*********XDCA0002*大额分期申请（试算）调用通联试算接口开始,查询参数为:{}", JSON.toJSONString(d13160ReqDto));
            ResultDto<D13160RespDto> d13160RespDtoResultDto = dscms2TonglianClientService.d13160(d13160ReqDto);
            logger.info("*********XDCA0002*大额分期申请（试算）调用通联试算接口结束,返回参数为:{}", JSON.toJSONString(d13160RespDtoResultDto));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, d13160RespDtoResultDto.getCode())&&"0".equals(optcod)) {//返回成功
                D13160RespDto d13160RespDto = d13160RespDtoResultDto.getData();
                //返回信息
                xdca0002DataRespDto.setSerno(d13160RespDto.getRgstid());// 业务流水号
                xdca0002DataRespDto.setCardno(d13160RespDto.getCardno());// 卡号
                xdca0002DataRespDto.setCrcycd(d13160RespDto.getCrcycd());// 币种
                xdca0002DataRespDto.setRgstid(d13160RespDto.getRgstid());// 分期申请顺序号
                //分期总期数
                String count = d13160RespDto.getLnittm();
                xdca0002DataRespDto.setLnittm((count == null || "".equals(count)) ? 0:Integer.parseInt(count));// 分期总期数
                xdca0002DataRespDto.setLnfemd(d13160RespDto.getLnfemd());// 分期手续费收取方式
                xdca0002DataRespDto.setLnitpn(d13160RespDto.getLnitpn());// 分期总本金
                xdca0002DataRespDto.setLnfdpt(d13160RespDto.getLnfdpt());// 分期每期应还本金
                xdca0002DataRespDto.setLnfttm(d13160RespDto.getLnfttm());// 分期首期应还本金
                xdca0002DataRespDto.setLnflt2(d13160RespDto.getLnflt2());// 分期末期应还本金
                xdca0002DataRespDto.setLnitfi(d13160RespDto.getLnitfi());// 分期总手续费
                xdca0002DataRespDto.setLnfdfi(d13160RespDto.getLnfdfi());// 分期每期手续费
                xdca0002DataRespDto.setLnftfi(d13160RespDto.getLnftfi());// 分期首期手续费
                xdca0002DataRespDto.setLnfltm(d13160RespDto.getLnfltm());// 分期末期手续费
                xdca0002DataRespDto.setSendmo(d13160RespDto.getSendmo());// 分期放款方式
                xdca0002DataRespDto.setDbnknm(d13160RespDto.getDbnknm());// 分期放款银行名称
                xdca0002DataRespDto.setDbkbch(d13160RespDto.getDbkbch());// 分期放款开户行号
                xdca0002DataRespDto.setDbkact(d13160RespDto.getDbkact());// 分期放款账号
                xdca0002DataRespDto.setBkctnm(d13160RespDto.getBkctnm());// 分期放款账户姓名
                xdca0002DataRespDto.setPapose(d13160RespDto.getPapose());// 资金用途
                xdca0002DataRespDto.setLoanet(d13160RespDto.getLoanet());// 分期放款账户对公/对私标识
                xdca0002DataRespDto.setSalman(d13160RespDto.getSalman());// 分期营销人员姓名
                xdca0002DataRespDto.setSaleno(d13160RespDto.getSaleno());// 分期营销人员编号
                xdca0002DataRespDto.setSalech(d13160RespDto.getSalech());// 分期营销人员所属分行
            }else if(Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, d13160RespDtoResultDto.getCode())&&"1".equals(optcod)){
                xdca0002DataRespDto.setOpFlag("S");
            }else{
                xdca0002DataRespDto.setOpFlag("F");
                xdca0002DataRespDto.setOpMsg(d13160RespDtoResultDto.getMessage());//错误信息
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value);
        return xdca0002DataRespDto;
    }
}
