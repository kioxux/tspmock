/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.repository.mapper.AccTfLocMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.AccTfLocVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTfLocService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 20:47:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccTfLocService {

    @Autowired
    private AccTfLocMapper accTfLocMapper;
    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccTfLoc selectByPrimaryKey(String pkId) {
        return accTfLocMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccTfLoc> selectAll(QueryModel model) {
        List<AccTfLoc> records = (List<AccTfLoc>) accTfLocMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccTfLoc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccTfLoc> list = accTfLocMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccTfLoc record) {
        return accTfLocMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccTfLoc record) {
        return accTfLocMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccTfLoc record) {
        return accTfLocMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccTfLoc record) {
        return accTfLocMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accTfLocMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accTfLocMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selectForAccTfLocInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:07
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccTfLoc> selectForAccTfLocInfo(String cusId) {
        return accTfLocMapper.selectForAccTfLocInfo(cusId);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 16:02
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccTfLoc> selectByContNo(String contNo) {
        return accTfLocMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: selectAccTfLocRepay
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccTfLoc> selectAccTfLocRepay(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccTfLoc> list = accTfLocMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param contNo
     * @return countAccTfLocCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    public int countAccTfLocCountByContNo(String contNo) {
        return accTfLocMapper.countAccTfLocCountByContNo(contNo);
    }

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询开证台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccTfLoc selectByBillNo(String billNo) {
        return accTfLocMapper.selectByBillNo(billNo);
    }

    /**
     * 异步导出保函台账列表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccTfLoc(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("startDate desc");
            String apiUrl = "/api/acctfloc/exportAccTfLoc";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectAccTfLocByExcel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccTfLocVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<AccTfLocVo> selectAccTfLocByExcel(QueryModel model) {
        List<AccTfLocVo> accTfLocVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccTfLoc> list = accTfLocMapper.selectByModel(model);
        PageHelper.clearPage();

        for (AccTfLoc accTfLoc : list) {
            AccTfLocVo accTfLocVo = new AccTfLocVo();
            String orgName = OcaTranslatorUtils.getOrgName(accTfLoc.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(accTfLoc.getManagerId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(accTfLoc, accTfLocVo);
            accTfLocVo.setManagerBrIdName(orgName);
            accTfLocVo.setManagerIdName(userName);
            accTfLocVoList.add(accTfLocVo);
        }
        return accTfLocVoList;
    }

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据pvpSerno查询 借据号billNO
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectAccTfLocBillNo(String pvpSerno){
        AccTfLoc accTfLoc = accTfLocMapper.selectAccTfLocBillNo(pvpSerno) ;
        if(Objects.nonNull(accTfLoc)){
            return accTfLoc.getBillNo() ;
        }
        return null ;
    }

    /**
     * 根据合同编号查询用信敞口余额
     * @param contNos
     * @return
     */
    public BigDecimal selectTotalSpacAmtByContNos(String contNos){
        return accTfLocMapper.selectTotalSpacAmtByContNos(contNos);
    }

    /**
     * 风险分类审批结束更新客户未结清开证台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:30
     **/
    public int updateLoanFiveAndTenClassByCusId(Map<String, String> map) {
        return accTfLocMapper.updateLoanFiveAndTenClassByCusId(map);
    }
}
