package cn.com.yusys.yusp.service.server.xdxw0033;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0033.req.Xdxw0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0033.resp.Xdxw0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0033Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-18 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0033Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0033Service.class);

    @Resource
    private CreditQryBizRealMapper creditQryBizRealMapper;

    /**
     * 根据流水号查询征信报告关联信息
     *
     * @param xdxw0033DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0033DataRespDto getXdxw0033(Xdxw0033DataReqDto xdxw0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataReqDto));
        Xdxw0033DataRespDto xdxw0033DataRespDto = new Xdxw0033DataRespDto();
        try {
            String indgtSerno = xdxw0033DataReqDto.getIndgtSerno();
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0033.resp.List> list = new ArrayList<>();
            if (StringUtils.isEmpty(indgtSerno)) {
                throw BizException.error(null, EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
            } else {
                list = creditQryBizRealMapper.getCreditBizInfoBySurvey(indgtSerno);
            }
            // 2021年10月25日17:19:17 hubp
            if(list.size() > 0){
                for(cn.com.yusys.yusp.dto.server.xdxw0033.resp.List list1 : list){
                    String tempDate = list1.getRelaDate().substring(0,10);
                    list1.setRelaDate(tempDate);
                    // 2021年11月19日17:34:10 hubp 将征信报告reqId从url中提出
                    String url = list1.getCreditReportPK();
                    if (StringUtils.isEmpty(url)) {
                        throw BizException.error(null, "20070", "reqId为空！");
                    }
                    String url1 = url.substring(0,url.indexOf("reqId="));
                    String creditReportPK = url.substring(url1.length(),url.length()).substring(6,38);
                    list1.setCreditReportPK(creditReportPK);
                }
            }
            xdxw0033DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataRespDto));
        return xdxw0033DataRespDto;
    }
}
