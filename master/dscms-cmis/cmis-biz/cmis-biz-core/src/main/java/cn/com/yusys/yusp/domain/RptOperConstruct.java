/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperConstruct
 * @类描述: rpt_oper_construct数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 21:13:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_construct")
public class RptOperConstruct extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 所属性质 **/
	@Column(name = "LAND_CHA", unique = false, nullable = true, length = 200)
	private String landCha;
	
	/** 施工或安装资质 **/
	@Column(name = "CONS_LIC", unique = false, nullable = true, length = 200)
	private String consLic;
	
	/** 经营模式 **/
	@Column(name = "OPER_MODE", unique = false, nullable = true, length = 5)
	private String operMode;
	
	/** 是否存在他人挂靠 **/
	@Column(name = "OTHER_DEPEND_IND", unique = false, nullable = true, length = 5)
	private String otherDependInd;
	
	/** 是否存在施工质量问题 **/
	@Column(name = "QUALITY_ISSUE_IND", unique = false, nullable = true, length = 5)
	private String qualityIssueInd;
	
	/** 最近第三年自建承接工程 **/
	@Column(name = "NEAR_THIRD_SELF_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearThirdSelfBuildProjcet;
	
	/** 最近第三年自建已完工工程 **/
	@Column(name = "NEAR_THIRD_SELF_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearThirdSelfFinProjcet;
	
	/** 最近第三年他人挂靠承接工程 **/
	@Column(name = "NEAR_THIRD_OTHER_DEPEND_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearThirdOtherDependBuildProjcet;
	
	/** 最近第三年他人挂靠已完工工程 **/
	@Column(name = "NEAR_THIRD_OTHER_DEPEND_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearThirdOtherDependFinProjcet;
	
	/** 最近第二年自建承接工程 **/
	@Column(name = "NEAR_SECOND_SELF_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearSecondSelfBuildProjcet;
	
	/** 最近第二年自建已完工工程 **/
	@Column(name = "NEAR_SECOND_SELF_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearSecondSelfFinProjcet;
	
	/** 最近第二年他人挂靠承接工程 **/
	@Column(name = "NEAR_SECOND_OTHER_DEPEND_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearSecondOtherDependBuildProjcet;
	
	/** 最近第二年他人挂靠已完工工程 **/
	@Column(name = "NEAR_SECOND_OTHER_DEPEND_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearSecondOtherDependFinProjcet;
	
	/** 最近第一年自建承接工程 **/
	@Column(name = "NEAR_FIRST_SELF_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearFirstSelfBuildProjcet;
	
	/** 最近第一年自建已完工工程 **/
	@Column(name = "NEAR_FIRST_SELF_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearFirstSelfFinProjcet;
	
	/** 最近第一年他人挂靠承接工程 **/
	@Column(name = "NEAR_FIRST_OTHER_DEPEND_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String nearFirstOtherDependBuildProjcet;
	
	/** 最近第一年他人挂靠已完工工程 **/
	@Column(name = "NEAR_FIRST_OTHER_DEPEND_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String nearFirstOtherDependFinProjcet;
	
	/** 今年当前自建在建工程 **/
	@Column(name = "CURR_SELF_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String currSelfBuildProjcet;
	
	/** 今年当前自建尚未动工工程 **/
	@Column(name = "CURR_SELF_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String currSelfFinProjcet;
	
	/** 今年当前自建意向性工程 **/
	@Column(name = "CURR_OTHER_DEPEND_BUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String currOtherDependBuildProjcet;
	
	/** 今年当前他人挂靠在建工程 **/
	@Column(name = "CURR_OTHER_DEPEND_FIN_PROJCET", unique = false, nullable = true, length = 200)
	private String currOtherDependFinProjcet;
	
	/** 今年当前他人挂靠尚未动工工程 **/
	@Column(name = "CURR_OTHER_DEPEND_NOBUILD_PROJCET", unique = false, nullable = true, length = 200)
	private String currOtherDependNobuildProjcet;
	
	/** 今年当前他人挂靠意向性工程 **/
	@Column(name = "CURR_OTHER_DEPEND_READY_PROJCET", unique = false, nullable = true, length = 200)
	private String currOtherDependReadyProjcet;
	
	/** 其他需说明事项 **/
	@Column(name = "OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String otherNeedDesc;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param landCha
	 */
	public void setLandCha(String landCha) {
		this.landCha = landCha;
	}
	
    /**
     * @return landCha
     */
	public String getLandCha() {
		return this.landCha;
	}
	
	/**
	 * @param consLic
	 */
	public void setConsLic(String consLic) {
		this.consLic = consLic;
	}
	
    /**
     * @return consLic
     */
	public String getConsLic() {
		return this.consLic;
	}
	
	/**
	 * @param operMode
	 */
	public void setOperMode(String operMode) {
		this.operMode = operMode;
	}
	
    /**
     * @return operMode
     */
	public String getOperMode() {
		return this.operMode;
	}
	
	/**
	 * @param otherDependInd
	 */
	public void setOtherDependInd(String otherDependInd) {
		this.otherDependInd = otherDependInd;
	}
	
    /**
     * @return otherDependInd
     */
	public String getOtherDependInd() {
		return this.otherDependInd;
	}
	
	/**
	 * @param qualityIssueInd
	 */
	public void setQualityIssueInd(String qualityIssueInd) {
		this.qualityIssueInd = qualityIssueInd;
	}
	
    /**
     * @return qualityIssueInd
     */
	public String getQualityIssueInd() {
		return this.qualityIssueInd;
	}
	
	/**
	 * @param nearThirdSelfBuildProjcet
	 */
	public void setNearThirdSelfBuildProjcet(String nearThirdSelfBuildProjcet) {
		this.nearThirdSelfBuildProjcet = nearThirdSelfBuildProjcet;
	}
	
    /**
     * @return nearThirdSelfBuildProjcet
     */
	public String getNearThirdSelfBuildProjcet() {
		return this.nearThirdSelfBuildProjcet;
	}
	
	/**
	 * @param nearThirdSelfFinProjcet
	 */
	public void setNearThirdSelfFinProjcet(String nearThirdSelfFinProjcet) {
		this.nearThirdSelfFinProjcet = nearThirdSelfFinProjcet;
	}
	
    /**
     * @return nearThirdSelfFinProjcet
     */
	public String getNearThirdSelfFinProjcet() {
		return this.nearThirdSelfFinProjcet;
	}
	
	/**
	 * @param nearThirdOtherDependBuildProjcet
	 */
	public void setNearThirdOtherDependBuildProjcet(String nearThirdOtherDependBuildProjcet) {
		this.nearThirdOtherDependBuildProjcet = nearThirdOtherDependBuildProjcet;
	}
	
    /**
     * @return nearThirdOtherDependBuildProjcet
     */
	public String getNearThirdOtherDependBuildProjcet() {
		return this.nearThirdOtherDependBuildProjcet;
	}
	
	/**
	 * @param nearThirdOtherDependFinProjcet
	 */
	public void setNearThirdOtherDependFinProjcet(String nearThirdOtherDependFinProjcet) {
		this.nearThirdOtherDependFinProjcet = nearThirdOtherDependFinProjcet;
	}
	
    /**
     * @return nearThirdOtherDependFinProjcet
     */
	public String getNearThirdOtherDependFinProjcet() {
		return this.nearThirdOtherDependFinProjcet;
	}
	
	/**
	 * @param nearSecondSelfBuildProjcet
	 */
	public void setNearSecondSelfBuildProjcet(String nearSecondSelfBuildProjcet) {
		this.nearSecondSelfBuildProjcet = nearSecondSelfBuildProjcet;
	}
	
    /**
     * @return nearSecondSelfBuildProjcet
     */
	public String getNearSecondSelfBuildProjcet() {
		return this.nearSecondSelfBuildProjcet;
	}
	
	/**
	 * @param nearSecondSelfFinProjcet
	 */
	public void setNearSecondSelfFinProjcet(String nearSecondSelfFinProjcet) {
		this.nearSecondSelfFinProjcet = nearSecondSelfFinProjcet;
	}
	
    /**
     * @return nearSecondSelfFinProjcet
     */
	public String getNearSecondSelfFinProjcet() {
		return this.nearSecondSelfFinProjcet;
	}
	
	/**
	 * @param nearSecondOtherDependBuildProjcet
	 */
	public void setNearSecondOtherDependBuildProjcet(String nearSecondOtherDependBuildProjcet) {
		this.nearSecondOtherDependBuildProjcet = nearSecondOtherDependBuildProjcet;
	}
	
    /**
     * @return nearSecondOtherDependBuildProjcet
     */
	public String getNearSecondOtherDependBuildProjcet() {
		return this.nearSecondOtherDependBuildProjcet;
	}
	
	/**
	 * @param nearSecondOtherDependFinProjcet
	 */
	public void setNearSecondOtherDependFinProjcet(String nearSecondOtherDependFinProjcet) {
		this.nearSecondOtherDependFinProjcet = nearSecondOtherDependFinProjcet;
	}
	
    /**
     * @return nearSecondOtherDependFinProjcet
     */
	public String getNearSecondOtherDependFinProjcet() {
		return this.nearSecondOtherDependFinProjcet;
	}
	
	/**
	 * @param nearFirstSelfBuildProjcet
	 */
	public void setNearFirstSelfBuildProjcet(String nearFirstSelfBuildProjcet) {
		this.nearFirstSelfBuildProjcet = nearFirstSelfBuildProjcet;
	}
	
    /**
     * @return nearFirstSelfBuildProjcet
     */
	public String getNearFirstSelfBuildProjcet() {
		return this.nearFirstSelfBuildProjcet;
	}
	
	/**
	 * @param nearFirstSelfFinProjcet
	 */
	public void setNearFirstSelfFinProjcet(String nearFirstSelfFinProjcet) {
		this.nearFirstSelfFinProjcet = nearFirstSelfFinProjcet;
	}
	
    /**
     * @return nearFirstSelfFinProjcet
     */
	public String getNearFirstSelfFinProjcet() {
		return this.nearFirstSelfFinProjcet;
	}
	
	/**
	 * @param nearFirstOtherDependBuildProjcet
	 */
	public void setNearFirstOtherDependBuildProjcet(String nearFirstOtherDependBuildProjcet) {
		this.nearFirstOtherDependBuildProjcet = nearFirstOtherDependBuildProjcet;
	}
	
    /**
     * @return nearFirstOtherDependBuildProjcet
     */
	public String getNearFirstOtherDependBuildProjcet() {
		return this.nearFirstOtherDependBuildProjcet;
	}
	
	/**
	 * @param nearFirstOtherDependFinProjcet
	 */
	public void setNearFirstOtherDependFinProjcet(String nearFirstOtherDependFinProjcet) {
		this.nearFirstOtherDependFinProjcet = nearFirstOtherDependFinProjcet;
	}
	
    /**
     * @return nearFirstOtherDependFinProjcet
     */
	public String getNearFirstOtherDependFinProjcet() {
		return this.nearFirstOtherDependFinProjcet;
	}
	
	/**
	 * @param currSelfBuildProjcet
	 */
	public void setCurrSelfBuildProjcet(String currSelfBuildProjcet) {
		this.currSelfBuildProjcet = currSelfBuildProjcet;
	}
	
    /**
     * @return currSelfBuildProjcet
     */
	public String getCurrSelfBuildProjcet() {
		return this.currSelfBuildProjcet;
	}
	
	/**
	 * @param currSelfFinProjcet
	 */
	public void setCurrSelfFinProjcet(String currSelfFinProjcet) {
		this.currSelfFinProjcet = currSelfFinProjcet;
	}
	
    /**
     * @return currSelfFinProjcet
     */
	public String getCurrSelfFinProjcet() {
		return this.currSelfFinProjcet;
	}
	
	/**
	 * @param currOtherDependBuildProjcet
	 */
	public void setCurrOtherDependBuildProjcet(String currOtherDependBuildProjcet) {
		this.currOtherDependBuildProjcet = currOtherDependBuildProjcet;
	}
	
    /**
     * @return currOtherDependBuildProjcet
     */
	public String getCurrOtherDependBuildProjcet() {
		return this.currOtherDependBuildProjcet;
	}
	
	/**
	 * @param currOtherDependFinProjcet
	 */
	public void setCurrOtherDependFinProjcet(String currOtherDependFinProjcet) {
		this.currOtherDependFinProjcet = currOtherDependFinProjcet;
	}
	
    /**
     * @return currOtherDependFinProjcet
     */
	public String getCurrOtherDependFinProjcet() {
		return this.currOtherDependFinProjcet;
	}
	
	/**
	 * @param currOtherDependNobuildProjcet
	 */
	public void setCurrOtherDependNobuildProjcet(String currOtherDependNobuildProjcet) {
		this.currOtherDependNobuildProjcet = currOtherDependNobuildProjcet;
	}
	
    /**
     * @return currOtherDependNobuildProjcet
     */
	public String getCurrOtherDependNobuildProjcet() {
		return this.currOtherDependNobuildProjcet;
	}
	
	/**
	 * @param currOtherDependReadyProjcet
	 */
	public void setCurrOtherDependReadyProjcet(String currOtherDependReadyProjcet) {
		this.currOtherDependReadyProjcet = currOtherDependReadyProjcet;
	}
	
    /**
     * @return currOtherDependReadyProjcet
     */
	public String getCurrOtherDependReadyProjcet() {
		return this.currOtherDependReadyProjcet;
	}
	
	/**
	 * @param otherNeedDesc
	 */
	public void setOtherNeedDesc(String otherNeedDesc) {
		this.otherNeedDesc = otherNeedDesc;
	}
	
    /**
     * @return otherNeedDesc
     */
	public String getOtherNeedDesc() {
		return this.otherNeedDesc;
	}


}