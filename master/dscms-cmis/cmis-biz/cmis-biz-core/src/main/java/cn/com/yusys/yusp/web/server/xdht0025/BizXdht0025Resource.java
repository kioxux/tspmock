package cn.com.yusys.yusp.web.server.xdht0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0025.req.Xdht0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0025.resp.Xdht0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同签订/撤销（优享贷和增享贷）
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0025:合同签订/撤销（优享贷和增享贷）")
@RestController
@RequestMapping("/api/dscms")
public class BizXdht0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0025Resource.class);

    /**
     * 交易码：xdht0025
     * 交易描述：合同签订/撤销（优享贷和增享贷）
     *
     * @param xdht0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订/撤销（优享贷和增享贷）")
    @PostMapping("/xdht0025")
    protected @ResponseBody
    ResultDto<Xdht0025DataRespDto> xdht0025(@Validated @RequestBody Xdht0025DataReqDto xdht0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025DataReqDto));
        Xdht0025DataRespDto xdht0025DataRespDto = new Xdht0025DataRespDto();// 响应Dto:合同签订/撤销（优享贷和增享贷）
        ResultDto<Xdht0025DataRespDto> xdht0025DataResultDto = new ResultDto<>();
        try {
            String cont_no = xdht0025DataReqDto.getCont_no();//合同号
            String Deal_type = xdht0025DataReqDto.get$eal_type();//请求类型
            String creditImageNo = xdht0025DataReqDto.getCreditImageNo();//个人征信影像编号
            String bigDataCreditNo = xdht0025DataReqDto.getBigDataCreditNo();//大数据信用影像编号
            String loanDirectionNo = xdht0025DataReqDto.getLoanDirectionNo();//贷款用途影像编号
            String contImageNo = xdht0025DataReqDto.getContImageNo();//电子合同影像编号
            String repayType = xdht0025DataReqDto.getRepayType();//还款方式
            // 从xdht0025DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdht0025DataRespDto对象开始
            xdht0025DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xdht0025DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdht0025DataRespDto对象结束
            // 封装xdht0025DataResultDto中正确的返回码和返回信息
            xdht0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, e.getMessage());
            // 封装xdht0025DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0025DataRespDto到xdht0025DataResultDto中
        xdht0025DataResultDto.setData(xdht0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025DataReqDto));
        return xdht0025DataResultDto;
    }
}
