/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAppr;
import cn.com.yusys.yusp.repository.mapper.IntbankOrgAdmitApprMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-05-10 21:29:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IntbankOrgAdmitApprService extends BizInvestCommonService{

    private static final Logger logger = LoggerFactory.getLogger(IntbankOrgAdmitApprService.class);

    @Autowired
    private IntbankOrgAdmitApprMapper intbankOrgAdmitApprMapper;

    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IntbankOrgAdmitAppr selectByPrimaryKey(String pkId) {
        return intbankOrgAdmitApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IntbankOrgAdmitAppr> selectAll(QueryModel model) {
        List<IntbankOrgAdmitAppr> records = (List<IntbankOrgAdmitAppr>) intbankOrgAdmitApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IntbankOrgAdmitAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IntbankOrgAdmitAppr> list = intbankOrgAdmitApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IntbankOrgAdmitAppr record) {
        return intbankOrgAdmitApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IntbankOrgAdmitAppr record) {
        return intbankOrgAdmitApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IntbankOrgAdmitAppr record) {
        return intbankOrgAdmitApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IntbankOrgAdmitAppr record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return intbankOrgAdmitApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return intbankOrgAdmitApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return intbankOrgAdmitApprMapper.deleteByIds(ids);
    }

    /**
     * 通过流水号查询最终的授信审批数据
     * @param serno
     * @return
     */
    public IntbankOrgAdmitAppr queryFinalApprBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.setSort(" update_time desc");
        queryModel.setSize(1);
        queryModel.setPage(1);
        List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = selectByModel(queryModel);
        if (intbankOrgAdmitApprs!= null && intbankOrgAdmitApprs.size()>0){
            return intbankOrgAdmitApprs.get(0);
        }
        return null;
    }

    /**
     * 同业机构授信准入-生成审批表
     * @param intbankOrgAdmitApp
     * @param currentUserId
     * @param currentOrgId
     */
    public int insertOrgAmditApprInfo(IntbankOrgAdmitApp intbankOrgAdmitApp, String currentUserId, String currentOrgId,String issueReportType) {
        /**
         * 生成审批表
         */
        IntbankOrgAdmitAppr intbankOrgAdmitAppr = new IntbankOrgAdmitAppr();
        BeanUtils.copyProperties(intbankOrgAdmitApp,intbankOrgAdmitAppr);
        intbankOrgAdmitAppr.setPkId(generatePkId());
        //生成审批流水号
        intbankOrgAdmitAppr.setApproveSerno(generateSerno(SeqConstant.INTBANK_ADMIT_SEQ));
        //input信息采用申请表数据
//        intbankOrgAdmitAppr.setInputId(currentUserId);
//        intbankOrgAdmitAppr.setInputBrId(currentOrgId);
        intbankOrgAdmitAppr.setCreateTime(getCurrrentDate());
        //【提交时间】默认为任务生成时间
//        intbankOrgAdmitAppr.setInputDate(getCurrrentDateStr());
        intbankOrgAdmitAppr.setUpdId(currentUserId);
        intbankOrgAdmitAppr.setUpdBrId(currentOrgId);
//        String endDate = DateUtils.addMonth(intbankOrgAdmitApp.getInputDate(), DateFormatEnum.DEFAULT.getValue(),intbankOrgAdmitApp.getTerm());
//        intbankOrgAdmitAppr.setEndDate(endDate);
        intbankOrgAdmitAppr.setUpdDate(getCurrrentDateStr());
        intbankOrgAdmitAppr.setUpdateTime(getCurrrentDate());
        intbankOrgAdmitAppr.setCreateTime(DateUtils.getCurrTimestamp());
        intbankOrgAdmitAppr.setUpdateTime(DateUtils.getCurrTimestamp());

        if (!StringUtils.isBlank(issueReportType)){
            //判断节点是否为出具审查报告节点、出具批复节点，设置对应的ISSUE_REPORT_TYPE出具报告类型值
            intbankOrgAdmitAppr.setIssueReportType(issueReportType);
        }
        return insert(intbankOrgAdmitAppr);
    }

    /**
     * @方法名称: updateRestByPkId
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkId(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<IntbankOrgAdmitAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            IntbankOrgAdmitAppr intbankOrgAdmitAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            intbankOrgAdmitAppr.setReviewResult(reviewResult);
            //获取前端传入参数restDesc-审批意见   审批意见不等同于结论性描述，此处不更新 modify by zhangjw 20210806
            String restDesc = (String)model.getCondition().get("restDesc");
            intbankOrgAdmitAppr.setRestDesc(restDesc);
            //更新 更新数据时间
            intbankOrgAdmitAppr.setUpdateTime(getCurrrentDate());
            return intbankOrgAdmitApprMapper.updateByPrimaryKey(intbankOrgAdmitAppr);
        }
        return 0;
    }

    /**
     * @方法名称: updateRestByPkIdZH
     * @方法描述: 根据Serno更新信贷管理部风险派驻岗审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkIdZH(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<IntbankOrgAdmitAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            IntbankOrgAdmitAppr intbankOrgAdmitAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            intbankOrgAdmitAppr.setReviewResultZh(reviewResult);
            //获取前端传入参数restDesc-审批意见   审批意见不等同于结论性描述，此处不更新 modify by zhangjw 20210806
            String restDesc = (String)model.getCondition().get("restDesc");
            intbankOrgAdmitAppr.setRestDescZh(restDesc);
            //更新 更新数据时间
            intbankOrgAdmitAppr.setUpdateTime(getCurrrentDate());
            return intbankOrgAdmitApprMapper.updateByPrimaryKey(intbankOrgAdmitAppr);
        }
        return 0;
    }

    /**
     * @param intbankOrgAdmitApp 同业授信机构准入根据最新批复生成批复信息
     * @param currentUserId
     * @param currentOrgId
     * @param issueReportType
     * @param approveStatus
     * @param cur_next_id
     */
    public void generateIntbankOrgAdmitApprService(IntbankOrgAdmitApp intbankOrgAdmitApp, String currentUserId, String currentOrgId, String issueReportType, String approveStatus, String cur_next_id){
        logger.info(this.getClass().getName()+":根据最新批复信息，生成新的批复信息！");

        logger.info("用信条件拷贝判断 开始==》cur_next_id【{}】",cur_next_id);
        String[] ingoreProperties = getApprIngoreProperties(cur_next_id);
        logger.info("用信条件拷贝判断 结束==》ingoreProperties【{}】", Arrays.toString(ingoreProperties));

        //更新申请表至审批中状态
        intbankOrgAdmitApp.setApproveStatus(approveStatus);
        intbankOrgAdmitAppService.update(intbankOrgAdmitApp);

        //申请流水号
        String serno = intbankOrgAdmitApp.getSerno() ;
        //根据申请流水号获取最新的批复信息
        IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprMapper.selectOrgApprBySernoNewInfo(serno) ;
        IntbankOrgAdmitAppr intbankOrgAdmitApprNew = new IntbankOrgAdmitAppr() ;
        //如果审批信息不为空
        if(intbankOrgAdmitAppr!=null){
            //数据拷贝
            BeanUtils.copyProperties(intbankOrgAdmitAppr, intbankOrgAdmitApprNew,ingoreProperties);

            //判断是否拷贝 评审结论和结论性描述
            //1.通用规则：评审结论和结论性描述，都不copy至下一节点；
            //2.例外规则：
            //      流程审批的下一节点如果是信贷管理部风险派驻岗，
            //      则获取上一条审批记录中出具报告类型为审查报告的且ZH审批结论为空的数据，
            //      copy评审结论和结论性描述至生成的最新审批记录数据中
            String nextNodeId = cur_next_id.split(";")[1];
            if (CmisBizConstants.Xdglbfxpzg_NodeIds.contains(nextNodeId+",")){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno",serno);
                queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                queryModel.addCondition("issueReportType",CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
                queryModel.setSort(" createTime desc");
                List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = selectAll(queryModel);
                if (CollectionUtils.nonEmpty(intbankOrgAdmitApprs)){
                    for (IntbankOrgAdmitAppr orgAdmitAppr : intbankOrgAdmitApprs) {
                        if (StringUtils.isBlank(orgAdmitAppr.getReviewResultZh())){
                            intbankOrgAdmitApprNew.setRestDesc(orgAdmitAppr.getRestDesc());
                            intbankOrgAdmitApprNew.setRestDescZh(orgAdmitAppr.getRestDescZh());
                            intbankOrgAdmitApprNew.setReviewResultZh(orgAdmitAppr.getReviewResultZh());
                            intbankOrgAdmitApprNew.setReviewResult(orgAdmitAppr.getReviewResult());
                            break;
                        }
                    }
                    //拷贝上个审查报告相关信息
                    IntbankOrgAdmitAppr intbankOrgAdmitAppr1 = intbankOrgAdmitApprs.get(0);
                    intbankOrgAdmitApprNew.setInteAnaly(intbankOrgAdmitAppr1.getInteAnaly());
                    intbankOrgAdmitApprNew.setInteAnalyZh(intbankOrgAdmitAppr1.getInteAnalyZh());
                }
            }

            //生成主键信息
            intbankOrgAdmitApprNew.setPkId(generatePkId());
            //申请审批流水号 DOTO:此处应该取审批节点流水号吧
            intbankOrgAdmitApprNew.setApproveSerno(generateSerno(SeqConstant.INTBANK_ADMIT_SEQ));
            //判断节点是否为出具审查报告节点、出具批复节点，设置对应的ISSUE_REPORT_TYPE出具报告类型值
            intbankOrgAdmitApprNew.setIssueReportType(issueReportType);
            //更新日期
            intbankOrgAdmitApprNew.setUpdDate(getCurrrentDateStr());
            //更新时间
            intbankOrgAdmitApprNew.setUpdateTime(getCurrrentDate());
            //创建时间
            intbankOrgAdmitApprNew.setCreateTime(getCurrrentDate());
            intbankOrgAdmitApprNew.setUpdBrId(currentOrgId);
            intbankOrgAdmitApprNew.setUpdId(currentUserId);

            intbankOrgAdmitApprMapper.insert(intbankOrgAdmitApprNew) ;

            if (ingoreProperties.length == 4){
                //拷贝用信条件
                lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(intbankOrgAdmitAppr.getApproveSerno(), intbankOrgAdmitApprNew.getApproveSerno());
            }

        }else{
            logger.info("未查找到最新审批信息");
        }
    }

    /**
     * 获取最新审批信息
     * @param condition
     * @return
     */
    public Map<String,Object> selectLastApprBySerno(Map condition) {
        String serno = (String) condition.get("serno");
        String cusId = (String) condition.get("cusId");
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort(" createTime desc ");
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = selectByModel(queryModel);
        if (intbankOrgAdmitApprs!=null && intbankOrgAdmitApprs.size()>0){
            IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprs.get(0);
            return (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(intbankOrgAdmitAppr);
        }else{
            //审批表数据获取失败，从申请表获取数据
            IntbankOrgAdmitApp intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(serno);
            if (intbankOrgAdmitApp != null) {
                return (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(intbankOrgAdmitApp);
            }
        }
        return new HashMap();
    }

    /**
     * 获取最新批复数据
     * @param serno
     * @return
     */
    public IntbankOrgAdmitAppr selectLastAppr(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort(" updateTime desc");

        List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(intbankOrgAdmitApprs)){
            return intbankOrgAdmitApprs.get(0);
        }
        return null;
    }

    /**
     * 保存授信期限
     * 1.审批表数据更改
     * 2.申请表数据更改
     * @param condition
     * @return
     */
    public int saveTerm(Map condition) {
        int result = 0;
        String serno = (String) condition.get("serno");
        Integer term = (Integer) condition.get("term");

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort(" createTime desc ");
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = selectByModel(queryModel);
        if (intbankOrgAdmitApprs!=null && intbankOrgAdmitApprs.size()>0){
            IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprs.get(0);
            intbankOrgAdmitAppr.setTerm(term);
            return update(intbankOrgAdmitAppr);
        }else{
            //审批表数据获取失败，从申请表获取数据
            IntbankOrgAdmitApp intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(serno);
            if (intbankOrgAdmitApp != null) {
                intbankOrgAdmitApp.setTerm(term);
                return intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
            }
        }
        if (result == 0){
            logger.error("授信期限更新失败！");
        }
        return 0;
    }
}
