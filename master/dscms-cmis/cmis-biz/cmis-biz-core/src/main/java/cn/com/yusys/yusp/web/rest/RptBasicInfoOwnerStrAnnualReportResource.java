/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoOwnerStrAnnualReport;
import cn.com.yusys.yusp.service.RptBasicInfoOwnerStrAnnualReportService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoOwnerStrAnnualReportResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:57:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinfoownerstrannualreport")
public class RptBasicInfoOwnerStrAnnualReportResource {
    @Autowired
    private RptBasicInfoOwnerStrAnnualReportService rptBasicInfoOwnerStrAnnualReportService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoOwnerStrAnnualReport>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoOwnerStrAnnualReport> list = rptBasicInfoOwnerStrAnnualReportService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoOwnerStrAnnualReport>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoOwnerStrAnnualReport>> index(QueryModel queryModel) {
        List<RptBasicInfoOwnerStrAnnualReport> list = rptBasicInfoOwnerStrAnnualReportService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoOwnerStrAnnualReport>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptBasicInfoOwnerStrAnnualReport> show(@PathVariable("pkId") String pkId) {
        RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport = rptBasicInfoOwnerStrAnnualReportService.selectByPrimaryKey(pkId);
        return new ResultDto<RptBasicInfoOwnerStrAnnualReport>(rptBasicInfoOwnerStrAnnualReport);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoOwnerStrAnnualReport> create(@RequestBody RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport) throws URISyntaxException {
        rptBasicInfoOwnerStrAnnualReportService.insert(rptBasicInfoOwnerStrAnnualReport);
        return new ResultDto<RptBasicInfoOwnerStrAnnualReport>(rptBasicInfoOwnerStrAnnualReport);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport) throws URISyntaxException {
        int result = rptBasicInfoOwnerStrAnnualReportService.update(rptBasicInfoOwnerStrAnnualReport);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoOwnerStrAnnualReportService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoOwnerStrAnnualReportService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptBasicInfoOwnerStrAnnualReport>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptBasicInfoOwnerStrAnnualReport>>(rptBasicInfoOwnerStrAnnualReportService.selectByModel(model));
    }

    @PostMapping("/updateYearReport")
    protected ResultDto<Integer> updateYearReport(@RequestBody RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport){
        return new ResultDto<Integer>(rptBasicInfoOwnerStrAnnualReportService.updateSelective(rptBasicInfoOwnerStrAnnualReport));
    }
    @PostMapping("/insertYearReport")
    protected ResultDto<Integer> insertYearReport(@RequestBody RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport){
        rptBasicInfoOwnerStrAnnualReport.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptBasicInfoOwnerStrAnnualReportService.insertSelective(rptBasicInfoOwnerStrAnnualReport));
    }
    @PostMapping("/deleteYearReport")
    protected ResultDto<Integer> deleteYearReport(@RequestBody RptBasicInfoOwnerStrAnnualReport rptBasicInfoOwnerStrAnnualReport){
        String pkId = rptBasicInfoOwnerStrAnnualReport.getPkId();
        return new ResultDto<Integer>(rptBasicInfoOwnerStrAnnualReportService.deleteByPrimaryKey(pkId));
    }
}
