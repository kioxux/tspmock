package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppCorreShd
 * @类描述: lmt_app_corre_shd数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:36:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtAppCorreShdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 关联股东客户编号 **/
	private String shdCusId;
	
	/** 关联股东客户名称 **/
	private String shdCusName;
	
	/** 持股金额 **/
	private java.math.BigDecimal shdAmt;
	
	/** 持股比例 **/
	private java.math.BigDecimal shdPerc;
	
	/** 企业性质 **/
	private String corpCha;
	
	/** 备注 **/
	private String remark;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param shdCusId
	 */
	public void setShdCusId(String shdCusId) {
		this.shdCusId = shdCusId == null ? null : shdCusId.trim();
	}
	
    /**
     * @return ShdCusId
     */	
	public String getShdCusId() {
		return this.shdCusId;
	}
	
	/**
	 * @param shdCusName
	 */
	public void setShdCusName(String shdCusName) {
		this.shdCusName = shdCusName == null ? null : shdCusName.trim();
	}
	
    /**
     * @return ShdCusName
     */	
	public String getShdCusName() {
		return this.shdCusName;
	}
	
	/**
	 * @param shdAmt
	 */
	public void setShdAmt(java.math.BigDecimal shdAmt) {
		this.shdAmt = shdAmt;
	}
	
    /**
     * @return ShdAmt
     */	
	public java.math.BigDecimal getShdAmt() {
		return this.shdAmt;
	}
	
	/**
	 * @param shdPerc
	 */
	public void setShdPerc(java.math.BigDecimal shdPerc) {
		this.shdPerc = shdPerc;
	}
	
    /**
     * @return ShdPerc
     */	
	public java.math.BigDecimal getShdPerc() {
		return this.shdPerc;
	}
	
	/**
	 * @param corpCha
	 */
	public void setCorpCha(String corpCha) {
		this.corpCha = corpCha == null ? null : corpCha.trim();
	}
	
    /**
     * @return CorpCha
     */	
	public String getCorpCha() {
		return this.corpCha;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}