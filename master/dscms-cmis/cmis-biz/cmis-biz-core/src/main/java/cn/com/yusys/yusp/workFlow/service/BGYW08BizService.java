package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpDelayAgreement;
import cn.com.yusys.yusp.domain.IqpDelayPayment;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:13
 * @desc 还款计划变更
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW08BizService implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW08BizService.class);

    @Autowired
    private IqpDelayPaymentService iqpDelayPaymentService;

    @Autowired
    private IqpDelayAgreementService iqpDelayAgreementService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //利率变更（小微）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG018.equals(bizType)) {
            iqpBillAcctChgBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    private void iqpBillAcctChgBizApp(ResultInstanceDto instanceInfo, String currentOpType, String delaySerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpDelayPayment iqpDelayPayment = iqpDelayPaymentService.selectByPrimaryKey(delaySerno);
            if (StringUtils.isBlank(delaySerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请" + delaySerno + "申请主表信息");
            if (iqpDelayPayment == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                //TODO 修改借据表还款方式
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);

                //生成待签订协议
                IqpDelayAgreement iqpDelayAgreement = new IqpDelayAgreement();
                BeanUtils.copyProperties(iqpDelayPayment,iqpDelayAgreement);
                iqpDelayAgreement.setAgrSerno(sequenceTemplateClient.getSequenceTemplate("IQP",new HashMap<>()));
                iqpDelayAgreement.setCoreStatus("100");//未生效
                iqpDelayAgreement.setApproveStatus("000");// 待发起
                iqpDelayAgreementService.insertSelective(iqpDelayAgreement);

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpDelayPaymentService.updateSelective(iqpDelayPayment);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW08.equals(flowCode);
    }
}
