package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0047Service;
import cn.com.yusys.yusp.service.risk.RiskItem0048Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0048Resource
 * @类描述:  影像是否已扫描校验
 * @功能描述:
 * @创建人: hubp
 * @创建时间: 2021年7月28日15:56:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0048影像是否已扫描校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0048")
public class RiskItem0048Resource {

    @Autowired
    private RiskItem0048Service riskItem0048Service;
    /**
     * @方法名称: riskItem0048
     * @方法描述: 影像是否已扫描校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: hubp
     * @创建时间: 2021年7月28日15:56:28
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "影像是否已扫描校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0048(@RequestBody QueryModel queryModel) {
        //当流程中打回到客户经理节点时获取不到数据 因已走过风险拦截所以放行
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0048Service.riskItem0048(queryModel));
    }
}
