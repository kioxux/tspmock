package cn.com.yusys.yusp.service.server.xdxw0022;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0022.req.Xdxw0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0022.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0022.resp.Xdxw0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0022Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021-06-03 16:50:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0022Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0022Service.class);

    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Resource
    private AdminSmUserService adminSmUserService; // 用户信息
    @Resource
    private AdminSmOrgService adminSmOrgService; // 机构信息

    /**
     * 根据证件号查询信贷系统的申请信息
     * 智能审贷系统贷款时,查询关系人（借款人、配偶、保证人）的信贷申请历史信息列表
     * @param xdxw0022DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0022DataRespDto xdxw0022(Xdxw0022DataReqDto xdxw0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataReqDto));
        Xdxw0022DataRespDto xdxw0022DataRespDto = new Xdxw0022DataRespDto();
        try {

            if (StringUtils.isEmpty(xdxw0022DataReqDto.getCertNo())) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            } else {
                // 证件号
                String certCode = xdxw0022DataReqDto.getCertNo();
                java.util.List<List> lists = lmtSurveyReportMainInfoMapper.selectLmtSurveyReportMainInfoByCertNo(certCode);
                // 开始新增
                for (int i = 0; i < lists.size(); i++) {
                    List appInfo = lists.get(i);
                    // 客户名称翻译
                    String userName = "";
                    ResultDto<AdminSmUserDto> resultUserDto = adminSmUserService.getByLoginCode(appInfo.getManagerId());
                    if (ResultDto.success().getCode().equals(resultUserDto.getCode())) {
                        AdminSmUserDto adminSmUserDto = resultUserDto.getData();
                        if (!Objects.isNull(adminSmUserDto)) {
                            userName = adminSmUserDto.getUserName();
                        }
                    }
                    appInfo.setManagerName(userName);
                    // 机构名称翻译
                    String orgName = "";
                    ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(appInfo.getOrgName());
                    if (ResultDto.success().getCode().equals(resultOrgDto.getCode())) {
                        AdminSmOrgDto adminSmOrgrDto = resultOrgDto.getData();
                        if (!Objects.isNull(adminSmOrgrDto)) {
                            orgName = adminSmOrgrDto.getOrgName();
                        }
                    }
                    appInfo.setOrgName(orgName);
                    // 查询对应的字典
                    logger.info("从Redis中获取字典项缓存开始");
                    // 码值转换
                    String loanStatusName = DictTranslatorUtils.findValueByDictKey("STD_ZB_APPR_STATUS", appInfo.getLoanStatus());
                    appInfo.setLoanStatus(loanStatusName);
                    // 担保方式名称
                    String guarType = appInfo.getAssureMeans();
                    String guarTypeName = cn.com.yusys.yusp.commons.util.StringUtils.EMPTY;
                    if (StringUtil.isNotEmpty(guarType)) {
                        guarTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY", guarType);
                    }
                    appInfo.setAssureMeans(guarTypeName);
                }
                xdxw0022DataRespDto.setList(lists);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataRespDto));
        return xdxw0022DataRespDto;
    }
}
