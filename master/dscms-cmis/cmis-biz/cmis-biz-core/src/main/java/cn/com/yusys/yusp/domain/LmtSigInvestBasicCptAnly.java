/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicCptAnly
 * @类描述: lmt_sig_invest_basic_cpt_anly数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 19:07:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_basic_cpt_anly")
public class LmtSigInvestBasicCptAnly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 底层分析类别值 **/
	@Column(name = "BOTTOM_ANALY_CLS_VALVE", unique = false, nullable = true, length = 5)
	private String bottomAnalyClsValve;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 单户余额区间 **/
	@Column(name = "SINGLE_BALANCE_RANGE", unique = false, nullable = true, length = 50)
	private String singleBalanceRange;
	
	/** 利率分布 **/
	@Column(name = "RATE_DISTRIBUTION", unique = false, nullable = true, length = 40)
	private String rateDistribution;
	
	/** 剩余期限 **/
	@Column(name = "REMAINING_TERM", unique = false, nullable = true, length = 40)
	private String remainingTerm;
	
	/** 行业 **/
	@Column(name = "TRADE", unique = false, nullable = true, length = 5)
	private String trade;
	
	/** 余额占比 **/
	@Column(name = "BAL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal balPerc;
	
	/** 笔数占比 **/
	@Column(name = "QNT_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal qntPerc;
	
	/** 分布类型 **/
	@Column(name = "DISTRIBUT_TYPE", unique = false, nullable = true, length = 5)
	private String distributType;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bottomAnalyClsValve
	 */
	public void setBottomAnalyClsValve(String bottomAnalyClsValve) {
		this.bottomAnalyClsValve = bottomAnalyClsValve;
	}
	
    /**
     * @return bottomAnalyClsValve
     */
	public String getBottomAnalyClsValve() {
		return this.bottomAnalyClsValve;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param singleBalanceRange
	 */
	public void setSingleBalanceRange(String singleBalanceRange) {
		this.singleBalanceRange = singleBalanceRange;
	}
	
    /**
     * @return singleBalanceRange
     */
	public String getSingleBalanceRange() {
		return this.singleBalanceRange;
	}
	
	/**
	 * @param rateDistribution
	 */
	public void setRateDistribution(String rateDistribution) {
		this.rateDistribution = rateDistribution;
	}
	
    /**
     * @return rateDistribution
     */
	public String getRateDistribution() {
		return this.rateDistribution;
	}
	
	/**
	 * @param remainingTerm
	 */
	public void setRemainingTerm(String remainingTerm) {
		this.remainingTerm = remainingTerm;
	}
	
    /**
     * @return remainingTerm
     */
	public String getRemainingTerm() {
		return this.remainingTerm;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}
	
    /**
     * @return trade
     */
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param balPerc
	 */
	public void setBalPerc(java.math.BigDecimal balPerc) {
		this.balPerc = balPerc;
	}
	
    /**
     * @return balPerc
     */
	public java.math.BigDecimal getBalPerc() {
		return this.balPerc;
	}
	
	/**
	 * @param qntPerc
	 */
	public void setQntPerc(java.math.BigDecimal qntPerc) {
		this.qntPerc = qntPerc;
	}
	
    /**
     * @return qntPerc
     */
	public java.math.BigDecimal getQntPerc() {
		return this.qntPerc;
	}
	
	/**
	 * @param distributType
	 */
	public void setDistributType(String distributType) {
		this.distributType = distributType;
	}
	
    /**
     * @return distributType
     */
	public String getDistributType() {
		return this.distributType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}