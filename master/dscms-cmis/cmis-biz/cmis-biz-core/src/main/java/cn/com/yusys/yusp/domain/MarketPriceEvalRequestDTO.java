package cn.com.yusys.yusp.domain;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

/**
 * @version 1.0.0
 * @项目名称:eval
 * @类名称:EvalRequestDTO
 * @类描述:市场价格法估值计算请求DTO
 * @功能描述:
 * @创建时间:2021年3月24日
 * @修改备注:
 * @修改日期 修改人员        修改原因 --------    --------		----------------------------------------
 * @Copyright (c) 2018宇信科技-版权所有
 */
public class MarketPriceEvalRequestDTO {

    @NotNull
    private BigDecimal number;
    @NotNull
    private BigDecimal price;
    private String channel;

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
