package cn.com.yusys.yusp.web.server.xdcz0028;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0028.req.Xdcz0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.Xdcz0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0028.Xdcz0028Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:生成商贷账号数据文件
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0028:生成商贷账号数据文件")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0028Resource.class);

    @Autowired
    private Xdcz0028Service xdcz0028Service;
    /**
     * 交易码：xdcz0028
     * 交易描述：生成商贷账号数据文件
     *
     * @param xdcz0028DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("生成商贷账号数据文件")
    @PostMapping("/xdcz0028")
    protected @ResponseBody
    ResultDto<Xdcz0028DataRespDto>  xdcz0028(@Validated @RequestBody Xdcz0028DataReqDto xdcz0028DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028DataReqDto));
        Xdcz0028DataRespDto  xdcz0028DataRespDto  = new Xdcz0028DataRespDto();// 响应Dto:小贷用途承诺书文本生成pdf
        ResultDto<Xdcz0028DataRespDto>xdcz0028DataResultDto = new ResultDto<>();
        String cert_code = xdcz0028DataReqDto.getCert_code();//证件号
        String cus_id = xdcz0028DataReqDto.getCus_id();//客户号
        String cont_no = xdcz0028DataReqDto.getCont_no();//合同号
        String tranDate = xdcz0028DataReqDto.getTranDate();//交易日期
        String totlQnt = xdcz0028DataReqDto.getTotlQnt();//总笔数
        String fileName = xdcz0028DataReqDto.getFileName();//文件名
        String seqNo = xdcz0028DataReqDto.getSeqNo();//批次号
        try {
            // 从xdcz0028DataReqDto获取业务值进行业务逻辑处理
            xdcz0028DataRespDto = xdcz0028Service.xdcz0028(xdcz0028DataReqDto);
            xdcz0028DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0028DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value,e.getMessage());
            // 封装xdcz0028DataResultDto中异常返回码和返回信息
            xdcz0028DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0028DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0028DataRespDto到xdcz0028DataResultDto中
        xdcz0028DataResultDto.setData(xdcz0028DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028DataRespDto));
        return xdcz0028DataResultDto;
    }
}
