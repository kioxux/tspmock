/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.dto.IqpContExtDto;
import cn.com.yusys.yusp.vo.IqpContExtVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpContExt;
import cn.com.yusys.yusp.service.IqpContExtService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpContExtResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 15:34:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcontext")
public class IqpContExtResource {
    @Autowired
    private IqpContExtService iqpContExtService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpContExt>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpContExt> list = iqpContExtService.selectAll(queryModel);
        return new ResultDto<List<IqpContExt>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpContExt>> index(QueryModel queryModel) {
        List<IqpContExt> list = iqpContExtService.selectByModel(queryModel);
        return new ResultDto<List<IqpContExt>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:展期申请列表及历史
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<IqpContExt>> query(@RequestBody QueryModel queryModel) {
        List<IqpContExt> list = iqpContExtService.selectByModel(queryModel);
        return new ResultDto<List<IqpContExt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/show")
    protected ResultDto<IqpContExt> show(@RequestBody String iqpSerno) {
        IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpContExt>(iqpContExt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpContExt> create(@Validated @RequestBody IqpContExt iqpContExt) throws URISyntaxException {
        iqpContExtService.insert(iqpContExt);
        return new ResultDto<IqpContExt>(iqpContExt);
    }

    /**
     * @函数名称:create
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tempsave")
    protected ResultDto<IqpContExt> tempsave(@RequestBody IqpContExt iqpContExt) throws URISyntaxException {
        iqpContExtService.appExtInit(iqpContExt);
        return new ResultDto<IqpContExt>(iqpContExt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateextapp")
    protected ResultDto<Integer> updateextapp(@RequestBody IqpContExtVo iqpContExtVo) throws URISyntaxException {
        int result = iqpContExtService.updateextapp(iqpContExtVo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpContExt iqpContExt) throws URISyntaxException {
        int result = iqpContExtService.update(iqpContExt);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicdelete
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicdelete")
    protected ResultDto<Integer> logicdelete(@RequestBody IqpContExt iqpContExt) throws URISyntaxException {
        int result = iqpContExtService.updateSelective(iqpContExt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpContExtService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpContExtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:展期申请列表及历史
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryforzhcx")
    protected ResultDto<List<IqpContExtDto>> queryForZhcx(@RequestBody QueryModel queryModel) {
        List<IqpContExtDto> list = iqpContExtService.selectByModelForZhcx(queryModel);
        return new ResultDto<List<IqpContExtDto>>(list);
    }
}
