/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdDrawCheck
 * @类描述: sxkd_draw_check数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-11 22:06:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "sxkd_draw_check")
public class SxkdDrawCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	/** 出账申请流水 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = false, length = 40)
	private String pvpSerno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 100)
	private String contNo;
	
	/** 中文合同编号 **/
	@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 100)
	private String contCnNo;
	
	/** 放款账户 **/
	@Column(name = "DISB_ACCT", unique = false, nullable = true, length = 40)
	private String disbAcct;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 放款金额 **/
	@Column(name = "PVP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pvpAmt;
	
	/** 放款时间 **/
	@Column(name = "PVP_DATE", unique = false, nullable = true, length = 10)
	private String pvpDate;
	
	/** 借据到期日 **/
	@Column(name = "BILL_END_DATE", unique = false, nullable = true, length = 10)
	private String billEndDate;
	
	/** 出账状态 **/
	@Column(name = "PVP_STATUS", unique = false, nullable = true, length = 20)
	private String pvpStatus;
	
	/** 标志（用于区分动产质押贷还是省心快贷） **/
	@Column(name = "OP_FLAG", unique = false, nullable = true, length = 40)
	private String opFlag;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 40)
	private String repayMode;
	
	/** 还款账号 **/
	@Column(name = "REPAY_ACCNO", unique = false, nullable = true, length = 40)
	private String repayAccno;
	
	/** 投向名称 **/
	@Column(name = "LOAN_TER_NAME", unique = false, nullable = true, length = 40)
	private String loanTerName;
	
	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 5)
	private String loanTer;
	
	/** 省心e付影像流水号 **/
	@Column(name = "IMG_SERNO", unique = false, nullable = false, length = 40)
	private String imgSerno;
	
	/** 审核状态 **/
	@Column(name = "AUDIT_STATUS", unique = false, nullable = true, length = 20)
	private String auditStatus;
	
	/** 审核时间 **/
	@Column(name = "AUDIT_DATE", unique = false, nullable = true, length = 10)
	private String auditDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 10)
	private String approveStatus;
	
	/** 客户经理号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 40)
	private String managerId;
	
	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 管户机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 40)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}
	
    /**
     * @return contCnNo
     */
	public String getContCnNo() {
		return this.contCnNo;
	}
	
	/**
	 * @param disbAcct
	 */
	public void setDisbAcct(String disbAcct) {
		this.disbAcct = disbAcct;
	}
	
    /**
     * @return disbAcct
     */
	public String getDisbAcct() {
		return this.disbAcct;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param pvpAmt
	 */
	public void setPvpAmt(java.math.BigDecimal pvpAmt) {
		this.pvpAmt = pvpAmt;
	}
	
    /**
     * @return pvpAmt
     */
	public java.math.BigDecimal getPvpAmt() {
		return this.pvpAmt;
	}
	
	/**
	 * @param pvpDate
	 */
	public void setPvpDate(String pvpDate) {
		this.pvpDate = pvpDate;
	}
	
    /**
     * @return pvpDate
     */
	public String getPvpDate() {
		return this.pvpDate;
	}
	
	/**
	 * @param billEndDate
	 */
	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}
	
    /**
     * @return billEndDate
     */
	public String getBillEndDate() {
		return this.billEndDate;
	}
	
	/**
	 * @param pvpStatus
	 */
	public void setPvpStatus(String pvpStatus) {
		this.pvpStatus = pvpStatus;
	}
	
    /**
     * @return pvpStatus
     */
	public String getPvpStatus() {
		return this.pvpStatus;
	}
	
	/**
	 * @param opFlag
	 */
	public void setOpFlag(String opFlag) {
		this.opFlag = opFlag;
	}
	
    /**
     * @return opFlag
     */
	public String getOpFlag() {
		return this.opFlag;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno;
	}
	
    /**
     * @return repayAccno
     */
	public String getRepayAccno() {
		return this.repayAccno;
	}
	
	/**
	 * @param loanTerName
	 */
	public void setLoanTerName(String loanTerName) {
		this.loanTerName = loanTerName;
	}
	
    /**
     * @return loanTerName
     */
	public String getLoanTerName() {
		return this.loanTerName;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
	
    /**
     * @return loanTer
     */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param imgSerno
	 */
	public void setImgSerno(String imgSerno) {
		this.imgSerno = imgSerno;
	}
	
    /**
     * @return imgSerno
     */
	public String getImgSerno() {
		return this.imgSerno;
	}
	
	/**
	 * @param auditStatus
	 */
	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}
	
    /**
     * @return auditStatus
     */
	public String getAuditStatus() {
		return this.auditStatus;
	}
	
	/**
	 * @param auditDate
	 */
	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}
	
    /**
     * @return auditDate
     */
	public String getAuditDate() {
		return this.auditDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}