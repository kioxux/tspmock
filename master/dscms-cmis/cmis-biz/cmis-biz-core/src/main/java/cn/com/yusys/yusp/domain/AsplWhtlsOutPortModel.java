/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AsplWhtls
 * @类描述: aspl_whtls数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "资产池白名单配置模板", fileType = ExcelCsv.ExportFileType.XLS)
public class AsplWhtlsOutPortModel {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@ExcelField(title = "业务流水号", viewLength = 40)
	private String serno;
	
	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 40)
	private String cusId;

	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 40)
	private String cusName;
	
	/** 合同编号 **/
	@ExcelField(title = "合同编号", viewLength = 40)
	private String contNo;
	
	/** 合同金额 **/
	@ExcelField(title = "合同金额", viewLength = 20)
	private java.math.BigDecimal contAmt;
	
	/** 导入模式 **/
	@ExcelField(title = "导入模式",dictCode = "STD_ASPL_IMPORT_MODE",viewLength = 20)
	private String importMode;
	
	/** 原因描述 **/
	@ExcelField(title = "原因描述", viewLength = 60)
	private String importResn;
	
	/** 主管客户经理 **/
	@ExcelField(title = "主管客户经理编号", viewLength = 20)
	private String managerId;
	
	/** 主管机构 **/
	@ExcelField(title = "主管机构编号", viewLength = 20)
	private String managerBrId;

	/** 登记人 **/
	@ExcelField(title = "登记人编号", viewLength = 20)
	private String inputId;

	/** 登记机构 **/
	@ExcelField(title = "登记机构编号", viewLength = 20)
	private String inputBrId;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param importMode
	 */
	public void setImportMode(String importMode) {
		this.importMode = importMode;
	}
	
    /**
     * @return importMode
     */
	public String getImportMode() {
		return this.importMode;
	}
	
	/**
	 * @param importResn
	 */
	public void setImportResn(String importResn) {
		this.importResn = importResn;
	}
	
    /**
     * @return importResn
     */
	public String getImportResn() {
		return this.importResn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
}