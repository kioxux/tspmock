/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwydRentIncomeGuess;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysJyxwydRentIncomeGuessMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeGuessService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysJyxwydRentIncomeGuessService {

    @Autowired
    private RptSpdAnysJyxwydRentIncomeGuessMapper rptSpdAnysJyxwydRentIncomeGuessMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptSpdAnysJyxwydRentIncomeGuess selectByPrimaryKey(String pkId) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptSpdAnysJyxwydRentIncomeGuess> selectAll(QueryModel model) {
        List<RptSpdAnysJyxwydRentIncomeGuess> records = (List<RptSpdAnysJyxwydRentIncomeGuess>) rptSpdAnysJyxwydRentIncomeGuessMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptSpdAnysJyxwydRentIncomeGuess> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysJyxwydRentIncomeGuess> list = rptSpdAnysJyxwydRentIncomeGuessMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptSpdAnysJyxwydRentIncomeGuess record) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysJyxwydRentIncomeGuess record) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptSpdAnysJyxwydRentIncomeGuess record) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysJyxwydRentIncomeGuess record) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询
     * @param serno
     * @return
     */
    public List<RptSpdAnysJyxwydRentIncomeGuess> queryIncomeGuessBySerno(String serno) {
        return rptSpdAnysJyxwydRentIncomeGuessMapper.queryIncomeGuessBySerno(serno);
    }

    public int save(RptSpdAnysJyxwydRentIncomeGuess rptSpdAnysJyxwydRentIncomeGuess){
        String pkId = rptSpdAnysJyxwydRentIncomeGuess.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptSpdAnysJyxwydRentIncomeGuess);
        }else {
            rptSpdAnysJyxwydRentIncomeGuess.setPkId(StringUtils.getUUID());
            return insert(rptSpdAnysJyxwydRentIncomeGuess);
        }
    }
}
