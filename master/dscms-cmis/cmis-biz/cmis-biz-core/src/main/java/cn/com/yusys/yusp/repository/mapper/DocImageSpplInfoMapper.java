/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.DocImageSpplInfoPoDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.DocImageSpplInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocImageSpplInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 16:17:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocImageSpplInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocImageSpplInfo selectByPrimaryKey(@Param("disiSerno") String disiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocImageSpplInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocImageSpplInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocImageSpplInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocImageSpplInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocImageSpplInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("disiSerno") String disiSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据业务流水号和审批状态获取影像补扫任务数
     * @author jijian_yx
     * @date 2021/6/30 14:39
     **/
    int selectByBizSerno(Map<String, String> map);

    List<DocImageSpplInfoPoDto> selectSernoByCusId(QueryModel queryModel);

    /**
     * 获取合作方准入/协议影像目录
     * @author jijian_yx
     * @date 2021/9/8 14:37
     **/
    String getCoopImageCode(String bizSerno);

    /**
     * 根据借据号获取出账流水号
     * @author jijian_yx
     * @date 2021/9/16 14:32
     **/
    String selectPvpSernoByBillNo(String billNo);

    /** 获取放款业务条线 **/
    String getBelgLineByBizSerno(String bizSerno);

    /** 是否银承出账 **/
    int isYCAmtByBizSerno(String bizSerno);

    /** 是否委托贷款出账 **/
    int isWTDKAmtByBizSerno(String bizSerno);

    /** 是否零售授信业务 **/
    int isLSIqpByBizSerno(String bizSerno);

    /** 是否单户授信业务 **/
    int isDHIqpByBizSerno(String bizSerno);

    /** 是否集团授信业务 **/
    int isJTIqpByBizSerno(String bizSerno);
}