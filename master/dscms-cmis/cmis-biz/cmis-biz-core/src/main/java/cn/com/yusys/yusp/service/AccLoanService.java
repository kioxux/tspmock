/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.batch.dto.batch.CmisBatchDkkhmxDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.AccLoanToppDto;
import cn.com.yusys.yusp.dto.AllAccContDto;
import cn.com.yusys.yusp.dto.BizAccLoanDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.req.Ln3075ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.resp.Ln3075RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.req.CmisLmt0066ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.resp.CmisLmt0066RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0014.CmisLmt0014Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.vo.AccLoanCusVo;
import cn.com.yusys.yusp.vo.AccLoanListVo;
import cn.com.yusys.yusp.vo.AccLoanVo;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-09 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class AccLoanService {

    private static final Logger log = LoggerFactory.getLogger(AccLoanService.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;
    @Autowired
    private AccDiscService accDiscService;
    @Autowired
    private AccTfLocService accTfLocService;
    @Autowired
    private AccCvrsService accCvrsService;
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private Dscms2SjztClientService dscms2SjztClientService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;
    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;
    @Autowired
    private LmtChgSubAccLoanRelService lmtChgSubAccLoanRelService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private CmisBatchClientService cmisBatchClientService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private CmisLmt0014Service cmisLmt0014Service;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccLoan selectByPrimaryKey(String billNo) {
        return accLoanMapper.selectByPrimaryKey(billNo);
    }

    /**
     * /**
     *
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccLoan> selectAll(QueryModel model) {
        List<AccLoan> records = (List<AccLoan>) accLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccLoan record) {
        return accLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccLoan record) {
        return accLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccLoan record) {
        return accLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccLoan record) {
        return accLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String billNo) {
        return accLoanMapper.deleteByPrimaryKey(billNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accLoanMapper.deleteByIds(ids);
    }

    /**
     * 通过参数集合查询借据台账信息
     *
     * @param params
     * @return
     */
    public List<AccLoan> selectAccLoanByParams(Map params) {
        return accLoanMapper.selectAccLoanByParams(params);
    }

    /**
     * @return
     * @方法名称: selectAccLoanByContNO
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccLoan> selectAccLoanByContNo(Map params) {
        return accLoanMapper.selectAccLoanByContNo(params);
    }

    /**
     * 获取贷款台账信息
     * 获取当前主办人，且台账状态为正常（1-正常）的数据
     *
     * @param bizAccLoanDto
     * @return
     */
    public List<BizAccLoanDto> getAccLoan(@RequestBody BizAccLoanDto bizAccLoanDto) {
        String managerId = bizAccLoanDto.getManagerId();
        String bizType = bizAccLoanDto.getBizType();
        if (StringUtils.isBlank(managerId)) {
            log.info("主办人为空。");
            return null;
        }
        if (StringUtils.isBlank(bizType)) {
            log.info("业务类型为空。");
            return null;
        }
        return accLoanMapper.selectAccLoan(managerId, CmisBizConstants.ACC_STATUS_1, bizType);
    }

    /**
     * 业务统计-余额（万元）
     *
     * @param map
     * @return
     */
    public List<Map> getBusBalance(Map map) {
        return accLoanMapper.selectBusBalance(map);
    }

    /**
     * 业务提醒-N天待还贷款台账统计
     */
    int countAccLoanByDays(int dayNum) {
        return accLoanMapper.countAccLoanByDays(dayNum);
    }

    ;

    /**
     * @方法名称: selectAccDto
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccLoanToppDto> selectAccDto(QueryModel model) {
        model.setSort("pvpSerno asc");
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoanToppDto> accLoanSubDtoList = accLoanMapper.selectAccLoanSubDto(model);
        PageHelper.clearPage();
        return accLoanSubDtoList;

    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-27 20:19
     * @注释 查询借据信息
     */
    public List<AccLoan> selectbilllist(String surveySerno) {
        return accLoanMapper.selectbilllist(surveySerno);
    }

    /**
     * @方法名称: selectAccLoanRepay
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccLoan> selectAccLoanRepay(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 获取基本信息
     *
     * @param pvpSerno
     * @return
     */
    public AccLoan selectByAccLoanPvpSerno(String pvpSerno) {
        //return pvpLoanAppMapper.selectByPvpLoanSernoKey(iqpSerno);
        return accLoanMapper.selectByPvpSerno(pvpSerno);
    }

    /**
     * 获取基本信息
     *
     * @param billNo
     * @return
     */
    public AccLoan queryAccLoanDataByBillNo(String billNo) {
        return accLoanMapper.queryAccLoanDataByBillNo(billNo);
    }


    /**
     * @方法名称：selectForAccLoanInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 14:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccLoan> selectForAccLoanInfo(String cusId) {
        return accLoanMapper.selectForAccLoanInfo(cusId);
    }


    /**
     * @return
     * @方法名称: queryAccLoanByCont
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public List<AccLoan> queryAccLoanByCont(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:44
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccLoan> selectByContNo(String contNo) {
        return accLoanMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称：getStockBill
     * @方法描述：存量借据
     * @创建人：zhangming12
     * @创建时间：2021/5/21 16:05
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AllAccContDto> getStockBill(QueryModel queryModel) {
        String contNo = (String) queryModel.getCondition().get("contNo");
        if (StringUtils.isBlank(contNo)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        List<AllAccContDto> allAccContDtoList = new ArrayList<>();

        // 普通贷款合同台账
        List<AccLoan> accLoanList = this.selectByContNo(contNo);
        if (!accLoanList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccLoan accLoan : accLoanList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accLoan, allAccContDto);
                allAccContDto.setBillType("01");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }
        //  银承合同台账
        List<AccAccp> accAccpList = accAccpService.selectByContNo(contNo);
        if (!accAccpList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccAccp accAccp : accAccpList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accAccp, allAccContDto);
                allAccContDto.setBillNo(accAccp.getCoreBillNo());
                allAccContDto.setBillType("02");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }
        // 委托贷款台账
        List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByContNo(contNo);
        if (!accEntrustLoanList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccEntrustLoan accEntrustLoan : accEntrustLoanList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accEntrustLoan, allAccContDto);
                allAccContDto.setBillType("03");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }

        //保函台账
        List<AccCvrs> accCvrsList = accCvrsService.selectByContNo(contNo);
        if (!accCvrsList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccCvrs accCvrs : accCvrsList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accCvrs, allAccContDto);
                allAccContDto.setBillType("04");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }

        // 开证台账
        List<AccTfLoc> accTfLocList = accTfLocService.selectByContNo(contNo);
        if (!accTfLocList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccTfLoc accTfLoc : accTfLocList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accTfLoc, allAccContDto);
                allAccContDto.setBillType("05");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }

        // 贴现台账
        List<AccDisc> accDiscList = accDiscService.selectByContNo(contNo);
        if (!accDiscList.isEmpty()) {
            Collection<AllAccContDto> allAccContDtos = new ArrayList<>();
            for (AccDisc accDisc : accDiscList) {
                AllAccContDto allAccContDto = new AllAccContDto();
                BeanUtils.beanCopy(accDisc, allAccContDto);
                allAccContDto.setBillType("05");
                allAccContDtos.add(allAccContDto);
            }
            allAccContDtoList.addAll(allAccContDtos);
        }
        return allAccContDtoList;
    }

    /**
     * @author zlf
     * @date 2021/5/25 10:48
     * @version 1.0.0
     * @desc 根据借据号查询单条信息
     * @修改历史 修改时间 修改人员 修改原因
     */
    public AccLoan selectByBillNo(String billNo) {
        return accLoanMapper.selectByBillNo(billNo);
    }

    /**
     * @方法名称: getAccLoanList
     * @方法描述: 根据合同号查询贷款台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.xdht0012.resp.BillList> getAccLoanList(@Param("contNo") String contNo) {
        return accLoanMapper.getAccLoanList(contNo);
    }

    /**
     * @方法名称: selectByCusIdOnAssetType
     * @方法描述: 根据客户编号查询 包括台账状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccLoanDto> selectByCusIdOnAssetType(String cusId) {
        List<AccLoanDto> records = (List<AccLoanDto>) accLoanMapper.selectByCusIdOnAssetType(cusId);
        return records;
    }

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询 清收计划管理Excel导入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccLoanDto> selectByCusId(AccLoanDto accLoandto) {
        List<AccLoanDto> records = (List<AccLoanDto>) accLoanMapper.selectByCusId(accLoandto);
        if (CollectionUtils.nonEmpty(records)) {
            return records;
        } else {
            return null;
        }
    }


    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询 不包括台账状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccLoanDto> selectByCusIdNotType(String cusId) {
        List<AccLoanDto> records = (List<AccLoanDto>) accLoanMapper.selectByCusIdNotType(cusId);
        return records;
    }

    /**
     * 查询客户所在企业是否有逾期、欠息贷款
     *
     * @param queryMap
     * @return
     */
    public int queryAccLoanDebitInit(Map<String, String> queryMap) {
        return accLoanMapper.queryAccLoanDebitInit(queryMap);
    }

    /**
     * 查询合同所用币种是否为人民币
     *
     * @param
     * @return
     */
    public String selectCurType(String billNo, String contNo) {
        QueryModel model = new QueryModel();
        model.addCondition("billNo", billNo);
        model.addCondition("contNo", contNo);
        List<AccLoan> accLoans = accLoanMapper.selectByModel(model);
        if (accLoans != null && accLoans.size() > 0) {
            AccLoan accLoan = accLoans.get(0);
            return accLoan.getContCurType();
        }
        return null;
    }


    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:44
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccLoanDto> queryAccLoanByContNoForNpam(List<String> contNoList) {
        List<AccLoanDto> list = accLoanMapper.queryAccLoanByContNoForNpam(contNoList);
        return list;
    }

    /**
     * 该客户所在企业贷款历史总额
     *
     * @param
     * @return
     */
    public BigDecimal getHisLoanAmt(String cus_id) {
        return accLoanMapper.getHisLoanAmt(cus_id);
    }


    /**
     * 根据客户号查询是否存在未关闭的贷款台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAccByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAccByCusId(cusId);
    }

    /**
     * @方法名称：selectSumLoanAmtByParams
     * @方法描述：根据合同号，台账状态查询贷款总余额
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    public BigDecimal selectSumLoanBalAmtByParams(String contNo, String[] status) {
        return accLoanMapper.selectSumLoanBalAmtByParams(contNo, status);
    }

    /**
     * 获取全行线上签约的合同的总贷款余额
     *
     * @return
     */
    public BigDecimal getTotalOnlineLoanBalance() {
        return accLoanMapper.getTotalOnlineLoanBalance();
    }

    /**
     * 获取合同下7天内的放款金额
     *
     * @return
     * @Author: YX-LZW
     */
    public BigDecimal get7DayTotalLoanAmtByContNo(Map<String, String> queryMap) {
        return accLoanMapper.get7DayTotalLoanAmtByContNo(queryMap);
    }

    /**
     * @方法名称：queryAccLoanByCusId
     * @方法描述：
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccLoanDto> queryAccLoanByCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoanDto> list = accLoanMapper.queryAccLoanByCusId(model);
        PageHelper.clearPage();
        return list;
    }

    public void deleteByBillNo(String billNo) {
        accLoanMapper.deleteByBillNo(billNo);
    }

    /**
     * @方法名称：selectInfoByCusIdDate
     * @方法描述：根据客户编号灵活查询
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model) {
        return accLoanMapper.selectInfoByCusIdDate(model);
    }

    /**
     * @param model 分页查询类
     * @函数名称:
     * @函数描述:查询贷款台账下的机构信息
     * @参数与返回说明:
     * @算法描述:
     * @author cainingbo_yx
     */
    public List<String> selectSigDisbOrgIdListByModel(QueryModel model) {
        return accLoanMapper.selectSigDisbOrgIdListByModel(model);
    }

    /**
     * @param
     * @函数名称:
     * @函数描述:综合查询（贷款风险分类支行汇总表（实时））
     * @参数与返回说明:
     * @算法描述:
     */
    public AccDiscClassificationDto selectClassificationSubBranchSummary(String orgCode) {
        return accLoanMapper.selectClassificationSubBranchSummary(orgCode);
    }

    /**
     * 异步导出对公贷款实时报表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccLoan(QueryModel model) {
        model.getCondition().put("belgLineList", "03,04");
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            String apiUrl = "/api/accloan/exportaccloan";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return queryAccLoan(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccLoanVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步导出对私贷款实时报表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccLoanCus(QueryModel model) {
        model.getCondition().put("belgLineList", "01,02");
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            String apiUrl = "/api/accloan/exportaccloancus";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return queryAccLoan(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccLoanCusVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 根据查询条件查询台账信息并返回
     *
     * @param model
     * @return
     */
    public List<AccLoan> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        long start = System.currentTimeMillis();
        List<AccLoan> list = accLoanMapper.querymodelByCondition(model);
        long end = System.currentTimeMillis();
        log.info("AccLoanService : querymodelByCondition, cost " + (end - start) + " ms");
        PageHelper.clearPage();
        return list;
    }

    /**
     * 下载贷款台账
     *
     * @param model
     * @return
     */
    public ProgressDto asyncExportAccLoanList(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("loanStartDate desc");
            String apiUrl = "/api/accloan/exportAccLoanList";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return exportAccLoanData(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccLoanListVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    private List<AccLoanListVo> exportAccLoanData(QueryModel model) {
        List<AccLoanListVo> loanListVoList = new ArrayList<>();
        List<AccLoan> accLoanList = querymodelByCondition(model);
        for (AccLoan accLoan : accLoanList) {
            AccLoanListVo accLoanListVo = new AccLoanListVo();
            String managerIdName = OcaTranslatorUtils.getUserName(accLoan.getManagerId());// 客户经理名称
            String managerBrIdName = OcaTranslatorUtils.getOrgName(accLoan.getManagerBrId());// 责任机构名称
            org.springframework.beans.BeanUtils.copyProperties(accLoan, accLoanListVo);
            accLoanListVo.setManagerIdName(managerIdName);
            accLoanListVo.setManagerBrIdName(managerBrIdName);
            loanListVoList.add(accLoanListVo);
        }
        return loanListVoList;
    }

    /**
     * @return
     * @方法名称: queryAccLoan
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccLoanQuerytDto> queryAccLoan(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.selectByModel(model);
        List<AccLoanQuerytDto> result = new ArrayList<>();
        for (AccLoan loanlist : list) {
            AccLoanQuerytDto AccLoanMap = new AccLoanQuerytDto();
            org.springframework.beans.BeanUtils.copyProperties(loanlist, AccLoanMap);
            String managerIdName = OcaTranslatorUtils.getUserName(loanlist.getManagerId());// 客户经理名称
            String managerBrIdName = OcaTranslatorUtils.getOrgName(loanlist.getManagerBrId());// 责任机构名称
            AccLoanMap.setManagerIdName(managerIdName);
            AccLoanMap.setManagerBrIdName(managerBrIdName);
            result.add(AccLoanMap);
        }
        PageHelper.clearPage();
        return result;
    }

    /**
     * @方法名称: selectAccLoanInfoByParams
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:qw
     */
    public List<AccLoan> selectAccLoanInfoByParams(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List list = new ArrayList();
        if (model.getCondition().get("contNo") == null || "".equals(model.getCondition().get("contNo"))) {
            return list;
        }
        list = accLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页(pvp_authorize)
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccLoan> selectByModelAll(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.selectByModelAll(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @author zlf
     * @date 2021/5/25 10:48
     * @version 1.0.0
     * @desc 根据借据号查询单条信息
     * @修改历史 修改时间 修改人员 修改原因
     */
    public AccLoanAndPvpDto selectByBillNoPvp(AccLoan accLoan) {
        return accLoanMapper.selectByBillNoPvp(accLoan);
    }

    /**
     * 根据担保合同编号查询对应的借款合同关联的未结清贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countUsedRecordsByGuarContNo(String guarContNo) {
        return accLoanMapper.countUsedRecordsByGuarContNo(guarContNo);
    }

    /**
     * 根据担保合同编号查询对应的借款合同关联的贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countAccLoanRecordsByGuarContNo(String guarContNo) {
        return accLoanMapper.countAccLoanRecordsByGuarContNo(guarContNo);
    }

    /**
     * 根据合同编号查询未结清贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countUsedRecordsByContNo(String guarContNo) {
        return accLoanMapper.countUsedRecordsByContNo(guarContNo);
    }

    /**
     * 查询担保合同应的核销及转让贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countHxAndZrAccLoanRecords(String guarContNo) {
        return accLoanMapper.countHxAndZrAccLoanRecords(guarContNo);
    }

    /**
     * 查询担保合同对应的产品是个人一手住房按揭贷款、个人一手商用房按揭贷款的贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countFirstHouseAccLoanRecords(String guarContNo) {
        return accLoanMapper.countFirstHouseAccLoanRecords(guarContNo);
    }

    /**
     * @注释 信贷客户核心业绩统计查询列表
     */
    public ResultDto<XdhxQueryTotalListRespDto> xdhxQueryTotalList(XdhxQueryTotalListReqDto reqDto) {
        return dscms2SjztClientService.xdhxQueryTotalList(reqDto);
    }

    /**
     * @方法名称: updateAccStatusByBillNo
     * @方法描述: 根据借据编号修改台账状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateAccStatusByBillNo(AccLoan record) {
        int count = 0;
        if (record != null) {
            AccLoan accLoan = accLoanMapper.selectByBillNo(record.getBillNo());
            if (accLoan != null) {
                count = accLoanMapper.updateByPrimaryKey(record);
            } else {
                throw BizException.error(null, "99999", "当前没有借据信息");
            }
        }
        return count;
    }

    /**
     * 更新台账五级/十级分类
     *
     * @author jijian_yx
     * @date 2021/8/10 23:37
     **/
    @Transactional
    public int updateAccLoanByBillNo(Map<String, String> map) {
        return accLoanMapper.updateAccLoanByBillNo(map);
    }

    /**
     * @方法名称: queryAccLoanListByCusId
     * @方法描述: 根据客户号查询是否存在未关闭的贷款台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccLoan> queryAccLoanListByCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoan> list = accLoanMapper.queryAccLoanListByCusId(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据业务条线获取可用余额
     *
     * @param orgIdList
     * @param belgLine
     * @return
     */
    public BigDecimal getCurrMonthAllowLoanBalance(List<String> orgIdList, String belgLine, List<String> linshouPrdIds) {
        if (CollectionUtils.nonEmpty(orgIdList) && !StringUtils.isBlank(belgLine)) {
            BigDecimal currMonthAllowLoanBalance = accLoanMapper.getCurrMonthAllowLoanBalance(orgIdList, belgLine,linshouPrdIds);
            return currMonthAllowLoanBalance == null ? BigDecimal.ZERO : currMonthAllowLoanBalance;
        }
        return BigDecimal.ZERO;
    }

    /**
     * @return
     * @方法名称: selectAccLoanByContNO
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccLoan> queryAccLoanByContNo(Map params) {
        return accLoanMapper.queryAccLoanByContNo(params);
    }

    /**
     * @return
     * @方法名称: selectUnClearByContNo
     * @方法描述: 根据合同编号查询未结清
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectUnClearByContNo(String contNo) {
        return accLoanMapper.selectUnClearByContNo(contNo);
    }

    /**
     * @return
     * @方法名称: selectSumLoanByTContNo
     * @方法描述: 根据合同编号查询未结清
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectSumLoanByTContNo(String tContNo) {
        return accLoanMapper.selectSumLoanByTContNo(tContNo);
    }

    /**
     * @方法名称: selectFiveClassByCusId
     * @方法描述: 通过客户编号查询台账的五级分类
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccLoan selectFiveClassByCusId(String cusId) {
        return accLoanMapper.selectFiveClassByCusId(cusId);
    }

    /**
     * @方法名称: queryAssociatedAccLoan
     * @方法描述: 查询关联的贷款台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccLoan> queryAssociatedAccLoan(QueryModel queryModel) throws Exception {
        List<AccLoan> accLoanList = new ArrayList<>();
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(queryModel.getCondition().get("loanModal"))
                || CmisCommonConstants.STD_LOAN_MODAL_6.equals(queryModel.getCondition().get("loanModal"))) {
            String contNo = (String) queryModel.getCondition().get("contNo");
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            if (Objects.isNull(ctrLoanCont)) {
                CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
                if (Objects.isNull(ctrHighAmtAgrCont)) {
                    log.error("根据合同编号【{}】，查询合同信息异常！", contNo);
                    throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
                } else {
                    // 最高额授信协议的借新还旧、无还本续贷业务列表
                    /*
                     * 1.根据合同编号查询最新的分项额度编号
                     * 2.根据返回的分项额度编号和产品编号查询出账的产品额度编号
                     */
                    CmisLmt0066ReqDto cmisLmt0066ReqDto = new CmisLmt0066ReqDto();
                    cmisLmt0066ReqDto.setDealBizNo(contNo);
                    log.info("根据合同编号【{}】，前往额度系统查询合同关联的额度分项", contNo);
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDtoResultDto = cmisLmtClientService.cmislmt0066(cmisLmt0066ReqDto);
                    log.info("根据合同编号【{}】，前往额度系统查询合同关联的额度分项,响应报文为", JSON.toJSONString(cmisLmt0066RespDtoResultDto));
                    if (!"0".equals(cmisLmt0066RespDtoResultDto.getCode())) {
                        log.error("接口调用异常！");
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    if (!"0000".equals(cmisLmt0066RespDtoResultDto.getData().getErrorCode())) {
                        log.error("调用额度66接口查询合同关联的额度分项异常，异常信息为【{}】", cmisLmt0066RespDtoResultDto.getData().getErrorMsg());
                        throw BizException.error(null, cmisLmt0066RespDtoResultDto.getData().getErrorCode(), cmisLmt0066RespDtoResultDto.getData().getErrorMsg());
                    }
                    String lmtAccNo = cmisLmt0066RespDtoResultDto.getData().getParentId();
                    log.info("本最高额授信协议【{}】关联的分项额度编号为【{}】", contNo, lmtAccNo);
                    CmisLmt0046ReqDto cmisLmt0046ReqDto = new CmisLmt0046ReqDto();
                    cmisLmt0046ReqDto.setPrdId((String) queryModel.getCondition().get("prdId"));
                    cmisLmt0046ReqDto.setAccSubNo(lmtAccNo);
                    log.info("根据分项额度编号【{}】，前往额度系统查询分项下额度品种开始,请求报文为:【{}】", lmtAccNo, JSON.toJSONString(cmisLmt0046ReqDto));
                    ResultDto<CmisLmt0046RespDto> cmisLmt0046RespDtoResultDto = cmisLmtClientService.cmislmt0046(cmisLmt0046ReqDto);
                    log.info("根据分项额度编号【{}】，前往额度系统查询分项下额度品种结束,响应报文为:【{}】", lmtAccNo, JSON.toJSONString(cmisLmt0046RespDtoResultDto));
                    if (!"0".equals(cmisLmt0046RespDtoResultDto.getCode())) {
                        log.error("接口调用异常！");
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    if (!"0000".equals(cmisLmt0046RespDtoResultDto.getData().getErrorCode())) {
                        log.error("根据分项额度编号【{}】，前往额度系统查询分项下额度品种异常！", lmtAccNo);
                        throw BizException.error(null, cmisLmt0046RespDtoResultDto.getData().getErrorCode(), cmisLmt0046RespDtoResultDto.getData().getErrorMsg());
                    }
                    if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(cmisLmt0046RespDtoResultDto.getData().getHasBussFlag())) {
                        log.error("根据分项额度编号【{}】，前往额度系统查询分项下额度品种异常！", ctrHighAmtAgrCont.getLmtAccNo());
                        throw BizException.error(null, EcbEnum.ECB020047.key, "授信分项中不存在该产品[" + queryModel.getCondition().get("prdId") + "]！");
                    }
                    String accSubPrdNo = cmisLmt0046RespDtoResultDto.getData().getSubNo();
                    LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(accSubPrdNo);
                    if (Objects.isNull(lmtReplyAccSubPrd)) {
                        log.error("根据品种额度编号【{}】，查询分项下额度品种信息！", accSubPrdNo);
                        throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
                    }
                    List<AccLoan> accLoanListData = lmtChgSubAccLoanRelService.queryBySubPrdSerno(lmtReplyAccSubPrd.getSubPrdSerno());
                    accLoanList = accLoanListData;
                }
            } else {
                // 贷款合同的借新还旧、无还本续贷业务列表
                CmisLmt0066ReqDto cmisLmt0066ReqDto = new CmisLmt0066ReqDto();
                cmisLmt0066ReqDto.setDealBizNo(contNo);
                log.info("根据合同编号【{}】，前往额度系统查询合同关联的额度品种编号", contNo);
                ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDtoResultDto = cmisLmtClientService.cmislmt0066(cmisLmt0066ReqDto);
                log.info("根据合同编号【{}】，前往额度系统查询合同关联的额度品种编号,响应报文为", JSON.toJSONString(cmisLmt0066RespDtoResultDto));
                if (!"0".equals(cmisLmt0066RespDtoResultDto.getCode())) {
                    log.error("接口调用异常！");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(cmisLmt0066RespDtoResultDto.getData().getErrorCode())) {
                    log.error("调用额度66接口查询合同关联的额度品种异常，异常信息为【{}】", cmisLmt0066RespDtoResultDto.getData().getErrorMsg());
                    throw BizException.error(null, cmisLmt0066RespDtoResultDto.getData().getErrorCode(), cmisLmt0066RespDtoResultDto.getData().getErrorMsg());
                }
                String lmtAccNo = cmisLmt0066RespDtoResultDto.getData().getLimitSubNo();
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
                if (Objects.isNull(lmtReplyAccSubPrd)) {
                    log.error("根据品种额度编号【{}】，查询分项下额度品种信息！", lmtAccNo);
                    throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
                }
                List<AccLoan> accLoanListData = lmtChgSubAccLoanRelService.queryBySubPrdSerno(lmtReplyAccSubPrd.getSubPrdSerno());
                accLoanList = accLoanListData;
            }
        } else {
            // 小企业无还本续贷列表
            QueryModel queryModelData = new QueryModel();
            queryModelData.addCondition("cusId", queryModel.getCondition().get("cusId"));
            queryModelData.addCondition("contNo", queryModel.getCondition().get("contNo"));
            queryModelData.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            queryModelData.addCondition("guarMode", CmisCommonConstants.GUAR_MODE_10);
            queryModelData.addCondition("accStatusOther", CmisCommonConstants.ACC_STATUS_OTHER);
            queryModelData.setPage(queryModel.getPage());
            queryModelData.setSize(queryModel.getSize());
            List<AccLoan> accLoanListData = this.selectByModel(queryModelData);
            accLoanList = accLoanListData;
        }
        PageHelper.clearPage();
        return accLoanList;
    }

    /**
     * @方法名称：queryBadCus
     * @方法描述：
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccLoanDto> queryBadCus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccLoanDto> list = accLoanMapper.queryBadCus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据借据号从核心同步贷款台账金额及状态
     *
     * @author jijian_yx
     * @date 2021/10/7 14:18
     **/
    public ResultDto synchronizeAccLoan(String billNo) {
        ResultDto resultDto = new ResultDto();
        String code = "";
        String message = "";
        try {
            // ln3100 贷款信息查询
            AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(billNo);
            if (null == accLoan) {
                code = "9999";
                message = "贷款台账信息不存在!";
            } else if (Objects.equals(accLoan.getAccStatus(), "7")) {
                // 台账状态 STD_ACC_STATUS 7 作废
                // 发送额度系统
                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
                List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accLoan.getManagerBrId()));//金融机构代码
                // 唯一编号
                cmisLmt0014ReqDto.setSerno(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                cmisLmt0014ReqDto.setInputId(accLoan.getInputId());
                cmisLmt0014ReqDto.setInputBrId(accLoan.getInputBrId());
                cmisLmt0014ReqDto.setInputDate(accLoan.getInputDate());
                dealBizList.setDealBizNo(billNo);
                dealBizList.setRecoverType("06");// 06 撤销占用
                dealBizList.setRecoverStd("01");// 01 余额
                dealBizList.setBalanceAmtCny(BigDecimal.ZERO);// 台账总余额（人民币）
                dealBizList.setSpacBalanceAmtCny(BigDecimal.ZERO);// 台账敞口余额（人民币）
                cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
                cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

                log.info("作废状态贷款台账,根据借据编号【" + billNo + "】前往额度系统-台账额度恢复请求报文：" + cmisLmt0014ReqDto.toString());
                CmisLmt0014RespDto cmisLmt0014RespDto = cmisLmt0014Service.cmisLmt0014(cmisLmt0014ReqDto);
                log.info("作废状态贷款台账同步完成,根据借据编号【" + billNo + "】前往额度系统-台账额度恢复返回报文：" + cmisLmt0014RespDto.toString());
                code = "0";
                message = "同步成功";
            } else {
                log.info("综合查询贷款台账同步核心开始,借据号[{}],台账状态[{}],贷款余额[{}]", billNo, accLoan.getAccStatus(), accLoan.getLoanBalance());
                Supplier<ResultDto<Ln3100RespDto>> supplierResp = ResultDto<Ln3100RespDto>::new;
                ResultDto<Ln3100RespDto> ln3100RespDtos = supplierResp.get();
                Supplier<Ln3100ReqDto> supplier = Ln3100ReqDto::new;
                Ln3100ReqDto ln3100ReqDto = supplier.get();
                //查询笔数
                ln3100ReqDto.setChxunbis(1);
                //贷款借据号
                ln3100ReqDto.setDkjiejuh(billNo);
                //贷款账号
                ln3100ReqDto.setDkzhangh("");
                //起始笔数
                ln3100ReqDto.setQishibis(0);
                // 调用核心接口，获取贷款台账信息
                ln3100RespDtos = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
                if (null != ln3100RespDtos) {
                    if (!Objects.equals(ln3100RespDtos.getCode(), "0")) {
                        code = ln3100RespDtos.getCode();
                        message = ln3100RespDtos.getMessage();
                        return resultDto;
                    }
                    Ln3100RespDto ln3100RespDto = ln3100RespDtos.getData();
                    String ziczhtai = ln3100RespDto.getZiczhtai();// 资产状态 0不适用;1正常;2转让;3证券化;4化债;5核销
                    String dkzhhzht = ln3100RespDto.getDkzhhzht();// 贷款账户状态 0正常;1销户;2已核销;3准销户;4录入;5已减免
                    BigDecimal ysqianxi = Optional.ofNullable(ln3100RespDto.getYsqianxi()).orElse(BigDecimal.ZERO);// 应收欠息
                    BigDecimal csqianxi = Optional.ofNullable(ln3100RespDto.getCsqianxi()).orElse(BigDecimal.ZERO);// 催收欠息
                    BigDecimal yshofaxi = Optional.ofNullable(ln3100RespDto.getYshofaxi()).orElse(BigDecimal.ZERO);// 应收罚息
                    BigDecimal cshofaxi = Optional.ofNullable(ln3100RespDto.getCshofaxi()).orElse(BigDecimal.ZERO);// 催收罚息
                    BigDecimal fuxiiiii = Optional.ofNullable(ln3100RespDto.getFuxiiiii()).orElse(BigDecimal.ZERO);// 复息
                    BigDecimal yuqibjin = Optional.ofNullable(ln3100RespDto.getYuqibjin()).orElse(BigDecimal.ZERO);// 逾期本金
                    BigDecimal zhchbjin = Optional.ofNullable(ln3100RespDto.getZhchbjin()).orElse(BigDecimal.ZERO);// 正常本金
                    BigDecimal hexiaobj = Optional.ofNullable(ln3100RespDto.getHexiaobj()).orElse(BigDecimal.ZERO);// 核销本金
                    BigDecimal exchangeRate = Optional.ofNullable(accLoan.getExchangeRate()).orElse(BigDecimal.ONE);// 汇率

                    BigDecimal total = ysqianxi.add(csqianxi).add(yshofaxi).add(cshofaxi).add(fuxiiiii).add(yuqibjin);
                    log.info("核心 => 借据号：" + accLoan.getBillNo() + "；资产状态：" + ziczhtai + "；贷款账户状态：" + dkzhhzht + "；合计金额：" + total);

                    String accStatus = accLoan.getAccStatus();//台账状态 STD_ACC_STATUS 0已关闭;1正常;2逾期;3呆滞;4呆账;5已核销;6未出账;7作废;8转让
                    if (Objects.equals(ziczhtai, "2")) {
                        accStatus = "8";// 8转让
                    } else if (Objects.equals(dkzhhzht, "0") && total.compareTo(BigDecimal.ZERO) == 0) {
                        accStatus = "1";// 1正常
                    } else if (Objects.equals(dkzhhzht, "1")) {
                        accStatus = "0";// 0已关闭
                    } else if (Objects.equals(dkzhhzht, "2")) {
                        accStatus = "5";// 5已核销
                    } else if (Objects.equals(dkzhhzht, "5")) {
                        accStatus = "0";// 0已关闭
                    } else if (total.compareTo(BigDecimal.ZERO) > 0) {
                        accStatus = "2";// 2逾期
                    }
                    BigDecimal loanBalance = yuqibjin.add(zhchbjin).add(hexiaobj);// 贷款余额
                    BigDecimal exchangeRmbBal = loanBalance.multiply(exchangeRate);// 折人民币余额
                    log.info("信贷 => 借据号：" + accLoan.getBillNo() + "；台账状态：" + accLoan.getAccStatus() + "；" +
                            "贷款余额：" + accLoan.getLoanBalance().toPlainString() + "；折人民币余额：" + accLoan.getExchangeRmbBal().toPlainString());
                    log.info("核心 => 借据号：" + accLoan.getBillNo() + "；台账状态：" + accStatus + "；" +
                            "贷款余额：" + loanBalance.toPlainString() + "；折人民币余额：" + exchangeRmbBal.toPlainString());
                    accLoan.setLoanBalance(loanBalance);
                    accLoan.setExchangeRmbBal(exchangeRmbBal);
                    accLoan.setAccStatus(accStatus);
                    int result = accLoanMapper.updateByPrimaryKeySelective(accLoan);

                    // 台账状态、余额更新成功，前往额度系统释放额度
                    if (result > 0) {
                        // 发送额度系统
                        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                        CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
                        List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
                        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accLoan.getManagerBrId()));//金融机构代码
                        // 唯一编号
                        cmisLmt0014ReqDto.setSerno(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                        cmisLmt0014ReqDto.setInputId(accLoan.getInputId());
                        cmisLmt0014ReqDto.setInputBrId(accLoan.getInputBrId());
                        cmisLmt0014ReqDto.setInputDate(accLoan.getInputDate());
                        dealBizList.setDealBizNo(billNo);
                        dealBizList.setRecoverType("03");// 03还款
                        dealBizList.setRecoverStd("01");// 01余额
                        dealBizList.setBalanceAmtCny(accLoan.getExchangeRmbBal());// 台账总余额（人民币）
                        // 获取担保方式 21：低风险质押；40：全额保证金；60：低风险；  敞口 送 0
                        String guarMode = accLoan.getGuarMode();
                        if ("21".equals(guarMode) || "40".equals(guarMode) || "60".equals(guarMode)) {
                            dealBizList.setSpacBalanceAmtCny(BigDecimal.ZERO);// 台账敞口余额（人民币）
                        } else {
                            dealBizList.setSpacBalanceAmtCny(accLoan.getExchangeRmbBal());// 台账敞口余额（人民币）
                        }
                        cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
                        cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

                        log.info("贷款台账同步完成,根据借据编号【" + billNo + "】前往额度系统-台账额度恢复请求报文：" + cmisLmt0014ReqDto.toString());
                        CmisLmt0014RespDto cmisLmt0014RespDto = cmisLmt0014Service.cmisLmt0014(cmisLmt0014ReqDto);
                        log.info("贷款台账同步完成,根据借据编号【" + billNo + "】前往额度系统-台账额度恢复返回报文：" + cmisLmt0014RespDto.toString());
                    }

                    code = "0";
                    message = "同步成功";
                    log.info("综合查询贷款台账同步核心结束,借据号[{}],台账状态[{}],贷款余额[{}]", billNo, accStatus, loanBalance);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "9999";
            message = e.getMessage();
            log.info("综合查询贷款台账同步核心异常,原因[{}]", message);
        } finally {
            resultDto.setCode(code);
            resultDto.setMessage(message);
        }
        return resultDto;
    }

    /**
     * @方法名称: updateAccLoan
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateAccLoan(List<AccLoanDto> accLoanDtoList) {
        int count = 0;
        if (CollectionUtils.nonEmpty(accLoanDtoList)) {
            for (AccLoanDto accLoanDto : accLoanDtoList) {
                count = accLoanMapper.updateAccLoan(accLoanDto);
            }
        }
        return count;
    }

    /**
     * 获取多笔借据号下最大逾期天数
     *
     * @author jijian_yx
     * @date 2021/10/13 21:19
     **/
    public int getMaxOverdueDay(List<String> billNos) {
        return accLoanMapper.getMaxOverdueDay(billNos);
    }

    /**
     * 根据客户号查询是否存在未关闭的贷款台账ZX
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAccZXByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAccZXByCusId(cusId);
    }

    /**
     * 根据客户号查询是否存在未结清的银承台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAccpByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAccpByCusId(cusId);
    }

    /**
     * 根据客户号查询是否存在未结清的贴现台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAccdByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAccdByCusId(cusId);
    }

    /**
     * 根据客户号查询是否存在未结清的保函台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAcccByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAcccByCusId(cusId);
    }

    /**
     * 根据客户号查询是否存在未结清的开证台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAcctByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAcctByCusId(cusId);
    }

    /**
     * 根据客户号查询是否存在未结清的委托贷款台账
     *
     * @param cusId
     * @return
     */
    public BigDecimal checkCusNotClosedAcceByCusId(String cusId) {
        return accLoanMapper.checkCusNotClosedAcceByCusId(cusId);
    }

    /**
     * 根据借据号获取还款明细
     * 从日终落表获取客户借据下全量还款明细
     *
     * @author jijian_yx
     * @date 2021/10/20 21:52
     **/
    public ResultDto<List<CmisBatchDkkhmxDto>> getRepayListFromBatch(QueryModel model) {
        return cmisBatchClientService.getRepayListFromBatch(model);
    }

    /**
     * 根据合同编号查询贷款余额总和
     *
     * @param contNos
     * @return
     */
    public BigDecimal selectTotalSpacAmtByContNos(String contNos) {
        return accLoanMapper.selectTotalSpacAmtByContNos(contNos);
    }

    /**
     * @author zhoumw
     * @date 2021年10月22日16:52:28
     * @version 1.0.0
     * @desc 根据借据编号查询五级十级分类
     * @修改历史 修改时间 修改人员 修改原因
     */
    public AccLoanDto selectClassByBillNo(String billNo) {
        return accLoanMapper.selectClassByBillNo(billNo);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/10/23 11:15
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<AccLoan> selectByContNoAndStatus(String contNo) {
        return accLoanMapper.selectByContNoAndStatus(contNo);
    }

    /**
     * @方法名称: queryRepayBillRel
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccLoan> queryAccLoanByPvpserno(QueryModel model) {
        List<AccLoan> list = new ArrayList();
        PageHelper.startPage(model.getPage(), model.getSize());
        String pvpSerno = (String) model.getCondition().get("pvpSerno");
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        log.info("查询放款信息为：" + pvpLoanApp);
        //无还本续贷查询原批复信息中的原合同号，再根据原合同号查询未结清的借据信息
        if ("6".equals(pvpLoanApp.getLoanModal())) {
            List<AccLoan> accList = accLoanMapper.selectByPvpSernoAndModal(model);
            if (CollectionUtils.nonEmpty(accList)) {
                PageHelper.clearPage();
                return accList;
            }
            //借新还旧根据远借据号查询员台账信息
        } else if ("3".equals(pvpLoanApp.getLoanModal())) {
            String replyNo = pvpLoanApp.getReplyNo();
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replyNo);
            log.info("查询批复信息为：" + lmtCrdReplyInfo);
            if (lmtCrdReplyInfo != null) {
                String xdOrigiBillNo = lmtCrdReplyInfo.getXdOrigiBillNo();
                AccLoan accloan = accLoanMapper.selectByBillNo(xdOrigiBillNo);
                if (accloan != null) {
                    list.add(accloan);
                    return list;
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * @方法名称: queryRepayBillRel
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateAccLoanForZQ(QueryModel model) {
        int count = 0;
        // 获取借据信息
        String billNo = (String) model.getCondition().get("billNo");
        // 展期到期日
        String zhanqdqr = (String) model.getCondition().get("extEndDate");
        // 展期金额
        String zhanqije = (String) model.getCondition().get("extAmt");
        // 获取台账信息
        AccLoan accLoan = accLoanMapper.selectByBillNo(billNo);
        if(Objects.nonNull(accLoan)){
            Ln3075ReqDto reqDto = new Ln3075ReqDto();
            // 业务操作标志
            reqDto.setDaikczbz("3");
            // 贷款借据号
            reqDto.setDkjiejuh(billNo);
            //  展期序号
            reqDto.setZhanqixh(new BigDecimal("0"));
            // 展期合同号
            reqDto.setZhanqhth("");
            // 展期日期
            reqDto.setZhanqirq(accLoan.getLoanEndDate().replace("-",""));
            // 展期到期日
            reqDto.setZhanqdqr(zhanqdqr.replace("-",""));
            // 展期金额
            reqDto.setZhanqije(new BigDecimal(zhanqije));
            // 利率类型 lilvleix
            String lilvleix = accLoan.getRateAdjMode(); // 利率调整方式
            String lilvfdfs = accLoan.getIrFloatType(); //  利率浮动方式
            if("01".equals(lilvleix)){ // 01 ---> 6 固定利率
                lilvleix = "6";
                lilvfdfs = "0";
            } else {
                lilvleix = "1";// 02 --> 1 浮动利率
                if("00".equals(lilvfdfs)){ //  00	LPR加点
                    lilvfdfs = "1";
                } else { // 百分比
                    lilvfdfs = "2";
                }
            }
            reqDto.setLilvleix(lilvleix);
            // 正常利率
            BigDecimal zhchlilv = BigDecimal.ZERO;
            zhchlilv = accLoan.getExecRateYear().multiply(new BigDecimal(100)).setScale(7, BigDecimal.ROUND_HALF_UP);
            reqDto.setZhchlilv(zhchlilv);

            // 利率调整方式
            String lilvtzfs = accLoan.getRateAdjType();
            if("IMM".equals(lilvtzfs)){ //立即调整
                lilvtzfs = "4";
            } else if("DDA".equals(lilvtzfs)){ //满一年
                lilvtzfs = "1";
            } else if("NYF".equals(lilvtzfs)){ //每年1.1
                lilvtzfs = "2";
            } else if("FIX".equals(lilvtzfs)){ //固定日
                lilvtzfs = "2";
            } else if("FIA".equals(lilvtzfs)){ //按月
                lilvtzfs = "2";
            } else {
                lilvtzfs = "";
            }
            if("6".equals(lilvleix) || "".equals(lilvleix) || null == lilvleix){ // 01 ---> 6 固定利率
                lilvtzfs = "0";
            }

            reqDto.setLilvtzfs(lilvtzfs);
            // 利率调整周期
            String lilvtzzq = "";
            // 结息间隔周期
            String eiIntervalCycle = accLoan.getEiIntervalCycle();
            // 结息周期单位
            String eiIntervalUnit = accLoan.getEiIntervalUnit();
            // 还款日
            String repayDay = accLoan.getDeductDay();
            repayDay = repayDay.length() == 1 ? "0".concat(repayDay) : repayDay;

            // 标识位 对公客户号以8开头的
            String sy_flag = "A";
            // 节假日顺延标志位,对公客户默认展示1
            if(accLoan.getCusId() !=null && accLoan.getCusId().startsWith("8")){
                sy_flag="N";
            }

            if("Q".equals(eiIntervalUnit)){ // 季
                lilvtzzq = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay + "E";
            } else if ("M".equals(eiIntervalUnit)){ // 按月
                lilvtzzq =  eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay;
            } else if ("Y".equals(eiIntervalUnit)){ // 按年
                lilvtzzq =   eiIntervalCycle + eiIntervalUnit + sy_flag + "12" + repayDay;
            } else {
                lilvtzzq = "";
            }
            reqDto.setLilvtzzq(lilvtzzq);

            // 利率浮动方式
            reqDto.setLilvfdfs(lilvfdfs);

            // 利率浮动值
            BigDecimal lilvfdzh = BigDecimal.ZERO;
            if("1".equals(lilvfdfs)){ //  00	LPR加点
                lilvfdzh = accLoan.getRateFloatPoint();
            } else { // 百分比
                BigDecimal  irFloatRate = accLoan.getIrFloatRate() ;
                if(null != irFloatRate){
                    lilvfdzh = irFloatRate.multiply(new BigDecimal(100));
                } else {
                    lilvfdzh = accLoan.getRateFloatPoint();
                }
            }
            reqDto.setLilvfdzh(lilvfdzh.setScale(6, BigDecimal.ROUND_HALF_UP));

            // 贷款账号
            reqDto.setDkzhangh("");
            // 合同编号
            reqDto.setHetongbh("");
            // 客户号
            reqDto.setKehuhaoo(accLoan.getCusId());
            // 客户名
            reqDto.setKehuzwmc(accLoan.getCusName());
            // 币种
            reqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + accLoan.getContCurType()));
            log.info("逾期贷款展期调用[ln3075|逾期贷款展期]逻辑开始,请求参数为:[{}]", JSON.toJSONString(reqDto));
            ResultDto<Ln3075RespDto> ln3075ResultDto = dscms2CoreLnClientService.ln3075(reqDto);
            log.info("逾期贷款展期调用[ln3075|逾期贷款展期]逻辑结束,响应参数为:[{}]", JSON.toJSONString(ln3075ResultDto));
            String ln3075Code = Optional.ofNullable(ln3075ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3075Meesage = Optional.ofNullable(ln3075ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3075ResultDto.getCode())) {
                count = 1;
            } else {
                throw BizException.error(null, ln3075Code, ln3075Meesage);
            }
        } else {
            throw BizException.error(null, "", "未获取到台账信息!");
        }
        return count;
    }

    /**
     * @方法名称: selectAccLoanByContNoCount
     * @方法描述: 根据传参返回台账笔数
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectAccLoanByParamCount(AccLoan accLoan) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo",accLoan.getContNo());
        List<AccLoan> records = (List<AccLoan>) accLoanMapper.selectByModel(queryModel);
        return records.size();
    }

}