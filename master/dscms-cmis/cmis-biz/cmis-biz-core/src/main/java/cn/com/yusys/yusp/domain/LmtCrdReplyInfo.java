/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyInfo
 * @类描述: lmt_crd_reply_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-25 13:58:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_crd_reply_info")
public class LmtCrdReplyInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 批复流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "REPLY_SERNO")
	private String replySerno;
	
	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = false, length = 40)
	private String surveySerno;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = false, length = 80)
	private String prdName;
	
	/** 产品代码 **/
	@Column(name = "PRD_ID", unique = false, nullable = false, length = 40)
	private String prdId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = false, length = 40)
	private String certCode;
	
	/** 客户等级 **/
	@Column(name = "CUS_LVL", unique = false, nullable = true, length = 4)
	private String cusLvl;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 批复金额 **/
	@Column(name = "REPLY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyAmt;
	
	/** 期限类型 **/
	@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
	private String termType;
	
	/** 申请期限 **/
	@Column(name = "APP_TERM", unique = false, nullable = true, length = 10)
	private Integer appTerm;
	
	/** 额度宽限期（月） **/
	@Column(name = "LMT_GRAPER", unique = false, nullable = true, length = 10)
	private Integer lmtGraper;
	
	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;
	
	/** 批复起始日 **/
	@Column(name = "REPLY_START_DATE", unique = false, nullable = true, length = 10)
	private String replyStartDate;
	
	/** 批复到期日 **/
	@Column(name = "REPLY_END_DATE", unique = false, nullable = true, length = 10)
	private String replyEndDate;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 额度类型 **/
	@Column(name = "LIMIT_TYPE", unique = false, nullable = true, length = 5)
	private String limitType;
	
	/** 本次用信金额 **/
	@Column(name = "CURT_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLoanAmt;
	
	/** 是否受托支付 **/
	@Column(name = "IS_BE_ENTRUSTED_PAY", unique = false, nullable = true, length = 5)
	private String isBeEntrustedPay;
	
	/** 是否有用信条件 **/
	@Column(name = "LOAN_COND_FLG", unique = false, nullable = true, length = 5)
	private String loanCondFlg;
	
	/** 受托类型 **/
	@Column(name = "TRU_PAY_TYPE", unique = false, nullable = true, length = 5)
	private String truPayType;
	
	/** 审批类型 **/
	@Column(name = "APPR_TYPE", unique = false, nullable = true, length = 5)
	private String apprType;
	
	/** 是否涉农 **/
	@Column(name = "AGRI_FLAG", unique = false, nullable = true, length = 5)
	private String agriFlag;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 5)
	private String belgLine;
	
	/** 是否无还本续贷 **/
	@Column(name = "IS_WXBXD", unique = false, nullable = true, length = 5)
	private String isWxbxd;
	
	/** 续贷原合同编号 **/
	@Column(name = "XD_ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String xdOrigiContNo;

	/** 续贷原借据号 **/
	@Column(name = "XD_ORIGI_BILL_NO", unique = false, nullable = true, length = 40)
	private String xdOrigiBillNo;

	/** 续贷原借据号 **/
	@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
	private String loanModal;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 审批权限 **/
	@Column(name = "APPR_AUTH", unique = false, nullable = true, length = 100)
	private String apprAuth;
	
	/** 用信条件 **/
	@Column(name = "LOAN_COND", unique = false, nullable = true, length = 2000)
	private String loanCond;
	
	/** 风控建议 **/
	@Column(name = "RISK_ADVICE", unique = false, nullable = true, length = 2000)
	private String riskAdvice;


	public String getLoanModal() {
		return loanModal;
	}

	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	public String getXdOrigiBillNo() {
		return xdOrigiBillNo;
	}

	public void setXdOrigiBillNo(String xdOrigiBillNo) {
		this.xdOrigiBillNo = xdOrigiBillNo;
	}

	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusLvl
	 */
	public void setCusLvl(String cusLvl) {
		this.cusLvl = cusLvl;
	}
	
    /**
     * @return cusLvl
     */
	public String getCusLvl() {
		return this.cusLvl;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param replyAmt
	 */
	public void setReplyAmt(java.math.BigDecimal replyAmt) {
		this.replyAmt = replyAmt;
	}
	
    /**
     * @return replyAmt
     */
	public java.math.BigDecimal getReplyAmt() {
		return this.replyAmt;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}
	
    /**
     * @return termType
     */
	public String getTermType() {
		return this.termType;
	}
	
	/**
	 * @param appTerm
	 */
	public void setAppTerm(Integer appTerm) {
		this.appTerm = appTerm;
	}
	
    /**
     * @return appTerm
     */
	public Integer getAppTerm() {
		return this.appTerm;
	}
	
	/**
	 * @param lmtGraper
	 */
	public void setLmtGraper(Integer lmtGraper) {
		this.lmtGraper = lmtGraper;
	}
	
    /**
     * @return lmtGraper
     */
	public Integer getLmtGraper() {
		return this.lmtGraper;
	}
	
	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}
	
    /**
     * @return replyStatus
     */
	public String getReplyStatus() {
		return this.replyStatus;
	}
	
	/**
	 * @param replyStartDate
	 */
	public void setReplyStartDate(String replyStartDate) {
		this.replyStartDate = replyStartDate;
	}
	
    /**
     * @return replyStartDate
     */
	public String getReplyStartDate() {
		return this.replyStartDate;
	}
	
	/**
	 * @param replyEndDate
	 */
	public void setReplyEndDate(String replyEndDate) {
		this.replyEndDate = replyEndDate;
	}
	
    /**
     * @return replyEndDate
     */
	public String getReplyEndDate() {
		return this.replyEndDate;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	
    /**
     * @return limitType
     */
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param curtLoanAmt
	 */
	public void setCurtLoanAmt(java.math.BigDecimal curtLoanAmt) {
		this.curtLoanAmt = curtLoanAmt;
	}
	
    /**
     * @return curtLoanAmt
     */
	public java.math.BigDecimal getCurtLoanAmt() {
		return this.curtLoanAmt;
	}
	
	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay;
	}
	
    /**
     * @return isBeEntrustedPay
     */
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}
	
	/**
	 * @param loanCondFlg
	 */
	public void setLoanCondFlg(String loanCondFlg) {
		this.loanCondFlg = loanCondFlg;
	}
	
    /**
     * @return loanCondFlg
     */
	public String getLoanCondFlg() {
		return this.loanCondFlg;
	}
	
	/**
	 * @param truPayType
	 */
	public void setTruPayType(String truPayType) {
		this.truPayType = truPayType;
	}
	
    /**
     * @return truPayType
     */
	public String getTruPayType() {
		return this.truPayType;
	}
	
	/**
	 * @param apprType
	 */
	public void setApprType(String apprType) {
		this.apprType = apprType;
	}
	
    /**
     * @return apprType
     */
	public String getApprType() {
		return this.apprType;
	}
	
	/**
	 * @param agriFlag
	 */
	public void setAgriFlag(String agriFlag) {
		this.agriFlag = agriFlag;
	}
	
    /**
     * @return agriFlag
     */
	public String getAgriFlag() {
		return this.agriFlag;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param isWxbxd
	 */
	public void setIsWxbxd(String isWxbxd) {
		this.isWxbxd = isWxbxd;
	}
	
    /**
     * @return isWxbxd
     */
	public String getIsWxbxd() {
		return this.isWxbxd;
	}
	
	/**
	 * @param xdOrigiContNo
	 */
	public void setXdOrigiContNo(String xdOrigiContNo) {
		this.xdOrigiContNo = xdOrigiContNo;
	}
	
    /**
     * @return xdOrigiContNo
     */
	public String getXdOrigiContNo() {
		return this.xdOrigiContNo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param apprAuth
	 */
	public void setApprAuth(String apprAuth) {
		this.apprAuth = apprAuth;
	}
	
    /**
     * @return apprAuth
     */
	public String getApprAuth() {
		return this.apprAuth;
	}
	
	/**
	 * @param loanCond
	 */
	public void setLoanCond(String loanCond) {
		this.loanCond = loanCond;
	}
	
    /**
     * @return loanCond
     */
	public String getLoanCond() {
		return this.loanCond;
	}
	
	/**
	 * @param riskAdvice
	 */
	public void setRiskAdvice(String riskAdvice) {
		this.riskAdvice = riskAdvice;
	}
	
    /**
     * @return riskAdvice
     */
	public String getRiskAdvice() {
		return this.riskAdvice;
	}


}