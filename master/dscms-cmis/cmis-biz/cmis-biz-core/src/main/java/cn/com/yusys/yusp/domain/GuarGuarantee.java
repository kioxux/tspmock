/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGuarantee
 * @类描述: guar_guarantee数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-28 20:27:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_guarantee")
public class GuarGuarantee extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 保证_ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUARANTY_ID")
	private String guarantyId;
	
	/** 保证担保形式 STD_ZB_GUARANTY_TYPE **/
	@Column(name = "GUARANTY_TYPE", unique = false, nullable = true, length = 5)
	private String guarantyType;
	
	/** 保证方式 STD_ZB_GUARANTEE_TY **/
	@Column(name = "GUARANTEE_TYPE", unique = false, nullable = true, length = 5)
	private String guaranteeType;
	
	/** 币种 STD_ZX_CUR_TYPE **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 3)
	private String curType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal guarAmt;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 120)
	private String remark;
	
	/** 担保人客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 担保人类型 STD_ZB_CUS_TYP **/
	@Column(name = "CUS_TYP", unique = false, nullable = true, length = 5)
	private String cusTyp;
	
	/** 担保人名称 **/
	@Column(name = "ASSURE_NAME", unique = false, nullable = true, length = 60)
	private String assureName;
	
	/** 与借款人关系  STD_ZB_RELATION **/
	@Column(name = "RELATION_LENDER", unique = false, nullable = true, length = 5)
	private String relationLender;
	
	/** 担保人贷款卡号 **/
	@Column(name = "COM_LOAN_CARD", unique = false, nullable = true, length = 16)
	private String comLoanCard;
	
	/** 保证人状态 STD_ZB_GUAR_STATUS **/
	@Column(name = "STATUS_CODE", unique = false, nullable = true, length = 5)
	private String statusCode;
	
	/** 担保人证件号码 **/
	@Column(name = "ASSURE_CERT_CODE", unique = false, nullable = true, length = 40)
	private String assureCertCode;
	
	/** 担保人证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "CER_TYPE", unique = false, nullable = true, length = 2)
	private String cerType;
	
	/** 担保类型细分 STD_ZB_DB_DETAIL **/
	@Column(name = "GUIDE_TYPE", unique = false, nullable = true, length = 50)
	private String guideType;
	
	/** 专业担保公司合作方台帐编号 **/
	@Column(name = "PRJ_ACC_NO", unique = false, nullable = true, length = 32)
	private String prjAccNo;
	
	/** 已恢复额度 **/
	@Column(name = "RESTORED_AMT", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal restoredAmt;
	
	/** 创建人用户编号 **/
	@Column(name = "CREATE_USER_ID", unique = false, nullable = true, length = 30)
	private String createUserId;
	
	/** 客户类型STD_ZB_CUS_TYPE **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 3)
	private String cusType;
	
	/** 企业所有制STD_ZB_QYSYZ **/
	@Column(name = "COM_QYSYZ", unique = false, nullable = true, length = 5)
	private String comQysyz;
	
	/** 保证法律有效性STD_ZB_BZFLYXX **/
	@Column(name = "ENSURE_LEGAL_VALIDITY", unique = false, nullable = true, length = 10)
	private String ensureLegalValidity;
	
	/** 内部评级STD_ZB_GRADE_RANK **/
	@Column(name = "INNER_LEVEL", unique = false, nullable = true, length = 10)
	private String innerLevel;
	
	/** 是否专业担保公司 **/
	@Column(name = "IS_GUAR_COM", unique = false, nullable = true, length = 10)
	private String isGuarCom;
	
	/** 保证人与借款人关联关系STD_ZB_BZRYJKRGLGX **/
	@Column(name = "GUARANTOR_BORROWER_RELATION", unique = false, nullable = true, length = 10)
	private String guarantorBorrowerRelation;
	
	/** 保证人净资产 **/
	@Column(name = "CUS_NET_ASSETS", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal cusNetAssets;
	
	/** 保证人唯一标识 **/
	@Column(name = "GUARANTY_ID_NEW", unique = false, nullable = true, length = 32)
	private String guarantyIdNew;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 借款人客户编号 **/
	@Column(name = "BORROWER_CUS_ID", unique = false, nullable = true, length = 20)
	private String borrowerCusId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param guarantyId
	 */
	public void setGuarantyId(String guarantyId) {
		this.guarantyId = guarantyId;
	}
	
    /**
     * @return guarantyId
     */
	public String getGuarantyId() {
		return this.guarantyId;
	}
	
	/**
	 * @param guarantyType
	 */
	public void setGuarantyType(String guarantyType) {
		this.guarantyType = guarantyType;
	}
	
    /**
     * @return guarantyType
     */
	public String getGuarantyType() {
		return this.guarantyType;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	
    /**
     * @return guaranteeType
     */
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusTyp
	 */
	public void setCusTyp(String cusTyp) {
		this.cusTyp = cusTyp;
	}
	
    /**
     * @return cusTyp
     */
	public String getCusTyp() {
		return this.cusTyp;
	}
	
	/**
	 * @param assureName
	 */
	public void setAssureName(String assureName) {
		this.assureName = assureName;
	}
	
    /**
     * @return assureName
     */
	public String getAssureName() {
		return this.assureName;
	}
	
	/**
	 * @param relationLender
	 */
	public void setRelationLender(String relationLender) {
		this.relationLender = relationLender;
	}
	
    /**
     * @return relationLender
     */
	public String getRelationLender() {
		return this.relationLender;
	}
	
	/**
	 * @param comLoanCard
	 */
	public void setComLoanCard(String comLoanCard) {
		this.comLoanCard = comLoanCard;
	}
	
    /**
     * @return comLoanCard
     */
	public String getComLoanCard() {
		return this.comLoanCard;
	}
	
	/**
	 * @param statusCode
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
    /**
     * @return statusCode
     */
	public String getStatusCode() {
		return this.statusCode;
	}
	
	/**
	 * @param assureCertCode
	 */
	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode;
	}
	
    /**
     * @return assureCertCode
     */
	public String getAssureCertCode() {
		return this.assureCertCode;
	}
	
	/**
	 * @param cerType
	 */
	public void setCerType(String cerType) {
		this.cerType = cerType;
	}
	
    /**
     * @return cerType
     */
	public String getCerType() {
		return this.cerType;
	}
	
	/**
	 * @param guideType
	 */
	public void setGuideType(String guideType) {
		this.guideType = guideType;
	}
	
    /**
     * @return guideType
     */
	public String getGuideType() {
		return this.guideType;
	}
	
	/**
	 * @param prjAccNo
	 */
	public void setPrjAccNo(String prjAccNo) {
		this.prjAccNo = prjAccNo;
	}
	
    /**
     * @return prjAccNo
     */
	public String getPrjAccNo() {
		return this.prjAccNo;
	}
	
	/**
	 * @param restoredAmt
	 */
	public void setRestoredAmt(java.math.BigDecimal restoredAmt) {
		this.restoredAmt = restoredAmt;
	}
	
    /**
     * @return restoredAmt
     */
	public java.math.BigDecimal getRestoredAmt() {
		return this.restoredAmt;
	}
	
	/**
	 * @param createUserId
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
    /**
     * @return createUserId
     */
	public String getCreateUserId() {
		return this.createUserId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param comQysyz
	 */
	public void setComQysyz(String comQysyz) {
		this.comQysyz = comQysyz;
	}
	
    /**
     * @return comQysyz
     */
	public String getComQysyz() {
		return this.comQysyz;
	}
	
	/**
	 * @param ensureLegalValidity
	 */
	public void setEnsureLegalValidity(String ensureLegalValidity) {
		this.ensureLegalValidity = ensureLegalValidity;
	}
	
    /**
     * @return ensureLegalValidity
     */
	public String getEnsureLegalValidity() {
		return this.ensureLegalValidity;
	}
	
	/**
	 * @param innerLevel
	 */
	public void setInnerLevel(String innerLevel) {
		this.innerLevel = innerLevel;
	}
	
    /**
     * @return innerLevel
     */
	public String getInnerLevel() {
		return this.innerLevel;
	}
	
	/**
	 * @param isGuarCom
	 */
	public void setIsGuarCom(String isGuarCom) {
		this.isGuarCom = isGuarCom;
	}
	
    /**
     * @return isGuarCom
     */
	public String getIsGuarCom() {
		return this.isGuarCom;
	}
	
	/**
	 * @param guarantorBorrowerRelation
	 */
	public void setGuarantorBorrowerRelation(String guarantorBorrowerRelation) {
		this.guarantorBorrowerRelation = guarantorBorrowerRelation;
	}
	
    /**
     * @return guarantorBorrowerRelation
     */
	public String getGuarantorBorrowerRelation() {
		return this.guarantorBorrowerRelation;
	}
	
	/**
	 * @param cusNetAssets
	 */
	public void setCusNetAssets(java.math.BigDecimal cusNetAssets) {
		this.cusNetAssets = cusNetAssets;
	}
	
    /**
     * @return cusNetAssets
     */
	public java.math.BigDecimal getCusNetAssets() {
		return this.cusNetAssets;
	}
	
	/**
	 * @param guarantyIdNew
	 */
	public void setGuarantyIdNew(String guarantyIdNew) {
		this.guarantyIdNew = guarantyIdNew;
	}
	
    /**
     * @return guarantyIdNew
     */
	public String getGuarantyIdNew() {
		return this.guarantyIdNew;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param borrowerCusId
	 */
	public void setBorrowerCusId(String borrowerCusId) {
		this.borrowerCusId = borrowerCusId;
	}
	
    /**
     * @return borrowerCusId
     */
	public String getBorrowerCusId() {
		return this.borrowerCusId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}