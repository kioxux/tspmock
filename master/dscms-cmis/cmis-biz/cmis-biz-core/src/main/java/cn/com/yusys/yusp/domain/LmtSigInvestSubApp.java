/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestSubApp
 * @类描述: lmt_sig_invest_sub_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-07 15:23:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_sub_app")
public class LmtSigInvestSubApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 交易市场 **/
	@Column(name = "TRANS_MARKET", unique = false, nullable = true, length = 5)
	private String transMarket;
	
	/** 交易结构 **/
	@Column(name = "TRAN_STR", unique = false, nullable = true, length = 4000)
	private String tranStr;
	
	/** 交易结构图片路径 **/
	@Column(name = "TRAN_STR_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String tranStrPicturePath;
	
	/** 投资类型 **/
	@Column(name = "INVEST_TYPE", unique = false, nullable = true, length = 5)
	private String investType;
	
	/** 承销方式 **/
	@Column(name = "UNDERWRITING_MODE", unique = false, nullable = true, length = 5)
	private String underwritingMode;
	
	/** 我行承销金额 **/
	@Column(name = "BANK_UNDERWRITING_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankUnderwritingAmt;
	
	/** 债项内部分档 **/
	@Column(name = "DEBT_INNER_LEVEL", unique = false, nullable = true, length = 5)
	private String debtInnerLevel;
	
	/** 受托人客户编号 **/
	@Column(name = "ENTRUSTED_CUS_ID", unique = false, nullable = true, length = 20)
	private String entrustedCusId;
	
	/** 受托人客户名称 **/
	@Column(name = "ENTRUSTED_CUS_NAME", unique = false, nullable = true, length = 80)
	private String entrustedCusName;
	
	/** 产品总金额(元) **/
	@Column(name = "PRD_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prdTotalAmt;
	
	/** 资金保管机构 **/
	@Column(name = "CAP_KEEP_ORG", unique = false, nullable = true, length = 40)
	private String capKeepOrg;
	
	/** 主承销商 **/
	@Column(name = "MANAGE_NAME", unique = false, nullable = true, length = 200)
	private String manageName;
	
	/** 非标准化投资类型 **/
	@Column(name = "NSTD_INVEST_TYPE", unique = false, nullable = true, length = 5)
	private String nstdInvestType;
	
	/** 募集资金用途 **/
	@Column(name = "RAISE_AMT_PURP", unique = false, nullable = true, length = 2000)
	private String raiseAmtPurp;
	
	/** 发起人是否为管理人 **/
	@Column(name = "IS_SPONSOR_MANA", unique = false, nullable = true, length = 5)
	private String isSponsorMana;
	
	/** 发起人客户号 **/
	@Column(name = "START_INTBANK_CUS_ID", unique = false, nullable = true, length = 20)
	private String startIntbankCusId;
	
	/** 发起人客户名称 **/
	@Column(name = "START_INTBANK_CUS_NAME", unique = false, nullable = true, length = 80)
	private String startIntbankCusName;
	
	/** 发起人外部评级 **/
	@Column(name = "START_INTBANK_EVAL_LEVEL", unique = false, nullable = true, length = 20)
	private String startIntbankEvalLevel;
	
	/** 发起人外部评级时间 **/
	@Column(name = "START_INTBANK_EVAL_TIME", unique = false, nullable = true, length = 20)
	private String startIntbankEvalTime;
	
	/** 发起人外部评级机构 **/
	@Column(name = "START_INTBANK_EVAL_ORG", unique = false, nullable = true, length = 20)
	private String startIntbankEvalOrg;
	
	/** 发起人是否实现破产隔离 **/
	@Column(name = "IS_SPONSOR_BKRTY_ISOLN", unique = false, nullable = true, length = 5)
	private String isSponsorBkrtyIsoln;
	
	/** 管理人是否实现破产隔离 **/
	@Column(name = "IS_MANA_BKRTY_ISOLN", unique = false, nullable = true, length = 5)
	private String isManaBkrtyIsoln;
	
	/** 是否存在担保及增信情况 **/
	@Column(name = "IS_EXIST_GUAR_CREDITECT", unique = false, nullable = true, length = 5)
	private String isExistGuarCreditect;
	
	/** 其他担保/增信情况说明 **/
	@Column(name = "GUAR_DESC_EXT", unique = false, nullable = true, length = 4000)
	private String guarDescExt;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param transMarket
	 */
	public void setTransMarket(String transMarket) {
		this.transMarket = transMarket;
	}
	
    /**
     * @return transMarket
     */
	public String getTransMarket() {
		return this.transMarket;
	}
	
	/**
	 * @param tranStr
	 */
	public void setTranStr(String tranStr) {
		this.tranStr = tranStr;
	}
	
    /**
     * @return tranStr
     */
	public String getTranStr() {
		return this.tranStr;
	}
	
	/**
	 * @param tranStrPicturePath
	 */
	public void setTranStrPicturePath(String tranStrPicturePath) {
		this.tranStrPicturePath = tranStrPicturePath;
	}
	
    /**
     * @return tranStrPicturePath
     */
	public String getTranStrPicturePath() {
		return this.tranStrPicturePath;
	}
	
	/**
	 * @param investType
	 */
	public void setInvestType(String investType) {
		this.investType = investType;
	}
	
    /**
     * @return investType
     */
	public String getInvestType() {
		return this.investType;
	}
	
	/**
	 * @param underwritingMode
	 */
	public void setUnderwritingMode(String underwritingMode) {
		this.underwritingMode = underwritingMode;
	}
	
    /**
     * @return underwritingMode
     */
	public String getUnderwritingMode() {
		return this.underwritingMode;
	}
	
	/**
	 * @param bankUnderwritingAmt
	 */
	public void setBankUnderwritingAmt(java.math.BigDecimal bankUnderwritingAmt) {
		this.bankUnderwritingAmt = bankUnderwritingAmt;
	}
	
    /**
     * @return bankUnderwritingAmt
     */
	public java.math.BigDecimal getBankUnderwritingAmt() {
		return this.bankUnderwritingAmt;
	}
	
	/**
	 * @param debtInnerLevel
	 */
	public void setDebtInnerLevel(String debtInnerLevel) {
		this.debtInnerLevel = debtInnerLevel;
	}
	
    /**
     * @return debtInnerLevel
     */
	public String getDebtInnerLevel() {
		return this.debtInnerLevel;
	}
	
	/**
	 * @param entrustedCusId
	 */
	public void setEntrustedCusId(String entrustedCusId) {
		this.entrustedCusId = entrustedCusId;
	}
	
    /**
     * @return entrustedCusId
     */
	public String getEntrustedCusId() {
		return this.entrustedCusId;
	}
	
	/**
	 * @param entrustedCusName
	 */
	public void setEntrustedCusName(String entrustedCusName) {
		this.entrustedCusName = entrustedCusName;
	}
	
    /**
     * @return entrustedCusName
     */
	public String getEntrustedCusName() {
		return this.entrustedCusName;
	}
	
	/**
	 * @param prdTotalAmt
	 */
	public void setPrdTotalAmt(java.math.BigDecimal prdTotalAmt) {
		this.prdTotalAmt = prdTotalAmt;
	}
	
    /**
     * @return prdTotalAmt
     */
	public java.math.BigDecimal getPrdTotalAmt() {
		return this.prdTotalAmt;
	}
	
	/**
	 * @param capKeepOrg
	 */
	public void setCapKeepOrg(String capKeepOrg) {
		this.capKeepOrg = capKeepOrg;
	}
	
    /**
     * @return capKeepOrg
     */
	public String getCapKeepOrg() {
		return this.capKeepOrg;
	}
	
	/**
	 * @param manageName
	 */
	public void setManageName(String manageName) {
		this.manageName = manageName;
	}
	
    /**
     * @return manageName
     */
	public String getManageName() {
		return this.manageName;
	}
	
	/**
	 * @param nstdInvestType
	 */
	public void setNstdInvestType(String nstdInvestType) {
		this.nstdInvestType = nstdInvestType;
	}
	
    /**
     * @return nstdInvestType
     */
	public String getNstdInvestType() {
		return this.nstdInvestType;
	}
	
	/**
	 * @param raiseAmtPurp
	 */
	public void setRaiseAmtPurp(String raiseAmtPurp) {
		this.raiseAmtPurp = raiseAmtPurp;
	}
	
    /**
     * @return raiseAmtPurp
     */
	public String getRaiseAmtPurp() {
		return this.raiseAmtPurp;
	}
	
	/**
	 * @param isSponsorMana
	 */
	public void setIsSponsorMana(String isSponsorMana) {
		this.isSponsorMana = isSponsorMana;
	}
	
    /**
     * @return isSponsorMana
     */
	public String getIsSponsorMana() {
		return this.isSponsorMana;
	}
	
	/**
	 * @param startIntbankCusId
	 */
	public void setStartIntbankCusId(String startIntbankCusId) {
		this.startIntbankCusId = startIntbankCusId;
	}
	
    /**
     * @return startIntbankCusId
     */
	public String getStartIntbankCusId() {
		return this.startIntbankCusId;
	}
	
	/**
	 * @param startIntbankCusName
	 */
	public void setStartIntbankCusName(String startIntbankCusName) {
		this.startIntbankCusName = startIntbankCusName;
	}
	
    /**
     * @return startIntbankCusName
     */
	public String getStartIntbankCusName() {
		return this.startIntbankCusName;
	}
	
	/**
	 * @param startIntbankEvalLevel
	 */
	public void setStartIntbankEvalLevel(String startIntbankEvalLevel) {
		this.startIntbankEvalLevel = startIntbankEvalLevel;
	}
	
    /**
     * @return startIntbankEvalLevel
     */
	public String getStartIntbankEvalLevel() {
		return this.startIntbankEvalLevel;
	}
	
	/**
	 * @param startIntbankEvalTime
	 */
	public void setStartIntbankEvalTime(String startIntbankEvalTime) {
		this.startIntbankEvalTime = startIntbankEvalTime;
	}
	
    /**
     * @return startIntbankEvalTime
     */
	public String getStartIntbankEvalTime() {
		return this.startIntbankEvalTime;
	}
	
	/**
	 * @param startIntbankEvalOrg
	 */
	public void setStartIntbankEvalOrg(String startIntbankEvalOrg) {
		this.startIntbankEvalOrg = startIntbankEvalOrg;
	}
	
    /**
     * @return startIntbankEvalOrg
     */
	public String getStartIntbankEvalOrg() {
		return this.startIntbankEvalOrg;
	}
	
	/**
	 * @param isSponsorBkrtyIsoln
	 */
	public void setIsSponsorBkrtyIsoln(String isSponsorBkrtyIsoln) {
		this.isSponsorBkrtyIsoln = isSponsorBkrtyIsoln;
	}
	
    /**
     * @return isSponsorBkrtyIsoln
     */
	public String getIsSponsorBkrtyIsoln() {
		return this.isSponsorBkrtyIsoln;
	}
	
	/**
	 * @param isManaBkrtyIsoln
	 */
	public void setIsManaBkrtyIsoln(String isManaBkrtyIsoln) {
		this.isManaBkrtyIsoln = isManaBkrtyIsoln;
	}
	
    /**
     * @return isManaBkrtyIsoln
     */
	public String getIsManaBkrtyIsoln() {
		return this.isManaBkrtyIsoln;
	}
	
	/**
	 * @param isExistGuarCreditect
	 */
	public void setIsExistGuarCreditect(String isExistGuarCreditect) {
		this.isExistGuarCreditect = isExistGuarCreditect;
	}
	
    /**
     * @return isExistGuarCreditect
     */
	public String getIsExistGuarCreditect() {
		return this.isExistGuarCreditect;
	}
	
	/**
	 * @param guarDescExt
	 */
	public void setGuarDescExt(String guarDescExt) {
		this.guarDescExt = guarDescExt;
	}
	
    /**
     * @return guarDescExt
     */
	public String getGuarDescExt() {
		return this.guarDescExt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}