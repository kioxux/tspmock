/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAptiInfo
 * @类描述: coop_plan_apti_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-13 21:32:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_plan_apti_info")
public class CoopPlanAptiInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方类型 **/
	@Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 10)
	private String partnerType;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 证书类型 **/
	@Column(name = "LICE_TYPE", unique = false, nullable = true, length = 10)
	private String liceType;
	
	/** 证书名称 **/
	@Column(name = "LICE_NAME", unique = false, nullable = true, length = 120)
	private String liceName;
	
	/** 证书编号 **/
	@Column(name = "LICE_NO", unique = false, nullable = true, length = 100)
	private String liceNo;
	
	/** 资质等级 **/
	@Column(name = "APTI_LVL", unique = false, nullable = true, length = 10)
	private String aptiLvl;
	
	/** 资质证书登记机构 **/
	@Column(name = "APTI_LICE_REGI_ORG", unique = false, nullable = true, length = 100)
	private String aptiLiceRegiOrg;
	
	/** 资质证书登记日期 **/
	@Column(name = "APTI_LICE_REGI_DATE", unique = false, nullable = true, length = 20)
	private String aptiLiceRegiDate;
	
	/** 资质证书到期日期 **/
	@Column(name = "APTI_LICE_END_DATE", unique = false, nullable = true, length = 20)
	private String aptiLiceEndDate;
	
	/** 资质说明 **/
	@Column(name = "APTI_DESC", unique = false, nullable = true, length = 500)
	private String aptiDesc;
	
	/** 是否我行信贷客户 **/
	@Column(name = "IS_BANK_CRET_CUS", unique = false, nullable = true, length = 10)
	private String isBankCretCus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	
    /**
     * @return partnerType
     */
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param liceType
	 */
	public void setLiceType(String liceType) {
		this.liceType = liceType;
	}
	
    /**
     * @return liceType
     */
	public String getLiceType() {
		return this.liceType;
	}
	
	/**
	 * @param liceName
	 */
	public void setLiceName(String liceName) {
		this.liceName = liceName;
	}
	
    /**
     * @return liceName
     */
	public String getLiceName() {
		return this.liceName;
	}
	
	/**
	 * @param liceNo
	 */
	public void setLiceNo(String liceNo) {
		this.liceNo = liceNo;
	}
	
    /**
     * @return liceNo
     */
	public String getLiceNo() {
		return this.liceNo;
	}
	
	/**
	 * @param aptiLvl
	 */
	public void setAptiLvl(String aptiLvl) {
		this.aptiLvl = aptiLvl;
	}
	
    /**
     * @return aptiLvl
     */
	public String getAptiLvl() {
		return this.aptiLvl;
	}
	
	/**
	 * @param aptiLiceRegiOrg
	 */
	public void setAptiLiceRegiOrg(String aptiLiceRegiOrg) {
		this.aptiLiceRegiOrg = aptiLiceRegiOrg;
	}
	
    /**
     * @return aptiLiceRegiOrg
     */
	public String getAptiLiceRegiOrg() {
		return this.aptiLiceRegiOrg;
	}
	
	/**
	 * @param aptiLiceRegiDate
	 */
	public void setAptiLiceRegiDate(String aptiLiceRegiDate) {
		this.aptiLiceRegiDate = aptiLiceRegiDate;
	}
	
    /**
     * @return aptiLiceRegiDate
     */
	public String getAptiLiceRegiDate() {
		return this.aptiLiceRegiDate;
	}
	
	/**
	 * @param aptiLiceEndDate
	 */
	public void setAptiLiceEndDate(String aptiLiceEndDate) {
		this.aptiLiceEndDate = aptiLiceEndDate;
	}
	
    /**
     * @return aptiLiceEndDate
     */
	public String getAptiLiceEndDate() {
		return this.aptiLiceEndDate;
	}
	
	/**
	 * @param aptiDesc
	 */
	public void setAptiDesc(String aptiDesc) {
		this.aptiDesc = aptiDesc;
	}
	
    /**
     * @return aptiDesc
     */
	public String getAptiDesc() {
		return this.aptiDesc;
	}
	
	/**
	 * @param isBankCretCus
	 */
	public void setIsBankCretCus(String isBankCretCus) {
		this.isBankCretCus = isBankCretCus;
	}
	
    /**
     * @return isBankCretCus
     */
	public String getIsBankCretCus() {
		return this.isBankCretCus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}