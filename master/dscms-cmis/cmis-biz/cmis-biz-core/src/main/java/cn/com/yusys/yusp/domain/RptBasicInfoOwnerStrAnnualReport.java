/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoOwnerStrAnnualReport
 * @类描述: rpt_basic_info_owner_str_annual_report数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:57:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_owner_str_annual_report")
public class RptBasicInfoOwnerStrAnnualReport extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 年度 **/
	@Column(name = "YEAR", unique = false, nullable = true, length = 20)
	private String year;
	
	/** 数据来源 **/
	@Column(name = "DATA_SOURCE", unique = false, nullable = true, length = 5)
	private String dataSource;
	
	/** 年末总资产 **/
	@Column(name = "YEAR_END_ASSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearEndAssetAmt;
	
	/** 年末净资产 **/
	@Column(name = "YEAR_END_PURE_ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearEndPureAssetValue;
	
	/** 年末资产负债率 **/
	@Column(name = "YEAR_END_DEBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearEndDebt;
	
	/** 年度经营主营业务收入 **/
	@Column(name = "YEAR_MAIN_BUSI_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearMainBusiIncome;
	
	/** 年度经营利润总额 **/
	@Column(name = "YEAR_BUSI_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearBusiTotalProfit;
	
	/** 年度经营年末融资余额 **/
	@Column(name = "YEAR_BUSI_LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearBusiLoanBalance;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param year
	 */
	public void setYear(String year) {
		this.year = year;
	}
	
    /**
     * @return year
     */
	public String getYear() {
		return this.year;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	
    /**
     * @return dataSource
     */
	public String getDataSource() {
		return this.dataSource;
	}
	
	/**
	 * @param yearEndAssetAmt
	 */
	public void setYearEndAssetAmt(java.math.BigDecimal yearEndAssetAmt) {
		this.yearEndAssetAmt = yearEndAssetAmt;
	}
	
    /**
     * @return yearEndAssetAmt
     */
	public java.math.BigDecimal getYearEndAssetAmt() {
		return this.yearEndAssetAmt;
	}
	
	/**
	 * @param yearEndPureAssetValue
	 */
	public void setYearEndPureAssetValue(java.math.BigDecimal yearEndPureAssetValue) {
		this.yearEndPureAssetValue = yearEndPureAssetValue;
	}
	
    /**
     * @return yearEndPureAssetValue
     */
	public java.math.BigDecimal getYearEndPureAssetValue() {
		return this.yearEndPureAssetValue;
	}
	
	/**
	 * @param yearEndDebt
	 */
	public void setYearEndDebt(java.math.BigDecimal yearEndDebt) {
		this.yearEndDebt = yearEndDebt;
	}
	
    /**
     * @return yearEndDebt
     */
	public java.math.BigDecimal getYearEndDebt() {
		return this.yearEndDebt;
	}
	
	/**
	 * @param yearMainBusiIncome
	 */
	public void setYearMainBusiIncome(java.math.BigDecimal yearMainBusiIncome) {
		this.yearMainBusiIncome = yearMainBusiIncome;
	}
	
    /**
     * @return yearMainBusiIncome
     */
	public java.math.BigDecimal getYearMainBusiIncome() {
		return this.yearMainBusiIncome;
	}
	
	/**
	 * @param yearBusiTotalProfit
	 */
	public void setYearBusiTotalProfit(java.math.BigDecimal yearBusiTotalProfit) {
		this.yearBusiTotalProfit = yearBusiTotalProfit;
	}
	
    /**
     * @return yearBusiTotalProfit
     */
	public java.math.BigDecimal getYearBusiTotalProfit() {
		return this.yearBusiTotalProfit;
	}
	
	/**
	 * @param yearBusiLoanBalance
	 */
	public void setYearBusiLoanBalance(java.math.BigDecimal yearBusiLoanBalance) {
		this.yearBusiLoanBalance = yearBusiLoanBalance;
	}
	
    /**
     * @return yearBusiLoanBalance
     */
	public java.math.BigDecimal getYearBusiLoanBalance() {
		return this.yearBusiLoanBalance;
	}


}