/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AsplAorgList
 * @类描述: aspl_aorg_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "资产池承兑行名单配置导出模板", fileType = ExcelCsv.ExportFileType.XLS)
public class AsplAorgListOutPort {
    private static final long serialVersionUID = 1L;

	/** 总行行号 **/
	@ExcelField(title = "总行行号", viewLength = 40)
	private String headBankNo;
	
	/** ECIF编号 **/
	@ExcelField(title = "ECIF编号", viewLength = 40)
	private String ecifNo;
	
	/** 同业名称 **/
	@ExcelField(title = "同业名称", viewLength = 40)
	private String intbankName;
	
	/** 信用等级 **/
	@ExcelField(title = "信用等级", viewLength = 20)
	private String creditLevel;
	
	/** 抵质押率 **/
	@ExcelField(title = "抵质押率", viewLength = 20)
	private java.math.BigDecimal pldimnRate;
	
	/** 状态 **/
	@ExcelField(title = "状态", viewLength = 20)
	private String status;

	/** 主管客户经理 **/
	@ExcelField(title = "主管客户经理编号", viewLength = 20)
	private String managerId;

	/** 主管机构 **/
	@ExcelField(title = "主管机构编号", viewLength = 20)
	private String managerBrId;

	/** 登记人 **/
	@ExcelField(title = "登记人编号", viewLength = 20)
	private String inputId;

	/** 登记机构 **/
	@ExcelField(title = "登记机构编号", viewLength = 20)
	private String inputBrId;
	
	/**
	 * @param headBankNo
	 */
	public void setHeadBankNo(String headBankNo) {
		this.headBankNo = headBankNo;
	}
	
    /**
     * @return headBankNo
     */
	public String getHeadBankNo() {
		return this.headBankNo;
	}
	
	/**
	 * @param ecifNo
	 */
	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}
	
    /**
     * @return ecifNo
     */
	public String getEcifNo() {
		return this.ecifNo;
	}
	
	/**
	 * @param intbankName
	 */
	public void setIntbankName(String intbankName) {
		this.intbankName = intbankName;
	}
	
    /**
     * @return intbankName
     */
	public String getIntbankName() {
		return this.intbankName;
	}
	
	/**
	 * @param creditLevel
	 */
	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}
	
    /**
     * @return creditLevel
     */
	public String getCreditLevel() {
		return this.creditLevel;
	}
	
	/**
	 * @param pldimnRate
	 */
	public void setPldimnRate(java.math.BigDecimal pldimnRate) {
		this.pldimnRate = pldimnRate;
	}
	
    /**
     * @return pldimnRate
     */
	public java.math.BigDecimal getPldimnRate() {
		return this.pldimnRate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

}