/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanApp;
import cn.com.yusys.yusp.service.OtherRecordAccpSignPlanAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignPlanAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-09 20:28:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherrecordaccpsignplanapp")
public class OtherRecordAccpSignPlanAppResource {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(OtherRecordAccpSignPlanAppResource.class);
    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Autowired
    private OtherRecordAccpSignPlanAppService otherRecordAccpSignPlanAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherRecordAccpSignPlanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherRecordAccpSignPlanApp> list = otherRecordAccpSignPlanAppService.selectAll(queryModel);
        return new ResultDto<List<OtherRecordAccpSignPlanApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<OtherRecordAccpSignPlanApp>> index(@RequestBody QueryModel queryModel) {
        List<OtherRecordAccpSignPlanApp> list = otherRecordAccpSignPlanAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordAccpSignPlanApp>>(list);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<OtherRecordAccpSignPlanApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherRecordAccpSignPlanApp> list = otherRecordAccpSignPlanAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordAccpSignPlanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherRecordAccpSignPlanApp> show(@PathVariable("serno") String serno) {
        OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp = otherRecordAccpSignPlanAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherRecordAccpSignPlanApp>(otherRecordAccpSignPlanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<OtherRecordAccpSignPlanApp> create(@RequestBody OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp) throws URISyntaxException {
        otherRecordAccpSignPlanAppService.insert(otherRecordAccpSignPlanApp);
        return new ResultDto<OtherRecordAccpSignPlanApp>(otherRecordAccpSignPlanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp) throws URISyntaxException {
        //否决、打回
        if(CmisBizConstants.OPR_TYPE_02.equals(otherRecordAccpSignPlanApp.getOprType())
                &&CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherRecordAccpSignPlanApp.getApproveStatus())){
            otherRecordAccpSignPlanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherRecordAccpSignPlanApp.getSerno());
            workflowCoreClient.deleteByBizId(otherRecordAccpSignPlanApp.getSerno());
        }
        otherRecordAccpSignPlanApp.setOprType(CmisBizConstants.OPR_TYPE_01);
        int result = otherRecordAccpSignPlanAppService.update(otherRecordAccpSignPlanApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateOprTypeUnderLmt
     * @函数描述:授信场景下，逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) throws URISyntaxException {
        int result = 0;
        OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp =  otherRecordAccpSignPlanAppService.selectByPrimaryKey(serno);
        //否决、打回s
        if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherRecordAccpSignPlanApp.getApproveStatus())){
            otherRecordAccpSignPlanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherRecordAccpSignPlanApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherRecordAccpSignPlanApp.getSerno());
            workflowCoreClient.deleteByBizId(otherRecordAccpSignPlanApp.getSerno());
        }else{
            otherRecordAccpSignPlanApp.setOprType(CmisBizConstants.OPR_TYPE_02);
        }
        result = otherRecordAccpSignPlanAppService.update(otherRecordAccpSignPlanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherRecordAccpSignPlanAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherRecordAccpSignPlanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    
    /**
     * @方法名称: getotherrecordaccpsignplanapp
     * @方法描述: 根据入参获取当前客户经理名下银票签发业务每周计划申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherrecordaccpsignplanapp")
    protected ResultDto<List<OtherRecordAccpSignPlanApp>> getOtherAccpPerferFeeApp(@RequestBody QueryModel model) {
        List<OtherRecordAccpSignPlanApp> list = otherRecordAccpSignPlanAppService.selectByModel(model);
        return new ResultDto<List<OtherRecordAccpSignPlanApp>>(list);
    }

    /**
     * @方法名称: upIsUpperApprAuth
     * @方法描述:  上调权限处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/upIsUpperApprAuth")
    protected ResultDto<Integer> upIsUpperApprAuth(@RequestBody Map<String,String> params) {
        int result = 0;
        // 获取流水号
        String serno =  params.get("serno");
        // 是否上调权限
        String  isUpperApprAuth = params.get("isUpperApprAuth");
        if(!"".equals(serno) && null != serno){
            OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp =  otherRecordAccpSignPlanAppService.selectByPrimaryKey(serno);
            if(null != otherRecordAccpSignPlanApp){
                otherRecordAccpSignPlanApp.setIsUpperApprAuth(isUpperApprAuth);
                result = otherRecordAccpSignPlanAppService.update(otherRecordAccpSignPlanApp);
            }
        }
        return new ResultDto<Integer>(result);
    }

}
