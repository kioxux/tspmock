package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtBuildingPro
 * @类描述: lmt_building_pro数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtBuildingProDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 项目总投资 **/
	private java.math.BigDecimal proInvest;
	
	/** 开发商名称 **/
	private String deveName;
	
	/** 项目开工时间 **/
	private String proStartTime;
	
	/** 是否竣工 STD_ZB_YES_NO **/
	private String isCompletion;
	
	/** 竣工时间 **/
	private String completionTime;
	
	/** 是否封顶 STD_ZB_YES_NO **/
	private String isCapping;
	
	/** 封顶时间 **/
	private String cappingTime;
	
	/** 地理位置 **/
	private String geograPlace;
	
	/** 街道 **/
	private String street;
	
	/** 地理面积（平米） **/
	private java.math.BigDecimal geograSqu;
	
	/** 住宅建筑面积（平米） **/
	private java.math.BigDecimal resiArchSqu;
	
	/** 总建筑面积（平米） **/
	private java.math.BigDecimal totlArchSqu;
	
	/** 住宅销售单价（元/平米） **/
	private java.math.BigDecimal resiSalePrice;
	
	/** 商业建筑面积（平米） **/
	private java.math.BigDecimal commArchSqu;
	
	/** 商业销售单价（元/平米） **/
	private java.math.BigDecimal commSalePrice;
	
	/** 是否五证齐全 STD_ZB_YES_NO **/
	private String isCompFiveLice;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proInvest
	 */
	public void setProInvest(java.math.BigDecimal proInvest) {
		this.proInvest = proInvest;
	}
	
    /**
     * @return ProInvest
     */	
	public java.math.BigDecimal getProInvest() {
		return this.proInvest;
	}
	
	/**
	 * @param deveName
	 */
	public void setDeveName(String deveName) {
		this.deveName = deveName == null ? null : deveName.trim();
	}
	
    /**
     * @return DeveName
     */	
	public String getDeveName() {
		return this.deveName;
	}
	
	/**
	 * @param proStartTime
	 */
	public void setProStartTime(String proStartTime) {
		this.proStartTime = proStartTime == null ? null : proStartTime.trim();
	}
	
    /**
     * @return ProStartTime
     */	
	public String getProStartTime() {
		return this.proStartTime;
	}
	
	/**
	 * @param isCompletion
	 */
	public void setIsCompletion(String isCompletion) {
		this.isCompletion = isCompletion == null ? null : isCompletion.trim();
	}
	
    /**
     * @return IsCompletion
     */	
	public String getIsCompletion() {
		return this.isCompletion;
	}
	
	/**
	 * @param completionTime
	 */
	public void setCompletionTime(String completionTime) {
		this.completionTime = completionTime == null ? null : completionTime.trim();
	}
	
    /**
     * @return CompletionTime
     */	
	public String getCompletionTime() {
		return this.completionTime;
	}
	
	/**
	 * @param isCapping
	 */
	public void setIsCapping(String isCapping) {
		this.isCapping = isCapping == null ? null : isCapping.trim();
	}
	
    /**
     * @return IsCapping
     */	
	public String getIsCapping() {
		return this.isCapping;
	}
	
	/**
	 * @param cappingTime
	 */
	public void setCappingTime(String cappingTime) {
		this.cappingTime = cappingTime == null ? null : cappingTime.trim();
	}
	
    /**
     * @return CappingTime
     */	
	public String getCappingTime() {
		return this.cappingTime;
	}
	
	/**
	 * @param geograPlace
	 */
	public void setGeograPlace(String geograPlace) {
		this.geograPlace = geograPlace == null ? null : geograPlace.trim();
	}
	
    /**
     * @return GeograPlace
     */	
	public String getGeograPlace() {
		return this.geograPlace;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param geograSqu
	 */
	public void setGeograSqu(java.math.BigDecimal geograSqu) {
		this.geograSqu = geograSqu;
	}
	
    /**
     * @return GeograSqu
     */	
	public java.math.BigDecimal getGeograSqu() {
		return this.geograSqu;
	}
	
	/**
	 * @param resiArchSqu
	 */
	public void setResiArchSqu(java.math.BigDecimal resiArchSqu) {
		this.resiArchSqu = resiArchSqu;
	}
	
    /**
     * @return ResiArchSqu
     */	
	public java.math.BigDecimal getResiArchSqu() {
		return this.resiArchSqu;
	}
	
	/**
	 * @param totlArchSqu
	 */
	public void setTotlArchSqu(java.math.BigDecimal totlArchSqu) {
		this.totlArchSqu = totlArchSqu;
	}
	
    /**
     * @return TotlArchSqu
     */	
	public java.math.BigDecimal getTotlArchSqu() {
		return this.totlArchSqu;
	}
	
	/**
	 * @param resiSalePrice
	 */
	public void setResiSalePrice(java.math.BigDecimal resiSalePrice) {
		this.resiSalePrice = resiSalePrice;
	}
	
    /**
     * @return ResiSalePrice
     */	
	public java.math.BigDecimal getResiSalePrice() {
		return this.resiSalePrice;
	}
	
	/**
	 * @param commArchSqu
	 */
	public void setCommArchSqu(java.math.BigDecimal commArchSqu) {
		this.commArchSqu = commArchSqu;
	}
	
    /**
     * @return CommArchSqu
     */	
	public java.math.BigDecimal getCommArchSqu() {
		return this.commArchSqu;
	}
	
	/**
	 * @param commSalePrice
	 */
	public void setCommSalePrice(java.math.BigDecimal commSalePrice) {
		this.commSalePrice = commSalePrice;
	}
	
    /**
     * @return CommSalePrice
     */	
	public java.math.BigDecimal getCommSalePrice() {
		return this.commSalePrice;
	}
	
	/**
	 * @param isCompFiveLice
	 */
	public void setIsCompFiveLice(String isCompFiveLice) {
		this.isCompFiveLice = isCompFiveLice == null ? null : isCompFiveLice.trim();
	}
	
    /**
     * @return IsCompFiveLice
     */	
	public String getIsCompFiveLice() {
		return this.isCompFiveLice;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}