package cn.com.yusys.yusp.service.client.bsp.ypxt.xdypgyrcx;


import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：押品共有人信息查询
 *
 * @author xll
 * @version 1.0
 * @since 2021年5月10日 下午1:22:06
 */
@Service
@Transactional
public class XdypgyrcxService {
    private static final Logger logger = LoggerFactory.getLogger(XdypgyrcxService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * 业务逻辑处理方法：押品共有人信息查询
     *
     * @param xdypgyrcxReqDto
     * @return
     */
    @Transactional
    public XdypgyrcxRespDto xdypgyrcx(XdypgyrcxReqDto xdypgyrcxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value);
        validateAndSetValue(xdypgyrcxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxReqDto));
        ResultDto<XdypgyrcxRespDto> xdypgyrcx2ResultDto = dscms2YpxtClientService.xdypgyrcx(xdypgyrcxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcx2ResultDto));

        String xdypgyrcxCode = Optional.ofNullable(xdypgyrcx2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdypgyrcxMeesage = Optional.ofNullable(xdypgyrcx2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        XdypgyrcxRespDto xdypgyrcxRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdypgyrcx2ResultDto.getCode())) {
            //  获取相关的值并解析
            xdypgyrcxRespDto = xdypgyrcx2ResultDto.getData();
        }else{//未查询到相关信息
            xdypgyrcx2ResultDto.setCode(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value);
        return xdypgyrcxRespDto;
    }


    /**
     * 校验xdypgyrcxReqDto对象并赋值
     *
     * @param xdypgyrcxReqDto
     */
    private void validateAndSetValue(XdypgyrcxReqDto xdypgyrcxReqDto) {
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxReqDto));
        xdypgyrcxReqDto.setGuaranty_id(Optional.ofNullable(xdypgyrcxReqDto.getGuaranty_id()).orElse(StringUtils.EMPTY));//押品编号
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxReqDto));
    }


}
