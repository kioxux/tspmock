package cn.com.yusys.yusp.web.server.xddb0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0022.req.Xddb0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0022.resp.Xddb0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0022.Xddb0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDDB0022:根据押品编号查询核心及信贷系统有无押品数据")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0022Resource.class);

    @Autowired
    private Xddb0022Service xddb0022sService;

    /**
     * 交易码：xddb0022
     * 交易描述：根据押品编号查询核心及信贷系统有无押品数据
     *
     * @param xddb0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据押品编号查询核心及信贷系统有无押品数据")
    @PostMapping("/xddb0022")
    protected @ResponseBody
    ResultDto<Xddb0022DataRespDto> xddb0022(@Validated @RequestBody Xddb0022DataReqDto xddb0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataReqDto));
        Xddb0022DataRespDto xddb0022DataRespDto = new Xddb0022DataRespDto();// 响应Dto
        ResultDto<Xddb0022DataRespDto> xddb0022DataResultDto = new ResultDto<>();
        try {
            // 从xddb0022DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataReqDto));
            xddb0022DataRespDto = xddb0022sService.xddb0022(xddb0022DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataRespDto));
            // 封装xddb0022DataResultDto中正确的返回码和返回信息
            xddb0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, e.getMessage());
            // 封装xddb0022DataResultDto中异常返回码和返回信息
            xddb0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0022DataRespDto到xddb0022DataResultDto中
        xddb0022DataResultDto.setData(xddb0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataResultDto));
        return xddb0022DataResultDto;
    }
}
