/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccAccpDto;
import cn.com.yusys.yusp.dto.BailAccInfoDto;
import cn.com.yusys.yusp.service.LmtReplyAccSubPrdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.BailAccInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailAccInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-16 14:59:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "保证金账号信息")
@RequestMapping("/api/bailaccinfo")
public class BailAccInfoResource {
    @Autowired
    private BailAccInfoService bailAccInfoService;
    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BailAccInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<BailAccInfo> list = bailAccInfoService.selectAll(queryModel);
        return new ResultDto<List<BailAccInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/showList")
    protected ResultDto<List<BailAccInfo>> index(@RequestBody QueryModel queryModel) {
        List<BailAccInfo> list = bailAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<BailAccInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BailAccInfo> show(@PathVariable("pkId") String pkId) {
        BailAccInfo bailAccInfo = bailAccInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<BailAccInfo>(bailAccInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BailAccInfo> create(@RequestBody BailAccInfo bailAccInfo) throws URISyntaxException {
        bailAccInfoService.insert(bailAccInfo);
        return new ResultDto<BailAccInfo>(bailAccInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BailAccInfo bailAccInfo) throws URISyntaxException {
        int result = bailAccInfoService.updateSelective(bailAccInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = bailAccInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = bailAccInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 保证金信息保存方法
     * @param bailAccInfo
     * @return
     */
    @ApiOperation("保证金信息保存方法")
    @PostMapping("/savebailaccinfo")
    public ResultDto<Map> saveBailAccInfo(@RequestBody BailAccInfo bailAccInfo){
        Map result = bailAccInfoService.saveBailAccInfo(bailAccInfo);
        return new ResultDto<>(result);
    }


    /**
     * 保证金信息逻辑删除方法
     * @param bailAccInfo
     * @return
     */
    @ApiOperation("保证金信息逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody BailAccInfo bailAccInfo){
        Integer result = bailAccInfoService.logicDelete(bailAccInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getList
     * @函数描述:按照流水号查询保证金信息方法
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("按照流水号查询保证金信息方法")
    @PostMapping("/getList")
    protected ResultDto<Object> getList(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        if(null==params.get("serno")||"".equals(params.get("serno"))){
            resultDto.setMessage("流水号为空！");
        }else{
            List<BailAccInfo> list = bailAccInfoService.selectBySerno((String) params.get("serno"));
            if (list.size()>0) {
                resultDto.setCode(0);
                resultDto.setData(list);
                resultDto.setMessage("查询成功！");
            } else {
                resultDto.setCode(200);
                resultDto.setData(list);
                resultDto.setMessage("无对应的数据！");
            }
        }

        return resultDto;
    }

    /**
     * @函数名称:sendCoreForBail
     * @函数描述:发送核心查询保证金信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("发送核心查询保证金信息")
    @PostMapping("/sendcoreforbail")
    protected  ResultDto<List<BailAccInfoDto>> sendCoreForBail(@RequestBody QueryModel queryModel) {
        return bailAccInfoService.sendCoreForBail(queryModel);
    }

    /**
     * 资产池协议新增保存操作
     * @param bailAccInfo
     * @函数描述:保证金账户信息新增保存
     * @return
     */
    @ApiOperation("保证金账户信息新增保存")
    @PostMapping("/savebailinfo")
    public ResultDto<Map> saveBailInfo(@RequestBody BailAccInfo bailAccInfo){
        Map result = bailAccInfoService.saveBailInfo(bailAccInfo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebailaccinfobyserno")
    protected ResultDto<Integer> updateBailAccInfoBySerno(@RequestBody BailAccInfo bailAccInfo) throws URISyntaxException {
        int result = bailAccInfoService.updateBailAccInfoBySerno(bailAccInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:修改资产池保证金比例
     * @参数与返回说明:
     * @算法描述:保证金比例只能比授信批复保证金比例高
     */
    @PostMapping("/updateasplbailaccbyserno")
    protected ResultDto<Integer> updateAsplBailAccBySerno(@RequestBody BailAccInfo bailAccInfo) throws URISyntaxException {
        // 获取授信批复信息
        String cusId = bailAccInfo.getCusId();
        if(StringUtils.nonEmpty(cusId)){
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdDataByParams(cusId);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                return new ResultDto<Integer>().message("未查询到受信批复信息！").code("9999");
            }
            BigDecimal bailRate = bailAccInfo.getBailRate();
            BigDecimal lmtbailRate = lmtReplyAccSubPrd.getBailPreRate().setScale(4);
            if(bailRate.compareTo(lmtbailRate) >= 0 ){// 修改的保证金比例大于等于授信批复的保证金比例
                bailAccInfoService.updateBailAccInfoBySerno(bailAccInfo);
                return new ResultDto<Integer>().message("修改成功！").code("0");
            }else{
                return new ResultDto<Integer>().message("修改的保证金比例不允许大于授信批复中的保证金比例！").code("9999");
            }
        }else{
            return new ResultDto<Integer>().message("未获取到【cusId】值，请联系管理员！").code("9999");
        }
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:根据根据核心银承编号回显银承台账信息
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @ApiOperation("根据流水号回显银承台账保证金信息")
    @PostMapping("/queryBySerno")
    protected ResultDto<List<BailAccInfoDto>> queryBySerno(@RequestBody QueryModel queryModel) {
        List<BailAccInfoDto> bailAccInfoDto = bailAccInfoService.queryBySerno(queryModel);
        return new ResultDto<List<BailAccInfoDto>>(bailAccInfoDto);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:根据根据核心银承编号回显银承台账信息
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @ApiOperation("根据流水号回显保证金信息")
    @PostMapping("/selectInfoBySerno")
    protected ResultDto<Map> selectInfoBySerno(@RequestBody QueryModel queryModel) {
        String serno = (String)queryModel.getCondition().get("serno");
        Map<String,String> map = bailAccInfoService.selectAsplBailBySerno(serno);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:核心查询保证金账号余额
     * @参数与返回说明:
     * @算法描述:
     * xs
     */
    @ApiOperation("核心查询保证金账号余额")
    @PostMapping("/sendCoreQueryBail")
    protected ResultDto<Map> sendCoreQueryBail(@RequestBody Map<String,Object> map) {
        Map<String,Object> resultMap = new HashMap<>();
        ResultDto<Map> resultDto = bailAccInfoService.sendCoreQueryBail(map);
        return resultDto;
    }
    /**
     * @函数名称:computeAvalBail
     * @函数描述:计算资产池可提取保证金金额
     * @参数与返回说明:
     * @算法描述:
     * xs
     */
    @ApiOperation("计算可提取保证金金额")
    @PostMapping("/computeAvalBail")
    protected ResultDto<Map> computeAvalBail(@RequestBody Map<String,String> map) {
        Map<String,Object> resultMap = new HashMap<>();
        ResultDto<Map> resultDto = bailAccInfoService.computeAvalBail(map);
        return resultDto;
    }

    /**
     * @函数名称:computeAvalBail
     * @函数描述:计算资产池可提取保证金金额
     * @参数与返回说明:
     * @算法描述:
     * xs
     */
    @ApiOperation("计算可提取保证金金额")
    @PostMapping("/getBailInfo")
    protected ResultDto<List<BailAccInfoDto>> getBailInfo(@RequestBody QueryModel queryModel) {
        return bailAccInfoService.getBailInfo(queryModel);
    }
}
