package cn.com.yusys.yusp.web.server.xdxw0046;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0046.req.Xdxw0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0046.resp.Xdxw0046DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdxw0046.Xdxw0046Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优享贷批复结果反馈
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0046:优享贷批复结果反馈")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0046Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0046Resource.class);

    @Autowired
    private Xdxw0046Service xdxw0046Service;

    /**
     * 交易码：xdxw0046
     * 交易描述：优享贷批复结果反馈
     *
     * @param xdxw0046DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优享贷批复结果反馈")
    @PostMapping("/xdxw0046")
    protected @ResponseBody
    ResultDto<Xdxw0046DataRespDto> xdxw0046(@Validated @RequestBody Xdxw0046DataReqDto xdxw0046DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataReqDto));
        Xdxw0046DataRespDto xdxw0046DataRespDto = new Xdxw0046DataRespDto();// 响应Dto:优享贷批复结果反馈
        ResultDto<Xdxw0046DataRespDto> xdxw0046DataResultDto = new ResultDto<>();
        // 从xdxw0046DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0046Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataReqDto));
            xdxw0046DataRespDto = xdxw0046Service.insertSurveyResult(xdxw0046DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataRespDto));
            // 封装xdxw0046DataResultDto中正确的返回码和返回信息
            xdxw0046DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0046DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, e.getMessage());
            // 封装xdxw0046DataResultDto中异常返回码和返回信息
            xdxw0046DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0046DataResultDto.setMessage(EpbEnum.EPB099999.value + ":" + e.getMessage());
        }
        // 封装xdxw0046DataRespDto到xdxw0046DataResultDto中
        xdxw0046DataResultDto.setData(xdxw0046DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataRespDto));
        return xdxw0046DataResultDto;
    }
}
