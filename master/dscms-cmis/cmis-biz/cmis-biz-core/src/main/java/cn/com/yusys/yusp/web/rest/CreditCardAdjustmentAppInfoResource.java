/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.CardBusinessDto;
import cn.com.yusys.yusp.dto.CardDetailDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.req.D11010ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.resp.D11010RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.req.D12000ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.resp.D12000RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import cn.com.yusys.yusp.service.Dscms2TonglianClientService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.vo.CreditCardAdjustmentListVo;
import cn.com.yusys.yusp.vo.CreditCardBusinessDetailVo;
import cn.com.yusys.yusp.vo.CreditCardBusinessListVo;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentAppInfo;
import cn.com.yusys.yusp.service.CreditCardAdjustmentAppInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAdjustmentAppInfoResource
 * @类描述: #额度申请模块
 * @功能描述: 
 * @创建人: ZSM
 * @创建时间: 2021-05-24 15:54:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditcardadjustmentappinfo")
public class CreditCardAdjustmentAppInfoResource {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private CreditCardAdjustmentAppInfoService creditCardAdjustmentAppInfoService;
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardAdjustmentAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardAdjustmentAppInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditCardAdjustmentAppInfo>> index(QueryModel queryModel) {
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAdjustmentAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CreditCardAdjustmentAppInfo> show(@PathVariable("serno") String serno) {
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CreditCardAdjustmentAppInfo>(creditCardAdjustmentAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增额度申请")
    @PostMapping("/")
    protected ResultDto<CreditCardAdjustmentAppInfo> create(@RequestBody CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo) throws URISyntaxException {
        creditCardAdjustmentAppInfoService.insert(creditCardAdjustmentAppInfo);
        return new ResultDto<CreditCardAdjustmentAppInfo>(creditCardAdjustmentAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的新增额度申请")
    @PostMapping("/save")
    protected ResultDto<CreditCardAdjustmentAppInfo> save(@RequestBody CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo) throws URISyntaxException {
        creditCardAdjustmentAppInfoService.save(creditCardAdjustmentAppInfo);
        return new ResultDto<CreditCardAdjustmentAppInfo>(creditCardAdjustmentAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("更新额度申请")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo) throws URISyntaxException {
        int result = creditCardAdjustmentAppInfoService.update(creditCardAdjustmentAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键删除额度申请")
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(serno);
        int result = creditCardAdjustmentAppInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);

    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的通过主键删除额度申请")
    @PostMapping("/cut")
    protected ResultDto<Integer> cut(@RequestBody CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfos) {
        String serno = creditCardAdjustmentAppInfos.getSerno();
        int result = creditCardAdjustmentAppInfoService.cutByPrimaryKey(serno);
        return new ResultDto<Integer>(result);

    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量删除额度申请")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardAdjustmentAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询额度申请均为待发起的")
    @PostMapping("/querymodel")
    protected ResultDto<List<CreditCardAdjustmentAppInfo>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoService.selectByModelStatus(queryModel);
        return new ResultDto<List<CreditCardAdjustmentAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询额度申请")
    @PostMapping("/query")
    protected ResultDto<List<CreditCardAdjustmentAppInfo>> queryPost(@RequestBody QueryModel queryModel) {
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAdjustmentAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询额度申请不等于某状态")
    @PostMapping("/querybystatus")
    protected ResultDto<List<CreditCardAdjustmentAppInfo>> queryByStatus(@RequestBody QueryModel queryModel) {
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoService.selectByNotStatus(queryModel);
        return new ResultDto<List<CreditCardAdjustmentAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键查询额度申请")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardAdjustmentAppInfo> showPost(@RequestBody CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfos) {
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(creditCardAdjustmentAppInfos.getSerno());
        return new ResultDto<CreditCardAdjustmentAppInfo>(creditCardAdjustmentAppInfo);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            获取历史交易信息
     * @算法描述:
     */
    @ApiOperation("查询额度申请历史交易信息")
    @PostMapping("/findhistorybusinesslist")
    protected ResultDto<List<CreditCardBusinessListVo>> findHistoryBusinessList(@RequestBody  QueryModel queryModel) {
        Map condition = queryModel.getCondition();
        // TODO(“需要调用接口组接口") ；
        D12050ReqDto d12050ReqDto = new D12050ReqDto();
        String cardNo = "";
        String stmtStartDate = "";
        String stmtEndDate ="";
        if(condition.containsKey("cardNo")){
            cardNo = (String) condition.get("cardNo");
        }
        if(condition.containsKey("stmtStartDate") ){
            stmtStartDate = (String) condition.get("stmtStartDate");
            if(stmtStartDate !=null && !"".equals(stmtStartDate)){
                stmtStartDate= stmtStartDate.substring(0,4)+stmtStartDate.substring(5,7);
            }
        }
        if(condition.containsKey("stmtEndDate")){
            stmtEndDate = (String) condition.get("stmtEndDate");
            if(stmtEndDate !=null && !"".equals(stmtEndDate)){
                stmtEndDate = stmtEndDate.substring(0,4)+stmtEndDate.substring(5,7);
            }
        }
        d12050ReqDto.setCardno(cardNo);
        d12050ReqDto.setCrcycd("156");
        String page = String.valueOf(queryModel.getPage());
        String size = String.valueOf(queryModel.getSize());
        BigDecimal start = new BigDecimal(0);
        BigDecimal end =  new BigDecimal(9);
        if("1".equals(page)){
            start =  new BigDecimal(0);
            end =  new BigDecimal(9);
        }else{
            start =(new BigDecimal(page).subtract(new BigDecimal(1))).multiply(new BigDecimal(size));
            end = new BigDecimal(page).multiply(new BigDecimal(size)).subtract(new BigDecimal(1));
        }
        d12050ReqDto.setFrtrow(start.toPlainString());
        d12050ReqDto.setLstrow(end.toPlainString());
        d12050ReqDto.setStsart(stmtStartDate);
        d12050ReqDto.setEnddat(stmtEndDate);
        log.info("发送报文,{}"+d12050ReqDto);
        ResultDto<D12050RespDto> d12050ResultDto = dscms2TonglianClientService.d12050(d12050ReqDto);
        log.info("返回报文,{}"+d12050ResultDto);
        List<CreditCardBusinessListVo>sernoList = new ArrayList<>();
        java.util.List<cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.List> list = d12050ResultDto.getData().getList();
        if(list != null && list.size() > 0){
            for(int i=0 ; i<list.size() ; i++){
                CreditCardBusinessListVo creditCardBusinessListVos = new CreditCardBusinessListVo();
                creditCardBusinessListVos.setCusName(list.get(i).getCustnm());
                creditCardBusinessListVos.setCardNo(cardNo);
                creditCardBusinessListVos.setBillDate(list.get(i).getBilldt());
                creditCardBusinessListVos.setUpBillBal(list.get(i).getStbgbl());
                creditCardBusinessListVos.setCurDebitBal(list.get(i).getCdamdb());
                creditCardBusinessListVos.setCurCrditBal(list.get(i).getCdamcr());
                creditCardBusinessListVos.setAllNeedPay(list.get(i).getQlgcbl());
                creditCardBusinessListVos.setCurAccountBal(list.get(i).getStcrbl());
                // TODO(“cardtype账户类型？") ；
                sernoList.add(creditCardBusinessListVos);
            }
        }
        ResultDto result = new ResultDto<List<CreditCardBusinessListVo>>(sernoList);
        result.setTotal(d12050ResultDto.getData().getTotrow());
        return result;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询额度申请历史账单详情")
    @PostMapping("/findhistorydetail")
    protected ResultDto<List<CreditCardBusinessDetailVo>> findHistoryDetail(@RequestBody  QueryModel queryModel) {
        Map condition = queryModel.getCondition();
        String cardNo = "";
        String stmtStartDate = "";
        String stmtEndDate ="";
        if(condition.containsKey("cardNo")){
            cardNo = (String) condition.get("cardNo");
        }
        if(condition.containsKey("stmtStartDate")){
            stmtStartDate = (String) condition.get("stmtStartDate");
        }
        if(condition.containsKey("stmtEndDate")) {
            stmtEndDate = (String) condition.get("stmtEndDate");
        }
        String year = stmtStartDate.substring(0,4);
        String mon = stmtStartDate.substring(4,6);
        String day = stmtStartDate.substring(6,8);
        stmtStartDate =  year+""+mon;
        String page = String.valueOf(queryModel.getPage());
        String size = String.valueOf(queryModel.getSize());
        BigDecimal start = new BigDecimal(0);
        BigDecimal end =  new BigDecimal(9);
        if("1".equals(page)){
            start =  new BigDecimal(0);
            end =  new BigDecimal(9);
        }else{
            start =(new BigDecimal(page).subtract(new BigDecimal(1))).multiply(new BigDecimal(size));
            end = new BigDecimal(page).multiply(new BigDecimal(size)).subtract(new BigDecimal(1));
        }
        D12011ReqDto d12011ReqDto = new D12011ReqDto();
        d12011ReqDto.setCardno(cardNo);
        d12011ReqDto.setCrcycd("156");
        d12011ReqDto.setFrtrow(start.toPlainString());
        d12011ReqDto.setLstrow(end.toPlainString());
        d12011ReqDto.setStmtdt(stmtStartDate);
        log.info("发送报文："+d12011ReqDto);
        ResultDto<D12011RespDto> d12011ResultDto = dscms2TonglianClientService.d12011(d12011ReqDto);
        log.info("返回报文："+d12011ResultDto);
        java.util.List<cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.List> list = d12011ResultDto.getData().getList();
        List<CreditCardBusinessDetailVo>sernoList = new ArrayList<>();
        if(list != null && list.size() > 0){
            for(int i=0 ; i<list.size() ; i++){
                CreditCardBusinessDetailVo creditCardBusinessDetailVo = new CreditCardBusinessDetailVo();
                creditCardBusinessDetailVo.setCardNo(cardNo); //卡号
                creditCardBusinessDetailVo.setTndate(list.get(i).getTndate()); //交易日期
                creditCardBusinessDetailVo.setAqnead(list.get(i).getAqnead()); //受理机构名称地址
                creditCardBusinessDetailVo.setTncode(list.get(i).getTncode()); //交易码
                creditCardBusinessDetailVo.setTncrcd(list.get(i).getTncrcd()); //交易币种代码
                creditCardBusinessDetailVo.setDbcrid(list.get(i).getDbcrid()); //借贷标志
                creditCardBusinessDetailVo.setTxnamt(list.get(i).getTxnamt()); //交易金额
                creditCardBusinessDetailVo.setTnstds(list.get(i).getTnstds()); //账单交易描述
                creditCardBusinessDetailVo.setPtdate(list.get(i).getPtdate()); //入账日期
                creditCardBusinessDetailVo.setPtcrcd(list.get(i).getPtcrcd()); //入账币种
                creditCardBusinessDetailVo.setPstamt(list.get(i).getPstamt()); //入账币种金额
                creditCardBusinessDetailVo.setCupsou(list.get(i).getCupsou()); //资金来源
                creditCardBusinessDetailVo.setCuptno(list.get(i).getCuptno()); //付款方账号
                creditCardBusinessDetailVo.setCupdno(list.get(i).getCupdno()); //付款方卡号
                creditCardBusinessDetailVo.setCupame(list.get(i).getCupame()); //付款方名称
                creditCardBusinessDetailVo.setCupent(list.get(i).getCupent()); //附言
                // TODO(“cardtype账户类型？") ；
                sernoList.add(creditCardBusinessDetailVo);
            }
        }
        return new ResultDto<List<CreditCardBusinessDetailVo>>(sernoList);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param cardBusinessDto
     *            通过卡号查询卡详情
     * @算法描述:
     */
    @ApiOperation("通过卡号查询卡详情")
    @PostMapping("/findcardbycardno")
    protected ResultDto<CardDetailDto> findCardByCardNo(@RequestBody CardBusinessDto cardBusinessDto) throws ParseException {
        String cardNo = cardBusinessDto.getCardNo();
        // 校验卡号的有效性及状态
        D14020ReqDto reqDto = new D14020ReqDto();
        reqDto.setCardno(cardNo);
        log.info("卡片信息查询发送报文："+reqDto);
        ResultDto<D14020RespDto> repDto=  dscms2TonglianClientService.d14020(reqDto);
        log.info("卡片信息查询返回报文："+repDto);
        if("0".equals(repDto.getCode())){
            String actvin = repDto.getData().getActvin();
            String bkcode = repDto.getData().getBkcode();
            if(!"Y".equals(actvin)){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "卡号无效,卡未激活！");
            }
            //锁定码值判断账户状态
            if(!"".equals(bkcode) && bkcode !=null){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该账户状态异常！");
            }
            String cdepdt =  repDto.getData().getCdepdt();//卡片有效期
            //取系统日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            SimpleDateFormat  formate = new SimpleDateFormat("yyyy-MM-dd");
            Date nowDay = formate.parse(openDay);
            String year = cdepdt.substring(0,4);
            String mon = cdepdt.substring(4,6);
            cdepdt = year +"-"+mon+"-01";
            Date cardDay = formate.parse(cdepdt);
            if(nowDay.compareTo(cardDay)>0) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "卡号已过有效期！");
            }
            }else{
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, repDto.getMessage());
        }
        D11010ReqDto d11010ReqDto = new D11010ReqDto();
        d11010ReqDto.setCardno(cardNo);
        d11010ReqDto.setOpt("0");
        d11010ReqDto.setNtnaty("156");
        ResultDto<D11010RespDto> d11010RespDtoResultDto = dscms2TonglianClientService.d11010(d11010ReqDto);
        D12000ReqDto d12000ReqDto = new D12000ReqDto();
        d12000ReqDto.setCardno(cardNo);
        d12000ReqDto.setCrcycd("156");
        ResultDto<D12000RespDto> d12000RespDtoResultDto = dscms2TonglianClientService.d12000(d12000ReqDto);
        CardDetailDto cardDetailDto = new CardDetailDto();
        if(d11010RespDtoResultDto.getData() != null){
            String certType = d11010RespDtoResultDto.getData().getIdtftp();
            if("I".equals(certType)){
                certType = "A";
            } else if ("A".equals(certType)) {
                certType = "G";
            }else if ("S".equals(certType)) {
                certType = "H";
            }else if ("P".equals(certType)) {
                certType = "B";
            }else if ("O".equals(certType)) {
                certType = "T";
            }else if ("R".equals(certType)) {
                certType = "C";
            }else if ("H".equals(certType)) {
                certType = "D";
            }else if ("W".equals(certType)) {
                certType = "E";
            }else if ("C".equals(certType)) {
                certType = "Y";
            }else if ("L".equals(certType)||"G".equals(certType)||"J".equals(certType)) {
                certType = "W";
            }else{
                certType = "T";
            }
            cardDetailDto.setCertType(certType);
            cardDetailDto.setCertCode(d11010RespDtoResultDto.getData().getIdtfno());
            cardDetailDto.setCusName(d11010RespDtoResultDto.getData().getCustnm());
        }
        if(d12000RespDtoResultDto.getData() != null){
            cardDetailDto.setCardPrd(d12000RespDtoResultDto.getData().getPrctnm());
            cardDetailDto.setOrigCreditCardLmt(d12000RespDtoResultDto.getData().getCdtlmt());
        }
        return new ResultDto<CardDetailDto>(cardDetailDto);
    }

    @PostMapping("/findcard")
    protected ResultDto<D13160RespDto> findCard(@RequestBody String cardNo) {
        D14020ReqDto d14020ReqDto = new D14020ReqDto();
        d14020ReqDto.setCardno(cardNo);
        ResultDto<D14020RespDto> d14020RespDtoResultDto = dscms2TonglianClientService.d14020(d14020ReqDto);
        D15011ReqDto reqDto = new D15011ReqDto();
        reqDto.setCardno(cardNo);
        reqDto.setCrcycd("156");
        reqDto.setOpt("1");
        reqDto.setCdtlmt(BigDecimal.valueOf(66666));
        ResultDto<D15011RespDto> d15011RespDtoResultDto = dscms2TonglianClientService.d15011(reqDto);

        D13160ReqDto d13160ReqDto = new D13160ReqDto();
        d13160ReqDto.setCardno(cardNo); //卡号
        d13160ReqDto.setCrcycd("156"); //币种
        d13160ReqDto.setLoannt(BigDecimal.valueOf(66666)); //分期金额
        d13160ReqDto.setLoanterm("6"); //分期期数
        d13160ReqDto.setLnfemd(""); //分期手续费收取方式
        d13160ReqDto.setLoanmethod("F"); //分期本金分配方式
        d13160ReqDto.setLothod(""); //分期手续费计算方式
        d13160ReqDto.setLoanrate("0.1"); //分期手续费比例
        d13160ReqDto.setLoanind("Y");
        d13160ReqDto.setOptcod("1");

        D13087ReqDto d13087ReqDto = new D13087ReqDto();
        d13087ReqDto.setCardno(cardNo);//卡号
        d13087ReqDto.setRgstid("424");//分期申请号
        ResultDto<D13087RespDto> d13087RespDtoResultDto = dscms2TonglianClientService.d13087(d13087ReqDto);
        //调用通联接口进行试算
        ResultDto<D13160RespDto> d13160RespDtoResultDto = dscms2TonglianClientService.d13160(d13160ReqDto);

        return d13160RespDtoResultDto;
    }

    /**
     * 导出模板
     */
    @ApiOperation("导出模板")
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate(){
        ProgressDto progressDto = creditCardAdjustmentAppInfoService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @ApiOperation("批量导入")
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库，StudentScore为导入数据的类
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(CreditCardAdjustmentListVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(creditCardAdjustmentAppInfoService::insertInBatch)));
        log.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }

    /**
     * @param  reditCardAdjustmentAppInfo
     * @return
     * @author wzy
     * @date 2021/7/5 10:53
     * @version 1.0.0
     * @desc   请求征信
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("请求征信")
    @PostMapping("/getcreditreportinfo")
    public ResultDto<Integer> getCreditReportInfo(@RequestBody CreditCardAdjustmentAppInfo reditCardAdjustmentAppInfo){
        int result = creditCardAdjustmentAppInfoService.getCreditReportInfo(reditCardAdjustmentAppInfo);
        return new ResultDto<Integer>(result);
    }
}
