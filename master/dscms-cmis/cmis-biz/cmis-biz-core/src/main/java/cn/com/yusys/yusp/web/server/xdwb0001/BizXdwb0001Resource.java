package cn.com.yusys.yusp.web.server.xdwb0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdwb0001.req.Xdwb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.resp.Xdwb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdwb0001.Xdwb0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小省联社金融服务平台借据信息查询接口
 *
 * @author zcrbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDWB0001:省联社金融服务平台借据信息查询接口")
@RestController
@RequestMapping("/api/bizwb4bsp")
public class BizXdwb0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdwb0001Resource.class);
    @Autowired
    private Xdwb0001Service xdwb0001Service;

    /**
     * 交易码：xdwb0001
     * 交易描述：小省联社金融服务平台借据信息查询接口
     *
     * @param xdwb0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("省联社金融服务平台借据信息查询接口")
    @PostMapping("/xdwb0001")
    protected @ResponseBody
    ResultDto<Xdwb0001DataRespDto> xdwb0001(@Validated @RequestBody Xdwb0001DataReqDto xdwb0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataReqDto));
        Xdwb0001DataRespDto xdwb0001DataRespDto = new Xdwb0001DataRespDto();// 响应Dto:更新信贷台账信息
        ResultDto<Xdwb0001DataRespDto> xdwb0001DataResultDto = new ResultDto<>();

        try {
            // 从xdwb0001DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataReqDto));
            xdwb0001DataRespDto = xdwb0001Service.xdwb0001(xdwb0001DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataRespDto));
            // 封装xdwb0001DataResultDto中正确的返回码和返回信息
            xdwb0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdwb0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, e.getMessage());
            // 封装xdwb0001DataResultDto中异常返回码和返回信息
            xdwb0001DataResultDto.setCode(e.getErrorCode());
            xdwb0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, e.getMessage());
            // 封装xdwb0001DataResultDto中异常返回码和返回信息
            xdwb0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdwb0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdwb0001DataRespDto到xdwb0001DataResultDto中
        xdwb0001DataResultDto.setData(xdwb0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataResultDto));
        return xdwb0001DataResultDto;
    }
}
