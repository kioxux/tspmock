/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeDetail
 * @类描述: rpt_spd_anys_jyxwyd_rent_income_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-12 22:03:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jyxwyd_rent_income_detail")
public class RptSpdAnysJyxwydRentIncomeDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 租金收入流水号 **/
	@Column(name = "RENT_SERNO", unique = false, nullable = true, length = 40)
	private String rentSerno;
	
	/** 年份 **/
	@Column(name = "YEAR", unique = false, nullable = true, length = 10)
	private String year;
	
	/** 金额 **/
	@Column(name = "AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param rentSerno
	 */
	public void setRentSerno(String rentSerno) {
		this.rentSerno = rentSerno;
	}
	
    /**
     * @return rentSerno
     */
	public String getRentSerno() {
		return this.rentSerno;
	}
	
	/**
	 * @param year
	 */
	public void setYear(String year) {
		this.year = year;
	}
	
    /**
     * @return year
     */
	public String getYear() {
		return this.year;
	}
	
	/**
	 * @param amt
	 */
	public void setAmt(java.math.BigDecimal amt) {
		this.amt = amt;
	}
	
    /**
     * @return amt
     */
	public java.math.BigDecimal getAmt() {
		return this.amt;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}