package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.DocArchiveInfo;
import cn.com.yusys.yusp.domain.DocReadAppInfo;
import cn.com.yusys.yusp.domain.DocReadDetailInfo;
import cn.com.yusys.yusp.domain.dto.DocReceiveInfoDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.Doc005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.IndexData;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.resp.Doc005RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.repository.mapper.DocReadAppInfoMapper;
import cn.com.yusys.yusp.repository.mapper.DocReadDetailInfoMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 17:00:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocReadAppInfoService {

    @Autowired
    private DocReadAppInfoMapper docReadAppInfoMapper;
    @Autowired
    private DocReadDetailInfoService docReadDetailInfoService;
    @Autowired
    private Dscms2DocClientService dscms2DocClientService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private DocReadDelayInfoService docReadDelayInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DocReadDetailInfoMapper docReadDetailInfoMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public DocReadAppInfo selectByPrimaryKey(String draiSerno) {
        return docReadAppInfoMapper.selectByPrimaryKey(draiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<DocReadAppInfo> selectAll(QueryModel model) {
        List<DocReadAppInfo> records = (List<DocReadAppInfo>) docReadAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocReadAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("read_rqstr_date desc");
        List<DocReadAppInfo> list = docReadAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryDocReceiveInfo
     * @方法描述: 条件查询 - 查询接收中和归还中的档案数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocReceiveInfoDto> queryDocReceiveInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("read_rqstr_date desc");
        List<DocReceiveInfoDto> list = docReadAppInfoMapper.queryDocReceiveInfo(model);
        if (null != list && list.size() > 0) {
            for (DocReceiveInfoDto docReceiveInfoDto : list) {
                String draiSerno = docReceiveInfoDto.getDraiSerno();
                String delayBackDate = docReadDelayInfoService.getDelayBackDate(draiSerno);
                docReceiveInfoDto.setDelayBackDate(delayBackDate);
            }
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: updateDocStsApply
     * @方法描述: 调阅申请通过的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @算法描述: 错误码
     */
    public Map<String, String> updateDocStsApply(String draiSerno, String backDate) {
        Map<String, String> res = new HashMap<>();
        if (StringUtils.nonBlank(draiSerno)) {
            // 修改
            String updId = "";
            // 修改
            String updBrId = "";
            // 修改日期
            String updDate = "";
            // 修改时间
            Date updateTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                updId = userInfo.getLoginCode();
                // 申请机构
                updBrId = userInfo.getOrg().getCode();
                // 申请时间
                updDate = DateUtils.getCurrDateStr();
            }
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("docStauts", "07");// 07 待出库
            param.put("updId", updId);
            param.put("updBrId", updBrId);
            param.put("updDate", updDate);
            param.put("updateTime", updateTime);
            param.put("draiSerno", draiSerno);
            param.put("backDate", backDate);
            //int count = docReadAppInfoMapper.updateDocStsApply(param);
            List<String> docNos = docReadDetailInfoMapper.getDocNosByDraiSerno(draiSerno);
            int count = 0;
            if (CollectionUtils.nonEmpty(docNos)) {
                param.put("docNos", docNos);
                count = docReadAppInfoMapper.updateDocStsApplyByDocNos(param);
            }

            if (count > 0) {
                res.put("code", "0");
                res.put("msg", "调阅申请成功!");
            } else {
                res.put("code", "-1");
                res.put("msg", "调阅申请失败!");
            }
        } else {
            res.put("code", "-1");
            res.put("msg", "发起归还失败!");
        }
        return res;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(DocReadAppInfo record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String inputDate = "";
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.parseDateByDef(openDay);
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            // 申请时间
            inputDate = openDay;
        }
        record.setInputId(inputId);
        record.setInputBrId(inputBrId);
        record.setInputDate(inputDate);
        record.setCreateTime(createTime);
        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return docReadAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(DocReadAppInfo record) {
        return docReadAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(DocReadAppInfo record) {
        // 修改
        String updId = "";
        // 修改
        String updBrId = "";
        // 修改日期
        String updDate = "";
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 修改时间
        Date updateTime = DateUtils.parseDateByDef(openDay);
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
            // 申请时间
            updDate = openDay;
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        record.setUpdateTime(updateTime);
        int i = docReadAppInfoMapper.updateByPrimaryKeySelective(record);

        // 更新条件参数
        WFBizParamDto param = new WFBizParamDto();
        Map<String, Object> params = new HashMap<>();
        param.setBizId(record.getDraiSerno());
        params.put("readType", record.getReadType());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);

        return i;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(DocReadAppInfo record) {
        return docReadAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 删除档案申请记录的同时也要删除关联的档案记录
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String draiSerno) {
        int result = 0;
        DocReadAppInfo docReadAppInfo = docReadAppInfoMapper.selectByPrimaryKey(draiSerno);
        if (null != docReadAppInfo) {
            if (Objects.equals(docReadAppInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_992)) {
                docReadAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                docReadAppInfoMapper.updateByPrimaryKeySelective(docReadAppInfo);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(draiSerno);
                result = 1;
            } else if (Objects.equals(docReadAppInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_000)) {
                docReadDetailInfoService.deleteBySerno(draiSerno);
                result = docReadAppInfoMapper.deleteByPrimaryKey(draiSerno);
            }
        }
        return result;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return docReadAppInfoMapper.deleteByIds(ids);
    }

    /**
     * 档案申请出库
     *
     * @param draiSerno
     */
    public void applyDelivery(String draiSerno) {
        DocReadAppInfo info = docReadAppInfoMapper.selectByPrimaryKey(draiSerno);
        // 获取对应的档案明细列表
        List<DocReadDetailInfo> infoList = docReadDetailInfoService.selectByDraiSerno(info.getDraiSerno());
        for (DocReadDetailInfo docReadDetailInfo : infoList) {
            // 档案入库接口必传字段
            Doc005ReqDto doc005ReqDto = new Doc005ReqDto();
            // 初始化属性
            List<IndexData> list = new ArrayList<>();
            // 初始化indexdata内容
            IndexData indexData = new IndexData();
            // 调阅流水信息主键唯一(调阅明细id)
            indexData.setPkBappId(docReadDetailInfo.getDrdiSerno());
            // 调阅流水号
            indexData.setBappSeqNo(info.getDraiSerno());
            // 调阅单位
            indexData.setBappOrg(info.getReadOrg());
            // 调阅原因
            indexData.setBappReason(info.getReadReason());
            // 调阅人
            indexData.setBappIdCard("");
            indexData.setBappPhoneNumber("");
            indexData.setBappReader("");
            if (StringUtils.nonEmpty(info.getReadId())) {
                AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(info.getReadId());
                if (null != adminSmUserDto) {
                    // 身份证号
                    indexData.setBappIdCard(adminSmUserDto.getCertNo());
                    // 联系电话
                    indexData.setBappPhoneNumber(adminSmUserDto.getUserMobilephone());
                    // 调阅人
                    indexData.setBappReader(adminSmUserDto.getUserName());
                }
            }
            // 批准人
            indexData.setBappApprove(info.getInputId());
            // 操作机构
            indexData.setBappApproveOrg(info.getInputBrId());
            // 用户code
            indexData.setUserCode(info.getInputId());
            // 用户机构
            // 参照入库机构特殊类型特殊处理 DocArchiveInfoService.archiveOpt
            String orgCode = info.getInputBrId();
            // 公积金账务机构特殊处理
            if (Objects.equals("18", docReadDetailInfo.getDocType())) {
                orgCode = "999999";
            } else if (Objects.equals("21", docReadDetailInfo.getDocType()) || Objects.equals("22", docReadDetailInfo.getDocType())) {
                // 除新浦支行067000、即墨支行047000，其他小贷送016000
                if (!Objects.equals(orgCode, "047000") && !Objects.equals(orgCode, "067000")) {
                    orgCode = "016000";
                }
            }
            // 东海、寿光机构号特殊处理
            if (orgCode.startsWith("81")) {
                orgCode = "810100";// 东海村镇银行营业部
            } else if (orgCode.startsWith("80")) {
                orgCode = "800100";// 寿光村镇银行营业部
            }
            indexData.setUserOrg(orgCode);
            // 调阅证明（设置成调阅用途说明）
            indexData.setBappProve(info.getReadPurposeDesc());
            // 授权人
            indexData.setAuthCode(info.getInputId());
            // 调阅类型 1 内部调阅 2 外部调阅 3 纸质调阅
            indexData.setBappType("3");
            // 是否打印 0 未打印 1 已打印
            indexData.setPrint("1");
            // 公共用户
            indexData.setPublicCode("");
            // 调阅状态：0-待审批；1-审批中；2-审批通过；3-审批不通过  4-未审批
            indexData.setApproveStatus("2");
            // 出库状态 ：0-未出库  1-待出库   2-已出库   3-已归还,24：待归还，25:出库在途，26：归还在途，27:退回在途
            indexData.setOutRoomStatus("0");
            // 出库时间
            indexData.setOutRoomTime("");
            // 入库时间
            indexData.setInRoomTime("");
            // 交易日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            indexData.setTradeDate(openDay);
            // 交易机构号
            indexData.setTradeOrgCode(info.getInputBrId());
            // 交易柜员号
            indexData.setTradeUserCode(info.getInputId());
            // 交易机构名称
            indexData.setTradeOrgCodeName("");
            // 出库内容
            indexData.setOutRoomContent("");
            // 档案类型编号
            indexData.setFileTypeNum("0100050006");
            // 档案位置信息
            indexData.setStorePosition("");
            // 附件名称
            indexData.setAttachName("");
            // 出库机构
            indexData.setOutbranch("");
            if (StringUtils.nonEmpty(info.getReadOrg())) {
                AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(info.getReadOrg());
                if (null != adminSmOrgDto) {
                    indexData.setOutbranch(adminSmOrgDto.getOrgName());
                }
            }
            // 逾期标示 0-已经逾期 1-已经出库但是还未逾期 2-归还途中 3-未出库 4-其他
            indexData.setStatus("");
            // 审批流程ID
            indexData.setCaseId("");
            // 证件类型
            indexData.setBappIdType("");
            // 0-未出库  1-待出库   2-已出库   3-已归还,24：待归还，25:出库在途，26：归还在途
            indexData.setOutRoomStatusName("0");
            // 调阅记录id
            indexData.setFkBappId("");
            // 档案记录id
            indexData.setFkFileId("");
            // 档案编号
            indexData.setFileNum(docReadDetailInfo.getDocNo());
            // 档案名称
            indexData.setAccFileName(docReadDetailInfo.getCusName());
            // 创建时间
            indexData.setCreateTime("");
            // 出库时间
            indexData.setOuttime("");
            // 调阅申请日期
            indexData.setBappStartDate(info.getReadRqstrDate());
            // 调阅归还日期
            indexData.setBappEndDate(info.getBackDate());
            // 出库状态  0:出库登记 1:待出库 2:已出库  3：已归还  4：未出库
            indexData.setAccFileStatus("0");
            List<DocArchiveInfo> docArchiveInfos = docArchiveInfoService.selectByDocNo(docReadDetailInfo.getDocNo());
            if (Objects.nonNull(docArchiveInfos) && !docArchiveInfos.isEmpty()) {
                Map<String, String> map = new HashMap<>();
                map.put("K_BILLNO", docArchiveInfos.get(0).getDocSerno());
                // 索引信息(业务编号、合同号等信息):{\"K_BILLNO\":\"016000170816166\"}
                indexData.setIndexMap(JSON.toJSONString(map));
            }
            list.add(indexData);
            doc005ReqDto.setIndexData(list);
            // 调用档案系统接口，进行提交入库
            ResultDto<Doc005RespDto> doc005RespDto = dscms2DocClientService.doc005(doc005ReqDto);
            String doc005Code = Optional.ofNullable(doc005RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            if (Objects.equals(doc005Code, SuccessEnum.CMIS_SUCCSESS.key) && Objects.equals("申请出库成功!", doc005RespDto.getMessage())) {
                // 申请出库成功（把状态从待出库状态更新成07：待出库）
                docReadDetailInfoService.synDocStatus(docReadDetailInfo.getDrdiSerno(), "07", null);
            }
        }
    }
}
