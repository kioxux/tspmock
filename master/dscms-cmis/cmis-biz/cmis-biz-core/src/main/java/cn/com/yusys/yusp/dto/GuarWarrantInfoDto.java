package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantInfo
 * @类描述: guar_warrant_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 17:13:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarWarrantInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;

	/** 抵质押分类 **/
	private String grtFlag;

	/** 权证编号 **/
	private String warrantNo;

	/** 核心担保编号 **/
	private String coreGuarantyNo;
	
	/** 核心担保品序号 **/
	private String coreGuarantySeq;
	
	/** 权利凭证号 **/
	private String certiRecordId;
	
	/** 权证类别 **/
	private String certiCatalog;
	
	/** 权证类型 **/
	private String certiTypeCd;
	
	/** 是否电子权证 **/
	private String isEWarrant;
	
	/** 权证名称 **/
	private String warrantName;
	
	/** 权证发证机关名称 **/
	private String certiOrgName;
	
	/** 权证发证日期 **/
	private String certiStartDate;
	
	/** 权证到期日期 **/
	private String certiEndDate;
	
	/** 权证登记日期 **/
	private String warrantInputDate;
	
	/** 权证入库日期 **/
	private String inDate;
	
	/** 权证正常出库日期 **/
	private String outDate;
	
	/** 权利金额(权利价值) **/
	private java.math.BigDecimal certiAmt;

	/** 押品顺位标识 **/
	private String mortOrderFlag;
	
	/** 管存机关 **/
	private String depositOrgNo;
	
	/** 账务机构 **/
	private String finaBrId;
	
	/** 权证备注信息 **/
	private String certiComment;
	
	/** 权证临时借用人名称 **/
	private String tempBorrowerName;
	
	/** 权证临时出库日期 **/
	private String tempOutDate;
	
	/** 权证预计归还时间 **/
	private String preBackDate;
	
	/** 权证实际归还日期 **/
	private String realBackDate;
	
	/** 权证状态 **/
	private String certiState;
	
	/** 备注 **/
	private String remark;

	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;

	/** 证明权利或事项 **/
	private String proCertiItem;

	/** 是否集中作业打印 **/
	private String isFocusWorkPrint;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag == null ? null : grtFlag.trim();
	}
	
    /**
     * @return GrtFlag
     */	
	public String getGrtFlag() {
		return this.grtFlag;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo == null ? null : warrantNo.trim();
	}
	
    /**
     * @return WarrantNo
     */	
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo == null ? null : coreGuarantyNo.trim();
	}
	
    /**
     * @return CoreGuarantyNo
     */	
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param coreGuarantySeq
	 */
	public void setCoreGuarantySeq(String coreGuarantySeq) {
		this.coreGuarantySeq = coreGuarantySeq == null ? null : coreGuarantySeq.trim();
	}
	
    /**
     * @return CoreGuarantySeq
     */	
	public String getCoreGuarantySeq() {
		return this.coreGuarantySeq;
	}
	
	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId == null ? null : certiRecordId.trim();
	}
	
    /**
     * @return CertiRecordId
     */	
	public String getCertiRecordId() {
		return this.certiRecordId;
	}
	
	/**
	 * @param certiCatalog
	 */
	public void setCertiCatalog(String certiCatalog) {
		this.certiCatalog = certiCatalog == null ? null : certiCatalog.trim();
	}
	
    /**
     * @return CertiCatalog
     */	
	public String getCertiCatalog() {
		return this.certiCatalog;
	}
	
	/**
	 * @param certiTypeCd
	 */
	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd == null ? null : certiTypeCd.trim();
	}
	
    /**
     * @return CertiTypeCd
     */	
	public String getCertiTypeCd() {
		return this.certiTypeCd;
	}
	
	/**
	 * @param isEWarrant
	 */
	public void setIsEWarrant(String isEWarrant) {
		this.isEWarrant = isEWarrant == null ? null : isEWarrant.trim();
	}
	
    /**
     * @return IsEWarrant
     */	
	public String getIsEWarrant() {
		return this.isEWarrant;
	}
	
	/**
	 * @param warrantName
	 */
	public void setWarrantName(String warrantName) {
		this.warrantName = warrantName == null ? null : warrantName.trim();
	}
	
    /**
     * @return WarrantName
     */	
	public String getWarrantName() {
		return this.warrantName;
	}
	
	/**
	 * @param certiOrgName
	 */
	public void setCertiOrgName(String certiOrgName) {
		this.certiOrgName = certiOrgName == null ? null : certiOrgName.trim();
	}
	
    /**
     * @return CertiOrgName
     */	
	public String getCertiOrgName() {
		return this.certiOrgName;
	}
	
	/**
	 * @param certiStartDate
	 */
	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate == null ? null : certiStartDate.trim();
	}
	
    /**
     * @return CertiStartDate
     */	
	public String getCertiStartDate() {
		return this.certiStartDate;
	}
	
	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate == null ? null : certiEndDate.trim();
	}
	
    /**
     * @return CertiEndDate
     */	
	public String getCertiEndDate() {
		return this.certiEndDate;
	}
	
	/**
	 * @param warrantInputDate
	 */
	public void setWarrantInputDate(String warrantInputDate) {
		this.warrantInputDate = warrantInputDate == null ? null : warrantInputDate.trim();
	}
	
    /**
     * @return WarrantInputDate
     */	
	public String getWarrantInputDate() {
		return this.warrantInputDate;
	}
	
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate == null ? null : inDate.trim();
	}
	
    /**
     * @return InDate
     */	
	public String getInDate() {
		return this.inDate;
	}
	
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate == null ? null : outDate.trim();
	}
	
    /**
     * @return OutDate
     */	
	public String getOutDate() {
		return this.outDate;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return CertiAmt
     */	
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}

	/**
	 * @param mortOrderFlag
	 */
	public void setMortOrderFlag(String mortOrderFlag) {
		this.mortOrderFlag = mortOrderFlag == null ? null : mortOrderFlag.trim();
	}
	
    /**
     * @return MortOrderFlag
     */	
	public String getMortOrderFlag() {
		return this.mortOrderFlag;
	}
	
	/**
	 * @param depositOrgNo
	 */
	public void setDepositOrgNo(String depositOrgNo) {
		this.depositOrgNo = depositOrgNo == null ? null : depositOrgNo.trim();
	}
	
    /**
     * @return DepositOrgNo
     */	
	public String getDepositOrgNo() {
		return this.depositOrgNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param certiComment
	 */
	public void setCertiComment(String certiComment) {
		this.certiComment = certiComment == null ? null : certiComment.trim();
	}
	
    /**
     * @return CertiComment
     */	
	public String getCertiComment() {
		return this.certiComment;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName == null ? null : tempBorrowerName.trim();
	}
	
    /**
     * @return TempBorrowerName
     */	
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate == null ? null : tempOutDate.trim();
	}
	
    /**
     * @return TempOutDate
     */	
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate == null ? null : preBackDate.trim();
	}
	
    /**
     * @return PreBackDate
     */	
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate == null ? null : realBackDate.trim();
	}
	
    /**
     * @return RealBackDate
     */	
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param certiState
	 */
	public void setCertiState(String certiState) {
		this.certiState = certiState == null ? null : certiState.trim();
	}
	
    /**
     * @return CertiState
     */	
	public String getCertiState() {
		return this.certiState;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

    /**
     * @return ManagerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param proCertiItem
	 */
	public void setProCertiItem(String proCertiItem) {
		this.proCertiItem = proCertiItem == null ? null : proCertiItem.trim();
	}
	
    /**
     * @return ProCertiItem
     */	
	public String getProCertiItem() {
		return this.proCertiItem;
	}

	/**
	 * @param isFocusWorkPrint
	 */
	public void setIsFocusWorkPrint(String isFocusWorkPrint) {
		this.isFocusWorkPrint = isFocusWorkPrint == null ? null : isFocusWorkPrint.trim();
	}

	/**
	 * @return IsFocusWorkPrint
	 */
	public String getIsFocusWorkPrint() {
		return this.isFocusWorkPrint;
	}


}