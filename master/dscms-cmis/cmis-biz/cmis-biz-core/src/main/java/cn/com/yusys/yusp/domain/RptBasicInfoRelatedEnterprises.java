/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRelatedEnterprises
 * @类描述: rpt_basic_info_related_enterprises数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-10 17:17:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_related_enterprises")
public class RptBasicInfoRelatedEnterprises extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 关联企业名称 **/
	@Column(name = "REL_CUS_NAME", unique = false, nullable = true, length = 40)
	private String relCusName;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 股权结构 **/
	@Column(name = "STK_STR", unique = false, nullable = true, length = 5)
	private String stkStr;
	
	/** 主营业务 **/
	@Column(name = "MAIN_BUSI", unique = false, nullable = true, length = 40)
	private String mainBusi;
	
	/** 土地(平方) **/
	@Column(name = "LAND_SQU", unique = false, nullable = true, length = 40)
	private String landSqu;
	
	/** 房产(平方) **/
	@Column(name = "HOUSE_SQU", unique = false, nullable = true, length = 40)
	private String houseSqu;
	
	/** 抵押情况 **/
	@Column(name = "PLD_CASE", unique = false, nullable = true, length = 5)
	private String pldCase;
	
	/** 上年销售 **/
	@Column(name = "LAST_YEAR_SALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearSale;
	
	/** 净利润 **/
	@Column(name = "PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal profit;
	
	/** 本期资产总额 **/
	@Column(name = "CURR_ASS_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currAssTotal;
	
	/** 本期净资产 **/
	@Column(name = "CURR_PURE_ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currPureAssetValue;
	
	/** 他行融资额度 **/
	@Column(name = "OTHER_BANK_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherBankLmt;
	
	/** 对外担保额度 **/
	@Column(name = "OUTGUAR_TOTAL_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outguarTotalLmt;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param relCusName
	 */
	public void setRelCusName(String relCusName) {
		this.relCusName = relCusName;
	}
	
    /**
     * @return relCusName
     */
	public String getRelCusName() {
		return this.relCusName;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param stkStr
	 */
	public void setStkStr(String stkStr) {
		this.stkStr = stkStr;
	}
	
    /**
     * @return stkStr
     */
	public String getStkStr() {
		return this.stkStr;
	}
	
	/**
	 * @param mainBusi
	 */
	public void setMainBusi(String mainBusi) {
		this.mainBusi = mainBusi;
	}
	
    /**
     * @return mainBusi
     */
	public String getMainBusi() {
		return this.mainBusi;
	}
	
	/**
	 * @param landSqu
	 */
	public void setLandSqu(String landSqu) {
		this.landSqu = landSqu;
	}
	
    /**
     * @return landSqu
     */
	public String getLandSqu() {
		return this.landSqu;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(String houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return houseSqu
     */
	public String getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param pldCase
	 */
	public void setPldCase(String pldCase) {
		this.pldCase = pldCase;
	}
	
    /**
     * @return pldCase
     */
	public String getPldCase() {
		return this.pldCase;
	}
	
	/**
	 * @param lastYearSale
	 */
	public void setLastYearSale(java.math.BigDecimal lastYearSale) {
		this.lastYearSale = lastYearSale;
	}
	
    /**
     * @return lastYearSale
     */
	public java.math.BigDecimal getLastYearSale() {
		return this.lastYearSale;
	}
	
	/**
	 * @param profit
	 */
	public void setProfit(java.math.BigDecimal profit) {
		this.profit = profit;
	}
	
    /**
     * @return profit
     */
	public java.math.BigDecimal getProfit() {
		return this.profit;
	}
	
	/**
	 * @param currAssTotal
	 */
	public void setCurrAssTotal(java.math.BigDecimal currAssTotal) {
		this.currAssTotal = currAssTotal;
	}
	
    /**
     * @return currAssTotal
     */
	public java.math.BigDecimal getCurrAssTotal() {
		return this.currAssTotal;
	}
	
	/**
	 * @param currPureAssetValue
	 */
	public void setCurrPureAssetValue(java.math.BigDecimal currPureAssetValue) {
		this.currPureAssetValue = currPureAssetValue;
	}
	
    /**
     * @return currPureAssetValue
     */
	public java.math.BigDecimal getCurrPureAssetValue() {
		return this.currPureAssetValue;
	}
	
	/**
	 * @param otherBankLmt
	 */
	public void setOtherBankLmt(java.math.BigDecimal otherBankLmt) {
		this.otherBankLmt = otherBankLmt;
	}
	
    /**
     * @return otherBankLmt
     */
	public java.math.BigDecimal getOtherBankLmt() {
		return this.otherBankLmt;
	}
	
	/**
	 * @param outguarTotalLmt
	 */
	public void setOutguarTotalLmt(java.math.BigDecimal outguarTotalLmt) {
		this.outguarTotalLmt = outguarTotalLmt;
	}
	
    /**
     * @return outguarTotalLmt
     */
	public java.math.BigDecimal getOutguarTotalLmt() {
		return this.outguarTotalLmt;
	}


}