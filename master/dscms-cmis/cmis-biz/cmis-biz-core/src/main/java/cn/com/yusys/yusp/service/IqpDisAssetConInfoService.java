/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDisAssetConInfo;
import cn.com.yusys.yusp.repository.mapper.IqpDisAssetConInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetConInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-05-06 16:37:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpDisAssetConInfoService {

    @Autowired
    private IqpDisAssetConInfoMapper iqpDisAssetConInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpDisAssetConInfo selectByPrimaryKey(String iqpSerno) {
        return iqpDisAssetConInfoMapper.selectByPrimaryKey(iqpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDisAssetConInfo> selectAll(QueryModel model) {
        List<IqpDisAssetConInfo> records = (List<IqpDisAssetConInfo>) iqpDisAssetConInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpDisAssetConInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDisAssetConInfo> list = iqpDisAssetConInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpDisAssetConInfo record) {
        return iqpDisAssetConInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpDisAssetConInfo record) {
        return iqpDisAssetConInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpDisAssetConInfo record) {
        return iqpDisAssetConInfoMapper.updateByPrimaryKey(record);
    }


    /**
     * @方法名称: updateIqpDisAssetConInfo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateIqpDisAssetConInfo(IqpDisAssetConInfo record) {

        IqpDisAssetConInfo iqpDisAssetConInfo = iqpDisAssetConInfoMapper.selectByPrimaryKey(record.getIqpSerno());
        if(iqpDisAssetConInfo == null){
            iqpDisAssetConInfoMapper.insert(record);
        }else{
            iqpDisAssetConInfoMapper.updateByPrimaryKey(record);
        }
        return 0;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpDisAssetConInfo record) {
        return iqpDisAssetConInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpDisAssetConInfoMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDisAssetConInfoMapper.deleteByIds(ids);
    }
}
