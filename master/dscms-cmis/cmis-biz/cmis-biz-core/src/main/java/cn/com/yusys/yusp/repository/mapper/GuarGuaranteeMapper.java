/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.GuarGuaranteeDto;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称:
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-28 20:27:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarGuaranteeMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    GuarGuarantee selectByPrimaryKey(@Param("guarantyId") String guarantyId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<GuarGuarantee> selectByModel(QueryModel model);

    /**
     * 根据担保合同编号查询关联的保证人信息
     * @param model
     * @return
     */
    List<GuarGuarantee> queryListByGuarContNo(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(GuarGuarantee record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(GuarGuarantee record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(GuarGuarantee record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(GuarGuarantee record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("guarantyId") String guarantyId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    List<GuarBizRelGuaranteeDto> selectByIqpSernoModel(QueryModel queryModel);

    /**
     * @函数名称:selectGuarGuaranteeByGuarContNo
     * @函数描述:根据担保合同编号获取保证人信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarGuarantee> selectGuarGuaranteeByGuarContNo(QueryModel queryModel);

    /**
     * 根据授信分项编号查询担保合同关联的保证人信息
     * @param queryModel
     * @return
     */
    String selectGuarantyIdsByLmtAccNo(QueryModel queryModel);

    /**
     * 根据客户号和担保合同编号去查询签约日期
     *
     * @param queryMap
     * @return
     */
    String queryGuarGuaranteeSignDateByMap(Map queryMap);

    /**
     * 根据担保合同号查询cusid_list、 cusname_list
     *
     * @param dbContNo
     * @return
     */
    HashMap<String, Object> selectlListByDbContNo(String dbContNo);

    /**
     * 根据合同号查询担保合同未签约的保证人数量
     * @param contNo
     * @return
     */
    int selectCountByContNo(String contNo);

    /**
     * 根据合同号更新借款合同或担保合同签约状态
     * @param queryMap
     * @return
     */
    int updateContStatusByContNo(Map queryMap);

    /**
     * @函数名称:queryGuarGuaranteeIsUnderLmt
     * @函数描述:根据业务流水号查询授信项下的押品保证人信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarGuarantee> queryGuarGuaranteeIsUnderLmt(QueryModel queryModel);

    /**
     * @函数名称:queryGuarGuaranteeBySerno
     * @函数描述:根据业务分项流水号查询授信项下的押品保证人信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarGuarantee> queryGuarGuaranteeBySerno(String serno);

    /**
     * @函数名称:queryGuarBaseInfoBySerno
     * @函数描述:根据业务分项流水号查询授信项下的抵质押信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarBaseInfoBySerno(String serno);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * 根据担保合同编号获取保证人编号
     * @param guarContNo 担保合同号
     * @return 保证人编号
     */
    List<GuarGuarantee> queryCusIDByGuarContNo(@Param("guarContNo") String guarContNo);

    List<Map<String,Object>> queryGuarGuaranteeBySubSerno(@Param("subSerno") String subPrdSerno);

    /**
     * 根据申请流水号查询保证人信息
     * @param serno
     * @return
     */
    ArrayList<GuarGuarantee> queryGuaranteeInfo(String serno);

    /**
     * 根据流水号查询授信分项下保证人
     * @param serno
     * @return
     */
    List<GuarGuarantee> getGuarGuaranteeBySerno(@Param("serno") String serno);

    /**
     * 根据流水号查询授信分项下主担保下保证人
     * @param serno
     * @return
     */
    List<GuarGuarantee> getMainGuarGuaranteeBySerno(@Param("serno") String serno,@Param("isAddGuar") String isAddGuar);

    /**
     * 根据流水号查询授信分项下保证人
     * @param serno
     * @return
     */
    List<GuarGuarantee> queryGuarGuaranteeDataBySerno(@Param("serno") String serno);

    /**
     * @方法名称: queryGuarGuaranteeByGuarContNo
     * @方法描述: 根据担保合同编号关联查询保证人信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<GuarGuaranteeDto> queryGuarGuaranteeByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * @param map
     * @return List
     * @author css
     * @desc 审查报告查询保证人列表
     * @修改历史: V1.0
     */

    List<Map> querGuaranteeRelByLmtAppSerno(Map map);
}