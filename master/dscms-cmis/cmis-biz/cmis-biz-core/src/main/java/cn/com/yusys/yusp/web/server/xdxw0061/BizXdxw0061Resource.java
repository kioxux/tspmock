package cn.com.yusys.yusp.web.server.xdxw0061;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0061.req.Xdxw0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0061.resp.Xdxw0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0061.Xdxw0061Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:通过无还本续贷调查表编号查询配偶核心客户号
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0061:通过无还本续贷调查表编号查询配偶核心客户号")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0061Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0061Resource.class);

    @Autowired
    private Xdxw0061Service xdxw0061Service;

    /**
     * 交易码：xdxw0061
     * 交易描述：通过无还本续贷调查表编号查询配偶核心客户号
     *
     * @param xdxw0061DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("通过无还本续贷调查表编号查询配偶核心客户号")
    @PostMapping("/xdxw0061")
    protected @ResponseBody
    ResultDto<Xdxw0061DataRespDto> xdxw0061(@Validated @RequestBody Xdxw0061DataReqDto xdxw0061DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0061DataReqDto));
        Xdxw0061DataRespDto xdxw0061DataRespDto = new Xdxw0061DataRespDto();// 响应Dto:通过无还本续贷调查表编号查询配偶核心客户号
        ResultDto<Xdxw0061DataRespDto> xdxw0061DataResultDto = new ResultDto<>();
        String indgtSerno = xdxw0061DataReqDto.getIndgtSerno();//调查流水号
        // 从xdxw0061DataReqDto获取业务值进行业务逻辑处理
        try {
            if (StringUtils.isBlank(indgtSerno)) {
                xdxw0061DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0061DataResultDto.setMessage("调查流水号【indgtSerno】不能为空！");
                return xdxw0061DataResultDto;
            }
            xdxw0061DataRespDto = xdxw0061Service.xdxw0061Service(xdxw0061DataReqDto);
            // 封装xdxw0061DataResultDto中正确的返回码和返回信息
            xdxw0061DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0061DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, e.getMessage());
            // 封装xdxw0061DataResultDto中异常返回码和返回信息
            xdxw0061DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0061DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0061DataRespDto到xdxw0061DataResultDto中
        xdxw0061DataResultDto.setData(xdxw0061DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0061DataResultDto));
        return xdxw0061DataResultDto;
    }
}
