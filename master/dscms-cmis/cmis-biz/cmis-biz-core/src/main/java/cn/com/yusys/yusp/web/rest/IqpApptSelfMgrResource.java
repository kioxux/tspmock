/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpApptSelfMgr;
import cn.com.yusys.yusp.service.IqpApptSelfMgrService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptSelfMgrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:16:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpapptselfmgr")
public class IqpApptSelfMgrResource {
    @Autowired
    private IqpApptSelfMgrService iqpApptSelfMgrService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpApptSelfMgr>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpApptSelfMgr> list = iqpApptSelfMgrService.selectAll(queryModel);
        return new ResultDto<List<IqpApptSelfMgr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpApptSelfMgr>> index(QueryModel queryModel) {
        List<IqpApptSelfMgr> list = iqpApptSelfMgrService.selectByModel(queryModel);
        return new ResultDto<List<IqpApptSelfMgr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<IqpApptSelfMgr> show(@PathVariable("serno") String serno) {
        IqpApptSelfMgr iqpApptSelfMgr = iqpApptSelfMgrService.selectByPrimaryKey(serno);
        return new ResultDto<IqpApptSelfMgr>(iqpApptSelfMgr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpApptSelfMgr> create(@RequestBody IqpApptSelfMgr iqpApptSelfMgr) throws URISyntaxException {
        iqpApptSelfMgrService.insert(iqpApptSelfMgr);
        return new ResultDto<IqpApptSelfMgr>(iqpApptSelfMgr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpApptSelfMgr iqpApptSelfMgr) throws URISyntaxException {
        int result = iqpApptSelfMgrService.update(iqpApptSelfMgr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpApptSelfMgrService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpApptSelfMgrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
