package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayDateChg;
import cn.com.yusys.yusp.service.IqpRepayDateChgService;
import cn.com.yusys.yusp.util.BizUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayDateChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: fangzhen
 * @创建时间: 2021-01-12 19:12:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepaydatechg")
public class IqpRepayDateChgResource {
    @Autowired
    private IqpRepayDateChgService iqpRepayDateChgService;

    /**
     * 点击下一步 保存数据到还款日变更申请表  申请状态为待提交
     *
     * @param iqpRepayDateChg
     * @return
     */
    @PostMapping("/commit")
    public ResultDto<Integer> commit(@RequestBody IqpRepayDateChg iqpRepayDateChg) {
        int result = this.iqpRepayDateChgService.commit(iqpRepayDateChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRepayDateChg>> index(QueryModel queryModel) {
        List<IqpRepayDateChg> list = iqpRepayDateChgService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * 添加日期变更申请  初始化审核状态为带发起   操作类型为新增
     *
     * @param iqpRepayDateChg
     * @return
     */
    @PostMapping("/save")
    public ResultDto<Integer> save(@RequestBody IqpRepayDateChg iqpRepayDateChg) {
        int result = this.iqpRepayDateChgService.save(iqpRepayDateChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 逻辑删除还款日变更申请
     * @param iqpRepayDateChg
     * @return
     */
    @PostMapping("/updateOprTypeByPrimaryKey")
    public ResultDto<Integer> updateOprTypeByPrimaryKey(@RequestBody IqpRepayDateChg iqpRepayDateChg) {
        int result = this.iqpRepayDateChgService.updateOprTypeByPrimaryKey(iqpRepayDateChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * 首页点击修改按钮   再点击保存按钮保存数据
     * @param iqpRepayDateChg
     * @return
     */
    @PostMapping("/updateCommit")
    public ResultDto<Integer> updateCommit(@RequestBody IqpRepayDateChg iqpRepayDateChg) {
        int result = this.iqpRepayDateChgService.updateCommit(iqpRepayDateChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 发起申请校验当前借据是否又其他流程处于审核状态
     * @param iqpRepayDateChg
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/checkIsExistIqpRepayWayChgBizByBillNo")
    protected ResultDto<Integer> checkIsExistIqpRepayWayChgBizByBillNo(@RequestBody IqpRepayDateChg iqpRepayDateChg) throws URISyntaxException {
        BizUtils bizUtils = new BizUtils();
        int  result=this.iqpRepayDateChgService.checkIsExistChgBizByBillNo(iqpRepayDateChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据主键查询IqpRepayDateChg对象
     * @param iqpRepayDateChg
     * @return
     */
    @PostMapping("/selectByPrimaryKey")
    public ResultDto<IqpRepayDateChg> selectByPrimaryKey(@RequestBody IqpRepayDateChg iqpRepayDateChg) {
        IqpRepayDateChg result = this.iqpRepayDateChgService.selectByPrimaryKey(iqpRepayDateChg);
        return new ResultDto<IqpRepayDateChg>(result);
    }

}
