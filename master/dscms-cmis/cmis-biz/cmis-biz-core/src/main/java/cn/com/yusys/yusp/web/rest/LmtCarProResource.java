/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtCarPro;
import cn.com.yusys.yusp.service.LmtCarProService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarProResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 16:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtcarpro")
public class LmtCarProResource {
    @Autowired
    private LmtCarProService lmtCarProService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtCarPro>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtCarPro> list = lmtCarProService.selectAll(queryModel);
        return new ResultDto<List<LmtCarPro>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtCarPro>> index(QueryModel queryModel) {
        List<LmtCarPro> list = lmtCarProService.selectByModel(queryModel);
        return new ResultDto<List<LmtCarPro>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtCarPro> show(@PathVariable("serno") String serno) {
        LmtCarPro lmtCarPro = lmtCarProService.selectByPrimaryKey(serno);
        return new ResultDto<LmtCarPro>(lmtCarPro);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtCarPro> create(@RequestBody LmtCarPro lmtCarPro) throws URISyntaxException {
        lmtCarProService.insert(lmtCarPro);
        return new ResultDto<LmtCarPro>(lmtCarPro);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtCarPro lmtCarPro) throws URISyntaxException {
        int result = lmtCarProService.update(lmtCarPro);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtCarProService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtCarProService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
