package cn.com.yusys.yusp.service.server.xdht0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0026.CmisLmt0026Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0002Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0002Service.class);
    @Autowired
    private CtrAccpContService ctrAccpContService;
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private PvpAccpAppService pvpAccpAppService;
    @Autowired
    private BailDepositInfoService bailDepositInfoService;
    @Autowired
    private CmisLmt0026Service cmisLmt0026Service;


    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    /**
     * 银承协议查询 </br>
     * 场景：票据系统调用信贷银承协议校验及协议信息返回接口 </br>
     * 接口说明：信贷根据客户号+银承协议编号先校验当前协议剩余额度是否足额，不足额时拦截并提示"该笔合同额度已被占用完,不能出账!";足额时返回协议信息给票据系统 </br>
     * 业务逻辑：</br>
     * 1.请求参数【银承协议编号accpContNo】【客户号cusId】非空检查</br>
     * 2.根据【银承协议编号accpContNo】+【协议状态cont_state='200'】查询是否存在有效的银承协议，不存在拦截并提示"根据电票系统银承协议编号："+cont_no+ "信贷系统没有获取到生效的银承协议!"</br>
     * 3.根据【银承协议编号accpContNo】+【协议状态cont_state='200'】+【客户号cusId】查询是否存在有效的银承协议，不存在拦截并提示"根据电票系统银承协议编号："+cont_no+"和客户号："+cus_id+"信贷系统没有获取到生效的银承协议!"</br>
     * 1).获取当前协议下用信余额:loan_balance=loan_balance1-bzj1+loan_balance2+loan_balance3-bzj2 </br>
     * A).汇总有效银承台账敞口金额:SELECT SUM(accp_amount-security_money_amt) as loan_balance1 FROM ACC_ACCP WHERE CONT_NO='"+CONT_NO+"' AND ACCOUNT_STATUS='1' </br>
     * B).有效台账下当天补交保证金总额：select SUM(security_money_amt) bzj1 from REGISTER_SECURITY_INFO where cont_no in (select hx_cont_no from acc_accp where account_status='1' and cont_no='"+cont_no+"') and cont_state ='0' and register_date='"+strCurDate+"' </br>
     * C).出账申请审批中的敞口总额：select SUM(apply_amount-security_money_amt) loan_balance2 from pvp_accp_app where cont_no='"+cont_no+"' and approve_status in('000','001','111') </br>
     * D).出账审批通过尚未生成银承台账   </br>
     * 敞口总额：select SUM(p.apply_amount-p.security_money_amt) loan_balance3 from pvp_accp_app p left join pvp_authorize a on p.serno=a.serno where p.cont_no='"+cont_no+"'and p.approve_status='997' and a.host_serno not in (select distinct hx_cont_no from acc_accp where cont_no='"+cont_no+"') </br>
     * 已缴存保证金总额：select sum(security_money_amt) bzj2 from REGISTER_SECURITY_INFO where cont_state ='0' and cont_no in (select a.host_serno from pvp_accp_app p left join pvp_authorize a on p.serno=a.serno where p.cont_no='"+cont_no+"'and p.approve_status='997' and a.host_serno not in (select distinct hx_cont_no from acc_accp where cont_no='"+cont_no+"')) </br>
     * 2).银承协议余额是否足额校验：Ctr_Accp_Cont.cont_amt<=loan_balance拦截并提示"该笔合同额度已被占用完,不能出账!" </br>
     * 3).授信余额是否足额校验（当前合同使用授信时）：调用额度系统额度校验接口 </br>
     * 4).上述银承协议余额、授信余额校验通过，组装返回报文返回票据系统 </br>
     * 备注：老信贷代码GetAccpContInfoAcction.java </br>
     * 20210608 坤经理补充业务逻辑： </br>
     * 1.REGISTER_SECURITY_INFO保证金缴存登记簿，外围系统保证金缴存通知信贷，信贷登记保证金信息 </br>
     * 2.新信贷pvp_accp_app.app_amt=老信贷pvp_accp_app.apply_amount </br>
     * 3.新信贷pvp_accp_app.bail_amt=老信贷pvp_accp_app.security_money_amt </br>
     * 4.新信贷pvp_accp_app.pc_serno、pvp_authorize.host_serno、acc_accp.hx_cont_no缺失，此字段是票据系统推送的批次号，需要同步增加  </br>
     * 5.新信贷pvp_accp_app.pvp_serno=pvp_authorize.pvp_serno=acc_accp.pvp_serno </br>
     *
     * @param xdht0002DataReqDto
     * @return xdht0002DataRespDto
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0002DataRespDto xdht0002(Xdht0002DataReqDto xdht0002DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataReqDto));
        Xdht0002DataRespDto xdht0002DataRespDto = new Xdht0002DataRespDto();
        String contNo = xdht0002DataReqDto.getContNo();//银承协议编号
        String cusId = xdht0002DataReqDto.getCusId();//客户号
        String cnContNo = StringUtils.EMPTY;//中文合同号
        String drwrCusId = StringUtils.EMPTY;//出票人客户号
        String billNo = StringUtils.EMPTY;//借据号
        String drwrName = StringUtils.EMPTY;//出票人名称
        String contType = StringUtils.EMPTY;//合同类型
        String assureMeans = StringUtils.EMPTY;//担保方式
        String agrTotlAmt = StringUtils.EMPTY;//协议总金额
        String curType = StringUtils.EMPTY;//币种
        String contStartDate = StringUtils.EMPTY;//合同签订日期
        String contEndDate = StringUtils.EMPTY;//合同到期日期
        String contStatus = StringUtils.EMPTY;//合同状态
        BigDecimal aorgAgrLowSecurityRate = null;//银承协议最低保证金比例
        try {
            //根据银承协议编号存在生效的合同
            int num = 0;
            if (!StringUtils.isEmpty(xdht0002DataReqDto.getContNo())) {
                contNo = xdht0002DataReqDto.getContNo(); //银承协议编号
                num = ctrAccpContService.queryNumContByContNo(contNo);
            }
            // 2.根据【银承协议编号accpContNo】+【协议状态cont_state='200'】查询是否存在有效的银承协议，不存在拦截并提示"根据电票系统银承协议编号："+cont_no+ "信贷系统没有获取到生效的银承协议!"</br>
            if (num <= 0) {
                // 根据电票系统银承协议编号[{}]信贷系统没有获取到生效的银承协议
                String ecb010015Value = EcbEnum.ECB010015.value.replace("{}", contNo);
                // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, ecb010015Value);
                throw BizException.error(null, EcbEnum.ECB010015.key, ecb010015Value);
            }
            // 3.根据【银承协议编号accpContNo】+【协议状态cont_state='200'】+【客户号cusId】查询是否存在有效的银承协议，不存在拦截并提示"根据电票系统银承协议编号："+cont_no+"和客户号："+cus_id+"信贷系统没有获取到生效的银承协议!"</br>
            xdht0002DataRespDto = ctrAccpContService.queryContInfoByContNoAndCusId(xdht0002DataReqDto);
            String ecb010043Value = EcbEnum.ECB010043.value.replace("{}", contNo).replace("{}", xdht0002DataReqDto.getCusId());//// 根据电票系统银承协议编号[{}]和客户号[{}]信贷系统没有获取到生效的银承协议
            Optional.ofNullable(xdht0002DataRespDto).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010043.key, ecb010043Value));
            cnContNo = xdht0002DataRespDto.getCnContNo();//中文合同号
            drwrCusId = xdht0002DataRespDto.getDrwrCusId();//出票人客户号
            billNo = xdht0002DataRespDto.getBillNo();//借据号
            drwrName = xdht0002DataRespDto.getDrwrName();//出票人名称
            contType = Objects.equals(xdht0002DataRespDto.getContType(), "3") ? "2" : xdht0002DataRespDto.getContType();//合同类型 最高额授信协议传2 其余传合同原值
            assureMeans = xdht0002DataRespDto.getAssureMeans();//担保方式
            agrTotlAmt = xdht0002DataRespDto.getAgrTotlAmt();//协议总金额
            curType = xdht0002DataRespDto.getCurType();//币种
            contStartDate = xdht0002DataRespDto.getContStartDate();//合同签订日期
            contEndDate = xdht0002DataRespDto.getContEndDate();//合同到期日期
            contStatus = xdht0002DataRespDto.getContStatus();//合同状态
            aorgAgrLowSecurityRate = xdht0002DataRespDto.getAorgAgrLowSecurityRate();//银承协议最低保证金比例
            //  1).获取当前协议下用信余额:loan_balance=loan_balance1-bzj1+loan_balance2+loan_balance3-bzj2 </br>
            //  A).汇总有效银承台账敞口金额:SELECT SUM(accp_amount-security_money_amt) as loan_balance1 FROM ACC_ACCP WHERE CONT_NO='"+CONT_NO+"' AND ACCOUNT_STATUS='1' </br>
            Map accAccpMap = new HashMap<>();
            accAccpMap.put("contNo", contNo);//银承协议编号
            BigDecimal spacAmt = accAccpService.querySpacAmtByAccAccpMap(accAccpMap);
            //  B).有效台账下当天补交保证金总额：select SUM(security_money_amt) bzj1 from  REGISTER_SECURITY_INFO   where cont_no in (select hx_cont_no from acc_accp where account_status='1' and cont_no='"+cont_no+"') and cont_state ='0' and register_date='"+strCurDate+"' </br>
            // REGISTER_SECURITY_INFO 对应新信贷 BAIL_DEPOSIT_INFO
            Map bdiMap = new HashMap();
            bdiMap.put("contNo", contNo);
            bdiMap.put("depositInputDate", DateUtils.getCurrDateStr());// 缴存登记日期 ,获取系统当前日期
            BigDecimal dtbjBailAmt = bailDepositInfoService.queryDtbjBailAmtByBdiMap(bdiMap);
            // C).出账申请审批中的敞口总额：select SUM(apply_amount-security_money_amt) loan_balance2 from pvp_accp_app where cont_no='"+cont_no+"' and approve_status in('000','001','111') </br>
            Map pvpAccpAppMap = new HashMap<>();
            pvpAccpAppMap.put("contNo", contNo);//银承协议编号
            BigDecimal approvingSpacAmt = pvpAccpAppService.queryApprovingSpacAmtByMap(pvpAccpAppMap);//出账申请审批中的敞口总额
            // D).出账审批通过尚未生成银承台账   </br>
            // 敞口总额：select SUM(p.apply_amount-p.security_money_amt) loan_balance3 from pvp_accp_app p left join pvp_authorize a on p.serno=a.serno where p.cont_no='"+cont_no+"'and p.approve_status='997'
            //  and a.host_serno not in (select distinct hx_cont_no from acc_accp where cont_no='"+cont_no+"') </br>
            BigDecimal approvedSpacAmt = pvpAccpAppService.queryApprovedSpacAmtByMap(pvpAccpAppMap);// 出账审批通过尚未生成银承台账的敞口总额
            // 已缴存保证金总额：select sum(security_money_amt) bzj2 from REGISTER_SECURITY_INFO = where cont_state ='0' and cont_no in (select a.host_serno from pvp_accp_app p left join pvp_authorize a on p.serno=a.serno where
            // p.cont_no='"+cont_no+"'and p.approve_status='997' and a.host_serno not in (select distinct hx_cont_no from acc_accp where cont_no='"+cont_no+"')) </br>
            BigDecimal yjcBailAmt = bailDepositInfoService.queryYjcBailAmtByBdiMap(bdiMap);//已缴存保证金总额
            // 当前协议下用信余额:loan_balance=loan_balance1-bzj1+loan_balance2+loan_balance3-bzj2
            BigDecimal loanBalance = spacAmt.subtract(dtbjBailAmt).add(approvingSpacAmt).add(approvedSpacAmt).subtract(yjcBailAmt);
            agrTotlAmt = xdht0002DataRespDto.getAgrTotlAmt();//协议总金额
            // 2).银承协议余额是否足额校验：Ctr_Accp_Cont.cont_amt<=loan_balance拦截并提示"该笔合同额度已被占用完,不能出账!" </br>
            if (loanBalance.compareTo(NumberUtils.toBigDecimal(agrTotlAmt)) > 0) {
                throw BizException.error(null, EcbEnum.ECB010053.key, EcbEnum.ECB010053.value);//该笔合同额度已被占用完,不能出账!
            }
            // 3).授信余额是否足额校验（当前合同使用授信时）：调用额度系统额度校验接口 </br>

            Map queryLmtAccNoMap = new HashMap();
            queryLmtAccNoMap.put("contNo", contNo);
            // 根据合同编号、合同状态查询 授信台账编号
            // String subSerno = ctrAccpContService.queryLmtAccNo(queryLmtAccNoMap);
            List<CmisLmt0010ReqDealBizListDto> dealBizList = ctrAccpContMapper.getDealBizList(contNo);
            CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(dealBizList);
            // 系统编号
            cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            // 金融机构代码
            cmisLmt0010ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            // 合同编号
            cmisLmt0010ReqDto.setBizNo(contNo);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010ReqDto));
            ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010RespDtoResultDto));
            if (Objects.equals(cmisLmt0010RespDtoResultDto.getCode(), EpbEnum.EPB099999.key)) {
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, EcbEnum.ECB010053.value);
                throw BizException.error(null, EcbEnum.ECB010053.key, EcbEnum.ECB010053.value);
            } else {
                // 4).上述银承协议余额、授信余额校验通过，组装返回报文返回票据系统 </br>
                xdht0002DataRespDto.setContNo(contNo);//银承协议编号
                xdht0002DataRespDto.setCnContNo(cnContNo);//中文合同号
                xdht0002DataRespDto.setDrwrCusId(drwrCusId);//出票人客户号
                xdht0002DataRespDto.setBillNo(billNo);//借据号
                xdht0002DataRespDto.setDrwrName(drwrName);//出票人名称
                xdht0002DataRespDto.setContType(contType);//合同类型
                xdht0002DataRespDto.setAssureMeans(assureMeans);//担保方式
                xdht0002DataRespDto.setAgrTotlAmt(agrTotlAmt); //协议总金额
                xdht0002DataRespDto.setCurType(curType);//币种
                xdht0002DataRespDto.setContStartDate(contStartDate);//合同签订日期
                xdht0002DataRespDto.setContEndDate(contEndDate); //合同到期日期
                xdht0002DataRespDto.setContStatus(contStatus); //合同状态
                xdht0002DataRespDto.setAorgAgrLowSecurityRate(aorgAgrLowSecurityRate);//银承协议最低保证金比例
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataRespDto));
        return xdht0002DataRespDto;
    }

}
