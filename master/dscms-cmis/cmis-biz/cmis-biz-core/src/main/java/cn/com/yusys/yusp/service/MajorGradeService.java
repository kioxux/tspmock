package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.MajorGrade;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.req.Irs19ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.resp.Irs19RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.repository.mapper.MajorGradeMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 20:06:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class MajorGradeService {

    private static final Logger log = LoggerFactory.getLogger(MajorGradeService.class);

    @Autowired
    private MajorGradeMapper majorGradeMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public MajorGrade selectByPrimaryKey(String pkId) {
        return majorGradeMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<MajorGrade> selectAll(QueryModel model) {
        List<MajorGrade> records = (List<MajorGrade>) majorGradeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<MajorGrade> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<MajorGrade> list = majorGradeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(MajorGrade record) {
        return majorGradeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(MajorGrade record) {
        return majorGradeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(MajorGrade record) {
        return majorGradeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(MajorGrade record) {
        return majorGradeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return majorGradeMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return majorGradeMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int checkHasPic(@RequestBody MajorGrade majorGrade) {
        return majorGradeMapper.checkHasPic(majorGrade);
    }

    /**
     * 根据申请流水号查询是否存在多笔
     *
     * @param serno
     * @return
     */
    public int getSumFromLmtAppDetails(String serno) {
        return majorGradeMapper.getSumFromLmtAppDetails(serno);
    }


    /**
     * 查看票据池申请是否有额度(如果票据池有额度的可以不用分项)
     *
     * @param serno
     * @return
     */
    public String getSumPjcFromLmtAppl(String serno) {
        return majorGradeMapper.getSumPjcFromLmtAppl(serno);
    }

    /**
     * 根据申请流水号查询是否存在
     *
     * @param lmtSerno
     * @return
     */
    public MajorGrade selectByLmtSerno(String lmtSerno) {
        return majorGradeMapper.selectByLmtSerno(lmtSerno);
    }

    /**
     * 发送接口
     *
     * @param majorGrade
     * @return
     */
    public int sendIrs19(MajorGrade majorGrade) {
        log.info("发送评级贷款信息："+majorGrade.getLmtSerno());
        /**
         * 点击“专业贷款信息”界面中的保存按钮后，对该笔授信的“专业贷款类型”进行判断，规则如下：
         1、项目融资专业贷款判断标准：
         问题2.1选择A
         问题3.1选择D和E以外的选项
         问题5选择D以外的选项
         若满足以上条件，专业贷款类型为“项目融资专业贷款”。
         2、房地产专业贷款判断标准：
         问题2.2选择B
         问题3.2选择E和F以外的选项
         问题5选择D以外的选项
         若满足以上条件，专业贷款类型为“房地产融资专业贷款”。
         3、若不满足1、2判断标准，则专业贷款类型为“非专业贷款”。
         */

        //String  loan_purp= majorGrade.getLoanPurp();//贷款用途
        //String  rz_repay_way= majorGrade.getRzRepayWay();//项目融资还款来源主要依赖于
        //String  fdz_repay_way=majorGrade.getFdcRepayWay();//房地产融资还款来源主要依赖于
        //String  bank_contrl_rz=majorGrade.getBankContrlRz();//银行对融资形成的资产及其所产生收入的控制权情况
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(majorGrade.getCusId());
        if("2".equals(cusBaseClientDto.getCusCatalog())){
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(majorGrade.getCusId()).getData();
            LmtApp lmtApp = lmtAppService.selectBySerno(majorGrade.getLmtSerno());
            ResultDto<AdminSmUserDto> adminSmUserDto = adminSmUserService.getByLoginCode(lmtApp.getManagerId());
            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(lmtApp.getManagerBrId());
            Irs19ReqDto irs19ReqDto = new Irs19ReqDto();
            irs19ReqDto.setSxserialno(majorGrade.getLmtSerno());
            irs19ReqDto.setCustid(majorGrade.getCusId());
            irs19ReqDto.setCustname(majorGrade.getCusName());
            if(cusBaseClientDto.getCertType().equals("A")){
                irs19ReqDto.setCerttype("10");
            }else if(cusBaseClientDto.getCertType().equals("B")){
                irs19ReqDto.setCerttype("12");
            }else if(cusBaseClientDto.getCertType().equals("C")){
                irs19ReqDto.setCerttype("11");
            }else if(cusBaseClientDto.getCertType().equals("D")){
                irs19ReqDto.setCerttype("15");
            }else if(cusBaseClientDto.getCertType().equals("E")){
                irs19ReqDto.setCerttype("16");
            }else if(cusBaseClientDto.getCertType().equals("G")){
                irs19ReqDto.setCerttype("13");
            }else if(cusBaseClientDto.getCertType().equals("H")){
                irs19ReqDto.setCerttype("14");
            }else if(cusBaseClientDto.getCertType().equals("Q")){
                irs19ReqDto.setCerttype("20");
            }else if(cusBaseClientDto.getCertType().equals("S")){
                irs19ReqDto.setCerttype("18");
            }else if(cusBaseClientDto.getCertType().equals("V")){
                irs19ReqDto.setCerttype("22");
            }else if(cusBaseClientDto.getCertType().equals("X")){
                irs19ReqDto.setCerttype("17");
            }else if(cusBaseClientDto.getCertType().equals("Y")){
                irs19ReqDto.setCerttype("19");
            }else {
                irs19ReqDto.setCerttype("1X");
            }
            irs19ReqDto.setCertid(cusBaseClientDto.getCertCode());
            // TODO 待确定客户模块使用字段是 REGI_AREA_CODE 还是 GEGI_ADDR
            irs19ReqDto.setCantoncode(cusCorpDto.getRegiAddr());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            irs19ReqDto.setSxbegdate(lmtApp.getInputDate().substring(0, 10));
            irs19ReqDto.setSxenddate(simpleDateFormat.format(DateUtils.addMonth(DateUtils.parseDateByDef(lmtApp.getInputDate()), Integer.valueOf(lmtApp.getLmtTerm()))));
            irs19ReqDto.setCreadittype(majorGrade.getMajorLoanType());
            BigDecimal count = lmtApp.getLowRiskTotalLmtAmt().add(lmtApp.getOpenTotalLmtAmt());
            irs19ReqDto.setCreaditsum(count);
            irs19ReqDto.setUserid1(lmtApp.getManagerId());
            irs19ReqDto.setUsername(adminSmUserDto.getData().getUserName());
            irs19ReqDto.setOrgid(lmtApp.getManagerBrId());
            irs19ReqDto.setOrgname(adminSmOrgDtoResultDto.getData().getOrgName());
            Irs19RespDto irs19RespDto = dscms2IrsClientService.irs19(irs19ReqDto).getData();
        }
        return 0;
    }
}
