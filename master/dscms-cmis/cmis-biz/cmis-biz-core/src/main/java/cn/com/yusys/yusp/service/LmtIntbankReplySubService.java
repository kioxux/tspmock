/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankReplySubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplySubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-25 20:55:15
 * @创建时间: 2021-05-27 10:51:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankReplySubService extends BizInvestCommonService{

    @Autowired
    private LmtIntbankReplySubMapper lmtIntbankReplySubMapper;

    @Autowired
    private LmtIntbankAppSubService lmtIntbankAppSubService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService ;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankReplySub selectByPrimaryKey(String pkId) {
        return lmtIntbankReplySubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankReplySub> selectAll(QueryModel model) {
        List<LmtIntbankReplySub> records = (List<LmtIntbankReplySub>) lmtIntbankReplySubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankReplySub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankReplySub> list = lmtIntbankReplySubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankReplySub record) {
        return lmtIntbankReplySubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankReplySub record) {
        return lmtIntbankReplySubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankReplySub record) {
        return lmtIntbankReplySubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankReplySub record) {
        return lmtIntbankReplySubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankReplySubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankReplySubMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: initLmtIntbankAccSubInfo
     * @方法描述: 根据同业授信审批分项表 或 同业授信申请表产生 台账分项信息
     * @参数与返回说明: lmtIntbankAccSub
     * @算法描述: 无
     */

    public LmtIntbankReplySub initLmtIntbankReplySubInfo(LmtIntbankApprSub subBean, LmtIntbankReply lmtIntbankReply) {
        //生成主键
        Map paramMap = new HashMap<>();
        String pkValue = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, paramMap);
        //批复分项编号
        String replySubSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.INTBANK_LMT_REPLY_SUB_SEQ, paramMap);
        //同业授信申请表，期限，起始日，到期日等字段需要重新赋值
        Integer lmtTerm = ((LmtIntbankApprSub) subBean).getLmtTerm();
        if(lmtTerm==null) lmtTerm=0 ;
        //创建批复分项对象
        LmtIntbankReplySub lmtIntbankReplySub = new LmtIntbankReplySub();
        BeanUtils.copyProperties(subBean, lmtIntbankReplySub);
        //设置主键
        lmtIntbankReplySub.setPkId(pkValue);
        //设置批复编号
        lmtIntbankReplySub.setReplySerno(lmtIntbankReply.getReplySerno());
        //分项编号
        lmtIntbankReplySub.setReplySubSerno(replySubSerno);
        //原分项编号，原金额等处理
        lmtIntbankReplySub.setOrigiLmtSubNo(subBean.getOrigiLmtAccSubNo());
        lmtIntbankReplySub.setOrigiLmtSubAmt(subBean.getOrigiLmtAccSubAmt());
        lmtIntbankReplySub.setOrigiLmtSubTerm(subBean.getOrigiLmtAccSubTerm());
        //起始日期
        String startDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        //到期日
        String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd", lmtTerm);
        //期限
        lmtIntbankReplySub.setTerm(lmtTerm);
        //起始日期
        lmtIntbankReplySub.setStartDate(startDate);
        //到日期
        lmtIntbankReplySub.setEndDate(endDate);
        //登记日期
        lmtIntbankReplySub.setInputDate(getCurrrentDateStr());
        //最新更新日期
        lmtIntbankReplySub.setUpdDate(getCurrrentDateStr());
        //创建日期
        lmtIntbankReplySub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtIntbankReplySub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //操作类型
        //lmtIntbankReplySub.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        return lmtIntbankReplySub;
    }
    /**
     * @方法名称: copyToLmtAppSub
     * @方法描述: 将原先的授信批复分项及对应的适用产品明细要挂载在新的授信流水号下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyToLmtAppSub(String originSerno, String curSerno,String approveStatus) {
        HashMap<String, String> params = new HashMap();
        String subSerno = "";
        String originSubSerno = "";
        LmtIntbankAppSub lmtIntbankAppSub = new LmtIntbankAppSub();
        LmtIntbankReplySub lmtIntbankReplySub = new LmtIntbankReplySub();
        params.put("replySerno", originSerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",originSerno);
        params.put("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtIntbankReplySub> subList = lmtIntbankReplySubMapper.queryLmtIntbankReplySubByParams(params);
        User userInfo = SessionUtils.getUserInformation();
        if (subList.size() > 0) {
            for (int i = 0; i < subList.size(); i++) {
                lmtIntbankReplySub = subList.get(i);
                // originSubSerno = lmtIntbankReplySub.getReplySubSerno();
                subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                BeanUtils.copyProperties(lmtIntbankReplySub, lmtIntbankAppSub);
                lmtIntbankAppSub.setPkId(UUID.randomUUID().toString());
                lmtIntbankAppSub.setSerno(curSerno);
                lmtIntbankAppSub.setSubSerno(subSerno);

                lmtIntbankAppSub.setOrigiLmtAccSubNo(lmtIntbankReplySub.getOrigiLmtSubNo());
                lmtIntbankAppSub.setOrigiLmtAccSubTerm(lmtIntbankReplySub.getOrigiLmtSubTerm());
                lmtIntbankAppSub.setOrigiLmtAccSubAmt(lmtIntbankReplySub.getOrigiLmtSubAmt());

                lmtIntbankAppSub.setInputId(userInfo.getLoginCode());
                lmtIntbankAppSub.setInputBrId(userInfo.getOrg().getCode());
                lmtIntbankAppSub.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                lmtIntbankAppSub.setCreateTime(getCurrrentDate());

                lmtIntbankAppSub.setUpdId(userInfo.getLoginCode());
                lmtIntbankAppSub.setUpdBrId(userInfo.getOrg().getCode());
                lmtIntbankAppSub.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
                lmtIntbankAppSub.setUpdateTime(getCurrrentDate());

                if (!CmisCommonConstants.WF_STATUS_998.equals(approveStatus)){
                    //否决的不需要设置原额度台账分项编号等字段
                    String replySubSerno = lmtIntbankReplySub.getReplySubSerno() ;
                    //根据批复流水号，获取分项批复对应的分项台账
                    LmtIntbankAccSub lmtIntbankAccSub = lmtIntbankAccSubService.selectByReplySubSerno(replySubSerno) ;
                    if (lmtIntbankAccSub != null) {
                        lmtIntbankAppSub.setOrigiLmtAccSubNo(lmtIntbankAccSub.getAccSubNo());
                        lmtIntbankAppSub.setOrigiLmtAccSubTerm(lmtIntbankAccSub.getLmtTerm());
                        lmtIntbankAppSub.setOrigiLmtAccSubAmt(lmtIntbankAccSub.getLmtAmt());
                    }
                }
                int count = lmtIntbankAppSubService.insert(lmtIntbankAppSub);
                // copyGuarRel = lmtAppGuarRelService.copyLmtAppGuarRel(originSubSerno,subSerno);
                if (count != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw BizException.error(null, EpbEnum.EPB099999.key,EpbEnum.EPB099999.value);
                }
            }
        }
        return true;
    }

    /**
     * 根据批复分项流水号查询
     * @param replySubSerno
     * @return
     */
    public LmtIntbankReplySub selectByReplySubSerno(String replySubSerno){
        return lmtIntbankReplySubMapper.selectByReplySubSerno(replySubSerno);

    }

    /**
     * 批复变更-生成批复信息
     * @param lmtIntbankReplyChgSub
     * @param serno
     * @param newReplySerno
     * @param currentOrgId
     * @param currentUserId
     */
    public void insertReplySub(LmtIntbankReplyChgSub lmtIntbankReplyChgSub, String serno, String newReplySerno, String currentOrgId, String currentUserId) {
        LmtIntbankReplySub lmtIntbankReplySub = new LmtIntbankReplySub();
        copyProperties(lmtIntbankReplyChgSub,lmtIntbankReplySub);
        //使用原申请流水号
        lmtIntbankReplySub.setSerno(serno);
        //使用新的批复流水号
        lmtIntbankReplySub.setReplySerno(newReplySerno);
        //初始化新增属性
        initInsertDomainProperties(lmtIntbankReplySub,currentUserId,currentOrgId);
        insert(lmtIntbankReplySub);
    }
}
