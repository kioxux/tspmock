/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditInfo
 * @类描述: iqp_cus_credit_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 22:08:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_cus_credit_info")
public class IqpCusCreditInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 与借款人关系 **/
	@Column(name = "DEBIT_RELA", unique = false, nullable = true, length = 5)
	private String debitRela;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 现有消费类融资余额(万元) **/
	@Column(name = "CONSUME_FIN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal consumeFinBal;
	
	/** 现有消费类融资月还款额(万元) **/
	@Column(name = "CONSUME_MON_REPAY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal consumeMonRepay;
	
	/** 贷款当前逾期金额 **/
	@Column(name = "LOAN_CURT_OVERDUE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanCurtOverdueAmt;
	
	/** 贷款单月最高逾期金额(元) **/
	@Column(name = "LOAN_HIGH_OVERDUE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanHighOverdueAmt;
	
	/** 贷款最长逾期月数 **/
	@Column(name = "LOAN_LGST_OVERDUE_MON", unique = false, nullable = true, length = 10)
	private String loanLgstOverdueMon;
	
	/** 贷记卡当前逾期金额 **/
	@Column(name = "DEBIT_CURT_OVERDUE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debitCurtOverdueAmt;
	
	/** 贷记卡单月最高逾期金额(元) **/
	@Column(name = "DEBIT_HIGH_OVERDUE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debitHighOverdueAmt;
	
	/** 贷记卡最长逾期月数 **/
	@Column(name = "DEBIT_LGST_OVERDUE_MON", unique = false, nullable = true, length = 10)
	private String debitLgstOverdueMon;
	
	/** 两年内逾期次数 **/
	@Column(name = "IN_TWO_OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private String inTwoOverdueTimes;
	
	/** 两年外逾期次数 **/
	@Column(name = "OUT_TWO_OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private String outTwoOverdueTimes;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param debitRela
	 */
	public void setDebitRela(String debitRela) {
		this.debitRela = debitRela;
	}
	
    /**
     * @return debitRela
     */
	public String getDebitRela() {
		return this.debitRela;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param consumeFinBal
	 */
	public void setConsumeFinBal(java.math.BigDecimal consumeFinBal) {
		this.consumeFinBal = consumeFinBal;
	}
	
    /**
     * @return consumeFinBal
     */
	public java.math.BigDecimal getConsumeFinBal() {
		return this.consumeFinBal;
	}
	
	/**
	 * @param consumeMonRepay
	 */
	public void setConsumeMonRepay(java.math.BigDecimal consumeMonRepay) {
		this.consumeMonRepay = consumeMonRepay;
	}
	
    /**
     * @return consumeMonRepay
     */
	public java.math.BigDecimal getConsumeMonRepay() {
		return this.consumeMonRepay;
	}
	
	/**
	 * @param loanCurtOverdueAmt
	 */
	public void setLoanCurtOverdueAmt(java.math.BigDecimal loanCurtOverdueAmt) {
		this.loanCurtOverdueAmt = loanCurtOverdueAmt;
	}
	
    /**
     * @return loanCurtOverdueAmt
     */
	public java.math.BigDecimal getLoanCurtOverdueAmt() {
		return this.loanCurtOverdueAmt;
	}
	
	/**
	 * @param loanHighOverdueAmt
	 */
	public void setLoanHighOverdueAmt(java.math.BigDecimal loanHighOverdueAmt) {
		this.loanHighOverdueAmt = loanHighOverdueAmt;
	}
	
    /**
     * @return loanHighOverdueAmt
     */
	public java.math.BigDecimal getLoanHighOverdueAmt() {
		return this.loanHighOverdueAmt;
	}
	
	/**
	 * @param loanLgstOverdueMon
	 */
	public void setLoanLgstOverdueMon(String loanLgstOverdueMon) {
		this.loanLgstOverdueMon = loanLgstOverdueMon;
	}
	
    /**
     * @return loanLgstOverdueMon
     */
	public String getLoanLgstOverdueMon() {
		return this.loanLgstOverdueMon;
	}
	
	/**
	 * @param debitCurtOverdueAmt
	 */
	public void setDebitCurtOverdueAmt(java.math.BigDecimal debitCurtOverdueAmt) {
		this.debitCurtOverdueAmt = debitCurtOverdueAmt;
	}
	
    /**
     * @return debitCurtOverdueAmt
     */
	public java.math.BigDecimal getDebitCurtOverdueAmt() {
		return this.debitCurtOverdueAmt;
	}
	
	/**
	 * @param debitHighOverdueAmt
	 */
	public void setDebitHighOverdueAmt(java.math.BigDecimal debitHighOverdueAmt) {
		this.debitHighOverdueAmt = debitHighOverdueAmt;
	}
	
    /**
     * @return debitHighOverdueAmt
     */
	public java.math.BigDecimal getDebitHighOverdueAmt() {
		return this.debitHighOverdueAmt;
	}
	
	/**
	 * @param debitLgstOverdueMon
	 */
	public void setDebitLgstOverdueMon(String debitLgstOverdueMon) {
		this.debitLgstOverdueMon = debitLgstOverdueMon;
	}
	
    /**
     * @return debitLgstOverdueMon
     */
	public String getDebitLgstOverdueMon() {
		return this.debitLgstOverdueMon;
	}
	
	/**
	 * @param inTwoOverdueTimes
	 */
	public void setInTwoOverdueTimes(String inTwoOverdueTimes) {
		this.inTwoOverdueTimes = inTwoOverdueTimes;
	}
	
    /**
     * @return inTwoOverdueTimes
     */
	public String getInTwoOverdueTimes() {
		return this.inTwoOverdueTimes;
	}
	
	/**
	 * @param outTwoOverdueTimes
	 */
	public void setOutTwoOverdueTimes(String outTwoOverdueTimes) {
		this.outTwoOverdueTimes = outTwoOverdueTimes;
	}
	
    /**
     * @return outTwoOverdueTimes
     */
	public String getOutTwoOverdueTimes() {
		return this.outTwoOverdueTimes;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


}