package cn.com.yusys.yusp.dto.resp;

import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class GrtGuarContBySernoDto implements Serializable {

    private GrtGuarCont grtGuarCont;

    private List<GuarGuarantee> guarGuarantee;   //保证人

    //业务流水号
    String serno;

    //合同编号
    String contNo;

    public GrtGuarCont getGrtGuarCont() {
        return grtGuarCont;
    }

    public void setGrtGuarCont(GrtGuarCont grtGuarCont) {
        this.grtGuarCont = grtGuarCont;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public List<GuarGuarantee> getGuarGuarantee() {
        return guarGuarantee;
    }

    public void setGuarGuarantee(List<GuarGuarantee> guarGuarantee) {
        this.guarGuarantee = guarGuarantee;
    }

}
