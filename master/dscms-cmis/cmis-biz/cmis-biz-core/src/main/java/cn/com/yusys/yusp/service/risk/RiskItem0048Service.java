package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.Dscms2ImageClientService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.DocFlavor;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021年7月28日16:04:21
 * @desc 影像是否已扫描校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0048Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0048Service.class);

    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;


    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021年7月28日16:04:21
     * @version 1.0.0
     * @desc    影像是否已扫描校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0048(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        log.info("风险拦截0048载入参数：【{}】", JSON.toJSONString(queryModel));
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        String topOutsystemCode = StringUtils.EMPTY;
        if ("XW009".equals(bizType)) {
            // 小微--增享贷
            topOutsystemCode = "JYD1";
            //校验是否完成影像
            ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
            imageDataReqDto.setDocId(iqpSerno);
            imageDataReqDto.setOutSystemCode(topOutsystemCode);
            log.info("影像发送参数：【{}】", JSON.toJSONString(imageDataReqDto));
            ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
            log.info("影像返回参数：【{}】", JSON.toJSONString(map));
            if (map.getData() == null || map.getData().size() == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("未获取到影像信息"); //未获取到影像信息
                return riskResultDto;
            } else {
                // 对影像中“基本信息”和“经营权属”两类进行必传校验，基本信息>=2张，经营权属>=3张；
                Map<String, Integer> tempMap = map.getData();
                BigDecimal zl = new BigDecimal(0);
                BigDecimal yqs = new BigDecimal(0);
                if (tempMap.containsKey("JYD1JKRJCZL")) {
                    zl = new BigDecimal(tempMap.get("JYD1JKRJCZL"));
                }
                if (tempMap.containsKey("JYD1JKRJYQS")) {
                    yqs = new BigDecimal(tempMap.get("JYD1JKRJYQS"));
                }
                if (zl.compareTo(new BigDecimal(2)) < 0 || yqs.compareTo(new BigDecimal(3)) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc("基本信息必须>=2张，经营权属必须>=3张！");
                    return riskResultDto;
                }
            }
        } else if ("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
            //校验是否完成影像
            if ("022028".equals(iqpLoanApp.getPrdId())) {
                topOutsystemCode = "GRXFDKSX;XD_FZHYXCL";
            } else {
                topOutsystemCode = "GRXFDKSX";
            }
            String[] topOutsystem = topOutsystemCode.split(";");
            boolean flg = false;
            for (String topOut : topOutsystem) {
                ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
                imageDataReqDto.setDocId(iqpSerno);
                imageDataReqDto.setOutSystemCode(topOut);
                ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
                log.info("*************获取影像系统返回报文***********【{}】", JSON.toJSONString(map));
                if (map.getData() != null && map.getData().size() > 0) {
                    flg = true;
                    break;
                }
            }
            if (!flg) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("未获取到影像信息"); //未获取到影像信息
                return riskResultDto;
            }
        } else {
            Map index = new HashMap();
            Map param = (LinkedHashMap) queryModel.getCondition().get("param");
            if (CollectionUtils.isEmpty(param)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
                return riskResultDto;
            }
            if (StringUtils.isBlank(iqpSerno)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
                return riskResultDto;
            }
            // 影像目录编号
            if (param.containsKey("topOutsystemCode")) {
                topOutsystemCode = param.get("topOutsystemCode").toString();
            }
            // 获取影像参数index
            if (param.containsKey("index")) {
                index = (Map) param.get("index");

            }
            log.info("*************影像是否已扫描校验***********【{}】", iqpSerno);
            //校验是否完成影像
            ImageDataReqDto imageDataReqDto = new ImageDataReqDto();

            imageDataReqDto.setDocId(iqpSerno);
            imageDataReqDto.setOutSystemCode(topOutsystemCode);
            imageDataReqDto.setIndexMap(index);
            ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
            if (map.getData() == null || map.getData().size() == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("未获取到影像信息"); //未获取到影像信息
                return riskResultDto;
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************影像是否已扫描校验***********【{}】", iqpSerno);
        return riskResultDto;
    }
}
