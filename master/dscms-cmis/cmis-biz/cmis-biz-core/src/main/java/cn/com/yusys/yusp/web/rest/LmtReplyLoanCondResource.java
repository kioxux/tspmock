/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyLoanCond;
import cn.com.yusys.yusp.service.LmtReplyLoanCondService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyLoanCondResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyloancond")
public class LmtReplyLoanCondResource {
    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyLoanCond>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyLoanCond> list = lmtReplyLoanCondService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyLoanCond>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyLoanCond>> index(QueryModel queryModel) {
        List<LmtReplyLoanCond> list = lmtReplyLoanCondService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyLoanCond>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyLoanCond> show(@PathVariable("pkId") String pkId) {
        LmtReplyLoanCond lmtReplyLoanCond = lmtReplyLoanCondService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyLoanCond>(lmtReplyLoanCond);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyLoanCond> create(@RequestBody LmtReplyLoanCond lmtReplyLoanCond) throws URISyntaxException {
        lmtReplyLoanCondService.insert(lmtReplyLoanCond);
        return new ResultDto<LmtReplyLoanCond>(lmtReplyLoanCond);
    }

    /**
     * @方法名称：createBySelective
     * @方法描述：只插入费控字段
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-25 下午 2:48
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/createBySelective")
    protected ResultDto<Integer> createBySelective(@RequestBody LmtReplyLoanCond lmtReplyLoanCond) throws URISyntaxException {
         int result = lmtReplyLoanCondService.insertSelective(lmtReplyLoanCond);
        return new ResultDto<Integer>(result);
    }

    /**
       * @方法名称：update
       * @方法描述：只更新费控字段
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-26 上午 10:51
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyLoanCond lmtReplyLoanCond) throws URISyntaxException {
        int result = lmtReplyLoanCondService.updateSelective(lmtReplyLoanCond);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyLoanCondService.deleteByPrimaryKey(pkId);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyLoanCondService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:generateImgCondDetailsData
     * @函数描述:查询授信批复的用信条件落实数据，并添加到用信条件落实情况表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/generateimgconddetailsdata")
    protected ResultDto<List<LmtReplyLoanCond>> generateImgCondDetailsData(@RequestBody Map map) {
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByReplySerno((String) map.get("replySerno"));
        return new ResultDto<List<LmtReplyLoanCond>>(lmtReplyLoanCondList);
    }
}
