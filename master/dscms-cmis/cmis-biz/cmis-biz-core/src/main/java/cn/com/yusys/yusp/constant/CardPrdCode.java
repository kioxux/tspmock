package cn.com.yusys.yusp.constant;

/**
 * 业务流程常量类
 */
public class CardPrdCode {
    /**普卡**/
    public final static String CARD_PRD_CODE_PK = new String("000001") ;
    /**金卡**/
    public final static String  CARD_PRD_CODE_JK = new String("000002") ;
    /**白金卡**/
    public final static String  CARD_PRD_CODE_BJK =  new String("000003");
    /**520主题卡**/
    public final static String  CARD_PRD_CODE_520 = new String("000006") ;
    /**村务卡普卡**/
    public final static String  CARD_PRD_CODE_CWKPK =  new String("000007");
    /**村务卡金卡**/
    public final static String  CARD_PRD_CODE_CWKJK = new String("000008") ;
    /**乐悠金卡**/
    public final static String  CARD_PRD_CODE_LYJK = new String( "000011");
    /**电子信用卡**/
    public final static String  CARD_PRD_CODE_DZXYK = new String("000012");
    /**乐悠金电子卡**/
    public final static String  CARD_PRD_CODE_LYJDZK = new String("000013");
    /**随用金卡**/
    public final static String  CARD_PRD_CODE_SYJK = new String("000014");
    /**随用金电子卡**/
    public final static String  CARD_PRD_CODE_SYJDZK = new String("000015");
    /**京东联名卡**/
    public final static String  CARD_PRD_CODE_JDLMK = new String("000016");
}
