/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrAccpCont;
import cn.com.yusys.yusp.domain.CtrDiscCont;
import cn.com.yusys.yusp.domain.CtrTfLocCont;
import cn.com.yusys.yusp.service.CtrAccpContService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAccpContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-14 15:01:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "银承合同")
@RequestMapping("/api/ctraccpcont")
public class CtrAccpContResource {
    @Autowired
    private CtrAccpContService ctrAccpContService;

	/**
     * 全表查询.
     *
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrAccpCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrAccpCont> list = ctrAccpContService.selectAll(queryModel);
        return new ResultDto<List<CtrAccpCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrAccpCont>> index(QueryModel queryModel) {
        List<CtrAccpCont> list = ctrAccpContService.selectByModel(queryModel);
        return new ResultDto<List<CtrAccpCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrAccpCont> show(@PathVariable("pkId") String pkId) {
        CtrAccpCont ctrAccpCont = ctrAccpContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrAccpCont>(ctrAccpCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrAccpCont> create(@RequestBody CtrAccpCont ctrAccpCont) throws URISyntaxException {
        ctrAccpContService.insert(ctrAccpCont);
        return new ResultDto<CtrAccpCont>(ctrAccpCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrAccpCont ctrAccpCont) throws URISyntaxException {
        int result = ctrAccpContService.updateSelective(ctrAccpCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrAccpContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrAccpContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:待签定列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrAccpCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrAccpCont> list = ctrAccpContService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrAccpCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrAccpCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrAccpCont> list = ctrAccpContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrAccpCont>>(list);
    }

    /**
     * 合同签订保存方法
     * @param ctrAccpCont
     * @return
     */
    @ApiOperation("合同签订保存方法")
    @PostMapping("/onSign")
    protected ResultDto<Integer> onSign(@RequestBody CtrAccpCont ctrAccpCont) {
        Integer result   = ctrAccpContService.onSign(ctrAccpCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:onLogOut
     * @函数描述:银承贷款合同注销
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onlogout")
    protected ResultDto<Map> onLogOut(@RequestBody Map params)  {
        Map rtnData= ctrAccpContService.onLogOut(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrAccpCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrAccpCont> list = ctrAccpContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrAccpCont>>(list);
    }

    /**
     * @函数名称:queryCtrAccpContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctraccpcontdatabyserno")
    protected ResultDto<CtrAccpCont> queryCtrAccpContDataBySerno(@RequestBody String serno) {
        CtrAccpCont ctrAccpCont = ctrAccpContService.selectByIqpSerno(serno);
        return new ResultDto<CtrAccpCont>(ctrAccpCont);
    }

    /**
     * @函数名称:queryCtrTfLocContDataByContNo
     * @函数描述:根据合同号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据合同号查询合同信息")
    @PostMapping("/queryctraccpcontdatabycontno")
    protected ResultDto<CtrAccpCont> queryCtrAccpContDataByContNo(@RequestBody String contNo) {
        CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(contNo);
        return new ResultDto<CtrAccpCont>(ctrAccpCont);
    }
}
