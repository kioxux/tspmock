/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarLowRisk
 * @类描述: rpt_lmt_repay_anys_guar_low_risk数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-23 21:20:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_low_risk")
public class RptLmtRepayAnysGuarLowRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 申请企业 **/
	@Column(name = "APP_ENTER", unique = false, nullable = true, length = 80)
	private String appEnter;
	
	/** 贷款品种 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 融资金额 **/
	@Column(name = "CPTL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cptlAmt;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 担保分析 **/
	@Column(name = "GUAR_ANALY", unique = false, nullable = true, length = 40)
	private String guarAnaly;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appEnter
	 */
	public void setAppEnter(String appEnter) {
		this.appEnter = appEnter;
	}
	
    /**
     * @return appEnter
     */
	public String getAppEnter() {
		return this.appEnter;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param cptlAmt
	 */
	public void setCptlAmt(java.math.BigDecimal cptlAmt) {
		this.cptlAmt = cptlAmt;
	}
	
    /**
     * @return cptlAmt
     */
	public java.math.BigDecimal getCptlAmt() {
		return this.cptlAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param guarAnaly
	 */
	public void setGuarAnaly(String guarAnaly) {
		this.guarAnaly = guarAnaly;
	}
	
    /**
     * @return guarAnaly
     */
	public String getGuarAnaly() {
		return this.guarAnaly;
	}


}