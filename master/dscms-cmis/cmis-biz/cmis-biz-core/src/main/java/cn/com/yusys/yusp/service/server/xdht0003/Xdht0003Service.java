package cn.com.yusys.yusp.service.server.xdht0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0003.req.Xdht0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.List;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.Xdht0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.repository.mapper.CtrDiscContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0003Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0003Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0003Service.class);

    @Resource
    private CtrDiscContMapper ctrDiscContMapper;

    @Resource
    private AccDiscMapper accDiscMapper;

    /**
     * 信贷贴现合同号查询
     *
     * @param xdht0003DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0003DataRespDto xdht0003(Xdht0003DataReqDto xdht0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataReqDto));
        Xdht0003DataRespDto xdht0003DataRespDto = new Xdht0003DataRespDto();
        java.util.List<List> lists = new ArrayList<>();
        lists = ctrDiscContMapper.getXdht0003(xdht0003DataReqDto);
        try {
            Optional.ofNullable(lists).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value));
            for (List list:
                lists) {
                //协议金额
                BigDecimal agrAmt = list.getAgrAmt() == null ? BigDecimal.ZERO : list.getAgrAmt();
                //出账金额
                BigDecimal pvpAmt = accDiscMapper.getPvpAmt(list);
                list.setPvpAmt(pvpAmt);
                //最高额合同剩余额度
                BigDecimal highContSurplusLmt = agrAmt.subtract(pvpAmt);
                list.setHighContSurplusLmt(highContSurplusLmt);
            }
            xdht0003DataRespDto.setList(lists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataReqDto));
        return xdht0003DataRespDto;
    }
}
