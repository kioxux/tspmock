package cn.com.yusys.yusp.service.server.xddb0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.req.YpztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.YpztcxRespDto;
import cn.com.yusys.yusp.dto.server.xddb0006.req.Xddb0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0006.resp.Xddb0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.Dscms2YphsxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 业务逻辑处理类:信贷押品状态查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Service
@Transactional(rollbackFor = {BizException.class, Exception.class})
public class Xddb0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0006Service.class);

    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    /**
     * 对公智能风控 调用信贷，信贷去不动产平台
     * <p>
     * 根据区县代码和不动产权证书号、不动产登记证明号，到不动产登记中心去查询具体的不动产信息
     * 区县代码如果是国际，那根据押品类型去押品权证系统dblink查询具体的不动产信息
     * 区县代码如果不是国际，那发接口到全国不动产登记中心去查，（高新区、姑苏区、相城区）的区县代码需要做变换。
     *
     * @param xddb0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0006DataRespDto xddb0006(Xddb0006DataReqDto xddb0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value);
        Xddb0006DataRespDto xddb0006DataRespDto = new Xddb0006DataRespDto();
        try {
            String mocenu = xddb0006DataReqDto.getMocenu();
            String certnu = xddb0006DataReqDto.getCertnu();
            String distco = xddb0006DataReqDto.getCertnu();

            logger.info("**************信贷押品状态查询开始,不动产权登记证明号【{}】****************", xddb0006DataReqDto.getDistco());
            YpztcxReqDto ypztcxReqDto = new YpztcxReqDto();
            ypztcxReqDto.setDistco("GJ"); // 国结调用此接口
            ypztcxReqDto.setCertnu("1"); //不动产权证书号 调用接口国结时，老代码逻辑此字段未使用因校验必输，传值1
            ypztcxReqDto.setMocenu(mocenu); //不动产权登记证明号
            logger.info("信贷押品状态查询请求报文：【{}】", JSON.toJSONString(ypztcxReqDto));
            ResultDto<YpztcxRespDto> ypztcxReqResultDto = dscms2YphsxtClientService.ypztcx(ypztcxReqDto);
            logger.info("信贷押品状态查询返回报文：【{}】", JSON.toJSONString(ypztcxReqResultDto));
            String ypztcxCode = Optional.ofNullable(ypztcxReqResultDto.getCode()).orElse(StringUtils.EMPTY);
            // 接口返回信息
            //0 - 交易成功(查到未查封)
            //0001 - 抵押物编号：""+guar+"" 未查询到信息！
            //5555 - 抵押物编号：""+guarno+"" 存在查封情况！
            //0002 - 合同编号："+mocenu+" 未查询到信息！
            //0003 - 抵押物类型非不动产类型！
            //9999 - 通讯异常等信息
            if ("9999".equals(ypztcxCode)) {
                logger.info("**************信贷押品状态查询结束，存在已查封抵押物不通过，合同号【{}】****************", mocenu);
                xddb0006DataRespDto.setOpFlag("9999");
                xddb0006DataRespDto.setOpMsg("交易失败，与不动产登记中心通讯异常");
            } else if ("0001".equals(ypztcxCode) || "0002".equals(ypztcxCode)) {
                logger.info("**************信贷押品状态查询结束，存在已查封抵押物通过，合同号【{}】****************", mocenu);
                xddb0006DataRespDto.setOpFlag("0001");
                xddb0006DataRespDto.setOpMsg("未查询到信息");
            }else if("5555".equals(ypztcxCode)){
                xddb0006DataRespDto.setOpFlag("9999");
                xddb0006DataRespDto.setOpMsg("存在查封情况");
            }else{
                xddb0006DataRespDto.setOpFlag("0000");
                xddb0006DataRespDto.setOpMsg("交易成功(查到未查封)");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value);
        return xddb0006DataRespDto;
    }
}
