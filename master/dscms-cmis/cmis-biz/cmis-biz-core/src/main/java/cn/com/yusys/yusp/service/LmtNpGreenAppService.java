/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtNpGreenApp;
import cn.com.yusys.yusp.repository.mapper.LmtNpGreenAppMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtNpGreenAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-30 16:45:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtNpGreenAppService {

    @Autowired
    private LmtNpGreenAppMapper lmtNpGreenAppMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtNpGreenApp selectByPrimaryKey(String serno) {
        return lmtNpGreenAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtNpGreenApp> selectAll(QueryModel model) {
        List<LmtNpGreenApp> records = (List<LmtNpGreenApp>) lmtNpGreenAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtNpGreenApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtNpGreenApp> list = lmtNpGreenAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtNpGreenApp record) {
        return lmtNpGreenAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtNpGreenApp record) {
        return lmtNpGreenAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtNpGreenApp record) {
        return lmtNpGreenAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtNpGreenApp record) {
        return lmtNpGreenAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtNpGreenAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtNpGreenAppMapper.deleteByIds(ids);
    }

    /**
     * 获取内评低准入例外审批信息
     *
     * @param model
     * @return
     */
    public List<LmtNpGreenApp> getLmtGreenApp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("inputDate desc");
        PageHelper.clearPage();
        List<LmtNpGreenApp> list = selectByModel(model);
        return list;
    }

    /**
     * 获取内评低准入例外审批历史信息
     *
     * @param model
     * @return
     */
    public List<LmtNpGreenApp> getLmtGreenAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_993);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("inputDate desc");
        PageHelper.clearPage();
        List<LmtNpGreenApp> list = selectByModel(model);
        return list;
    }

    public Map insertNpGreenApp(LmtNpGreenApp lmtNpGreenApp) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            if (StringUtils.isBlank(lmtNpGreenApp.getSerno())) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            String serno = lmtNpGreenApp.getSerno();
            LmtNpGreenApp lmtNpGreenApp1 = selectByPrimaryKey(serno);
            if (Objects.nonNull(lmtNpGreenApp1) && CmisBizConstants.OPR_TYPE_01.equals(lmtNpGreenApp1.getOprType())) {
                rtnCode = EcbEnum.ECB020036.key;
                rtnMsg = EcbEnum.ECB020036.value;
            } else if (Objects.nonNull(lmtNpGreenApp1) && CmisBizConstants.OPR_TYPE_02.equals(lmtNpGreenApp1.getOprType())) {
                lmtNpGreenApp.setOprType(CmisBizConstants.OPR_TYPE_01);
                int count =   updateSelective(lmtNpGreenApp);
                if (count > 0) {
                    rtnCode = EcbEnum.ECB010000.key;
                    rtnMsg = EcbEnum.ECB010000.value;
                    return rtnData;
                } else {
                    rtnCode = EcbEnum.ECB020035.key;
                    rtnMsg = EcbEnum.ECB020035.value;
                }
            } else {
                User userInfo = SessionUtils.getUserInformation();
                lmtNpGreenApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtNpGreenApp.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_000);
                lmtNpGreenApp.setInputId(userInfo.getLoginCode());
                lmtNpGreenApp.setInputBrId(userInfo.getOrg().getCode());
                lmtNpGreenApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                lmtNpGreenApp.setUpdId(userInfo.getLoginCode());
                lmtNpGreenApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtNpGreenApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                lmtNpGreenApp.setCreateTime(DateUtils.getCurrDate());
                lmtNpGreenApp.setUpdateTime(DateUtils.getCurrDate());
            }
            int count = insertSelective(lmtNpGreenApp);
            if (count > 0) {
                rtnCode = EcbEnum.ECB010000.key;
                rtnMsg = EcbEnum.ECB010000.value;
                return rtnData;
            } else {
                rtnCode = EcbEnum.ECB020035.key;
                rtnMsg = EcbEnum.ECB020035.value;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 保存方法
     *
     * @param lmtNpGreenApp
     * @return
     */
    public int save(LmtNpGreenApp lmtNpGreenApp) {
        User userInfo = SessionUtils.getUserInformation();
        lmtNpGreenApp.setUpdId(userInfo.getLoginCode());
        lmtNpGreenApp.setUpdBrId(userInfo.getOrg().getCode());
        lmtNpGreenApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        return lmtNpGreenAppMapper.updateByPrimaryKeySelective(lmtNpGreenApp);
    }

    /**
     * 逻辑删除
     *
     * @param lmtNpGreenApp
     * @return
     */
    public int logicDelete(LmtNpGreenApp lmtNpGreenApp) {
        User userInfo = SessionUtils.getUserInformation();
        lmtNpGreenApp.setUpdId(userInfo.getLoginCode());
        lmtNpGreenApp.setUpdBrId(userInfo.getOrg().getCode());
        lmtNpGreenApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtNpGreenApp.setOprType(CmisBizConstants.OPR_TYPE_02);
        return lmtNpGreenAppMapper.updateByPrimaryKeySelective(lmtNpGreenApp);
    }
}
