/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperFinLeasCompany
 * @类描述: rpt_oper_fin_leas_company数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 20:38:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_fin_leas_company")
public class RptOperFinLeasCompany extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 目前租赁（保理）资产规模 **/
	@Column(name = "FACTOR_ASSET_SIZE", unique = false, nullable = true, length = 200)
	private String factorAssetSize;
	
	/** 其中省内业务 **/
	@Column(name = "PROVINCE_BUSI", unique = false, nullable = true, length = 200)
	private String provinceBusi;
	
	/** 其中省外业务 **/
	@Column(name = "OUT_PROVINCE_BUSI", unique = false, nullable = true, length = 200)
	private String outProvinceBusi;
	
	/** 其中企业类业务 **/
	@Column(name = "COMP_BUSI", unique = false, nullable = true, length = 200)
	private String compBusi;
	
	/** 其中政府类业务 **/
	@Column(name = "GOVE_BUSI", unique = false, nullable = true, length = 200)
	private String goveBusi;
	
	/** 对外投资情况其他需说明事项 **/
	@Column(name = "OUTER_INVEST_REMARK", unique = false, nullable = true, length = 65535)
	private String outerInvestRemark;
	
	/** 最近第两年主营业务收入 **/
	@Column(name = "NEAR_SECOND_MAIN_BUSI_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondMainBusiIncome;
	
	/** 最近第两年投资收益 **/
	@Column(name = "NEAR_SECOND_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondInvestIncome;
	
	/** 最近第两年利润总额 **/
	@Column(name = "NEAR_SECOND_BUSI_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondBusiTotalProfit;
	
	/** 最近第两年净利润 **/
	@Column(name = "NEAR_SECOND_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondProfit;
	
	/** 最近第两年其他需说明事项 **/
	@Column(name = "SECOND_YEAR_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String secondYearOtherNeedDesc;
	
	/** 最近第一年主营业务收入 **/
	@Column(name = "NEAR_FIRST_MAIN_BUSI_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstMainBusiIncome;
	
	/** 最近第一年投资收益 **/
	@Column(name = "NEAR_FIRST_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstInvestIncome;
	
	/** 最近第一年利润总额 **/
	@Column(name = "NEAR_FIRST_BUSI_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstBusiTotalProfit;
	
	/** 最近第一年净利润 **/
	@Column(name = "NEAR_FIRST_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstProfit;
	
	/** 最近第一年其他需说明事项 **/
	@Column(name = "FIRST_YEAR_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String firstYearOtherNeedDesc;
	
	/** 当前年主营业务收入 **/
	@Column(name = "CURR_CURR_SECOND_MAIN_BUSI_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currCurrSecondMainBusiIncome;
	
	/** 当前年投资收益 **/
	@Column(name = "CURR_SECOND_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currSecondInvestIncome;
	
	/** 当前年利润总额 **/
	@Column(name = "CURR_SECOND_BUSI_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currSecondBusiTotalProfit;
	
	/** 当前年净利润 **/
	@Column(name = "CURR_SECOND_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currSecondProfit;
	
	/** 当前年其他需说明事项 **/
	@Column(name = "CURR_YEAR_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String currYearOtherNeedDesc;
	
	/** 主营业务收入备注 **/
	@Column(name = "MAIN_BUSI_INCOME_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusiIncomeMemo;
	
	/** 投资收益备注 **/
	@Column(name = "INVEST_INCOME_MEMO", unique = false, nullable = true, length = 65535)
	private String investIncomeMemo;
	
	/** 利润总额备注 **/
	@Column(name = "BUSI_TOTAL_PROFIT_MEMO", unique = false, nullable = true, length = 65535)
	private String busiTotalProfitMemo;
	
	/** 净利润备注 **/
	@Column(name = "PROFIT_MEMO", unique = false, nullable = true, length = 65535)
	private String profitMemo;
	
	/** 其他需说明事项备注 **/
	@Column(name = "OTHER_NEED_DESC_MEMO", unique = false, nullable = true, length = 65535)
	private String otherNeedDescMemo;
	
	/** 损益情况其他需说明事项 **/
	@Column(name = "PROFIT_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String profitOtherNeedDesc;
	

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param factorAssetSize
	 */
	public void setFactorAssetSize(String factorAssetSize) {
		this.factorAssetSize = factorAssetSize;
	}
	
    /**
     * @return factorAssetSize
     */
	public String getFactorAssetSize() {
		return this.factorAssetSize;
	}
	
	/**
	 * @param provinceBusi
	 */
	public void setProvinceBusi(String provinceBusi) {
		this.provinceBusi = provinceBusi;
	}
	
    /**
     * @return provinceBusi
     */
	public String getProvinceBusi() {
		return this.provinceBusi;
	}
	
	/**
	 * @param outProvinceBusi
	 */
	public void setOutProvinceBusi(String outProvinceBusi) {
		this.outProvinceBusi = outProvinceBusi;
	}
	
    /**
     * @return outProvinceBusi
     */
	public String getOutProvinceBusi() {
		return this.outProvinceBusi;
	}
	
	/**
	 * @param compBusi
	 */
	public void setCompBusi(String compBusi) {
		this.compBusi = compBusi;
	}
	
    /**
     * @return compBusi
     */
	public String getCompBusi() {
		return this.compBusi;
	}
	
	/**
	 * @param goveBusi
	 */
	public void setGoveBusi(String goveBusi) {
		this.goveBusi = goveBusi;
	}
	
    /**
     * @return goveBusi
     */
	public String getGoveBusi() {
		return this.goveBusi;
	}
	
	/**
	 * @param outerInvestRemark
	 */
	public void setOuterInvestRemark(String outerInvestRemark) {
		this.outerInvestRemark = outerInvestRemark;
	}
	
    /**
     * @return outerInvestRemark
     */
	public String getOuterInvestRemark() {
		return this.outerInvestRemark;
	}
	
	/**
	 * @param nearSecondMainBusiIncome
	 */
	public void setNearSecondMainBusiIncome(java.math.BigDecimal nearSecondMainBusiIncome) {
		this.nearSecondMainBusiIncome = nearSecondMainBusiIncome;
	}
	
    /**
     * @return nearSecondMainBusiIncome
     */
	public java.math.BigDecimal getNearSecondMainBusiIncome() {
		return this.nearSecondMainBusiIncome;
	}
	
	/**
	 * @param nearSecondInvestIncome
	 */
	public void setNearSecondInvestIncome(java.math.BigDecimal nearSecondInvestIncome) {
		this.nearSecondInvestIncome = nearSecondInvestIncome;
	}
	
    /**
     * @return nearSecondInvestIncome
     */
	public java.math.BigDecimal getNearSecondInvestIncome() {
		return this.nearSecondInvestIncome;
	}
	
	/**
	 * @param nearSecondBusiTotalProfit
	 */
	public void setNearSecondBusiTotalProfit(java.math.BigDecimal nearSecondBusiTotalProfit) {
		this.nearSecondBusiTotalProfit = nearSecondBusiTotalProfit;
	}
	
    /**
     * @return nearSecondBusiTotalProfit
     */
	public java.math.BigDecimal getNearSecondBusiTotalProfit() {
		return this.nearSecondBusiTotalProfit;
	}
	
	/**
	 * @param nearSecondProfit
	 */
	public void setNearSecondProfit(java.math.BigDecimal nearSecondProfit) {
		this.nearSecondProfit = nearSecondProfit;
	}
	
    /**
     * @return nearSecondProfit
     */
	public java.math.BigDecimal getNearSecondProfit() {
		return this.nearSecondProfit;
	}
	
	/**
	 * @param secondYearOtherNeedDesc
	 */
	public void setSecondYearOtherNeedDesc(String secondYearOtherNeedDesc) {
		this.secondYearOtherNeedDesc = secondYearOtherNeedDesc;
	}
	
    /**
     * @return secondYearOtherNeedDesc
     */
	public String getSecondYearOtherNeedDesc() {
		return this.secondYearOtherNeedDesc;
	}
	
	/**
	 * @param nearFirstMainBusiIncome
	 */
	public void setNearFirstMainBusiIncome(java.math.BigDecimal nearFirstMainBusiIncome) {
		this.nearFirstMainBusiIncome = nearFirstMainBusiIncome;
	}
	
    /**
     * @return nearFirstMainBusiIncome
     */
	public java.math.BigDecimal getNearFirstMainBusiIncome() {
		return this.nearFirstMainBusiIncome;
	}
	
	/**
	 * @param nearFirstInvestIncome
	 */
	public void setNearFirstInvestIncome(java.math.BigDecimal nearFirstInvestIncome) {
		this.nearFirstInvestIncome = nearFirstInvestIncome;
	}
	
    /**
     * @return nearFirstInvestIncome
     */
	public java.math.BigDecimal getNearFirstInvestIncome() {
		return this.nearFirstInvestIncome;
	}
	
	/**
	 * @param nearFirstBusiTotalProfit
	 */
	public void setNearFirstBusiTotalProfit(java.math.BigDecimal nearFirstBusiTotalProfit) {
		this.nearFirstBusiTotalProfit = nearFirstBusiTotalProfit;
	}
	
    /**
     * @return nearFirstBusiTotalProfit
     */
	public java.math.BigDecimal getNearFirstBusiTotalProfit() {
		return this.nearFirstBusiTotalProfit;
	}
	
	/**
	 * @param nearFirstProfit
	 */
	public void setNearFirstProfit(java.math.BigDecimal nearFirstProfit) {
		this.nearFirstProfit = nearFirstProfit;
	}
	
    /**
     * @return nearFirstProfit
     */
	public java.math.BigDecimal getNearFirstProfit() {
		return this.nearFirstProfit;
	}
	
	/**
	 * @param firstYearOtherNeedDesc
	 */
	public void setFirstYearOtherNeedDesc(String firstYearOtherNeedDesc) {
		this.firstYearOtherNeedDesc = firstYearOtherNeedDesc;
	}
	
    /**
     * @return firstYearOtherNeedDesc
     */
	public String getFirstYearOtherNeedDesc() {
		return this.firstYearOtherNeedDesc;
	}
	
	/**
	 * @param currCurrSecondMainBusiIncome
	 */
	public void setCurrCurrSecondMainBusiIncome(java.math.BigDecimal currCurrSecondMainBusiIncome) {
		this.currCurrSecondMainBusiIncome = currCurrSecondMainBusiIncome;
	}
	
    /**
     * @return currCurrSecondMainBusiIncome
     */
	public java.math.BigDecimal getCurrCurrSecondMainBusiIncome() {
		return this.currCurrSecondMainBusiIncome;
	}
	
	/**
	 * @param currSecondInvestIncome
	 */
	public void setCurrSecondInvestIncome(java.math.BigDecimal currSecondInvestIncome) {
		this.currSecondInvestIncome = currSecondInvestIncome;
	}
	
    /**
     * @return currSecondInvestIncome
     */
	public java.math.BigDecimal getCurrSecondInvestIncome() {
		return this.currSecondInvestIncome;
	}
	
	/**
	 * @param currSecondBusiTotalProfit
	 */
	public void setCurrSecondBusiTotalProfit(java.math.BigDecimal currSecondBusiTotalProfit) {
		this.currSecondBusiTotalProfit = currSecondBusiTotalProfit;
	}
	
    /**
     * @return currSecondBusiTotalProfit
     */
	public java.math.BigDecimal getCurrSecondBusiTotalProfit() {
		return this.currSecondBusiTotalProfit;
	}
	
	/**
	 * @param currSecondProfit
	 */
	public void setCurrSecondProfit(java.math.BigDecimal currSecondProfit) {
		this.currSecondProfit = currSecondProfit;
	}
	
    /**
     * @return currSecondProfit
     */
	public java.math.BigDecimal getCurrSecondProfit() {
		return this.currSecondProfit;
	}
	
	/**
	 * @param currYearOtherNeedDesc
	 */
	public void setCurrYearOtherNeedDesc(String currYearOtherNeedDesc) {
		this.currYearOtherNeedDesc = currYearOtherNeedDesc;
	}
	
    /**
     * @return currYearOtherNeedDesc
     */
	public String getCurrYearOtherNeedDesc() {
		return this.currYearOtherNeedDesc;
	}
	
	/**
	 * @param mainBusiIncomeMemo
	 */
	public void setMainBusiIncomeMemo(String mainBusiIncomeMemo) {
		this.mainBusiIncomeMemo = mainBusiIncomeMemo;
	}
	
    /**
     * @return mainBusiIncomeMemo
     */
	public String getMainBusiIncomeMemo() {
		return this.mainBusiIncomeMemo;
	}
	
	/**
	 * @param investIncomeMemo
	 */
	public void setInvestIncomeMemo(String investIncomeMemo) {
		this.investIncomeMemo = investIncomeMemo;
	}
	
    /**
     * @return investIncomeMemo
     */
	public String getInvestIncomeMemo() {
		return this.investIncomeMemo;
	}
	
	/**
	 * @param busiTotalProfitMemo
	 */
	public void setBusiTotalProfitMemo(String busiTotalProfitMemo) {
		this.busiTotalProfitMemo = busiTotalProfitMemo;
	}
	
    /**
     * @return busiTotalProfitMemo
     */
	public String getBusiTotalProfitMemo() {
		return this.busiTotalProfitMemo;
	}
	
	/**
	 * @param profitMemo
	 */
	public void setProfitMemo(String profitMemo) {
		this.profitMemo = profitMemo;
	}
	
    /**
     * @return profitMemo
     */
	public String getProfitMemo() {
		return this.profitMemo;
	}
	
	/**
	 * @param otherNeedDescMemo
	 */
	public void setOtherNeedDescMemo(String otherNeedDescMemo) {
		this.otherNeedDescMemo = otherNeedDescMemo;
	}
	
    /**
     * @return otherNeedDescMemo
     */
	public String getOtherNeedDescMemo() {
		return this.otherNeedDescMemo;
	}
	
	/**
	 * @param profitOtherNeedDesc
	 */
	public void setProfitOtherNeedDesc(String profitOtherNeedDesc) {
		this.profitOtherNeedDesc = profitOtherNeedDesc;
	}
	
    /**
     * @return profitOtherNeedDesc
     */
	public String getProfitOtherNeedDesc() {
		return this.profitOtherNeedDesc;
	}
	

}