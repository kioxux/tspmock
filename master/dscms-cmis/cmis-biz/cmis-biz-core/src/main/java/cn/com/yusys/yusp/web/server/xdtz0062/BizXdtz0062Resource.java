package cn.com.yusys.yusp.web.server.xdtz0062;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0062.req.Xdtz0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.resp.Xdtz0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0062.Xdtz0062Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户的个人消费贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0062:查询客户的个人消费贷款")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0062Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0062Resource.class);
    @Autowired
    private Xdtz0062Service xdtz0062Service;

    /**
     * 交易码：xdtz0062
     * 交易描述：查询客户的个人消费贷款
     *
     * @param xdtz0062DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户的个人消费贷款")
    @PostMapping("/xdtz0062")
    protected @ResponseBody
    ResultDto<Xdtz0062DataRespDto> xdtz0062(@Validated @RequestBody Xdtz0062DataReqDto xdtz0062DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataReqDto));
        Xdtz0062DataRespDto xdtz0062DataRespDto = new Xdtz0062DataRespDto();// 响应Dto:更新信贷台账信息
        ResultDto<Xdtz0062DataRespDto> xdtz0062DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0062DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataReqDto));
            xdtz0062DataRespDto = xdtz0062Service.xdtz0062(xdtz0062DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataRespDto));
            // 封装xdtz0062DataResultDto中正确的返回码和返回信息
            xdtz0062DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0062DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, e.getMessage());
            // 封装xdtz0062DataResultDto中异常返回码和返回信息
            xdtz0062DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0062DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0062DataRespDto到xdtz0062DataResultDto中
        xdtz0062DataResultDto.setData(xdtz0062DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataResultDto));
        return xdtz0062DataResultDto;
    }
}
