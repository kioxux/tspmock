/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.service.DocAsplTcontService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAsplTcontResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:15:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docaspltcont")
public class DocAsplTcontResource {
    @Autowired
    private DocAsplTcontService docAsplTcontService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<DocAsplTcont>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<DocAsplTcont> list = docAsplTcontService.selectAll(queryModel);
        return new ResultDto<List<DocAsplTcont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/list")
    protected ResultDto<List<DocAsplTcont>> index(@RequestBody QueryModel queryModel) {
        queryModel.setSort("input_date desc");
        List<DocAsplTcont> list = docAsplTcontService.selectByModel(queryModel);
        return new ResultDto<List<DocAsplTcont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<DocAsplTcont> show(@PathVariable("pkId") String pkId) {
        DocAsplTcont docAsplTcont = docAsplTcontService.selectByPrimaryKey(pkId);
        return new ResultDto<DocAsplTcont>(docAsplTcont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DocAsplTcont> create(@RequestBody DocAsplTcont docAsplTcont) {
        docAsplTcontService.insert(docAsplTcont);
        return new ResultDto<DocAsplTcont>(docAsplTcont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Map> update(@RequestBody DocAsplTcont docAsplTcont) {
        Map<String,String> map = new HashMap<>();
        int result = docAsplTcontService.updateSelective(docAsplTcont);
        String rtnCode = "9999";
        String rtnMsg = "贸易合同保存失败";
        if(result>0){
            rtnCode = "0000";
            rtnMsg = "贸易合同保存成功";
        }
        map.put("rtnCode",rtnCode);
        map.put("rtnMsg",rtnMsg);
        return new ResultDto<Map>(map);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = docAsplTcontService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docAsplTcontService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByTcontSerno")
    protected ResultDto<DocAsplTcont> selectByTcontSerno(@RequestBody Map map) {
        String tcontImgId = (String) map.get("tcontImgId");
        DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tcontImgId);
        // 可用信用总金额（实时计算）
        BigDecimal contHighAvlAmt = docAsplTcontService.getContHighAvlAmt(docAsplTcont);
        docAsplTcont.setContHighAvlAmt(contHighAvlAmt);
        return new ResultDto<DocAsplTcont>(docAsplTcont);
    }

    /**
     * @函数名称:insert
     * @函数描述:贸易合同新增操作
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易合同新增操作")
    @PostMapping("/insert")
    public ResultDto<Map> insert(@RequestBody DocAsplTcont docAsplTcont) {
        Map<String,String> map = docAsplTcontService.insertSelective(docAsplTcont);
        return new ResultDto<Map>(map);
    }
}
