/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CreditCardAppInfo;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.GuarBaseInfoDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCusLsnpInfo;
import cn.com.yusys.yusp.service.IqpCusLsnpInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusLsnpInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-04 19:28:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "客户零售内评信息")
@RequestMapping("/api/iqpcuslsnpinfo")
public class IqpCusLsnpInfoResource {
    @Autowired
    private IqpCusLsnpInfoService iqpCusLsnpInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCusLsnpInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCusLsnpInfo> list = iqpCusLsnpInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpCusLsnpInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpCusLsnpInfo>> index(QueryModel queryModel) {
        List<IqpCusLsnpInfo> list = iqpCusLsnpInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpCusLsnpInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpCusLsnpInfo> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpCusLsnpInfo>(iqpCusLsnpInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCusLsnpInfo> create(@RequestBody IqpCusLsnpInfo iqpCusLsnpInfo) throws URISyntaxException {
        iqpCusLsnpInfoService.insert(iqpCusLsnpInfo);
        return new ResultDto<IqpCusLsnpInfo>(iqpCusLsnpInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCusLsnpInfo iqpCusLsnpInfo) throws URISyntaxException {
        int result = iqpCusLsnpInfoService.update(iqpCusLsnpInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpCusLsnpInfoService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCusLsnpInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @author zlf
     * @date 2021/5/4 16:41
     * @version 1.0.0
     * @desc    内评测算
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("内评测算")
    @PostMapping("/updatelevel")
    protected ResultDto<IqpCusLsnpInfo> updateLevel(@RequestBody IqpCusLsnpInfo iqpCusLsnpInfo) throws URISyntaxException {
        ResultDto<IqpCusLsnpInfo> result = iqpCusLsnpInfoService.updateLevel(iqpCusLsnpInfo);
        return result;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据业务流水号查询零售内评信息")
    @PostMapping("/selectbyserno")
    protected  ResultDto<List<IqpCusLsnpInfo>> selectBySerno(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        List<IqpCusLsnpInfo> list = iqpCusLsnpInfoService.selectByIqpSerno(creditCardAppInfo.getSerno());
        return new ResultDto<List<IqpCusLsnpInfo>>(list);
    }


    @PostMapping("/selectbyiqpserno")
    protected  ResultDto<List<IqpCusLsnpInfo>> selectOne(@RequestBody IqpCusLsnpInfo iqpCusLsnpInfo) {
        List<IqpCusLsnpInfo> list = iqpCusLsnpInfoService.selectByIqpSerno(iqpCusLsnpInfo.getIqpSerno());
        return new ResultDto<List<IqpCusLsnpInfo>>(list);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/8/19 21:41
     * @version 1.0.0
     * @desc  根据流水号与产品好查询零售内评信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号与产品查询零售内评信息")
    @PostMapping("/selectbyiqpsernoandprd")
    protected  ResultDto<IqpCusLsnpInfo> selectByIqpSernoAndprd(@RequestBody IqpCusLsnpInfo iqpCusLsnpInfo) {
       IqpCusLsnpInfo list = iqpCusLsnpInfoService.selectByIqpSernoAndPrd(iqpCusLsnpInfo);
        return new ResultDto<IqpCusLsnpInfo>(list);
    }

    /**
     * @author zzr
     * @date 2021/9/3 10:41
     * @version 1.0.0
     * @desc    对公授信内评测算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("对公授信内评测算")
    @PostMapping("/updatelevelforlmt")
    protected ResultDto<Map> updateLevelForLmt(@RequestBody String iqpSerno) throws URISyntaxException {
        return new ResultDto<Map>(iqpCusLsnpInfoService.updateLevelForLmt(iqpSerno));
    }

    @PostMapping("/selectbylmtserno")
    protected  ResultDto<IqpCusLsnpInfo> selectByLmtSerno(@RequestBody String serno) {
        IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByLmtSerno(serno);
        return new ResultDto<IqpCusLsnpInfo>(iqpCusLsnpInfo);
    }


    /**
     * @函数名称:getGuarInfoUrl
     * @函数描述:获取押品系统url
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getlsnprirsurl")
    protected ResultDto<String> getLsnpRirsUrl(@RequestBody IqpLoanApp iqpLoanApp) throws UnsupportedEncodingException {
        return new ResultDto<String>(iqpCusLsnpInfoService.getLsnpRirsUrl(iqpLoanApp));
    }
}
