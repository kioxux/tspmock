package cn.com.yusys.yusp.service.server.xdqt0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusOnlineReservationInfo;
import cn.com.yusys.yusp.domain.MajorGradeInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.req.Xdqt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.resp.Xdqt0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.req.Xdsx0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.resp.Xdsx0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusOnlineReservationInfoMapper;
import cn.com.yusys.yusp.repository.mapper.MajorGradeInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:企业网银推送预约信息
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdqt0003Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdqt0003Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Resource
    private AdminSmUserService adminSmUserService;

    @Resource
    private CommonService commonService;

    @Autowired
    private SenddxService senddxService;

    @Resource
    private CusOnlineReservationInfoMapper cusOnlineReservationInfoMapper;
    /**
     * 企业网银推送预约信息
     *
     * @param xdqt0003DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdqt0003DataRespDto getWyAppointmentAction(Xdqt0003DataReqDto xdqt0003DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value);
        //返回对象
        Xdqt0003DataRespDto xdqt0003DataRespDto = new Xdqt0003DataRespDto();

        String cusId = xdqt0003DataReqDto.getCusId();//客户编号
        String cusName = xdqt0003DataReqDto.getCusName();//客户名称
        String unifiedCreditCode = xdqt0003DataReqDto.getUnifiedCreditCode();//统一社会信用代码
        String cusLinkman = xdqt0003DataReqDto.getCusLinkman();//客户联系人
        String phone = xdqt0003DataReqDto.getPhone();//联系电话
        String appointTime = xdqt0003DataReqDto.getAppointTime();//预约时间
        String appointType = xdqt0003DataReqDto.getAppointType();//预约类型
        String managerId = "";//管护人
        String mainBrId = "";//管护机构
        try {
            if (unifiedCreditCode != null && unifiedCreditCode.length()>0 ){
                if(appointType != null && appointType.length()>0){
                    //初始化任务编号
                    String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO,new HashMap());
                    CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);//根据客户号
                    if (cusBaseDto == null) {
                        xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xdqt0003DataRespDto.setOpMsg("不存在该客户");
                    } else {
                        managerId = cusBaseDto.getManagerId();
                        mainBrId = cusBaseDto.getMainBrId();
                        // 插入xd_biz_survey_rel
                        CusOnlineReservationInfo cusOnlineReservationInfo
                                = BeanUtils.beanCopy(xdqt0003DataReqDto, CusOnlineReservationInfo.class);
                        cusOnlineReservationInfo.setTaskNo(serno);
                        cusOnlineReservationInfo.setManagerId(managerId);
                        cusOnlineReservationInfo.setBelgOrg(mainBrId);
                        cusOnlineReservationInfo.setPrcRst("00");
                        cusOnlineReservationInfoMapper.insertSelective(cusOnlineReservationInfo);
                    }
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode("00143925");
                    String code = resultDto.getCode();//返回结果
                    String userMobilephone = "";
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        userMobilephone = adminSmUserDto.getUserMobilephone(); // 客户经理号电话号码
                    }
                    if (StringUtils.isNotBlank(userMobilephone)) {//如果有号码就给客户发短信
                        SenddxReqDto senddxReqDto = new SenddxReqDto();
                        senddxReqDto.setInfopt("dx");
                        SenddxReqList senddxReqList = new SenddxReqList();
                        senddxReqList.setMobile(userMobilephone);
                        senddxReqList.setSmstxt("企业客户" + cusName + "已向我行进行在线预约，请尽快将任务分配至客户经理处理。");
                        senddxReqDto.setSenddxReqList(Arrays.asList(senddxReqList));
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
                        senddxService.senddx(senddxReqDto);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
                    }
                    xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);// 成功失败标志
                    xdqt0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);// 描述信息
                } else {
                    xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xdqt0003DataRespDto.setOpMsg("客户预约业务类型不能为空");
                }
            } else {
                xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdqt0003DataRespDto.setOpMsg("客户统一社会信用代码不能为空");
            }

        } catch (BizException e) {
            xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key); // 成功失败标志
            xdqt0003DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value); // 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdqt0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
            xdqt0003DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value);
        return xdqt0003DataRespDto;
    }
}
