package cn.com.yusys.yusp.service.server.xdqt0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.domain.bat.BatApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.bat.BatApprStrMtableInfo;
import cn.com.yusys.yusp.domain.bat.BatBizWydPojo;
import cn.com.yusys.yusp.dto.CfgLprRateDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.repository.mapper.wyd.*;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.cus.xdkh0002.Xdkh0002Service;
import com.alibaba.fastjson.JSON;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Service
public class WydBatchService {

    private static final Logger logger = LoggerFactory.getLogger(WydBatchService.class);

    @Resource
    private TmpWydCommMapper tmpWydCommMapper;
    @Resource
    private TmpWydCreditlineTmpMapper tmpWydCreditlineTmpMapper;
    @Resource
    private TmpWydContractTmpMapper tmpWydContractTmpMapper;
    @Resource
    private TmpWydLoanTmpMapper tmpWydLoanTmpMapper;
    @Resource
    private TmpWydLoanDetailTmpMapper tmpWydLoanDetailTmpMapper;
    @Resource
    private TmpWydIouIetmTmpMapper tmpWydIouIetmTmpMapper;
    @Resource
    private TmpWydLoanOverdueMapper tmpWydLoanOverdueMapper;


    @Resource
    private LmtAppMapper lmtAppMapper;
    @Resource
    private LmtAppSubMapper lmtAppSubMapper;
    @Resource
    private LmtAppSubPrdMapper lmtAppSubPrdMapper;
    @Resource
    private LmtReplyMapper lmtReplyMapper;
    @Resource
    private LmtReplySubMapper lmtReplySubMapper;
    @Resource
    private LmtReplySubPrdMapper lmtReplySubPrdMapper;
    @Resource
    private LmtReplyAccMapper lmtReplyAccMapper;
    @Resource
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;
    @Resource
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private AccLoanMapper accLoanMapper;
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Resource
    private Xdkh0002Service xdkh0002Service;

	@Resource
    private ICmisCfgClientService cmisCfgClientService;

    @Autowired
    private SenddxService senddxService;


    /**
     * 从文件中读取数据
     * @author zdl
     * @throws Exception
     */
    public void loandDateFromFile (String tableName,String columns, File file) throws FileNotFoundException {
        //this.getColumnsByTableName(tableName);
        this.deleteAll(tableName);
        Scanner scanner = new Scanner(file,"UTF-8");
        int count = 0;
        List<String> valueList  = new ArrayList<String>();
        while(scanner.hasNextLine())
        {
            String line = scanner.nextLine();
            if(count==0){//跳过第一行表头
                count++;
                continue;
            }else{
                count++;
            }
            if(!StringUtils.isEmpty(line))
            {
                if(count<=500){
                    valueList.add("('"+ line.replace("|","','")+"'),");
                }else{
                    this.insertDataListToTable(tableName,columns,valueList);
                    valueList.clear();
                    valueList.add("('"+ line.replace("|","','")+"'),");
                    count=1;
                }
            }
        }
        if(valueList.size()!=0){
            this.insertDataListToTable(tableName,columns,valueList);
        }
    }
    /**
     * 将数据集写入指定表的指定列
     * @author zdl
     *
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int insertDataListToTable(String tableName,String columns,List<String> valueList){
        int lastIndex = valueList.size()-1;
        String lastLine = valueList.get(lastIndex);
        lastLine = lastLine.substring(0,lastLine.length()-1);
        valueList.remove(lastIndex);
        valueList.add(lastLine);

        return tmpWydCommMapper.batchInsert(tableName,columns,valueList);
    }
    /**
     * 清空指定表
     * @author zdl
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void deleteAll(String tableName){
        tmpWydCommMapper.deleteAll(tableName);
    }
    /**
     * 根据主键匹配，将A,B表中同时存在的数据从A表中删除
     * @author zdl
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void deleleExistsData(String tableA,String tableB,String indexCondition ){
        tmpWydCommMapper.deleleExistsData(tableA,tableB,indexCondition);
    }
    /**
     * 将B表中数据全部插入A表
     * @author zdl
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void insertIntoDataFromB2A(String tableA,String tableB,String columns){
        tmpWydCommMapper.insertIntoDataFromB2A(tableA,tableB,columns);
    }
    /**
     * 初始化需要装载数据的表
     * @author zdl
     * @throws Exception
     */
    List<BatBizWydPojo> initLoadTableList(){
        List<BatBizWydPojo> tableList = new ArrayList<BatBizWydPojo>();
        BatBizWydPojo tmpWydLoan = new BatBizWydPojo();
        tmpWydLoan.setTableA("TMP_WYD_LOAN");
        tmpWydLoan.setTableB("TMP_WYD_LOAN_TMP");
        tmpWydLoan.setColumns(BatEnums.TMP_WYD_LOAN.value);
        tmpWydLoan.setPrimary(BatEnums.PRIMARY_TMP_WYD_LOAN.value);
        tmpWydLoan.setDataFileName("01_LOAN_D");
        tableList.add(tmpWydLoan);

        BatBizWydPojo tmpWydHkPlan = new BatBizWydPojo();
        tmpWydHkPlan.setTableA("TMP_WYD_HK_PLAN");
        tmpWydHkPlan.setTableB("TMP_WYD_HK_PLAN_TMP");
        tmpWydHkPlan.setColumns(BatEnums.TMP_WYD_HK_PLAN.value);
        tmpWydHkPlan.setPrimary(BatEnums.PRIMARY_TMP_WYD_HK_PLAN.value);
        tmpWydHkPlan.setDataFileName("02_PAYMENTSCHED_D");
        tableList.add(tmpWydHkPlan);

        BatBizWydPojo tmpWydHkDetails = new BatBizWydPojo();
        tmpWydHkDetails.setTableA("TMP_WYD_HK_DETAILS");
        tmpWydHkDetails.setTableB("TMP_WYD_HK_DETAILS_TMP");
        tmpWydHkDetails.setColumns(BatEnums.TMP_WYD_HK_DETAILS.value);
        tmpWydHkDetails.setPrimary(BatEnums.PRIMARY_TMP_WYD_HK_DETAILS.value);
        tmpWydHkDetails.setDataFileName("03_PAYMENTDETAIL_D");
        tableList.add(tmpWydHkDetails);

        BatBizWydPojo tmpWydContract = new BatBizWydPojo();
        tmpWydContract.setTableA("TMP_WYD_CONTRACT");
        tmpWydContract.setTableB("TMP_WYD_CONTRACT_TMP");
        tmpWydContract.setColumns(BatEnums.TMP_WYD_CONTRACT.value);
        tmpWydContract.setPrimary(BatEnums.PRIMARY_TMP_WYD_CONTRACT.value);
        tmpWydContract.setDataFileName("04_CONTRACT_D");
        tableList.add(tmpWydContract);

        BatBizWydPojo tmpWydCreditline = new BatBizWydPojo();
        tmpWydCreditline.setTableA("TMP_WYD_CREDITLINE");
        tmpWydCreditline.setTableB("TMP_WYD_CREDITLINE_TMP");
        tmpWydCreditline.setColumns(BatEnums.TMP_WYD_CREDITLINE.value);
        tmpWydCreditline.setPrimary(BatEnums.PRIMARY_TMP_WYD_CREDITLINE.value);
        tmpWydCreditline.setDataFileName("05_CREDITLINE_D");
        tableList.add(tmpWydCreditline);

        BatBizWydPojo tmpWydGuarGuarantor = new BatBizWydPojo();
        tmpWydGuarGuarantor.setTableA("TMP_WYD_GUAR_GUARANTOR");
        tmpWydGuarGuarantor.setTableB("TMP_WYD_GUAR_GUARANTOR_TMP");
        tmpWydGuarGuarantor.setColumns(BatEnums.TMP_WYD_GUAR_GUARANTOR.value);
        tmpWydGuarGuarantor.setPrimary(BatEnums.PRIMARY_TMP_WYD_GUAR_GUARANTOR.value);
        tmpWydGuarGuarantor.setDataFileName("06_GUARANTEE_GUARANTOR_D");
        tableList.add(tmpWydGuarGuarantor);

        BatBizWydPojo tmpWydGuarCont = new BatBizWydPojo();
        tmpWydGuarCont.setTableA("TMP_WYD_GUAR_CONT");
        tmpWydGuarCont.setTableB("TMP_WYD_GUAR_CONT_TMP");
        tmpWydGuarCont.setColumns(BatEnums.TMP_WYD_GUAR_CONT.value);
        tmpWydGuarCont.setPrimary(BatEnums.PRIMARY_TMP_WYD_GUAR_CONT.value);
        tmpWydGuarCont.setDataFileName("07_GUARANTEECONT_D");
        tableList.add(tmpWydGuarCont);

        BatBizWydPojo tmpWydIouGuarRef = new BatBizWydPojo();
        tmpWydIouGuarRef.setTableA("TMP_WYD_IOU_GUAR_REF");
        tmpWydIouGuarRef.setTableB("TMP_WYD_IOU_GUAR_REF_TMP");
        tmpWydIouGuarRef.setColumns(BatEnums.TMP_WYD_IOU_GUAR_REF.value);
        tmpWydIouGuarRef.setPrimary(BatEnums.PRIMARY_TMP_WYD_IOU_GUAR_REF.value);
        tmpWydIouGuarRef.setDataFileName("08_GUARANTEERELATION_D");
        tableList.add(tmpWydIouGuarRef);

        BatBizWydPojo tmpWydLoanTransDetail = new BatBizWydPojo();
        tmpWydLoanTransDetail.setTableA("TMP_WYD_LOAN_TRANS_DETAIL");
        tmpWydLoanTransDetail.setTableB("TMP_WYD_LOAN_TRANS_DETAIL_TMP");
        tmpWydLoanTransDetail.setColumns(BatEnums.TMP_WYD_LOAN_TRANS_DETAIL.value);
        tmpWydLoanTransDetail.setPrimary(BatEnums.PRIMARY_TMP_WYD_LOAN_TRANS_DETAIL.value);
        tmpWydLoanTransDetail.setDataFileName("09_LOAN_TRANSDETAIL_D");
        tableList.add(tmpWydLoanTransDetail);

        BatBizWydPojo tmpWydLoanDetail = new BatBizWydPojo();
        tmpWydLoanDetail.setTableA("TMP_WYD_LOAN_DETAIL");
        tmpWydLoanDetail.setTableB("TMP_WYD_LOAN_DETAIL_TMP");
        tmpWydLoanDetail.setColumns(BatEnums.TMP_WYD_LOAN_DETAIL.value);
        tmpWydLoanDetail.setPrimary(BatEnums.PRIMARY_TMP_WYD_LOAN_DETAIL.value);
        tmpWydLoanDetail.setDataFileName("10_LOANDETAIL_D");
        tableList.add(tmpWydLoanDetail);

        BatBizWydPojo tmpWydEntFinInfo = new BatBizWydPojo();
        tmpWydEntFinInfo.setTableA("TMP_WYD_ENT_FIN_INFO");
        tmpWydEntFinInfo.setTableB("TMP_WYD_ENT_FIN_INFO_TMP");
        tmpWydEntFinInfo.setColumns(BatEnums.TMP_WYD_ENT_FIN_INFO.value);
        tmpWydEntFinInfo.setPrimary(BatEnums.PRIMARY_TMP_WYD_ENT_FIN_INFO.value);
        tmpWydEntFinInfo.setDataFileName("11_ENT_FINANCE_INFO_D");
        tableList.add(tmpWydEntFinInfo);

        /*************12号和13号文件暂时不装数，由中台落数
        BatBizWydPojo tmpWydRbtEleRecd = new BatBizWydPojo();
        tmpWydRbtEleRecd.setTableA("TMP_WYD_RBT_ELE_RECD");
        tmpWydRbtEleRecd.setTableB("TMP_WYD_RBT_ELE_RECD_TMP");
        tmpWydRbtEleRecd.setColumns(BatEnums.TMP_WYD_RBT_ELE_RECD.value);
        tmpWydRbtEleRecd.setPrimary(BatEnums.PRIMARY_TMP_WYD_RBT_ELE_RECD.value);
        tmpWydRbtEleRecd.setDataFileName("12_ROBOT_COLLECTION_D");
        tableList.add(tmpWydRbtEleRecd);

        BatBizWydPojo tmpWydIntnalepdRecd = new BatBizWydPojo();
        tmpWydIntnalepdRecd.setTableA("TMP_WYD_INTNALEPD_RECD");
        tmpWydIntnalepdRecd.setTableB("TMP_WYD_INTNALEPD_RECD_TMP");
        tmpWydIntnalepdRecd.setColumns(BatEnums.TMP_WYD_INTNALEPD_RECD.value);
        tmpWydIntnalepdRecd.setPrimary(BatEnums.PRIMARY_TMP_WYD_INTNALEPD_RECD.value);
        tmpWydIntnalepdRecd.setDataFileName("13_INSIDE_COLLECTION_D");
        tableList.add(tmpWydIntnalepdRecd);
         *************/

        BatBizWydPojo tmpWydCusInvoice = new BatBizWydPojo();
        tmpWydCusInvoice.setTableA("TMP_WYD_CUS_INVOICE");
        tmpWydCusInvoice.setTableB("TMP_WYD_CUS_INVOICE_TMP");
        tmpWydCusInvoice.setColumns(BatEnums.TMP_WYD_CUS_INVOICE.value);
        tmpWydCusInvoice.setPrimary(BatEnums.PRIMARY_TMP_WYD_CUS_INVOICE.value);
        tmpWydCusInvoice.setDataFileName("14_ENTERPRISE_INVOICE_D");
        tableList.add(tmpWydCusInvoice);

        BatBizWydPojo tmpWydAcctSubDetail = new BatBizWydPojo();
        tmpWydAcctSubDetail.setTableA("TMP_WYD_ACCT_SUB_DETAIL");
        tmpWydAcctSubDetail.setTableB("TMP_WYD_ACCT_SUB_DETAIL_TMP");
        tmpWydAcctSubDetail.setColumns(BatEnums.TMP_WYD_ACCT_SUB_DETAIL.value);
        tmpWydAcctSubDetail.setPrimary(BatEnums.PRIMARY_TMP_WYD_ACCT_SUB_DETAIL.value);
        tmpWydAcctSubDetail.setDataFileName("15_ACCT_SUBLEDGER_DETAIL_D");
        tableList.add(tmpWydAcctSubDetail);

        BatBizWydPojo tmpWydAcctSubLedger = new BatBizWydPojo();
        tmpWydAcctSubLedger.setTableA("TMP_WYD_ACCT_SUB_LEDGER");
        tmpWydAcctSubLedger.setTableB("TMP_WYD_ACCT_SUB_LEDGER_TMP");
        tmpWydAcctSubLedger.setColumns(BatEnums.TMP_WYD_ACCT_SUB_LEDGER.value);
        tmpWydAcctSubLedger.setPrimary(BatEnums.PRIMARY_TMP_WYD_ACCT_SUB_LEDGER.value);
        tmpWydAcctSubLedger.setDataFileName("16_ACCT_SUBSIDIARY_LEDGER_D");
        tableList.add(tmpWydAcctSubLedger);

        BatBizWydPojo tmpWydWarnInfo = new BatBizWydPojo();
        tmpWydWarnInfo.setTableA("TMP_WYD_WARN_INFO");
        tmpWydWarnInfo.setTableB("TMP_WYD_WARN_INFO_TMP");
        tmpWydWarnInfo.setColumns(BatEnums.TMP_WYD_WARN_INFO.value);
        tmpWydWarnInfo.setPrimary(BatEnums.PRIMARY_TMP_WYD_WARN_INFO.value);
        tmpWydWarnInfo.setDataFileName("17_WARN_INFO_D");
        tableList.add(tmpWydWarnInfo);

        BatBizWydPojo tmpWydCusManager = new BatBizWydPojo();
        tmpWydCusManager.setTableA("TMP_WYD_CUS_MANAGER");
        tmpWydCusManager.setTableB("TMP_WYD_CUS_MANAGER_TMP");
        tmpWydCusManager.setColumns(BatEnums.TMP_WYD_CUS_MANAGER.value);
        tmpWydCusManager.setPrimary(BatEnums.PRIMARY_TMP_WYD_CUS_MANAGER.value);
        tmpWydCusManager.setDataFileName("18_CUSTOMER_MANAGER_D");
        tableList.add(tmpWydCusManager);

        BatBizWydPojo tmpWydCusInfo = new BatBizWydPojo();
        tmpWydCusInfo.setTableA("TMP_WYD_CUS_INFO");
        tmpWydCusInfo.setTableB("TMP_WYD_CUS_INFO_TMP");
        tmpWydCusInfo.setColumns(BatEnums.TMP_WYD_CUS_INFO.value);
        tmpWydCusInfo.setPrimary(BatEnums.PRIMARY_TMP_WYD_CUS_INFO.value);
        tmpWydCusInfo.setDataFileName("19_CUSTOMER_INFO_D");
        tableList.add(tmpWydCusInfo);

        BatBizWydPojo tmpWydAccountFlow = new BatBizWydPojo();
        tmpWydAccountFlow.setTableA("TMP_WYD_ACCOUNT_FLOW");
        tmpWydAccountFlow.setTableB("TMP_WYD_ACCOUNT_FLOW_TMP");
        tmpWydAccountFlow.setColumns(BatEnums.TMP_WYD_ACCOUNT_FLOW.value);
        tmpWydAccountFlow.setPrimary(BatEnums.PRIMARY_TMP_WYD_ACCOUNT_FLOW.value);
        tmpWydAccountFlow.setDataFileName("20_ACCOUNT_FLOW_D");
        tableList.add(tmpWydAccountFlow);

        BatBizWydPojo pspWydTaskList = new BatBizWydPojo();
        pspWydTaskList.setTableA("cmis_psp.PSP_WYD_TASK_LIST");
        pspWydTaskList.setTableB("cmis_psp.PSP_WYD_TASK_LIST_TMP");
        pspWydTaskList.setColumns(BatEnums.PSP_WYD_TASK_LIST.value);
        pspWydTaskList.setPrimary(BatEnums.PRIMARY_PSP_WYD_TASK_LIST.value);
        pspWydTaskList.setDataFileName("LoanAfter");
        tableList.add(pspWydTaskList);

        BatBizWydPojo tmpWydIouIetm = new BatBizWydPojo();
        tmpWydIouIetm.setTableA("TMP_WYD_IOU_IETM");
        tmpWydIouIetm.setTableB("TMP_WYD_IOU_IETM_TMP");
        tmpWydIouIetm.setColumns(BatEnums.TMP_WYD_IOU_IETM.value);
        tmpWydIouIetm.setPrimary(BatEnums.PRIMARY_TMP_WYD_IOU_IETM.value);
        tmpWydIouIetm.setDataFileName("LendingNumber");
        tableList.add(tmpWydIouIetm);


        return tableList;
    }


    /**
     * 保存或更新微业贷业务信息
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional
    public void saveOrUpdateWydBusinessInfo(String batchDate) throws Exception{
        logger.info("将微业贷信息插入各业务模块表中开始>>>");
        //1、保存或更新微业贷业务信息--授信信息
        this.saveOrUpdateWydLmtInfo();
        //2、存或更新微业贷业务信息--合同信息
        this.saveOrUpdateWydCtrInfo();
        //3、保存或更新微业贷业务信息--借据信息
        this.saveOrUpdateWydAccInfo(batchDate);
        logger.info("将微业贷信息插入各业务模块表中结束<<<");
    }

    /**
     * 保存或更新微业贷业务信息--合同信息
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void saveOrUpdateWydCtrInfo(){
        logger.info("保存或更新微业贷业务信息【合同信息：ctr_loan_cont】开始>>>");
        List<TmpWydContractTmp> wydContractList = tmpWydContractTmpMapper.selectAll();
        if(!CollectionUtils.isEmpty(wydContractList)) {
            boolean exists;//记录是否存在标记
            String curType = CmisCommonConstants.CUR_TYPE_CNY;//币种 人民币
            String guarWay = CmisCommonConstants.GUAR_MODE_00;//主担保方式 00 信用
            String contType = CmisCommonConstants.STD_CONT_TYPE_1;//合同类型 1 一般合同
            String prdTypeProp = "P042";//产品类型属性 P042 微业贷
            String loanModal = CmisBizConstants.IQP_LOAN_MODAL_1;//贷款形式 1 新增
            String loanCha = "01";//贷款性质 01 自营贷款
            String termType = CmisBizConstants.STD_TERM_TYPE_M;//期限类型 M 月
            String lprPriceInterval = "A1";//A1 1年期
            BigDecimal curtLprRate = this.getCurtLprRateByLprType(lprPriceInterval,curType);//當前LPR利率
            String bizType = CmisCommonConstants.STD_BUSI_TYPE_02;//业务类型 STD_BUSI_TYPE 02-普通贷款合同
            String approveStatus = CmisBizConstants.APPLY_STATE_PASS;//申请状态 997 通过
            String oprType = CmisBizConstants.OPR_TYPE_01;//操作类型 01	新增
            String belgLine = CmisBizConstants.BELGLINE_DG_03;//所属条线 03 对公
            String managerId = CmisBizConstants.WYD_MANAGER_ID;//客户经理
            String managerBrId = CmisBizConstants.WYD_MANAGER_BR_ID;//管户机构
            for (TmpWydContractTmp wydContract : wydContractList) {
                //1、数据准备
                String contNo = wydContract.getContractNo();//合同编号
                String replySerno = "PF" + wydContract.getLimitNo();//批复流水号
                String lmtAccNo = "EDPZ" + wydContract.getLimitNo() + "010";//授信额度编号
                String cusName = wydContract.getCustName();//客户名称
                //根据客户名称查询客户号
                String cusId = this.getComCusIdByName(cusName);
                if (StringUtils.isEmpty(cusId)){
                    cusId = wydContract.getCustId();
                };

                //合同起始日
                String contStartDate = StringUtils.isNotEmpty(wydContract.getStartDate())
                        ? DateUtils.formatDate(DateUtils.parseDate(wydContract.getStartDate(),"yyyy/MM/dd"),
                        DateFormatEnum.DEFAULT.getValue())
                        : StringUtils.EMPTY;
                //合同到期日
                String contEndDate = StringUtils.isNotEmpty(wydContract.getEndDate())
                        ? DateUtils.formatDate(DateUtils.parseDate(wydContract.getEndDate(),"yyyy/MM/dd"),
                        DateFormatEnum.DEFAULT.getValue())
                        : StringUtils.EMPTY;

                BigDecimal contAmt = new BigDecimal(wydContract.getContractAmt());//合同金额
                String email = wydContract.getEnterpriseEmail();//EMAIL

                TmpWydLoanTmp wydLoan = tmpWydLoanTmpMapper.selectByContractNo(contNo);
                TmpWydLoanDetailTmp wydLoanDetail = tmpWydLoanDetailTmpMapper.selectByContractNo(contNo);
                String tremMonth = "0";//默认0
                String contStatus = StringUtils.EMPTY;//合同状态
                String repayMode = StringUtils.EMPTY;//还款方式
                String loanPayoutAccno = StringUtils.EMPTY;//贷款发放账号
                String loanPayoutAccName = StringUtils.EMPTY;//贷款发放账号名称
                if(wydLoan != null) {
                    if (StringUtils.isNotEmpty(wydLoan.getTremmonth())) {
                        tremMonth = wydLoan.getTremmonth();
                    }
                    String loanStatus;
                    if (StringUtils.isNotEmpty(wydLoan.getLoanstatus())) {
                        loanStatus = wydLoan.getLoanstatus();
                        if ("01".equals(loanStatus) || "02".equals(loanStatus) || "03".equals(loanStatus)) {
                            contStatus = CmisBizConstants.IQP_CONT_STS_200;
                        } else {
                            contStatus = CmisBizConstants.IQP_CONT_STS_600;
                        }
                    }
                    if (StringUtils.isNotEmpty(wydLoan.getCorpuspaymethod())) {
                        repayMode = this.transitionRepayWay(wydLoan.getCorpuspaymethod());
                    }
                    loanPayoutAccno = wydLoan.getInAcctNo();
                    loanPayoutAccName = wydLoan.getInAcctNoName();

                }
                String prdId = this.getWydPrdInfoByTrem(Integer.parseInt(tremMonth)).get("prdId");//产品编号
                String prdName = this.getWydPrdInfoByTrem(Integer.parseInt(tremMonth)).get("prdName");//产品名称

                BigDecimal execRateYear = null;//执行年利率
                BigDecimal realityIrM = null;//执行利率(月)
                BigDecimal rulingIr = null;//基准利率（年）
                BigDecimal rulingIrM = null;//基准利率（月）
                String irAdjustType = StringUtils.EMPTY;//利率调整方式 01 固定利率
                String loanTer = StringUtils.EMPTY;//贷款投向
                //BigDecimal lprBp = null;//LPR浮動點數
                if(wydLoanDetail != null){
                    if(StringUtils.isNotEmpty(wydLoanDetail.getRate())) {
                        execRateYear = new BigDecimal(wydLoanDetail.getRate());
                        if(execRateYear.compareTo(new BigDecimal("1")) == 1){
                            execRateYear = execRateYear.divide(new BigDecimal("100"));
                        }
                        realityIrM = execRateYear.divide(new BigDecimal("12"),BigDecimal.ROUND_HALF_UP);
                        //lprBp = execRateYear.subtract(curtLprRate).multiply(new BigDecimal("10000"));
                    }

                    if(StringUtils.isNotEmpty(wydLoanDetail.getBaseRate())) {
                        rulingIr = new BigDecimal(wydLoanDetail.getBaseRate());
                        if(rulingIr.compareTo(new BigDecimal("1")) == 1){
                            rulingIr = rulingIr.divide(new BigDecimal("100"));
                        }
                        rulingIrM = rulingIr.divide(new BigDecimal("12"),BigDecimal.ROUND_HALF_UP);
                    }

                    if(StringUtils.isNotEmpty(wydLoanDetail.getRateType())) {
                        if("01".equals(wydLoanDetail.getRateType())){
                            irAdjustType = "01";
                        }else{
                            irAdjustType = "02";
                        }
                    }
                    loanTer = wydLoanDetail.getLoanPurpose();
                }
                Integer contTerm = new Integer(tremMonth);//合同期限
                BigDecimal appTerm = new BigDecimal(tremMonth);//申请期限

                //2、保存或更新
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
                if(ctrLoanCont == null){
                    exists = false;
                    ctrLoanCont = new CtrLoanCont();
                    ctrLoanCont.setContNo(contNo);
                    ctrLoanCont.setPrdTypeProp(prdTypeProp);
                    ctrLoanCont.setLmtAccNo(lmtAccNo);
                    ctrLoanCont.setLoanModal(loanModal);
                    ctrLoanCont.setLoanCha(loanCha);
                    ctrLoanCont.setGuarWay(guarWay);
                    ctrLoanCont.setCurType(curType);
                    ctrLoanCont.setContType(contType);
                    //ctrLoanCont.setLprPriceInterval(lprPriceInterval);
                    ctrLoanCont.setCurtLprRate(curtLprRate);
                    ctrLoanCont.setTermType(termType);
                    ctrLoanCont.setLoanTer(loanTer);
                    ctrLoanCont.setApproveStatus(approveStatus);
                    ctrLoanCont.setBizType(bizType);
                    ctrLoanCont.setOprType(oprType);
                    ctrLoanCont.setManagerId(managerId);
                    ctrLoanCont.setManagerBrId(managerBrId);
                    ctrLoanCont.setInputId(managerId);
                    ctrLoanCont.setInputBrId(managerBrId);
                    ctrLoanCont.setUpdId(managerId);
                    ctrLoanCont.setUpdBrId(managerBrId);
                    ctrLoanCont.setOprType(oprType);
                    ctrLoanCont.setBelgLine(belgLine);
                    ctrLoanCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    ctrLoanCont.setCreateTime(DateUtils.getCurrDate());
                }else{
                    exists = true;
                }
                ctrLoanCont.setReplyNo(replySerno);
                ctrLoanCont.setCusId(cusId);
                ctrLoanCont.setCusName(cusName);
                ctrLoanCont.setContTerm(contTerm);
                ctrLoanCont.setPrdId(prdId);
                ctrLoanCont.setPrdName(prdName);
                ctrLoanCont.setContStartDate(contStartDate);
                ctrLoanCont.setContEndDate(contEndDate);
                ctrLoanCont.setContStatus(contStatus);
                ctrLoanCont.setContAmt(contAmt);
                //ctrLoanCont.setLprPriceInterval(lprPriceInterval);
                ctrLoanCont.setAppTerm(appTerm);
                ctrLoanCont.setExecRateYear(execRateYear);
                ctrLoanCont.setRealityIrM(realityIrM);
                ctrLoanCont.setRulingIr(rulingIr);
                ctrLoanCont.setRealityIrM(rulingIrM);
                ctrLoanCont.setIrAdjustType(irAdjustType);
                ctrLoanCont.setRepayMode(repayMode);
                ctrLoanCont.setSignDate(contStartDate);
                ctrLoanCont.setLoanPayoutAccno(loanPayoutAccno);
                ctrLoanCont.setLoanPayoutAccName(loanPayoutAccName);
                ctrLoanCont.setEmail(email);
                ctrLoanCont.setLoanTer(loanTer);
                //ctrLoanCont.setLprBp(lprBp);
                ctrLoanCont.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                ctrLoanCont.setUpdateTime(DateUtils.getCurrDate());
                if(exists){
                    ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                }else{
                    ctrLoanContMapper.insertSelective(ctrLoanCont);
                }
            }
        }
        logger.info("保存或更新微业贷业务信息【合同信息：ctr_loan_cont】结束<<<");
    }

    /**
     * 保存或更新微业贷业务信息--出账申请、借据信息
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void saveOrUpdateWydAccInfo(String batchDate){
        logger.info("保存或更新微业贷业务信息【出账申请：pvp_loan_app、借据信息：acc_loan】开始>>>");
        List<TmpWydLoanTmp> wydLoanList = tmpWydLoanTmpMapper.selectAll();
        if(!CollectionUtils.isEmpty(wydLoanList)) {
            boolean exists;//记录是否存在标记
            String curType = CmisCommonConstants.CUR_TYPE_CNY;//币种 人民币
            String guarWay = CmisCommonConstants.GUAR_MODE_00;//主担保方式 00 信用
            String prdTypeProp = "P042";//产品类型属性 P042 微业贷
            String loanModal = CmisBizConstants.IQP_LOAN_MODAL_1;//贷款形式 1 新增
            String deductDay = "21";//扣款日 默认每月21号
            String termType = CmisBizConstants.STD_TERM_TYPE_M;//期限类型 M 月
            String eiIntervalUnit = CmisBizConstants.STD_TERM_TYPE_M;//结息间隔周期单位 M 月
            String deductType = "AUTO";//扣款方式
            String nextRateAdjInterval = "1";//下一次利率调整间隔
            String nextRateAdjUnit = CmisBizConstants.STD_TERM_TYPE_M;//下一次利率调整间隔单位
            String lprPriceInterval = "A1";//A1 1年期
            BigDecimal curtLprRate = this.getCurtLprRateByLprType(lprPriceInterval,curType);
            BigDecimal irFloatRate = new BigDecimal("0");//利率浮动百分比
            BigDecimal overdueRatePefloat = new BigDecimal("0.5");//利率浮动百分比
            String loanUseType = "A01";//借款用途类型 STD_LOAN_USE_TYPE  A01 流动资金-资金周转
            String isSbsy = CmisCommonConstants.YES_NO_0;//是否贴息
            BigDecimal sbsyPerc = new BigDecimal("0");//贴息比例
            BigDecimal exchangeRate = new BigDecimal("1");//汇率
            String approveStatus = CmisBizConstants.APPLY_STATE_PASS;//申请状态 997 通过
            String oprType = CmisBizConstants.OPR_TYPE_01;//操作类型 01	新增
            String belgLine = CmisBizConstants.BELGLINE_DG_03;//所属条线 03 对公
            String finaBrId = CmisBizConstants.WYD_FINA_BR_ID;//账务机构
            String finaBrName = CmisBizConstants.WYD_FINA_BR_NAME;//账务机构名称
            String managerId = CmisBizConstants.WYD_MANAGER_ID;//客户经理
            String managerBrId = CmisBizConstants.WYD_MANAGER_BR_ID;//管户机构
            for (TmpWydLoanTmp wydLoan : wydLoanList) {

                //数据准备
                String pvpSerno = wydLoan.getLendingRef();//出账申请流水号
                String contNo = wydLoan.getContractNo();//合同号
                String billNo = wydLoan.getLendingRef();//借据号
                String cusName = wydLoan.getFullname();//客户名称
                //根据客户名称查询客户号
                String cusId = this.getComCusIdByName(cusName);
                BigDecimal contAmt = null;//合同金额
                String contStartDate =StringUtils.EMPTY;
                String contEndDate =StringUtils.EMPTY;
                TmpWydContractTmp wydContract = tmpWydContractTmpMapper.selectByPrimaryKey(contNo);
                if(wydContract != null){

                    if (StringUtils.isEmpty(cusId)){
                        cusId = wydContract.getCustId();
                    }

                    contAmt = new BigDecimal(wydContract.getContractAmt());
                    cusId = StringUtils.isEmpty(cusId) ? wydContract.getCustId() : cusId;
                    //合同起始日
                    contStartDate = StringUtils.isNotEmpty(wydContract.getStartDate())
                            ? DateUtils.formatDate(DateUtils.parseDate(wydContract.getStartDate(),"yyyy/MM/dd"),
                            DateFormatEnum.DEFAULT.getValue())
                            : StringUtils.EMPTY;
                    //合同到期日
                    contEndDate = StringUtils.isNotEmpty(wydContract.getEndDate())
                            ? DateUtils.formatDate(DateUtils.parseDate(wydContract.getEndDate(),"yyyy/MM/dd"),
                            DateFormatEnum.DEFAULT.getValue())
                            : StringUtils.EMPTY;

                }
                BigDecimal pvpAmt = new BigDecimal(wydLoan.getBusinesssum());//本次出账金额
                BigDecimal loanBalance = new BigDecimal(wydLoan.getBalance());//余额
                BigDecimal exchangeRmbAmt = pvpAmt.multiply(exchangeRate);//折合人民币金额
                BigDecimal exchangeRmbBal = loanBalance.multiply(exchangeRate);//折合人民币余额
                //贷款起始日
                String loanStartDate = StringUtils.isNotEmpty(wydLoan.getPutoutdate())
                        ? DateUtils.formatDate(DateUtils.parseDate(wydLoan.getPutoutdate(),DateFormatEnum.DATE_COMPACT.getValue()),
                        DateFormatEnum.DEFAULT.getValue())
                        : StringUtils.EMPTY;
                //贷款到期日
                String loanEndDate = StringUtils.isNotEmpty(wydLoan.getMaturity())
                        ? DateUtils.formatDate(DateUtils.parseDate(wydLoan.getMaturity(),DateFormatEnum.DATE_COMPACT.getValue()),
                        DateFormatEnum.DEFAULT.getValue())
                        : StringUtils.EMPTY;
                TmpWydLoanDetailTmp wydLoanDetail = tmpWydLoanDetailTmpMapper.selectByPrimaryKey(billNo);
                BigDecimal execRateYear = null;//执行年利率
                BigDecimal rulingIr = null;//基准利率（年）
                String irAdjustType = StringUtils.EMPTY;//利率调整方式 01 固定利率
                String loanTer = StringUtils.EMPTY;//贷款投向
                String isBeEntrustedPay = StringUtils.EMPTY;//是否受托支付
                BigDecimal overdueExecRate = null;//逾期执行利率(年利率)
                BigDecimal rateFloatPoint = null;//利率浮动点数
                String agriLoanTer = StringUtils.EMPTY;
                String accStatus = StringUtils.EMPTY;//台账状态
                String settlDate = StringUtils.EMPTY;//结清日期(终止)
                int overdueDay = 0;//逾期天數
                if(wydLoanDetail != null){
                    if(StringUtils.isNotEmpty(wydLoanDetail.getRate())) {
                        execRateYear = new BigDecimal(wydLoanDetail.getRate());
                        if(execRateYear.compareTo(new BigDecimal("1")) == 1){
                            execRateYear = execRateYear.divide(new BigDecimal("100"));
                        }
                        isBeEntrustedPay = "01".equals(wydLoanDetail.getPayWay()) ? CmisCommonConstants.YES_NO_1 : CmisCommonConstants.YES_NO_0;
                        overdueExecRate = execRateYear.multiply(new BigDecimal("1.5"));
                        rateFloatPoint = execRateYear.subtract(curtLprRate).multiply(new BigDecimal("10000"));
                    }
                    if(StringUtils.isNotEmpty(wydLoanDetail.getBaseRate())) {
                        rulingIr = new BigDecimal(wydLoanDetail.getBaseRate());
                        if(rulingIr.compareTo(new BigDecimal("1")) == 1){
                            rulingIr = rulingIr.divide(new BigDecimal("100"));
                        }
                    }
                    if(StringUtils.isNotEmpty(wydLoanDetail.getRateType())) {
                        if("01".equals(wydLoanDetail.getRateType())){
                            irAdjustType = "01";
                        }else{
                            irAdjustType = "02";
                        }
                    }
                    loanTer = wydLoanDetail.getLoanPurpose();
                    settlDate  = StringUtils.isNotEmpty(wydLoanDetail.getPaidOutDate())
                            ? DateUtils.formatDate(DateUtils.parseDate(wydLoanDetail.getPaidOutDate(),"yyyy/MM/dd"),
                            DateFormatEnum.DEFAULT.getValue())
                            : StringUtils.EMPTY;;
                    if ("01".equals(wydLoanDetail.getAgrSptType())){
                        agriLoanTer = "F06";
                    }else if ("02".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F07";
                    }else if ("03".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F08";
                    }else if ("04".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F09";
                    }else if ("05".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F10";
                    }else if ("06".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F11";
                    }else if ("07".equals(wydLoanDetail.getAgrSptType())) {
                        agriLoanTer = "F12";
                    }else{
                        agriLoanTer = wydLoanDetail.getAgrSptType();
                    }
                    if(StringUtils.isNotEmpty(wydLoanDetail.getCardState())){
                        if("0".equals(wydLoanDetail.getCardState())){
                            accStatus = CmisCommonConstants.ACC_STATUS_1;
                        }else if("1".equals(wydLoanDetail.getCardState())){
                            accStatus = CmisCommonConstants.ACC_STATUS_2;
                        }else if("2".equals(wydLoanDetail.getCardState())){
                            accStatus = CmisCommonConstants.ACC_STATUS_0;
                        }else if("3".equals(wydLoanDetail.getCardState())){
                            accStatus = CmisCommonConstants.ACC_STATUS_5;
                        }else if("4".equals(wydLoanDetail.getCardState())){
                            accStatus = CmisCommonConstants.ACC_STATUS_6;
                        }
                    }
                    if(StringUtils.isNotEmpty(wydLoanDetail.getOverdueDays())){
                        overdueDay = Integer.parseInt(wydLoanDetail.getOverdueDays());
                    }

                    //保存或更新借据逾期天数信息
                    if(overdueDay > 0) {
                        this.saveOrUpdateWydOverdueDays(batchDate, billNo, cusId, overdueDay);
                    }
                }

                if(StringUtils.isEmpty(accStatus)){
                    if("01".equals(wydLoan.getLoanstatus())){
                        accStatus = CmisCommonConstants.ACC_STATUS_1;
                    }else if("02".equals(wydLoan.getLoanstatus())  ||  "03".equals(wydLoan.getLoanstatus())){
                        accStatus = CmisCommonConstants.ACC_STATUS_2;
                    }else if("04".equals(wydLoan.getLoanstatus())){
                        accStatus = CmisCommonConstants.ACC_STATUS_0;
                    }else if("05".equals(wydLoan.getLoanstatus())){
                        accStatus = CmisCommonConstants.ACC_STATUS_6;
                    }else if("06".equals(wydLoan.getStatus())){
                        accStatus = CmisCommonConstants.ACC_STATUS_5;
                    }
                }

                String loanTrem = wydLoan.getTremmonth();
                String prdId = this.getWydPrdInfoByTrem(Integer.parseInt(loanTrem)).get("prdId");//产品编号
                String prdName = this.getWydPrdInfoByTrem(Integer.parseInt(loanTrem)).get("prdName");//产品名称
                String repayMode = this.transitionRepayWay(wydLoan.getCorpuspaymethod());//還款方式
                String replySerno = "PF" + wydContract.getLimitNo();//批复流水号
                String lmtAccNo = "EDPZ" + wydContract.getLimitNo() + "010";//授信额度编号
                String loanSubjectNo = StringUtils.EMPTY;//科目号
                TmpWydIouIetmTmp tmpWydIouIetmTmp = tmpWydIouIetmTmpMapper.selectByPrimaryKey(billNo);
                if(tmpWydIouIetmTmp != null){
                    loanSubjectNo = tmpWydIouIetmTmp.getIetmCd();
                }

                if("04".equals(wydLoan.getLoanstatus())){//借據結清時，更新合同狀態為註銷
                    CtrLoanCont ctrLoanCont = ctrLoanContMapper.queryCtrLoanContInfoByContNo(wydLoan.getContractNo());
                    if(ctrLoanCont != null){
                        ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_600);//合同狀態，600 註銷
                        ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
                    }
                }


                //1、保存或更新出账申请信息
                PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPvpLoanSernoKey(pvpSerno);
                if(pvpLoanApp == null){
                    exists = false;
                    pvpLoanApp = new PvpLoanApp();
                    pvpLoanApp.setPvpSerno(pvpSerno);
                    pvpLoanApp.setContNo(contNo);
                    pvpLoanApp.setBillNo(billNo);
                    pvpLoanApp.setContCurType(curType);
                    pvpLoanApp.setCurType(curType);
                    pvpLoanApp.setPrdTypeProp(prdTypeProp);
                    pvpLoanApp.setLoanModal(loanModal);
                    pvpLoanApp.setGuarMode(guarWay);
                    pvpLoanApp.setOprType(oprType);
                    pvpLoanApp.setIrFloatRate(irFloatRate);
                    pvpLoanApp.setLoanUseType(loanUseType);
                    pvpLoanApp.setSbsyPerc(sbsyPerc);
                    pvpLoanApp.setIsSbsy(isSbsy);
                    pvpLoanApp.setApproveStatus(approveStatus);
                    pvpLoanApp.setDeductDay(deductDay);
                    pvpLoanApp.setEiIntervalUnit(eiIntervalUnit);
                    pvpLoanApp.setDeductType(deductType);
                    pvpLoanApp.setNextRateAdjInterval(nextRateAdjInterval);
                    pvpLoanApp.setNextRateAdjUnit(nextRateAdjUnit);
                    pvpLoanApp.setOverdueRatePefloat(overdueRatePefloat);
                    pvpLoanApp.setFtp("0");
                    pvpLoanApp.setLoanTypeDetail("08");//款类别细分 08 普通-经营贷款
                    pvpLoanApp.setGuarDetailMode("30");//担保方式细分 30 信用-其它
                    pvpLoanApp.setIsSteelLoan("0");//是否钢贸行业贷款
                    pvpLoanApp.setIsStainlessLoan("0");//是否不锈钢行业贷款
                    pvpLoanApp.setIsPovertyReliefLoan("0");//是否扶贫贴息贷款
                    pvpLoanApp.setIsLaborIntenSbsyLoan("0");//是否劳动密集型小企业贴息贷款
                    pvpLoanApp.setIsCphsRurDelpLoan("0");//是否农村综合开发贷款标志
                    pvpLoanApp.setGoverSubszHouseLoan("00");//保障性安居工程贷款
                    pvpLoanApp.setEngyEnviProteLoan("00");//项目贷款节能环保
                    pvpLoanApp.setIsOperPropertyLoan("0");//是否经营性物业贷款
                    pvpLoanApp.setRealproLoan("00");//房地产贷款
                    pvpLoanApp.setBelgLine(belgLine);
                    pvpLoanApp.setManagerId(managerId);
                    pvpLoanApp.setManagerBrId(managerBrId);
                    pvpLoanApp.setFinaBrId(finaBrId);
                    pvpLoanApp.setFinaBrIdName(finaBrName);
                    pvpLoanApp.setDisbOrgNo(finaBrId);//放款机构号
                    pvpLoanApp.setDisbOrgName(finaBrName);//放款机构名称
                    pvpLoanApp.setInputId(managerId);
                    pvpLoanApp.setInputBrId(managerBrId);
                    pvpLoanApp.setUpdId(managerId);
                    pvpLoanApp.setUpdBrId(managerBrId);
                    pvpLoanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    pvpLoanApp.setCreateTime(DateUtils.getCurrDate());
                }else{
                    exists = true;
                }
                pvpLoanApp.setReplyNo(replySerno);
                pvpLoanApp.setLmtAccNo(lmtAccNo);
                pvpLoanApp.setCusId(cusId);
                pvpLoanApp.setCusName(cusName);
                pvpLoanApp.setPrdId(prdId);
                pvpLoanApp.setPrdName(prdName);
                pvpLoanApp.setContAmt(contAmt);
                pvpLoanApp.setPvpAmt(pvpAmt);
                pvpLoanApp.setExchangeRate(exchangeRate);
                pvpLoanApp.setLoanStartDate(loanStartDate);
                pvpLoanApp.setLoanEndDate(loanEndDate);
                pvpLoanApp.setStartDate(contStartDate);
                pvpLoanApp.setEndDate(contEndDate);
                pvpLoanApp.setLoanTer(loanTer);
                pvpLoanApp.setExecRateYear(execRateYear);
                pvpLoanApp.setLprRateIntval(lprPriceInterval);
                pvpLoanApp.setCurtLprRate(curtLprRate);
                pvpLoanApp.setRateFloatPoint(rateFloatPoint);
                pvpLoanApp.setLoanTerm(loanTrem);
                pvpLoanApp.setAppTerm(loanTrem);
                pvpLoanApp.setLoanTermUnit(termType);
                pvpLoanApp.setRepayMode(repayMode);
                pvpLoanApp.setRateAdjMode(irAdjustType);
                pvpLoanApp.setIsBeEntrustedPay(isBeEntrustedPay);
                pvpLoanApp.setOverdueExecRate(overdueExecRate);
                pvpLoanApp.setAgriLoanTer(agriLoanTer);
                pvpLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                pvpLoanApp.setUpdateTime(DateUtils.getCurrDate());
                pvpLoanApp.setLoanSubjectNo(loanSubjectNo);
                if(exists){
                    pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
                }else{
                    pvpLoanAppMapper.insertSelective(pvpLoanApp);
                }
                //2、保存或更新借据信息
                AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(billNo);
                if(accLoan == null){
                    exists = false;
                    accLoan = new AccLoan();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(pvpLoanApp, accLoan);
                    accLoan.setPkId(pkId);
                    accLoan.setFiveClass("10");//五级分类
                    accLoan.setTenClass("13");//十级分类
                }else{
                    accLoan.setBillNo(null);
                    exists = true;
                }
                accLoan.setReplyNo(replySerno);
                accLoan.setLmtAccNo(lmtAccNo);
                accLoan.setCusId(cusId);
                accLoan.setCusName(cusName);
                accLoan.setPrdId(prdId);
                accLoan.setPrdName(prdName);
                accLoan.setLoanStartDate(loanStartDate);
                accLoan.setLoanEndDate(loanEndDate);
                accLoan.setLoanTer(loanTer);
                accLoan.setExecRateYear(execRateYear);
                accLoan.setLprRateIntval(lprPriceInterval);
                accLoan.setCurtLprRate(curtLprRate);
                accLoan.setRateFloatPoint(rateFloatPoint);
                accLoan.setLoanTerm(loanTrem);
                accLoan.setLoanTermUnit(termType);
                accLoan.setRepayMode(repayMode);
                accLoan.setRateAdjMode(irAdjustType);
                accLoan.setIsBeEntrustedPay(isBeEntrustedPay);
                accLoan.setOverdueExecRate(overdueExecRate);
                accLoan.setAgriLoanTer(agriLoanTer);
                accLoan.setLoanAmt(pvpAmt);
                accLoan.setLoanBalance(loanBalance);
                accLoan.setExchangeRmbAmt(exchangeRmbAmt);
                accLoan.setExchangeRmbBal(exchangeRmbBal);
                accLoan.setAccStatus(accStatus);
                accLoan.setSettlDate(settlDate);
                accLoan.setRulingIr(rulingIr);
                accLoan.setSubjectNo(loanSubjectNo);
                accLoan.setOverdueDay(overdueDay);
                accLoan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                accLoan.setUpdateTime(DateUtils.getCurrDate());
                if(exists){
                    accLoanMapper.updateByPrimaryKeySelective(accLoan);
                }else{
                    accLoanMapper.insertSelective(accLoan);
                }
            }
        }
        logger.info("保存或更新微业贷业务信息【出账申请：pvp_loan_app、借据信息：acc_loan】结束<<<");
    }


    /**
     * 保存或更新微业贷业务信息--授信信息
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional
    public void saveOrUpdateWydLmtInfo() {
        logger.info("保存或更新微业贷业务信息【授信信息：lmt_app、lmt_app_sub、lmt_app_sub_prd、lmt_reply、" +
                "lmt_reply_sub、lmt_reply_sub_prd、lmt_reply_acc、lmt_reply_acc_sub、lmt_reply_acc_sub_prd" +
                "、appr_str_mtable_info、appr_lmt_sub_basic_info】开始>>>");
        List<TmpWydCreditlineTmp> wydCreditlineList = tmpWydCreditlineTmpMapper.selectAll();
        if (!CollectionUtils.isEmpty(wydCreditlineList)) {
            boolean exists;//记录是否存在标记
            for (TmpWydCreditlineTmp wydCreditline : wydCreditlineList) {

                String serno = wydCreditline.getLimitNo();//流水号
                String replySerno = "PF" + serno;//批复流水号
                String replySubSerno = "PFFX" + serno;//批复分项流水号
                String subSerno = "SQFX" + serno;//授信申请分项流水号
                String replySubPrdSerno = "PFFXPZ" + serno + "010";
                String subPrdSerno = "SQFXPZ" + serno + "010";
                String accNo = "" + serno;//台账号(老信贷授信协议编号)
                String accSubNo = "ED" + serno;
                String accSubPrdNo = "EDPZ" + serno + "010";
                String lmtBizTypeProp = "P042";//授信品种类型属性 P042 微业贷
                String curType = CmisCommonConstants.CUR_TYPE_CNY;//币种 wydCreditline.getCcyCd()
                String guarMode = CmisCommonConstants.GUAR_MODE_00;//担保方式 00 信用
                String cusType = "211";//客户类型
                Integer lmtGraperTerm = 0;//宽限期
                BigDecimal lowRiskTotalLmtAmt = new BigDecimal(0);//低风险额度
                String isPreLmt = CmisCommonConstants.YES_NO_0;//是否预授信额度 0 否
                String isRevolvLimit = CmisCommonConstants.YES_NO_0;//是否循环额度 1 是
                String oprType = CmisBizConstants.OPR_TYPE_01;//操作类型 01	新增
                String replyStatus;//批复状态 01：生效 02：失效
                String isSfcaLmt = CmisCommonConstants.YES_NO_0;//是否存量授信标志 0-否
                String finalApprBrType = "03";//终审机构 03 总行
                String lmtType = "01";//业务类型 01：授信新增
                String isGrp = CmisCommonConstants.YES_NO_0;//是否集团授信 0-否
                BigDecimal bailPreRate = new BigDecimal(0);//保证金预留比例
                String cusName = wydCreditline.getCustName();//客户名称
                //根据客户名称查询客户号
                String cusId = this.getComCusIdByName(cusName);
                if (StringUtils.isEmpty(cusId)){
                    cusId = wydCreditline.getCustId();
                }
                if ("01".equals(wydCreditline.getStatus())) {//00:登记中,01:有效,02:到期未结清失效
                    replyStatus = "01";
                } else {
                    replyStatus = "02";
                }
                //敞口额度合计
                BigDecimal openTotalLmtAmt = new BigDecimal(StringUtils.isNotEmpty(wydCreditline.getCreditLine()) ? wydCreditline.getCreditLine() : "0");
                String replyInureDate = wydCreditline.getStartDate();//批复生效日期
                String endDate = wydCreditline.getMaturityDate();//到期日
                int lmtTerm = DateUtils.getMonthsByTwoDates(replyInureDate, endDate, DateFormatEnum.DEFAULT.getValue());//授信期限
                String lmtBizType = this.getWydPrdInfoByTrem(lmtTerm).get("prdId");
                String lmtBizTypeName = this.getWydPrdInfoByTrem(lmtTerm).get("prdName");;
                String subName = "" + lmtBizTypeName;//分项名称
                String managerId = CmisBizConstants.WYD_MANAGER_ID;//客户经理
                String managerBrId = CmisBizConstants.WYD_MANAGER_BR_ID;//管户机构
                String updDate = wydCreditline.getExtendDate();//最近修改日期
                //1、保存或更新授信批复表信息 LMT_APP
                LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
                if (lmtApp == null) {
                    exists = false;
                    lmtApp = new LmtApp();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    lmtApp.setPkId(pkId);
                    lmtApp.setSerno(serno);
                    lmtApp.setCusType(cusType);
                    lmtApp.setLmtGraperTerm(lmtGraperTerm);
                    lmtApp.setLowRiskTotalLmtAmt(lowRiskTotalLmtAmt);
                    lmtApp.setLmtType("01");//授信类型 01：授信新增
                    lmtApp.setIsGrp(isGrp);//
                    lmtApp.setOprType(oprType);
                    lmtApp.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);//审批状态 997：通过
                    lmtApp.setManagerId(managerId);
                    lmtApp.setManagerBrId(managerBrId);
                    lmtApp.setInputId(managerId);
                    lmtApp.setInputBrId(managerBrId);
                    lmtApp.setUpdId(managerId);
                    lmtApp.setUpdBrId(managerBrId);
                    lmtApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    lmtApp.setCreateTime(DateUtils.getCurrDate());
                } else {
                    exists = true;
                }
                lmtApp.setCusName(cusName);
                lmtApp.setCusId(cusId);
                lmtApp.setOpenTotalLmtAmt(openTotalLmtAmt);
                lmtApp.setCurType(curType);
                lmtApp.setLmtTerm(lmtTerm);
                lmtApp.setUpdDate(updDate);
                lmtApp.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtAppMapper.updateByPrimaryKeySelective(lmtApp);
                } else {
                    lmtAppMapper.insertSelective(lmtApp);
                }

                //2、保存或更新授信批复分项明细表 LMT_APP_SUB
                LmtAppSub lmtAppSub = lmtAppSubMapper.selectBySubSerno(subSerno);
                if (lmtAppSub == null) {
                    exists = false;
                    lmtAppSub = new LmtAppSub();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    lmtAppSub.setPkId(pkId);
                    lmtAppSub.setSerno(serno);
                    //lmtAppSub.setReplySerno(replySerno);
                    //lmtAppSub.setReplySubSerno(replySubSerno);
                    lmtAppSub.setSubSerno(subSerno);
                    lmtAppSub.setSubName(subName);
                    lmtAppSub.setGuarMode(guarMode);
                    lmtAppSub.setIsPreLmt(isPreLmt);
                    lmtAppSub.setIsRevolvLimit(isRevolvLimit);
                    lmtAppSub.setOprType(oprType);
                    lmtAppSub.setInputId(managerId);
                    lmtAppSub.setInputBrId(managerBrId);
                    lmtAppSub.setUpdId(managerId);
                    lmtAppSub.setUpdBrId(managerBrId);
                    lmtAppSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    lmtAppSub.setCreateTime(DateUtils.getCurrDate());
                } else {
                    exists = true;
                }
                lmtAppSub.setCusName(cusName);
                lmtAppSub.setCusId(cusId);
                lmtAppSub.setLmtAmt(openTotalLmtAmt);
                lmtAppSub.setCurType(curType);
                lmtAppSub.setLmtTerm(lmtTerm);
                lmtAppSub.setUpdDate(updDate);
                lmtAppSub.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtAppSubMapper.updateByPrimaryKeySelective(lmtAppSub);
                } else {
                    lmtAppSubMapper.insertSelective(lmtAppSub);
                }
                //3、保存或更新授信批复分项适用品种明细表 LMT_APP_SUB_PRD
                LmtAppSubPrd lmtAppSubPrd = lmtAppSubPrdMapper.selectBySubPrdSerno(subPrdSerno);
                if (lmtAppSubPrd == null) {
                    exists = false;
                    lmtAppSubPrd = new LmtAppSubPrd();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    lmtAppSubPrd.setPkId(pkId);
                    //lmtAppSubPrd.setReplySubSerno(replySubSerno);
                    //lmtAppSubPrd.setReplySubPrdSerno(replySubPrdSerno);
                    lmtAppSubPrd.setSubPrdSerno(subPrdSerno);
                    lmtAppSubPrd.setSubSerno(subSerno);
                    lmtAppSubPrd.setLmtBizTypeProp(lmtBizTypeProp);
                    lmtAppSubPrd.setIsPreLmt(isPreLmt);
                    lmtAppSubPrd.setIsRevolvLimit(isRevolvLimit);
                    lmtAppSubPrd.setBailPreRate(bailPreRate);
                    lmtAppSubPrd.setLmtGraperTerm(lmtGraperTerm);
                    lmtAppSubPrd.setOprType(oprType);
                    lmtAppSubPrd.setInputId(managerId);
                    lmtAppSubPrd.setInputBrId(managerBrId);
                    lmtAppSubPrd.setUpdId(managerId);
                    lmtAppSubPrd.setUpdBrId(managerBrId);
                    lmtAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    lmtAppSubPrd.setCreateTime(DateUtils.getCurrDate());
                } else {
                    exists = true;
                }
                lmtAppSubPrd.setCusName(cusName);
                lmtAppSubPrd.setCusId(cusId);
                lmtAppSubPrd.setLmtBizType(lmtBizType);
                lmtAppSubPrd.setLmtBizTypeName(lmtBizTypeName);
                lmtAppSubPrd.setLmtAmt(openTotalLmtAmt);
                lmtAppSubPrd.setCurType(curType);
                lmtAppSubPrd.setLmtTerm(lmtTerm);
                lmtAppSubPrd.setIsSfcaLmt(isSfcaLmt);

                lmtAppSubPrd.setUpdDate(updDate);
                lmtAppSubPrd.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtAppSubPrdMapper.updateByPrimaryKeySelective(lmtAppSubPrd);
                } else {
                    lmtAppSubPrdMapper.insertSelective(lmtAppSubPrd);
                }

                //4、保存或更新授信批复表信息 LMT_REPLY
                LmtReply lmtReply = lmtReplyMapper.getReply(replySerno);
                if (lmtReply == null) {
                    exists = false;
                    lmtReply = new LmtReply();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(lmtApp, lmtReply);
                    lmtReply.setPkId(pkId);
                    lmtReply.setReplySerno(replySerno);
                } else {
                    exists = true;
                }

                lmtReply.setCusName(cusName);
                lmtReply.setCusId(cusId);
                lmtReply.setOpenTotalLmtAmt(openTotalLmtAmt);
                lmtReply.setCurType(curType);
                lmtReply.setReplyInureDate(replyInureDate);
                lmtReply.setLmtTerm(lmtTerm);
                lmtReply.setReplyStatus(replyStatus);
                lmtReply.setUpdDate(updDate);
                lmtReply.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplyMapper.updateByPrimaryKeySelective(lmtReply);
                } else {
                    lmtReplyMapper.insertSelective(lmtReply);
                }

                //5、保存或更新授信批复分项明细表 LMT_REPLY_SUB
                LmtReplySub lmtReplySub = lmtReplySubMapper.queryLmtReplySubByReplySubSerno(replySubSerno);
                if (lmtReplySub == null) {
                    exists = false;
                    lmtReplySub = new LmtReplySub();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(lmtAppSub, lmtReplySub);
                    lmtReplySub.setPkId(pkId);
                    lmtReplySub.setSerno(serno);
                    lmtReplySub.setReplySerno(replySerno);
                    lmtReplySub.setReplySubSerno(replySubSerno);
                } else {
                    exists = true;
                    lmtReplySub.setSerno(null);//an not update the shardkey
                }
                lmtReplySub.setCusName(cusName);
                lmtReplySub.setCusId(cusId);
                lmtReplySub.setLmtAmt(openTotalLmtAmt);
                lmtReplySub.setCurType(curType);
                lmtReplySub.setLmtTerm(lmtTerm);
                lmtReplySub.setUpdDate(updDate);
                lmtReplySub.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplySubMapper.updateByPrimaryKeySelective(lmtReplySub);
                } else {
                    lmtReplySubMapper.insertSelective(lmtReplySub);
                }
                //6、保存或更新授信批复分项适用品种明细表 LMT_REPLY_SUB_PRD
                LmtReplySubPrd lmtReplySubPrd = lmtReplySubPrdMapper.queryLmtReplySubPrdByReplySubPrdSerno(replySubPrdSerno);
                if (lmtReplySubPrd == null) {
                    exists = false;
                    lmtReplySubPrd = new LmtReplySubPrd();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(lmtAppSubPrd, lmtReplySubPrd);
                    lmtReplySubPrd.setPkId(pkId);
                    lmtReplySubPrd.setReplySubSerno(replySubSerno);
                    lmtReplySubPrd.setReplySubPrdSerno(replySubPrdSerno);
                } else {
                    exists = true;
                }
                lmtReplySubPrd.setCusName(cusName);
                lmtReplySubPrd.setCusId(cusId);
                lmtReplySubPrd.setLmtBizType(lmtBizType);
                lmtReplySubPrd.setLmtBizTypeName(lmtBizTypeName);
                lmtReplySubPrd.setLmtAmt(openTotalLmtAmt);
                lmtReplySubPrd.setCurType(curType);
                lmtReplySubPrd.setLmtTerm(lmtTerm);
                lmtReplySubPrd.setIsSfcaLmt(isSfcaLmt);
                lmtReplySubPrd.setUpdDate(updDate);
                lmtReplySubPrd.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplySubPrdMapper.updateByPrimaryKeySelective(lmtReplySubPrd);
                } else {
                    lmtReplySubPrdMapper.insertSelective(lmtReplySubPrd);
                }

                //7、保存或更新授信批复台账表 LMT_REPLY_ACC
                int lmtReplayAccNum = lmtReplyAccMapper.getCountLmtReplayAccNoDateBycusId(cusId);

                LmtReplyAcc lmtReplyAcc = lmtReplyAccMapper.selectBySerno(serno);
                if (lmtReplyAcc == null) {
                    lmtReplyAcc = new LmtReplyAcc();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(false);
                    BeanUtils.copyProperties(lmtReply, lmtReplyAcc);
                    lmtReplyAcc.setPkId(pkId);
                    lmtReplyAcc.setAccNo(accNo);
                    exists = false;
                } else {
                    exists = true;
                }
                lmtReplyAcc.setCusName(cusName);
                lmtReplyAcc.setCusId(cusId);
                lmtReplyAcc.setOpenTotalLmtAmt(openTotalLmtAmt);
                lmtReplyAcc.setCurType(curType);
                lmtReplyAcc.setLmtTerm(lmtTerm);
                lmtReplyAcc.setLmtType(lmtType);
                lmtReplyAcc.setFinalApprBrType(finalApprBrType);
                lmtReplyAcc.setIsGrp(isGrp);
                lmtReplyAcc.setInureDate(replyInureDate);
                lmtReplyAcc.setAccStatus(replyStatus);
                lmtReplyAcc.setUpdDate(updDate);
                lmtReplyAcc.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplyAccMapper.updateByPrimaryKeySelective(lmtReplyAcc);
                } else {
                    if(lmtReplayAccNum == 0){//如果此客户存在有效的授信批复台账，不处理
                        lmtReplyAccMapper.insertSelective(lmtReplyAcc);
                    }
                }

                //8、保存或更新授信批复台账分项明细表 LMT_REPLY_ACC_SUB
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubMapper.selectByAccSubNo(accSubNo);
                if (lmtReplyAccSub == null) {
                    exists = false;
                    lmtReplyAccSub = new LmtReplyAccSub();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(lmtReplySub, lmtReplyAccSub);
                    lmtReplyAccSub.setPkId(pkId);
                    lmtReplyAccSub.setSerno(serno);
                    lmtReplyAccSub.setAccSubNo(accSubNo);
                    lmtReplyAccSub.setAccNo(accNo);
                } else {
                    exists = true;
                }
                lmtReplyAccSub.setCusName(cusName);
                lmtReplyAccSub.setCusType(cusType);
                lmtReplyAccSub.setCusId(cusId);
                lmtReplyAccSub.setAccStatus(replyStatus);
                lmtReplyAccSub.setLmtAmt(openTotalLmtAmt);
                lmtReplyAccSub.setCurType(curType);
                lmtReplyAccSub.setLmtTerm(lmtTerm);
                lmtReplyAccSub.setUpdDate(updDate);
                lmtReplyAccSub.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplyAccSubMapper.updateByPrimaryKeySelective(lmtReplyAccSub);
                } else {
                    if(lmtReplayAccNum == 0) {//如果此客户存在有效的授信批复台账，不处理
                        lmtReplyAccSubMapper.insertSelective(lmtReplyAccSub);
                    }
                }

                //9、授信批复分项适用品种明细表 ： LMT_REPLY_ACC_SUB_PRD
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByAccSubPrdNo(accSubPrdNo);
                if (lmtReplyAccSubPrd == null) {
                    exists = false;
                    lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(lmtReplySubPrd, lmtReplyAccSubPrd);
                    lmtReplyAccSubPrd.setPkId(pkId);
                    lmtReplyAccSubPrd.setAccSubNo(accSubNo);
                    lmtReplyAccSubPrd.setAccSubPrdNo(accSubPrdNo);
                } else {
                    exists = true;
                }
                lmtReplyAccSubPrd.setCusName(cusName);
                lmtReplyAccSubPrd.setCusId(cusId);
                lmtReplyAccSubPrd.setCusType(cusType);
                lmtReplyAccSubPrd.setLmtAmt(openTotalLmtAmt);
                lmtReplyAccSubPrd.setCurType(curType);
                lmtReplyAccSubPrd.setLmtTerm(lmtTerm);
                lmtReplyAccSubPrd.setUpdDate(updDate);
                lmtReplyAccSubPrd.setUpdateTime(DateUtils.getCurrDate());
                if (exists) {
                    lmtReplyAccSubPrdMapper.updateByPrimaryKeySelective(lmtReplyAccSubPrd);
                } else {
                    if(lmtReplayAccNum == 0) {//如果此客户存在有效的授信批复台账，不处理
                        lmtReplyAccSubPrdMapper.insertSelective(lmtReplyAccSubPrd);
                    }
                }
                //10 额度批复主信息 APPR_STR_MTABLE_INFO
                BatApprStrMtableInfo batApprStrMtableInfo = tmpWydCommMapper.getBatApprStrMtableByAppSerno(accNo);
                if(batApprStrMtableInfo == null){
                    exists = false;
                    batApprStrMtableInfo = new BatApprStrMtableInfo();
                    String asmPkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    batApprStrMtableInfo.setPkId(asmPkId);//主键
                    batApprStrMtableInfo.setApprSerno(accNo);
                    batApprStrMtableInfo.setInputId(managerId);
                    batApprStrMtableInfo.setInputBrId(managerBrId);
                    batApprStrMtableInfo.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    batApprStrMtableInfo.setCreateTime(DateUtils.getCurrDate());
                } else {
                    exists = true;
                }

                batApprStrMtableInfo.setCusId(cusId);
                batApprStrMtableInfo.setCusName(cusName);
                batApprStrMtableInfo.setCusType("2");
                batApprStrMtableInfo.setApprStatus(replyStatus);
                batApprStrMtableInfo.setInstuCde("C1115632000023");//张家港农村商业银行
                batApprStrMtableInfo.setTerm(lmtTerm);
                batApprStrMtableInfo.setStartDate(replyInureDate);
                batApprStrMtableInfo.setEndDate(endDate);
                batApprStrMtableInfo.setLmtType("01");//单一客户额度
                batApprStrMtableInfo.setLmtMode("01");//综合授信
                batApprStrMtableInfo.setOprType(oprType);
                batApprStrMtableInfo.setUpdId(managerId);
                batApprStrMtableInfo.setUpdBrId(managerBrId);
                batApprStrMtableInfo.setUpdDate(updDate);
                batApprStrMtableInfo.setUpdateTime(DateUtils.getCurrDate());
                if(exists) {
                    tmpWydCommMapper.updateBatApprStrMtableInfoByPrimaryKeySelective(batApprStrMtableInfo);
                }else{
                    if(lmtReplayAccNum == 0) {//如果此客户存在有效的授信批复台账，不处理
                        tmpWydCommMapper.insertBatApprStrMtableInfo(batApprStrMtableInfo);
                    }
                }

                BatApprLmtSubBasicInfo batApprLmtSubBasicInfo = tmpWydCommMapper.getBatApprLmtSubBasicInfoBySerno(accSubNo);
                if(batApprLmtSubBasicInfo ==null){
                    exists = false;
                    batApprLmtSubBasicInfo = new BatApprLmtSubBasicInfo();
                    String altsbPkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    batApprLmtSubBasicInfo.setPkId(altsbPkId);
                    batApprLmtSubBasicInfo.setFkPkid(accNo);
                    batApprLmtSubBasicInfo.setApprSubSerno(accSubNo);
                    batApprLmtSubBasicInfo.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    batApprLmtSubBasicInfo.setCreateTime(DateUtils.getCurrDate());
                    batApprLmtSubBasicInfo.setInputId(managerId);
                    batApprLmtSubBasicInfo.setInputBrId(managerBrId);
                }else{
                    exists = true;
                }

                batApprLmtSubBasicInfo.setCusId(cusId);
                batApprLmtSubBasicInfo.setCusName(cusName);
                batApprLmtSubBasicInfo.setCusType("2");
                batApprLmtSubBasicInfo.setCurType(curType);
                String limitSubNo = this.getLmtSubNoByPrdId(lmtBizType);
                batApprLmtSubBasicInfo.setLimitSubNo(limitSubNo);
                batApprLmtSubBasicInfo.setLimitSubName(lmtBizTypeName);
                batApprLmtSubBasicInfo.setLmtBizTypeProp(lmtBizTypeProp);
                batApprLmtSubBasicInfo.setIsLriskLmt(CmisCommonConstants.YES_NO_0);
                batApprLmtSubBasicInfo.setLmtSubType("01");//01 额度分项 02 产品分项
                batApprLmtSubBasicInfo.setIsRevolv(isRevolvLimit);
                batApprLmtSubBasicInfo.setLimitType("01");//02 专项 01	综合
                batApprLmtSubBasicInfo.setSuitGuarWay(guarMode);
                batApprLmtSubBasicInfo.setLmtDate(endDate);
                batApprLmtSubBasicInfo.setLmtGraper(0);
                batApprLmtSubBasicInfo.setAvlAmt(openTotalLmtAmt);
                //batApprLmtSubBasicInfo.setOutstndAmt(openTotalLmtAmt);
                batApprLmtSubBasicInfo.setSpacAmt(openTotalLmtAmt);
                //batApprLmtSubBasicInfo.setSpacOutstndAmt(openTotalLmtAmt);
                //batApprLmtSubBasicInfo.setLoanBalance(openTotalLmtAmt);
                batApprLmtSubBasicInfo.setStatus("01");
                batApprLmtSubBasicInfo.setCurType(curType);//币种
                batApprLmtSubBasicInfo.setBailPreRate(bailPreRate);//保证金预留比例
                //TODO batApprLmtSubBasicInfo.setRateYear();
                //batApprLmtSubBasicInfo.setLmtAmtAdd(openTotalLmtAmt);
                //batApprLmtSubBasicInfo.setPvpOutstndAmt(openTotalLmtAmt);
                batApprLmtSubBasicInfo.setAvlOutstndAmt(new BigDecimal("0"));
                batApprLmtSubBasicInfo.setIsPreCrd(CmisCommonConstants.YES_NO_0);
                batApprLmtSubBasicInfo.setIsIvlMf(CmisCommonConstants.YES_NO_0);
                batApprLmtSubBasicInfo.setStartDate(replyInureDate);
                batApprLmtSubBasicInfo.setTerm(lmtTerm);
                batApprLmtSubBasicInfo.setLoanSpacBalance(new BigDecimal("0"));
                batApprLmtSubBasicInfo.setOprType(oprType);
                batApprLmtSubBasicInfo.setUpdId(managerId);
                batApprLmtSubBasicInfo.setUpdBrId(managerBrId);
                batApprLmtSubBasicInfo.setUpdDate(updDate);
                batApprLmtSubBasicInfo.setUpdateTime(DateUtils.getCurrDate());
                if(exists) {
                    tmpWydCommMapper.updateBatApprLmtSubBasicInfoByPrimaryKeySelective(batApprLmtSubBasicInfo);
                }else{
                    if(lmtReplayAccNum == 0) {//如果此客户存在有效的授信批复台账，不处理
                        tmpWydCommMapper.insertBatApprLmtSubBasicInfo(batApprLmtSubBasicInfo);
                    }
                }
                Map param = new HashMap();
                param.put("cusId",cusId);
                param.put("isRevolv",isRevolvLimit);
                param.put("limitSubNo",limitSubNo);
                param.put("lmtBizTypeProp",lmtBizTypeProp);
                BatApprLmtSubBasicInfo batApprLmtSubPrdBasicInfo = tmpWydCommMapper.getWydApprLmtSubBasicInfoByCus(param);
                if(batApprLmtSubPrdBasicInfo == null){
                    exists = false;
                    batApprLmtSubPrdBasicInfo = new BatApprLmtSubBasicInfo();
                    String altsbpPkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(Boolean.FALSE);
                    BeanUtils.copyProperties(batApprLmtSubBasicInfo, batApprLmtSubPrdBasicInfo);
                    batApprLmtSubPrdBasicInfo.setPkId(altsbpPkId);
                    batApprLmtSubPrdBasicInfo.setParentId(accSubNo);

                    batApprLmtSubPrdBasicInfo.setApprSubSerno(accSubPrdNo);
                }else{
                    exists = true;
                }
                batApprLmtSubPrdBasicInfo.setAvlAmt(openTotalLmtAmt);
                batApprLmtSubPrdBasicInfo.setSpacAmt(openTotalLmtAmt);
                batApprLmtSubPrdBasicInfo.setLmtSubType("02");//01 额度分项 02 产品分项
                batApprLmtSubPrdBasicInfo.setUpdId(managerId);
                batApprLmtSubPrdBasicInfo.setUpdBrId(managerBrId);
                batApprLmtSubPrdBasicInfo.setUpdDate(updDate);
                batApprLmtSubPrdBasicInfo.setUpdateTime(DateUtils.getCurrDate());

                if(exists) {
                    tmpWydCommMapper.updateBatApprLmtSubBasicInfoByPrimaryKeySelective(batApprLmtSubPrdBasicInfo);
                }else{
                    if(lmtReplayAccNum == 0) {//如果此客户存在有效的授信批复台账，不处理
                        tmpWydCommMapper.insertBatApprLmtSubBasicInfo(batApprLmtSubPrdBasicInfo);
                    }
                }
                
            }
        }
        logger.info("保存或更新微业贷业务信息【授信信息：lmt_app、lmt_app_sub、lmt_app_sub_prd、lmt_reply、" +
                "lmt_reply_sub、lmt_reply_sub_prd、lmt_reply_acc、lmt_reply_acc_sub、lmt_reply_acc_sub_prd" +
                "、appr_str_mtable_info、appr_lmt_sub_basic_info】结束<<<");
    }


    /**
     * 微业贷风险分类1
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void riskClass1ForWyd() {
        List<Map> riskClassResultList = tmpWydCommMapper.getWydRiskClassResult();
        logger.info("月末微业贷风险分类1任务开始>>>");
        //1、普通规则
        int count = 0;//计数器
        for (Map riskClassResult : riskClassResultList ) {

            String cusId = StringUtils.isNotEmpty(riskClassResult.get("cusId").toString()) ? riskClassResult.get("cusId").toString() : StringUtils.EMPTY;
            String fiveClass = StringUtils.isNotEmpty(riskClassResult.get("fiveClass").toString()) ? riskClassResult.get("fiveClass").toString() : StringUtils.EMPTY;
            String tenClass = StringUtils.isNotEmpty(riskClassResult.get("tenClass").toString()) ? riskClassResult.get("tenClass").toString() : StringUtils.EMPTY;
            String classDate = StringUtils.isNotEmpty(riskClassResult.get("classDate").toString()) ? riskClassResult.get("classDate").toString() : StringUtils.EMPTY;
            if(StringUtils.isNotEmpty(fiveClass) && StringUtils.isNotEmpty(tenClass)){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("cusId",cusId);
                queryModel.addCondition("prdTypeProp","P042");
                List<AccLoan> accLoanList = accLoanMapper.querymodelByCondition(queryModel);
                boolean modifyFlag = false;
                for (AccLoan accLoan: accLoanList) {
                    if("0".equals(accLoan.getAccStatus()) || "7".equals(accLoan.getAccStatus())){
                        continue;
                    }
                    if((Integer.parseInt(accLoan.getFiveClass())<Integer.parseInt(fiveClass))) {
                        accLoan.setFiveClass(fiveClass);
                        modifyFlag = true;
                    }
                    if((Integer.parseInt(accLoan.getTenClass())<Integer.parseInt(tenClass))){
                        accLoan.setTenClass(tenClass);
                        modifyFlag = true;
                    }
                    if(modifyFlag){
                        accLoan.setClassDate(classDate);
                        accLoan.setUpdateTime(DateUtils.getCurrDate());
                        accLoan.setBillNo(null);
                        accLoanMapper.updateByPrimaryKeySelective(accLoan);
                        count ++;
                        logger.info("微业贷风险分类任务1更新：" + cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(accLoan));
                    }

                }
            }
        }

        logger.info("月末微业贷风险分类1任务结束,共更新" + count + "条记录。>>>");

    }
    /**
    * 微业贷风险分类2
    * @author zrcbank-fengjj
    * @throws Exception
    */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void riskClass2ForWyd() {
        logger.info("月末微业贷风险分类任务2开始>>>");
        int count = 0;//计数器
        //2、进入次级及以下的客户，连续六个月正常还款之后，五十级分类回到关注、关注3
        List<Map> notOverdueDaysResultList = tmpWydCommMapper.getWydNotOverdueDays();
        for (Map riskClassResult : notOverdueDaysResultList ) {
            String cusId = StringUtils.isNotEmpty(riskClassResult.get("cusId").toString()) ? riskClassResult.get("cusId").toString() : StringUtils.EMPTY;
            String days = StringUtils.isNotEmpty(riskClassResult.get("days").toString()) ? riskClassResult.get("days").toString() : "0";
            if(Integer.parseInt(days) >= 180){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("cusId",cusId);
                queryModel.addCondition("prdTypeProp","P042");
                //queryModel.addCondition("fiveClass","30");
                //查出该客户所有借据
                List<AccLoan> accLoanList = accLoanMapper.querymodelByCondition(queryModel);
                String date = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                for (AccLoan accLoan: accLoanList) {
                    if("0".equals(accLoan.getAccStatus()) || "7".equals(accLoan.getAccStatus())){
                        continue;
                    }
                    accLoan.setFiveClass("20");
                    accLoan.setTenClass("21");
                    accLoan.setClassDate(date);
                    accLoan.setUpdateTime(DateUtils.getCurrDate());
                    accLoan.setBillNo(null);
                    accLoanMapper.updateByPrimaryKeySelective(accLoan);
                    count ++;
                    logger.info("微业贷风险分类任务2更新：" + cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(accLoan));
                    //次级转正常后逾期天数记录表中增加一条记录
                    this.saveOrUpdateWydOverdueDays(date,accLoan.getBillNo(),cusId,0);
                }

            }
        }
        logger.info("月末微业贷风险分类2任务结束,共更新" + count + "条记录。>>>");
    }

    /**
     * 微业贷风险分类3
     * @author zrcbank-fengjj
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void riskClass3ForWyd() throws Exception {
        logger.info("月末微业贷风险分类任务3开始>>>");
        int count = 0;//计数器
        //进入关注类的客户，连续三个月正常还款之后，五十级分类回到正常、正常3
        List<Map> notOverdueDaysResultList = tmpWydCommMapper.getWydNotOverdueDays();
        for (Map riskClassResult : notOverdueDaysResultList ) {
            String cusId = StringUtils.isNotEmpty(riskClassResult.get("cusId").toString()) ? riskClassResult.get("cusId").toString() : StringUtils.EMPTY;
            String days = StringUtils.isNotEmpty(riskClassResult.get("days").toString()) ? riskClassResult.get("days").toString() : "0";
            if(Integer.parseInt(days) >= 90){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("cusId",cusId);
                queryModel.addCondition("prdTypeProp","P042");
                //queryModel.addCondition("fiveClass","20");
                //查出该客户所有借据
                List<AccLoan> accLoanList = accLoanMapper.querymodelByCondition(queryModel);
                for (AccLoan accLoan: accLoanList) {
                    if("0".equals(accLoan.getAccStatus()) || "7".equals(accLoan.getAccStatus())){
                        continue;
                    }
                    accLoan.setFiveClass("10");
                    accLoan.setTenClass("13");
                    accLoan.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    accLoan.setUpdateTime(DateUtils.getCurrDate());
                    accLoan.setBillNo(null);
                    count ++;
                    logger.info("微业贷风险分类任务3更新：" + cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(accLoan));
                    accLoanMapper.updateByPrimaryKeySelective(accLoan);
                }
            }
        }
        logger.info("月末微业贷风险分类任务3结束,共更新" + count + "条记录。>>>");
    }

    /**
     * 连接FTP下载文件
     * @author zdl
     * @throws Exception
     */
    String downRemoteFiles(String username, String password, String host, String localPath, String remotePath, String port, String batchdate) throws Exception{
        String remotePathParent = remotePath + batchdate;
        remotePath = remotePathParent+"/EAST";
        Session sshSession = null;
        boolean connect1 = false;
        ChannelSftp sftp = null;
        OutputStream output = null ;
        JSch jsch = new JSch();
        // 文件下载成功标志
        String flag = "fail";
        try {
            int tryCountAll = 5;
            int tryCount = 0;
            while(tryCount++ < tryCountAll && !connect1){
                if (tryCount>1){
                    try {
                        Thread.sleep(5000*(tryCount-1));
                    } catch (Exception e) {
                        throw new Exception("连接FTP现在文件失败！");
                    }
                    System.out.println("第"+tryCount+"次尝试连接");
                }
                sshSession = jsch.getSession(username, host, Integer.parseInt(port));

                sshSession.setPassword(password);
                Properties properties = new Properties();
                properties.put("StrictHostKeyChecking", "no");
                sshSession.setConfig(properties);
                sshSession.connect(50000);

                //获取sftp连接
                sftp =  (ChannelSftp) sshSession.openChannel("sftp");
                sftp.connect(50000);
                connect1= true;
            }
            File file = new File(localPath);
            if(!file.exists()){
                if(!file.mkdirs()){
                    logger.info("本地文件夹："+localPath+"创建失败");
                    throw new Exception("本地文件夹创建失败");
                }
            }
            //下载 LoanAfter LendingNumber
            sftp.cd(remotePathParent);
            Vector<?> v1 = sftp.ls(remotePathParent);
            if (v1.size() > 0) {
                Iterator<?> it1 = v1.iterator();
                while(it1.hasNext()){
                    LsEntry lsentry1 =   (LsEntry) it1.next();
                    String fileName1  = lsentry1.getFilename();
                    if ("LoanAfter".equalsIgnoreCase(fileName1) || "LendingNumber".equalsIgnoreCase(fileName1)){
                        File file2 = new File(localPath+fileName1+".txt"); //原文件转换为 txt文件格式
                        if(file2.exists()){
                            file2.delete();
                        }
                        file2.createNewFile();
                        output = new FileOutputStream(file2);
                        sftp.get(fileName1, output);
                    }
                }
            }
            //进入远程文件路径
            sftp.cd(remotePath);
            //文件夹下所有文件集合
            Vector<?> v = sftp.ls(remotePath);
            if(v.size() > 0){
                Iterator<?> it = v.iterator();
                while(it.hasNext()){
                    LsEntry lsentry =   (LsEntry) it.next();
                    String fileName  = lsentry.getFilename();
                    if (!".".equals(fileName) && !"..".equals(fileName) && !fileName.endsWith("OK") && !fileName.endsWith("zip")){
                        File file2 = new File(localPath+fileName+".txt"); //原文件转换为 txt文件格式
                        if(file2.exists()){
                            file2.delete();
                        }
                        file2.createNewFile();
                        output = new FileOutputStream(file2);
                        sftp.get(fileName, output);
                    }
                }
                flag = "success";
            }else{
                logger.info("微业贷文件未生成成功");
                throw new Exception("微业贷文件未生成成功");
            }
        }finally{
            try {
                if (output != null) {
                    output.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

            if (sftp.isConnected()) {
                sftp.disconnect();
            }
            if (sshSession.isConnected()) {
                sshSession.disconnect();
            }
        }
        return flag;
    }


    /**
     * 根据期限获取微业贷产品信息prdId、prdName
     * @author zrcbank-fengjj
     * @param trem 期限
     * @return 产品信息
     */
    private Map<String,String> getWydPrdInfoByTrem(int trem){
        Map<String,String> prdInfo = new HashMap<>();
        if(trem <= 12){
            prdInfo.put("prdId","012007");
            prdInfo.put("prdName","短期流动资金贷款");
        }else{
            prdInfo.put("prdId","012008");
            prdInfo.put("prdName","中长期流动资金贷款");
        }
        return prdInfo;
    }

    /**
     * 根据产品号转化额度分项编号
     * @param prdId
     * @return
     */
    private String  getLmtSubNoByPrdId(String prdId){
        String lmtSubNo = "";
        if("012007".equals(prdId)){
            lmtSubNo = "10010101";
        }else if("012008".equals(prdId)){
            lmtSubNo = "10010102";
        }
        return lmtSubNo;
    }


    /**
     * 根据客户名称获取对公客户号：调用客户服务接口xdkh0002
     * @author zrcbank-fengjj
     * @param cusName 客户名称
     * @return cusId 客户号
     */
    private String getComCusIdByName(String cusName){
        Xdkh0002DataReqDto xdkh0002DataReqDto = new Xdkh0002DataReqDto();
        xdkh0002DataReqDto.setCusName(cusName);
        xdkh0002DataReqDto.setQueryType("02");
        xdkh0002DataReqDto.setCertCode("");
        Xdkh0002DataRespDto xdkh0002DataRespDto = xdkh0002Service.xdkh0002(xdkh0002DataReqDto);
        String cusId = (xdkh0002DataRespDto != null && StringUtils.isNotEmpty(xdkh0002DataRespDto.getCusId()))
                ? xdkh0002DataRespDto.getCusId() : StringUtils.EMPTY;//客户号
        if(StringUtils.isEmpty(cusId)){
            logger.info("客户【" + cusName + "】，在客户服务中未查询到客户号！");
        }
        return cusId;
    }

    /**
     * 校验微业贷借据合规信息（客户号不合法，科目号为空）
     * @param bizDate 交易日期
     * @param msgPhones 要发短信的手机号
     */
    public void checkWydAccLoanInfo(String bizDate, String msgPhones){
        try{
            int abnormalWydAccLoanCount = tmpWydCommMapper.getWydAccLoanByBizDate(bizDate);
            if(abnormalWydAccLoanCount > 0){
                if(StringUtils.isNotEmpty(msgPhones)){
                    String msg = "微业贷台账表中存在客户号不合法或者科目号为空，请及时处理！";
                    String[] msgPhoneArr = msgPhones.split(",");
                    if(msgPhoneArr.length > 0){
                        List<SenddxReqList> senddxReqList = new ArrayList<>();
                        for (String msgPhone : msgPhoneArr) {
                            SenddxReqList senddxReq = new SenddxReqList();
                            senddxReq.setMobile(msgPhone);
                            senddxReq.setSmstxt(msg);
                            senddxReqList.add(senddxReq);
                        }
                        logger.info("发送的短信内容：" + JSON.toJSONString(senddxReqList));

                        SenddxReqDto senddxReqDto = new SenddxReqDto();
                        senddxReqDto.setInfopt("dx");
                        senddxReqDto.setSenddxReqList(senddxReqList);
                        try {
                            senddxService.senddx(senddxReqDto);
                        }catch(Exception e){
                            logger.info("发送短信失败：" + e.getMessage());
                        }
                    }
                }
            }
        }catch(Exception e){
            logger.info("校验微业贷借据合规信息异常信息：" + e.getMessage());
        }
    }

    /**
     * 跟據LPR類型和幣種查詢當前LPR利率
     * @author zrcbank-fengjj
     * @param lprType LPR類型
     * @param curType 幣種
     * @return 當前LPR利率
     */
    private BigDecimal getCurtLprRateByLprType(String lprType,String curType){
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("rateTypeId",lprType);
        queryModel.getCondition().put("rateTypeName","10");
        queryModel.getCondition().put("curType",curType);
        ResultDto<CfgLprRateDto> resultDto= cmisCfgClientService.selectone(queryModel);
        return resultDto.getData().getRate();
    }


    /**
     * 還款方式裝換
     * @author zrcbank-fengjj
     * @param wydRepayWay 微業貸文件原始還款方式
     * @return 信貸還款方式
     */
    private String transitionRepayWay(String wydRepayWay){

        String repayMode = StringUtils.EMPTY;
        if ("RPT-02".equals(wydRepayWay)
                || "RPT-22".equals(wydRepayWay)
                || "RPT-24".equals(wydRepayWay)) {
            repayMode = "A003";
        } else if ("RPT-03".equals(wydRepayWay)) {
            repayMode = "A009";
        } else if ("RPT-04".equals(wydRepayWay)) {
            repayMode = "A001";
        }
        return repayMode;
    }

    /**
     * 判断该日期是否是月末
     * @author zrcbank-fengjj
     * @param date 日期：yyyy-MM-dd
     * @return 是否月末
     */
    public boolean isLastDayOfMonth(String date){

        Date d = DateUtils.parseDate(date,DateFormatEnum.DEFAULT.getValue());
        int year  = DateUtils.getYear(d);//年
        int month = DateUtils.getMonth(d);//月
        int day = DateUtils.get(d,Calendar.DATE);//日
        int totalDays = DateUtils.getMonthTotalDays(year,month);//月份总天数

        if(day == totalDays){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 保存或更新微业贷借据状态历史
     * @author zrcbank-fengjj
     * @param dataDt 数据日期
     * @param billNo 借据号
     * @param cusId 客户号
     * @param consOverdueDays 借据逾期天数
     */
    public void saveOrUpdateWydOverdueDays(String dataDt,String billNo,String cusId,int consOverdueDays){
        TmpWydLoanOverdue tmpWydLoanOverdue = tmpWydLoanOverdueMapper.selectByPrimaryKey(dataDt,billNo);
        if(tmpWydLoanOverdue == null){
            tmpWydLoanOverdue = new TmpWydLoanOverdue();
            tmpWydLoanOverdue.setDataDt(dataDt);
            tmpWydLoanOverdue.setLendingRef(billNo);
            tmpWydLoanOverdue.setCusId(cusId);
            tmpWydLoanOverdue.setConsOverdueDays(consOverdueDays);
            tmpWydLoanOverdueMapper.insertSelective(tmpWydLoanOverdue);
        }else{
            tmpWydLoanOverdue.setConsOverdueDays(consOverdueDays);
            tmpWydLoanOverdueMapper.updateByPrimaryKeySelective(tmpWydLoanOverdue);
        }
    }
    /**
     * 批量更新创建人、创建时间等字段
     * @author zdl
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void batUpdate8Colum(String tableName,String batchDate){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sf.format(new Date());
        //INPUT_ID,INPUT_BR_ID,INPUT_DATE,UPD_ID,UPD_BR_ID,UPD_DATE,CREATE_TIME,UPDATE_TIME
        String inputId = CmisBizConstants.WYD_MANAGER_ID;
        String inputBrId = CmisBizConstants.WYD_MANAGER_BR_ID;
        String inputDate = batchDate;
        String updId = CmisBizConstants.WYD_MANAGER_ID;
        String updBrId = CmisBizConstants.WYD_MANAGER_BR_ID;
        String updDate = batchDate;
        String createTime = time;
        String updateTime = time;

        tmpWydCommMapper.batchUpdate(tableName,inputId,inputBrId,inputDate,updId,updBrId,updDate,createTime,updateTime);
    }


    /**
     * 发送微业贷跑批短信
     * @author zrcbank-fengjj
     * @param msgPhones 手机号
     * @param batStatus 跑批状态
     * @param bizDate 跑批日期
     * @throws Exception
     */
    public void sendWydBatchMsg(String msgPhones, String batStatus, String bizDate){

        if(StringUtils.isNotEmpty(msgPhones)){
            String msg = "微业贷批量执行FLAG！批量日期为：" + bizDate + "，请知悉！";
            if("100".equals(batStatus)){//跑批成功
                msg = msg.replace("FLAG","成功");
            }else{
                msg = msg.replace("FLAG","失败");
            }
            String[] msgPhoneArr = msgPhones.split(",");
            if(msgPhoneArr.length > 0){
                List<SenddxReqList> senddxReqList = new ArrayList<>();
                for (String msgPhone : msgPhoneArr) {
                    SenddxReqList senddxReq = new SenddxReqList();
                    senddxReq.setMobile(msgPhone);
                    senddxReq.setSmstxt(msg);
                    senddxReqList.add(senddxReq);
                }

                logger.info("发送的短信内容：" + JSON.toJSONString(senddxReqList));

                SenddxReqDto senddxReqDto = new SenddxReqDto();
                senddxReqDto.setInfopt("dx");
                senddxReqDto.setSenddxReqList(senddxReqList);
                try {
                    senddxService.senddx(senddxReqDto);
                }catch(Exception e){
                    logger.info("发送短信失败：" + e.getMessage());
                }
            }
        }
    }

}
