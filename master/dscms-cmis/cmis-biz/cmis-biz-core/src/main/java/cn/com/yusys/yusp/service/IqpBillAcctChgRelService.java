/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpBillAcctChgRel;
import cn.com.yusys.yusp.repository.mapper.IqpBillAcctChgRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillAcctChgRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-25 09:09:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpBillAcctChgRelService {

    @Autowired
    private IqpBillAcctChgRelMapper iqpBillAcctChgRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpBillAcctChgRel selectByPrimaryKey(String pkId) {
        return iqpBillAcctChgRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpBillAcctChgRel> selectAll(QueryModel model) {
        List<IqpBillAcctChgRel> records = (List<IqpBillAcctChgRel>) iqpBillAcctChgRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpBillAcctChgRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpBillAcctChgRel> list = iqpBillAcctChgRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpBillAcctChgRel record) {
        return iqpBillAcctChgRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpBillAcctChgRel record) {
        return iqpBillAcctChgRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpBillAcctChgRel record) {
        return iqpBillAcctChgRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpBillAcctChgRel record) {
        return iqpBillAcctChgRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpBillAcctChgRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpBillAcctChgRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertIqpBillAcctChgRelDataByBillNo
     * @方法描述: 根据借据号插入还款账号信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertIqpBillAcctChgRelDataByBillNo(HashMap param) {
        return iqpBillAcctChgRelMapper.insertIqpBillAcctChgRelDataByBillNo(param);
    }

    /**
     * @方法名称: selectByParam
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpBillAcctChgRel> selectByParam(HashMap param) {
        List<IqpBillAcctChgRel> records = (List<IqpBillAcctChgRel>) iqpBillAcctChgRelMapper.selectByParam(param);
        return records;
    }

    /**
     * @方法名称: updateByIqpSernoWithDelete
     * @方法描述: 根据申请流水号执行 删除 操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByIqpSernoWithDelete(String iqpSerno) {
        return iqpBillAcctChgRelMapper.updateByIqpSernoWithDelete(iqpSerno);
    }
}
