package cn.com.yusys.yusp.web.server.xdzc0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0017.req.Xdzc0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.resp.Xdzc0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0017.Xdzc0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:新增购销合同申请撤回
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0017:新增购销合同申请撤回")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0017Resource.class);

    @Autowired
    private Xdzc0017Service xdzc0017Service;
  /**
     * 交易码：xdzc0017
     * 交易描述：新增购销合同申请撤回
     *
     * @param xdzc0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新增购销合同申请撤回")
    @PostMapping("/xdzc0017")
    protected @ResponseBody
    ResultDto<Xdzc0017DataRespDto> xdzc0017(@Validated @RequestBody Xdzc0017DataReqDto xdzc0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017DataReqDto));
        Xdzc0017DataRespDto xdzc0017DataRespDto = new Xdzc0017DataRespDto();// 响应Dto:新增购销合同申请撤回
        ResultDto<Xdzc0017DataRespDto> xdzc0017DataResultDto = new ResultDto<>();
        String cusId = xdzc0017DataReqDto.getCusId();//客户号
        String contNo = xdzc0017DataReqDto.getContNo();//合同编号
        try {
            xdzc0017DataRespDto = xdzc0017Service.xdzc0017Service(xdzc0017DataReqDto);
            xdzc0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, e.getMessage());
            // 封装xdzc0017DataResultDto中异常返回码和返回信息
            xdzc0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0017DataRespDto到xdzc0017DataResultDto中
        xdzc0017DataResultDto.setData(xdzc0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017DataResultDto));
        return xdzc0017DataResultDto;
    }
}
