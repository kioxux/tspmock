package cn.com.yusys.yusp.web.server.xdxw0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0022.req.Xdxw0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0022.resp.Xdxw0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0022.Xdxw0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
/**
 * 接口处理类:根据证件号查询信贷系统的申请信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0022:根据证件号查询信贷系统的申请信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0022Resource.class);

    @Autowired
    private Xdxw0022Service xdxw0022Service;

    /**
     * 交易码：xdxw0022
     * 交易描述：根据证件号查询信贷系统的申请信息
     *
     * @param xdxw0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据证件号查询信贷系统的申请信息")
    @PostMapping("/xdxw0022")
    protected @ResponseBody
    ResultDto<Xdxw0022DataRespDto> xdxw0022(@Validated @RequestBody Xdxw0022DataReqDto xdxw0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataReqDto));
        Xdxw0022DataRespDto xdxw0022DataRespDto = new Xdxw0022DataRespDto();// 响应Dto:根据证件号查询信贷系统的申请信息
        ResultDto<Xdxw0022DataRespDto> xdxw0022DataResultDto = new ResultDto<>();
        String certNo = xdxw0022DataReqDto.getCertNo();//证件号
        try {
            if (StringUtils.isEmpty(certNo)) {
                xdxw0022DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
                return xdxw0022DataResultDto;
            }
            xdxw0022DataRespDto = xdxw0022Service.xdxw0022(xdxw0022DataReqDto);
            xdxw0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, e.getMessage());
            // 封装xdxw0022DataResultDto中异常返回码和返回信息
            xdxw0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0022DataRespDto到xdxw0022DataResultDto中
        xdxw0022DataResultDto.setData(xdxw0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataResultDto));
        return xdxw0022DataResultDto;
    }
}
