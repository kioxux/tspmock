/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CoopPartnerLstInfo;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysMcd;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysMcdMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-23 23:15:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysMcdService {

    @Autowired
    private RptSpdAnysMcdMapper rptSpdAnysMcdMapper;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private CoopPartnerLstInfoService coopPartnerLstInfoService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysMcd selectByPrimaryKey(String serno) {
        return rptSpdAnysMcdMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptSpdAnysMcd> selectAll(QueryModel model) {
        List<RptSpdAnysMcd> records = (List<RptSpdAnysMcd>) rptSpdAnysMcdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysMcd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysMcd> list = rptSpdAnysMcdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysMcd record) {
        return rptSpdAnysMcdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysMcd record) {
        return rptSpdAnysMcdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysMcd record) {
        return rptSpdAnysMcdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysMcd record) {
        return rptSpdAnysMcdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptSpdAnysMcdMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysMcdMapper.deleteByIds(ids);
    }

    public RptSpdAnysMcd initMcd(Map map) {
        String serno = map.get("serno").toString();
        String cusId = map.get("cusId").toString();
        RptSpdAnysMcd result = new RptSpdAnysMcd();
        RptSpdAnysMcd rptSpdAnysMcd = rptSpdAnysMcdMapper.selectByPrimaryKey(serno);
        if (Objects.nonNull(rptSpdAnysMcd)) {
            return rptSpdAnysMcd;
        } else {
            List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectSubPrdBySerno(serno);
            if (CollectionUtils.nonEmpty(lmtAppSubPrds)) {
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                    if ("P019".equals(lmtAppSubPrd.getLmtBizTypeProp())) {
                        if (lmtAppSubPrd.getPartnerNo() != null) {
                            CoopPartnerLstInfo coopPartnerLstInfo = coopPartnerLstInfoService.selectByPrimaryKey(lmtAppSubPrd.getPartnerNo());
                            if (coopPartnerLstInfo.getPartnerType().equals(CmisBizConstants.STD_PARTNER_TYPE_4)) {
                                QueryModel queryModel = new QueryModel();
                                queryModel.addCondition("cusId", coopPartnerLstInfo.getPartnerNo());
                                //查询合作方额度
                                ResultDto<List<Map<String, Object>>> listResultDto = cmisLmtClientService.queryListByInstuCde(queryModel);
                                if(listResultDto!=null&&listResultDto.getData()!=null){
                                    List<Map<String, Object>> data = listResultDto.getData();
                                    if(CollectionUtils.nonEmpty(data)){
                                        Map<String,Object> temp = data.get(0);
                                        result.setPartnerAmt(BigDecimal.valueOf(Double.parseDouble(temp.get("totalAmt").toString())));
                                        result.setOutstndAmt(BigDecimal.valueOf(Double.parseDouble(temp.get("totalUseAmt").toString())));
                                        result.setLeftAmt(BigDecimal.valueOf(Double.parseDouble(temp.get("totalValAmt").toString())));
                                    }
                                }
                                result.setSerno(serno);
                                result.setPartnerId(coopPartnerLstInfo.getPartnerNo());
                                result.setPartnerName(coopPartnerLstInfo.getPartnerName());
                                int count = rptSpdAnysMcdMapper.insert(result);
                                if (count > 0) {
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
