/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.AccAccp;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.AccEntrustLoan;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccEntrustLoanMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 21:23:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccEntrustLoanMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccEntrustLoan selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccEntrustLoan> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccEntrustLoan record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccEntrustLoan record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccEntrustLoan record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccEntrustLoan record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称：selectForAccEntrustLoanInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:21
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccEntrustLoan> selectForAccEntrustLoanInfo(@Param("cusId") String cusId);

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:57
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccEntrustLoan> selectByContNo(@Param("contNo") String contNo);


    /**
     * @方法名称：selectAccEntrustLoanBySernoKey
     * @方法描述：
     * @创建人：zxz
     * @创建时间：2021/5/21 16:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    AccEntrustLoan selectAccEntrustLoanBySernoKey(String serno) ;

    /**
     * @param contNo
     * @return countAccLoanCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    int countEntrustLoanCountByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称：selectAccEntrustLoanByBillNo
     * @方法描述：
     * @创建人：
     * @创建时间：2021/6/6 00:09
     * @修改记录：修改时间 修改人员 修改时间
     */
    AccEntrustLoan selectAccEntrustLoanByBillNo(@Param("billNo") String billNo) ;

    /**
     * 根据借据号查询可还款的委托贷款台账
     * @param billNo
     * @param managerId
     * @return
     */
    List<AccEntrustLoan> selectRepayAccEntrustLoanByParam(@Param("billNo") String billNo, @Param("managerId") String managerId);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId")String cusId);

    /**
     * @param contNo
     * @return selectLoanSumAmtByContNo
     * @desc  根据合同编号获取当前合同的委托贷款用信金额
     * @修改历史:
     */
    BigDecimal selectLoanSumAmtByContNo(@Param("contNo") String contNo);
    /**
     * @Description:根据借据编号更新委托贷款台账信息
     * @Date: 2021/6/13 23:40
     * @param queryMap: 借据编号
     * @return: int
     **/
    int updateAccEntrustLoanByBillNo(Map<String, String> queryMap);


    /**
     * @方法名称: queryByBillNo
     * @方法描述: 根据借据编号查询委托贷款台账数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccEntrustLoan queryByBillNo(@Param("billNo") String billNo);

    /**
     * 根据查询条件查询台账信息并返回
     * @param model
     * @return
     */
    List<AccEntrustLoan> querymodelByCondition(QueryModel model);

    /**
     * 风险分类审批结束更新客户未结清委托贷款台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:38
     **/
    int updateLoanFiveAndTenClassByCusId(Map<String, String> map);
}