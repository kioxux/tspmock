package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveInfo
 * @类描述: doc_archive_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 17:11:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocArchiveInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 档案流水号 **/
	private String docSerno;
	
	/** 档案编号 **/
	private String docNo;
	
	/** 归档模式 **/
	private String archiveMode;
	
	/** 档案分类 **/
	private String docClass;
	
	/** 档案类型 **/
	private String docType;
	
	/** 关联业务编号 **/
	private String bizSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 任务标识 **/
	private String taskFlag;
	
	/** 档案状态 **/
	private String docStauts;
	
	/** 生成日期 **/
	private String createDate;
	
	/** 完成日期 **/
	private String finishDate;
	
	/** 入库操作人 **/
	private String optUsr;
	
	/** 入库操作机构 **/
	private String optOrg;
	
	/** 入库操作时间 **/
	private String storageDate;
	
	/** 删除原因 **/
	private String delReason;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 资料总页数 **/
	private String totalPage;
	
	/** 库存位置 **/
	private String location;
	
	/** 贷款账号 **/
	private String loanNo;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 贷款期限(月) **/
	private String loanTerm;
	
	/** 账户状态 **/
	private String accStatus;
	
	/** 委托扣款账号 **/
	private String entrustedDeductNo;
	
	/** 放款日期 **/
	private String encashDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 联系电话 **/
	private String telephone;
	
	/** 电话 **/
	private String phone;

	/** 合同编号 **/
	private String contNo;

	/** 借据编号 **/
	private String billNo;

	/** 档案业务品种 **/
	private String docBizType;

	/** 起始日期 **/
	private String startDate;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno == null ? null : docSerno.trim();
	}
	
    /**
     * @return DocSerno
     */	
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo == null ? null : docNo.trim();
	}
	
    /**
     * @return DocNo
     */	
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param archiveMode
	 */
	public void setArchiveMode(String archiveMode) {
		this.archiveMode = archiveMode == null ? null : archiveMode.trim();
	}
	
    /**
     * @return ArchiveMode
     */	
	public String getArchiveMode() {
		return this.archiveMode;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass == null ? null : docClass.trim();
	}
	
    /**
     * @return DocClass
     */	
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType == null ? null : docType.trim();
	}
	
    /**
     * @return DocType
     */	
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param taskFlag
	 */
	public void setTaskFlag(String taskFlag) {
		this.taskFlag = taskFlag == null ? null : taskFlag.trim();
	}
	
    /**
     * @return TaskFlag
     */	
	public String getTaskFlag() {
		return this.taskFlag;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts == null ? null : docStauts.trim();
	}
	
    /**
     * @return DocStauts
     */	
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate == null ? null : createDate.trim();
	}
	
    /**
     * @return CreateDate
     */	
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param finishDate
	 */
	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate == null ? null : finishDate.trim();
	}
	
    /**
     * @return FinishDate
     */	
	public String getFinishDate() {
		return this.finishDate;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr == null ? null : optUsr.trim();
	}
	
    /**
     * @return OptUsr
     */	
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg == null ? null : optOrg.trim();
	}
	
    /**
     * @return OptOrg
     */	
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param storageDate
	 */
	public void setStorageDate(String storageDate) {
		this.storageDate = storageDate == null ? null : storageDate.trim();
	}
	
    /**
     * @return StorageDate
     */	
	public String getStorageDate() {
		return this.storageDate;
	}
	
	/**
	 * @param delReason
	 */
	public void setDelReason(String delReason) {
		this.delReason = delReason == null ? null : delReason.trim();
	}
	
    /**
     * @return DelReason
     */	
	public String getDelReason() {
		return this.delReason;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param totalPage
	 */
	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage == null ? null : totalPage.trim();
	}
	
    /**
     * @return TotalPage
     */	
	public String getTotalPage() {
		return this.totalPage;
	}
	
	/**
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location == null ? null : location.trim();
	}
	
    /**
     * @return Location
     */	
	public String getLocation() {
		return this.location;
	}
	
	/**
	 * @param loanNo
	 */
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo == null ? null : loanNo.trim();
	}
	
    /**
     * @return LoanNo
     */	
	public String getLoanNo() {
		return this.loanNo;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm == null ? null : loanTerm.trim();
	}
	
    /**
     * @return LoanTerm
     */	
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param entrustedDeductNo
	 */
	public void setEntrustedDeductNo(String entrustedDeductNo) {
		this.entrustedDeductNo = entrustedDeductNo == null ? null : entrustedDeductNo.trim();
	}
	
    /**
     * @return EntrustedDeductNo
     */	
	public String getEntrustedDeductNo() {
		return this.entrustedDeductNo;
	}
	
	/**
	 * @param encashDate
	 */
	public void setEncashDate(String encashDate) {
		this.encashDate = encashDate == null ? null : encashDate.trim();
	}
	
    /**
     * @return EncashDate
     */	
	public String getEncashDate() {
		return this.encashDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return endDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone == null ? null : telephone.trim();
	}
	
    /**
     * @return Telephone
     */	
	public String getTelephone() {
		return this.telephone;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}

	/**
	 * @return Phone
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}

	/**
	 * @return Phone
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param docBizType
	 */
	public void setDocBizType(String docBizType) {
		this.docBizType = docBizType == null ? null : docBizType.trim();
	}

	/**
	 * @return DocBizType
	 */
	public String getDocBizType() {
		return this.docBizType;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}

	/**
	 * @return StartDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}

	/**
	 * @return PrdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

	/**
	 * @return PrdName
	 */
	public String getPrdName() {
		return this.prdName;
	}
}