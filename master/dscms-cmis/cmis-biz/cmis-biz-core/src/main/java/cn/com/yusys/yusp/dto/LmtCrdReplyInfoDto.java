package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyInfo
 * @类描述: lmt_crd_reply_info数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:43:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCrdReplyInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查编号 **/
	private String surveySerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 客户等级 **/
	private String cusLvl;
	
	/** 担保方式 **/
	private String grtMode;
	
	/** 批复金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 期限类型 **/
	private String termType;
	
	/** 申请期限 **/
	private String applyTerm;
	
	/** 批复起始日 **/
	private String contStartDate;
	
	/** 批复到期日 **/
	private String contEndDate;
	
	/** 执行年利率 **/
	private java.math.BigDecimal rate;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 额度类型 **/
	private String limitType;
	
	/** 本次用信金额 **/
	private java.math.BigDecimal curtLoanAmt;
	
	/** 是否受托支付 **/
	private String truPayFlg;
	
	/** 是否有用信条件 **/
	private String loanCondFlg;
	
	/** 受托类型  **/
	private String truPayType;
	
	/** 审批类型  **/
	private String apprType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 是否无还本续贷 **/
	private String iswxbxd;

	/** 续贷原合同编号 **/
	private String xdOrigiContNo;

	/** 续贷原借据号 **/
	private String xdOrigiBillNo;

	/** 贷款形式 **/
	private String loanModal;


	public String getLoanModal() {
		return loanModal;
	}

	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	public String getXdOrigiBillNo() {
		return xdOrigiBillNo;
	}

	public void setXdOrigiBillNo(String xdOrigiBillNo) {
		this.xdOrigiBillNo = xdOrigiBillNo;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusLvl
	 */
	public void setCusLvl(String cusLvl) {
		this.cusLvl = cusLvl == null ? null : cusLvl.trim();
	}
	
    /**
     * @return CusLvl
     */	
	public String getCusLvl() {
		return this.cusLvl;
	}
	
	/**
	 * @param grtMode
	 */
	public void setGrtMode(String grtMode) {
		this.grtMode = grtMode == null ? null : grtMode.trim();
	}
	
    /**
     * @return GrtMode
     */	
	public String getGrtMode() {
		return this.grtMode;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}
	
    /**
     * @return TermType
     */	
	public String getTermType() {
		return this.termType;
	}
	
	/**
	 * @param applyTerm
	 */
	public void setApplyTerm(String applyTerm) {
		this.applyTerm = applyTerm == null ? null : applyTerm.trim();
	}
	
    /**
     * @return ApplyTerm
     */	
	public String getApplyTerm() {
		return this.applyTerm;
	}
	
	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate == null ? null : contStartDate.trim();
	}
	
    /**
     * @return ContStartDate
     */	
	public String getContStartDate() {
		return this.contStartDate;
	}
	
	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate == null ? null : contEndDate.trim();
	}
	
    /**
     * @return ContEndDate
     */	
	public String getContEndDate() {
		return this.contEndDate;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return Rate
     */	
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType == null ? null : limitType.trim();
	}
	
    /**
     * @return LimitType
     */	
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param curtLoanAmt
	 */
	public void setCurtLoanAmt(java.math.BigDecimal curtLoanAmt) {
		this.curtLoanAmt = curtLoanAmt;
	}
	
    /**
     * @return CurtLoanAmt
     */	
	public java.math.BigDecimal getCurtLoanAmt() {
		return this.curtLoanAmt;
	}
	
	/**
	 * @param truPayFlg
	 */
	public void setTruPayFlg(String truPayFlg) {
		this.truPayFlg = truPayFlg == null ? null : truPayFlg.trim();
	}
	
    /**
     * @return TruPayFlg
     */	
	public String getTruPayFlg() {
		return this.truPayFlg;
	}
	
	/**
	 * @param loanCondFlg
	 */
	public void setLoanCondFlg(String loanCondFlg) {
		this.loanCondFlg = loanCondFlg == null ? null : loanCondFlg.trim();
	}
	
    /**
     * @return LoanCondFlg
     */	
	public String getLoanCondFlg() {
		return this.loanCondFlg;
	}
	
	/**
	 * @param truPayType
	 */
	public void setTruPayType(String truPayType) {
		this.truPayType = truPayType == null ? null : truPayType.trim();
	}
	
    /**
     * @return TruPayType
     */	
	public String getTruPayType() {
		return this.truPayType;
	}
	
	/**
	 * @param apprType
	 */
	public void setApprType(String apprType) {
		this.apprType = apprType == null ? null : apprType.trim();
	}
	
    /**
     * @return ApprType
     */	
	public String getApprType() {
		return this.apprType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param iswxbxd
	 */
	public void setIswxbxd(String iswxbxd) {
		this.iswxbxd = iswxbxd;
	}

	/**
	 * @return iswxbxd
	 */
	public String getIswxbxd() {
		return this.iswxbxd;
	}

	/**
	 * @param xdOrigiContNo
	 */
	public void setXdOrigiContNo(String xdOrigiContNo) {
		this.xdOrigiContNo = xdOrigiContNo;
	}

	/**
	 * @return xdOrigiContNo
	 */
	public String getXdOrigiContNo() {
		return this.xdOrigiContNo;
	}



}