/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.compatible.dto.Obj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptPersonalBusiness;
import cn.com.yusys.yusp.service.RptPersonalBusinessService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptPersonalBusinessResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:13:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptpersonalbusiness")
public class RptPersonalBusinessResource {
    @Autowired
    private RptPersonalBusinessService rptPersonalBusinessService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptPersonalBusiness>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptPersonalBusiness> list = rptPersonalBusinessService.selectAll(queryModel);
        return new ResultDto<List<RptPersonalBusiness>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptPersonalBusiness>> index(QueryModel queryModel) {
        List<RptPersonalBusiness> list = rptPersonalBusinessService.selectByModel(queryModel);
        return new ResultDto<List<RptPersonalBusiness>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptPersonalBusiness> show(@PathVariable("serno") String serno) {
        RptPersonalBusiness rptPersonalBusiness = rptPersonalBusinessService.selectByPrimaryKey(serno);
        return new ResultDto<RptPersonalBusiness>(rptPersonalBusiness);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptPersonalBusiness> create(@RequestBody RptPersonalBusiness rptPersonalBusiness) throws URISyntaxException {
        rptPersonalBusinessService.insert(rptPersonalBusiness);
        return new ResultDto<RptPersonalBusiness>(rptPersonalBusiness);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptPersonalBusiness rptPersonalBusiness) throws URISyntaxException {
        int result = rptPersonalBusinessService.update(rptPersonalBusiness);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptPersonalBusinessService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptPersonalBusinessService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptPersonalBusiness> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptPersonalBusiness>(rptPersonalBusinessService.selectByPrimaryKey(serno));
    }
    @PostMapping("/initPersonal")
    protected ResultDto<RptPersonalBusiness> initPersonal(@RequestBody Map map){
        return new ResultDto<RptPersonalBusiness>(rptPersonalBusinessService.initPersonal(map));
    }
}
