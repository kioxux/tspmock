package cn.com.yusys.yusp.web.server.xdxt0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.dto.server.xdxt0004.req.Xdxt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.Xdxt0004DataRespDto;
import cn.com.yusys.yusp.service.server.xdxt0004.Xdxt0004Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;


/**
 * 接口处理类:根据工号获取所辖区域
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0004:根据工号获取所辖区域")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0004Resource.class);

    @Autowired
    private Xdxt0004Service xdxt0004Service;
    /**
     * 交易码：xdxt0004
     * 交易描述：根据工号获取所辖区域
     *
     * @param xdxt0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据工号获取所辖区域")
    @PostMapping("/xdxt0004")
    protected @ResponseBody
    ResultDto<Xdxt0004DataRespDto> xdxt0004(@Validated @RequestBody Xdxt0004DataReqDto xdxt0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataReqDto));
        Xdxt0004DataRespDto xdxt0004DataRespDto = new Xdxt0004DataRespDto();// 响应Dto:根据工号获取所辖区域
        ResultDto<Xdxt0004DataRespDto> xdxt0004DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0004DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataReqDto));
            xdxt0004DataRespDto = xdxt0004Service.getXdxt0004(xdxt0004DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataRespDto));
            // 封装xdxt0004DataResultDto中正确的返回码和返回信息
            xdxt0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, e.getMessage());
            // 封装xdxt0004DataResultDto中异常返回码和返回信息
            xdxt0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0004DataRespDto到xdxt0004DataResultDto中
        xdxt0004DataResultDto.setData(xdxt0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataResultDto));
        return xdxt0004DataResultDto;
    }
}
