package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtAcc
 * @类描述: lmt_sig_invest_basic_lmt_acc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:24:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestBasicLmtAccDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信台账号 **/
	private String accNo;
	
	/** 底层授信台账号 **/
	private String basicAccNo;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 批复流水号 **/
	private String replySerno;
	
	/** 底层批复流水号 **/
	private String basicReplySerno;
	
	/** 底层申请流水号 **/
	private String basicSerno;
	
	/** 底层客户编号 **/
	private String basicCusId;
	
	/** 底层客户名称 **/
	private String basicCusName;
	
	/** 底层客户大类 **/
	private String basicCusCatalog;
	
	/** 底层客户类型 **/
	private String basicCusType;
	
	/** 底层授信品种编号 **/
	private String basicLmtBizType;
	
	/** 底层授信品种名称 **/
	private String basicLmtBizTypeName;
	
	/** 授信金额 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 授信期限 **/
	private Integer lmtTerm;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 是否大额授信 **/
	private String isLargeLmt;
	
	/** 是否需报备董事长 **/
	private String isReportChairman;
	
	/** 底层基础资产基本情况分析 **/
	private String basicAssetBasicCaseAnaly;
	
	/** 经营情况分析 **/
	private String operCaseAnaly;
	
	/** 其他说明 **/
	private String otherDesc;
	
	/** 其他情况分析 **/
	private String otherCaseAnaly;
	
	/** 同业授信准入 **/
	private String intbankLmtAdmit;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 台账状态 **/
	private String accStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo == null ? null : accNo.trim();
	}
	
    /**
     * @return AccNo
     */	
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param basicAccNo
	 */
	public void setBasicAccNo(String basicAccNo) {
		this.basicAccNo = basicAccNo == null ? null : basicAccNo.trim();
	}
	
    /**
     * @return BasicAccNo
     */	
	public String getBasicAccNo() {
		return this.basicAccNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno == null ? null : replySerno.trim();
	}
	
    /**
     * @return ReplySerno
     */	
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param basicReplySerno
	 */
	public void setBasicReplySerno(String basicReplySerno) {
		this.basicReplySerno = basicReplySerno == null ? null : basicReplySerno.trim();
	}
	
    /**
     * @return BasicReplySerno
     */	
	public String getBasicReplySerno() {
		return this.basicReplySerno;
	}
	
	/**
	 * @param basicSerno
	 */
	public void setBasicSerno(String basicSerno) {
		this.basicSerno = basicSerno == null ? null : basicSerno.trim();
	}
	
    /**
     * @return BasicSerno
     */	
	public String getBasicSerno() {
		return this.basicSerno;
	}
	
	/**
	 * @param basicCusId
	 */
	public void setBasicCusId(String basicCusId) {
		this.basicCusId = basicCusId == null ? null : basicCusId.trim();
	}
	
    /**
     * @return BasicCusId
     */	
	public String getBasicCusId() {
		return this.basicCusId;
	}
	
	/**
	 * @param basicCusName
	 */
	public void setBasicCusName(String basicCusName) {
		this.basicCusName = basicCusName == null ? null : basicCusName.trim();
	}
	
    /**
     * @return BasicCusName
     */	
	public String getBasicCusName() {
		return this.basicCusName;
	}
	
	/**
	 * @param basicCusCatalog
	 */
	public void setBasicCusCatalog(String basicCusCatalog) {
		this.basicCusCatalog = basicCusCatalog == null ? null : basicCusCatalog.trim();
	}
	
    /**
     * @return BasicCusCatalog
     */	
	public String getBasicCusCatalog() {
		return this.basicCusCatalog;
	}
	
	/**
	 * @param basicCusType
	 */
	public void setBasicCusType(String basicCusType) {
		this.basicCusType = basicCusType == null ? null : basicCusType.trim();
	}
	
    /**
     * @return BasicCusType
     */	
	public String getBasicCusType() {
		return this.basicCusType;
	}
	
	/**
	 * @param basicLmtBizType
	 */
	public void setBasicLmtBizType(String basicLmtBizType) {
		this.basicLmtBizType = basicLmtBizType == null ? null : basicLmtBizType.trim();
	}
	
    /**
     * @return BasicLmtBizType
     */	
	public String getBasicLmtBizType() {
		return this.basicLmtBizType;
	}
	
	/**
	 * @param basicLmtBizTypeName
	 */
	public void setBasicLmtBizTypeName(String basicLmtBizTypeName) {
		this.basicLmtBizTypeName = basicLmtBizTypeName == null ? null : basicLmtBizTypeName.trim();
	}
	
    /**
     * @return BasicLmtBizTypeName
     */	
	public String getBasicLmtBizTypeName() {
		return this.basicLmtBizTypeName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param isLargeLmt
	 */
	public void setIsLargeLmt(String isLargeLmt) {
		this.isLargeLmt = isLargeLmt == null ? null : isLargeLmt.trim();
	}
	
    /**
     * @return IsLargeLmt
     */	
	public String getIsLargeLmt() {
		return this.isLargeLmt;
	}
	
	/**
	 * @param isReportChairman
	 */
	public void setIsReportChairman(String isReportChairman) {
		this.isReportChairman = isReportChairman == null ? null : isReportChairman.trim();
	}
	
    /**
     * @return IsReportChairman
     */	
	public String getIsReportChairman() {
		return this.isReportChairman;
	}
	
	/**
	 * @param basicAssetBasicCaseAnaly
	 */
	public void setBasicAssetBasicCaseAnaly(String basicAssetBasicCaseAnaly) {
		this.basicAssetBasicCaseAnaly = basicAssetBasicCaseAnaly == null ? null : basicAssetBasicCaseAnaly.trim();
	}
	
    /**
     * @return BasicAssetBasicCaseAnaly
     */	
	public String getBasicAssetBasicCaseAnaly() {
		return this.basicAssetBasicCaseAnaly;
	}
	
	/**
	 * @param operCaseAnaly
	 */
	public void setOperCaseAnaly(String operCaseAnaly) {
		this.operCaseAnaly = operCaseAnaly == null ? null : operCaseAnaly.trim();
	}
	
    /**
     * @return OperCaseAnaly
     */	
	public String getOperCaseAnaly() {
		return this.operCaseAnaly;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc == null ? null : otherDesc.trim();
	}
	
    /**
     * @return OtherDesc
     */	
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param otherCaseAnaly
	 */
	public void setOtherCaseAnaly(String otherCaseAnaly) {
		this.otherCaseAnaly = otherCaseAnaly == null ? null : otherCaseAnaly.trim();
	}
	
    /**
     * @return OtherCaseAnaly
     */	
	public String getOtherCaseAnaly() {
		return this.otherCaseAnaly;
	}
	
	/**
	 * @param intbankLmtAdmit
	 */
	public void setIntbankLmtAdmit(String intbankLmtAdmit) {
		this.intbankLmtAdmit = intbankLmtAdmit == null ? null : intbankLmtAdmit.trim();
	}
	
    /**
     * @return IntbankLmtAdmit
     */	
	public String getIntbankLmtAdmit() {
		return this.intbankLmtAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}