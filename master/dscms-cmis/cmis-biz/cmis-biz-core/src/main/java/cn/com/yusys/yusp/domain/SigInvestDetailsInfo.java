/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SigInvestDetailsInfo
 * @类描述: sig_invest_details_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 10:10:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "sig_invest_details_info")
public class SigInvestDetailsInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 项目编号 **/
	@Column(name = "PRO_NO", unique = false, nullable = false, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 200)
	private String proName;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 项目当前总市值 **/
	@Column(name = "PRO_CURT_MARKET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proCurtMarketValue;
	
	/** 我行投资当前市值 **/
	@Column(name = "INVEST_CURT_MARKET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investCurtMarketValue;
	
	/** 底层资产编号 **/
	@Column(name = "BASIC_ASSET_NO", unique = false, nullable = true, length = 40)
	private String basicAssetNo;
	
	/** 底层资产名称 **/
	@Column(name = "BASIC_ASSET_NAME", unique = false, nullable = true, length = 80)
	private String basicAssetName;
	
	/** 底层资产类型 **/
	@Column(name = "BASIC_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String basicAssetType;
	
	/** 底层资产客户编号 **/
	@Column(name = "BASIC_ASSET_CUS_ID", unique = false, nullable = true, length = 20)
	private String basicAssetCusId;
	
	/** 底层资产客户名称 **/
	@Column(name = "BASIC_ASSET_CUS_NAME", unique = false, nullable = true, length = 80)
	private String basicAssetCusName;
	
	/** 底层资产持仓占比 **/
	@Column(name = "BASIC_ASSET_POST_SCALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal basicAssetPostScale;
	
	/** 底层资产风险暴露金额 **/
	@Column(name = "BASIC_ASSET_RISK_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal basicAssetRiskAmt;
	
	/** 是否为合格担保 **/
	@Column(name = "IS_VALID_GUAR", unique = false, nullable = true, length = 5)
	private String isValidGuar;
	
	/** 底层增信担保方式 **/
	@Column(name = "BASIC_ASSET_GUAR_MODE", unique = false, nullable = true, length = 5)
	private String basicAssetGuarMode;
	
	/** 底层资产增信人客户编号 **/
	@Column(name = "BASIC_ASSET_GUAR_CUS_ID", unique = false, nullable = true, length = 20)
	private String basicAssetGuarCusId;
	
	/** 底层资产增信人客户名称 **/
	@Column(name = "BASIC_ASSET_GUAR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String basicAssetGuarCusName;
	
	/** 底层资产增信人担保金额 **/
	@Column(name = "BASIC_ASSET_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal basicAssetGuarAmt;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param proCurtMarketValue
	 */
	public void setProCurtMarketValue(java.math.BigDecimal proCurtMarketValue) {
		this.proCurtMarketValue = proCurtMarketValue;
	}
	
    /**
     * @return proCurtMarketValue
     */
	public java.math.BigDecimal getProCurtMarketValue() {
		return this.proCurtMarketValue;
	}
	
	/**
	 * @param investCurtMarketValue
	 */
	public void setInvestCurtMarketValue(java.math.BigDecimal investCurtMarketValue) {
		this.investCurtMarketValue = investCurtMarketValue;
	}
	
    /**
     * @return investCurtMarketValue
     */
	public java.math.BigDecimal getInvestCurtMarketValue() {
		return this.investCurtMarketValue;
	}
	
	/**
	 * @param basicAssetNo
	 */
	public void setBasicAssetNo(String basicAssetNo) {
		this.basicAssetNo = basicAssetNo;
	}
	
    /**
     * @return basicAssetNo
     */
	public String getBasicAssetNo() {
		return this.basicAssetNo;
	}
	
	/**
	 * @param basicAssetName
	 */
	public void setBasicAssetName(String basicAssetName) {
		this.basicAssetName = basicAssetName;
	}
	
    /**
     * @return basicAssetName
     */
	public String getBasicAssetName() {
		return this.basicAssetName;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType;
	}
	
    /**
     * @return basicAssetType
     */
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param basicAssetCusId
	 */
	public void setBasicAssetCusId(String basicAssetCusId) {
		this.basicAssetCusId = basicAssetCusId;
	}
	
    /**
     * @return basicAssetCusId
     */
	public String getBasicAssetCusId() {
		return this.basicAssetCusId;
	}
	
	/**
	 * @param basicAssetCusName
	 */
	public void setBasicAssetCusName(String basicAssetCusName) {
		this.basicAssetCusName = basicAssetCusName;
	}
	
    /**
     * @return basicAssetCusName
     */
	public String getBasicAssetCusName() {
		return this.basicAssetCusName;
	}
	
	/**
	 * @param basicAssetPostScale
	 */
	public void setBasicAssetPostScale(java.math.BigDecimal basicAssetPostScale) {
		this.basicAssetPostScale = basicAssetPostScale;
	}
	
    /**
     * @return basicAssetPostScale
     */
	public java.math.BigDecimal getBasicAssetPostScale() {
		return this.basicAssetPostScale;
	}
	
	/**
	 * @param basicAssetRiskAmt
	 */
	public void setBasicAssetRiskAmt(java.math.BigDecimal basicAssetRiskAmt) {
		this.basicAssetRiskAmt = basicAssetRiskAmt;
	}
	
    /**
     * @return basicAssetRiskAmt
     */
	public java.math.BigDecimal getBasicAssetRiskAmt() {
		return this.basicAssetRiskAmt;
	}
	
	/**
	 * @param isValidGuar
	 */
	public void setIsValidGuar(String isValidGuar) {
		this.isValidGuar = isValidGuar;
	}
	
    /**
     * @return isValidGuar
     */
	public String getIsValidGuar() {
		return this.isValidGuar;
	}
	
	/**
	 * @param basicAssetGuarMode
	 */
	public void setBasicAssetGuarMode(String basicAssetGuarMode) {
		this.basicAssetGuarMode = basicAssetGuarMode;
	}
	
    /**
     * @return basicAssetGuarMode
     */
	public String getBasicAssetGuarMode() {
		return this.basicAssetGuarMode;
	}
	
	/**
	 * @param basicAssetGuarCusId
	 */
	public void setBasicAssetGuarCusId(String basicAssetGuarCusId) {
		this.basicAssetGuarCusId = basicAssetGuarCusId;
	}
	
    /**
     * @return basicAssetGuarCusId
     */
	public String getBasicAssetGuarCusId() {
		return this.basicAssetGuarCusId;
	}
	
	/**
	 * @param basicAssetGuarCusName
	 */
	public void setBasicAssetGuarCusName(String basicAssetGuarCusName) {
		this.basicAssetGuarCusName = basicAssetGuarCusName;
	}
	
    /**
     * @return basicAssetGuarCusName
     */
	public String getBasicAssetGuarCusName() {
		return this.basicAssetGuarCusName;
	}
	
	/**
	 * @param basicAssetGuarAmt
	 */
	public void setBasicAssetGuarAmt(java.math.BigDecimal basicAssetGuarAmt) {
		this.basicAssetGuarAmt = basicAssetGuarAmt;
	}
	
    /**
     * @return basicAssetGuarAmt
     */
	public java.math.BigDecimal getBasicAssetGuarAmt() {
		return this.basicAssetGuarAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}