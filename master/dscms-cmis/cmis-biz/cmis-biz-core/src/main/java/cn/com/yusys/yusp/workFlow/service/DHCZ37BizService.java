package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @className DHCZ37BizService
 * @Description 人行征信查询审批流程（东海）
 * @Date 2020/12/21 : 10:43
 */
@Service
public class DHCZ37BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DHCZ37BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        handlerDHM01(resultInstanceDto);
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }

    public void handlerDHM01(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CreditReportQryLst creditReportQryLst = creditReportQryLstService.selectByPrimaryKey(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("征信查询业务申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);

            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("征信查询业务申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                creditReportQryLstService.handleBusinessDataAfterStart(iqpSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("征信查询业务申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("征信查询务申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                creditReportQryLstService.handleBusinessDataAfterEnd(iqpSerno, resultInstanceDto);
                Map<String, String> map = new HashMap<>();
                map.put("cusName", creditReportQryLst.getCusName());
                map.put("prdName", "人行征信查询审批");
                map.put("result", "通过");
                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
                sendMessage.sendMessage("MSG_CF_M_0016",map,"1",creditReportQryLst.getManagerId(),byLoginCode.getData().getUserMobilephone());
        } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    creditReportQryLstService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
                }
                Map<String, String> map = new HashMap<>();
                map.put("cusName", creditReportQryLst.getCusName());
                map.put("prdName", "人行征信查询审批");
                map.put("result", "退回");
                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
                sendMessage.sendMessage("MSG_CF_M_0016",map,"1",creditReportQryLst.getManagerId(),byLoginCode.getData().getUserMobilephone());
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("征信查询业务申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    creditReportQryLstService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
                }
                Map<String, String> map = new HashMap<>();
                map.put("cusName", creditReportQryLst.getCusName());
                map.put("prdName", "人行征信查询审批");
                map.put("result", "退回");
                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
                sendMessage.sendMessage("MSG_CF_M_0016",map,"1",creditReportQryLst.getManagerId(),byLoginCode.getData().getUserMobilephone());
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("征信查询业务申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    creditReportQryLstService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("征信查询业务申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                creditReportQryLstService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("征信查询业务申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                creditReportQryLstService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
                //creditReportQryLstService.handleBusinessAfterRefuse(iqpSerno);
            } else {
                log.warn("征信查询业务申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_TYPE_DHCZ37.equals(flowCode);
    }
}
