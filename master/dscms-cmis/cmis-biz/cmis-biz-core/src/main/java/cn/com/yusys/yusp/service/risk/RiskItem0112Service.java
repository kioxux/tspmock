package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.GrtGuarBizRstRelService;
import cn.com.yusys.yusp.service.GrtGuarContService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 担保合同抵押顺位校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0112Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0112Service.class);
    
    @Autowired
    private CtrLoanContService CtrLoanContService; // 合同主表
    
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService; //  grt_guar_biz_rst_rel  担保合同与业务表

    @Autowired
    private GrtGuarContService grtGuarContService; // 担保合同表
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/8/30 21:20
     * @version 1.0.0
     * @desc  担保合同抵押顺位校验
     *      合同签订时候，抵押顺位与是否浮动抵押未选择，则拦截
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0112(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = queryModel.getCondition().get("bizId").toString();
        log.info("*************担保合同抵押顺位校验开始***********合同编号：【{}】", contNo);
        if (StringUtils.isBlank(contNo)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        CtrLoanCont ctrLoanCont = CtrLoanContService.selectByPrimaryKey(contNo);
        if (Objects.isNull(ctrLoanCont)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
            return riskResultDto;
        }

        // 直接通过合同编号查找其项下关联的担保合同，并进行遍历
        QueryModel model = new QueryModel();
        model.addCondition("contNo", contNo);
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectByModel(model);
        log.info("*************根据合同编号：【{}】，查询到担保合同数量为：【{}】", contNo, grtGuarBizRstRelList.size());
        if (grtGuarBizRstRelList.size() > 0) {
            // size为0直接让他过
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                // 2021年9月8日14:26:35 hubp ，原来是判断借款合同担保方式为抵押，就进行校验，现在改为判断担保合同担保当时
                if (CmisCommonConstants.GUAR_MODE_10.equals(grtGuarCont.getGuarWay())) {
                    // 抵押顺位与是否浮动抵押未选择,则拦截
                    if (StringUtils.isBlank(grtGuarCont.getPldOrder()) || StringUtils.isBlank(grtGuarCont.getIsFloatPld())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11201);
                        return riskResultDto;
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************担保合同抵押顺位校验结束***********合同编号：【{}】", contNo);
        return riskResultDto;
    }
    
}
