package cn.com.yusys.yusp.web.server.xdxw0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0002.req.Xdxw0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0002.resp.Xdxw0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:小微贷前调查信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXW0002:小微贷前调查信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0002Resource.class);

    /**
     * 交易码：xdxw0002
     * 交易描述：小微贷前调查信息查询
     *
     * @param xdxw0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微贷前调查信息查询")
    @PostMapping("/xdxw0002")
    protected @ResponseBody
    ResultDto<Xdxw0002DataRespDto> xdxw0002(@Validated @RequestBody Xdxw0002DataReqDto xdxw0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, JSON.toJSONString(xdxw0002DataReqDto));
        Xdxw0002DataRespDto xdxw0002DataRespDto = new Xdxw0002DataRespDto();// 响应Dto:小微贷前调查信息查询
        ResultDto<Xdxw0002DataRespDto> xdxw0002DataResultDto = new ResultDto<>();
        // 从xdxw0002DataReqDto获取业务值进行业务逻辑处理
        String managerId = xdxw0002DataReqDto.getManagerId();//客户经理编号
        String queryType = xdxw0002DataReqDto.getQueryType();//查询类型
        String surveyType = xdxw0002DataReqDto.getSurveyType();//调查表类型
        String cusName = xdxw0002DataReqDto.getCusName();//客户名
        String certNo = xdxw0002DataReqDto.getCertNo();//客户证件号
        String pageNo = xdxw0002DataReqDto.getPageNo();//页码
        String pageSize = xdxw0002DataReqDto.getPageSize();//每页条数
        try {
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0002DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdxw0002DataRespDto.setTotalNum(StringUtils.EMPTY);// 总数
            cn.com.yusys.yusp.dto.server.xdxw0002.resp.List xdxw0002RespList = new cn.com.yusys.yusp.dto.server.xdxw0002.resp.List();
            xdxw0002RespList.setSerno(StringUtils.EMPTY);// 流水号
            xdxw0002RespList.setLoanType(StringUtils.EMPTY);// 贷款类型
            xdxw0002RespList.setPrdType(StringUtils.EMPTY);// 产品细分
            xdxw0002RespList.setApprStatus(StringUtils.EMPTY);// 审批状态
            xdxw0002RespList.setCusId(StringUtils.EMPTY);// 客户名
            xdxw0002RespList.setCertNo(StringUtils.EMPTY);// 身份证号
            xdxw0002RespList.setOperAddr(StringUtils.EMPTY);// 经营地址
            xdxw0002RespList.setMainBusi(StringUtils.EMPTY);// 主营业务
            xdxw0002RespList.setApplyAmt(StringUtils.EMPTY);// 申请金额
            xdxw0002RespList.setApplyTerm(StringUtils.EMPTY);// 申请期限
            xdxw0002RespList.setLoanResn(StringUtils.EMPTY);// 贷款原因
            xdxw0002RespList.setLoanUse(StringUtils.EMPTY);// 贷款用途
            xdxw0002RespList.setRepayMode(StringUtils.EMPTY);// 还款方式
            xdxw0002RespList.setGrtMode(StringUtils.EMPTY);// 担保方式
            xdxw0002RespList.setChrem(StringUtils.EMPTY);// 现金/银行存款/理财
            xdxw0002RespList.setActrec(StringUtils.EMPTY);// 应收账款
            xdxw0002RespList.setDepositAmt(StringUtils.EMPTY);// 存货
            xdxw0002RespList.setEquip(StringUtils.EMPTY);// 设备（净值）
            xdxw0002RespList.setOwnRealpro(StringUtils.EMPTY);// 自有房产
            xdxw0002RespList.setOwnCar(StringUtils.EMPTY);// 自有车辆
            xdxw0002RespList.setDebtAmt(StringUtils.EMPTY);// 负债总额
            xdxw0002RespList.setBankLoan(StringUtils.EMPTY);// 银行借款
            xdxw0002RespList.setOtherDebt(StringUtils.EMPTY);// 其他负债
            xdxw0002RespList.setOutguar(StringUtils.EMPTY);// 对外担保
            xdxw0002RespList.setSalesAmt(new BigDecimal(0L));// 销售收入-总额
            xdxw0002RespList.setCostAmt(new BigDecimal(0L));// 可变成本-总额
            xdxw0002RespList.setExpendAmt(new BigDecimal(0L));// 固定支出-总额
            xdxw0002RespList.setProfit(new BigDecimal(0L));// 净利润
            xdxw0002RespList.setTurnoverAmt(new BigDecimal(0L));// 营业额检验
            xdxw0002RespList.setMearn(new BigDecimal(0L));// 月均收入
            xdxw0002RespList.setCash(new BigDecimal(0L));// 现金
            xdxw0002RespList.setDep(new BigDecimal(0L));// 存款
            xdxw0002RespList.setRealpro(new BigDecimal(0L));// 房产
            xdxw0002RespList.setCar(new BigDecimal(0L));// 汽车
            xdxw0002RespList.setIndivLoan(new BigDecimal(0L));// 私人借款
            xdxw0002RespList.setTotalIncome(new BigDecimal(0L));// 总收入
            xdxw0002RespList.setPaybill(new BigDecimal(0L));// 工资收入
            xdxw0002RespList.setOperProfit(new BigDecimal(0L));// 经营利润
            xdxw0002RespList.setYearHomeSpend(new BigDecimal(0L));// 年家庭开支
            xdxw0002RespList.setWorkUnitName(StringUtils.EMPTY);// 工作单位名称
            xdxw0002RespList.setWorkUnitName(StringUtils.EMPTY);// 工作单位名称
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            xdxw0002DataRespDto.setList(Arrays.asList(xdxw0002RespList));
            // TODO 封装xdxw0002DataRespDto对象结束
            // 封装xdxw0002DataResultDto中正确的返回码和返回信息
            xdxw0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, e.getMessage());
            // 封装xdxw0002DataResultDto中异常返回码和返回信息
            xdxw0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0002DataRespDto到xdxw0002DataResultDto中
        xdxw0002DataResultDto.setData(xdxw0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, JSON.toJSONString(xdxw0002DataResultDto));
        return xdxw0002DataResultDto;
    }
}
