package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.Cmislmt0016LmtCopSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.core.dp2099.Dp2099Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0009.CmisLmt0009Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0016.CmisLmt0016Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 合作方保证金校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0104Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0104Service.class);

    @Autowired
    private Dp2099Service dp2099Service;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService; // 放款申请表

    @Autowired
    private IqpLoanAppService iqpLoanAppService; // 业务申请表

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisLmt0016Service cmisLmt0016Service;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/8/30 21:20
     * @version 1.0.0
     * @desc 合作方：房地产开发商
     * （1）合作方保证金为后缴，系统只做提示，不做强制拦截
     * （2）检查规则如下：
     *     合作方保证金账户余额 < 合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例  ----系统风险提示
     * 合作方保证金账户余额 >= 合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例 ----通过
     * （3）检查环节：
     *     零售贷款出账申请环节，客户经理提交时风险拦截进行检查
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0104(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            String pvpSerno = queryModel.getCondition().get("bizId").toString();
            log.info("*************合作方保证金校验开始***********【{}】", pvpSerno);
            if (StringUtils.isBlank(pvpSerno)) {
                // 为空不通过
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            BigDecimal bzjAmt = BigDecimal.ZERO; // 保证金余额
            BigDecimal bailPerc = BigDecimal.ZERO; // 保证金比例
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); // 为查询到放款信息
                return riskResultDto;
            }
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(pvpLoanApp.getIqpSerno());
            if (!Objects.isNull(iqpLoanApp) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsOutstndTrdLmtAmt())) {
                // 调用额度16接口，查询出合作方项目编号
                CmisLmt0016ReqDto cmisLmt0016ReqDto = new CmisLmt0016ReqDto();
                Cmislmt0016LmtCopSubAccListRespDto lmt0016 = null;
                cmisLmt0016ReqDto.setCusId(iqpLoanApp.getCoopCusId());
                cmisLmt0016ReqDto.setSubAccNo(iqpLoanApp.getTdpAgrNo());
                cmisLmt0016ReqDto.setStartNum(0);
                cmisLmt0016ReqDto.setPageCount(100);
                CmisLmt0016RespDto cmisLmt0016RespDto = null;
                try {
                    log.info("根据项目编号【{}】，前往额度系统-第三方额度查询请求报文：【{}】", iqpLoanApp.getProNo(), JSON.toJSONString(cmisLmt0016ReqDto));
                    cmisLmt0016RespDto = cmisLmt0016Service.cmisLmt0016(cmisLmt0016ReqDto);
                    log.info("根据项目编号【{}】，前往额度系统-第三方额度查询返回报文：【{}】", iqpLoanApp.getProNo(), JSON.toJSONString(cmisLmt0016RespDto));
                } catch (Exception e){
                    log.info("根据项目编号【{}】，前往额度系统-第三方额度查询失败！，失败信息【{}】", iqpLoanApp.getProNo(),JSON.toJSONString(cmisLmt0016RespDto));
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10401);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                    return riskResultDto;
                }

                if (cmisLmt0016RespDto.getLmtCopSubAccList().size() == 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10401); // 未查询到合作方详细信息！
                    return riskResultDto;
                } else {
                    lmt0016 = cmisLmt0016RespDto.getLmtCopSubAccList().get(0);
                    CoopPlanAccInfo coopPlanAccInfo = coopPlanAccInfoService.selectByCoopPlanNo(lmt0016.getAccNo());
                    log.info("根据合作方编号【{}】查询到合作方详细信息：【{}】", lmt0016.getAccNo(), JSON.toJSONString(coopPlanAccInfo));
                    bailPerc = coopPlanAccInfo.getBailPerc();
                    // 如果保证金比例为空，或者为0，则直接通过
                    if(Objects.isNull(bailPerc) || bailPerc.compareTo(BigDecimal.ZERO) == 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                        return riskResultDto;
                    }
                    if (StringUtils.isBlank(coopPlanAccInfo.getPartnerType()) || StringUtils.isBlank(coopPlanAccInfo.getBailAccNo())
                            || StringUtils.isBlank(coopPlanAccInfo.getBailAccNoSubSeq())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10405); // 未查询到合作方账户信息！
                        return riskResultDto;
                    }
                    // 合作方为房地产开发商才开始校验
                    if ("1".equals(coopPlanAccInfo.getPartnerType())) {
                        Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
                        dp2099ReqDto.setChaxleix("0"); // 查询类型 0--客户 1--非客户
                        dp2099ReqDto.setChaxfanw("3"); // 查询范围 1--指定机构 2--直辖 3--全辖 4--本机构
                        // dp2099ReqDto.setKehuhaoo(coopPartnerAgrAccInfo.getPartnerNo()); // 客户号
                        dp2099ReqDto.setKehuzhao(coopPlanAccInfo.getBailAccNo()); // 客户账号
                        dp2099ReqDto.setZhhaoxuh(coopPlanAccInfo.getBailAccNoSubSeq()); // 子账户序号
                        log.info("根据合作方编号【{}】前往核心系统查询保证金账户请求报文：【{}】", coopPlanAccInfo.getPartnerNo(), dp2099ReqDto.toString());
                        Dp2099RespDto dp2099RespDto = dp2099Service.dp2099(dp2099ReqDto);
                        log.info("根据合作方编号【{}】前往核心系统查询保证金账户返回报文：【{}】", coopPlanAccInfo.getPartnerNo(), dp2099RespDto.toString());
                        if (dp2099RespDto.getList().size() == 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10402); // 未查询到合作方保证金余额信息！
                            return riskResultDto;
                        } else {
                            bzjAmt = dp2099RespDto.getList().get(0).getKeyongye();
                        }
                        // 获取合作方下关联的所有贷款金额
                        CmisLmt0039ReqDto cmisLmt0039ReqDto = new CmisLmt0039ReqDto();
                        cmisLmt0039ReqDto.setApprSerno(lmt0016.getAccNo());
                        log.info("根据放款流水号【{}】前往额度系统-第三方额度查询请求报文：【{}】", pvpSerno, cmisLmt0039ReqDto.toString());
                        ResultDto<CmisLmt0039RespDto> cmisLmt0039RespDto = cmisLmtClientService.cmislmt0039(cmisLmt0039ReqDto);
                        log.info("根据放款流水号【{}】前往额度系统-第三方额度查询返回报文：【{}】", pvpSerno, cmisLmt0039RespDto.toString());

                        String code = cmisLmt0039RespDto.getData().getErrorCode();
                        if (!"0000".equals(code)) {
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额失败！", iqpLoanApp.getProNo());
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10403);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                            return riskResultDto;
                        } else {
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额成功！", iqpLoanApp.getProNo());
                            BigDecimal totalAmt = cmisLmt0039RespDto.getData().getSumLoanTotalCny(); //合作方下关联的所有贷款金额 * 合作方保证金比例
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额为：【{}】", iqpLoanApp.getProNo(),totalAmt.toPlainString());
                            if (this.getResult(bzjAmt, totalAmt, bailPerc, pvpLoanApp.getPvpAmt())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10404);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            log.info("*************合作方保证金校验结束***********【{}】", pvpSerno);

        }catch (Exception e){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("合作方保证金校验异常");
            log.info("*************合作方保证金校验异常***********"+ e.getMessage());
        }
        return riskResultDto;
    }

    /**
     * @param bzjAmt, totalAmt, bailPerc, pvpAmt
     * @return boolean
     * @author hubp
     * @date 2021/9/1 11:00
     * @version 1.0.0
     * @desc  计算结果
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private boolean getResult (BigDecimal bzjAmt,BigDecimal totalAmt,BigDecimal bailPerc,BigDecimal pvpAmt) {
        // 合作方保证金账户余额 < 合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
        log.info("通过保证金余额:【{}】，合作方关联贷款总金额:【{}】，保证金比例:【{}】，本次放款金额:【{}】，开始计算是否通过", bzjAmt.toPlainString(),totalAmt.toPlainString(),bailPerc.toPlainString(),pvpAmt.toPlainString());
        // 先计算 --》合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
        BigDecimal allAmt = totalAmt.multiply(bailPerc).add(pvpAmt.multiply(bailPerc));
        log.info("合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例之和为【{}】",allAmt.toPlainString());
        if(bzjAmt.compareTo(allAmt) < 0){
            return true;
        }
        return false;
    }
}
