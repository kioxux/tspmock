package cn.com.yusys.yusp.service.client.common;

import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationInfo;
import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.req.Cmiscus0017ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.resp.Cmiscus0017RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import cn.com.yusys.yusp.service.client.cus.queryAllCusIndiv.QueryAllCusIndivService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 通用外围接口调用类
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class CommonService {

    private static final Logger logger = LoggerFactory.getLogger(CommonService.class);
    @Autowired
    private CmisCus0006Service cmisCus0006Service;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmRoleService adminSmRoleService;

    @Autowired
    private QueryAllCusIndivService queryAllCusIndivService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private DscmsXtClientService dscmsXtClientService;

    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;

    @Autowired
    private DataAuthorizationService dataAuthorizationService;

    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;

    /**
     * 从redis中获取cusBaseList
     *
     * @return
     */
    public List<CusBaseDto> getCommonDto(Map<String, String> param) {
        CmisCus0006ReqDto cmisCus0006ReqDto = new CmisCus0006ReqDto();//请求Dto：查询客户基本信息
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();//响应Dto：查询客户基本信息
        //通过客户证件号查询客户信息
        param.forEach((k, v) -> {
            if (Objects.equals(k, "certCode")) {
                cmisCus0006ReqDto.setCertCode(v);
            }
            if (Objects.equals(k, "cusId")) {
                cmisCus0006ReqDto.setCusId(v);
            }
        });
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
        cmisCus0006RespDto = cmisCus0006Service.cmisCus0006(cmisCus0006ReqDto);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006RespDto));

        if (cmisCus0006RespDto != null) {
            return cmisCus0006RespDto.getCusBaseList();
        }

        return new ArrayList<CusBaseDto>();
    }

    /**
     * 通过身份证号获取客户基本信息
     *
     * @param cusCertNo
     * @return
     */
    public List<String> getCusBaseByCertCode(String cusCertNo) {
        Map<String, String> map = new HashMap<>();
        map.put("certCode", cusCertNo);
        List<CusBaseDto> cusBaseList = getCommonDto(map);
        // 客户ids
        List<String> cusIds = new ArrayList<>();
        if (CollectionUtils.nonEmpty(cusBaseList)) {
            cusIds = cusBaseList.parallelStream()
                    .map(cusBaseDto -> cusBaseDto.getCusId()) // 获取cusBaseDto中客户号
                    .collect(Collectors.toList());// 转换成List
        }
        return cusIds;
    }

    /**
     * 通过客户号获取客户基本信息
     *
     * @param cusId
     * @return
     */
    public CusBaseDto getCusBaseByCusId(String cusId) {
        Map<String, String> map = new HashMap<>();
        map.put("cusId", cusId);
        List<CusBaseDto> cusBaseList = getCommonDto(map);
        //返回客户信息
        CusBaseDto returnCusBaseDto = null;
        if (!CollectionUtils.isEmpty(cusBaseList) && cusBaseList.size() > 0) {//存在客户记录
            returnCusBaseDto = cusBaseList.get(0);
        }
        return returnCusBaseDto;
    }

    /**
     * 通过证件号获取客户信息列表
     *
     * @param certCode
     * @return
     */
    public List<CusBaseDto> getCusBaseListByCertCode(String certCode) {
        Map<String, String> map = new HashMap<>();
        map.put("certCode", certCode);
        List<CusBaseDto> cusBaseList = getCommonDto(map);
        // 客户ids
        List<CusBaseDto> cusBaseDtoList = new ArrayList<>();
        if (CollectionUtils.nonEmpty(cusBaseList)) {
            cusBaseDtoList = cusBaseList.parallelStream()
                    .filter(cusBaseDto -> Objects.equals(certCode, cusBaseDto.getCertCode()))//判断 请求参数中客户证件号和cusBaseDto中证件号是否相等
                    .collect(Collectors.toList());// 转换成List
        }
        //返回客户信息
        return cusBaseDtoList;
    }

    /**
     * 根据证件编号列表查询客户编号列表
     *
     * @param certNos
     * @return
     */
    public List<String> getCusIdsByCertNos(List<String> certNos) {
        List<String> cusIds = new ArrayList<>();
        for (String certNo : certNos) {
            Map<String, String> map = new HashMap<>();
            map.put("certCode", certNo);
            List<CusBaseDto> cusBaseList = getCommonDto(map);
            if (CollectionUtils.nonEmpty(cusBaseList)) {
                for (CusBaseDto cusBaseDto : cusBaseList) {
                    cusIds.add(cusBaseDto.getCusId());
                }
            }
        }
        return cusIds;
    }

    /**
     * 根据机构编号列表分页查询用户和机构信息
     *
     * @param userAndOrgInfoReqDto
     * @return
     */
    public List<UserAndOrgInfoRespDto> getUserAndOrgInfo(UserAndOrgInfoReqDto userAndOrgInfoReqDto) {
        logger.info("调用oca接口根据机构编号列表分页查询用户和机构信息", "请求参数:" + userAndOrgInfoReqDto.toString());
        List<UserAndOrgInfoRespDto> userAndOrgInfoRespDtos = new ArrayList<>();
        ResultDto<List<UserAndOrgInfoRespDto>> resultDto = null;
        try {
            resultDto = adminSmOrgService.getUserAndOrgInfo(userAndOrgInfoReqDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                userAndOrgInfoRespDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("调用oca接口根据机构编号列表分页查询用户和机构信息异常", "异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口根据机构编号列表分页查询用户和机构信息", "响应参数:" + JSON.toJSONString(resultDto));
        return userAndOrgInfoRespDtos;
    }

    /**
     * 调用oca接口通过用户号查询用户信息
     *
     * @param loginCode
     * @return
     */
    public AdminSmUserDto getByLoginCode(String loginCode) {
        logger.info("调用oca接口通过用户号查询用户信息", "请求参数:" + loginCode);
        AdminSmUserDto adminSmUserDto = new AdminSmUserDto();
        ResultDto<AdminSmUserDto> resultDto = null;
        try {
            resultDto = adminSmUserService.getByLoginCode(loginCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                adminSmUserDto = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("调用oca接口通过用户号查询用户信息", "异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口通过用户号查询用户信息", "响应参数:" + JSON.toJSONString(resultDto));
        return adminSmUserDto;
    }

    /**
     * 调用oca接口通过角色编号查询用户信息
     *
     * @param getUserInfoByRoleCodeDto
     * @return
     */
    public List<AdminSmUserDto> getUserInfoByRoleCode(GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto) {
        logger.info("调用oca接口通过角色编号查询用户信息", "请求参数:" + getUserInfoByRoleCodeDto.toString());
        List<AdminSmUserDto> adminSmUserDtos = new ArrayList<>();
        ResultDto<List<AdminSmUserDto>> resultDto = null;
        try {
            resultDto = adminSmRoleService.getuserlist(getUserInfoByRoleCodeDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                adminSmUserDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("调用oca接口通过角色编号查询用户信息", "异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口通过角色编号查询用户信息", "响应参数:" + JSON.toJSONString(resultDto));
        return adminSmUserDtos;
    }

    /**
     * 调用oca接口通过岗位编号查询用户信息
     *
     * @param getUserInfoByDutyCodeDto
     * @return
     */
    public List<AdminSmUserDto> getUserInfoByDutyCodeDto(GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        logger.info("调用oca接口通过岗位编号查询用户信息请求参数:" + getUserInfoByDutyCodeDto.toString());
        List<AdminSmUserDto> adminSmUserDtos = new ArrayList<>();
        ResultDto<List<AdminSmUserDto>> resultDto = null;
        try {
            resultDto = adminSmUserDutyRelService.getUserList(getUserInfoByDutyCodeDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                adminSmUserDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("调用oca接口通过岗位编号查询用户信息异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口通过岗位编号查询用户信息响应参数:" + JSON.toJSONString(resultDto));
        return adminSmUserDtos;
    }

    /**
     * 调用oca接口通过岗位编号查询用户信息
     * ResultDto中会多返回从Redis中翻译字段,后端转换异常,直接返回List
     *
     * @param getUserInfoByDutyCodeDto
     * @return
     */
    public List<AdminSmUserDto> getUserInfoByDutyCodeDtoNew(GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        logger.info("调用oca接口通过岗位编号查询用户信息请求参数:" + getUserInfoByDutyCodeDto.toString());
        List<AdminSmUserDto> adminSmUserDtos = new ArrayList<>();
        try {
            adminSmUserDtos = adminSmUserDutyRelService.getUserListNew(getUserInfoByDutyCodeDto);
        } catch (Exception e) {
            logger.info("调用oca接口通过岗位编号查询用户信息异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口通过岗位编号查询用户信息响应参数:" + JSON.toJSONString(adminSmUserDtos));
        return adminSmUserDtos;
    }

    public String getIsEqualJRSCBZC() {
        String isEqualJRSCBZC = CmisCommonConstants.STD_ZB_YES_NO_0;
        //流程路由金融市场总部风险合规部负责人是否等于金融市场总部总裁
        String fxhgbfzrDuty = CmisBizConstants.DUTY_JRB04;
        String jrscbzcDuty = CmisBizConstants.DUTY_JRB10;

        GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
        getUserInfoByDutyCodeDto.setDutyCode(fxhgbfzrDuty);
        getUserInfoByDutyCodeDto.setPageNum(1);
        getUserInfoByDutyCodeDto.setPageSize(100);
        List<AdminSmUserDto> fxhgbfzrDutyList = this.getUserInfoByDutyCodeDto(getUserInfoByDutyCodeDto);
        logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：getUserInfoByDutyCodeDto   fxhgbfzrDutyList  -----------> " + fxhgbfzrDutyList.toString());
        if ((fxhgbfzrDutyList != null && fxhgbfzrDutyList.size() > 1) || (fxhgbfzrDutyList == null && fxhgbfzrDutyList.size() <= 0)) {
            isEqualJRSCBZC = CmisCommonConstants.STD_ZB_YES_NO_0;
            return isEqualJRSCBZC;
        } else {
            getUserInfoByDutyCodeDto.setDutyCode(jrscbzcDuty);
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(100);
            List<AdminSmUserDto> fjrscbzcDutyList = this.getUserInfoByDutyCodeDto(getUserInfoByDutyCodeDto);
            logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：getUserInfoByDutyCodeDto   fxhgbfzrDutyList  -----------> " + fjrscbzcDutyList.toString());
            if ((fjrscbzcDutyList != null && fjrscbzcDutyList.size() > 1) || (fjrscbzcDutyList == null && fjrscbzcDutyList.size() <= 0)) {
                isEqualJRSCBZC = CmisCommonConstants.STD_ZB_YES_NO_0;
                return isEqualJRSCBZC;
            } else if (fjrscbzcDutyList != null && fjrscbzcDutyList.size() == 1) {
                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：fxhgbfzrDutyUserCode     -----------> " + fxhgbfzrDutyList.get(0));
                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：fjrscbzcDutyUserCode     -----------> " + fjrscbzcDutyList.get(0));
                String fzrMap = JSON.toJSONString(fxhgbfzrDutyList.get(0));
                String zcMap = JSON.toJSONString(fjrscbzcDutyList.get(0));

                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：fzrMap     -----------> " + fzrMap);
                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：zcMap     -----------> " + zcMap);

                AdminSmUserDto fzrDto = JSON.parseObject(fzrMap, AdminSmUserDto.class);
                AdminSmUserDto zcDto = JSON.parseObject(zcMap, AdminSmUserDto.class);

                String fxhgbfzrDutyUserCode = fzrDto.getUserCode();
                String fjrscbzcDutyUserCode = zcDto.getUserCode();

                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：fxhgbfzrDutyUserCode     -----------> " + fxhgbfzrDutyUserCode);
                logger.info("【判断金融市场总部风险合规部负责人是否等于金融市场总部总裁】：fjrscbzcDutyUserCode     -----------> " + fjrscbzcDutyUserCode);
                if (fxhgbfzrDutyUserCode.equals(fjrscbzcDutyUserCode)) {
                    isEqualJRSCBZC = CmisCommonConstants.STD_ZB_YES_NO_1;
                }
            }
        }

        return isEqualJRSCBZC;
    }

    /**
     * 根据身份证号获取个人客户基本信息
     *
     * @param certCode
     * @return
     */
    public CusIndivAllDto getCusIndivAllDtoByCertCode(String certCode) {
        QueryModel qm = new QueryModel();
        qm.addCondition("certNo", certCode);
        List<CusIndivAllDto> cusIndivAllDtos = queryAllCusIndivService.queryAllCusIndiv(qm);
        if (CollectionUtils.nonEmpty(cusIndivAllDtos)) {
            CusIndivAllDto cusIndivAllDto = JSONObject.parseObject(JSON.toJSONString(cusIndivAllDtos.get(0)), CusIndivAllDto.class);
            return cusIndivAllDto;
        }
        return new CusIndivAllDto();
    }

    /**
     * 根据客户号获取个人客户基本信息
     *
     * @param cusId 客户号
     * @return CusIndivAllDto 客户基本信息
     */
    public CusIndivAllDto getCusIndivAllDtoByCusId(String cusId) {
        QueryModel qm = new QueryModel();
        qm.addCondition("cusId", cusId);
        List<CusIndivAllDto> cusIndivAllDtos = queryAllCusIndivService.queryAllCusIndiv(qm);
        if (CollectionUtils.nonEmpty(cusIndivAllDtos)) {
            String str = JSON.toJSONString(cusIndivAllDtos.get(0));
            CusIndivAllDto gusIndivAllDto = JSONObject.parseObject(str, CusIndivAllDto.class);
            return gusIndivAllDto;
        }
        return new CusIndivAllDto();
    }

    /**
     * 调用oca接口通过用户信息查询岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    public List<UserAndDutyRespDto> getUserAndDuty(UserAndDutyReqDto userAndDutyReqDto) {
        logger.info("调用oca接口通过角色编号查询用户信息,请求参数:【{}】", userAndDutyReqDto.toString());
        List<UserAndDutyRespDto> userAndDutyRespDtos = new ArrayList<>();
        ResultDto<List<UserAndDutyRespDto>> resultDto = null;
        try {
            resultDto = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                userAndDutyRespDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.error("调用oca接口通过角色编号查询用户信息,异常信息为:【{}】", e.getMessage());
        }
        logger.info("调用oca接口通过角色编号查询用户信息,响应参数:【{}】", JSON.toJSONString(resultDto));
        return userAndDutyRespDtos;
    }

    /**
     * 调用cus接口查询客户去年12期财报中最大的主营业务收入z
     *
     * @param cmiscus0017ReqDto
     * @return
     */
    public BigDecimal getMaxAmt(Cmiscus0017ReqDto cmiscus0017ReqDto) {
        logger.info("调用cus接口查询客户去年12期财报中最大的主营业务收入", "请求参数:" + cmiscus0017ReqDto.toString());
        BigDecimal amt = BigDecimal.ZERO;
        ResultDto<Cmiscus0017RespDto> resultDto = null;
        try {
            resultDto = cmisCusClientService.cmiscus0017(cmiscus0017ReqDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                amt = resultDto.getData().getAmt();
            }
        } catch (Exception e) {
            logger.info("调用cus接口查询客户去年12期财报中最大的主营业务收入", "异常信息为:" + e.getMessage());
        }
        logger.info("调用cus接口查询客户去年12期财报中最大的主营业务收入", "响应参数:" + JSON.toJSONString(resultDto));
        return amt;
    }

    /**
     * 调用oca接口 通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    public List<CommonUserQueryRespDto> getCommonUser(CommonUserQueryReqDto commonUserQueryReqDto) {
        logger.info("调用oca接口 通用客户经理查询", "请求参数:" + commonUserQueryReqDto.toString());
        List<CommonUserQueryRespDto> commonUserQueryRespDtos = new ArrayList<>();
        ResultDto<List<CommonUserQueryRespDto>> resultDto = null;
        try {
            resultDto = adminSmUserService.getCommonUserInfo(commonUserQueryReqDto);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                commonUserQueryRespDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("调用oca接口 通用客户经理查询", "异常信息为:" + e.getMessage());
        }
        logger.info("调用oca接口 通用客户经理查询", "响应参数:" + JSON.toJSONString(resultDto));
        return commonUserQueryRespDtos;
    }

    /**
     * 调用oca接口 查询是否小微客户经理 Y：是，N:否
     *
     * @param loginCode
     * @return
     */
    public String getIsXWUser(String loginCode) {
        logger.info("调用oca接口查询是否小微客户经理", "请求参数:" + loginCode);
        GetIsXwUserDto getIsXwUserDto = new GetIsXwUserDto();
        ResultDto<GetIsXwUserDto> resultDto = null;
        try {
            resultDto = adminSmUserService.getIsXWUserByLoginCode(loginCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                getIsXwUserDto = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("查询是否小微客户经理", "异常信息为:" + e.getMessage());
        }
        logger.info("查询是否小微客户经理", "响应参数:" + JSON.toJSONString(resultDto));
        return getIsXwUserDto.getIsXWUser();
    }

    /**
     * 调用oca接口 根据机构号查询机构信息
     *
     * @param orgCode
     * @return
     */
    public AdminSmOrgDto getByOrgCode(String orgCode) {
        logger.info("根据机构号查询机构信息开始", "请求参数:" + orgCode);
        AdminSmOrgDto adminSmOrgDto = new AdminSmOrgDto();
        ResultDto<AdminSmOrgDto> resultDto = null;
        try {
            resultDto = adminSmOrgService.getByOrgCode(orgCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                adminSmOrgDto = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据机构号查询机构信息异常", "异常信息为:" + e.getMessage());
        }
        logger.info("根据机构号查询机构信息结束", "响应参数:" + JSON.toJSONString(resultDto));
        return adminSmOrgDto;
    }

    /**
     * 调用cfg接口 根据目录层级查询模糊产品信息
     *
     * @param cataloglevelName
     * @return
     */
    public List<CfgPrdBasicinfoDto> queryBasicInfoByCatalog(String cataloglevelName) {
        logger.info("根据目录层级查询模糊产品信息开始", "请求参数:" + cataloglevelName);
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = new ArrayList<>();
        ResultDto<List<CfgPrdBasicinfoDto>> resultDto = null;
        try {
            resultDto = iCmisCfgClientService.queryBasicInfoByCatalog(cataloglevelName);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                cfgPrdBasicinfoDtos = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据目录层级查询模糊产品信息异常", "异常信息为:" + e.getMessage());
        }
        logger.info("根据目录层级查询模糊产品信息结束", "响应参数:" + JSON.toJSONString(resultDto));
        return cfgPrdBasicinfoDtos;
    }

    /**
     * 根据机构编号获取下级机构号
     *
     * @author jijian_yx
     * @date 2021/8/24 17:28
     **/
    public List<String> getLowerOrgId(String orgCode) {
        logger.info("根据机构编号获取下级机构号开始，请求参数:" + orgCode);
        List<String> orgCodes = new ArrayList<>();
        ResultDto<List<String>> resultDto = null;
        try {
            resultDto = adminSmOrgService.getLowerOrgId(orgCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                orgCodes = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据机构编号获取下级机构号异常，异常信息为:" + e.getMessage());
        }
        logger.info("根据机构编号获取下级机构号结束，响应参数:" + JSON.toJSONString(resultDto));
        return orgCodes;
    }

    /**
     * 根据业务流水号获取流程审批同意节点名称
     *
     * @author jijian_yx
     * @date 2021/8/24 17:28
     **/
    public List<String> getInstanceNodeNameByBizId(String bizId) {
        logger.info("根据业务流水号获取流程审批同意节点名称，请求参数:" + bizId);
        List<String> nodeNames = new ArrayList<>();
        ResultDto<List<String>> resultDto = null;
        try {
            resultDto = dscmsXtClientService.getInstanceNameByBizId(bizId);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                nodeNames = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据业务流水号获取流程审批同意节点名称，异常信息为:" + e.getMessage());
        }
        logger.info("根据业务流水号获取流程审批同意节点名称，响应参数:" + JSON.toJSONString(resultDto));
        return nodeNames;
    }

    /*
     * 证件映射 信贷--->风控
     * @param  信贷证件码值
     * @return  风控证件码值
     *
     */
    public String toFkCertType(String xdbz) {
        String fkbz = "";
        if ("A".equals(xdbz)) { //身份证
            fkbz = "10";
        } else if ("C".equals(xdbz)) { //户口簿
            fkbz = "11";
        } else if ("B".equals(xdbz)) {//护照
            fkbz = "12";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            fkbz = "15";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            fkbz = "16";
        } else if ("S".equals(xdbz)) { // 外国人居留证
            fkbz = "18";
        } else if ("Y".equals(xdbz)) {//警官证
            fkbz = "19";
        } else if ("W".equals(xdbz)) {//港、澳、台身份证
            fkbz = "21";
        } else if ("P".equals(xdbz)) {//其他证件
            fkbz = "1X";
        } else if ("X".equals(xdbz)) {//临时身份证
            fkbz = "17";
        } else if ("G".equals(xdbz)) {//军官证
            fkbz = "13";
        } else if ("H".equals(xdbz)) {//士兵证
            fkbz = "14";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            fkbz = "20";
        } else if ("V".equals(xdbz)) {//境外企业代码
            fkbz = "22";
        } else if ("M".equals(xdbz)) {//营业执照
            fkbz = "24";
        } else {
            fkbz = xdbz;//未匹配到的证件类型
        }
        return fkbz;
    }

    /**
     * 适用于导出查询sql中设置配置的角色相关数据权限
     *
     * @author jijian_yx
     * @date 2021/10/11 22:10
     **/
    public String setDataAuthority(String apiUrl) {
        logger.info("导出查询sql设置角色数据权限开始");
        String dataAuth = "";
        try {
            User loginUser = SessionUtils.getUserInformation();
            List<DataAuthorizationInfo> dataAuthorizationInfoList =
                    dataAuthorizationService.matching(apiUrl, "POST", loginUser.getUserId());
            if (null != dataAuthorizationInfoList && dataAuthorizationInfoList.size() > 0) {
                for (DataAuthorizationInfo dataAuthorizationInfo : dataAuthorizationInfoList) {
                    String sqlTemplate = dataAuthorizationInfo.getSqlTemplate();
                    dataAuth += " AND " + sqlTemplate;
                }
                dataAuth = dataAuth.replaceAll("\\$", "");
                dataAuth = dataAuth.replaceAll("\\{", "");
                dataAuth = dataAuth.replaceAll("}", "");
            }
            String currentOrgCode = loginUser.getOrg().getCode();// 当前用户所属的机构码
            if (!cn.com.yusys.yusp.commons.util.StringUtils.isBlank(currentOrgCode)) {
                if (dataAuth.contains("_orgCode")) {
                    dataAuth = dataAuth.replaceAll("_orgCode", "'" + currentOrgCode + "'");
                }
                if (dataAuth.contains("_orgTree")) {
                    Object hget = yuspRedisTemplate.hget("orgTree", currentOrgCode);
                    if (Objects.nonNull(hget)) {
                        List<String> orgTreeList = (List<String>) ObjectMapperUtils.instance().convertValue(hget, List.class);
                        StringBuilder orgTree = new StringBuilder();
                        if (null != orgTreeList && orgTreeList.size() > 0) {
                            for (String orgCode : orgTreeList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(orgTree.toString())) {
                                    orgTree.append("'").append(orgCode).append("'");
                                } else {
                                    orgTree.append(",'").append(orgCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_orgTree", orgTree.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_orgTree", "''");
                    }
                }
            }
            String currentUserCode = loginUser.getUserId();
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(currentUserCode)) {
                if (dataAuth.contains("_userCode")) {
                    dataAuth = dataAuth.replaceAll("_userCode", "'" + currentUserCode + "'");
                }
                if (dataAuth.contains("_areaXwUser")) {
                    Object areaXwUserHget = yuspRedisTemplate.hget("areaXwUser", currentUserCode);
                    if (Objects.nonNull(areaXwUserHget)) {
                        List<String> areaXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwUserHget, List.class);
                        StringBuilder areaXwUser = new StringBuilder();
                        if (null != areaXwUserList && areaXwUserList.size() > 0) {
                            for (String wxCode : areaXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(areaXwUser.toString())) {
                                    areaXwUser.append("'").append(wxCode).append("'");
                                } else {
                                    areaXwUser.append(",'").append(wxCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_areaXwUser", areaXwUser.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_areaXwUser", "''");
                    }
                }
                if (dataAuth.contains("_areaXwOrg")) {
                    Object areaXwOrgHget = yuspRedisTemplate.hget("areaXwOrg", currentUserCode);
                    if (Objects.nonNull(areaXwOrgHget)) {
                        List<String> areaXwOrgList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwOrgHget, List.class);
                        StringBuilder areaXwOrg = new StringBuilder();
                        if (null != areaXwOrgList && areaXwOrgList.size() > 0) {
                            for (String wxOrgCode : areaXwOrgList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(areaXwOrg.toString())) {
                                    areaXwOrg.append("'").append(wxOrgCode).append("'");
                                } else {
                                    areaXwOrg.append(",'").append(wxOrgCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_areaXwOrg", areaXwOrg.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_areaXwOrg", "''");
                    }
                }
                if (dataAuth.contains("_allXwUser")) {
                    Object allXwHget = yuspRedisTemplate.hget("allXwUser", currentUserCode);
                    if (Objects.nonNull(allXwHget)) {
                        List<String> allXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(allXwHget, List.class);
                        StringBuilder allXwUser = new StringBuilder();
                        if (null != allXwUserList && allXwUserList.size() > 0) {
                            for (String xwCode : allXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(allXwUser.toString())) {
                                    allXwUser.append("'").append(xwCode).append("'");
                                } else {
                                    allXwUser.append(",'").append(xwCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_allXwUser", allXwUser.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_allXwUser", "''");
                    }
                }
                if (dataAuth.contains("_jzzyUser")) {
                    Object jzzyHget = yuspRedisTemplate.hget("jzzyUser", currentUserCode);
                    if (Objects.nonNull(jzzyHget)) {
                        List<String> jzzyUserList = (List<String>) ObjectMapperUtils.instance().convertValue(jzzyHget, List.class);
                        StringBuilder jzzyUser = new StringBuilder();
                        if (null != jzzyUserList && jzzyUserList.size() > 0) {
                            for (String jzzyCode : jzzyUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(jzzyUser.toString())) {
                                    jzzyUser.append("'").append(jzzyCode).append("'");
                                } else {
                                    jzzyUser.append(",'").append(jzzyCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_jzzyUser", jzzyUser.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_jzzyUser", "''");
                    }
                }
                if (dataAuth.contains("_orgXwUser")) {
                    Object orgXwHget = yuspRedisTemplate.hget("orgXwUser", currentUserCode);
                    if (Objects.nonNull(orgXwHget)) {
                        List<String> orgXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(orgXwHget, List.class);
                        StringBuilder orgXwUser = new StringBuilder();
                        if (null != orgXwUserList && orgXwUserList.size() > 0) {
                            for (String xwCode : orgXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(orgXwUser.toString())) {
                                    orgXwUser.append("'").append(xwCode).append("'");
                                } else {
                                    orgXwUser.append(",'").append(xwCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_orgXwUser", orgXwUser.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_orgXwUser", "''");
                    }
                }
                if (dataAuth.contains("_orgNotXwUser")) {
                    Object orgNotXwHget = yuspRedisTemplate.hget("orgNotXwUser", currentUserCode);
                    if (Objects.nonNull(orgNotXwHget)) {
                        List<String> orgNotXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(orgNotXwHget, List.class);
                        StringBuilder orgNotXwUser = new StringBuilder();
                        if (null != orgNotXwUserList && orgNotXwUserList.size() > 0) {
                            for (String xwCode : orgNotXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(orgNotXwUser.toString())) {
                                    orgNotXwUser.append("'").append(xwCode).append("'");
                                } else {
                                    orgNotXwUser.append(",'").append(xwCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_orgNotXwUser", orgNotXwUser.toString());
                    } else {
                        dataAuth = dataAuth.replaceAll("_orgNotXwUser", "''");
                    }
                }
            }
            logger.info("导出查询sql设置角色数据权限,API[{}],数据权限[{}]", apiUrl, dataAuth);
        } catch (Exception e) {
            logger.info("导出查询sql设置角色数据权限异常[{}]", e.getMessage());
            e.printStackTrace();
        }
        logger.info("导出查询sql设置角色数据权限结束");
        return dataAuth;
    }
}
