package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @创建人 WH
 * @创建时间 2021-05-08
 * @return WfPvpLoanAppLsBiz
 **/
@Service
public class WfPvpLoanAppLsSpBiz implements ClientBizInterface{

        private final Logger log = LoggerFactory.getLogger(WfPvpLoanAppLsSpBiz.class);
        @Autowired
        private AmqpTemplate amqpTemplate;
        @Autowired
        private PvpLoanAppService pvpLoanAppService;
        @Override
        public void bizOp(ResultInstanceDto resultInstanceDto) {
            String currentOpType = resultInstanceDto.getCurrentOpType();
            String extSerno = resultInstanceDto.getBizId();
            log.info("后业务处理类型:" + currentOpType);
            try {
                if (OpType.STRAT.equals(currentOpType)) {
                    log.info("发起操作:" + resultInstanceDto);
                }else if (OpType.RUN.equals(currentOpType)) {
                    log.info("-------业务处理：------");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
                }else if (OpType.JUMP.equals(currentOpType)) {
                    log.info("跳转操作:" + resultInstanceDto);
                } else if (OpType.END.equals(currentOpType)) {
                    log.info("结束操作:" + resultInstanceDto);
                    end(extSerno);
                    log.info("结束操作完成:" + resultInstanceDto);
                } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                    log.info("退回操作:" + resultInstanceDto);
                    boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                    if(isFirstNode){
                        updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                    }
                } else if (OpType.CALL_BACK.equals(currentOpType)) {
                    log.info("打回操作:" + resultInstanceDto);
                    boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                    if(isFirstNode){
                        updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                    }
                } else if (OpType.TACK_BACK.equals(currentOpType)) {
                    log.info("拿回操作:" + resultInstanceDto);
                    boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                    if(isFirstNode){
                        updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                    }
                } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                    log.info("拿回初始节点操作:" + resultInstanceDto);
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }else if (OpType.REFUSE.equals(currentOpType)) {
                    log.info("否决操作:" + resultInstanceDto);
                    // 否决改变标志 审批中 111-> 审批不通过 998
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
                } else {
                    log.warn("未知操作:" + resultInstanceDto);
                }
            } catch (Exception e) {
                log.error("后业务处理失败", e);
                try {
                    BizCommonUtils bizCommonUtils = new BizCommonUtils();
                    bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                } catch (Exception e1) {
                    log.error("发送异常消息失败", e1);
                }
            }
        }

        /**
         * @创建人 WH
         * @创建时间 2021-05-10 19:35
         * @注释 此处改为自己对应的流程名称 WF_PVP_LOAN_APP_LS
         */
        @Override
        public boolean should(ResultInstanceDto resultInstanceDto) {
            String flowCode = resultInstanceDto.getBizType();
            return "WF_PVP_LOAN_APP_LSSP".equals(flowCode);

        }

        /**
         * @创建人 WH
         * @创建时间 2021-04-25 21:53
         * @注释 审批状态更换   替换对应的 service
         */
        public void updateStatus(String serno,String state){
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            pvpLoanApp.setApproveStatus(state);
            pvpLoanAppService.updateSelective(pvpLoanApp);
        }
        /**
         * @创建人 WH
         * @创建时间 2021-04-25 21:53
         * @注释 审批通过 替换对应的service和 自己对应的修改完成后的方法 reviewend
         */
        public void end(String extSerno) {
            //审批通过的操作
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(extSerno);
            pvpLoanAppService.reviewend(pvpLoanApp);
        }

    }

