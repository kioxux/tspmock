package cn.com.yusys.yusp.service.server.xdtz0042;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.resp.Xdtz0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Optional;

/**
 * 接口处理类:申请人行内贷款五级分类异常笔数
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0042Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0042Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * 交易码：xdtz0042
     * 交易描述：申请人行内贷款五级分类异常笔数
     *
     * @param certNo
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0042DataRespDto getAccByCertNo(String certNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, certNo);
        Xdtz0042DataRespDto xdtz0042DataRespDto = new Xdtz0042DataRespDto();
        try {
            //通过客户证件号查询客户信息
            logger.info("***************XDTZ0042*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certNo)).orElse(new CusBaseClientDto());
            logger.info("***************XDTZ0042*根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusBaseClientDto));
            // 客户编号
            String cusId = cusBaseClientDto.getCusId();
            if (StringUtil.isNotEmpty(cusId)) {//查询客户号不能为空
                xdtz0042DataRespDto = accLoanMapper.getAccByCusId(cusId);
            } else {
                xdtz0042DataRespDto.setTotalNum(0);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataRespDto));
        return xdtz0042DataRespDto;
    }

}
