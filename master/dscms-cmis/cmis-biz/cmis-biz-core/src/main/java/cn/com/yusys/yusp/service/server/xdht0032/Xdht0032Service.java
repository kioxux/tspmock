package cn.com.yusys.yusp.service.server.xdht0032;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0032.req.Xdht0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0032.resp.Xdht0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 业务逻辑类:根据核心客户号查询我行信用类合同金额汇总
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdht0032Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0032Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易码：xdht0032
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0032DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0032DataRespDto xdht0032(Xdht0032DataReqDto xdht0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataReqDto));
        Xdht0032DataRespDto xdht0032DataRespDto = new Xdht0032DataRespDto();
        try {
            String queryType = xdht0032DataReqDto.getQueryType();//查询类型
            String cusId = xdht0032DataReqDto.getCusId();//客户号
            QueryModel queryModel = new QueryModel();
            BigDecimal cdtContTotlAmt = BigDecimal.ZERO;// 信用类合同总金额
            BigDecimal latestInureYqdContAmt = BigDecimal.ZERO;//最近生效优企贷合同金额
            List<BigDecimal> latestInureYqdContAmtList = new ArrayList<>();
            if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_APPLYAMOUNT.key, queryType)) {
                queryModel.addCondition("guar_way", "00");
                queryModel.addCondition("cont_status", "200");
                queryModel.addCondition("cus_id", cusId);
                logger.info("根据核心客户号查询我行信用类合同金额汇总开始,查询参数为:{}", JSON.toJSONString(queryModel));
                cdtContTotlAmt = ctrLoanContMapper.queryApplyAmount(queryModel);
                logger.info("根据核心客户号查询我行信用类合同金额汇总结束,返回结果为:{}", JSON.toJSONString(cdtContTotlAmt));
            } else if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_LASTYQDCONTAMT.key, queryType)) {
                queryModel.addCondition("biz_type", "SC010008");
                queryModel.addCondition("cont_status", "200");
                queryModel.addCondition("cus_id", cusId);
                queryModel.setSort(" sign_date desc");
                logger.info("查询最近生效的一笔优企贷合同金额开始,查询参数为:{}", JSON.toJSONString(queryModel));
                latestInureYqdContAmt = ctrLoanContMapper.queryLastYqdContAmt(queryModel);
                logger.info("查询最近生效的一笔优企贷合同金额结束,返回结果为:{}", JSON.toJSONString(latestInureYqdContAmt));
            }
            xdht0032DataRespDto.setCdtContTotlAmt(cdtContTotlAmt.toEngineeringString());
            xdht0032DataRespDto.setLatestInureYqdContAmt(latestInureYqdContAmt.toEngineeringString());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataRespDto));
        return xdht0032DataRespDto;
    }
}
