/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstZxd;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstZxdMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstZxdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-07-26 20:11:30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstZxdService {
    private Logger logger = LoggerFactory.getLogger(CusLstZxdService.class);
    @Autowired
    private CusLstZxdMapper cusLstZxdMapper;
    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstZxd selectByPrimaryKey(String serno) {
        return cusLstZxdMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstZxd> selectAll(QueryModel model) {
        List<CusLstZxd> records = (List<CusLstZxd>) cusLstZxdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstZxd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstZxd> list = cusLstZxdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstZxd record) {
        return cusLstZxdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstZxd record) {
        return cusLstZxdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstZxd record) {
        return cusLstZxdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstZxd record) {
        return cusLstZxdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstZxdMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstZxdMapper.deleteByIds(ids);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/8/5 14:14
     * @注释 根据客户号查询增享贷白名单
     */
    public CusLstZxdDto selectzxdbycusid(CusLstZxdDto cusLstZxdDto) {
        return cusLstZxdMapper.selectzxdbycusid(cusLstZxdDto);
    }


    /**
     * @param cusLstZxdDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/10/7 19:21
     * @version 1.0.0
     * @desc 客户经理点击生成调查表
     * 1、生成调查主表、基本表、模型结果信息
     * 2、更新白名单办理状态为‘03’--办理中
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto generatesurvey(CusLstZxdDto cusLstZxdDto) {
        logger.info("根据名单流水号【{}】生成调查表信息开始！", cusLstZxdDto.getSerno());
        // 增享贷名单对象信息
        CusLstZxd cusLstZxd = null;
        // 调查表主表信息
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = null;
        // 调查表基本信息
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = null;

        try {
            logger.info("根据名单流水号【{}】查询增享贷白名单信息开始！", cusLstZxdDto.getSerno());
            cusLstZxd = this.cusLstZxdMapper.selectByPrimaryKey(cusLstZxdDto.getSerno());
            logger.info("根据名单流水号【{}】查询增享贷白名单信息而结束！", cusLstZxdDto.getSerno());

            logger.info("根据名单流水号【{}】查询调查表主表信息开始！", cusLstZxdDto.getSerno());
            lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByListSerno(cusLstZxdDto.getSerno());
            if (Objects.isNull(lmtSurveyReportMainInfo)) {
                return new ResultDto().code("9999").message("未查询到调查主表信息！");
            }
            logger.info("根据名单流水号【{}】查询调查表主表信息结束！", cusLstZxdDto.getSerno());

            logger.info("根据调查流水号【{}】查询调查表基本信息开始！", lmtSurveyReportMainInfo.getSurveySerno());
            lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.isNull(lmtSurveyReportBasicInfo)) {
                return new ResultDto().code("9999").message("未查询到调查基本信息！");
            }
            logger.info("根据调查流水号【{}】查询调查表基本信息结束！", lmtSurveyReportMainInfo.getSurveySerno());

            // 更新调查表信息开始
            lmtSurveyReportMainInfo.setManagerId(cusLstZxd.getManagerId());
            lmtSurveyReportMainInfo.setManagerBrId(cusLstZxd.getManagerBrId());
            lmtSurveyReportMainInfo.setUpdateTime(new Date());
            lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);

            lmtSurveyReportBasicInfo.setManagerId(cusLstZxd.getManagerId());
            lmtSurveyReportBasicInfo.setManagerBrId(cusLstZxd.getManagerBrId());
            lmtSurveyReportBasicInfo.setUpdateTime(new Date());
            lmtSurveyReportBasicInfoService.update(lmtSurveyReportBasicInfo);

            // 更新白名单状态开始 更新为 03--办理中
            cusLstZxd.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_03.key);
            cusLstZxdMapper.updateByPrimaryKey(cusLstZxd);
        } catch (BeansException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return new ResultDto().code("9999").message("生成调查表信息异常！" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return new ResultDto().code("9999").message("生成调查表信息异常！");
        }
        logger.info("根据名单流水号【{}】生成调查表信息结束！", cusLstZxdDto.getSerno());
        return new ResultDto().message("生成调查表信息成功!");
    }

}
