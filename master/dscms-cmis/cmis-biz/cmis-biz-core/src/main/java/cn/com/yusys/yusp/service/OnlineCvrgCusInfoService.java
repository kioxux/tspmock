/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OnlineCvrgCusInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.OnlineCvrgCusInfoMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OnlineCvrgCusInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 20:50:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OnlineCvrgCusInfoService {
    private static final Logger log = LoggerFactory.getLogger(OnlineCvrgCusInfoService.class);
    @Autowired
    private OnlineCvrgCusInfoMapper onlineCvrgCusInfoMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private AdminSmUserService  adminSmUserService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OnlineCvrgCusInfo selectByPrimaryKey(String serno) {
        return onlineCvrgCusInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OnlineCvrgCusInfo> selectAll(QueryModel model) {
        List<OnlineCvrgCusInfo> records = (List<OnlineCvrgCusInfo>) onlineCvrgCusInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OnlineCvrgCusInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OnlineCvrgCusInfo> list = onlineCvrgCusInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OnlineCvrgCusInfo record) {
        return onlineCvrgCusInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OnlineCvrgCusInfo record) {
        return onlineCvrgCusInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OnlineCvrgCusInfo record) {
        return onlineCvrgCusInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OnlineCvrgCusInfo record) {
        return onlineCvrgCusInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return onlineCvrgCusInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return onlineCvrgCusInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: onlineCvrgCusInfolist
     * @方法描述: 获取在线投标保函预约管理在途预约
     * @创建者： zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<OnlineCvrgCusInfo> onlineCvrgCusInfolist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyResult", 1);
        return onlineCvrgCusInfoMapper.onlineCvrgCusInfolist(model);
    }

    /**
     * @方法名称: onlineCvrgCusInfoHislist
     * @方法描述: 获取在线投标保函预约管理在历史预约信息
     * @创建者： zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<OnlineCvrgCusInfo> onlineCvrgCusInfoHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyHisResult", 1);
        return onlineCvrgCusInfoMapper.onlineCvrgCusInfolist(model);
    }

    /**
     * @方法名称: queryOnlineCvrgCusInfoByParams
     * @方法描述: 根据入参查询在线投标保函预约
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zl
     * @创建时间: 2021-09-28 14:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public OnlineCvrgCusInfo queryOnlineCvrgCusInfoByParams(Map queryMap) {
        return onlineCvrgCusInfoMapper.onlineCvrgCusInfoMapper(queryMap);
    }

    /**
     * @方法名称: onlineCvrgCusInfoUpdate
     * @方法描述: 在线投标保函预约管理分配任务
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onlineCvrgCusInfoUpdate(OnlineCvrgCusInfo record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String cusName = ""; // 企业客户名称
        String contacts = ""; // 联系人
        String phone = "";// 联系电话
        try {
            log.info("在线投标保函预约管理任务分配开始！");
            cusName = record.getCusName();

            log.info("在线投标保函预约管理任务分配发送短信开始" + record);
            //短信通知
            String managerId = record.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                contacts = record.getContacts();
                phone = record.getPhone();
                //短信模板 企业客户"+cus_name+"已向我行进行电子投标保函业务申请，请尽快核实情况后联系客户（联系人："+contacts+",联系电话："+phone+"）
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = "MSG_XW_M_0016";//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", cusName);
                paramMap.put("contacts", contacts);
                paramMap.put("phone", phone);
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            }
            log.info("在线投标保函预约管理任务分配发送短信结束" + record);

            // 获取修改日期
            record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info("在线投标保函预约管理任务分配开始" + record);
            int count = onlineCvrgCusInfoMapper.updateByPrimaryKey(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "在线投标保函预约管理任务分配失败！");
            }
            log.info("在线投标保函预约管理任务分配结束！");
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("在线投标保函预约管理任务分配出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

}
