/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocDestroyDetailList;
import cn.com.yusys.yusp.service.DocDestroyDetailListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyDetailListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 20:16:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "档案销毁明细")
@RestController
@RequestMapping("/api/docDestroyDetailList")
public class DocDestroyDetailListResource {

    @Autowired
    private DocDestroyDetailListService docDestroyDetailListService;
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("档案销毁明细_查询档案销毁明细列表（分页查询）")
    @PostMapping("/query/pageList")
    protected ResultDto<List<DocDestroyDetailList>> index(@RequestBody QueryModel queryModel) {
        List<DocDestroyDetailList> list = docDestroyDetailListService.selectByModel(queryModel);
        return new ResultDto<List<DocDestroyDetailList>>(list);
    }

}
