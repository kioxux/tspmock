package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppAssist;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-10 14:42:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanAppSLDto implements Serializable{
    private static final long serialVersionUID = 1L;

    private IqpLoanApp iqpLoanApp;
    private IqpLoanAppAssist iqpLoanAppAssist;
    private IqpHouse iqpHouse;

    // 审批模式：1-单签，2-双签1,3-双签2,4-贷审会模式
    private String approvalModel;

    // 是否pc端发起:1-是,0-否
    private String isPc;

    //自动审批
    private String limitFlag;

    //报价利率
    private java.math.BigDecimal offerRate;

    //综合风险等级
    private String inteRiskLvl;

    //规则风险等级
    private String ruleRiskLvl;

    // 关联方交易是否超权 0-否,1-是
    private String isRelationSuperPower;

    public String getIsPc() {
        return isPc;
    }

    public void setIsPc(String isPc) {
        this.isPc = isPc;
    }

    public String getApprovalModel() {
        return approvalModel;
    }

    public void setApprovalModel(String approvalModel) {
        this.approvalModel = approvalModel;
    }

    public IqpLoanApp getIqpLoanApp() {
        return iqpLoanApp;
    }

    public void setIqpLoanApp(IqpLoanApp iqpLoanApp) {
        this.iqpLoanApp = iqpLoanApp;
    }

    public IqpLoanAppAssist getIqpLoanAppAssist() {
        return iqpLoanAppAssist;
    }

    public void setIqpLoanAppAssist(IqpLoanAppAssist iqpLoanAppAssist) {
        this.iqpLoanAppAssist = iqpLoanAppAssist;
    }

    public IqpHouse getIqpHouse() {
        return iqpHouse;
    }

    public void setIqpHouse(IqpHouse iqpHouse) {
        this.iqpHouse = iqpHouse;
    }

    public String getLimitFlag() {
        return limitFlag;
    }

    public void setLimitFlag(String limitFlag) {
        this.limitFlag = limitFlag;
    }

    public String getInteRiskLvl() {
        return inteRiskLvl;
    }

    public void setInteRiskLvl(String inteRiskLvl) {
        this.inteRiskLvl = inteRiskLvl;
    }

    public BigDecimal getOfferRate() {
        return offerRate;
    }

    public void setOfferRate(BigDecimal offerRate) {
        this.offerRate = offerRate;
    }

    public String getRuleRiskLvl() {
        return ruleRiskLvl;
    }

    public void setRuleRiskLvl(String ruleRiskLvl) {
        this.ruleRiskLvl = ruleRiskLvl;
    }

    public String getIsRelationSuperPower() {
        return isRelationSuperPower;
    }

    public void setIsRelationSuperPower(String isRelationSuperPower) {
        this.isRelationSuperPower = isRelationSuperPower;
    }

}
