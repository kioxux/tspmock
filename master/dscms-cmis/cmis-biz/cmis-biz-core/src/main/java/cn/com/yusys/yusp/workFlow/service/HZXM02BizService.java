package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.BusiImageRelInfo;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.domain.CoopPlanApp;
import cn.com.yusys.yusp.domain.CoopProAccInfo;
import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @param
 * @功能描述:合作方协议准入变更流程
 * @author: yumeng
 * @date: 19:29 2021/5/18
 * @return:
 */
@Service
public class HZXM02BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(HZXM02BizService.class);

    @Autowired
    private CoopPlanAppService coopPlanAppService;

    @Autowired
    private CoopPlanProInfoService coopPlanProInfoService;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

    @Autowired
    private CoopProAccInfoService coopProAccInfoService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private BusiImageRelInfoService busiImageRelInfoService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
            String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("合作方准入申请后业务处理类型" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        try {
            CoopPlanApp coopPlanApp = coopPlanAppService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPlanAppService.update(coopPlanApp);
                log.info("合作方准入申请【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPlanAppService.update(coopPlanApp);
                log.info("合作方准入申请【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                if (CmisBizConstants.COOP_PLAN_HZ001.equals(bizType) || CmisBizConstants.COOP_PLAN_HZ003.equals(bizType)) {
                    //合作方申请
                    coopPlanAppService.wfApply(serno);
                } else if (CmisBizConstants.COOP_PLAN_HZ002.equals(bizType) || CmisBizConstants.COOP_PLAN_HZ004.equals(bizType)) {
                    //合作方变更
                    coopPlanAppService.wfAlter(serno);
                }

                //调用影像平台交易，为该笔业务影像批量打上已审核标识。
                sendImage(resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        coopPlanAppService.update(coopPlanApp);
                    }
                });
                log.info("合作方准入申请【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPlanAppService.update(coopPlanApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPlanAppService.update(coopPlanApp);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                coopPlanAppService.update(coopPlanApp);
            } else {
                log.info("合作方准入申请【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方准入申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "HZXM02".equals(flowCode);
    }

    /**
     * 审批流程通过后，调用影像平台交易，为该笔业务影像批量打上已审核标识。无需推送审核人工号。
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        String serno = resultInstanceDto.getBizId();

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<BusiImageRelInfo> busiImageRelInfos = busiImageRelInfoService.selectAll(queryModel);

        if (CollectionUtils.isNotEmpty(busiImageRelInfos)){
            for (BusiImageRelInfo busiImageRelInfo : busiImageRelInfos) {
                String topOutsystemCode = busiImageRelInfo.getTopOutsystemCode();
                //影像编号
                String imageNo = busiImageRelInfo.getImageNo();
                log.info("合作方项目准入、变更流程，业务流水号【"+serno+"】，影像编号【"+imageNo+"】，topOutSystemCode："+topOutsystemCode);
                bizCommonService.sendImage(imageNo,topOutsystemCode,"");
            }
        }
    }
}