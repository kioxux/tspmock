package cn.com.yusys.yusp.web.server.xdcz0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0004.req.Xdcz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.resp.Xdcz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0004.Xdcz0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:承兑签发审批结果综合服务
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0004:承兑签发审批结果综合服务")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0004.BizXdcz0004Resource.class);
    @Autowired
    private Xdcz0004Service xdcz0004Service;
    /**
     * 交易码：xdcz0004
     * 交易描述：承兑签发审批结果综合服务
     *
     *
     * @param xdcz0004DataReqDto
     * @throws Exception
     * @return
     */


    @ApiOperation("承兑签发审批结果综合服务")
    @PostMapping("/xdcz0004")
    protected @ResponseBody
    ResultDto<Xdcz0004DataRespDto>  xdcz0004(@Validated @RequestBody Xdcz0004DataReqDto xdcz0004DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004DataReqDto));
        Xdcz0004DataRespDto  xdcz0004DataRespDto  = new Xdcz0004DataRespDto();//
        ResultDto<Xdcz0004DataRespDto>xdcz0004DataResultDto = new ResultDto<>();
        BigDecimal paySecurityAmt = xdcz0004DataReqDto.getPaySecurityAmt();//已交保证金金额
        String pvpStatus = xdcz0004DataReqDto.getPvpStatus();//出账状态
        String drfpoIsseMk = xdcz0004DataReqDto.getDrfpoIsseMk();//票据池出票标记
        String serno = xdcz0004DataReqDto.getSerno();//批次号
        try {
            // 从xdcz0004DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdcz0004DataRespDto = xdcz0004Service.xdcz0004(xdcz0004DataReqDto);

            xdcz0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value,e.getMessage());
            // 封装xdcz0004DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0004DataRespDto到xdcz0004DataResultDto中
        xdcz0004DataResultDto.setData(xdcz0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004DataRespDto));
        return xdcz0004DataResultDto;
    }
}
