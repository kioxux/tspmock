package cn.com.yusys.yusp.util;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * FTP实用工具类
 *
 * @author
 * @version 1.0
 * @since 2021年6月30日 下午9:56:54
 */
public class FtpUtils {
    private static final Logger logger = LoggerFactory.getLogger(FtpUtils.class);

    /**
     * 登录用户名,密码,IP等参数
     */
    private String name, password, ip;
    /**
     * 登录端口
     */
    private int port;
    /**
     * FTP操作对象
     */
    private FTPClient ftpClient;

    /**
     * 构造函数
     *
     * @param userName 用户名
     * @param password 密码
     * @param ip       IP
     * @param port     端口
     * @throws Exception
     */
    public FtpUtils(String userName, String password, String ip, int port) throws Exception {
        logger.info("ip>>" + ip + " port>>" + port + " userName>>" + userName + " password>>" + password);
        this.init(userName, password, ip, port);
    }


    /**
     * 构造函数 初始化传入要连接的FTP配置信息
     *
     * @param
     * @throws Exception
     */
    public FtpUtils(String ip,int port,String userName,String password) throws Exception {
        logger.info("ip>>" + ip + " port>>" + port + " userName>>" + userName + " password>>" + password);
        this.init(userName, password, ip, port);
    }

    /**
     * 检查远程FTP服务端文件是否存在
     *
     * @param remoteFolder 远程文件路径
     * @param fileName     文件名
     * @return
     * @throws Exception
     */
    public boolean checkFTPFile(String remoteFolder, String fileName) throws Exception {
        boolean flag = false;
        try {
            // 切换到指定目录
            ftpClient.changeWorkingDirectory(remoteFolder);
            logger.info("切换到FTP远程目录成功：" + remoteFolder);
            // 检查文件是否存在
            flag = this.isFtpFileExists(fileName);
            logger.info("FTP远程文件" + remoteFolder + fileName + (flag ? "存在" : "不存在"));
        } catch (Exception e) {
            throw e;
        } finally {
            // this.closeConnect();
        }
        return flag;
    }

    /**
     * 初始化FTP连接参数
     *
     * @param userName 用户名
     * @param password 密码
     * @param ip       IP
     * @param port     端口
     */
    private void init(String userName, String password, String ip, int port) {
        this.name = userName;
        this.password = password;
        this.ip = ip;
        this.port = port;
    }

    /**
     * 检查远程FTP服务端文件是否存在
     *
     * @param fileName 文件名
     * @author leehuang
     * @version 1.0
     * @since 2020年6月12日 下午9:56:54
     */
    private boolean isFtpFileExists(String fileName) throws Exception {
        boolean flag = false;
        FTPFile[] files = ftpClient.listFiles();
        for (FTPFile file : files) {
            if (file.isFile()) {
                if (fileName.equals(file.getName())) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 关闭远程FTP服务器连接
     */
    public void closeConnect() {
        /** 判断当前ftpClient对象不为null和FTP已经被连接就关闭 */
        if (this.ftpClient != null && this.ftpClient.isConnected()) {
            try {
                this.ftpClient.disconnect();
                /** 销毁FTP连接 */
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    /**
     * @Title: connectServer
     * @Description: 连接到FTP服务器
     * @Parmaters: @throws Exception
     * @Return: void
     * @Throws:
     * @Author:simon
     * @CreateDate:2016-1-22上午09:46:58
     * @Version:0.1
     * @ModifyLog:2016-1-22上午09:46:58
     */
    public void connectServer() throws Exception {
        /** 如果当前ftpClient对象是null且没有连接就连接到FTP服务器 */
        if (this.ftpClient == null || !this.ftpClient.isConnected()) {
            try {
                /** 如果ftpClient对象为null就实例化一个新的 */
                this.ftpClient = new FTPClient();
                this.ftpClient.setDefaultPort(this.port);
                /** 设置默认的IP地址 */
                this.ftpClient.connect(this.ip);
                /** 连接到FTP服务器 */
                /** 登录到这台FTP服务器 */
                if (ftpClient.login(this.name, this.password)) {
                    logger.info("FTP Server: " + this.ip + "'s User " + this.name + " 登录成功!");
                } else {
                    throw BizException.error(null, EcnEnum.ECN060008.key, EcnEnum.ECN060008.value, this.ip, this.name);
                }
            } catch (BizException e) {
                throw e;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw BizException.error(null, EcnEnum.ECN060007.key, EcnEnum.ECN060007.value + e.getMessage());
            }
        }
    }

    /**
     * 下载远程FTP服务器文件至本地
     *
     * @param remoteFile 远程文件绝对路径
     * @param localFile  本地文件绝对路径
     * @throws Exception
     */
    public void download(String remoteFile, String localFile) throws Exception {
        logger.info("下载远程FTP服务器文件至本地开始，远程文件绝对路径：[" + remoteFile + "]、本地文件绝对路径：[" + localFile + "]");
        BufferedOutputStream buffOut = null;
        try {
            buffOut = new BufferedOutputStream(new FileOutputStream(localFile));
            /** 写入本地文件 */
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.retrieveFile(remoteFile, buffOut);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcnEnum.ECN060010.key, EcnEnum.ECN060010.value + e.getMessage());
        } finally {
            try {
                if (buffOut != null) {
                    buffOut.close();
                }
            } catch (Exception e) {
                logger.error("下载远程FTP服务器文件至本地时，关闭buffOut发生异常," + e.getMessage(), e);
            }
        }
        logger.info("下载远程FTP服务器文件至本地结束，远程文件绝对路径：[" + remoteFile + "]、本地文件绝对路径：[" + localFile + "]");
    }

    /**
     * 上传文件至服务器
     * @param remoteFile
     * @param localFile
     * @throws Exception
     */
    public void upload(String remoteFile, File localFile) throws Exception {
        logger.info("上传文件至远程FTP服务器开始，远程文件绝对路径：[" + remoteFile + "]、本地文件绝对路径：[" + localFile + "]");
        BufferedInputStream inputStream = null;
        try {
            /** 上传文件 */
            inputStream = new BufferedInputStream(new FileInputStream(localFile));
            ftpClient.changeWorkingDirectory(remoteFile);
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.storeFile(localFile.getName(),inputStream);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcnEnum.ECN060011.key, EcnEnum.ECN060011.value + e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                logger.error("上传文件至远程FTP服务器结束，关闭buffOut发生异常," + e.getMessage(), e);
            }
        }
        logger.info("上传文件至远程FTP服务器结束，远程文件绝对路径：[" + remoteFile + "]、本地文件绝对路径：[" + localFile + "]");
    }
    /** 创建文件夹
     * @param remoteFile
     * @param newDirectory
     * @throws Exception
     */
    public void makeDirectory(String remoteFile,String openDay,String newDirectory) throws Exception {
        logger.info("远程文件绝对路径：[" + remoteFile + "]、新增文件夹：[" + newDirectory + "]");
        try {
            /** 创建文件夹 */
            boolean flag =ftpClient.changeWorkingDirectory(remoteFile+"/"+newDirectory);
            if(!flag){
                ftpClient.changeWorkingDirectory(remoteFile);
                ftpClient.makeDirectory(openDay);
                ftpClient.changeWorkingDirectory(remoteFile+"/"+openDay);
                ftpClient.makeDirectory(newDirectory);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcnEnum.ECN060012.key, EcnEnum.ECN060012.value + e.getMessage());
        }
        logger.info("远程文件绝对路径：[" + remoteFile + "]、新增文件夹：[" + newDirectory + "]");
    }
}
