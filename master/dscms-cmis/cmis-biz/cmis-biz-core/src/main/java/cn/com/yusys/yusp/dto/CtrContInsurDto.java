package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContInsur
 * @类描述: ctr_cont_insur数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-05 16:30:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrContInsurDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 是否投保 STD_ZB_IS_INSUR **/
	private String isInsur;
	
	/** 保险种类 STD_ZB_INSUR_TYPE **/
	private String insurType;
	
	/** 保险单编号 **/
	private String insurNo;
	
	/** 保险起始日期 **/
	private String insurBegDate;
	
	/** 保险截止日期 **/
	private String insurEndDate;
	
	/** 保险单签订日期 **/
	private String insurSignDate;
	
	/** 保险机构名称 **/
	private String insurOrgName;
	
	/** 保险金额（元） **/
	private java.math.BigDecimal insurAmt;
	
	/** 保险费（元） **/
	private java.math.BigDecimal insurFee;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param isInsur
	 */
	public void setIsInsur(String isInsur) {
		this.isInsur = isInsur == null ? null : isInsur.trim();
	}
	
    /**
     * @return IsInsur
     */	
	public String getIsInsur() {
		return this.isInsur;
	}
	
	/**
	 * @param insurType
	 */
	public void setInsurType(String insurType) {
		this.insurType = insurType == null ? null : insurType.trim();
	}
	
    /**
     * @return InsurType
     */	
	public String getInsurType() {
		return this.insurType;
	}
	
	/**
	 * @param insurNo
	 */
	public void setInsurNo(String insurNo) {
		this.insurNo = insurNo == null ? null : insurNo.trim();
	}
	
    /**
     * @return InsurNo
     */	
	public String getInsurNo() {
		return this.insurNo;
	}
	
	/**
	 * @param insurBegDate
	 */
	public void setInsurBegDate(String insurBegDate) {
		this.insurBegDate = insurBegDate == null ? null : insurBegDate.trim();
	}
	
    /**
     * @return InsurBegDate
     */	
	public String getInsurBegDate() {
		return this.insurBegDate;
	}
	
	/**
	 * @param insurEndDate
	 */
	public void setInsurEndDate(String insurEndDate) {
		this.insurEndDate = insurEndDate == null ? null : insurEndDate.trim();
	}
	
    /**
     * @return InsurEndDate
     */	
	public String getInsurEndDate() {
		return this.insurEndDate;
	}
	
	/**
	 * @param insurSignDate
	 */
	public void setInsurSignDate(String insurSignDate) {
		this.insurSignDate = insurSignDate == null ? null : insurSignDate.trim();
	}
	
    /**
     * @return InsurSignDate
     */	
	public String getInsurSignDate() {
		return this.insurSignDate;
	}
	
	/**
	 * @param insurOrgName
	 */
	public void setInsurOrgName(String insurOrgName) {
		this.insurOrgName = insurOrgName == null ? null : insurOrgName.trim();
	}
	
    /**
     * @return InsurOrgName
     */	
	public String getInsurOrgName() {
		return this.insurOrgName;
	}
	
	/**
	 * @param insurAmt
	 */
	public void setInsurAmt(java.math.BigDecimal insurAmt) {
		this.insurAmt = insurAmt;
	}
	
    /**
     * @return InsurAmt
     */	
	public java.math.BigDecimal getInsurAmt() {
		return this.insurAmt;
	}
	
	/**
	 * @param insurFee
	 */
	public void setInsurFee(java.math.BigDecimal insurFee) {
		this.insurFee = insurFee;
	}
	
    /**
     * @return InsurFee
     */	
	public java.math.BigDecimal getInsurFee() {
		return this.insurFee;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}