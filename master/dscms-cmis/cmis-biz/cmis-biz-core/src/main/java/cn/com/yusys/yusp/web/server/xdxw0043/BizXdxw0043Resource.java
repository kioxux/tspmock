package cn.com.yusys.yusp.web.server.xdxw0043;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0043.req.Xdxw0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0043.resp.Xdxw0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0043.Xdxw0043Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:无还本续贷待办接收
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0043:无还本续贷待办接收")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0043Resource.class);

    @Autowired
    private Xdxw0043Service xdxw0043Service;

    /**
     * 交易码：xdxw0043
     * 交易描述：无还本续贷待办接收
     *
     * @param xdxw0043DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("无还本续贷待办接收")
    @PostMapping("/xdxw0043")
    protected @ResponseBody
    ResultDto<Xdxw0043DataRespDto> xdxw0043(@Validated @RequestBody Xdxw0043DataReqDto xdxw0043DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataReqDto));
        Xdxw0043DataRespDto xdxw0043DataRespDto = new Xdxw0043DataRespDto();// 响应Dto:无还本续贷待办接收
        ResultDto<Xdxw0043DataRespDto> xdxw0043DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0024DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataReqDto));
            xdxw0043DataRespDto = xdxw0043Service.xdcz0043(xdxw0043DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataRespDto));
            // 封装xdxw0043DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0043DataRespDto.getOpFlag();
            String opMessage = xdxw0043DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0043DataResultDto.setMessage(opMessage);
            } else {
                xdxw0043DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0043DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, e.getMessage());
            // 封装xdxw0043DataResultDto中异常返回码和返回信息
            xdxw0043DataResultDto.setCode(e.getErrorCode());
            xdxw0043DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, e.getMessage());
            // 封装xdxw0043DataResultDto中异常返回码和返回信息
            xdxw0043DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0043DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0043DataRespDto到xdxw0043DataResultDto中
        xdxw0043DataResultDto.setData(xdxw0043DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataResultDto));
        return xdxw0043DataResultDto;
    }
}
