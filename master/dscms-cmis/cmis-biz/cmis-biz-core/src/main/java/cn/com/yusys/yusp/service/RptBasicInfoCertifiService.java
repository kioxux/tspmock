/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.CusCorpCertDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasicInfoCertifi;
import cn.com.yusys.yusp.repository.mapper.RptBasicInfoCertifiMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoCertifiService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:57:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicInfoCertifiService {

    @Autowired
    private RptBasicInfoCertifiMapper rptBasicInfoCertifiMapper;
	@Autowired
    private ICusClientService iCusClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptBasicInfoCertifi selectByPrimaryKey(String pkId) {
        return rptBasicInfoCertifiMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptBasicInfoCertifi> selectAll(QueryModel model) {
        List<RptBasicInfoCertifi> records = (List<RptBasicInfoCertifi>) rptBasicInfoCertifiMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptBasicInfoCertifi> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasicInfoCertifi> list = rptBasicInfoCertifiMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptBasicInfoCertifi record) {
        return rptBasicInfoCertifiMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptBasicInfoCertifi record) {
        return rptBasicInfoCertifiMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptBasicInfoCertifi record) {
        return rptBasicInfoCertifiMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptBasicInfoCertifi record) {
        return rptBasicInfoCertifiMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptBasicInfoCertifiMapper.deleteByPrimaryKey(pkId);
    }

    public List<RptBasicInfoCertifi> selectBySerno(String serno){
        return rptBasicInfoCertifiMapper.selectBySerno(serno);
    }
    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicInfoCertifiMapper.deleteByIds(ids);
    }

    public List<RptBasicInfoCertifi> initCertifi(QueryModel model){
        String serno = model.getCondition().get("serno").toString();
        String cusId = model.getCondition().get("cusId").toString();
        List<RptBasicInfoCertifi> resultData = new ArrayList<>();
        List<RptBasicInfoCertifi> rptBasicInfoCertifis = selectBySerno(serno);
        if(CollectionUtils.nonEmpty(rptBasicInfoCertifis)){
            return rptBasicInfoCertifis;
        }else{
            ResultDto<List<CusCorpCertDto>> listResultDto = iCusClientService.selectByCusId(cusId);
            if(listResultDto!=null&&listResultDto.getData()!=null){
                List<CusCorpCertDto> data = new ArrayList<>();
                data = listResultDto.getData();
                if(CollectionUtils.nonEmpty(data)){
                    for (Object map : data) {
                        CusCorpCertDto cusCorpCertDto = ObjectMapperUtils.instance().convertValue(map, CusCorpCertDto.class);
                        RptBasicInfoCertifi rptBasicInfoCertifi = new RptBasicInfoCertifi();
                        rptBasicInfoCertifi.setPkId(StringUtils.getUUID());
                        rptBasicInfoCertifi.setSerno(serno);
                        rptBasicInfoCertifi.setLiceName(cusCorpCertDto.getAptiName());
                        rptBasicInfoCertifi.setLiceNo(cusCorpCertDto.getCertCode());
                        rptBasicInfoCertifi.setLiceIdate(cusCorpCertDto.getEndDate());
                        int count = rptBasicInfoCertifiMapper.insertSelective(rptBasicInfoCertifi);
                        if(count >0){
                            resultData.add(rptBasicInfoCertifi);
                        }
                    }
                }
            }
        }
        return resultData;
    }
    public int save(RptBasicInfoCertifi rptBasicInfoCertifi){
        String pkId = rptBasicInfoCertifi.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptBasicInfoCertifi);
        }else {
            rptBasicInfoCertifi.setPkId(StringUtils.getUUID());
            return insert(rptBasicInfoCertifi);
        }
    }
}
