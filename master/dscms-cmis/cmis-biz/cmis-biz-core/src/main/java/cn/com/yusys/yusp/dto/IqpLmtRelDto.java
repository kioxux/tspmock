package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLmtRel
 * @类描述: iqp_lmt_rel数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-16 11:31:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLmtRelDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 申请流水号 **/
	private String iqpSerno;

	/** 授信协议编号 **/
	private String lmtCtrNo;

	/** 授信台账编号 **/
	private String lmtLimitNo;

	/** 额度类型 STD_ZB_THI_LMT_TYP **/
	private String limitType;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/**第三方额度类型**/
	private String thirdLimitType;
	/**第三方额度编号**/
	private String thirdLimitId;

	//用于第三方额度校验的入参
	/**客户编号**/
	private String cusId;
	/**产品编号**/
	private String prdId;
	/**融资担保 单户限额**/
	private BigDecimal singleAmt;
	/**融资担保 担保总敞口限额**/
	private BigDecimal guarTotlSpac;
	/**融资担保 产品总限额**/
	private BigDecimal totlAmt;
	/**融资担保 产品单户限额   + 合作方额度  单户限额**/
	private BigDecimal sigAmt;
	/**融资担保 产品单笔限额**/
	private BigDecimal oneAmt;
	/**合作方额度  授信限额**/
	private BigDecimal lmtAmt;
	/**校验阶段  第三方额度校验的阶段：新增页保存+修改页保存**/
	private String thCheckStage;
	/**申请主表  申请金额**/
	private BigDecimal appAmt;
	/**申请主表   风险敞口余额**/
	private BigDecimal riskOpenAmt;


	public BigDecimal getRiskOpenAmt() {
		return riskOpenAmt;
	}

	public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}

	public String getThCheckStage() {
		return thCheckStage;
	}

	public void setThCheckStage(String thCheckStage) {
		this.thCheckStage = thCheckStage;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdId() {
		return prdId;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public BigDecimal getGuarTotlSpac() {
		return guarTotlSpac;
	}

	public void setGuarTotlSpac(BigDecimal guarTotlSpac) {
		this.guarTotlSpac = guarTotlSpac;
	}

	public BigDecimal getOneAmt() {
		return oneAmt;
	}

	public void setOneAmt(BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}

	public BigDecimal getSigAmt() {
		return sigAmt;
	}

	public void setSigAmt(BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}

	public BigDecimal getSingleAmt() {
		return singleAmt;
	}

	public void setSingleAmt(BigDecimal singleAmt) {
		this.singleAmt = singleAmt;
	}

	public BigDecimal getTotlAmt() {
		return totlAmt;
	}

	public void setTotlAmt(BigDecimal totlAmt) {
		this.totlAmt = totlAmt;
	}

	public String getThirdLimitId() { return thirdLimitId; }
	public void setThirdLimitId(String thirdLimitId) { this.thirdLimitId = thirdLimitId; }


	public String getThirdLimitType() { return thirdLimitType; }
	public void setThirdLimitType(String thirdLimitType) { this.thirdLimitType = thirdLimitType; }



	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

    /**
     * @return PkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}

    /**
     * @return IqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}

    /**
     * @return LmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}

	/**
	 * @param lmtLimitNo
	 */
	public void setLmtLimitNo(String lmtLimitNo) {
		this.lmtLimitNo = lmtLimitNo == null ? null : lmtLimitNo.trim();
	}

    /**
     * @return LmtLimitNo
     */
	public String getLmtLimitNo() {
		return this.lmtLimitNo;
	}

	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType == null ? null : limitType.trim();
	}

    /**
     * @return LimitType
     */
	public String getLimitType() {
		return this.limitType;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}


}
