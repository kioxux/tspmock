/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.dto.LmtGrpAppSubDto;
import cn.com.yusys.yusp.service.LmtGrpMemRelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpAppr;
import cn.com.yusys.yusp.service.LmtGrpApprService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpApprResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-14 09:54:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrpappr")
public class LmtGrpApprResource {
    @Autowired
    private LmtGrpApprService lmtGrpApprService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpAppr> list = lmtGrpApprService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpAppr>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpAppr>> index(QueryModel queryModel) {
        List<LmtGrpAppr> list = lmtGrpApprService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpAppr> show(@PathVariable("pkId") String pkId) {
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpAppr>(lmtGrpAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpAppr> create(@RequestBody LmtGrpAppr lmtGrpAppr) throws URISyntaxException {
        lmtGrpApprService.insert(lmtGrpAppr);
        return new ResultDto<LmtGrpAppr>(lmtGrpAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpAppr lmtGrpAppr) throws URISyntaxException {
        int result = lmtGrpApprService.update(lmtGrpAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: queryLmtGrpApprByGrpSerno
     * @函数描述: 集团申请信息数据详情查询接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询审批数据")
    @PostMapping("/querylmtgrpapprbygrpserno")
    protected ResultDto<LmtGrpAppr> queryLmtGrpApprByGrpSerno(@RequestBody Map<String,String> params) {
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryLmtGrpApprByGrpSerno(params.get("grpSerno"));
        return new ResultDto<>(lmtGrpAppr);
    }

    /**
     * @函数名称: queryLmtGrpApprByGrpSernoAndIssueReportType
     * @函数描述: 根据集团申请流水号和出具报告类型查询审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号和出具报告类型查询审批数据")
    @PostMapping("/querylmtgrpapprbygrpsernoandissuereporttype")
    protected ResultDto<LmtGrpAppr> queryLmtGrpApprByGrpSernoAndIssueReportType(@RequestBody Map<String,String> params) {
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryLmtGrpApprByGrpSernoAndIssueReportType(params);
        return new ResultDto<>(lmtGrpAppr);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtGrpAppr> save(@RequestBody LmtGrpAppr lmtGrpAppr) throws URISyntaxException {
        LmtGrpAppr lmtGrpAppr1 = lmtGrpApprService.selectByPrimaryKey(lmtGrpAppr.getPkId());
        lmtGrpAppr.setOprType("01");
        if(lmtGrpAppr1!=null){
            lmtGrpApprService.update(lmtGrpAppr);
        }else{
            lmtGrpApprService.insert(lmtGrpAppr);
        }
        return ResultDto.success(lmtGrpAppr);
    }

    /**
     * @函数名称: querySernoByGrpSerno
     * @函数描述: 根据集团申请流水号查询单一客户流水号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询审批数据")
    @PostMapping("/querySernoByGrpSerno")
    protected ResultDto<List<String>> querySernoByGrpSerno(@RequestBody String grpSerno) {
        List<String> list = lmtGrpMemRelService.querySernoByGrpSerno(grpSerno);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryInfoByGrpSerno
     * @函数描述: 集团申请信息数据详情查询接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询审批数据")
    @PostMapping("/queryInfoByGrpSerno")
    protected ResultDto<LmtGrpAppr> queryInfoByGrpSerno(@RequestBody  String grpSerno) {
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryInfoByGrpSerno(grpSerno);
        return new ResultDto<>(lmtGrpAppr);
    }

    /**
     * @函数名称: updatelmtapprchoose
     * @函数描述: 根据流水号更新审批选择条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updatelmtapprchoose")
    protected ResultDto<Integer> updateLmtApprChoose(@RequestBody Map<String, Object> params) {
        int result = lmtGrpApprService.updateLmtApprChoose(params);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: queryLmtApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询成员客户审批信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据集团申请流水号查询成员客户审批信息")
    @PostMapping("/querylmtapprbygrpserno")
    protected ResultDto<List<LmtGrpMemRel>> queryLmtApprByGrpSerno(@RequestBody Map<String,String> params) {
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpApprService.queryLmtApprByGrpSerno(params.get("serno"));
        return new ResultDto<List<LmtGrpMemRel>>(lmtGrpMemRelList);
    }

    /**
     * @函数名称: queryLmtGrpApprSubAndPrdByGrpSerno
     * @函数描述: 根据集团授信申请流水号获取审批中的集团分项明细
     * @创建人: css
     */
    @ApiOperation("根据集团授信申请流水号获取审批中的集团分项明细")
    @PostMapping("/querylmtgrpappsubandprdbygrpapproveserno")
    protected ResultDto<List<Map>> queryLmtGrpApprSubAndPrdByGrpApproveSerno(@RequestBody Map<String,String> params) {
        List<Map> lmtGrpAppSubDto = lmtGrpApprService.queryLmtGrpApprSubAndPrdByGrpApproveSerno(params);
        return ResultDto.success(lmtGrpAppSubDto);
    }

    /**
     * @函数名称: refshGrpAppData
     * @函数描述: 重新拉去授信申请数据
     * @创建人: css
     */
    @ApiOperation("重新拉去授信申请数据")
    @PostMapping("/refshGrpAppData")
    protected ResultDto<LmtGrpAppr> refshGrpAppData(@RequestBody Map<String,String> params) throws Exception {
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.refshGrpAppData(params);
        return new ResultDto<LmtGrpAppr>(lmtGrpAppr);
    }

    /**
     * 更新核查报告文件路径
     * @return
     */
    @PostMapping("/updateFilePath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtGrpApprService.updateFilePath(condition);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称: queryTotalLmtAmtByGrpApproveSerno
     * @函数描述: 根据集团授信审批流水号获取审批中的集团敞口合计和低风险合计
     * @创建人: css
     */
    @ApiOperation("根据集团授信审批流水号获取审批中的集团敞口合计和低风险合计")
    @PostMapping("/querytotallmtamtbygrpapproveserno")
    protected ResultDto<Map> queryTotalLmtAmtByGrpApproveSerno(@RequestBody Map<String,String> params) {
        Map map = lmtGrpApprService.queryTotalLmtAmtByGrpApproveSerno(params);
        return ResultDto.success(map);
    }
}
