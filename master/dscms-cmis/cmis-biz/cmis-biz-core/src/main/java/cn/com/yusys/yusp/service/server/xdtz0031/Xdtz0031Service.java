package cn.com.yusys.yusp.service.server.xdtz0031;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.req.Xdtz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.AccLoanList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:根据合同号获取借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0031Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0031Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CommonService commonService;

    /**
     * 根据合同号获取借据信息
     * @param xdtz0031DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public List<AccLoanList> getLoanList(Xdtz0031DataReqDto xdtz0031DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031DataReqDto));
        List<AccLoanList> result = null;
        String cert_code = xdtz0031DataReqDto.getCert_code();//证件号
        String cus_id = xdtz0031DataReqDto.getCus_id();//客户号
        String cont_no = xdtz0031DataReqDto.getCont_no();//合同号
        String account_status = xdtz0031DataReqDto.getAccount_status();//借据状态
        try {

            Map queryMap = new HashMap();
            queryMap.put("certCode", cert_code);
            queryMap.put("cusId", cus_id);
            queryMap.put("contNo", cont_no);
            if (StringUtils.isNotBlank(account_status)) {
                if (account_status.endsWith(",")) {
                    account_status = account_status.substring(0, account_status.length() -1);
                }
                String[] accStatus = account_status.split(",");
                queryMap.put("accountStatus", accStatus);
            }
            if (StringUtils.isNotBlank(cert_code)) {
                List<String> cusIds = commonService.getCusBaseByCertCode(cert_code);
                if (CollectionUtils.isEmpty(cusIds)) {
                    result = new ArrayList<>();
                    return result;
                }
                queryMap.put("cusIds", cusIds);
            }
            result = accLoanMapper.getLoanList(queryMap);


            if (CollectionUtils.nonEmpty(result)) {
                for (AccLoanList accLoanList : result) {
                    Map<String, String> param = new HashMap<>();
                    param.put("cusId", accLoanList.getCus_id());
                    List<CusBaseDto> commonDto = commonService.getCommonDto(param);
                    if (CollectionUtils.nonEmpty(commonDto)) {
                        accLoanList.setCert_code(commonDto.get(0).getCertCode());
                    } else {
                        accLoanList.setCert_code(StringUtils.EMPTY);
                    }

                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(result));
        return result;
    }
}
