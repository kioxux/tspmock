package cn.com.yusys.yusp.web.server.xdkh0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0023.req.Xdkh0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0023.resp.Xdkh0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0023.Xdkh0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:还款试算计划查询日期
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDKH0023:还款试算计划查询日期")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class CusXdkh0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0023Resource.class);

    @Autowired
    private Xdkh0023Service xdkh0023Service;
    /**
     * 交易码：xdkh0023
     * 交易描述：还款试算计划查询日期
     *
     * @param xdkh0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款试算计划查询日期")
    @PostMapping("/xdkh0023")
    protected @ResponseBody
    ResultDto<Xdkh0023DataRespDto> xdkh0023(@Validated @RequestBody Xdkh0023DataReqDto xdkh0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023DataReqDto));
        Xdkh0023DataRespDto xdkh0023DataRespDto = new Xdkh0023DataRespDto();// 响应Dto:还款试算计划查询日期
        ResultDto<Xdkh0023DataRespDto> xdkh0023DataResultDto = new ResultDto<>();
        String cusId = xdkh0023DataReqDto.getCusId();//客户号
        String certNo = xdkh0023DataReqDto.getCertNo();//证件号码
        try {
            if (StringUtils.isBlank(cusId)) {
                xdkh0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdkh0023DataResultDto.setMessage("客户号【cusId】不能为空！");
                return xdkh0023DataResultDto;
            } else if (StringUtils.isBlank(certNo)) {
                xdkh0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdkh0023DataResultDto.setMessage("证件号码【certNo】不能为空！");
                return xdkh0023DataResultDto;
            }
            //从xdkh0023DataReqDto获取业务值进行业务逻辑处理
            xdkh0023DataRespDto =  xdkh0023Service.xdkh0023(xdkh0023DataReqDto);
            // 封装xdkh0023DataResultDto中正确的返回码和返回信息
            xdkh0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, e.getMessage());
            // 封装xdkh0023DataResultDto中异常返回码和返回信息
            xdkh0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0023DataRespDto到xdkh0023DataResultDto中
        xdkh0023DataResultDto.setData(xdkh0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023DataResultDto));
        return xdkh0023DataResultDto;
    }
}
