package cn.com.yusys.yusp.service.client.bsp.ypxt.coninf;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/28 14:21
 * @desc 信贷担保合同信息同步
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class ConinfService {
    private static final Logger logger = LoggerFactory.getLogger(ConinfService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * @param coninfReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.resp.coninfRespDto
     * @author 王玉坤
     * @date 2021/6/28 22:59
     * @version 1.0.0
     * @desc 信贷担保合同信息同步
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ConinfRespDto coninf(ConinfReqDto coninfReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value, JSON.toJSONString(coninfReqDto));
        ResultDto<ConinfRespDto> coninf2ResultDto = dscms2YpxtClientService.coninf(coninfReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value, JSON.toJSONString(coninf2ResultDto));

        String coninfCode = Optional.ofNullable(coninf2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String coninfMeesage = Optional.ofNullable(coninf2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ConinfRespDto coninfRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, coninf2ResultDto.getCode())) {
            //  获取相关的值并解析
            coninfRespDto = coninf2ResultDto.getData();
        } else {//未查询到相关信息
            coninf2ResultDto.setCode(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value);
        return coninfRespDto;
    }
}
