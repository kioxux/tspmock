package cn.com.yusys.yusp.service.server.xdxw0044;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdxw0044.req.Xdxw0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0044.resp.Xdxw0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.LmtBsInfoService;
import cn.com.yusys.yusp.service.LmtPlListInfoService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.web.server.xdxw0044.BizXdxw0044Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 优抵贷待办事项接口
 *
 * @author sunzhen
 * @version 1.0
 */
@Service
public class Xdxw0044Service {

    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0044Resource.class);

    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;
    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private SenddxService senddxService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CfgXdFinReportMapper cfgXdFinReportMapper;
    @Autowired
    private LmtBsInfoMapper lmtBsInfoMapper;
    @Autowired
    private LmtPlListInfoMapper lmtPlListInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private LmtBsInfoService lmtBsInfoService;
    @Autowired
    private LmtPlListInfoService lmtPlListInfoService;
    @Autowired
    MessageCommonService messageCommonService;
    @Autowired
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper;//模型审批结果表

    /**
     * 交易描述：优抵贷待办事项接口
     *
     * @param xdxw0044DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdxw0044DataRespDto xdxw0044(Xdxw0044DataReqDto xdxw0044DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value);
        Xdxw0044DataRespDto xdxw0044DataRespDto = new Xdxw0044DataRespDto();
        try {
            //获取传入参数
            String applySerno = xdxw0044DataReqDto.getApply_serno();//业务流水
            String cusName = xdxw0044DataReqDto.getCus_name();//申请人名称
            String certNo = xdxw0044DataReqDto.getCert_no();//证件号码
            String telphone = xdxw0044DataReqDto.getTelphone();//手机
            String companyName = xdxw0044DataReqDto.getCompany_name();//企业名称
            String legalRepre = xdxw0044DataReqDto.getLegal_repre();//法人代表
            String judicalAudi = xdxw0044DataReqDto.getJudical_audi();//司法审核
            BigDecimal applyAmount = xdxw0044DataReqDto.getApply_amount();//申请金额
            BigDecimal applyRate = xdxw0044DataReqDto.getApply_rate();//申请利率
            Integer applyLimit = xdxw0044DataReqDto.getApply_limit();//申请期限
            String cusType = xdxw0044DataReqDto.getCus_type();//客户类型
            String cusMgrId = xdxw0044DataReqDto.getCus_mgr_id();//营销客户经理编号
            String entCertType = xdxw0044DataReqDto.getEnt_cert_type();//企业证件类型
            String entCertId = xdxw0044DataReqDto.getEnt_cert_id();//企业证件号
            String reportType = xdxw0044DataReqDto.getReport_type();//调查表类型
            String comTaxLevel = xdxw0044DataReqDto.getCom_tax_level();//企业当前税务信用评级
            BigDecimal comProfitMargin = xdxw0044DataReqDto.getCom_profit_margin();//企业近1年税前利润率
            BigDecimal comRatal = xdxw0044DataReqDto.getCom_ratal();//企业近1年综合应纳税额
            BigDecimal debtIncomeRatio = xdxw0044DataReqDto.getDebt_income_ratio();//负债收入比
            BigDecimal comSalesIncome = xdxw0044DataReqDto.getCom_sales_income();//企业近1年销售收入
            String taxGradeResult = xdxw0044DataReqDto.getTax_grade_result();//税务模型评级结果
            String taxScore = xdxw0044DataReqDto.getTax_score();//税务模型评分
            String qyType = Optional.ofNullable(xdxw0044DataReqDto.getQy_type()).orElse(StringUtils.EMPTY);//企业类型
            String sysDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT); //当前系统时间(String)
            Date date = DateUtils.parseDate(sysDate, DateFormatEnum.DEFAULT.getValue()); //当前系统时间(Date)


            //必填项校验
            if (StringUtils.isBlank(applySerno)) {
                throw new Exception("业务流水为空！");
            }
            if (StringUtils.isBlank(cusName)) {
                throw new Exception("申请人名称为空！");
            }
            if (StringUtils.isBlank(certNo)) {
                throw new Exception("证件号码为空！");
            }
            if (StringUtils.isBlank(telphone)) {
                throw new Exception("手机为空！");
            }
            if (Objects.isNull(applyAmount)) {
                throw new Exception("申请金额为空！");
            }

            /******第一步：确认任务分配人*******/
            logger.info("*****XDXW0044:第一步：确认任务分配人****");
            // TODO 分配人先写死 待王玉坤确认分配规则后 重新修改
//            GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
//            getUserInfoByRoleCodeDto.setRoleCode("1103");
//            getUserInfoByRoleCodeDto.setPageNum(1);
//            getUserInfoByRoleCodeDto.setPageSize(10);
//            List<AdminSmUserDto> userInfoByRoleCode = commonService.getUserInfoByRoleCode(getUserInfoByRoleCodeDto);
//            AdminSmUserDto userDto = null;
//            if (userInfoByRoleCode.size() > 0) {
//                userDto = userInfoByRoleCode.get(0);
//            }
//
//            if (Objects.isNull(userDto)) {
//                throw new Exception("任务分配人获取失败！");
//            }

//            String telnum = "13963306120";//userDto.getUserMobilephone(); //电话
//            String actorno = "19010161";//userDto.getLoginCode(); //工号
//            String actorname = "王浩";//userDto.getUserName(); //名称

            /*******************查询优抵贷产品详细信息**************************/
            ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(DscmsBizXwEnum.YDD_TYPE_SC020009.key);
            CfgPrdBasicinfoDto cfgPrdBasicinfoDto = Optional.ofNullable(prdresultDto.getData()).orElse(new CfgPrdBasicinfoDto());
            /********************获取系统当期营业日期************************/
            String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期


            /******第二步：插入调查任务分配表*******/
            logger.info("*****XDXW0044:第二步：插入调查任务分配表****");
            LmtSurveyTaskDivis lmtSurveyTaskDivis = new LmtSurveyTaskDivis();
            lmtSurveyTaskDivis.setPkId(applySerno); //主键
            lmtSurveyTaskDivis.setSurveySerno(applySerno); //调查流水号
            lmtSurveyTaskDivis.setBizSerno(applySerno); //第三方业务流水号
            lmtSurveyTaskDivis.setPrdName(cfgPrdBasicinfoDto.getPrdName()); //产品名称
            lmtSurveyTaskDivis.setPrdType(cfgPrdBasicinfoDto.getPrdType()); //产品类型
            lmtSurveyTaskDivis.setPrdId(DscmsBizXwEnum.YDD_TYPE_SC020009.key); //产品编号
            lmtSurveyTaskDivis.setAppChnl(DscmsBizXwEnum.APP_CHNL_00.key); //申请渠道  微信小程序
            CusBaseDto cusInfoDto = new CusBaseDto();
            List<CusBaseDto> cusBaseDtos = commonService.getCusBaseListByCertCode(certNo);
            if (CollectionUtils.nonEmpty(cusBaseDtos)) {
                cusInfoDto = cusBaseDtos.get(0);
            }
            if (StringUtil.isEmpty(cusInfoDto.getCusId())) {
                throw BizException.error(null, EcbEnum.ECB010017.key, "证件号查询对应客户结果为空！");
            }
            lmtSurveyTaskDivis.setCusId(cusInfoDto.getCusId()); //客户编号
            lmtSurveyTaskDivis.setCusName(cusName); //客户名称
            lmtSurveyTaskDivis.setCertType(cusInfoDto.getCertType()); //证件类型
            lmtSurveyTaskDivis.setCertCode(certNo); //证件号码
            lmtSurveyTaskDivis.setAppAmt(applyAmount); //申请金额
            lmtSurveyTaskDivis.setPhone(telphone); //手机号码
            lmtSurveyTaskDivis.setWork(""); //工作
            lmtSurveyTaskDivis.setLoanPurp(""); //贷款用途
            lmtSurveyTaskDivis.setManagerName(""); //客户经理名称
            lmtSurveyTaskDivis.setManagerId(""); //客户经理编号
            lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
            //TODO WH  2021年7月20日15:22:44  发现这个参数为空 先这样
            lmtSurveyTaskDivis.setIsStopOffline(cfgPrdBasicinfoDto.getIdStopOffline()==null?"1":cfgPrdBasicinfoDto.getIdStopOffline()); //是否线下调查
            lmtSurveyTaskDivis.setIntoTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)); //进件时间
            lmtSurveyTaskDivis.setDivisStatus(DscmsBizXwEnum.DIVIS_STATUS_101.key); //调查分配状态,未分配
            lmtSurveyTaskDivis.setMarConfirmStatus(DscmsBizXwEnum.CONFIRM_STATUS_00.key); //客户经理确认状态,默认待确认
            lmtSurveyTaskDivis.setDivisTime(""); //分配时间
            lmtSurveyTaskDivis.setDivisId(StringUtils.EMPTY); //分配人
            lmtSurveyTaskDivis.setMarId(cusMgrId); //营销人
            lmtSurveyTaskDivis.setPrcId(""); //处理人
            lmtSurveyTaskDivis.setBelgLine(DscmsBizXwEnum.BELG_LINE_01.key); //所属条线,默认小微条线
            lmtSurveyTaskDivis.setCreateTime(date); //创建时间
            lmtSurveyTaskDivis.setUpdateTime(date); //修改时间

            /******第三步：插入调查任务表*******/
            logger.info("*****XDXW0044:第三步：插入调查任务表****");
            //(1)客户授信调查表
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
            lmtSurveyReportMainInfo.setSurveySerno(applySerno); //调查流水号
            lmtSurveyReportMainInfo.setBizSerno(applySerno); //第三方业务流水号
            lmtSurveyReportMainInfo.setPrdName(cfgPrdBasicinfoDto.getPrdName()); //产品名称
            lmtSurveyReportMainInfo.setPrdType(cfgPrdBasicinfoDto.getPrdType()); //产品类型
            lmtSurveyReportMainInfo.setPrdId(DscmsBizXwEnum.YDD_TYPE_SC020009.key); //产品编号
            if (Objects.equals(taxGradeResult, "R5") || qyType.contains("个体")) {
                lmtSurveyReportMainInfo.setSurveyType(DscmsBizXwEnum.SURVEY_TYPE_9.key); //小微调查报告类型,优低贷STD_SURVEY_SUB_TYPE
            } else {
                lmtSurveyReportMainInfo.setSurveyType(DscmsBizXwEnum.SURVEY_TYPE_15.key); //小微调查报告类型,优抵贷税务调查报告
            }

            lmtSurveyReportMainInfo.setCusId(cusInfoDto.getCusId()); //客户编号
            lmtSurveyReportMainInfo.setCusName(cusName); //客户名称
            lmtSurveyReportMainInfo.setCertType(cusInfoDto.getCertType()); //证件类型
            lmtSurveyReportMainInfo.setCertCode(certNo); //证件号码
            lmtSurveyReportMainInfo.setAppAmt(applyAmount); //申请金额
            lmtSurveyReportMainInfo.setApproveStatus(CommonConstance.APPROVE_STATUS_000); //审批状态
            lmtSurveyReportMainInfo.setDataSource(DscmsBizXwEnum.DATA_SOURCE_01.key); //数据来源,人工新增 [{"key":"00","value":"人工新增"},{"key":"01","value":"系统推送"}]
            lmtSurveyReportMainInfo.setIntoTime(sysDate); //进件时间
            lmtSurveyReportMainInfo.setIsAutodivis("0"); //是否自动分配
            lmtSurveyReportMainInfo.setIsStopOffline("1"); //是否线下调查
            lmtSurveyReportMainInfo.setOprType("01"); //操作类型
            lmtSurveyReportMainInfo.setMarId(cusMgrId); //营销人
            lmtSurveyReportMainInfo.setManagerBrId(""); //主管机构
            lmtSurveyReportMainInfo.setManagerId(""); //主管客户经理
            lmtSurveyReportMainInfo.setInputId(""); //登记人
            lmtSurveyReportMainInfo.setInputBrId(""); //登记机构
            lmtSurveyReportMainInfo.setInputDate(openday); //登记日期
            lmtSurveyReportMainInfo.setUpdId(""); //最后修改人
            lmtSurveyReportMainInfo.setUpdBrId(""); //最后修改机构
            lmtSurveyReportMainInfo.setUpdDate(""); //最后修改日期
            lmtSurveyReportMainInfo.setCreateTime(date); //创建时间
            lmtSurveyReportMainInfo.setUpdateTime(date); //修改时间

            //(2)调查报告基本信息
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
            lmtSurveyReportBasicInfo.setSurveySerno(applySerno); //调查流水号
            lmtSurveyReportBasicInfo.setCusName(cusName); //客户编号
            lmtSurveyReportBasicInfo.setCusId(cusInfoDto.getCusId()); //客户名称
            lmtSurveyReportBasicInfo.setCertCode(certNo); //证件号码
            lmtSurveyReportBasicInfo.setPhone(telphone); //电话号码
            lmtSurveyReportBasicInfo.setMarStatus(""); //婚姻状况
            lmtSurveyReportBasicInfo.setSpouseCusId(""); //配偶客户编号
            lmtSurveyReportBasicInfo.setSpouseName(""); //配偶姓名
            lmtSurveyReportBasicInfo.setSpouseCertCode(""); //配偶证件号码
            lmtSurveyReportBasicInfo.setSpousePhone(""); //配偶电话
            lmtSurveyReportBasicInfo.setIsHaveChildren(""); //有无子女
            lmtSurveyReportBasicInfo.setEdu(""); //学历
            lmtSurveyReportBasicInfo.setWorkUnit(""); //工作单位
            lmtSurveyReportBasicInfo.setResiYears(""); //居住年限
            lmtSurveyReportBasicInfo.setLivingAddr(""); //居住地址
            lmtSurveyReportBasicInfo.setIsOnlinePld(""); //是否线上抵押
            lmtSurveyReportBasicInfo.setGuarMode(""); //担保方式
            lmtSurveyReportBasicInfo.setAppAmt(applyAmount); //申请金额
            lmtSurveyReportBasicInfo.setModelAdviceAmt(new BigDecimal("0")); //模型建议金额
            lmtSurveyReportBasicInfo.setModelAdviceRate(new BigDecimal("0")); //模型建议利率
            lmtSurveyReportBasicInfo.setRefRate(new BigDecimal("0")); //参考利率
            lmtSurveyReportBasicInfo.setAdviceAmt(applyAmount); //建议金额
            lmtSurveyReportBasicInfo.setAdviceRate(new BigDecimal("0")); //建议利率
            lmtSurveyReportBasicInfo.setAdviceTerm(""); //建议期限
            lmtSurveyReportBasicInfo.setIsRenewLoan("0"); //是否续贷
            lmtSurveyReportBasicInfo.setIsSceneInquest(""); //是否现场勘验
            lmtSurveyReportBasicInfo.setModelFstResult(""); //模型初步结果
            lmtSurveyReportBasicInfo.setLoanPurp(""); //贷款用途
            lmtSurveyReportBasicInfo.setIsAgri(""); //是否农户
            lmtSurveyReportBasicInfo.setIsNewEmployee(""); //是否新员工
            lmtSurveyReportBasicInfo.setNewEmployeeName(""); //新员工名称
            lmtSurveyReportBasicInfo.setNewEmployeePhone(""); //新员工电话
            lmtSurveyReportBasicInfo.setOldBillNo(""); //原借据号
            lmtSurveyReportBasicInfo.setOldBillAmt(new BigDecimal("0")); //原借据金额
            lmtSurveyReportBasicInfo.setOldBillBalance(new BigDecimal("0")); //原借据余额
            lmtSurveyReportBasicInfo.setOldBillLoanRate(new BigDecimal("0")); //原借据利率
            lmtSurveyReportBasicInfo.setDebtFlag(""); //借款人负债较上期增加是否超50%
            lmtSurveyReportBasicInfo.setGuarFlag(""); //借款人对外是否提供过多担保或大量资产被抵押
            lmtSurveyReportBasicInfo.setAccidentFlag(""); //借款人及其家庭是否发生意外
            lmtSurveyReportBasicInfo.setGuaranteeFlag(""); //抵/质押物是否异常
            lmtSurveyReportBasicInfo.setActivityFlag(""); //借款人经营活动是否正常
            lmtSurveyReportBasicInfo.setManagementBelongFlag(""); //借款人经营所有权是否发生重大变化
            lmtSurveyReportBasicInfo.setSurveyMode(""); //调查模式
            lmtSurveyReportBasicInfo.setSingleSurveySerno(""); //单人调查流水号
            lmtSurveyReportBasicInfo.setOprType("01"); //操作类型
            lmtSurveyReportBasicInfo.setMarId(cusMgrId); //营销人
            lmtSurveyReportBasicInfo.setManagerBrId(""); //主管机构
            lmtSurveyReportBasicInfo.setManagerId(""); //主管客户经理
            lmtSurveyReportBasicInfo.setInputId(""); //登记人
            lmtSurveyReportBasicInfo.setInputBrId(""); //登记机构
            lmtSurveyReportBasicInfo.setInputDate(openday); //登记日期
            lmtSurveyReportBasicInfo.setUpdId(""); //最后修改人
            lmtSurveyReportBasicInfo.setUpdBrId(""); //最后修改机构
            lmtSurveyReportBasicInfo.setUpdDate(""); //最后修改日期
            lmtSurveyReportBasicInfo.setCreateTime(date); //创建时间
            lmtSurveyReportBasicInfo.setUpdateTime(date); //修改时间

            //(3)调查报告企业信息
            LmtSurveyReportComInfo lmtSurveyReportComInfo = new LmtSurveyReportComInfo();
            lmtSurveyReportComInfo.setSurveySerno(applySerno); //调查流水号
            lmtSurveyReportComInfo.setConName(companyName); //企业名称
            lmtSurveyReportComInfo.setLegal(legalRepre); //法人代表
            lmtSurveyReportComInfo.setOperAddr(""); //经营地址
            lmtSurveyReportComInfo.setOperTerm(""); //经营期限
            lmtSurveyReportComInfo.setMainBusi(""); //主营业务
            lmtSurveyReportComInfo.setCorpType(qyType); //企业类型
            lmtSurveyReportComInfo.setBlicYears(""); //营业执照年限
            lmtSurveyReportComInfo.setTrade(""); //行业
            lmtSurveyReportComInfo.setCorpCertType(entCertType); //企业证件类型
            lmtSurveyReportComInfo.setCorpCertCode(entCertId); //企业证件号码
            lmtSurveyReportComInfo.setActMngAddr(""); //实际经营地址
            lmtSurveyReportComInfo.setActOperTrade(""); //实际行业
            lmtSurveyReportComInfo.setIsActOperPerson(""); //是否实际经营人
            lmtSurveyReportComInfo.setIsOperNormal(""); //经营是否正常
            lmtSurveyReportComInfo.setCorpDesc(""); //描述
            lmtSurveyReportComInfo.setJudicialAuditResult(""); //司法审核结果
            lmtSurveyReportComInfo.setCorpFullTaxMonths(""); //企业完整纳税月份数
            lmtSurveyReportComInfo.setCorpCurtTxtCdtEval(comTaxLevel); //企业当前税务信用评级
            lmtSurveyReportComInfo.setCorpLt1yearInteTax(comRatal); //企业近1年综合应纳税额
            lmtSurveyReportComInfo.setCorpLt1yearPretaxProfit(comProfitMargin); //企业近1年税前利润率
            lmtSurveyReportComInfo.setCorpLt1yearSaleIncome(comSalesIncome); //企业近1年销售收入
            lmtSurveyReportComInfo.setTaxModelEvalResult(taxGradeResult); //税务模型评级结果
            lmtSurveyReportComInfo.setTaxModelGrade(taxScore); //税务模型评分
            lmtSurveyReportComInfo.setOprType(""); //操作类型
            lmtSurveyReportComInfo.setDebtEarningPerc(debtIncomeRatio); //负债收入比
            lmtSurveyReportComInfo.setManagerBrId(""); //主管机构
            lmtSurveyReportComInfo.setManagerId(""); //主管客户经理
            lmtSurveyReportComInfo.setInputId(""); //登记人
            lmtSurveyReportComInfo.setInputBrId(""); //登记机构
            lmtSurveyReportComInfo.setInputDate(openday); //登记日期
            lmtSurveyReportComInfo.setUpdId(""); //最后修改人
            lmtSurveyReportComInfo.setUpdBrId(""); //最后修改机构
            lmtSurveyReportComInfo.setUpdDate(""); //最后修改日期
            lmtSurveyReportComInfo.setCreateTime(date); //创建时间
            lmtSurveyReportComInfo.setUpdateTime(date); //修改时间

            /****** 如果是尽职调查 初始化损益明细表和资产负债表******/
            logger.info("*****XDXW0044:初始化损益明细表和资产负债表************");

            if (lmtSurveyReportMainInfo.getSurveyType().equals(DscmsBizXwEnum.SURVEY_TYPE_9.key)) {
                //资产负债表和损益明细
                List<CfgXdFinReport> cfgXdFinReportList = cfgXdFinReportMapper.selectByPrd(DscmsBizXwEnum.YDD_TYPE_SC020009.key);
                logger.info("*****XDXW0044:查询到损益明细和资产负债信息************"+cfgXdFinReportList);
                cfgXdFinReportList.forEach(s->{
                     if (s.getTypeChild().equals("1")) {
                         String pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                         //插入资产负债表
                         LmtBsInfo lmtBsInfo=  new LmtBsInfo();
                         lmtBsInfo.setPkId(pkId);
                         lmtBsInfo.setSurveySerno(applySerno);
                         lmtBsInfo.setSubject(s.getCodeText());
                         lmtBsInfo.setSubjectValue(s.getCodeValue());
                         lmtBsInfoService.insert(lmtBsInfo);
                         logger.info("*****XDXW0044:初始资产负债表************"+lmtBsInfo);
                     }else if (s.getTypeChild().equals("2")){
                         String pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());

                         LmtPlListInfo lmtPlListInfo = new LmtPlListInfo();
                         lmtPlListInfo.setPkId(pkId);
                         lmtPlListInfo.setSurveySerno(applySerno);
                         lmtPlListInfo.setSubject(s.getCodeText());
                         lmtPlListInfo.setSubjectValue(s.getCodeValue());
                         lmtPlListInfoService.insert(lmtPlListInfo);
                         logger.info("*****XDXW0044:初始化损益明细表************"+lmtPlListInfo);
                     }else {
                         logger.info("*****XDXW0044:初始化损益明细表和资产负债表存在脏数据************");
                     }

                });
            }

            // 生成LMT_MODEL_APPR_RESULT_INFO模型结果表
            LmtModelApprResultInfo lmtModelApprResultInfo = new LmtModelApprResultInfo();
            lmtModelApprResultInfo.setSurveySerno(applySerno);//调查流水号
            lmtModelApprResultInfo.setModelApprTime(openday);//模型审批时间
            lmtModelApprResultInfo.setModelGrade("");//模型评级
            lmtModelApprResultInfo.setModelScore("");//模型得分
            lmtModelApprResultInfo.setModelAmt(applyAmount);//模型金额
            lmtModelApprResultInfo.setModelRate(new BigDecimal("0"));//模型利率
            lmtModelApprResultInfo.setJojc("");//司法案件判断
            lmtModelApprResultInfo.setModelRstStatus("003");//模型结果状态
            lmtModelApprResultInfo.setBizUniqueNo(applySerno);//业务唯一编号
            lmtModelApprResultInfo.setModelAdvice("");//模型意见
            lmtModelApprResultInfo.setModelFstLmt(applyAmount);//模型初始额度
            lmtModelApprResultInfo.setCreateTime(date);//创建时间
            lmtModelApprResultInfo.setUpdateTime(date);//修改时间
            //LMT_MODEL_APPR_RESULT_INFO模型结果表插入数据库
            logger.info("**********XDXW0044**插入模型结果表lmtModelApprResultInfo开始,插入参数为:{}", JSON.toJSONString(lmtModelApprResultInfo));
            lmtModelApprResultInfoMapper.insert(lmtModelApprResultInfo);

            logger.info("*****XDXW0044:初始化损益明细表和资产负债表结束************");
            /******第四步：发送短信*******/
            logger.info("********************XDXW0044**发送的短信***********************");
//            String messageType = "MSG_XW_M_0011";//  短信编号
//            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
//            Map paramMap = new HashMap();//短信填充参数
//            paramMap.put("cusName", cusName);
//            paramMap.put("prdName", "优抵贷");
//            String managerId = "";//客户经理
//            String linkWay = "";//联系方式
//            try {
//                //执行发送借款人操作
//                ResultDto<Integer> resultDto1 = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, "");
//            } catch (Exception e) {
//                throw new Exception("发送短信失败！");
//            }

            /******第五步：保存数据*******/
            logger.info("*****XDXW0044:第五步：保存数据************");
            logger.info("**********XDXW0044**插入lmtSurveyReportComInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportComInfo));
            int insert = lmtSurveyReportComInfoMapper.insert(lmtSurveyReportComInfo);
            logger.info("**********XDXW0044**插入lmtSurveyReportComInfo结束,返回结果为:{}", JSON.toJSONString(insert));

            logger.info("**********XDXW0044**插入lmtSurveyTaskDivis开始,插入参数为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
            int insert1 = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
            logger.info("**********XDXW0044**插入lmtSurveyTaskDivis结束,返回结果为:{}", JSON.toJSONString(insert1));

            logger.info("**********XDXW0044**插入lmtSurveyReportBasicInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
            int insert2 = lmtSurveyReportBasicInfoMapper.insert(lmtSurveyReportBasicInfo);
            logger.info("**********XDXW0044**插入lmtSurveyReportBasicInfo结束,返回结果为:{}", JSON.toJSONString(insert2));

            logger.info("**********XDXW0044**插入lmtSurveyReportMainInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
            int insert3 = lmtSurveyReportMainInfoMapper.insert(lmtSurveyReportMainInfo);
            logger.info("**********XDXW0044**插入lmtSurveyReportMainInfo结束,返回结果为:{}", JSON.toJSONString(insert3));

            if (insert < 1 || insert1 < 1 || insert2 < 1 || insert3 < 1) {
                throw new Exception("数据插入失败！");
            }

            xdxw0044DataRespDto.setOpFlag(CmisBizConstants.YES);
            xdxw0044DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, e.getMessage());
            xdxw0044DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdxw0044DataRespDto.setOpMsg("系统异常:" + e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value);
        return xdxw0044DataRespDto;
    }
}
