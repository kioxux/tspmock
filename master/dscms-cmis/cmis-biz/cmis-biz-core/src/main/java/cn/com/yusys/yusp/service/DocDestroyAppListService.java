package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.DocAccList;
import cn.com.yusys.yusp.domain.DocArchiveInfo;
import cn.com.yusys.yusp.domain.DocDestroyAppList;
import cn.com.yusys.yusp.domain.DocDestroyDetailList;
import cn.com.yusys.yusp.dto.req.DocDestroyDetailListDTO;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.DocDestroyAppListMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyAppListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 15:08:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocDestroyAppListService {

    private static final Logger log = LoggerFactory.getLogger(DocDestroyAppListService.class);

    @Autowired
    private DocDestroyAppListMapper docDestroyAppListMapper;

    @Autowired
    private DocDestroyDetailListService docDestroyDetailListService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private DocAccListService docAccListService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocDestroyAppList selectByPrimaryKey(String ddalSerno) {
        return docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocDestroyAppList> selectAll(QueryModel model) {
        List<DocDestroyAppList> records = (List<DocDestroyAppList>) docDestroyAppListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocDestroyAppList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocDestroyAppList> list = docDestroyAppListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 档案销毁_新增档案销毁内容(同时增加关联的档案销毁明细内容)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(DocDestroyAppList record) {
        String ddalSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_XH_SEQ, new HashMap());
        if (Objects.nonNull(record)) {
            // 档案销毁流水号
            record.setDdalSerno(ddalSerno);
            // 保存的时候，仅仅设置状态为：待发起
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            // 保存的时候，销毁状态设置为：待销毁(01:待销毁，02：已销毁)
            record.setDestroyStatus("01");
            // 设置销毁户数为0
            record.setDestroyCus("0");
            return docDestroyAppListMapper.insertSelective(record);
        } else {
            return 0;
        }
    }

    /**
     * @方法名称: introduceDocDestroyDetails
     * @方法描述: 档案销毁_新增档案销毁内容(同时增加关联的档案销毁明细内容)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int introduceDocDestroyDetails(DocDestroyDetailListDTO docDestroyDetailListDTO) {
        DocDestroyAppList docDestroyAppList = null;
        int insertCount = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar now = Calendar.getInstance();
        Calendar other = Calendar.getInstance();
        // 引入时是否需要新增标识
        boolean insertFlag = false;
        for (DocDestroyDetailList docDestroyDetailList : docDestroyDetailListDTO.getDocDestroyDetailLists()) {
            List<DocDestroyDetailList> list = docDestroyDetailListService.selectBydocNo(docDestroyDetailList.getDocNo());
            List<DocAccList> docList = docAccListService.selectByDocNo(docDestroyDetailList.getDocNo());
            // 根据档案号，去档案销毁明细中查询是否已经存在（如果不存在则直接插入）
            if (Objects.nonNull(list) && list.size() > 0) {
                // 如果已经存在，排除档案销毁审批状态是：拒绝或者取消的。
                docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(list.get(0).getDdalSerno());
                if (Objects.equals(CmisCommonConstants.WF_STATUS_990, docDestroyAppList.getApproveStatus()) || Objects.equals(CmisCommonConstants.WF_STATUS_998, docDestroyAppList.getApproveStatus())) {
                    insertFlag = true;
                }
            } else {
                insertFlag = true;
            }
            if (insertFlag) {
                docDestroyDetailList.setDddlSerno(StringUtils.getUUID());
                // 销毁流水号
                docDestroyDetailList.setDdalSerno(docDestroyDetailListDTO.getDdalSerno());
                // 销毁状态：待销毁
                docDestroyDetailList.setDestroyStatus("01");
                // 设置保管年限以及资料件数
                if (Objects.nonNull(docList) && docList.size() > 0) {
                    if (!StringUtils.isEmpty(docList.get(0).getStorageOptDate())) {
                        try {
                            other.setTime(sdf.parse(docList.get(0).getStorageOptDate()));
                            docDestroyDetailList.setInKeepYears(String.valueOf(now.get(Calendar.YEAR) - other.get(Calendar.YEAR) + 1));
                        } catch (ParseException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                    // 资料件数
                    DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docList.get(0).getDocSerno());
                    if (Objects.nonNull(docArchiveInfo)) {
                        docDestroyDetailList.setDocNum(docArchiveInfo.getTotalPage());
                    }
                }
                docDestroyDetailListService.insertSelective(docDestroyDetailList);
                insertCount++;
            }
        }
        if (insertCount > 0) {
            docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(docDestroyDetailListDTO.getDdalSerno());
            Integer destroyCus = !StringUtils.isEmpty(docDestroyAppList.getDestroyCus()) ? Integer.parseInt(docDestroyAppList.getDestroyCus()) + insertCount : insertCount;
            docDestroyAppList.setDestroyCus(String.valueOf(destroyCus));
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                docDestroyAppList.setUpdId(userInfo.getLoginCode());// 更新人
                docDestroyAppList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                docDestroyAppList.setUpdDate(openDay);// 更新日期
            }
            docDestroyAppListMapper.updateByPrimaryKeySelective(docDestroyAppList);
        }
        return insertCount;
    }

    /**
     * @方法名称: commitDocDestroyApp
     * @方法描述: 提交发起流程
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Boolean commitDocDestroyApp(String ddalSerno) {
        return true;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocDestroyAppList record) {
        return docDestroyAppListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocDestroyAppList record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docDestroyAppListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocDestroyAppList record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docDestroyAppListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String ddalSerno) {
        int result = 0;
        DocDestroyAppList docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);
        // 只有待发起的，才能进行删除
        if (Objects.nonNull(docDestroyAppList)) {
            if (CmisCommonConstants.WF_STATUS_992.equals(docDestroyAppList.getApproveStatus())) {
                docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                docDestroyAppListMapper.updateByPrimaryKeySelective(docDestroyAppList);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(ddalSerno);
                result = 1;
            } else if (CmisCommonConstants.WF_STATUS_000.equals(docDestroyAppList.getApproveStatus())) {
                // 档案销毁删除成功后，再去删除档案销毁明细
                if (docDestroyAppListMapper.deleteByPrimaryKey(ddalSerno) > 0) {
                    docDestroyDetailListService.deleteByDdalSerno(ddalSerno);
                    result = 1;
                }
            }
        }
        return result;
    }

    /**
     * 档案销毁_删除档案销毁明细内容（仅待发起状态的数据才可以删除）
     *
     * @param ddalSerno
     * @param dddlSerno
     * @return
     */
    public int deleteDocDetail(String ddalSerno, String dddlSerno) {
        DocDestroyAppList docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);
        // 只有待发起流程的件，才能进行删除
        if (Objects.nonNull(docDestroyAppList) && CmisCommonConstants.WF_STATUS_000.equals(docDestroyAppList.getApproveStatus())) {
            // 删除成功的话，销毁户数减去一
            if (docDestroyDetailListService.deleteByPrimaryKey(dddlSerno) > 0) {
                docDestroyAppList.setDestroyCus(String.valueOf(Integer.parseInt(docDestroyAppList.getDestroyCus()) - 1));
                return docDestroyAppListMapper.updateByPrimaryKey(docDestroyAppList);
            } else {
                throw BizException.error(null, "9999", "该件未被删除，请确认！！");
            }
        } else {
            throw BizException.error(null, "9999", "只有待发起流程的件，才允许删除对应的档案销毁明细！！");
        }
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docDestroyAppListMapper.deleteByIds(ids);
    }

    /**
     * 销毁状态：更新为已销毁
     *
     * @return
     */
    public int changeDestroyed(String ddalSerno) {
        // 只有审批通过的申请销毁件，销毁状态才能变更为：已销毁
        if (docDestroyAppListMapper.updateAppDestroyStatus(ddalSerno, CmisCommonConstants.WF_STATUS_997, "02") > 0) {
            List<String> docNoList = docDestroyDetailListService.selectDocNoListByDdalSerno(ddalSerno);
            if (docDestroyDetailListService.updateAppDestroyDetailStatus(docNoList, "02") > 0) {
                docAccListService.updatedocAccStatus(docNoList, "12");
            }
            return 1;
        } else {
            throw BizException.error(null, "9999", "只有审批通过的件，销毁状态才能由待销毁变更为已销毁！！");
        }
    }

    /**
     * 更新状态
     *
     * @param ddalSerno
     * @param destroyStatus
     * @return
     */
    public int updateAppStatus(String ddalSerno, String destroyStatus) {
        return docDestroyAppListMapper.updateAppStatus(ddalSerno, destroyStatus);
    }

    /**
     * 流程发起进行的业务处理
     * 0、更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param ddalSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String ddalSerno) {
        try {
            if (StringUtils.isBlank(ddalSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取档案销毁申请" + ddalSerno + "申请主表信息");
            DocDestroyAppList docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);
            if (Objects.isNull(docDestroyAppList)) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            log.info("流程发起-更新档案销毁申请" + ddalSerno + "流程审批状态为【111】-审批中");
            if (docDestroyAppListMapper.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_111) < 1) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }

        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("档案销毁申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批通过后进行的业务操作
     * 1、更新申请主表的审批状态以及审批通过时间
     *
     * @param ddalSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String ddalSerno) {
        try {
            if (StringUtils.isBlank(ddalSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("审批通过-获取档案销毁申请" + ddalSerno + "申请主表信息");
            DocDestroyAppList docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);
            if (Objects.isNull(docDestroyAppList)) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            docDestroyAppListMapper.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_997);
            log.info("流程发起-更新档案销毁申请" + ddalSerno + "流程审批状态为【997】-审批结束");
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批拒绝时的业务操作，后续的业务处理
     * 2、更新申请主表的审批状态为998 拒绝
     *
     * @param ddalSerno
     */
    public void handleBusinessAfterRefuse(String ddalSerno) {
        try {
            if (StringUtils.isBlank(ddalSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("档案销毁拒绝流程-获取档案销毁申请" + ddalSerno + "申请信息");
            DocDestroyAppList docDestroyAppList = docDestroyAppListMapper.selectByPrimaryKey(ddalSerno);

            if (Objects.isNull(docDestroyAppList)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            docDestroyAppListMapper.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_998);
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("档案销毁" + ddalSerno + "流程拒绝业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

}
