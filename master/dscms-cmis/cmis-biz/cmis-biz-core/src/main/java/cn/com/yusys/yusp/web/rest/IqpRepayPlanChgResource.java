/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayPlanChg;
import cn.com.yusys.yusp.service.IqpRepayPlanChgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayPlanChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 21:32:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepayplanchg")
public class IqpRepayPlanChgResource {
    @Autowired
    private IqpRepayPlanChgService iqpRepayPlanChgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRepayPlanChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRepayPlanChg> list = iqpRepayPlanChgService.selectAll(queryModel);
        return new ResultDto<List<IqpRepayPlanChg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRepayPlanChg>> index(QueryModel queryModel) {
        List<IqpRepayPlanChg> list = iqpRepayPlanChgService.selectByModel(queryModel);
        return new ResultDto<List<IqpRepayPlanChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpRepayPlanChg> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpRepayPlanChg iqpRepayPlanChg = iqpRepayPlanChgService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpRepayPlanChg>(iqpRepayPlanChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpRepayPlanChg> create(@RequestBody IqpRepayPlanChg iqpRepayPlanChg) throws URISyntaxException {
        iqpRepayPlanChgService.insert(iqpRepayPlanChg);
        return new ResultDto<IqpRepayPlanChg>(iqpRepayPlanChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRepayPlanChg iqpRepayPlanChg) throws URISyntaxException {
        int result = iqpRepayPlanChgService.update(iqpRepayPlanChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpRepayPlanChgService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRepayPlanChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
