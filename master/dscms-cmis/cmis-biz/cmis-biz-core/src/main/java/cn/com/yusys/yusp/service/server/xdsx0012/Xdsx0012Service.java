package cn.com.yusys.yusp.service.server.xdsx0012;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0012Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0012Service.class);


}
