/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustRel
 * @类描述: lmt_grp_sub_prd_adjust_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:25:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_sub_prd_adjust_rel")
public class LmtGrpSubPrdAdjustRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 分项流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 敞口额度 **/
	@Column(name = "OPEN_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openLmtAmt;

	/** 调剂后敞口额度 **/
	@Column(name = "ADJUST_OPEN_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adjustOpenLmtAmt;

	/** 低风险额度 **/
	@Column(name = "LOW_RISK_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskLmtAmt;

	/** 调剂后低风险额度 **/
	@Column(name = "ADJUST_LOW_RISK_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adjustLowRiskLmtAmt;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	/**
	 * @return grpSerno
	 */
	public String getGrpSerno() {
		return this.grpSerno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param openLmtAmt
	 */
	public void setOpenLmtAmt(java.math.BigDecimal openLmtAmt) {
		this.openLmtAmt = openLmtAmt;
	}

	/**
	 * @return openLmtAmt
	 */
	public java.math.BigDecimal getOpenLmtAmt() {
		return this.openLmtAmt;
	}

	/**
	 * @param adjustOpenLmtAmt
	 */
	public void setAdjustOpenLmtAmt(java.math.BigDecimal adjustOpenLmtAmt) {
		this.adjustOpenLmtAmt = adjustOpenLmtAmt;
	}

	/**
	 * @return adjustOpenLmtAmt
	 */
	public java.math.BigDecimal getAdjustOpenLmtAmt() {
		return this.adjustOpenLmtAmt;
	}

	/**
	 * @param lowRiskLmtAmt
	 */
	public void setLowRiskLmtAmt(java.math.BigDecimal lowRiskLmtAmt) {
		this.lowRiskLmtAmt = lowRiskLmtAmt;
	}

	/**
	 * @return lowRiskLmtAmt
	 */
	public java.math.BigDecimal getLowRiskLmtAmt() {
		return this.lowRiskLmtAmt;
	}

	/**
	 * @param adjustLowRiskLmtAmt
	 */
	public void setAdjustLowRiskLmtAmt(java.math.BigDecimal adjustLowRiskLmtAmt) {
		this.adjustLowRiskLmtAmt = adjustLowRiskLmtAmt;
	}

	/**
	 * @return adjustLowRiskLmtAmt
	 */
	public java.math.BigDecimal getAdjustLowRiskLmtAmt() {
		return this.adjustLowRiskLmtAmt;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

}