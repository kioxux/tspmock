package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtRenewLoanAppInfo;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.ComLoanInfoDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtRenewLoanAppInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: HxdListQueryServices
 * @类描述: #服务类 优企贷无还本名单查询修改新增
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ComLoanInfoServices {
    private static final Logger logger = LoggerFactory.getLogger(ComLoanInfoServices.class);

    @Autowired
    private LmtRenewLoanAppInfoMapper lmtRenewLoanAppInfoMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号

    /**
     * @方法名称: hxlist
     * @方法描述: 优企贷无还本名单查询修改新增
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ComLoanInfoDto> yqdbmd(ComLoanInfoDto comLoanInfoDto) {
        //查询条件
        String type = comLoanInfoDto.getType();//操作类型
        String serno = comLoanInfoDto.getSerno();//流水号
        String cusName = comLoanInfoDto.getCusName();//客户名称
        String certCode = comLoanInfoDto.getCertCode();//证件号
        String cusId = comLoanInfoDto.getCusId();//证件号
        String oldContNo = comLoanInfoDto.getOldContNo();//原合同编号
        String contType = comLoanInfoDto.getContType();//合同类型
        BigDecimal contAmt = comLoanInfoDto.getContAmt();//合同金额
        BigDecimal loanBalance = comLoanInfoDto.getLoanBalance();//
        String telPhone = comLoanInfoDto.getTelPhone();//
        String conName = comLoanInfoDto.getConName();//
        String unifyCreditCode = comLoanInfoDto.getUnifyCreditCode();//
        String managerId = comLoanInfoDto.getManagerId();//客户经理
        String managerBrId = comLoanInfoDto.getManagerBrId();
        String pageNum = comLoanInfoDto.getStartPageNum();//页码
        String pageSize = comLoanInfoDto.getPageSize();//每页条数

        //分页查询
        int startPageNum = 1;//页码
        int pageSizeNum = 10;//每页条数
        //定义返回列表
        java.util.List<ComLoanInfoDto> resList = new ArrayList<>();
        if ((StringUtil.isEmpty(pageSize) && StringUtil.isEmpty(pageNum)) && "0".equals(type)) {
            return resList;
        } else if ("0".equals(type)) {
            startPageNum = Integer.parseInt(pageNum);
            pageSizeNum = Integer.parseInt(pageSize);
        }

        if (StringUtil.isEmpty(type)) {
            throw BizException.error(null, EpbEnum.EPB099999.key, "申请类型为空");
        } else if ("0".equals(type)) {//查询操作
            if (StringUtil.isEmpty(managerId)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "查询时客户经理不为空");
            }
        } else if ("1".equals(type)) {
            if (StringUtil.isEmpty(cusId)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "客户号为空");
            } else if (StringUtil.isEmpty(cusName)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "客户姓名为空");
            } else if (StringUtil.isEmpty(certCode)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "证件号为空");
            } else if (StringUtil.isEmpty(oldContNo)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "原合同号为空");
            } else if (StringUtil.isEmpty(contType)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "合同类型为空");
            } else if (contAmt == null) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "合同金额为空");
            } else if (loanBalance == null) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "借据余额为空");
            } else if (StringUtil.isEmpty(telPhone)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "手机号为空");
            } else if (StringUtil.isEmpty(managerId)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "客户经理为空");
            } else if (StringUtil.isEmpty(managerBrId)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "机构号为空");
            }
        } else if ("2".equals(type)) {
            if (StringUtil.isEmpty(serno)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "流水号为空");
            }
        }

        //查询操作
        if ("0".equals(type)) {
            //查询条件
            Map map = new HashMap<>();
            map.put("managerId", managerId);
            map.put("cusName", cusName);
            map.put("certCode", certCode);
            logger.info("************优企贷无还本名单查询,查询参数为:{}", JSON.toJSONString(map));
            PageHelper.startPage(startPageNum, pageSizeNum);
            resList = lmtRenewLoanAppInfoMapper.selectComLoanInfoByModel(map);
            PageHelper.clearPage();
            logger.info("**********优企贷无还本名单查询结束,返回结果为:{}", JSON.toJSONString(resList));
            resList = resList.parallelStream().map(ret -> {
                ComLoanInfoDto temp = new ComLoanInfoDto();
                BeanUtils.copyProperties(ret, temp);
                //获取客户经理名称
                String managerName = findManagerName(ret.getManagerBrId());
                temp.setManagerName(managerName);
                //获取机构名称
                String managerBrName = findManagerBrName(ret.getManagerBrId());
                temp.setManagerBrName(managerBrName);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
        } else if ("1".equals(type)) {//新增操作
            //查询条件
            Map map = new HashMap<>();
            map.put("certCode", certCode);
            logger.info("**********根据证件号查询是否存在名单记录,查询参数为:{}", JSON.toJSONString(resList));
            resList = lmtRenewLoanAppInfoMapper.selectComLoanInfoByModel(map);
            logger.info("**********根据证件号查询是否存在名单记录结束,返回结果为:{}", JSON.toJSONString(resList));
            if (resList.size() > 0) {
                ComLoanInfoDto comLoanInfoDtolist = resList.get(0);
                String state = comLoanInfoDtolist.getStatus();
                if ("000".equals(state)) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "该客户存在待发起名单，不要重复新增！");
                } else if ("001".equals(state) || "997".equals(state)) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "该客户存在生效的提前周转名单，不要重复新增！");
                }
            }
            //营业日
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            //插入记录
            LmtRenewLoanAppInfo record = new LmtRenewLoanAppInfo();
            record.setSerno(serno);
            record.setCusName(cusName);
            record.setCusId(cusId);
            record.setCertCode(certCode);
            record.setConName(conName);
            record.setContAmt(contAmt);
            record.setBillBalance(loanBalance);
            record.setContType(contType);
            record.setOldContNo(oldContNo);
            record.setUnifyCreditCode(unifyCreditCode);
            record.setAppPhone(telPhone);
            record.setAppDate(openDay);
            record.setStatus("000");
            record.setManagerId(managerId);
            record.setManagerBrId(managerBrId);
            logger.info("**********新增名单记录,查询参数为:{}", JSON.toJSONString(record));
            int flag = lmtRenewLoanAppInfoMapper.insert(record);
            logger.info("**********新增名单记录结束,返回结果为:{}", JSON.toJSONString(flag));
        } else if ("2".equals(type)) {
            logger.info("**********删除名单记录,参数为:{}", JSON.toJSONString(serno));
            int flag = lmtRenewLoanAppInfoMapper.deleteByPrimaryKey(serno);
            logger.info("**********删除名单记录结束,返回结果为:{}", JSON.toJSONString(flag));
        }
        //返回
        return resList;
    }

    /**
     * @方法名称: yqdcht
     * @方法描述: 合同查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ComLoanInfoDto> yqdcht(ComLoanInfoDto comLoanInfoDto) {
        // 查询条件
        String cusName = comLoanInfoDto.getCusName();//客户名称
        String certCode = comLoanInfoDto.getCertCode();//证件号
        String managerId = comLoanInfoDto.getManagerId();//客户经理
        String pageNum = comLoanInfoDto.getStartPageNum();//页码
        String pageSize = comLoanInfoDto.getPageSize();//每页条数

        // 分页查询
        int startPageNum = 1;//页码
        int pageSizeNum = 10;//每页条数
        // 定义返回列表
        java.util.List<ComLoanInfoDto> resList = new ArrayList<>();
        if (StringUtil.isEmpty(pageSize) && StringUtil.isEmpty(pageNum)) {
            return resList;
        } else {
            startPageNum = Integer.parseInt(pageNum);
            pageSizeNum = Integer.parseInt(pageSize);
        }
        // 查询时客户经理不为空
        if (StringUtil.isEmpty(managerId)) {
            throw BizException.error(null, EpbEnum.EPB099999.key, "查询时客户经理不为空");
        }
        //营业日
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 查询条件
        Map map = new HashMap<>();
        map.put("managerId", managerId);
        map.put("cusName", cusName);
        map.put("certCode", certCode);
        map.put("openDay", openDay);
        logger.info("**********合同查询,查询参数为:{}", JSON.toJSONString(map));
        PageHelper.startPage(startPageNum, pageSizeNum);
        resList = ctrLoanContMapper.selectComLoanInfoByManagerId(map);
        PageHelper.clearPage();
        logger.info("**********合同查询结束,返回结果为:{}", JSON.toJSONString(resList));
        resList = resList.parallelStream().map(ret -> {
            ComLoanInfoDto temp = new ComLoanInfoDto();
            BeanUtils.copyProperties(ret, temp);
            //获取客户经理名称
            String managerName = findManagerName(ret.getManagerBrId());
            temp.setManagerName(managerName);
            //获取机构名称
            String managerBrName = findManagerBrName(ret.getManagerBrId());
            temp.setManagerBrName(managerBrName);
            //返回列表
            return temp;
        }).collect(Collectors.toList());
        //返回
        return resList;
    }

    /***
     * 获取责任人名称
     * **/
    public String findManagerName(String managerId) {
        String managerName = "";
        if (StringUtil.isNotEmpty(managerId)) {
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                managerName = adminSmUserDto.getUserName();
            }
        }
        return managerName;
    }

    /***
     * 获取责任机构名称
     * **/
    public String findManagerBrName(String managerBrId) {
        String managerName = "";
        if (StringUtil.isNotEmpty(managerBrId)) {
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(managerBrId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                managerName = adminSmOrgDto.getOrgName();
            }
        }
        return managerName;
    }
}
