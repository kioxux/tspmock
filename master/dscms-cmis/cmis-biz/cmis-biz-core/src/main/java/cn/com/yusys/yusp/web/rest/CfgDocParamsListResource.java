/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.DocImageSpplInfo;
import cn.com.yusys.yusp.dto.CfgDocParamsListDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgDocParamsList;
import cn.com.yusys.yusp.service.CfgDocParamsListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgdocparamslist")
public class CfgDocParamsListResource {
    @Autowired
    private CfgDocParamsListService cfgDocParamsListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgDocParamsList>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgDocParamsList> list = cfgDocParamsListService.selectAll(queryModel);
        return new ResultDto<List<CfgDocParamsList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgDocParamsList>> index(QueryModel queryModel) {
        List<CfgDocParamsList> list = cfgDocParamsListService.selectByModel(queryModel);
        return new ResultDto<List<CfgDocParamsList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cdplSerno}")
    protected ResultDto<CfgDocParamsList> show(@PathVariable("cdplSerno") String cdplSerno) {
        CfgDocParamsList cfgDocParamsList = cfgDocParamsListService.selectByPrimaryKey(cdplSerno);
        return new ResultDto<CfgDocParamsList>(cfgDocParamsList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgDocParamsList> create(@RequestBody CfgDocParamsList cfgDocParamsList) throws URISyntaxException {
        cfgDocParamsListService.insert(cfgDocParamsList);
        return new ResultDto<CfgDocParamsList>(cfgDocParamsList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgDocParamsList cfgDocParamsList) throws URISyntaxException {
        int result = cfgDocParamsListService.update(cfgDocParamsList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cdplSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cdplSerno") String cdplSerno) {
        int result = cfgDocParamsListService.deleteByPrimaryKey(cdplSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgDocParamsListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectByCondition
     * @函数描述:根据条件查询档案参数配置列表
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * @author cainingbo_yx
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<CfgDocParamsList>> selectByCondition(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort("doc_type asc");
        }
        List<CfgDocParamsList> list = cfgDocParamsListService.selectByModel(queryModel);
        return new ResultDto<List<CfgDocParamsList>>(list);
    }

    /**
     * @函数名称:saveCfgDocParamsList
     * @函数描述:档案参数新增
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    @PostMapping("/saveDoImageSpplInfo")
    protected ResultDto<Map> saveCfgDocParamsList(@RequestBody CfgDocParamsList cfgDocParamsList) throws URISyntaxException {
        Map result = cfgDocParamsListService.saveDoImageSpplInfo(cfgDocParamsList);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:saveCfgDocParamsLists
     * @函数描述:档案参数新增+档案参数清单保存
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    @PostMapping("/saveCfgDocParamsLists")
    protected ResultDto<CfgDocParamsListDto> saveCfgDocParamsLists(@RequestBody CfgDocParamsListDto cfgDocParamsListDto) throws URISyntaxException {
        cfgDocParamsListService.saveDoImageSpplInfos(cfgDocParamsListDto);
        return new ResultDto<CfgDocParamsListDto>(cfgDocParamsListDto);
    }

    /**
     * @函数名称:updateCfgDocParamsLists
     * @函数描述:对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateCfgDocParamsLists")
    protected ResultDto<Integer> updateCfgDocParamsLists(@RequestBody CfgDocParamsListDto cfgDocParamsListDto) throws URISyntaxException {
        int result = cfgDocParamsListService.updateCfgDocParamsLists(cfgDocParamsListDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:根据主键查询档案参数配置列表
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<CfgDocParamsList> selectByPrimaryKey(@RequestBody String cdplSerno) {
        CfgDocParamsList cfgDocParamsList = cfgDocParamsListService.selectByPrimaryKey(cdplSerno);
        return new ResultDto<CfgDocParamsList>(cfgDocParamsList);
    }

    /**
     * @函数名称:deleteByCdplSerno
     * @函数描述:根据主键删除档案配置信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByCdplSerno")
    protected ResultDto<Map> deleteByCdplSerno(@RequestBody CfgDocParamsList cfgDocParamsList) {
        Map result = cfgDocParamsListService.deleteByCdplSerno(cfgDocParamsList);
        return new ResultDto<>(result);
    }
}
