package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.FptMPpCustomerBlackDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisPspClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RiskItem0107Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0107Service.class);

    @Autowired
    private CmisPspClientService cmisPspClientService;

    /**
     * @方法名称: riskItem0107
     * @方法描述: 合作方黑灰名单检查
     * @参数与返回说明:
     * @算法描述:
     * 合作方如果在风险预警推送的黑灰名单内，则提示。
     * @创建人: yfs
     * @创建时间: 2021-09-01 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0107(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("授信申请担保信息校验开始*******************业务流水号：【{}】，客户号：【{}】",serno,cusId);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //获取客户信息失败
            return riskResultDto;
        }
        ResultDto<FptMPpCustomerBlackDto> resultDto = cmisPspClientService.queryFptMPpCustomerBlackByCusId(cusId);
        if(Objects.isNull(resultDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10701);
            return riskResultDto;
        }
        FptMPpCustomerBlackDto fptMPpCustomerBlackDto = resultDto.getData();
        if(Objects.nonNull(fptMPpCustomerBlackDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10702);
            return riskResultDto;
        }
        log.info("合作方黑灰名单检查结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}