package cn.com.yusys.yusp.service.server.xdsx0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import cn.com.yusys.yusp.dto.server.xdsx0016.req.Xdsx0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.resp.Xdsx0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.RiskXdGuarantyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * 接口处理类:抵押查封信息同步
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdsx0016Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0016Service.class);

    @Autowired
    private RiskXdGuarantyMapper riskXdGuarantyMapper;//抵押查封信息同步
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * 抵押查封信息同步
     *
     * @param xdsx0016DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0016DataRespDto xdsx0016(Xdsx0016DataReqDto xdsx0016DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value);
        //返回对象
        Xdsx0016DataRespDto xdsx0016DataRespDto = new Xdsx0016DataRespDto();
        try {
            if("".equals(xdsx0016DataReqDto.getDy_no())){
                xdsx0016DataRespDto.setOpFlag("F");
                xdsx0016DataRespDto.setOpMsg("抵押品编号不能为空");
                return xdsx0016DataRespDto;
            }
            if("".equals(xdsx0016DataReqDto.getBdc_no())){
                xdsx0016DataRespDto.setOpFlag("F");
                xdsx0016DataRespDto.setOpMsg("不动产证号不能为空");
                return xdsx0016DataRespDto;
            }
            if("".equals(xdsx0016DataReqDto.getJk_cus_name())){
                xdsx0016DataRespDto.setOpFlag("F");
                xdsx0016DataRespDto.setOpMsg("借款人名称不能为空");
                return xdsx0016DataRespDto;
            }
            RiskXdGuaranty riskXdGuaranty = new RiskXdGuaranty();
            //获取系统当前日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
            //流水号
            riskXdGuaranty.setSerno(serno);
            //抵押品编号
            riskXdGuaranty.setDyNo(xdsx0016DataReqDto.getDy_no());
            //不动产证号
            riskXdGuaranty.setBdcNo(xdsx0016DataReqDto.getBdc_no());
            //借款人名称
            riskXdGuaranty.setJkCusName(xdsx0016DataReqDto.getJk_cus_name());
            //登记日期
            riskXdGuaranty.setApplyTime(openDay);
            //客户经理号
            riskXdGuaranty.setManagerId(xdsx0016DataReqDto.getManager_id());
            //修改次数
            riskXdGuaranty.setIsUpdate("0");
            //合同号
            riskXdGuaranty.setContNo(xdsx0016DataReqDto.getCont_no());
            //预授信流水
            riskXdGuaranty.setPreAppNo(xdsx0016DataReqDto.getPre_app_no());
            //审批状态
            riskXdGuaranty.setApproveStatus("000");
            riskXdGuarantyMapper.insertSelective(riskXdGuaranty);
            xdsx0016DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xdsx0016DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value);
        return xdsx0016DataRespDto;
    }
}
