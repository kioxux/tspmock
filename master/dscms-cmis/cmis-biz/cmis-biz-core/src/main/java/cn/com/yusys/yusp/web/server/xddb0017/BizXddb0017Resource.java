package cn.com.yusys.yusp.web.server.xddb0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0017.req.Xddb0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.Xddb0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0017.Xddb0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取抵押登记双录音视频信息列表
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0017:获取抵押登记双录音视频信息列表")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0017Resource.class);
    @Autowired
    private Xddb0017Service xddb0017Service;

    /**
     * 交易码：xddb0017
     * 交易描述：获取抵押登记双录音视频信息列表
     *
     * @param xddb0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("获取抵押登记双录音视频信息列表")
    @PostMapping("/xddb0017")
    protected @ResponseBody
    ResultDto<Xddb0017DataRespDto> xddb0017(@Validated @RequestBody Xddb0017DataReqDto xddb0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017DataReqDto));
        Xddb0017DataRespDto xddb0017DataRespDto = new Xddb0017DataRespDto();// 响应Dto:获取抵押登记双录音视频信息列表
        ResultDto<Xddb0017DataRespDto> xddb0017DataResultDto = new ResultDto<>();
        String guarno = xddb0017DataReqDto.getGuarno();//抵押物编号
        try {
            // 从xddb0017DataReqDto获取业务值进行业务逻辑处理
            xddb0017DataRespDto = xddb0017Service.xddb0017(xddb0017DataReqDto);
            // 封装xddb0017DataResultDto中正确的返回码和返回信息
            xddb0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, e.getMessage());
            // 封装xddb0017DataResultDto中异常返回码和返回信息
            xddb0017DataResultDto.setCode(e.getErrorCode());
            xddb0017DataResultDto.setMessage(e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, e.getMessage());
            // 封装xddb0017DataResultDto中异常返回码和返回信息
            xddb0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0017DataRespDto到xddb0017DataResultDto中
        xddb0017DataResultDto.setData(xddb0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017DataResultDto));
        return xddb0017DataResultDto;
    }
}
