package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.FdyddLmtSubGuar;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarBizRel;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.dto.GuarBaseInfoRelDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 房抵e点贷授信押品关联流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX08BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX08BizService.class);//定义log


    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;
    @Autowired
    private FdyddLmtSubGuarService fdyddLmtSubGuarService;
    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private GuarBizRelService guarBizRelService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 房贷尽调结果录入
        if ("YX016".equals(bizType)){
            fdyddLmtSubGuarBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 房抵e点贷授信押品关联流程
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void fdyddLmtSubGuarBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            FdyddLmtSubGuar fdyddLmtSubGuar = fdyddLmtSubGuarService.selectBySubSerno(serno);
            log.info("房抵e点贷授信押品关联流程:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                // 获取押品关系表
                Map map = new HashMap();
                map.put("serno", serno);
                //获取业务标识位，通过标识位进行逻辑新增操作
                map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                guarBizRelService.updateBySerno(map);
                //申请表审批状态更新为 997--通过
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_997);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("房抵e点贷授信押品关联流程:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(fdyddLmtSubGuar, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("房抵e点贷授信押品关联流程:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 流程审批状态更新
     * sfResultInfo 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (FdyddLmtSubGuar fdyddLmtSubGuar, String approveStatus){
        fdyddLmtSubGuar.setApproveStatus(approveStatus);
        fdyddLmtSubGuarService.updateSelectiveBySubSerno(fdyddLmtSubGuar);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX08).equals(flowCode);
    }
}
