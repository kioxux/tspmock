package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;

public class GuarBasicGuarInfBusinessIndustryHousrVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfBusinessIndustryHousr getGuarInfBusinessIndustryHousr() {
        return guarInfBusinessIndustryHousr;
    }

    public void setGuarInfBusinessIndustryHousr(GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr) {
        this.guarInfBusinessIndustryHousr = guarInfBusinessIndustryHousr;
    }
}
