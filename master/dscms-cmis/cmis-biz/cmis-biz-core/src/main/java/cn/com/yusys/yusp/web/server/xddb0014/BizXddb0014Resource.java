package cn.com.yusys.yusp.web.server.xddb0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0014.req.Xddb0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0014.resp.Xddb0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0014.Xddb0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:抵押登记注销风控
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0014:抵押登记注销风控")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0014Resource.class);

    @Autowired
    private Xddb0014Service xddb0014Service;
    /**
     * 交易码：xddb0014
     * 交易描述：抵押登记注销风控
     *
     * @param xddb0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记注销风控")
    @PostMapping("/xddb0014")
    protected @ResponseBody
    ResultDto<Xddb0014DataRespDto> xddb0014(@Validated @RequestBody Xddb0014DataReqDto xddb0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataReqDto));
        Xddb0014DataRespDto xddb0014DataRespDto = new Xddb0014DataRespDto();// 响应Dto:抵押登记注销风控
        ResultDto<Xddb0014DataRespDto> xddb0014DataResultDto = new ResultDto<>();
        try {
			String retype = xddb0014DataReqDto.getRetype();//登记类型
			String guarid = xddb0014DataReqDto.getGuarid();//押品编号
            // 从xddb0014DataReqDto获取业务值进行业务逻辑处理
            //  调用xdxw0014Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataReqDto));
            xddb0014DataRespDto = xddb0014Service.selcetGuarBaseInfoByCusName(xddb0014DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xddb0014DataRespDto));
            // 封装xddb0014DataResultDto中正确的返回码和返回信息
            xddb0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, e.getMessage());
            // 封装xddb0014DataResultDto中异常返回码和返回信息
            //  EpbEnum.EPB099999 待调整 开始
            xddb0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EpbEnum.EPB099999 待调整  结束
        }
        // 封装xddb0014DataRespDto到xddb0014DataResultDto中
        xddb0014DataResultDto.setData(xddb0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataRespDto));
        return xddb0014DataResultDto;
    }
}
