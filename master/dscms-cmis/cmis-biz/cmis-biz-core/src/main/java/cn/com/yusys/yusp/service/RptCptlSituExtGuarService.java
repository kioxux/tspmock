/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptCptlSituExtGuar;
import cn.com.yusys.yusp.repository.mapper.RptCptlSituExtGuarMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituExtGuarService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:53:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptCptlSituExtGuarService {

    @Autowired
    private RptCptlSituExtGuarMapper rptCptlSituExtGuarMapper;
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptCptlSituExtGuar selectByPrimaryKey(String pkId) {
        return rptCptlSituExtGuarMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptCptlSituExtGuar> selectAll(QueryModel model) {
        List<RptCptlSituExtGuar> records = (List<RptCptlSituExtGuar>) rptCptlSituExtGuarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptCptlSituExtGuar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptCptlSituExtGuar> list = rptCptlSituExtGuarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptCptlSituExtGuar record) {
        return rptCptlSituExtGuarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptCptlSituExtGuar record) {
        return rptCptlSituExtGuarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptCptlSituExtGuar record) {
        return rptCptlSituExtGuarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptCptlSituExtGuar record) {
        return rptCptlSituExtGuarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptCptlSituExtGuarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptCptlSituExtGuarMapper.deleteByIds(ids);
    }

    /**
     * 查询集团融资对外担保情况
     * @param model
     * @return
     */
    public List<Map<String,Object>> selectGrpExtGuar(QueryModel model){
        String grpSerno = model.getCondition().get("serno").toString();
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            String serno = lmtGrpMemRel.getSingleSerno();
            String cusName = lmtGrpMemRel.getCusName();
            if(StringUtils.isBlank(serno)){
                return new ArrayList<>();
            }
            List<RptCptlSituExtGuar> rptCptlSituExtGuarList = rptCptlSituExtGuarMapper.selectBySerno(serno);
            for (RptCptlSituExtGuar rptCptlSituExtGuar : rptCptlSituExtGuarList) {
                Map<String, Object> map = (Map<String, Object>) BeanUtils.beanToMap(rptCptlSituExtGuar);
                map.put("cusName", cusName);
                result.add(map);
            }
        }
        return result;
    }
    public List<RptCptlSituExtGuar> selectBySerno(String serno){
        return rptCptlSituExtGuarMapper.selectBySerno(serno);
    }
}
