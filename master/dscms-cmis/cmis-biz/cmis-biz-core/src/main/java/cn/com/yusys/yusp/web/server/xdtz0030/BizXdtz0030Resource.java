package cn.com.yusys.yusp.web.server.xdtz0030;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0030.req.Xdtz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.resp.Xdtz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0030.Xdtz0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:查看信贷贴现台账中票据是否已经存在
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0030:查看信贷贴现台账中票据是否已经存在")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0030Resource.class);

    @Autowired
    private Xdtz0030Service xdtz0030Service;
    /**
     * 交易码：xdtz0030
     * 交易描述：查看信贷贴现台账中票据是否已经存在
     *
     * @param xdtz0030DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查看信贷贴现台账中票据是否已经存在")
    @PostMapping("/xdtz0030")
    protected @ResponseBody
    ResultDto<Xdtz0030DataRespDto> xdtz0030(@Validated @RequestBody Xdtz0030DataReqDto xdtz0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataReqDto));
        Xdtz0030DataRespDto xdtz0030DataRespDto = new Xdtz0030DataRespDto();// 响应Dto:查看信贷贴现台账中票据是否已经存在
        ResultDto<Xdtz0030DataRespDto> xdtz0030DataResultDto = new ResultDto<>();
        String porderNo = xdtz0030DataReqDto.getPorderNo();
        try {
            // 从xdtz0030DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, porderNo);
            xdtz0030DataRespDto = Optional.ofNullable(xdtz0030Service.getPorderNumByPorderNo(porderNo))
                    .orElse(new Xdtz0030DataRespDto(0));
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataRespDto));
            // 封装xdtz0030DataResultDto中正确的返回码和返回信息
            xdtz0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, e.getMessage());
            // 封装xdtz0030DataResultDto中异常返回码和返回信息
            xdtz0030DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0030DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0030DataRespDto到xdtz0030DataResultDto中
        xdtz0030DataResultDto.setData(xdtz0030DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataResultDto));
        return xdtz0030DataResultDto;
    }
}