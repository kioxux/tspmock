package cn.com.yusys.yusp.service.server.xdxw0051;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0051.req.Xdxw0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0051.resp.Xdxw0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtPlListInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0036Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0051Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0051Service.class);
    @Autowired
    private LmtPlListInfoMapper lmtPlListInfoMapper;

    /**
     * 根据业务唯一编号查询无还本续贷贷销售收入
     *
     * @param Xdxw0051DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0051DataRespDto xdxw0051(Xdxw0051DataReqDto Xdxw0051DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value);
        Xdxw0051DataRespDto Xdxw0051DataRespDto = new Xdxw0051DataRespDto();
        String bizSerno = Xdxw0051DataReqDto.getApplySerno();//业务编号
        try {
            //销售收入
            BigDecimal saleAmt = new BigDecimal(0);
            //根据业务唯一编号查询无还本续贷贷销售收入
            Map queryMap = new HashMap();
            queryMap.put("bizSerno", bizSerno);
            saleAmt = lmtPlListInfoMapper.selectNearlyYearByPrimaryKey(queryMap);
            //返回销售金额
            Xdxw0051DataRespDto.setSaleAmt(saleAmt.toString());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value);

        return Xdxw0051DataRespDto;
    }
}
