package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BusiAppCoopRel
 * @类描述: busi_app_coop_rel数据实体类
 * @功能描述: 业务与合作方协议关联表
 * @创建人: user
 * @创建时间: 2021-06-17 21:54:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "busi_app_coop_rel")
public class BusiAppCoopRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /** 合同号 **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /** 中文合同编号 **/
    @Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 80)
    private String contCnNo;

    /** 合作协议编号 **/
    @Column(name = "COOP_AGR_NO", unique = false, nullable = true, length = 40)
    private String coopAgrNo;

    /** 借据号 **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;


    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }
    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return contCnNo
     */
    public String getContNo() {
        return this.contCnNo;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contCnNo
     */
    public String getContCnNo() {
        return this.contCnNo;
    }

    /**
     * @param contCnNo
     */
    public void setContCnNo(String contCnNo) {
        this.contCnNo = contCnNo;
    }

    /**
     * @return coopAgrNo
     */
    public String getCoopAgrNo() {
        return this.coopAgrNo;
    }

    /**
     * @param coopAgrNo
     */
    public void setCoopAgrNo(String coopAgrNo) {
        this.coopAgrNo = coopAgrNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }
}
