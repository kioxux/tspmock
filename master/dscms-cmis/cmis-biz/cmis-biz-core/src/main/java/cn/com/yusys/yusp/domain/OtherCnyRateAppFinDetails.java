/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppFinDetails
 * @类描述: other_cny_rate_app_fin_details数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-06 15:35:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_app_fin_details")
public class OtherCnyRateAppFinDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 分项流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SUB_SERNO")
	private String subSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 所属年份 **/
	@Column(name = "BELG_YEAR", unique = false, nullable = true, length = 5)
	private String belgYear;
	
	/** 是否我行 **/
	@Column(name = "IS_SELF_BANK", unique = false, nullable = true, length = 5)
	private String isSelfBank;
	
	/** 他行名称 **/
	@Column(name = "OTHER_BANK_NAME", unique = false, nullable = true, length = 80)
	private String otherBankName;
	
	/** 是否关联客户 **/
	@Column(name = "IS_REL_CUS", unique = false, nullable = true, length = 5)
	private String isRelCus;
	
	/** 关联客户类型 **/
	@Column(name = "REL_CUS_TYPE", unique = false, nullable = true, length = 5)
	private String relCusType;
	
	/** 关联客户编号 **/
	@Column(name = "REL_CUS_ID", unique = false, nullable = true, length = 40)
	private String relCusId;
	
	/** 关联客户名称 **/
	@Column(name = "REL_CUS_NAME", unique = false, nullable = true, length = 80)
	private String relCusName;
	
	/** 上期客户评级 **/
	@Column(name = "LAST_CUS_CRD_GRADE", unique = false, nullable = true, length = 5)
	private String lastCusCrdGrade;
	
	/** 本期客户评级 **/
	@Column(name = "CUR_CUS_CRD_GRADE", unique = false, nullable = true, length = 5)
	private String curCusCrdGrade;
	
	/** 分项品种流水号 **/
	@Column(name = "SUB_PRD_SERNO", unique = false, nullable = true, length = 40)
	private String subPrdSerno;
	
	/** 额度分项编号 **/
	@Column(name = "ACC_SUB_PRD_NO", unique = false, nullable = true, length = 40)
	private String accSubPrdNo;
	
	/** 授信品种编号 **/
	@Column(name = "LMT_BIZ_TYPE", unique = false, nullable = true, length = 20)
	private String lmtBizType;
	
	/** 授信品种名称 **/
	@Column(name = "LMT_BIZ_TYPE_NAME", unique = false, nullable = true, length = 80)
	private String lmtBizTypeName;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private Integer loanTerm;
	
	/** 担保方式 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 执行利率(年) **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 定价方式 **/
	@Column(name = "PRICE_MODE", unique = false, nullable = true, length = 5)
	private String priceMode;
	
	/** LPR利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** LPR基准日期 **/
	@Column(name = "RATE_INTVAL_DATE", unique = false, nullable = true, length = 20)
	private String rateIntvalDate;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;
	
	/** 当前LPR值 **/
	@Column(name = "CUR_LPR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curLpr;
	
	/** 利率调整选项 **/
	@Column(name = "RATE_ADJUST_CYCLE", unique = false, nullable = true, length = 5)
	private String rateAdjustCycle;
	
	/** 下一次利率调整间隔 **/
	@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private Integer nextRateAdjInterval;
	
	/** 第一次调整日 **/
	@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 20)
	private String firstAdjDate;
	
	/** 保证人编号 **/
	@Column(name = "GUARNTR_NO", unique = false, nullable = true, length = 40)
	private String guarntrNo;
	
	/** 对外担保总额 **/
	@Column(name = "OUTGUAR_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outguarTotalAmt;
	
	/** 其中为本行担保额 **/
	@Column(name = "ORG_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal orgGuarAmt;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param belgYear
	 */
	public void setBelgYear(String belgYear) {
		this.belgYear = belgYear;
	}
	
    /**
     * @return belgYear
     */
	public String getBelgYear() {
		return this.belgYear;
	}
	
	/**
	 * @param isSelfBank
	 */
	public void setIsSelfBank(String isSelfBank) {
		this.isSelfBank = isSelfBank;
	}
	
    /**
     * @return isSelfBank
     */
	public String getIsSelfBank() {
		return this.isSelfBank;
	}
	
	/**
	 * @param otherBankName
	 */
	public void setOtherBankName(String otherBankName) {
		this.otherBankName = otherBankName;
	}
	
    /**
     * @return otherBankName
     */
	public String getOtherBankName() {
		return this.otherBankName;
	}
	
	/**
	 * @param isRelCus
	 */
	public void setIsRelCus(String isRelCus) {
		this.isRelCus = isRelCus;
	}
	
    /**
     * @return isRelCus
     */
	public String getIsRelCus() {
		return this.isRelCus;
	}
	
	/**
	 * @param relCusType
	 */
	public void setRelCusType(String relCusType) {
		this.relCusType = relCusType;
	}
	
    /**
     * @return relCusType
     */
	public String getRelCusType() {
		return this.relCusType;
	}
	
	/**
	 * @param relCusId
	 */
	public void setRelCusId(String relCusId) {
		this.relCusId = relCusId;
	}
	
    /**
     * @return relCusId
     */
	public String getRelCusId() {
		return this.relCusId;
	}
	
	/**
	 * @param relCusName
	 */
	public void setRelCusName(String relCusName) {
		this.relCusName = relCusName;
	}
	
    /**
     * @return relCusName
     */
	public String getRelCusName() {
		return this.relCusName;
	}
	
	/**
	 * @param lastCusCrdGrade
	 */
	public void setLastCusCrdGrade(String lastCusCrdGrade) {
		this.lastCusCrdGrade = lastCusCrdGrade;
	}
	
    /**
     * @return lastCusCrdGrade
     */
	public String getLastCusCrdGrade() {
		return this.lastCusCrdGrade;
	}
	
	/**
	 * @param curCusCrdGrade
	 */
	public void setCurCusCrdGrade(String curCusCrdGrade) {
		this.curCusCrdGrade = curCusCrdGrade;
	}
	
    /**
     * @return curCusCrdGrade
     */
	public String getCurCusCrdGrade() {
		return this.curCusCrdGrade;
	}
	
	/**
	 * @param subPrdSerno
	 */
	public void setSubPrdSerno(String subPrdSerno) {
		this.subPrdSerno = subPrdSerno;
	}
	
    /**
     * @return subPrdSerno
     */
	public String getSubPrdSerno() {
		return this.subPrdSerno;
	}
	
	/**
	 * @param accSubPrdNo
	 */
	public void setAccSubPrdNo(String accSubPrdNo) {
		this.accSubPrdNo = accSubPrdNo;
	}
	
    /**
     * @return accSubPrdNo
     */
	public String getAccSubPrdNo() {
		return this.accSubPrdNo;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}
	
    /**
     * @return lmtBizType
     */
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName;
	}
	
    /**
     * @return lmtBizTypeName
     */
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public Integer getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param priceMode
	 */
	public void setPriceMode(String priceMode) {
		this.priceMode = priceMode;
	}
	
    /**
     * @return priceMode
     */
	public String getPriceMode() {
		return this.priceMode;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param rateIntvalDate
	 */
	public void setRateIntvalDate(String rateIntvalDate) {
		this.rateIntvalDate = rateIntvalDate;
	}
	
    /**
     * @return rateIntvalDate
     */
	public String getRateIntvalDate() {
		return this.rateIntvalDate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param curLpr
	 */
	public void setCurLpr(java.math.BigDecimal curLpr) {
		this.curLpr = curLpr;
	}
	
    /**
     * @return curLpr
     */
	public java.math.BigDecimal getCurLpr() {
		return this.curLpr;
	}
	
	/**
	 * @param rateAdjustCycle
	 */
	public void setRateAdjustCycle(String rateAdjustCycle) {
		this.rateAdjustCycle = rateAdjustCycle;
	}
	
    /**
     * @return rateAdjustCycle
     */
	public String getRateAdjustCycle() {
		return this.rateAdjustCycle;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(Integer nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}
	
    /**
     * @return nextRateAdjInterval
     */
	public Integer getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}
	
    /**
     * @return firstAdjDate
     */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param guarntrNo
	 */
	public void setGuarntrNo(String guarntrNo) {
		this.guarntrNo = guarntrNo;
	}
	
    /**
     * @return guarntrNo
     */
	public String getGuarntrNo() {
		return this.guarntrNo;
	}
	
	/**
	 * @param outguarTotalAmt
	 */
	public void setOutguarTotalAmt(java.math.BigDecimal outguarTotalAmt) {
		this.outguarTotalAmt = outguarTotalAmt;
	}
	
    /**
     * @return outguarTotalAmt
     */
	public java.math.BigDecimal getOutguarTotalAmt() {
		return this.outguarTotalAmt;
	}
	
	/**
	 * @param orgGuarAmt
	 */
	public void setOrgGuarAmt(java.math.BigDecimal orgGuarAmt) {
		this.orgGuarAmt = orgGuarAmt;
	}
	
    /**
     * @return orgGuarAmt
     */
	public java.math.BigDecimal getOrgGuarAmt() {
		return this.orgGuarAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}