/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.service.LmtAppSubPrdService;
import cn.com.yusys.yusp.service.LmtApprLoanCondService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.RepayCapPlanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: LmtGrpAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-07 10:06:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "集团授信申请")
@RestController
@RequestMapping("/api/lmtgrpapp")
public class LmtGrpAppResource {


    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    /**
     * @函数名称: queryAllGrpAppListForPartMgr
     * @函数描述: 集团授信申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信分开填报列表")
    @PostMapping("/queryallgrpapplistforpartmgr")
    protected ResultDto<List<LmtGrpApp>> queryAllGrpAppListForPartMgr() {
        List<LmtGrpApp> list = lmtGrpAppService.queryAllGrpAppListForPartMgr(SessionUtils.getLoginCode());
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: update
     * @函数描述: 集团授信申请数据修改接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信申请数据修改接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpApp lmtGrpApp) {
        int result = lmtGrpAppService.update(lmtGrpApp);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称: updatelmtgrpappdicussdata
     * @函数描述: 集团授信申请再议数据申请向导保存接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-8 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信申请再议数据申请向导保存接口")
    @PostMapping("/updatelmtgrpappdicussdata")
    protected ResultDto<Integer> updateLmtGrpAppDicussData(@RequestBody LmtGrpApp lmtGrpApp) {
        int result = lmtGrpAppService.updateLmtGrpAppDicussData(lmtGrpApp);
        return new ResultDto<>(result);
    }



    /**
     * @函数名称: queryHisList
     * @函数描述: 集团授信历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信历史列表")
    @PostMapping("/queryhislist")
    protected ResultDto<List<LmtGrpApp>> queryHisList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryHisList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryAppList
     * @函数描述: 集团授信申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信申请列表")
    @PostMapping("/queryapplist")
    protected ResultDto<List<LmtGrpApp>> queryAppList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryAppList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryAppHisList
     * @函数描述: 集团授信申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信申请历史列表")
    @PostMapping("/queryapphislist")
    protected ResultDto<List<LmtGrpApp>> queryAppHisList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryAppHisList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryChgList
     * @函数描述: 集团授信变更申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信变更申请列表")
    @PostMapping("/querychglist")
    protected ResultDto<List<LmtGrpApp>> queryChgList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryChgList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryChgHisList
     * @函数描述: 集团授信变更申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信变更申请历史列表")
    @PostMapping("/querychghislist")
    protected ResultDto<List<LmtGrpApp>> queryChgHisList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryChgHisList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryFinerList
     * @函数描述: 集团授信细化申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信细化申请列表")
    @PostMapping("/queryfinerlist")
    protected ResultDto<List<LmtGrpApp>> queryFinerList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryFinerList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryFinerHisList
     * @函数描述: 集团授信细化申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信细化申请历史列表")
    @PostMapping("/queryfinerhislist")
    protected ResultDto<List<LmtGrpApp>> queryFinerHisList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryFinerHisList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryAdjustList
     * @函数描述: 集团授信细化申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信调剂申请列表")
    @PostMapping("/queryadjustlist")
    protected ResultDto<List<LmtGrpApp>> queryAdjustList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryAdjustList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryAdjustHisList
     * @函数描述: 集团授信调剂申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信调剂申请历史列表")
    @PostMapping("/queryadjusthislist")
    protected ResultDto<List<LmtGrpApp>> queryAdjustHisList(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> list = lmtGrpAppService.queryAdjustHisList(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @函数名称: queryLmtGrpAppBySerno
     * @函数描述: 集团申请信息数据详情查询接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团申请信息数据详情查询接口")
    @PostMapping("/querylmtgrpappbygrpserno")
    protected ResultDto<LmtGrpApp> queryLmtGrpAppByGrpSerno(@RequestBody Map<String, String> params) {
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(params.get("grpSerno"));
        return new ResultDto<>(lmtGrpApp);
    }

    /**
     * @函数名称: queryLmtGrpAppBySingleSerno
     * @函数描述: 根据单一客户申请流水号查询集团申请信息数据详情
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-20 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据单一客户申请流水号查询集团申请信息数据详情")
    @PostMapping("/querylmtgrpappbysingleserno")
    protected ResultDto<LmtGrpApp> queryLmtGrpAppBySingleSerno(@RequestBody Map<String, String> params) {
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppBySingleSerno(params.get("serno"));
        return new ResultDto<>(lmtGrpApp);
    }


    /**
     * @函数名称: logicDelete
     * @函数描述: 集团申请数据逻辑删除接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团申请数据逻辑删除接口")
    @PostMapping("/logicdelete")
    protected ResultDto<Integer> logicDelete(@RequestBody String grpSerno) {
        return ResultDto.success(lmtGrpAppService.logicDelete(grpSerno));
    }

    /**
     * @函数名称: guideSave
     * @函数描述: 集团授信申请向导页面保存接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("集团授信申请向导页面保存接口")
    @PostMapping("/guidesave")
    protected ResultDto<String> guideSave(@RequestBody LmtGrpApp lmtGrpApp) throws ParseException {
        return ResultDto.success(lmtGrpAppService.guideSave(lmtGrpApp));
    }


    /**
     * @函数名称: getLmtTypeByGrpNo
     * @函数描述: 根据集团客户编号获取集团的授信类型
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团客户编号获取集团的授信类型")
    @PostMapping("/getlmttypebygrpno")
    protected ResultDto<String> getLmtTypeByGrpNo(@RequestBody String grpCusId) {
        return ResultDto.success(lmtGrpAppService.getLmtTypeByGrpNo(grpCusId));
    }

    /**
     * @函数名称: judgeIsExistOnWayApp
     * @函数描述: 根据集团客户号查询集团客户是否存在在途其他的集团授信申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团客户号查询集团客户是否存在在途其他的集团授信申请")
    @PostMapping("/judgeisexistonwayapp")
    protected ResultDto<Boolean> judgeIsExistOnWayApp(@RequestBody String grpCusId) {
        return ResultDto.success(lmtGrpAppService.judgeIsExistOnWayApp(grpCusId));
    }

    /**
     * @函数名称: queryRemindCountByManagerId
     * @函数描述: 通过客户经理号查询待处理的集团客户授信申报填报的数量接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过客户经理号查询待处理的集团客户授信申报填报的数量")
    @PostMapping("/queryremindcountbymanagerid")
    protected ResultDto<Integer> queryRemindCountByManagerId(@RequestBody String managerId) {
        return ResultDto.success(lmtGrpAppService.queryRemindCountByManagerId(managerId));
    }

    /**
     * @方法名称：queryByCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 10:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/querybygrpcusid")
    protected ResultDto<List<LmtGrpApp>> queryByGrpCusId(@RequestBody QueryModel queryModel) {
        List<LmtGrpApp> lmtAppList = lmtGrpAppService.queryByGrpCusId(queryModel);
        return new ResultDto<>(lmtAppList);
    }

    /**
     * @方法名称：handleBusinessAfterEnd
     * @方法描述：提交结束后处理,暂时使用后续删除此方法
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-05-24 上午 10:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/handlebusinessafterend")
    protected ResultDto<Boolean> handleBusinessAfterEnd(@RequestBody LmtGrpApp lmtGrpApp) throws Exception {
        String serno = lmtGrpApp.getGrpSerno();
        // 当前登录人信息
        User userInfo = SessionUtils.getUserInformation();
        String curUser = userInfo.getLoginCode();
        String curOrg = userInfo.getOrg().getCode();
        lmtGrpAppService.handleBusinessAfterEnd(serno,curUser,curOrg,"");
        return new ResultDto<Boolean>(true);
    }

    /**
     * @函数名称: queryLmtGrpApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询授信分项品种数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询授信分项品种数据")
    @PostMapping("/queryLmtApprSubByGrpSerno")
    protected ResultDto<List<LmtAppSubPrd>> queryLmtApprSubByGrpSerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtAppSubPrd> list = lmtAppSubPrdService.queryLmtApprSubByGrpSerno(params.get("grpSerno"));
        return new ResultDto<List<LmtAppSubPrd>>(list);
    }

    /**
     * @函数名称: queryLmtGrpApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询用信条件数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询用信条件数据")
    @PostMapping("/queryLmtApprLoanCondByGrpSerno")
    protected ResultDto<List<LmtApprLoanCond>> queryLmtApprLoanCondByGrpSerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprLoanCond> list = lmtApprLoanCondService.queryLmtApprLoanCondByGrpSerno(params.get("grpSerno"),params.get("condType"));
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }

    /**
     * @函数名称: queryLmtGrpApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询还款计划数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询还款计划数据")
    @PostMapping("/queryRepayCapPlanByGrpSerno")
    protected ResultDto<List<RepayCapPlan>> queryRepayCapPlanByGrpSerno(@RequestBody Map<String,String> params) {
        ArrayList<RepayCapPlan> list = repayCapPlanService.queryRepayCapPlanByGrpSerno(params.get("grpSerno"));
        return new ResultDto<List<RepayCapPlan>>(list);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtGrpApp> save(@RequestBody LmtGrpApp lmtGrpApp) throws URISyntaxException {
        LmtGrpApp lmtGrpApp1 = lmtGrpAppService.queryInfoByGrpSerno(lmtGrpApp.getGrpSerno());
        lmtGrpApp.setOprType("01");
        if(lmtGrpApp1!=null){
            lmtGrpAppService.update(lmtGrpApp);
        }else{
            lmtGrpAppService.insert(lmtGrpApp);
        }
        return ResultDto.success(lmtGrpApp);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryInfoByGrpSerno")
    protected ResultDto<LmtGrpApp> queryInfoByGrpSerno(@RequestBody String grpSerno) {
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(grpSerno);
        return  ResultDto.success(lmtGrpApp);
    }

    /**
     * 根据集团流水号查询成员授信申请信息
     * @param grpSerno
     * @return
     */
    @PostMapping("/getLmtAppByGrpSerno")
    protected ResultDto<List<LmtApp>> getLmtAppByGrpSerno(@RequestBody String grpSerno){
        return new ResultDto<List<LmtApp>>(lmtGrpAppService.getLmtAppByGrpSerno(grpSerno));
    }
    /**
     * 根据集团流水号查询参与申报成员授信申请信息
     * @param grpSerno
     * @return
     */
    @PostMapping("/getLmtAppByGrpSernoIsDeclare")
    protected ResultDto<List<LmtApp>> getLmtAppByGrpSernoIsDeclare(@RequestBody String grpSerno){
        return new ResultDto<List<LmtApp>>(lmtGrpAppService.getLmtAppByGrpSernoIsDeclare(grpSerno));
    }
    /**
     * 根据流水号获取原申请信息
     */
    @PostMapping("/getLastGrpAppBySerno")
    protected ResultDto<LmtGrpApp> getLastGrpAppBySerno(@RequestBody String grpSerno){
        return new ResultDto<LmtGrpApp>(lmtGrpAppService.getLastGrpAppBySerno(grpSerno));
    }

    /**
     * 校验当前集团向下的成团客户是否全部审批通过
     */
    @PostMapping("/checkIsApproveSuccess")
    protected ResultDto<Boolean> checkIsApproveSuccess(@RequestBody String grpSerno){
        return ResultDto.success(lmtGrpAppService.checkIsApproveSuccess(grpSerno));
    }

    /**
     *  校验授信调剂是否在授信申报的登记中
     */
    @PostMapping("/checkAdjustOnLmtGrpApp")
    protected ResultDto<Boolean> checkAdjustOnLmtGrpApp(@RequestBody String grpSerno){
        return ResultDto.success(lmtGrpAppService.checkAdjustOnLmtGrpApp(grpSerno));
    }

    /**
     *  校验授信调剂前后的授信总额度是否一致
     */
    @PostMapping("/checkadjustamtissame")
    protected ResultDto<Boolean> checkAdjustAmtIsSame(@RequestBody Map map){
        return ResultDto.success(lmtGrpAppService.checkAdjustAmtIsSame(map));
    }

}
