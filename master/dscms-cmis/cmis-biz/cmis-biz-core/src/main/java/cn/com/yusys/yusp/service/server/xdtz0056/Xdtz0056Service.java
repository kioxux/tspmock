package cn.com.yusys.yusp.service.server.xdtz0056;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0056.resp.Xdtz0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/5 12:48
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xdtz0056Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0056Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @Description:根据billNo查询还款业务类型
     * @Author: YX-WJ
     * @Date: 2021/6/5 18:56
     * @param billno:
     * @return: cn.com.yusys.yusp.dto.server.xdtz0056.resp.Xdtz0056DataRespDto
     **/
    public Xdtz0056DataRespDto xdtz0056(String billno) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value);
        Xdtz0056DataRespDto xdtz0056DataRespDto = new Xdtz0056DataRespDto();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, billno);
            String prdId = accLoanMapper.selectPrdIdByBillNo(billno);
            if (StringUtils.isNotBlank(prdId)) {
                Map<String,String> paramMap = new HashMap<>();
                paramMap.put("prdId", prdId);
                ResultDto<Integer> resultDto = iCmisCfgClientService.queryContInfoByEnName4Biz(paramMap);
                Integer count = resultDto.getData();
                xdtz0056DataRespDto.setBiztype(count == 0 ? "01" : "02");
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, billno);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value);
        return xdtz0056DataRespDto;
    }
}
