/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetFinan
 * @类描述: iqp_dis_asset_finan数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_dis_asset_finan")
public class IqpDisAssetFinan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 金融资产表主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "FINAN_PK")
	private String finanPk;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	@Column(name = "APPT_CODE", unique = false, nullable = false, length = 32)
	private String apptCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 金融产品类型 STD_ZB_FIN_PRD_TYP **/
	@Column(name = "FIN_PRD_TYPE", unique = false, nullable = true, length = 5)
	private String finPrdType;
	
	/** 金融产品名称 **/
	@Column(name = "FIN_PRD_NAME", unique = false, nullable = true, length = 80)
	private String finPrdName;
	
	/** 是否满足起息日在三个月以上  STD_ZB_YES_NO **/
	@Column(name = "DEPOSIT_TYPE", unique = false, nullable = true, length = 2)
	private String depositType;
	
	/** 时点余额 **/
	@Column(name = "TIME_POINT_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal timePointBalance;
	
	/** 活期存款（含第三方存管证券保证金）近3个月日均余额 **/
	@Column(name = "AVER_DAILY_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal averDailyBalance;
	
	/** 发行方 STD_ZB_LSSUER_TYPE **/
	@Column(name = "LSSUER_TYPE", unique = false, nullable = true, length = 2)
	private String lssuerType;
	
	/** 风险等级 STD_ZB_RISK_LEVEL **/
	@Column(name = "RISK_LEVEL", unique = false, nullable = true, length = 2)
	private String riskLevel;
	
	/** 认购金额 **/
	@Column(name = "BAY_AMOUNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bayAmount;
	
	/** 是否保本 STD_ZB_YES_NO **/
	@Column(name = "CAPITAL_TYPE", unique = false, nullable = true, length = 2)
	private String capitalType;
	
	/** 基金类型 STD_ZB_FUND_TYPE **/
	@Column(name = "FUND_TYPE", unique = false, nullable = true, length = 2)
	private String fundType;
	
	/** 近1月以内某时点的基金市值 **/
	@Column(name = "MARKET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal marketValue;
	
	/** 股票属性 STD_ZB_SHARES_TYPE **/
	@Column(name = "SHARES_TYPE", unique = false, nullable = true, length = 2)
	private String sharesType;
	
	/** 近1个月内某时点的股票账户市值 **/
	@Column(name = "SHARES_ACCOUNT_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sharesAccountValue;
	
	/** 近1个月内某时点或近一个财务年度的每股净资产 **/
	@Column(name = "NET_ASSETS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal netAssets;
	
	/** 股份数 **/
	@Column(name = "SHARES_NUMBER", unique = false, nullable = true, length = 30)
	private String sharesNumber;
	
	/** 其他金融资产（折算后）金额 **/
	@Column(name = "OTHER_FINAN_ASSET", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherFinanAsset;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 客户经理认定收入金额 **/
	@Column(name = "MANAGER_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	@Column(name = "BRANCH_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	@Column(name = "HEAD_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal headExIncome;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param finanPk
	 */
	public void setFinanPk(String finanPk) {
		this.finanPk = finanPk;
	}
	
    /**
     * @return finanPk
     */
	public String getFinanPk() {
		return this.finanPk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param finPrdType
	 */
	public void setFinPrdType(String finPrdType) {
		this.finPrdType = finPrdType;
	}
	
    /**
     * @return finPrdType
     */
	public String getFinPrdType() {
		return this.finPrdType;
	}
	
	/**
	 * @param finPrdName
	 */
	public void setFinPrdName(String finPrdName) {
		this.finPrdName = finPrdName;
	}
	
    /**
     * @return finPrdName
     */
	public String getFinPrdName() {
		return this.finPrdName;
	}
	
	/**
	 * @param depositType
	 */
	public void setDepositType(String depositType) {
		this.depositType = depositType;
	}
	
    /**
     * @return depositType
     */
	public String getDepositType() {
		return this.depositType;
	}
	
	/**
	 * @param timePointBalance
	 */
	public void setTimePointBalance(java.math.BigDecimal timePointBalance) {
		this.timePointBalance = timePointBalance;
	}
	
    /**
     * @return timePointBalance
     */
	public java.math.BigDecimal getTimePointBalance() {
		return this.timePointBalance;
	}
	
	/**
	 * @param averDailyBalance
	 */
	public void setAverDailyBalance(java.math.BigDecimal averDailyBalance) {
		this.averDailyBalance = averDailyBalance;
	}
	
    /**
     * @return averDailyBalance
     */
	public java.math.BigDecimal getAverDailyBalance() {
		return this.averDailyBalance;
	}
	
	/**
	 * @param lssuerType
	 */
	public void setLssuerType(String lssuerType) {
		this.lssuerType = lssuerType;
	}
	
    /**
     * @return lssuerType
     */
	public String getLssuerType() {
		return this.lssuerType;
	}
	
	/**
	 * @param riskLevel
	 */
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}
	
    /**
     * @return riskLevel
     */
	public String getRiskLevel() {
		return this.riskLevel;
	}
	
	/**
	 * @param bayAmount
	 */
	public void setBayAmount(java.math.BigDecimal bayAmount) {
		this.bayAmount = bayAmount;
	}
	
    /**
     * @return bayAmount
     */
	public java.math.BigDecimal getBayAmount() {
		return this.bayAmount;
	}
	
	/**
	 * @param capitalType
	 */
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType;
	}
	
    /**
     * @return capitalType
     */
	public String getCapitalType() {
		return this.capitalType;
	}
	
	/**
	 * @param fundType
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	
    /**
     * @return fundType
     */
	public String getFundType() {
		return this.fundType;
	}
	
	/**
	 * @param marketValue
	 */
	public void setMarketValue(java.math.BigDecimal marketValue) {
		this.marketValue = marketValue;
	}
	
    /**
     * @return marketValue
     */
	public java.math.BigDecimal getMarketValue() {
		return this.marketValue;
	}
	
	/**
	 * @param sharesType
	 */
	public void setSharesType(String sharesType) {
		this.sharesType = sharesType;
	}
	
    /**
     * @return sharesType
     */
	public String getSharesType() {
		return this.sharesType;
	}
	
	/**
	 * @param sharesAccountValue
	 */
	public void setSharesAccountValue(java.math.BigDecimal sharesAccountValue) {
		this.sharesAccountValue = sharesAccountValue;
	}
	
    /**
     * @return sharesAccountValue
     */
	public java.math.BigDecimal getSharesAccountValue() {
		return this.sharesAccountValue;
	}
	
	/**
	 * @param netAssets
	 */
	public void setNetAssets(java.math.BigDecimal netAssets) {
		this.netAssets = netAssets;
	}
	
    /**
     * @return netAssets
     */
	public java.math.BigDecimal getNetAssets() {
		return this.netAssets;
	}
	
	/**
	 * @param sharesNumber
	 */
	public void setSharesNumber(String sharesNumber) {
		this.sharesNumber = sharesNumber;
	}
	
    /**
     * @return sharesNumber
     */
	public String getSharesNumber() {
		return this.sharesNumber;
	}
	
	/**
	 * @param otherFinanAsset
	 */
	public void setOtherFinanAsset(java.math.BigDecimal otherFinanAsset) {
		this.otherFinanAsset = otherFinanAsset;
	}
	
    /**
     * @return otherFinanAsset
     */
	public java.math.BigDecimal getOtherFinanAsset() {
		return this.otherFinanAsset;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return managerIncome
     */
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return branchExIncome
     */
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return headExIncome
     */
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}