package cn.com.yusys.yusp.web.server.xdxw0047;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0047.req.Xdxw0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0047.resp.Xdxw0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0047.Xdxw0047Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:增享贷2.0风控模型A任务推送
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0047:增享贷2.0风控模型A任务推送")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0047Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0047Resource.class);

    @Resource
    private Xdxw0047Service xdxw0047Service;

    /**
     * 交易码：xdxw0047
     * 交易描述：增享贷2.0风控模型A任务推送
     *
     * @param xdxw0047DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("增享贷2.0风控模型A任务推送")
    @PostMapping("/xdxw0047")
    protected @ResponseBody
    ResultDto<Xdxw0047DataRespDto> xdxw0047(@Validated @RequestBody Xdxw0047DataReqDto xdxw0047DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataReqDto));
        Xdxw0047DataRespDto xdxw0047DataRespDto = new Xdxw0047DataRespDto();// 响应Dto:增享贷2.0风控模型A任务推送
        ResultDto<Xdxw0047DataRespDto> xdxw0047DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0047DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataReqDto));
            xdxw0047DataRespDto = xdxw0047Service.xdxw0047(xdxw0047DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataRespDto));
            xdxw0047DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0047DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            xdxw0047DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0047DataRespDto.setOpMsg(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0047DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0047DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            xdxw0047DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0047DataRespDto.setOpMsg(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0047DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0047DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0047DataRespDto到xdxw0047DataResultDto中
        xdxw0047DataResultDto.setData(xdxw0047DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataRespDto));
        return xdxw0047DataResultDto;
    }
}
