/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarBizRel;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.service.GuarBizRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarbizrel")
public class GuarBizRelResource {
    @Autowired
    private GuarBizRelService guarBizRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarBizRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarBizRel> list = guarBizRelService.selectAll(queryModel);
        return new ResultDto<List<GuarBizRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarBizRel>> index(QueryModel queryModel) {
        List<GuarBizRel> list = guarBizRelService.selectByModel(queryModel);
        return new ResultDto<List<GuarBizRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<GuarBizRel> show(@PathVariable("pkId") String pkId) {
        GuarBizRel guarBizRel = guarBizRelService.selectByPrimaryKey(pkId);
        return new ResultDto<GuarBizRel>(guarBizRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarBizRel> create(@RequestBody GuarBizRel guarBizRel) throws URISyntaxException {
        guarBizRelService.insert(guarBizRel);
        return new ResultDto<GuarBizRel>(guarBizRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarBizRel guarBizRel) throws URISyntaxException {
        int result = guarBizRelService.update(guarBizRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = guarBizRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteByPrimaryKey
     * @函数描述:根据主键删除业务与押品的关系
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPrimaryKey")
    protected ResultDto<Integer> deleteByPrimaryKey(@RequestBody GuarBizRel guarBizRel ) {
        int result = guarBizRelService.deleteByPrimaryKey(guarBizRel.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarBizRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author 王祝远
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    @PostMapping("/saveguarinforelguarbaseinfo")
    protected ResultDto<Integer> saveGuarInfoRel(@RequestBody GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        Integer result = guarBizRelService.insertGuarBizRelGuarBase(guarBizRelGuarBaseDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author zhangliang15
     * @date 2021/09/27 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    @PostMapping("/ffdSaveGuarInfoRel")
    protected ResultDto<Integer> ffdSaveGuarInfoRel(@RequestBody GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        Integer result = guarBizRelService.ffdSaveGuarInfoRel(guarBizRelGuarBaseDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param guarBizRelGuaranteeDto
     * @return Integer
     * @author wzy
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存与保证人关系
     * @修改历史: V1.0
     */
    @PostMapping("/saveguarinforelGuarantee")
    protected ResultDto<Integer> saveGuarInfoRelGuarantee(@RequestBody GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        return new ResultDto<>(guarBizRelService.insertGuarBizRel(guarBizRelGuaranteeDto));
    }
    /**
     * @param guarBizRelGuaranteeDto
     * @return Integer
     * @author wzy
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存与保证人关系
     * @修改历史: V1.0
     */
    @PostMapping("/updateguarinforelGuarantee")
    protected ResultDto<Integer> updateguarinforelGuarantee(@RequestBody GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        return new ResultDto<Integer>(guarBizRelService.updateGuarBizRel(guarBizRelGuaranteeDto));
    }
    /**
     * @param guarBizRelGuaranteeDto
     * @return Integer
     * @author wzy
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存与保证人关系
     * @修改历史: V1.0
     */
    @PostMapping("/deleteguarinforelGuarantee")
    protected ResultDto<Integer> deleteGuarInfoRelGuarantee(@RequestBody GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        Integer result = guarBizRelService.deleteGuarInfoRelGuarantee(guarBizRelGuaranteeDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author 王祝远
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    @PostMapping("/updateguarinforelguarbaseinfo")
    protected ResultDto<Integer> updateguarinforelguarbaseinfo(@RequestBody GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        Integer result = guarBizRelService.updateGuarBizRelGuarBase(guarBizRelGuarBaseDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 更新押品及业务关联押品信息
     * @param guarBizRelGuarBaseDto
     * @return
     */
    @PostMapping("/updateguarbaseinfo")
    protected ResultDto<Integer> updateGuarBaseInfo(@RequestBody GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        Integer result = guarBizRelService.updateGuarBaseInfo(guarBizRelGuarBaseDto);
        return new ResultDto<>(result);
    }

    /**
     * 查询业务关联押品信息
     * @param pkId
     * @return
     */
    @GetMapping("/queryguarbizrelguarbasedto/{pkId}")
    protected ResultDto<GuarBizRelGuarBaseDto> selectGuarBizRelGuarBaseDto(@PathVariable("pkId") String pkId) {
        GuarBizRelGuarBaseDto guarBizRelGuarBaseDto = guarBizRelService.selectGuarBizRelGuarBaseDto(pkId);
        return new ResultDto<>(guarBizRelGuarBaseDto);
    }

    /**
     * 更新押品及业务关联押品信息
     * @param queryModel
     * @return
     */
    @PostMapping("/checkguarnoisexist")
    protected ResultDto<String> checkGuarNoIsExist(@RequestBody QueryModel queryModel) {
        String result = guarBizRelService.checkGuarNoIsExist(queryModel);
        return new ResultDto<>(result);
    }
}
