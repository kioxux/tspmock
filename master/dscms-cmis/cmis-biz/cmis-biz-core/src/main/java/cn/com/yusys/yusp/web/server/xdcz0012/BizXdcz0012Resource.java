package cn.com.yusys.yusp.web.server.xdcz0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0012.req.Xdcz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0012.resp.Xdcz0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:出账记录详情查看
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0012:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0012.BizXdcz0012Resource.class);
    /**
     * 交易码：xdcz0012
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0012DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0012")
    protected @ResponseBody
    ResultDto<Xdcz0012DataRespDto>  xdcz0012(@Validated @RequestBody Xdcz0012DataReqDto xdcz0012DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012DataReqDto));
        Xdcz0012DataRespDto  xdcz0012DataRespDto  = new Xdcz0012DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0012DataRespDto>xdcz0012DataResultDto = new ResultDto<>();
        xdcz0012DataReqDto.getBillNo();//借据号
        xdcz0012DataReqDto.getFlag();//标志
        xdcz0012DataReqDto.getList();


        try {
            // 从xdcz0012DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0012DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdcz0012DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xdcz0012DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdcz0012DataRespDto对象结束
            // 封装xdcz0012DataResultDto中正确的返回码和返回信息
            xdcz0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value,e.getMessage());
            // 封装xdcz0012DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0012DataRespDto到xdcz0012DataResultDto中
        xdcz0012DataResultDto.setData(xdcz0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012DataRespDto));
        return xdcz0012DataResultDto;
    }
}
