/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpPrePaymentMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 10:34:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpPrePaymentMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpPrePayment selectByPrimaryKey(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpPrePayment> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpPrePayment record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpPrePayment record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpPrePayment record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpPrePayment record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @Description:根据billNo查询在途的还款申请数量
     * @Author: YX-WJ
     * @Date: 2021/6/3 20:58
     * @param billNo : 台账编号
     * @return: cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto
     **/
    int selecOnTheWayCountByBillNo(String billNo);

    /**
     * @Description:根据billNo查询在途的还款申请合同号
     * @Author: YX-WJ
     * @Date: 2021/6/3 21:54
     * @param billNo:
     * @return: java.lang.String
     **/
    String selectOnTheWayContNoByBillNo(String billNo);

    /**
     * @Description:根据billNo查询在途的还款金额
     * @Author: YX-WJ
     * @Date: 2021/6/3 21:57
     * @param billNo:
     * @return: java.math.BigDecimal
     **/
    BigDecimal selectOnTheWayAmtByBillNo(String billNo);

    /**
     * @Description:分页查询指定日期的数据
     * @Author: YX-WJ
     * @Date: 2021/6/10 23:48
     * @param queryDate:查询日期
     * @param pageSize:起始页数
     * @param startNum:分页大小
     * @return: java.util.List<cn.com.yusys.yusp.dto.server.xddh0013.resp.List>
     **/
    List<cn.com.yusys.yusp.dto.server.xddh0013.resp.List> queryByQueryDate(@Param("queryDate")String queryDate,
                                                                           @Param("pageSize")int pageSize,
                                                                           @Param("startNum")int startNum);

    /**
     * @Description:查询指定日期的总条数
     * @Author: YX-WJ
     * @Date: 2021/6/11 0:03
     * @param queryDate:
     * @return: int
     **/
    int queryCoountByQueryDate(@Param("queryDate") String queryDate);
}