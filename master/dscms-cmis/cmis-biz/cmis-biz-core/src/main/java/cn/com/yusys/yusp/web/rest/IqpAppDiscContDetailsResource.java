/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAppDiscContDetails;
import cn.com.yusys.yusp.service.IqpAppDiscContDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpAppDiscContDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-07 09:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpappdisccontdetails")
public class IqpAppDiscContDetailsResource {
    @Autowired
    private IqpAppDiscContDetailsService iqpAppDiscContDetailsService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAppDiscContDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAppDiscContDetails> list = iqpAppDiscContDetailsService.selectAll(queryModel);
        return new ResultDto<List<IqpAppDiscContDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAppDiscContDetails>> index(QueryModel queryModel) {
        List<IqpAppDiscContDetails> list = iqpAppDiscContDetailsService.selectByModel(queryModel);
        return new ResultDto<List<IqpAppDiscContDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAppDiscContDetails> show(@PathVariable("pkId") String pkId) {
        IqpAppDiscContDetails iqpAppDiscContDetails = iqpAppDiscContDetailsService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAppDiscContDetails>(iqpAppDiscContDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAppDiscContDetails> create(@RequestBody IqpAppDiscContDetails iqpAppDiscContDetails) throws URISyntaxException {
        iqpAppDiscContDetailsService.insert(iqpAppDiscContDetails);
        return new ResultDto<IqpAppDiscContDetails>(iqpAppDiscContDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAppDiscContDetails iqpAppDiscContDetails) throws URISyntaxException {
        int result = iqpAppDiscContDetailsService.update(iqpAppDiscContDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAppDiscContDetailsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAppDiscContDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
