/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgAssistantDetailInfo;
import cn.com.yusys.yusp.repository.mapper.CfgAssistantDetailInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgAssistantDetailInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-28 15:47:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgAssistantDetailInfoService {

    @Autowired
    private CfgAssistantDetailInfoMapper cfgAssistantDetailInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgAssistantDetailInfo selectByPrimaryKey(String pk1) {
        return cfgAssistantDetailInfoMapper.selectByPrimaryKey(pk1);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgAssistantDetailInfo> selectAll(QueryModel model) {
        List<CfgAssistantDetailInfo> records = (List<CfgAssistantDetailInfo>) cfgAssistantDetailInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgAssistantDetailInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgAssistantDetailInfo> list = cfgAssistantDetailInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgAssistantDetailInfo record) {
        //避免重复添加
        String bizType = record.getBizType();
        String serno = record.getSerno();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        queryModel.addCondition("bizType",bizType);
        List<CfgAssistantDetailInfo> list = cfgAssistantDetailInfoMapper.selectByModel(queryModel);
        if(list.size() > 0 ){
            throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key,"\"请勿重复新增\"" + EcsEnum.E_SAVE_FAIL.value);
        }

        return cfgAssistantDetailInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgAssistantDetailInfo record) {
        return cfgAssistantDetailInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgAssistantDetailInfo record) {
        return cfgAssistantDetailInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgAssistantDetailInfo record) {
        return cfgAssistantDetailInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgAssistantDetailInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgAssistantDetailInfoMapper.deleteByIds(ids);
    }


    //批量更新
    public long dealList(List<CfgAssistantDetailInfo> lists){
        return lists.parallelStream().map(cfgAssistantDetailInfoMapper::updateByPrimaryKeySelective).count();
    }
}
