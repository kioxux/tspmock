package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCloestInfo
 * @类描述: lmt_guare_cloest_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 20:01:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGuareCloestInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 楼盘名称 **/
	private String buildingName;
	
	/** 楼栋 **/
	private String building;
	
	/** 楼层 **/
	private String floor;
	
	/** 房间号 **/
	private String roomNo;
	
	/** 估价 **/
	private java.math.BigDecimal assEvaAmt;
	
	/** 总价 **/
	private java.math.BigDecimal totalAmt;
	
	/** 面积 **/
	private java.math.BigDecimal squ;
	
	/** 地址 **/
	private String addr;
	
	/** 查询人员 **/
	private String qryUser;
	
	/** 查询日期 **/
	private String qryDate;
	
	/** 查询人员工号 **/
	private String qryId;
	
	/** 电话 **/
	private String phone;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName == null ? null : buildingName.trim();
	}
	
    /**
     * @return BuildingName
     */	
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param building
	 */
	public void setBuilding(String building) {
		this.building = building == null ? null : building.trim();
	}
	
    /**
     * @return Building
     */	
	public String getBuilding() {
		return this.building;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor == null ? null : floor.trim();
	}
	
    /**
     * @return Floor
     */	
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param roomNo
	 */
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo == null ? null : roomNo.trim();
	}
	
    /**
     * @return RoomNo
     */	
	public String getRoomNo() {
		return this.roomNo;
	}
	
	/**
	 * @param assEvaAmt
	 */
	public void setAssEvaAmt(java.math.BigDecimal assEvaAmt) {
		this.assEvaAmt = assEvaAmt;
	}
	
    /**
     * @return AssEvaAmt
     */	
	public java.math.BigDecimal getAssEvaAmt() {
		return this.assEvaAmt;
	}
	
	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	
    /**
     * @return TotalAmt
     */	
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return Squ
     */	
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr == null ? null : addr.trim();
	}
	
    /**
     * @return Addr
     */	
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param qryUser
	 */
	public void setQryUser(String qryUser) {
		this.qryUser = qryUser == null ? null : qryUser.trim();
	}
	
    /**
     * @return QryUser
     */	
	public String getQryUser() {
		return this.qryUser;
	}
	
	/**
	 * @param qryDate
	 */
	public void setQryDate(String qryDate) {
		this.qryDate = qryDate == null ? null : qryDate.trim();
	}
	
    /**
     * @return QryDate
     */	
	public String getQryDate() {
		return this.qryDate;
	}
	
	/**
	 * @param qryId
	 */
	public void setQryId(String qryId) {
		this.qryId = qryId == null ? null : qryId.trim();
	}
	
    /**
     * @return QryId
     */	
	public String getQryId() {
		return this.qryId;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}