/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageLogoutApp
 * @类描述: guar_mortgage_logout_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-22 09:44:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_mortgage_logout_app")
public class GuarMortgageLogoutApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 40)
	private String guarTypeCd;
	
	/** 抵质押物名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;
	
	/** 押品所有人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 押品所有人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 担保合同类型 **/
	@Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String guarContType;
	
	/** 本次担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 20)
	private String appDate;
	
	/** 登记原因 **/
	@Column(name = "REG_REASON", unique = false, nullable = true, length = 40)
	private String regReason;
	
	/** 登记机构类型 **/
	@Column(name = "REG_ORG_TYPE", unique = false, nullable = true, length = 10)
	private String regOrgType;
	
	/** 抵质押登记机构名称 **/
	@Column(name = "REG_ORG_NAME", unique = false, nullable = true, length = 256)
	private String regOrgName;
	
	/** 抵质押登记价值 **/
	@Column(name = "REG_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regValue;
	
	/** 抵质押登记日期 **/
	@Column(name = "REG_DATE", unique = false, nullable = true, length = 10)
	private String regDate;
	
	/** 抵质押登记失效日期 **/
	@Column(name = "REG_INVALID_DATE", unique = false, nullable = true, length = 10)
	private String regInvalidDate;
	
	/** 是否在线办理抵押 **/
	@Column(name = "IS_REG_ONLINE", unique = false, nullable = true, length = 5)
	private String isRegOnline;
	
	/** 是否预抵押 **/
	@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
	private String beforehandInd;
	
	/** 预抵押登记日期 **/
	@Column(name = "BEFOREHAND_REG_START_DATE", unique = false, nullable = true, length = 10)
	private String beforehandRegStartDate;
	
	/** 预抵押登记失效日期 **/
	@Column(name = "BEFOREHAND_REG_END_DATE", unique = false, nullable = true, length = 10)
	private String beforehandRegEndDate;
	
	/** 预登记方式 **/
	@Column(name = "REG_WAY", unique = false, nullable = true, length = 10)
	private String regWay;
	
	/** 预计办理正式登记日期 **/
	@Column(name = "FORE_REG_DATE", unique = false, nullable = true, length = 10)
	private String foreRegDate;
	
	/** 放款模式 **/
	@Column(name = "LOAN_MODE", unique = false, nullable = true, length = 5)
	private String loanMode;
	
	/** 登记办理状态 **/
	@Column(name = "REG_STATUS", unique = false, nullable = true, length = 10)
	private String regStatus;
	
	/** 收件收据编号/合同备案号/其他 **/
	@Column(name = "VALUE1", unique = false, nullable = true, length = 30)
	private String value1;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 40)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}
	
    /**
     * @return guarContType
     */
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param regReason
	 */
	public void setRegReason(String regReason) {
		this.regReason = regReason;
	}
	
    /**
     * @return regReason
     */
	public String getRegReason() {
		return this.regReason;
	}
	
	/**
	 * @param regOrgType
	 */
	public void setRegOrgType(String regOrgType) {
		this.regOrgType = regOrgType;
	}
	
    /**
     * @return regOrgType
     */
	public String getRegOrgType() {
		return this.regOrgType;
	}
	
	/**
	 * @param regOrgName
	 */
	public void setRegOrgName(String regOrgName) {
		this.regOrgName = regOrgName;
	}
	
    /**
     * @return regOrgName
     */
	public String getRegOrgName() {
		return this.regOrgName;
	}
	
	/**
	 * @param regValue
	 */
	public void setRegValue(java.math.BigDecimal regValue) {
		this.regValue = regValue;
	}
	
    /**
     * @return regValue
     */
	public java.math.BigDecimal getRegValue() {
		return this.regValue;
	}
	
	/**
	 * @param regDate
	 */
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	
    /**
     * @return regDate
     */
	public String getRegDate() {
		return this.regDate;
	}
	
	/**
	 * @param regInvalidDate
	 */
	public void setRegInvalidDate(String regInvalidDate) {
		this.regInvalidDate = regInvalidDate;
	}
	
    /**
     * @return regInvalidDate
     */
	public String getRegInvalidDate() {
		return this.regInvalidDate;
	}
	
	/**
	 * @param isRegOnline
	 */
	public void setIsRegOnline(String isRegOnline) {
		this.isRegOnline = isRegOnline;
	}
	
    /**
     * @return isRegOnline
     */
	public String getIsRegOnline() {
		return this.isRegOnline;
	}
	
	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}
	
    /**
     * @return beforehandInd
     */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}
	
	/**
	 * @param beforehandRegStartDate
	 */
	public void setBeforehandRegStartDate(String beforehandRegStartDate) {
		this.beforehandRegStartDate = beforehandRegStartDate;
	}
	
    /**
     * @return beforehandRegStartDate
     */
	public String getBeforehandRegStartDate() {
		return this.beforehandRegStartDate;
	}
	
	/**
	 * @param beforehandRegEndDate
	 */
	public void setBeforehandRegEndDate(String beforehandRegEndDate) {
		this.beforehandRegEndDate = beforehandRegEndDate;
	}
	
    /**
     * @return beforehandRegEndDate
     */
	public String getBeforehandRegEndDate() {
		return this.beforehandRegEndDate;
	}
	
	/**
	 * @param regWay
	 */
	public void setRegWay(String regWay) {
		this.regWay = regWay;
	}
	
    /**
     * @return regWay
     */
	public String getRegWay() {
		return this.regWay;
	}
	
	/**
	 * @param foreRegDate
	 */
	public void setForeRegDate(String foreRegDate) {
		this.foreRegDate = foreRegDate;
	}
	
    /**
     * @return foreRegDate
     */
	public String getForeRegDate() {
		return this.foreRegDate;
	}
	
	/**
	 * @param loanMode
	 */
	public void setLoanMode(String loanMode) {
		this.loanMode = loanMode;
	}
	
    /**
     * @return loanMode
     */
	public String getLoanMode() {
		return this.loanMode;
	}
	
	/**
	 * @param regStatus
	 */
	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}
	
    /**
     * @return regStatus
     */
	public String getRegStatus() {
		return this.regStatus;
	}
	
	/**
	 * @param value1
	 */
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	
    /**
     * @return value1
     */
	public String getValue1() {
		return this.value1;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}