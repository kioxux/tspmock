package cn.com.yusys.yusp.web.server.xdsx0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0022.req.Xdsx0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.resp.Xdsx0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0022.Xdsx0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 接口处理类:推送苏州地方征信给信贷
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0022:推送苏州地方征信给信贷")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0022Resource {

    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0022Resource.class);
    @Autowired
    private Xdsx0022Service xdsx0022Service;

    /**
     * 交易码：xdsx0022
     * 交易描述：推送苏州地方征信给信贷
     *
     * @param xdsx0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送苏州地方征信给信贷")
    @PostMapping("/xdsx0022")
    protected @ResponseBody
    ResultDto<Xdsx0022DataRespDto> xdsx0022(@Validated @RequestBody Xdsx0022DataReqDto xdsx0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key,
                DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022DataReqDto));
        Xdsx0022DataRespDto xdsx0022DataRespDto = new Xdsx0022DataRespDto();// 响应Dto:推送苏州地方征信给信贷
        ResultDto<Xdsx0022DataRespDto> xdsx0022DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0022DataReqDto获取业务值进行业务逻辑处理
            String connRecordId = xdsx0022DataReqDto.getConnRecordId();//对接记录ID
            String conName = xdsx0022DataReqDto.getConName();//企业名称
            String conCode = xdsx0022DataReqDto.getConCode();//企业代码
            String initBrId = xdsx0022DataReqDto.getInitBrId();//具体发起机构
            BigDecimal expectCrdLmt = xdsx0022DataReqDto.getExpectCrdLmt();//借款人预期借款额度
            String bizType = xdsx0022DataReqDto.getBizType();//具体品种
            String crdTerm = xdsx0022DataReqDto.getCrdTerm();//授信期限
            String taxEval = xdsx0022DataReqDto.getTaxEval();//纳税评级
            String creditScroe = xdsx0022DataReqDto.getCreditScroe();//征信报告信用评分
            BigDecimal szObankLoanBal = xdsx0022DataReqDto.getSzObankLoanBal();//信用贷款用信余额
            String isBlklsOrGreyls = xdsx0022DataReqDto.getIsBlklsOrGreyls();//是否黑灰名单客户
            String isSzTaxDisplay = xdsx0022DataReqDto.getIsSzTaxDisplay();//苏州地方征信是否显示纳税信息
            BigDecimal poiorTaxAmt = xdsx0022DataReqDto.getPoiorTaxAmt();//上年度纳税金额
            String isRent = xdsx0022DataReqDto.getIsRent();//是否租用
            String isOwn = xdsx0022DataReqDto.getIsOwn();//是否自有
            String isHavingProp = xdsx0022DataReqDto.getIsHavingProp();//是否具备自有产权证
            String szCreditScore = xdsx0022DataReqDto.getSzCreditScore();//企业苏州地方征信信用情况
            BigDecimal bsRate = xdsx0022DataReqDto.getBsRate();//资产负债率
            BigDecimal depositSaleRate = xdsx0022DataReqDto.getDepositSaleRate();//企业存量贷款与销售收入比率
            String patentInfo = xdsx0022DataReqDto.getPatentInfo();//专利信息情况
            String profitGrowInfo = xdsx0022DataReqDto.getProfitGrowInfo();//利润增长情况
            String controlerOpYear = xdsx0022DataReqDto.getControlerOpYear();//实际控制人从事本行业年限
            String conTrade = xdsx0022DataReqDto.getConTrade();//企业所属行业
            String providSsPayInfo = xdsx0022DataReqDto.getProvidSsPayInfo();//公积金、社保缴纳情况
            String utilitiesPayInfo = xdsx0022DataReqDto.getUtilitiesPayInfo();//水电气费缴纳情况
            String reprSettleInfo = xdsx0022DataReqDto.getReprSettleInfo();//法人或实际控制人落户情况
            BigDecimal totalScore = xdsx0022DataReqDto.getTotalScore();//总分值
            String isAdmit = xdsx0022DataReqDto.getIsAdmit();//是否准入（自动测算）
            BigDecimal maxPrecrdAmt = xdsx0022DataReqDto.getMaxPrecrdAmt();//最大预授信额度
            xdsx0022DataRespDto = xdsx0022Service.xdsx0022(xdsx0022DataReqDto);
            // 封装xdsx0022DataResultDto中正确的返回码和返回信息
            xdsx0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, e.getMessage());
            // 封装xdsx0022DataResultDto中异常返回码和返回信息
            xdsx0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0022DataRespDto到xdsx0022DataResultDto中
        xdsx0022DataResultDto.setData(xdsx0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value,
                JSON.toJSONString(xdsx0022DataRespDto));
        return xdsx0022DataResultDto;
    }
}
