package cn.com.yusys.yusp.service.client.bsp.ypxt.contra;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/28 14:21
 * @desc 押品与担保合同关系同步
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class ContraService {
    private static final Logger logger = LoggerFactory.getLogger(ContraService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * @param contraReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.ypxt.contra.resp.contraRespDto
     * @author 王玉坤
     * @date 2021/6/28 23:42
     * @version 1.0.0
     * @desc 押品与担保合同关系同步
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ContraRespDto contra(ContraReqDto contraReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value, JSON.toJSONString(contraReqDto));
        ResultDto<ContraRespDto> contra2ResultDto = dscms2YpxtClientService.contra(contraReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value, JSON.toJSONString(contra2ResultDto));

        String contraCode = Optional.ofNullable(contra2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String contraMeesage = Optional.ofNullable(contra2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ContraRespDto contraRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, contra2ResultDto.getCode())) {
            //  获取相关的值并解析
            contraRespDto = contra2ResultDto.getData();
        } else {//未查询到相关信息
            contra2ResultDto.setCode(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value);
        return contraRespDto;
    }
}
