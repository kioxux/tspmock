/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req.Fkpj35ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.resp.Xdzc0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.AsplIoPoolDetailsMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.pqc.math.linearalgebra.BigEndianConversions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.AsplIoPoolMapper;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 21:04:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplIoPoolService {

    private static final Logger logger = LoggerFactory.getLogger(AsplIoPoolService.class);
    @Resource
    private AsplIoPoolMapper asplIoPoolMapper;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplIoPool selectByPrimaryKey(String pkId) {
        return asplIoPoolMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplIoPool> selectAll(QueryModel model) {
        List<AsplIoPool> records = (List<AsplIoPool>) asplIoPoolMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplIoPool> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplIoPool> list = asplIoPoolMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplIoPool record) {
        return asplIoPoolMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplIoPool record) {
        return asplIoPoolMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplIoPool record) {
        return asplIoPoolMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplIoPool record) {
        return asplIoPoolMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplIoPoolMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplIoPoolMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertAsplIoPoolList(@Param("list")List<AsplIoPool> list) {
        return asplIoPoolMapper.insertAsplIoPoolList(list);
    }

    /**
     * @方法名称: inoutPoolList
     * @方法描述: 查询列表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplIoPool> inoutPoolList(QueryModel model) {
        return asplIoPoolMapper.selectByModel(model);
    }

    /**
     * @方法名称: queryAsplIoPoolByParams
     * @方法描述: 根据入参查询出入池申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public AsplIoPool queryAsplIoPoolByParams(Map queryMap) {
        return asplIoPoolMapper.queryAsplIoPoolByParams(queryMap);
    }

    /**
     * @方法名称: updateApproveStatusAfterFlow
     * @方法描述: 更新状态信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-06-15 15:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public int updateStatusBySerno(String serno, String wfStatus) {
        // 流程结束后 更新审批状态
        return asplIoPoolMapper.updateStatusBySerno(serno,wfStatus);
    }


    /**
     * 新增一笔出池记录
     * @param map
     * @return
     */
    @Transactional
    public ResultDto<AsplIoPool> addOutPoollist(Map map) {
        String returnCode = "0";
        String returnMsg = "出池申请新增成功";
        AsplIoPool asplIoPool = new AsplIoPool();
        try{
            // 校验当前用户是否存在在途的出池记录（审批状态为 000，111）
            String cusId = (String)map.get("cusId");
            String contNo = (String) map.get("contNo");
            QueryModel model = new QueryModel();
            model.addCondition("apprStatusList","000,111");
            model.addCondition("cusId",cusId);
            model.addCondition("contNo",contNo);
            List<AsplIoPool> list = selectAll(model);
            if(CollectionUtils.nonEmpty(list)){
                returnCode = "999";
                returnMsg = "当前用户存在在途出池申请业务";
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, returnMsg);
            }
            // 申请出池流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
            asplIoPool.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
            asplIoPool.setSerno(serno);//业务流水号
            asplIoPool.setContNo(ctrAsplDetails.getContNo());//合同编号
            asplIoPool.setInoutType(CmisCommonConstants.INOUT_TYPE_0);// 出入类型 （出池）
            asplIoPool.setCusId(ctrAsplDetails.getCusId());// 客户编号
            asplIoPool.setCusName(ctrAsplDetails.getCusName());// 客户名称
            asplIoPool.setLmtAmt(BigDecimal.ZERO);//授信额度
            asplIoPool.setOutstndAmt(BigDecimal.ZERO);//todo 已用额度
            asplIoPool.setStartDate(ctrAsplDetails.getStartDate());//起始日期
            asplIoPool.setEndDate(ctrAsplDetails.getEndDate());//到期日期
            asplIoPool.setResn(StringUtils.EMPTY);//原因
            asplIoPool.setApprStatus(CmisCommonConstants.WF_STATUS_000);//审批状态(特殊出池需要走审批)
            asplIoPool.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            asplIoPool.setInputId(ctrAsplDetails.getInputId());//登记人
            asplIoPool.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
            asplIoPool.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
            asplIoPool.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
            asplIoPool.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
            asplIoPool.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
            asplIoPool.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
            asplIoPool.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
            asplIoPool.setCreateTime(DateUtils.getCurrTimestamp());//创建时间
            asplIoPool.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
            insertSelective(asplIoPool);
        }catch (Exception e){
            logger.error("池申请新增失败！",e);
            returnCode = "9999";
            returnMsg = e.getMessage();
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }finally{
            return new ResultDto<AsplIoPool>(asplIoPool).code(returnCode).message(returnMsg);
        }
    }

    /**
     * 特殊出池审批
     * @param serno
     */
    public void handleBusinessDataAfterEnd(String serno) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_FKPJ35.key, DscmsEnum.TRADE_CODE_FKPJ35.value);
        try {
            // 根据出池流水号查询出池记录
            AsplIoPool asplIoPool = asplIoPoolMapper.queryAsplIoPoolBySerno(serno);
            // 出池客户编号
            String cusId = asplIoPool.getCusId();
            // 出池明细
            List<AsplIoPoolDetails> asplIoPoolDetailsList = asplIoPoolDetailsService.selectBySerno(serno);
            // 后期这边要加判断 是不是银票
            for(AsplIoPoolDetails asplIoPoolDetails:asplIoPoolDetailsList){
                Fkpj35ReqDto fkpj35ReqDto = new Fkpj35ReqDto();
                fkpj35ReqDto.setBillno(asplIoPoolDetails.getAssetNo());
                fkpj35ReqDto.setCusid(cusId);
                fkpj35ReqDto.setsBatchNo(serno);
                ResultDto<Fkpj35ReqDto> resultDto = dscms2PjxtClientService.fkpj35(fkpj35ReqDto);
                logger.info("票据承兑签发审批请求【fkpj35】："+ Objects.toString(resultDto));
                if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
                    logger.error("资产池:出池流水【{}】票据系统出异常，资产编号:【{}】",serno,asplIoPoolDetails.getAssetNo());
                }
            }
        } catch (BizException e) {
            throw BizException.error(null, "9999", e.getMessage());
        }  catch (Exception e) {
            throw BizException.error(null, "9999", e.getMessage());
        }
    }
    /**
     * 承兑行白名单额度恢复
     * @param
     * @return
     */
    public void sendCmisLmt0025(List<AsplAssetsList> asplAssetsLists, CtrAsplDetails ctrAsplDetails){
        List<CmisLmt0025OccRelListReqDto> occRelList = new ArrayList<CmisLmt0025OccRelListReqDto>();
        CmisLmt0025OccRelListReqDto dealBiz = new CmisLmt0025OccRelListReqDto();
        asplAssetsLists.forEach(e->{
            dealBiz.setDealBizNo(e.getAssetNo());//交易业务编号
            occRelList.add(dealBiz);
        });
        CmisLmt0025ReqDto cmisLmt0025ReqDto = new CmisLmt0025ReqDto();
        cmisLmt0025ReqDto.setOccRelList(occRelList);
        // 系统编号
        cmisLmt0025ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);
        // 金融机构代码
        cmisLmt0025ReqDto.setInstuCde("001");
        // 合同编号
        // 合同编号
        cmisLmt0025ReqDto.setInputId(ctrAsplDetails.getManagerId());
        cmisLmt0025ReqDto.setInputBrId(ctrAsplDetails.getManagerBrId());
        cmisLmt0025ReqDto.setInputDate(DateUtils.getCurrDateStr());

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025ReqDto));
        ResultDto<CmisLmt0025RespDto> cmisLmt0025RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0025(cmisLmt0025ReqDto)).orElse(new ResultDto<>());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025RespDtoResultDto));
        if (Objects.equals(cmisLmt0025RespDtoResultDto.getCode(), "0")) {
            if(!Objects.equals(cmisLmt0025RespDtoResultDto.getData().getErrorCode(), "0000")){
                throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getData().getErrorMsg());
            }
        }else{
            throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getMessage());
        }
    }
    /**
     *  生成Co3202ReqDto对象(资产池 解质押-chuku )
     * */
    public Co3202ReqDto getCo3202ReqDto(GuarBaseInfo guarBaseInfo, AsplAssetsList asplAssetsList){
        //是否票据池业务
        Co3202ReqDto co3202ReqDto = new Co3202ReqDto();
        co3202ReqDto.setDaikczbz("1");//业务操作标志（资产池）
        co3202ReqDto.setDzywbhao(guarBaseInfo.getGuarNo());//抵质押物编号
        co3202ReqDto.setDzywminc(guarBaseInfo.getPldimnMemo());//抵质押物名称
        co3202ReqDto.setDizyfshi("2");//抵质押方式
        co3202ReqDto.setChrkleix("2");//出入库类型
        co3202ReqDto.setSyqrkehh(guarBaseInfo.getGuarCusId());//所有权人客户号
        co3202ReqDto.setSyqrkehm(guarBaseInfo.getGuarCusName());//所有权人客户名
        co3202ReqDto.setRuzjigou(guarBaseInfo.getFinaBrId());//入账机构
        co3202ReqDto.setHuobdhao(toConverCurrency(guarBaseInfo.getCurType()));//货币代号
        co3202ReqDto.setMinyjiaz(guarBaseInfo.getConfirmAmt());//名义价值
        co3202ReqDto.setShijjiaz(guarBaseInfo.getConfirmAmt());//实际价值
        co3202ReqDto.setDizybilv(guarBaseInfo.getMortagageRate()); //抵质押比率
        co3202ReqDto.setKeyongje(null);//可用金额
        co3202ReqDto.setShengxrq(guarBaseInfo.getInputDate().replace("-", "").substring(0,8));//生效日期
        // TODO
        co3202ReqDto.setDaoqriqi(asplAssetsList.getAssetEndDate().replace("-", "").substring(0,8));// 到期日期
        co3202ReqDto.setDzywztai("2");//抵质押物状态
        co3202ReqDto.setZhaiyoms(""); //摘要
        return co3202ReqDto;
    }

    /*
     * 新核心改造 币种映射 信贷--->核心
     * @param xdbz 信贷币种码值
     * @return hxbz 核心币种码值
     */
    public static String toConverCurrency(String xdbz){
        String hxbz = "";
        if("CNY".equals(xdbz)){
            hxbz = "01";
        }else if("MOP".equals(xdbz)){//澳门币
            hxbz = "81";
        }else if("CAD".equals(xdbz)){//加元
            hxbz = "28";
        }else if("CHF".equals(xdbz)){//瑞士法郎
            hxbz = "15";
        }else if("JPY".equals(xdbz)){//日元
            hxbz = "27";
        }else if("EUR".equals(xdbz)){//欧元
            hxbz = "38";
        }else if("GBP".equals(xdbz)){//英镑
            hxbz = "12";
        }else if("HKD".equals(xdbz)){//港币
            hxbz = "13";
        }else if("AUD".equals(xdbz)){//澳元
            hxbz = "29";
        }else if("USD".equals(xdbz)){//美元
            hxbz = "14";
        }else if("SGD".equals(xdbz)){//新加坡元
            hxbz = "18";
        }else if("SEK".equals(xdbz)){//瑞典克郎
            hxbz = "21";
        }else if("DKK".equals(xdbz)){//丹麦克朗
            hxbz = "22";
        }else if("NOK".equals(xdbz)){//挪威克朗
            hxbz = "23";
        }else{
            hxbz = xdbz;//未匹配到的币种发信贷的币种过去（DEM 德国马克;MSD 克鲁赛罗;NLG 荷兰盾;BEF 比利时法郎;ITL 意大利里拉;FRF 法国法郎;ATS 奥地利先令;FIM 芬兰马克）
        }
        return hxbz;
    }

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    public AsplIoPool queryAsplIoPoolBySerno(String serno) {
        return asplIoPoolMapper.queryAsplIoPoolBySerno(serno);
    }

    /**
     * @函数名称:delteoutpoollist
     * @函数描述:删除一笔出池记录
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional
    public ResultDto<AsplIoPool> deleteOutpoollist(Map map) {
        String returnCode = "0";
        String returnMsg = "出池申请删除成功";
        try{
            String serno = (String) map.get("serno");
            AsplIoPool asplIoPool = queryAsplIoPoolBySerno(serno);
            if(Objects.equals(CmisCommonConstants.WF_STATUS_992,asplIoPool.getApprStatus())){
                // 修改状态为自行退出
                updateStatusBySerno(serno,CmisCommonConstants.WF_STATUS_996);
                workflowCoreClient.deleteByBizId(serno);
            }else{
                // 删除出池记录和出池记录明细
                deleteOutpoolBySerno(serno);
                asplIoPoolDetailsService.deleteOutpoolDetailsBySerno(serno);
            }
        }catch (Exception e){
            logger.error("出池申请删除异常！",e);
            returnCode = "9999";
            returnMsg = "出池申请删除异常";
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }finally{
            return new ResultDto<AsplIoPool>().code(returnCode).message(returnMsg);
        }
    }

    /**
     * 根据流水号删除
     * @param serno
     * @return
     */
    private int deleteOutpoolBySerno(String serno) {
        return asplIoPoolMapper.deleteBySerno(serno);
    }

    /**
     * 根据流水号更新批总金额
     * @param serno
     * @param lmtAmt
     * @return
     */
    public int updateLmtAmtBySerno(String serno, BigDecimal lmtAmt) {
        Map<String,Object> map = new HashMap<>();
        map.put("serno",serno);
        map.put("lmtAmt",lmtAmt);
        return asplIoPoolMapper.updateLmtAmtBySerno(map);
    }

    /**
     * 保存出池原因
     * @param map
     * @return
     */
    public ResultDto<Integer> saveAsplIoPool(Map map) {
        String returnCode = "0";
        String returnMsg = "出池申请保存成功";
        try{
            String resn = (String) map.get("resn");
            asplIoPoolMapper.saveAsplIoPoolresn(map);
        }catch (Exception e){
            logger.error("出池申请保存异常！",e);
            returnCode = "9999";
            returnMsg = "出池申请保存异常";
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }finally{
            return new ResultDto<AsplIoPool>().code(returnCode).message(returnMsg);
        }
    }
}
