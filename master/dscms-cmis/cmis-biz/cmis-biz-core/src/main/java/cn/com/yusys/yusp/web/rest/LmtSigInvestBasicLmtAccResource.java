/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLmtAcc;
import cn.com.yusys.yusp.service.LmtSigInvestBasicLmtAccService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:24:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasiclmtacc")
public class LmtSigInvestBasicLmtAccResource {
    @Autowired
    private LmtSigInvestBasicLmtAccService lmtSigInvestBasicLmtAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicLmtAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicLmtAcc> list = lmtSigInvestBasicLmtAccService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLmtAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicLmtAcc>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicLmtAcc> list = lmtSigInvestBasicLmtAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLmtAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicLmtAcc> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc = lmtSigInvestBasicLmtAccService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicLmtAcc>(lmtSigInvestBasicLmtAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicLmtAcc> create(@RequestBody LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc) throws URISyntaxException {
        lmtSigInvestBasicLmtAccService.insert(lmtSigInvestBasicLmtAcc);
        return new ResultDto<LmtSigInvestBasicLmtAcc>(lmtSigInvestBasicLmtAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc) throws URISyntaxException {
        int result = lmtSigInvestBasicLmtAccService.update(lmtSigInvestBasicLmtAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicLmtAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicLmtAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
