package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpSehandEsInfo
 * @类描述: pvp_sehand_es_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-09-01 22:15:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpSehandEsInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 放款流水号 **/
	private String pvpSerno;
	
	/** 托管协议编号 **/
	private String tgxyNo;
	
	/** 证件类型 **/
	private String certType;
	
	/** 买方身份证号码 **/
	private String buyerCertCode;
	
	/** 买方姓名 **/
	private String buyerName;
	
	/** 贷款金额 **/
	private java.math.BigDecimal applyAmount;
	
	/** 资金性质 **/
	private String capitalType;
	
	/** 核心交易流水 **/
	private String coreTranSerno;
	
	/** 发送状态 **/
	private String sendStatus;
	
	/** 监管账号 **/
	private String acctNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param tgxyNo
	 */
	public void setTgxyNo(String tgxyNo) {
		this.tgxyNo = tgxyNo == null ? null : tgxyNo.trim();
	}
	
    /**
     * @return TgxyNo
     */	
	public String getTgxyNo() {
		return this.tgxyNo;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param buyerCertCode
	 */
	public void setBuyerCertCode(String buyerCertCode) {
		this.buyerCertCode = buyerCertCode == null ? null : buyerCertCode.trim();
	}
	
    /**
     * @return BuyerCertCode
     */	
	public String getBuyerCertCode() {
		return this.buyerCertCode;
	}
	
	/**
	 * @param buyerName
	 */
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName == null ? null : buyerName.trim();
	}
	
    /**
     * @return BuyerName
     */	
	public String getBuyerName() {
		return this.buyerName;
	}
	
	/**
	 * @param applyAmount
	 */
	public void setApplyAmount(java.math.BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}
	
    /**
     * @return ApplyAmount
     */	
	public java.math.BigDecimal getApplyAmount() {
		return this.applyAmount;
	}
	
	/**
	 * @param capitalType
	 */
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType == null ? null : capitalType.trim();
	}
	
    /**
     * @return CapitalType
     */	
	public String getCapitalType() {
		return this.capitalType;
	}
	
	/**
	 * @param coreTranSerno
	 */
	public void setCoreTranSerno(String coreTranSerno) {
		this.coreTranSerno = coreTranSerno == null ? null : coreTranSerno.trim();
	}
	
    /**
     * @return CoreTranSerno
     */	
	public String getCoreTranSerno() {
		return this.coreTranSerno;
	}
	
	/**
	 * @param sendStatus
	 */
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus == null ? null : sendStatus.trim();
	}
	
    /**
     * @return SendStatus
     */	
	public String getSendStatus() {
		return this.sendStatus;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}