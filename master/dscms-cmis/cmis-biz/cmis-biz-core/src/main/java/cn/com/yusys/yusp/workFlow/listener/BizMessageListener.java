package cn.com.yusys.yusp.workFlow.listener;


import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className BizMessageListener
 * @Description 流程mq监听处理
 * @Date 2021年6月21日14:22:56
 */
@Service
public class BizMessageListener {
    private final Logger log = LoggerFactory.getLogger(BizMessageListener.class);


    private ClientBizFactory clientBizFactory;

    @Autowired
    public BizMessageListener(ClientBizFactory clientBizFactory) {
        this.clientBizFactory = clientBizFactory;
    }

    @RabbitListener(queuesToDeclare = {
            @Queue("yusp-flow.DGSX01"),//对公授信申报审批流程
            @Queue("yusp-flow.DGSX02"),//集团额度调剂审批流程
            @Queue("yusp-flow.DGSX03"),//对公额度冻结、解冻、终止审批流程
            @Queue("yusp-flow.DGSX04"),//对公授信批复变更审批流程
            @Queue("yusp-flow.DGSX05"),//集团成员授信申报审核子流程
            @Queue("yusp-flow.DGSX06"),//对公内评低准入例外审批流程

            @Queue("yusp-flow.DGYX01"),//一般合同申请审批流程
            @Queue("yusp-flow.DGYX02"),//贴现协议申请审批流程
            @Queue("yusp-flow.DGYX03"),//合同影像审核流程
            @Queue("yusp-flow.DGYX04"),//一般贷款出账审批流程
            @Queue("yusp-flow.DGYX05"),//银承出账审批流程
            @Queue("yusp-flow.DGYX06"),//合同影像审核流程 一般贴现协议

            @Queue("yusp-flow.XWYW01"),//小微授信申请审批流程（信贷端）
            @Queue("yusp-flow.XWYW02"),//小微放款申请审批流程-线下放款
            @Queue("yusp-flow.XWYW03"),//小微签章审批
            @Queue("yusp-flow.XWYW04"),//小微追缴还款冲正
            @Queue("yusp-flow.XWYW05"),//小微对公客户授信审批流程

            @Queue("yusp-flow.XKYW01"),//信用卡申请pc端流程
            @Queue("yusp-flow.XKYW02"),//信用卡申请移动端流程

            @Queue("yusp-flow.QTSX01"),//零售优惠利率申请审批流程
            @Queue("yusp-flow.QTSX02"),//对公人民币利率定价审批流程
            @Queue("yusp-flow.QTSX03"),//外币利率定价申请流程
            @Queue("yusp-flow.QTSX04"),//公司类授信业务备案审批通用流程
            @Queue("yusp-flow.QTSX05"),//用信审核备案审批流程
            @Queue("yusp-flow.QTSX06"),//授信抵押物价值认定审批流程
            @Queue("yusp-flow.QTSX07"),//小微人民币利率定价审批流程

            @Queue("yusp-flow.ZCYW01"),//资产出池审批流程
            @Queue("yusp-flow.ZCYW02"),//发票补录
            @Queue("yusp-flow.ZCYW03"),//购销合同
            @Queue("yusp-flow.ZCYW04"),//资产池协议审批流程 、协议变更

            @Queue("yusp-flow.XKYW03"),//信用卡调额申请审批流程-分支机构
            @Queue("yusp-flow.XKYW04"),//信用卡调额申请审批流程（总行部门）
            @Queue("yusp-flow.XKYW05"),//信用卡大额分期申请审批流程
            @Queue("yusp-flow.XKYW06"),//信用卡大额分期放款审批流程

            @Queue("yusp-flow.ZXGL01"),//人行征信查询审批流程（分支机构）
            @Queue("yusp-flow.ZXGL02"),//人行征信查询审批流程（小微条线）
            @Queue("yusp-flow.ZXGL03"),//苏州征信查询审批流程

            @Queue("yusp-flow.LSYW01"),//零售业务授信审批流程
            @Queue("yusp-flow.LSYW02"),//零售空白合同签订审核流程
            @Queue("yusp-flow.LSYW03"),//零售放款审核流程-空白合同模式
            @Queue("yusp-flow.LSYW04"),//零售放款审核流程-生成打印模式
            @Queue("yusp-flow.LSYW05"),//零售线上提款启用审核流程
            @Queue("yusp-flow.LSYW06"),//零售生成打印合同签订审核流程
            @Queue("yusp-flow.LSYW07"),//合同批复变更流程
            @Queue("yusp-flow.SGCZ07"),//零售授信申请审批流程（寿光）
            @Queue("yusp-flow.DHCZ07"),//零售授信申请审批流程（东海）
            @Queue("yusp-flow.HZXM01"),
            @Queue("yusp-flow.HZXM02"),//合作方_项目准入_变更审批流程-总行部门
            @Queue("yusp-flow.HZXM03"),
            @Queue("yusp-flow.HZXM04"),

            @Queue("yusp-flow.TYSX01"),
            @Queue("yusp-flow.TYSX02"),
            @Queue("yusp-flow.TYSX03"),
            @Queue("yusp-flow.TYSX04"),
            @Queue("yusp-flow.TYSX06"),

            @Queue("yusp-flow.DBGL01"),
            @Queue("yusp-flow.DBGL02"),
            @Queue("yusp-flow.DBGL03"),
            @Queue("yusp-flow.DBGL04"),
            @Queue("yusp-flow.DBGL05"),
            @Queue("yusp-flow.DBGL06"),
            @Queue("yusp-flow.DBGL07"),
            @Queue("yusp-flow.DBGL08"),
            @Queue("yusp-flow.DBGL09"),

            @Queue("yusp-flow.SGCZ16"),//权证出库审批流程-非诉讼借阅（寿光）
            @Queue("yusp-flow.SGCZ17"),//权证出库审批流程-诉讼借阅（寿光）
            @Queue("yusp-flow.DHCZ16"),//权证出库审批流程-非诉讼借阅（东海）
            @Queue("yusp-flow.DHCZ17"),//权证出库审批流程-诉讼借阅（东海）

            @Queue("yusp-flow.HZXM07"),//合作方名单退出审批流程-分支机构
            @Queue("yusp-flow.HZXM08"),//合作方名单退出审批流程-总行部门
            @Queue("yusp-flow.HZXM09"),//合作方保证金提取审批流程

            @Queue("yusp-flow.DAGL01"),
            @Queue("yusp-flow.DAGL02"),
            @Queue("yusp-flow.DAGL03"),
            @Queue("yusp-flow.DAGL04"),
            @Queue("yusp-flow.DAGL05"),
            @Queue("yusp-flow.DAGL06"),
            //业务变更
            @Queue("yusp-flow.BGYW01"),
            @Queue("yusp-flow.BGYW02"),
            @Queue("yusp-flow.BGYW03"),
            @Queue("yusp-flow.BGYW04"),
            @Queue("yusp-flow.BGYW05"),
            @Queue("yusp-flow.BGYW06"),
            @Queue("yusp-flow.BGYW07"),
            @Queue("yusp-flow.BGYW08"),
            @Queue("yusp-flow.BGYW09"),
            @Queue("yusp-flow.BGYW10"),
            @Queue("yusp-flow.BGYW11"),
            @Queue("yusp-flow.BGYW12"),
            @Queue("yusp-flow.BGYW13"),
            @Queue("yusp-flow.BGYW14"),
            @Queue("yusp-flow.BGYW15"),// 停息申请审批流程
            @Queue("yusp-flow.BGYW16"),// 取消停息申请审批流程
			
            @Queue("yusp-flow.DAGL08"),
            @Queue("yusp-flow.DAGL09"),
            @Queue("yusp-flow.DAGL10"),
            @Queue("yusp-flow.DAGL11"),
            @Queue("yusp-flow.DAGL12"),
            @Queue("yusp-flow.DAGL13"),
            @Queue("yusp-flow.DAGL14"),

            @Queue("yusp-flow.XTGL02"),//空白凭证修改作废审批流程
            @Queue("yusp-flow.DGYX07"),//房抵e点贷尽调结果录入流程
            @Queue("yusp-flow.DGYX09"),//房抵e点贷押品查询查封流程
            @Queue("yusp-flow.DGYX08"),//房抵e点贷授信押品关联流程
            @Queue("yusp-flow.DGYX10"),//房抵e点贷无还本续贷审核流程
            @Queue("yusp-flow.DGYX11"),//省心快贷线上提款查封查验流程
            @Queue("yusp-flow.DGYX12"),//省心快贷提款利率修改流程
            @Queue("yusp-flow.DGYX13"),//垫款申请审批流程

            @Queue("yusp-flow.SGCZ29"),// 利率定价流程-经营性及消费非按揭（寿光）
            @Queue("yusp-flow.SGCZ04"),// 对公授信申报审批流程（寿光）
            @Queue("yusp-flow.SGCZ05"),// 对公授信批复变更审批流程（寿光）
            @Queue("yusp-flow.SGCZ06"),// 对公出账申请审批流程（寿光）
            @Queue("yusp-flow.SGCZ08"),// 利率定价流程-一手房按揭贷款（寿光）
            @Queue("yusp-flow.SGCZ09"),// 利率定价流程-二手房按揭贷款（寿光）
            @Queue("yusp-flow.SGCZ10"),// 利率定价流程-展期（寿光）
            @Queue("yusp-flow.SGCZ11"),// 保证金存款特惠利率审批流程（寿光）
            @Queue("yusp-flow.SGCZ12"),// 授信抵押物价值认定审批流程（寿光）
            @Queue("yusp-flow.SGCZ13"),// 还款账号变更审批流程（寿光）
            @Queue("yusp-flow.SGCZ14"),// 还款计划变更审批流程（寿光）
            @Queue("yusp-flow.SGCZ15"),// 主动还款申请审批流程（寿光）
            @Queue("yusp-flow.SGCZ30"),// 合作方准入与变更审批流程-普惠金融部（寿光）
            @Queue("yusp-flow.SGCZ03"),// 合作方协议新增、变更、续签流程-分支机构（寿光）
            @Queue("yusp-flow.SGCZ31"),// 合作方协议新增、变更、续签流程-普惠金融部（寿光）
            @Queue("yusp-flow.SGCZ32"),// 合作方名单退出审批流程-分支机构（寿光）
            @Queue("yusp-flow.SGCZ33"),// 合作方名单退出审批流程-普惠金融部（寿光）
            @Queue("yusp-flow.SGCZ34"),// 合作方保证金提取审批流程-分支机构（寿光）
            @Queue("yusp-flow.SGCZ35"),// 合作方保证金提取审批流程-普惠金融部（寿光）
            @Queue("yusp-flow.SGCZ36"),// 合作方准入与变更审批流程-分支机构（寿光）
            @Queue("yusp-flow.SGCZ40"),// 贷款停息审批流程（寿光）
            @Queue("yusp-flow.SGCZ41"),// 贷款取消停息审批流程（寿光）

            @Queue("yusp-flow.DHCZ08"),// 利率定价流程-一手房按揭贷款（东海）
            @Queue("yusp-flow.DHCZ09"),// 利率定价流程-二手房按揭贷款（东海）
            @Queue("yusp-flow.DHCZ10"),// 利率定价流程-展期（东海）
            @Queue("yusp-flow.DHCZ11"),// 保证金存款特惠利率审批流程（东海）
            @Queue("yusp-flow.DHCZ12"),// 授信抵押物价值认定审批流程（东海）
            @Queue("yusp-flow.DHCZ13"),// 还款账号变更审批流程（东海）
            @Queue("yusp-flow.DHCZ14"),// 还款计划变更审批流程（东海）
            @Queue("yusp-flow.DHCZ15"),// 主动还款申请审批流程（东海）
            @Queue("yusp-flow.DHCZ29"),// 利率定价流程-经营性及消费非按揭（东海）
            @Queue("yusp-flow.DHCZ30"),// 合作方准入与变更审批流程-普惠金融部（东海）
            @Queue("yusp-flow.DHCZ03"),// 合作方协议新增、变更、续签流程-分支机构（东海）
            @Queue("yusp-flow.DHCZ31"),// 合作方协议新增、变更、续签流程-普惠金融部（东海）
            @Queue("yusp-flow.DHCZ32"),// 合作方名单退出审批流程-分支机构（东海）
            @Queue("yusp-flow.DHCZ33"),// 合作方名单退出审批流程-普惠金融部（东海）
            @Queue("yusp-flow.DHCZ34"),// 合作方保证金提取审批流程-分支机构（东海）
            @Queue("yusp-flow.DHCZ35"),// 合作方保证金提取审批流程-普惠金融部（东海）
            @Queue("yusp-flow.DHCZ36"),// 合作方准入与变更审批流程-分支机构-东海
            @Queue("yusp-flow.DHCZ37"),// 人行征信查询审批流程（东海）

            @Queue("yusp-flow.SGCZ18"),// 档案调阅/延期审批流程（寿光）
            @Queue("yusp-flow.SGCZ20"),// 档案销毁审批流程（寿光）
            @Queue("yusp-flow.SGCZ21"), // 影像补扫审批流程（寿光）
            @Queue("yusp-flow.DHCZ18"),// 档案调阅/延期审批流程（东海）
            @Queue("yusp-flow.DHCZ20"),// 档案销毁审批流程（东海）
            @Queue("yusp-flow.DHCZ21"), // 影像补扫审批流程（东海）

            @Queue("yusp-flow.DHCZ04"),// 对公授信申报审批流程（东海）
            @Queue("yusp-flow.DHCZ05"),// 对公授信批复变更审批流程（东海）
            @Queue("yusp-flow.DHCZ06"),// 对公出账申请审批流程（东海）
            @Queue("yusp-flow.DHCZ40"),// 贷款停息审批流程（东海）
            @Queue("yusp-flow.DHCZ41"),// 贷款取消停息审批流程（东海）

            @Queue("yusp-flow.XTGL03"),// 数据修改统计分类或科目投向审批流程（对公及零售）
            @Queue("yusp-flow.XTGL04"),// 数据修改统计分类或科目投向审批流程（小微）
            @Queue("yusp-flow.XTGL05"),// 数据修改统计分类或科目投向审批流程（网金）
            @Queue("yusp-flow.XTGL06"),// 数据修改其他信息审批流程（对公及零售）
            @Queue("yusp-flow.XTGL07"),// 数据修改其他信息审批流程（小微）
            @Queue("yusp-flow.XTGL08"),// 数据修改其他信息审批流程（小微）
            @Queue("yusp-flow.SGCZ42"),// 数据修改统计分类或科目投向审批流程（寿光）
            @Queue("yusp-flow.SGCZ43"),// 数据修改其他信息审批流程（寿光）
            @Queue("yusp-flow.DHCZ42"),// 数据修改统计分类或科目投向审批流程（东海）
            @Queue("yusp-flow.DHCZ43")// 数据修改其他信息审批流程（东海）

    })// 队列名称为【yusp-flow.流程申请类型】,可以添加多个
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) {
        log.info("biz服务客户端业务监听:" + message);
        clientBizFactory.processBiz(message);

    }
}



