/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubAppr;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubRst;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoSubRstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubRstService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:23:30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoSubRstService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoSubRstMapper lmtSigInvestBasicInfoSubRstMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSubRst selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubRstMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoSubRst> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoSubRst> records = (List<LmtSigInvestBasicInfoSubRst>) lmtSigInvestBasicInfoSubRstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtSigInvestBasicInfoSubRst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoSubRst> list = lmtSigInvestBasicInfoSubRstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoSubRst record) {
        return lmtSigInvestBasicInfoSubRstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoSubRst record) {
        return lmtSigInvestBasicInfoSubRstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoSubRst record) {
        return lmtSigInvestBasicInfoSubRstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoSubRst record) {
        return lmtSigInvestBasicInfoSubRstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubRstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoSubRstMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: initLmtSigInvestBasicInfoSubRstInfo
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSubRst initLmtSigInvestBasicInfoSubRstInfo(LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr) {
        //主键
        String pkValue = generatePkId();
        //初始化对象
        LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst = new LmtSigInvestBasicInfoSubRst() ;
        //数据库拷贝
        BeanUtils.copyProperties(sigInvestBasicInfoSubAppr, lmtSigInvestBasicInfoSubRst);
        //设置主键
        lmtSigInvestBasicInfoSubRst.setPkId(pkValue);
        lmtSigInvestBasicInfoSubRst.setBasicReplySerno(generateSerno(SeqConstant.INVEST_LMT_REPLY_SEQ));
        //最新更新日期
        lmtSigInvestBasicInfoSubRst.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicInfoSubRst.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicInfoSubRst.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestBasicInfoSubRst;
    }

    /**
     * 批复变更流程-生成批复记录
     * @param lmtSigInvestBasicInfoSub
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     */
    public void insertBasicInfoSubRst(LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub, String oldSerno, String replySerno, String currentOrgId, String currentUserId) {
        LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst = new LmtSigInvestBasicInfoSubRst();
        BeanUtils.copyProperties(lmtSigInvestBasicInfoSub,lmtSigInvestBasicInfoSubRst);
        lmtSigInvestBasicInfoSubRst.setSerno(oldSerno);
        lmtSigInvestBasicInfoSubRst.setReplySerno(replySerno);
        //初始化新增通用属性
        initInsertDomainProperties(lmtSigInvestBasicInfoSubRst,currentUserId,currentOrgId);
        insert(lmtSigInvestBasicInfoSubRst);
    }
}
