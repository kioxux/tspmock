package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocImageSpplInfo
 * @类描述: doc_image_sppl_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-07 16:15:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocImageSpplInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 影像补扫编号 **/
	private String disiSerno;
	
	/** 原业务流水号 **/
	private String bizSerno;
	
	/** 影像补扫类型 **/
	private String spplType;
	
	/** 任务状态 **/
	private String taskFlag;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 是否删除资料 **/
	private String isDelData;
	
	/** 删除内容 **/
	private String delContent;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 补扫业务品种 **/
	private String spplBizType;
	
	/** 原业务流程实例 **/
	private String bizInstanceId;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;
	
	/**
	 * @param disiSerno
	 */
	public void setDisiSerno(String disiSerno) {
		this.disiSerno = disiSerno == null ? null : disiSerno.trim();
	}
	
    /**
     * @return DisiSerno
     */	
	public String getDisiSerno() {
		return this.disiSerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param spplType
	 */
	public void setSpplType(String spplType) {
		this.spplType = spplType == null ? null : spplType.trim();
	}
	
    /**
     * @return SpplType
     */	
	public String getSpplType() {
		return this.spplType;
	}
	
	/**
	 * @param taskFlag
	 */
	public void setTaskFlag(String taskFlag) {
		this.taskFlag = taskFlag == null ? null : taskFlag.trim();
	}
	
    /**
     * @return TaskFlag
     */	
	public String getTaskFlag() {
		return this.taskFlag;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param isDelData
	 */
	public void setIsDelData(String isDelData) {
		this.isDelData = isDelData == null ? null : isDelData.trim();
	}
	
    /**
     * @return IsDelData
     */	
	public String getIsDelData() {
		return this.isDelData;
	}
	
	/**
	 * @param delContent
	 */
	public void setDelContent(String delContent) {
		this.delContent = delContent == null ? null : delContent.trim();
	}
	
    /**
     * @return DelContent
     */	
	public String getDelContent() {
		return this.delContent;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param spplBizType
	 */
	public void setSpplBizType(String spplBizType) {
		this.spplBizType = spplBizType == null ? null : spplBizType.trim();
	}
	
    /**
     * @return SpplBizType
     */	
	public String getSpplBizType() {
		return this.spplBizType;
	}
	
	/**
	 * @param bizInstanceId
	 */
	public void setBizInstanceId(String bizInstanceId) {
		this.bizInstanceId = bizInstanceId == null ? null : bizInstanceId.trim();
	}
	
    /**
     * @return BizInstanceId
     */	
	public String getBizInstanceId() {
		return this.bizInstanceId;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}

	/**
	 * @return PrdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

	/**
	 * @return PrdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

}