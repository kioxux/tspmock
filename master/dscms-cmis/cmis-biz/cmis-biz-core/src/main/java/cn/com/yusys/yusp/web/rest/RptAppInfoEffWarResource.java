/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.dto.RptAppInfoEffWarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptAppInfoEffWar;
import cn.com.yusys.yusp.service.RptAppInfoEffWarService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptAppInfoEffWarResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 21:11:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptappinfoeffwar")
public class RptAppInfoEffWarResource {
    @Autowired
    private RptAppInfoEffWarService rptAppInfoEffWarService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptAppInfoEffWar>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptAppInfoEffWar> list = rptAppInfoEffWarService.selectAll(queryModel);
        return new ResultDto<List<RptAppInfoEffWar>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptAppInfoEffWar>> index(QueryModel queryModel) {
        List<RptAppInfoEffWar> list = rptAppInfoEffWarService.selectByModel(queryModel);
        return new ResultDto<List<RptAppInfoEffWar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptAppInfoEffWar> show(@PathVariable("pkId") String pkId) {
        RptAppInfoEffWar rptAppInfoEffWar = rptAppInfoEffWarService.selectByPrimaryKey(pkId);
        return new ResultDto<RptAppInfoEffWar>(rptAppInfoEffWar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptAppInfoEffWar> create(@RequestBody RptAppInfoEffWar rptAppInfoEffWar) throws URISyntaxException {
        rptAppInfoEffWarService.insert(rptAppInfoEffWar);
        return new ResultDto<RptAppInfoEffWar>(rptAppInfoEffWar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptAppInfoEffWar rptAppInfoEffWar) throws URISyntaxException {
        int result = rptAppInfoEffWarService.update(rptAppInfoEffWar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptAppInfoEffWarService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptAppInfoEffWarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected  ResultDto<List<RptAppInfoEffWar>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptAppInfoEffWar>>(rptAppInfoEffWarService.selectByModel(model));
    }

    @PostMapping("/updateEffWar")
    protected ResultDto<Integer> updateEffWar(@RequestBody RptAppInfoEffWar rptAppInfoEffWar){
        return new ResultDto<Integer>(rptAppInfoEffWarService.updateSelective(rptAppInfoEffWar));
    }

    @PostMapping("/selectGrpEffWar")
    protected ResultDto<List<RptAppInfoEffWarDto>> selectGrpEffWar(@RequestBody QueryModel model){
        return new ResultDto<List<RptAppInfoEffWarDto>>(rptAppInfoEffWarService.selectGrpEffWar(model));
    }
    @PostMapping("/insertEffWar")
    protected ResultDto<Integer> insertEffWar(@RequestBody RptAppInfoEffWar rptAppInfoEffWar){
        rptAppInfoEffWar.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptAppInfoEffWarService.insertSelective(rptAppInfoEffWar));
    }
    @PostMapping("/deleteEffWar")
    protected ResultDto<Integer> deleteEffWar(@RequestBody RptAppInfoEffWar rptAppInfoEffWar){
        String pkId = rptAppInfoEffWar.getPkId();
        return new ResultDto<Integer>(rptAppInfoEffWarService.deleteByPrimaryKey(pkId));
    }
    @PostMapping("/initEffWar")
    protected ResultDto<List<RptAppInfoEffWar>> initEffWar(@RequestBody QueryModel model){
        return new ResultDto<List<RptAppInfoEffWar>>(rptAppInfoEffWarService.initEffWar(model));
    }
}
