package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.DocImageSpplInfo;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.DocImageSpplInfoPoDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CmisBizClientService;
import cn.com.yusys.yusp.service.DocImageSpplInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author cainingbo
 * @className DAGL10BizService
 * @Description 档案补扫申请后处理
 * @Date 2021/06/25
 */
@Service
public class DAGL10BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DAGL10BizService.class);

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();

        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoService.selectByPrimaryKey(serno);
        String bizSerno = docImageSpplInfo.getBizSerno();

        //将权限判断放入flowParam中
        WFBizParamDto param = new WFBizParamDto();
        Map<String, Object> params = new HashMap<>();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());

        DocImageSpplInfoPoDto docImageSpplInfoPoDto = null;
        if (Objects.equals(resultInstanceDto.getBizType(), "DA011")) {
            // 合同审核影像补扫
            docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailHTByBizSerno(serno);
        } else if (Objects.equals(resultInstanceDto.getBizType(), "DA026")) {
            // 展期协议影像补扫
            docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailZQByBizSerno(bizSerno);
        } else if (Objects.equals(resultInstanceDto.getBizType(), "DA029")) {
            // 担保变更协议影像补扫
            docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailDBBGByBizSerno(bizSerno);
        }

        String isCom = "";
        if (docImageSpplInfoPoDto != null && docImageSpplInfoPoDto.getIsCom() != null) {
            isCom = docImageSpplInfoPoDto.getIsCom();//是否对公
            params.put("isCom", isCom);
        }
        params.put("spplType", docImageSpplInfo.getSpplType());
        params.put("spplBizType", docImageSpplInfo.getSpplBizType());
        params.put("contNo", docImageSpplInfo.getContNo());
        params.put("billNo", docImageSpplInfo.getBillNo());
        params.put("serno", bizSerno);
        params.put("isDel", docImageSpplInfo.getIsDelData());
        params.put("bizInstanceId", docImageSpplInfo.getBizInstanceId());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("影像补扫审批后业务处理类型" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "流程提交操作，流程参数" + resultInstanceDto);
                DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                docImageSpplInfoService.updateSelective(info);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理-修改业务数据状态为通过
                DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                docImageSpplInfoService.updateSelective(info);

                // 合同审核影像补扫/展期协议影像补扫/担保变更协议影像补扫-分支机构 生成档案任务池任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(bizSerno);
                centralFileTaskdto.setTraceId(info.getContNo());
                centralFileTaskdto.setCusId(info.getCusId());
                centralFileTaskdto.setCusName(info.getCusName());
                centralFileTaskdto.setBizType(resultInstanceDto.getBizType());
                centralFileTaskdto.setInputId(info.getInputId());
                centralFileTaskdto.setInputBrId(info.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 02 非纯指令
                centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                centralFileTaskdto.setTaskType("02"); // 02 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);

                // 补扫完成更新原业务资料齐全
                docImageSpplInfoService.updateBusinessInfo(info.getBizSerno());
                // 人工发起的影像补扫生成归档任务
                docImageSpplInfoService.insertNewDocTask(info);
                // 推送影像审批信息
                docImageSpplInfoService.sendImage(resultInstanceDto, info);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docImageSpplInfoService.updateSelective(info);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docImageSpplInfoService.updateSelective(info);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    docImageSpplInfoService.updateSelective(info);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                docImageSpplInfoService.updateSelective(info);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("影像补扫申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                DocImageSpplInfo info = docImageSpplInfoService.selectByPrimaryKey(serno);
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                docImageSpplInfoService.updateSelective(info);
            } else {
                log.warn("影像补扫申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DAGL10.equals(flowCode));
    }
}
