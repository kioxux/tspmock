/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherCnyRateAppTax
 * @类描述: other_cny_rate_app_tax数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-05 11:19:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_app_tax")
public class OtherCnyRateAppTax extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 报表类型 **/
	@Column(name = "REPORT_TYPE", unique = false, nullable = false, length = 5)
	private String reportType;
	
	/** 所属年份 **/
	@Column(name = "BELG_YEAR", unique = false, nullable = false, length = 5)
	private String belgYear;
	
	/** 销售收入 **/
	@Column(name = "SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal saleIncome;
	
	/** 利润总额 **/
	@Column(name = "PROFIT_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal profitTotal;
	
	/** 资产负债率 **/
	@Column(name = "ASSET_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetDebtRate;
	
	/** 应纳增值税 **/
	@Column(name = "INCREMENT_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incrementTax;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param reportType
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
    /**
     * @return reportType
     */
	public String getReportType() {
		return this.reportType;
	}
	
	/**
	 * @param belgYear
	 */
	public void setBelgYear(String belgYear) {
		this.belgYear = belgYear;
	}
	
    /**
     * @return belgYear
     */
	public String getBelgYear() {
		return this.belgYear;
	}
	
	/**
	 * @param saleIncome
	 */
	public void setSaleIncome(java.math.BigDecimal saleIncome) {
		this.saleIncome = saleIncome;
	}
	
    /**
     * @return saleIncome
     */
	public java.math.BigDecimal getSaleIncome() {
		return this.saleIncome;
	}
	
	/**
	 * @param profitTotal
	 */
	public void setProfitTotal(java.math.BigDecimal profitTotal) {
		this.profitTotal = profitTotal;
	}
	
    /**
     * @return profitTotal
     */
	public java.math.BigDecimal getProfitTotal() {
		return this.profitTotal;
	}
	
	/**
	 * @param assetDebtRate
	 */
	public void setAssetDebtRate(java.math.BigDecimal assetDebtRate) {
		this.assetDebtRate = assetDebtRate;
	}
	
    /**
     * @return assetDebtRate
     */
	public java.math.BigDecimal getAssetDebtRate() {
		return this.assetDebtRate;
	}
	
	/**
	 * @param incrementTax
	 */
	public void setIncrementTax(java.math.BigDecimal incrementTax) {
		this.incrementTax = incrementTax;
	}
	
    /**
     * @return incrementTax
     */
	public java.math.BigDecimal getIncrementTax() {
		return this.incrementTax;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}


}