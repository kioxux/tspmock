/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.vo.CreditReportQryLstDZVo;
import cn.com.yusys.yusp.vo.CreditReportQryLstVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditReportQryLstMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CreditReportQryLstMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CreditReportQryLst selectByPrimaryKey(@Param("crqlSerno") String crqlSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CreditReportQryLst> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CreditReportQryLst record);

    /**
     * 批量插入
     * @param creditReportQryList
     * @return
     */
    int insertCreditReportQryList(@Param("list")List<CreditReportQryLst> creditReportQryList);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CreditReportQryLst record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CreditReportQryLst record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CreditReportQryLst record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("crqlSerno") String crqlSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
       * @方法名称：selectByAppLySerno
       * @方法描述：根据授信申请流水号查询
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-28 下午 6:31
       * @修改记录：修改时间   修改人员  修改原因
       */
    List<CreditReportQryLst> selectByAppLySerno(@Param("bizSerno") String bizSerno);

    /**
     * @方法名称：selectByAppLySerno
     * @方法描述：根据授信申请流水号查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-28 下午 6:31
     * @修改记录：修改时间   修改人员  修改原因
     */
    List<CreditReportQryLst> getGrpCusRelation(@Param("grpSerno") String grpSerno);

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 根据流水号更新审批状态
     * @参数与返回说明:
     *  @创建人：zxz
     * @算法描述: 无
     */
    int updateApproveStatus(@Param("crqlSerno") String iqpSerno, @Param("approveStatus") String approveStatus);

    /**
     * @方法名称: updateApproveStatusforzx
     * @方法描述: 根据流水号更新审批状态(xdzx0003)
     * @参数与返回说明:
     *  @创建人：zxz
     * @算法描述: 无
     */
    int updateApproveStatusforzx(@Param("crqlSerno") String iqpSerno, @Param("approveStatus") String approveStatus, @Param("ImageNo") String ImageNo);

    /**
     * @方法名称: updateApproveDate
     * @方法描述: 根据流水号更新流程发起时间（暂时借用征信成功发起时间（SEND_TIME））
     * @参数与返回说明:
     *  @创建人：zxz
     * @算法描述: 无
     */
    int updateApproveDate(@Param("crqlSerno") String iqpSerno);

    /**
     * @方法名称: selectCreditReportQryLstByCrqlSerno
     * @方法描述: 根据业务流水号查询征信信息
     * @参数与返回说明:
     *  @创建人：xx
     * @算法描述: 无
     */
    List<CreditReportQryLstDto> selectCreditReportQryLstByCrqlSerno(QueryModel model);

    List<CreditReportQryLstDto> selectAllCreditReportQryLstByCrqlSerno(QueryModel model);

    /**
     * @方法名称: getCreditInfo
     * @方法描述: 根据流水号获取征信台账信息
     * @参数与返回说明:
     *  @创建人：xuxin
     * @算法描述: 无
     */
    List<CreditReportQryLstDto> getCreditInfo(QueryModel model);

    List<CreditReportQryLstVo> getCreditInfoVo(QueryModel model);

    List<CreditReportQryLstDZVo> getCreditInfoDZVo(QueryModel model);

    /**
     * @方法名称: selectCreditByCertCodeAndTime
     * @方法描述: 根据证件号和创建时间查询征信信息
     * @参数与返回说明:
     *  @创建人：xx
     * @算法描述: 无
     */
    List<CreditReportQryLst> selectCreditByCertCodeAndTime(@Param("certCode") String certCode, @Param("createTime") String createTime);

    /**
     * 征信授权查看
     * @param xdzx0002DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdzx0002.resp.List> selectZxAuth(Xdzx0002DataReqDto xdzx0002DataReqDto);

    /**
     * @创建人 WH
     * @创建时间 2021/6/23 11:13
     * @注释 三十天内征信报告
     */
    List<CreditReportQryLst> selectbyxw(@Param("certCode")String certCode);

    /**
     * @创建人 hxl
     * @创建时间 2021/8/13 11:13
     * @注释 是否有征信报告
     */
    List<CreditReportQryLst> selectdanbaozhengxin(@Param("certCode")String certCode);

    /**
     * @方法名称: selectByPeriod
     * @方法描述: 根据业务阶段判断是否直接使用本地征信报告
     * @参数与返回说明:
     * @算法描述: 无
     */
    CreditReportQryLst selectByPeriod(CreditReportQryLst record);

    CreditReportQryLst selectReportUrlBySerno(String serno);

    /**
     * @方法名称: selectZXQueryCount
     * @方法描述: 查询客户经理当日发起的征信查询次数
     * @参数与返回说明:
     */
    int selectZXQueryCount(@Param("managerId") String managerId, @Param("qryClsList") String qryClsList, @Param("currentTime") String currentTime);

    /**
     * @方法名称: queryApproveStatusBySerno
     * @方法描述: 通过流水号查询审批状态
     * @参数与返回说明:
     */
    Xdzx0004DataRespDto queryApproveStatusBySerno(Xdzx0004DataReqDto xdzx0004DataReqDto);

    List<CreditReportQryLst> selectByCertCodeAndManagerId(@Param("certCode")String certCode, @Param("managerId") String managerId);

    /**
     * @方法名称: importCreditReportList
     * @方法描述: 引入30天内已查询的征信报告
     * @参数与返回说明:
     */
    List<CreditReportQryLst> importCreditReportList(QueryModel model);

    CreditReportQryLst selectAuthBookNoByModel(@Param("qryCls") String qryCls, @Param("managerId") String managerId, @Param("borrowRel") String borrowRel);
}