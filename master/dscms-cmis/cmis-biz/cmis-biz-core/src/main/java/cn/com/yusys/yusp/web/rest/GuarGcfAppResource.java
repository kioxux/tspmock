/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarGcfApp;
import cn.com.yusys.yusp.service.GuarGcfAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGcfAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-09 14:06:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guargcfapp")
public class GuarGcfAppResource {
    @Autowired
    private GuarGcfAppService guarGcfAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarGcfApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarGcfApp> list = guarGcfAppService.selectAll(queryModel);
        return new ResultDto<List<GuarGcfApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarGcfApp>> index(QueryModel queryModel) {
        List<GuarGcfApp> list = guarGcfAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarGcfApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<GuarGcfApp> show(@PathVariable("pkId") String pkId) {
        GuarGcfApp guarGcfApp = guarGcfAppService.selectByPrimaryKey(pkId);
        return new ResultDto<GuarGcfApp>(guarGcfApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarGcfApp> create(@RequestBody GuarGcfApp guarGcfApp) throws URISyntaxException {
        guarGcfAppService.insert(guarGcfApp);
        return new ResultDto<GuarGcfApp>(guarGcfApp);
    }
    /**
     * @函数名称:insertGuarGcfApp
     * @函数描述:省心快贷企业提款押品查封审核新增
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("")
    @PostMapping("/insertGuarGcfApp")
    protected ResultDto<Map> insertSfResultInfo(@RequestBody GuarGcfApp guarGcfApp) throws URISyntaxException {
        Map rtnData = guarGcfAppService.insertGuarGcfApp(guarGcfApp);
        return new ResultDto<>(rtnData);
    }


    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        GuarGcfApp guarGcfApp = guarGcfAppService.selectBySerno((String) params.get("pkId"));
        if (guarGcfApp != null) {
            resultDto.setCode(200);
            resultDto.setData(guarGcfApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:guarGcfApplist
     * @函数描述: 获取待发起省心快贷企业提款押品查封审核
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("省心快贷企业提款押品查封审核申请列表")
    @PostMapping("/guarGcfApplist")
    protected ResultDto<List<GuarGcfApp>> guarGcfApplist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pkId desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarGcfApp> list = guarGcfAppService.guarGcfApplist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<GuarGcfApp>>(list);
    }

    /**
     * @函数名称:guarGcfAppHislist
     * @函数描述: 获取审批历史省心快贷企业提款押品查封审核
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("省心快贷企业提款押品查封审核历史列表")
    @PostMapping("/guarGcfAppHislist")
    protected ResultDto<List<GuarGcfApp>> guarGcfAppHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pkId desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarGcfApp> list = guarGcfAppService.guarGcfAppHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<GuarGcfApp>>(list);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarGcfApp guarGcfApp) throws URISyntaxException {
        int result = guarGcfAppService.update(guarGcfApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:物理删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("省心快贷企业提款押品查封审核申请列表删除")
    @PostMapping("/guarGcfAppdelete")
    public ResultDto<Map> guarGcfAppdelete(@RequestBody GuarGcfApp guarGcfApp) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = guarGcfAppService.guarGcfAppdelete(guarGcfApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = guarGcfAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarGcfAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
