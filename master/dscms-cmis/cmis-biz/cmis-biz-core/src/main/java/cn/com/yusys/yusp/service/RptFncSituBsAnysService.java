/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.RptFncSituBs;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.service.risk.RiskItem0041Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptFncSituBsAnys;
import cn.com.yusys.yusp.repository.mapper.RptFncSituBsAnysMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituBsAnysService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-06 16:33:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptFncSituBsAnysService {

    @Autowired
    private RptFncSituBsAnysMapper rptFncSituBsAnysMapper;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private RptFncSituBsService rptFncSituBsService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptFncSituBsAnys selectByPrimaryKey(String pkId) {
        return rptFncSituBsAnysMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptFncSituBsAnys> selectAll(QueryModel model) {
        List<RptFncSituBsAnys> records = (List<RptFncSituBsAnys>) rptFncSituBsAnysMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptFncSituBsAnys> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptFncSituBsAnys> list = rptFncSituBsAnysMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptFncSituBsAnys record) {
        return rptFncSituBsAnysMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptFncSituBsAnys record) {
        return rptFncSituBsAnysMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptFncSituBsAnys record) {
        return rptFncSituBsAnysMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptFncSituBsAnys record) {
        return rptFncSituBsAnysMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptFncSituBsAnysMapper.deleteByPrimaryKey(pkId);
    }

    public List<RptFncSituBsAnys> selectByParamMap(Map map){
        return rptFncSituBsAnysMapper.selectByParamMap(map);
    }
    /**
     * 小企业标准版及小企业版初始化
     *
     * @param map
     * @return
     */
    public List<RptFncSituBsAnys> initSmallStandard(Map map) {
        List<RptFncSituBsAnys> result = new ArrayList<>();
        String serno = map.get("serno").toString();
        String fncFlag = map.get("fncFlag").toString();
        String cusId = map.get("cusId").toString();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        List<RptFncSituBsAnys> rptFncSituBsAnysList = this.selectByParamMap(map);
        if (CollectionUtils.nonEmpty(rptFncSituBsAnysList)) {
            for (RptFncSituBsAnys rptFncSituBsAnys : rptFncSituBsAnysList) {
                Integer sort = rptFncSituBsAnys.getSort();
                if (sort == null){
                    return rptFncSituBsAnysList;
                }
            }
            rptFncSituBsAnysList.sort( Comparator.comparing(RptFncSituBsAnys::getSort, (d1, d2) -> d1.compareTo(d2)));
            return rptFncSituBsAnysList;
        }
        ResultDto<List<FinanIndicAnalyDto>> rptFncTotalProfit = iCusClientService.getRptFncTotalProfit(map);
        if (rptFncTotalProfit != null && rptFncTotalProfit.getData() != null) {
            List<FinanIndicAnalyDto> data = rptFncTotalProfit.getData();
            String inputYear = "";
            if (CmisBizConstants.STD_FNC_FLAG_4.equals(fncFlag)) {
                RptFncSituBsAnys rptFncSituBsAnys1 = new RptFncSituBsAnys();
                rptFncSituBsAnys1.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys1.setSerno(serno);
                rptFncSituBsAnys1.setSubjectName("开票销售收入");
                rptFncSituBsAnys1.setSort(1);
                insert(rptFncSituBsAnys1);
                RptFncSituBsAnys rptFncSituBsAnys2 = new RptFncSituBsAnys();
                rptFncSituBsAnys2.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys2.setSerno(serno);
                rptFncSituBsAnys2.setSubjectName("缴纳增值税");
                rptFncSituBsAnys2.setSort(2);
                insert(rptFncSituBsAnys2);
                RptFncSituBsAnys rptFncSituBsAnys3 = new RptFncSituBsAnys();
                rptFncSituBsAnys3.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys3.setSerno(serno);
                rptFncSituBsAnys3.setSubjectName("缴纳所得税");
                rptFncSituBsAnys3.setSort(3);
                insert(rptFncSituBsAnys3);
                RptFncSituBsAnys rptFncSituBsAnys4 = new RptFncSituBsAnys();
                rptFncSituBsAnys4.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys4.setSerno(serno);
                rptFncSituBsAnys4.setSubjectName("税务报表净利润");
                rptFncSituBsAnys4.setSort(4);
                insert(rptFncSituBsAnys4);
                RptFncSituBsAnys rptFncSituBsAnys5 = new RptFncSituBsAnys();
                rptFncSituBsAnys5.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys5.setSerno(serno);
                rptFncSituBsAnys5.setSubjectName("用电量");
                rptFncSituBsAnys5.setSort(16);
                insert(rptFncSituBsAnys5);
                RptFncSituBsAnys rptFncSituBsAnys6 = new RptFncSituBsAnys();
                rptFncSituBsAnys6.setPkId(StringUtils.getUUID());
                rptFncSituBsAnys6.setSerno(serno);
                rptFncSituBsAnys6.setSubjectName("工人人数");
                rptFncSituBsAnys6.setSort(17);
                insert(rptFncSituBsAnys6);
            }
            for (FinanIndicAnalyDto finanIndicAnalyDto : data) {
                RptFncSituBsAnys rptFncSituBsAnys = new RptFncSituBsAnys();
                if (CmisBizConstants.STD_FNC_FLAG_4.equals(fncFlag) || CmisBizConstants.STD_FNC_FLAG_5.equals(fncFlag)) {
                    if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                        if ("销售收入".equals(finanIndicAnalyDto.getItemName())) {
                            rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                            rptFncSituBsAnys.setSerno(serno);
                            rptFncSituBsAnys.setSubjectName("销售收入");
                            rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            rptFncSituBsAnys.setFncFlag(fncFlag);
                            inputYear = finanIndicAnalyDto.getInputYear();
                            rptFncSituBsAnys.setSort(5);
                            int count = insert(rptFncSituBsAnys);
                            if (count > 0) {
                                result.add(rptFncSituBsAnys);
                            }
                            continue;
                        }
                    } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                        if ("年度销售收入".equals(finanIndicAnalyDto.getItemName())) {
                            rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                            rptFncSituBsAnys.setSerno(serno);
                            rptFncSituBsAnys.setSubjectName("销售收入");
                            rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            rptFncSituBsAnys.setFncFlag(fncFlag);
                            inputYear = finanIndicAnalyDto.getInputYear();
                            rptFncSituBsAnys.setSort(5);
                            int count = insert(rptFncSituBsAnys);
                            if (count > 0) {
                                result.add(rptFncSituBsAnys);
                            }
                            continue;
                        }
                    }

                    if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                        if ("利润总额".equals(finanIndicAnalyDto.getItemName())) {
                            rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                            rptFncSituBsAnys.setSerno(serno);
                            rptFncSituBsAnys.setSubjectName("利润总额");
                            rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            rptFncSituBsAnys.setFncFlag(fncFlag);
                            rptFncSituBsAnys.setSort(6);
                            inputYear = finanIndicAnalyDto.getInputYear();
                            int count = insert(rptFncSituBsAnys);
                            if (count > 0) {
                                result.add(rptFncSituBsAnys);
                            }
                            continue;
                        }
                    } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                        if ("年度利润".equals(finanIndicAnalyDto.getItemName())) {
                            rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                            rptFncSituBsAnys.setSerno(serno);
                            rptFncSituBsAnys.setSubjectName("利润总额");
                            rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            rptFncSituBsAnys.setFncFlag(fncFlag);
                            inputYear = finanIndicAnalyDto.getInputYear();
                            rptFncSituBsAnys.setSort(6);
                            int count = insert(rptFncSituBsAnys);
                            if (count > 0) {
                                result.add(rptFncSituBsAnys);
                            }
                            continue;
                        }
                    }
                    if ("实际净利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(7);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产净值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(8);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("存货".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(9);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应收账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(10);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(11);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资产负债率".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(12);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("经营活动现金净流量".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(13);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("所有者权益".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(14);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应收账款周转率".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(15);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                } else if (CmisBizConstants.STD_FNC_FLAG_6.equals(fncFlag)) {
                    if ("货币资金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(1);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("现金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(2);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("银行存款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("银行存款");
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setSort(3);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应收账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(4);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应收款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应收款");
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(5);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(6);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("存货".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("存货");
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(7);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他流动资产".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(8);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资产总计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(9);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产原值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(10);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产净值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(11);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("在建工程".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(12);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("无形资产".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("无形资产净值");
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(13);
                        insert(rptFncSituBsAnys);
                        result.add(rptFncSituBsAnys);
                        continue;
                    }
                    if ("短期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(14);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付票据".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(15);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(16);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预收帐款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(17);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付职工薪酬".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(18);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应交税费".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(19);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(20);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应付款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应付款小计");
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(21);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("流动负债合计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(22);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(23);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期应付款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(24);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("负债总额".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(25);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("实收资本".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(26);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资本公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(27);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("盈余公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(28);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("未分配利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(29);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("所有者权益".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(30);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                } else if (CmisBizConstants.STD_FNC_FLAG_1.equals(fncFlag) || CmisBizConstants.STD_FNC_FLAG_2.equals(fncFlag)) {
                    //以最近一期报表分析 以近两年一期报表分析
                    if ("货币资金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(1);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("现金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(2);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("银行存款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("银行存款");
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(3);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应收账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(4);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应收款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应收款");
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(5);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(6);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("存货".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("存货");
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(7);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他流动资产".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(8);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资产总计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(9);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产原值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(10);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产净值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(11);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("在建工程".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(12);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("无形资产".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("无形资产净值");
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(13);
                        insert(rptFncSituBsAnys);
                        result.add(rptFncSituBsAnys);
                        continue;
                    }
                    if ("短期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(14);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付票据".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(15);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(16);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预收帐款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(17);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付职工薪酬".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(18);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应交税费".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(19);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(20);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应付款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应付款");
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(21);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("流动负债合计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(22);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(23);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期应付款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(24);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("负债总额".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(25);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("实收资本".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(26);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资本公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(27);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("盈余公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(28);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("未分配利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(29);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("所有者权益".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setLastTwoYear(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptFncSituBsAnys.setLastYear(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptFncSituBsAnys.setCurYearLastMonth(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(30);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                } else if (CmisBizConstants.STD_FNC_FLAG_3.equals(fncFlag)) {
                    if ("应收账款周转率".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSort(41);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("货币资金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(1);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("现金".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(2);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("银行存款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("我行银行存款");
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(3);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        RptFncSituBsAnys temp = new RptFncSituBsAnys();
                        temp.setPkId(StringUtils.getUUID());
                        temp.setSerno(serno);
                        temp.setSubjectName("他行银行存款");
                        temp.setFncFlag(fncFlag);
                        temp.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp.setSort(4);
                        insert(temp);
                        result.add(temp);
                        continue;
                    }
                    if ("应收账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(5);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应收款".equals(finanIndicAnalyDto.getItemName())) {
                        RptFncSituBsAnys temp = new RptFncSituBsAnys();
                        temp.setPkId(StringUtils.getUUID());
                        temp.setSerno(serno);
                        temp.setSubjectName("其他应收款关联往来");
                        temp.setFncFlag(fncFlag);
                        temp.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp.setSort(6);
                        insert(temp);
                        result.add(temp);
                        RptFncSituBsAnys temp1 = new RptFncSituBsAnys();
                        temp1.setPkId(StringUtils.getUUID());
                        temp1.setSerno(serno);
                        temp1.setSubjectName("其他应收款对外往来");
                        temp1.setFncFlag(fncFlag);
                        temp1.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp1.setSort(7);
                        insert(temp1);
                        result.add(temp1);
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应收款小计");
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(8);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(9);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("存货".equals(finanIndicAnalyDto.getItemName())) {
                        RptFncSituBsAnys temp = new RptFncSituBsAnys();
                        temp.setPkId(StringUtils.getUUID());
                        temp.setSerno(serno);
                        temp.setSubjectName("存货原材料");
                        temp.setFncFlag(fncFlag);
                        temp.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp.setSort(10);
                        insert(temp);
                        result.add(temp);
                        RptFncSituBsAnys temp1 = new RptFncSituBsAnys();
                        temp1.setPkId(StringUtils.getUUID());
                        temp1.setSerno(serno);
                        temp1.setSubjectName("存货生产成本");
                        temp1.setFncFlag(fncFlag);
                        temp1.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp1.setSort(11);
                        insert(temp1);
                        result.add(temp1);
                        RptFncSituBsAnys temp2 = new RptFncSituBsAnys();
                        temp2.setPkId(StringUtils.getUUID());
                        temp2.setSerno(serno);
                        temp2.setSubjectName("存货库存商品");
                        temp2.setFncFlag(fncFlag);
                        temp2.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp2.setSort(12);
                        insert(temp2);
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("存货小计");
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(13);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他流动资产".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        rptFncSituBsAnys.setSort(14);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资产总计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(15);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产原值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(16);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("固定资产净值".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(17);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("在建工程".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(18);
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("无形资产".equals(finanIndicAnalyDto.getItemName())) {
                        RptFncSituBsAnys temp = new RptFncSituBsAnys();
                        temp.setPkId(StringUtils.getUUID());
                        temp.setSerno(serno);
                        temp.setSubjectName("无形资产原值");
                        temp.setFncFlag(fncFlag);
                        temp.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        temp.setSort(19);
                        insert(temp);
                        result.add(temp);
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("无形资产净值");
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_01);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        rptFncSituBsAnys.setSort(20);
                        insert(rptFncSituBsAnys);
                        result.add(rptFncSituBsAnys);
                        continue;
                    }
                    if ("短期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(21);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付票据".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(22);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付账款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(23);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("预收帐款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(24);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付职工薪酬".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(25);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应交税费".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(26);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("应付利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(27);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("其他应付款".equals(finanIndicAnalyDto.getItemName())) {
                        RptFncSituBsAnys temp = new RptFncSituBsAnys();
                        temp.setPkId(StringUtils.getUUID());
                        temp.setSerno(serno);
                        temp.setSubjectName("其他应付款关联往来");
                        temp.setFncFlag(fncFlag);
                        temp.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        temp.setSort(28);
                        insert(temp);
                        result.add(temp);
                        RptFncSituBsAnys temp1 = new RptFncSituBsAnys();
                        temp1.setPkId(StringUtils.getUUID());
                        temp1.setSerno(serno);
                        temp1.setSubjectName("其他应付款对外往来");
                        temp1.setFncFlag(fncFlag);
                        temp1.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        temp1.setSort(29);
                        insert(temp1);
                        result.add(temp1);
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName("其他应付款小计");
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(30);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("流动负债合计".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(32);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期借款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(33);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("长期应付款".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(34);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("负债总额".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(35);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("实收资本".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(36);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("资本公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(37);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("盈余公积".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(38);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("未分配利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(39);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                    if ("所有者权益".equals(finanIndicAnalyDto.getItemName())) {
                        rptFncSituBsAnys.setPkId(StringUtils.getUUID());
                        rptFncSituBsAnys.setSerno(serno);
                        rptFncSituBsAnys.setSubjectName(finanIndicAnalyDto.getItemName());
                        rptFncSituBsAnys.setAmtSr(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        rptFncSituBsAnys.setFncFlag(fncFlag);
                        rptFncSituBsAnys.setSubjectType(CmisBizConstants.STD_RPT_SUB_TYPE_02);
                        rptFncSituBsAnys.setSort(40);
                        inputYear = finanIndicAnalyDto.getInputYear();
                        int count = insert(rptFncSituBsAnys);
                        if (count > 0) {
                            result.add(rptFncSituBsAnys);
                        }
                        continue;
                    }
                }
            }
            RptFncSituBs rptFncSituBs = rptFncSituBsService.selectByPrimaryKey(serno);
            if (Objects.nonNull(rptFncSituBs)) {
                rptFncSituBs.setInputYear(inputYear);
                rptFncSituBsService.updateSelective(rptFncSituBs);
            } else {
                RptFncSituBs temp = new RptFncSituBs();
                temp.setSerno(serno);
                temp.setInputYear(inputYear);
                rptFncSituBsService.insert(temp);
            }
        }
        result.sort( Comparator.comparing(RptFncSituBsAnys::getSort, (d1, d2) -> d1.compareTo(d2)));
        return result;
    }
    public int updateFnc(String serno){
        return rptFncSituBsAnysMapper.deleteBySerno(serno);
    }


}
