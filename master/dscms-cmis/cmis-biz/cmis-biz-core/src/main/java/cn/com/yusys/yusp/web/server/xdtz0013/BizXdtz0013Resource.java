package cn.com.yusys.yusp.web.server.xdtz0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0013.Xdtz0013Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0013.req.Xdtz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.resp.Xdtz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0013:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0013Resource.class);

    @Autowired
    private Xdtz0013Service xdtz0013Service;

    /**
     * 交易码：xdtz0013
     * 交易描述：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
     *
     * @param xdtz0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）")
    @PostMapping("/xdtz0013")
    protected @ResponseBody
    ResultDto<Xdtz0013DataRespDto> xdtz0013(@Validated @RequestBody Xdtz0013DataReqDto xdtz0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataReqDto));
        Xdtz0013DataRespDto xdtz0013DataRespDto = new Xdtz0013DataRespDto();// 响应Dto:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
        ResultDto<Xdtz0013DataRespDto> xdtz0013DataResultDto = new ResultDto<>();
        String certType = xdtz0013DataReqDto.getCertType();//证件类型
        String certNo = xdtz0013DataReqDto.getCertNo();//证件号码
        try {
            // 从xdtz0013DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0013DataReqDto));
            xdtz0013DataRespDto = xdtz0013Service.xdtz0013(xdtz0013DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0013DataRespDto));
            // 封装xdtz0013DataResultDto中正确的返回码和返回信息
            xdtz0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0013DataResultDto.setMessage("SuccessEnum.CMIS_SUCCSESS.value");
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, e.getMessage());
            // 封装xdtz0013DataResultDto中异常返回码和返回信息
            xdtz0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0013DataRespDto到xdtz0013DataResultDto中
        xdtz0013DataResultDto.setData(xdtz0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataResultDto));
        return xdtz0013DataResultDto;
    }
}
