/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.dto.LmtBsInfoDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtBsInfo;
import cn.com.yusys.yusp.service.LmtBsInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtBsInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-05-10 15:10:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产负债信息")
@RequestMapping("/api/lmtbsinfo")
public class LmtBsInfoResource {
    @Autowired
    private LmtBsInfoService lmtBsInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtBsInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtBsInfo> list = lmtBsInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtBsInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtBsInfo>> index(QueryModel queryModel) {
        List<LmtBsInfo> list = lmtBsInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtBsInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtBsInfo> show(@PathVariable("pkId") String pkId) {
        LmtBsInfo lmtBsInfo = lmtBsInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtBsInfo>(lmtBsInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtBsInfo> create(@RequestBody LmtBsInfo lmtBsInfo) throws URISyntaxException {
        lmtBsInfoService.insert(lmtBsInfo);
        return new ResultDto<LmtBsInfo>(lmtBsInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtBsInfo lmtBsInfo) throws URISyntaxException {
        int result = lmtBsInfoService.update(lmtBsInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtBsInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtBsInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectBySurveySerno
     * @函数描述:通过调查流水号查询信息
     * @参数与返回说明:
     * @auther: hubp
     */
    @ApiOperation("通过调查流水号查询资产负债信息")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<List<LmtBsInfo>> selectBySurveySerno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        List<LmtBsInfo> list = lmtBsInfoService.selectBySurveySerno(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<List<LmtBsInfo>>(list);
    }

    /*
     * @param [saveData]
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/5/18 16:56
     * @version 1.0.0
     * @desc 更新传入的数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("更新传入的数据")
    @PostMapping("/savebsinfo")
    protected ResultDto<Integer> saveBsInfo(@RequestBody LmtBsInfoDto lmtBsInfoDto) {
        int result = lmtBsInfoService.updateSelective(lmtBsInfoDto);
        return new ResultDto<Integer>(result);
    }
}
