package cn.com.yusys.yusp.web.server.xdxw0084;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0084.req.Xdxw0084DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0084.resp.Xdxw0084DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0084.Xdxw0084Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:共借人送达地址确认书文本生成PDF
 *
 * @author zrcb
 * @version 1.0
 */
@Api(tags = "XDXW0084:共借人送达地址确认书文本生成PDF")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0084Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0084Resource.class);

    @Autowired
    private Xdxw0084Service xdxw0084Service;

    /**
     * 交易码：xdxw0084
     * 交易描述：共借人送达地址确认书文本生成PDF
     *
     * @param xdxw0084DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("共借人送达地址确认书文本生成PDF")
    @PostMapping("/xdxw0084")
    protected @ResponseBody
    ResultDto<Xdxw0084DataRespDto> xdxw0084(@Validated @RequestBody Xdxw0084DataReqDto xdxw0084DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataReqDto));
        Xdxw0084DataRespDto xdxw0084DataRespDto = new Xdxw0084DataRespDto();// 响应Dto:优企贷还款账号变更
        ResultDto<Xdxw0084DataRespDto> xdxw0084DataResultDto = new ResultDto<>();
        try {
            String cusName = xdxw0084DataReqDto.getCusName();
            String signDate = xdxw0084DataReqDto.getSignDate();

            if(StringUtil.isEmpty(cusName)||StringUtil.isEmpty(signDate)){
                xdxw0084DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0084DataResultDto.setMessage("参数不能为空");
                return xdxw0084DataResultDto;
            }
            // 从xdxw0084DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataReqDto));
            xdxw0084DataRespDto = xdxw0084Service.xdxw0084(xdxw0084DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataRespDto));
            // 封装xdxw0084DataResultDto中正确的返回码和返回信息
            xdxw0084DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0084DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch(BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, e.getMessage());
            // 封装xdxw0084DataResultDto中异常返回码和返回信息
            xdxw0084DataResultDto.setCode(e.getErrorCode());
            xdxw0084DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, e.getMessage());
            // 封装xdxw0084DataResultDto中异常返回码和返回信息
            xdxw0084DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0084DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0084DataRespDto到xdxw0084DataResultDto中
        xdxw0084DataResultDto.setData(xdxw0084DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataResultDto));
        return xdxw0084DataResultDto;
    }
}
