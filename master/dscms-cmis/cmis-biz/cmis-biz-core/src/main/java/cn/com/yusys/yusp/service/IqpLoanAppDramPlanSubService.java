/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppDramPlanSubMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppDramPlanSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-23 14:36:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanAppDramPlanSubService {

    @Autowired
    private IqpLoanAppDramPlanSubMapper iqpLoanAppDramPlanSubMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpLoanAppDramPlanSub selectByPrimaryKey(String pkId) {
        return iqpLoanAppDramPlanSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanAppDramPlanSub> selectAll(QueryModel model) {
        List<IqpLoanAppDramPlanSub> records = (List<IqpLoanAppDramPlanSub>) iqpLoanAppDramPlanSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpLoanAppDramPlanSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanAppDramPlanSub> list = iqpLoanAppDramPlanSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
    /***
     * @param contNo
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub>
     * @author hubp
     * @date 2021/4/23 16:27
     * @version 1.0.0
     * @desc 根据合同号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<IqpLoanAppDramPlanSub> selectByContNo(String contNo) {
        List<IqpLoanAppDramPlanSub> list = iqpLoanAppDramPlanSubMapper.selectByContNo(contNo);
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpLoanAppDramPlanSub record) {
        return iqpLoanAppDramPlanSubMapper.insert(record);
    }

    /***
     * @param record
     * @return int
     * @author hubp
     * @date 2021/4/24 9:37
     * @version 1.0.0
     * @desc 从主键是否有值来判断是插入还是更新
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public int queryIqpLoanAppDramPlanSub(IqpLoanAppDramPlanSub record) {
        String pkId = record.getPkId();
        int row = 0;
        if (pkId == null || "".equals(pkId)) {
            record.setPkId(UUID.randomUUID().toString());
            row = iqpLoanAppDramPlanSubMapper.insert(record);
        } else {
            row = this.updateSelective(record);
        }
        return row;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanAppDramPlanSub record) {
        return iqpLoanAppDramPlanSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpLoanAppDramPlanSub record) {
        return iqpLoanAppDramPlanSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanAppDramPlanSub record) {
        return iqpLoanAppDramPlanSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpLoanAppDramPlanSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanAppDramPlanSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpLoanAppDramPlanSub> selectByIqpSerno(QueryModel model) {
        return iqpLoanAppDramPlanSubMapper.selectByIqpSerno(model);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */

    public boolean addOrUpdateAllTable(List<IqpLoanAppDramPlanSub> list) {
        for(IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub : list){
            // 判断当前行是否已存在
            IqpLoanAppDramPlanSub record = selectByPrimaryKey(iqpLoanAppDramPlanSub.getPkId());
            if(record != null){
                // 更新操作
                User userInfo = SessionUtils.getUserInformation();
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                iqpLoanAppDramPlanSub.setUpdId(userInfo.getLoginCode());
                iqpLoanAppDramPlanSub.setUpdBrId(userInfo.getOrg().getCode());
                iqpLoanAppDramPlanSub.setUpdDate(nowDate);
                iqpLoanAppDramPlanSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                updateSelective(iqpLoanAppDramPlanSub);
            }else{
                // 新增操作
                insertSelective(iqpLoanAppDramPlanSub);
            }
        }
        return true;
    }
}
