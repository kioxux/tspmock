package cn.com.yusys.yusp.web.server.xdsx0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0015.req.*;
import cn.com.yusys.yusp.dto.server.xdsx0015.resp.Xdsx0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0015.Xdsx0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 接口处理类:已批复的授信申请信息、押品信息以及合同信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0015:已批复的授信申请信息、押品信息以及合同信息同步")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0015Resource.class);

    @Autowired
    private Xdsx0015Service xdsx0015Service;
    /**
     * 交易码：xdsx0015
     * 交易描述：已批复的授信申请信息、押品信息以及合同信息同步
     *
     * @param xdsx0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("已批复的授信申请信息、押品信息以及合同信息同步")
    @PostMapping("/xdsx0015")
    protected @ResponseBody
    ResultDto<Xdsx0015DataRespDto> xdsx0015(@Validated @RequestBody Xdsx0015DataReqDto xdsx0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015DataReqDto));
        Xdsx0015DataRespDto xdsx0015DataRespDto = new Xdsx0015DataRespDto();// 响应Dto:已批复的授信申请信息、押品信息以及合同信息同步
        ResultDto<Xdsx0015DataRespDto> xdsx0015DataResultDto = new ResultDto<>();
        List<ListDb> listDb = xdsx0015DataReqDto.getListDb();
        List<ListDy> listDy = xdsx0015DataReqDto.getListDy();
        List<ListFx> listFx = xdsx0015DataReqDto.getListFx();
        List<ListGrt> listGrt = xdsx0015DataReqDto.getListGrt();
        List<ListYw> listYw = xdsx0015DataReqDto.getListYw();

        String op_flag = xdsx0015DataReqDto.getOp_flag();//类型
        String cus_id = xdsx0015DataReqDto.getCus_id();//客户号
        String cus_name = xdsx0015DataReqDto.getCus_name();//客户名称
        String cur_type = xdsx0015DataReqDto.getCur_type();//币种
        String app_crd_totl_amt = xdsx0015DataReqDto.getApp_crd_totl_amt();//循环授信敞口额度
        String app_temp_crd_totl_amt = xdsx0015DataReqDto.getApp_temp_crd_totl_amt();//临时授信敞口额额度
        String crd_totl_sum_amt = xdsx0015DataReqDto.getCrd_totl_sum_amt();//授信总额
        String apply_date = xdsx0015DataReqDto.getApply_date();//授信起始日
        String over_date = xdsx0015DataReqDto.getOver_date();//授信到期日
        String delay_months = xdsx0015DataReqDto.getDelay_months();//宽限期（月）
        String inpret_val = xdsx0015DataReqDto.getInpret_val();//数字解读值
        String inpret_val_risk_lvl = xdsx0015DataReqDto.getInpret_val_risk_lvl();//数字解读值风险等级
        String complex_risk_lvl = xdsx0015DataReqDto.getComplex_risk_lvl();//综合风险等级
        String lmt_advice = xdsx0015DataReqDto.getLmt_advice();//额度建议
        String input_date = xdsx0015DataReqDto.getInput_date();//录入时间
        String manager_id = xdsx0015DataReqDto.getManager_id();//管户客户经理号
        String fdydd_reality_ir = xdsx0015DataReqDto.getFdydd_reality_ir();//省心快贷利率
        String manager_br_id = xdsx0015DataReqDto.getManager_br_id();//管户机构
        String assure_means_main = xdsx0015DataReqDto.getAssure_means_main();//合同担保方式
        String yx_serno = xdsx0015DataReqDto.getYx_serno();//影像流水号
        String fx_serno = xdsx0015DataReqDto.getFx_serno();//授信协议分项编号
        String fx_type = xdsx0015DataReqDto.getFx_type();//授信协议分项类型
        //listFx
        String fx_assure_means_main = listFx.get(0).getFx_assure_means_main();//分项担保方式
        String crd_lmt_type = listFx.get(0).getCrd_lmt_type();//授信额度类型
        String crd_lmt = listFx.get(0).getCrd_lmt();//授信额度（元）
        String product = listFx.get(0).getProduct();//公司特色产品
        String fx_score = listFx.get(0).getFx_score();//风险评估得分
        String flag_serno = listFx.get(0).getFlag_serno();//标志字段
        //listYw
        String biz_type = listYw.get(0).getBiz_type();//业务品种
        String listyw_crd_lmt = listYw.get(0).getCrd_lmt();//额度
        String mini_rate = listYw.get(0).getMini_rate();//利率最低上浮比例
        String apply_amount = listYw.get(0).getApply_amount();//贷款申请金额(元)
        String loan_term = listYw.get(0).getLoan_term();//申请期限(月)
        String ruling_ir = listYw.get(0).getRuling_ir();//基准利率(年)
        String reality_ir_y = listYw.get(0).getReality_ir_y();//执行利率(年)
        String floating_rate = listYw.get(0).getFloating_rate();//浮动比例
        String apply_score = listYw.get(0).getApply_score();//申请评分
        String apply_score_risk_lvl = listYw.get(0).getApply_score_risk_lvl();//申请评分风险等级
        String rule_risk_lvl = listYw.get(0).getRule_risk_lvl();//规则风险等级
        String listyw_complex_risk_lvl = listYw.get(0).getComplex_risk_lvl();//综合风险等级
        String listyw_lmt_advice = listYw.get(0).getLmt_advice();//额度建议
        String price_advice = listYw.get(0).getPrice_advice();//定价建议
        String listyw_flag_serno = listYw.get(0).getFlag_serno();//标志字段
        // listGrt
        String grt_serno = listGrt.get(0).getGrt_serno();//抵质押物编号/保证群编号
        String grt_flag = listGrt.get(0).getGrt_flag();//担保标志
        String listGrt_flag_serno = listGrt.get(0).getFlag_serno();//标志字段
        //listDb
        String db_cus_id = listDb.get(0).getDb_cus_id();//担保合同客户编号
        String db_cus_name = listDb.get(0).getDb_cus_name();//担保合同客户名
        String guar_way = listDb.get(0).getGuar_way();//担保方式
        String db_flag = listDb.get(0).getDb_flag();//担保合同与抵押物对应关系
        //listDy
        String guaranty_id = listDy.get(0).getGuaranty_id();//抵押物编号
        String guaranty_type = listDy.get(0).getGuaranty_type();//抵押物类型
        String vin = listDy.get(0).getVin();//车架号
        String listDy_cus_id = listDy.get(0).getCus_id();//质押人ID
        String listDy_cus_name = listDy.get(0).getCus_name();//质押人名称
        String gage_name = listDy.get(0).getGage_name();//质押物名称
        String eval_date = listDy.get(0).getEval_date();//评估日期
        String max_mortagage_amt = listDy.get(0).getMax_mortagage_amt();//最高可质押金额（元）
        String guide_type = listDy.get(0).getGuide_type();//担保品类型细分
        String newcode = listDy.get(0).getNewcode();//新押品编码
        String confirm_eval = listDy.get(0).getConfirm_eval();//我行认定价值
        String confirm_date = listDy.get(0).getConfirm_date();//认定日期
        String listDy_db_flag = listDy.get(0).getDb_flag();//担保合同与抵押物对应关系
        try {
            // 从xdsx0015DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用xdsx0015Service层开始
            xdsx0015DataRespDto = xdsx0015Service.xdsx0015(xdsx0015DataReqDto);
            // TODO 调用xdsx0015Service层结束
            // 封装xdsx0015DataResultDto中正确的返回码和返回信息
            xdsx0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, e.getMessage());
            // 封装xdsx0015DataResultDto中异常返回码和返回信息
            xdsx0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0015DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, e.getMessage());
            // 封装xdsx0015DataResultDto中异常返回码和返回信息
            xdsx0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0015DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdsx0015DataRespDto到xdsx0015DataResultDto中
        xdsx0015DataResultDto.setData(xdsx0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015DataResultDto));
        return xdsx0015DataResultDto;
    }
}
