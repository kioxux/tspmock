package cn.com.yusys.yusp.service.server.xdcz0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.req.Xdcz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.resp.Xdcz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.bouncycastle.pqc.math.linearalgebra.BigEndianConversions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: xdcz0003Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0003Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdcz0003.Xdcz0003Service.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    @Autowired
    private PvpAccpAppService pvpAccpAppService;//银承出账申请表
    @Autowired
    private CtrAccpContService ctrAccpContService;  // 银承合同详情
    @Autowired
    private SenddxService senddxService; //业务逻辑处理类：短信/微信发送批量接口
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AsplAccpTaskService asplAccpTaskService;

    @Autowired
    private AccTContRelService accTContRelService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private DocAsplTcontService docAsplTcontService;

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private PvpAccpAppDrftSubService pvpAccpAppDrftSubService;

    /**
     * 电子银行承兑汇票出账申请
     *
     * @param xdcz0003DataReqDto
     * @return
     * @desc 票据系统组装出账批次信息后调用此接口发送信贷，
     * 1出账申请：信贷根据接口中银承协议编号进行额度校验，占用金额小于等于合同可用金额时，
     * 插入出账申请表pvp_accp_app中（票据池出账也插入这张表中），发短信通知客户经理，再发起出账审批流程；否则返回额度不足。
     * 2出账撤销：票据发起撤销审批时调用此接口发送信贷，信贷判断出账审批状态为待发起、手工作废、退回时，
     * 允许撤销，删除出账申请表pvp_accp_app信息；否则不允许撤销。
     * 业务场景：票据系统承兑汇票出账环节调用信贷接口
     * 接口说明：信贷接收出账通知后，根据prcCode出账场景不同，分别处理出账申请、出账撤销操作
     * 业务逻辑：
     * 1.出账处理前校验
     * 1).请求参数校验：【批次号pcSerno】【承兑协议号accpContNo】非空检查
     * 2.tradeCode='1'时，出账申请：
     * 1).票据池业务，需结合表生成pvp_accp_app信息（approve_status='000',chargeoff_status='1'）
     * 2).非票据池业务
     * A).计算当前承兑协议（CTR_ACCP_CONT）下已出账票据总敞口金额loan_balance=CTR_ACCP_CONT.CONT_AMT-SUM(ACC_ACCP.accp_amount-security_money_amt)
     * B).计算合同可用额度=CTR_ACCP_CONT.cont_amt-loan_balance
     * C).判断本次申请的敞口金额（）>合同可用额度时，拦截并提示合同可用额度不足；调用额度系统授信额度校验接口
     * D).校验接口中的保【证金比例security_money_rate】是否大于授信下保证金比例
     * E).生成pvp_accp_app信息（approve_status='000',chargeoff_status='1'）
     * F).发短信通知客户经理
     * 3.tradeCode='2'时，出账撤销：根据接口参数【批次号pcSerno】删除Pvp_Accp_App表记录
     * <p>
     * 备注：老系统代码AddOrDelPvpAccpAction.java
     * ctr_pjc_cont_details是否有数据，程序有关联但未使用其数据、非票据池未赋值CUS_ID
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0003DataRespDto xdcz0003(Xdcz0003DataReqDto xdcz0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value);
        Xdcz0003DataRespDto xdcz0003DataRespDto = new Xdcz0003DataRespDto();
        //默认成功
        xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
        xdcz0003DataRespDto.setOpMsg("电子银行承兑汇票申请成功！");
        String contNo = xdcz0003DataReqDto.getContNo();//银承协议编号
        String prcCode = xdcz0003DataReqDto.getPrcCode();//处理码
        String seq = xdcz0003DataReqDto.getSeq();//批次号
        Integer appPorderQnt = xdcz0003DataReqDto.getAppPorderQnt();//申请汇票数量
        BigDecimal applyAmt = xdcz0003DataReqDto.getApplyAmt();//申请金额
        String drftSignDate = xdcz0003DataReqDto.getDrftSignDate();//票据签订日期
        String drftEndDate = xdcz0003DataReqDto.getDrftEndDate();//票据到期日期
        String managerId = xdcz0003DataReqDto.getManagerId();//责任人客户经理号
        String bailAcct = xdcz0003DataReqDto.getBailAcct();//保证金账号
        BigDecimal bailRate = xdcz0003DataReqDto.getBailRate();//保证金比例
        String bailIntType = xdcz0003DataReqDto.getBailIntType();//保证金计息方式
        String bailAcctName = xdcz0003DataReqDto.getBailAcctName();//保证金户名
        BigDecimal chrgRate = xdcz0003DataReqDto.getChrgRate();//手续费比例
        BigDecimal chrgAmt = xdcz0003DataReqDto.getChrgAmt();//手续费金额
        String drfpoIsseMk = xdcz0003DataReqDto.getDrfpoIsseMk();//票据池出票标记
        String drftMedia = xdcz0003DataReqDto.getDrftMedia();//票据介质
        String isOtherSign = xdcz0003DataReqDto.getIsOtherSign();//是否他行代签
        String isOnlineMk = xdcz0003DataReqDto.getIsOnlineMk();//是否线上化标记
        String cusId = xdcz0003DataReqDto.getCusId();//客户号
        String imageNo = xdcz0003DataReqDto.getImageNo();//影像编号
        String eContNo = xdcz0003DataReqDto.geteContNo();//电子合同编号
        String pvpImgSerno = xdcz0003DataReqDto.getPvpImgSerno();//出账影像流水
        String tContNoImgs = xdcz0003DataReqDto.gettContNoImgs();//购销合同影像流水数组
        if (StringUtils.isBlank(prcCode)) {
            xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0003DataRespDto.setOpMsg("prcCode不能为空");
            return xdcz0003DataRespDto;
        }

        if (StringUtils.isBlank(seq)) {
            xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0003DataRespDto.setOpMsg("seq不能为空");
            return xdcz0003DataRespDto;
        }

        if (StringUtils.isBlank(contNo)) {
            xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0003DataRespDto.setOpMsg("contNo不能为空");
            return xdcz0003DataRespDto;
        }
        if (Objects.isNull(applyAmt)){
            xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0003DataRespDto.setOpMsg("applyAmt不能为空");
            return xdcz0003DataRespDto;
        }
        try {
            if ("1".equals(drftMedia)) {// 票据系统定义 1 银票 2 电票
                drftMedia = "0";//银票 0 (是否电票) 0否 1是
            } else if ("2".equals(drftMedia)) {
                drftMedia = "1";//电票 1
            }
            // 如果是资产池银票 优先校验购销合同
            if (Objects.equals("0", drfpoIsseMk)){
                // 出票校验
                logger.info("场景0：出票校验开始，参数为：contNo:{},applyAmt:{}", contNo, applyAmt);
                OpRespDto opRespDto = ctrAsplDetailsService.isPvpAccpApp(contNo, applyAmt);
                logger.info("场景0：出票校验开始，返回为：{}", JSON.toJSONString(opRespDto));
                if(CmisBizConstants.FAIL.equals(opRespDto.getOpFlag())){
                    BeanUtils.copyProperties(opRespDto,xdcz0003DataRespDto);
                    return xdcz0003DataRespDto;
                }
                // 获取所有的购销合同总的可用金额
                logger.info("场景0：获取所有的购销合同总的可用金额开始，参数为：{}", tContNoImgs);
                BigDecimal contHighAvlAmt = getContHighAvlAmt(tContNoImgs);
                logger.info("场景0：获取所有的购销合同总的可用金额结束，返回为：{}", contHighAvlAmt);
                // 校验当前出账的银票总额是否小于购销合同总可用余额，如果不能满足，不允许放款
                if(applyAmt.compareTo(contHighAvlAmt)>0){
                    xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                    xdcz0003DataRespDto.setOpMsg("购销合同可用信余额小于银承出票总额,购销合同影像流水【"+tContNoImgs
                            +"】,可用信余额【"+ Objects.toString(contHighAvlAmt)
                            +"】, 当前出票【"+Objects.toString(applyAmt)+"】");
                    return xdcz0003DataRespDto;
                }
            }
            if ("1".equals(prcCode)) {//出账申请
                CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);
                // 都是线上化
                logger.info("开始合同额度校验");
                ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDtoResultDto = sendCmisLmt0010(contNo, drfpoIsseMk, applyAmt);
                if (Objects.equals(cmisLmt0010RespDtoResultDto.getCode(), EpbEnum.EPB099999.key)) {
                    xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                    xdcz0003DataRespDto.setOpMsg(cmisLmt0010RespDtoResultDto.getMessage());
                    return xdcz0003DataRespDto;
                }
                if (Objects.equals("0", drfpoIsseMk)) {
                    logger.info("资产池出票申请开始");
                    logger.info("查询资产池协议台账表开始，参数为：{}", contNo);
                    CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
                    logger.info("查询资产池协议台账表结束，返回为：{}", JSON.toJSONString(ctrAsplDetails));
                    if (Objects.isNull(ctrAsplDetails)) {
                        throw BizException.error(null, "9999", "未查询协议编号：【{}】,资产池信息", contNo);
                    }
                    // 同步银承出账申请
                    logger.info("资产池出账同步,银承出账申请");
                    PvpAccpApp pvpAccpApp =  insertPvpAccpAppByAspl(xdcz0003DataReqDto, cusBaseDto, ctrAsplDetails, drftMedia);
                    // 同步 银承出票台账 和 银承出账授权
                    logger.info("资产池出账同步,银承出票台账和银承出账授权");
                    insertAccAccpInfo(pvpAccpApp);
                    // 出账成功，关联购销合同和银承出账
                    logger.info("资产池出账同步,关联购销合同和银承出账");
                    insertAccTContRel(seq, tContNoImgs, ctrAsplDetails, applyAmt);
                    // 新增购销背景收集任务
                    logger.info("资产池出账同步,新增发票补录收集任务");
                    AsplAccpTask asplAccpTask = getAsplAccpTask(xdcz0003DataReqDto, ctrAsplDetails);
                    asplAccpTaskService.insertSelective(asplAccpTask);
                    // 去额度系统占用额度(资产池)
                    logger.info("资产池出账同步,额度系统开始占用额度(资产池)");
                    sendCmisLmt0013(seq);
                    xdcz0003DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
                    xdcz0003DataRespDto.setOpMsg("出账成功");// 描述信息
                    logger.info("资产池出票申请结束");

                } else {
                    PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(xdcz0003DataReqDto.getSeq());
                    if (Objects.isNull(pvpAccpApp)) {
                        pvpAccpAppService.insertSelective(getPvpAccpAppByAccp(xdcz0003DataReqDto, cusBaseDto, drftMedia));
                        // 生成银承出账申请数据后，引入合同关联的保证金信息
                        CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(contNo);
                        if(Objects.nonNull(ctrAccpCont)){
                            List<BailAccInfo> bailAccInfoList = bailAccInfoService.selectBySerno(ctrAccpCont.getSerno());
                            if(CollectionUtils.nonEmpty(bailAccInfoList)) {
                                for (BailAccInfo bailAccInfo : bailAccInfoList) {
                                    BailAccInfo bailAccInfoData = new BailAccInfo();
                                    BeanUtils.copyProperties(bailAccInfo, bailAccInfoData);
                                    bailAccInfoData.setSerno(seq);
                                    int insertCount = bailAccInfoService.insertSelective(bailAccInfoData);
                                    if (insertCount <= 0) {
                                        logger.error("保存保证金信息【{}】异常！", seq);
                                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                                    }
                                }
                            }
                        }
                    }
                }
                PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(seq);
                if(Objects.nonNull(pvpAccpApp)){
                    // 插入票据明细（包括资产池和普通的银承出账）
                    ResultDto<List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>> resultDto = null;
                    Xdpj23ReqDto xdpj23ReqDto = new Xdpj23ReqDto();
                    xdpj23ReqDto.setsBatchNo(seq);
                    xdpj23ReqDto.setTurnPageShowNum("100");
                    xdpj23ReqDto.setTurnPageBeginPos("1");
                    LocalDateTime now = LocalDateTime.now();
                    xdpj23ReqDto.setMac("");//mac地址
                    xdpj23ReqDto.setIpaddr("");//ip地址
                    xdpj23ReqDto.setServdt(tranDateFormtter.format(now));//交易日期
                    xdpj23ReqDto.setServti(tranTimestampFormatter.format(now));//交易时间
                    List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> list = null;
                    logger.info("通过批次号【{}】，前往票据系统查询票据明细开始,请求报文为:【{}】", seq, JSON.toJSONString(xdpj23ReqDto));
                    ResultDto<Xdpj23RespDto> xdpj23RespDtoResultDto = dscms2PjxtClientService.xdpj23(xdpj23ReqDto);
                    logger.info("通过批次号【{}】，前往票据系统查询票据明细结束,响应报文为:【{}】", seq, JSON.toJSONString(xdpj23RespDtoResultDto));
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj23RespDtoResultDto.getCode())
                            && Objects.nonNull(xdpj23RespDtoResultDto.getData())) {
                        Xdpj23RespDto data = xdpj23RespDtoResultDto.getData();
                        List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>  xdpj23List = data.getList();
                        if(CollectionUtils.nonEmpty(xdpj23List)){
                            for (cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List listData : xdpj23List) {
                                PvpAccpAppDrftSub pvpAccpAppDrftSub = new PvpAccpAppDrftSub();
                                pvpAccpAppDrftSub.setPkId(UUID.randomUUID().toString());//票号
                                pvpAccpAppDrftSub.setSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>()));//业务流水还
                                pvpAccpAppDrftSub.setDrftNo(listData.getBillno());//票号
                                pvpAccpAppDrftSub.setAccpPvpSeq(seq);//批次号
                                pvpAccpAppDrftSub.setDrftAmt(listData.getfBillAmount());//票面金额
                                pvpAccpAppDrftSub.setStartDate(listData.getIsseDt());//出票日期
                                pvpAccpAppDrftSub.setEndDate(listData.getDueDt());//到期日
                                pvpAccpAppDrftSub.setDrwr(listData.getDrwrNm());//出票人
                                pvpAccpAppDrftSub.setPyee(listData.getPyeeNm());//收款人
                                pvpAccpAppDrftSub.setPyeeAccno(listData.getPyeeAcctId());//收款人账号
                                pvpAccpAppDrftSub.setPyeeAcctsvcrName(listData.getPayeeBankName());//收款人开户行名称
                                pvpAccpAppDrftSub.setAccptr(listData.getAccptrNm());//承兑行名称
                                pvpAccpAppDrftSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                                pvpAccpAppDrftSub.setInputId(pvpAccpApp.getInputId());//登记人
                                pvpAccpAppDrftSub.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
                                pvpAccpAppDrftSub.setInputDate(pvpAccpApp.getInputDate());//登记日期
                                pvpAccpAppDrftSub.setManagerId(pvpAccpApp.getManagerId());//责任人
                                pvpAccpAppDrftSub.setManagerBrId(pvpAccpApp.getManagerBrId());//责任机构
                                Date nowDate = DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
                                pvpAccpAppDrftSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                                pvpAccpAppDrftSub.setCreateTime(nowDate);
                                pvpAccpAppDrftSub.setUpdateTime(nowDate);
                                int insertCount = pvpAccpAppDrftSubService.insert(pvpAccpAppDrftSub);
                                if(insertCount<=0){
                                    throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                                }
                            }
                        }
                    }
                }
            }
            if ("2".equals(prcCode)) {//申请撤销
                //逻辑删除 待发起、退回、生效的  电子银行承兑汇票出账申请
//                2出账撤销：票据发起撤销审批时调用此接口发送信贷，信贷判断出账审批状态为待发起、手工作废、退回时，
//     *      允许撤销，删除出账申请表pvp_accp_app信息；否则不允许撤销
                PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(seq);
                if (Objects.nonNull(pvpAccpApp) && Arrays.asList(new String[]{"000", "992"}).contains(pvpAccpApp.getApproveStatus())) {
                    pvpAccpAppService.deleteByPrimaryKey(pvpAccpApp.getPkId());
                } else {
                    xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                    xdcz0003DataRespDto.setOpMsg("根据票据系统银承协议编号accpContNo，对应的银承出账不是【未出账】状态，不可撤销!");
                    return xdcz0003DataRespDto;
                }
            }
            xdcz0003DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
            xdcz0003DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            xdcz0003DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdcz0003DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdcz0003DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdcz0003DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value);
        return xdcz0003DataRespDto;
    }

    /**
     * 新增购销背景收集任务（发票补录任务）
     */
    private AsplAccpTask getAsplAccpTask(Xdcz0003DataReqDto xdcz0003DataReqDto, CtrAsplDetails ctrAsplDetails) {
        AsplAccpTask asplAccpTask = new AsplAccpTask();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        asplAccpTask.setPkId(UUID.randomUUID().toString().replace("-", ""));// 主键
        asplAccpTask.setTaskId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.GXBJ_SERNO_SEQ, new HashMap()));// 任务编号
        asplAccpTask.setBillImgId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YX_SERNO_SEQ, new HashMap()));// 发票影像流水号
        asplAccpTask.setContNo(xdcz0003DataReqDto.getContNo());// 合同编号
//        asplAccpTask.setContCnNo(xdcz0003DataReqDto.geteContNo());// 中文合同号
        asplAccpTask.setPvpSerno(xdcz0003DataReqDto.getSeq());// 出账流水号(批次号)
        asplAccpTask.setCoreContNo(xdcz0003DataReqDto.getSeq());// 银承核心合同号
        asplAccpTask.setCusId(xdcz0003DataReqDto.getCusId());// 客户编号
        asplAccpTask.setCusName(ctrAsplDetails.getCusName());// 客户名称
        asplAccpTask.setTaskCreateDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 任务生成日期
        // 当前时间加60天
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.getCurrDate());
        calendar.add(Calendar.DATE, 60);
        asplAccpTask.setNeedFinishDate(DateUtils.formatDate(calendar.getTime(), DateFormatEnum.DATETIME.getValue()));// 要求完成日期
        asplAccpTask.setBatchDrftAmt(xdcz0003DataReqDto.getApplyAmt());// 批次票面金额
        asplAccpTask.setBatchQnt(xdcz0003DataReqDto.getAppPorderQnt());// 批次条数
        asplAccpTask.setCollectDate(StringUtils.EMPTY);// 收集日期
        asplAccpTask.setApproveStatus(BizFlowConstant.WF_STATUS_000);// 审批状态
        asplAccpTask.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 操作类型
        asplAccpTask.setRemark(StringUtils.EMPTY);// 备注描述
        asplAccpTask.setIsAddBill(CommonConstance.STD_ZB_YES_NO_0);// 是否完成补扫 0否
        asplAccpTask.setInputId(ctrAsplDetails.getInputId());// 登记人
        asplAccpTask.setInputBrId(ctrAsplDetails.getInputBrId());// 登记机构
        asplAccpTask.setInputDate(openDay);// 登记日期
        asplAccpTask.setUpdId(ctrAsplDetails.getUpdId());// 最近修改人
        asplAccpTask.setUpdBrId(ctrAsplDetails.getUpdBrId());// 最近修改机构
        asplAccpTask.setUpdDate(openDay);// 最近修改日期
        asplAccpTask.setManagerId(ctrAsplDetails.getManagerId());// 主管客户经理
        asplAccpTask.setManagerBrId(ctrAsplDetails.getManagerBrId());// 主管机构
        asplAccpTask.setCreateTime(DateUtils.getCurrDate());// 创建时间
        asplAccpTask.setUpdateTime(DateUtils.getCurrDate());// 修改时间
        return asplAccpTask;
    }

    /**
     * 关联购销合同和银承出账
     */
    private void insertAccTContRel(String pvpSerno, String tContNoImgs, CtrAsplDetails ctrAsplDetails, BigDecimal applyAmt) {
        AccTContRel accTContRel = null;
        AccAccp accAccp = accAccpService.selectByPvpSerno(pvpSerno);
        String[] tContNoImgArray = tContNoImgs.split("\\|");
        for (String tContNoImg : tContNoImgArray) {
            // 根据购销合同影像流水号，查询购销合同
            DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tContNoImg);

            accTContRel = new AccTContRel();
            accTContRel.setPkId(UUID.randomUUID().toString().replace("-", ""));
            accTContRel.setPvpSerno(pvpSerno);
            accTContRel.setType("1");
            accTContRel.setTContNo(docAsplTcont.getTcontNo());
            accTContRel.setContNo(ctrAsplDetails.getContNo());
            accTContRel.setTContSerno(tContNoImg);
            accTContRel.setCoreBillNo(accAccp.getCoreBillNo());
            accTContRel.setPvpAmt(accAccp.getDrftTotalAmt());
            accTContRel.setInputId(ctrAsplDetails.getContNo());// 登记人
            accTContRel.setInputBrId(ctrAsplDetails.getInputBrId());// 登记机构
            accTContRel.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));// 登记日期
            accTContRel.setUpdId(ctrAsplDetails.getUpdId());// 最近修改人
            accTContRel.setUpdBrId(ctrAsplDetails.getUpdBrId());// 最近修改机构
            accTContRel.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));// 最近修改日期
            accTContRel.setManagerId(ctrAsplDetails.getManagerId());// 主管客户经理
            accTContRel.setManagerBrId(ctrAsplDetails.getManagerBrId());// 主管机构
            accTContRel.setCreateTime(DateUtils.getCurrDate());// 创建时间
            accTContRel.setUpdateTime(DateUtils.getCurrDate());// 修改时间

            accTContRelService.insertSelective(accTContRel);
        }
    }

    /**
     * 根据资产池协议初始化银票出账申请
     */
    public PvpAccpApp insertPvpAccpAppByAspl(Xdcz0003DataReqDto xdcz0003DataReqDto, CusBaseDto cusBaseDto, CtrAsplDetails ctrAsplDetails, String drftMedia) {
        PvpAccpApp pvpAccpApp = new PvpAccpApp();
        BigDecimal applyAmt = xdcz0003DataReqDto.getApplyAmt();//申请金额
        BigDecimal chrgRate = xdcz0003DataReqDto.getChrgRate();//手续费比例
        BigDecimal bailRate = Optional.ofNullable(xdcz0003DataReqDto.getBailRate()).orElse(BigDecimal.ZERO);//保证金比例
        String contNo = xdcz0003DataReqDto.getContNo();// 协议编号
        String cusId = xdcz0003DataReqDto.getCusId();//客户号
        String managerId = xdcz0003DataReqDto.getManagerId();
        // 1 省心E票 0不是
        String isSxep = xdcz0003DataReqDto.getIsOnlineMk();
        pvpAccpApp.setIsSxep(isSxep);// 是否省心E票
        pvpAccpApp.setPvpMode(xdcz0003DataReqDto.getIsOnlineMk());//是否线上化标记（是否省心E票）
        pvpAccpApp.setPkId(UUID.randomUUID().toString());
        pvpAccpApp.setPvpSerno(xdcz0003DataReqDto.getSeq());//批次号
        pvpAccpApp.setContNo(contNo);//银承协议编号
        pvpAccpApp.setApplyPorder(Integer.parseInt(Objects.toString(xdcz0003DataReqDto.getAppPorderQnt())));//申请汇票数量
        pvpAccpApp.setAppAmt(applyAmt);//申请金额
        pvpAccpApp.setDraftSignDate(xdcz0003DataReqDto.getDrftSignDate());//票据签订日期
        pvpAccpApp.setEndDate(xdcz0003DataReqDto.getDrftEndDate());//票据到期日期
        pvpAccpApp.setManagerId(managerId);//责任人客户经理号
        pvpAccpApp.setInputId(managerId);
        pvpAccpApp.setChrgRate(chrgRate);//手续费比例
        pvpAccpApp.setCusId(xdcz0003DataReqDto.getCusId());//客户号
        pvpAccpApp.setIsOtherSign(xdcz0003DataReqDto.getIsOtherSign());//是否他行代签
        //保证金账号
        pvpAccpApp.setBailPerc(bailRate);//保证金比例
        pvpAccpApp.setBailAmt(applyAmt.multiply(bailRate));//保证金金额
        pvpAccpApp.setOprType("01");
        pvpAccpApp.setApproveStatus("997");
        pvpAccpApp.setChrgType("02");//转账


        pvpAccpApp.setStartDate(ctrAsplDetails.getStartDate());
        pvpAccpApp.setContAmt(ctrAsplDetails.getContAmt());
        pvpAccpApp.setContCurType(ctrAsplDetails.getCurType());
        pvpAccpApp.setGuarMode(ctrAsplDetails.getGuarMode());
        pvpAccpApp.setPrdId("042200");
        pvpAccpApp.setPrdName("银行承兑汇票开立");
        pvpAccpApp.setContType(ctrAsplDetails.getContType());
        pvpAccpApp.setPvpMode("1");//系统自动出账

        pvpAccpApp.setPrdTypeProp("");// 产品类型属性
        pvpAccpApp.setContHighDisb(ctrAsplDetails.getContAmt());// 本合同项下最高可用金额
        pvpAccpApp.setAorgType("3");// 承兑行类型
        pvpAccpApp.setAorgName("张家港农村商业银行");// 承兑行名称
        pvpAccpApp.setAorgNo("314305670002");// 承兑行行号
        // 从购销合同拿
        String tContNoImgs = xdcz0003DataReqDto.gettContNoImgs();
        if (StringUtils.isEmpty(tContNoImgs)) {
            throw BizException.error(null, "9999", "购销合同影像流水不能为空");
        }
        String[] tContNoImgArray = tContNoImgs.split("\\|");
        // 业务说目前只有一个
        String tContNoImgSerno = tContNoImgArray[0];
        logger.info("查询贸易合同表开始，参数为:{}", tContNoImgSerno);
        DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tContNoImgSerno);
        logger.info("查询贸易合同表结束，返回为:{}", JSON.toJSONString(docAsplTcont));
        pvpAccpApp.setDaorgName(docAsplTcont.getToppName());// 出票人开户户名
        pvpAccpApp.setDaorgNo(docAsplTcont.getToppAcctNo());// 出票人开户行账号
        pvpAccpApp.setIsUtilLmt("1");// 是否使用授信额度

        pvpAccpApp.setLmtAccNo(ctrAsplDetails.getLmtAccNo());
        pvpAccpApp.setReturnDraftInterestType("2");//活期
        pvpAccpApp.setWyImageSerno(xdcz0003DataReqDto.getImageNo());// 网银影像流水号

        pvpAccpApp.setReplyNo(ctrAsplDetails.getReplySerno());
        //保证金计息方式
        //保证金户名
        pvpAccpApp.setChrgAmt(xdcz0003DataReqDto.getChrgAmt());//手续费金额
        pvpAccpApp.setIsEDrft(drftMedia);//票据介质 IS_E_DRFT

        pvpAccpApp.setCusName(cusBaseDto.getCusName());
        pvpAccpApp.setInputBrId(ctrAsplDetails.getInputBrId());
        pvpAccpApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        pvpAccpApp.setManagerBrId(ctrAsplDetails.getManagerBrId());
        pvpAccpApp.setIsPool(CmisCommonConstants.YES_NO_1);// 是否资产池
        logger.info("新增出账申请，参数为:{}", JSON.toJSONString(pvpAccpApp));
        pvpAccpAppService.insertSelective(pvpAccpApp);
        return pvpAccpApp;
    }

    /**
     * 根据银承合同生成
     */
    public PvpAccpApp getPvpAccpAppByAccp(Xdcz0003DataReqDto xdcz0003DataReqDto, CusBaseDto cusBaseDto, String drftMedia) {
        // 1 省心E票 0不是
        String isSxep = xdcz0003DataReqDto.getIsOnlineMk();
        //申请金额
        BigDecimal applyAmt = xdcz0003DataReqDto.getApplyAmt();
        //手续费比例
        BigDecimal chrgRate = xdcz0003DataReqDto.getChrgRate();
        BigDecimal bailRate = Optional.ofNullable(xdcz0003DataReqDto.getBailRate()).orElse(BigDecimal.ZERO);
        // 协议编号
        String contNo = xdcz0003DataReqDto.getContNo();
        //客户号
        String cusId = xdcz0003DataReqDto.getCusId();
        String managerId = xdcz0003DataReqDto.getManagerId();

        PvpAccpApp pvpAccpApp = new PvpAccpApp();
        pvpAccpApp.setIsSxep(isSxep);// 是否省心E票
        pvpAccpApp.setPkId(UUID.randomUUID().toString());
        pvpAccpApp.setPvpSerno(xdcz0003DataReqDto.getSeq());//批次号
        pvpAccpApp.setContNo(contNo);//银承协议编号
        pvpAccpApp.setApplyPorder(Integer.parseInt(Objects.toString(xdcz0003DataReqDto.getAppPorderQnt())));//申请汇票数量
        pvpAccpApp.setAppAmt(applyAmt);//申请金额
        pvpAccpApp.setDraftSignDate(xdcz0003DataReqDto.getDrftSignDate());//票据签订日期
        pvpAccpApp.setChrgRate(chrgRate);//手续费比例
        pvpAccpApp.setCusId(xdcz0003DataReqDto.getCusId());//客户号
        pvpAccpApp.setIsOtherSign(xdcz0003DataReqDto.getIsOtherSign());
        //保证金账号
        pvpAccpApp.setBailPerc(bailRate);//保证金比例
        pvpAccpApp.setBailAmt(applyAmt.multiply(bailRate));//保证金金额
        pvpAccpApp.setOprType("01");
        pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        pvpAccpApp.setChrgType("02");//转账

        CtrAccpCont ctrAccpCont = ctrAccpContMapper.getContUnionHighContByContNo(contNo);
        pvpAccpApp.setStartDate(ctrAccpCont.getStartDate());
        pvpAccpApp.setEndDate(ctrAccpCont.getEndDate());//票据到期日期
        pvpAccpApp.setContAmt(ctrAccpCont.getContAmt());
        pvpAccpApp.setContCurType(ctrAccpCont.getCurType());
        pvpAccpApp.setGuarMode(ctrAccpCont.getGuarMode());
        if(ctrAccpCont.getPrdId().length()>6){
            CmisLmt0038ReqDto cmisLmt0038ReqDto = new CmisLmt0038ReqDto();
            cmisLmt0038ReqDto.setLimitSubNo(ctrAccpCont.getPrdId());
            logger.info("银承出账申请【{}】，前往额度系统查询适用产品开始,请求报文为:【{}】", xdcz0003DataReqDto.getSeq(), cmisLmt0038ReqDto.toString());
            ResultDto<CmisLmt0038RespDto>  resultDtoDto = cmisLmtClientService.cmislmt0038(cmisLmt0038ReqDto);
            logger.info("银承出账申请【{}】，前往额度系统查询适用产品结束,响应报文为:【{}】", xdcz0003DataReqDto.getSeq(), resultDtoDto.toString());
            if(!"0".equals(resultDtoDto.getCode())){
                logger.error("接口调用失败");
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(resultDtoDto.getData().getErrorCode()) && resultDtoDto.getData()!=null){
                logger.error("根据额度产品编号【{}】,查询对应的用信适用产品失败",ctrAccpCont.getPrdId());
                throw BizException.error(null,resultDtoDto.getData().getErrorCode(),resultDtoDto.getData().getErrorMsg());
            }
            CmisLmt0038RespDto data = resultDtoDto.getData();
            pvpAccpApp.setPrdId(data.getCmisLmt0038PrdListRespDtoList().get(0).getPrdId());
            pvpAccpApp.setPrdName(data.getCmisLmt0038PrdListRespDtoList().get(0).getPrdName());
        }else{
            pvpAccpApp.setPrdId(ctrAccpCont.getPrdId());
            pvpAccpApp.setPrdName(ctrAccpCont.getPrdName());
        }
        pvpAccpApp.setPrdTypeProp(ctrAccpCont.getPrdTypeProp());
        pvpAccpApp.setContType(ctrAccpCont.getContType());
        pvpAccpApp.setContHighDisb(ctrAccpCont.getContHighAvlAmt());
        pvpAccpApp.setPvpMode("1");//系统自动出账
        pvpAccpApp.setAorgType(ctrAccpCont.getAorgType());
        pvpAccpApp.setAorgName(ctrAccpCont.getAorgName());
        pvpAccpApp.setAorgNo(ctrAccpCont.getAorgNo());
        pvpAccpApp.setDaorgName(ctrAccpCont.getDaorgName());
        pvpAccpApp.setDaorgNo(ctrAccpCont.getDaorgNo());
        pvpAccpApp.setIsUtilLmt(ctrAccpCont.getIsUtilLmt());
        pvpAccpApp.setLmtAccNo(ctrAccpCont.getLmtAccNo());
        pvpAccpApp.setReturnDraftInterestType("2");//活期
        if (Objects.equals(CommonConstance.STD_ZB_YES_NO_1, isSxep)) {
            // 影像流水号(省心E票 用传参，非省心E票 用银承合同)
            pvpAccpApp.setWyImageSerno(xdcz0003DataReqDto.gettContNoImgs());
        }
        if (StringUtils.isBlank(ctrAccpCont.getOrigiContNo())) {//用这个编号充当批复编号 因为最高额合同有批复编号而银承合同没有
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(ctrAccpCont.getSerno());
            pvpAccpApp.setReplyNo(iqpAccpApp.getReplyNo());
        } else {
            pvpAccpApp.setReplyNo(ctrAccpCont.getOrigiContNo());
        }

        //保证金计息方式
        //保证金户名
        pvpAccpApp.setChrgAmt(xdcz0003DataReqDto.getChrgAmt());//手续费金额
        pvpAccpApp.setIsEDrft(drftMedia);//是否电子票据 IS_E_DRFT
        String ctrBeginFlag = ctrAccpCont.getCtrBeginFlag();
        if (Objects.equals(ctrBeginFlag, "1")) {
            pvpAccpApp.setIsContImageAudit("1");//合同影像是否审核
        } else {
            pvpAccpApp.setIsContImageAudit("0");//合同影像是否审核
        }
        Date now = DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
        pvpAccpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        pvpAccpApp.setCreateTime(now);
        pvpAccpApp.setUpdateTime(now);
        pvpAccpApp.setCusName(cusBaseDto.getCusName());
        pvpAccpApp.setInputBrId(ctrAccpCont.getManagerBrId());
        pvpAccpApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        pvpAccpApp.setManagerBrId(ctrAccpCont.getManagerBrId());
        pvpAccpApp.setManagerId(ctrAccpCont.getManagerId());//责任人客户经理号
        pvpAccpApp.setInputId(ctrAccpCont.getInputId());
        return pvpAccpApp;
    }

    /**
     * 初始化接口参数
     *
     * @param contNo
     * @param drfpoIsseMk
     * @return
     */
    ResultDto<CmisLmt0010RespDto> sendCmisLmt0010(String contNo, String drfpoIsseMk, BigDecimal applyAmt) {
        List<CmisLmt0010ReqDealBizListDto> dealBizList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
        // 判断是否是资产池银票出票
        if (Objects.equals("0", drfpoIsseMk)) {
            CmisLmt0010ReqDealBizListDto dealBiz = ctrAsplDetailsService.getDealBizList(contNo);
            dealBiz.setDealBizAmt(applyAmt);
            dealBiz.setDealBizSpacAmt(applyAmt);
            dealBizList.add(dealBiz);
        } else {
            dealBizList = ctrAccpContMapper.getDealBizList(contNo);
        }
        CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
        cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(dealBizList);
        // 系统编号
        cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
        // 金融机构代码
        cmisLmt0010ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
        // 合同编号
        cmisLmt0010ReqDto.setBizNo(contNo);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010ReqDto));
        ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto)).orElse(new ResultDto<>());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010RespDtoResultDto));
        return cmisLmt0010RespDtoResultDto;
    }

    /**
     * 放款占额
     *
     * @param pvpSerno
     */

    void sendCmisLmt0013(String pvpSerno) {
        AccAccp accAccp = accAccpService.selectByPvpSerno(pvpSerno);
        //发台账占用接口占合同额度
        CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
        cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0013ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
        cmisLmt0013ReqDto.setSerno(accAccp.getPvpSerno());// 交易流水号（出账流水号）
        cmisLmt0013ReqDto.setBizNo(accAccp.getContNo());//合同编号
        cmisLmt0013ReqDto.setInputId(accAccp.getInputId());//登记人
        cmisLmt0013ReqDto.setInputBrId(accAccp.getInputBrId());//登记机构
        cmisLmt0013ReqDto.setInputDate(accAccp.getInputDate());//登记日期

        List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDto = new ArrayList<CmisLmt0013ReqDealBizListDto>();
        CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizDto = new CmisLmt0013ReqDealBizListDto();
        cmisLmt0013ReqDealBizDto.setDealBizNo(accAccp.getCoreBillNo());//台账编号
        cmisLmt0013ReqDealBizDto.setIsFollowBiz("0");//是否无缝衔接
        cmisLmt0013ReqDealBizDto.setOrigiDealBizNo("");//原交易业务编号
        cmisLmt0013ReqDealBizDto.setOrigiDealBizStatus("");//原交易业务状态
        cmisLmt0013ReqDealBizDto.setOrigiRecoverType("");//原交易业务恢复类型
        cmisLmt0013ReqDealBizDto.setOrigiBizAttr("");//原交易属性
        cmisLmt0013ReqDealBizDto.setCusId(accAccp.getCusId());//客户编号
        cmisLmt0013ReqDealBizDto.setCusName(accAccp.getCusName());//客户名称
        cmisLmt0013ReqDealBizDto.setPrdId(accAccp.getPrdId());//产品编号
        cmisLmt0013ReqDealBizDto.setPrdName(accAccp.getPrdName());//产品名称
        cmisLmt0013ReqDealBizDto.setPrdTypeProp(accAccp.getPrdTypeProp());//产品类型属性
        cmisLmt0013ReqDealBizDto.setDealBizAmtCny(accAccp.getDrftTotalAmt());//台账占用总额(人民币)
        cmisLmt0013ReqDealBizDto.setDealBizSpacAmtCny(BigDecimal.ZERO);//台账占用敞口(人民币)
        cmisLmt0013ReqDealBizDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
        cmisLmt0013ReqDealBizDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
        cmisLmt0013ReqDealBizDto.setStartDate(accAccp.getIsseDate());//台账起始日
        cmisLmt0013ReqDealBizDto.setEndDate(StringUtils.EMPTY);//台账到期日
        cmisLmt0013ReqDealBizDto.setDealBizStatus("200");//台账状态
        cmisLmt0013ReqDealBizListDto.add(cmisLmt0013ReqDealBizDto);
        cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDto);
        logger.info("银承出票申请发起流程-更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步开始");
        ResultDto<CmisLmt0013RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
        logger.info("银承出票申请发起流程-更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步结束");
        if (Objects.isNull(resultDtoDto.getData()) || !SuccessEnum.SUCCESS.key.equals(resultDtoDto.getData().getErrorCode())) {
            String code = resultDtoDto.getData().getErrorCode();
            logger.error("额度占用接口返回码：" + code + "，银承出票申请流水号：" + pvpSerno + "，额度占用接口返回信息:", resultDtoDto.getData().getErrorMsg());
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);

        }
    }
    /**
     * 资产池银承台账同步
     */
    public void insertAccAccpInfo(PvpAccpApp pvpAccpApp) {
        /** 计算保证金金额 */
        String pvpSerno = pvpAccpApp.getPvpSerno();
        BigDecimal secAmtThis = BigDecimal.ZERO;
        BigDecimal secAmtSum = BigDecimal.ZERO;
        BigDecimal procFeeAmount2This = BigDecimal.ZERO;// 现金
        BigDecimal procFeeAmount2Sum = BigDecimal.ZERO;// 现金

        BigDecimal procFeeAmountThis = BigDecimal.ZERO;// 转账
        BigDecimal procFeeAmountSum = BigDecimal.ZERO;// 转账
        BigDecimal secAmtTotal = pvpAccpApp.getBailAmt();// 保证金

        BigDecimal procFeeAmount = pvpAccpApp.getChrgAmt();// 手续费(转账)
        BigDecimal procFeeAmount2 = pvpAccpApp.getChrgAmt();// 手续费(现金)
        BigDecimal procFeePercent = pvpAccpApp.getChrgRate();//手续费率

        // IqpAccpAppPorderSub iqpAccpAppPorderSub = list.get(i);
        logger.info("资产池银票出账授权同步" + pvpSerno + "");
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        if (pvpAccpApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        Map seqMap = new HashMap();
        String tran_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        logger.info("pvpAuthorize出账通知生成流水号为：{}", tran_serno);
        pvpAuthorize.setTranSerno(tran_serno);// 交易流水号
        pvpAuthorize.setPvpSerno(pvpAccpApp.getPvpSerno());// 出账号
        pvpAuthorize.setContNo(pvpAccpApp.getContNo());//非报文字段(校验担保品是否入库用到)
        pvpAuthorize.setFldvalue33(pvpAccpApp.getGuarMode());//非报文字段(校验担保品是否入库用到)
        pvpAuthorize.setFldvalue34("");//非报文字段(校验担保品是否入库用到)
        pvpAuthorize.setFldvalue40("【银票】");
        logger.info("调用oca接口查询用户信息开始，参数为：{}", pvpAccpApp.getManagerId());
        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(pvpAccpApp.getManagerId());
        logger.info("调用oca接口查询用户信息结束，返回为：{}", JSON.toJSONString(resultDto));
        AdminSmUserDto adminSmUserDto = resultDto.getData();
        pvpAuthorize.setFldvalue39(adminSmUserDto.getUserName());//客户经理姓名

        // TODO 银承核心编号 = 汇票编号 = 出账流水 = 批次号
        pvpAuthorize.setBillNo(pvpAccpApp.getPvpSerno());//借据编号
        pvpAuthorize.setTranId("CRE800");// 交易码
        pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_2);// 交易状态
        pvpAuthorize.setTranDate(openDay);// 交易日期
        pvpAuthorize.setTranAmt(pvpAccpApp.getAppAmt());//申请金额
        BigDecimal drftAmt = pvpAccpApp.getAppAmt();//票面金额（申请金额）
        BigDecimal secRt = pvpAccpApp.getBailPerc();//保证金比例
        pvpAuthorize.setPrdId(pvpAccpApp.getPrdId());//产品编号
        pvpAuthorize.setPrdName(pvpAccpApp.getPrdName());//产品名称
        pvpAuthorize.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());
        pvpAuthorize.setFinaBrId(pvpAccpApp.getFinaBrId());//账务机构
        pvpAuthorize.setManagerId(pvpAccpApp.getManagerId());//责任人
        pvpAuthorize.setManagerBrId(pvpAccpApp.getManagerBrId());//申请机构
        pvpAuthorize.setCusId(pvpAccpApp.getCusId());//客户编号
        pvpAuthorize.setCusName(pvpAccpApp.getCusName());//客户名称

        pvpAuthorize.setFldvalue01(pvpAccpApp.getPvpSerno());//汇票编号
        pvpAuthorize.setFldvalue02(pvpAccpApp.getDaorgNo());//出票人账号
        pvpAuthorize.setFldvalue03(String.valueOf(pvpAccpApp.getAppAmt()));//出票金额
        pvpAuthorize.setFldvalue07(pvpAccpApp.getContNo());//协议编号
        pvpAuthorize.setFldvalue08("");//保证金账号
        pvpAuthorize.setFldvalue09(secAmtThis + "");
        pvpAuthorize.setFldvalue10(pvpAccpApp.getEndDate());//汇票到期日
        pvpAuthorize.setFldvalue11(pvpAccpApp.getChrgRate() + "");//手续费率
        pvpAuthorize.setFldvalue15(pvpAccpApp.getContCurType());//币别
        pvpAuthorize.setFldvalue16("");//保证金计息方式
        pvpAuthorize.setFldvalue17(pvpAccpApp.getReturnDraftInterestType());//保证金退票计息方式
        pvpAuthorize.setFldvalue18("");//保证金协议月利率
        pvpAuthorize.setFldvalue19("");//结算账号
        pvpAuthorize.setFldvalue20("");//结算账号户名
        pvpAuthorize.setFldvalue21(String.valueOf(procFeeAmount));//手续费金额(转账)(每张票的)
        pvpAuthorize.setFldvalue22(String.valueOf(procFeeAmount2));//手续费金额(现金)(每张票的)
        pvpAuthorize.setFldvalue23(String.valueOf(pvpAccpApp.getBailAmt()));//保证金金额
        pvpAuthorize.setFldvalue24(String.valueOf(pvpAccpApp.getApplyPorder()));//汇票张数
        pvpAuthorize.setFldvalue25(String.valueOf(pvpAccpApp.getOverdueRate()));//逾期垫款利率
        pvpAuthorize.setFldvalue27(pvpAccpApp.getAorgNo());//承兑机构
        pvpAuthorize.setFldvalue35(pvpAccpApp.getAorgNo());//承兑行编号
        pvpAuthorize.setFldvalue36(pvpAccpApp.getIssuedOrgNo());//签发机构
        pvpAuthorize.setFldvalue37(pvpAccpApp.getContCurType());//币种
        pvpAuthorize.setFldvalue38(pvpAccpApp.getEndDate());//到期日
        logger.info("新增出账通知，参数为：{}", JSON.toJSONString(pvpAuthorize));
        pvpAuthorizeService.insertSelective(pvpAuthorize);

        logger.info("放款审批通过-放款申请" + pvpSerno + "生成借据信息");
        AccAccp accAccp = new AccAccp();
        String pkId = StringUtils.uuid(true);
        accAccp.setPkId(pkId);//主键
        accAccp.setCoreBillNo(pvpAccpApp.getPvpSerno());//银承核心编号 批次号
        accAccp.setPvpSerno(pvpAccpApp.getPvpSerno());//出账流水号
        accAccp.setContNo(pvpAccpApp.getContNo());//合同编号
        accAccp.setCusId(pvpAccpApp.getCusId());//客户编号
        accAccp.setCusName(pvpAccpApp.getCusName());//客户名称
        accAccp.setPrdId(pvpAccpApp.getPrdId());//产品编号
        accAccp.setPrdName(pvpAccpApp.getPrdName());//产品名称
        accAccp.setCurType(pvpAccpApp.getContCurType());//贷款发放币种
        accAccp.setGuarMode(pvpAccpApp.getGuarMode());//担保方式
        accAccp.setIsEDrft(pvpAccpApp.getIsEDrft());//是否电子票据
        accAccp.setDrftTotalAmt(pvpAccpApp.getAppAmt());//票据总金额(申请金额)
        accAccp.setDrftBalanceAmt(pvpAccpApp.getAppAmt());//票据总金额(申请金额)
        accAccp.setOrigiOpenAmt(pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt()));//原始敞口金额
        accAccp.setIsSxep(pvpAccpApp.getIsSxep());//是否省心E票
        accAccp.setIsseCnt(pvpAccpApp.getApplyPorder() + "");//开票张数
        accAccp.setIsseDate(pvpAccpApp.getDraftSignDate());//出票日期
        accAccp.setBailPerc(pvpAccpApp.getBailPerc());//保证金比例
        accAccp.setBailAmt(pvpAccpApp.getBailAmt());//保证金金额
        accAccp.setChrgRate(pvpAccpApp.getChrgRate());//手续费率
        accAccp.setSpacAmt(drftAmt.subtract(secAmtThis));//敞口金额
        accAccp.setChrgAmt(pvpAccpApp.getChrgAmt());//手续费金额
        accAccp.setChrgType(pvpAccpApp.getChrgType());//手续费类型
        accAccp.setOverdueRate(pvpAccpApp.getOverdueRate());//逾期垫款年利率
        accAccp.setReturnDraftInterestType(pvpAccpApp.getReturnDraftInterestType());//退票计息方式
        accAccp.setAorgType(pvpAccpApp.getAorgType());//承兑行类型
        accAccp.setAorgNo(pvpAccpApp.getAorgNo());//承兑行行号
        accAccp.setAorgName(pvpAccpApp.getAorgName());//承兑行名称
        accAccp.setIsUtilLmt(pvpAccpApp.getIsUtilLmt());//是否使用授信额度
        accAccp.setLmtAccNo(pvpAccpApp.getLmtAccNo());//授信额度编号
        accAccp.setReplyNo(pvpAccpApp.getReplyNo());//批复编号
        accAccp.setFinaBrId(pvpAccpApp.getFinaBrId());//账务机构编号
        accAccp.setFinaBrIdName(pvpAccpApp.getFinaBrIdName());//账务机构名称
        accAccp.setDisbOrgName(pvpAccpApp.getIssuedOrgName());//放款机构名称
        accAccp.setDisbOrgNo(pvpAccpApp.getIssuedOrgNo());//放款机构编号
        accAccp.setFiveClass("10");//五级分类
        accAccp.setTenClass("11");//十级分类
        accAccp.setClassDate(openDay);//分类日期
        accAccp.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());//产品类型属性
        accAccp.setIsOtherSign(pvpAccpApp.getIsOtherSign());//是否他行代签
        accAccp.setAccStatus(CmisCommonConstants.ACC_STATUS_1);//台账状态
        accAccp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
        accAccp.setUpdId(pvpAccpApp.getManagerId());
        accAccp.setUpdBrId(pvpAccpApp.getManagerBrId());
        accAccp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        accAccp.setManagerId(pvpAccpApp.getManagerId());
        accAccp.setManagerBrId(pvpAccpApp.getManagerBrId());
        accAccp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        accAccp.setIsPool(CommonConstance.STD_ZB_YES_NO_1);// 是否资产池
        logger.info("新增银承台账，参数为：{}", JSON.toJSONString(accAccp));
        accAccpService.insertSelective(accAccp);

    }
    /**
     * 获取购销合同可用信总金额
      */
    BigDecimal getContHighAvlAmt(String tContNoImgs){
        BigDecimal sum = BigDecimal.ZERO;
        // 这里的购销合同可能是多个
        String[] tContNoImgArray = tContNoImgs.split("\\|");
        for(String tContNoImg : tContNoImgArray){
            DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tContNoImg);
            sum = sum.add(docAsplTcontService.getContHighAvlAmt(docAsplTcont));
        }
        // 可用信用总金额（实时计算）
        return sum;
    }
}
