package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstZxd;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.domain.LmtSurveyTaskDivis;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CusLstZxdService;
import cn.com.yusys.yusp.service.LmtSurveyReportBasicInfoService;
import cn.com.yusys.yusp.service.LmtSurveyReportMainInfoService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @创建人 WH
 * @创建时间 2021-04-14
 * @return SurveyReportMainInfoBiz
 **/
@Service
public class XWYW01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(XWYW01BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private LmtSurveyReportMainInfoService lmtsurveyReportMainInfoService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private LmtSurveyReportBasicInfoService basicInfoService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        this.handlerXW001(resultInstanceDto);
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);

    }


    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "XWYW01".equals(flowCode);
//        return "XD_PERFER_RATE_APPLY_TEST".equals(flowCode);

    }

    public void handlerXW001(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                refuse(extSerno);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批状态更换
     */
    public void updateStatus(String serno, String state) {
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtsurveyReportMainInfoService.selectByPrimaryKey(serno);
        lmtSurveyReportMainInfo.setApproveStatus(state);
        lmtsurveyReportMainInfoService.updateSelective(lmtSurveyReportMainInfo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批通过
     */
    public void end(String extSerno) {
        //根据产品类型走不同的地方
        LmtSurveyReportMainInfo mainInfo = lmtsurveyReportMainInfoService.selectByPrimaryKey(extSerno);
        String surveyType = mainInfo.getSurveyType();
        log.info("小微授信审批通过日志-流水号：【{}】，产品类型：【{}】", extSerno,surveyType);
        if ("11".equals(surveyType)) {
            //惠享贷
            lmtsurveyReportMainInfoService.endPass(extSerno);
            // 惠享贷通过后，取发送短信通知
            sendMessage(extSerno , OpType.END);
        } else if ("9".equals(surveyType) || "15".equals(surveyType)) {
            //优抵贷
            lmtsurveyReportMainInfoService.yddEndPass(mainInfo);
        } else if ("7".equals(surveyType) || "14".equals(surveyType)) {
            //无还本
            lmtsurveyReportMainInfoService.whbEndPass(mainInfo);
        } else if ("12".equals(surveyType) ) {
            //优企贷
            mainInfo.setApproveStatus("997");
            lmtsurveyReportMainInfoService.update(mainInfo);
            lmtsurveyReportMainInfoService.yqdEndPass(mainInfo,"Y");
        }else if ("10".equals(surveyType)){
            //增享待
            lmtsurveyReportMainInfoService.zxdEndPass(mainInfo);
        }


    }
    /**
     * @创建人 WH
     * @创建时间 2021/7/27 16:18
     * @注释 拒绝操作
     */
    public void refuse(String extSerno){
        //根据产品类型走不同的地方
        LmtSurveyReportMainInfo mainInfo = lmtsurveyReportMainInfoService.selectByPrimaryKey(extSerno);
        String surveyType = mainInfo.getSurveyType();
       if ("12".equals(surveyType) ) {
            //优企贷拒绝操作
            lmtsurveyReportMainInfoService.yqdEndPass(mainInfo,"N");
        } else if("10".equals(surveyType)){
        // 更新增享贷白名单信息 办理状态为04--拒绝
           CusLstZxd cusLstZxd = new CusLstZxd();
           CusLstZxdService cusLstZxdService = new CusLstZxdService();
           cusLstZxd = cusLstZxdService.selectByPrimaryKey(mainInfo.getListSerno());
           cusLstZxd.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_04.key);
           cusLstZxdService.update(cusLstZxd);
       } else if ("11".equals(surveyType)) {
           // 惠享贷拒绝后，取发送短信通知
           sendMessage(extSerno , OpType.REFUSE);
       }
        updateStatus(extSerno,CmisCommonConstants.WF_STATUS_998);

    }

    /**
     * @param surveySerno , currentOpType
     * @return void
     * @author hubp
     * @date 2021/9/29 10:18
     * @version 1.0.0
     * @desc  消息提醒
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendMessage(String surveySerno , String currentOpType) {
        LmtSurveyReportMainInfo mainInfo = lmtsurveyReportMainInfoService.selectByPrimaryKey(surveySerno);
        log.info("*************惠享贷审批任务短信通知开始，调查流水号：【{}】",surveySerno);
        String managerId = mainInfo.getManagerId();
        String mgrTel = StringUtils.EMPTY;
        try {
            /**  先发送客户经理  */
            log.info("***********【{}】调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            String messageType = StringUtils.EMPTY; // 短信编号
            String receivedUserType = StringUtils.EMPTY; //接收人员类型
            if(OpType.END.equals(currentOpType)){
                messageType = "MSG_XW_M_0008";// 审核成功
                receivedUserType = "1";//1--客户经理 2--借款人
            } else {
                messageType = "MSG_XW_M_0009";// 审核失败
                receivedUserType = "1";//1--客户经理 2--借款人
            }
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName",mainInfo.getCusName());
            paramMap.put("prdName", mainInfo.getPrdName());
            //执行发送借款人操作
            log.info("*************惠享贷审批任务短信通知客户经理调用开始，调查流水号：【{}】，发送报文：【{}】",surveySerno, JSON.toJSONString(paramMap));
            ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            log.info("*************惠享贷审批任务短信通知客户经理调用结束，调查流水号：【{}】，返回报文：【{}】",surveySerno,JSON.toJSONString(resultMessageDto));



            /**  再发送借款人  */
            // 查询电话号码
            LmtSurveyReportBasicInfo basicInfo = basicInfoService.selectByPrimaryKey(surveySerno);
            mgrTel = basicInfo.getPhone();
            String messageTypeC = StringUtils.EMPTY; // 短信编号
            String receivedUserTypeC = StringUtils.EMPTY; //接收人员类型
            if(OpType.END.equals(currentOpType)){
                messageTypeC = "MSG_XW_C_0008";// 审核成功
                receivedUserTypeC = "2";//1--客户经理 2--借款人
            } else {
                messageTypeC = "MSG_XW_C_0009";// 审核失败
                receivedUserTypeC = "2";//1--客户经理 2--借款人
            }
            Map paramMapC = new HashMap();//短信填充参数
            paramMapC.put("cusName",mainInfo.getCusName());
            paramMapC.put("prdName", mainInfo.getPrdName());
            //执行发送借款人操作
            log.info("*************惠享贷审批任务短信通知客户经理调用开始，调查流水号：【{}】，发送报文：【{}】",surveySerno, JSON.toJSONString(paramMap));
            ResultDto<Integer> resultMessageDtoC = messageCommonService.sendMessage(messageTypeC, paramMapC, receivedUserTypeC, managerId, mgrTel);
            log.info("*************惠享贷审批任务短信通知客户经理调用结束，调查流水号：【{}】，返回报文：【{}】",surveySerno,JSON.toJSONString(resultMessageDtoC));
        } catch (Exception e) {
            log.error("*************惠享贷审批任务短信通知异常，调查流水号：【{}】，异常信息：【{}】",surveySerno,JSON.toJSONString(e));
        }
        log.info("*************惠享贷审批任务短信通知结束，调查流水号：【{}】",surveySerno);
    }

}
