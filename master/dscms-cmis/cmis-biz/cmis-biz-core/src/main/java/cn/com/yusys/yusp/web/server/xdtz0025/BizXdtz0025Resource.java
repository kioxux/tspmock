package cn.com.yusys.yusp.web.server.xdtz0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0025.Xdtz0025Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0025.req.Xdtz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.resp.Xdtz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:查询指定贷款开始日的优企贷客户贷款余额合计
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0025:查询指定贷款开始日的优企贷客户贷款余额合计")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0025Resource.class);

    @Autowired
    private Xdtz0025Service xdtz0025Service;

    /**
     * 交易码：xdtz0025
     * 交易描述：查询指定贷款开始日的优企贷客户贷款余额合计
     *
     * @param xdtz0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询指定贷款开始日的优企贷客户贷款余额合计")
    @PostMapping("/xdtz0025")
    protected @ResponseBody
    ResultDto<Xdtz0025DataRespDto> xdtz0025(@Validated @RequestBody Xdtz0025DataReqDto xdtz0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataReqDto));
        Xdtz0025DataRespDto xdtz0025DataRespDto = new Xdtz0025DataRespDto();// 响应Dto:查询指定贷款开始日的优企贷客户贷款余额合计
        ResultDto<Xdtz0025DataRespDto> xdtz0025DataResultDto = new ResultDto<>();
        String loanStartDate = xdtz0025DataReqDto.getLoanStartDate();//贷款开始日
        try {
            // 从xdtz0025DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0025DataReqDto));
            xdtz0025DataRespDto = xdtz0025Service.xdtz0025(xdtz0025DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0025DataRespDto));
            // 封装xdtz0025DataResultDto中正确的返回码和返回信息
            xdtz0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, e.getMessage());
            // 封装xdtz0025DataResultDto中异常返回码和返回信息
            xdtz0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0025DataRespDto到xdtz0025DataResultDto中
        xdtz0025DataResultDto.setData(xdtz0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataResultDto));
        return xdtz0025DataResultDto;
    }
}
