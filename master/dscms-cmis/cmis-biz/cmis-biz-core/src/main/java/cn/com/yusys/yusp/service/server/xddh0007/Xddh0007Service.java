package cn.com.yusys.yusp.service.server.xddh0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Lstkhzmx;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.client.bsp.core.ln3104.Ln3104Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xddh0007Service {

    private static final Logger logger = LoggerFactory.getLogger(Xddh0007Service.class);

    @Autowired
    private Ln3104Service ln3104Service;

    public ResultDto<Xddh0007DataRespDto> getRepayList(Xddh0007DataReqDto xddh0007DataReqDto, Map<String, String> map) {
        // TODO 应 卡与消金部 朱伟国 要求,还款明细需查该笔借据下所有明细,获取方式从核心实时获取改为从日终"贷款客户账户交易明细表(bat_s_core_klnl_dkkhmx)"获取
        // TODO 该实时查询方法暂时不用,换成 /api/accloan/getRepayListFromBatch
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataReqDto));
        ResultDto<Xddh0007DataRespDto> respDtoResultDto = new ResultDto<>();
        Xddh0007DataRespDto xddh0007DataRespDto = new Xddh0007DataRespDto();
        String billNo = xddh0007DataReqDto.getBillNo();//借据号
        Integer startPageNum = xddh0007DataReqDto.getStartPageNum();//起始页数
        Integer pageSize = xddh0007DataReqDto.getPageSize();//分页大小
        Ln3104ReqDto ln3104ReqDto = new Ln3104ReqDto();
        ln3104ReqDto.setDkjiejuh(billNo);
        String endDate = DateUtils.formatDate(DateUtils.getCurrDate(), DateFormatEnum.DATE_COMPACT.getValue());
        String startDate = DateUtils.addYear(DateUtils.getCurrDate(), DateFormatEnum.DATE_COMPACT.getValue(), -1);
        if (null != map.get("startDate")) {
            startDate = map.get("startDate").replaceAll("-", "");
        }
        if (null != map.get("endDate")) {
            endDate = map.get("endDate").replaceAll("-", "");
        }
        ln3104ReqDto.setQishriqi(startDate);// 起始日期
        ln3104ReqDto.setZhzhriqi(endDate);// 终止日期
//        ln3104ReqDto.setQishibis(startPageNum);
//        ln3104ReqDto.setChxunbis(pageSize);
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(ln3104ReqDto));
        ResultDto<Ln3104RespDto> ln3104RespDtoResultDto = ln3104Service.ln3104(ln3104ReqDto);
        Ln3104RespDto data = ln3104RespDtoResultDto.getData();
        respDtoResultDto.setCode(ln3104RespDtoResultDto.getCode());
        respDtoResultDto.setMessage(Optional.ofNullable(ln3104RespDtoResultDto.getMessage()).orElse("新核心系统中无对应记录"));
        if (Objects.nonNull(data) && Objects.nonNull(data.getZongbish()) && Objects.nonNull(data.getLstkhzmx())) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(data));
            xddh0007DataRespDto.setTotalQnt(Optional.ofNullable(data.getZongbish()).orElse(0));
            List<Lstkhzmx> lstkhzmx = Optional.ofNullable(data.getLstkhzmx()).orElse(new ArrayList<>());
            List<cn.com.yusys.yusp.dto.server.xddh0007.resp.List> collect = lstkhzmx.parallelStream().map(khzmx -> {
                cn.com.yusys.yusp.dto.server.xddh0007.resp.List temp = new cn.com.yusys.yusp.dto.server.xddh0007.resp.List();
                temp.setCusName(Optional.ofNullable(khzmx.getKehmingc()).orElse(StringUtils.EMPTY));// 客户姓名
                temp.setCusCertNo(StringUtils.EMPTY);// 客户身份证号
                temp.setBillNo(Optional.ofNullable(khzmx.getDkjiejuh()).orElse(StringUtils.EMPTY));// 借据号
                temp.setLoanStatus(StringUtils.EMPTY);// 贷款状态
                temp.setLoanAmt(Optional.ofNullable(khzmx.getFkjineee()).orElse(new BigDecimal(0L)));// 借据金额
                temp.setSerno(Optional.ofNullable(khzmx.getJiaoyils()).orElse(StringUtils.EMPTY));// 交易流水
                temp.setLoanBalance(new BigDecimal(0L));// 贷款余额
                temp.setRepayMode(Optional.ofNullable(khzmx.getZijnlaiy()).orElse(StringUtils.EMPTY));//还款模式
                temp.setRepayWay(Optional.ofNullable(khzmx.getHuankzht()).orElse(StringUtils.EMPTY));//还款方式
                temp.setHasbcCap(Optional.ofNullable(khzmx.getGhbenjin()).orElse(new BigDecimal(0L)));// 已还本金
                temp.setHasbcInt(Optional.ofNullable(khzmx.getGhysyjlx()).orElse(new BigDecimal(0L)));// 已还利息
                temp.setHasbcOdint(Optional.ofNullable(khzmx.getGhynshfx()).orElse(new BigDecimal(0L)));//已还罚息
                temp.setHasbcCi(Optional.ofNullable(khzmx.getGhfxfuxi()).orElse(new BigDecimal(0L)));//已还复利
                temp.setRepayAmt(Optional.ofNullable(khzmx.getHkzongee()).orElse(new BigDecimal(0L)));// 还款金额
                temp.setCurType(Optional.ofNullable(khzmx.getHuobdhao()).orElse(StringUtils.EMPTY));// 币种
                temp.setLoanDate(StringUtils.EMPTY);// 贷款日期
                temp.setLoanEndDate(StringUtils.EMPTY);// 贷款到期日
                temp.setLoanRate(new BigDecimal(0L));// 贷款利率
                temp.setRepayTime(Optional.ofNullable(khzmx.getJiaoyirq()).orElse(StringUtils.EMPTY));// 还款时间
                return temp;
            }).collect(Collectors.toList());
            xddh0007DataRespDto.setList(collect);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataRespDto));
            respDtoResultDto.setData(xddh0007DataRespDto);
            return respDtoResultDto;
        } else {
            return respDtoResultDto;
        }
    }
}
