/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordSpecialLoanApp
 * @类描述: other_record_special_loan_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 20:37:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_record_special_loan_app")
public class OtherRecordSpecialLoanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 批复编号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String replySerno;
	
	/** 授信分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 40)
	private String limitSubNo;
	
	/** 本次申请放款金额 **/
	@Column(name = "CURT_APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtAppAmt;
	
	/** 本次申请事项 **/
	@Column(name = "CURT_APP_CASE", unique = false, nullable = true, length = 4000)
	private String curtAppCase;
	
	/** 申请理由 **/
	@Column(name = "APP_RESN", unique = false, nullable = true, length = 4000)
	private String appResn;
	
	/** 是否有还款计划 **/
	@Column(name = "IS_REPAY_PLAN", unique = false, nullable = true, length = 5)
	private String isRepayPlan;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 已累计支付融资金额 **/
	@Column(name = "FINANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal financeAmt;
	
	/** 已累计支付自筹金额 **/
	@Column(name = "SELF_RAISED_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfRaisedAmt;
	
	/** 已累计支付资本金金额 **/
	@Column(name = "CAPITAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal capitalAmt;
	
	/** 已累计支付土建金额 **/
	@Column(name = "CIVIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal civilAmt;
	
	/** 已累计支付设备金额 **/
	@Column(name = "DEVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal deviceAmt;
	
	/** 已累计支付其他金额 **/
	@Column(name = "OTHER_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherAmt;
	
	/** 已累计支付土建交易金额 **/
	@Column(name = "CIVIL_TRADE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal civilTradeAmt;
	
	/** 已累计支付设备交易金额 **/
	@Column(name = "DEVICE_TRADE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal deviceTradeAmt;
	
	/** 已累计支付其他交易金额 **/
	@Column(name = "OTHER_TRADE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherTradeAmt;
	
	/** 已累计支付土建交易对象 **/
	@Column(name = "CIVIL_TRADE_OBJECT", unique = false, nullable = true, length = 200)
	private String civilTradeObject;
	
	/** 已累计支付设备交易对象 **/
	@Column(name = "DEVICE_TRADE_OBJECT", unique = false, nullable = true, length = 200)
	private String deviceTradeObject;
	
	/** 已累计支付其他交易对象 **/
	@Column(name = "OTHER_TRADE_OBJECT", unique = false, nullable = true, length = 200)
	private String otherTradeObject;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param curtAppAmt
	 */
	public void setCurtAppAmt(java.math.BigDecimal curtAppAmt) {
		this.curtAppAmt = curtAppAmt;
	}
	
    /**
     * @return curtAppAmt
     */
	public java.math.BigDecimal getCurtAppAmt() {
		return this.curtAppAmt;
	}
	
	/**
	 * @param curtAppCase
	 */
	public void setCurtAppCase(String curtAppCase) {
		this.curtAppCase = curtAppCase;
	}
	
    /**
     * @return curtAppCase
     */
	public String getCurtAppCase() {
		return this.curtAppCase;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}
	
    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param isRepayPlan
	 */
	public void setIsRepayPlan(String isRepayPlan) {
		this.isRepayPlan = isRepayPlan;
	}
	
    /**
     * @return isRepayPlan
     */
	public String getIsRepayPlan() {
		return this.isRepayPlan;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param financeAmt
	 */
	public void setFinanceAmt(java.math.BigDecimal financeAmt) {
		this.financeAmt = financeAmt;
	}
	
    /**
     * @return financeAmt
     */
	public java.math.BigDecimal getFinanceAmt() {
		return this.financeAmt;
	}
	
	/**
	 * @param selfRaisedAmt
	 */
	public void setSelfRaisedAmt(java.math.BigDecimal selfRaisedAmt) {
		this.selfRaisedAmt = selfRaisedAmt;
	}
	
    /**
     * @return selfRaisedAmt
     */
	public java.math.BigDecimal getSelfRaisedAmt() {
		return this.selfRaisedAmt;
	}
	
	/**
	 * @param capitalAmt
	 */
	public void setCapitalAmt(java.math.BigDecimal capitalAmt) {
		this.capitalAmt = capitalAmt;
	}
	
    /**
     * @return capitalAmt
     */
	public java.math.BigDecimal getCapitalAmt() {
		return this.capitalAmt;
	}
	
	/**
	 * @param civilAmt
	 */
	public void setCivilAmt(java.math.BigDecimal civilAmt) {
		this.civilAmt = civilAmt;
	}
	
    /**
     * @return civilAmt
     */
	public java.math.BigDecimal getCivilAmt() {
		return this.civilAmt;
	}
	
	/**
	 * @param deviceAmt
	 */
	public void setDeviceAmt(java.math.BigDecimal deviceAmt) {
		this.deviceAmt = deviceAmt;
	}
	
    /**
     * @return deviceAmt
     */
	public java.math.BigDecimal getDeviceAmt() {
		return this.deviceAmt;
	}
	
	/**
	 * @param otherAmt
	 */
	public void setOtherAmt(java.math.BigDecimal otherAmt) {
		this.otherAmt = otherAmt;
	}
	
    /**
     * @return otherAmt
     */
	public java.math.BigDecimal getOtherAmt() {
		return this.otherAmt;
	}
	
	/**
	 * @param civilTradeAmt
	 */
	public void setCivilTradeAmt(java.math.BigDecimal civilTradeAmt) {
		this.civilTradeAmt = civilTradeAmt;
	}
	
    /**
     * @return civilTradeAmt
     */
	public java.math.BigDecimal getCivilTradeAmt() {
		return this.civilTradeAmt;
	}
	
	/**
	 * @param deviceTradeAmt
	 */
	public void setDeviceTradeAmt(java.math.BigDecimal deviceTradeAmt) {
		this.deviceTradeAmt = deviceTradeAmt;
	}
	
    /**
     * @return deviceTradeAmt
     */
	public java.math.BigDecimal getDeviceTradeAmt() {
		return this.deviceTradeAmt;
	}
	
	/**
	 * @param otherTradeAmt
	 */
	public void setOtherTradeAmt(java.math.BigDecimal otherTradeAmt) {
		this.otherTradeAmt = otherTradeAmt;
	}
	
    /**
     * @return otherTradeAmt
     */
	public java.math.BigDecimal getOtherTradeAmt() {
		return this.otherTradeAmt;
	}
	
	/**
	 * @param civilTradeObject
	 */
	public void setCivilTradeObject(String civilTradeObject) {
		this.civilTradeObject = civilTradeObject;
	}
	
    /**
     * @return civilTradeObject
     */
	public String getCivilTradeObject() {
		return this.civilTradeObject;
	}
	
	/**
	 * @param deviceTradeObject
	 */
	public void setDeviceTradeObject(String deviceTradeObject) {
		this.deviceTradeObject = deviceTradeObject;
	}
	
    /**
     * @return deviceTradeObject
     */
	public String getDeviceTradeObject() {
		return this.deviceTradeObject;
	}
	
	/**
	 * @param otherTradeObject
	 */
	public void setOtherTradeObject(String otherTradeObject) {
		this.otherTradeObject = otherTradeObject;
	}
	
    /**
     * @return otherTradeObject
     */
	public String getOtherTradeObject() {
		return this.otherTradeObject;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}