/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtReplyAccSub;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubPrdMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:14:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplyAccSubPrdMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtReplyAccSubPrd selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtReplyAccSubPrd> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtReplyAccSubPrd record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtReplyAccSubPrd record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtReplyAccSubPrd record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtReplyAccSubPrd record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    List<LmtReplyAccSubPrd> queryLmtReplyAccSubPrdByParams(Map queryMap);

    /**
     * @方法名称: insertLmtReplyAccSubPrdList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertLmtReplyAccSubPrdList(@Param("list")List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList);

    /**
     * @方法名称: insertLmtReplyAccSubPrdList
     * @方法描述: 根据授信分项流水号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateLmtReplyAccSubPrd(@Param("serno")String serno);
    /**
     * @方法名称: queryWldcdAccSubNoByCusId
     * @方法描述: 根据客户号查询物联网动产贷授信分项流水号
     * @参数与返回说明:
     * @算法描述: 无
     */

    String queryWldcdAccSubNoByCusId(@Param("serno")String serno);

    /**
     * @函数名称:getLmtReplyAccSubPrdByAccSubNo
     * @函数描述:根据分项额度编号查询
     * @参数与返回说明:
     * @算法描述:
     */
    LmtReplyAccSubPrd getLmtReplyAccSubPrdByAccSubNo(@Param("accSubNo") String accSubNo);

    /**
     * @函数名称:getLmtReplyAccSubPrdByAccSubPrdNo
     * @函数描述:根据分项额度产品编号查询
     * @参数与返回说明:
     * @算法描述:
     */
    LmtReplyAccSubPrd getLmtReplyAccSubPrdByAccSubPrdNo(@Param("accSubPrdNo") String accSubPrdNo);

    /**
     * @函数名称:deleteByAccSubNo
     * @函数描述:根据授信协议分项流水号关联删除授信协议分项产品
     * @参数与返回说明:accSubNo 授信协议分项流水号
     * @算法描述:
     */
    int deleteByAccSubNo(String accSubNo);

    /**
     * @函数名称:getLmtReplyAccSubPrdByAccSubNo
     * @函数描述:根据分项额度编号查询
     * @参数与返回说明:
     * @算法描述:
     */
    LmtReplyAccSubPrd getLmtReplyAccSubPrdData(Map map);

    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项品种编号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    String getLmtReplyAccSubDataByAccSubPrdNo(@Param("accSubPrdNo") String accSubPrdNo);

    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项额度号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    String getLmtReplyAccSubDataByAccSubNo(@Param("accSubNo") String accSubNo);

    /**
     * @函数名称:getAccNoByAccSubPrdNo
     * @函数描述:通过分项品种编号查询授信台账号
     * @参数与返回说明:
     * @算法描述:
     */
    String getAccNoByAccSubPrdNo(@Param("accSubPrdNo") String accSubPrdNo);

    /**
     * @函数名称:getAccSubNoByAccSubPrdNo
     * @函数描述:通过分项品种编号查询分项额度号
     * @参数与返回说明:
     * @算法描述:
     */
    String getAccSubNoByAccSubPrdNo(@Param("accSubPrdNo") String accSubPrdNo);

    /**
     * @函数名称:getLmtReplyAccSubPrdByReplyNo
     * @函数描述:根据批复编号查询分项明细信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByReplyNo(String replyNo);

    /**
     * 根据批复分项流水号查询批复分项产品台账
     * @param replySubNoList
     * @return
     */
    List<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByReplySubNos(@Param("list") List<String> replySubNoList );

    /**
     * @函数名称:getLmtReplyAccSubPrdByCusId
     * @函数描述:根据查到的集团成员客户号查询授信批复台账
     * @参数与返回说明:
     * @算法描述:
     */
    List<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByCusId(String cusId);


    /**
     * @方法名称: selectCountLmtReplayHouseLoanBycusId
     * @方法描述: 查找该客户是否存在房抵e点贷的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    int queryCountLmtReplayHouseBycusId(HashMap<String, String> paramMap);

    /**
     * @方法名称:
     * @方法描述: 根据入参获取产品分项最晚到期日
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getLastEndDate(Map<String,String> map);

    /**
     * @方法名称:
     * @方法描述: 根据入参获取产品分项最早起始日
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getEarlyStartDate(Map<String,String> map);

    String getLastDateByAccNo(String accNo);

    String getEarlyDateByAccNo(String accNo);

    String getRevolimitLastDateByAccNo(String accNo);
}