package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 利率定价流程-展期 --东海村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class DHCZ10BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ10BizService.class);

    @Autowired
    private SGCZ10BizService sGCZ10BizService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        // 利率定价-展期 东海村镇
        if (BizFlowConstant.DHF04.equals(bizType)) {
            sGCZ10BizService.QT002biz(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.DHCZ10.equals(flowCode);
    }
}
