/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.BasicAndSingDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportBasicInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "调查报告基本信息")
@RequestMapping("/api/lmtsurveyreportbasicinfo")
public class LmtSurveyReportBasicInfoResource {
    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;
    @Autowired
    private LmtModelApprResultInfoService lmtModelApprResultInfoService;

    /**
     * 7
     * 全表查询.
     *
     * @return7
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyReportBasicInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyReportBasicInfo> list = lmtSurveyReportBasicInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyReportBasicInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyReportBasicInfo>> index(QueryModel queryModel) {
        List<LmtSurveyReportBasicInfo> list = lmtSurveyReportBasicInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyReportBasicInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{surveySerno}")
    protected ResultDto<LmtSurveyReportBasicInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyReportBasicInfo>(lmtSurveyReportBasicInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyReportBasicInfo> create(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) throws URISyntaxException {
        lmtSurveyReportBasicInfoService.insert(lmtSurveyReportBasicInfo);
        return new ResultDto<LmtSurveyReportBasicInfo>(lmtSurveyReportBasicInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) throws URISyntaxException {
        int result = lmtSurveyReportBasicInfoService.update(lmtSurveyReportBasicInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtSurveyReportBasicInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyReportBasicInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param surveyReportBasicAndCom
     * @return Boolean
     * @author 王玉坤
     * @date 2021/4/23 16:36
     * @version 1.0.0
     * @desc 模型审批接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("模型审批接口")
    @PostMapping("/modelapprove")
    protected ResultDto<Boolean> modelapprove(@RequestBody SurveyReportBasicAndCom surveyReportBasicAndCom) {
        ResultDto<Boolean> resultDto = null;
        try {
            resultDto = lmtSurveyReportBasicInfoService.modelapprove(surveyReportBasicAndCom);
        } catch (Exception e) {
            resultDto = new ResultDto<Boolean>(Boolean.FALSE).code(-1).message(e.getMessage());
        }
        return resultDto;
    }

    /**
     * @param surveyReportBasicAndCom
     * @return ResultDto<Integer>
     * @author 王玉坤
     * @date 2021/4/24 18:48
     * @version 1.0.0
     * @desc 保存企业信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("保存企业信息")
    @PostMapping("/savebasicandcom")
    protected ResultDto<Integer> savebasicandcom(@RequestBody SurveyReportBasicAndCom surveyReportBasicAndCom) {
        return    lmtSurveyReportBasicInfoService.savebasicandcom(surveyReportBasicAndCom);

    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/21 14:11
     * @注释 无还本普转新增接口 给移动端用的
     */
    @PostMapping("/savebasicandcomwhb")
    protected ResultDto<Integer> savebasicandcomwhb(@RequestBody SurveyReportBasicAndCom surveyReportBasicAndCom) {
        return  lmtSurveyReportBasicInfoService.savebasicandcomwhb(surveyReportBasicAndCom);

    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-25 22:28
     * @注释 查询模型审批是否通过
     */
    @ApiOperation("查询模型审批是否通过")
    @PostMapping("/selectmodelok/{surveyNo}")
    protected ResultDto selectmodelok(@PathVariable("surveyNo") String surveyNo) {
        LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveyNo);
        return new ResultDto(lmtModelApprResultInfo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-16 18:12
     * @注释 查询单条数据 并返回给前端展示
     */
    @ApiOperation("查询单条数据 并返回给前端展示")
    @PostMapping("/selectbasicandcom")
    protected ResultDto<SurveyReportBasicAndCom> selectbasicandcom(@RequestBody LmtSurveyReportDto dto) {
        ResultDto result= lmtSurveyReportBasicInfoService.selectbasicandcom(dto.getSurveySerno());
        return result;
    }

    /**
     * @创建人 wzy
     * @创建时间 14:12 2021-04-12
     * @return 保存修改基本信息
     **/
    @ApiOperation("保存修改基本信息")
    @PostMapping("/savebasic")
    protected ResultDto<Integer> savebasic(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        int result =   lmtSurveyReportBasicInfoService.savebasic(lmtSurveyReportBasicInfo);
        return new ResultDto<Integer>(result).message("保存成功");
    }

    /**
     * @创建人 hubp
     * @param lmtSurveyReportBasicInfo
     * @return
     * @注释  查询双录系统，根据借款人证件类型+证件号码，返回调查流水号
     */
    @ApiOperation("查询双录系统，根据借款人证件类型+证件号码，返回调查流水号")
    @PostMapping("/selectSing")
    protected ResultDto<List<BasicAndSingDto>> selectSing(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        List<BasicAndSingDto> result = lmtSurveyReportBasicInfoService.selectSing(lmtSurveyReportBasicInfo);
        return new ResultDto<List<BasicAndSingDto>>(result).message("保存成功");
    }
    /**
     * @param lmtSurveyReportBasicInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto>
     * @author hubp
     * @date 2021/5/22 14:51
     * @version 1.0.0
     * @desc    通过客户姓名和证件号码，查询客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过客户姓名和证件号码，查询客户信息")
    @PostMapping("/selectcusinfo")
    protected ResultDto<S00101RespDto> selectCusInfo(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        S00101RespDto result = lmtSurveyReportBasicInfoService.selectCusInfo(lmtSurveyReportBasicInfo);
        return new ResultDto<S00101RespDto>(result).code("0").message("查询成功！");
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/7 17:14
     * @注释 保存接口 增享贷
     */
    @ApiOperation("新增/保存接口")
    @PostMapping("/saveorupdate")
    protected ResultDto<Integer> saveorupdate(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) throws URISyntaxException {
            return     lmtSurveyReportBasicInfoService.saveorupdate(lmtSurveyReportBasicInfo);
    }

     /**
    * @author zlf
    * @date 2021/6/8 10:20
    * @version 1.0.0
    * @desc    查单条
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/selectone")
    protected ResultDto<LmtSurveyReportBasicInfo> showOne(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtSurveyReportBasicInfo>(lmtSurveyReportBasicInfo);
    }



    /**
     * @创建人 WB
     * @创建时间 2021-06-08 10:12
     * @注释 通过客户号查询单条数据 并返回给前端展示
     */
    @ApiOperation("通过客户号查询单条数据 并返回给前端展示")
    @PostMapping("/selecylmtsurveyreportbasicinfo")
    protected int selectBasiciInfoByCus(@RequestBody String certCode) {
        List<LmtSurveyReportBasicInfo> list = lmtSurveyReportBasicInfoService.selectBasiciInfoByCus(certCode);
        return list.size();
    }


    /**
     * @创建人 zrcbank-fengjj
     * @创建时间 2021-08-16 19:37
     * @注释 通过传入调查编号去找借据编号
     */
    @ApiOperation("通过传入调查编号去找借据")
    @PostMapping("/selectOldBillBySurveySerno")
    public ResultDto<List<AccLoan>> selectOldBillBySurveySerno(String surveySerno){
        return lmtSurveyReportBasicInfoService.selectOldBillBySurveySerno(surveySerno);
    }
}
