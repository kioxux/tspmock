/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestSubApp;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestSubAppMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestSubAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 14:59:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestSubAppService extends BizInvestCommonService {


    @Autowired
    private LmtSigInvestSubAppMapper lmtSigInvestSubAppMapper;

    @Autowired
    private LmtAppRelGuarService lmtAppRelGuarService;


	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestSubApp selectByPrimaryKey(String pkId) {
        return lmtSigInvestSubAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestSubApp> selectAll(QueryModel model) {
        List<LmtSigInvestSubApp> records = (List<LmtSigInvestSubApp>) lmtSigInvestSubAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestSubApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestSubApp> list = lmtSigInvestSubAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestSubApp record) {
        return lmtSigInvestSubAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestSubApp record) {
        return lmtSigInvestSubAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestSubApp record) {
        return lmtSigInvestSubAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestSubApp record) {
        return lmtSigInvestSubAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestSubAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestSubAppMapper.deleteByIds(ids);
    }

    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public LmtSigInvestSubApp selectBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestSubApp> lmtSigInvestSubApps = selectByModel(queryModel);
        if (lmtSigInvestSubApps!=null && lmtSigInvestSubApps.size()>0){
            return lmtSigInvestSubApps.get(0);
        }
        return null;
    }

    /**
     * 添加授信从表数据
     * @param serno
     * @param info
     * @param isUpdate
     */
    public int addOrUpdateLmtSigInvestSubApp(String serno, Map info, boolean isUpdate) {
        LmtSigInvestSubApp subApp = null;
        //更新Bean临时存储原PkId(info中可能会覆盖pkid)
        String origiPkId = null;
        if (isUpdate){
            subApp = selectBySerno(serno);
            if (subApp == null){
                return 0;
            }else{
                origiPkId = subApp.getPkId();
            }
        }else{
            subApp = new LmtSigInvestSubApp();
        }
        mapToBean(info,subApp);

        if (isUpdate){
            //是否存在担保及增信情况
            String isExistGuarCreditect = subApp.getIsExistGuarCreditect();
            //否 删除担保人、增信人信息
            if (CmisBizConstants.STD_ZB_YES_NO_N.equals(isExistGuarCreditect)){
                lmtAppRelGuarService.deleteLogicBySerno(serno);
            }

            subApp.setPkId(origiPkId);
            //初始化（更新）通用domain信息
            initUpdateDomainProperties(subApp);
            return lmtSigInvestSubAppMapper.updateByPrimaryKey(subApp);
        }else{
            subApp.setSerno(serno);
            //初始化(新增)通用domain信息
            initInsertDomainProperties(subApp);
            return lmtSigInvestSubAppMapper.insert(subApp);
        }
    }

    /**
     * 更新 更新其他担保和增信说明
     * @param serno
     * @param guarDescExt
     * @return
     */
    public int updateGuarDescExt(String serno, String guarDescExt) {
        return lmtSigInvestSubAppMapper.updateGuarDescExt(serno,guarDescExt);
    }

    /**
     * 申请复议拷贝从表数据
     * @param origiLmtAmtSerno
     * @param serno
     * @return
     */
    public int copyToSubApp(String origiLmtAmtSerno, String serno) {
        int result = 0;
        LmtSigInvestSubApp origiSubApp = selectBySerno(origiLmtAmtSerno);
        if (origiSubApp != null){
            LmtSigInvestSubApp newApp = new LmtSigInvestSubApp();
            org.springframework.beans.BeanUtils.copyProperties(origiSubApp,newApp);
            newApp.setPkId(generatePkId());
            newApp.setSerno(serno);
            newApp.setInputId(getCurrentUser().getLoginCode());
            newApp.setInputBrId(getCurrentUser().getOrg().getCode());
            newApp.setInputDate(getCurrrentDateStr());
            newApp.setUpdId(getCurrentUser().getLoginCode());
            newApp.setUpdBrId(getCurrentUser().getOrg().getCode());
            newApp.setUpdDate(getCurrrentDateStr());
            newApp.setCreateTime(getCurrrentDate());
            newApp.setUpdateTime(getCurrrentDate());
            result = insert(newApp);
        }
        return result;
    }


    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public List<LmtSigInvestSubApp> selectListBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestSubApp> lmtSigInvestSubApps = selectByModel(queryModel);
        if (lmtSigInvestSubApps!=null && lmtSigInvestSubApps.size()>0){
            return lmtSigInvestSubApps;
        }
        return null;
    }
}
