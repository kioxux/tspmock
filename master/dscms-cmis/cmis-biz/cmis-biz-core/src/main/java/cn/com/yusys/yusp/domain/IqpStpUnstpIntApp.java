/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpStpUnstpIntApp
 * @类描述: iqp_stp_unstp_int_app数据实体类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-13 18:08:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_stp_unstp_int_app")
public class IqpStpUnstpIntApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 表内欠息 **/
	@Column(name = "INNER_OWE_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal innerOweInt;
	
	/** 表外欠息 **/
	@Column(name = "OUT_OWE_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outOweInt;
	
	/** 复利 **/
	@Column(name = "PS_COMM_OD_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal psCommOdInt;
	
	/** 计息类型 STD_ZB_YES_NO **/
	@Column(name = "INT_TYPE", unique = false, nullable = true, length = 5)
	private String intType;
	
	/** 是否计收复利 STD_ZB_YES_NO **/
	@Column(name = "IS_CAL_CI", unique = false, nullable = true, length = 5)
	private String isCalCi;
	
	/** 停息日期 **/
	@Column(name = "STOP_DATE", unique = false, nullable = true, length = 10)
	private String stopDate;
	
	/** 恢复计息日期 **/
	@Column(name = "UNSTOP_DATE", unique = false, nullable = true, length = 10)
	private String unstopDate;
	
	/** 停息/恢复计息原因 **/
	@Column(name = "RESN", unique = false, nullable = true, length = 250)
	private String resn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param innerOweInt
	 */
	public void setInnerOweInt(java.math.BigDecimal innerOweInt) {
		this.innerOweInt = innerOweInt;
	}
	
    /**
     * @return innerOweInt
     */
	public java.math.BigDecimal getInnerOweInt() {
		return this.innerOweInt;
	}
	
	/**
	 * @param outOweInt
	 */
	public void setOutOweInt(java.math.BigDecimal outOweInt) {
		this.outOweInt = outOweInt;
	}
	
    /**
     * @return outOweInt
     */
	public java.math.BigDecimal getOutOweInt() {
		return this.outOweInt;
	}
	
	/**
	 * @param psCommOdInt
	 */
	public void setPsCommOdInt(java.math.BigDecimal psCommOdInt) {
		this.psCommOdInt = psCommOdInt;
	}
	
    /**
     * @return psCommOdInt
     */
	public java.math.BigDecimal getPsCommOdInt() {
		return this.psCommOdInt;
	}
	
	/**
	 * @param intType
	 */
	public void setIntType(String intType) {
		this.intType = intType;
	}
	
    /**
     * @return intType
     */
	public String getIntType() {
		return this.intType;
	}
	
	/**
	 * @param isCalCi
	 */
	public void setIsCalCi(String isCalCi) {
		this.isCalCi = isCalCi;
	}
	
    /**
     * @return isCalCi
     */
	public String getIsCalCi() {
		return this.isCalCi;
	}
	
	/**
	 * @param stopDate
	 */
	public void setStopDate(String stopDate) {
		this.stopDate = stopDate;
	}
	
    /**
     * @return stopDate
     */
	public String getStopDate() {
		return this.stopDate;
	}
	
	/**
	 * @param unstopDate
	 */
	public void setUnstopDate(String unstopDate) {
		this.unstopDate = unstopDate;
	}
	
    /**
     * @return unstopDate
     */
	public String getUnstopDate() {
		return this.unstopDate;
	}
	
	/**
	 * @param resn
	 */
	public void setResn(String resn) {
		this.resn = resn;
	}
	
    /**
     * @return resn
     */
	public String getResn() {
		return this.resn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}