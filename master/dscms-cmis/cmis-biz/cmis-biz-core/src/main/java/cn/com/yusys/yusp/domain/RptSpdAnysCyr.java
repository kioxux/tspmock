/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysCyr
 * @类描述: rpt_spd_anys_cyr数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-21 20:19:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_cyr")
public class RptSpdAnysCyr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 是否首次申请 **/
	@Column(name = "IS_FIRST_APP", unique = false, nullable = true, length = 5)
	private String isFirstApp;
	
	/** 基本情况有无变化 **/
	@Column(name = "IS_BASIC_INFO_CHG", unique = false, nullable = true, length = 5)
	private String isBasicInfoChg;
	
	/** 基本情况变化说明 **/
	@Column(name = "BASIC_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String basicChgDesc;
	
	/** 企业股权结构有无变化 **/
	@Column(name = "IS_ENTER_EQUITY_CHG", unique = false, nullable = true, length = 5)
	private String isEnterEquityChg;
	
	/** 企业股权结构变化说明 **/
	@Column(name = "ENTER_EQUITY_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String enterEquityChgDesc;
	
	/** 借款人婚姻状况变化 **/
	@Column(name = "IS_MARRIED_CHG", unique = false, nullable = true, length = 5)
	private String isMarriedChg;
	
	/** 借款人婚姻状况变化说明 **/
	@Column(name = "MARRIED_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String marriedChgDesc;
	
	/** 经营场所有无变化 **/
	@Column(name = "IS_SITE_OF_BUSINESS_CHG", unique = false, nullable = true, length = 5)
	private String isSiteOfBusinessChg;
	
	/** 经营场所变化说明 **/
	@Column(name = "SITE_OF_BUSINESS_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String siteOfBusinessChgDesc;
	
	/** 家庭住址、联系方式有无变化 **/
	@Column(name = "IS_ADDRESS_PHONE_CHG", unique = false, nullable = true, length = 5)
	private String isAddressPhoneChg;
	
	/** 家庭住址、联系方式变化说明 **/
	@Column(name = "ADDRESS_PHONE_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String addressPhoneChgDesc;
	
	/** 经营资产情况有无变化 **/
	@Column(name = "IS_BUSSINESS_ASSETS_CHG", unique = false, nullable = true, length = 5)
	private String isBussinessAssetsChg;
	
	/** 经营资产情况变化说明 **/
	@Column(name = "BUSSINESS_ASSETS_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String bussinessAssetsChgDesc;
	
	/** 个人信用状况有无变化 **/
	@Column(name = "IS_INDIV_CREDIT_CHG", unique = false, nullable = true, length = 5)
	private String isIndivCreditChg;
	
	/** 个人信用状况变化说明 **/
	@Column(name = "INDIV_CREDIT_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String indivCreditChgDesc;
	
	/** 企业信用状况有无变化 **/
	@Column(name = "IS_CORP_CREDIT_CHG", unique = false, nullable = true, length = 5)
	private String isCorpCreditChg;
	
	/** 企业信用状况变化说明 **/
	@Column(name = "CORP_CREDIT_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String corpCreditChgDesc;
	
	/** 关联方有无变化 **/
	@Column(name = "IS_RELATION_CHG", unique = false, nullable = true, length = 5)
	private String isRelationChg;
	
	/** 关联方变化说明 **/
	@Column(name = "RELATION_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String relationChgDesc;
	
	/** 经营信息有无变化 **/
	@Column(name = "IS_OPER_INFO_CHG", unique = false, nullable = true, length = 5)
	private String isOperInfoChg;
	
	/** 经营信息变化说明 **/
	@Column(name = "OPER_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String operChgDesc;
	
	/** 资产负债情况及主要科目有无变化 **/
	@Column(name = "IS_MAIN_SUB_CHG", unique = false, nullable = true, length = 5)
	private String isMainSubChg;
	
	/** 资产负债情况及主要科目变化说明 **/
	@Column(name = "MAIN_SUB_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String mainSubChgDesc;
	
	/** 主要财务指标有无变化 **/
	@Column(name = "IS_MAIN_TARGET_CHG", unique = false, nullable = true, length = 5)
	private String isMainTargetChg;
	
	/** 主要财务指标变化说明 **/
	@Column(name = "MAIN_TARGET_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String mainTargetChgDesc;
	
	/** 经营流水、纳税、水电气用量情况有无变化 **/
	@Column(name = "IS_MANAGE_COND_CHG", unique = false, nullable = true, length = 5)
	private String isManageCondChg;
	
	/** 经营流水、纳税、水电气用量情况说明 **/
	@Column(name = "MANGE_COND_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String mangeCondChgDesc;
	
	/** 销售额有无变化情况 **/
	@Column(name = "IS_SALE_CHG", unique = false, nullable = true, length = 5)
	private String isSaleChg;
	
	/** 销售额变化情况说明 **/
	@Column(name = "SALE_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String saleChgDesc;
	
	/** 关联企业有无变化情况 **/
	@Column(name = "IS_RELATION_CORP_CHG", unique = false, nullable = true, length = 5)
	private String isRelationCorpChg;
	
	/** 关联企业变化情况说明 **/
	@Column(name = "RELATION_CORP_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String relationCorpChgDesc;
	
	/** 销贷比是否超过60% **/
	@Column(name = "IS_PIN_TO_BORROW_THAN", unique = false, nullable = true, length = 5)
	private String isPinToBorrowThan;
	
	/** 销贷比是否超过60%说明 **/
	@Column(name = "PIN_TO_BORROW_THAN_DESC", unique = false, nullable = true, length = 65535)
	private String pinToBorrowThanDesc;
	
	/** 资产负债率是否超过75% **/
	@Column(name = "IS_ASSET_LIABILITY_THAN", unique = false, nullable = true, length = 5)
	private String isAssetLiabilityThan;
	
	/** 资产负债率是否超过75%说明 **/
	@Column(name = "ASSET_LIABILITY_THAN_DESC", unique = false, nullable = true, length = 65535)
	private String assetLiabilityThanDesc;
	
	/** 是否连续两年亏损 **/
	@Column(name = "IS_TWO_YEAR_LOSS", unique = false, nullable = true, length = 5)
	private String isTwoYearLoss;
	
	/** 是否连续两年亏损说明 **/
	@Column(name = "TWO_YEAR_LOSS_DESC", unique = false, nullable = true, length = 65535)
	private String twoYearLossDesc;
	
	/** 抵押物有无变化 **/
	@Column(name = "IS_GUAR_CHG", unique = false, nullable = true, length = 5)
	private String isGuarChg;
	
	/** 抵押物变化说明 **/
	@Column(name = "GUAR_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String guarChgDesc;
	
	/** 抵押物状态、地类用途、所有权人等基本情况有无变化 **/
	@Column(name = "IS_BASIC_COND_CHG", unique = false, nullable = true, length = 5)
	private String isBasicCondChg;
	
	/** 抵押物状态、地类用途、所有权人等基本情况变化说明 **/
	@Column(name = "BASIC_COND_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String basicCondChgDesc;
	
	/** 抵押物评估价值、使用情况、租金年限及价格有无变化 **/
	@Column(name = "IS_GUAR_COND_CHG", unique = false, nullable = true, length = 5)
	private String isGuarCondChg;
	
	/** 抵押物评估价值、使用情况、租金年限及价格变化说明 **/
	@Column(name = "GUAR_COND_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String guarCondChgDesc;
	
	/** 抵押人与借款人关系有无变化 **/
	@Column(name = "IS_BORROW_RELATION_CHG", unique = false, nullable = true, length = 5)
	private String isBorrowRelationChg;
	
	/** 抵押人与借款人关系变化说明 **/
	@Column(name = "BORROW_RELATION_CHG_DESC", unique = false, nullable = true, length = 65535)
	private String borrowRelationChgDesc;
	
	/** 抵押物状态查询结果是否正常 **/
	@Column(name = "IS_GUAR_SEARCH_RESULT_NORMAL", unique = false, nullable = true, length = 5)
	private String isGuarSearchResultNormal;
	
	/** 融资变化情况我行授信总额 **/
	@Column(name = "LMT_AMT_CHG", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmtChg;
	
	/** 融资变化情况授信有效期 **/
	@Column(name = "LMT_TERM_CHG", unique = false, nullable = true, length = 10)
	private Integer lmtTermChg;
	
	/** 融资变化情况我行贷款余额 **/
	@Column(name = "LOAN_BALANCE_CHG", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalanceChg;
	
	/** 融资变化情况个人贷款变化 **/
	@Column(name = "INDIV_LOAN_CHG", unique = false, nullable = true, length = 65535)
	private String indivLoanChg;
	
	/** 融资变化情况企业贷款变化 **/
	@Column(name = "CORP_LOAN_CHG", unique = false, nullable = true, length = 65535)
	private String corpLoanChg;
	
	/** 融资变化情况消费类 **/
	@Column(name = "CONSUME_CHG", unique = false, nullable = true, length = 65535)
	private String consumeChg;
	
	/** 个人准入条件 **/
	@Column(name = "INDIV_ACCESS_CONDI", unique = false, nullable = true, length = 65535)
	private String indivAccessCondi;
	
	/** 企业准入条件 **/
	@Column(name = "CORP_ACCESS_CONDI", unique = false, nullable = true, length = 65535)
	private String corpAccessCondi;
	
	/** 存量房抵贷条件 **/
	@Column(name = "STOCK_FDD_CONDI", unique = false, nullable = true, length = 65535)
	private String stockFddCondi;
	
	/** 补充说明 **/
	@Column(name = "SUPPLEMENTARY_NOTES", unique = false, nullable = true, length = 65535)
	private String supplementaryNotes;
	
	/** 还款来源分析 **/
	@Column(name = "REPAYMENT_SOURCE_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String repaymentSourceAnalysis;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param isFirstApp
	 */
	public void setIsFirstApp(String isFirstApp) {
		this.isFirstApp = isFirstApp;
	}
	
    /**
     * @return isFirstApp
     */
	public String getIsFirstApp() {
		return this.isFirstApp;
	}
	
	/**
	 * @param isBasicInfoChg
	 */
	public void setIsBasicInfoChg(String isBasicInfoChg) {
		this.isBasicInfoChg = isBasicInfoChg;
	}
	
    /**
     * @return isBasicInfoChg
     */
	public String getIsBasicInfoChg() {
		return this.isBasicInfoChg;
	}
	
	/**
	 * @param basicChgDesc
	 */
	public void setBasicChgDesc(String basicChgDesc) {
		this.basicChgDesc = basicChgDesc;
	}
	
    /**
     * @return basicChgDesc
     */
	public String getBasicChgDesc() {
		return this.basicChgDesc;
	}
	
	/**
	 * @param isEnterEquityChg
	 */
	public void setIsEnterEquityChg(String isEnterEquityChg) {
		this.isEnterEquityChg = isEnterEquityChg;
	}
	
    /**
     * @return isEnterEquityChg
     */
	public String getIsEnterEquityChg() {
		return this.isEnterEquityChg;
	}
	
	/**
	 * @param enterEquityChgDesc
	 */
	public void setEnterEquityChgDesc(String enterEquityChgDesc) {
		this.enterEquityChgDesc = enterEquityChgDesc;
	}
	
    /**
     * @return enterEquityChgDesc
     */
	public String getEnterEquityChgDesc() {
		return this.enterEquityChgDesc;
	}
	
	/**
	 * @param isMarriedChg
	 */
	public void setIsMarriedChg(String isMarriedChg) {
		this.isMarriedChg = isMarriedChg;
	}
	
    /**
     * @return isMarriedChg
     */
	public String getIsMarriedChg() {
		return this.isMarriedChg;
	}
	
	/**
	 * @param marriedChgDesc
	 */
	public void setMarriedChgDesc(String marriedChgDesc) {
		this.marriedChgDesc = marriedChgDesc;
	}
	
    /**
     * @return marriedChgDesc
     */
	public String getMarriedChgDesc() {
		return this.marriedChgDesc;
	}
	
	/**
	 * @param isSiteOfBusinessChg
	 */
	public void setIsSiteOfBusinessChg(String isSiteOfBusinessChg) {
		this.isSiteOfBusinessChg = isSiteOfBusinessChg;
	}
	
    /**
     * @return isSiteOfBusinessChg
     */
	public String getIsSiteOfBusinessChg() {
		return this.isSiteOfBusinessChg;
	}
	
	/**
	 * @param siteOfBusinessChgDesc
	 */
	public void setSiteOfBusinessChgDesc(String siteOfBusinessChgDesc) {
		this.siteOfBusinessChgDesc = siteOfBusinessChgDesc;
	}
	
    /**
     * @return siteOfBusinessChgDesc
     */
	public String getSiteOfBusinessChgDesc() {
		return this.siteOfBusinessChgDesc;
	}
	
	/**
	 * @param isAddressPhoneChg
	 */
	public void setIsAddressPhoneChg(String isAddressPhoneChg) {
		this.isAddressPhoneChg = isAddressPhoneChg;
	}
	
    /**
     * @return isAddressPhoneChg
     */
	public String getIsAddressPhoneChg() {
		return this.isAddressPhoneChg;
	}
	
	/**
	 * @param addressPhoneChgDesc
	 */
	public void setAddressPhoneChgDesc(String addressPhoneChgDesc) {
		this.addressPhoneChgDesc = addressPhoneChgDesc;
	}
	
    /**
     * @return addressPhoneChgDesc
     */
	public String getAddressPhoneChgDesc() {
		return this.addressPhoneChgDesc;
	}
	
	/**
	 * @param isBussinessAssetsChg
	 */
	public void setIsBussinessAssetsChg(String isBussinessAssetsChg) {
		this.isBussinessAssetsChg = isBussinessAssetsChg;
	}
	
    /**
     * @return isBussinessAssetsChg
     */
	public String getIsBussinessAssetsChg() {
		return this.isBussinessAssetsChg;
	}
	
	/**
	 * @param bussinessAssetsChgDesc
	 */
	public void setBussinessAssetsChgDesc(String bussinessAssetsChgDesc) {
		this.bussinessAssetsChgDesc = bussinessAssetsChgDesc;
	}
	
    /**
     * @return bussinessAssetsChgDesc
     */
	public String getBussinessAssetsChgDesc() {
		return this.bussinessAssetsChgDesc;
	}
	
	/**
	 * @param isIndivCreditChg
	 */
	public void setIsIndivCreditChg(String isIndivCreditChg) {
		this.isIndivCreditChg = isIndivCreditChg;
	}
	
    /**
     * @return isIndivCreditChg
     */
	public String getIsIndivCreditChg() {
		return this.isIndivCreditChg;
	}
	
	/**
	 * @param indivCreditChgDesc
	 */
	public void setIndivCreditChgDesc(String indivCreditChgDesc) {
		this.indivCreditChgDesc = indivCreditChgDesc;
	}
	
    /**
     * @return indivCreditChgDesc
     */
	public String getIndivCreditChgDesc() {
		return this.indivCreditChgDesc;
	}
	
	/**
	 * @param isCorpCreditChg
	 */
	public void setIsCorpCreditChg(String isCorpCreditChg) {
		this.isCorpCreditChg = isCorpCreditChg;
	}
	
    /**
     * @return isCorpCreditChg
     */
	public String getIsCorpCreditChg() {
		return this.isCorpCreditChg;
	}
	
	/**
	 * @param corpCreditChgDesc
	 */
	public void setCorpCreditChgDesc(String corpCreditChgDesc) {
		this.corpCreditChgDesc = corpCreditChgDesc;
	}
	
    /**
     * @return corpCreditChgDesc
     */
	public String getCorpCreditChgDesc() {
		return this.corpCreditChgDesc;
	}
	
	/**
	 * @param isRelationChg
	 */
	public void setIsRelationChg(String isRelationChg) {
		this.isRelationChg = isRelationChg;
	}
	
    /**
     * @return isRelationChg
     */
	public String getIsRelationChg() {
		return this.isRelationChg;
	}
	
	/**
	 * @param relationChgDesc
	 */
	public void setRelationChgDesc(String relationChgDesc) {
		this.relationChgDesc = relationChgDesc;
	}
	
    /**
     * @return relationChgDesc
     */
	public String getRelationChgDesc() {
		return this.relationChgDesc;
	}
	
	/**
	 * @param isOperInfoChg
	 */
	public void setIsOperInfoChg(String isOperInfoChg) {
		this.isOperInfoChg = isOperInfoChg;
	}
	
    /**
     * @return isOperInfoChg
     */
	public String getIsOperInfoChg() {
		return this.isOperInfoChg;
	}
	
	/**
	 * @param operChgDesc
	 */
	public void setOperChgDesc(String operChgDesc) {
		this.operChgDesc = operChgDesc;
	}
	
    /**
     * @return operChgDesc
     */
	public String getOperChgDesc() {
		return this.operChgDesc;
	}
	
	/**
	 * @param isMainSubChg
	 */
	public void setIsMainSubChg(String isMainSubChg) {
		this.isMainSubChg = isMainSubChg;
	}
	
    /**
     * @return isMainSubChg
     */
	public String getIsMainSubChg() {
		return this.isMainSubChg;
	}
	
	/**
	 * @param mainSubChgDesc
	 */
	public void setMainSubChgDesc(String mainSubChgDesc) {
		this.mainSubChgDesc = mainSubChgDesc;
	}
	
    /**
     * @return mainSubChgDesc
     */
	public String getMainSubChgDesc() {
		return this.mainSubChgDesc;
	}
	
	/**
	 * @param isMainTargetChg
	 */
	public void setIsMainTargetChg(String isMainTargetChg) {
		this.isMainTargetChg = isMainTargetChg;
	}
	
    /**
     * @return isMainTargetChg
     */
	public String getIsMainTargetChg() {
		return this.isMainTargetChg;
	}
	
	/**
	 * @param mainTargetChgDesc
	 */
	public void setMainTargetChgDesc(String mainTargetChgDesc) {
		this.mainTargetChgDesc = mainTargetChgDesc;
	}
	
    /**
     * @return mainTargetChgDesc
     */
	public String getMainTargetChgDesc() {
		return this.mainTargetChgDesc;
	}
	
	/**
	 * @param isManageCondChg
	 */
	public void setIsManageCondChg(String isManageCondChg) {
		this.isManageCondChg = isManageCondChg;
	}
	
    /**
     * @return isManageCondChg
     */
	public String getIsManageCondChg() {
		return this.isManageCondChg;
	}
	
	/**
	 * @param mangeCondChgDesc
	 */
	public void setMangeCondChgDesc(String mangeCondChgDesc) {
		this.mangeCondChgDesc = mangeCondChgDesc;
	}
	
    /**
     * @return mangeCondChgDesc
     */
	public String getMangeCondChgDesc() {
		return this.mangeCondChgDesc;
	}
	
	/**
	 * @param isSaleChg
	 */
	public void setIsSaleChg(String isSaleChg) {
		this.isSaleChg = isSaleChg;
	}
	
    /**
     * @return isSaleChg
     */
	public String getIsSaleChg() {
		return this.isSaleChg;
	}
	
	/**
	 * @param saleChgDesc
	 */
	public void setSaleChgDesc(String saleChgDesc) {
		this.saleChgDesc = saleChgDesc;
	}
	
    /**
     * @return saleChgDesc
     */
	public String getSaleChgDesc() {
		return this.saleChgDesc;
	}
	
	/**
	 * @param isRelationCorpChg
	 */
	public void setIsRelationCorpChg(String isRelationCorpChg) {
		this.isRelationCorpChg = isRelationCorpChg;
	}
	
    /**
     * @return isRelationCorpChg
     */
	public String getIsRelationCorpChg() {
		return this.isRelationCorpChg;
	}
	
	/**
	 * @param relationCorpChgDesc
	 */
	public void setRelationCorpChgDesc(String relationCorpChgDesc) {
		this.relationCorpChgDesc = relationCorpChgDesc;
	}
	
    /**
     * @return relationCorpChgDesc
     */
	public String getRelationCorpChgDesc() {
		return this.relationCorpChgDesc;
	}
	
	/**
	 * @param isPinToBorrowThan
	 */
	public void setIsPinToBorrowThan(String isPinToBorrowThan) {
		this.isPinToBorrowThan = isPinToBorrowThan;
	}
	
    /**
     * @return isPinToBorrowThan
     */
	public String getIsPinToBorrowThan() {
		return this.isPinToBorrowThan;
	}
	
	/**
	 * @param pinToBorrowThanDesc
	 */
	public void setPinToBorrowThanDesc(String pinToBorrowThanDesc) {
		this.pinToBorrowThanDesc = pinToBorrowThanDesc;
	}
	
    /**
     * @return pinToBorrowThanDesc
     */
	public String getPinToBorrowThanDesc() {
		return this.pinToBorrowThanDesc;
	}
	
	/**
	 * @param isAssetLiabilityThan
	 */
	public void setIsAssetLiabilityThan(String isAssetLiabilityThan) {
		this.isAssetLiabilityThan = isAssetLiabilityThan;
	}
	
    /**
     * @return isAssetLiabilityThan
     */
	public String getIsAssetLiabilityThan() {
		return this.isAssetLiabilityThan;
	}
	
	/**
	 * @param assetLiabilityThanDesc
	 */
	public void setAssetLiabilityThanDesc(String assetLiabilityThanDesc) {
		this.assetLiabilityThanDesc = assetLiabilityThanDesc;
	}
	
    /**
     * @return assetLiabilityThanDesc
     */
	public String getAssetLiabilityThanDesc() {
		return this.assetLiabilityThanDesc;
	}
	
	/**
	 * @param isTwoYearLoss
	 */
	public void setIsTwoYearLoss(String isTwoYearLoss) {
		this.isTwoYearLoss = isTwoYearLoss;
	}
	
    /**
     * @return isTwoYearLoss
     */
	public String getIsTwoYearLoss() {
		return this.isTwoYearLoss;
	}
	
	/**
	 * @param twoYearLossDesc
	 */
	public void setTwoYearLossDesc(String twoYearLossDesc) {
		this.twoYearLossDesc = twoYearLossDesc;
	}
	
    /**
     * @return twoYearLossDesc
     */
	public String getTwoYearLossDesc() {
		return this.twoYearLossDesc;
	}
	
	/**
	 * @param isGuarChg
	 */
	public void setIsGuarChg(String isGuarChg) {
		this.isGuarChg = isGuarChg;
	}
	
    /**
     * @return isGuarChg
     */
	public String getIsGuarChg() {
		return this.isGuarChg;
	}
	
	/**
	 * @param guarChgDesc
	 */
	public void setGuarChgDesc(String guarChgDesc) {
		this.guarChgDesc = guarChgDesc;
	}
	
    /**
     * @return guarChgDesc
     */
	public String getGuarChgDesc() {
		return this.guarChgDesc;
	}
	
	/**
	 * @param isBasicCondChg
	 */
	public void setIsBasicCondChg(String isBasicCondChg) {
		this.isBasicCondChg = isBasicCondChg;
	}
	
    /**
     * @return isBasicCondChg
     */
	public String getIsBasicCondChg() {
		return this.isBasicCondChg;
	}
	
	/**
	 * @param basicCondChgDesc
	 */
	public void setBasicCondChgDesc(String basicCondChgDesc) {
		this.basicCondChgDesc = basicCondChgDesc;
	}
	
    /**
     * @return basicCondChgDesc
     */
	public String getBasicCondChgDesc() {
		return this.basicCondChgDesc;
	}
	
	/**
	 * @param isGuarCondChg
	 */
	public void setIsGuarCondChg(String isGuarCondChg) {
		this.isGuarCondChg = isGuarCondChg;
	}
	
    /**
     * @return isGuarCondChg
     */
	public String getIsGuarCondChg() {
		return this.isGuarCondChg;
	}
	
	/**
	 * @param guarCondChgDesc
	 */
	public void setGuarCondChgDesc(String guarCondChgDesc) {
		this.guarCondChgDesc = guarCondChgDesc;
	}
	
    /**
     * @return guarCondChgDesc
     */
	public String getGuarCondChgDesc() {
		return this.guarCondChgDesc;
	}
	
	/**
	 * @param isBorrowRelationChg
	 */
	public void setIsBorrowRelationChg(String isBorrowRelationChg) {
		this.isBorrowRelationChg = isBorrowRelationChg;
	}
	
    /**
     * @return isBorrowRelationChg
     */
	public String getIsBorrowRelationChg() {
		return this.isBorrowRelationChg;
	}
	
	/**
	 * @param borrowRelationChgDesc
	 */
	public void setBorrowRelationChgDesc(String borrowRelationChgDesc) {
		this.borrowRelationChgDesc = borrowRelationChgDesc;
	}
	
    /**
     * @return borrowRelationChgDesc
     */
	public String getBorrowRelationChgDesc() {
		return this.borrowRelationChgDesc;
	}
	
	/**
	 * @param isGuarSearchResultNormal
	 */
	public void setIsGuarSearchResultNormal(String isGuarSearchResultNormal) {
		this.isGuarSearchResultNormal = isGuarSearchResultNormal;
	}
	
    /**
     * @return isGuarSearchResultNormal
     */
	public String getIsGuarSearchResultNormal() {
		return this.isGuarSearchResultNormal;
	}
	
	/**
	 * @param lmtAmtChg
	 */
	public void setLmtAmtChg(java.math.BigDecimal lmtAmtChg) {
		this.lmtAmtChg = lmtAmtChg;
	}
	
    /**
     * @return lmtAmtChg
     */
	public java.math.BigDecimal getLmtAmtChg() {
		return this.lmtAmtChg;
	}
	
	/**
	 * @param lmtTermChg
	 */
	public void setLmtTermChg(Integer lmtTermChg) {
		this.lmtTermChg = lmtTermChg;
	}
	
    /**
     * @return lmtTermChg
     */
	public Integer getLmtTermChg() {
		return this.lmtTermChg;
	}
	
	/**
	 * @param loanBalanceChg
	 */
	public void setLoanBalanceChg(java.math.BigDecimal loanBalanceChg) {
		this.loanBalanceChg = loanBalanceChg;
	}
	
    /**
     * @return loanBalanceChg
     */
	public java.math.BigDecimal getLoanBalanceChg() {
		return this.loanBalanceChg;
	}
	
	/**
	 * @param indivLoanChg
	 */
	public void setIndivLoanChg(String indivLoanChg) {
		this.indivLoanChg = indivLoanChg;
	}
	
    /**
     * @return indivLoanChg
     */
	public String getIndivLoanChg() {
		return this.indivLoanChg;
	}
	
	/**
	 * @param corpLoanChg
	 */
	public void setCorpLoanChg(String corpLoanChg) {
		this.corpLoanChg = corpLoanChg;
	}
	
    /**
     * @return corpLoanChg
     */
	public String getCorpLoanChg() {
		return this.corpLoanChg;
	}
	
	/**
	 * @param consumeChg
	 */
	public void setConsumeChg(String consumeChg) {
		this.consumeChg = consumeChg;
	}
	
    /**
     * @return consumeChg
     */
	public String getConsumeChg() {
		return this.consumeChg;
	}
	
	/**
	 * @param indivAccessCondi
	 */
	public void setIndivAccessCondi(String indivAccessCondi) {
		this.indivAccessCondi = indivAccessCondi;
	}
	
    /**
     * @return indivAccessCondi
     */
	public String getIndivAccessCondi() {
		return this.indivAccessCondi;
	}
	
	/**
	 * @param corpAccessCondi
	 */
	public void setCorpAccessCondi(String corpAccessCondi) {
		this.corpAccessCondi = corpAccessCondi;
	}
	
    /**
     * @return corpAccessCondi
     */
	public String getCorpAccessCondi() {
		return this.corpAccessCondi;
	}
	
	/**
	 * @param stockFddCondi
	 */
	public void setStockFddCondi(String stockFddCondi) {
		this.stockFddCondi = stockFddCondi;
	}
	
    /**
     * @return stockFddCondi
     */
	public String getStockFddCondi() {
		return this.stockFddCondi;
	}
	
	/**
	 * @param supplementaryNotes
	 */
	public void setSupplementaryNotes(String supplementaryNotes) {
		this.supplementaryNotes = supplementaryNotes;
	}
	
    /**
     * @return supplementaryNotes
     */
	public String getSupplementaryNotes() {
		return this.supplementaryNotes;
	}
	
	/**
	 * @param repaymentSourceAnalysis
	 */
	public void setRepaymentSourceAnalysis(String repaymentSourceAnalysis) {
		this.repaymentSourceAnalysis = repaymentSourceAnalysis;
	}
	
    /**
     * @return repaymentSourceAnalysis
     */
	public String getRepaymentSourceAnalysis() {
		return this.repaymentSourceAnalysis;
	}


}