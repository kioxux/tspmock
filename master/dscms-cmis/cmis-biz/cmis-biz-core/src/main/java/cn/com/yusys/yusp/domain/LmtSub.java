/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSub
 * @类描述: lmt_sub数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-23 10:19:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sub")
public class LmtSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 授信额度编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "lmt_limit_no")
	private String lmtLimitNo;
	
	/** 全局流水号 **/
	@Column(name = "serno", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 业务申请编号 **/
	@Column(name = "lmt_serno", unique = false, nullable = false, length = 40)
	private String lmtSerno;
	
	/** 客户编号 **/
	@Column(name = "cus_id", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 分项类别 STD_ZB_SUB_TYP **/
	@Column(name = "sub_type", unique = false, nullable = true, length = 30)
	private String subType;
	
	/** 额度类型 STD_ZB_LMT_TYPE **/
	@Column(name = "limit_type", unique = false, nullable = true, length = 5)
	private String limitType;
	
	/** 额度品种编号 **/
	@Column(name = "lmt_variet_no", unique = false, nullable = true, length = 40)
	private String lmtVarietNo;
	
	/** 适用产品编号 **/
	@Column(name = "suit_prd_no", unique = false, nullable = true, length = 40)
	private String suitPrdNo;
	
	/** 授信额度 **/
	@Column(name = "lmt_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	@Column(name = "guar_way", unique = false, nullable = true, length = 5)
	private String guarWay;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "cur_type", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 是否预授信 STD_ZX_YES_NO **/
	@Column(name = "is_pre_lmt", unique = false, nullable = true, length = 5)
	private String isPreLmt;
	
	/** 原授信起始日 **/
	@Column(name = "lmt_start_date", unique = false, nullable = true, length = 20)
	private String lmtStartDate;
	
	/** 原授信到期日 **/
	@Column(name = "lmt_end_date", unique = false, nullable = true, length = 20)
	private String lmtEndDate;
	
	/** 是否调整期限 **/
	@Column(name = "is_adj_term", unique = false, nullable = true, length = 5)
	private String isAdjTerm;
	
	/** 授信期限类型 **/
	@Column(name = "lmt_term_type", unique = false, nullable = true, length = 5)
	private String lmtTermType;
	
	/** 授信期限 **/
	@Column(name = "lmt_term", unique = false, nullable = true, length = 3)
	private Integer lmtTerm;
	
	/** 绿色产业类型 **/
	@Column(name = "green_indus_type", unique = false, nullable = true, length = 100)
	private String greenIndusType;
	
	/** 绿色授信项目类型 STD_ZB_GREEN_PRO_TYP **/
	@Column(name = "green_pro_type", unique = false, nullable = true, length = 5)
	private String greenProType;
	
	/** 放款方式 STD_ZB_PUTOUT_TYP **/
	@Column(name = "pvp_way", unique = false, nullable = true, length = 5)
	private String pvpWay;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 渠道来源 **/
	@Column(name = "chnl_sour", unique = false, nullable = true, length = 5)
	private String chnlSour;
	
	/** 更新人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 原授信额度编号 **/
	@Column(name = "old_lmt_limit_no", unique = false, nullable = true, length = 40)
	private String oldLmtLimitNo;

	/**授信类型 **/
	@Column(name = "lmt_type", unique = false, nullable = true, length = 40)
	private String lmtType;

	/**
	 * @return lmtType
	 */
	public String getLmtType() {
		return lmtType;
	}

	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}

	/**
	 * @param lmtLimitNo
	 */
	public void setLmtLimitNo(String lmtLimitNo) {
		this.lmtLimitNo = lmtLimitNo;
	}
	
    /**
     * @return lmtLimitNo
     */
	public String getLmtLimitNo() {
		return this.lmtLimitNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param subType
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
    /**
     * @return subType
     */
	public String getSubType() {
		return this.subType;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	
    /**
     * @return limitType
     */
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param lmtVarietNo
	 */
	public void setLmtVarietNo(String lmtVarietNo) {
		this.lmtVarietNo = lmtVarietNo;
	}
	
    /**
     * @return lmtVarietNo
     */
	public String getLmtVarietNo() {
		return this.lmtVarietNo;
	}
	
	/**
	 * @param suitPrdNo
	 */
	public void setSuitPrdNo(String suitPrdNo) {
		this.suitPrdNo = suitPrdNo;
	}
	
    /**
     * @return suitPrdNo
     */
	public String getSuitPrdNo() {
		return this.suitPrdNo;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}
	
    /**
     * @return guarWay
     */
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param isPreLmt
	 */
	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt;
	}
	
    /**
     * @return isPreLmt
     */
	public String getIsPreLmt() {
		return this.isPreLmt;
	}
	
	/**
	 * @param lmtStartDate
	 */
	public void setLmtStartDate(String lmtStartDate) {
		this.lmtStartDate = lmtStartDate;
	}
	
    /**
     * @return lmtStartDate
     */
	public String getLmtStartDate() {
		return this.lmtStartDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param isAdjTerm
	 */
	public void setIsAdjTerm(String isAdjTerm) {
		this.isAdjTerm = isAdjTerm;
	}
	
    /**
     * @return isAdjTerm
     */
	public String getIsAdjTerm() {
		return this.isAdjTerm;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType;
	}

	/**
     * @return lmtTermType
     */
	public String getLmtTermType() {
		return lmtTermType;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param greenIndusType
	 */
	public void setGreenIndusType(String greenIndusType) {
		this.greenIndusType = greenIndusType;
	}
	
    /**
     * @return greenIndusType
     */
	public String getGreenIndusType() {
		return this.greenIndusType;
	}
	
	/**
	 * @param greenProType
	 */
	public void setGreenProType(String greenProType) {
		this.greenProType = greenProType;
	}
	
    /**
     * @return greenProType
     */
	public String getGreenProType() {
		return this.greenProType;
	}
	
	/**
	 * @param pvpWay
	 */
	public void setPvpWay(String pvpWay) {
		this.pvpWay = pvpWay;
	}
	
    /**
     * @return pvpWay
     */
	public String getPvpWay() {
		return this.pvpWay;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour;
	}
	
    /**
     * @return chnlSour
     */
	public String getChnlSour() {
		return this.chnlSour;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param oldLmtLimitNo
	 */
	public void setOldLmtLimitNo(String oldLmtLimitNo) {
		this.oldLmtLimitNo = oldLmtLimitNo;
	}
	
    /**
     * @return oldLmtLimitNo
     */
	public String getOldLmtLimitNo() {
		return this.oldLmtLimitNo;
	}


}