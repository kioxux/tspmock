/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpEntrustLoanApp
 * @类描述: pvp_entrust_loan_app数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-05-11 21:38:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_entrust_loan_app")
public class PvpEntrustLoanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 出账模式 **/
	@Column(name = "PVP_MODE", unique = false, nullable = true, length = 5)
	private String pvpMode;
	
	/** 合同影像是否审核 **/
	@Column(name = "IS_CONT_IMAGE_AUDIT", unique = false, nullable = true, length = 5)
	private String isContImageAudit;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 合同最高可放款金额 **/
	@Column(name = "CONT_HIGH_DISB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighDisb;
	
	/** 放款金额 **/
	@Column(name = "PVP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pvpAmt;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 贷款起始日期 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 贷款到期日期 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private String loanTerm;
	
	/** 贷款期限单位 **/
	@Column(name = "LOAN_TERM_UNIT", unique = false, nullable = true, length = 5)
	private String loanTermUnit;
	
	/** 利率调整方式 **/
	@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
	private String rateAdjMode;
	
	/** 是否分段计息 **/
	@Column(name = "IS_SEG_INTEREST", unique = false, nullable = true, length = 5)
	private String isSegInterest;
	
	/** LPR利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;
	
	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 逾期利率浮动比 **/
	@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRatePefloat;
	
	/** 逾期执行年利率 **/
	@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueExecRate;
	
	/** 复息利率浮动比 **/
	@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciRatePefloat;
	
	/** 复息执行年利率 **/
	@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciExecRate;
	
	/** 利率调整选项 **/
	@Column(name = "RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
	private String rateAdjType;
	
	/** 下一次利率调整间隔 **/
	@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private String nextRateAdjInterval;
	
	/** 下一次利率调整间隔单位 **/
	@Column(name = "NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 5)
	private String nextRateAdjUnit;
	
	/** 第一次调整日 **/
	@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 20)
	private String firstAdjDate;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 结息间隔周期 **/
	@Column(name = "EI_INTERVAL_CYCLE", unique = false, nullable = true, length = 10)
	private String eiIntervalCycle;
	
	/** 结息间隔周期单位 **/
	@Column(name = "EI_INTERVAL_UNIT", unique = false, nullable = true, length = 5)
	private String eiIntervalUnit;
	
	/** 扣款方式 **/
	@Column(name = "DEDUCT_TYPE", unique = false, nullable = true, length = 5)
	private String deductType;
	
	/** 扣款日 **/
	@Column(name = "DEDUCT_DAY", unique = false, nullable = true, length = 20)
	private String deductDay;
	
	/** 贷款发放账号 **/
	@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 40)
	private String loanPayoutAccno;
	
	/** 贷款发放账号子序号 **/
	@Column(name = "LOAN_PAYOUT_SUB_NO", unique = false, nullable = true, length = 40)
	private String loanPayoutSubNo;
	
	/** 发放账号名称 **/
	@Column(name = "PAYOUT_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String payoutAcctName;
	
	/** 是否受托支付 **/
	@Column(name = "IS_BE_ENTRUSTED_PAY", unique = false, nullable = true, length = 5)
	private String isBeEntrustedPay;
	
	/** 贷款还款账号 **/
	@Column(name = "REPAY_ACCNO", unique = false, nullable = true, length = 40)
	private String repayAccno;
	
	/** 贷款还款账户子序号 **/
	@Column(name = "REPAY_SUB_ACCNO", unique = false, nullable = true, length = 40)
	private String repaySubAccno;
	
	/** 还款账户名称 **/
	@Column(name = "REPAY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String repayAcctName;
	
	/** 是否贴息 **/
	@Column(name = "IS_SBSY", unique = false, nullable = true, length = 5)
	private String isSbsy;

	/** 是否节假日顺延 **/
	@Column(name = "IS_HOLIDAY_DELAY", unique = false, nullable = true, length = 5)
	private String isHolidayDelay;

	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 40)
	private String loanTer;
	
	/** 贷款科目号 **/
	@Column(name = "LOAN_SUBJECT_NO", unique = false, nullable = true, length = 20)
	private String loanSubjectNo;
	
	/** 借款用途类型 **/
	@Column(name = "LOAN_PURP_TYPE", unique = false, nullable = true, length = 5)
	private String loanPurpType;
	
	/** 涉农贷款投向 **/
	@Column(name = "AGRI_LOAN_TER", unique = false, nullable = true, length = 5)
	private String agriLoanTer;
	
	/** 贷款承诺标志 **/
	@Column(name = "LOAN_PROMISE_FLAG", unique = false, nullable = true, length = 5)
	private String loanPromiseFlag;
	
	/** 贷款承诺类型 **/
	@Column(name = "LOAN_PROMISE_TYPE", unique = false, nullable = true, length = 5)
	private String loanPromiseType;
	
	/** 贴息人存款账号 **/
	@Column(name = "SBSY_DEP_ACCNO", unique = false, nullable = true, length = 40)
	private String sbsyDepAccno;
	
	/** 贴息比例 **/
	@Column(name = "SBSY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyPerc;
	
	/** 贴息到期日期 **/
	@Column(name = "SBYS_ENDDATE", unique = false, nullable = true, length = 20)
	private String sbysEnddate;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 委托人客户编号 **/
	@Column(name = "CONSIGNOR_CUS_ID", unique = false, nullable = true, length = 40)
	private String consignorCusId;
	
	/** 委托人客户名称 **/
	@Column(name = "CONSIGNOR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String consignorCusName;
	
	/** 委托人结算账号 **/
	@Column(name = "CONSIGNOR_ID_SETTL_ACCNO", unique = false, nullable = true, length = 40)
	private String consignorIdSettlAccno;
	
	/** 委托贷款手续费收取方式 **/
	@Column(name = "CSGN_LOAN_CHRG_COLLECT_TYPE", unique = false, nullable = true, length = 5)
	private String csgnLoanChrgCollectType;
	
	/** 委托贷款手续费比例 **/
	@Column(name = "CSGN_LOAN_CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csgnLoanChrgRate;
	
	/** 委托贷款手续费金额 **/
	@Column(name = "CSGN_LOAN_CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csgnLoanChrgAmt;
	
	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 放款机构编号 **/
	@Column(name = "DISB_ORG_NO", unique = false, nullable = true, length = 40)
	private String disbOrgNo;
	
	/** 放款机构名称 **/
	@Column(name = "DISB_ORG_NAME", unique = false, nullable = true, length = 80)
	private String disbOrgName;
	
	/** 资料全否 **/
	@Column(name = "FILE_SUF_FLAG", unique = false, nullable = true, length = 5)
	private String fileSufFlag;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}

	/**
	 * @return billNo
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param pvpMode
	 */
	public void setPvpMode(String pvpMode) {
		this.pvpMode = pvpMode;
	}
	
    /**
     * @return pvpMode
     */
	public String getPvpMode() {
		return this.pvpMode;
	}
	
	/**
	 * @param isContImageAudit
	 */
	public void setIsContImageAudit(String isContImageAudit) {
		this.isContImageAudit = isContImageAudit;
	}
	
    /**
     * @return isContImageAudit
     */
	public String getIsContImageAudit() {
		return this.isContImageAudit;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contHighDisb
	 */
	public void setContHighDisb(java.math.BigDecimal contHighDisb) {
		this.contHighDisb = contHighDisb;
	}
	
    /**
     * @return contHighDisb
     */
	public java.math.BigDecimal getContHighDisb() {
		return this.contHighDisb;
	}
	
	/**
	 * @param pvpAmt
	 */
	public void setPvpAmt(java.math.BigDecimal pvpAmt) {
		this.pvpAmt = pvpAmt;
	}
	
    /**
     * @return pvpAmt
     */
	public java.math.BigDecimal getPvpAmt() {
		return this.pvpAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param loanTermUnit
	 */
	public void setLoanTermUnit(String loanTermUnit) {
		this.loanTermUnit = loanTermUnit;
	}
	
    /**
     * @return loanTermUnit
     */
	public String getLoanTermUnit() {
		return this.loanTermUnit;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}
	
    /**
     * @return rateAdjMode
     */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param isSegInterest
	 */
	public void setIsSegInterest(String isSegInterest) {
		this.isSegInterest = isSegInterest;
	}
	
    /**
     * @return isSegInterest
     */
	public String getIsSegInterest() {
		return this.isSegInterest;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return curtLprRate
     */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}
	
    /**
     * @return overdueRatePefloat
     */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}
	
	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}
	
    /**
     * @return overdueExecRate
     */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}
	
	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}
	
    /**
     * @return ciRatePefloat
     */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}
	
	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}
	
    /**
     * @return ciExecRate
     */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}
	
    /**
     * @return rateAdjType
     */
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}
	
    /**
     * @return nextRateAdjInterval
     */
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit;
	}
	
    /**
     * @return nextRateAdjUnit
     */
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}
	
    /**
     * @return firstAdjDate
     */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle;
	}
	
    /**
     * @return eiIntervalCycle
     */
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}
	
	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit;
	}
	
    /**
     * @return eiIntervalUnit
     */
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}
	
	/**
	 * @param deductType
	 */
	public void setDeductType(String deductType) {
		this.deductType = deductType;
	}
	
    /**
     * @return deductType
     */
	public String getDeductType() {
		return this.deductType;
	}
	
	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay;
	}
	
    /**
     * @return deductDay
     */
	public String getDeductDay() {
		return this.deductDay;
	}
	
	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}
	
    /**
     * @return loanPayoutAccno
     */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}
	
	/**
	 * @param loanPayoutSubNo
	 */
	public void setLoanPayoutSubNo(String loanPayoutSubNo) {
		this.loanPayoutSubNo = loanPayoutSubNo;
	}
	
    /**
     * @return loanPayoutSubNo
     */
	public String getLoanPayoutSubNo() {
		return this.loanPayoutSubNo;
	}
	
	/**
	 * @param payoutAcctName
	 */
	public void setPayoutAcctName(String payoutAcctName) {
		this.payoutAcctName = payoutAcctName;
	}
	
    /**
     * @return payoutAcctName
     */
	public String getPayoutAcctName() {
		return this.payoutAcctName;
	}
	
	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay;
	}
	
    /**
     * @return isBeEntrustedPay
     */
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}
	
	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno;
	}
	
    /**
     * @return repayAccno
     */
	public String getRepayAccno() {
		return this.repayAccno;
	}
	
	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno;
	}
	
    /**
     * @return repaySubAccno
     */
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName;
	}
	
    /**
     * @return repayAcctName
     */
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param isSbsy
	 */
	public void setIsSbsy(String isSbsy) {
		this.isSbsy = isSbsy;
	}
	
    /**
     * @return isSbsy
     */
	public String getIsSbsy() {
		return this.isSbsy;
	}

	/**
	 * @param isHolidayDelay
	 */
	public void setIsHolidayDelay(String isHolidayDelay) {
		this.isHolidayDelay = isHolidayDelay;
	}

	/**
	 * @return isHolidayDelay
	 */
	public String getIsHolidayDelay() {
		return this.isHolidayDelay;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
	
    /**
     * @return loanTer
     */
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param loanSubjectNo
	 */
	public void setLoanSubjectNo(String loanSubjectNo) {
		this.loanSubjectNo = loanSubjectNo;
	}
	
    /**
     * @return loanSubjectNo
     */
	public String getLoanSubjectNo() {
		return this.loanSubjectNo;
	}
	
	/**
	 * @param loanPurpType
	 */
	public void setLoanPurpType(String loanPurpType) {
		this.loanPurpType = loanPurpType;
	}
	
    /**
     * @return loanPurpType
     */
	public String getLoanPurpType() {
		return this.loanPurpType;
	}
	
	/**
	 * @param agriLoanTer
	 */
	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer;
	}
	
    /**
     * @return agriLoanTer
     */
	public String getAgriLoanTer() {
		return this.agriLoanTer;
	}
	
	/**
	 * @param loanPromiseFlag
	 */
	public void setLoanPromiseFlag(String loanPromiseFlag) {
		this.loanPromiseFlag = loanPromiseFlag;
	}
	
    /**
     * @return loanPromiseFlag
     */
	public String getLoanPromiseFlag() {
		return this.loanPromiseFlag;
	}
	
	/**
	 * @param loanPromiseType
	 */
	public void setLoanPromiseType(String loanPromiseType) {
		this.loanPromiseType = loanPromiseType;
	}
	
    /**
     * @return loanPromiseType
     */
	public String getLoanPromiseType() {
		return this.loanPromiseType;
	}
	
	/**
	 * @param sbsyDepAccno
	 */
	public void setSbsyDepAccno(String sbsyDepAccno) {
		this.sbsyDepAccno = sbsyDepAccno;
	}
	
    /**
     * @return sbsyDepAccno
     */
	public String getSbsyDepAccno() {
		return this.sbsyDepAccno;
	}
	
	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}
	
    /**
     * @return sbsyPerc
     */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}
	
	/**
	 * @param sbysEnddate
	 */
	public void setSbysEnddate(String sbysEnddate) {
		this.sbysEnddate = sbysEnddate;
	}
	
    /**
     * @return sbysEnddate
     */
	public String getSbysEnddate() {
		return this.sbysEnddate;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param consignorCusId
	 */
	public void setConsignorCusId(String consignorCusId) {
		this.consignorCusId = consignorCusId;
	}
	
    /**
     * @return consignorCusId
     */
	public String getConsignorCusId() {
		return this.consignorCusId;
	}
	
	/**
	 * @param consignorCusName
	 */
	public void setConsignorCusName(String consignorCusName) {
		this.consignorCusName = consignorCusName;
	}
	
    /**
     * @return consignorCusName
     */
	public String getConsignorCusName() {
		return this.consignorCusName;
	}
	
	/**
	 * @param consignorIdSettlAccno
	 */
	public void setConsignorIdSettlAccno(String consignorIdSettlAccno) {
		this.consignorIdSettlAccno = consignorIdSettlAccno;
	}
	
    /**
     * @return consignorIdSettlAccno
     */
	public String getConsignorIdSettlAccno() {
		return this.consignorIdSettlAccno;
	}
	
	/**
	 * @param csgnLoanChrgCollectType
	 */
	public void setCsgnLoanChrgCollectType(String csgnLoanChrgCollectType) {
		this.csgnLoanChrgCollectType = csgnLoanChrgCollectType;
	}
	
    /**
     * @return csgnLoanChrgCollectType
     */
	public String getCsgnLoanChrgCollectType() {
		return this.csgnLoanChrgCollectType;
	}
	
	/**
	 * @param csgnLoanChrgRate
	 */
	public void setCsgnLoanChrgRate(java.math.BigDecimal csgnLoanChrgRate) {
		this.csgnLoanChrgRate = csgnLoanChrgRate;
	}
	
    /**
     * @return csgnLoanChrgRate
     */
	public java.math.BigDecimal getCsgnLoanChrgRate() {
		return this.csgnLoanChrgRate;
	}
	
	/**
	 * @param csgnLoanChrgAmt
	 */
	public void setCsgnLoanChrgAmt(java.math.BigDecimal csgnLoanChrgAmt) {
		this.csgnLoanChrgAmt = csgnLoanChrgAmt;
	}
	
    /**
     * @return csgnLoanChrgAmt
     */
	public java.math.BigDecimal getCsgnLoanChrgAmt() {
		return this.csgnLoanChrgAmt;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo;
	}
	
    /**
     * @return disbOrgNo
     */
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}
	
	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName;
	}
	
    /**
     * @return disbOrgName
     */
	public String getDisbOrgName() {
		return this.disbOrgName;
	}
	
	/**
	 * @param fileSufFlag
	 */
	public void setFileSufFlag(String fileSufFlag) {
		this.fileSufFlag = fileSufFlag;
	}
	
    /**
     * @return fileSufFlag
     */
	public String getFileSufFlag() {
		return this.fileSufFlag;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}