package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyChg
 * @类描述: lmt_crd_reply_chg数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:22:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCrdReplyChgDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 批复编号 **/
	private String replyNo;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品代码 **/
	private String prdId;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户姓名 **/
	private String cusName;
	
	/** 原批复额度 **/
	private java.math.BigDecimal oldReplyAmt;
	
	/** 原批复期限 **/
	private Integer oldReplyTerm;
	
	/** 原批复利率 **/
	private java.math.BigDecimal oldReplyRate;
	
	/** 原还款方式 **/
	private String oldRepayMode;
	
	/** 原担保方式 **/
	private String oldGuarMode;
	
	/** 原用信条件 **/
	private String oldLoanCond;
	
	/** 原风控建议 **/
	private String oldRiskAdvice;
	
	/** 批复变更额度 **/
	private java.math.BigDecimal replyAmtChg;
	
	/** 批复变更期限 **/
	private Integer replyTermChg;
	
	/** 批复变更利率 **/
	private java.math.BigDecimal replyRateChg;
	
	/** 批复变更还款方式 **/
	private String repayModeChg;
	
	/** 批复变更担保方式 **/
	private String guarModeChg;
	
	/** 批复变更用信条件 **/
	private String loanCondChg;
	
	/** 批复变更风控建议 **/
	private String riskAdviceChg;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 备注 **/
	private String memo;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}
	
    /**
     * @return ReplyNo
     */	
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param oldReplyAmt
	 */
	public void setOldReplyAmt(java.math.BigDecimal oldReplyAmt) {
		this.oldReplyAmt = oldReplyAmt;
	}
	
    /**
     * @return OldReplyAmt
     */	
	public java.math.BigDecimal getOldReplyAmt() {
		return this.oldReplyAmt;
	}
	
	/**
	 * @param oldReplyTerm
	 */
	public void setOldReplyTerm(Integer oldReplyTerm) {
		this.oldReplyTerm = oldReplyTerm;
	}
	
    /**
     * @return OldReplyTerm
     */	
	public Integer getOldReplyTerm() {
		return this.oldReplyTerm;
	}
	
	/**
	 * @param oldReplyRate
	 */
	public void setOldReplyRate(java.math.BigDecimal oldReplyRate) {
		this.oldReplyRate = oldReplyRate;
	}
	
    /**
     * @return OldReplyRate
     */	
	public java.math.BigDecimal getOldReplyRate() {
		return this.oldReplyRate;
	}
	
	/**
	 * @param oldRepayMode
	 */
	public void setOldRepayMode(String oldRepayMode) {
		this.oldRepayMode = oldRepayMode == null ? null : oldRepayMode.trim();
	}
	
    /**
     * @return OldRepayMode
     */	
	public String getOldRepayMode() {
		return this.oldRepayMode;
	}
	
	/**
	 * @param oldGuarMode
	 */
	public void setOldGuarMode(String oldGuarMode) {
		this.oldGuarMode = oldGuarMode == null ? null : oldGuarMode.trim();
	}
	
    /**
     * @return OldGuarMode
     */	
	public String getOldGuarMode() {
		return this.oldGuarMode;
	}
	
	/**
	 * @param oldLoanCond
	 */
	public void setOldLoanCond(String oldLoanCond) {
		this.oldLoanCond = oldLoanCond == null ? null : oldLoanCond.trim();
	}
	
    /**
     * @return OldLoanCond
     */	
	public String getOldLoanCond() {
		return this.oldLoanCond;
	}
	
	/**
	 * @param oldRiskAdvice
	 */
	public void setOldRiskAdvice(String oldRiskAdvice) {
		this.oldRiskAdvice = oldRiskAdvice == null ? null : oldRiskAdvice.trim();
	}
	
    /**
     * @return OldRiskAdvice
     */	
	public String getOldRiskAdvice() {
		return this.oldRiskAdvice;
	}
	
	/**
	 * @param replyAmtChg
	 */
	public void setReplyAmtChg(java.math.BigDecimal replyAmtChg) {
		this.replyAmtChg = replyAmtChg;
	}
	
    /**
     * @return ReplyAmtChg
     */	
	public java.math.BigDecimal getReplyAmtChg() {
		return this.replyAmtChg;
	}
	
	/**
	 * @param replyTermChg
	 */
	public void setReplyTermChg(Integer replyTermChg) {
		this.replyTermChg = replyTermChg;
	}
	
    /**
     * @return ReplyTermChg
     */	
	public Integer getReplyTermChg() {
		return this.replyTermChg;
	}
	
	/**
	 * @param replyRateChg
	 */
	public void setReplyRateChg(java.math.BigDecimal replyRateChg) {
		this.replyRateChg = replyRateChg;
	}
	
    /**
     * @return ReplyRateChg
     */	
	public java.math.BigDecimal getReplyRateChg() {
		return this.replyRateChg;
	}
	
	/**
	 * @param repayModeChg
	 */
	public void setRepayModeChg(String repayModeChg) {
		this.repayModeChg = repayModeChg == null ? null : repayModeChg.trim();
	}
	
    /**
     * @return RepayModeChg
     */	
	public String getRepayModeChg() {
		return this.repayModeChg;
	}
	
	/**
	 * @param guarModeChg
	 */
	public void setGuarModeChg(String guarModeChg) {
		this.guarModeChg = guarModeChg == null ? null : guarModeChg.trim();
	}
	
    /**
     * @return GuarModeChg
     */	
	public String getGuarModeChg() {
		return this.guarModeChg;
	}
	
	/**
	 * @param loanCondChg
	 */
	public void setLoanCondChg(String loanCondChg) {
		this.loanCondChg = loanCondChg == null ? null : loanCondChg.trim();
	}
	
    /**
     * @return LoanCondChg
     */	
	public String getLoanCondChg() {
		return this.loanCondChg;
	}
	
	/**
	 * @param riskAdviceChg
	 */
	public void setRiskAdviceChg(String riskAdviceChg) {
		this.riskAdviceChg = riskAdviceChg == null ? null : riskAdviceChg.trim();
	}
	
    /**
     * @return RiskAdviceChg
     */	
	public String getRiskAdviceChg() {
		return this.riskAdviceChg;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}