/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppXw;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-06 14:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpLoanAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     * @return
     */
    
    IqpLoanApp selectByPrimaryKey(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpLoanApp> selectByModel(QueryModel model);

    /**
     * @param model
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author hubp
     * @date 2021/6/28 20:33
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<IqpLoanAppXw> xwSelectByModel(QueryModel model);

    /***
     * @param model
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpLoanApp>>
     * @author hubp
     * @date 2021/4/27 21:56
     * @version 1.0.0
     * @desc    根据申请状态查询业务申请历史信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<IqpLoanApp> selectByStatus(QueryModel model);
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpLoanApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpLoanApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpLoanApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpLoanApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    HashMap<String, Object> getBaseInfoDto(String iqpSerno);
    /**
     * 通过入参集合查询业务申请信息
     * @param params
     * @return
     */
    List<IqpLoanApp> selectIqpLoanAppListByParams(Map params);
    /**
     * 通过业务申请主键进行逻辑删除，即修改opr_type
     * @param delMap
     * @return
     */
    int updateByParams(Map delMap);

    int updateApproveStatus(@Param("iqpSerno") String iqpSerno, @Param("approveStatus") String approveStatus);

    /**
     * 通过第三方额度id查询符合条件的流程中的申请信息
     * @param params
     * @return
     */
    List<IqpLoanApp> selectIqpLoanAppByThLimitId(Map params);


    HashMap<String, String> getDoReconsidFromBk(@Param("iqpSerno") String iqpSerno);

    /**
     * 查看拒绝记录
     * @param cusId
     * @return
     */
    int getCusRejectByCusId(@Param("cusId") String cusId);

    /**
     * 根据协议编号查询所有下属合同的业务申请流水号
     * @param lmtCtrNo
     * @return
     */
    List<String> getIqpSernoNumbers(String lmtCtrNo);

    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     * @param lmtCtrNo
     * @return
     */
    Integer getZaiTuAppCount(String lmtCtrNo);

    /**
     * 根据个人授信额度分项编号 查询在途的业务申请（特殊业务、额度项下业务）申请流水号
     * @param lmtLimitNos
     * @return
     */

    List<String> getIqpsernoByLmtCtrNo(@Param("lmtLimitNos") String lmtLimitNos);

    /**
     * 交易码：selectIqpLoanAppCountByCreditNo
     * 交易描述：查询合同申请表中是否存在在途的线上房抵e点申请
     *
     * @param paramMap
     * @return
     * @throws Exception
     */
    int selectFdeddIqpLoanAppCountByCusId(HashMap<String, String> paramMap);

    /**
     * 根据传入编号 查询是否有授信编号相同的数据
     * @param surveyNo
     * @return
     */
    IqpLoanApp selectBySurveyNo(String surveyNo);

    /**
     * @方法名称: selectIqpLoanAppListData
     * @方法描述: 业务申请 条件列表 查询方法重写
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpLoanApp> selectIqpLoanAppListData(QueryModel model);

    /**
     * @方法名称: selectIqpLoanAppHisListData
     * @方法描述: 业务申请 条件历史列表 查询方法重写
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpLoanApp> selectIqpLoanAppHisListData(QueryModel model);

    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:17
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<IqpLoanApp> selectForLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    IqpLoanApp selectBySerno(String serno);

    /**
     * @param cusId
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author hubp
     * @date 2021/5/29 20:42
     * @version 1.0.0
     * @desc    根据证件号码查询申请信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<IqpLoanApp> selectByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号获取申请列表
     * @param cusId
     * @return
     */
    List findPreApplyList(@Param("cusId") String cusId);

    /**
     * 根据合同号更新approve_status=‘997’
     * @param contNo
     * @return
     */
    int updateByContNo(String contNo);

    /**
     * 查授信台账号对应非待发起的用信申请
     * @param lmtAccNo
     * @return
     */
    String selectOnApproveIqpByLmtAccNo(String lmtAccNo);

    /**
     * 通过批复编号获取手机银行影像流水号
     * @param replyNo
     * @return
     * @创建者：zhangliang15
     */
    String selectThirdChnlSernoByReplyNo(String replyNo);

    /**
     * @函数名称:deleteByAccSubNo
     * @函数描述:根据授信协议分项流水号关联删除贷款申请
     * @参数与返回说明:accSubNo 授信协议分项流水号
     * @算法描述:
     */
    int deleteByAccSubNo(String accSubNo);

    IqpLoanApp selectByIqpSerno(String iqpSerno);

    /**
     * @函数名称:selectCdCount
     * @函数描述:根据流水号查询查询是否存在"存单质押"的方式
     * @参数与返回说明:accSubNo 流水号
     * @算法描述:
     */
    int selectCdCount(String iqpSerno);

    /**
     * @函数名称:selectFdcCount
     * @函数描述:根据流水号查询查询是否存在"房地产"
     * @参数与返回说明:accSubNo 流水号
     * @算法描述:
     */
    int selectFdcCount(String iqpSerno);

    /**
     * @函数名称:getAllIqpByInputId
     * @函数描述:根据客户经理工号查询合同申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> getAllIqpByInputId(QueryModel model);

    /**
     * @函数名称:selectXwztCount
     * @函数描述:根据客户号查询是否有在途业务-小微客户批量移交
     * @参数与返回说明:accSubNo 流水号
     * @算法描述:
     */
    int selectXwztCount(String cusId);

    /**
     * @函数名称:getCtrContByCusIdAndBizType
     * @函数描述:查询贸易融资合同
     * @参数与返回说明:List<IqpLoanApp>
     * @算法描述:
     */
    List<IqpLoanApp> getContByCusIdAndBizType(Map param);




    /**
     * 根据客户号查所有零售按揭审批中的贷款金额
     * @param cusId
     * @return
     */
    BigDecimal selectAppAmtAj(@Param("cusId")String cusId);

    /**
     * 根据客户号查所有零售非按揭审批中的贷款金额
     * @param params
     * @return
     */
    BigDecimal selectAppAmtFaj(Map params);

}