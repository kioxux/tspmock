/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankApprSub;
import cn.com.yusys.yusp.service.LmtIntbankApprService;
import cn.com.yusys.yusp.service.LmtIntbankApprSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankApprSubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-26 08:56:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankapprsub")
public class LmtIntbankApprSubResource {
    @Autowired
    private LmtIntbankApprSubService lmtIntbankApprSubService;
    @Autowired
    private LmtIntbankApprService lmtIntbankApprService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankApprSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankApprSub> list = lmtIntbankApprSubService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankApprSub>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankApprSub>> index(QueryModel queryModel) {
        List<LmtIntbankApprSub> list = lmtIntbankApprSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankApprSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankApprSub> show(@PathVariable("pkId") String pkId) {
        LmtIntbankApprSub lmtIntbankApprSub = lmtIntbankApprSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankApprSub>(lmtIntbankApprSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankApprSub> create(@RequestBody LmtIntbankApprSub lmtIntbankApprSub) throws URISyntaxException {
        lmtIntbankApprSubService.insert(lmtIntbankApprSub);
        return new ResultDto<LmtIntbankApprSub>(lmtIntbankApprSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankApprSub lmtIntbankApprSub) throws URISyntaxException {
        int result = lmtIntbankApprSubService.update(lmtIntbankApprSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankApprSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankApprSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据审批编号查询
     *
     * @param model
     * @return
     */
    @PostMapping("/selectByApproveSerno")
    protected ResultDto<List<LmtIntbankApprSub>> selectByApproveSerno(@RequestBody QueryModel model) {
        return new ResultDto<List<LmtIntbankApprSub>>(lmtIntbankApprSubService.selectByModel(model));
    }

    /**
     * 动态修改审批分项
     *
     * @param lmtIntbankApprSub
     * @return
     */
    @PostMapping("/updateApprSub")
    protected ResultDto<Integer> updateApprSub(@RequestBody LmtIntbankApprSub lmtIntbankApprSub) {
        return new ResultDto<Integer>(lmtIntbankApprSubService.updateSelective(lmtIntbankApprSub));
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtIntbankApprSub>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<LmtIntbankApprSub>>(lmtIntbankApprSubService.selectByModel(model));
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody QueryModel queryModel) {
        String pkId = (String) queryModel.getCondition().get("pkId");
        int result = lmtIntbankApprSubService.logicDelete(pkId);
        String serno = (String) queryModel.getCondition().get("serno");
        lmtIntbankApprService.updateLmtAmt(serno);
        return new ResultDto<Integer>(result);
    }
}
