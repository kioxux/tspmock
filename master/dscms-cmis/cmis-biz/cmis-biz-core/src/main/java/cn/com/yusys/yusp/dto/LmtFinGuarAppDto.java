package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarApp
 * @类描述: lmt_fin_guar_app数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtFinGuarAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;

	/** 融资协议名称 **/
	private String finCtrName;
	
	/** 原业务流水号 **/
	private String oldSerno;
	
	/** 融资协议编号 **/
	private String finCtrNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 授信类型 STD_ZB_LMT_TYP **/
	private String lmtType;
	
	/** 担保类别 STD_ZB_GUAR_TYP **/
	private String guarType;
	
	/** 评级结果 STD_ZB_EVAL_RST **/
	private String evalResult;
	
	/** 担保放大倍数 **/
	private String guarBailMultiple;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 担保金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 单户限额 **/
	private java.math.BigDecimal sigAmt;
	
	/** 单笔限额 **/
	private java.math.BigDecimal oneAmt;
	
	/** 担保总敞口限额 **/
	private java.math.BigDecimal guarTotlSpac;
	
	/** 授信期限类型 STD_ZB_TERM_TYP **/
	private String lmtTermType;
	
	/** 授信期限 **/
	private java.math.BigDecimal lmtTerm;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改时间 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param finCtrName
	 */
	public void setFinCtrName(String finCtrName) {
		this.finCtrName = finCtrName == null ? null : finCtrName.trim();
	}

	/**
	 * @return finCtrName
	 */
	public String getFinCtrName() {
		return this.finCtrName;
	}
	
	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno == null ? null : oldSerno.trim();
	}
	
    /**
     * @return OldSerno
     */	
	public String getOldSerno() {
		return this.oldSerno;
	}
	
	/**
	 * @param finCtrNo
	 */
	public void setFinCtrNo(String finCtrNo) {
		this.finCtrNo = finCtrNo == null ? null : finCtrNo.trim();
	}
	
    /**
     * @return FinCtrNo
     */	
	public String getFinCtrNo() {
		return this.finCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param evalResult
	 */
	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult == null ? null : evalResult.trim();
	}
	
    /**
     * @return EvalResult
     */	
	public String getEvalResult() {
		return this.evalResult;
	}
	
	/**
	 * @param guarBailMultiple
	 */
	public void setGuarBailMultiple(String guarBailMultiple) {
		this.guarBailMultiple = guarBailMultiple == null ? null : guarBailMultiple.trim();
	}
	
    /**
     * @return GuarBailMultiple
     */	
	public String getGuarBailMultiple() {
		return this.guarBailMultiple;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return SigAmt
     */	
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return OneAmt
     */	
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param guarTotlSpac
	 */
	public void setGuarTotlSpac(java.math.BigDecimal guarTotlSpac) {
		this.guarTotlSpac = guarTotlSpac;
	}
	
    /**
     * @return GuarTotlSpac
     */	
	public java.math.BigDecimal getGuarTotlSpac() {
		return this.guarTotlSpac;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType == null ? null : lmtTermType.trim();
	}
	
    /**
     * @return LmtTermType
     */	
	public String getLmtTermType() {
		return this.lmtTermType;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(java.math.BigDecimal lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public java.math.BigDecimal getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}