package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareInfo
 * @类描述: lmt_guare_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-19 17:22:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGuareInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 云评估编号 **/
	private String cloudEvalNo;
	
	/** 抵押物类型 **/
	private String pawnType;
	
	/** 所有权人 **/
	private String owner;
	
	/** 所有权人客户编号 **/
	private String ownerCusNo;
	
	/** 所有权人证件号码 **/
	private String ownerCertNo;
	
	/** 土地性质 **/
	private String landCha;
	
	/** 房屋性质 **/
	private String houseCha;
	
	/** 地址 **/
	private String addr;
	
	/** 面积 **/
	private java.math.BigDecimal squ;
	
	/** 使用情况 **/
	private String utilCase;
	
	/** 评估金额 **/
	private java.math.BigDecimal evalAmt;
	
	/** 一低金额 **/
	private java.math.BigDecimal firstPldAmt;
	
	/** 一低余额 **/
	private java.math.BigDecimal firstPldBal;
	
	/** 该抵押物项下贷款总金额 **/
	private java.math.BigDecimal pawnLoanTotlAmt;
	
	/** 抵质押率 **/
	private java.math.BigDecimal pldimnRate;
	
	/** 是否一级学区房 **/
	private String isLevel1Sdr;
	
	/** 学区房地址 **/
	private String sdrAddr;
	
	/** 学区房名称 **/
	private String sdrName;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cloudEvalNo
	 */
	public void setCloudEvalNo(String cloudEvalNo) {
		this.cloudEvalNo = cloudEvalNo == null ? null : cloudEvalNo.trim();
	}
	
    /**
     * @return CloudEvalNo
     */	
	public String getCloudEvalNo() {
		return this.cloudEvalNo;
	}
	
	/**
	 * @param pawnType
	 */
	public void setPawnType(String pawnType) {
		this.pawnType = pawnType == null ? null : pawnType.trim();
	}
	
    /**
     * @return PawnType
     */	
	public String getPawnType() {
		return this.pawnType;
	}
	
	/**
	 * @param owner
	 */
	public void setOwner(String owner) {
		this.owner = owner == null ? null : owner.trim();
	}
	
    /**
     * @return Owner
     */	
	public String getOwner() {
		return this.owner;
	}
	
	/**
	 * @param ownerCusNo
	 */
	public void setOwnerCusNo(String ownerCusNo) {
		this.ownerCusNo = ownerCusNo == null ? null : ownerCusNo.trim();
	}
	
    /**
     * @return OwnerCusNo
     */	
	public String getOwnerCusNo() {
		return this.ownerCusNo;
	}
	
	/**
	 * @param ownerCertNo
	 */
	public void setOwnerCertNo(String ownerCertNo) {
		this.ownerCertNo = ownerCertNo == null ? null : ownerCertNo.trim();
	}
	
    /**
     * @return OwnerCertNo
     */	
	public String getOwnerCertNo() {
		return this.ownerCertNo;
	}
	
	/**
	 * @param landCha
	 */
	public void setLandCha(String landCha) {
		this.landCha = landCha == null ? null : landCha.trim();
	}
	
    /**
     * @return LandCha
     */	
	public String getLandCha() {
		return this.landCha;
	}
	
	/**
	 * @param houseCha
	 */
	public void setHouseCha(String houseCha) {
		this.houseCha = houseCha == null ? null : houseCha.trim();
	}
	
    /**
     * @return HouseCha
     */	
	public String getHouseCha() {
		return this.houseCha;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr == null ? null : addr.trim();
	}
	
    /**
     * @return Addr
     */	
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return Squ
     */	
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param utilCase
	 */
	public void setUtilCase(String utilCase) {
		this.utilCase = utilCase == null ? null : utilCase.trim();
	}
	
    /**
     * @return UtilCase
     */	
	public String getUtilCase() {
		return this.utilCase;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param firstPldAmt
	 */
	public void setFirstPldAmt(java.math.BigDecimal firstPldAmt) {
		this.firstPldAmt = firstPldAmt;
	}
	
    /**
     * @return FirstPldAmt
     */	
	public java.math.BigDecimal getFirstPldAmt() {
		return this.firstPldAmt;
	}
	
	/**
	 * @param firstPldBal
	 */
	public void setFirstPldBal(java.math.BigDecimal firstPldBal) {
		this.firstPldBal = firstPldBal;
	}
	
    /**
     * @return FirstPldBal
     */	
	public java.math.BigDecimal getFirstPldBal() {
		return this.firstPldBal;
	}
	
	/**
	 * @param pawnLoanTotlAmt
	 */
	public void setPawnLoanTotlAmt(java.math.BigDecimal pawnLoanTotlAmt) {
		this.pawnLoanTotlAmt = pawnLoanTotlAmt;
	}
	
    /**
     * @return PawnLoanTotlAmt
     */	
	public java.math.BigDecimal getPawnLoanTotlAmt() {
		return this.pawnLoanTotlAmt;
	}
	
	/**
	 * @param pldimnRate
	 */
	public void setPldimnRate(java.math.BigDecimal pldimnRate) {
		this.pldimnRate = pldimnRate;
	}
	
    /**
     * @return PldimnRate
     */	
	public java.math.BigDecimal getPldimnRate() {
		return this.pldimnRate;
	}
	
	/**
	 * @param isLevel1Sdr
	 */
	public void setIsLevel1Sdr(String isLevel1Sdr) {
		this.isLevel1Sdr = isLevel1Sdr == null ? null : isLevel1Sdr.trim();
	}
	
    /**
     * @return IsLevel1Sdr
     */	
	public String getIsLevel1Sdr() {
		return this.isLevel1Sdr;
	}
	
	/**
	 * @param sdrAddr
	 */
	public void setSdrAddr(String sdrAddr) {
		this.sdrAddr = sdrAddr == null ? null : sdrAddr.trim();
	}
	
    /**
     * @return SdrAddr
     */	
	public String getSdrAddr() {
		return this.sdrAddr;
	}
	
	/**
	 * @param sdrName
	 */
	public void setSdrName(String sdrName) {
		this.sdrName = sdrName == null ? null : sdrName.trim();
	}
	
    /**
     * @return SdrName
     */	
	public String getSdrName() {
		return this.sdrName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}