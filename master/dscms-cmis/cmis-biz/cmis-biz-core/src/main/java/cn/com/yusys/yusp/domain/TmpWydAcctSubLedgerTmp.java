/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydAcctSubLedger
 * @类描述: tmp_wyd_acct_sub_ledger数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_acct_sub_ledger_tmp")
public class TmpWydAcctSubLedgerTmp {
	
	/** 流水号 **/
	@Id
	@Column(name = "SERIALNO")
	private String serialno;
	
	/** 借据编号 **/
	@Column(name = "RELATIVEOBJECTNO", unique = false, nullable = true, length = 32)
	private String relativeobjectno;
	
	/** 记账机构 **/
	@Column(name = "ACCOUNTINGORGID", unique = false, nullable = true, length = 10)
	private String accountingorgid;
	
	/** 币种 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 10)
	private String currency;
	
	/** 余额方向 **/
	@Column(name = "DIRECTION", unique = false, nullable = true, length = 5)
	private String direction;
	
	/** 借方余额 **/
	@Column(name = "DEBITBALANCE", unique = false, nullable = true, length = 20)
	private String debitbalance;
	
	/** 贷方余额 **/
	@Column(name = "CREDITBALANCE", unique = false, nullable = true, length = 20)
	private String creditbalance;
	
	/** 借方当日发生额 **/
	@Column(name = "DEBITAMTDAY", unique = false, nullable = true, length = 20)
	private String debitamtday;
	
	/** 贷方当日发生额 **/
	@Column(name = "CREDITAMTDAY", unique = false, nullable = true, length = 20)
	private String creditamtday;
	
	/** 借方当月发生额 **/
	@Column(name = "DEBITAMTMONTH", unique = false, nullable = true, length = 20)
	private String debitamtmonth;
	
	/** 贷方当月发生额 **/
	@Column(name = "CREDITAMTMONTH", unique = false, nullable = true, length = 20)
	private String creditamtmonth;
	
	/** 借方当年发生额 **/
	@Column(name = "DEBITAMTYEAR", unique = false, nullable = true, length = 20)
	private String debitamtyear;
	
	/** 贷方当年发生额 **/
	@Column(name = "CREDITAMTYEAR", unique = false, nullable = true, length = 20)
	private String creditamtyear;
	
	/** 创建日期 **/
	@Column(name = "CREATEDATE", unique = false, nullable = true, length = 10)
	private String createdate;
	
	/** 更新日期 **/
	@Column(name = "UPDATEDATE", unique = false, nullable = true, length = 10)
	private String updatedate;
	
	/** 系统科目号 **/
	@Column(name = "ACCOUNTCODENO", unique = false, nullable = true, length = 20)
	private String accountcodeno;
	
	/** 银行科目号 **/
	@Column(name = "EXACCOUNTCODENO", unique = false, nullable = true, length = 20)
	private String exaccountcodeno;
	
	
	/**
	 * @param serialno
	 */
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	
    /**
     * @return serialno
     */
	public String getSerialno() {
		return this.serialno;
	}
	
	/**
	 * @param relativeobjectno
	 */
	public void setRelativeobjectno(String relativeobjectno) {
		this.relativeobjectno = relativeobjectno;
	}
	
    /**
     * @return relativeobjectno
     */
	public String getRelativeobjectno() {
		return this.relativeobjectno;
	}
	
	/**
	 * @param accountingorgid
	 */
	public void setAccountingorgid(String accountingorgid) {
		this.accountingorgid = accountingorgid;
	}
	
    /**
     * @return accountingorgid
     */
	public String getAccountingorgid() {
		return this.accountingorgid;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param direction
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
    /**
     * @return direction
     */
	public String getDirection() {
		return this.direction;
	}
	
	/**
	 * @param debitbalance
	 */
	public void setDebitbalance(String debitbalance) {
		this.debitbalance = debitbalance;
	}
	
    /**
     * @return debitbalance
     */
	public String getDebitbalance() {
		return this.debitbalance;
	}
	
	/**
	 * @param creditbalance
	 */
	public void setCreditbalance(String creditbalance) {
		this.creditbalance = creditbalance;
	}
	
    /**
     * @return creditbalance
     */
	public String getCreditbalance() {
		return this.creditbalance;
	}
	
	/**
	 * @param debitamtday
	 */
	public void setDebitamtday(String debitamtday) {
		this.debitamtday = debitamtday;
	}
	
    /**
     * @return debitamtday
     */
	public String getDebitamtday() {
		return this.debitamtday;
	}
	
	/**
	 * @param creditamtday
	 */
	public void setCreditamtday(String creditamtday) {
		this.creditamtday = creditamtday;
	}
	
    /**
     * @return creditamtday
     */
	public String getCreditamtday() {
		return this.creditamtday;
	}
	
	/**
	 * @param debitamtmonth
	 */
	public void setDebitamtmonth(String debitamtmonth) {
		this.debitamtmonth = debitamtmonth;
	}
	
    /**
     * @return debitamtmonth
     */
	public String getDebitamtmonth() {
		return this.debitamtmonth;
	}
	
	/**
	 * @param creditamtmonth
	 */
	public void setCreditamtmonth(String creditamtmonth) {
		this.creditamtmonth = creditamtmonth;
	}
	
    /**
     * @return creditamtmonth
     */
	public String getCreditamtmonth() {
		return this.creditamtmonth;
	}
	
	/**
	 * @param debitamtyear
	 */
	public void setDebitamtyear(String debitamtyear) {
		this.debitamtyear = debitamtyear;
	}
	
    /**
     * @return debitamtyear
     */
	public String getDebitamtyear() {
		return this.debitamtyear;
	}
	
	/**
	 * @param creditamtyear
	 */
	public void setCreditamtyear(String creditamtyear) {
		this.creditamtyear = creditamtyear;
	}
	
    /**
     * @return creditamtyear
     */
	public String getCreditamtyear() {
		return this.creditamtyear;
	}
	
	/**
	 * @param createdate
	 */
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	
    /**
     * @return createdate
     */
	public String getCreatedate() {
		return this.createdate;
	}
	
	/**
	 * @param updatedate
	 */
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
    /**
     * @return updatedate
     */
	public String getUpdatedate() {
		return this.updatedate;
	}
	
	/**
	 * @param accountcodeno
	 */
	public void setAccountcodeno(String accountcodeno) {
		this.accountcodeno = accountcodeno;
	}
	
    /**
     * @return accountcodeno
     */
	public String getAccountcodeno() {
		return this.accountcodeno;
	}
	
	/**
	 * @param exaccountcodeno
	 */
	public void setExaccountcodeno(String exaccountcodeno) {
		this.exaccountcodeno = exaccountcodeno;
	}
	
    /**
     * @return exaccountcodeno
     */
	public String getExaccountcodeno() {
		return this.exaccountcodeno;
	}


}