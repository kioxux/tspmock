/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.RptOperationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperation;
import cn.com.yusys.yusp.service.RptOperationService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperationResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:33:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperation")
public class RptOperationResource {
    @Autowired
    private RptOperationService rptOperationService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperation>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperation> list = rptOperationService.selectAll(queryModel);
        return new ResultDto<List<RptOperation>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperation>> index(QueryModel queryModel) {
        List<RptOperation> list = rptOperationService.selectByModel(queryModel);
        return new ResultDto<List<RptOperation>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptOperation> show(@PathVariable("serno") String serno) {
        RptOperation rptOperation = rptOperationService.selectByPrimaryKey(serno);
        return new ResultDto<RptOperation>(rptOperation);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperation> create(@RequestBody RptOperation rptOperation) throws URISyntaxException {
        rptOperationService.insert(rptOperation);
        return new ResultDto<RptOperation>(rptOperation);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperation rptOperation) throws URISyntaxException {
        int result = rptOperationService.update(rptOperation);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptOperationService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperationService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    @PostMapping("/selectBySerno")
    protected ResultDto<RptOperation> selectBySerno(@RequestBody Map<String,Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptOperation>(rptOperationService.selectByPrimaryKey(serno));
    }
    @PostMapping("/updateRptOperation")
    protected ResultDto<Integer> updateRptOperation(@RequestBody RptOperation rptOperation){
        return new ResultDto<Integer>(rptOperationService.updateRptOperation(rptOperation));
    }

    @PostMapping("/insertRptOperation")
    protected ResultDto<Integer> insertRptOperation(@RequestBody RptOperation rptOperation){
        return new ResultDto<Integer>(rptOperationService.insertRptOperation(rptOperation));
    }

}
