/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: LmtReply
 * @类描述: lmt_reply数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:21:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_reply")
public class LmtReply extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String replySerno;
	
	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 原授信期限 **/
	@Column(name = "ORIGI_LMT_TERM", unique = false, nullable = true, length = 10)
	private String origiLmtTerm;
	
	/** 原授信宽限期 **/
	@Column(name = "ORIGI_LMT_GRAPER_TERM", unique = false, nullable = true, length = 10)
	private String origiLmtGraperTerm;
	
	/** 原敞口额度合计 **/
	@Column(name = "ORIGI_OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenTotalLmtAmt;
	
	/** 原低风险额度合计 **/
	@Column(name = "ORIGI_LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLowRiskTotalLmtAmt;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 终审机构 **/
	@Column(name = "", unique = false, nullable = true, length = 20)
	private String finalApprBrType;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;
	
	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;
	
	/** 用信审核方式 **/
	@Column(name = "LOAN_APPR_MODE", unique = false, nullable = true, length = 5)
	private String loanApprMode;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 敞口额度合计 **/
	@Column(name = "OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openTotalLmtAmt;
	
	/** 低风险额度合计 **/
	@Column(name = "LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskTotalLmtAmt;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 授信宽限期 **/
	@Column(name = "LMT_GRAPER_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtGraperTerm;

	/**
	 * 是否集团授信
	 **/
	@Column(name = "IS_GRP", unique = false, nullable = true, length = 5)
	private String isGrp;

	/** 委托人类型 **/
	@Column(name = "CONSIGNOR_TYPE", unique = false, nullable = true, length = 5)
	private String consignorType;
	
	/** 委托人客户编号 **/
	@Column(name = "CONSIGNOR_CUS_ID", unique = false, nullable = true, length = 40)
	private String consignorCusId;
	
	/** 委托人客户名称 **/
	@Column(name = "CONSIGNOR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String consignorCusName;
	
	/** 委托人证件号码 **/
	@Column(name = "CONSIGNOR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String consignorCertCode;
	
	/** 委托人证件类型 **/
	@Column(name = "CONSIGNOR_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String consignorCertType;
	
	/** 贷后管理要求 **/
	@Column(name = "PSP_MANA_NEED", unique = false, nullable = true, length = 4000)
	private String pspManaNeed;
	
	/** 批复生效日期 **/
	@Column(name = "REPLY_INURE_DATE", unique = false, nullable = true, length = 10)
	private String replyInureDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;

	/** 是否涉及阶段性担保 **/
	@Column(name = "IS_PER_GUR", unique = false, nullable = true, length = 5)
	private String isPerGur;

	/** 阶段性担保期限（月） **/
	@Column(name = "PER_GUR_TERM", unique = false, nullable = true, length = 10)
	private Integer perGurTerm;

	/** 是否按要求进行贷后管理  **/
	@Column(name = "IS_REQUEST_MANAGE_AFTER_LOAN", unique = false, nullable = true, length = 5)
	private String isRequestManageAfterLoan;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}
	
    /**
     * @return origiLmtReplySerno
     */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(String origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}
	
    /**
     * @return origiLmtTerm
     */
	public String getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}
	
	/**
	 * @param origiLmtGraperTerm
	 */
	public void setOrigiLmtGraperTerm(String origiLmtGraperTerm) {
		this.origiLmtGraperTerm = origiLmtGraperTerm;
	}
	
    /**
     * @return origiLmtGraperTerm
     */
	public String getOrigiLmtGraperTerm() {
		return this.origiLmtGraperTerm;
	}

	/**
	 * @param origiOpenTotalLmtAmt
	 */
	public void setOrigiOpenTotalLmtAmt(java.math.BigDecimal origiOpenTotalLmtAmt) {
		this.origiOpenTotalLmtAmt = origiOpenTotalLmtAmt;
	}
	
    /**
     * @return origiOpenTotalLmtAmt
     */
	public java.math.BigDecimal getOrigiOpenTotalLmtAmt() {
		return this.origiOpenTotalLmtAmt;
	}
	
	/**
	 * @param origiLowRiskTotalLmtAmt
	 */
	public void setOrigiLowRiskTotalLmtAmt(java.math.BigDecimal origiLowRiskTotalLmtAmt) {
		this.origiLowRiskTotalLmtAmt = origiLowRiskTotalLmtAmt;
	}
	
    /**
     * @return origiLowRiskTotalLmtAmt
     */
	public java.math.BigDecimal getOrigiLowRiskTotalLmtAmt() {
		return this.origiLowRiskTotalLmtAmt;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}
	
    /**
     * @return finalApprBrType
     */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}
	
    /**
     * @return replyStatus
     */
	public String getReplyStatus() {
		return this.replyStatus;
	}
	
	/**
	 * @param loanApprMode
	 */
	public void setLoanApprMode(String loanApprMode) {
		this.loanApprMode = loanApprMode;
	}
	
    /**
     * @return loanApprMode
     */
	public String getLoanApprMode() {
		return this.loanApprMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param openTotalLmtAmt
	 */
	public void setOpenTotalLmtAmt(java.math.BigDecimal openTotalLmtAmt) {
		this.openTotalLmtAmt = openTotalLmtAmt;
	}
	
    /**
     * @return openTotalLmtAmt
     */
	public java.math.BigDecimal getOpenTotalLmtAmt() {
		return this.openTotalLmtAmt;
	}
	
	/**
	 * @param lowRiskTotalLmtAmt
	 */
	public void setLowRiskTotalLmtAmt(java.math.BigDecimal lowRiskTotalLmtAmt) {
		this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
	}
	
    /**
     * @return lowRiskTotalLmtAmt
     */
	public java.math.BigDecimal getLowRiskTotalLmtAmt() {
		return this.lowRiskTotalLmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param lmtGraperTerm
	 */
	public void setLmtGraperTerm(Integer lmtGraperTerm) {
		this.lmtGraperTerm = lmtGraperTerm;
	}
	
    /**
     * @return lmtGraperTerm
     */
	public Integer getLmtGraperTerm() {
		return this.lmtGraperTerm;
	}

	/**
	 * @param isGrp
	 */
	public void setIsGrp(String isGrp) {
		this.isGrp = isGrp;
	}

	/**
	 * @return isGrp
	 */
	public String getIsGrp() {
		return this.isGrp;
	}

	/**
	 * @param consignorType
	 */
	public void setConsignorType(String consignorType) {
		this.consignorType = consignorType;
	}
	
    /**
     * @return consignorType
     */
	public String getConsignorType() {
		return this.consignorType;
	}
	
	/**
	 * @param consignorCusId
	 */
	public void setConsignorCusId(String consignorCusId) {
		this.consignorCusId = consignorCusId;
	}
	
    /**
     * @return consignorCusId
     */
	public String getConsignorCusId() {
		return this.consignorCusId;
	}
	
	/**
	 * @param consignorCusName
	 */
	public void setConsignorCusName(String consignorCusName) {
		this.consignorCusName = consignorCusName;
	}
	
    /**
     * @return consignorCusName
     */
	public String getConsignorCusName() {
		return this.consignorCusName;
	}
	
	/**
	 * @param consignorCertCode
	 */
	public void setConsignorCertCode(String consignorCertCode) {
		this.consignorCertCode = consignorCertCode;
	}
	
    /**
     * @return consignorCertCode
     */
	public String getConsignorCertCode() {
		return this.consignorCertCode;
	}
	
	/**
	 * @param consignorCertType
	 */
	public void setConsignorCertType(String consignorCertType) {
		this.consignorCertType = consignorCertType;
	}
	
    /**
     * @return consignorCertType
     */
	public String getConsignorCertType() {
		return this.consignorCertType;
	}
	
	/**
	 * @param pspManaNeed
	 */
	public void setPspManaNeed(String pspManaNeed) {
		this.pspManaNeed = pspManaNeed;
	}
	
    /**
     * @return pspManaNeed
     */
	public String getPspManaNeed() {
		return this.pspManaNeed;
	}
	
	/**
	 * @param replyInureDate
	 */
	public void setReplyInureDate(String replyInureDate) {
		this.replyInureDate = replyInureDate;
	}
	
    /**
     * @return replyInureDate
     */
	public String getReplyInureDate() {
		return this.replyInureDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param perGurTerm
	 */
	public void setPerGurTerm(Integer perGurTerm) {
		this.perGurTerm = perGurTerm;
	}

	/**
	 * @return perGurTerm
	 */
	public Integer getPerGurTerm() {
		return this.perGurTerm;
	}

	/**
	 * @return isPerGur
	 */
	public String getIsPerGur() {
		return isPerGur;
	}


	/**
	 * @param isPerGur
	 */
	public void setIsPerGur(String isPerGur) {
		this.isPerGur = isPerGur;
	}

	/**
	 * @return isRequestManageAfterLoan
	 */
	public String getIsRequestManageAfterLoan() {
		return isRequestManageAfterLoan;
	}


	/**
	 * @param isRequestManageAfterLoan
	 */
	public void setIsRequestManageAfterLoan(String isRequestManageAfterLoan) {
		this.isRequestManageAfterLoan = isRequestManageAfterLoan;
	}


}