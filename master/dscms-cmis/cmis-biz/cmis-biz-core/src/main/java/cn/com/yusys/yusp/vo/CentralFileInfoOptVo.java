package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.domain.CentralFileInfo;
import cn.com.yusys.yusp.domain.CentralFileOptRecord;

import java.util.List;

public class CentralFileInfoOptVo {
    private List<CentralFileInfo> centralfileinfoList;

    private CentralFileOptRecord centralfileoptrecord;

    public List<CentralFileInfo> getCentralfileinfoList() {
        return centralfileinfoList;
    }

    public void setCentralfileinfoList(List<CentralFileInfo> centralfileinfoList) {
        this.centralfileinfoList = centralfileinfoList;
    }

    public CentralFileOptRecord getCentralfileoptrecord() {
        return centralfileoptrecord;
    }

    public void setCentralfileoptrecord(CentralFileOptRecord centralfileoptrecord) {
        this.centralfileoptrecord = centralfileoptrecord;
    }
}
