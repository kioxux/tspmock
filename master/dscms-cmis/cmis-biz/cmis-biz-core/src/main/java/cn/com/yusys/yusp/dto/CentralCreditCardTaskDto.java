package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralCreditCardTask
 * @类描述: central_credit_card_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CentralCreditCardTaskDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 业务类型 **/
	private String bizType;
	
	/** 业务类型细分 **/
	private String bizSubType;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件号码 **/
	private String certType;
	
	/** 单位名称 **/
	private String cprtName;
	
	/** 申请卡产品 **/
	private String creditCardType;
	
	/** 申请渠道 **/
	private String appChnl;
	
	/** 任务生成时间 **/
	private String taskStartTime;
	
	/** 任务加急标识 **/
	private String taskUrgentFlag;
	
	/** 接收人 **/
	private String receiverId;
	
	/** 接收机构 **/
	private String receiverOrg;
	
	/** 任务状态 **/
	private String taskStatus;
	
	/** 审批状态 **/
	private String apprStatus;
	
	/** 作废原因 **/
	private String cancelResn;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType == null ? null : bizType.trim();
	}
	
    /**
     * @return BizType
     */	
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizSubType
	 */
	public void setBizSubType(String bizSubType) {
		this.bizSubType = bizSubType == null ? null : bizSubType.trim();
	}
	
    /**
     * @return BizSubType
     */	
	public String getBizSubType() {
		return this.bizSubType;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param cprtName
	 */
	public void setCprtName(String cprtName) {
		this.cprtName = cprtName == null ? null : cprtName.trim();
	}
	
    /**
     * @return CprtName
     */	
	public String getCprtName() {
		return this.cprtName;
	}
	
	/**
	 * @param creditCardType
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType == null ? null : creditCardType.trim();
	}
	
    /**
     * @return CreditCardType
     */	
	public String getCreditCardType() {
		return this.creditCardType;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl == null ? null : appChnl.trim();
	}
	
    /**
     * @return AppChnl
     */	
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param taskStartTime
	 */
	public void setTaskStartTime(String taskStartTime) {
		this.taskStartTime = taskStartTime == null ? null : taskStartTime.trim();
	}
	
    /**
     * @return TaskStartTime
     */	
	public String getTaskStartTime() {
		return this.taskStartTime;
	}
	
	/**
	 * @param taskUrgentFlag
	 */
	public void setTaskUrgentFlag(String taskUrgentFlag) {
		this.taskUrgentFlag = taskUrgentFlag == null ? null : taskUrgentFlag.trim();
	}
	
    /**
     * @return TaskUrgentFlag
     */	
	public String getTaskUrgentFlag() {
		return this.taskUrgentFlag;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId == null ? null : receiverId.trim();
	}
	
    /**
     * @return ReceiverId
     */	
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverOrg
	 */
	public void setReceiverOrg(String receiverOrg) {
		this.receiverOrg = receiverOrg == null ? null : receiverOrg.trim();
	}
	
    /**
     * @return ReceiverOrg
     */	
	public String getReceiverOrg() {
		return this.receiverOrg;
	}
	
	/**
	 * @param taskStatus
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus == null ? null : taskStatus.trim();
	}
	
    /**
     * @return TaskStatus
     */	
	public String getTaskStatus() {
		return this.taskStatus;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param cancelResn
	 */
	public void setCancelResn(String cancelResn) {
		this.cancelResn = cancelResn == null ? null : cancelResn.trim();
	}
	
    /**
     * @return CancelResn
     */	
	public String getCancelResn() {
		return this.cancelResn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}