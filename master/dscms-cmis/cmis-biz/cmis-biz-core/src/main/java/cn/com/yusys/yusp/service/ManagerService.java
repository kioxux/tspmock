package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/27 10:06
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class ManagerService {

    @Autowired
    private CommonService commonService;

    /**
     * @param managerId
     * @return cn.com.yusys.yusp.dto.DutyAndRoleDto
     * @author hubp
     * @date 2021/7/27 19:20
     * @version 1.0.0
     * @desc  查找角色和岗位信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public DutyAndRoleDto getManagerInfo (String managerId) {
        DutyAndRoleDto dutyAndRoleDto = new DutyAndRoleDto();
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        userAndDutyReqDto.setManagerId(managerId);
        List<UserAndDutyRespDto> userAndDutyRespDtos = commonService.getUserAndDuty(userAndDutyReqDto);
        if(userAndDutyRespDtos.size() > 0){
            dutyAndRoleDto.setDutyList(userAndDutyRespDtos);
        }
        CommonUserQueryReqDto commonUserQueryReqDto = new CommonUserQueryReqDto();
        commonUserQueryReqDto.setLoginCode(managerId);
        List<CommonUserQueryRespDto>  commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);
        if(commonUserQueryRespDtos.size() > 0){
            dutyAndRoleDto.setRoleList(commonUserQueryRespDtos);
        }
        return dutyAndRoleDto;
    }
}
