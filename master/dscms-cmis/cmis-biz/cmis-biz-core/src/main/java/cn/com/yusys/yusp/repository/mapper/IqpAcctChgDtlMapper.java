/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpAcctChgDtl;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctChgDtlMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-21 16:10:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpAcctChgDtlMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpAcctChgDtl selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpAcctChgDtl> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpAcctChgDtl record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpAcctChgDtl record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpAcctChgDtl record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpAcctChgDtl record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: checkExistIqpAcctDtl
     * @方法描述: 存在待变更的合同下帐号信息返回true，不存在返回false
     * @参数与返回说明:
     * @算法描述: 无
     */

    int checkExistIqpAcctDtl(@Param("cont_no") String cont_no);

    /**
     * @方法名称: deleteByIqpSerno
     * @方法描述: 根据申请流水号 执行 删除操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIqpSerno(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: insertDefValueAfterContAdd
     * @方法描述: 根据合同号执行 新增操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertDefValueAfterContAdd(@Param("iqpSerno") String iqpSerno);
}