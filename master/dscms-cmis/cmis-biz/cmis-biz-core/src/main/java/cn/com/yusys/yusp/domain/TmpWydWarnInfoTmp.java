/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydWarnInfo
 * @类描述: tmp_wyd_warn_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_warn_info_tmp")
public class TmpWydWarnInfoTmp {
	
	/** 流水号 **/
	@Id
	@Column(name = "BUSI_SNO")
	private String busiSno;
	
	/** 客户编号 **/
	@Column(name = "CUST_NO", unique = false, nullable = true, length = 32)
	private String custNo;
	
	/** 纳税人识别号 **/
	@Column(name = "TAX_PAYER_ID", unique = false, nullable = true, length = 25)
	private String taxPayerId;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 256)
	private String custName;
	
	/** 法定代表人 **/
	@Column(name = "LEGAL_PERSON", unique = false, nullable = true, length = 128)
	private String legalPerson;
	
	/** 身份证号码 **/
	@Column(name = "CERT_ID", unique = false, nullable = true, length = 100)
	private String certId;
	
	/** 预警日期 **/
	@Column(name = "BEGIN_DATE", unique = false, nullable = true, length = 8)
	private String beginDate;
	
	/** 预警结束日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 8)
	private String endDate;
	
	/** 预警类型 **/
	@Column(name = "WARN_TYPE", unique = false, nullable = true, length = 2)
	private String warnType;
	
	/** 命中预警规则描述 **/
	@Column(name = "RULE_DESC", unique = false, nullable = true)
	private String ruleDesc;
	
	
	/**
	 * @param busiSno
	 */
	public void setBusiSno(String busiSno) {
		this.busiSno = busiSno;
	}
	
    /**
     * @return busiSno
     */
	public String getBusiSno() {
		return this.busiSno;
	}
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param taxPayerId
	 */
	public void setTaxPayerId(String taxPayerId) {
		this.taxPayerId = taxPayerId;
	}
	
    /**
     * @return taxPayerId
     */
	public String getTaxPayerId() {
		return this.taxPayerId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param legalPerson
	 */
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	
    /**
     * @return legalPerson
     */
	public String getLegalPerson() {
		return this.legalPerson;
	}
	
	/**
	 * @param certId
	 */
	public void setCertId(String certId) {
		this.certId = certId;
	}
	
    /**
     * @return certId
     */
	public String getCertId() {
		return this.certId;
	}
	
	/**
	 * @param beginDate
	 */
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	
    /**
     * @return beginDate
     */
	public String getBeginDate() {
		return this.beginDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param warnType
	 */
	public void setWarnType(String warnType) {
		this.warnType = warnType;
	}
	
    /**
     * @return warnType
     */
	public String getWarnType() {
		return this.warnType;
	}
	
	/**
	 * @param ruleDesc
	 */
	public void setRuleDesc(String ruleDesc) {
		this.ruleDesc = ruleDesc;
	}
	
    /**
     * @return ruleDesc
     */
	public String getRuleDesc() {
		return this.ruleDesc;
	}


}