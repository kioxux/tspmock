package cn.com.yusys.yusp.web.server.xddb0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xddb0020.Xddb0020Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddb0020.req.Xddb0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0020.resp.Xddb0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0020:根据客户名查询抵押物类型")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0020Resource.class);

    @Autowired
    private Xddb0020Service xddb0020sService;

    /**
     * 交易码：xddb0020
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param xddb0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户名查询抵押物类型")
    @PostMapping("/xddb0020")
    protected @ResponseBody
    ResultDto<Xddb0020DataRespDto> xddb0020(@Validated @RequestBody Xddb0020DataReqDto xddb0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataReqDto));
        Xddb0020DataRespDto xddb0020DataRespDto = new Xddb0020DataRespDto();// 响应Dto:根据客户名查询抵押物类型
        ResultDto<Xddb0020DataRespDto> xddb0020DataResultDto = new ResultDto<>();
        String cus_name = xddb0020DataReqDto.getCus_name();//客户名称
        try {
            // 从xddb0020DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataReqDto));
            xddb0020DataRespDto = xddb0020sService.xddb0020(xddb0020DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataRespDto));

            // 封装xddb0020DataResultDto中正确的返回码和返回信息
            xddb0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, e.getMessage());
            // 封装xddb0020DataResultDto中异常返回码和返回信息
            xddb0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0020DataRespDto到xddb0020DataResultDto中
        xddb0020DataResultDto.setData(xddb0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataResultDto));
        return xddb0020DataResultDto;
    }
}
