package cn.com.yusys.yusp.web.server.xdxw0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0013.req.Xdxw0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0013.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0013.resp.Xdxw0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0013.Xdxw0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0013:申请人在本行当前逾期贷款数量")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0013Resource.class);

    @Autowired
    private Xdxw0013Service xdxw0013Service;

    /**
     * 交易码：xdxw0013
     * 交易描述：优企贷共借人、合同信息查询
     *
     * @param xdxw0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷共借人、合同信息查询")
    @PostMapping("/xdxw0013")
    protected @ResponseBody
    ResultDto<Xdxw0013DataRespDto> xdxw0013(@Validated @RequestBody Xdxw0013DataReqDto xdxw0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013DataReqDto));
        Xdxw0013DataRespDto xdxw0013DataRespDto = new Xdxw0013DataRespDto();// 响应Dto:优企贷共借人、合同信息查询
        ResultDto<Xdxw0013DataRespDto> xdxw0013DataResultDto = new ResultDto<>();
        String commonCertNo = xdxw0013DataReqDto.getCommonCertNo();//共借人证件号
        // 从xdxw0013DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0013Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, commonCertNo);
            java.util.List<List> lists = xdxw0013Service.selectCtrLoanContsByCommonCertNo(commonCertNo);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(lists));
            // 封装xdxw0013DataRespDto对象开始
            if (lists.size() == 0) {
                // 封装xdxw0013DataResultDto中正确的返回码和返回信息
                xdxw0013DataResultDto.setCode("1111");
                xdxw0013DataResultDto.setMessage("未查到共借人信息");
            } else {
                xdxw0013DataRespDto.setList(lists);
                xdxw0013DataRespDto.setContQnt(lists.size());
                // 封装xdxw0013DataResultDto中正确的返回码和返回信息
                xdxw0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, e.getMessage());
            // 封装xdxw0013DataResultDto中异常返回码和返回信息
            xdxw0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0013DataRespDto到xdxw0013DataResultDto中
        xdxw0013DataResultDto.setData(xdxw0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013DataResultDto));
        return xdxw0013DataResultDto;
    }
}
