/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfLivingRoom;
import cn.com.yusys.yusp.service.GuarInfLivingRoomService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfLivingRoomResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinflivingroom")
public class GuarInfLivingRoomResource {
    @Autowired
    private GuarInfLivingRoomService guarInfLivingRoomService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfLivingRoom>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfLivingRoom> list = guarInfLivingRoomService.selectAll(queryModel);
        return new ResultDto<List<GuarInfLivingRoom>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfLivingRoom>> index(QueryModel queryModel) {
        List<GuarInfLivingRoom> list = guarInfLivingRoomService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfLivingRoom>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfLivingRoom> show(@PathVariable("serno") String serno) {
        GuarInfLivingRoom guarInfLivingRoom = guarInfLivingRoomService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfLivingRoom>(guarInfLivingRoom);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfLivingRoom> create(@RequestBody GuarInfLivingRoom guarInfLivingRoom) throws URISyntaxException {
        guarInfLivingRoomService.insert(guarInfLivingRoom);
        return new ResultDto<GuarInfLivingRoom>(guarInfLivingRoom);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfLivingRoom guarInfLivingRoom) throws URISyntaxException {
        int result = guarInfLivingRoomService.update(guarInfLivingRoom);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfLivingRoomService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfLivingRoomService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGguarInfLivingRoom
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGguarInfLivingRoom")
    protected ResultDto<Integer> preserveGuarCargoPledge(@RequestBody GuarInfLivingRoom guarInfLivingRoom) {
        int result = guarInfLivingRoomService.preserveGguarInfLivingRoom(guarInfLivingRoom);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfLivingRoom> queryBySerno(@RequestBody GuarInfBusinessIndustryHousr record) {
        GuarInfLivingRoom guarInfLivingRoom = guarInfLivingRoomService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfLivingRoom>(guarInfLivingRoom);
    }
}
