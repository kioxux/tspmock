package cn.com.yusys.yusp.web.server.xddb0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0007.req.Xddb0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0007.resp.Xddb0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0007.Xddb0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:电票质押请求
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0007:信贷押品状态查询")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0007Resource.class);

    @Autowired
	private Xddb0007Service xddb0007Service;

    /**
     * 交易码：xddb0007
     * 交易描述：电票质押请求
     *
     * @param xddb0007DataReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("xddb0007:信贷押品状态查询")
    @PostMapping("/xddb0007")
    protected @ResponseBody
    ResultDto<Xddb0007DataRespDto> xddb0007(@Validated @RequestBody Xddb0007DataReqDto xddb0007DataReqDto) throws Exception {
		logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, JSON.toJSONString(xddb0007DataReqDto));
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();// 响应Dto:电票质押请求
        ResultDto<Xddb0007DataRespDto> xddb0007DataResultDto = new ResultDto<>();
        // 从xddb0007DataReqDto获取业务值进行业务逻辑处理
        try {
            // TODO 调用XXXXXService层开始
			xddb0007DataRespDto = xddb0007Service.xddb0007(xddb0007DataReqDto);
            // TODO 调用XXXXXService层结束
            // 封装xddb0007DataResultDto中正确的返回码和返回信息
            xddb0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException e) {
			logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
			// 封装xdkh0008DataResultDto中异常返回码和返回信息
			xddb0007DataResultDto.setCode(e.getErrorCode());
			xddb0007DataResultDto.setMessage(e.getMessage());
		}catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            // 封装xddb0007DataResultDto中异常返回码和返回信息
            xddb0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0007DataResultDto.setMessage(e.getMessage());
        }
        // 封装xddb0007DataRespDto到xddb0007DataResultDto中
        xddb0007DataResultDto.setData(xddb0007DataRespDto);
		logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, JSON.toJSONString(xddb0007DataResultDto));
        return xddb0007DataResultDto;
    }
}
