package cn.com.yusys.yusp.web.server.xdht0031;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0031.req.Xdht0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.Xdht0031.Xdht0031Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.JSONSerializer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询经营性贷款合同额度
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0031:根据核心客户号查询经营性贷款合同额度")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0031Resource.class);

    @Autowired
    private Xdht0031Service xdht0031Service;

    /**
     * 交易码：xdht0031
     * 交易描述：根据核心客户号查询经营性贷款合同额度
     *
     * @param xdht0031DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("根据核心客户号查询经营性贷款合同额度")
    @PostMapping("/xdht0031")
    protected @ResponseBody
    ResultDto<Xdht0031DataRespDto> xdht0031(@Validated @RequestBody Xdht0031DataReqDto xdht0031DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataReqDto));
        Xdht0031DataRespDto  xdht0031DataRespDto  = new Xdht0031DataRespDto();// 响应Dto:根据核心客户号查询经营性贷款合同额度
        ResultDto<Xdht0031DataRespDto> xdht0031DataResultDto = new ResultDto<>();
        String cusId = xdht0031DataReqDto.getCusId();//客户号
        try {
            // 从xdht0031DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataReqDto));
            //xdht0031DataRespDto = xdht0031Service.xdht0031(xdht0031DataReqDto);
            xdht0031DataRespDto = Optional.ofNullable(xdht0031Service.xdht0031(cusId))
                    .orElse(new Xdht0031DataRespDto(StringUtils.EMPTY));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataRespDto));
            // 封装xdht0031DataResultDto中正确的返回码和返回信息
            //xdht0031DataRespDto.setApplyAmt(StringUtils.EMPTY);//申请金额
            xdht0031DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0031DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value,e.getMessage());
            // 封装xdht0031DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0031DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0031DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0031DataRespDto到xdht0031DataResultDto中
        xdht0031DataResultDto.setData(xdht0031DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataRespDto));

        return xdht0031DataResultDto;
    }
}

