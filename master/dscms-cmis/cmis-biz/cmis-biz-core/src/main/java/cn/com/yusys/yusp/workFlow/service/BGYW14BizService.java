package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpChgTrupayAcctApp;
import cn.com.yusys.yusp.domain.IqpRateChgApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.req.Ln3055ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.resp.Ln3055RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CentralFileTaskService;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.IqpChgTrupayAcctAppService;
import cn.com.yusys.yusp.service.IqpRateChgAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 马顺
 * @version 1.0.0
 * @date 2021/7/2 19:37
 * @desc 受托支付账号变更
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW14BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(BGYW14BizService.class);//定义log

    @Autowired
    private IqpChgTrupayAcctAppService iqpChgTrupayAcctAppService;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 受托支付账号变更
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG025.equals(bizType)) {
            handleBG025BizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param
     * @return void
     * @author tangxun
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 利率变更申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void handleBG025BizApp(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "受托支付账号变更审批"+serno+"流程操作:";
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                createCentralFileTask4PvpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_BG025,resultInstanceDto) ;
                iqpChgTrupayAcctAppService.handleBusinessAfterStart(serno);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto.toString());
                iqpChgTrupayAcctAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpChgTrupayAcctAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpChgTrupayAcctAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                iqpChgTrupayAcctAppService.handleBusinessAfterRefuse(serno);
            }  else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }


    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW14.equals(flowCode);
    }

    public void createCentralFileTask4PvpLoanApp(String pkId,String bizType,ResultInstanceDto instanceInfo){
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = iqpChgTrupayAcctAppService.selectByPrimaryKey(pkId);
        //3、新增临时档案任务
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(pkId);
        centralFileTaskdto.setCusId(iqpChgTrupayAcctApp.getCusId());
        centralFileTaskdto.setCusName(iqpChgTrupayAcctApp.getCusName());
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(iqpChgTrupayAcctApp.getInputId());
        centralFileTaskdto.setInputBrId(iqpChgTrupayAcctApp.getInputBrId());
        centralFileTaskdto.setOptType("02"); // 非纯指令
        centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
        centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
        centralFileTaskdto.setTaskType("02"); // 档案暂存
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }
}
