/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtFrUfrAccRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAccRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:39:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtFrUfrAccRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtFrUfrAccRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtFrUfrAccRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtFrUfrAccRel record);
    
    /**
     * @方法名称: selectByParamsKey
     * @方法描述: 根据入参查询冻结/解冻额度台账关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    LmtFrUfrAccRel selectByParamsKey(@Param("lmtLimitNo") String lmtLimitNo,@Param("frUfrSerno") String frUfrSerno);
    
    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtFrUfrAccRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtFrUfrAccRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtFrUfrAccRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    
    /**
     * @方法名称: selectByFrUfrSerno
     * @方法描述: 根据入参查询冻结/解冻额度台账关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    List<LmtFrUfrAccRel> selectByFrUfrSerno(@Param("frUfrSerno") String frUfrSerno);

    /**
     * @方法名称: selectByLmtMap
     * @方法描述: 根据入参关联查询主子表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtFrUfrAccRel> selectByLmtMap(Map lmtMap);
    /**
     * @方法名称: selectByParamsMap
     * @方法描述: 根据入参关联查询主子表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map>  selectByParamsMap(Map lmtMap);
}