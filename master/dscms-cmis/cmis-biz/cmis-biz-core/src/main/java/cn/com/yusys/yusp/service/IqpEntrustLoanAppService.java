package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.CtrEntrustLoanCont;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.domain.LmtReplyAccSub;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.IqpEntrustLoanAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpEntrustLoanAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-15 14:31:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpEntrustLoanAppService {

    @Autowired
    private IqpEntrustLoanAppMapper iqpEntrustLoanAppMapper;

    private static final Logger log = LoggerFactory.getLogger(IqpEntrustLoanAppService.class);
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;//委托贷款合同主表

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;//BSP封装调用核心系统的接口

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同表

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private  AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpEntrustLoanApp selectByPrimaryKey(String pkId) {
        return iqpEntrustLoanAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpEntrustLoanApp> selectAll(QueryModel model) {
        List<IqpEntrustLoanApp> records = (List<IqpEntrustLoanApp>) iqpEntrustLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpEntrustLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpEntrustLoanApp> list = iqpEntrustLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpEntrustLoanApp record) {
        return iqpEntrustLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpEntrustLoanApp record) {
        return iqpEntrustLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpEntrustLoanApp record) {
        return iqpEntrustLoanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpEntrustLoanApp record) {
        return iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpEntrustLoanAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpEntrustLoanAppMapper.deleteByIds(ids);
    }

    /**
     * 委托贷款申请新增页面点击下一步
     * @param iqpEntrustLoanApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpEntrustLoanAppInfo(IqpEntrustLoanApp iqpEntrustLoanApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (iqpEntrustLoanApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpEntrustLoanApp.getCusId();
            // 将 地址与联系方式 信息更新值贷款申请表中
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",cusId);
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",cusId, cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpEntrustLoanApp.setPhone(CusIndivAllDto.getMobile());
                    iqpEntrustLoanApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpEntrustLoanApp.setEmail(CusIndivAllDto.getEmail());
                    iqpEntrustLoanApp.setQq(CusIndivAllDto.getQq());
                    iqpEntrustLoanApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpEntrustLoanApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpEntrustLoanApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpEntrustLoanApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpEntrustLoanApp.setFax(cusCorpDto.getFax());
                    iqpEntrustLoanApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpEntrustLoanApp.setQq(cusCorpDto.getQq());
                    iqpEntrustLoanApp.setWechat(cusCorpDto.getWechatNo());
                    iqpEntrustLoanApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpEntrustLoanApp.setInputId(userInfo.getLoginCode());
                iqpEntrustLoanApp.setInputBrId(userInfo.getOrg().getCode());
                iqpEntrustLoanApp.setManagerId(userInfo.getLoginCode());
                iqpEntrustLoanApp.setManagerBrId(userInfo.getOrg().getCode());
                iqpEntrustLoanApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }

            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO,seqMap);
            HashMap<String, String> param = new HashMap<>();
            String dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_09);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);

            iqpEntrustLoanApp.setSerno(serno);
            iqpEntrustLoanApp.setContNo(contNo);
            iqpEntrustLoanApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpEntrustLoanApp.setPkId(StringUtils.uuid(true));
            iqpEntrustLoanApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_09);
            iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpEntrustLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);


            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpEntrustLoanApp.getLmtAccNo());
            if(lmtReplyAccSubPrd!=null){
                iqpEntrustLoanApp.setCsgnLoanChrgCollectRate(lmtReplyAccSubPrd.getChrgRate());//手续费率
                iqpEntrustLoanApp.setCsgnLoanChrgCollectType(lmtReplyAccSubPrd.getChrgCollectMode());//手续费收取方式
                iqpEntrustLoanApp.setConsignorType(lmtReplyAccSubPrd.getConsignorType());//委托人类型
                iqpEntrustLoanApp.setConsignorCusId(lmtReplyAccSubPrd.getConsignorCusId());//委托人客户编号
                iqpEntrustLoanApp.setConsignorCusName(lmtReplyAccSubPrd.getConsignorCusName());//委托人客户名称
                iqpEntrustLoanApp.setConsignorCertCode(lmtReplyAccSubPrd.getConsignorCertCode());//委托人证件号码
                iqpEntrustLoanApp.setConsignorCertType(lmtReplyAccSubPrd.getConsignorCertType());//委托人证件类型
            }

            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpEntrustLoanApp.getLmtAccNo())){
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",iqpEntrustLoanApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpEntrustLoanApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            int insertCount = iqpEntrustLoanAppMapper.insertSelective(iqpEntrustLoanApp);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno",serno);
            result.put("contNo",contNo);
            log.info("委托贷款申请"+serno+"-保存成功！");
            //根据选择的额度分项自动生成担保合同
            IqpEntrustLoanApp iqpEntrustLoanAppData = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(serno);

            if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpEntrustLoanAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpEntrustLoanAppData.getGuarMode())
                    && !CmisCommonConstants.GUAR_MODE_40.equals(iqpEntrustLoanAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpEntrustLoanAppData.getGuarMode())) {
                String subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(iqpEntrustLoanAppData.getLmtAccNo());
                GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                grtGuarContDto.setSerno(iqpEntrustLoanAppData.getSerno());//业务流水号
                grtGuarContDto.setCusId(iqpEntrustLoanAppData.getCusId());//借款人编号
                grtGuarContDto.setBizLine(iqpEntrustLoanAppData.getBelgLine());//业务条线
                grtGuarContDto.setGuarWay(iqpEntrustLoanAppData.getGuarMode());//担保方式   //10抵押 20 质押 30保证
                grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                grtGuarContDto.setGuarAmt(iqpEntrustLoanAppData.getContAmt());//担保金额
                grtGuarContDto.setGuarTerm(iqpEntrustLoanAppData.getContTerm());//担保期限
                grtGuarContDto.setGuarStartDate(iqpEntrustLoanAppData.getStartDate());//担保起始日
                grtGuarContDto.setGuarEndDate(iqpEntrustLoanAppData.getEndDate());//担保终止日
                grtGuarContDto.setReplyNo(iqpEntrustLoanAppData.getReplyNo());//批复编号
                grtGuarContDto.setLmtAccNo(iqpEntrustLoanAppData.getLmtAccNo());//授信额度编号
                grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                grtGuarContDto.setCusName(iqpEntrustLoanAppData.getCusName());//借款人名称
                grtGuarContDto.setGuarContType(iqpEntrustLoanAppData.getContType());//担保合同类型
                grtGuarContDto.setInputId(iqpEntrustLoanAppData.getInputId());//登记人
                grtGuarContDto.setInputBrId(iqpEntrustLoanAppData.getInputBrId());//登记机构
                grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
            }

            // 生成用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpEntrustLoanAppData.getReplyNo());
            queryMap.put("contNo",iqpEntrustLoanAppData.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(iqpEntrustLoanApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            if ("1".equals(qryCls)) {
                creditReportQryLstAndRealDto.setQryResn("17");
            }
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        }catch(YuspException e){
            log.error("委托贷款申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("委托贷款申请异常！",e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }
    /**
     * @方法名称: deleteIqpEntrustLoanAppinfo
     * @方法描述: 根据主键对委托贷款申请明细信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 对委托贷款申请信息进行逻辑删除，更新对核销申请明细信息进行逻辑删除
     */
    @Transactional
    public Map deleteIqpEntrustLoanAppinfo(String iqpSerno) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            log.info(String.format("根据主键%s对委托贷款申请明细信息进行逻辑删除", iqpSerno));
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);

            log.info(String.format("根据主键%s对委托贷款申请明细信息进行逻辑删除,获取用户登录信息", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpEntrustLoanApp.setUpdId(userInfo.getLoginCode());
                iqpEntrustLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpEntrustLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对委托贷款申请信息进行逻辑删除,信息进行逻辑删除", iqpSerno));
            //iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            if(CmisCommonConstants.WF_STATUS_000.equals(iqpEntrustLoanApp.getApproveStatus())){
                //删除关联的担保合同及担保合同与押品关系表
                bizCommonService.logicDeleteGrtGuarCont(iqpEntrustLoanApp.getSerno());

                iqpEntrustLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            }else if(CmisCommonConstants.WF_STATUS_992.equals(iqpEntrustLoanApp.getApproveStatus())){
                iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
                ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpSerno);
                if (iqpEntrustLoanApp.getLmtAccNo() != null && !"".equals(iqpEntrustLoanApp.getLmtAccNo())) {
                    String guarMode = iqpEntrustLoanApp.getGuarMode();
                    //恢复敞口
                    BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                    //恢复总额
                    BigDecimal recoverAmtCny = BigDecimal.ZERO;
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                            || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                        recoverSpacAmtCny = BigDecimal.ZERO;
                        recoverAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                    } else {
                        recoverSpacAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                        recoverAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                    }
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(iqpEntrustLoanApp.getContNo());//合同编号
                    cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                    cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                    cmisLmt0012ReqDto.setInputId(iqpEntrustLoanApp.getInputId());
                    cmisLmt0012ReqDto.setInputBrId(iqpEntrustLoanApp.getInputBrId());
                    cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    String code = resultDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.error("业务申请恢复额度异常！");
                        throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除委托贷款申请信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpEntrustLoanApp selectByIqpEntrustLoanSernoKey(String iqpSerno) {
        return iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("流程发起-获取委托贷款申请" + iqpSerno + "申请主表信息");
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);
        if (iqpEntrustLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        int updateCount = 0;
        log.info("流程发起-更新委托贷款合同申请" + iqpSerno + "流程审批状态为【111】-审批中");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //向额度系统发送接口,占用额度
        this.sendToLmt(iqpEntrustLoanApp);
    }

    public void sendToLmt(IqpEntrustLoanApp iqpEntrustLoanApp) {
        //向额度系统发送接口,占用额度
        String guarMode = iqpEntrustLoanApp.getGuarMode();
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpEntrustLoanApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpEntrustLoanApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //不是低风险业务
        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode) && !CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                && !CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpEntrustLoanApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpEntrustLoanApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpEntrustLoanApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpEntrustLoanApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpEntrustLoanApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpEntrustLoanApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpEntrustLoanApp.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpEntrustLoanApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpEntrustLoanApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpEntrustLoanApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpEntrustLoanApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpEntrustLoanApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpEntrustLoanApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(iqpEntrustLoanApp.getContHighAvlAmt());//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpEntrustLoanApp.getContHighAvlAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpEntrustLoanApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpEntrustLoanApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }

        //是低风险且授信不足额的占额
        if ((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode))){

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpEntrustLoanApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpEntrustLoanApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpEntrustLoanApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpEntrustLoanApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpEntrustLoanApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpEntrustLoanApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpEntrustLoanApp.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpEntrustLoanApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpEntrustLoanApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpEntrustLoanApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpEntrustLoanApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpEntrustLoanApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpEntrustLoanApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpEntrustLoanApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpEntrustLoanApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("委托贷款合同申请否决流程-获取委托贷款合同申请" + iqpSerno + "申请信息");
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);
            if (iqpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (iqpEntrustLoanApp.getLmtAccNo() != null && !"".equals(iqpEntrustLoanApp.getLmtAccNo())) {
                String guarMode = iqpEntrustLoanApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                } else {
                    recoverSpacAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                    recoverAmtCny = iqpEntrustLoanApp.getContHighAvlAmt();
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpEntrustLoanApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpEntrustLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpEntrustLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("委托贷款合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("委托贷款合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + iqpSerno + "申请信息");
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);

            if (iqpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpEntrustLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpEntrustLoanAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取委托贷款合同申请" + iqpSerno + "申请主表信息");
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(iqpSerno);
        if (iqpEntrustLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        int updateCount = 0;
        log.info("审批通过-更新委托贷款业务申请" + iqpSerno + "流程审批状态为【997】-通过");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }

        log.info("审批通过生成获委托贷款现合同信息-合同填充业务申请" + iqpSerno + "开始");
        //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
        //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
        CtrEntrustLoanCont ctrEntrustLoanCont = new CtrEntrustLoanCont();
        BeanUtils.copyProperties(iqpEntrustLoanApp, ctrEntrustLoanCont);
        ctrEntrustLoanCont.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_09);
        ctrEntrustLoanCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);
        ctrEntrustLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);//合同状态默认【未生效】
        //iqpContHighAmtAgr.setContPrintNum(new BigDecimal("0"));//合同打印次数默认设置为【0】
        int insertCount = ctrEntrustLoanContService.insertSelective(ctrEntrustLoanCont);
        if (insertCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }
        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpEntrustLoanApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpEntrustLoanApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrEntrustLoanCont origCtrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(origiContNo);
                if(Objects.nonNull(origCtrEntrustLoanCont)) {
                    origCtrEntrustLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    updateCount = ctrEntrustLoanContService.updateSelective(origCtrEntrustLoanCont);
                    if (updateCount < 1) {
                        throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                    }
                }
            }
        }

        //发额度接口占额
        String guarMode = iqpEntrustLoanApp.getGuarMode();
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpEntrustLoanApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpEntrustLoanApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //判断是否是低风险的一步流程，是：进行低风险占额后再按照正常流程走；否：直接按照正常流程走
        if((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode))) {

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpEntrustLoanApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpEntrustLoanApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpEntrustLoanApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpEntrustLoanApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpEntrustLoanApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpEntrustLoanApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpEntrustLoanApp.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpEntrustLoanApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpEntrustLoanApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpEntrustLoanApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpEntrustLoanApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpEntrustLoanApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpEntrustLoanApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpEntrustLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("委托贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
        log.info("审批通过生成获委托贷款现合同信息-合同填充业务申请" + iqpSerno + "结束");
    }
    /**
     * 委托贷款申请提交保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpEntrustAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
                rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "委托贷款申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpEntrustLoanApp iqpEntrustLoanApp = JSONObject.parseObject(JSON.toJSONString(params), IqpEntrustLoanApp.class);
            if (iqpEntrustLoanApp == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存委托贷款额度申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpEntrustLoanApp.setUpdId(userInfo.getLoginCode());
                iqpEntrustLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpEntrustLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpEntrustLoanApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_09);
            }

            log.info(logPrefix + "保存委托贷款申请数据");
            int updCount = iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }
        } catch (Exception e) {
            log.error("保存委托贷款额度申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpEntrustLoanApp> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpEntrustLoanAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询已处理数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpEntrustLoanApp> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpEntrustLoanAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: getAccNoInfo
     * @方法描述: 发送核心查询客户账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto<Ib1253RespDto> getAccNoInfo(Map map) {
        Ib1253ReqDto  ib1253ReqDto = new Ib1253ReqDto();
        ib1253ReqDto.setKehuzhao((String)map.get("loanPayoutAccno"));//客户账号
        ib1253ReqDto.setZhhaoxuh("");//子账户序号
        ib1253ReqDto.setYanmbzhi("0");//密码校验方式 0--不校验1--校验查询密码 2--校验交易密码

        ib1253ReqDto.setMimammmm("");//密码
        ib1253ReqDto.setKehzhao2("");//客户账号2
        ib1253ReqDto.setShifoubz("0");//是否标志 1--是 0--否

        ib1253ReqDto.setZhufldm1("");//账户分类代码1
        ib1253ReqDto.setZhufldm2("");//账户分类代码2

        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        return resultDto;
    }

    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpEntrustLoanApp> selectForLmtAccNo(String lmtAccNo){
        return iqpEntrustLoanAppMapper.selectByLmtAccNo(lmtAccNo);
    }

    public ResultDto<List<CfgSorgLoanManyDto>> selecSorgLoanMany(QueryModel queryModel) {
        ResultDto<List<CfgSorgLoanManyDto>> RestltList = new ResultDto<>();
        String managerBrNo = (String)queryModel.getCondition().get("managerBrNo");
        QueryModel queryModelNew = new QueryModel();
        queryModelNew.getCondition().put("managerBrNo", managerBrNo);
        RestltList = iCmisCfgClientService.selecSorgLoanMany(queryModelNew);
        return RestltList;
    }

    public ResultDto<List<CfgSorgFinaDto>> selectSorgFina(QueryModel queryModel) {
        ResultDto<List<CfgSorgFinaDto>> restltList = new ResultDto<>();
        restltList = iCmisCfgClientService.selecSorgFina(queryModel);
        String jsonData = JSONObject.toJSONString(restltList.getData());
        List<CfgSorgFinaDto>  tempList = JSON.parseArray(jsonData, CfgSorgFinaDto.class);
        if(tempList.size()>0){
            for(int i=0;i<tempList.size();i++){
                CfgSorgFinaDto cfgSorgFinaDto = (CfgSorgFinaDto) tempList.get(i);
                String name = OcaTranslatorUtils.getOrgName(cfgSorgFinaDto.getFinaBrNo());
                cfgSorgFinaDto.setFinaBrName(name);
            }
        }
        return new ResultDto<List<CfgSorgFinaDto>>(tempList);
    }
    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpEntrustLoanApp selectBySerno(String serno) {
        return iqpEntrustLoanAppMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称: selectAccpOrg
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<List<CfgAccpOrgRelDto>> selectAccpOrg(QueryModel queryModel) {
        ResultDto<List<CfgAccpOrgRelDto>> RestltList = new ResultDto<>();
        RestltList = iCmisCfgClientService.selectAccpOrg(queryModel);
        List<CfgAccpOrgRelDto>  tempList = RestltList.getData();
        if(tempList.size()>0){
            for(int i=0;i<tempList.size();i++){
                CfgAccpOrgRelDto CfgAccpOrgRelDto = tempList.get(i);
                String name = OcaTranslatorUtils.getOrgName(CfgAccpOrgRelDto.getPayBrId());
                CfgAccpOrgRelDto.setPayBrName(name);
            }
        }
        return new ResultDto<List<CfgAccpOrgRelDto>>(tempList);
    }
    
    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 委托贷款申请判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpEntrustLoanApp iqpEntrustLoanApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("委托贷款申请判断是否足额,流水号【{}】", iqpEntrustLoanApp.getSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpEntrustLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpEntrustLoanApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpEntrustLoanApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" + iqpEntrustLoanApp.getLmtAccNo());
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        if(cmisLmt0026RespDto != null && iqpEntrustLoanApp.getContHighAvlAmt().compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 委托贷款申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("委托贷款申请更新流程参数,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpEntrustLoanApp iqpEntrustLoanApp = this.selectBySerno(serno);
        Map<String, Object> params = new HashMap<>();
        if(CmisCommonConstants.GUAR_MODE_60.equals(iqpEntrustLoanApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_21.equals(iqpEntrustLoanApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_40.equals(iqpEntrustLoanApp.getGuarMode())) {
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", iqpEntrustLoanApp.getContHighAvlAmt().add( iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpEntrustLoanApp.getCusId(), iqpEntrustLoanApp.getManagerBrId())));
        }else{
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", new BigDecimal("0"));
        }
        // 是否足额
        params.put("isAmtEnough", checkLmtAmtIsEnough(iqpEntrustLoanApp));
        // 申请金额
        params.put("amt", iqpEntrustLoanApp.getContHighAvlAmt());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    /**
     * @方法名称: iqpEntrustLoanSubmitNoFlow
     * @方法描述: 村镇银行无流程提交后续处理 参照DGYX01BizService
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map iqpEntrustLoanSubmitNoFlow(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            log.info("村镇银行委托贷款申请提交审批无流程处理开始：" + serno);
            // 1.针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
            this.handleBusinessDataAfterStart(serno);
            // 2.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
            this.handleBusinessDataAfterEnd(serno);
            log.info("村镇银行委托贷款申请提交审批无流程微信通知开始：" + serno);
            //微信通知
            IqpEntrustLoanApp iqpEntrustLoanApp = this.selectBySerno(serno);
            String managerId = iqpEntrustLoanApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", iqpEntrustLoanApp.getCusName());
                paramMap.put("prdName", "一般合同申请");
                paramMap.put("result", "通过");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                log.info("村镇银行委托贷款申请提交审批无流程处理结束：" + serno);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("村镇银行委托贷款申请提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: sendOnlinePldRemind
     * @方法描述: 首页消息提醒
     * @参数与返回说明:
     * @创建者：qw
     * @算法描述: 无
     */
    public void sendOnlinePldRemind(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        log.info("业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
        //针对流程到办结节点，进行以下处理
        //首页消息提醒
        IqpEntrustLoanApp iqpEntrustLoanApp = this.selectBySerno(serno);
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpEntrustLoanApp.getIsOlPld())){
            String managerId = iqpEntrustLoanApp.getManagerId();
            String managerBrId = iqpEntrustLoanApp.getManagerBrId();
            if (StringUtil.isNotEmpty(managerId)) {
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_DG_M_0001";//短信编号
                    // 翻译管护经理名称和管护经理机构名称
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
                    if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                    if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                    //短信填充参数
                    Map paramMap = new HashMap();
                    paramMap.put("cusName", iqpEntrustLoanApp.getCusName());
                    paramMap.put("managerId", managerName);
                    paramMap.put("managerBrId", managerBrName);
                    paramMap.put("instanceId", resultInstanceDto.getInstanceId());
                    paramMap.put("nodeSign", resultInstanceDto.getNodeSign());
                    //执行发送客户经理操作
                    messageCommonService.sendonlinepldremind(messageType, receivedUserType, paramMap);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
    }
}
