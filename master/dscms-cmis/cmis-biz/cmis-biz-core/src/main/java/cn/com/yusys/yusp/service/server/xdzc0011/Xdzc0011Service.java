package cn.com.yusys.yusp.service.server.xdzc0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0011.req.Xdzc0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0011.resp.Xdzc0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcfEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.core.ib1253.Ib1253Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3020.Ln3020Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:资产池超短贷放款
 *
 * @Author xs
 * @Date 2021/6/15 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0011Service {

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AsplWhtlsService asplWhtlsService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private ToppAcctSubService toppAcctSubServcie;

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private DocAsplTcontService docAsplTcontService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private AccTContRelService accTContRelService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0011.Xdzc0011Service.class);

    /**
     * 交易码：xdzc0011
     * 交易描述：
     * 1、资产池超短贷放款校验 禁止在月末最后2个工作日（支持可配置工作日时间）发起超短贷提款
     * 2、发起超短贷款出账申请（PVP_LOAN_APP插入数据），判断是否是资产池白名单，如果不是白名单用户，审批状态为待发起（000）
     *    如果是白名单用户，审批状态通过（997）
     * 3、出账授权 发核心接口ln3020
     * 4、贷款台账
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0011DataRespDto xdzc0011Service(Xdzc0011DataReqDto xdzc0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value);
        Xdzc0011DataRespDto xdzc0011DataRespDto = new Xdzc0011DataRespDto();
        xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0011DataRespDto.setOpMsg("出票校验逻辑报错");// 描述信息

        String contNo = xdzc0011DataReqDto.getContNo();//协议编号
        String tContImg = xdzc0011DataReqDto.getTcontNo(); // 购销合同流水号 tContSerno
        String tContSerno = xdzc0011DataReqDto.getTcontImgId();// 购销合同影像流水号
        BigDecimal appAmt = xdzc0011DataReqDto.getAppAmt();//申请金额
        String billStartDate = xdzc0011DataReqDto.getStartDate();//借据起始日
        String billEndDate = xdzc0011DataReqDto.getEndDate();//借据到期日
        int loanTerm = xdzc0011DataReqDto.getLoanTerm();//贷款期限
        String billNo = xdzc0011DataReqDto.getBillNo(); // 超短贷借据编号
        String billCnNo = xdzc0011DataReqDto.getBillCnNo(); // 超短贷借据中文编号
        String repayMode = xdzc0011DataReqDto.getRepayMode(); // 还款方式
        String dkyongtu =  xdzc0011DataReqDto.getDkyongtu(); // 贷款用途
        String finaBrId = xdzc0011DataReqDto.getFinaBrId();//账务机构
        String cusAccNo = xdzc0011DataReqDto.getCusAccNo();//客户账户
        String billSerno = xdzc0011DataReqDto.getBillSerno();//批次号
        String payAccNo = xdzc0011DataReqDto.getPayAccNo();//受托支付账号
        String payAccName = xdzc0011DataReqDto.getPayAccName();//受托支付账号名称
        String payOrgNo = xdzc0011DataReqDto.getPayOrgNo();//受托支付开户行号
        String payOrgName = xdzc0011DataReqDto.getPayOrgName();//受托支付开户行名称

        int work_days = 0;
        try {
            // 获取工作日配置参数
//            try{
//                AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
//                adminSmPropQueryDto.setPropName(CmisCommonConstants.WORK_DAYS);
//                String workDays = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
//                work_days =  Integer.parseInt(workDays);
//            } catch (Exception e){
//                logger.info("系统参数【超短贷工作日】【WORK_DAYS】获取失败");
//                xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
//                xdzc0011DataRespDto.setOpMsg("系统参数【超短贷工作日】【WORK_DAYS】获取失败");// 描述信息
//            }
//            logger.info("资产池超短贷放款-校验 禁止在月末最后" + work_days + "个工作日（支持可配置工作日时间）发起超短贷提款");
//            //为控制贷款规模，禁止在月末最后2个工作日（支持可配置工作日时间）发起超短贷提款；
//            String openDay = stringRedisTemplate.opsForValue().get("openDay");// 获取当前营业时间
//            Date nowDate =  DateUtils.parseDate(openDay, "yyyy-MM-dd");
//            // 调用配置服务下当接口
//            ResultDto<Integer> CmisCfgresult = iCmisCfgClientService.getWorkDaysByMonth(nowDate);
//            if ("0".equals(CmisCfgresult.getCode())) {
//                int days = CmisCfgresult.getData();
//                if(days < work_days){
//                    xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
//                    xdzc0011DataRespDto.setOpMsg(EcbEnum.E_WORK_DAY_02.value);// 描述信息
//                    logger.info(EcbEnum.E_WORK_DAY_02.value);
//                    return xdzc0011DataRespDto;
//                }
//            }else{
//                xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
//                xdzc0011DataRespDto.setOpMsg( EcfEnum.ECF040000.value +  "未能获取到当前日期距离月底有多少工作日");// 描述信息
//                return xdzc0011DataRespDto;
//            }
            // 资产超短贷出账校验校验
            OpRespDto opRespDto = ctrAsplDetailsService.isPvpLoanApp(contNo,appAmt,billStartDate,loanTerm);
            if(CmisBizConstants.FAIL.equals(opRespDto.getOpFlag())){
                BeanUtils.copyProperties(opRespDto,xdzc0011DataRespDto);
                return xdzc0011DataRespDto;
            }
            logger.info("资产池超短贷出账校验校验:" + opRespDto);
            //2、发起超短贷款出账申请（PVP_ENTRUST_LOAN_APP插入数据）
            //判断是否是资产池白名单 如果是 放款申请通过，执行后续的业务处理，不是则待审批，方法结束
            logger.info("资产池超短贷出账申请开始");
            String apprStatus = BizFlowConstant.WF_STATUS_000;
            // boolean isWhtlsByconNo = asplWhtlsService.isWhtlsByconNo(contNo);// 是否白名单
            //  默认都是白名单
            boolean isWhtlsByconNo = true;
            logger.info("资产池协议："+contNo+"，是否白名单："+isWhtlsByconNo);

            // TODO 改成拦截名单
            // 如果是白名单，审批通过
            if(isWhtlsByconNo){
                apprStatus = BizFlowConstant.WF_STATUS_997;
            }
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
            // 获取保证金账号
            String serno = ctrAsplDetails.getSerno();
            BailAccInfo bailAccInfo = bailAccInfoService.selectInfoBySerno(serno);
            // 批次号做出账流水
            String pvpSerno = billSerno;
            String iqpSerno = serno;
            // 获取购销(贸易)合同影像流水号
            DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tContSerno);
            // 生成贷款出账申请
            PvpLoanApp pvpLoanApp = getPvpLoanApp(xdzc0011DataReqDto,ctrAsplDetails,pvpSerno,iqpSerno,apprStatus,bailAccInfo,docAsplTcont);
            pvpLoanAppService.insertSelective(pvpLoanApp);
            // 交易对手录入
            insertToppAcctSub(ctrAsplDetails,xdzc0011DataReqDto,iqpSerno,docAsplTcont,pvpSerno);
            // TODO 后面改成拦截名单
            if(!isWhtlsByconNo){
                xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0011DataRespDto.setOpMsg("非白名单超短贷款出账，待审核");// 描述信息
                return xdzc0011DataRespDto;
            }
            // 调用出账申请审批通过流程处理类
            ResultDto resultDto = pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
            if (Objects.nonNull(resultDto)) {
                // 关联购销出账
                insertAccTContRel(pvpSerno, tContSerno, ctrAsplDetails,appAmt);
                if (Objects.equals("0", resultDto.getCode())) {
                    logger.info("资产池出账同步,额度系统开始占用额度(资产池)生效");
                    sendCmisLmt0013(pvpSerno,"200");
                    xdzc0011DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
                    xdzc0011DataRespDto.setOpMsg("出账成功");// 描述信息

                } else {
                    logger.info("资产池出账同步,额度系统开始占用额度(资产池)未生效");
                    sendCmisLmt0013(pvpSerno,"100");
                    xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                    xdzc0011DataRespDto.setOpMsg(resultDto.getMessage());// 描述信息
                }
            } else {
                xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0011DataRespDto.setOpMsg("出账处理流程失败");// 描述信息
            }

        } catch (BizException e) {
            xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0011DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdzc0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0011DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        } finally {
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value);
            return xdzc0011DataRespDto;
        }
    }

    /**
     * 交易对手录入
     * @param ctrAsplDetails
     * @param xdzc0011DataReqDto
     */
    private void insertToppAcctSub(CtrAsplDetails ctrAsplDetails, Xdzc0011DataReqDto xdzc0011DataReqDto,String iqpSerno,DocAsplTcont docAsplTcont,String pvpSerno) {
        logger.info("交易对手录入");
        // 交易对手账号
        ToppAcctSub toppAcctSub = new ToppAcctSub();
        toppAcctSub.setPkId(iqpSerno);// 主键
        toppAcctSub.setBizSerno(pvpSerno);// 业务主键号
        toppAcctSub.setBizSence(CmisBizConstants.BIZ_SCENCE_3);// 业务场景 出账申请3
        toppAcctSub.setIsOnline(CmisCommonConstants.STD_ZB_YES_NO_1); //是否线上
//        toppAcctSub.setToppAcctNo(docAsplTcont.getToppAcctNo());// 交易对手账号
//        toppAcctSub.setToppName(docAsplTcont.getToppName());// 交易对手名称
//        toppAcctSub.setAcctsvcrNo(docAsplTcont.getOpanOrgNo());// 开户行行号
//        toppAcctSub.setAcctsvcrName(docAsplTcont.getOpanOrgName());// 开户行名称
        toppAcctSub.setToppAcctNo(xdzc0011DataReqDto.getPayAccNo());// 交易对手账号
        toppAcctSub.setToppName(xdzc0011DataReqDto.getPayAccName());// 交易对手名称
        toppAcctSub.setAcctsvcrNo(xdzc0011DataReqDto.getPayOrgNo());// 开户行行号
        toppAcctSub.setAcctsvcrName(xdzc0011DataReqDto.getPayOrgName());// 开户行名称
        // TODO 判断是否本账户 开户行行号
        toppAcctSub.setIsBankAcct(checkIsBankAcct(xdzc0011DataReqDto.getPayAccNo()));// 是否本行账户
        toppAcctSub.setToppAmt(xdzc0011DataReqDto.getAppAmt());
        toppAcctSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 操作类型
        toppAcctSub.setInputId(docAsplTcont.getInputId());// 登记人
        toppAcctSub.setInputBrId(docAsplTcont.getInputBrId());// 登记机构
        toppAcctSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 登记日期
        toppAcctSub.setUpdId(docAsplTcont.getUpdId());// 最后修改人
        toppAcctSub.setUpdBrId(docAsplTcont.getUpdBrId());// 最后修改机构
        toppAcctSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 最后修改日期
        toppAcctSub.setCreateTime(DateUtils.getCurrDate());//最近修改日期
        toppAcctSubServcie.insertSelective(toppAcctSub);
    }

    /**
     * 初始化出账申请 对公
     * 贷款还款方式默认：按期付息到期还本，
     * 结息周期默认：月，
     * 结息间隔默认：1，
     * 扣款日默认：21，
     * 贷款扣款方式默认：自动扣款，
     * 借款用途默认：流动资金
     */
    PvpLoanApp getPvpLoanApp(Xdzc0011DataReqDto xdzc0011DataReqDto,CtrAsplDetails ctrAsplDetails,String pvpSerno,String iqpSerno,String apprStatus,BailAccInfo bailAccInfo,DocAsplTcont docAsplTcont){
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        pvpLoanApp.setBillNo(xdzc0011DataReqDto.getBillNo());
        pvpLoanApp.setIqpSerno(iqpSerno);//业务流水号非空
        pvpLoanApp.setPvpSerno(pvpSerno);// 放款流水号
        pvpLoanApp.setContNo(ctrAsplDetails.getContNo());// 合同编号
        pvpLoanApp.setContCnNo(ctrAsplDetails.getContCnNo());// 中文合同编号
        pvpLoanApp.setCusId(ctrAsplDetails.getCusId());// 客户编号
        pvpLoanApp.setCusName(ctrAsplDetails.getCusName());// 客户名称
        pvpLoanApp.setPrdId("012007");// 产品编号 012007 短期流动资金贷款
        pvpLoanApp.setPrdName("短期流动资金贷款");// 产品名称
        pvpLoanApp.setPrdTypeProp("");// 产品类型属性
        pvpLoanApp.setGuarMode(ctrAsplDetails.getGuarMode());// 贷款担保方式
        pvpLoanApp.setCurType(ctrAsplDetails.getCurType());// 币种
        pvpLoanApp.setContCurType(ctrAsplDetails.getCurType());// 合同金额
        pvpLoanApp.setContAmt(ctrAsplDetails.getContAmt());// 资产池合同金额
        pvpLoanApp.setPvpAmt(xdzc0011DataReqDto.getAppAmt());// 放款金额
        pvpLoanApp.setStartDate(ctrAsplDetails.getStartDate());// 起始日
        pvpLoanApp.setEndDate(ctrAsplDetails.getEndDate());// 到期日
        pvpLoanApp.setLoanStartDate(xdzc0011DataReqDto.getStartDate());// 贷款起始日
        pvpLoanApp.setLoanEndDate(xdzc0011DataReqDto.getEndDate());// 贷款到期日
        pvpLoanApp.setLoanTerm(String.valueOf(xdzc0011DataReqDto.getLoanTerm()));// 贷款期限
        pvpLoanApp.setReplyNo(ctrAsplDetails.getReplySerno()); // 批复流水号
        pvpLoanApp.setLmtAccNo(ctrAsplDetails.getLmtAccNo());// 授信台账编号
        pvpLoanApp.setInputId(ctrAsplDetails.getInputId());
        pvpLoanApp.setInputBrId(ctrAsplDetails.getInputBrId());
        pvpLoanApp.setManagerId(ctrAsplDetails.getManagerId());
        pvpLoanApp.setManagerBrId(ctrAsplDetails.getManagerBrId());
        pvpLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);// 操作类型
        pvpLoanApp.setApproveStatus(apprStatus);// 审批状态
        pvpLoanApp.setIsBeEntrustedPay(CmisCommonConstants.STD_ZB_YES_NO_1);// 是否受托支付(这里默认是)
        pvpLoanApp.setLprRateIntval(ctrAsplDetails.getLprPriceInterval());// LPR授信利率区间
        pvpLoanApp.setCurtLprRate(ctrAsplDetails.getChrgRate());// 当前LPR利率
        pvpLoanApp.setRateFloatPoint(ctrAsplDetails.getRateFloatPoint());// 浮动点数
        pvpLoanApp.setEiIntervalCycle("1");
        pvpLoanApp.setEiIntervalUnit("M"); //结息间隔周期单位
        pvpLoanApp.setDeductType("AUTO");//扣款扣息方式->扣款方式
        pvpLoanApp.setDeductDay("21");//还款日->扣款日
        pvpLoanApp.setLoanUseType("A01");//借款用途类型
        pvpLoanApp.setRepayMode("A001");//还款方式
        pvpLoanApp.setPvpMode(CommonConstance.STD_PVP_MODE_1);// 自动出账（默认）
        pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_DG_03);// 所属条线
        pvpLoanApp.setLoanModal(CmisBizConstants.IQP_LOAN_MODAL_1); //贷款形式--默认新增
        pvpLoanApp.setIrFloatType(CmisBizConstants.IR_FLOAT_TYPE_00);// 利率调整方式 "LPR加点" 00
        pvpLoanApp.setOverdueExecRate(ctrAsplDetails.getOverdueExecRate());// 逾期执行利率(年利率)
        pvpLoanApp.setOverdueRatePefloat(new BigDecimal(0.5));// 逾期罚息有关 默认上升0.5
        pvpLoanApp.setCiExecRate(ctrAsplDetails.getCiExecRate());// 复息执行利率
        pvpLoanApp.setCiRatePefloat(new BigDecimal(0.5));// 复息利率浮动比
        pvpLoanApp.setApproveStatus(apprStatus);//审批状态
        pvpLoanApp.setGuarMode(ctrAsplDetails.getGuarMode());//担保方式
        pvpLoanApp.setLoanTermUnit("M");//贷款期限类型
        pvpLoanApp.setIsCfirmPay("1");//是否立即发起受托支付
        pvpLoanApp.setIndtUpFlag(StringUtils.EMPTY);//工业转型升级标识
        pvpLoanApp.setCulIndustryFlag(StringUtils.EMPTY);//文化产业标识
        pvpLoanApp.setExecRateYear(ctrAsplDetails.getExecRateYear());// 执行年利率
        pvpLoanApp.setLoanPayoutAccno("");// 核心说的 资产池 入账机构就是结算账户 还款账户用结算账户
        pvpLoanApp.setRepayAccno(ctrAsplDetails.getAcctNo()); // 核心还款账户 结算账户
        pvpLoanApp.setRepayAcctName("");// 还款账户
        pvpLoanApp.setDisbOrgNo(ctrAsplDetails.getManagerBrId());// 放款机构编号
        pvpLoanApp.setDisbOrgName("");
        pvpLoanApp.setFinaBrId(ctrAsplDetails.getManagerBrId().substring(0, 5) + "1"); // 账务机构 管户机构最后一位  0 -》1
        pvpLoanApp.setFinaBrIdName(""); // 账务机构名称
        pvpLoanApp.setLoanTer(docAsplTcont.getLoanTer()); // 贷款投向
        pvpLoanApp.setIsHolidayDelay(CmisCommonConstants.STD_ZB_YES_NO_1); //节假日是否顺延(这里默认是)
        pvpLoanApp.setRateAdjMode(ctrAsplDetails.getRateAdjMode()); // 利率调整方式 固定利率
        pvpLoanApp.setIsPool("1"); // 是否资产池 1是 0 否
        pvpLoanApp.setPayMode("1"); // 默认受托支付
        return pvpLoanApp;
    }
    /**
     * 关联购销合同和贷款出账
     */
    private void insertAccTContRel(String pvpSerno, String tContSerno,CtrAsplDetails ctrAsplDetails,BigDecimal applyAmt) {
        AccTContRel accTContRel = null;
        AccLoan accloan = accLoanService.selectByAccLoanPvpSerno(pvpSerno);
        accTContRel =  new AccTContRel();
        accTContRel.setPkId(UUID.randomUUID().toString().replace("-",""));
        accTContRel.setPvpSerno(pvpSerno);
        accTContRel.setType("1");
        accTContRel.setTContNo(StringUtils.EMPTY);
        accTContRel.setContNo(ctrAsplDetails.getContNo());
        accTContRel.setTContSerno(tContSerno);
        accTContRel.setCoreBillNo(accloan.getBillNo());
        accTContRel.setPvpAmt(accloan.getLoanAmt());
        accTContRel.setInputId(ctrAsplDetails.getContNo());// 登记人
        accTContRel.setInputBrId(ctrAsplDetails.getInputBrId());// 登记机构
        accTContRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 登记日期
        accTContRel.setUpdId(ctrAsplDetails.getUpdId());// 最近修改人
        accTContRel.setUpdBrId(ctrAsplDetails.getUpdBrId());// 最近修改机构
        accTContRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 最近修改日期
        accTContRel.setManagerId(ctrAsplDetails.getManagerId());// 主管客户经理
        accTContRel.setManagerBrId(ctrAsplDetails.getManagerBrId());// 主管机构
        accTContRel.setCreateTime(DateUtils.getCurrDate());// 创建时间
        accTContRel.setUpdateTime(DateUtils.getCurrDate());// 修改时间
        accTContRelService.insertSelective(accTContRel);
    }
    /**
     * 放款占额
     *
     * @param pvpSerno bizStatus 200 有效
     */

    public void sendCmisLmt0013(String pvpSerno,String bizStatus) {
        AccLoan accLoan = accLoanService.selectByAccLoanPvpSerno(pvpSerno);
        //发台账占用接口占合同额度
        CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
        cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accLoan.getManagerBrId()));//金融机构代码
        cmisLmt0013ReqDto.setSerno(accLoan.getPvpSerno());// 交易流水号（出账流水号）
        cmisLmt0013ReqDto.setBizNo(accLoan.getContNo());//合同编号
        cmisLmt0013ReqDto.setInputId(accLoan.getInputId());//登记人
        cmisLmt0013ReqDto.setInputBrId(accLoan.getInputBrId());//登记机构
        cmisLmt0013ReqDto.setInputDate(accLoan.getInputDate());//登记日期
        List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDto = new ArrayList<CmisLmt0013ReqDealBizListDto>();
        CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizDto = new CmisLmt0013ReqDealBizListDto();
        cmisLmt0013ReqDealBizDto.setDealBizNo(accLoan.getBillNo());//台账编号
        cmisLmt0013ReqDealBizDto.setIsFollowBiz("0");//是否无缝衔接
        cmisLmt0013ReqDealBizDto.setOrigiDealBizNo("");//原交易业务编号
        cmisLmt0013ReqDealBizDto.setOrigiDealBizStatus("");//原交易业务状态
        cmisLmt0013ReqDealBizDto.setOrigiRecoverType("");//原交易业务恢复类型
        cmisLmt0013ReqDealBizDto.setOrigiBizAttr("");//原交易属性
        cmisLmt0013ReqDealBizDto.setCusId(accLoan.getCusId());//客户编号
        cmisLmt0013ReqDealBizDto.setCusName(accLoan.getCusName());//客户名称
        cmisLmt0013ReqDealBizDto.setPrdId(accLoan.getPrdId());//产品编号
        cmisLmt0013ReqDealBizDto.setPrdName(accLoan.getPrdName());//产品名称
        cmisLmt0013ReqDealBizDto.setPrdTypeProp(accLoan.getPrdTypeProp());//产品类型属性
        cmisLmt0013ReqDealBizDto.setDealBizAmtCny(accLoan.getLoanAmt());//台账占用总额(人民币)
        cmisLmt0013ReqDealBizDto.setDealBizSpacAmtCny(BigDecimal.ZERO);//台账占用敞口(人民币)
        cmisLmt0013ReqDealBizDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
        cmisLmt0013ReqDealBizDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
        cmisLmt0013ReqDealBizDto.setStartDate(accLoan.getLoanStartDate());//台账起始日
        cmisLmt0013ReqDealBizDto.setEndDate(accLoan.getLoanEndDate());//台账到期日
        cmisLmt0013ReqDealBizDto.setDealBizStatus(bizStatus);//台账状态
        cmisLmt0013ReqDealBizListDto.add(cmisLmt0013ReqDealBizDto);
        cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDto);
        logger.info("更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步开始");
        ResultDto<CmisLmt0013RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
        logger.info("银承出票申请发起流程-更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步结束");
        if (Objects.isNull(resultDtoDto.getData()) || !SuccessEnum.SUCCESS.key.equals(resultDtoDto.getData().getErrorCode())) {
            String code = resultDtoDto.getData().getErrorCode();
            logger.error("额度占用接口返回码：" + code + "，银承出票申请流水号：" + pvpSerno + "，额度占用接口返回信息:", resultDtoDto.getData().getErrorMsg());
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);

        }
    }

    /**
     * 校验是否本行账户
     * @param accNO
     * @return 1是本行 0非本行
     */
    public String checkIsBankAcct(String accNO){
        // Ib1253请求对象
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        // Ib1253返回对象
        // Ib1253RespDto ib1253RespDto = new Ib1253RespDto();
        // 账号
        ib1253ReqDto.setKehuzhao(accNO);
        // TODO 查询结果集类型  没有该字段
        // 是否标志  1--是 0--否
        ib1253ReqDto.setShifoubz("0");
        // 子账号序号
        ib1253ReqDto.setZhhaoxuh("");
        // 调用ib1253接口
        ResultDto<Ib1253RespDto> ib1253RespDtoResultDto  =  dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        logger.info("【ib1253】接口："+Objects.toString(ib1253RespDtoResultDto));
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ib1253RespDtoResultDto.getCode())) {
            // 获取相关的值并解析
            // ib1253RespDto = ib1253RespDtoResultDto.getData();
            return CmisCommonConstants.STD_ZB_YES_NO_1;
        } else {
            return  CmisCommonConstants.STD_ZB_YES_NO_0;
        }
    }
}
