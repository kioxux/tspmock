/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.bat.BatBizRiskSign;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.service.bat.BatBizRiskSignService;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanApp;
import cn.com.yusys.yusp.service.CoopPlanAppService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:06:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplanapp")
public class CoopPlanAppResource {
    @Autowired
    private CoopPlanAppService coopPlanAppService;
    @Autowired
    private BatBizRiskSignService batBizRiskSignService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanApp> list = coopPlanAppService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanApp>> index(QueryModel queryModel) {
        List<CoopPlanApp> list = coopPlanAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPlanApp>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPlanApp> list = coopPlanAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CoopPlanApp> show(@PathVariable("serno") String serno) {
        CoopPlanApp coopPlanApp = coopPlanAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopPlanApp>(coopPlanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CoopPlanApp> create(@RequestBody CoopPlanApp coopPlanApp) throws URISyntaxException {
        ResultDto<CoopPlanApp> dto = coopPlanAppService.insert(coopPlanApp);
        return dto;
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPlanApp coopPlanApp) throws URISyntaxException {
        int result = coopPlanAppService.updateSelective(coopPlanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopPlanAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:queryBail
     * @函数描述:查询保证金账号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBail")
    protected ResultDto queryBail(@RequestBody String jsonData) {
        JSONObject json = JSONObject.parseObject(jsonData);
        String bailAccNo = json.getString("bailAccNo");
        //接口还在联调中后续接口联调通过后再修改为调接口返回数据
        return coopPlanAppService.queryBail(bailAccNo);
    }

    /**
     * @函数名称:queryCoopLmtAmt
     * @函数描述:查询已用合作额度
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/totlCoopLmtAmt")
    protected ResultDto<CmisLmt0027RespDto> queryCoopLmtAmt(@RequestBody String jsonData) {
        JSONObject json = JSONObject.parseObject(jsonData);
        String accNo = json.getString("partnerNo");
        String instuCde = json.getString("instuCde");
        return coopPlanAppService.queryCoopLmtAmt(accNo, instuCde);
    }

    /**
     * @函数名称:checkSubmit
     * @函数描述:检查是否有在途的数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSubmit")
    protected ResultDto checkSubmit(@RequestBody CoopPlanApp coopPlanApp) {
        return coopPlanAppService.checkSubmit(coopPlanApp);
    }
    /**
     * @param queryModel 分页查询类
     * @函数名称:queryDataAndRisk
     * @函数描述:是否风险预警客户
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDataAndRisk")
    protected ResultDto<CoopPlanApp> queryDataAndRisk(@RequestBody QueryModel queryModel) {
        CoopPlanApp coopPlanApp = new CoopPlanApp();
        List<BatBizRiskSign> list2 = batBizRiskSignService.selectByModel(queryModel);
        if(list2.size()==0){
            coopPlanApp.setIsBankRiskAltCus("0");
        }else{
            coopPlanApp.setIsBankRiskAltCus("1");
        }
        //调客户接口查关联方和集团
        return new ResultDto<CoopPlanApp>(coopPlanApp);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:queryDetail
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetail")
    protected ResultDto<List<CoopPlanApp>> queryDetail(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPlanApp> list = coopPlanAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanApp>>(list);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateBailAccInfoEmptyBySerno")
    protected ResultDto<Integer> updateBailAccInfoEmptyBySerno(@RequestBody CoopPlanApp coopPlanApp) throws URISyntaxException {
        // 当保证金比例为0更新保证金账号，保证金子账号
        int result = coopPlanAppService.updateBailAccInfoEmptyBySerno(coopPlanApp.getSerno());
        return new ResultDto<Integer>(result);
    }
}
