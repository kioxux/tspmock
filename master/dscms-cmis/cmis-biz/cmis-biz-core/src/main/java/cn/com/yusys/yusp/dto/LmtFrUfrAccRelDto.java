package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAccRel
 * @类描述: lmt_fr_ufr_acc_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:39:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtFrUfrAccRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 冻结/解冻流水号 **/
	private String frUfrSerno;
	
	/** 授信额度编号 **/
	private String lmtLimitNo;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;

	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param frUfrSerno
	 */
	public void setFrUfrSerno(String frUfrSerno) {
		this.frUfrSerno = frUfrSerno == null ? null : frUfrSerno.trim();
	}
	
    /**
     * @return FrUfrSerno
     */	
	public String getFrUfrSerno() {
		return this.frUfrSerno;
	}
	
	/**
	 * @param lmtLimitNo
	 */
	public void setLmtLimitNo(String lmtLimitNo) {
		this.lmtLimitNo = lmtLimitNo == null ? null : lmtLimitNo.trim();
	}
	
    /**
     * @return LmtLimitNo
     */	
	public String getLmtLimitNo() {
		return this.lmtLimitNo;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}

}