package cn.com.yusys.yusp.service.server.xdxw0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.server.xdxw0021.req.Xdxw0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0021.resp.Xdxw0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0021Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-18 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional(rollbackFor = {BizException.class, Exception.class})
public class Xdxw0021Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdxw0021.Xdxw0021Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;
    @Resource
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;


    /**
     * 根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
     *
     * @param xdxw0021DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0021DataRespDto xdxw0021(Xdxw0021DataReqDto xdxw0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataReqDto));
        Xdxw0021DataRespDto xdxw0021DataRespDto = new Xdxw0021DataRespDto();

        String accStatus = StringUtils.EMPTY; //借据状态
        String tradeStatus = StringUtils.EMPTY; //放款状态
        String contStatus = StringUtils.EMPTY; //合同状态
        try {
            String serno = xdxw0021DataReqDto.getSerno();//调查流水号
            //查询合同状态
            QueryModel ctrLoanContModel = new QueryModel();
            ctrLoanContModel.addCondition("surveySerno", serno);
            logger.info("*****XDXW0021**查询合同状态信息开始,查询参数为:{}", JSON.toJSONString(ctrLoanContModel));
            List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByModel(ctrLoanContModel);
            logger.info("*****XDXW0021**查询合同状态信息结束,返回参数为:{}", JSON.toJSONString(ctrLoanContList));
            CtrLoanCont ctrLoanCont = new CtrLoanCont();
            if (CollectionUtils.nonEmpty(ctrLoanContList)) {
                ctrLoanCont = ctrLoanContList.get(0);
                if (ctrLoanCont != null) {
                    contStatus = ctrLoanCont.getContStatus();
                }
            }
            //放款状态
            //根据放款申请表pvp_loan_app 的放款流水号去 授权信息表pvp_authorize 查出账状态
            PvpLoanApp pvpLoanApp = new PvpLoanApp();
            QueryModel pvpLoanAppModel = new QueryModel();
            pvpLoanAppModel.addCondition("surveySerno", serno);
            logger.info("*****XDXW0021**查询放款申请表pvp_loan_app信息开始,查询参数为:{}", JSON.toJSONString(pvpLoanAppModel));
            List<PvpLoanApp> pvpLoanAppList = pvpLoanAppMapper.selectByModel(pvpLoanAppModel);
            logger.info("*****XDXW0021**查询放款申请表pvp_loan_app信息结束,返回参数为:{}", JSON.toJSONString(pvpLoanAppList));
            if (CollectionUtils.nonEmpty(pvpLoanAppList)) {
                pvpLoanApp = pvpLoanAppList.get(0);
                QueryModel pvpAuthorizeModel = new QueryModel();
                pvpAuthorizeModel.addCondition("pvpSerno", pvpLoanApp.getPvpSerno());
                logger.info("*****XDXW0021**查询授权信息表pvp_authorize信息开始,查询参数为:{}", JSON.toJSONString(pvpAuthorizeModel));
                List<PvpAuthorize> pvpAuthorizeList = pvpAuthorizeMapper.selectByModel(pvpAuthorizeModel);
                logger.info("*****XDXW0021**查询授权信息表pvp_authorize信息结束,返回参数为:{}", JSON.toJSONString(pvpAuthorizeList));
                if (CollectionUtils.nonEmpty(pvpLoanAppList)) {
                    PvpAuthorize pvpAuthorize = pvpAuthorizeList.get(0);
                    if (pvpAuthorize != null) {
                        //放款状态
                        tradeStatus = pvpAuthorize.getTradeStatus();
                        //借据状态
                        if (StringUtils.isNotEmpty(pvpAuthorize.getBillNo())) {
                            AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(pvpAuthorizeList.get(0).getBillNo());
                            if (accLoan != null) {
                                accStatus = accLoan.getAccStatus();
                            }
                        }
                    }
                }
            }
            //  封装xdxw0021DataRespDto对象开始
            xdxw0021DataRespDto.setDisbStatus(tradeStatus);//放款状态
            xdxw0021DataRespDto.setBillStatus(accStatus);//借据状态
            xdxw0021DataRespDto.setContStatus(contStatus);//合同状态
            //  封装xdxw0021DataRespDto对象结束
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataReqDto));
        return xdxw0021DataRespDto;
    }
}
