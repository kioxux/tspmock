/*
 * Automatically generated by code generator
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanApp;
import org.apache.ibatis.annotations.Param;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanAppSub;
/**
 * @Project Name: cmis-biz-coreModule
 * @Class Name: OtherRecordAccpSignPlanAppSubMapper
 * @Class Description: #Dao Class
 * @Function Description: 
 * @Creator: hhj123456
 * @Create Time: 2021-06-09 20:26:05
 * @Modification Note: 
 * @Modification Records: Modified Time    Modified By    Modified Reason
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) Yusys Technologies Co., Ltd. All Rights Reserved.
 */
public interface OtherRecordAccpSignPlanAppSubMapper {

    /**
     * @Method Name: selectByPrimaryKey
     * @Method Description: Query by primary key
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 
     */
    
    OtherRecordAccpSignPlanAppSub selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @Method Name: selectByModel
     * @Method Description: Query by Condition List
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 
     */
    List<OtherRecordAccpSignPlanAppSub> selectByModel(QueryModel model);
	
    /**
     * @Method Name: insert
     * @Method Description: insert
     * @Parameter and Return Description:
     * @Algorithm Desciption: 
     */
    
    int insert(OtherRecordAccpSignPlanAppSub record);

    /**
     * @Method Name: insertSelective
     * @Method Description: insert - Insert only non-empty fields
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 
     */
    
    int insertSelective(OtherRecordAccpSignPlanAppSub record);

    /**
     * @Method Name: updateByPrimaryKey
     * @Method Description: update by primary key 
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 
     */
    
    int updateByPrimaryKey(OtherRecordAccpSignPlanAppSub record);

    /**
     * @Method Name: updateByPrimaryKeySelective
     * @Method Description: update by primary key - Update only non-empty fields
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 
     */
    
    int updateByPrimaryKeySelective(OtherRecordAccpSignPlanAppSub record);

    /**
     * @Method Name: deleteByPrimaryKey
     * @Method Description: delete by primary key
     * @Parameter and Return Description: sysId - primary key
     * @Algorithm Desciption: 
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @Method Name: deleteByIds
     * @Method Description: Delete by primary keys
     * @Parameter and Return Description: 
     * @Algorithm Desciption: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<OtherRecordAccpSignPlanAppSub> selectByCusId(Map params);
}