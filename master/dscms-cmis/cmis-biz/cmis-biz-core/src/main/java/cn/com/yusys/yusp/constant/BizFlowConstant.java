package cn.com.yusys.yusp.constant;

/**
 * 业务流程常量类
 */
public class BizFlowConstant {
    /**默认的分隔符**/
    public final static String DEF_SPILIT_COMMMA = ",";

    /**xianglei测试流程编码**/
    public final static String BIZ_TYPE_IQP_XIANGLEITEST_APP = "xianglei_test";

    /**单一客户授信申请流程编码**/
    public final static String BIZ_TYPE_LMT_APP = "LMT_APP";

    /**单一客户授信批复变更流程编码**/
    public final static String BIZ_TYPE_LMT_REPLY_CHG_APP = "LMT_REPLY_CHG";

    /**单笔单批流程编码**/
    public final static String BIZ_TYPE_IQP_SINGLE_APP = "IQP_SINGLE_BATCH_APP";
    /**额度项下流程编码**/
    public final static String BIZ_TYPE_IQP_UQ_APP = "IQP_UNDER_QUATO_APP";
    /**特殊业务流程**/
    public final static String BIZ_TYPE_IQP_SPECIAL_APP = "IQP_SPECIAL_APP";
    /**担保变更业务流程**/
    public final static String BIZ_TYPE_GRT_GUAR_CHG_APP = "GRT_GUAR_CHG_APP";
    /**放款业务流程**/
    public final static String BIZ_TYPE_PVP_LOAN_APP = "PVP_LOAN_APP";
    /**业务展期**/
    public final static String BIZ_TYPE_IQP_LOAN_EXT_APP = "IQP_LOAN_EXT_APP";
    /**展期放款流程**/
    public final static String BIZ_TYPE_PVP_EXT_LOAN_APP = "PVP_EXT_LOAN_APP";
    /**合作方额度申请**/
    public final static String BIZ_TYPE_LMT_COOP_APP = "WF_LMT_COOP_APP";
    /**合作方额度-共享范围调整申请**/
    public final static String BIZ_TYPE_LMT_COOP_SHARED_APP = "WF_LMT_COOP_SHARED_APP";
    /**担保公司授信申请**/
    public final static String BIZ_TYPE_LMT_FIN_GUAR_APP = "LMT_FIN_GUAR_APP";

    /**担保公司-共享范围调整申请**/
    public final static String BIZ_TYPE_LMT_FIN_SHARED_APP = "WF_LMT_FIN_SHARED_APP";

    /**还款方式变更申请流程编码**/
    public final static String BIZ_TYPE_IQP_REPAY_WAY_CHG = "IQP_REPAY_WAY_CHG_FLOW";

    /**还款计划变更申请流程编码**/
    public final static String BIZ_TYPE_IQP_REPAY_PLAN = "IQP_REPAY_PLAN";
    /**利息减免变更申请流程编码**/
    public final static String BIZ_TYPE_IQP_INTEREST_PLAN = "IQP_REPAY_INTEREST_CHG";
    /**还款间隔周期变更申请流程编码**/
    public final static String BIZ_TYPE_IQP_REPAY_TERM_CHG = "IQP_REPAY_TERM_CHG_FLOW";
    /**利率调整申请**/
    public final static String BIZ_TYPE_IQP_RATE_CHG_APP = "IQP_RATE_CHG_APP";
    /**期限调整申请**/
    public final static String BIZ_TYPE_IQP_TERM_CHG_APP = "IQP_TERM_CHG_APP";
    /**停息申请业务流程**/
    public final static String BIZ_TYPE_IQP_STP_INT_APP = "IQP_STP_INT_APP";
    /**恢复计息申请业务流程**/
    public final static String BIZ_TYPE_IQP_UNSTP_INT_APP = "IQP_UNSTP_INT_APP";
    /**还款日变更申请流程**/
    public final static String BIZ_TYPE_IQP_REPAY_DATE_CHG = "IQP_REPAY_DATE_CHG";
    /**权证出入库申请流程**/
    public final static String BIZ_TYPE_IQP_CERTI_INOUT_APP = "IQP_CERTI_INOUT_APP";
    /**权证出入库申请流程**/
    public final static String BIZ_TYPE_GUAR_WARRANT_RENEW_APP = "GUAR_WARRANT_RENEW_APP";

    /**合同账号变更申请业务流程**/
    public final static String IQP_ACCT_CHG_CONT = "IQP_ACCT_CHG_CONT";
    /**放款申请退汇申请业务流程**/
    public final static String PVP_RRU_UPD_APP = "PVP_RRU_UPD_APP";
    /**还款方式变更申请流程编码**/
    public final static String BIZ_TYPE_IQP_BILL_ACCT_CHG_APP = "IQP_BILL_ACCT_CHG_APP";
    /**贷款投向调整业务流程**/
    public final static String BIZ_TYPE_IQP_LOAN_TER_CHG = "IQP_LOAN_TER_CHG";
    /**授信审批通知书有效期流程**/
    public final static String BIZ_TYPE_IQP_IDT_ADJ_APP = "IQP_IDT_ADJ_APP";
    /**呆账核销申请业务流程**/
    public final static String BIZ_TYPE_IQP_WRITEOFF = "IQP_WRITE_OFF";
    /**个人额度申请**/
    public final static String BIZ_TYPE_LMT_INDIV_APP = "LMT_INDIV_APP";
    
    /**额度冻结/解冻申请**/
    public final static String BIZ_TYPE_LMT_FR_UFR_APP = "LMT_FR_UFR_APP";

    /** 担保公司授信代偿申请 **/
    public final static String BIZ_TYPE_LMT_FIN_SP_APP = "LMT_FIN_SP_APP_FLOW";
    /** 担保公司授信代偿申请 **/
    public final static String BIZ_TYPE_WF_LMT_SURVEY_REPORT_MAIN = "WF_LMT_SURVEY_REPORT_MAIN";

    /** 业务流程申请 **/
    public final static String BIZ_TYPE_ZXZ_TEST = "WF_ZXZ_TEST";

    /** 最高额授信申请 **/
    public final static String BIZ_TYPE_IQP_HIGHAMT_OFF = "IQP_HIGHAMT_OFF";

    /** 合作方额度调整申请 **/
    public final static String LMT_COOP_CHG_APP = "LMT_COOP_CHG_APP_FLOW";

    /** 单一额度调整申请 **/
    public final static String LMT_APPR_CHG_APP = "LMT_APPR_CHG_APP_FLOW";

    /** 合作方协议申请 **/
    public final static String COOP_PARTNER_AGR_APPLY = "COOP_PARTNER_AGR_APPLY";

    /** 展期申请 **/
    public final static String IQP_CONTEXT_APPLY = "IQP_CONTEXT_APPLY";

    /** 展期协议申请 **/
    public final static String IQP_CONTEXT_CONT_APPLY = "IQP_CONTEXT_CONT_APPLY";

    /** 零售优惠利率申请 **/
    public final static String QT001 = "QT001";

    public final static String QTSX01 = "QTSX01";
    /** 利率定价流程-经营性及消费非按揭 寿光村镇 **/
    public final static String SGCZ29 = "SGCZ29";
    /** 利率定价流程-经营性及消费非按揭 东海村镇 **/
    public final static String DHCZ29 = "DHCZ29";



    /** 利率定价-一手房按揭贷款 东海村镇 **/
    public final static String DHCZ08 = "DHCZ08";
    /** 利率定价-一手房按揭贷款 寿光村镇 **/
    public final static String SGCZ08 = "SGCZ08";
    /** 利率定价-二手房按揭贷款 寿光村镇 **/
    public final static String SGCZ09 = "SGCZ09";
    /** 利率定价-二手房按揭贷款 东海村镇 **/
    public final static String DHCZ09 = "DHCZ09";

    /** 合作方准入与变更审批流程-分支机构（寿光） **/
    public final static String SGCZ36 = "SGCZ36";

    /** 合作方准入与变更审批流程-普惠金融部（寿光） **/
    public final static String SGCZ30 = "SGCZ30";

    /** 合作方协议新增、变更、续签流程-分支机构（寿光） **/
    public final static String SGCZ03 = "SGCZ03";

    /** 合作方协议新增、变更、续签流程-普惠金融部（寿光） **/
    public final static String SGCZ31 = "SGCZ31";

    /** 合作方名单退出审批流程-分支机构（寿光） **/
    public final static String SGCZ32 = "SGCZ32";

    /** 合作方名单退出审批流程-普惠金融部（寿光） **/
    public final static String SGCZ33 = "SGCZ33";

    /** 合作方保证金提取审批流程-分支机构（寿光） **/
    public final static String SGCZ34 = "SGCZ34";

    /** 合作方保证金提取审批流程-普惠金融部（寿光） **/
    public final static String SGCZ35 = "SGCZ35";

    /** 合作方准入与变更审批流程-分支机构（东海） **/
    public final static String DHCZ36 = "DHCZ36";

    /** 合作方准入与变更审批流程-普惠金融部（东海） **/
    public final static String DHCZ30 = "DHCZ30";

    /** 合作方协议新增、变更、续签流程-分支机构（东海） **/
    public final static String DHCZ03 = "DHCZ03";

    /** 合作方协议新增、变更、续签流程-普惠金融部（东海） **/
    public final static String DHCZ31 = "DHCZ31";

    /** 合作方名单退出审批流程-分支机构（东海） **/
    public final static String DHCZ32 = "DHCZ32";

    /** 合作方名单退出审批流程-普惠金融部（东海） **/
    public final static String DHCZ33 = "DHCZ33";

    /** 合作方保证金提取审批流程-分支机构（东海） **/
    public final static String DHCZ34 = "DHCZ34";

    /** 合作方保证金提取审批流程-普惠金融部（东海） **/
    public final static String DHCZ35 = "DHCZ35";

    /** 对公人民币利率定价 **/
    public final static String QT002 = "QT002";

    /** 利率定价-展期 寿光村镇 **/
    public final static String SGF04 = "SGF04";
    /** 利率定价-展期 东海村镇 **/
    public final static String DHF04 = "DHF04";

    public final static String QTSX02 = "QTSX02";

    /** 小微人民币利率定价审批流程 **/
    public final static String QTSX07 = "QTSX07";

    /** 利率定价-展期 寿光村镇 **/
    public final static String SGCZ10 = "SGCZ10";
    /** 利率定价-展期 东海村镇 **/
    public final static String DHCZ10 = "DHCZ10";
    
    public final static String BG001 = "BG001";

    /** 对公外币利率定价 **/
    public final static String QT003 = "QT003";

    public final static String QTSX03 = "QTSX03";

    /** 保证金存款特惠利率申请 **/
    public final static String QT004 = "QT004";

    /** 保证金存款特惠利率申请 -寿光村镇 **/
    public final static String SGF05 = "SGF05";
    /** 保证金存款特惠利率申请 -东海村镇 **/
    public final static String DHF05 = "DHF05";
    /** 银票手续费率优惠申请 -寿光村镇 **/
    public final static String SGF06 = "SGF06";
    /** 银票手续费率优惠申请 -东海村镇 **/
    public final static String DHF06 = "DHF06";
    /** 贴现优惠利率申请 -寿光村镇 **/
    public final static String SGF07 = "SGF07";
    /** 贴现优惠利率申请 -东海村镇 **/
    public final static String DHF07 = "DHF07";

    public final static String QTSX04 = "QTSX04";

    /** 保证金存款特惠利率申请-寿光村镇 **/
    public final static String SGCZ11 = "SGCZ11";
    /** 保证金存款特惠利率申请-东海村镇 **/
    public final static String DHCZ11 = "DHCZ11";

    /** 银票手续费率优惠申请 **/
    public final static String QT005 = "QT005";

    /** 贴现优惠利率申请 **/
    public final static String QT006 = "QT006";

    /** 银票签发及全资质押类业务备案 **/
    public final static String QT007 = "QT007";

    /** 银票签发业务每周计划表 **/
    public final static String QT008 = "QT008";

    /** 中行代签电票申请 **/
    public final static String QT009 = "QT009";

    /** 用信审核备案 **/
    public final static String QT010 = "QT010";
    /** 用信审核备案流程编码 **/
    public final static String QTSX05 = "QTSX05";

    /** 授信抵押价值认定 **/
    public final static String QT011 = "QT011";
    /** 授信抵押价值认定流程编码 **/
    public final static String QTSX06 = "QTSX06";
    /** 授信抵押价值认定流程编码-寿光村镇 **/
    public final static String SGCZ12 = "SGCZ12";
    /** 授信抵押价值认定流程编码-东海村镇 **/
    public final static String DHCZ12 = "DHCZ12";

    /** 其他申请事项 **/
    public final static String QT012 = "QT012";



    /**流程状态标识位 000-待发起**/
    public final static String WF_STATUS_000 = "000";
    /**流程状态标识位 111-审批中**/
    public final static String WF_STATUS_111 = "111";
    /**流程状态标识位 990-取消**/
    public final static String WF_STATUS_990 = "990";
    /**流程状态标识位 991-拿回**/
    public final static String WF_STATUS_991 = "991";
    /**流程状态标识位 992-打回**/
    public final static String WF_STATUS_992 = "992";
    /**流程状态标识位 996-自行退出**/
    public final static String WF_STATUS_996 = "996";
    /**流程状态标识位 997-通过**/
    public final static String WF_STATUS_997 = "997";
    /**流程状态标识位 998-拒绝**/
    public final static String WF_STATUS_998 = "998";

    /** 不允许重复申请相同业务的流程状态List 包括 待发起、审批中、追回、打回 **/
    public final static String WF_STATUS_CAN_NOT_APPLY_SAME =
            WF_STATUS_000 + DEF_SPILIT_COMMMA +
            WF_STATUS_111 + DEF_SPILIT_COMMMA +
            WF_STATUS_991 + DEF_SPILIT_COMMMA +
            WF_STATUS_992 ;
    /** 不允许重复提交相同的还款方式变更业务的流程状态List 包括 审批中、追回、打回 **/
    public final static String REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME = "111,991,992";
    
    /**额度流程审批标志位**/
    public final static String BIZ_TYPE_LMT = "lmt";
    /**业务流程审批标志位**/
    public final static String BIZ_TYPE_BIZ = "biz";

    /** 数据修改统计分类或科目投向审批流程（对公及零售） **/
    public final static String XTGL03 = "XTGL03";

    /** 数据修改其他信息审批流程（对公及零售） **/
    public final static String XTGL06 = "XTGL06";
    /** 数据修改统计分类或科目投向审批流程（小微） **/
    public final static String XTGL04 = "XTGL04";
    /** 数据修改其他信息审批流程（小微） **/
    public final static String XTGL07 = "XTGL07";
    /** 数据修改统计分类或科目投向审批流程（网金） **/
    public final static String XTGL05 = "XTGL05";
    /** 数据修改其他信息审批流程（网金） **/
    public final static String XTGL08 = "XTGL08";
    /** 数据修改统计分类或科目投向审批流程（寿光） **/
    public final static String SGCZ42 = "SGCZ42";
    /** 数据修改其他信息审批流程（寿光） **/
    public final static String SGCZ43 = "SGCZ43";
    /** 数据修改统计分类或科目投向审批流程（东海） **/
    public final static String DHCZ42 = "DHCZ42";
    /** 数据修改其他信息审批流程（东海） **/
    public final static String DHCZ43 = "DHCZ43";

    
}
