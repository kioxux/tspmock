package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.IqpStockLoanInfo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 12393
 * @version 1.0.0
 * @date 2021/5/8 16:13
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class IqpStockLoanInfoDto {

    /* 存量授信额度合计*/
    private java.math.BigDecimal lmtAmtTotal;

    /* 余额合计*/
    private java.math.BigDecimal balanceTotal;


    public BigDecimal getLmtAmtTotal() {
        return lmtAmtTotal;
    }

    public void setLmtAmtTotal(BigDecimal lmtAmtTotal) {
        this.lmtAmtTotal = lmtAmtTotal;
    }

    public BigDecimal getBalanceTotal() {
        return balanceTotal;
    }

    public void setBalanceTotal(BigDecimal balanceTotal) {
        this.balanceTotal = balanceTotal;
    }
}
