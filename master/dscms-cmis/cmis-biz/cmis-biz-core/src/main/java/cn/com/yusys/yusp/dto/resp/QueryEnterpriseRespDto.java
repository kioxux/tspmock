package cn.com.yusys.yusp.dto.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/4/24 15:29
 * @desc 查询工商信息返回信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class QueryEnterpriseRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /* 企业名称 */
    @JsonProperty(value = "conName")
    private String conName;

    /* 企业类型 */
    @JsonProperty(value = "corpType")
    private String corpType;

    /* 企业证件类型 */
    @JsonProperty(value = "corpCertType")
    private String corpCertType;

    /* 企业证件号码*/
    @JsonProperty(value = "corpCertCode")
    private String corpCertCode;

    /* 企业法人 */
    @JsonProperty(value = "legal")
    private String legal;

    /* 营业执照年限 */
    @JsonProperty(value = "blicYears")
    private String blicYears;

    /* 行业 */
    @JsonProperty(value = "trade")
    private String trade;

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getCorpType() {
        return corpType;
    }

    public void setCorpType(String corpType) {
        this.corpType = corpType;
    }

    public String getCorpCertType() {
        return corpCertType;
    }

    public void setCorpCertType(String corpCertType) {
        this.corpCertType = corpCertType;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getBlicYears() {
        return blicYears;
    }

    public void setBlicYears(String blicYears) {
        this.blicYears = blicYears;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getCorpCertCode() {
        return corpCertCode;
    }

    public void setCorpCertCode(String corpCertCode) {
        this.corpCertCode = corpCertCode;
    }
}
