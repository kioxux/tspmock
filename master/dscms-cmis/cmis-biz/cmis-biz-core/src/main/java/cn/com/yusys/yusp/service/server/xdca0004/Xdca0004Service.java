package cn.com.yusys.yusp.service.server.xdca0004;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditCtrLoanCont;
import cn.com.yusys.yusp.dto.server.xdca0004.req.Xdca0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0004.resp.Xdca0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditCtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0004Service
 * @类描述: #服务类 大额分期合同详情接口
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdca0004Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0004Service.class);

    @Autowired
    private CreditCtrLoanContMapper creditCtrLoanContMapper;

    /**
     * 大额分期合同详情查询
     *
     * @param xdca0004DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0004DataRespDto xdca0004(Xdca0004DataReqDto xdca0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value);
        Xdca0004DataRespDto xdca0004DataRespDto = new Xdca0004DataRespDto();
        try {
            //请求报文
            String contNo = xdca0004DataReqDto.getContNo();//合同编号
            //查询大额分期合同表
            logger.info("*********XDCA0004*大额分期合同详情查询接口开始,查询参数为:{}", JSON.toJSONString(contNo));
            CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContMapper.selectByPrimaryKey(contNo);
            logger.info("*********XDCA0004*大额分期合同详情查询接口结束,返回参数为:{}", JSON.toJSONString(creditCtrLoanCont));

            if (creditCtrLoanCont != null) {//存在合同记录
                xdca0004DataRespDto.setContNo(creditCtrLoanCont.getContNo());// 合同编号
                xdca0004DataRespDto.setSerno(creditCtrLoanCont.getSerno());// 业务流水号
                xdca0004DataRespDto.setCusName(creditCtrLoanCont.getCusName());// 客户姓名
                xdca0004DataRespDto.setCertType(creditCtrLoanCont.getCertType());// 证件类型
                xdca0004DataRespDto.setCertCode(creditCtrLoanCont.getCertCode());// 证件号码
                xdca0004DataRespDto.setCardNo(creditCtrLoanCont.getCardNo());// 卡号
                xdca0004DataRespDto.setLoanPlan(creditCtrLoanCont.getLoanPlan());// 分期计划
                xdca0004DataRespDto.setLoanAmount(creditCtrLoanCont.getLoanFeeCalcMethod());// 分期金额
                xdca0004DataRespDto.setLoanTerm(creditCtrLoanCont.getLoanAmount());// 分期期数
                xdca0004DataRespDto.setSendMode(Integer.parseInt(creditCtrLoanCont.getLoanTerm()));// 放款方式
                xdca0004DataRespDto.setGuarMode(creditCtrLoanCont.getGuarMode());// 担保方式
                xdca0004DataRespDto.setLoanFeeMethod(creditCtrLoanCont.getLoanFeeMethod());// 分期手续费收取方式
                xdca0004DataRespDto.setLoanPrinDistMethod(creditCtrLoanCont.getLoanPrinDistMethod());// 分期本金分配方式
                xdca0004DataRespDto.setLoanFeeCalcMethod(creditCtrLoanCont.getLoanFeeCalcMethod());// 分期手续费计算方式
                xdca0004DataRespDto.setLoanFeeRate(creditCtrLoanCont.getLoanFeeCalcMethod());// 分期手续费比例
                xdca0004DataRespDto.setLoanrTarget(new BigDecimal(0L));// 分期放款账户对公/对私标识
                xdca0004DataRespDto.setDdBankBranch(creditCtrLoanCont.getDdBankBranch());// 分期放款开户行号
                xdca0004DataRespDto.setDdBankName(creditCtrLoanCont.getDdBankName());// 分期放款银行名称
                xdca0004DataRespDto.setDdBankAccNo(creditCtrLoanCont.getDdBankAccNo());// 分期放款账号
                xdca0004DataRespDto.setDdBankAccName(creditCtrLoanCont.getDdBankAccName());// 分期放款账户姓名
                xdca0004DataRespDto.setDisbAcctPhone(creditCtrLoanCont.getDisbAcctPhone());// 放款账户移动电话
                xdca0004DataRespDto.setDisbAcctCertType(creditCtrLoanCont.getDisbAcctCertType());// 放款账户证件类型
                xdca0004DataRespDto.setDisbAcctCertCode(creditCtrLoanCont.getDisbAcctCertCode());// 放款账户证件号码
                xdca0004DataRespDto.setPaymentPurpose(creditCtrLoanCont.getPaymentPurpose());// 资金用途
                xdca0004DataRespDto.setYearInterestRate(creditCtrLoanCont.getAppDate());// 分期折算近似年化利率
                xdca0004DataRespDto.setSalesManNo(creditCtrLoanCont.getLoanAmount());// 分期营销客户经理号
                xdca0004DataRespDto.setSalesMan(creditCtrLoanCont.getSalesMan());// 分期营销人员姓名
                xdca0004DataRespDto.setSalesManPhone(creditCtrLoanCont.getSalesManPhone());// 分期营销人员手机号
                xdca0004DataRespDto.setSalesManOwingBranch(creditCtrLoanCont.getSalesManOwingBranch());// 分期营销人员所属支行
                xdca0004DataRespDto.setContStatus(creditCtrLoanCont.getContStatus());// 合同状态
                xdca0004DataRespDto.setAppDate(creditCtrLoanCont.getAppDate());// 申请日期
            } else {
                xdca0004DataRespDto.setContNo(StringUtils.EMPTY);// 合同编号
                xdca0004DataRespDto.setSerno(StringUtils.EMPTY);// 业务流水号
                xdca0004DataRespDto.setCusName(StringUtils.EMPTY);// 客户姓名
                xdca0004DataRespDto.setCertType(StringUtils.EMPTY);// 证件类型
                xdca0004DataRespDto.setCertCode(StringUtils.EMPTY);// 证件号码
                xdca0004DataRespDto.setCardNo(StringUtils.EMPTY);// 卡号
                xdca0004DataRespDto.setLoanPlan(StringUtils.EMPTY);// 分期计划
                xdca0004DataRespDto.setLoanAmount(StringUtils.EMPTY);// 分期金额
                xdca0004DataRespDto.setLoanTerm(new BigDecimal(0L));// 分期期数
                xdca0004DataRespDto.setSendMode(new Integer(1));// 放款方式
                xdca0004DataRespDto.setGuarMode(StringUtils.EMPTY);// 担保方式
                xdca0004DataRespDto.setLoanFeeMethod(StringUtils.EMPTY);// 分期手续费收取方式
                xdca0004DataRespDto.setLoanPrinDistMethod(StringUtils.EMPTY);// 分期本金分配方式
                xdca0004DataRespDto.setLoanFeeCalcMethod(StringUtils.EMPTY);// 分期手续费计算方式
                xdca0004DataRespDto.setLoanFeeRate(StringUtils.EMPTY);// 分期手续费比例
                xdca0004DataRespDto.setLoanrTarget(new BigDecimal(0L));// 分期放款账户对公/对私标识
                xdca0004DataRespDto.setDdBankBranch(StringUtils.EMPTY);// 分期放款开户行号
                xdca0004DataRespDto.setDdBankName(StringUtils.EMPTY);// 分期放款银行名称
                xdca0004DataRespDto.setDdBankAccNo(StringUtils.EMPTY);// 分期放款账号
                xdca0004DataRespDto.setDdBankAccName(StringUtils.EMPTY);// 分期放款账户姓名
                xdca0004DataRespDto.setDisbAcctPhone(StringUtils.EMPTY);// 放款账户移动电话
                xdca0004DataRespDto.setDisbAcctCertType(StringUtils.EMPTY);// 放款账户证件类型
                xdca0004DataRespDto.setDisbAcctCertCode(StringUtils.EMPTY);// 放款账户证件号码
                xdca0004DataRespDto.setPaymentPurpose(StringUtils.EMPTY);// 资金用途
                xdca0004DataRespDto.setYearInterestRate(StringUtils.EMPTY);// 分期折算近似年化利率
                xdca0004DataRespDto.setSalesManNo(new BigDecimal(0L));// 分期营销客户经理号
                xdca0004DataRespDto.setSalesMan(StringUtils.EMPTY);// 分期营销人员姓名
                xdca0004DataRespDto.setSalesManPhone(StringUtils.EMPTY);// 分期营销人员手机号
                xdca0004DataRespDto.setSalesManOwingBranch(StringUtils.EMPTY);// 分期营销人员所属支行
                xdca0004DataRespDto.setContStatus(StringUtils.EMPTY);// 合同状态
                xdca0004DataRespDto.setAppDate(StringUtils.EMPTY);// 申请日期
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value);
        return xdca0004DataRespDto;
    }
}
