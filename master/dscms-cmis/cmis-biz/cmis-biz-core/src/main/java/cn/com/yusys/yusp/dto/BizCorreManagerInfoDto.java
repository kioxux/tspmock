package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BizCorreManagerInfo
 * @类描述: biz_corre_manager_info数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 11:05:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BizCorreManagerInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	/** 主键 **/
	private String pkId;
	
	/** 业务申请编号 **/
	private String bizSerno;
	
	/** 人员工号 **/
	private String correManagerId;
	
	/** 人员类型 STD_ZB_MGR_TYPE **/
	private String correMgrType;
	
	/** 所属机构编码 **/
	private String correManagerBrId;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param correManagerId
	 */
	public void setCorreManagerId(String correManagerId) {
		this.correManagerId = correManagerId == null ? null : correManagerId.trim();
	}
	
    /**
     * @return CorreManagerId
     */	
	public String getCorreManagerId() {
		return this.correManagerId;
	}
	
	/**
	 * @param correMgrType
	 */
	public void setCorreMgrType(String correMgrType) {
		this.correMgrType = correMgrType == null ? null : correMgrType.trim();
	}
	
    /**
     * @return CorreMgrType
     */	
	public String getCorreMgrType() {
		return this.correMgrType;
	}
	
	/**
	 * @param correManagerBrId
	 */
	public void setCorreManagerBrId(String correManagerBrId) {
		this.correManagerBrId = correManagerBrId == null ? null : correManagerBrId.trim();
	}
	
    /**
     * @return CorreManagerBrId
     */	
	public String getCorreManagerBrId() {
		return this.correManagerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}