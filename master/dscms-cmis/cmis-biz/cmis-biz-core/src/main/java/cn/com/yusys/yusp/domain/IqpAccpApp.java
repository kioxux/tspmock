/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-cord模块
 * @类名称: IqpAccpApp
 * @类描述: iqp_accp_app数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-13 10:16:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_accp_app")
public class IqpAccpApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;

	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 10)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;

	/** 申请业务类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 合同类型 **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 合同币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;

	/** 本合同项下最高可用信金额 **/
	@Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighAvlAmt;

	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private Integer contTerm;

	/** 起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;

	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;

	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;

	/** 是否续签 **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;

	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;

	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;

	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;

	/** 是否占用第三方额度 **/
	@Column(name = "IS_OUTSTND_TRD_LMT_AMT", unique = false, nullable = true, length = 5)
	private String isOutstndTrdLmtAmt;

	/** 第三方合同协议编号 **/
	@Column(name = "TDP_AGR_NO", unique = false, nullable = true, length = 40)
	private String tdpAgrNo;

	/** 合作方客户编号 **/
	@Column(name = "COOP_CUS_ID", unique = false, nullable = true, length = 20)
	private String coopCusId;

	/** 合作方客户名称 **/
	@Column(name = "COOP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String coopCusName;

	/** 是否电子用印 **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;

	/** 是否在线抵押 **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 5)
	private String isOlPld;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 5)
	private String belgLine;

	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;

	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;

	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;

	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;

	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;

	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 20)
	private String fax;

	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;

	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;

	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 40)
	private String wechat;

	/** 送达地址 **/
	@Column(name = "DELIVERY_ADDR", unique = false, nullable = true, length = 500)
	private String deliveryAddr;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 签发类型 **/
	@Column(name = "SIGNISSUE_TYPE", unique = false, nullable = true, length = 5)
	private String signissueType;

	/** 是否电子票据 **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;

	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;

	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;

	/** 手续费率 **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;

	/** 手续费金额 **/
	@Column(name = "CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgAmt;

	/** 承兑行类型 **/
	@Column(name = "AORG_TYPE", unique = false, nullable = true, length = 5)
	private String aorgType;

	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;

	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 40)
	private String aorgName;

	/** 出票人开户行账号 **/
	@Column(name = "DAORG_NO", unique = false, nullable = true, length = 40)
	private String daorgNo;

	/** 出票人开户户名 **/
	@Column(name = "DAORG_NAME", unique = false, nullable = true, length = 80)
	private String daorgName;

	/** 其他约定 **/
	@Column(name = "OTHER_AGREED", unique = false, nullable = true, length = 2000)
	private String otherAgreed;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}

	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}

	/**
	 * @return busiType
	 */
	public String getBusiType() {
		return this.busiType;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}

	/**
	 * @return contType
	 */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return contAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(java.math.BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}

	/**
	 * @return contHighAvlAmt
	 */
	public java.math.BigDecimal getContHighAvlAmt() {
		return this.contHighAvlAmt;
	}

	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}

	/**
	 * @return contTerm
	 */
	public Integer getContTerm() {
		return this.contTerm;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}

	/**
	 * @return paperContSignDate
	 */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}

	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}

	/**
	 * @return isRenew
	 */
	public String getIsRenew() {
		return this.isRenew;
	}

	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}

	/**
	 * @return origiContNo
	 */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param isOutstndTrdLmtAmt
	 */
	public void setIsOutstndTrdLmtAmt(String isOutstndTrdLmtAmt) {
		this.isOutstndTrdLmtAmt = isOutstndTrdLmtAmt;
	}

	/**
	 * @return isOutstndTrdLmtAmt
	 */
	public String getIsOutstndTrdLmtAmt() {
		return this.isOutstndTrdLmtAmt;
	}

	/**
	 * @param tdpAgrNo
	 */
	public void setTdpAgrNo(String tdpAgrNo) {
		this.tdpAgrNo = tdpAgrNo;
	}

	/**
	 * @return tdpAgrNo
	 */
	public String getTdpAgrNo() {
		return this.tdpAgrNo;
	}

	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId;
	}

	/**
	 * @return coopCusId
	 */
	public String getCoopCusId() {
		return this.coopCusId;
	}

	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName;
	}

	/**
	 * @return coopCusName
	 */
	public String getCoopCusName() {
		return this.coopCusName;
	}

	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}

	/**
	 * @return isESeal
	 */
	public String getIsESeal() {
		return this.isESeal;
	}

	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}

	/**
	 * @return isOlPld
	 */
	public String getIsOlPld() {
		return this.isOlPld;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}

	/**
	 * @return debtLevel
	 */
	public String getDebtLevel() {
		return this.debtLevel;
	}

	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}

	/**
	 * @return ead
	 */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}

	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}

	/**
	 * @return lgd
	 */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}

	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	/**
	 * @return linkman
	 */
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return fax
	 */
	public String getFax() {
		return this.fax;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * @return qq
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	/**
	 * @return wechat
	 */
	public String getWechat() {
		return this.wechat;
	}

	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}

	/**
	 * @return deliveryAddr
	 */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param signissueType
	 */
	public void setSignissueType(String signissueType) {
		this.signissueType = signissueType;
	}

	/**
	 * @return signissueType
	 */
	public String getSignissueType() {
		return this.signissueType;
	}

	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}

	/**
	 * @return isEDrft
	 */
	public String getIsEDrft() {
		return this.isEDrft;
	}

	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}

	/**
	 * @return bailPerc
	 */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}

	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	/**
	 * @return bailAmt
	 */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}

	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}

	/**
	 * @return chrgRate
	 */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}

	/**
	 * @param chrgAmt
	 */
	public void setChrgAmt(java.math.BigDecimal chrgAmt) {
		this.chrgAmt = chrgAmt;
	}

	/**
	 * @return chrgAmt
	 */
	public java.math.BigDecimal getChrgAmt() {
		return this.chrgAmt;
	}

	/**
	 * @param aorgType
	 */
	public void setAorgType(String aorgType) {
		this.aorgType = aorgType;
	}

	/**
	 * @return aorgType
	 */
	public String getAorgType() {
		return this.aorgType;
	}

	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}

	/**
	 * @return aorgNo
	 */
	public String getAorgNo() {
		return this.aorgNo;
	}

	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}

	/**
	 * @return aorgName
	 */
	public String getAorgName() {
		return this.aorgName;
	}

	/**
	 * @param daorgNo
	 */
	public void setDaorgNo(String daorgNo) {
		this.daorgNo = daorgNo;
	}

	/**
	 * @return daorgNo
	 */
	public String getDaorgNo() {
		return this.daorgNo;
	}

	/**
	 * @param daorgName
	 */
	public void setDaorgName(String daorgName) {
		this.daorgName = daorgName;
	}

	/**
	 * @return daorgName
	 */
	public String getDaorgName() {
		return this.daorgName;
	}

	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}

	/**
	 * @return otherAgreed
	 */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}