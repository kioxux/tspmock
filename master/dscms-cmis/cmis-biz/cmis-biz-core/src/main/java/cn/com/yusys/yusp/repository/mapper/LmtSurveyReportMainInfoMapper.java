/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfoAndCrd;
import cn.com.yusys.yusp.dto.DoubleViewDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.req.Xdxw0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.resp.Xdxw0056DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.req.Xdxw0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.resp.Xdxw0057DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.req.Xdxw0066DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.req.Xdxw0067DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.req.Xdxw0068DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0076.req.Xdxw0076DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportMainInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtSurveyReportMainInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtSurveyReportMainInfo selectByPrimaryKey(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtSurveyReportMainInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtSurveyReportMainInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtSurveyReportMainInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtSurveyReportMainInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtSurveyReportMainInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);
    /**
     * @创建人 WH
     * @创建时间 2021-04-29 19:41
     * @注释 条件分页查询->增加了额外的条件
     */
    List<LmtSurveyReportMainInfoAndCrd> findlistbymodel(QueryModel queryModel);

    /**
     * 根据核心客户号查询经营性贷款批复额度
     * @param xdxw0057DataReqDto
     * @return
     */
    Xdxw0057DataRespDto getLmtByQueryType(Xdxw0057DataReqDto xdxw0057DataReqDto);

    /**
     * 交易描述：查询调查表和其他关联信息
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0038.resp.List> selectLmtSurveyReportInfoByCusId(Map QueryMap);

    /**
     * 优企贷、优农贷授信调查信息查询
     * @param queryMap
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0064.resp.List> getSurveyList(Map queryMap);


    /**
     * @方法名称: selectDoubleViewByModel
     * @方法描述: 根据条件查询双录信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<DoubleViewDto> selectDoubleViewByModel(Map queryMap);

    /**
     * 调查基本信息查询
     * @param xdxw0066DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0066.resp.List> getSurveyReportList(Xdxw0066DataReqDto xdxw0066DataReqDto);

    /**
     * 调查基本信息查询
     * @param xdxw0067DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0067.resp.List> getComSurveyReportList(Xdxw0067DataReqDto xdxw0067DataReqDto);

    /**
     * 调查基本信息查询
     * @param xdxw0068DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0068.resp.List> xdxw0068(Xdxw0068DataReqDto xdxw0068DataReqDto);

    /**
     * 根据流水号取得优企贷信息的客户经理ID和客户经理名
     * @param xdxw0056DataReqDto
     * @return
     */
    Xdxw0056DataRespDto getManagerInfo(Xdxw0056DataReqDto xdxw0056DataReqDto);

    /**
     * 获取小贷申请
     * @param cusId
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> findSmlnApplyListByCusId(@Param("cusId") String cusId);

    /**
     * 查询在小贷调查申请记录总数
     * @param certCode
     * @return
     */
    int getLmtSurveyReportMainInfoCountByCertCode(@Param("certCode") String certCode);

    /**
     * 获取小贷申请
     * @param cusId
     * @return
     */
    int findSmlnApplyListxdtzByCusId(@Param("cusId") String cusId,@Param("prdId") String prdId);

    /**
     * 根据证件号查询信贷系统的申请信息
     * @param certCode
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0022.resp.List> selectLmtSurveyReportMainInfoByCertNo(@Param("certCode") String certCode);


    /**
     * 根据业务编号查询信贷系统的审批意见
     * @param pkValue
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0032.resp.ApprHisList> selectWfiAppAdviceByPkValue(@Param("pkValue") String pkValue);

    /**
     * 根据客户身份证号查询线上产品申请记录
     * @param certCode
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0074.resp.List> selectOnlineProjectApplyList(@Param("certCode") String certCode);


    /**
     * 渠道端查询个人客户我的授信（授信申请流程监控）
     * @param xdxw0076DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0076.resp.List> getXdxw0076(Xdxw0076DataReqDto xdxw0076DataReqDto);

    /**
     * 根据客户身份证号查询授信调查批复申请记录
     * @param certCode
     * @return
     */
    int selectReportCountByCertCode(@Param("certCode") String certCode);

    /**
     * @方法名称: updateBySurveySerno
     * @方法描述: 根据业务流水号更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: selectByBizserno
     * @方法描述: 客户授信调查主表
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtSurveyReportMainInfo selectByBizserno(@Param("bizSerno") String bizSerno);

    /**
     * @方法名称: selectByCrqlSerno
     * @方法描述: 查询产品名称和产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */

    java.util.Map<String,String> selectByCrqlSerno(@Param("crqlSerno") String crqlSerno);

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo
     * @author 王玉坤
     * @date 2021/10/7 20:58
     * @version 1.0.0
     * @desc  根据名单流水号信息查询调查表主表信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    LmtSurveyReportMainInfo selectByListSerno(@Param("listSerno") String surveySerno);

}