package cn.com.yusys.yusp.service.server.xdcz0028;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.template.FileSystemTemplate;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgSftpDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.req.Xdcz0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.PvpCheckList;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.Xdcz0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.util.SftpUtil;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0028Service
 * @类描述: #服务类
 * @功能描述:生成商贷账号数据文件
 * @创建人:YD
 * @创建时间: 2021-06-013 20:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0028Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0028Service.class);

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Resource(name = "sftpFileSystemTemplate")
    private FileSystemTemplate sftpFileSystemTemplate;

    /**
     * 生成商贷账号数据文件
     *
     * @param xdcz0028DataReqDto
     * @return
     * @desc
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0028DataRespDto xdcz0028(Xdcz0028DataReqDto xdcz0028DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value);
        Xdcz0028DataRespDto xdcz0028DataRespDto = new Xdcz0028DataRespDto();
        String tranDate = xdcz0028DataReqDto.getTranDate();//交易日期
        String totlQnt = xdcz0028DataReqDto.getTotlQnt();//总笔数
        String fileName = xdcz0028DataReqDto.getFileName();//文件名
        String seqNo = xdcz0028DataReqDto.getSeqNo();//批次号
        String opFlag = null;// 操作成功标志位
        String opMsg = null;// 描述信息
        String msg = "success";
        InputStreamReader read = null;
        BufferedReader bufferedReader = null;
        File writename = null;
        BufferedWriter out = null;
        try {

            String host = "";
            int port = 0;
            String username = "";
            String password = "";
            String savepath = "";
            String localpath = "";
            logger.info("***********调用ICmisCfgClientService根据渠道标识查询配置信息开始*START**************");
            ResultDto<CfgSftpDto> resultDto = iCmisCfgClientService.queryCfgSftpByBizScenseType("01");
            logger.info("***********调用ICmisCfgClientService根据渠道标识查询配置信息结束,返回{}*END*****************", JSON.toJSONString(resultDto));
            host = resultDto.getData().getHost();
            username = resultDto.getData().getUsername();
            port = Integer.parseInt(resultDto.getData().getPort());
            password = resultDto.getData().getPassword();
            savepath = resultDto.getData().getSavepath();//目标服务器地址
            localpath = resultDto.getData().getLocalpath();//本地生成的服务器地址

            File localdocfile = new File(localpath);
            if (!localdocfile.exists()) {
                localdocfile.mkdirs();
            }
            //获取文件名称
            String serverFileName = fileName;
            if (fileName.contains("/")) {
                serverFileName = fileName.substring(fileName.lastIndexOf("/") + 1);
            }


            logger.info("***********NACOS配置路径{}*END*****************", JSON.toJSONString(sftpFileSystemTemplate));
            FileInfo fileInfo = FileInfoUtils.createFileInfo(sftpFileSystemTemplate, serverFileName, "");
            logger.info("***********NACOS配置路径返回{}*END*****************", JSON.toJSONString(fileInfo));
            boolean download = FileInfoUtils.download(serverFileName, localpath, fileInfo);

            File file1 = new File(localpath + "/" + serverFileName);
            read = new InputStreamReader(new FileInputStream(file1), Charset.forName("GBK"));
            bufferedReader = new BufferedReader(read);
            String saveFileName = "wtzhsd009_" + seqNo + "_resp";
            String localFilePath = localpath + "/" + saveFileName + ".txt";
            writename = new File(localFilePath);
            boolean newFile = writename.createNewFile();// 创建新文件
            if (!newFile) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "文件[" + localFilePath + "]创建失败!");
            }
            String lineTxt = null;
            String result = "";
            int n = 1;
            int n1 = 0;
            double house_total = 0.0;
            while ((lineTxt = bufferedReader.readLine()) != null) {
                //System.out.println("行数" + n);
                //解析文件
                String[] line1 = lineTxt.split("\\|");
                String rqDate = line1[0].trim();//日期
                String loanNo = line1[1].trim();//贷款账号
                String cusName = line1[2].trim();//借款人姓名
                String hkMonuth = line1[3].trim();//请求还款明细月份

                // 查询参数
                Map queryMap = new HashMap<String, String>();
                // 借据编号
                queryMap.put("billNo", loanNo);
                // 流水号
                queryMap.put("cusName", cusName);
                List<PvpCheckList> pvpCheckList = pvpLoanAppMapper.selectPvpCheckListByParam(queryMap);
                //
                if (pvpCheckList.size() < 1 || pvpCheckList == null) {
                    xdcz0028DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdcz0028DataRespDto.setOpMsg("根据借据号" + loanNo + "找不到出账信息！");
                    return xdcz0028DataRespDto;
                } else {
                    //
                    PvpCheckList pvpCheck = pvpCheckList.get(0);
                    String cifArea = pvpCheck.getCifArea();
                    if ("".equals(cifArea) || cifArea == null) {
                        cifArea = " ";
                    }
                    String housingTotal = pvpCheck.getHousingTotal();
                    if ("".equals(housingTotal) || housingTotal == null) {
                        housingTotal = " ";
                    }
                    String gethomeNum1 = pvpCheck.getGethomeNum1();
                    if ("01".equals(gethomeNum1)) {
                        gethomeNum1 = "1";
                    }
                    if ("02".equals(gethomeNum1)) {
                        gethomeNum1 = "0";
                    }
                    house_total += Double.valueOf(StringUtils.isBlank(pvpCheck.getHousingTotal()) ? "0" : pvpCheck.getHousingTotal());
                    result += rqDate + "|016|" + loanNo + "|" + pvpCheck.getBookingId() + "|" + cifArea
                            + "|" + housingTotal + "|" + pvpCheck.getHousingZone() + "|" + pvpCheck.getParceName()
                            + "|" + gethomeNum1 + "|" + pvpCheck.getHousingContId() + "|\r" + "\n";
                    n1++;
                    System.out.println(n + "、" + result);
                }
                n++;
            }
            result = tranDate + "|" + seqNo + "|" + n1 + "|" + house_total + "|\r" + "\n" + result;
            //写入对象
            out = new BufferedWriter(new FileWriter(writename));// 建立写入对象
            out.write(result);
            out.flush();// 把缓冲区内容压入文件

            logger.info("**********biz生成文件成功开始上传本地文件路径名称{},文件名称{},服务器路径{}*****", JSON.toJSONString(writename), JSON.toJSONString(writename.getName()), JSON.toJSONString(savepath));
//          sftpFileSystemTemplate.upload(writename, writename.getName(), savepath);
            //连接Sftp上传
            try {
                logger.info("**********连接ftp开始上传文件*****");
                Map<String, String> paramMap = new HashMap<>();
                paramMap.put("ip", host);//帆软服务IP
                paramMap.put("port", String.valueOf(port));
                paramMap.put("username", username);
                paramMap.put("password", password);
                // 本地生成文件路径
                paramMap.put("localFilePath", localFilePath);
                // 上传目标服务器文件名称+路劲
                paramMap.put("fileNewName", writename.getName());
                //paramMap.put("ftpPath", "/home/cmis/upload/");//45服务器地址
                paramMap.put("ftpPath", savepath);//帆软服务地址
                logger.info("**********查询参数{}*****", JSON.toJSONString(paramMap));
                boolean falg = upload(paramMap);
                if (falg) {
                    logger.info("**********连接ftp开始上传文件成功**SUCCESS*****");
                } else {
                    logger.info("**********连接ftp开始上传文失败*FAIL****");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            xdcz0028DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdcz0028DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        } finally {
            read.close();
            if (null != out) {
                out.close();
            }
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value);
        return xdcz0028DataRespDto;
    }

    /**
     * 上传问文件
     *
     * @param paramMap
     * @return
     */
    public boolean upload(Map<String, String> paramMap) {
        String path = paramMap.get("localFilePath");
        String fileNewName = paramMap.get("fileNewName");
        String ftpPath = paramMap.get("ftpPath");
        String ip = paramMap.get("ip");
        Integer port = Integer.parseInt(paramMap.get("port"));
        String username = paramMap.get("username");
        String password = paramMap.get("password");
        boolean upLoadFlag = Boolean.FALSE;
        try {
            SftpUtil sftpUtil = SftpUtil.getSftpUtil(ip, port, username, password);
            upLoadFlag = sftpUtil.upload(new File(path), fileNewName, ftpPath);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            SftpUtil.release();
        }
        return upLoadFlag;
    }
}

