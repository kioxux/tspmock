package cn.com.yusys.yusp.service.server.xdxw0058;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0058.req.Xdxw0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0058.resp.Xdxw0058DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 业务逻辑类:根据流水号查询借据号
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdxw0058Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0058Service.class);
    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    /**
     * 交易码：xdxw0058
     * 交易描述：根据流水号查询借据号
     *
     * @param xdxw0058DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0058DataRespDto xdxw0058(Xdxw0058DataReqDto xdxw0058DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdxw0058DataReqDto));
        Xdxw0058DataRespDto xdxw0058DataRespDto = new Xdxw0058DataRespDto();
        try {
            // 从xdxw0058DataReqDto获取业务值进行业务逻辑处理
            String applySerno = xdxw0058DataReqDto.getApplySerno();//申请流水号
            Map queryMap = new HashMap<>();
            queryMap.put("survey_serno", applySerno);//申请流水号
            logger.info("根据风控流水号查询借据号开始,查询参数为:{}", JSON.toJSONString(queryMap));
            String billNo = lmtSurveyReportBasicInfoMapper.queryXDBillNo(queryMap);// 借据编号
            logger.info("根据风控流水号查询借据号结束,返回结果为:{}", JSON.toJSONString(billNo));
            xdxw0058DataRespDto.setBillNo(billNo);// 借据编号
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdxw0058DataRespDto));
        return xdxw0058DataRespDto;
    }
}
