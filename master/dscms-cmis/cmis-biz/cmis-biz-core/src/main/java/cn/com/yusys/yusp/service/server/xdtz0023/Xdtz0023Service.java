package cn.com.yusys.yusp.service.server.xdtz0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.BailDepositInfo;
import cn.com.yusys.yusp.dto.server.xdtz0023.req.Xdtz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.resp.Xdtz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;

import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Xdtz0023Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0023Service.class);

    @Autowired
    private AccTfLocMapper accTfLocMapper;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    @Autowired
    private BailDepositInfoMapper  bailDepositInfoMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：Xdtz0023
     * 交易描述：保证金登记入账
     *
     * @param xdtz0023DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0023DataRespDto xdtz0023(Xdtz0023DataReqDto xdtz0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataReqDto));
        //定义返回信息
        Xdtz0023DataRespDto xdtz0023DataRespDto = new Xdtz0023DataRespDto();
        //得到请求字段
        String billNo = xdtz0023DataReqDto.getBillNo();//借据号
        String oprtype = xdtz0023DataReqDto.getOprtype();//操作类型
        String bizType = xdtz0023DataReqDto.getBizType();//业务类型
        String cusId = xdtz0023DataReqDto.getCusId();//客户编号
        String cusName = xdtz0023DataReqDto.getCusName();//客户姓名
        String finaDept = xdtz0023DataReqDto.getFinaDept();//财务部门
        String endDate = xdtz0023DataReqDto.getEndDate();//到期日期
        String msg = "";
        List<cn.com.yusys.yusp.dto.server.xdtz0023.req.List> lists = xdtz0023DataReqDto.getList();
        //业务处理开始
        try {
            if(lists.size()>0 && lists!=null && !"02".equals(oprtype)){
                for(int i=0;i<lists.size();i++){
                    cn.com.yusys.yusp.dto.server.xdtz0023.req.List list = lists.get(i);
                    //列表数据
                    //List list = new List();
                    //得到列表数据信息
                    String bailAcct  = list.getBailAcct();//保证金账户
                    BigDecimal securityAmt = list.getSecurityAmt();//保证金金额
                    String intMode = list.getIntMode();//计息方式
                    String  bailIntDepAcctNo = list.getBailIntDepAcctNo();//保证金利息存入账号
                    BigDecimal bailExchgRate = list.getBailExchgRate();//保证金汇率
                    String cusSettlAcct = list.getCusSettlAcct();//客户结算账户
                    BigDecimal cretQuotationExchgRate = list.getCretQuotationExchgRate();//信贷牌价汇率
                    //转换付息方式
                    if("1Y".equals(intMode)){
                        intMode="301";
                    }else if("2Y".equals(intMode)){
                        intMode="302";
                    }else if("3033Y".equals(intMode)){
                        intMode="303";
                    }else if("5Y".equals(intMode)){
                        intMode="305";
                    }else if("6M".equals(intMode)){
                        intMode="206";
                    }else if("3M".equals(intMode)){
                        intMode="203";
                    }
                    if("".equals(billNo)){
                        xdtz0023DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0023DataRespDto.setOpMsg("借据号不能为空！");
                        return xdtz0023DataRespDto;
                        //01：入账
                    }else if("01".equals(oprtype)){
                        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
                        BailDepositInfo bailDepositInfo = new BailDepositInfo();
                        bailDepositInfo.setSerno(serno);//流水号
                        bailDepositInfo.setBailType(bizType);//业务类型
                        bailDepositInfo.setContNo(billNo);//借据号
                        bailDepositInfo.setBailAccNo(bailAcct);//保证金账户
                        bailDepositInfo.setBailAmt(securityAmt);//保证金金额
                        bailDepositInfo.setInterestMode(intMode);//计息方式
                        bailDepositInfo.setRqstrAccName(cusName);//户名
                        bailDepositInfo.setRqstrAccNo(cusSettlAcct);//客户结算账户
                        bailDepositInfo.setContDep(finaDept);//财务部门
                        bailDepositInfo.setEndDate(endDate);//到期日期
                        bailDepositInfo.setBailInterestDepAcct(bailIntDepAcctNo);//保证金利息存入账号
                        //根据借据编号查询台账信息
                        AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(billNo);
                        if(accLoan==null){
                            xdtz0023DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                            xdtz0023DataRespDto.setOpMsg("借据对应台账信息不存在！");
                            return xdtz0023DataRespDto;
                        }
                        bailDepositInfo.setDepositInputId(accLoan.getInputId());//缴存登记人
                        bailDepositInfo.setDepositInputDate("");//缴存登记日期
                        bailDepositInfo.setStatus("0");//状态
                        bailDepositInfo.setCurType(accLoan.getContCurType());//币种
                        bailDepositInfo.setDepositInputBrId(accLoan.getInputBrId());
                        bailDepositInfo.setBailRate(cretQuotationExchgRate);
                        bailDepositInfo.setBailRate(bailExchgRate);
                        int j =bailDepositInfoMapper.insertSelective(bailDepositInfo);
                        if(j==0){
                            xdtz0023DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                            xdtz0023DataRespDto.setOpMsg("保证金入账失败！");
                            return xdtz0023DataRespDto;
                        }
                        //更新保证金额度
                    }else if("03".equals(oprtype)){
                        //（1）通过借据号保证金账号更新保证金登记表中的金额.
                        BailDepositInfo bailDepositInfo = new BailDepositInfo();
                        bailDepositInfo.setContNo(billNo);//借据编号
                        bailDepositInfo.setBailAmt(securityAmt);//保证金金额
                        bailDepositInfo.setBailAccNo(bailAcct);//保证金账户
                        bailDepositInfoMapper.updatebailAmt(bailDepositInfo);
                        // （2）通过借据号将保证金金额小于等于零的相关信息状态置为'1'失效状态。
                        bailDepositInfoMapper.updatebailAmtAndStatus(bailDepositInfo);
                    }
                }
                //设置查询参数
                //02：冲正（删除保证金）
            }else if("02".equals(oprtype)){
                bailDepositInfoMapper.deleteByContNo(billNo);
            }else{
                xdtz0023DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                xdtz0023DataRespDto.setOpMsg("操作类型传入值错误！");
                return xdtz0023DataRespDto;
            }
            xdtz0023DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
            xdtz0023DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataRespDto));
        return xdtz0023DataRespDto;
    }
}