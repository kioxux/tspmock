/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdzqgjh;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanRepayPlanTemMapper;
import cn.com.yusys.yusp.service.server.xddh0008.Xddh0008Service;
import com.github.pagehelper.Page;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.IqpRepayWayChgMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayWayChgService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-20 14:25:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpRepayWayChgService {
    private static final Logger log = LoggerFactory.getLogger(IqpRepayWayChgService.class);
    @Autowired
    private IqpRepayWayChgMapper iqpRepayWayChgMapper;
    @Autowired
    private Xddh0008Service xddh0008Service;
    @Autowired
    private AccLoanRepayPlanTemMapper accLoanRepayPlanTemMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpRepayWayChg selectByPrimaryKey(String iqpSerno) {
        return iqpRepayWayChgMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpRepayWayChg> selectAll(QueryModel model) {
        List<IqpRepayWayChg> records = (List<IqpRepayWayChg>) iqpRepayWayChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpRepayWayChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayWayChg> list = iqpRepayWayChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpRepayWayChg record) {
        return iqpRepayWayChgMapper.insert(record);
    }

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpRepayWayChg iqpRepayWayChg) {

        QueryModel model1 = new QueryModel();
        model1.addCondition("billNo", iqpRepayWayChg.getBillNo());
        model1.addCondition("apply","Y");
        List<IqpRepayWayChg> list1 = this.selectByModel(model1);
        if(list1.size()>0){
            throw BizException.error(null, "999999","\"该借据存在在途还款计划变更申请，请勿重复发起！\"" + "保存失败！");
        }

        QueryModel model = new QueryModel();
        model.addCondition("billNo", iqpRepayWayChg.getBillNo());
        List<AccLoan> list = accLoanMapper.selectByModel(model);
        if (list.isEmpty()) {
            //借据号不存在
            throw BizException.error(null, EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
        }
        //TODO 存在性校验
        AccLoan accLoan = list.get(0);
        iqpRepayWayChg.setBillNo(accLoan.getBillNo());                                //借据编号
        iqpRepayWayChg.setCusId(accLoan.getCusId());                                  //客户编号
        iqpRepayWayChg.setCusName(accLoan.getCusName());                              //客户名称
        iqpRepayWayChg.setContNo(accLoan.getContNo());                                //合同编号
        iqpRepayWayChg.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
        iqpRepayWayChg.setLoanBalance(accLoan.getLoanBalance());                      //贷款余额
        iqpRepayWayChg.setPrdId(accLoan.getPrdId());                                  //产品ID
        iqpRepayWayChg.setPrdName(accLoan.getPrdName());                              //产品名称
        iqpRepayWayChg.setLoanStartDate(accLoan.getLoanStartDate());                  //贷款起始日
        iqpRepayWayChg.setLoanEndDate(accLoan.getLoanEndDate());                      //贷款到期日
        iqpRepayWayChg.setOldRepayMode(accLoan.getRepayMode());                   //原还款方式
        iqpRepayWayChg.setOldEiIntervalCycle(accLoan.getEiIntervalCycle());           //原结息间隔周期
        iqpRepayWayChg.setOldEiIntervalUnit(accLoan.getEiIntervalUnit());             //原结息间隔周期单位
        iqpRepayWayChg.setApproveStatus("000");
        iqpRepayWayChg.setContStatus("100"); //未生效
        iqpRepayWayChg.setContApproveStatus("000");
        iqpRepayWayChg.setAppChnl("02"); // 申请渠道 02-PC端
        iqpRepayWayChg.setManagerId(accLoan.getManagerId());// 主管客户经理
        iqpRepayWayChg.setManagerBrId(accLoan.getManagerBrId());// 主管机构
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        iqpRepayWayChg.setInputId(userInfo.getLoginCode()); // 当前用户号
        iqpRepayWayChg.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
        iqpRepayWayChg.setInputDate(DateUtils.getCurrDateStr());
        iqpRepayWayChg.setCreateTime(DateUtils.getCurrTimestamp());
        iqpRepayWayChg.setUpdateTime(iqpRepayWayChg.getCreateTime());
        iqpRepayWayChg.setUpdDate(iqpRepayWayChg.getInputDate());
        iqpRepayWayChg.setUpdId(iqpRepayWayChg.getInputId());
        iqpRepayWayChg.setUpdBrId(iqpRepayWayChg.getInputBrId());
        return iqpRepayWayChgMapper.insertSelective(iqpRepayWayChg);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpRepayWayChg record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return iqpRepayWayChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpRepayWayChg record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return iqpRepayWayChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpRepayWayChgMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRepayWayChgMapper.deleteByIds(ids);
    }

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private RepayCapPlanService repayCapPlanService;

    /***
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdzqgjh>
     * @author tangxun
     * @date 2021/5/24 19:31
     * @version 1.0.0
     * @desc queryRepayPlan
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Lstdkhk> queryRepayPlan(QueryModel queryModel) {
        // 设置分页参数
        Page<Lstdkhk> page = new Page<>();
        Map<String, Object> condition = queryModel.getCondition();
        // 调用核心查询还款计划-为了获取上次结息日
        Xddh0008DataReqDto xddh0008DataReqDto = new Xddh0008DataReqDto();
        xddh0008DataReqDto.setBillNo(condition.get("billNo").toString());
        Xddh0008DataRespDto xddh0008DataRespDto = xddh0008Service.getRepayList(xddh0008DataReqDto).getData();
        // 判断是否查询变更前的还款计划
        if ("old".equals(Optional.ofNullable(condition.get("appResn")).orElse(""))) {
            if (xddh0008DataRespDto != null) {
                List<Lstdkhk> lstdkhkList = new ArrayList<Lstdkhk>();
                for (int i =0; i < xddh0008DataRespDto.getTotalQnt(); i++) {
                    cn.com.yusys.yusp.dto.server.xddh0008.resp.List oldPmShd = xddh0008DataRespDto.getList().get(i);
                    int startNum = queryModel.getSize() * (queryModel.getPage() - 1) ;
                    int endNum = queryModel.getSize() * queryModel.getPage();
                    if (oldPmShd.getDateno() > startNum && oldPmShd.getDateno() <= endNum) {
                        Lstdkhk lstdkhk = new cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk();
                        lstdkhk.setBenqqish(Optional.ofNullable(oldPmShd.getDateno()).orElse(0));// 本期期数
                        lstdkhk.setQixiriqi(Optional.ofNullable(oldPmShd.getBillDate()).orElse(StringUtils.EMPTY));// 起息日期
                        lstdkhk.setHuankriq(Optional.ofNullable(oldPmShd.getEndDate()).orElse(StringUtils.EMPTY));// 还款日期
                        lstdkhk.setKxqdqirq(Optional.ofNullable(oldPmShd.getEndDate()).orElse(StringUtils.EMPTY));// 宽限期到期日
                        if (oldPmShd.getDateno() == 1) {
                            lstdkhk.setJixibenj(new BigDecimal(Optional.ofNullable(
                                    condition.get("loanAmt"))
                                    .orElse("0").toString()));// 计息本金
                        } else {
                            lstdkhk.setJixibenj(Optional.ofNullable(
                                    xddh0008DataRespDto.getList().get(i-1).getLoanBalance())
                                    .orElse(new BigDecimal(0L)));// 计息本金
                        }
                        lstdkhk.setLeijcslx(Optional.ofNullable(oldPmShd.getInterest()).orElse(new BigDecimal(0L)));// 累计产生利息
                        lstdkhk.setMeiqhkze(Optional.ofNullable(oldPmShd.getInstmAmt()).orElse(new BigDecimal(0L)));// 每期还款总额
                        lstdkhk.setZhanghye(Optional.ofNullable(oldPmShd.getLoanBalance()).orElse(new BigDecimal(0L)));// 账户余额
                        lstdkhkList.add(lstdkhk);
                    }
                }
                page.addAll(lstdkhkList);
                page.setTotal(xddh0008DataRespDto.getTotalQnt());
                page.setPages(queryModel.getPage());
                page.setPageNum(queryModel.getSize());
            } else {
                throw BizException.error(null, "", "核心不存在对应的还款计划,请检查!");
            }
        } else {
            String startDate = stringRedisTemplate.opsForValue().get("openDay")
                    .replaceAll("-", "");
            if (xddh0008DataRespDto != null) {
                QueryModel pmShdModel = new QueryModel();
                pmShdModel.addCondition("billNo", condition.get("billNo").toString());
                pmShdModel.setSort("enddate");
                List<AccLoanRepayPlanTem> pmShdTempList = accLoanRepayPlanTemMapper.selectByModel(pmShdModel);
                for (int i=0; i< pmShdTempList.size(); i++) {
                    if (pmShdTempList.get(i).getEnddate().compareTo(startDate) > 0) {
                        if (i > 0) {
                            startDate = pmShdTempList.get(i-1).getEnddate();
                        }
                        break;
                    }
                }
            } else {
                throw BizException.error(null, "", "核心不存在对应的还款计划,请检查!");
            }
            Ln3110ReqDto ln3110ReqDto = new Ln3110ReqDto();//req对象
            //daikjine	贷款金额
            ln3110ReqDto.setDaikjine(new BigDecimal(Optional.ofNullable(condition.get("loanBalance")).orElse("0").toString()));
            //sftsdkbz	是否特殊贷款标志
            ln3110ReqDto.setSftsdkbz("1");
            //tsdkjxqs	特殊贷款计息总期数
            AccLoan accLoan = accLoanMapper.selectByBillNo(condition.get("billNo").toString());
            //jixiguiz	计息规则
            ln3110ReqDto.setJixiguiz("01");
            //zhchlilv	正常利率
            ln3110ReqDto.setZhchlilv(accLoan.getExecRateYear().multiply(new BigDecimal(100)));
            //hkzhouqi	还款周期
            // 还款间隔单位
            String eiIntervalUnit = Optional.ofNullable(condition.get("eiIntervalUnit")).orElse("").toString();
            // 还款间隔时间
            String eiIntervalCycle = Optional.ofNullable(condition.get("eiIntervalCycle")).orElse("").toString();

            PvpLoanApp pvpLoan = pvpLoanAppService.queryPvpLoanAppDataByBillNo(accLoan.getBillNo());
            String syFlag = "N";//  N是顺延   A不顺延
            if(Objects.nonNull(pvpLoan)){
                if("0".equals(pvpLoan.getIsHolidayDelay())){// 1是 0否
                    syFlag = "A";
                }
            }
            ln3110ReqDto.setHkzhouqi(this.getHkzhouqi(eiIntervalUnit,
                    eiIntervalCycle,syFlag, accLoan.getDeductDay()));
            //hkqixian	还款期限(月)
            ln3110ReqDto.setHkqixian("");
            //qixiriqi	起息日期 TODO 存疑
            ln3110ReqDto.setQixiriqi(startDate);
            //daoqriqi	到期日期 TODO 存疑
            ln3110ReqDto.setDaoqriqi(condition.get("loanEndDate").toString().replaceAll("-", ""));
            //huankfsh	还款方式 TODO

//        1--利随本清
//        2--多次还息一次还本
//        3--等额本息
//        4--等额本金
//        5--等比累进
//        6--等额累进
//        7--定制还款

            ln3110ReqDto.setHuankfsh(this.toConverLoanPaymMtd(Objects.toString(condition.get("repayMode"))));
            //qigscfsh	期供生成方式
            ln3110ReqDto.setQigscfsh("");
            //qglxleix	期供利息类型
            ln3110ReqDto.setQglxleix("");
            //dechligz	等额处理规则
            ln3110ReqDto.setDechligz("2");
            //meiqhkze	每期还款总额
            //meiqhbje	每期还本金额

            //baoliuje	保留金额
            //kuanxiqi	宽限期
            //leijinzh	累进值
            //leijqjsh	累进区间期数
//        qishibis	起始笔数
//        chxunbis	查询笔数

            ln3110ReqDto.setChxunbis(queryModel.getSize());
            int qishibis = queryModel.getSize() * (queryModel.getPage() - 1);
            ln3110ReqDto.setQishibis(qishibis);
            //shifoudy	是否打印
            ln3110ReqDto.setShifoudy("0");
            //scihkrbz	首次还款日模式
            ln3110ReqDto.setScihkrbz("2");
            //mqihkfsh	末期还款方式
            ln3110ReqDto.setMqihkfsh("3");
            //dzhhkjih	定制还款计划
            if ("7".equals(ln3110ReqDto.getHuankfsh())) {
                ln3110ReqDto.setDzhhkjih("1");
                QueryModel model = new QueryModel();
                model.addCondition("serno", condition.get("iqpSerno"));
                List<RepayCapPlan> list = repayCapPlanService.selectAll(model);
                List<Lstdzqgjh> lstdzqgjhs = new ArrayList<>();
                /**
                 *         └------------benqqish		本期期数
                 *         └------------qishriqi		起始日期
                 *         └------------daoqriqi		到期日期
                 *         └------------huanbjee		还本金额
                 *         └------------hxijinee		还息金额
                 *         └------------zwhkriqi		最晚还款日
                 */
                String start = ln3110ReqDto.getQixiriqi();
                for (int i = 0; i < list.size(); i++) {
                    Lstdzqgjh lstdzqgjh = new Lstdzqgjh();
                    lstdzqgjh.setBenqqish(i+1);
                    lstdzqgjh.setQishriqi(start);
                    start = list.get(i).getRepayDate().replaceAll("-","");
                    lstdzqgjh.setDaoqriqi(start);
                    lstdzqgjh.setHuanbjee(list.get(i).getRepayAmt());
                    lstdzqgjhs.add(lstdzqgjh);
                    //记录下一期起始日
                    ;
                }
                ln3110ReqDto.setLstdzqgjh(lstdzqgjhs);
            } else {
                ln3110ReqDto.setDzhhkjih("0");
            }
            //ljsxqish	累进首段期数
            //dzhhkzhl	定制还款种类
            //xzuetqhk	需足额提前还款
            //dzhkriqi	定制还款日期
            //huanbjee	还本金额
            //tqhkhxfs	提前还款还息方式
            //huankzhh	还款账号
            //hkzhhzxh	还款账号子序号
            //benqqish	本期期数
            //qishriqi	起始日期
            //daoqriqi	到期日期
            //huanbjee	还本金额
            //hxijinee	还息金额
            //zwhkriqi	最晚还款日
            ResultDto<Ln3110RespDto> ln3110ResultDto = dscms2CoreLnClientService.ln3110(ln3110ReqDto);
            String ln3110Code = Optional.ofNullable(ln3110ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3110Meesage = Optional.ofNullable(ln3110ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Ln3110RespDto ln3110RespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3110ResultDto.getCode())) {
                //  获取相关的值并解析
                ln3110RespDto = ln3110ResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, ln3110Code, ln3110Meesage);
            }
            page.addAll(ln3110RespDto.getLstdkhk());
            page.setTotal(ln3110RespDto.getZongqish());
            page.setPages(queryModel.getPage());
            page.setPageNum(queryModel.getSize());
        }
        return page;
    }

    /***
     * @param repayMode
     * @return java.lang.String
     * @author tangxun
     * @date 2021/5/21 19:29
     * @version 1.0.0
     * @desc
     * 1--利随本清               A001	按月结息，到期还本
     * 2--多次还息一次还本         A002	等额本息
     * 3--等额本息               A009	利随本清
     * 4--等额本金               A013	按月结息，按季还本
     * 5--等比累进               A014	按月结息，按半年还本
     * 6--等额累进               A015	按月结息，按年还本
     * 7--定制还款               A003     等额本金
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String transRepayType(String repayMode) {
        String mode = "";
        switch (repayMode) {
            //按月结息，到期还本
            case "A001":
                mode = "2";
                break;
            case "A002":
                mode = "3";
                break;
            case "A003":
                mode = "4";
                break;
            case "A009":
                mode = "1";
                break;
            case "A030":
                mode = "7";
                break;
            default:
                mode = "2";

        }
        return mode;
    }
    /**
     * 贷款还款方式映射 信贷-->核心
     * @param xdhkfs 信贷贷款还款方式码值
     * @return hxhkfs 核心贷款还款方式码值
     */
    public String toConverLoanPaymMtd(String xdhkfs) {
			/*信贷字典项：
			A001-按期付息,到期还本
			A002-等额本息
			A003-等额本金
			A009-利随本清
			A012-按226比例还款
			A013-按月还息按季还本
			A014-按月还息按半年还本
			A015-按月还息,按年还本
			A016-新226
			A017-前6个月按月还息，后6个月等额本息
			A018-前4个月按月还息，后8个月等额本息
			A019-第一年按月还息，接下来等额本息
			A020-334比例还款
			A021-433比例还款
			A022-10年期等额本息
			A023-按月付息，定期还本
			A030-定制还款
            A040-按期付息,按计划还本*/
        // 新核心字典项：1--利随本清、2--多次还息一次还本、3--等额本息、4--等额本金、5--等比累进、6--等额累进、7--定制还款
        String hxhkfs = "";
        if ("A001".equals(xdhkfs) || "A023".equals(xdhkfs)) {
            hxhkfs = "2";
        } else if("A002".equals(xdhkfs) || "A022".equals(xdhkfs) || "A031".equals(xdhkfs)) {
            hxhkfs = "3";
        } else if("A003".equals(xdhkfs)) {
            hxhkfs = "4";
        } else if("A009".equals(xdhkfs)) {
            hxhkfs = "1";
        } else if("A030".equals(xdhkfs) || "A040".equals(xdhkfs)) {
            hxhkfs = "7";
        }
        return hxhkfs;
    }
    /**
     *
     * 获取还款周期
     * */
    public String getHkzhouqi(String paym_freq_unit, String paym_freq_freq, String sy_flag, String due_day) {
        String hkzhouqi = "";
        if ("Q".equals(paym_freq_unit)) {
            //paym_freq_unit = "Q"; // 老信贷按季是Q
            hkzhouqi = paym_freq_freq + paym_freq_unit + sy_flag + due_day + "E" ; //还款周期
        } else if("M".equals(paym_freq_unit)) {
            hkzhouqi = paym_freq_freq + paym_freq_unit + sy_flag + due_day; //还款周期
        } else if("Y".equals(paym_freq_unit)) {
            hkzhouqi = paym_freq_freq + paym_freq_unit + sy_flag + "12" + due_day;//还款周期
        }
        return hkzhouqi;
    }
}
