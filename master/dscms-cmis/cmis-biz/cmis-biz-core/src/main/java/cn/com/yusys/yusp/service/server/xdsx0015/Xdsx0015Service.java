package cn.com.yusys.yusp.service.server.xdsx0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdsx0015.req.*;
import cn.com.yusys.yusp.dto.server.xdsx0015.resp.Xdsx0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:已批复的授信申请信息、押品信息以及合同信息同步
 *
 * @author xs
 * @version 1.0
 */
@Service
public class Xdsx0015Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0015Service.class);

    @Autowired
    private LmtAppMapper lmtAppMapper;//授信申请主表
    @Autowired
    private LmtAppSubMapper lmtAppSubMapper;//授信分项表
    @Autowired
    private LmtAppSubPrdMapper lmtAppSubPrdMapper;//授信分项产品表
    @Autowired
    private LmtReplyAccMapper lmtReplyAccMapper;//授信批复台账表
    @Autowired
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;//授信批复台账明细表
    @Autowired
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;//授信批复分项产品表
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;//押品信息创建base
    @Autowired
    private GuarBizRelMapper guarBizRelMapper;//授信与押品关系表
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;//贷款申请主表
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;//担保合同表
    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;//贷款申请与担保合同关系表
    @Autowired
    private GrtGuarContRelMapper grtGuarContRelMapper;//担保合同与押品关系表
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;//贷款合同
    @Autowired
    private AccLoanMapper accLoanMapper;//贷款台账
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private LmtReplyService lmtReplyService;
    @Autowired
    private LmtReplyAccService lmtReplyAccService;
    @Autowired
    private LmtReplyMapper lmtReplyMapper;
    @Resource
    private ICusClientService iCusClientService;//注入客户服务接口

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 1，授信：
     * 授信申请主表     LMT_APP
     * 授信分项表       LMT_APP_SUB
     * 授信分项产品表   LMT_APP_SUB_PRD
     * 授信批复台账表   LMT_REPLY_ACC
     * 授信批复台账明细表   LMT_REPLY_ACC_SUB
     * 授信批复台账分项产品表 LMT_REPLY_ACC_SUB_PRD
     * 押品信息创建  GUAR_BASE_INFO
     * 授信与押品关系表  GUAR_BIZ_REL
     * 2，用信
     * 贷款申请主表  IQP_LOAN_APP
     * 担保合同表 GRT_GUAR_CONT
     * 贷款申请与担保合同关系表  GRT_GUAR_BIZ_RST_REL
     * 担保合同与押品关系表      GRT_GUAR_CONT_REL
     *
     * @param xdsx0015DataReqDto
     * @return
     */
    @Transactional
    public Xdsx0015DataRespDto xdsx0015(Xdsx0015DataReqDto xdsx0015DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value);
        //返回对象
        Xdsx0015DataRespDto xdsx0015DataRespDto = new Xdsx0015DataRespDto();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

        try {
            /**
             * 1房抵e点贷
             * 2物联动产贷
             **/
            logger.info("已批复的授信申请信息、押品信息以及合同信息同步开始");
            //担保合同信息
            List<ListDb> listDb = xdsx0015DataReqDto.getListDb();
            //抵押物信息
            List<ListDy> listDy = xdsx0015DataReqDto.getListDy();
            //授信分项信息
            List<ListFx> listFx = xdsx0015DataReqDto.getListFx();
            //担保品信息
            List<ListGrt> listGrt = xdsx0015DataReqDto.getListGrt();
            //业务信息

            List<ListYw> listYw = xdsx0015DataReqDto.getListYw();
            //授信基本信息
            String op_flag = xdsx0015DataReqDto.getOp_flag();//类型
            String cus_id = xdsx0015DataReqDto.getCus_id();//客户号
            String cus_name = xdsx0015DataReqDto.getCus_name();//客户名称
            String cur_type = xdsx0015DataReqDto.getCur_type();//币种
            String apply_date = xdsx0015DataReqDto.getApply_date();//授信起始日
            String over_date = xdsx0015DataReqDto.getOver_date();//授信到期日
            String delay_months = xdsx0015DataReqDto.getDelay_months();//宽限期（月）
            String input_date = xdsx0015DataReqDto.getInput_date();//录入时间
            String manager_id = xdsx0015DataReqDto.getManager_id();//管户客户经理号
            String manager_br_id = xdsx0015DataReqDto.getManager_br_id();//管户机构
            String yx_serno = xdsx0015DataReqDto.getYx_serno();//影像流水号
            String fx_serno = xdsx0015DataReqDto.getFx_serno();//授信协议分项编号

            //listFx
            if (!(listFx != null && listFx.size() > 0)) {
                throw new Exception("授信分项列表不能为空");
            }
            String product = listFx.get(0).getProduct();//公司特色产品
            String lmtBizTypeProp = ""; // 授信品种类型属性
            // 产品属性映射 老信贷公司特色产品：5 房抵e点贷
            if ("5".equals(product)){
                lmtBizTypeProp = "P034"; // 房抵e点贷
            }

            if (!(listYw != null && listYw.size() > 0)) {
                throw new Exception("业务列表不能为空");
            }
            //listYw
            String loan_term = listYw.get(0).getLoan_term();//申请期限(月)

            if (!(listGrt != null && listGrt.size() > 0)) {
                throw new Exception("担保品列表不能为空");
            }

            if (!(listDb != null && listDb.size() > 0)) {
                throw new Exception("担保合同列表不能为空");
            }

            if (!(listDy != null && listDy.size() > 0)) {
                throw new Exception("抵押物列表不能为空");
            }
            CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cus_id);
            // 获取个人客户基本信息
            CusIndivDto cusIndivDto = new CusIndivDto();
            logger.info("根据客户号获取个人客户基本信息请求：" + cus_id);
            cusIndivDto = cmisCusClientService.queryCusindivByCusid(cus_id).getData();
            logger.info("根据客户号获取个人客户基本信息返回：" + cusIndivDto);
            if (Objects.isNull(cusIndivDto)) {
                throw new Exception("个人客户基本信息列表不能为空");
            }
            String cusType = cusIndivDto.getCusType();//客户类型
            String certCode = cusBaseDto.getCertCode();//证件号
            String certType = cusBaseDto.getCertType();//证件类型

            //授信起始日
            if(StringUtils.isEmpty(apply_date) || StringUtils.isEmpty(over_date)){
                throw new Exception("授信起始日为空");
            }
            //授信期限映射
            int term = 0;
            if ("000100".equals(loan_term)){
                term = 1;//(月)
            } else if ("000300".equals(loan_term)){
                term = 3;//(月)
            } else if ("000600".equals(loan_term)){
                term = 6;//(月)
            } else if ("100000".equals(loan_term)){
                term = 120;//(月)10年
            } else if ("050000".equals(loan_term)){
                term = 60;//(月)5年
            } else if ("030000".equals(loan_term)){
                term = 36;//(月)3年
            } else if ("010000".equals(loan_term)){
                term = 12;//(月)1年
            } else {
                throw new Exception("授信期限为空");
            }

            Map dataMap = new HashMap();
            dataMap.put("cus_id", cus_id);
            dataMap.put("cus_name", cus_name);
            dataMap.put("cus_type", cusType);
            dataMap.put("cert_code", certCode);
            dataMap.put("cert_type", certType);
            dataMap.put("cur_type", cur_type);
            dataMap.put("delay_months", delay_months);
            dataMap.put("input_date", input_date);
            dataMap.put("manager_id", manager_id);
            dataMap.put("manager_br_id", manager_br_id);
            dataMap.put("lmtBizTypeProp", lmtBizTypeProp);
            dataMap.put("fx_serno", fx_serno);
            dataMap.put("term", term);
            dataMap.put("yx_serno",yx_serno);
//            1、若类型为2物联动产贷时，根据客户号查询对公客户信息，执行以下操作： 此接口现仅用于房抵e点贷，物联动产贷逻辑为老信贷。
            if ("2".equals(op_flag)) {
                logger.info("物联动产贷产品处理开始");
//            1）根据抵押品列表，先查询质押品表是否存在同一质押品，若不存在则生成一条记录查询质押品表；
                //押品信息创建base  GUAR_BASE_INFO
                List<GuarBaseInfo> guarBaseInfoList = listDy
                        .stream()
                        .map(e -> {
                            GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                            guarBaseInfo.setSerno(UUID.randomUUID().toString().substring(0, 20));
                            guarBaseInfo.setGuarNo(e.getGuaranty_id());//抵押物编号
                            guarBaseInfo.setGrtFlag(e.getGuaranty_type());//抵押物类型
                            guarBaseInfo.setPldimnMemo(e.getGage_name());//质押物名称
                            guarBaseInfo.setGuarCusId(e.getCus_id());//质押人ID
                            guarBaseInfo.setGuarNo(e.getEval_date());//评估日期
                            guarBaseInfo.setGuarNo(e.getGuaranty_id());
                            guarBaseInfo.setGuarNo(e.getGuaranty_id());
                            guarBaseInfo.setInputId(cusBaseDto.getInputId());//
                            guarBaseInfo.setInputBrId(cusBaseDto.getInputBrId());
                            guarBaseInfo.setUpdId(cusBaseDto.getInputBrId());//
                            guarBaseInfo.setUpdBrId(cusBaseDto.getUpdBrId());//
                            return guarBaseInfo;
                        })
                        .collect(Collectors.toList());
                guarBaseInfoMapper.insertGuarBaseInfoList(guarBaseInfoList);
//            2）根据客户号以及产品编号找到授信协议中的分项编号；若查询不到则直接返回报错“该客户不存在有效的动产质押贷授信”；
                String subAccNo = lmtReplyAccSubPrdMapper.queryWldcdAccSubNoByCusId(cus_id);
                if (!"".equals(subAccNo)) {
                    throw new Exception("该客户不存在有效的动产质押贷授信");
                }
//            3）查询授信分项与担保品关联表中是否存在押品关联，若不存在则生成一条记录插入授信分项与担保品关联表中；
                QueryModel model = new QueryModel();
                model.addCondition("serno", subAccNo);
                List<GuarBizRel> guarBizRelList = guarBizRelMapper.selectByModel(model);
                if (!(guarBizRelList != null && !guarBizRelList.isEmpty())) {
                    List<GuarBizRel> gBizRelList = listGrt.stream().map(e -> {
                        GuarBizRel guarBizRel = new GuarBizRel();
                        guarBizRel.setPkId(UUID.randomUUID().toString().substring(0, 20));
                        guarBizRel.setSerno(subAccNo);//业务流水号
                        guarBizRel.setGuarNo(e.getGrt_serno());//押品统一编号
                        guarBizRel.setInputId(manager_id);//登记人
                        guarBizRel.setInputId(cusBaseDto.getInputId());//
                        guarBizRel.setInputBrId(cusBaseDto.getInputBrId());
                        guarBizRel.setUpdId(cusBaseDto.getInputBrId());//
                        guarBizRel.setUpdBrId(cusBaseDto.getUpdBrId());//
                        return guarBizRel;
                    }).collect(Collectors.toList());
                    guarBizRelList.addAll(gBizRelList);
                }
                guarBizRelMapper.insertGuarBizRelList(guarBizRelList);
//            4）生成贷款申请、担保合同及担保合同与业务、押品的关联
                insertIqpGrtCont(cusIndivDto, listDb, listDy, dataMap);
                logger.info("物联动产贷产品处理结束");
            }
//            2、当类型为1房抵e点贷时，根据客户号查询个人客户信息，若查询不到，则返回报错“该客户号在信贷系统不存在，请确认！”，否则执行以下操作：
            if ("1".equals(op_flag)) {
                logger.info("房抵e点贷产品处理开始：");
//            1）根据客户号查询贷款合同表，查询是否存在生效的合同；
                BigDecimal contNum = ctrLoanContMapper.checkCusSxContByCusId(cus_id);
//            2）根据客户号查询贷款台账表，查询是否存在未关闭的贷款台账；
                BigDecimal accNum = accLoanMapper.checkCusNotClosedAccByCusId(cus_id);
//            3）根据客户号查询授信协议表，查询是否存在授信协议；
                LmtReplyAcc lmtReplyAcc = lmtReplyAccMapper.queryLmtAppSernoByCusId(cus_id);
//            4）若存在授信协议且不存在生效的合同且不存在未关闭的贷款台账：
                logger.info("判断存在授信协议且不存在生效的合同且不存在未关闭的贷款台账：");
                if (Objects.nonNull(lmtReplyAcc) && contNum.compareTo(BigDecimal.ZERO) == 0 && accNum.compareTo(BigDecimal.ZERO) == 0) {
                    String inureDate = lmtReplyAcc.getInureDate();
                    Integer lmtTerm = lmtReplyAcc.getLmtTerm();
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDate parse = LocalDate.parse(inureDate, dateTimeFormatter);
                    LocalDate now = LocalDate.parse(openDay, dateTimeFormatter);
                    int years = parse.until(now).getYears();
                    int months = parse.until(now).getMonths();
                    // 授信失效超过1年，做授信新增
                    if ((years * 12 + months) - lmtTerm > 12) {
                        sxxz(xdsx0015DataReqDto, dataMap, cusIndivDto);
                    } else {
                        // 不超过1年做授信续作
                        LmtApp lmtApp = lmtAppService.selectBySerno(lmtReplyAcc.getSerno());
                        Map map = lmtAppService.saveLmtApp(lmtApp, "0");
                        if (!Objects.equals(EcbEnum.ECB010000.key, map.get("rtnCode"))) {
                            throw BizException.error(null, map.get("rtnCode").toString(), map.get("rtnMsg"));
                        }
                    }
                } else {
                    logger.info("不满足存在授信协议且不存在生效的合同且不存在未关闭的贷款台账条件，生成授信申请表开始：");
                    sxxz(xdsx0015DataReqDto, dataMap, cusIndivDto);
                }
            }
            //2用信
            xdsx0015DataRespDto.setCus_id(cus_id);
            xdsx0015DataRespDto.setDb_cus_name(cus_name);
            logger.info("Xdsx0015:已批复的授信申请信息、押品信息以及合同信息同步结束，返回信息：" + xdsx0015DataRespDto);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, e.getMessage());
            throw BizException.error(null, EpbEnum.EPB099999.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value);
        return xdsx0015DataRespDto;
    }

    public void sxxz(Xdsx0015DataReqDto xdsx0015DataReqDto, Map dataMap, CusIndivDto cusIndivDto) throws Exception {
        //授信基本信息
        String cus_id = xdsx0015DataReqDto.getCus_id();//客户号
        String cus_name = xdsx0015DataReqDto.getCus_name();//客户名称
        String cur_type = xdsx0015DataReqDto.getCur_type();//币种
        String crd_totl_sum_amt = xdsx0015DataReqDto.getCrd_totl_sum_amt();//授信总额
        String delay_months = xdsx0015DataReqDto.getDelay_months();//宽限期（月）
        String input_date = xdsx0015DataReqDto.getInput_date();//录入时间
        String manager_id = xdsx0015DataReqDto.getManager_id();//管户客户经理号
        String manager_br_id = xdsx0015DataReqDto.getManager_br_id();//管户机构
        int term = (int) dataMap.get("term");
        // 5)若不存在授信协议，执行以下操作：
        // 5.1）生成授信申请表;
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
        dataMap.put("serno", serno);
        LmtApp lmtApp = new LmtApp();
        lmtApp.setPkId(UUID.randomUUID().toString());
        lmtApp.setLmtType(CmisCommonConstants.LMT_TYPE_01);//授信类型
        lmtApp.setIsGrp(CmisCommonConstants.YES_NO_0);//是否集团 0否
        lmtApp.setCusId(cus_id);//客户编号
        lmtApp.setCusName(cus_name);//客户名称
        lmtApp.setCusType(cusIndivDto.getCusType());//客户类型
        lmtApp.setSerno(serno);//申请流水号
        lmtApp.setCurType(cur_type);//币种
        lmtApp.setOpenTotalLmtAmt(new BigDecimal(crd_totl_sum_amt));//授信总额
        lmtApp.setLowRiskTotalLmtAmt(new BigDecimal("0"));//低风险额度合计
        lmtApp.setEvalHighCurfundLmtAmt(new BigDecimal("0"));//测算最高流动资金贷款额度
        lmtApp.setLmtTerm(term);//授信期限
        lmtApp.setLmtGraperTerm(Integer.parseInt(delay_months));//授信宽限期
        lmtApp.setSxkdRiskResult("0");//省心快贷风险拦截结果
        lmtApp.setIsSubAutoAppr("0");//是否提交自动化审批 1是0否
        lmtApp.setManagerId(manager_id);//主管客户经理
        lmtApp.setManagerBrId(manager_br_id);//主管机构
        lmtApp.setInputDate(input_date);//登记日期
        lmtApp.setInputId(manager_id);//登陆客户经理
        lmtApp.setInputBrId(manager_br_id);//登录机构
        lmtApp.setUpdId(manager_id);//最近修改人
        lmtApp.setUpdBrId(manager_br_id);//最近修改机构
        lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//最近修改日期;
        lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型新增
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//审批状态
        lmtAppMapper.insert(lmtApp);
        logger.info("生成授信申请表结束：" + serno);
        // 5.2）
        // * 1、插入授信申请分项表 LMT_APP_SUB
        // * 2、插入押品信息表 GUAR_BASE_INFO
        // * 3、插入押品业务关联表 GUAR_BIZ_REL
        // * 4、插入授信申请分项品种表 LMT_APP_SUB_PRD
        insertLmtAppSubInfo(cusIndivDto,xdsx0015DataReqDto.getListDy(),xdsx0015DataReqDto.getListFx(),xdsx0015DataReqDto.getListYw(),xdsx0015DataReqDto.getListGrt(),dataMap);

        // 许晓要求：此段逻辑仅判断<=500万 走自动审批
/*//            5.61）当授信总额大于200万且小于等于500万时，授信申请为人工审批，生成贷款申请、担保合同；
                    if (new BigDecimal(crd_totl_sum_amt).compareTo(new BigDecimal(2000000)) > 0 && new BigDecimal(crd_totl_sum_amt).compareTo(new BigDecimal(5000000)) <= 0) {
                        insertIqpGrtCont(cusBaseDto, listDb, listDy, dataMap);
                    }*/
//            5.3）当授信总额小于等于500万，授信申请为自动审批，直接执行授信审批流程后处理生成授信协议
        if (new BigDecimal(crd_totl_sum_amt).compareTo(new BigDecimal(5000000)) <= 0) {
            logger.info("当授信总额小于等于500万，授信申请为自动审批，直接执行授信审批流程后处理生成授信协议处理开始：" + serno);
            lmtAppService.handleBusinessAfterStart(serno);
            lmtAppService.handleBusinessAfterEnd(serno, manager_id, manager_br_id, "");
            dataMap.put("serno", serno);//授信流水号
            insertIqpGrtCont(cusIndivDto, xdsx0015DataReqDto.getListDb(), xdsx0015DataReqDto.getListDy(), dataMap);
            logger.info("当授信总额小于等于500万，授信申请为自动审批，直接执行授信审批流程后处理生成授信协议处理结束：" + serno);
        }
    }

    /**
     * 1、插入授信申请分项表 LMT_APP_SUB
     * 2、插入押品信息表 GUAR_BASE_INFO
     * 3、插入押品业务关联表 GUAR_BIZ_REL
     * 4、插入授信申请分项品种表 LMT_APP_SUB_PRD
     */
    private void insertLmtAppSubInfo(CusIndivDto cusIndivDto, List<ListDy> listDy, List<ListFx> listFx, List<ListYw> listYw, List<ListGrt> listGrt, Map dataMap) {
        String cus_id = (String) dataMap.get("cus_id");
        String cus_name = (String) dataMap.get("cus_name");
        String cus_type = (String) dataMap.get("cus_type");
        String cert_type = (String) dataMap.get("cert_type");
        String cert_code = (String) dataMap.get("cert_code");
        String cur_type = (String) dataMap.get("cur_type");
        String delay_months = (String) dataMap.get("delay_months");
        String input_date = (String) dataMap.get("input_date");
        String manager_id = (String) dataMap.get("manager_id");
        String manager_br_id = (String) dataMap.get("manager_br_id");
        String lmtBizTypeProp = (String) dataMap.get("lmtBizTypeProp");
        String serno = (String) dataMap.get("serno");
        int term = (int) dataMap.get("term");
        // 1)生成抵押品信息;
        //押品信息创建  GUAR_BASE_INFO
        logger.info("*****XDSX0015:抵押品信息插入开始************");
        listDy.forEach(dy->{
            //根据押品编号获取押品信息表是否存在此押品，若存在，不落表
            int guarCount = guarBaseInfoMapper.selectContByGuarNo(dy.getGuaranty_id());
            if (0 == guarCount) {
                String guarSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GUAR_BASE_SEQ, new HashMap<>());
                GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                guarBaseInfo.setSerno(guarSerno);//押品流水
                guarBaseInfo.setGuarNo(dy.getGuaranty_id());//抵押物编号
                guarBaseInfo.setPldimnMemo(dy.getGage_name());//质押物名称
                guarBaseInfo.setGuarTypeCd(dy.getGuaranty_type());//抵押物类型
                guarBaseInfo.setGrtFlag("01");//抵押/质押标识 01抵押
                guarBaseInfo.setCusId(cus_id);//借款人
                guarBaseInfo.setGuarCusId(cus_id);//押品所有人编号
                guarBaseInfo.setGuarCusName(cus_name);//押品所有人名称
                guarBaseInfo.setGuarCusType(cus_type);//押品所有人类型
                guarBaseInfo.setGuarCertType(cert_type);//押品所有人证件类型
                guarBaseInfo.setGuarCertCode(cert_code);//押品所有人证件号码
                guarBaseInfo.setEvalDate(dy.getEval_date());//评估日期
                guarBaseInfo.setNewcode(dy.getGuaranty_type());//新押品编码
                guarBaseInfo.setNewlabel(dy.getGage_name());//新押品编码名称
                guarBaseInfo.setCreateSys("0");// 0信贷系统  1押品系统
                guarBaseInfo.setRegState("02");// 02未登记 01 已登记
                guarBaseInfo.setInputId(manager_id);//登陆客户经理
                guarBaseInfo.setInputBrId(manager_br_id);//登录机构
                guarBaseInfo.setManagerId(manager_id);//主管客户经理
                guarBaseInfo.setManagerBrId(manager_br_id);//主管机构
                guarBaseInfo.setUpdId(manager_id);//
                guarBaseInfo.setUpdBrId(manager_br_id);//
                guarBaseInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型新增
                guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//审批状态
                guarBaseInfo.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 登记日期
                guarBaseInfo.setEvalAmt(StringUtils.isNotBlank(dy.getConfirm_eval()) ? new BigDecimal(dy.getConfirm_eval()) : BigDecimal.ZERO);//评估价值
                guarBaseInfoMapper.insertSelective(guarBaseInfo);
                logger.info("*****XDSX0015:抵押品信息插入************" + guarBaseInfo);
            }
        });
        logger.info("生成抵押品信息结束" );
        //2)生成授信申请分项、授信申请产品表、授信分项与押品关系表
        //授信分项产品表 LMT_APP_SUB_PRD
        logger.info("*****XDSX0015:生成授信申请分项、授信申请产品表、授信分项与押品关系表开始************");
        // 获取产品属性
        listFx.forEach(fx->{
            String subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()); //授信申请分项流水号
            LmtAppSub lmtAppSub = new LmtAppSub();
            lmtAppSub.setPkId(UUID.randomUUID().toString());
            lmtAppSub.setSubSerno(subSerno);//授信申请分项流水号
            lmtAppSub.setSubName("其他个人经营性贷款");//分项名称
            lmtAppSub.setCusId(cus_id);//客户编号
            lmtAppSub.setCusName(cus_name);//客户名称
            lmtAppSub.setSerno(serno);//授信申请流水号
            // lmtAppSub.setGuarMode(fx.getFx_assure_means_main());//分项担保方式
            lmtAppSub.setGuarMode("10");//担保方式：抵押
            lmtAppSub.setCurType(cur_type);//币种
            lmtAppSub.setLmtAmt(new BigDecimal(fx.getCrd_lmt()));//授信额度（元）
            lmtAppSub.setLmtTerm(term);//授信期限
            lmtAppSub.setIsPreLmt("0");//是否预授信额度 1是0否
            lmtAppSub.setIsRevolvLimit("1");//是否循环额度 1是0否
            lmtAppSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型新增
            lmtAppSub.setInputId(manager_id);// 登记人
            lmtAppSub.setInputBrId(manager_br_id);//
            lmtAppSub.setUpdId(manager_id);//
            lmtAppSub.setUpdBrId(manager_br_id);//
            listYw.forEach(yw->{
                // 判断是否是该分项下的产品信息
                if (yw.getFlag_serno()!=null && yw.getFlag_serno().equals(fx.getFlag_serno())){
                    String subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()); //分项品种流水号
                    LmtAppSubPrd lmtAppSubPrd = new LmtAppSubPrd();
                    lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                    lmtAppSubPrd.setSubSerno(subSerno); // 分项流水号
                    lmtAppSubPrd.setSubPrdSerno(subPrdSerno); // 分项品种流水号
                    lmtAppSubPrd.setLmtBizType("20010102");//授信品种编号
                    lmtAppSubPrd.setLmtBizTypeName("其他个人经营性贷款");//授信品种编号
                    lmtAppSubPrd.setLmtBizTypeProp(lmtBizTypeProp); // 授信品种类型属性
                    lmtAppSubPrd.setIsRevolvLimit("1");//是否循环 1是0否
                    lmtAppSubPrd.setCusId(cus_id);//客户编号
                    lmtAppSubPrd.setCusName(cus_name);//客户名称
                    //lmtAppSubPrd.setGuarMode(listFx.get(i).getFx_assure_means_main());//担保方式
                    lmtAppSubPrd.setGuarMode("10");//担保方式：抵押
                    lmtAppSubPrd.setCurType(cur_type);//币种
                    lmtAppSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型新增
                    lmtAppSubPrd.setLmtTerm(term);//授信期限
                    lmtAppSubPrd.setLmtGraperTerm(0);//授信宽限期
                    lmtAppSubPrd.setChgFlag("0");//是否变更 1是0否
                    lmtAppSubPrd.setIsPreLmt("0");//是否预授信
                    lmtAppSubPrd.setIsSfcaLmt("0");//是否存量授信标志
                    lmtAppSubPrd.setRepayMode("A001");//还款方式 STD_REPAY_MODE 按期付息到期还本
                    lmtAppSubPrd.setEiMode("01");//结息方式 STD_EI_MODE 按月结息
                    lmtAppSubPrd.setLmtAmt(new BigDecimal(yw.getCrd_lmt()));//授信额度
                    // 判断年利率是否为空
                    if((StringUtils.isNotBlank(yw.getReality_ir_y()))) {
                        lmtAppSubPrd.setRateYear(new BigDecimal(yw.getReality_ir_y()));//年利率
                    }
                    lmtAppSubPrd.setInputId(manager_id);//
                    lmtAppSubPrd.setInputBrId(manager_br_id);
                    lmtAppSubPrd.setUpdId(manager_id);//
                    lmtAppSubPrd.setUpdBrId(manager_br_id);//
                    lmtAppSubPrdMapper.insertSelective(lmtAppSubPrd);
                }
            });
            //授信与押品关系表  GUAR_BIZ_REL
            listGrt.forEach(grt->{
                GuarBizRel guarBizRel = new GuarBizRel();
                guarBizRel.setPkId(UUID.randomUUID().toString().substring(0, 20));
                guarBizRel.setSerno(subSerno);//业务流水号
                guarBizRel.setGuarNo(grt.getGrt_serno());//押品统一编号
                guarBizRel.setOprType(CmisCommonConstants.OP_TYPE_01);//操作类型
                guarBizRel.setInputId(manager_id);//登记人
                guarBizRel.setInputBrId(manager_br_id);
                guarBizRel.setUpdId(manager_id);//
                guarBizRel.setUpdBrId(manager_br_id);//

                guarBizRel.setIsAddGuar("101");//主担保 2021-11-24新增
                guarBizRelMapper.insertSelective(guarBizRel);
            });
            lmtAppSubMapper.insertSelective(lmtAppSub);
            logger.info("*****XDSX0015:生成授信申请分项、授信申请产品表、授信分项与押品关系表结束************,分项流水：" + subSerno);
        });
    }

    /**
     * 1、生成贷款申请iqp_loan_app；
     * 2、生成担保合同信息Grt_Guar_Cont
     * 3、生成贷款申请与担保合同的关联GRT_GUAR_BIZ_RST_REL
     * 4、生成担保合同与押品关联关系grt_guar_cont_rel
     */
    private void insertIqpGrtCont(CusIndivDto cusIndivDto, List<ListDb> listDb, List<ListDy> listDy, Map map) {
        //获取授信批复台账表
        LmtReplyAcc lmtReplyAcc = lmtReplyAccMapper.selectBySerno((String)map.get("serno"));
        // 授信批复台账分项明细表 --待确认 房贷是否仅生成一笔分项明细与分项产品明细
        LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubMapper.selectBySerno((String)map.get("serno"));
        //授信批复台账分项适用品种明细表
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByAccSubNo(lmtReplyAccSub.getAccSubNo());

        String cus_id = (String) map.get("cus_id");
        String cus_name = (String) map.get("cus_name");
        String cur_type = (String) map.get("cur_type");
        String delay_months = (String) map.get("delay_months");
        String input_date = (String) map.get("input_date");
        String manager_id = (String) map.get("manager_id");
        String manager_br_id = (String) map.get("manager_br_id");
        String limitAccNo = (String) map.get("fx_serno");
        String yx_serno = (String) map.get("yx_serno");
        //贷款申请主表IQP_LOAN_APP
        IqpLoanApp iqpLoanApp = new IqpLoanApp();
        String iqpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap());
        String contNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, new HashMap());
        //iqpLoanApp.setSerno(iqpSerno);
        iqpLoanApp.setIqpSerno(iqpSerno);
        iqpLoanApp.setCusId(cus_id);// 客户编号
        iqpLoanApp.setCusName(cus_name);// 客户名称
        iqpLoanApp.setCertCode(cusIndivDto.getCertCode());// 证件号码
        iqpLoanApp.setBizType("02");// 业务类型
        iqpLoanApp.setPrdId("022011");// 产品编号
        iqpLoanApp.setPrdName("其他个人经营性贷款");// 产品名称
        iqpLoanApp.setPrdTypeProp("P034"); //产品属性 房抵e点贷;
        iqpLoanApp.setContType("2");//合同类型 STD_CONT_TYPE 1：一般 2：最高额
        iqpLoanApp.setLoanModal("1");//贷款形式 1：新增STD_LOAN_MODAL
        //iqpLoanApp.setLoanCha("");//贷款性质
        iqpLoanApp.setPreferRateFlag("0");//是否申请优惠利率
        iqpLoanApp.setBeforehandInd("0");//是否先放款后抵押
        iqpLoanApp.setIsOutstndTrdLmtAmt("0");//第三方标识
        iqpLoanApp.setGuarWay("10");//主担保方式 ：抵押
        iqpLoanApp.setIsRenew("0");//是否续签
        iqpLoanApp.setIsUtilLmt("1");//是否使用授信额度
        iqpLoanApp.setThirdChnlSerno(yx_serno);//手机银行影像流水号
        // 调用 对公客户基本信息查询接口，
        // 将 地址与联系方式 信息更新值贷款申请表中
        CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cus_id).getData();
        if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
            iqpLoanApp.setLinkman(cusCorpDto.getFreqLinkman());//联系人
            iqpLoanApp.setPhone(cusCorpDto.getFreqLinkmanTel());// 手机号码
            iqpLoanApp.setFax(cusCorpDto.getFax());//传真
            iqpLoanApp.setEmail(cusCorpDto.getLinkmanEmail());//邮箱
            iqpLoanApp.setQq(cusCorpDto.getQq());//QQ
            iqpLoanApp.setWechat(cusCorpDto.getWechatNo());//微信
            iqpLoanApp.setDeliveryAddr(cusCorpDto.getSendAddr());//送达地址
        }
        iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
        iqpLoanApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);//所属条线
        iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//审批状态
        iqpLoanApp.setContNo(contNo);//合同号
        iqpLoanApp.setAppCurType(cur_type);// 币种
        iqpLoanApp.setGraper(delay_months);// 宽限期
        iqpLoanApp.setInputDate(input_date);// 录入时间
        iqpLoanApp.setManagerId(manager_id);// 管户经理
        iqpLoanApp.setManagerBrId(manager_br_id);// 管户机构
        iqpLoanApp.setInputId(manager_id);//
        iqpLoanApp.setInputBrId(manager_br_id);
        iqpLoanApp.setUpdId(manager_id);//
        iqpLoanApp.setUpdBrId(manager_br_id);//
        iqpLoanApp.setIsOnlineDraw("1");//是否线上提款
        iqpLoanApp.setLmtAccNo(lmtReplyAccSubPrd.getAccSubPrdNo()); //授信额度编号
        iqpLoanApp.setReplyNo(lmtReplyAcc.getReplySerno()); //批复流水号
        //担保合同与押品关系表 GRT_GUAR_CONT_REL
        List<GrtGuarContRel> grtGuarContRelList = new ArrayList<>();
        ////担保合同表 GRT_GUAR_CONT
        logger.info("生成担保合同表开始：");
        for (int i = 0; i < listDb.size(); i++) {
            //担保方式映射
            GrtGuarCont grtGuarCont = new GrtGuarCont();
            String grtContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, new HashMap<>());
            String guarPkId = UUID.randomUUID().toString();
            grtGuarCont.setGuarPkId(guarPkId);// 担保合同流水编号
            grtGuarCont.setGuarContNo(grtContNo);//担保合同编号
            grtGuarCont.setBizLine(CmisCommonConstants.STD_BELG_LINE_03);//所属条线
            grtGuarCont.setGuarContCnNo("");//担保合同中文合同编号
            grtGuarCont.setIsSuperaddGuar(CmisCommonConstants.STD_ZB_YES_NO_0);//是否追加担保1是 0否
            grtGuarCont.setCusId(listDb.get(i).getDb_cus_id());//借款人客户编号
            grtGuarCont.setCusName(listDb.get(i).getDb_cus_name());//担保合同客户名
            grtGuarCont.setGuarContType("B");//担保合同类型 A一般担保合同 B 最高额担保合同
            //grtGuarCont.setGuarWay(listDb.get(i).getGuar_way());//担保方式  //10抵押 20 质押 30保证
            grtGuarCont.setGuarWay("10");//担保方式  //10抵押 20 质押 30保证
            grtGuarCont.setIsOnlinePld("1");//是否在线抵押 1是 0否
            grtGuarCont.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);//是否授信项下 1是 0否
            grtGuarCont.setLnCardNo("");//贷款卡号
            grtGuarCont.setCurType("CNY");//币种
            grtGuarCont.setTermType("");//期限类型 D日  M月  Y年
//            grtGuarCont.setGuarTerm("");//担保期限
//            grtGuarCont.setGuarStartDate("");//担保起始日
//            grtGuarCont.setGuarEndDate("");//担保终止日
            grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_100);//担保合同状态//100 未生效 101 生效 104 注销  107 作废
            grtGuarCont.setReplyNo(lmtReplyAcc.getReplySerno());//批复编号
            grtGuarCont.setLmtAccNo(lmtReplyAccSubPrd.getAccSubPrdNo());//授信额度编号
            grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            grtGuarCont.setInputId(manager_id);//
            grtGuarCont.setInputBrId(manager_br_id);
            grtGuarCont.setManagerId(manager_id);//
            grtGuarCont.setManagerBrId(manager_br_id);
            grtGuarCont.setUpdId(manager_id);//
            grtGuarCont.setUpdBrId(manager_br_id);//
                for (int j = 0; j < listDy.size(); j++) {
                    String pkId = UUID.randomUUID().toString();
                    GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
                    GrtGuarBizRstRel guarBizRstRel = new GrtGuarBizRstRel();
                    grtGuarContRel.setPkId(UUID.randomUUID().toString());
                    grtGuarContRel.setGuarContNo(grtContNo); //担保合同编号
                    grtGuarContRel.setContNo(contNo);
                    grtGuarContRel.setGuarNo(listDy.get(j).getGuaranty_id());// 押品统一编号
                    grtGuarContRel.setIsMainGuar("");//是否主押品 1是 0否
                    grtGuarContRel.setStatus("1");//1生效 0失效
                    grtGuarContRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    grtGuarContRel.setManagerId(manager_id);// 管户客户经理号
                    grtGuarContRel.setManagerBrId(manager_br_id);// 管户机构
                    grtGuarContRel.setInputId(manager_id);//
                    grtGuarContRel.setInputBrId(manager_br_id);
                    grtGuarContRel.setUpdId(manager_id);//
                    grtGuarContRel.setUpdBrId(manager_br_id);//
                    grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    //贷款申请与担保合同关系表  GRT_GUAR_BIZ_RST_REL
                    guarBizRstRel.setPkId(pkId);
                    guarBizRstRel.setSerno(iqpSerno);// 业务流水号
                    guarBizRstRel.setContNo(contNo);//合同编号
                    guarBizRstRel.setGuarPkId(pkId);// 担保合同流水号
                    guarBizRstRel.setGuarContNo(grtContNo);// 担保合同编号
                    guarBizRstRel.setGuarAmt(new BigDecimal(0));//担保金额
                    guarBizRstRel.setIsAddGuar(CmisCommonConstants.STD_ZB_YES_NO_0);//是否追加担保 1是 0否
                    guarBizRstRel.setIsPerGur(CmisCommonConstants.STD_ZB_YES_NO_0);//是否阶段性担保 1是 0否
                    guarBizRstRel.setCorreRel(CmisCommonConstants.STD_ZB_YES_NO_1);//关联关系 1生效 2 新增 3解除
                    guarBizRstRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    guarBizRstRel.setInputId(manager_id);//
                    guarBizRstRel.setInputBrId(manager_br_id);
                    guarBizRstRel.setUpdId(manager_id);//
                    guarBizRstRel.setUpdBrId(manager_br_id);//
                    // 担保合同与押品关系表新增
                    grtGuarContRelMapper.insertSelective(grtGuarContRel);
                    logger.info("担保合同与押品关系表新增结束,合同编号：" + contNo);
                    //  贷款申请与担保合同关系新增
                    grtGuarBizRstRelMapper.insertSelective(guarBizRstRel);
                    logger.info("贷款申请与担保合同关系新增结束,合同编号：" + contNo);
                }
            //担保合同新增
            grtGuarContMapper.insertSelective(grtGuarCont);
            logger.info("担保合同新增结束,担保合同编号：" + grtContNo);
        }
        //贷款申请新增
        iqpLoanAppMapper.insert(iqpLoanApp);
        logger.info("贷款申请新增结束,合同编号：" + contNo);
    }
}
