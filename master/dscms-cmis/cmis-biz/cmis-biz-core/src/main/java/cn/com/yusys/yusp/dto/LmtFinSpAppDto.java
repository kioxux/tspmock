package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinSpApp
 * @类描述: lmt_fin_sp_app数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 09:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtFinSpAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 融资协议编号 **/
	private String finCtrNo;
	
	/** 担保公司客户编号 **/
	private String cusId;
	
	/** 代偿申请日期 **/
	private String subpayAppDate;
	
	/** 本次代偿笔数 **/
	private String curtCurSubpayQnt;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 代偿总额 **/
	private java.math.BigDecimal subpayAmt;
	
	/** 收款人账号 **/
	private String pyeeAcctNo;
	
	/** 收款人账户名 **/
	private String pyeeAcctName;
	
	/** 收款人开户行行号 **/
	private String pyeeOpacOrgNo;
	
	/** 收款人开户行行名 **/
	private String pyeeOpanOrgName;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param finCtrNo
	 */
	public void setFinCtrNo(String finCtrNo) {
		this.finCtrNo = finCtrNo == null ? null : finCtrNo.trim();
	}
	
    /**
     * @return FinCtrNo
     */	
	public String getFinCtrNo() {
		return this.finCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param subpayAppDate
	 */
	public void setSubpayAppDate(String subpayAppDate) {
		this.subpayAppDate = subpayAppDate == null ? null : subpayAppDate.trim();
	}
	
    /**
     * @return SubpayAppDate
     */	
	public String getSubpayAppDate() {
		return this.subpayAppDate;
	}
	
	/**
	 * @param curtCurSubpayQnt
	 */
	public void setCurtCurSubpayQnt(String curtCurSubpayQnt) {
		this.curtCurSubpayQnt = curtCurSubpayQnt == null ? null : curtCurSubpayQnt.trim();
	}
	
    /**
     * @return CurtCurSubpayQnt
     */	
	public String getCurtCurSubpayQnt() {
		return this.curtCurSubpayQnt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param subpayAmt
	 */
	public void setSubpayAmt(java.math.BigDecimal subpayAmt) {
		this.subpayAmt = subpayAmt;
	}
	
    /**
     * @return SubpayAmt
     */	
	public java.math.BigDecimal getSubpayAmt() {
		return this.subpayAmt;
	}
	
	/**
	 * @param pyeeAcctNo
	 */
	public void setPyeeAcctNo(String pyeeAcctNo) {
		this.pyeeAcctNo = pyeeAcctNo == null ? null : pyeeAcctNo.trim();
	}
	
    /**
     * @return PyeeAcctNo
     */	
	public String getPyeeAcctNo() {
		return this.pyeeAcctNo;
	}
	
	/**
	 * @param pyeeAcctName
	 */
	public void setPyeeAcctName(String pyeeAcctName) {
		this.pyeeAcctName = pyeeAcctName == null ? null : pyeeAcctName.trim();
	}
	
    /**
     * @return PyeeAcctName
     */	
	public String getPyeeAcctName() {
		return this.pyeeAcctName;
	}
	
	/**
	 * @param pyeeOpacOrgNo
	 */
	public void setPyeeOpacOrgNo(String pyeeOpacOrgNo) {
		this.pyeeOpacOrgNo = pyeeOpacOrgNo == null ? null : pyeeOpacOrgNo.trim();
	}
	
    /**
     * @return PyeeOpacOrgNo
     */	
	public String getPyeeOpacOrgNo() {
		return this.pyeeOpacOrgNo;
	}
	
	/**
	 * @param pyeeOpanOrgName
	 */
	public void setPyeeOpanOrgName(String pyeeOpanOrgName) {
		this.pyeeOpanOrgName = pyeeOpanOrgName == null ? null : pyeeOpanOrgName.trim();
	}
	
    /**
     * @return PyeeOpanOrgName
     */	
	public String getPyeeOpanOrgName() {
		return this.pyeeOpanOrgName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}