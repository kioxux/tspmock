/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.LoanEdCheck;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req.Fb1146ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp.Fb1146RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LoanEdCheckMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LoanEdCheckService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:31:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LoanEdCheckService {

    @Autowired
    private LoanEdCheckMapper loanEdCheckMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LoanEdCheckService.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LoanEdCheck selectByPrimaryKey(String serno) {
        return loanEdCheckMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LoanEdCheck> selectAll(QueryModel model) {
        List<LoanEdCheck> records = (List<LoanEdCheck>) loanEdCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LoanEdCheck> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LoanEdCheck> list = loanEdCheckMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LoanEdCheck record) {
        return loanEdCheckMapper.insert(record);
    }

    /**
     * @方法名称: loanEdChecklist
     * @方法描述: 查询审核结果为空的数据（获取房抵e点贷受托信息待审核）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<LoanEdCheck> loanEdChecklist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("unreviewed", 1);
        return loanEdCheckMapper.selectByModel(model);
    }
    /**

     * @方法名称: riskXdGuarantyHislist
     * @方法描述: 查询审核结果为通过，未通过的数据（获取房抵e点贷受托信息审核）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<LoanEdCheck> loanEdCheckHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("reviewed", 1);
        return loanEdCheckMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public LoanEdCheck selectBySerno(String serno) {
        return loanEdCheckMapper.selectByPrimaryKey(serno);
    }


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LoanEdCheck record) {
        return loanEdCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LoanEdCheck record) {
        return loanEdCheckMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LoanEdCheck record) {
        return loanEdCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return loanEdCheckMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return loanEdCheckMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateLoanedcheck
     * @方法描述: 房抵e点贷受托支付受托信息审核
     * @参数与返回说明: 交易码fb1146
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public ResultDto<String> updateLoanedcheck(LoanEdCheck record) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (record != null) {
            log.info("房抵e点贷受托支付受托信息审核风控开始,流水号【{}】", record.getSerno());
            Fb1146ReqDto fb1146ReqDto = new Fb1146ReqDto();
            fb1146ReqDto.setApp_no(record.getSerno()); //申请流水号
            fb1146ReqDto.setVrify_task (record.getAuditRst()); //受托审核状态  0审核未通过1审核通过
            // 是否调整
            if("1".equals(record.getIsAdjRate())){
                fb1146ReqDto.setExec_rate(record.getAdjExecRate());//调整后利率
            }
            log.info("房抵e点贷客户经理否决调用受托信息审核请求：" + fb1146ReqDto);
            ResultDto<Fb1146RespDto> fb1146RespResultDto = dscms2CircpClientService.fb1146(fb1146ReqDto);
            log.info("房抵e点贷客户经理否决调用受托信息审核返回：" + fb1146RespResultDto);
            String fb1146Code = Optional.ofNullable(fb1146RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String fb1146Meesage = Optional.ofNullable(fb1146RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("房抵e点贷客户经理否决调用受托信息审核返回信息：" + fb1146Meesage);
            if (Objects.equals(fb1146Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 获取修改日期
                record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                loanEdCheckMapper.updateByPrimaryKey(record);
            } else {
                resultDto.setCode(fb1146Code);
                resultDto.setMessage(fb1146Meesage);
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        return resultDto;
    }

}
