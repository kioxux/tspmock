package cn.com.yusys.yusp.web.server.xdls0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0002.req.Xdls0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0002.resp.Xdls0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdls0002.Xdls0002Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 接口处理类:市民贷联系人信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0002Resource.class);

    @Autowired
    private Xdls0002Service xdls0002Service;


    /**
     * 交易码：xdls0002
     * 交易描述：市民贷联系人信息查询
     *
     * @param xdls0002DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdls0002")
    protected @ResponseBody
    ResultDto<Xdls0002DataRespDto> xdls0002(@Validated @RequestBody Xdls0002DataReqDto xdls0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002DataReqDto));
        Xdls0002DataRespDto xdls0002DataRespDto = new Xdls0002DataRespDto();// 响应Dto:市民贷联系人信息查询
        ResultDto<Xdls0002DataRespDto> xdls0002DataResultDto = new ResultDto<>();
        // 从xdls0002DataReqDto获取业务值进行业务逻辑处理
        String indgtSerno = xdls0002DataReqDto.getIndgtSerno();//调查流水号
        try {
            xdls0002DataRespDto = xdls0002Service.xdls0006(xdls0002DataReqDto);

            // 封装xdls0002DataResultDto中正确的返回码和返回信息
            xdls0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, e.getMessage());
            // 封装xdls0002DataResultDto中异常返回码和返回信息
            xdls0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdls0002DataRespDto到xdls0002DataResultDto中
        xdls0002DataResultDto.setData(xdls0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002DataResultDto));
        return xdls0002DataResultDto;
    }
}
