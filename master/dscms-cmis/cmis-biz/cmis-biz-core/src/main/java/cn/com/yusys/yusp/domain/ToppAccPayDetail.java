/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAccPayDetail
 * @类描述: topp_acc_pay_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-10 17:13:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "topp_acc_pay_detail")
public class ToppAccPayDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = false, length = 40)
	private String pvpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 交易对手账户 **/
	@Column(name = "TOPP_ACCNO", unique = false, nullable = true, length = 40)
	private String toppAccno;
	
	/** 交易对手名称 **/
	@Column(name = "TOPP_NAME", unique = false, nullable = true, length = 80)
	private String toppName;
	
	/** 交易对手金额 **/
	@Column(name = "TOPP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal toppAmt;
	
	/** 交易对手开户行号 **/
	@Column(name = "TOPP_BANK_NO", unique = false, nullable = true, length = 40)
	private String toppBankNo;
	
	/** 交易对手开户行名称 **/
	@Column(name = "TOPP_BANK_NAME", unique = false, nullable = true, length = 100)
	private String toppBankName;
	
	/** 状态 **/
	@Column(name = "status", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 原因一 **/
	@Column(name = "reason_first", unique = false, nullable = true, length = 65535)
	private String reasonFirst;
	
	/** 原因二 **/
	@Column(name = "reason_second", unique = false, nullable = true, length = 65535)
	private String reasonSecond;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 冻结编号 **/
	@Column(name = "FREEZE_NO", unique = false, nullable = true, length = 40)
	private String freezeNo;
	
	/** 支付对手账户 **/
	@Column(name = "PAY_ACCNO", unique = false, nullable = true, length = 40)
	private String payAccno;
	
	/** 支付对手名称 **/
	@Column(name = "PAY_NAME", unique = false, nullable = true, length = 80)
	private String payName;
	
	/** 业务受理编号 **/
	@Column(name = "BUS_NO", unique = false, nullable = true, length = 40)
	private String busNo;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param toppAccno
	 */
	public void setToppAccno(String toppAccno) {
		this.toppAccno = toppAccno;
	}
	
    /**
     * @return toppAccno
     */
	public String getToppAccno() {
		return this.toppAccno;
	}
	
	/**
	 * @param toppName
	 */
	public void setToppName(String toppName) {
		this.toppName = toppName;
	}
	
    /**
     * @return toppName
     */
	public String getToppName() {
		return this.toppName;
	}
	
	/**
	 * @param toppAmt
	 */
	public void setToppAmt(java.math.BigDecimal toppAmt) {
		this.toppAmt = toppAmt;
	}
	
    /**
     * @return toppAmt
     */
	public java.math.BigDecimal getToppAmt() {
		return this.toppAmt;
	}
	
	/**
	 * @param toppBankNo
	 */
	public void setToppBankNo(String toppBankNo) {
		this.toppBankNo = toppBankNo;
	}
	
    /**
     * @return toppBankNo
     */
	public String getToppBankNo() {
		return this.toppBankNo;
	}
	
	/**
	 * @param toppBankName
	 */
	public void setToppBankName(String toppBankName) {
		this.toppBankName = toppBankName;
	}
	
    /**
     * @return toppBankName
     */
	public String getToppBankName() {
		return this.toppBankName;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param reasonFirst
	 */
	public void setReasonFirst(String reasonFirst) {
		this.reasonFirst = reasonFirst;
	}
	
    /**
     * @return reasonFirst
     */
	public String getReasonFirst() {
		return this.reasonFirst;
	}
	
	/**
	 * @param reasonSecond
	 */
	public void setReasonSecond(String reasonSecond) {
		this.reasonSecond = reasonSecond;
	}
	
    /**
     * @return reasonSecond
     */
	public String getReasonSecond() {
		return this.reasonSecond;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param freezeNo
	 */
	public void setFreezeNo(String freezeNo) {
		this.freezeNo = freezeNo;
	}
	
    /**
     * @return freezeNo
     */
	public String getFreezeNo() {
		return this.freezeNo;
	}
	
	/**
	 * @param payAccno
	 */
	public void setPayAccno(String payAccno) {
		this.payAccno = payAccno;
	}
	
    /**
     * @return payAccno
     */
	public String getPayAccno() {
		return this.payAccno;
	}
	
	/**
	 * @param payName
	 */
	public void setPayName(String payName) {
		this.payName = payName;
	}
	
    /**
     * @return payName
     */
	public String getPayName() {
		return this.payName;
	}
	
	/**
	 * @param busNo
	 */
	public void setBusNo(String busNo) {
		this.busNo = busNo;
	}
	
    /**
     * @return busNo
     */
	public String getBusNo() {
		return this.busNo;
	}


}