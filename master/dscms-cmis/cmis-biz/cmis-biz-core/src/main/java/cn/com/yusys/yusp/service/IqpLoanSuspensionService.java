/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpLoanSuspension;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.IqpLoanSuspensionMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanSuspensionService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-29 10:33:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanSuspensionService {

    private static final Logger logger = LoggerFactory.getLogger(DocArchiveInfoService.class);

    @Autowired
    private IqpLoanSuspensionMapper iqpLoanSuspensionMapper;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private AccLoanService accLoanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLoanSuspension selectByPrimaryKey(String ilsSerno) {
        return iqpLoanSuspensionMapper.selectByPrimaryKey(ilsSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpLoanSuspension> selectAll(QueryModel model) {
        List<IqpLoanSuspension> records = (List<IqpLoanSuspension>) iqpLoanSuspensionMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpLoanSuspension> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        if (!StringUtils.isEmpty(model.getCondition().get("approveStatus"))) {
            // 前端上送的审批状态,以","分割
            String approveStatus = model.getCondition().get("approveStatus").toString();
            List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
            model.getCondition().put("approveStatusList", approveStatusList);
            model.getCondition().remove("approveStatus");
        }
        List<IqpLoanSuspension> list = iqpLoanSuspensionMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpLoanSuspension record) {
        return iqpLoanSuspensionMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanSuspension record) {
        return iqpLoanSuspensionMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpLoanSuspension record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        record.setUpdId(userInfo.getLoginCode());
        record.setUpdBrId(userInfo.getOrg().getCode());
        return iqpLoanSuspensionMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanSuspension record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        record.setUpdId(userInfo.getLoginCode());
        record.setUpdBrId(userInfo.getOrg().getCode());
        return iqpLoanSuspensionMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ilsSerno) {
        return iqpLoanSuspensionMapper.deleteByPrimaryKey(ilsSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanSuspensionMapper.deleteByIds(ids);
    }

    /**
     * 发送核心记账
     *
     * @author jijian_yx
     * @date 2021/7/31 16:18
     **/
    public void sendToHX(IqpLoanSuspension loanSuspension) {
        Ln3030ReqDto ln3030ReqDto = null;
        try {
            // 拼装请求参数
            ln3030ReqDto = new Ln3030ReqDto();
            ln3030ReqDto.setDkkhczbz("4");//操作标志 1录入；2修改；3复核；4直通
            ln3030ReqDto.setDkjiejuh(loanSuspension.getBillNo());// 贷款借据号
            ln3030ReqDto.setKehuhaoo(loanSuspension.getCusId());// 客户号
            ln3030ReqDto.setKehmingc(loanSuspension.getCusName());// 客户名称

            // 停止操作码 01:停息;02:取消停息
            String susOpt = loanSuspension.getSusOpt();
            if ("01".equals(susOpt)) {
                ln3030ReqDto.setJixibzhi("0");// 计息标志 0:不计息
            } else if ("02".equals(susOpt)) {
                ln3030ReqDto.setJixibzhi("1");// 计息标志 1:计息
            }
            // 是否补计提停息阶段利息 0:否;1:是
            String isCalculateInt = loanSuspension.getIsCalculateInt();
            if ("0".equals(isCalculateInt)) {
                ln3030ReqDto.setScjixirq(loanSuspension.getSusInputDate().replace("-", ""));// 上次计息日
            } else if ("1".equals(isCalculateInt)) {
                ln3030ReqDto.setScjixirq("");// 上次计息日
            }
            AccLoan accLoan = accLoanService.selectByBillNo(loanSuspension.getBillNo());
            ln3030ReqDto.setYuqililv(accLoan.getOverdueExecRate().multiply(new BigDecimal(100)));//逾期利率 ln3030 接口返回预期利率不能为空，暂时传值
            logger.info("贷款停息发送核心入参:[{}]", ln3030ReqDto);

            // 调用核心接口开始
            ResultDto<Ln3030RespDto> ln3030ResultDto = dscms2CoreLnClientService.ln3030(ln3030ReqDto);
            String ln3030Code = Optional.ofNullable(ln3030ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3030Meesage = Optional.ofNullable(ln3030ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Ln3030RespDto ln3030RespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3030ResultDto.getCode())) {
                //  获取相关的值并解析
                ln3030RespDto = ln3030ResultDto.getData();
                logger.info("贷款停息发送核心成功,返回结果[{}]", ln3030RespDto);
                loanSuspension.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                iqpLoanSuspensionMapper.updateByPrimaryKeySelective(loanSuspension);
            } else {
                logger.info("贷款停息发送核心失败,返回结果[{}]", ln3030Meesage);
                loanSuspension.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                loanSuspension.setErrorMessage(ln3030Meesage);
                iqpLoanSuspensionMapper.updateByPrimaryKeySelective(loanSuspension);
            }
        } catch (Exception e) {
            loanSuspension.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
            iqpLoanSuspensionMapper.updateByPrimaryKeySelective(loanSuspension);
            logger.error("贷款停息申请发送核心失败,失败原因[{}]", e.getMessage());
        }

    }
}
