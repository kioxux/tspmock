package cn.com.yusys.yusp.service.server.xdcz0013;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.req.Xdcz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.resp.Xdcz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:出账记录详情查看
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0013Service.class);

    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 出账记录详情查看
     * @param xdcz0013DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0013DataRespDto checkLmt(Xdcz0013DataReqDto xdcz0013DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataReqDto));
        Xdcz0013DataRespDto xdcz0013DataRespDto = new Xdcz0013DataRespDto();
        try {
            BigDecimal applyAmt = Optional.ofNullable(xdcz0013DataReqDto.getApplyAmt()).orElse(BigDecimal.ZERO);//申请金额
            String cusId = xdcz0013DataReqDto.getCusId();//客户编号
            String cusName = xdcz0013DataReqDto.getCusName();//客户名称
            BigDecimal bailRate = Optional.ofNullable(xdcz0013DataReqDto.getBailRate()).orElse(BigDecimal.ZERO);//保证金比例
            String contNo = xdcz0013DataReqDto.getContNo();//合同号

            BigDecimal dealBizSpacAmt = applyAmt.subtract(applyAmt.multiply(bailRate).divide(new BigDecimal("100")));//敞口金额
            // 额度校验
            CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
            cmisLmt0010ReqDealBizListDto.setCusId(cusId);
            cmisLmt0010ReqDealBizListDto.setCusName(cusName);
            cmisLmt0010ReqDealBizListDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0010ReqDealBizListDto.setDealBizAmt(applyAmt);//台账占用总额
            cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(dealBizSpacAmt);//台账占用敞口
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(bailRate);//保证金比例
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(applyAmt.multiply(bailRate));//保证金金额
            cmisLmt0010ReqDealBizListDto.setPrdId("042200");
            cmisLmt0010ReqDealBizListDto.setPrdName("银行承兑汇票");
            CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(Arrays.asList(cmisLmt0010ReqDealBizListDto));
            // 系统编号
            cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            // 金融机构代码
            cmisLmt0010ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            // 合同编号
            cmisLmt0010ReqDto.setBizNo(contNo);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010ReqDto));
            ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010RespDtoResultDto));
            if (Objects.equals(cmisLmt0010RespDtoResultDto.getCode(), EpbEnum.EPB099999.key)) {
                xdcz0013DataRespDto.setOpFlag(CmisBizConstants.NO);
                xdcz0013DataRespDto.setOpMsg(cmisLmt0010RespDtoResultDto.getMessage());
                return xdcz0013DataRespDto;
            }
            xdcz0013DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdcz0013DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, e.getMessage());
            xdcz0013DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdcz0013DataRespDto.setOpMsg("系统异常");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataRespDto));
        return xdcz0013DataRespDto;
    }
}
