/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CfgAssistantInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgAssistantInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-28 15:47:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgAssistantInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CfgAssistantInfo selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CfgAssistantInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CfgAssistantInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CfgAssistantInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CfgAssistantInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CfgAssistantInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: getNoticeByPrdIdAndBizType
     * @方法描述: 根据产品编号、业务阶段，查询业务小助手的注意事项
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<Map<String, Object>> getNoticeByPrdIdAndBizType(QueryModel queryModel);

    /**
     * @方法名称: getNoticeByPrdIdAndBizType
     * @方法描述: 根据产品编号、业务阶段，查询业务小助手的注意事项(只查询产品属性为空的数据)
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> getNoticeByPrdIdAndBizTypeNonPrdTypeProp(QueryModel queryModel);

    /**
     * @方法名称: getNoticeByPrdIdAndBizType
     * @方法描述: 根据产品编号、业务阶段，查询业务小助手的注意事项(只查询产品属性为空的数据)
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgAssistantInfo> checkNoticeByPrdIdAndBizTypeNonPrdTypeProp(@Param("prdCode") String prdCode);
}