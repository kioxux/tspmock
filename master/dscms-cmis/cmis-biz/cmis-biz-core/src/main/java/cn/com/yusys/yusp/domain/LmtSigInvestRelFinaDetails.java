/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaDetails
 * @类描述: lmt_sig_invest_rel_fina_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-11 16:43:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_rel_fina_details")
public class LmtSigInvestRelFinaDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 财务科目号 **/
	@Column(name = "FNC_SUBJECT", unique = false, nullable = true, length = 200)
	private String fncSubject;
	
	/** 序号 **/
	@Column(name = "ORDER_ID", unique = false, nullable = true, length = 5)
	private String orderId;
	
	/** N-2科目值 **/
	@Column(name = "N2_SUBJECT_VALUE", unique = false, nullable = true, length = 200)
	private String n2SubjectValue;
	
	/** N-2较上期涨幅 **/
	@Column(name = "N2_PRE_RISE", unique = false, nullable = true, length = 200)
	private String n2PreRise;
	
	/** N-1科目值 **/
	@Column(name = "N1_SUBJECT_VALUE", unique = false, nullable = true, length = 200)
	private String n1SubjectValue;
	
	/** N-1较上期涨幅 **/
	@Column(name = "N1_PRE_RISE", unique = false, nullable = true, length = 200)
	private String n1PreRise;
	
	/** 当前科目值 **/
	@Column(name = "CURT_SUBJECT_VALUE", unique = false, nullable = true, length = 10)
	private String curtSubjectValue;
	
	/** 报表类型 **/
	@Column(name = "FNC_TYPE", unique = false, nullable = true, length = 5)
	private String fncType;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param fncSubject
	 */
	public void setFncSubject(String fncSubject) {
		this.fncSubject = fncSubject;
	}
	
    /**
     * @return fncSubject
     */
	public String getFncSubject() {
		return this.fncSubject;
	}
	
	/**
	 * @param orderId
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
    /**
     * @return orderId
     */
	public String getOrderId() {
		return this.orderId;
	}
	
	/**
	 * @param n2SubjectValue
	 */
	public void setN2SubjectValue(String n2SubjectValue) {
		this.n2SubjectValue = n2SubjectValue;
	}
	
    /**
     * @return n2SubjectValue
     */
	public String getN2SubjectValue() {
		return this.n2SubjectValue;
	}
	
	/**
	 * @param n2PreRise
	 */
	public void setN2PreRise(String n2PreRise) {
		this.n2PreRise = n2PreRise;
	}
	
    /**
     * @return n2PreRise
     */
	public String getN2PreRise() {
		return this.n2PreRise;
	}
	
	/**
	 * @param n1SubjectValue
	 */
	public void setN1SubjectValue(String n1SubjectValue) {
		this.n1SubjectValue = n1SubjectValue;
	}
	
    /**
     * @return n1SubjectValue
     */
	public String getN1SubjectValue() {
		return this.n1SubjectValue;
	}
	
	/**
	 * @param n1PreRise
	 */
	public void setN1PreRise(String n1PreRise) {
		this.n1PreRise = n1PreRise;
	}
	
    /**
     * @return n1PreRise
     */
	public String getN1PreRise() {
		return this.n1PreRise;
	}
	
	/**
	 * @param curtSubjectValue
	 */
	public void setCurtSubjectValue(String curtSubjectValue) {
		this.curtSubjectValue = curtSubjectValue;
	}
	
    /**
     * @return curtSubjectValue
     */
	public String getCurtSubjectValue() {
		return this.curtSubjectValue;
	}
	
	/**
	 * @param fncType
	 */
	public void setFncType(String fncType) {
		this.fncType = fncType;
	}
	
    /**
     * @return fncType
     */
	public String getFncType() {
		return this.fncType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}