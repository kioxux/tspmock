/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppSub
 * @类描述: lmt_intbank_app_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 16:09:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_intbank_app_sub")
public class LmtIntbankAppSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
	private String subSerno;
	
	/** 授信品种编号 **/
	@Column(name = "LMT_BIZ_TYPE", unique = false, nullable = true, length = 20)
	private String lmtBizType;
	
	/** 授信品种名称 **/
	@Column(name = "LMT_BIZ_TYPE_NAME", unique = false, nullable = true, length = 80)
	private String lmtBizTypeName;
	
	/** 变更标志 **/
	@Column(name = "CHG_FLAG", unique = false, nullable = true, length = 5)
	private String chgFlag;
	
	/** 原额度台账分项编号 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_NO", unique = false, nullable = true, length = 40)
	private String origiLmtAccSubNo;
	
	/** 原额度台账分项授信金额 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLmtAccSubAmt;
	
	/** 原额度台账分项期限 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtAccSubTerm;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 是否涉及货币基金 **/
	@Column(name = "IS_IVL_MF", unique = false, nullable = true, length = 5)
	private String isIvlMf;
	
	/** 货币基金总授信额度 **/
	@Column(name = "LMT_MF_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtMfAmt;
	
	/** 单只货币基金授信额度 **/
	@Column(name = "LMT_SINGLE_MF_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtSingleMfAmt;
	
	/** 期限 **/
	@Column(name = "TERM", unique = false, nullable = true, length = 10)
	private Integer term;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}
	
    /**
     * @return lmtBizType
     */
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName;
	}
	
    /**
     * @return lmtBizTypeName
     */
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param chgFlag
	 */
	public void setChgFlag(String chgFlag) {
		this.chgFlag = chgFlag;
	}
	
    /**
     * @return chgFlag
     */
	public String getChgFlag() {
		return this.chgFlag;
	}
	
	/**
	 * @param origiLmtAccSubNo
	 */
	public void setOrigiLmtAccSubNo(String origiLmtAccSubNo) {
		this.origiLmtAccSubNo = origiLmtAccSubNo;
	}
	
    /**
     * @return origiLmtAccSubNo
     */
	public String getOrigiLmtAccSubNo() {
		return this.origiLmtAccSubNo;
	}
	
	/**
	 * @param origiLmtAccSubAmt
	 */
	public void setOrigiLmtAccSubAmt(java.math.BigDecimal origiLmtAccSubAmt) {
		this.origiLmtAccSubAmt = origiLmtAccSubAmt;
	}
	
    /**
     * @return origiLmtAccSubAmt
     */
	public java.math.BigDecimal getOrigiLmtAccSubAmt() {
		return this.origiLmtAccSubAmt;
	}
	
	/**
	 * @param origiLmtAccSubTerm
	 */
	public void setOrigiLmtAccSubTerm(Integer origiLmtAccSubTerm) {
		this.origiLmtAccSubTerm = origiLmtAccSubTerm;
	}
	
    /**
     * @return origiLmtAccSubTerm
     */
	public Integer getOrigiLmtAccSubTerm() {
		return this.origiLmtAccSubTerm;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf;
	}
	
    /**
     * @return isIvlMf
     */
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtMfAmt
	 */
	public void setLmtMfAmt(java.math.BigDecimal lmtMfAmt) {
		this.lmtMfAmt = lmtMfAmt;
	}
	
    /**
     * @return lmtMfAmt
     */
	public java.math.BigDecimal getLmtMfAmt() {
		return this.lmtMfAmt;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(java.math.BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return lmtSingleMfAmt
     */
	public java.math.BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}