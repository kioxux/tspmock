package cn.com.yusys.yusp.web.server.xdzc0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0025.req.Xdzc0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.resp.Xdzc0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0025.Xdzc0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池业务开关检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0025:资产池业务开关检查")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0025Resource {
	private static final Logger logger = LoggerFactory.getLogger(BizXdzc0025Resource.class);

	@Autowired
	private Xdzc0025Service xdzc0025Service;
	/**
	 * 交易码：xdzc0025
	 * 交易描述：资产池业务开关检查
	 *
	 * @param xdzc0025DataReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("资产池业务开关检查")
	@PostMapping("/xdzc0025")
	protected @ResponseBody
	ResultDto<Xdzc0025DataRespDto> xdzc0025(@Validated @RequestBody Xdzc0025DataReqDto xdzc0025DataReqDto) throws Exception {
		logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025DataReqDto));
		Xdzc0025DataRespDto xdzc0025DataRespDto = new Xdzc0025DataRespDto();//
		ResultDto<Xdzc0025DataRespDto> xdzc0025DataResultDto = new ResultDto<>();
		try {
			xdzc0025DataRespDto = xdzc0025Service.xdzc0025Service(xdzc0025DataReqDto);
			xdzc0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
			xdzc0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
		} catch (Exception e) {
			logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, e.getMessage());
			// 封装xdzc0025DataResultDto中异常返回码和返回信息
			xdzc0025DataResultDto.setCode(EpbEnum.EPB099999.key);
			xdzc0025DataResultDto.setMessage(e.getMessage());
		}
		// 封装xdzc0025DataRespDto到xdzc0025DataResultDto中
		xdzc0025DataResultDto.setData(xdzc0025DataRespDto);
		logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025DataResultDto));
		return xdzc0025DataResultDto;
	}
}
