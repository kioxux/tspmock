/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrDiscPorderSub;
import cn.com.yusys.yusp.service.CtrDiscPorderSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrDiscPorderSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-13 09:08:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "贴现协议汇票明细")
@RequestMapping("/api/ctrdiscpordersub")
public class CtrDiscPorderSubResource {
    @Autowired
    private CtrDiscPorderSubService ctrDiscPorderSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrDiscPorderSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrDiscPorderSub> list = ctrDiscPorderSubService.selectAll(queryModel);
        return new ResultDto<List<CtrDiscPorderSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrDiscPorderSub>> index(QueryModel queryModel) {
        List<CtrDiscPorderSub> list = ctrDiscPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<CtrDiscPorderSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrDiscPorderSub> show(@PathVariable("pkId") String pkId) {
        CtrDiscPorderSub ctrDiscPorderSub = ctrDiscPorderSubService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrDiscPorderSub>(ctrDiscPorderSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrDiscPorderSub> create(@RequestBody CtrDiscPorderSub ctrDiscPorderSub) throws URISyntaxException {
        ctrDiscPorderSubService.insert(ctrDiscPorderSub);
        return new ResultDto<CtrDiscPorderSub>(ctrDiscPorderSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrDiscPorderSub ctrDiscPorderSub) throws URISyntaxException {
        int result = ctrDiscPorderSubService.update(ctrDiscPorderSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrDiscPorderSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrDiscPorderSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

     /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CtrDiscPorderSub>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CtrDiscPorderSub> list = ctrDiscPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<CtrDiscPorderSub>>(list);
    }
}
