package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0061Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0061Resource
 * @类描述: 贷款期限校验
 * @功能描述: 展期期限日期校验
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0061展期期限日期校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0061")
public class RiskItem0061Resource {

    @Autowired
    private RiskItem0061Service riskItem0061Service;
    /**
     * @方法名称: riskItem0061
     * @方法描述: 展期期限日期校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: macm
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "贷款期限校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0061(@RequestBody QueryModel queryModel) {
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0061Service.riskItem0061(queryModel));
    }
}
