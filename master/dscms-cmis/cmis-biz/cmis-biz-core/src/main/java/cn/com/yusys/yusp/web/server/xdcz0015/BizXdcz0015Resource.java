package cn.com.yusys.yusp.web.server.xdcz0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0015.req.Xdcz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.resp.Xdcz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0015.Xdcz0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:银票影像补录同步
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0015:银票影像补录同步")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0015Resource.class);

    @Autowired
    private Xdcz0015Service xdcz0015Service;
    /**
     * 交易码：xdcz0015
     * 交易描述：银票影像补录同步
     *
     * @param xdcz0015DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("银票影像补录同步")
    @PostMapping("/xdcz0015")
    protected @ResponseBody
    ResultDto<Xdcz0015DataRespDto>  xdcz0015(@Validated @RequestBody Xdcz0015DataReqDto xdcz0015DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataReqDto));
        Xdcz0015DataRespDto  xdcz0015DataRespDto  = new Xdcz0015DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0015DataRespDto>xdcz0015DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0015DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdcz0015DataRespDto = xdcz0015Service.xdcz0015(xdcz0015DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0015DataRespDto对象开始
            // TODO 封装xdcz0015DataRespDto对象结束
            // 封装xdcz0015DataResultDto中正确的返回码和返回信息
            xdcz0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value,e.getMessage());
            // 封装xdcz0015DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0015DataRespDto到xdcz0015DataResultDto中
        xdcz0015DataResultDto.setData(xdcz0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataRespDto));
        return xdcz0015DataResultDto;
    }
}
