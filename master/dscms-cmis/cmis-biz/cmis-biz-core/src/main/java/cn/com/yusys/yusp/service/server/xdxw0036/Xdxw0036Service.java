package cn.com.yusys.yusp.service.server.xdxw0036;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSurveyConInfo;
import cn.com.yusys.yusp.dto.server.xdxw0036.req.Xdxw0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0036.resp.Xdxw0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyConInfoMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0036Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0036Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0036Service.class);

    @Autowired
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;

    /**
     * 查询优抵贷调查结论
     *
     * @param Xdxw0036DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0036DataRespDto xdxw0036(Xdxw0036DataReqDto Xdxw0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value);
        Xdxw0036DataRespDto Xdxw0036DataRespDto = new Xdxw0036DataRespDto();
        String SurveySerno = Xdxw0036DataReqDto.getIndgtSerno();//客户调查表编号
        try {
            //根据客户调查表编号查询调查表信息
            logger.info("***********XDXW0036***根据客户调查表编号查询调查表信息开始Start;客户调查表编号:" + SurveySerno);
            LmtSurveyConInfo lmtSurveyConInfo = Optional.ofNullable(lmtSurveyConInfoMapper.selectByPrimaryKey(SurveySerno)).orElse(new LmtSurveyConInfo());
            logger.info("***********XDXW0036***根据客户调查表编号查询调查表信息结束END;客户调查表信息:" + lmtSurveyConInfo);
            Xdxw0036DataRespDto.setIndgtSerno(SurveySerno);
            Xdxw0036DataRespDto.setIsRealOperPer(lmtSurveyConInfo.getIsCusRealOperator());//客户是否实际经营人。
            Xdxw0036DataRespDto.setIsOperNormal(lmtSurveyConInfo.getIsCusOperNormal());//客户经营是否正常
            Xdxw0036DataRespDto.setLoanAmt(lmtSurveyConInfo.getAdviceAmt());//贷款金额
            Xdxw0036DataRespDto.setLoanRate(lmtSurveyConInfo.getAdviceRate());//贷款利率
            //贷款期限
            String loonTerm = lmtSurveyConInfo.getLoanTerm();
            if (StringUtil.isEmpty(loonTerm)) {//空值默认为0
                loonTerm = CmisBizConstants.NUM_ZERO;
            }
            BigDecimal loonTermBd = new BigDecimal(loonTerm);
            Xdxw0036DataRespDto.setLoanTerm(loonTermBd);//贷款期限
            //担保方式
            String guarType = lmtSurveyConInfo.getGuarMode();
            Xdxw0036DataRespDto.setAssureMeans(guarType);
            String guarTypeName = StringUtils.EMPTY;
            if (StringUtil.isNotEmpty(guarType)) {
                guarTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY", guarType);
            }
            Xdxw0036DataRespDto.setAssureMeansName(guarTypeName);//担保方式名称
            Xdxw0036DataRespDto.setRepayType(lmtSurveyConInfo.getRepayMode());//还款方式（调查结论）
            Xdxw0036DataRespDto.setIsAdmit(StringUtils.EMPTY);//是否准入
            Xdxw0036DataRespDto.setCusCha(lmtSurveyConInfo.getCusCha());//客户性质
            Xdxw0036DataRespDto.setIsOldPld(lmtSurveyConInfo.getIsOldColl());//是否原抵押物
            Xdxw0036DataRespDto.setTurnovAmt(lmtSurveyConInfo.getTurnovLmt());//周转额度
            Xdxw0036DataRespDto.setNewAmt(lmtSurveyConInfo.getNewAddLmt());//新增额度
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value);
        return Xdxw0036DataRespDto;
    }

}
