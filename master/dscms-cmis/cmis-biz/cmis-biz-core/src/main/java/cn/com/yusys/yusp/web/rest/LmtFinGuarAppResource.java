/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtFinGuarApp;
import cn.com.yusys.yusp.domain.LmtFinGuarList;
import cn.com.yusys.yusp.domain.LmtThrShrsAppRel;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.LmtFinGuarAppService;
import cn.com.yusys.yusp.service.LmtFinGuarListService;
import cn.com.yusys.yusp.service.LmtThrShrsAppRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtfinguarapp")
public class LmtFinGuarAppResource {
    @Autowired
    private LmtFinGuarAppService lmtFinGuarAppService;
    @Autowired
    private LmtFinGuarListService lmtFinGuarListService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private LmtThrShrsAppRelService lmtThrShrsAppRelService;

    private static final Logger log = LoggerFactory.getLogger(LmtFinGuarAppResource.class);

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtFinGuarApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtFinGuarApp> list = lmtFinGuarAppService.selectAll(queryModel);
        return new ResultDto<List<LmtFinGuarApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtFinGuarApp>> index(QueryModel queryModel) {
        List<LmtFinGuarApp> list = lmtFinGuarAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtFinGuarApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtFinGuarApp> show(@PathVariable("serno") String serno) {
        LmtFinGuarApp lmtFinGuarApp = lmtFinGuarAppService.selectByPrimaryKey(serno);
        return new ResultDto<LmtFinGuarApp>(lmtFinGuarApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtFinGuarApp> create(@RequestBody LmtFinGuarApp lmtFinGuarApp) throws URISyntaxException {
        lmtFinGuarAppService.insert(lmtFinGuarApp);
        return new ResultDto<LmtFinGuarApp>(lmtFinGuarApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtFinGuarApp lmtFinGuarApp) throws URISyntaxException {
        int result = lmtFinGuarAppService.update(lmtFinGuarApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtFinGuarAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtFinGuarAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: selectCountByOldSerno
     * @函数描述: 根据原业务流水号查询复议申请
     * @参数: serno 否决申请记录的业务流水号--复议申请原业务流水号
     * */
    @PostMapping("/selectcount/{serno}")
    protected ResultDto<LmtFinGuarApp> selectCountByOldSerno (@PathVariable("serno") String serno) {
        log.info("担保公司额度申请流水号serno："+serno+"复议记录查询！");
        LmtFinGuarApp result = lmtFinGuarAppService.selectCountByOldSerno(serno);
        return new ResultDto<LmtFinGuarApp>(result);
    }

    /**
     * @函数名称: selectCountByOldSerno
     * @函数描述: 根据原业务流水号查询复议申请
     * @参数: serno 否决申请记录的业务流水号--复议申请原业务流水号
     * */
    @PostMapping("/saveReconsData")
    protected ResultDto<Integer> saveReconsData (@RequestBody LmtFinGuarApp lmtFinGuarApp) {
        log.info("担保公司额度申请流水号serno："+lmtFinGuarApp.getOldSerno()+"复议申请数据保存！");
        int result = -1;
        //拼接序列号
        HashMap<String,String> param = new HashMap<>();
        //全局序列号业务类型 10-PC端
        param.put("biz", SeqConstant.YPSEQ_BIZ_10);
        //根据原业务流水号查询到原否决申请关联的担保公司明细信息
        List<LmtFinGuarList> lmtFinGuarLists = lmtFinGuarListService.selectBySerno(lmtFinGuarApp.getOldSerno());
        for(LmtFinGuarList lmtFinGuarList : lmtFinGuarLists){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",lmtFinGuarList.getPkId());
            lmtFinGuarList.setSerno(lmtFinGuarApp.getSerno());
            lmtFinGuarList.setPkId(StringUtils.uuid(true));
            List<LmtThrShrsAppRel> lmtThrShrsAppRelList = lmtThrShrsAppRelService.selectAll(queryModel);
            for(LmtThrShrsAppRel lmtThrShrsAppRel : lmtThrShrsAppRelList){
                lmtThrShrsAppRel.setSerno(lmtFinGuarList.getPkId());
                lmtThrShrsAppRel.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ,param));
                //插入新的共享范围
                lmtThrShrsAppRelService.insert(lmtThrShrsAppRel);
            }
            //插入新的申请明细
            result = lmtFinGuarListService.insert(lmtFinGuarList);
        }
        return new ResultDto<Integer>(result);
    }
}
