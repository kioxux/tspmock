/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutApp
 * @类描述: iqp_certi_inout_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-20 15:04:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_certi_inout_app")
public class IqpCertiInoutApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 担保合同流水号  **/
	@Column(name = "GUAR_PK_ID", unique = false, nullable = true, length = 40)
	private String guarPkId;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 借款人编号 **/
	@Column(name = "BORROWER_ID", unique = false, nullable = true, length = 30)
	private String borrowerId;
	
	/** 担保合同类型 STD_ZB_GRT_TYP **/
	@Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String guarContType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 担保起始日 **/
	@Column(name = "GUAR_START_DATE", unique = false, nullable = true, length = 10)
	private String guarStartDate;
	
	/** 担保终止日 **/
	@Column(name = "GUAR_END_DATE", unique = false, nullable = true, length = 10)
	private String guarEndDate;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	@Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
	private String guarWay;
	
	/** 押品包号 **/
	@Column(name = "PACKAGE_NO", unique = false, nullable = true, length = 40)
	private String packageNo;
	
	/** 待入库权证数量 **/
	@Column(name = "WAIT_IN_CERTI_QNT", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal waitInCertiQnt;
	
	/** 待出库权证数量 **/
	@Column(name = "WAIT_OUT_CERTI_QNT", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal waitOutCertiQnt;
	
	/** 取出还贷类型 STD_ZB_TOTLN_TYP **/
	@Column(name = "EXTR_LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String extrLoanType;
	
	/** 理由 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 权证出入库类型 STD_ZB_EXWA_TYP **/
	@Column(name = "INOUT_APPTYPE", unique = false, nullable = true, length = 5)
	private String inoutApptype;
	
	/** 权证状态 **/
	@Column(name = "CERTI_STATU", unique = false, nullable = true, length = 5)
	private String certiStatu;
	
	/** 是否记账 STD_ZB_YES_NO **/
	@Column(name = "IS_RECORD", unique = false, nullable = true, length = 5)
	private String isRecord;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 保管机构 **/
	@Column(name = "KEEP_BR_ID", unique = false, nullable = true, length = 20)
	private String keepBrId;
	
	/** 权证临时借用人名称 **/
	@Column(name = "TEMP_BORROWER_NAME", unique = false, nullable = true, length = 100)
	private String tempBorrowerName;
	
	/** 权证临时出库日期 **/
	@Column(name = "TEMP_OUT_DATE", unique = false, nullable = true, length = 10)
	private String tempOutDate;
	
	/** 权证预计归还时间 **/
	@Column(name = "PRE_BACK_DATE", unique = false, nullable = true, length = 10)
	private String preBackDate;
	
	/** 权证实际归还日期 **/
	@Column(name = "REAL_BACK_DATE", unique = false, nullable = true, length = 10)
	private String realBackDate;
	
	/** 权证临时出库原因 **/
	@Column(name = "TEMP_OUT_REASON", unique = false, nullable = true, length = 10)
	private String tempOutReason;
	
	/** 账务机构 **/
	@Column(name = "fina_br_id", unique = false, nullable = true, length = 20)
	private String finaBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId;
	}
	
    /**
     * @return guarPkId
     */
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param borrowerId
	 */
	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId;
	}
	
    /**
     * @return borrowerId
     */
	public String getBorrowerId() {
		return this.borrowerId;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}
	
    /**
     * @return guarContType
     */
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param guarStartDate
	 */
	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate;
	}
	
    /**
     * @return guarStartDate
     */
	public String getGuarStartDate() {
		return this.guarStartDate;
	}
	
	/**
	 * @param guarEndDate
	 */
	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate;
	}
	
    /**
     * @return guarEndDate
     */
	public String getGuarEndDate() {
		return this.guarEndDate;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}
	
    /**
     * @return guarWay
     */
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param packageNo
	 */
	public void setPackageNo(String packageNo) {
		this.packageNo = packageNo;
	}
	
    /**
     * @return packageNo
     */
	public String getPackageNo() {
		return this.packageNo;
	}
	
	/**
	 * @param waitInCertiQnt
	 */
	public void setWaitInCertiQnt(java.math.BigDecimal waitInCertiQnt) {
		this.waitInCertiQnt = waitInCertiQnt;
	}
	
    /**
     * @return waitInCertiQnt
     */
	public java.math.BigDecimal getWaitInCertiQnt() {
		return this.waitInCertiQnt;
	}
	
	/**
	 * @param waitOutCertiQnt
	 */
	public void setWaitOutCertiQnt(java.math.BigDecimal waitOutCertiQnt) {
		this.waitOutCertiQnt = waitOutCertiQnt;
	}
	
    /**
     * @return waitOutCertiQnt
     */
	public java.math.BigDecimal getWaitOutCertiQnt() {
		return this.waitOutCertiQnt;
	}
	
	/**
	 * @param extrLoanType
	 */
	public void setExtrLoanType(String extrLoanType) {
		this.extrLoanType = extrLoanType;
	}
	
    /**
     * @return extrLoanType
     */
	public String getExtrLoanType() {
		return this.extrLoanType;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inoutApptype
	 */
	public void setInoutApptype(String inoutApptype) {
		this.inoutApptype = inoutApptype;
	}
	
    /**
     * @return inoutApptype
     */
	public String getInoutApptype() {
		return this.inoutApptype;
	}
	
	/**
	 * @param certiStatu
	 */
	public void setCertiStatu(String certiStatu) {
		this.certiStatu = certiStatu;
	}
	
    /**
     * @return certiStatu
     */
	public String getCertiStatu() {
		return this.certiStatu;
	}
	
	/**
	 * @param isRecord
	 */
	public void setIsRecord(String isRecord) {
		this.isRecord = isRecord;
	}
	
    /**
     * @return isRecord
     */
	public String getIsRecord() {
		return this.isRecord;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param keepBrId
	 */
	public void setKeepBrId(String keepBrId) {
		this.keepBrId = keepBrId;
	}
	
    /**
     * @return keepBrId
     */
	public String getKeepBrId() {
		return this.keepBrId;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName;
	}
	
    /**
     * @return tempBorrowerName
     */
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate;
	}
	
    /**
     * @return tempOutDate
     */
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate;
	}
	
    /**
     * @return preBackDate
     */
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate;
	}
	
    /**
     * @return realBackDate
     */
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param tempOutReason
	 */
	public void setTempOutReason(String tempOutReason) {
		this.tempOutReason = tempOutReason;
	}
	
    /**
     * @return tempOutReason
     */
	public String getTempOutReason() {
		return this.tempOutReason;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/** 权证主键 **/
	@Column(name = "CERTI_PK_ID", unique = false, nullable = false, length = 40)
	private String certiPkId;
	/**
	 * @param certiPkId
	 */
	public void setCertiPkId(String certiPkId) {
		this.certiPkId = certiPkId;
	}
	
	/**
	 * @return certiPkId
	 */
	public String getCertiPkId() {
		return this.certiPkId;
	}
	
	/** 权证入库日期 **/
	@Column(name = "IN_DATE", unique = false, nullable = false, length = 40)
	private String inDate;
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}
	
	/**
	 * @return inDate
	 */
	public String getInDate() {
		return this.inDate;
	}
	
	/** 权证正常出库日期 **/
	@Column(name = "OUT_DATE", unique = false, nullable = false, length = 40)
	private String outDate;
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	
	/**
	 * @return outDate
	 */
	public String getOutDate() {
		return this.outDate;
	}

}