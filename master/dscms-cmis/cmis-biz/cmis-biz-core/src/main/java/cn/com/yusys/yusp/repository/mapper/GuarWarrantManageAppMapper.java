/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantManageAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:24:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarWarrantManageAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    GuarWarrantManageApp selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<GuarWarrantManageApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(GuarWarrantManageApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(GuarWarrantManageApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(GuarWarrantManageApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(GuarWarrantManageApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     *
     * @param guarNo
     * @return
     */
    int countOnTheWayRecordsByGuarNo(@Param("guarNo") String guarNo);

    /**
     * 查询核心担保编号在途的权证出库申请记录数
     * @param coreGuarantyNo
     * @return
     */
    int countOnTheWayRecordsByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据权证编号查询对应的权证入库的担保合同编号
     * @param warrantNo
     * @return
     */
    String selectGuarContNoByWarrantNo(@Param("warrantNo") String warrantNo);

    /**
     * 根据权利凭证号查询在途的权证入库流水号
     * @param certiRecordId
     * @return
     */
    String selectOnTheWaySernoByCertiRecordId(@Param("certiRecordId") String certiRecordId);

    /**
     * 根据核心担保编号查询权证入库担保编号
     * @param coreGuarantyNo
     * @return
     */
    String selectGuarContNoByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据担保编号查询权证出库核心担保编号
     * @param guarContNo
     * @return
     */
    String selectCoreGuarantyNoByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据核心担保编号查询权证入库流水号
     * @param coreGuarantyNo
     * @return
     */
    String selectSernoByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据核心担保编号查询权证出库信息
     * @param coreGuarantyNo
     * @return
     */
    GuarWarrantManageApp selectWarrantOutInfoByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);
}