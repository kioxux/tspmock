/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDisAssetIncomeDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDisAssetIncome;
import cn.com.yusys.yusp.repository.mapper.IqpDisAssetIncomeMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncomeService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpDisAssetIncomeService {

    private static final Logger log = LoggerFactory.getLogger(IqpDisAssetIncomeService.class);

    @Autowired
    private IqpDisAssetIncomeMapper iqpDisAssetIncomeMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpDisAssetIncome selectByPrimaryKey(String incomePk) {
        return iqpDisAssetIncomeMapper.selectByPrimaryKey(incomePk);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpDisAssetIncome> selectAll(QueryModel model) {
        List<IqpDisAssetIncome> records = (List<IqpDisAssetIncome>) iqpDisAssetIncomeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpDisAssetIncome> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpDisAssetIncome record) {
        int result = 0;
        String discountAssetType = record.getDiscountAssetType();
        if("1".equals(discountAssetType)){ //1工资收入
            QueryModel model = new QueryModel();
            model.addCondition("iqpSerno",record.getIqpSerno());
            model.addCondition("discountAssetType",discountAssetType);
            List<IqpDisAssetIncome> IqpDisAssetIncomeList = iqpDisAssetIncomeMapper.selectByModel(model);
            if (IqpDisAssetIncomeList != null && IqpDisAssetIncomeList.size()>0) {
                result = iqpDisAssetIncomeMapper.updateByPrimaryKey(record);
            } else {
                record.setIncomePk(UUID.randomUUID().toString());//暂时通过UUID生成序列号
                result = iqpDisAssetIncomeMapper.insert(record);
            }
        }else{ ///1工资收入2 经营收入 5 其他收入
            String incomePk = record.getIncomePk();
            if (incomePk!=null && !"".equals(incomePk)) {
                result = iqpDisAssetIncomeMapper.updateByPrimaryKey(record);
            } else {
                String serno = UUID.randomUUID().toString();//暂时通过UUID生成序列号
                record.setIncomePk(serno);
                result = iqpDisAssetIncomeMapper.insert(record);
            }
        }
        return result;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpDisAssetIncome record) {
        return iqpDisAssetIncomeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpDisAssetIncome update(IqpDisAssetIncome record) {
        IqpDisAssetIncome count = null;
        String incomePk = record.getIncomePk();
        log.info("评级测算" + record.getIqpSerno() + "测算后修改");
        int counts = iqpDisAssetIncomeMapper.updateByPrimaryKey(record);
        if (counts < 0) {
            throw new YuspException(EcbEnum.CTR_EXCEPTION_DEF.key, EcbEnum.CTR_EXCEPTION_DEF.value);
        } else {
            count = iqpDisAssetIncomeMapper.selectByPrimaryKey(incomePk);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpDisAssetIncome record) {
        return iqpDisAssetIncomeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String incomePk) {
        return iqpDisAssetIncomeMapper.deleteByPrimaryKey(incomePk);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDisAssetIncomeMapper.deleteByIds(ids);
    }

    public List<IqpDisAssetIncome> selectByIqpSerno(String iqpSernoOld) {
        return iqpDisAssetIncomeMapper.selectByIqpSerno(iqpSernoOld);
    }

    /**
     * 根据流水号查询月收入汇总
     *
     * @param iqpSerno
     * @return
     */
    public String getSumIncome(String iqpSerno) {
        return iqpDisAssetIncomeMapper.getSumIncome(iqpSerno);
    }

    /**
     * @author zlf
     * @date 2021/5/5 11:08
     * @version 1.0.0
     * @desc 根据业务流水号查单条
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpDisAssetIncome selectIqpSerno(String iqpSerno) {
        return iqpDisAssetIncomeMapper.selectIqpSerno(iqpSerno);
    }

    /**
     * @author zlf
     * @date 2021/5/14 14:42
     * @version 1.0.0
     * @desc 查询经营性收入 3
     * @修改历史 修改时间 修改人员 修改原因
     */
    public List<IqpDisAssetIncome> selectList(String iqpSerno) {
        List<IqpDisAssetIncome> lists = iqpDisAssetIncomeMapper.selectMore(iqpSerno);
        return lists;
    }

    /**
     * @author zlf
     * @date 2021/5/14 14:40
     * @version 1.0.0
     * @desc 查询经营性收入 3
     * @修改历史 修改时间 修改人员 修改原因
     */
    public IqpDisAssetIncomeDto selectMore(String iqpSerno) {
        List<IqpDisAssetIncome> lists = iqpDisAssetIncomeMapper.selectMore(iqpSerno);
        List<IqpDisAssetIncome> list1 = new ArrayList();
        List<IqpDisAssetIncome> list2 = new ArrayList();
        IqpDisAssetIncomeDto iqpDisAssetIncomeDto = new IqpDisAssetIncomeDto();
       lists.stream().forEach(s->{
           if ("2".equals(s.getDiscountAssetType())){//个体经营者经营收入
               list1.add(s);
           }else if ("5".equals(s.getDiscountAssetType())){//其他收入
               list2.add(s);
           }else if("1".equals(s.getDiscountAssetType())){//工资收入
               BeanUtils.copyProperties(s, iqpDisAssetIncomeDto);

           }
       });
        iqpDisAssetIncomeDto.setList1(list1);
        iqpDisAssetIncomeDto.setList2(list2);
        return iqpDisAssetIncomeDto;
}

    /**
     * @author zlf
     * @date 2021/5/14 14:40
     * @version 1.0.0
     * @desc 查询经营性收入 3
     * @修改历史 修改时间 修改人员 修改原因
     */
    public IqpDisAssetIncomeDto selectMoreOther(String iqpSerno) {
        List<IqpDisAssetIncome> lists = iqpDisAssetIncomeMapper.selectMoreOther(iqpSerno);
        IqpDisAssetIncomeDto iqpDisAssetIncomeDto = new IqpDisAssetIncomeDto();
        if (lists.size() > 0) {
            BeanUtils.copyProperties(lists.get(0), iqpDisAssetIncomeDto);
            iqpDisAssetIncomeDto.setList1(lists);
        }

        return iqpDisAssetIncomeDto;
    }

    /**
     * @author zlf
     * @date 2021/5/14 14:42
     * @version 1.0.0
     * @desc 查询其他收入 2
     * @修改历史 修改时间 修改人员 修改原因
     */
    public List<IqpDisAssetIncome> selectListOther(String iqpSerno) {
        List<IqpDisAssetIncome> lists = iqpDisAssetIncomeMapper.selectMoreOther(iqpSerno);
        return lists;
    }



    /**
     * @author shenli
     * @date 2021-6-24 20:28:12
     * @version 1.0.0
     * @desc 根据流水号查询经营与其他收入总计
     * @修改历史 修改时间 修改人员 修改原因
     */
    public BigDecimal selectSubtotalSumByAssetType(String iqpSerno) {
        return iqpDisAssetIncomeMapper.selectSubtotalSumByAssetType(iqpSerno);
    }
}
