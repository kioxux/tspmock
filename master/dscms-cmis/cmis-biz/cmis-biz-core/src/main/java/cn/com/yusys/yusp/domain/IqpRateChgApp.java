/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRateChgApp
 * @类描述: iqp_rate_chg_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 11:19:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_rate_chg_app")
public class IqpRateChgApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 贷款到期日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;
	
	/** 原利率调整方式 **/
	@Column(name = "OLD_RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
	private String oldRateAdjMode;
	
	/** 原LPR授信利率区间 **/
	@Column(name = "OLD_LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String oldLprRateIntval;
	
	/** 原LPR利率 **/
	@Column(name = "OLD_CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldCurtLprRate;
	
	/** 原浮动点数 **/
	@Column(name = "OLD_RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldRateFloatPoint;
	
	/** 原执行利率(年) **/
	@Column(name = "OLD_EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldExecRateYear;
	
	/** 原逾期利率浮动比 **/
	@Column(name = "OLD_OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldOverdueRatePefloat;
	
	/** 原逾期执行利率(年利率) **/
	@Column(name = "OLD_OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldOverdueExecRate;
	
	/** 原复息利率浮动比 **/
	@Column(name = "OLD_CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldCiRatePefloat;
	
	/** 原复息执行利率(年利率) **/
	@Column(name = "OLD_CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldCiExecRate;
	
	/** 原利率调整选项 **/
	@Column(name = "OLD_RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
	private String oldRateAdjType;
	
	/** 原下一次利率调整间隔 **/
	@Column(name = "OLD_NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private String oldNextRateAdjInterval;
	
	/** 原下一次利率调整间隔单位 **/
	@Column(name = "OLD_NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 10)
	private String oldNextRateAdjUnit;
	
	/** 原第一次调整日 **/
	@Column(name = "OLD_FIRST_ADJ_DATE", unique = false, nullable = true, length = 5)
	private String oldFirstAdjDate;
	
	/** 利率调整方式 **/
	@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
	private String rateAdjMode;
	
	/** LPR授信利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;
	
	/** 执行利率(年) **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 逾期利率浮动比 **/
	@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRatePefloat;
	
	/** 逾期执行利率(年利率) **/
	@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueExecRate;
	
	/** 复息利率浮动比 **/
	@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciRatePefloat;
	
	/** 复息执行利率(年利率) **/
	@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciExecRate;
	
	/** 利率调整选项 **/
	@Column(name = "RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
	private String rateAdjType;
	
	/** 下一次利率调整间隔 **/
	@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private String nextRateAdjInterval;
	
	/** 下一次利率调整间隔单位 **/
	@Column(name = "NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 10)
	private String nextRateAdjUnit;
	
	/** 调整后利率生效时间 **/
	@Column(name = "NEW_INURE_DATE", unique = false, nullable = true, length = 10)
	private String newInureDate;
	
	/** 第一次调整日 **/
	@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 5)
	private String firstAdjDate;
	
	/** 申请原因 **/
	@Column(name = "APP_RESN", unique = false, nullable = true, length = 500)
	private String appResn;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 是否需要签订协议  **/
	@Column(name = "IS_SIGN_AGR", unique = false, nullable = true, length = 5)
	private String isSignAgr;
	
	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;
	
	/** 协议编号 **/
	@Column(name = "ARG_NO", unique = false, nullable = true, length = 40)
	private String argNo;
	
	/** 合同状态    **/
	@Column(name = "CONT_STATUS", unique = false, nullable = true, length = 5)
	private String contStatus;
	
	/** 合同审批状态  **/
	@Column(name = "CONT_APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String contApproveStatus;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 80)
	private String appChnl;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 40)
	private String belgLine;

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param oldRateAdjMode
	 */
	public void setOldRateAdjMode(String oldRateAdjMode) {
		this.oldRateAdjMode = oldRateAdjMode;
	}
	
    /**
     * @return oldRateAdjMode
     */
	public String getOldRateAdjMode() {
		return this.oldRateAdjMode;
	}
	
	/**
	 * @param oldLprRateIntval
	 */
	public void setOldLprRateIntval(String oldLprRateIntval) {
		this.oldLprRateIntval = oldLprRateIntval;
	}
	
    /**
     * @return oldLprRateIntval
     */
	public String getOldLprRateIntval() {
		return this.oldLprRateIntval;
	}
	
	/**
	 * @param oldCurtLprRate
	 */
	public void setOldCurtLprRate(java.math.BigDecimal oldCurtLprRate) {
		this.oldCurtLprRate = oldCurtLprRate;
	}
	
    /**
     * @return oldCurtLprRate
     */
	public java.math.BigDecimal getOldCurtLprRate() {
		return this.oldCurtLprRate;
	}
	
	/**
	 * @param oldRateFloatPoint
	 */
	public void setOldRateFloatPoint(java.math.BigDecimal oldRateFloatPoint) {
		this.oldRateFloatPoint = oldRateFloatPoint;
	}
	
    /**
     * @return oldRateFloatPoint
     */
	public java.math.BigDecimal getOldRateFloatPoint() {
		return this.oldRateFloatPoint;
	}
	
	/**
	 * @param oldExecRateYear
	 */
	public void setOldExecRateYear(java.math.BigDecimal oldExecRateYear) {
		this.oldExecRateYear = oldExecRateYear;
	}
	
    /**
     * @return oldExecRateYear
     */
	public java.math.BigDecimal getOldExecRateYear() {
		return this.oldExecRateYear;
	}
	
	/**
	 * @param oldOverdueRatePefloat
	 */
	public void setOldOverdueRatePefloat(java.math.BigDecimal oldOverdueRatePefloat) {
		this.oldOverdueRatePefloat = oldOverdueRatePefloat;
	}
	
    /**
     * @return oldOverdueRatePefloat
     */
	public java.math.BigDecimal getOldOverdueRatePefloat() {
		return this.oldOverdueRatePefloat;
	}
	
	/**
	 * @param oldOverdueExecRate
	 */
	public void setOldOverdueExecRate(java.math.BigDecimal oldOverdueExecRate) {
		this.oldOverdueExecRate = oldOverdueExecRate;
	}
	
    /**
     * @return oldOverdueExecRate
     */
	public java.math.BigDecimal getOldOverdueExecRate() {
		return this.oldOverdueExecRate;
	}
	
	/**
	 * @param oldCiRatePefloat
	 */
	public void setOldCiRatePefloat(java.math.BigDecimal oldCiRatePefloat) {
		this.oldCiRatePefloat = oldCiRatePefloat;
	}
	
    /**
     * @return oldCiRatePefloat
     */
	public java.math.BigDecimal getOldCiRatePefloat() {
		return this.oldCiRatePefloat;
	}
	
	/**
	 * @param oldCiExecRate
	 */
	public void setOldCiExecRate(java.math.BigDecimal oldCiExecRate) {
		this.oldCiExecRate = oldCiExecRate;
	}
	
    /**
     * @return oldCiExecRate
     */
	public java.math.BigDecimal getOldCiExecRate() {
		return this.oldCiExecRate;
	}
	
	/**
	 * @param oldRateAdjType
	 */
	public void setOldRateAdjType(String oldRateAdjType) {
		this.oldRateAdjType = oldRateAdjType;
	}
	
    /**
     * @return oldRateAdjType
     */
	public String getOldRateAdjType() {
		return this.oldRateAdjType;
	}
	
	/**
	 * @param oldNextRateAdjInterval
	 */
	public void setOldNextRateAdjInterval(String oldNextRateAdjInterval) {
		this.oldNextRateAdjInterval = oldNextRateAdjInterval;
	}
	
    /**
     * @return oldNextRateAdjInterval
     */
	public String getOldNextRateAdjInterval() {
		return this.oldNextRateAdjInterval;
	}
	
	/**
	 * @param oldNextRateAdjUnit
	 */
	public void setOldNextRateAdjUnit(String oldNextRateAdjUnit) {
		this.oldNextRateAdjUnit = oldNextRateAdjUnit;
	}
	
    /**
     * @return oldNextRateAdjUnit
     */
	public String getOldNextRateAdjUnit() {
		return this.oldNextRateAdjUnit;
	}
	
	/**
	 * @param oldFirstAdjDate
	 */
	public void setOldFirstAdjDate(String oldFirstAdjDate) {
		this.oldFirstAdjDate = oldFirstAdjDate;
	}
	
    /**
     * @return oldFirstAdjDate
     */
	public String getOldFirstAdjDate() {
		return this.oldFirstAdjDate;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}
	
    /**
     * @return rateAdjMode
     */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return curtLprRate
     */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}
	
    /**
     * @return overdueRatePefloat
     */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}
	
	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}
	
    /**
     * @return overdueExecRate
     */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}
	
	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}
	
    /**
     * @return ciRatePefloat
     */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}
	
	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}
	
    /**
     * @return ciExecRate
     */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}
	
    /**
     * @return rateAdjType
     */
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}
	
    /**
     * @return nextRateAdjInterval
     */
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit;
	}
	
    /**
     * @return nextRateAdjUnit
     */
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}
	
	/**
	 * @param newInureDate
	 */
	public void setNewInureDate(String newInureDate) {
		this.newInureDate = newInureDate;
	}
	
    /**
     * @return newInureDate
     */
	public String getNewInureDate() {
		return this.newInureDate;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}
	
    /**
     * @return firstAdjDate
     */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}
	
    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param isSignAgr
	 */
	public void setIsSignAgr(String isSignAgr) {
		this.isSignAgr = isSignAgr;
	}
	
    /**
     * @return isSignAgr
     */
	public String getIsSignAgr() {
		return this.isSignAgr;
	}
	
	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}
	
    /**
     * @return paperContSignDate
     */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}
	
	/**
	 * @param argNo
	 */
	public void setArgNo(String argNo) {
		this.argNo = argNo;
	}
	
    /**
     * @return argNo
     */
	public String getArgNo() {
		return this.argNo;
	}
	
	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}
	
    /**
     * @return contStatus
     */
	public String getContStatus() {
		return this.contStatus;
	}
	
	/**
	 * @param contApproveStatus
	 */
	public void setContApproveStatus(String contApproveStatus) {
		this.contApproveStatus = contApproveStatus;
	}
	
    /**
     * @return contApproveStatus
     */
	public String getContApproveStatus() {
		return this.contApproveStatus;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}