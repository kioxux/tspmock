package cn.com.yusys.yusp.web.server.xdxw0032;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0032.req.Xdxw0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0032.resp.Xdxw0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0032.Xdxw0032Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询信贷系统的审批历史信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0032:查询信贷系统的审批历史信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0032Resource.class);

    @Autowired
    private Xdxw0032Service xdxw0032Service;

    /**
     * 交易码：xdxw0032
     * 交易描述：查询信贷系统的审批历史信息
     *
     * @param xdxw0032DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷系统的审批历史信息")
    @PostMapping("/xdxw0032")
    protected @ResponseBody
    ResultDto<Xdxw0032DataRespDto> xdxw0032(@Validated @RequestBody Xdxw0032DataReqDto xdxw0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataReqDto));
        Xdxw0032DataRespDto xdxw0032DataRespDto = new Xdxw0032DataRespDto();// 响应Dto:查询信贷系统的审批历史信息
        ResultDto<Xdxw0032DataRespDto> xdxw0032DataResultDto = new ResultDto<>();
        // 业务编号
        String pkValue = xdxw0032DataReqDto.getPkValue();
        try {
            if (StringUtils.isEmpty(pkValue)) {
                xdxw0032DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0032DataResultDto.setMessage(EpbEnum.EPB099999.value);
                return xdxw0032DataResultDto;
            }
            xdxw0032DataRespDto = xdxw0032Service.xdxw0032(xdxw0032DataReqDto);
            xdxw0032DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0032DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, e.getMessage());
            // 封装xdxw0032DataResultDto中异常返回码和返回信息
            xdxw0032DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0032DataResultDto.setMessage(EpbEnum.EPB099999.key);
        }
        // 封装xdxw0032DataRespDto到xdxw0032DataResultDto中
        xdxw0032DataResultDto.setData(xdxw0032DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataResultDto));
        return xdxw0032DataResultDto;
    }
}
