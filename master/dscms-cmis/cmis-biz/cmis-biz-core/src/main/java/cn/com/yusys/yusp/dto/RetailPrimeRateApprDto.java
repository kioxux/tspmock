package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateAppr
 * @类描述: retail_prime_rate_appr数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:20:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RetailPrimeRateApprDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 流水号 **/
	private String serno;
	
	/** 报价利率 **/
	private java.math.BigDecimal offerRate;
	
	/** 申请执行利率 **/
	private java.math.BigDecimal appRate;
	
	/** 批复利率 **/
	private java.math.BigDecimal replyRate;
	
	/** 审批结论 **/
	private String approveConclusion;
	
	/** 审批意见 **/
	private String approveAdvice;
	
	/** 审核岗位 **/
	private String approvePost;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param offerRate
	 */
	public void setOfferRate(java.math.BigDecimal offerRate) {
		this.offerRate = offerRate;
	}
	
    /**
     * @return OfferRate
     */	
	public java.math.BigDecimal getOfferRate() {
		return this.offerRate;
	}
	
	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}
	
    /**
     * @return AppRate
     */	
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}
	
	/**
	 * @param replyRate
	 */
	public void setReplyRate(java.math.BigDecimal replyRate) {
		this.replyRate = replyRate;
	}
	
    /**
     * @return ReplyRate
     */	
	public java.math.BigDecimal getReplyRate() {
		return this.replyRate;
	}
	
	/**
	 * @param approveConclusion
	 */
	public void setApproveConclusion(String approveConclusion) {
		this.approveConclusion = approveConclusion == null ? null : approveConclusion.trim();
	}
	
    /**
     * @return ApproveConclusion
     */	
	public String getApproveConclusion() {
		return this.approveConclusion;
	}
	
	/**
	 * @param approveAdvice
	 */
	public void setApproveAdvice(String approveAdvice) {
		this.approveAdvice = approveAdvice == null ? null : approveAdvice.trim();
	}
	
    /**
     * @return ApproveAdvice
     */	
	public String getApproveAdvice() {
		return this.approveAdvice;
	}
	
	/**
	 * @param approvePost
	 */
	public void setApprovePost(String approvePost) {
		this.approvePost = approvePost == null ? null : approvePost.trim();
	}
	
    /**
     * @return ApprovePost
     */	
	public String getApprovePost() {
		return this.approvePost;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}