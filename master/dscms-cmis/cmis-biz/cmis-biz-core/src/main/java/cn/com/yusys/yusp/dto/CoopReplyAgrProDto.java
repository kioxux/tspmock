package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAgrPro
 * @类描述: coop_reply_agr_pro数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-26 17:10:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopReplyAgrProDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	private String pkId;
	/** 批复编号 **/
	private String replyNo;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 项目类型 **/
	private String proType;
	
	/** 本次签订金额 **/
	private java.math.BigDecimal signAmt;
	
	/** 项目额度（元） **/
	private java.math.BigDecimal proLmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}
	
    /**
     * @return ReplyNo
     */	
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType == null ? null : proType.trim();
	}
	
    /**
     * @return ProType
     */	
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param signAmt
	 */
	public void setSignAmt(java.math.BigDecimal signAmt) {
		this.signAmt = signAmt;
	}
	
    /**
     * @return SignAmt
     */	
	public java.math.BigDecimal getSignAmt() {
		return this.signAmt;
	}
	
	/**
	 * @param proLmt
	 */
	public void setProLmt(java.math.BigDecimal proLmt) {
		this.proLmt = proLmt;
	}
	
    /**
     * @return ProLmt
     */	
	public java.math.BigDecimal getProLmt() {
		return this.proLmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}