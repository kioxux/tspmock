package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.Message;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSurveyConInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.LmtSurveyConInfoDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.PldList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyConInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw06.Fbxw06Service;
import cn.com.yusys.yusp.service.client.cus.cmiscus0007.CmisCus0007Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyConInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:39:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSurveyConInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LmtSurveyConInfoService.class);

    @Autowired
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private Fbxw06Service fbxw06Service;

    @Autowired
    private CmisCus0007Service cmisCus0007Service;//业务逻辑处理类：查询个人客户基本信息

    @Autowired
    private LmtGuareInfoMapper lmtGuareInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSurveyConInfo selectByPrimaryKey(String surveySerno) {
        return lmtSurveyConInfoMapper.selectByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSurveyConInfo> selectAll(QueryModel model) {
        List<LmtSurveyConInfo> records = (List<LmtSurveyConInfo>) lmtSurveyConInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyConInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyConInfo> list = lmtSurveyConInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSurveyConInfo record) {
        return lmtSurveyConInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSurveyConInfo record) {
        return lmtSurveyConInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSurveyConInfo record) {
        return lmtSurveyConInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSurveyConInfo record) {
        return lmtSurveyConInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveySerno) {
        return lmtSurveyConInfoMapper.deleteByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSurveyConInfoMapper.deleteByIds(ids);
    }

    /**
     * @return 新增 修改  调查结论信息逻辑 6.8不用了
     * @创建人 wzy
     * @创建时间 15:57 2021-04-12
     **/
    @Transactional(rollbackFor = Exception.class)
    public int saveSurveyConInfo(LmtSurveyConInfo lmtSurveyConInfo) {
        //基本信息有无创建
        int result;
        if (this.selectByPrimaryKey(lmtSurveyConInfo.getSurveySerno()) == null) {
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            lmtSurveyConInfo.setSurveySerno(serno);
            //无基本信息
            result = this.insertSelective(lmtSurveyConInfo);
        } else {
            result = this.updateSelective(lmtSurveyConInfo);
        }
        //企业信息有无创建
        return result;
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-03 10:11
     * @注释 增享贷 优惠利率测算
     */
    //利率信息测算
    public ResultDto calculate(LmtSurveyReportBasicInfo basic) {
        //  String surveySerno = lmtSurveyConInfo.getSurveySerno();
        Fbxw06ReqDto fbxw06ReqDto = new Fbxw06ReqDto();
        //查询调查主表信息
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(basic.getSurveySerno());
        //证件号码
        fbxw06ReqDto.setCert_code(lmtSurveyReportMainInfo.getCertCode());
        //证件类型
        fbxw06ReqDto.setCert_type(DicTranEnum.lookup("CERT_TYPE_XDTORIRCP_" + lmtSurveyReportMainInfo.getCertType()));
        //产品名称
        fbxw06ReqDto.setPrd_name(lmtSurveyReportMainInfo.getPrdName());
        //客户姓名
        fbxw06ReqDto.setCust_name(lmtSurveyReportMainInfo.getCusName());
        //客户ID
//        fbxw06ReqDto.setCust_id_core(lmtSurveyReportMainInfo.getCusId());
        fbxw06ReqDto.setCust_id_core("");
        //手机号码
        fbxw06ReqDto.setPhone(basic.getPhone());
        //贷款投向
        fbxw06ReqDto.setLoan_investment("");
        //担保方式
        fbxw06ReqDto.setGuarantee_method(basic.getGuarMode());
        //客户类型:1个体工商户,2一般自然人,3小微企业主
        //通过客户证件号查询客户信息
        String certNo = lmtSurveyReportMainInfo.getCertCode();
        LOGGER.info("****************根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
        CusIndivDto cusIndiv = queryCusIndiv(certNo);
        LOGGER.info("****************根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusIndiv));
        if(StringUtil.isNotEmpty(cusIndiv.getCusType())){
            fbxw06ReqDto.setCus_type(cusIndiv.getCusType());
        }else{
            fbxw06ReqDto.setCus_type("1");
        }
        //抵押/质押类型:1 一抵,2 二抵(如果担保方式为抵押/质押 必填)
        String guarMode = basic.getGuarMode();
        String pawnType = "";//抵押类型
        if ("10".equals(guarMode) || "20".equals(guarMode)) {
            List<PldList> pldList = lmtGuareInfoMapper.getPldListByindgtSerno(basic.getSurveySerno());
            if(CollectionUtils.nonNull(pldList)){
                pawnType =pldList.get(0).getPawnType();
                if("01".equals(pawnType)){
                    pawnType = "1";
                }else if("02".equals(pawnType)){
                    pawnType = "2";
                }
            }
            fbxw06ReqDto.setCollateral_pledge_type(pawnType);
        } else {
            fbxw06ReqDto.setCollateral_pledge_type("");
        }
        //调查流水号
        fbxw06ReqDto.setSurvey_serno(basic.getSurveySerno());
        //调用接口，开始测算
        Fbxw06RespDto fbxw06RespDto = fbxw06Service.fbxw06(fbxw06ReqDto);
        return new ResultDto(fbxw06RespDto);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-03 10:08
     * @注释 保存调查结论信息
     */
    public int saveOrUpdateCon(LmtSurveyConInfo lmtSurveyConInfo) {
        LmtSurveyConInfo conInfo = this.selectByPrimaryKey(lmtSurveyConInfo.getSurveySerno());
        //如果为空 新增
        int i = 0;
        if (conInfo == null) {
            i = this.insertSelective(lmtSurveyConInfo);
            //修改主表金额
            BigDecimal adviceAmt = lmtSurveyConInfo.getAdviceAmt();
            if (!Objects.isNull(adviceAmt)) {
                LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyConInfo.getSurveySerno());
                mainInfo.setAppAmt(adviceAmt);
                lmtSurveyReportMainInfoService.updateSelective(mainInfo);
            }
        } else {
            i = this.updateSelective(lmtSurveyConInfo);
            //修改主表金额
            BigDecimal adviceAmt = lmtSurveyConInfo.getAdviceAmt();
            if (!Objects.isNull(adviceAmt)) {
                LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyConInfo.getSurveySerno());
                mainInfo.setAppAmt(adviceAmt);
                lmtSurveyReportMainInfoService.updateSelective(mainInfo);
            }
        }
        return i;
    }

    /**
     * @创建人 李帅
     * @创建时间 2021-08-04 14:33
     * @注释 保存调查结论信息 录入调查报告基本信息表中‘IS_TQSD’等字段
     */
    public int saveOrUpdateConInfo(LmtSurveyConInfoDto lmtSurveyConInfoDto) {
        LmtSurveyConInfo conInfo = this.selectByPrimaryKey(lmtSurveyConInfoDto.getSurveySerno());
        LmtSurveyConInfo lmtSurveyConInfo = BeanUtils.beanCopy(lmtSurveyConInfoDto, LmtSurveyConInfo.class);
        //如果为空 新增
        int i = 0;
        if (conInfo == null) {
            i = this.insertSelective(lmtSurveyConInfo);
            //修改主表金额
            BigDecimal adviceAmt = lmtSurveyConInfoDto.getAdviceAmt();
            if (!Objects.isNull(adviceAmt)) {
                LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyConInfoDto.getSurveySerno());
                mainInfo.setAppAmt(adviceAmt);
                lmtSurveyReportMainInfoService.updateSelective(mainInfo);
            }
            // 修改基本信息表中'IS_TQSD'等字段
            this.updateLmtSurveyReportBasicInfo(lmtSurveyConInfoDto);
        } else {
            i = this.updateSelective(lmtSurveyConInfo);
            //修改主表金额
            BigDecimal adviceAmt = lmtSurveyConInfoDto.getAdviceAmt();
            if (!Objects.isNull(adviceAmt)) {
                LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyConInfoDto.getSurveySerno());
                mainInfo.setAppAmt(adviceAmt);
                lmtSurveyReportMainInfoService.updateSelective(mainInfo);
            }
            // 修改基本信息表中'IS_TQSD'等字段
            this.updateLmtSurveyReportBasicInfo(lmtSurveyConInfoDto);
        }
        return i;
    }

    /**
     * @创建人 李帅
     * @创建时间 2021-08-04 14:33
     * @注释 录入调查报告基本信息表中‘IS_TQSD’等字段
     */
    private int updateLmtSurveyReportBasicInfo(LmtSurveyConInfoDto lmtSurveyConInfoDto) {
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtSurveyConInfoDto.getSurveySerno());
        lmtSurveyReportBasicInfo.setIsTqsd(lmtSurveyConInfoDto.getIsTqsd());
        lmtSurveyReportBasicInfo.setAppLoanWay(lmtSurveyConInfoDto.getAppLoanWay());
        lmtSurveyReportBasicInfo.setIsCwhb(lmtSurveyConInfoDto.getIsCwhb());
        lmtSurveyReportBasicInfo.setWhbModelAmt(lmtSurveyConInfoDto.getWhbModelAmt());
        lmtSurveyReportBasicInfo.setWhbModelRate(lmtSurveyConInfoDto.getWhbModelRate());
        return lmtSurveyReportBasicInfoService.updateSelective(lmtSurveyReportBasicInfo);
    }

    /**
     * @param lmtSurveyConInfo
     * @return cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto
     * @author hubp
     * @date 2021/5/19 20:25
     * @version 1.0.0
     * @desc 调查结论参考利率测算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Fbxw06RespDto getRefRate(LmtSurveyConInfo lmtSurveyConInfo) {
        LOGGER.info("调查结论参考利率测算开始..................");
        Fbxw06RespDto fbxw06RespDto = null;
        try {
            String surveySerno = lmtSurveyConInfo.getSurveySerno();
            Fbxw06ReqDto fbxw06ReqDto = new Fbxw06ReqDto();
            //查询调查主表信息
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveySerno);

            //查询调查基本表信息
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
            if (lmtSurveyReportBasicInfo == null) {
                //查到主表信息为空 这时候可能存在没有手机号码等相关字段 抛出异常返回
                throw new YuspException(new Message("1", "未查询到基本信息，请先填写基本信息", "1"));
            }
            //证件号码
            fbxw06ReqDto.setCert_code(lmtSurveyReportMainInfo.getCertCode());
            //证件类型                                                            CERT_TYPE_XDTORIRCP_A
            fbxw06ReqDto.setCert_type(DicTranEnum.lookup("CERT_TYPE_XDTORIRCP_" + lmtSurveyReportMainInfo.getCertType()));
            //产品名称
            fbxw06ReqDto.setPrd_name(lmtSurveyReportMainInfo.getPrdName());
            //客户姓名
            fbxw06ReqDto.setCust_name(lmtSurveyReportMainInfo.getCusName());
            //客户ID
//            fbxw06ReqDto.setCust_id_core(lmtSurveyReportMainInfo.getCusId());
            fbxw06ReqDto.setCust_id_core("");
            //手机号码
            fbxw06ReqDto.setPhone(lmtSurveyReportBasicInfo.getPhone());
            //贷款投向
            fbxw06ReqDto.setLoan_investment("");
            //担保方式
            fbxw06ReqDto.setGuarantee_method(lmtSurveyConInfo.getGuarMode());
            //客户类型:1个体工商户,2一般自然人,3小微企业主
            //通过客户证件号查询客户信息
            String certNo = lmtSurveyReportMainInfo.getCertCode();
            LOGGER.info("****************根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
            CusIndivDto cusIndiv = queryCusIndiv(certNo);
            LOGGER.info("****************根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusIndiv));
            if(StringUtil.isNotEmpty(cusIndiv.getCusType())){
                fbxw06ReqDto.setCus_type(cusIndiv.getCusType());
            }else{
                fbxw06ReqDto.setCus_type("1");
            }
            //抵押/质押类型:1 一抵,2 二抵(如果担保方式为抵押/质押 必填)
            String guarMode = lmtSurveyReportBasicInfo.getGuarMode();
            String pawnType = "";//抵押类型
            if ("10".equals(guarMode) || "20".equals(guarMode)) {
                List<PldList> pldList = lmtGuareInfoMapper.getPldListByindgtSerno(surveySerno);
                if(CollectionUtils.nonNull(pldList)){
                    pawnType =pldList.get(0).getPawnType();
                    if("01".equals(pawnType)){
                        pawnType = "1";
                    }else if("02".equals(pawnType)){
                        pawnType = "2";
                    }
                }
                fbxw06ReqDto.setCollateral_pledge_type(pawnType);
            } else {
                fbxw06ReqDto.setCollateral_pledge_type("");
            }
            //调查流水号
            fbxw06ReqDto.setSurvey_serno(lmtSurveyConInfo.getSurveySerno());
            //调用接口，开始测算
            fbxw06RespDto = fbxw06Service.fbxw06(fbxw06ReqDto);
        } catch (YuspException e) {
            LOGGER.error("调查结论参考利率测算失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        } finally {
            LOGGER.info("调查结论参考利率测算结束..................");
        }
        return fbxw06RespDto;
    }

    /**
     * @创建人 李帅
     * @创建时间 2021/8/4 14:54
     * @注释 单条数据查询
     */
    public LmtSurveyConInfoDto selectConInfoBySurveySerno(String surveySerno) {
        return lmtSurveyConInfoMapper.selectConInfoBySurveySerno(surveySerno);
    }


    /**
     * @创建人 李帅
     * @创建时间 2021-08-05 13:33
     * @注释 查询客户名下上笔业务余额，调查建议期限，调查还款方式
     */
    public LmtSurveyConInfoDto getCusLastContInfo(String cusId) {
        return lmtSurveyConInfoMapper.getCusLastContInfo(cusId);
    }

    /********
     *根据证件号查询客户信息
     * *********/
    public CusIndivDto queryCusIndiv(String certNo) {
        CusIndivDto cusIndiv = new CusIndivDto();
        if (StringUtil.isNotEmpty(certNo)) {//证件号不能为空
            CmisCus0007ReqDto cmisCus0007ReqDto = new CmisCus0007ReqDto();
            cmisCus0007ReqDto.setCertCode(certNo);
            LOGGER.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
            CmisCus0007RespDto cmisCus0007RespDto = cmisCus0007Service.cmisCus0007(cmisCus0007ReqDto);
            LOGGER.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
            List<CusIndivDto> cusIndivList = cmisCus0007RespDto.getCusIndivList();
            cusIndiv = new CusIndivDto();
            List<CusIndivDto> collect = cusIndivList.stream().filter(cusIndivDto -> Objects.equals(certNo, cusIndivDto.getCertCode())).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(collect)) {
                cusIndiv = collect.get(0);
            }
        }
        return cusIndiv;
    }
}
