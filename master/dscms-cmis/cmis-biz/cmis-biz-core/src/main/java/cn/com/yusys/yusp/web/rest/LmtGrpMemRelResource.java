/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtGrpMemDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.LmtGrpMemRelService;
import cn.com.yusys.yusp.service.LmtGrpReplyAccService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.namespace.QName;
import java.net.URISyntaxException;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpMemRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-08 21:01:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api( tags = "集团授信关联成员客户信息")
@RestController
@RequestMapping("/api/lmtgrpmemrel")
public class LmtGrpMemRelResource {
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;


    /**
     * 集团客户流动资金额度试算
     *@创建人: ywl
     * @return
     */
    @PostMapping("/getGrpHighCurfund")
    protected ResultDto<List<LmtGrpMemRel>> getGrpHighCurfund(@RequestBody QueryModel queryModel) {
        String grpSerno = (String) queryModel.getCondition().get("grpSerno");
        List<LmtGrpMemRel> lmtGrpMemRel = lmtGrpMemRelService.getGrpHighCurfund(grpSerno);
        return new ResultDto<List<LmtGrpMemRel>>(lmtGrpMemRel);
    }

    /**
     * 集团客户限额及债项评级列表查询
     *@创建人: ywl
     * @return
     */
    @PostMapping("/getgrpevellist")
    protected ResultDto<List<Map>> getGrpEvelList(@RequestBody QueryModel queryModel) {
        List<Map> lmtGrpMemRel = lmtGrpMemRelService.getGrpEvelList((String) queryModel.getCondition().get("grpSerno"));
        return new ResultDto<List<Map>>(lmtGrpMemRel);
    }

    /**
     * 集团客户评级及债项评级
     *@创建人: ywl
     * @return
     */
    @PostMapping("/getGrpLadEvalList")
    protected ResultDto<List<LmtGrpMemRel>> getGrpLadEvalList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        String grpCusId = (String) queryModel.getCondition().get("grpCusId");
        List<LmtGrpMemRel> grpLadEvalList = lmtGrpMemRelService.getGrpLadEvalList(grpCusId);
        PageHelper.clearPage();
        return new ResultDto<List<LmtGrpMemRel>>(grpLadEvalList);
    }
    /**
     * @函数名称: queryLmtGrpMemRelByGrpSerno
     * @函数描述: 根据集团流水号查询集团授信关联成员列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号查询集团授信关联成员列表")
    @PostMapping("/querylmtgrpmemrelbygrpserno")
    protected ResultDto<List<LmtGrpMemRel>> queryLmtGrpMemRelByGrpSerno(@RequestBody String grpSerno) {
        List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: update
     * @函数描述: 更新授信额度是否可调剂
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号查询集团授信关联成员列表")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpMemRel lmtGrpMemRel) {
        int res = lmtGrpMemRelService.updateSelective(lmtGrpMemRel);
        return new ResultDto<Integer>(res);
    }

    /**
     * @函数名称: queryLmtGrpMemRelByGrpSernoAndMgr
     * @函数描述: 根据集团流水号与成员管护人号查询集团授信关联成员列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号与成员管护人号查询集团授信关联成员列表")
    @PostMapping("/querylmtgrpmemrelbygrpsernoandmgr")
    protected ResultDto<List<LmtGrpMemRel>> queryLmtGrpMemRelByGrpSernoAndMgr(@RequestBody String grpSerno) {
        List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSernoAndMgr(grpSerno);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: initLmtGrpMemRelStatusByGrpSerno
     * @函数描述: 根据集团流水号与成员管护人号查询集团授信关联成员列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号初始化集团授信关联成员列表的状态")
    @PostMapping("/initlmtgrpmemrelstatusbygrpserno")
    protected ResultDto<Integer> initLmtGrpMemRelStatusByGrpSerno(@RequestBody String grpSerno) {
        return ResultDto.success(lmtGrpMemRelService.initLmtGrpMemRelStatusByGrpSerno(grpSerno));
    }

    /**
     * @函数名称: updateIsPrtcptCurtDeclareByPkId
     * @函数描述: 根据主键更新是否参与申报的状态
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据主键更新是否参与申报的状态")
    @PostMapping("/updateisprtcptcurtdeclarebypkid")
    protected ResultDto<Integer> updateIsPrtcptCurtDeclareByPkId(@RequestBody String pkId, @RequestBody String isPrtcptCurtDeclare) {
        return ResultDto.success(lmtGrpMemRelService.updateIsPrtcptCurtDeclareByPkId(pkId, isPrtcptCurtDeclare));
    }

    /**
     * @函数名称: finishLmtGrpMemRelStatusByGrpSernoAndMgr
     * @函数描述: 根据集团流水号和成员管护人员号更新提交标志为已提交
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号和成员管护人员号更新提交标志为已提交")
    @PostMapping("/finishlmtgrpmemrelstatusbygrpsernoandmgr")
    protected ResultDto<Integer> finishLmtGrpMemRelStatusByGrpSernoAndMgr(@RequestBody String grpSerno) {
        return ResultDto.success(lmtGrpMemRelService.finishLmtGrpMemRelStatusByGrpSernoAndMgr(grpSerno));
    }

    /**
     * @函数名称: judgeIsAllManagerFinish
     * @函数描述: 判断是否所有客户经理已完成填报
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团授信申请流水号判断是否所有客户经理已完成填报")
    @PostMapping("/judgeisallmanagerfinish")
    protected ResultDto<String> judgeIsAllManagerFinish(@RequestBody String grpSerno) {
        return ResultDto.success(lmtGrpMemRelService.judgeIsAllManagerFinish(grpSerno));
    }

    /**
     * @函数名称: returnBackLmtGrpAppSingle
     * @函数描述: 退回客户经理集团授信申请填报
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("退回客户经理集团授信申请填报")
    @PostMapping("/returnbacklmtgrpappsingle")
    protected ResultDto<Integer> returnBackLmtGrpAppSingle(@RequestBody String pkId) {
        return ResultDto.success(lmtGrpMemRelService.returnBackLmtGrpAppSingle(pkId));
    }

    /**
     * @函数名称: judgeIsAllManagerFinish
     * @函数描述: 判断是否所有客户经理已完成填报
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团授信申请流水号判断是否已完成初始化成员客户经理填报状态")
    @PostMapping("/judgeisafterinitlmtgrpmemrelstatus")
    protected ResultDto<String> judgeIsAfterInitLmtGrpMemRelStatus(@RequestBody String grpSerno) {
        return ResultDto.success(lmtGrpMemRelService.judgeIsAfterInitLmtGrpMemRelStatus(grpSerno));
    }

    /**
     * @函数名称: selectByGrpSerNo
     * @函数描述: 根据集团授信申请流水号查询成员批复情况
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-24 14:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbygrpserno")
    protected ResultDto<List<LmtGrpMemRel>> selectByGrpSerno(@RequestBody Map map) {
        String grpSerno = (String) map.get("grpSerno");
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        return new ResultDto<>(lmtGrpMemRels);
    }

    /**
     * 根据集团申请流水号查询集团授信额度情况
     *@创建人: ywl
     * @return
     */
    @PostMapping("/querylmtgrpreplybygrpserno")
    protected ResultDto<List<LmtGrpMemRel>> selectLmtGrpReplyByGrpSerno(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        String grpSerno = (String) queryModel.getCondition().get("grpSerno");
        List<LmtGrpMemRel> grpLadEvalList = lmtGrpMemRelService.selectLmtGrpReplyByGrpSerno(grpSerno);
        PageHelper.clearPage();
        return new ResultDto<List<LmtGrpMemRel>>(grpLadEvalList);
    }

    /**
     * 查询集团上期授信
     * @param model
     * @return
     */
    @PostMapping("/selectLastLmt")
    protected ResultDto<List<LmtReplySubPrd>> selectLastLmt(@RequestBody QueryModel model){
        return new ResultDto<List<LmtReplySubPrd>>(lmtGrpMemRelService.selectLastLmt(model));
    }

    /**
     * 查询集团本期授信
     * @param model
     * @return
     */
    @PostMapping("/selectThisLmt")
    protected ResultDto<List<Map>> selectThisLmt(@RequestBody QueryModel model){
        return new ResultDto<List<Map>>(lmtGrpMemRelService.selectThisLmt(model));
    }

    /**
     * @函数名称: selectByGrpReplySerno
     * @函数描述: 根据集团授信申请流水号查询去变更表里面查询批复编号,再根据批复编号查询授信申报流水号,根据流水号查询成员批复情况
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-28 20:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbygrpreplyserno")
    protected ResultDto<List<LmtGrpMemRel>> selectByGrpReplySerno(@RequestBody Map map) {
        String grpSerno = (String) map.get("grpSerno");
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.selectByGrpReplySerno(grpSerno);
        return new ResultDto<>(lmtGrpMemRels);
    }

    /**
     * @函数名称: selectLmtGrpAppIsInFlow
     * @函数描述: 校验当前授信申报是否已进入流程
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-10 20:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/isinflow")
    protected ResultDto<Integer> selectLmtGrpAppIsInFlow(@RequestBody Map map) {
        String serno = (String) map.get("serno");
        int result = lmtGrpMemRelService.selectLmtGrpAppIsInFlow(serno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称: querySingleSernoByGrpSerno
     * @函数描述: 查询当前集团名下的客户
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-18 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号查询当前集团名下的客户")
    @PostMapping("/querysinglesernobygrpserno")
    protected ResultDto<String> querySingleSernoByGrpSerno(@RequestBody String grpSerno) {
        String serno = lmtGrpMemRelService.querySingleSernoByGrpSerno(grpSerno);
        return new ResultDto<>(serno);
    }

    /**
     * @函数名称: queryLmtGrpMemRelByGrpSerno
     * @函数描述: 查询当前集团名下的客户
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-18 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团流水号查询当前集团名下的客户")
    @PostMapping("/querycusbygrpserno")
    protected ResultDto<String> queryCusByGrpSerno(@RequestBody String grpSerno) {
        String cusIds = lmtGrpMemRelService.queryCusByGrpSerno(grpSerno);
        return new ResultDto<>(cusIds);
    }

    /**
     * @函数名称: selectGrpLmtGuarRingDataByGrpSerno
     * @函数描述: 根据集团流水号查询集团授信关联成员列表
     */
    @ApiOperation("根据集团流水号查询集团授信关联成员列表")
    @PostMapping("/selectgrplmtguarringdatabygrpserno")
    protected ResultDto<List<Map>> selectGrpLmtGuarRingDataByGrpSerno(@RequestBody Map map) {
        List<Map> list = lmtGrpMemRelService.selectGrpLmtGuarRingDataByGrpSerno((String) map.get("grpSerno"));
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: selectSingleApprLoanCondDataByGrpApproveSerno
     * @函数描述: 根据集团审批流水号查询集团授信项下成员列表
     */
    @ApiOperation("根据集团审批流水号查询集团授信项下成员列表")
    @PostMapping("/selectsingleapprloanconddatabygrpapproveserno")
    protected ResultDto<List<Map>> selectSingleApprLoanCondDataByGrpApproveSerno(@RequestBody Map map) {
        List<Map> list = lmtGrpMemRelService.selectSingleApprLoanCondDataByGrpApproveSerno((String) map.get("grpApproveSerno"));
        return new ResultDto<>(list);
    }

    /**
     * @函数名称: queryLmtGrpMemRelBySingleSerno
     * @函数描述: 根据授信流水查集团流水，再根据集团流水查集团批复
     */
    @ApiOperation("根据授信流水查集团流水，再根据集团流水查集团批复")
    @PostMapping("/queryLmtGrpMemRelBySingleSerno")
    protected ResultDto<LmtGrpReplyAcc> queryLmtGrpMemRelBySingleSerno(@RequestBody String singleSerno) throws Exception {
        LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(singleSerno);
        if(lmtGrpMemRel == null){
            throw new Exception("未找到该授信流水对应的集团数据");
        }
        String grpSerno=lmtGrpMemRel.getGrpSerno();
        LmtGrpReplyAcc lmtGrpReplyAcc=lmtGrpReplyAccService.queryDetailByGrpSerno(grpSerno);
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAcc);
    }
}
