/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpApplApptRel;
import cn.com.yusys.yusp.repository.mapper.IqpApplApptRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:23:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpApplApptRelService {

    @Autowired
    private IqpApplApptRelMapper iqpApplApptRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpApplApptRel selectByPrimaryKey(String pkId) {
        return iqpApplApptRelMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpApplApptRel> selectAll(QueryModel model) {
        List<IqpApplApptRel> records = (List<IqpApplApptRel>) iqpApplApptRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpApplApptRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpApplApptRel> list = iqpApplApptRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpApplApptRel record) {
        return iqpApplApptRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpApplApptRel record) {
        return iqpApplApptRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpApplApptRel record) {
        return iqpApplApptRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpApplApptRel record) {
        return iqpApplApptRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpApplApptRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpApplApptRelMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpApplApptRelMapper.updateByParams(delMap);
    }

    public List<IqpApplApptRel> selectByIqpSerno(String iqpSernoOld) {
        return iqpApplApptRelMapper.selectByIqpSerno(iqpSernoOld);
    }

    /**
     * 根据申请人编号,查询申请人配偶编号
     * @param apptCode
     * @param iqpSerno
     * @return
     */
    public IqpApplApptRel findWifeApptInfo(String iqpSerno, String apptCode) {
        return iqpApplApptRelMapper.findWifeApptInfo(iqpSerno,apptCode);
    }

    /**
     * 通过入参集合查询数据信息
     * @param param
     * @return
     */
    public List<IqpApplApptRel> selectIqpApplApptRelByParam(Map param){
        return iqpApplApptRelMapper.selectByParam(param);
    }
}
