package cn.com.yusys.yusp.web.server.xdxw0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0026.req.Xdxw0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0026.resp.SchoolAreaList;
import cn.com.yusys.yusp.dto.server.xdxw0026.resp.Xdxw0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 接口处理类:学区信息列表查询（分页）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0026:学区信息列表查询（分页）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0026Resource.class);

    /**
     * 交易码：xdxw0026
     * 交易描述：学区信息列表查询（分页）
     *
     * @param xdxw0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("学区信息列表查询（分页）")
    @PostMapping("/xdxw0026")
    protected @ResponseBody
    ResultDto<Xdxw0026DataRespDto> xdxw0026(@Validated @RequestBody Xdxw0026DataReqDto xdxw0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026DataReqDto));
        Xdxw0026DataRespDto xdxw0026DataRespDto = new Xdxw0026DataRespDto();// 响应Dto:学区信息列表查询（分页）
        ResultDto<Xdxw0026DataRespDto> xdxw0026DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0026DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
			String admitArea = xdxw0026DataReqDto.getAdmitArea();//准入区域（省市区中文）
			String schoolName = xdxw0026DataReqDto.getSchoolName();//学校名称
			String admitName = xdxw0026DataReqDto.getAdmitName();//准入名称
			Integer pageNum = xdxw0026DataReqDto.getPageNum();//页码
			Integer pageSize = xdxw0026DataReqDto.getPageSize();//一页查询数量
            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0026DataRespDto对象开始
			List<SchoolAreaList> schoolAreaList = new ArrayList<>();
			xdxw0026DataRespDto.setSchoolAreaList(schoolAreaList);
            // TODO 封装xdxw0026DataRespDto对象结束
            // 封装xdxw0026DataResultDto中正确的返回码和返回信息
            xdxw0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, e.getMessage());
            // 封装xdxw0026DataResultDto中异常返回码和返回信息
            xdxw0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0026DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0026DataRespDto到xdxw0026DataResultDto中
        xdxw0026DataResultDto.setData(xdxw0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026DataResultDto));
        return xdxw0026DataResultDto;
    }
}
