package cn.com.yusys.yusp.web.server.xdtz0061;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0061.req.Xdtz0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.resp.Xdtz0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0061.Xdtz0061Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小微电子签约借据号生成
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0061:小微电子签约借据号生成")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0061Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0061Resource.class);
    @Autowired
    private Xdtz0061Service xdtz0061Service;

    /**
     * 交易码：xdtz0061
     * 交易描述：小微电子签约借据号生成
     *
     * @param xdtz0061DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微电子签约借据号生成")
    @PostMapping("/xdtz0061")
    protected @ResponseBody
    ResultDto<Xdtz0061DataRespDto> xdtz0061(@Validated @RequestBody Xdtz0061DataReqDto xdtz0061DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataReqDto));
        Xdtz0061DataRespDto xdtz0061DataRespDto = new Xdtz0061DataRespDto();// 响应Dto:更新信贷台账信息
        ResultDto<Xdtz0061DataRespDto> xdtz0061DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0061DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataReqDto));
            xdtz0061DataRespDto = xdtz0061Service.xdtz0061(xdtz0061DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataRespDto));
            // 封装xdtz0061DataResultDto中正确的返回码和返回信息
            if("F".equals(xdtz0061DataRespDto.getOpFlag())){
                xdtz0061DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0061DataResultDto.setMessage(xdtz0061DataRespDto.getOpMsg());
            }else{
                xdtz0061DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdtz0061DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, e.getMessage());
            // 封装xdtz0061DataResultDto中异常返回码和返回信息
            xdtz0061DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0061DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0061DataRespDto到xdtz0061DataResultDto中
        xdtz0061DataResultDto.setData(xdtz0061DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataResultDto));
        return xdtz0061DataResultDto;
    }
}
