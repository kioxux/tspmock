package cn.com.yusys.yusp.repository.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarInfoCommMapper
 * @类描述: #Dao类（押品信息公共功能）
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-12-04 16:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarInfoCommMapper {
    Map getGuarInfo(@Param("tableName") String tableName, @Param("guarNo") String guarNo);

    int saveGuarDetail (Map map);
}
