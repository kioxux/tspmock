/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanAppPojo;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.PvpCheckList;
import cn.com.yusys.yusp.dto.server.xdht0013.req.Xdht0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0013.resp.Xdht0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.PvpInfoList;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.Xdtz0054DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-04 09:33:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PvpLoanAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    PvpLoanApp selectByPrimaryKey(@Param("pvpSerno") String pvpSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpLoanApp> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(PvpLoanApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(PvpLoanApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(PvpLoanApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(PvpLoanApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pvpSerno") String pvpSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
     * 通过放款申请流水号更新放款申请表
     * @param pvpSerno
     * @param approveStatus
     * @return
     * @创建人 by zxz
     */
    int updateApproveStatus(@Param("pvpSerno") String pvpSerno, @Param("approveStatus") String approveStatus);

    /**
     * @方法名称: selectByPvpLoanSernoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人 by zxz
     */
    PvpLoanApp selectByPvpLoanSernoKey(String pvpSerno);


    /**
     * @param
     * @return int
     * @author qw
     * @date 2021/4/26 0026 21:52
     * @version 1.0.0
     * @desc 根据合同编号查询审批中，审批通过的放款申请笔数
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int selectPvpLoanAppCountByContNo(Map pvpQueryMap);

    /**
     * @方法名称: countPvpLoanAppCountByContNo
     * @方法描述: 根据普通合同编号查询出账申请数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    int countPvpLoanAppCountByContNo(Map pvpQueryMap);

    /**
     * @方法名称: countPvpLoanAppCountByCtrDiscContNo
     * @方法描述: 根据贴现协议合同编号查询出账申请数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    int countPvpLoanAppCountByCtrDiscContNo(Map pvpQueryMap);

    /**
     * @方法名称: queryLoanContBysureySerno
     * @方法描述: 查询是否存在住房按揭抵押
     * @参数与返回说明:
     * @算法描述: 无
     */
    int queryLoanContBysureySerno(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: queryPvpLoanContBysureySerno
     * @方法描述: 查询该决议下是否已经有放款
     * @参数与返回说明:
     * @算法描述: 无
     */
    int queryPvpLoanContBysureySerno(@Param("surveySerno") String surveySerno);

    /**
     * 根据借据编号查询合同信息
     * @param xdht0013DataReqDto
     * @return
     */
    Xdht0013DataRespDto getContInfoByBillNo(Xdht0013DataReqDto xdht0013DataReqDto);

    /**
     * @方法名称: selectDataByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: WH-适用于小微-零售
     * @算法描述: 无
     */
    List<PvpLoanApp> selectDataByModel(QueryModel model);

    /**
     * 商贷分户实时查询
     *
     * @param model
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0045.resp.List> querySd0007LoanInfo(QueryModel model);

    /**
     * @创建人 WH
     * @创建时间 2021/5/17 13:46
     * @注释 扩展查询 拼接额外参数
     */
    PvpLoanAppPojo selectByPvpSerno(@Param("pvpSerno") String pvpSerno);

    /**
     * 作废出帐信息
     * @param pvpSerno
     * @return
     */
    int updateApproveStatusByPvpSerno(String pvpSerno);

    /**
     * 查询贷款出账信息
     *
     * @param queryMap
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0012.resp.PvpList> getPvpLoanAppList(Map queryMap);

    /**
     * 根据借据编号查询查询历史信息
     * xdtz0054
     *
     * @param pvpQueryMap
     * @return
     */
    List<PvpInfoList> selectPvpInfoListByParam(Map pvpQueryMap);
    /** 渠道端查询我的贷款-个人（流程监控）
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0077.resp.List> getLoanInfo4Indiv(Map param);
    /**
     * 渠道端查询我的贷款-对公（流程监控）
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0077.resp.List> getLoanInfo4Cus(Map param);
    /**
     *  根据借据号更新审批状态
     * @param pvpLoanApp
     * @return
     */
    int updateApproveStatusByBillNo(PvpLoanApp pvpLoanApp);

    /**
     * xdtz0028
     *
     * @param pvpQueryMap
     * @return
     */
    List<PvpCheckList> selectPvpCheckListByParam(Map pvpQueryMap);

    /**
     * 根据合同号查询总条数
     * @param contNo
     * @return
     */
    int countSelectPvpLoanAPPByContno(@Param("contNo") String contNo);

    /**
     * 根据合同号查询放款金额
     * @param contNo
     * @return
     */
    String selectPvpLoanAppSumByContno(@Param("contNo") String contNo);


    /**
     * 根据借据编号，合同编号询PvpLoanApp信息
     * @param querymap
     * @return
     */
    String selectPvpLoanAppSernoSumByContno(Map querymap);

    /**
     * 查询担保合同关联合同的在途出账记录数
     * @param guarContNo
     * @return
     */
    int countOnTheWayPvpLoanAppRecords(@Param("guarContNo") String guarContNo);

    int updateAuthStatusByPvpSerno(PvpLoanApp pvpLoanApp);

    /**
     * @方法名称: queryPvpLoanAppDataByBillNo
     * @方法描述: 根据借据编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人
     */
    PvpLoanApp queryPvpLoanAppDataByBillNo(@Param("billNo") String billNo);

    PvpLoanApp selectRunByContNo(String contNo);
}