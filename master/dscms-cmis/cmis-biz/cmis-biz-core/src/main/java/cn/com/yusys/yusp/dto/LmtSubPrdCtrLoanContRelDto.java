package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubPrdCtrLoanContRel
 * @类描述: lmt_sub_prd_ctr_loan_cont_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-25 21:21:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSubPrdCtrLoanContRelDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;
    /**
     * 分项品种流水号
     **/
    private String subPrdSerno;

    /**
     * 合同编号
     **/
    private String ContNo;

    /**
     * 业务类型
     **/
    private String bizType;

    /**
     * 合同金额
     **/
    private BigDecimal contAmt;

    /**
     * 合同币种
     **/
    private String curType;

    /**
     * 合同起始日
     **/
    private String contStartDate;

    /**
     * 合同到期日
     **/
    private String contEndDate;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 责任人
     **/
    private String managerId;

    /**
     * 责任机构
     **/
    private String managerBrId;


    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSubPrdSerno() {
        return subPrdSerno;
    }

    public void setSubPrdSerno(String subPrdSerno) {
        this.subPrdSerno = subPrdSerno;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }
}