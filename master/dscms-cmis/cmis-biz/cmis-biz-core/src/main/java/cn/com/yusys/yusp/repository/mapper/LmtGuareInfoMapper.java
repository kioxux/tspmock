/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtGuareInfo;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.PldList;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-05-19 17:22:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGuareInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtGuareInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGuareInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtGuareInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtGuareInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtGuareInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtGuareInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 交易码：xdxw0035
     * 交易描述：根据流水号查询无还本续贷抵押信息
     *
     * @param indgtSerno
     * @return
     */
    List<PldList> getPldListByindgtSerno(String indgtSerno);

    /**
     * @方法名称: selectBySurveyNoFirst
     * @方法描述: 根据调查表编号查询 带土地性质条件
     * @param survey_no
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0071.resp.List> selectBySurveyNoFirst(@Param("survey_no") String survey_no);
    /**
     * @方法名称: selectBySurveyNoSecond
     * @方法描述: 根据调查表编号查询 不带土地性质条件
     * @param survey_no
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0071.resp.List> selectBySurveyNoSecond(@Param("survey_no") String survey_no);

    /**
     * queryWhbxdDYL
     * @param queryMap
     * @return
     */
    BigDecimal queryWhbxdDYL(Map queryMap);
}