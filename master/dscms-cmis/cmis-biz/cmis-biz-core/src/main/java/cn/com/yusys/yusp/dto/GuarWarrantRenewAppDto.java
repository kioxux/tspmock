package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarWarrantRenewApp
 * @类描述: guar_warrant_renew_app数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-14 16:47:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarWarrantRenewAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水 **/
	private String serno;
	
	/** 核心担保编号 **/
	private String coreGrtNo;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 押品名称 **/
	private String pldimnMemo;
	
	/** 押品类型 **/
	private String guarType;
	
	/** 权利价值 **/
	private java.math.BigDecimal certiAmt;
	
	/** 抵押人编号 **/
	private String guarCusId;
	
	/** 抵押人 **/
	private String guarCusName;
	
	/** 账务机构 **/
	private String finaBrId;
	
	/** 权证状态 **/
	private String warrantState;
	
	/** 权证出库日期 **/
	private String warrantOutDate;
	
	/** 经办人 **/
	private String huser;
	
	/** 经办机构 **/
	private String horg;
	
	/** 是否发送集中作业中心 **/
	private String isSend;
	
	/** 出库类型 **/
	private String outType;
	
	/** 权证出库类型 **/
	private String warrantOutType;
	
	/** 归还时间 **/
	private String backDate;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coreGrtNo
	 */
	public void setCoreGrtNo(String coreGrtNo) {
		this.coreGrtNo = coreGrtNo == null ? null : coreGrtNo.trim();
	}
	
    /**
     * @return CoreGrtNo
     */	
	public String getCoreGrtNo() {
		return this.coreGrtNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return CertiAmt
     */	
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param warrantState
	 */
	public void setWarrantState(String warrantState) {
		this.warrantState = warrantState == null ? null : warrantState.trim();
	}
	
    /**
     * @return WarrantState
     */	
	public String getWarrantState() {
		return this.warrantState;
	}
	
	/**
	 * @param warrantOutDate
	 */
	public void setWarrantOutDate(String warrantOutDate) {
		this.warrantOutDate = warrantOutDate == null ? null : warrantOutDate.trim();
	}
	
    /**
     * @return WarrantOutDate
     */	
	public String getWarrantOutDate() {
		return this.warrantOutDate;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser == null ? null : huser.trim();
	}
	
    /**
     * @return Huser
     */	
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param horg
	 */
	public void setHorg(String horg) {
		this.horg = horg == null ? null : horg.trim();
	}
	
    /**
     * @return Horg
     */	
	public String getHorg() {
		return this.horg;
	}
	
	/**
	 * @param isSend
	 */
	public void setIsSend(String isSend) {
		this.isSend = isSend == null ? null : isSend.trim();
	}
	
    /**
     * @return IsSend
     */	
	public String getIsSend() {
		return this.isSend;
	}
	
	/**
	 * @param outType
	 */
	public void setOutType(String outType) {
		this.outType = outType == null ? null : outType.trim();
	}
	
    /**
     * @return OutType
     */	
	public String getOutType() {
		return this.outType;
	}
	
	/**
	 * @param warrantOutType
	 */
	public void setWarrantOutType(String warrantOutType) {
		this.warrantOutType = warrantOutType == null ? null : warrantOutType.trim();
	}
	
    /**
     * @return WarrantOutType
     */	
	public String getWarrantOutType() {
		return this.warrantOutType;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate == null ? null : backDate.trim();
	}
	
    /**
     * @return BackDate
     */	
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}