/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.InPoolAssetListDto;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.AsplIoPoolDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:15:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产出入池明细")
@RequestMapping("/api/aspliopooldetails")
public class AsplIoPoolDetailsResource {
    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplIoPoolDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplIoPoolDetails> list = asplIoPoolDetailsService.selectAll(queryModel);
        return new ResultDto<List<AsplIoPoolDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplIoPoolDetails>> index(QueryModel queryModel) {
        List<AsplIoPoolDetails> list = asplIoPoolDetailsService.selectByModel(queryModel);
        return new ResultDto<List<AsplIoPoolDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplIoPoolDetails> show(@PathVariable("pkId") String pkId) {
        AsplIoPoolDetails asplIoPoolDetails = asplIoPoolDetailsService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplIoPoolDetails>(asplIoPoolDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplIoPoolDetails> create(@RequestBody AsplIoPoolDetails asplIoPoolDetails) throws URISyntaxException {
        asplIoPoolDetailsService.insert(asplIoPoolDetails);
        return new ResultDto<AsplIoPoolDetails>(asplIoPoolDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplIoPoolDetails asplIoPoolDetails) throws URISyntaxException {
        int result = asplIoPoolDetailsService.update(asplIoPoolDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplIoPoolDetailsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplIoPoolDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:入池资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("入池资产清单")
    @PostMapping("/tosignlist")
    protected ResultDto<List<InPoolAssetListDto>> toSignlist(@RequestBody QueryModel queryModel) {
        List<InPoolAssetListDto> list = asplIoPoolDetailsService.inPoolAssetList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<InPoolAssetListDto>>(list);
    }

    /**
     * 导出入池资产清单
     */
    @ApiOperation("导出入池资产清单")
    @PostMapping("/exportinpoolassetlist")
    public ResultDto<ProgressDto> exportInPoolAssetList(@RequestBody Map<String,String> map) {
        ProgressDto progressDto = asplIoPoolDetailsService.exportInPoolAssetList(map);
        return ResultDto.success(progressDto);
    }

    /**
     * @方法名称: queryAsplIoPoolDetailsDataByParams
     * @方法描述: 根据入参查询出池资产信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询出入池资产信息")
    @PostMapping("/queryaspliopooldetailsdatabyparams")
    protected ResultDto<List<AsplIoPoolDetails>> queryAsplIoPoolDetailsDataByParams(@RequestBody Map map) {
        map.put("oprType", CommonConstance.OPR_TYPE_ADD);
        List<AsplIoPoolDetails> list = asplIoPoolDetailsService.queryAsplIoPoolDetailsDataByParams(map);
        return new ResultDto<List<AsplIoPoolDetails>>(list).total(list.size());
    }

    /**
     * @方法名称: checkAsplIoPoolDetailsDataByParams
     * @方法描述: 根据流水号校验出池资产信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据流水号校验出池资产信息")
    @PostMapping("/checkaspliopooldetails")
    protected ResultDto<OpRespDto> checkAsplIoPoolDetailsDataByParams(@RequestBody Map map) {
        map.put("oprType", CommonConstance.OPR_TYPE_ADD);
        List<AsplIoPoolDetails> list = asplIoPoolDetailsService.queryAsplIoPoolDetailsDataByParams(map);
        if(Objects.isNull(list)){
            return new ResultDto<OpRespDto>(null).code(9999).message("请添加出池资产清单，当前出池资产为空！");
        }
        List<String> reqAssetNoList = list.stream()
                .map(e -> e.getAssetNo())
                .collect(Collectors.toList());
        // 获取协议编号
        String contNo = (String) map.get("contNo");
        OpRespDto opRespDto = asplAssetsListService.isOupPoolVaild(contNo,reqAssetNoList);
        return new ResultDto<OpRespDto>(opRespDto);
    }
    /**
     * @方法名称: addOutPoolDetails
     * @方法描述: 新增出池明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("新增出池资产记录明细")
    @PostMapping("/addoutpooldetails")
    protected ResultDto<AsplIoPoolDetails> addOutPoolDetails(@RequestBody Map map) {
        return asplIoPoolDetailsService.addOutPoolDetails(map);
    }
    /**
     * @方法名称: delOutPoolDetails
     * @方法描述: 删除出池明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("删除出池资产记录明细")
    @PostMapping("/deleteutpooldetails")
    protected ResultDto<AsplIoPoolDetails> delOutPoolDetails(@RequestBody Map map) {
        return asplIoPoolDetailsService.delOutPoolDetails(map);
    }
}
