/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.DocArchiveMaterList;
import cn.com.yusys.yusp.service.DocArchiveMaterListService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveMaterListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docarchivematerlist")
public class DocArchiveMaterListResource {
    @Autowired
    private DocArchiveMaterListService docArchiveMaterListService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DocArchiveMaterList>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocArchiveMaterList> list = docArchiveMaterListService.selectAll(queryModel);
        return new ResultDto<List<DocArchiveMaterList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<DocArchiveMaterList>> index(@RequestBody QueryModel queryModel) {
        List<DocArchiveMaterList> list = docArchiveMaterListService.selectByModel(queryModel);
        return new ResultDto<List<DocArchiveMaterList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{admlSerno}")
    protected ResultDto<DocArchiveMaterList> show(@PathVariable("admlSerno") String admlSerno) {
        DocArchiveMaterList docArchiveMaterList = docArchiveMaterListService.selectByPrimaryKey(admlSerno);
        return new ResultDto<DocArchiveMaterList>(docArchiveMaterList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DocArchiveMaterList> create(@RequestBody DocArchiveMaterList docArchiveMaterList) throws URISyntaxException {
        docArchiveMaterListService.insert(docArchiveMaterList);
        return new ResultDto<DocArchiveMaterList>(docArchiveMaterList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocArchiveMaterList docArchiveMaterList) throws URISyntaxException {
        int result = docArchiveMaterListService.update(docArchiveMaterList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{admlSerno}")
    protected ResultDto<Integer> delete(@PathVariable("admlSerno") String admlSerno) {
        int result = docArchiveMaterListService.deleteByPrimaryKey(admlSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docArchiveMaterListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据档案流水号获取资料清单
     *
     * @author jijian_yx
     * @date 2021/6/21 16:01
     **/
    @PostMapping("/querybydocserno")
    @ApiOperation("根据档案流水号获取资料清单")
    protected ResultDto<List<DocArchiveMaterList>> queryByDocSerno(@RequestBody QueryModel queryModel) {
        List<DocArchiveMaterList> list = docArchiveMaterListService.queryByDocSerno(queryModel);
        return new ResultDto<List<DocArchiveMaterList>>(list);
    }


    /**
     * 根据档案流水号获取资料清单(查看)
     *
     * @author liuqi_yx
     * @date 2021/6/29 16:01
     **/
    @PostMapping("/querybydocserno2")
    protected ResultDto<List<DocArchiveMaterList>> queryByDocSerno2(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort("sort+0 asc,doc_type_data desc");
        }
        List<DocArchiveMaterList> list = docArchiveMaterListService.queryByDocSerno2(queryModel);
        return new ResultDto<List<DocArchiveMaterList>>(list);
    }

    /**
     * 根据档案流水号清除清单数据
     *
     * @author jijian_yx
     * @date 2021/11/24 17:10
     **/
    @PostMapping("/deleteByDocSerno")
    protected ResultDto<Integer> deleteByDocSerno(@RequestBody String docSerno) {
        int result = docArchiveMaterListService.deleteByDocSerno(docSerno);
        return new ResultDto<>(result);
    }
}
