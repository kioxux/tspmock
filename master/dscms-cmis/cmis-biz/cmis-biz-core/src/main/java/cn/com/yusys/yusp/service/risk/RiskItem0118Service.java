package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 关联方提醒
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0118Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0118Service.class);

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/8/30 21:20
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0118(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        if(StringUtils.isBlank(cusId)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人客户编号为空！");
            return riskResultDto;
        }
        int count = 0 ;
        log.info("*************关联方提醒开始***********借款人ID：【{}】", cusId);
        try{
            // 根据客户ID去查询关联人信息
            CmisCus0020ReqDto cmisCus0020ReqDto = new CmisCus0020ReqDto();
            cmisCus0020ReqDto.setCusId(cusId);
            log.info("*************关联方查询发送报文：【{}】", JSON.toJSONString(cmisCus0020ReqDto));
            ResultDto<CmisCus0020RespDto> result = cmisCusClientService.cmiscus0020(cmisCus0020ReqDto);
            log.info("*************关联方查询返回报文：【{}】", JSON.toJSONString(result));
            if(result.getCode().equals("9999")){
                throw BizException.error(null, "9999", "查询客户是否为关联方失败！");
            } else {
                count =  result.getData().getCount();
            }
            if(count > 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("借款人为我行关联方！");
                return riskResultDto;
            }
        } catch (BizException biz) {
            log.info("*************关联方提醒异常***********异常信息：【{}】", JSON.toJSONString(biz));
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(biz.getI18nData().toString());
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(e.toString());
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************关联方提醒结束***********借款人ID：【{}】", cusId);
        return riskResultDto;
    }
}
