/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplAppt
 * @类描述: iqp_appl_appt数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 22:53:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appl_appt")
public class IqpApplAppt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 申请人姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 发证机关 **/
	@Column(name = "ISSU_DEP", unique = false, nullable = true, length = 40)
	private String issuDep;
	
	/** 有效期开始日期 **/
	@Column(name = "ISSU_START_DT", unique = false, nullable = true, length = 10)
	private String issuStartDt;
	
	/** 有效期截止日期 **/
	@Column(name = "ISSU_END_DT", unique = false, nullable = true, length = 10)
	private String issuEndDt;
	
	/** 客户曾用名 **/
	@Column(name = "PER_CUST_NAME", unique = false, nullable = true, length = 80)
	private String perCustName;
	
	/** 英文名称 **/
	@Column(name = "EN_NAME", unique = false, nullable = true, length = 80)
	private String enName;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 国籍信息 **/
	@Column(name = "CNTY", unique = false, nullable = true, length = 5)
	private String cnty;
	
	/** 国籍补充信息 **/
	@Column(name = "CNTY_EXT", unique = false, nullable = true, length = 200)
	private String cntyExt;
	
	/** 地区 **/
	@Column(name = "AREA", unique = false, nullable = true, length = 5)
	private String area;
	
	/** 市级行政区/县/县级市 **/
	@Column(name = "PERMANENT_AREA", unique = false, nullable = true, length = 12)
	private String permanentArea;
	
	/** 户籍详细地址 **/
	@Column(name = "PERMANENT_ADDR", unique = false, nullable = true, length = 200)
	private String permanentAddr;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = true, length = 5)
	private String edu;
	
	/** 学位 **/
	@Column(name = "DEGREE", unique = false, nullable = true, length = 5)
	private String degree;
	
	/** 是否为农户 **/
	@Column(name = "IS_HOUSEHOLD", unique = false, nullable = true, length = 5)
	private String isHousehold;
	
	/** 是否自雇人士 **/
	@Column(name = "IS_SELF_EMPLOY", unique = false, nullable = true, length = 5)
	private String isSelfEmploy;
	
	/** 居住信息-市级行政区/县/县级市 **/
	@Column(name = "LIVING_AREA", unique = false, nullable = true, length = 12)
	private String livingArea;
	
	/** 居住地址 **/
	@Column(name = "LIVING_ADDR", unique = false, nullable = true, length = 200)
	private String livingAddr;
	
	/** 居住状况 **/
	@Column(name = "LIVING_STS", unique = false, nullable = true, length = 5)
	private String livingSts;
	
	/** 通讯地址类型
 **/
	@Column(name = "POST_SEND_ADDR_KIND", unique = false, nullable = true, length = 5)
	private String postSendAddrKind;
	
	/** 通讯信息-市级行政区/县/县级市 **/
	@Column(name = "POST_SEND_AREA", unique = false, nullable = true, length = 12)
	private String postSendArea;
	
	/** 通讯地址补充 **/
	@Column(name = "POST_SEND_ADDR", unique = false, nullable = true, length = 200)
	private String postSendAddr;
	
	/** 邮编 **/
	@Column(name = "POST_CODE", unique = false, nullable = true, length = 6)
	private String postCode;
	
	/** 电话号码/手机号 **/
	@Column(name = "TEL", unique = false, nullable = true, length = 30)
	private String tel;
	
	/** 备用电话 **/
	@Column(name = "BACK_TEL", unique = false, nullable = true, length = 30)
	private String backTel;
	
	/** 电子邮件 **/
	@Column(name = "MAIL", unique = false, nullable = true, length = 255)
	private String mail;
	
	/** 年龄 **/
	@Column(name = "AGE", unique = false, nullable = true, length = 10)
	private Integer age;
	
	/** 生日 **/
	@Column(name = "BIRTHDAY", unique = false, nullable = true, length = 10)
	private String birthday;
	
	/** 配偶是否共借人 **/
	@Column(name = "IS_COMM_BORR", unique = false, nullable = true, length = 5)
	private String isCommBorr;
	
	/** 工作单位 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 120)
	private String workUnit;
	
	/** 月收入（元） **/
	@Column(name = "MON_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal monIncome;
	
	/** 居住信息-邮编 **/
	@Column(name = "LIVING_ZIP", unique = false, nullable = true, length = 10)
	private String livingZip;
	
	/** 与主借人关系 **/
	@Column(name = "REL_WITH_APPLYER", unique = false, nullable = true, length = 5)
	private String relWithApplyer;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 5)
	private String duty;
	
	/** 单位所属行业 **/
	@Column(name = "CPRT_TRADE", unique = false, nullable = true, length = 5)
	private String cprtTrade;
	
	/** 客户风险预警级别 **/
	@Column(name = "CUS_RISK_LEVEL", unique = false, nullable = true, length = 500)
	private String cusRiskLevel;
	
	/** 重点客户依据说明 **/
	@Column(name = "PRIOR_CUS_DESC", unique = false, nullable = true, length = 500)
	private String priorCusDesc;
	
	/** 其他说明(如预警情况) **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 500)
	private String otherDesc;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param issuDep
	 */
	public void setIssuDep(String issuDep) {
		this.issuDep = issuDep;
	}
	
    /**
     * @return issuDep
     */
	public String getIssuDep() {
		return this.issuDep;
	}
	
	/**
	 * @param issuStartDt
	 */
	public void setIssuStartDt(String issuStartDt) {
		this.issuStartDt = issuStartDt;
	}
	
    /**
     * @return issuStartDt
     */
	public String getIssuStartDt() {
		return this.issuStartDt;
	}
	
	/**
	 * @param issuEndDt
	 */
	public void setIssuEndDt(String issuEndDt) {
		this.issuEndDt = issuEndDt;
	}
	
    /**
     * @return issuEndDt
     */
	public String getIssuEndDt() {
		return this.issuEndDt;
	}
	
	/**
	 * @param perCustName
	 */
	public void setPerCustName(String perCustName) {
		this.perCustName = perCustName;
	}
	
    /**
     * @return perCustName
     */
	public String getPerCustName() {
		return this.perCustName;
	}
	
	/**
	 * @param enName
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}
	
    /**
     * @return enName
     */
	public String getEnName() {
		return this.enName;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param cnty
	 */
	public void setCnty(String cnty) {
		this.cnty = cnty;
	}
	
    /**
     * @return cnty
     */
	public String getCnty() {
		return this.cnty;
	}
	
	/**
	 * @param cntyExt
	 */
	public void setCntyExt(String cntyExt) {
		this.cntyExt = cntyExt;
	}
	
    /**
     * @return cntyExt
     */
	public String getCntyExt() {
		return this.cntyExt;
	}
	
	/**
	 * @param area
	 */
	public void setArea(String area) {
		this.area = area;
	}
	
    /**
     * @return area
     */
	public String getArea() {
		return this.area;
	}
	
	/**
	 * @param permanentArea
	 */
	public void setPermanentArea(String permanentArea) {
		this.permanentArea = permanentArea;
	}
	
    /**
     * @return permanentArea
     */
	public String getPermanentArea() {
		return this.permanentArea;
	}
	
	/**
	 * @param permanentAddr
	 */
	public void setPermanentAddr(String permanentAddr) {
		this.permanentAddr = permanentAddr;
	}
	
    /**
     * @return permanentAddr
     */
	public String getPermanentAddr() {
		return this.permanentAddr;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}
	
    /**
     * @return edu
     */
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param degree
	 */
	public void setDegree(String degree) {
		this.degree = degree;
	}
	
    /**
     * @return degree
     */
	public String getDegree() {
		return this.degree;
	}
	
	/**
	 * @param isHousehold
	 */
	public void setIsHousehold(String isHousehold) {
		this.isHousehold = isHousehold;
	}
	
    /**
     * @return isHousehold
     */
	public String getIsHousehold() {
		return this.isHousehold;
	}
	
	/**
	 * @param isSelfEmploy
	 */
	public void setIsSelfEmploy(String isSelfEmploy) {
		this.isSelfEmploy = isSelfEmploy;
	}
	
    /**
     * @return isSelfEmploy
     */
	public String getIsSelfEmploy() {
		return this.isSelfEmploy;
	}
	
	/**
	 * @param livingArea
	 */
	public void setLivingArea(String livingArea) {
		this.livingArea = livingArea;
	}
	
    /**
     * @return livingArea
     */
	public String getLivingArea() {
		return this.livingArea;
	}
	
	/**
	 * @param livingAddr
	 */
	public void setLivingAddr(String livingAddr) {
		this.livingAddr = livingAddr;
	}
	
    /**
     * @return livingAddr
     */
	public String getLivingAddr() {
		return this.livingAddr;
	}
	
	/**
	 * @param livingSts
	 */
	public void setLivingSts(String livingSts) {
		this.livingSts = livingSts;
	}
	
    /**
     * @return livingSts
     */
	public String getLivingSts() {
		return this.livingSts;
	}
	
	/**
	 * @param postSendAddrKind
	 */
	public void setPostSendAddrKind(String postSendAddrKind) {
		this.postSendAddrKind = postSendAddrKind;
	}
	
    /**
     * @return postSendAddrKind
     */
	public String getPostSendAddrKind() {
		return this.postSendAddrKind;
	}
	
	/**
	 * @param postSendArea
	 */
	public void setPostSendArea(String postSendArea) {
		this.postSendArea = postSendArea;
	}
	
    /**
     * @return postSendArea
     */
	public String getPostSendArea() {
		return this.postSendArea;
	}
	
	/**
	 * @param postSendAddr
	 */
	public void setPostSendAddr(String postSendAddr) {
		this.postSendAddr = postSendAddr;
	}
	
    /**
     * @return postSendAddr
     */
	public String getPostSendAddr() {
		return this.postSendAddr;
	}
	
	/**
	 * @param postCode
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
    /**
     * @return postCode
     */
	public String getPostCode() {
		return this.postCode;
	}
	
	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	
    /**
     * @return tel
     */
	public String getTel() {
		return this.tel;
	}
	
	/**
	 * @param backTel
	 */
	public void setBackTel(String backTel) {
		this.backTel = backTel;
	}
	
    /**
     * @return backTel
     */
	public String getBackTel() {
		return this.backTel;
	}
	
	/**
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
    /**
     * @return mail
     */
	public String getMail() {
		return this.mail;
	}
	
	/**
	 * @param age
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	
    /**
     * @return age
     */
	public Integer getAge() {
		return this.age;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param isCommBorr
	 */
	public void setIsCommBorr(String isCommBorr) {
		this.isCommBorr = isCommBorr;
	}
	
    /**
     * @return isCommBorr
     */
	public String getIsCommBorr() {
		return this.isCommBorr;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param monIncome
	 */
	public void setMonIncome(java.math.BigDecimal monIncome) {
		this.monIncome = monIncome;
	}
	
    /**
     * @return monIncome
     */
	public java.math.BigDecimal getMonIncome() {
		return this.monIncome;
	}
	
	/**
	 * @param livingZip
	 */
	public void setLivingZip(String livingZip) {
		this.livingZip = livingZip;
	}
	
    /**
     * @return livingZip
     */
	public String getLivingZip() {
		return this.livingZip;
	}
	
	/**
	 * @param relWithApplyer
	 */
	public void setRelWithApplyer(String relWithApplyer) {
		this.relWithApplyer = relWithApplyer;
	}
	
    /**
     * @return relWithApplyer
     */
	public String getRelWithApplyer() {
		return this.relWithApplyer;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param cprtTrade
	 */
	public void setCprtTrade(String cprtTrade) {
		this.cprtTrade = cprtTrade;
	}
	
    /**
     * @return cprtTrade
     */
	public String getCprtTrade() {
		return this.cprtTrade;
	}
	
	/**
	 * @param cusRiskLevel
	 */
	public void setCusRiskLevel(String cusRiskLevel) {
		this.cusRiskLevel = cusRiskLevel;
	}
	
    /**
     * @return cusRiskLevel
     */
	public String getCusRiskLevel() {
		return this.cusRiskLevel;
	}
	
	/**
	 * @param priorCusDesc
	 */
	public void setPriorCusDesc(String priorCusDesc) {
		this.priorCusDesc = priorCusDesc;
	}
	
    /**
     * @return priorCusDesc
     */
	public String getPriorCusDesc() {
		return this.priorCusDesc;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}


}