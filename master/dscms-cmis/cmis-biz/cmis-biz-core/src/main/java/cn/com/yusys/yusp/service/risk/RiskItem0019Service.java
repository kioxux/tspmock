package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.Cmislmt0016LmtCopSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.core.dp2099.Dp2099Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0016.CmisLmt0016Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 若当前授信产品为惠享贷，且该客户下存在在途或者生效的优企贷、优农贷授信，则拦截
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0019Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0019Service.class);

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021年10月11日16:04:49
     * @version 1.0.0
     * @desc 若当前授信产品为惠享贷，且该客户下存在在途或者生效的优企贷、优农贷授信，则拦截
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0019(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("**********小微贷款产品互斥校验开始*******************业务流水号：【{}】",serno);
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 判断授信批复表，如果存在生效的优企贷、优农贷，则拦截
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(serno);
        log.info("**********小微贷款产品互斥校验*******************查询调查基本表：【{}】",JSON.toJSONString(lmtSurveyReportMainInfo));
        if (Objects.isNull(lmtSurveyReportMainInfo) || StringUtils.isBlank(lmtSurveyReportMainInfo.getPrdId())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0103);
            return riskResultDto;
        } else {
            // 判断当前授信产品是否为惠享贷
            if(StringUtils.isBlank(lmtSurveyReportMainInfo.getCertCode())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1902);
                return riskResultDto;
            }else {
                if ("SC060001".equals(lmtSurveyReportMainInfo.getPrdId())) {
                    // 根据证件号去查询
                    QueryModel model = new QueryModel();
                    model.addCondition("certCode", lmtSurveyReportMainInfo.getCertCode());
                    List<LmtCrdReplyInfo> list = lmtCrdReplyInfoService.selectByModel(model);
                    log.info("**********小微贷款产品互斥校验*******************,根据证件号码查询批复：【{}】",JSON.toJSONString(list));
                    if (list.size() > 0) {
                        for (LmtCrdReplyInfo lmtCrdReplyInfo : list) {
                            // SC010008：即是优企贷产品，SC020010：即是优农贷产品
                            if (("SC010008".equals(lmtCrdReplyInfo.getPrdId()) || "SC020010".equals(lmtCrdReplyInfo.getPrdId())) && "01".equals(lmtCrdReplyInfo.getReplyStatus())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1901);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
