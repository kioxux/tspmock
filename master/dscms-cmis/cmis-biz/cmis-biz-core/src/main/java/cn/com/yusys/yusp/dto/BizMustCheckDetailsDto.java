package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BizMustCheckDetails
 * @类描述: biz_must_check_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-12 15:49:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BizMustCheckDetailsDto implements Serializable{
	private static final long serialVersionUID = 1L;
	//页面标识列表
	private String idList;
	//页面名称列表
	private String  pageList;

	//业务类型
	private String bizType;
	//流水号
	private String serno;

	public String getIdList() {
		return idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getPageList() {
		return pageList;
	}

	public void setPageList(String pageList) {
		this.pageList = pageList;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
}