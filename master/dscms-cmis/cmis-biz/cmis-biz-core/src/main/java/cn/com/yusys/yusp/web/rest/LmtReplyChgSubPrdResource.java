/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.CusIndivSocialResp;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyChgSubPrd;
import cn.com.yusys.yusp.service.LmtReplyChgSubPrdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgSubPrdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-24 14:33:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplychgsubprd")
public class LmtReplyChgSubPrdResource {
    @Autowired
    private LmtReplyChgSubPrdService lmtReplyChgSubPrdService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyChgSubPrd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyChgSubPrd> list = lmtReplyChgSubPrdService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyChgSubPrd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyChgSubPrd>> index(QueryModel queryModel) {
        List<LmtReplyChgSubPrd> list = lmtReplyChgSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyChgSubPrd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyChgSubPrd> show(@PathVariable("pkId") String pkId) {
        LmtReplyChgSubPrd lmtReplyChgSubPrd = lmtReplyChgSubPrdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyChgSubPrd>(lmtReplyChgSubPrd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyChgSubPrd> create(@RequestBody LmtReplyChgSubPrd lmtReplyChgSubPrd) throws URISyntaxException {
        lmtReplyChgSubPrdService.insert(lmtReplyChgSubPrd);
        return new ResultDto<LmtReplyChgSubPrd>(lmtReplyChgSubPrd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyChgSubPrd lmtReplyChgSubPrd) throws URISyntaxException {
        int result = lmtReplyChgSubPrdService.update(lmtReplyChgSubPrd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyChgSubPrdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyChgSubPrdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据分项流水号查询批复变更分项适用品种明细表
     *
     *
     * @return
     */
    @PostMapping("/selectbysubserno")
    protected   ResultDto<List<LmtReplyChgSubPrd>> selectBySubSerno(@RequestBody String subSerno) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("subSerno",subSerno);
        List<LmtReplyChgSubPrd> lmtReplyChgSubPrdList = lmtReplyChgSubPrdService.queryLmtReplyChgSubPrdByParams(map);
        return new ResultDto<List<LmtReplyChgSubPrd>>(lmtReplyChgSubPrdList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<LmtReplyChgSubPrd> insert(@RequestBody LmtReplyChgSubPrd lmtReplyChgSubPrd) throws URISyntaxException {
        lmtReplyChgSubPrd.setPkId(UUID.randomUUID().toString());
        lmtReplyChgSubPrd.setSubPrdSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()));
        lmtReplyChgSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        lmtReplyChgSubPrdService.insert(lmtReplyChgSubPrd);
        return new ResultDto<LmtReplyChgSubPrd>(lmtReplyChgSubPrd);
    }
}