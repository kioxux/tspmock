package cn.com.yusys.yusp.service.server.xdtz0049;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0049.req.Xdtz0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.resp.Xdtz0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 接口处理类:客户贷款信息查询
 *
 * @author xs
 * @version 1.0
 * @des  信贷根据客户证件号查询acc_loan信息
 * 该接口不是list返回，对于多条情况时贷款产品、担保方式、还款方式采用拼接方式，
 * 中间逗号隔开，贷款金额、贷款余额求和。
 */
@Service
public class Xdtz0049Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdtz0011.Xdtz0011Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CommonService commonService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0049DataRespDto xdtz0049(Xdtz0049DataReqDto xdtz0049DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0049DataReqDto));
        Xdtz0049DataRespDto xdtz0049DataRespDto = new Xdtz0049DataRespDto();
        try {
            String certNo = xdtz0049DataReqDto.getCertNo();//身份证号

            String cusId = "";//客户编码
            List<String> cusIds = commonService.getCusBaseByCertCode(certNo);
            if(CollectionUtils.isEmpty(cusIds)){
                return xdtz0049DataRespDto;
            }
            List<Xdtz0049DataRespDto>  xdtz0049datarespdtos = accLoanMapper.xdtz0049(cusIds.get(0));
            if(xdtz0049datarespdtos.size()<1 || xdtz0049datarespdtos==null){
                return xdtz0049DataRespDto;
            }else{
                String loanPrd = "";//贷款产品
                String assureMeans = "";//担保方式
                String repayType = "";//还款方式
                BigDecimal loanAmtSum = new BigDecimal("0");
                BigDecimal loanBalanceSum = new BigDecimal("0");
                for(Xdtz0049DataRespDto xdtz0049datarespdto:xdtz0049datarespdtos){
                    loanPrd += xdtz0049datarespdto.getLoanPrd()+",";
                    assureMeans += xdtz0049datarespdto.getAssureMeans()+",";
                    repayType += xdtz0049datarespdto.getRepayType()+",";
                    loanAmtSum = loanAmtSum.add(xdtz0049datarespdto.getLoanAmt());
                    loanBalanceSum = loanBalanceSum.add(xdtz0049datarespdto.getLoanBalance());
                }
                xdtz0049DataRespDto.setCusId(xdtz0049datarespdtos.get(0).getCusId());
                xdtz0049DataRespDto.setCusName(xdtz0049datarespdtos.get(0).getCusName());
                xdtz0049DataRespDto.setLoanPrd(loanPrd);
                xdtz0049DataRespDto.setBelgTrade("");//所属行业
                xdtz0049DataRespDto.setCusType("");//客户类型
                xdtz0049DataRespDto.setAssureMeans(assureMeans);
                xdtz0049DataRespDto.setRepayType(repayType);
                xdtz0049DataRespDto.setLoanAmt(loanAmtSum);
                xdtz0049DataRespDto.setLoanBalance(loanBalanceSum);
            }
            //查询参数
            logger.info("客户贷款信息查询,查询参数为:{}", JSON.toJSONString(xdtz0049DataReqDto));
            logger.info("客户贷款信息查询,反回参数为:{}", JSON.toJSONString(xdtz0049DataRespDto));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, e.getMessage());
            throw new Exception(EcbEnum.ECB019999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049DataRespDto));
        return xdtz0049DataRespDto;
    }
}
