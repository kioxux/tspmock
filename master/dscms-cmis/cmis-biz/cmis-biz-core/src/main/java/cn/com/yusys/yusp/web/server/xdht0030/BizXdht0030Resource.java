package cn.com.yusys.yusp.web.server.xdht0030;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0030.req.Xdht0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0030.resp.Xdht0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0030.Xdht0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户我行合作次数
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0030:查询客户我行合作次数")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0030Resource.class);

    @Autowired
    private Xdht0030Service xdht0030Service;

    /**
     * 交易码：xdht0030
     * 交易描述：查询客户我行合作次数
     *
     * @param xdht0030DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("查询客户我行合作次数")
    @PostMapping("/xdht0030")
    protected @ResponseBody
    ResultDto<Xdht0030DataRespDto>    xdht0030(@Validated @RequestBody Xdht0030DataReqDto xdht0030DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030DataReqDto));
        Xdht0030DataRespDto  xdht0030DataRespDto  = new Xdht0030DataRespDto();// 响应Dto:查询客户我行合作次数
        ResultDto<Xdht0030DataRespDto>xdht0030DataResultDto = new ResultDto<>();
        try {
            // 从xdht0030DataReqDto获取业务值进行业务逻辑处理
            String certNo = xdht0030DataReqDto.getCertNo();//证件号码
            // 调用xdht0030Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, certNo);
            int count = xdht0030Service.getCoopTimesByCertCode(certNo);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, count);

            xdht0030DataRespDto.setCoopTimes(count);// 合作次数
            // 封装xdht0030DataResultDto中正确的返回码和返回信息
            xdht0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, e.getMessage());
            // 封装xdht0030DataResultDto中异常返回码和返回信息
            xdht0030DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0030DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0030DataRespDto到xdht0030DataResultDto中
        xdht0030DataResultDto.setData(xdht0030DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030DataResultDto));
        return xdht0030DataResultDto;
    }
}