/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtReplyChgSub;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgSubMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 10:22:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplyChgSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtReplyChgSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReplyChgSub> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtReplyChgSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtReplyChgSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtReplyChgSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateSelective(LmtReplyChgSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称：insertSelectiveList
     * @方法描述：批量插
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-26 下午 4:42
     * @修改记录：修改时间 修改人员  修改原因
     */
    int insertList(List<LmtReplyChgSub> list);

    /**
     * @方法名称：queryLmtReplyChgSubByParams
     * @方法描述：通过查询条件查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-05-03 14:44:44
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtReplyChgSub> queryLmtReplyChgSubByParams(HashMap<String, String> queryMap);

}