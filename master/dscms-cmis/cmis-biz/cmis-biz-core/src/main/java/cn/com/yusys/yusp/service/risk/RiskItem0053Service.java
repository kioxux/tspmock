package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpHouseService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.util.IDCardUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0053Service
 * @类描述: 个人按揭贷款-贷款期限校验
 * @功能描述: 个人按揭贷款-贷款期限校验
 * @创建人: hubp
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0053Service {

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpHouseService iqpHouseService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @方法名称: riskItem0053
     * @方法描述: 个人按揭贷款-贷款期限校验
     * @参数与返回说明:
     * @算法描述:
     * 1、申请住房贷款时，贷款期限+客户年龄>70周岁
     * 2、提交失败，个人和住房按揭贷款期限最长不超过30年（一手，二手都是），，
     * 3、个人住房按揭贷款 中，贷款期限+房龄必选小于等于50年
     * @创建人: hubp
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0053(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        String prdType = StringUtils.EMPTY;
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if (Objects.isNull(iqpLoanApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003); //通过授信申请流水号未获取到对应的授信申请信息
            return riskResultDto;
        } else {
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
            //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
            prdType = CfgPrdBasicinfoDto.getData().getPrdType();;
            if(prdType.equals("10")){
                // 如果属于按揭类
                IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);
                if (Objects.isNull(iqpHouse)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05301); //通过授信申请流水号未获取到对应的房屋信息
                    return riskResultDto;
                }
                Integer appTerm = 0; // 贷款期限
                Integer ageMonth = 0; // 客户年龄月份
                Integer houseYears = 0; // 房龄
                if(Objects.isNull(iqpLoanApp.getAppTerm())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05302); //贷款期限不可为空
                    return riskResultDto;
                }else{
                    appTerm = Double.valueOf(String.valueOf(iqpLoanApp.getAppTerm())).intValue();
                    if(appTerm > 30 * 12){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05303); //客户证件号码不可为空
                        return riskResultDto;
                    }
                }
                if(StringUtils.isBlank(iqpLoanApp.getCertCode())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05304); //客户证件号码不可为空
                    return riskResultDto;
                }else {
                    ageMonth = this.handleAge(iqpLoanApp.getCertCode());
                    if(appTerm + ageMonth > 70 * 12 ) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05305); //贷款期限+客户年龄不能大于70周岁
                        return riskResultDto;
                    }
                }
                if(Objects.isNull(iqpHouse.getHouseYears())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05306); //房龄不可为空
                    return riskResultDto;
                }else{
                    houseYears = iqpHouse.getHouseYears();
                    if(appTerm + (houseYears * 12) > 50 * 12){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05307); //贷款期限+房龄必选小于等于50年
                        return riskResultDto;
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param certCode
     * @return java.lang.Integer
     * @author hubp
     * @date 2021/7/29 17:12
     * @version 1.0.0
     * @desc 根据证件号码计算年龄 --精确到月
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private Integer handleAge(String certCode) {
        //获取年龄
        int age = IDCardUtils.getAgeByIdCard(certCode);
        // 获取身份证月份
        String cardMonth = IDCardUtils.getMonthByIdCard(certCode).toString();
        // 获取当前月份
        String nowMonth = Integer.toString(DateUtils.getCurrMonth());
        // 获取身份证日期
        String cardDate = IDCardUtils.getDateByIdCard(certCode).toString();
        // 获取当前日期
        String nowDate = Integer.toString(DateUtils.getCurrMonthDay());
        // 临时存在月份
        Integer tempMonth = 0;
        // 当周岁需要减一时，计算剩下的月份
        Integer i = 12 - (Integer.parseInt(nowMonth) - Integer.parseInt(cardMonth));
        if (cardMonth.compareTo(nowMonth) < 0) {
            age--;
            tempMonth = i + (age * 12);

        } else if (cardMonth.compareTo(nowMonth) == 0 && cardDate.compareTo(nowDate) <= 0) {
            age--;
            tempMonth = i + (age * 12);
        } else {
            tempMonth = (Integer.parseInt(cardMonth) - Integer.parseInt(nowMonth)) + (age * 12);
        }
        return tempMonth;
    }
}
