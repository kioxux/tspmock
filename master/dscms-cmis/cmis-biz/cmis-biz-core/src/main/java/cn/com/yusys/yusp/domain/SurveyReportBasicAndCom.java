package cn.com.yusys.yusp.domain;

/**
 * @创建人 WH
 * @创建时间 2021-04-12
 * @return SurveyReportBasicAndCom
 **/

public class SurveyReportBasicAndCom {
    private LmtSurveyReportBasicInfo surveyReportBasicInfo;
    private LmtSurveyReportComInfo surveyReportComInfo;
    private int i;

    @Override
    public String toString() {
        return "SurveyReportBasicAndCom{" +
                "surveyReportBasicInfo=" + surveyReportBasicInfo +
                ", surveyReportComInfo=" + surveyReportComInfo +
                ", i=" + i +
                '}';
    }

    public LmtSurveyReportBasicInfo getSurveyReportBasicInfo() {
        return surveyReportBasicInfo;
    }

    public void LmtSurveyReportBasicInfo(LmtSurveyReportBasicInfo surveyReportBasicInfo) {
        this.surveyReportBasicInfo = surveyReportBasicInfo;
    }

    public LmtSurveyReportComInfo getSurveyReportComInfo() {
        return surveyReportComInfo;
    }

    public void setSurveyReportComInfo(LmtSurveyReportComInfo surveyReportComInfo) {
        this.surveyReportComInfo = surveyReportComInfo;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
