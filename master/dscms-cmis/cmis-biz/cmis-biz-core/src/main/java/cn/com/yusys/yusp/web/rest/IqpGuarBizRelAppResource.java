/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpGuarBizRelApp;
import cn.com.yusys.yusp.service.IqpGuarBizRelAppService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarBizRelAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:55:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpguarbizrelapp")
public class IqpGuarBizRelAppResource {

    private static final Logger log = LoggerFactory.getLogger(IqpGuarBizRelAppResource.class);
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;

	/**
     * 全表查询.
     * 
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpGuarBizRelApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpGuarBizRelApp> list = iqpGuarBizRelAppService.selectAll(queryModel);
        return new ResultDto<List<IqpGuarBizRelApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpGuarBizRelApp>> index(QueryModel queryModel) {
        List<IqpGuarBizRelApp> list = iqpGuarBizRelAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpGuarBizRelApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpGuarBizRelApp> show(@PathVariable("pkId") String pkId) {
        IqpGuarBizRelApp iqpGuarBizRelApp = iqpGuarBizRelAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpGuarBizRelApp>(iqpGuarBizRelApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpGuarBizRelApp> create(@RequestBody IqpGuarBizRelApp iqpGuarBizRelApp) throws URISyntaxException {
        iqpGuarBizRelAppService.insert(iqpGuarBizRelApp);
        return new ResultDto<IqpGuarBizRelApp>(iqpGuarBizRelApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpGuarBizRelApp iqpGuarBizRelApp) throws URISyntaxException {
        int result = iqpGuarBizRelAppService.update(iqpGuarBizRelApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpGuarBizRelAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpGuarBizRelAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 引入担保合同信息
     * @param params
     * @return
     */
    @PostMapping("/queryIqpGuarBizRelAppExists")
    protected ResultDto<Map> queryIqpGuarBizRelAppExists(@RequestBody Map params){
        log.info("校验担保合同是否引入，入参信息："+params.toString());
        Map rtnData =  iqpGuarBizRelAppService.queryIqpGuarBizRelAppExists(params);
        return new ResultDto<Map>(rtnData);
    }
    /**
     * 引入合同信息
     * @param params
     * @return
     */
    @PostMapping("/queryIqpGuarBizRelAppContNo")
    protected ResultDto<Map> queryIqpGuarBizRelAppContNo(@RequestBody Map params){
        log.info("校验合同是否引入，入参信息："+params.toString());
        Map rtnData =  iqpGuarBizRelAppService.queryIqpGuarBizRelAppContNo(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 通过担保合同编号获取担保合同已用金额信息
     * @param params
     * @return
     */
    @PostMapping("/getAmtInfoByGuarContNo")
    protected  ResultDto<Map> getAmtInfoByGuarContNo(@RequestBody Map params){
        log.info("通过担保合同编号查询额度信息，入参信息："+params.toString());
        Map rtnData = iqpGuarBizRelAppService.getAmtInfoByGuarContNo(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 通过入参更新担保合同引用关系状态
     * @param params
     * @return
     */
    @PostMapping("/delImportRel")
    protected  ResultDto<Map> delImportRel(@RequestBody Map params){
        log.info("解除担保合同已用关系，入参信息："+params.toString());
        Map rtnData = iqpGuarBizRelAppService.delImportRel(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 更新担保合同与业务关系数据
     * @param params
     * @return
     */
    @PostMapping("/updateImportRel")
    protected ResultDto<Map> updateImportRel(@RequestBody Map params){
        log.info("更新担保合同以及关系数据，入参信息："+params.toString());
        Map rtnData = iqpGuarBizRelAppService.updateImportRel(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 生成担保变更申请更新担保与业务申请
     * @param iqpGuarBizRel
     * @return
     */
    @PostMapping("/querySelectIqpSerno/")
    protected ResultDto<Integer> querySelectIqpSerno(@RequestBody IqpGuarBizRelApp iqpGuarBizRel)throws Exception {
        int result = iqpGuarBizRelAppService.querySelectIqpSerno(iqpGuarBizRel);
        return new ResultDto<Integer>(result);
    }
}
