/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.vo.DocReadDelayInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocReadDelayInfo;
import cn.com.yusys.yusp.service.DocReadDelayInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDelayInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 09:36:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docreaddelayinfo")
public class DocReadDelayInfoResource {
    @Autowired
    private DocReadDelayInfoService docReadDelayInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<DocReadDelayInfoVo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocReadDelayInfoVo> list = docReadDelayInfoService.selectAll(queryModel);
        return new ResultDto<List<DocReadDelayInfoVo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<DocReadDelayInfoVo>> index(@RequestBody QueryModel queryModel) {
        List<DocReadDelayInfoVo> list = docReadDelayInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocReadDelayInfoVo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/detail")
    protected ResultDto<DocReadDelayInfo> show(@RequestBody DocReadDelayInfo param) {
        DocReadDelayInfo docReadDelayInfo = docReadDelayInfoService.selectByPrimaryKey(param.getDrdiSerno());
        return new ResultDto<DocReadDelayInfo>(docReadDelayInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<DocReadDelayInfo> create(@RequestBody DocReadDelayInfo docReadDelayInfo) throws URISyntaxException {
        docReadDelayInfoService.insert(docReadDelayInfo);
        return new ResultDto<DocReadDelayInfo>(docReadDelayInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocReadDelayInfo docReadDelayInfo) throws URISyntaxException {
        int result = docReadDelayInfoService.update(docReadDelayInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody DocReadDelayInfo param) {
        int result = docReadDelayInfoService.deleteByPrimaryKey(param.getDrdiSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete")
    protected ResultDto<Integer> deletes(@RequestBody DocReadDelayInfo param) {
        int result = docReadDelayInfoService.deleteByIds(param.getDraiSerno());
        return new ResultDto<Integer>(result);
    }
}
