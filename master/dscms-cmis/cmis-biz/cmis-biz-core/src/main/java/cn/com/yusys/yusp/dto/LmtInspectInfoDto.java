package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtInspectInfo
 * @类描述: lmt_inspect_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtInspectInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 勘验流水号 **/
	private String inspectSerno;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 云评估编号 **/
	private String cloudEvalNo;
	
	/** 勘验人 **/
	private String inspector;
	
	/** 勘验人证件类型 **/
	private String inspectorCertType;
	
	/** 勘验人证件号码 **/
	private String inspectorCertCode;
	
	/** 抵押物所有人 **/
	private String pawnOwner;
	
	/** 楼盘名称 **/
	private String buildingName;
	
	/** 楼栋 **/
	private String building;
	
	/** 面积 **/
	private java.math.BigDecimal squ;
	
	/** 地址 **/
	private String addr;
	
	/** 视频流水号 **/
	private String videoSerno;
	
	/** 勘验状态 **/
	private String inspectStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param inspectSerno
	 */
	public void setInspectSerno(String inspectSerno) {
		this.inspectSerno = inspectSerno == null ? null : inspectSerno.trim();
	}
	
    /**
     * @return InspectSerno
     */	
	public String getInspectSerno() {
		return this.inspectSerno;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cloudEvalNo
	 */
	public void setCloudEvalNo(String cloudEvalNo) {
		this.cloudEvalNo = cloudEvalNo == null ? null : cloudEvalNo.trim();
	}
	
    /**
     * @return CloudEvalNo
     */	
	public String getCloudEvalNo() {
		return this.cloudEvalNo;
	}
	
	/**
	 * @param inspector
	 */
	public void setInspector(String inspector) {
		this.inspector = inspector == null ? null : inspector.trim();
	}
	
    /**
     * @return Inspector
     */	
	public String getInspector() {
		return this.inspector;
	}
	
	/**
	 * @param inspectorCertType
	 */
	public void setInspectorCertType(String inspectorCertType) {
		this.inspectorCertType = inspectorCertType == null ? null : inspectorCertType.trim();
	}
	
    /**
     * @return InspectorCertType
     */	
	public String getInspectorCertType() {
		return this.inspectorCertType;
	}
	
	/**
	 * @param inspectorCertCode
	 */
	public void setInspectorCertCode(String inspectorCertCode) {
		this.inspectorCertCode = inspectorCertCode == null ? null : inspectorCertCode.trim();
	}
	
    /**
     * @return InspectorCertCode
     */	
	public String getInspectorCertCode() {
		return this.inspectorCertCode;
	}
	
	/**
	 * @param pawnOwner
	 */
	public void setPawnOwner(String pawnOwner) {
		this.pawnOwner = pawnOwner == null ? null : pawnOwner.trim();
	}
	
    /**
     * @return PawnOwner
     */	
	public String getPawnOwner() {
		return this.pawnOwner;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName == null ? null : buildingName.trim();
	}
	
    /**
     * @return BuildingName
     */	
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param building
	 */
	public void setBuilding(String building) {
		this.building = building == null ? null : building.trim();
	}
	
    /**
     * @return Building
     */	
	public String getBuilding() {
		return this.building;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return Squ
     */	
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr == null ? null : addr.trim();
	}
	
    /**
     * @return Addr
     */	
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param videoSerno
	 */
	public void setVideoSerno(String videoSerno) {
		this.videoSerno = videoSerno == null ? null : videoSerno.trim();
	}
	
    /**
     * @return VideoSerno
     */	
	public String getVideoSerno() {
		return this.videoSerno;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus == null ? null : inspectStatus.trim();
	}
	
    /**
     * @return InspectStatus
     */	
	public String getInspectStatus() {
		return this.inspectStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}