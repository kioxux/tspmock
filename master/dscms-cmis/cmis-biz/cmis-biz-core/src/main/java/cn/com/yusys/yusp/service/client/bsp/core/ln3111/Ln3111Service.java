package cn.com.yusys.yusp.service.client.bsp.core.ln3111;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111RespDto;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款归还试算
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3111Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;


    /**
     * 业务逻辑处理方法：贷款归还试算
     *
     * @param ln3111ReqDto
     * @return
     */
    @Transactional
    public Ln3111RespDto ln3111(Ln3111ReqDto ln3111ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value, JSON.toJSONString(ln3111ReqDto));
        ResultDto<Ln3111RespDto> ln3111ResultDto = dscms2CoreLnClientService.ln3111(ln3111ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value, JSON.toJSONString(ln3111ResultDto));
        String ln3111Code = Optional.ofNullable(ln3111ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3111Meesage = Optional.ofNullable(ln3111ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3111RespDto ln3111RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3111ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3111RespDto = ln3111ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(ln3111Code, ln3111Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value);
        return ln3111RespDto;
    }

    /**
     * 根据 [请求Data：提前还款 ]组装[请求Dto：贷款归还试算]
     *
     * @param xddh0006DataReqDto
     * @return
     */
    public Ln3111ReqDto buildLn3111ReqDto(Xddh0006DataReqDto xddh0006DataReqDto) {
        String billNo = xddh0006DataReqDto.getBillNo();//借据号
        String repayType = xddh0006DataReqDto.getRepayType();//还款模式
        BigDecimal advRepaymentAmt = xddh0006DataReqDto.getAdvRepaymentAmt();//提前还本金额
        Ln3111ReqDto ln3111ReqDto = GenericBuilder.of(Ln3111ReqDto::new)//请求DTO:贷款展期查询
                .with(Ln3111ReqDto::setDkzhangh, StringUtils.EMPTY)// 贷款账号
                .with(Ln3111ReqDto::setDkjiejuh, billNo)// 贷款借据号
                .with(Ln3111ReqDto::setHetongbh, StringUtils.EMPTY)// 合同编号
                .with(Ln3111ReqDto::setKehuhaoo, StringUtils.EMPTY)// 客户号
                .with(Ln3111ReqDto::setKehuzwmc, StringUtils.EMPTY)// 客户名
                .with(Ln3111ReqDto::setHuobdhao, StringUtils.EMPTY)// 货币代号
                .with(Ln3111ReqDto::setHuankriq, StringUtils.EMPTY)// 还款日期
                .with(Ln3111ReqDto::setZhchbjin, null)// 正常本金
                .with(Ln3111ReqDto::setYingjilx, null)// 应计利息
                .with(Ln3111ReqDto::setYuqibjin, null)// 逾期本金
                .with(Ln3111ReqDto::setDzhibjin, null)// 呆滞本金
                .with(Ln3111ReqDto::setDaizbjin, null)// 呆账本金
                .with(Ln3111ReqDto::setYsyjlixi, null)// 应收应计利息
                .with(Ln3111ReqDto::setCsyjlixi, null)// 催收应计利息
                .with(Ln3111ReqDto::setYsqianxi, null)// 应收欠息
                .with(Ln3111ReqDto::setCsqianxi, null)// 催收欠息
                .with(Ln3111ReqDto::setYsyjfaxi, null)// 应收应计罚息
                .with(Ln3111ReqDto::setCsyjfaxi, null)// 催收应计罚息
                .with(Ln3111ReqDto::setYshofaxi, null)// 应收罚息
                .with(Ln3111ReqDto::setCshofaxi, null)// 催收罚息
                .with(Ln3111ReqDto::setYingjifx, null)// 应计复息
                .with(Ln3111ReqDto::setFuxiiiii, null)// 复息
                .with(Ln3111ReqDto::setYingshfj, null)// 应收罚金
                .with(Ln3111ReqDto::setQiankzee, null)// 欠款总额
                .with(Ln3111ReqDto::setHuankzle, repayType)// 还款种类
                .with(Ln3111ReqDto::setDktqhkzl, repayType)//  提前还款种类类  1--指定本金2--指定总额3--全部结清4--只还息
                .with(Ln3111ReqDto::setHuankjee, advRepaymentAmt)// 还款金额
                .with(Ln3111ReqDto::setTqhkhxfs, StringUtils.EMPTY)// 提前还款还息方式
                .build();
        return ln3111ReqDto;
    }
}
