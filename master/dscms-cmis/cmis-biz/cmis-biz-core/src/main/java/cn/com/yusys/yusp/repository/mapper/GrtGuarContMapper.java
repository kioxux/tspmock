/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.GrtGuarContAndContNoDto;
import cn.com.yusys.yusp.dto.GrtGuarContClientDto;
import cn.com.yusys.yusp.dto.GrtGuarContRelDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.YpList;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-17 13:51:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GrtGuarContMapper {



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    GrtGuarCont selectByPrimaryKey(@Param("guarPkId") String guarPkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> selectByModel(QueryModel model);

    /**
     * @方法名称: selectGuarContByContNo
     * @方法描述: 根据合同编号查询担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> selectGuarContByContNo(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(GrtGuarCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(GrtGuarCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(GrtGuarCont record);

    /**
     * @方法名称: updateByGuarContNo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByGuarContNo(GrtGuarCont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(GrtGuarCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("guarPkId") String guarPkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByPrimaryKeyNum
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    int  selectByPrimaryKeyNum(@Param("guarPkId") String guarPkId);

    /**
     * @函数名称: getGrtGuarCont
     * @函数描述: 获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<GrtGuarContClientDto> getGrtGuarCont(GrtGuarContClientDto grtGuarContDto);

    /**
     * @方法名称: updateByGuarContNoKey
     * @方法描述: 根据担保合同编号更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int  updateByGuarContNoKey(GrtGuarCont grtGuarCont);
    /**
     * @方法名称: selectContNo
     * @方法描述: 根据担保合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    GrtGuarCont selectContNo(@Param("guarContNo") String guarContNo);
    /**
     * 通过入参信息查询数据是否存在
     * @param
     * @return
     */
    List<GrtGuarCont> selectDataByGuarNo(@Param("managerId") String loginCode);

    /**
     * 通过入参查询担保合同数据
     * @param queryMap
     * @return
     */
    List<GrtGuarCont> queryByParams(Map queryMap);

    /**
     * @param
     * @return java.util.List<cn.com.yusys.yusp.domain.GrtGuarCont>
     * @author shenli
     * @date 2021/4/22 0022 16:02
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<GrtGuarCont> selectDataByContNo(@Param("contNo") String contNo);

    /**
     * @param
     * @return java.util.List<cn.com.yusys.yusp.domain.GrtGuarCont>
     * @author shenli
     * @date 2021/4/22 0022 16:02
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<GrtGuarCont> selectDataBySerno(@Param("serno") String serno);

    /**
     * 信贷押品列表查询
     * @param contNo
     * @return
     */
    List<YpList> getContListByContNo(String contNo);


    /**
     * 根据合同类型查找担保合同表中信息
     * @param xdht0005DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0005.resp.List> getContInfoByContType(Xdht0005DataReqDto xdht0005DataReqDto);



    /**
     * 查询担保合同列表
     * @param ContNo
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0007.resp.List> getGrtContList(@Param("ContNo") String ContNo);

    /**
     * 查询是否有可用合同编号
     * @param contNo
     * @return
     */
    java.util.List<String> getGuarContNo(String contNo);

    /**
     * 根据条件查询（暂时通过权证号查询合同列表）
     * @param model
     * @return
     */
    List<GrtGuarCont>  queryBaseInfoByWarrantNo(QueryModel model);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据入参cusId查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> selectByCusId(String cusId);

    /**
     * 担保合同编号校验
     * @param guarContNo
     * @return
     */
    int  isExitGuarContNo(@Param("guarContNo")String guarContNo);

    /**
     * 根据押品编号查询担保合同信息
     * @param guarNo
     * @return
     */
    GrtGuarCont getGrtContInfoByGuarNo(@Param("guarNo")String guarNo);

     /**
     * @方法名称：queryLmtSubGuarCont
     * @方法描述：根据授信分项业务号获取担保合同信息
     * @创建人：zhangming12
     * @创建时间：2021/5/17 19:26
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<GrtGuarCont> queryLmtSubGuarCont(@Param("subSerno")String subSerno);

    List<GrtGuarCont> queryGuarContByContNo(@Param("contNo")String contNo);

    int updateSelectiveByGuarContNo(GrtGuarCont grtGuarCont);

    /**
     * @方法名称：insertGrtGuarContList
     * @方法描述： 批量新增
     * @创建人：xs
     * @创建时间：2021/5/25 19:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    int insertGrtGuarContList(@Param("list")List<GrtGuarCont> grtGuarContList);

    /**
     * @方法名称：deleteGrtGuarContRelLink
     * @方法描述：删除担保合同和抵押物的关系
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
     int deleteGrtGuarContRelLink(GrtGuarContRelDto grtGuarContRel);

    /**
     * @方法名称：updateGuarContStatus
     * @方法描述： 根据担保合同和业务的关系的合同编号更新担保合同状态
     * @创建人：xs
     * @创建时间：2021/06/01 21:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    int updateGuarContStatus(@Param("contNo")String contNo);

    /**
     * @方法名称：updateGuarIserchNoByGuarContNo
     * @方法描述： 根据合同号更新担保合同的担保双录编号字段
     * @创建人：gqh
     * @创建时间：2021/06/03 21:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    int updateGuarIserchNoByGuarContNo(@Param("contNo")String contNo, @Param("docNo")String docNo);

    /**
     * @方法名称：selectGuarContByBizRst
     * @方法描述： 根据担保合同和业务的关系的合同编号 查询担保合同信息
     * @创建人：xs
     * @创建时间：2021/06/012 21:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<GrtGuarCont> selectGuarContByBizRst(@Param("contNo")String contNo);

    /**
     * 根据担保合同号查询合同状态和签订时间
     *
     * @param contNo
     * @return
     */
    HashMap<String, Object> selectContStatuByContNo(@Param("contNo") String contNo);

    /**
     * 根据合同号更新担保合同签约状态
     * @param queryMap
     * @return
     */
    int updateContStatusByContNo(Map queryMap);

    /**
     * @方法名称：maxContCancelImport
     * @方法描述：最高额担保合同取消引入
     * @创建人：zfq
     * @创建时间：2021/6/8 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    int maxContCancelImport(GrtGuarBizRstRel grtGuarBizRstRel);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

	/**
     * @方法名称：selectGuarContByCnNo
     * 根据中文担保合同号查询担保合同编号
     * @param guarContCnNo
     * @return
     */
    String selectGuarContByCnNo(String guarContCnNo);

    /**
     * @函数名称:queryGrtGuarContByContNo
     * @函数描述:根据合同编号查询担保信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人： 周茂伟
     */
    List<GrtGuarContClientDto> queryGrtGuarContByContNo(List<String> contNoList);

    /**
     * @函数名称:queryGrtGuarContByContNo
     * @函数描述:根据合同编号查询担保信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人： 周茂伟
     */
    List<GrtGuarContClientDto> getGrtGuarContByContNo(List<String> contNoList);
	/**
     * 通过担保合同号查询担保合同信息
     * @param guarContNo 担保合同号
     * @return 担保合同信息
     */
    GrtGuarCont queryGrtGuarContByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * @函数名称:queryExistsWzx
     * @函数描述:根据参数权证编号，查询已入库的押品所关联的生效或者未生效的合同状态的担保合同是否存在(未注销)
     * @参数与返回说明:
     * @算法描述:
     */
    List<GrtGuarCont>  queryExistsWzx(QueryModel queryModel);

    /**
     * @方法名称: queryGrtGuarContByGuarNo
     * @方法描述: 根据押品统一编号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> queryGrtGuarContByGuarNo(QueryModel model);

    /**
     * @方法名称: queryGrtGuarContByCusId
     * @方法描述: 根据客户编号获取为他人提供的担保
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarContAndContNoDto> queryGrtGuarContByCusId(QueryModel model);

    /**
     * @方法名称: queryGrtGuarContByBorrowerCusId
     * @方法描述: 根据客户编号获取他人为其提供的担保
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> queryGrtGuarContByBorrowerCusId(QueryModel model);

    /**
     * @方法名称: selectByGuarPkIds
     * @方法描述:
     * @参数与返回说明: 根据担保合同流水号获取生效担保合同
     * @算法描述: 无
     */
    List<GrtGuarCont> selectByGuarPkIds(@Param("list") List<String> guarPkIds);

    /**
     * @方法名称: selectEffectByGuarPkIds
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> selectEffectByGuarPkIds(@Param("list") List<String> guarPkIds);

    /**
     * 根据担保合同编号查询对应的担保合同状态
     * @param guarContNo
     * @return
     */
    String selectGuarContStateByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据主合同编号查询关联的担保合同数据
     * @param contNo
     * @return
     */
    GrtGuarCont queryGrtGuarContDataByGuarPkId(@Param("contNo") String contNo);

    /**
     * @方法名称: selectByGuarContNos
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarCont> selectByGuarContNos(@Param("list") List<String> guarContNos);

    List<GrtGuarContClientDto> queryGrtGuarContByContNohtdy(List<String> contNoList);

    /**
     * 根据YP编号查询担保合同信息
     * @param coreGuarantyNo
     * @return
     */
    GrtGuarCont getGrtContInfoByGrpNo(@Param("coreGuarantyNo")String coreGuarantyNo);

}