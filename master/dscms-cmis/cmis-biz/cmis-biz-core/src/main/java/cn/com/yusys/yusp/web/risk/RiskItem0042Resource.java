package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.LmtAppSubService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: riskItem0042Resource
 * @类描述: 授信担保信息校验
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-07-13 14:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0042授信担保信息校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0042")
public class RiskItem0042Resource {

    @Autowired
    private LmtAppSubService lmtAppSubService;

    /**
     * @方法名称: riskItem0042
     * @方法描述: 授信担保信息校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "授信担保信息校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0042(@RequestBody QueryModel queryModel) {
        return ResultDto.success(lmtAppSubService.riskItem0042(queryModel.getCondition().get("bizId").toString()));
    }
}