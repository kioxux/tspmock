package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CtrContImageAuditApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CtrContImageAuditAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @创建人 WH
 * @创建时间 2021-05-07
 * @return WfctrContImageAuditAppApplybiz
 **/
@Service  //将实现类注入spring容器管理
public class WfCtrLoanContApplybiz implements ClientBizInterface {
    private final Logger logger = LoggerFactory.getLogger(WfCtrLoanContApplybiz.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String grtSerno = resultInstanceDto.getBizId();
        logger.info("后业务处理类型{}", currentOpType);
        CtrContImageAuditApp ctrContImageAuditApp = null;
        try {
            ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(grtSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程发起操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程提交操作，流程参数【{}】", grtSerno, resultInstanceDto);
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程跳转操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程结束操作，流程参数【{}】", grtSerno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程退回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程打回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程拿回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("线上提款启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else {
                logger.warn("担保变更申请" + grtSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "WF_CTR_LOAN_CONT_IMAGE".equals(flowCode);
    }

}
