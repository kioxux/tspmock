/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ModelApprResultInfo;
import cn.com.yusys.yusp.service.ModelApprResultInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ModelApprResultInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-14 15:41:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/modelapprresultinfo")
public class ModelApprResultInfoResource {
    @Autowired
    private ModelApprResultInfoService modelApprResultInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ModelApprResultInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ModelApprResultInfo> list = modelApprResultInfoService.selectAll(queryModel);
        return new ResultDto<List<ModelApprResultInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ModelApprResultInfo>> index(QueryModel queryModel) {
        List<ModelApprResultInfo> list = modelApprResultInfoService.selectByModel(queryModel);
        return new ResultDto<List<ModelApprResultInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{surveyNo}")
    protected ResultDto<ModelApprResultInfo> show(@PathVariable("surveyNo") String surveyNo) {
        ModelApprResultInfo modelApprResultInfo = modelApprResultInfoService.selectByPrimaryKey(surveyNo);
        return new ResultDto<ModelApprResultInfo>(modelApprResultInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ModelApprResultInfo> create(@RequestBody ModelApprResultInfo modelApprResultInfo) throws URISyntaxException {
        modelApprResultInfoService.insert(modelApprResultInfo);
        return new ResultDto<ModelApprResultInfo>(modelApprResultInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ModelApprResultInfo modelApprResultInfo) throws URISyntaxException {
        int result = modelApprResultInfoService.update(modelApprResultInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{surveyNo}")
    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
        int result = modelApprResultInfoService.deleteByPrimaryKey(surveyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = modelApprResultInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
