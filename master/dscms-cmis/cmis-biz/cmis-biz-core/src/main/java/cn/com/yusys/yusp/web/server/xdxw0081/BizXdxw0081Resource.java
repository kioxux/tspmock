package cn.com.yusys.yusp.web.server.xdxw0081;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0081.req.Xdxw0081DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0081.resp.Xdxw0081DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0081.Xdxw0081Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

/**
 * 接口处理类:检查是否有优农贷、优企贷、惠享贷合同
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDXW0081:检查是否有优农贷、优企贷、惠享贷合同")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0081Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0081Resource.class);

    @Autowired
    private Xdxw0081Service xdxw0081Service;

    /**
     * 交易码：xdxw0081
     * 交易描述：检查是否有优农贷、优企贷、惠享贷合同
     *
     * @param xdxw0081DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("检查是否有优农贷、优企贷、惠享贷合同")
    @PostMapping("/xdxw0081")
    protected @ResponseBody
    ResultDto<Xdxw0081DataRespDto> xdxw0081(@Validated @RequestBody Xdxw0081DataReqDto xdxw0081DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataReqDto));
        Xdxw0081DataRespDto xdxw0081DataRespDto = new Xdxw0081DataRespDto();// 响应Dto:勘验任务状态同步
        ResultDto<Xdxw0081DataRespDto> xdxw0081DataResultDto = new ResultDto<>();
        try {
            /*//获取请求参数
            String type = xdxw0081DataReqDto.getType();//操作类型
            String certCode = xdxw0081DataReqDto.getCertCode();//证件号码
            List list = xdxw0081DataReqDto.getList();

            if(StringUtil.isEmpty(type)){
                xdxw0081DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0081DataResultDto.setMessage("操作类型【type】不能为空！");
                return xdxw0081DataResultDto;
            }else if(("0".equals(type)||"1".equals(type))){//0:校验白名单查询
                if(StringUtil.isEmpty(certCode)){
                    xdxw0081DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdxw0081DataResultDto.setMessage("证件号码【certCode】不能为空！");
                    return xdxw0081DataResultDto;
                }
            }else if("2".equals(type)){
                if(CollectionUtils.isEmpty(list)){
                    xdxw0081DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdxw0081DataResultDto.setMessage("参数【list】不能为空！");
                    return xdxw0081DataResultDto;
                }
            }*/

            // 从xdxw0081DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataReqDto));
            xdxw0081DataRespDto = xdxw0081Service.xdxw0081(xdxw0081DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataRespDto));
            // 封装xdxw0081DataResultDto中正确的返回码和返回信息
            xdxw0081DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0081DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, e.getMessage());
            // 封装xdxw0081DataResultDto中异常返回码和返回信息
            xdxw0081DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0081DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0081DataRespDto到xdxw0081DataResultDto中
        xdxw0081DataResultDto.setData(xdxw0081DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataResultDto));
        return xdxw0081DataResultDto;
    }
}
