package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0076Service
 * @类描述: 出账放款汽车抵押贷款校验
 * @功能描述: 出账放款汽车抵押贷款校验
 * @创建人: hubp
 * @创建时间: 2021年8月11日08:38:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0076Service {

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * @方法名称: riskItem0076
     * @方法描述: 出账放款汽车抵押贷款校验
     * @参数与返回说明:
     * @算法描述:
     *  贷款品种：“个人汽车按揭贷款”------贷款担保当时为“抵押”的情况下。借款用途必须为“购车”，贷款类别为“消费——汽车贷款”，担保方式细分为“抵押-其他”
     * @创建人: hubp
     * @创建时间: 2021年8月11日08:38:26
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0076(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = queryModel.getCondition().get("bizId").toString();
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        if (Objects.isNull(pvpLoanApp) || StringUtils.isBlank(pvpLoanApp.getGuarMode()) || StringUtils.isBlank(pvpLoanApp.getPrdId())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001405);
            return riskResultDto;
        } else {
            if ("CM010001".equals(pvpLoanApp.getPrdId()) && "10".equals(pvpLoanApp.getGuarMode())) {
                if (StringUtils.isBlank(pvpLoanApp.getLoanUseType()) || StringUtils.isBlank(pvpLoanApp.getLoanTypeDetail()) || StringUtils.isBlank(pvpLoanApp.getGuarDetailMode())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07502);
                    return riskResultDto;
                } else {
                    // “个人汽车按揭贷款”------贷款担保当时为“抵押”的情况下
                    if (!"C03".equals(pvpLoanApp.getLoanUseType()) || !"02".equals(pvpLoanApp.getLoanTypeDetail()) || !"08".equals(pvpLoanApp.getGuarDetailMode())) {
                        //借款用途必须为“购车”，贷款类别为“消费——汽车贷款”，担保方式细分为“抵押-其他”
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07501);
                        return riskResultDto;
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
