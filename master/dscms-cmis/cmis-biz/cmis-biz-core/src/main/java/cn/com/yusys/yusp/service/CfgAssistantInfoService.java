/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgAssistantInfo;
import cn.com.yusys.yusp.repository.mapper.CfgAssistantInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgAssistantInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-28 15:47:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgAssistantInfoService {

    @Autowired
    private CfgAssistantInfoMapper cfgAssistantInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgAssistantInfo selectByPrimaryKey(String serno) {
        return cfgAssistantInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgAssistantInfo> selectAll(QueryModel model) {
        List<CfgAssistantInfo> records = (List<CfgAssistantInfo>) cfgAssistantInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgAssistantInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgAssistantInfo> list = cfgAssistantInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgAssistantInfo record) {
        //避免重复添加
        String prdCode = record.getPrdCode();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("prdCode",prdCode);
        queryModel.addCondition("PRD_NAME",record.getPrdName());
        if(record.getPrdTypeProp()!=null && !"".equals(record.getPrdTypeProp())){
            queryModel.addCondition("prdTypeProp",record.getPrdTypeProp());
            List<CfgAssistantInfo>  listCfgAssistantInfo = cfgAssistantInfoMapper.selectByModel(queryModel);
            if(listCfgAssistantInfo!=null && listCfgAssistantInfo.size() > 0 ){
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key,"\"请勿重复新增\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        }else{
            List<CfgAssistantInfo> list = cfgAssistantInfoMapper.checkNoticeByPrdIdAndBizTypeNonPrdTypeProp(prdCode);
            if(list!=null && list.size() > 0 ){
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key,"\"请勿重复新增\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        }

        return cfgAssistantInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgAssistantInfo record) {
        return cfgAssistantInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgAssistantInfo record) {
        return cfgAssistantInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgAssistantInfo record) {
        return cfgAssistantInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cfgAssistantInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgAssistantInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: getNoticeByPrdIdAndBizType
     * @方法描述: 根据产品编号、业务阶段，查询业务小助手的注意事项
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, Object>> getNoticeByPrdIdAndBizType(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cfgAssistantInfoMapper.getNoticeByPrdIdAndBizType(model);
        PageHelper.clearPage();
        return list;
    }

    public List<Map<String, Object>> getNoticeByPrdIdAndBizTypeNonPrdTypeProp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cfgAssistantInfoMapper.getNoticeByPrdIdAndBizTypeNonPrdTypeProp(model);
        PageHelper.clearPage();
        return list;
    }
}
