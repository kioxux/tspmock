/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalInEvalInfo
 * @类描述: guar_eval_in_eval_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-29 09:51:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_eval_in_eval_info")
public class GuarEvalInEvalInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 内评流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IN_EVAL_SERNO")
	private String inEvalSerno;
	
	/** 申请流水号 **/
	@Column(name = "EVAL_APPLY_SERNO", unique = false, nullable = false, length = 40)
	private String evalApplySerno;
	
	/** 评估方法 **/
	@Column(name = "EVAL_ENNAME", unique = false, nullable = false, length = 40)
	private String evalEnname;
	
	/** 内部评估价值 **/
	@Column(name = "EVAL_IN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalInAmt;
	
	/** 内部评估价值币种 **/
	@Column(name = "EVAL_IN_CURRENCY", unique = false, nullable = true, length = 10)
	private String evalInCurrency;
	
	/** 内部评估价值时间 **/
	@Column(name = "EVAL_IN_DATE", unique = false, nullable = true, length = 20)
	private String evalInDate;
	
	/** 评估方法选择理由 **/
	@Column(name = "EVAL_TYPE_RESN", unique = false, nullable = true, length = 750)
	private String evalTypeResn;
	
	/** 评估类型 **/
	@Column(name = "EVAL_TYPE", unique = false, nullable = true, length = 2)
	private String evalType;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	
	/**
	 * @param inEvalSerno
	 */
	public void setInEvalSerno(String inEvalSerno) {
		this.inEvalSerno = inEvalSerno;
	}
	
    /**
     * @return inEvalSerno
     */
	public String getInEvalSerno() {
		return this.inEvalSerno;
	}
	
	/**
	 * @param evalApplySerno
	 */
	public void setEvalApplySerno(String evalApplySerno) {
		this.evalApplySerno = evalApplySerno;
	}
	
    /**
     * @return evalApplySerno
     */
	public String getEvalApplySerno() {
		return this.evalApplySerno;
	}
	
	/**
	 * @param evalEnname
	 */
	public void setEvalEnname(String evalEnname) {
		this.evalEnname = evalEnname;
	}
	
    /**
     * @return evalEnname
     */
	public String getEvalEnname() {
		return this.evalEnname;
	}
	
	/**
	 * @param evalInAmt
	 */
	public void setEvalInAmt(java.math.BigDecimal evalInAmt) {
		this.evalInAmt = evalInAmt;
	}
	
    /**
     * @return evalInAmt
     */
	public java.math.BigDecimal getEvalInAmt() {
		return this.evalInAmt;
	}
	
	/**
	 * @param evalInCurrency
	 */
	public void setEvalInCurrency(String evalInCurrency) {
		this.evalInCurrency = evalInCurrency;
	}
	
    /**
     * @return evalInCurrency
     */
	public String getEvalInCurrency() {
		return this.evalInCurrency;
	}
	
	/**
	 * @param evalInDate
	 */
	public void setEvalInDate(String evalInDate) {
		this.evalInDate = evalInDate;
	}
	
    /**
     * @return evalInDate
     */
	public String getEvalInDate() {
		return this.evalInDate;
	}
	
	/**
	 * @param evalTypeResn
	 */
	public void setEvalTypeResn(String evalTypeResn) {
		this.evalTypeResn = evalTypeResn;
	}
	
    /**
     * @return evalTypeResn
     */
	public String getEvalTypeResn() {
		return this.evalTypeResn;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType;
	}
	
    /**
     * @return evalType
     */
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}