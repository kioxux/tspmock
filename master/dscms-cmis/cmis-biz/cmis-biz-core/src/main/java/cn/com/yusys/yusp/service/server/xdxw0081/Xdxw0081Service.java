package cn.com.yusys.yusp.service.server.xdxw0081;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtRenewLoanAppInfo;
import cn.com.yusys.yusp.dto.server.xdxw0081.req.Xdxw0081DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0081.resp.Xdxw0081DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtRenewLoanAppInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 接口处理类:检查是否有优农贷、优企贷、惠享贷合同
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdxw0081Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0081Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 检查是否有优农贷、优企贷、惠享贷合同
     *
     * @param xdxw0081DataReqDto
     * @return
     */
    public Xdxw0081DataRespDto xdxw0081(Xdxw0081DataReqDto xdxw0081DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataReqDto));
        Xdxw0081DataRespDto xdxw0081DataRespDto = new Xdxw0081DataRespDto();
        try {
            //获取请求参数
            String cert_code = xdxw0081DataReqDto.getCert_code();//身份证号
            String if_hxd = "" + ctrLoanContMapper.checkByPrdIdAndCertCode(cert_code,"SC060001");
            String if_ynd = "" + ctrLoanContMapper.checkByPrdIdAndCertCode(cert_code,"SC020010");
            String if_yqd = "" + ctrLoanContMapper.checkByPrdIdAndCertCode(cert_code,"SC010008");
            xdxw0081DataRespDto.setIf_hxd(if_hxd);
            xdxw0081DataRespDto.setIf_ynd(if_ynd);
            xdxw0081DataRespDto.setIf_yqd(if_yqd);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0081.key, DscmsEnum.TRADE_CODE_XDXW0081.value, JSON.toJSONString(xdxw0081DataRespDto));
        return xdxw0081DataRespDto;
    }
}
