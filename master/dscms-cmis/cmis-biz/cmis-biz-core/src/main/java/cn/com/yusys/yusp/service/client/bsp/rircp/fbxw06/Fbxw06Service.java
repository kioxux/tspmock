package cn.com.yusys.yusp.service.client.bsp.rircp.fbxw06;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/20 9:08
 * @desc    参考利率测算接口
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Fbxw06Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw06Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * 业务逻辑处理方法：参考利率测算接口
     *
     * @param fbxw06ReqDto
     * @return
     */
    @Transactional
    public Fbxw06RespDto fbxw06(Fbxw06ReqDto fbxw06ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value, JSON.toJSONString(fbxw06ReqDto));
        ResultDto<Fbxw06RespDto> fbxw06ResultDto = dscms2RircpClientService.fbxw06(fbxw06ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value, JSON.toJSONString(fbxw06ResultDto));
        String FBXW06Code = Optional.ofNullable(fbxw06ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String FBXW06Meesage = Optional.ofNullable(fbxw06ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fbxw06RespDto fbxw06RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fbxw06ResultDto.getCode())) {
            //  获取相关的值并解析
            fbxw06RespDto = fbxw06ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(FBXW06Code, FBXW06Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value);
        return fbxw06RespDto;
    }


}
