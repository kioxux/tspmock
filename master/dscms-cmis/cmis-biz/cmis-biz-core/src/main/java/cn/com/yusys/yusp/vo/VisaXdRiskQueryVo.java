package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: VisaXdRisk
 * @类描述: visa_xd_risk数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:42:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class VisaXdRiskQueryVo extends PageQuery{
	
	/** 流水号 **/
	private String serno;
	
	/** 客户信息流水号 **/
	private String crpSerno;
	
	/** 客户号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 客户手机号码 **/
	private String mobileNo;
	
	/** 管户经理编号 **/
	private String managerId;
	
	/** 管户经理名称 **/
	private String managerName;
	
	/** 面签时间 **/
	private String signatureTime;
	
	/** 面签地址 **/
	private String signatureAddr;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param crpSerno
	 */
	public void setCrpSerno(String crpSerno) {
		this.crpSerno = crpSerno == null ? null : crpSerno.trim();
	}
	
    /**
     * @return CrpSerno
     */	
	public String getCrpSerno() {
		return this.crpSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo == null ? null : mobileNo.trim();
	}
	
    /**
     * @return MobileNo
     */	
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param signatureTime
	 */
	public void setSignatureTime(String signatureTime) {
		this.signatureTime = signatureTime == null ? null : signatureTime.trim();
	}
	
    /**
     * @return SignatureTime
     */	
	public String getSignatureTime() {
		return this.signatureTime;
	}
	
	/**
	 * @param signatureAddr
	 */
	public void setSignatureAddr(String signatureAddr) {
		this.signatureAddr = signatureAddr == null ? null : signatureAddr.trim();
	}
	
    /**
     * @return SignatureAddr
     */	
	public String getSignatureAddr() {
		return this.signatureAddr;
	}


}