/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.CtrDiscContService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrDiscContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-13 09:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "贴现协议")
@RequestMapping("/api/ctrdisccont")
public class CtrDiscContResource {
    @Autowired
    private CtrDiscContService ctrDiscContService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrDiscCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrDiscCont> list = ctrDiscContService.selectAll(queryModel);
        return new ResultDto<List<CtrDiscCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrDiscCont>> index(QueryModel queryModel) {
        List<CtrDiscCont> list = ctrDiscContService.selectByModel(queryModel);
        return new ResultDto<List<CtrDiscCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrDiscCont> show(@PathVariable("pkId") String pkId) {
        CtrDiscCont ctrDiscCont = ctrDiscContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrDiscCont>(ctrDiscCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrDiscCont> create(@RequestBody CtrDiscCont ctrDiscCont) throws URISyntaxException {
        ctrDiscContService.insert(ctrDiscCont);
        return new ResultDto<CtrDiscCont>(ctrDiscCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrDiscCont ctrDiscCont) throws URISyntaxException {
        int result = ctrDiscContService.update(ctrDiscCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrDiscContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrDiscContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:onSign
     * @函数描述:贴现协议签订
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贴现协议签订")
    @PostMapping("/onsign")
    protected ResultDto<Integer> onSign(@RequestBody CtrDiscCont ctrDiscCont) throws URISyntaxException {
        int result = ctrDiscContService.onSign(ctrDiscCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:待签定列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrDiscCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrDiscCont> list = ctrDiscContService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrDiscCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrDiscCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrDiscCont> list = ctrDiscContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrDiscCont>>(list);
    }

    /**
     * @函数名称:onLogOut
     * @函数描述:普通贷款合同注销
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onlogout")
    protected ResultDto<Map> onLogOut(@RequestBody Map params) throws Exception {
        Map rtnData= ctrDiscContService.onLogOut(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:contReSign
     * @函数描述:贴现协议合同重签
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/contReSign")
    protected ResultDto<Map> contReSign(@RequestBody Map params) throws Exception {
        Map rtnData= ctrDiscContService.contReSign(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrDiscCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrDiscCont> list = ctrDiscContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrDiscCont>>(list);
    }

    /**
     * @函数名称:queryCtrDiscContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrdisccontdatabyserno")
    protected ResultDto<CtrDiscCont> queryCtrDiscContDataBySerno(@RequestBody String serno) {
        CtrDiscCont ctrDiscCont = ctrDiscContService.selectByIqpSerno(serno);
        return new ResultDto<CtrDiscCont>(ctrDiscCont);
    }

    /**
     * @函数名称:queryCtrDiscContDataByContNo
     * @函数描述:根据合同号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据合同号查询合同信息")
    @PostMapping("/queryctrdisccontdatabycontno")
    protected ResultDto<CtrDiscCont> queryCtrDiscContDataByContNo(@RequestBody String contNo) {
        CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(contNo);
        return new ResultDto<CtrDiscCont>(ctrDiscCont);
    }
}
