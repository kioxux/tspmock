package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpTruPayInfo
 * @类描述: pvp_tru_pay_info数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-08 20:46:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpTruPayInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 交易流水号 **/
	private String tranSerno;
	
	/** 放款流水号 **/
	private String pvpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 未受托支付类型 **/
	private String untruPayType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 账号 **/
	private String acctNo;
	
	/** 账号名称 **/
	private String acctName;
	
	/** 币种  STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 交易金额 **/
	private java.math.BigDecimal tranAmt;
	
	/** 交易日期 **/
	private String tranDate;
	
	/** 返回编码 **/
	private String returnCode;
	
	/** 返回说明 **/
	private String returnDesc;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 放款机构 **/
	private String acctBrId;
	
	/** 支付状态 STD_ZB_PAY_ST **/
	private String payStatus;
	
	/** 是否退汇标识 STD_ZB_YES_NO **/
	private String isReturnFlag;
	
	/** 退汇原因 **/
	private String rruResn;
	
	
	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno == null ? null : tranSerno.trim();
	}
	
    /**
     * @return TranSerno
     */	
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param untruPayType
	 */
	public void setUntruPayType(String untruPayType) {
		this.untruPayType = untruPayType == null ? null : untruPayType.trim();
	}
	
    /**
     * @return UntruPayType
     */	
	public String getUntruPayType() {
		return this.untruPayType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}
	
    /**
     * @return AcctName
     */	
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return TranAmt
     */	
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate == null ? null : tranDate.trim();
	}
	
    /**
     * @return TranDate
     */	
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode == null ? null : returnCode.trim();
	}
	
    /**
     * @return ReturnCode
     */	
	public String getReturnCode() {
		return this.returnCode;
	}
	
	/**
	 * @param returnDesc
	 */
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc == null ? null : returnDesc.trim();
	}
	
    /**
     * @return ReturnDesc
     */	
	public String getReturnDesc() {
		return this.returnDesc;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param acctBrId
	 */
	public void setAcctBrId(String acctBrId) {
		this.acctBrId = acctBrId == null ? null : acctBrId.trim();
	}
	
    /**
     * @return AcctBrId
     */	
	public String getAcctBrId() {
		return this.acctBrId;
	}
	
	/**
	 * @param payStatus
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus == null ? null : payStatus.trim();
	}
	
    /**
     * @return PayStatus
     */	
	public String getPayStatus() {
		return this.payStatus;
	}
	
	/**
	 * @param isReturnFlag
	 */
	public void setIsReturnFlag(String isReturnFlag) {
		this.isReturnFlag = isReturnFlag == null ? null : isReturnFlag.trim();
	}
	
    /**
     * @return IsReturnFlag
     */	
	public String getIsReturnFlag() {
		return this.isReturnFlag;
	}
	
	/**
	 * @param rruResn
	 */
	public void setRruResn(String rruResn) {
		this.rruResn = rruResn == null ? null : rruResn.trim();
	}
	
    /**
     * @return RruResn
     */	
	public String getRruResn() {
		return this.rruResn;
	}


}