/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.DataModify;
import cn.com.yusys.yusp.repository.mapper.DataModifyMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DataModifyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 17:25:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DataModifyService {

    @Autowired
    private DataModifyMapper dataModifyMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public DataModify selectByPrimaryKey(String serno) {
        return dataModifyMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据iqpprepayment
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<DataModify> selectAll(QueryModel model) {
        List<DataModify> records = (List<DataModify>) dataModifyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<DataModify> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DataModify> list = dataModifyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(DataModify record) {
        return dataModifyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(DataModify dataModify) {
//        QueryModel model1 = new QueryModel();
//        model1.addCondition("billNo", dataModify.getBillNo());
//        model1.addCondition("apply","Y");
//        List<DataModify> list1 = this.selectByModel(model1);
//        if(list1.size()>0){
//            throw BizException.error(null, "999999","\"该借据存在在途数据修改申请，请勿重复发起！\"" + "保存失败！");
//        }
        dataModify.setApproveStatus("000");
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        dataModify.setInputId(userInfo.getLoginCode()); // 当前用户号
        dataModify.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
        dataModify.setInputDate(DateUtils.getCurrDateStr());
        dataModify.setCreateTime(DateUtils.getCurrTimestamp());
        dataModify.setUpdateTime(dataModify.getCreateTime());
        dataModify.setUpdDate(dataModify.getInputDate());
        dataModify.setUpdId(dataModify.getInputId());
        dataModify.setUpdBrId(dataModify.getInputBrId());
        return dataModifyMapper.insertSelective(dataModify);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(DataModify record) {
        return dataModifyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(DataModify record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return dataModifyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return dataModifyMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return dataModifyMapper.deleteByIds(ids);
    }
}
