package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.req.CmisLmt0062ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.resp.CmisLmt0062RespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppMapper;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 大额风险暴露超限校验
 */
@Service
public class RiskItem0126Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0126Service.class);

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private BizInvestCommonService bizInvestCommonService;

    @Autowired
    private LmtAppMapper lmtAppMapper;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * 查询业务申请的客户【最新一天】指：
     * data_dt最大 的DM_RIS_KHFX_JGBX_JK【大额风险暴露-客户维度风险暴露汇总】 的数据   及   当天指标最新的阈值设置；
     * 拿本次业务申请金额 + 客户的大额风险暴露值（根据客户类型代码获取对应的风险暴露）  判断是否超过   对应指标类型的阈值设置，
     * 若超过监管限额， 则获取系统配置【是否强控突破监管限额】为是，则拦截业务；否则提示“客户风险暴露超过监管限额，无法通过。”
     * 若超过黄区阈值、红区阈值  则提示：“客户大额风险暴露计算值为[]，黄区指标为[]，红区指标为[]，超红区/黄区指标”；
     * <p>
     * PS:
     * 业务申请包括：授信申请、用信申请;
     * 若客户最新一天的大额风险暴露数据不存在，则类型默认为 01-非同业单一客户   指标默认为  02-非同业单一客户风险暴露占比；
     * 获取系统配置【是否强控突破监管限额】：getSysParameterByName("DE_TPJGXE"); -- 大额-突破监管限额；
     *
     * @param queryModel
     * @return
     */
    public RiskResultDto riskItem0126(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("【riskItem0126】大额风险暴露超限校验===>1 *******************业务流水号：【{}】 bizType【{}】", serno, bizType);

        try {
            //同业客户授信
            if (CmisBizConstants.STD_ZB_ZJTY_TY004.equals(bizType) || CmisBizConstants.STD_ZB_ZJTY_TY005.equals(bizType)
                    || CmisBizConstants.STD_ZB_ZJTY_TY006.equals(bizType)) {
                log.info("【riskItem0126】大额风险暴露超限校验===>2 *******************");
                LmtIntbankApp lmtIntbankApp = lmtIntbankAppService.selectBySerno(serno);
                if (lmtIntbankApp == null) {
                    log.info("【riskItem0126】大额风险暴露超限校验===>3 *******************");
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                    return riskResultDto;
                }
                BigDecimal lmtAmt = lmtIntbankApp.getLmtAmt();
                riskResultDto = execute(lmtIntbankApp.getCusId(), lmtAmt, "1", "1");
            }
            //单一客户授信（集团成员授信）
            if (CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType)) {
                log.info("【riskItem0126】大额风险暴露超限校验===>4 *******************");
                LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
                if (lmtApp == null) {
                    log.info("【riskItem0126】大额风险暴露超限校验===>5 *******************");
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                    return riskResultDto;
                }
                //敞口额度合计
                BigDecimal openTotalLmtAmt = lmtApp.getOpenTotalLmtAmt();
                riskResultDto = execute(lmtApp.getCusId(), openTotalLmtAmt, "1", "0");
            }
            //集团授信
            if (CmisFlowConstants.FLOW_TYPE_TYPE_SX008_TO_SX014.indexOf(bizType + ",") != -1) {
                log.info("【riskItem0126】大额风险暴露超限校验===>6 *******************");
                LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
                if (lmtGrpApp == null) {
                    log.info("【riskItem0126】大额风险暴露超限校验===>7 *******************");
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                    return riskResultDto;
                }
                //敞口额度合计
                BigDecimal openTotalLmtAmt = lmtGrpApp.getOpenTotalLmtAmt();
                riskResultDto = execute(lmtGrpApp.getGrpCusId(), openTotalLmtAmt, "1", "0");
            }
            // 零售放款申请（生成打印模式） 零售放款申请（空白合同模式）
            boolean lsfk = CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType)
                    || "SGE04".equals(bizType) || "DHE04".equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType);
            //对公贷款出账申请 小微放款申请 零售放款申请
            if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType) || lsfk) {
                log.info("【riskItem0126】大额风险暴露超限校验===>8 *******************");
                PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
                if (pvpLoanApp == null) {
                    log.info("【riskItem0126】大额风险暴露超限校验===>9 *******************");
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                    return riskResultDto;
                }
                BigDecimal contAmt = pvpLoanApp.getContAmt();
                riskResultDto = execute(pvpLoanApp.getCusId(), contAmt, "0", "0");
            }
            // 银承出账申请
            if (CmisFlowConstants.FLOW_TYPE_TYPE_YX012.equals(bizType)) {
                log.info("【riskItem0126】大额风险暴露超限校验===>10 *******************");
                PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
                if (pvpAccpApp == null) {
                    log.info("【riskItem0126】大额风险暴露超限校验===>11 *******************");
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                    return riskResultDto;
                }
                //敞口额度合计
                BigDecimal contAmt = pvpAccpApp.getContAmt();
                riskResultDto = execute(pvpAccpApp.getCusId(), contAmt, "0", "0");
            }
        } catch (Exception e) {
            log.info("【riskItem0126】大额风险暴露超限校验===>error *******************",e);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("大额风险暴露超限校验失败！");
        }
        return riskResultDto;
    }


    public RiskResultDto execute(String cusId, BigDecimal lmtAmt, String belongSx, String belongTysx) {
        log.info("【riskItem0126】大额风险暴露超限校验===>12 ******************* cusId【{}】 lmtAmt【{}】 belongSx【{}】 belongTysx【{}】"
                , cusId, lmtAmt, belongSx, belongTysx);
        RiskResultDto riskResultDto = new RiskResultDto();
        //是否强控突破监管限额
        String de_tpjgxe = bizInvestCommonService.getSysParameterByName("DE_TPJGXE");
        log.info("【riskItem0126】大额风险暴露超限校验===>12 ******************* de_tpjgxe【{}】", de_tpjgxe);
        CmisLmt0062ReqDto cmisLmt0062ReqDto = new CmisLmt0062ReqDto();
        cmisLmt0062ReqDto.setCustId(cusId);
        cmisLmt0062ReqDto.setBelongSx(belongSx);
        cmisLmt0062ReqDto.setBelongTysx(belongTysx);

        log.info("【riskItem0126】大额风险暴露超限校验===>13 ******************* cmisLmt0062ReqDto【{}】", cmisLmt0062ReqDto);
        ResultDto<CmisLmt0062RespDto> cmisLmt0062RespDtoResultDto = cmisLmtClientService.cmislmt0062(cmisLmt0062ReqDto);
        log.info("【riskItem0126】大额风险暴露超限校验===>14 ******************* cmisLmt0062ReqDtoResultDto【{}】", cmisLmt0062RespDtoResultDto);

        if (EpbEnum.EPB099999.key.equals(cmisLmt0062RespDtoResultDto.getCode())) {
            log.info("【riskItem0126】大额风险暴露超限校验===>15 *******************");
            //授信数据获取失败 RISK_ERROR_0005
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(cmisLmt0062RespDtoResultDto.getMessage());
            return riskResultDto;
        }

        CmisLmt0062RespDto data = cmisLmt0062RespDtoResultDto.getData();
        if (EpbEnum.EPB090004.key.equals(data.getErrorCode())) {
            log.info("【riskItem0126】大额风险暴露超限校验===>16 *******************");
            //授信数据获取失败 RISK_ERROR_0005
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(data.getErrorMsg());
            return riskResultDto;
        }

        //本次业务申请金额 + 客户的大额风险暴露值（根据客户类型代码获取对应的风险暴露）  判断是否超过   对应指标类型的阈值设置
        BigDecimal deRiskExpoAmt = bizInvestCommonService.changeValueToBigDecimal(data.getDeRiskExpoAmt().add(lmtAmt));
        //红区阈值
        BigDecimal riskRedReqAmt = bizInvestCommonService.changeValueToBigDecimal(data.getRiskRedReqAmt());
        //黄区阈值
        BigDecimal riskYellowReqAmt = bizInvestCommonService.changeValueToBigDecimal(data.getRiskYellowReqAmt());
        //指标限额要求（监管限额）
        BigDecimal riskIndexReqAmt = bizInvestCommonService.changeValueToBigDecimal(data.getRiskIndexReqAmt());
        log.info("【riskItem0126】大额风险暴露超限校验===>17 ******************* deRiskExpoAmt【{}】 riskRedReqAmt【{}】riskYellowReqAmt【{}】 riskIndexReqAmt【{}】",
                deRiskExpoAmt//2646623471.260000
                , riskRedReqAmt, riskYellowReqAmt, riskIndexReqAmt);

        //大额风险数据不存在 默认通过
        if (riskRedReqAmt.compareTo(BigDecimal.ZERO) == 0 && riskYellowReqAmt.compareTo(BigDecimal.ZERO) == 0
                && riskIndexReqAmt.compareTo(BigDecimal.ZERO) == 0){
            log.info("【riskItem0126】大额风险暴露超限校验===>22 *******************");
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        }

        //若超过监管限额， 则获取系统配置【是否强控突破监管限额】为是，则拦截业务；否则提示“客户风险暴露超过监管限额，无法通过。”
        if (deRiskExpoAmt.compareTo(riskIndexReqAmt) > 0 && "1".equals(de_tpjgxe)) {
            log.info("【riskItem0126】大额风险暴露超限校验===>18 ******************* deRiskExpoAmt【{}】 riskIndexReqAmt【{}】 de_tpjgxe【{}】"
                    ,deRiskExpoAmt,riskIndexReqAmt,de_tpjgxe);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("客户风险暴露超过监管限额，无法通过。");
            return riskResultDto;
        }
        //若超过黄区阈值、红区阈值  则提示：“客户大额风险暴露计算值为[]，黄区指标为[]，红区指标为[]，超红区/黄区指标”
        //超过红色阈值
        if (deRiskExpoAmt.compareTo(riskRedReqAmt) > 0) {
            log.info("【riskItem0126】大额风险暴露超限校验===>19 ******************* deRiskExpoAmt【{}】 riskRedReqAmt【{}】"
                    ,deRiskExpoAmt,riskRedReqAmt);
            //授信数据获取失败 RISK_ERROR_0005
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            String errorMsg = "客户大额风险暴露计算值为{1}，黄区指标为【{2}】，红区指标为【{3}】，超红区指标"
                    .replace("{1}", deRiskExpoAmt.toString())
                    .replace("{2}", riskYellowReqAmt.toString())
                    .replace("{3}", riskRedReqAmt.toString());
            riskResultDto.setRiskResultDesc(errorMsg);
            return riskResultDto;
        }
        //超过黄色阈值
        if (deRiskExpoAmt.compareTo(riskYellowReqAmt) > 0) {
            log.info("【riskItem0126】大额风险暴露超限校验===>20 ******************* deRiskExpoAmt【{}】 riskYellowReqAmt【{}】"
                    ,deRiskExpoAmt,riskYellowReqAmt);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            String errorMsg = "客户大额风险暴露计算值为{1}，黄区指标为【{2}】，红区指标为【{3}】，超黄区指标"
                    .replace("{1}", deRiskExpoAmt.toString())
                    .replace("{2}", riskYellowReqAmt.toString())
                    .replace("{3}", riskRedReqAmt.toString());
            riskResultDto.setRiskResultDesc(errorMsg);
            return riskResultDto;
        }

        log.info("【riskItem0126】大额风险暴露超限校验 结束===>21 *******************");
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
