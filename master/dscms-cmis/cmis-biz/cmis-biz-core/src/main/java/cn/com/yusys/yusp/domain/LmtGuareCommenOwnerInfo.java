/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCommenOwnerInfo
 * @类描述: lmt_guare_commen_owner_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-29 11:27:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_guare_commen_owner_info")
public class LmtGuareCommenOwnerInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 抵质押物编号 **/
	@Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
	private String pldimnNo;
	
	/** 共有人客户编号 **/
	@Column(name = "COMMEN_OWNER_CUS_ID", unique = false, nullable = true, length = 40)
	private String commenOwnerCusId;
	
	/** 共有人姓名 **/
	@Column(name = "COMMEN_OWNER_NAME", unique = false, nullable = true, length = 80)
	private String commenOwnerName;
	
	/** 共有人证件号码 **/
	@Column(name = "COMMEN_OWNER_CERT_CODE", unique = false, nullable = true, length = 40)
	private String commenOwnerCertCode;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param pldimnNo
	 */
	public void setPldimnNo(String pldimnNo) {
		this.pldimnNo = pldimnNo;
	}
	
    /**
     * @return pldimnNo
     */
	public String getPldimnNo() {
		return this.pldimnNo;
	}
	
	/**
	 * @param commenOwnerCusId
	 */
	public void setCommenOwnerCusId(String commenOwnerCusId) {
		this.commenOwnerCusId = commenOwnerCusId;
	}
	
    /**
     * @return commenOwnerCusId
     */
	public String getCommenOwnerCusId() {
		return this.commenOwnerCusId;
	}
	
	/**
	 * @param commenOwnerName
	 */
	public void setCommenOwnerName(String commenOwnerName) {
		this.commenOwnerName = commenOwnerName;
	}
	
    /**
     * @return commenOwnerName
     */
	public String getCommenOwnerName() {
		return this.commenOwnerName;
	}
	
	/**
	 * @param commenOwnerCertCode
	 */
	public void setCommenOwnerCertCode(String commenOwnerCertCode) {
		this.commenOwnerCertCode = commenOwnerCertCode;
	}
	
    /**
     * @return commenOwnerCertCode
     */
	public String getCommenOwnerCertCode() {
		return this.commenOwnerCertCode;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}