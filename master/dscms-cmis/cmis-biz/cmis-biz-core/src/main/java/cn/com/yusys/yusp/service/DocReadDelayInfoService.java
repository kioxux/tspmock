/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.DocReadDelayInfo;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.DocReadDelayInfoMapper;
import cn.com.yusys.yusp.vo.DocReadDelayInfoVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDelayInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 09:36:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocReadDelayInfoService {

    @Autowired
    private DocReadDelayInfoMapper docReadDelayInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocReadDelayInfo selectByPrimaryKey(String drdiSerno) {
        return docReadDelayInfoMapper.selectByPrimaryKey(drdiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocReadDelayInfoVo> selectAll(QueryModel model) {
        List<DocReadDelayInfoVo> records = (List<DocReadDelayInfoVo>) docReadDelayInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocReadDelayInfoVo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("delay_rqstr_date desc");
        List<DocReadDelayInfoVo> list = docReadDelayInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocReadDelayInfo record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String inputDate = "";
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.parseDateByDef(openDay);
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            // 申请时间
            inputDate = openDay;
        }
        record.setInputId(inputId);
        record.setInputBrId(inputBrId);
        record.setInputDate(inputDate);
        record.setCreateTime(createTime);
        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return docReadDelayInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocReadDelayInfo record) {
        return docReadDelayInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocReadDelayInfo record) {
        // 修改
        String updId = "";
        // 修改
        String updBrId = "";
        // 修改日期
        String updDate = "";
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 修改时间
        Date updateTime = DateUtils.parseDateByDef(openDay);
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
            // 申请时间
            updDate = openDay;
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        record.setUpdateTime(updateTime);
        return docReadDelayInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocReadDelayInfo record) {
        return docReadDelayInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String drdiSerno) {
        int result = 0;
        DocReadDelayInfo docReadDelayInfo = docReadDelayInfoMapper.selectByPrimaryKey(drdiSerno);
        if (null != docReadDelayInfo) {
            if (Objects.equals(docReadDelayInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_992)) {
                docReadDelayInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                docReadDelayInfoMapper.updateByPrimaryKeySelective(docReadDelayInfo);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(drdiSerno);
                result = 1;
            } else if (Objects.equals(docReadDelayInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_000)) {
                result = docReadDelayInfoMapper.deleteByPrimaryKey(drdiSerno);
            }
        }
        return result;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docReadDelayInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateDocStsDelay
     * @方法描述: 调阅延期申请通过的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @算法描述: 错误码
     */
    public Map<String, String> updateDocStsDelay(String draiSerno, String backDate) {
        Map<String, String> res = new HashMap<>();
        if (StringUtils.nonBlank(draiSerno)) {
            // 修改
            String updId = "";
            // 修改
            String updBrId = "";
            // 修改日期
            String updDate = "";
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 修改时间
            Date updateTime = DateUtils.parseDateByDef(openDay);
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                updId = userInfo.getLoginCode();
                // 申请机构
                updBrId = userInfo.getOrg().getCode();
                // 申请时间
                updDate = openDay;
            }
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("docStauts", "09");
            param.put("updId", updId);
            param.put("updBrId", updBrId);
            param.put("updDate", updDate);
            param.put("updateTime", updateTime);
            param.put("draiSerno", draiSerno);
            param.put("backDate", backDate);
            // 更新台账状态
            int count = docReadDelayInfoMapper.updateDocStsDelay(param);
            // 更新调阅明细状态
            int count2 = docReadDelayInfoMapper.updateDocStsRead(param);
            if (count > 0 && count2 > 0) {
                res.put("code", "0");
                res.put("msg", "调阅延期申请成功!");
            } else {
                res.put("code", "-1");
                res.put("msg", "调阅延期申请失败!");
            }
        } else {
            res.put("code", "-1");
            res.put("msg", "调阅延期失败!");
        }
        return res;
    }

    /**
     * @方法名称: updateDocStsDelay
     * @方法描述: 调阅延期申请通过的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @算法描述: 错误码
     */
    public Map<String, String> updateAccessBackDateDelay(String draiSerno, String backDate) {
        Map<String, String> res = new HashMap<>();
        if (StringUtils.nonBlank(draiSerno)) {
            // 修改
            String updId = "";
            // 修改
            String updBrId = "";
            // 修改日期
            String updDate = "";
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 修改时间
            Date updateTime = DateUtils.parseDateByDef(openDay);
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                updId = userInfo.getLoginCode();
                // 申请机构
                updBrId = userInfo.getOrg().getCode();
                // 申请时间
                updDate = openDay;
            }
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("updId", updId);
            param.put("updBrId", updBrId);
            param.put("updDate", updDate);
            param.put("updateTime", updateTime);
            param.put("draiSerno", draiSerno);
            param.put("backDate", backDate);
            int count = docReadDelayInfoMapper.updateAccessBackDateDelay(param);
            if (count > 0) {
                res.put("code", "0");
                res.put("msg", "调阅延期申请成功!");
            } else {
                res.put("code", "-1");
                res.put("msg", "调阅延期申请失败!");
            }
        } else {
            res.put("code", "-1");
            res.put("msg", "调阅延期失败!");
        }
        return res;
    }

    /**
     * 获取最新延期归还日期
     *
     * @author jijian_yx
     * @date 2021/10/14 2:43
     **/
    public String getDelayBackDate(String draiSerno) {
        return docReadDelayInfoMapper.getDelayBackDate(draiSerno);
    }
}
