/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.BlankCertiModifyAppDto;
import cn.com.yusys.yusp.repository.mapper.BlankCertiInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.BlankCertiModifyAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BlankCertiModifyAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:59:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BlankCertiModifyAppService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(GuarMortgageManageAppService.class);

    @Autowired
    private BlankCertiModifyAppMapper blankCertiModifyAppMapper;

    @Autowired
    private BlankCertiInfoMapper blankCertiInfoMapper;

    @Autowired
    private BlankCertiInfoService blankCertiInfoService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BlankCertiModifyApp selectByPrimaryKey(String pkId) {
        return blankCertiModifyAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BlankCertiModifyApp> selectAll(QueryModel model) {
        List<BlankCertiModifyApp> records = blankCertiModifyAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BlankCertiModifyApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BlankCertiModifyApp> list = blankCertiModifyAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BlankCertiModifyApp record) {
        return blankCertiModifyAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BlankCertiModifyApp record) {
        return blankCertiModifyAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BlankCertiModifyApp record) {
        return blankCertiModifyAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BlankCertiModifyApp record) {
        return blankCertiModifyAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return blankCertiModifyAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * 查询在途的申请流水号
     * @param queryModel
     * @return
     */
    public String selectOnTheWaySernoByModel(QueryModel queryModel){
        return blankCertiModifyAppMapper.selectOnTheWaySernoByModel(queryModel);
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 空白凭证变更流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: dumd
     * @创建时间: 2021-08-20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno,String bizType) {
        BlankCertiModifyApp blankCertiModifyApp = blankCertiModifyAppMapper.selectBySerno(serno);
        blankCertiModifyApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        blankCertiModifyAppMapper.updateByPrimaryKey(blankCertiModifyApp);

        //空白凭证变更申请凭证编号
        String certiNo = blankCertiModifyApp.getCertiNo();
        //根据空白凭证编号查询空白凭证信息
        BlankCertiInfo blankCertiInfo = blankCertiInfoService.selectByCertiNo(certiNo);

        if (CmisFlowConstants.FLOW_TYPE_TYPE_XT003.equals(bizType)){
            //如果是空白凭证修改，则将空白凭证台账里的凭证状态改为修改审批中
            blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_04);
        }else{
            //如果是空白凭证作废，则将空白凭证台账里的凭证状态改为作废审批中
            blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_05);
        }

        blankCertiInfoMapper.updateByPrimaryKey(blankCertiInfo);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 空白凭证变更流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述:
     * 1.将审批状态更新为997 通过
     * 2.
     * @创建人: dumd
     * @创建时间: 2021-08-20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno,String bizType) {
        BlankCertiModifyApp blankCertiModifyApp = blankCertiModifyAppMapper.selectBySerno(serno);
        blankCertiModifyApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        blankCertiModifyAppMapper.updateByPrimaryKey(blankCertiModifyApp);

        //空白凭证变更申请凭证编号
        String certiNo = blankCertiModifyApp.getCertiNo();
        //空白凭证变更申请修改后凭证编号
        String modifiedCertiNo = blankCertiModifyApp.getModifiedCertiNo();

        //根据空白凭证编号查询空白凭证信息
        BlankCertiInfo blankCertiInfo = blankCertiInfoService.selectByCertiNo(certiNo);

        if (CmisFlowConstants.FLOW_TYPE_TYPE_XT003.equals(bizType)){
            //如果是空白凭证修改，则将空白凭证台账里的该笔凭证编号改为新的凭证编号
            blankCertiInfo.setCertiNo(modifiedCertiNo);
            //先将该笔记录删除
            blankCertiInfoMapper.deleteByPrimaryKey(blankCertiInfo.getPkId());

            //将原最后修改相关字段清空
            blankCertiInfo.setUpdateTime(null);
            blankCertiInfo.setUpdBrId(null);
            blankCertiInfo.setUpdId(null);
            blankCertiInfo.setUpdDate(null);
            //将凭证状态改为未用
            blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_01);

            blankCertiInfoMapper.insertSelective(blankCertiInfo);
            log.info("空白凭证变更流程，流水号【"+serno+"】，空白凭证信息表里的凭证编号【"+certiNo+"】已改为【"+modifiedCertiNo+"】");
        }else {
            //如果是空白凭证作废，则将空白凭证台账里的该笔凭证状态改为作废
            blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_03);
            blankCertiInfoMapper.updateByPrimaryKey(blankCertiInfo);
            log.info("空白凭证变更流程，流水号【"+serno+"】，空白凭证信息表里的凭证编号为【"+certiNo+"】的凭证状态已从【作废审批中】改为【作废】");
        }
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 空白凭证变更流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: dumd
     * @创建时间: 2021-08-23
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) {
        BlankCertiModifyApp blankCertiModifyApp = blankCertiModifyAppMapper.selectBySerno(serno);
        blankCertiModifyApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        blankCertiModifyAppMapper.updateByPrimaryKey(blankCertiModifyApp);
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 空白凭证变更流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: dumd
     * @创建时间: 2021-08-23
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        BlankCertiModifyApp blankCertiModifyApp = blankCertiModifyAppMapper.selectBySerno(serno);
        blankCertiModifyApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        blankCertiModifyAppMapper.updateByPrimaryKey(blankCertiModifyApp);

        //空白凭证编号
        String certiNo = blankCertiModifyApp.getCertiNo();
        //根据空白凭证编号查询空白凭证信息
        BlankCertiInfo blankCertiInfo = blankCertiInfoService.selectByCertiNo(certiNo);
        //空白凭证信息改为未用
        blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_01);
        blankCertiInfoMapper.updateByPrimaryKey(blankCertiInfo);

        log.info("流水号为【"+serno+"】的空白凭证变更流程已被否决，将关联的凭证编号为【"+certiNo+"】的凭证台账信息的凭证状态改为【未用】");
    }

    /**
     * @方法名称: deleteOnLogic
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteOnLogic(BlankCertiModifyApp record) {
        int result = blankCertiModifyAppMapper.updateByPrimaryKeySelective(record);
        //将关联的空白凭证登记簿里的凭证状态改为未用
        BlankCertiInfo blankCertiInfo = blankCertiInfoService.selectByCertiNo(record.getCertiNo());
        blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_01);
        blankCertiInfoMapper.updateByPrimaryKey(blankCertiInfo);
        return result;
    }

    /**
     * 根据空白凭证变更对象暂存(新增/修改)数据
     * @param blankCertiModifyAppDto
     * @return
     */
    public int tempSave(BlankCertiModifyAppDto blankCertiModifyAppDto){
        String serno = blankCertiModifyAppDto.getSerno();
        BlankCertiModifyApp blankCertiModifyApp = blankCertiModifyAppMapper.selectBySerno(serno);

        if (blankCertiModifyApp==null){
            //新增，复制数据到blankCertiModifyApp
            blankCertiModifyApp = new BlankCertiModifyApp();
            BeanUtils.copyProperties(blankCertiModifyAppDto, blankCertiModifyApp);
            return blankCertiModifyAppMapper.insert(blankCertiModifyApp);
        }

        //复制数据到blankCertiModifyApp
        BeanUtils.copyProperties(blankCertiModifyAppDto, blankCertiModifyApp);
        return blankCertiModifyAppMapper.updateBySernoSelective(blankCertiModifyApp);
    }

    /**
     * 根据凭证编号查询在途的申请流水号
     * @param certiNo
     * @return
     */
    public String selectOnTheWaySernoByCertiNo(String certiNo){
        return blankCertiModifyAppMapper.selectOnTheWaySernoByCertiNo(certiNo);
    }
}