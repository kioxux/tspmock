/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.IqpAppDiscContDetails;
import cn.com.yusys.yusp.domain.IqpAppMain;
import cn.com.yusys.yusp.dto.IqpMainDiscDto;
import cn.com.yusys.yusp.repository.mapper.IqpAppMainMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpAppMainService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-03 15:23:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAppMainService {

    @Autowired
    private IqpAppMainMapper iqpAppMainMapper;

    @Autowired
    private IqpAppDiscContDetailsService iqpAppDiscContDetailsService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpAppMain selectByPrimaryKey(String pkId) {
        return iqpAppMainMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAppMain> selectAll(QueryModel model) {
        List<IqpAppMain> records = (List<IqpAppMain>) iqpAppMainMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpAppMain> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAppMain> list = iqpAppMainMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpAppMain record) {
        return iqpAppMainMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpAppMain record) {
        return iqpAppMainMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpAppMain record) {
        return iqpAppMainMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpAppMain record) {
        return iqpAppMainMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAppMainMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAppMainMapper.deleteByIds(ids);
    }



    public int insertAll(IqpMainDiscDto iqpMainDiscDto) {


        IqpAppMain iqpAppMain = new IqpAppMain();

        iqpAppMain.setUdpDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        iqpAppMain.setCreateTime(new Date());
        iqpAppMain.setUpdateTime(new Date());
        iqpAppMain.setApprStatus("000");
        User userInfo = SessionUtils.getUserInformation();
        iqpAppMain.setUdpId(userInfo.getLoginCode());
        iqpAppMain.setUdpBrId(userInfo.getOrg().getCode());
        iqpAppMain.setCusId(iqpMainDiscDto.getCusId());
        iqpAppMain.setCusName(iqpMainDiscDto.getCusName());
        iqpAppMain.setPrdId(iqpMainDiscDto.getPrdId());
        iqpAppMain.setPrdName(iqpMainDiscDto.getPrdName());

        //是否使用授信额度
        iqpAppMain.setIsUseLmtAmt(iqpMainDiscDto.getIsUseLmtAmt());
        //是否续签合同
        iqpAppMain.setIsRenew(iqpMainDiscDto.getIsRenew());
        //原合同编号
        iqpAppMain.setOrigiContNo(iqpMainDiscDto.getOrigiContNo());

        iqpAppMain.setSerno(StringUtils.getUUID());
        iqpAppMain.setPkId(StringUtils.getUUID());
        iqpAppMain.setInputId(userInfo.getLoginCode());
        iqpAppMain.setInputBrId(userInfo.getOrg().getCode());
        iqpAppMain.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));

        int iqpApp = iqpAppMainMapper.insert(iqpAppMain);
        IqpAppDiscContDetails iqpAppDiscContDetails = new IqpAppDiscContDetails();
        iqpAppDiscContDetails.setSerno(iqpAppMain.getSerno());
        iqpAppDiscContDetails.setPkId(StringUtils.getUUID());
        iqpAppDiscContDetails.setDiscContType(iqpMainDiscDto.getDiscContType());
        iqpAppDiscContDetails.setCreateTime(new Date());
        iqpAppDiscContDetails.setUpdateTime(new Date());
        iqpAppDiscContDetails.setUpdId(userInfo.getLoginCode());
        iqpAppDiscContDetails.setUpdBrId(userInfo.getOrg().getCode());
        //协议类型
        iqpAppDiscContDetails.setDiscContType(iqpMainDiscDto.getDiscContType());
        //承兑企业客户名称
        iqpAppDiscContDetails.setAcptCrpCusName(iqpMainDiscDto.getAcptCrpCusName());
        //承兑企业客户编号
        iqpAppDiscContDetails.setAcptCrpCusId(iqpMainDiscDto.getAcptCrpCusId());
        //票据种类
        iqpAppDiscContDetails.setDrftType(iqpMainDiscDto.getDrftType());
        iqpAppDiscContDetails.setRqstrLmtLimitNo(iqpMainDiscDto.getRqstrLmtLimitNo());
        int iqpAppDisc =iqpAppDiscContDetailsService.insert(iqpAppDiscContDetails);

        return iqpAppDisc;
    }
}
