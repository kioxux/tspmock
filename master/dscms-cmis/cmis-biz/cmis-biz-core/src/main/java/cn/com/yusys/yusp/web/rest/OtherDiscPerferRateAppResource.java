/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.OtherAccpPerferFeeApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.service.OtherBailDepPreferRateAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherDiscPerferRateApp;
import cn.com.yusys.yusp.service.OtherDiscPerferRateAppService;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherDiscPerferRateAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-02 17:20:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherdiscperferrateapp")
public class OtherDiscPerferRateAppResource {
    private static final Logger log = LoggerFactory.getLogger(OtherDiscPerferRateAppResource.class);
    @Autowired
    private OtherDiscPerferRateAppService otherDiscPerferRateAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherDiscPerferRateApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherDiscPerferRateApp> list = otherDiscPerferRateAppService.selectAll(queryModel);
        return new ResultDto<List<OtherDiscPerferRateApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<OtherDiscPerferRateApp>> index(@RequestBody QueryModel queryModel) {
        List<OtherDiscPerferRateApp> list = otherDiscPerferRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherDiscPerferRateApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherDiscPerferRateApp> show(@PathVariable("serno") String serno) {
        OtherDiscPerferRateApp otherDiscPerferRateApp = otherDiscPerferRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherDiscPerferRateApp>(otherDiscPerferRateApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<OtherDiscPerferRateApp> create(@RequestBody OtherDiscPerferRateApp otherDiscPerferRateApp) throws URISyntaxException {
    	otherDiscPerferRateAppService.insert(otherDiscPerferRateApp);
        return new ResultDto<OtherDiscPerferRateApp>(otherDiscPerferRateApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherDiscPerferRateApp otherDiscPerferRateApp) throws URISyntaxException {
        int result = 0;
        OtherDiscPerferRateApp otherDiscPerferRateApp1 = otherDiscPerferRateAppService.selectByPrimaryKey(otherDiscPerferRateApp.getSerno());
        if (otherDiscPerferRateApp1 != null) {
            //判断当前操作为逻辑删除 oprType=02  判断当前记录的申请状态为打回
            if (CmisBizConstants.OPR_TYPE_02.equals(otherDiscPerferRateApp.getOprType())
                    && CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherDiscPerferRateApp1.getApproveStatus())) {
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId：", otherDiscPerferRateApp1.getSerno());
                // 删除流程实例
                workflowCoreClient.deleteByBizId(otherDiscPerferRateApp1.getSerno());
                otherDiscPerferRateApp1.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                result = otherDiscPerferRateAppService.update(otherDiscPerferRateApp1);
                log.info("删除结束。。。");
            } else {
                result = otherDiscPerferRateAppService.updateSelective(otherDiscPerferRateApp);
            }
        }

        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = 0;
        //2.判断当前申请状态是否为退回，修改为自行退出
        result = otherDiscPerferRateAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:授信场景下，逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = 0;
        //2.判断当前申请状态是否为退回，修改为自行退出
        OtherDiscPerferRateApp otherAccpPerferFeeApp = otherDiscPerferRateAppService.selectByPrimaryKey(serno);
        if (otherAccpPerferFeeApp!=null){
            if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherAccpPerferFeeApp.getApproveStatus())){
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId：",otherAccpPerferFeeApp.getSerno());
                // 删除流程实例
                workflowCoreClient.deleteByBizId(otherAccpPerferFeeApp.getSerno());
                otherAccpPerferFeeApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                result = otherDiscPerferRateAppService.update(otherAccpPerferFeeApp);
                log.info("银票手续费率申请删除结束。。。");
            }else{
                otherAccpPerferFeeApp.setOprType(CmisBizConstants.OPR_TYPE_02);
                result = otherDiscPerferRateAppService.update(otherAccpPerferFeeApp);
            }
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherDiscPerferRateAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getotherdiscperferrateapp
     * @方法描述: 根据入参获取当前客户经理名下贴现优惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherdiscperferrateapp")
    protected ResultDto<List<OtherDiscPerferRateApp>> getOtherForRateApp(@RequestBody QueryModel model) {
        List<OtherDiscPerferRateApp> list = otherDiscPerferRateAppService.selectByModel(model);
        return new ResultDto<List<OtherDiscPerferRateApp>>(list);
    }

    /**
     * @方法名称: upIsUpperApprAuth
     * @方法描述:  上调权限处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/upIsUpperApprAuth")
    protected ResultDto<Integer> upIsUpperApprAuth(@RequestBody Map<String,String> params) {
        int result = 0;
        // 获取流水号
        String serno =  params.get("serno");
        // 是否上调权限
        String  isUpperApprAuth = params.get("isUpperApprAuth");
        if(!"".equals(serno) && null != serno){
            OtherDiscPerferRateApp otherDiscPerferRateApp =  otherDiscPerferRateAppService.selectByPrimaryKey(serno);
            if(null != otherDiscPerferRateApp){
                otherDiscPerferRateApp.setIsUpperApprAuth(isUpperApprAuth);
                result = otherDiscPerferRateAppService.update(otherDiscPerferRateApp);
            }
        }
        return new ResultDto<Integer>(result);
    }


}
