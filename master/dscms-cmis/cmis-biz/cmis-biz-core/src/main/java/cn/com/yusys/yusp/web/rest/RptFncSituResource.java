/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptFncSitu;
import cn.com.yusys.yusp.service.RptFncSituService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-06-18 18:54:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptfncsitu")
public class RptFncSituResource {
    @Autowired
    private RptFncSituService rptFncSituService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptFncSitu>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptFncSitu> list = rptFncSituService.selectAll(queryModel);
        return new ResultDto<List<RptFncSitu>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptFncSitu>> index(QueryModel queryModel) {
        List<RptFncSitu> list = rptFncSituService.selectByModel(queryModel);
        return new ResultDto<List<RptFncSitu>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptFncSitu> show(@PathVariable("serno") String serno) {
        RptFncSitu rptFncSitu = rptFncSituService.selectByPrimaryKey(serno);
        return new ResultDto<RptFncSitu>(rptFncSitu);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptFncSitu> create(@RequestBody RptFncSitu rptFncSitu) throws URISyntaxException {
        rptFncSituService.insert(rptFncSitu);
        return new ResultDto<RptFncSitu>(rptFncSitu);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptFncSitu rptFncSitu) throws URISyntaxException {
        int result = rptFncSituService.update(rptFncSitu);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptFncSituService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptFncSituService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询 调查报告财务情况资产负债小企业版
     *
     * @param map
     * @return RptFncSitu rptFncSitu
     */
    @ApiOperation("根据流水号查询")
    @PostMapping("/selectBySerno")
    protected ResultDto<RptFncSitu> selectBySerno(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        return new ResultDto<RptFncSitu>(rptFncSituService.selectByPrimaryKey(serno));
    }

    /**
     * 新增
     *
     * @param rptFncSitu
     * @return
     */
    @PostMapping("/insertFncSitu")
    protected ResultDto<Integer> insertFncSitu(@RequestBody RptFncSitu rptFncSitu) {
        return new ResultDto<Integer>(rptFncSituService.insertSelective(rptFncSitu));
    }

    @PostMapping("/upadteFncSitu")
    protected ResultDto<Integer> upadteFncSitu(@RequestBody RptFncSitu rptFncSitu) {
        return new ResultDto<Integer>(rptFncSituService.updateSelective(rptFncSitu));
    }

    /**
     * 保存信息
     *
     * @param rptFncSitu
     * @return
     */
    @PostMapping("/saveFncSitu")
    protected ResultDto<Integer> saveFncSitu(@RequestBody RptFncSitu rptFncSitu) {
        return new ResultDto<Integer>(rptFncSituService.saveFncSitu(rptFncSitu));
    }

    /**
     * 初始化信息
     *
     * @param map
     * @return
     */
    @PostMapping("/initFncSitu")
    public ResultDto<RptFncSitu> initFncSitu(@RequestBody Map map) {
        return new ResultDto<RptFncSitu>(rptFncSituService.initFncSitu(map));
    }

    @PostMapping("/resetFnc")
    public ResultDto<RptFncSitu> resetFnc(@RequestBody Map map){
        return  new ResultDto<RptFncSitu>(rptFncSituService.resetFnc(map));
    }
}
