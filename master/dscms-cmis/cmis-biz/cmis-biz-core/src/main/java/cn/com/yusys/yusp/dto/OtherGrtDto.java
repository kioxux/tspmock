package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

public class OtherGrtDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    private String pkId;

    /** 业务流水号 **/
    private String serno;

    /** 客户编号 **/
    private String cusId;

    /** 客户类型 **/
    private String cusType;

    /** 客户名称 **/
    private String cusName;

    /** 押品编号 **/
    private String guarNo;

    /** 房屋坐落位置 **/
    private String housePlace;

    /** 房屋建筑面积 **/
    private java.math.BigDecimal houseBuildSqu;

    /** 土地使用权面积 **/
    private java.math.BigDecimal landUtilSqu;

    /** 房产类型 **/
    private String houseType;

    /** 土地用途 **/
    private String landUse;

    /** 土地使用权性质 **/
    private String landUseCha;

    /** 评估来源 **/
    private String evalSource;

    /** 操作类型 **/
    private String oprType;

    /** 房产评估价值 **/
    private java.math.BigDecimal realproEvalValue;

    /** 土地评估价值 **/
    private java.math.BigDecimal landEvalValue;

    /** 房地产评估总价 **/
    private java.math.BigDecimal realproEvalTotalValue;

    /** 评估机构名称 **/
    private String evalOrgName;

    /** 评估报告出具时间 **/
    private String evalReportTime;

    /** 房产确认价值 **/
    private java.math.BigDecimal realproCfirmValue;

    /** 土地确认价值 **/
    private java.math.BigDecimal landCfirmValue;

    /** 房地产确认总价 **/
    private java.math.BigDecimal realproCfirmTotalValue;

    /** 该抵押物项下申请的融资金额 **/
    private java.math.BigDecimal pldAppFinPrice;

    /** 房地产抵押情况 **/
    private String realproPldCase;

    /** 抵押到期日 **/
    private String pldEndDate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getHousePlace() {
        return housePlace;
    }

    public void setHousePlace(String housePlace) {
        this.housePlace = housePlace;
    }

    public BigDecimal getHouseBuildSqu() {
        return houseBuildSqu;
    }

    public void setHouseBuildSqu(BigDecimal houseBuildSqu) {
        this.houseBuildSqu = houseBuildSqu;
    }

    public BigDecimal getLandUtilSqu() {
        return landUtilSqu;
    }

    public void setLandUtilSqu(BigDecimal landUtilSqu) {
        this.landUtilSqu = landUtilSqu;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getLandUse() {
        return landUse;
    }

    public void setLandUse(String landUse) {
        this.landUse = landUse;
    }

    public String getLandUseCha() {
        return landUseCha;
    }

    public void setLandUseCha(String landUseCha) {
        this.landUseCha = landUseCha;
    }

    public String getEvalSource() {
        return evalSource;
    }

    public void setEvalSource(String evalSource) {
        this.evalSource = evalSource;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public BigDecimal getRealproEvalValue() {
        return realproEvalValue;
    }

    public void setRealproEvalValue(BigDecimal realproEvalValue) {
        this.realproEvalValue = realproEvalValue;
    }

    public BigDecimal getLandEvalValue() {
        return landEvalValue;
    }

    public void setLandEvalValue(BigDecimal landEvalValue) {
        this.landEvalValue = landEvalValue;
    }

    public BigDecimal getRealproEvalTotalValue() {
        return realproEvalTotalValue;
    }

    public void setRealproEvalTotalValue(BigDecimal realproEvalTotalValue) {
        this.realproEvalTotalValue = realproEvalTotalValue;
    }

    public String getEvalOrgName() {
        return evalOrgName;
    }

    public void setEvalOrgName(String evalOrgName) {
        this.evalOrgName = evalOrgName;
    }

    public String getEvalReportTime() {
        return evalReportTime;
    }

    public void setEvalReportTime(String evalReportTime) {
        this.evalReportTime = evalReportTime;
    }

    public BigDecimal getRealproCfirmValue() {
        return realproCfirmValue;
    }

    public void setRealproCfirmValue(BigDecimal realproCfirmValue) {
        this.realproCfirmValue = realproCfirmValue;
    }

    public BigDecimal getLandCfirmValue() {
        return landCfirmValue;
    }

    public void setLandCfirmValue(BigDecimal landCfirmValue) {
        this.landCfirmValue = landCfirmValue;
    }

    public BigDecimal getRealproCfirmTotalValue() {
        return realproCfirmTotalValue;
    }

    public void setRealproCfirmTotalValue(BigDecimal realproCfirmTotalValue) {
        this.realproCfirmTotalValue = realproCfirmTotalValue;
    }

    public BigDecimal getPldAppFinPrice() {
        return pldAppFinPrice;
    }

    public void setPldAppFinPrice(BigDecimal pldAppFinPrice) {
        this.pldAppFinPrice = pldAppFinPrice;
    }

    public String getRealproPldCase() {
        return realproPldCase;
    }

    public void setRealproPldCase(String realproPldCase) {
        this.realproPldCase = realproPldCase;
    }

    public String getPldEndDate() {
        return pldEndDate;
    }

    public void setPldEndDate(String pldEndDate) {
        this.pldEndDate = pldEndDate;
    }
}
