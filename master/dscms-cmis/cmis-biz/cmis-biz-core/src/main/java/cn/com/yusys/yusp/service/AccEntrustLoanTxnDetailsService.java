/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccEntrustLoanTxnDetails;
import cn.com.yusys.yusp.dto.AccEntrustLoanTxnDetailsDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Listnm;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanTxnDetailsMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccEntrustLoanTxnDetailsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-02 21:36:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccEntrustLoanTxnDetailsService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(AccEntrustLoanTxnDetailsService.class);

    @Autowired
    private AccEntrustLoanTxnDetailsMapper accEntrustLoanTxnDetailsMapper;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccEntrustLoanTxnDetails selectByPrimaryKey(String pkId) {
        return accEntrustLoanTxnDetailsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccEntrustLoanTxnDetails> selectAll(QueryModel model) {
        List<AccEntrustLoanTxnDetails> records = (List<AccEntrustLoanTxnDetails>) accEntrustLoanTxnDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccEntrustLoanTxnDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccEntrustLoanTxnDetails> list = accEntrustLoanTxnDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccEntrustLoanTxnDetails record) {
        return accEntrustLoanTxnDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccEntrustLoanTxnDetails record) {
        return accEntrustLoanTxnDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccEntrustLoanTxnDetails record) {
        return accEntrustLoanTxnDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccEntrustLoanTxnDetails record) {
        return accEntrustLoanTxnDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accEntrustLoanTxnDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accEntrustLoanTxnDetailsMapper.deleteByIds(ids);
    }


    /**
     * 交易明细
     *
     * @author 刘权
     **/
    public ResultDto<Ln3103RespDto> sendToJYMX(AccEntrustLoanTxnDetails record) {

        Ln3103ReqDto ln3103ReqDto = new Ln3103ReqDto();
        //借据编号
        ln3103ReqDto.setDkjiejuh(record.getBillNo());

        ResultDto<Ln3103RespDto> resultDto = dscms2CoreLnClientService.ln3103(ln3103ReqDto);
        String ln3103Code = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3103Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        AccEntrustLoanTxnDetailsDto accEntrustLoanTxnDetailsDto = new AccEntrustLoanTxnDetailsDto();
        if (Objects.nonNull(resultDto.getData())) {
            List<Listnm> lstkhzmx = Optional.ofNullable(resultDto.getData().getListnm()).orElse(new ArrayList<>());

        } else {
            resultDto.setMessage(ln3103Meesage);
            return resultDto;
        }
        return resultDto;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccEntrustLoanTxnDetailsDto> queryAllBillDetail(QueryModel model) {
        String billNo = (String) model.getCondition().get("billNo");
        List<AccEntrustLoanTxnDetailsDto> list = new ArrayList<>();
        AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(billNo);
        Ln3103ReqDto reqDto = new Ln3103ReqDto();
        // 贷款借据号
        reqDto.setDkjiejuh(billNo);
        ResultDto<Ln3103RespDto> resultDto = dscms2CoreLnClientService.ln3103(reqDto);
        if (Objects.nonNull(resultDto)
                && Objects.equals("0", resultDto.getCode())
                && Objects.nonNull(resultDto.getData())
                && Objects.nonNull(resultDto.getData().getZongbish())
                && resultDto.getData().getZongbish() > 0) {
            List<Listnm> listnmList = resultDto.getData().getListnm();
            AccEntrustLoanTxnDetailsDto accEntrustLoanTxnDetailsDto = null;
            if (CollectionUtils.nonEmpty(listnmList)) {
                for (Listnm listnm : listnmList) {
                    accEntrustLoanTxnDetailsDto = new AccEntrustLoanTxnDetailsDto();
                    // 核心交易流水号
                    accEntrustLoanTxnDetailsDto.setHxSerno(listnm.getJiaoyils());
                    // 合同编号
                    accEntrustLoanTxnDetailsDto.setContNo(accEntrustLoan.getContNo());
                    // 借据编号
                    accEntrustLoanTxnDetailsDto.setBillNo(accEntrustLoan.getBillNo());
                    // 客户编号
                    accEntrustLoanTxnDetailsDto.setCusId(accEntrustLoan.getCusId());
                    // 客户名称
                    accEntrustLoanTxnDetailsDto.setCusName(accEntrustLoan.getCusName());
                    // 交易明细类型
                    accEntrustLoanTxnDetailsDto.setTxnDetailType(listnm.getYuezdshm());
                    // 币种
                    accEntrustLoanTxnDetailsDto.setCurType(listnm.getHuobdhao());
                    // 交易本金金额
                    accEntrustLoanTxnDetailsDto.setTxnBjAmt(listnm.getJiaoyije());
                    // 交易日期
                    accEntrustLoanTxnDetailsDto.setTranDate(listnm.getJiaoyirq());
                    // 余额
                    accEntrustLoanTxnDetailsDto.setTxnBjBalance(listnm.getYueeeeee());
                    // 增减
                    accEntrustLoanTxnDetailsDto.setAddOrReduce(listnm.getJiaoyifx());

                    list.add(accEntrustLoanTxnDetailsDto);
                }
            }
        }
        return list;
    }
}
