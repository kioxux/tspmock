package cn.com.yusys.yusp.service.client.lmt.cmislmt0021;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询客户基本信息
 *
 * @author xll
 * @version 1.0
 * @since 2021年5月10日 下午1:22:06
 */
@Service
public class CmisLmt0021Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0021Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * 业务逻辑处理方法：查询对公客户授信信息
     *
     * @param cmisLmt0021ReqDto
     * @return
     */
    @Transactional
    public CmisLmt0021RespDto cmisLmt0021(CmisLmt0021ReqDto cmisLmt0021ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, JSON.toJSONString(cmisLmt0021ReqDto));
        ResultDto<CmisLmt0021RespDto> cmisLmt0021ResultDto = cmisLmtClientService.cmisLmt0021(cmisLmt0021ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, JSON.toJSONString(cmisLmt0021ResultDto));

        String cmisLmt0021Code = Optional.ofNullable(cmisLmt0021ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0021Meesage = Optional.ofNullable(cmisLmt0021ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0021RespDto cmisLmt0021RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0021ResultDto.getCode()) && cmisLmt0021ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0021ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0021RespDto = cmisLmt0021ResultDto.getData();
        } else {
            if (cmisLmt0021ResultDto.getData() != null) {
                cmisLmt0021Code = cmisLmt0021ResultDto.getData().getErrorCode();
                cmisLmt0021Meesage = cmisLmt0021ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0021Code = EpbEnum.EPB099999.key;
                cmisLmt0021Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0021Code, cmisLmt0021Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value);
        return cmisLmt0021RespDto;
    }
}
