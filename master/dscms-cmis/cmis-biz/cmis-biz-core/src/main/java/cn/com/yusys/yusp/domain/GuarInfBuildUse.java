/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuildUse
 * @类描述: guar_inf_build_use数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_build_use")
public class GuarInfBuildUse extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 土地证（不动产权证号） **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 40)
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地用途 STD_ZB_LANDYT **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_USE_AREA", unique = false, nullable = true, length = 18)
	private String landUseArea;
	
	/** 闲置土地类型STD_ZB_XZTDLX **/
	@Column(name = "LAND_NOTINUSE_TYPE", unique = false, nullable = true, length = 10)
	private String landNotinuseType;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 750)
	private String landExplain;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 土地详细地址 **/
	@Column(name = "LAND_DETAILADD", unique = false, nullable = true, length = 100)
	private String landDetailadd;
	
	/** 宗地号 **/
	@Column(name = "PARCEL_NO", unique = false, nullable = true, length = 40)
	private String parcelNo;
	
	/** 购买时间 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 是否有地上定着物 **/
	@Column(name = "LAND_UP", unique = false, nullable = true, length = 10)
	private String landUp;
	
	/** 定着物种类 STD_ZB_DZWZL **/
	@Column(name = "LAND_UP_TYPE", unique = false, nullable = true, length = 10)
	private String landUpType;
	
	/** 地上建筑物项数 **/
	@Column(name = "LAND_BUILD_AMOUNT", unique = false, nullable = true, length = 10)
	private Integer landBuildAmount;
	
	/** 定着物所有权人名称 **/
	@Column(name = "LAND_UP_OWNERSHIP_NAME", unique = false, nullable = true, length = 100)
	private String landUpOwnershipName;
	
	/** 定着物所有权人范围 STD_ZB_LAND_UP_SCOPE **/
	@Column(name = "LAND_UP_OWNERSHIP_SCOPE", unique = false, nullable = true, length = 100)
	private String landUpOwnershipScope;
	
	/** 地上定着物说明 **/
	@Column(name = "LAND_UP_EXPLAIN", unique = false, nullable = true, length = 750)
	private String landUpExplain;
	
	/** 地上定着物总面积 **/
	@Column(name = "LAND_UP_ALL_AREA", unique = false, nullable = true, length = 5)
	private String landUpAllArea;
	
	/** 使用权抵押登记证号 **/
	@Column(name = "USE_CERT_NO", unique = false, nullable = true, length = 40)
	private String useCertNo;
	
	/** 使用权登记机关 **/
	@Column(name = "USE_CERT_DEP", unique = false, nullable = true, length = 100)
	private String useCertDep;
	
	/** 土地所在地段情况 STD_ZB_FCDDQK **/
	@Column(name = "LAND_P_INFO", unique = false, nullable = true, length = 10)
	private String landPInfo;
	
	/** 房产类别 STD_ZB_HOUSE_TYPE **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 10)
	private String houseType;
	
	/** 产权年限 **/
	@Column(name = "HOUSE_PR", unique = false, nullable = true, length = 3)
	private String housePr;
	
	/** 建筑年份 **/
	@Column(name = "ARCH_YEAR", unique = false, nullable = true, length = 10)
	private String archYear;
	
	/** 土地使用类型 STD_ZB_PROJECT_TYPE **/
	@Column(name = "LAND_USED_TYPE", unique = false, nullable = true, length = 10)
	private String landUsedType;
	
	/** 转让金拖欠金额（元） **/
	@Column(name = "TRANSF_UNPAID_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal transfUnpaidAmt;
	
	/** 所属地段STD_ZB_BELG_PLACE **/
	@Column(name = "BELONG_AREA", unique = false, nullable = true, length = 2)
	private String belongArea;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 750)
	private String street;
	
	/** 押品使用情况STD_ZB_GUAR_UTIL_CASE **/
	@Column(name = "GUAR_UTIL_CASE", unique = false, nullable = true, length = 10)
	private String guarUtilCase;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea;
	}
	
    /**
     * @return landUseArea
     */
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landNotinuseType
	 */
	public void setLandNotinuseType(String landNotinuseType) {
		this.landNotinuseType = landNotinuseType;
	}
	
    /**
     * @return landNotinuseType
     */
	public String getLandNotinuseType() {
		return this.landNotinuseType;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param landDetailadd
	 */
	public void setLandDetailadd(String landDetailadd) {
		this.landDetailadd = landDetailadd;
	}
	
    /**
     * @return landDetailadd
     */
	public String getLandDetailadd() {
		return this.landDetailadd;
	}
	
	/**
	 * @param parcelNo
	 */
	public void setParcelNo(String parcelNo) {
		this.parcelNo = parcelNo;
	}
	
    /**
     * @return parcelNo
     */
	public String getParcelNo() {
		return this.parcelNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param landUp
	 */
	public void setLandUp(String landUp) {
		this.landUp = landUp;
	}
	
    /**
     * @return landUp
     */
	public String getLandUp() {
		return this.landUp;
	}
	
	/**
	 * @param landUpType
	 */
	public void setLandUpType(String landUpType) {
		this.landUpType = landUpType;
	}
	
    /**
     * @return landUpType
     */
	public String getLandUpType() {
		return this.landUpType;
	}
	
	/**
	 * @param landBuildAmount
	 */
	public void setLandBuildAmount(Integer landBuildAmount) {
		this.landBuildAmount = landBuildAmount;
	}
	
    /**
     * @return landBuildAmount
     */
	public Integer getLandBuildAmount() {
		return this.landBuildAmount;
	}
	
	/**
	 * @param landUpOwnershipName
	 */
	public void setLandUpOwnershipName(String landUpOwnershipName) {
		this.landUpOwnershipName = landUpOwnershipName;
	}
	
    /**
     * @return landUpOwnershipName
     */
	public String getLandUpOwnershipName() {
		return this.landUpOwnershipName;
	}
	
	/**
	 * @param landUpOwnershipScope
	 */
	public void setLandUpOwnershipScope(String landUpOwnershipScope) {
		this.landUpOwnershipScope = landUpOwnershipScope;
	}
	
    /**
     * @return landUpOwnershipScope
     */
	public String getLandUpOwnershipScope() {
		return this.landUpOwnershipScope;
	}
	
	/**
	 * @param landUpExplain
	 */
	public void setLandUpExplain(String landUpExplain) {
		this.landUpExplain = landUpExplain;
	}
	
    /**
     * @return landUpExplain
     */
	public String getLandUpExplain() {
		return this.landUpExplain;
	}
	
	/**
	 * @param landUpAllArea
	 */
	public void setLandUpAllArea(String landUpAllArea) {
		this.landUpAllArea = landUpAllArea;
	}
	
    /**
     * @return landUpAllArea
     */
	public String getLandUpAllArea() {
		return this.landUpAllArea;
	}
	
	/**
	 * @param useCertNo
	 */
	public void setUseCertNo(String useCertNo) {
		this.useCertNo = useCertNo;
	}
	
    /**
     * @return useCertNo
     */
	public String getUseCertNo() {
		return this.useCertNo;
	}
	
	/**
	 * @param useCertDep
	 */
	public void setUseCertDep(String useCertDep) {
		this.useCertDep = useCertDep;
	}
	
    /**
     * @return useCertDep
     */
	public String getUseCertDep() {
		return this.useCertDep;
	}
	
	/**
	 * @param landPInfo
	 */
	public void setLandPInfo(String landPInfo) {
		this.landPInfo = landPInfo;
	}
	
    /**
     * @return landPInfo
     */
	public String getLandPInfo() {
		return this.landPInfo;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param housePr
	 */
	public void setHousePr(String housePr) {
		this.housePr = housePr;
	}
	
    /**
     * @return housePr
     */
	public String getHousePr() {
		return this.housePr;
	}
	
	/**
	 * @param archYear
	 */
	public void setArchYear(String archYear) {
		this.archYear = archYear;
	}
	
    /**
     * @return archYear
     */
	public String getArchYear() {
		return this.archYear;
	}
	
	/**
	 * @param landUsedType
	 */
	public void setLandUsedType(String landUsedType) {
		this.landUsedType = landUsedType;
	}
	
    /**
     * @return landUsedType
     */
	public String getLandUsedType() {
		return this.landUsedType;
	}
	
	/**
	 * @param transfUnpaidAmt
	 */
	public void setTransfUnpaidAmt(java.math.BigDecimal transfUnpaidAmt) {
		this.transfUnpaidAmt = transfUnpaidAmt;
	}
	
    /**
     * @return transfUnpaidAmt
     */
	public java.math.BigDecimal getTransfUnpaidAmt() {
		return this.transfUnpaidAmt;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea;
	}
	
    /**
     * @return belongArea
     */
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase;
	}
	
    /**
     * @return guarUtilCase
     */
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}