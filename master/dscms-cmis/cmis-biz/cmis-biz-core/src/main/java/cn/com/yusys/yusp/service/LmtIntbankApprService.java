/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import cn.com.yusys.yusp.domain.LmtIntbankAppr;
import cn.com.yusys.yusp.domain.LmtIntbankApprSub;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankApprMapper;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankApprSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Supplier;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 14:53:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankApprService extends BizInvestCommonService{

    private static final Logger logger = LoggerFactory.getLogger(LmtIntbankApprService.class);

    @Autowired
    private LmtIntbankApprMapper lmtIntbankApprMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankApprSubMapper lmtIntbankApprSubMapper;

    @Autowired
    private LmtIntbankApprSubService lmtIntbankApprSubService ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankAppr selectByPrimaryKey(String pkId) {
        return lmtIntbankApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankAppr> selectAll(QueryModel model) {
        List<LmtIntbankAppr> records = (List<LmtIntbankAppr>) lmtIntbankApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAppr> list = lmtIntbankApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankAppr record) {
        return lmtIntbankApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankAppr record) {
        return lmtIntbankApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankAppr record) {
        return lmtIntbankApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankAppr record) {
        if (StringUtils.isBlank(record.getPkId())){
            LmtIntbankAppr lmtIntbankAppr = selectLastBySerno(record.getSerno());
            if (lmtIntbankAppr != null){
                record.setPkId(lmtIntbankAppr.getPkId());
            }else{
                return 0;
            }
        }
        return lmtIntbankApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankApprMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectLmtIntBankApprApprBySerno
     * @方法描述: 根据流水号，获取最新一条批复审批表数据信息
     * @参数与返回说明: 最新一条批复审批信息
     * @算法描述: 无
     */
    public LmtIntbankAppr selectLmtIntBankApprApprBySerno(String serno) {
        return lmtIntbankApprMapper.selectLmtIntBankApprApprBySerno(serno);
    }

    /**
     * @方法名称: initLmtIntbankApprInfo
     * @方法描述: 根据申请信息，生成审批表申请信息
     * @参数与返回说明: 最新一条批复审批信息
     * @算法描述: 无
     */
    public LmtIntbankAppr initLmtIntbankApprInfo(LmtIntbankApp lmtIntbankApp){
        //主键流水号
        String pkValue = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, new HashedMap());
        //审批流水号
        String approveSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.INTBANK_LMT_SEQ, new HashedMap());
        //初始化审批表基本信息对象
        LmtIntbankAppr lmtIntbankAppr = new LmtIntbankAppr() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtIntbankApp, lmtIntbankAppr);
        //设置主键
        lmtIntbankAppr.setPkId(pkValue);
        //设置审批流水号
        lmtIntbankAppr.setApproveSerno(approveSerno);
        //TODO: 下边数据暂时不知道怎么取 ，但是放置
        //综合分析
        lmtIntbankAppr.setInteAnaly("");
        //评审结论
        lmtIntbankAppr.setReviewResult("");
        //结论性描述
        lmtIntbankAppr.setRestDesc("");
        //终审机构
        lmtIntbankAppr.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //审批模式
        lmtIntbankAppr.setApprMode("");
        //最新更新日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        lmtIntbankAppr.setUpdDate(DateUtils.getCurrDateStr());
        //创建日期
        lmtIntbankAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtIntbankAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtIntbankAppr ;
    }

    /**
     * @方法名称: updateLmtAmt
     * @方法描述: 更新授信金额
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateLmtAmt(String serno) {
        String lmtAmt = lmtIntbankApprSubMapper.selectLmtAmtByModel(serno);
        //更新授信金额操作参数
        Map delMap = new HashMap();
        delMap.put("serno",serno);
        delMap.put("lmtAmt",  lmtAmt);

        return lmtIntbankApprMapper.updateLmtAmtByParams(delMap);
    }


    /**
     * 获取最新审批表数据
     * @param serno
     * @return
     */
    public LmtIntbankAppr selectLastBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort(" createTime desc");
        queryModel.setPage(1);
        queryModel.setSize(1);

        List<LmtIntbankAppr> lmtIntbankApprs = selectByModel(queryModel);
        if (lmtIntbankApprs!=null && lmtIntbankApprs.size()>0){
            return lmtIntbankApprs.get(0);
        }
        return null;
    }

    /**
     * @方法名称: updateRestByPkId
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkId(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtIntbankAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtIntbankAppr lmtIntbankAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            lmtIntbankAppr.setReviewResult(reviewResult);
            //获取前端传入参数restDesc-审批意见
            String restDesc = (String)model.getCondition().get("restDesc");
            lmtIntbankAppr.setRestDesc(restDesc);
            //更新 更新数据时间
            lmtIntbankAppr.setUpdateTime(getCurrrentDate());
            return lmtIntbankApprMapper.updateByPrimaryKey(lmtIntbankAppr);
        }
        return 0;
    }

    /**
     * @方法名称: updateRestByPkIdZH
     * @方法描述: 根据Serno更新信贷管理部风险派驻岗审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkIdZH(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtIntbankAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtIntbankAppr lmtIntbankAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            lmtIntbankAppr.setReviewResultZh(reviewResult);
            //获取前端传入参数restDesc-审批意见
            String restDesc = (String)model.getCondition().get("restDesc");
            lmtIntbankAppr.setRestDescZh(restDesc);
            //更新 更新数据时间
            lmtIntbankAppr.setUpdateTime(getCurrrentDate());
            return lmtIntbankApprMapper.updateByPrimaryKey(lmtIntbankAppr);
        }
        return 0;
    }

    /**
     * 流程审批过程中，查询最新一条审批记录，根据审批记录，拷贝一条全新的记录信息
     * @param lmtIntbankApp
     * @param cur_next_id
     */
    @Transactional(rollbackFor=Exception.class)
    public void generateLmtIntbankApprService(LmtIntbankApp lmtIntbankApp, String issueReportType, String cur_next_id) throws Exception {

        logger.info("用信条件拷贝判断 开始==》cur_next_id【{}】",cur_next_id);
        String[] ingoreProperties = getApprIngoreProperties(cur_next_id);
        logger.info("用信条件拷贝判断 结束==》ingoreProperties【{}】", Arrays.toString(ingoreProperties));

        //根据流水号查询最新的一条批复信息
        String serno = lmtIntbankApp.getSerno() ;
        //根据流水号查询最新的审批记录
        LmtIntbankAppr lmtIntbankApprNew = lmtIntbankApprMapper.selectNewLmtIntApprBySerno(serno) ;
        //初始化新的审批对象
        LmtIntbankAppr lmtIntbankAppr = new LmtIntbankAppr() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtIntbankApprNew, lmtIntbankAppr,ingoreProperties);

        //判断是否拷贝 评审结论和结论性描述
        //1.通用规则：评审结论和结论性描述，都不copy至下一节点；
        //2.例外规则：
        //      流程审批的下一节点如果是信贷管理部风险派驻岗，
        //      则获取上一条审批记录中出具报告类型为审查报告的且ZH审批结论为空的数据，
        //      copy评审结论和结论性描述至生成的最新审批记录数据中
        String nextNodeId = cur_next_id.split(";")[1];
        if (CmisBizConstants.Xdglbfxpzg_NodeIds.contains(nextNodeId+",")){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",serno);
            BizInvestCommonService.checkParamsIsNull("serno",serno);
            queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
            queryModel.addCondition("issueReportType",CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
            queryModel.setSort(" createTime desc");
            List<LmtIntbankAppr> lmtIntbankApprs = selectAll(queryModel);
            if (CollectionUtils.isNotEmpty(lmtIntbankApprs)){
                for (LmtIntbankAppr intbankAppr : lmtIntbankApprs) {
                    if (StringUtils.isBlank(intbankAppr.getReviewResultZh())){
                        lmtIntbankAppr.setRestDesc(intbankAppr.getRestDesc());
                        lmtIntbankAppr.setReviewResult(intbankAppr.getReviewResult());
                        break;
                    }
                }
                LmtIntbankAppr lmtIntbankAppr1 = lmtIntbankApprs.get(0);
                lmtIntbankAppr.setInteAnaly(lmtIntbankAppr1.getInteAnaly());
                lmtIntbankAppr.setInteAnalyZh(lmtIntbankAppr1.getInteAnalyZh());
            }
        }


        //主键流水号
        lmtIntbankAppr.setPkId(generatePkId());
        //初始化审批流水号   TODO:该流水号是否从流程审批记录中获取
        lmtIntbankAppr.setApproveSerno(generateSerno(SeqConstant.INTBANK_LMT_SEQ));
        //更新日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        lmtIntbankAppr.setUpdDate(DateUtils.getCurrDateStr());
        //更新时间
        lmtIntbankAppr.setUpdateTime(getCurrrentDate());
        //创建时间
        lmtIntbankAppr.setCreateTime(getCurrrentDate());
        //具审查报告、出具批复
        lmtIntbankAppr.setIssueReportType(issueReportType);
        lmtIntbankApprMapper.insert(lmtIntbankAppr) ;

        if (ingoreProperties.length == 4){
            // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(lmtIntbankAppr.getApproveSerno(), lmtIntbankApprNew.getApproveSerno());
        }

        /*************** 处理同业分项审批信息 *********************/
        //审批流水号
        String approveSerno = lmtIntbankApprNew.getApproveSerno() ;
        //新的批复流水
        String newApproveSerno = lmtIntbankAppr.getApproveSerno() ;
        //处理分项表数据
        List<LmtIntbankApprSub> lmtIntbankApprSubList = lmtIntbankApprSubService.selectSubListByApproveSerno(approveSerno) ;
        //工厂模式创建对象
        Supplier<LmtIntbankApprSub> lmtApprSubSupplier = LmtIntbankApprSub::new ;
        //遍历分项申请信息，生成分项批复信息
        if(CollectionUtils.isNotEmpty(lmtIntbankApprSubList)){
            for (LmtIntbankApprSub lmtIntbankApprSubOld : lmtIntbankApprSubList) {
                LmtIntbankApprSub lmtIntbankApprSub = lmtApprSubSupplier.get() ;
                //数据拷贝
                BeanUtils.copyProperties(lmtIntbankApprSubOld, lmtIntbankApprSub);
                //初始化主键
                lmtIntbankApprSub.setPkId(generatePkId());
                //更新批复流水
                lmtIntbankApprSub.setApproveSerno(newApproveSerno);
                //重新生成分项批复流水
                lmtIntbankApprSub.setApproveSubSerno(generateSerno(SeqConstant.INTBANK_LMT_SUB_SEQ));
                //更新日期
                lmtIntbankApprSub.setUpdDate(getCurrrentDateStr());
                //更新时间
                lmtIntbankApprSub.setUpdateTime(getCurrrentDate());
                //创建时间
                lmtIntbankApprSub.setCreateTime(getCurrrentDate());
                //数据落地
                lmtIntbankApprSubService.insert(lmtIntbankApprSub) ;
            }
        }
    }

    /**
     * 上調權限修改
     * @param lmtIntbankApprMap
     * @return
     */
    public Integer updateUpApprAuth(Map lmtIntbankApprMap) {
        LmtIntbankAppr lmtIntbankApprL = new LmtIntbankAppr();
        String instanceId = (String) lmtIntbankApprMap.get("instanceId");
        mapToBean(lmtIntbankApprMap,lmtIntbankApprL);

        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(lmtIntbankApprL.getSerno());
        param.setInstanceId(instanceId);//
        Map<String, Object> params = new HashMap<>();
        params.put("isUpAppr",lmtIntbankApprL.getIsUpperApprAuth());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);

        return updateSelective(lmtIntbankApprL);
    }
}
