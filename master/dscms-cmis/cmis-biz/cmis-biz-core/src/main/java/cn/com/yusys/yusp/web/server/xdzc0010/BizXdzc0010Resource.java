package cn.com.yusys.yusp.web.server.xdzc0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0010.req.Xdzc0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.resp.Xdzc0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0010.Xdzc0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池超短贷校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0010:资产池超短贷校验接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0010Resource.class);

    @Autowired
    private Xdzc0010Service xdzc0010Service;

    /**
     * 交易码：xdzc0010
     * 交易描述：资产池超短贷校验接口
     *
     * @param xdzc0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池超短贷校验接口")
    @PostMapping("/xdzc0010")
    protected @ResponseBody
    ResultDto<Xdzc0010DataRespDto> xdzc0010(@Validated @RequestBody Xdzc0010DataReqDto xdzc0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010DataReqDto));
        Xdzc0010DataRespDto xdzc0010DataRespDto = new Xdzc0010DataRespDto();// 响应Dto:资产池超短贷校验接口
        ResultDto<Xdzc0010DataRespDto> xdzc0010DataResultDto = new ResultDto<>();
        try {
            xdzc0010DataRespDto = xdzc0010Service.xdzc0010Service(xdzc0010DataReqDto);
            xdzc0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, e.getMessage());
            // 封装xdzc0010DataResultDto中异常返回码和返回信息
            xdzc0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0010DataRespDto到xdzc0010DataResultDto中
        xdzc0010DataResultDto.setData(xdzc0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010DataResultDto));
        return xdzc0010DataResultDto;
    }
}
