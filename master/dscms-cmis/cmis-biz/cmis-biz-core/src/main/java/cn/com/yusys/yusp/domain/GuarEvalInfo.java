/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarEvalInfo
 * @类描述: guar_eval_info数据实体类
 * @功能描述: 
 * @创建人: 15430
 * @创建时间: 2020-12-04 21:46:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_eval_info")
public class GuarEvalInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;
	
	/** 评估方式 STD_ZB_EVAL_INOUT_TYPE **/
	@Column(name = "EVAL_IN_OUT_TYPE", unique = false, nullable = true, length = 10)
	private String evalInOutType;
	
	/** 评估类型 STD_ZB_EVAL_TYPE **/
	@Column(name = "EVAL_TYPE", unique = false, nullable = true, length = 10)
	private String evalType;
	
	/** 内部评估方法 STD_ZB_EVAL_METHOD **/
	@Column(name = "EVAL_IN_METHOD_ENNAME", unique = false, nullable = true, length = 10)
	private String evalInMethodEnname;
	
	/** 估值方法选择理由 **/
	@Column(name = "EVAL_METHOD_REASON", unique = false, nullable = true, length = 750)
	private String evalMethodReason;
	
	/** 评估日期/评估基准日 **/
	@Column(name = "EVAL_REAL_DATE", unique = false, nullable = true, length = 10)
	private String evalRealDate;
	
	/** 内部评估价值 **/
	@Column(name = "EVAL_IN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalInAmt;
	
	/** 外部评估价值有效期截止日 **/
	@Column(name = "OUT_END_DATE", unique = false, nullable = true, length = 10)
	private String outEndDate;
	
	/** 外部评估机构名称 **/
	@Column(name = "EVAL_OUT_ORG_NAME", unique = false, nullable = true, length = 100)
	private String evalOutOrgName;
	
	/** 外部评估机构组织机构代码 **/
	@Column(name = "EVAL_OUT_ORG_NO", unique = false, nullable = true, length = 40)
	private String evalOutOrgNo;
	
	/** 是否已取得正式评估报告 STD_ZB_YES_NO **/
	@Column(name = "IS_OUT_PRE_REPORT", unique = false, nullable = true, length = 10)
	private String isOutPreReport;
	
	/** 外部预评估报告的评估价值 **/
	@Column(name = "PRE_OUT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preOutAmt;
	
	/** 外部正式评估报告的评估价值 **/
	@Column(name = "EVAL_OUT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalOutAmt;
	
	/** 币种 STD_ZB_CURRENCY **/
	@Column(name = "EVAL_CURRENCY", unique = false, nullable = true, length = 10)
	private String evalCurrency;
	
	/** 汇率 **/
	@Column(name = "EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRate;
	
	/** 评估费用支付方式 STD_ZB_FEE_PAY **/
	@Column(name = "EVAL_VALUE_PAY", unique = false, nullable = true, length = 4)
	private String evalValuePay;
	
	/** 我行认定价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 评估认定日期 **/
	@Column(name = "EVAL_DATE", unique = false, nullable = true, length = 20)
	private String evalDate;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param evalInOutType
	 */
	public void setEvalInOutType(String evalInOutType) {
		this.evalInOutType = evalInOutType;
	}
	
    /**
     * @return evalInOutType
     */
	public String getEvalInOutType() {
		return this.evalInOutType;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType;
	}
	
    /**
     * @return evalType
     */
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param evalInMethodEnname
	 */
	public void setEvalInMethodEnname(String evalInMethodEnname) {
		this.evalInMethodEnname = evalInMethodEnname;
	}
	
    /**
     * @return evalInMethodEnname
     */
	public String getEvalInMethodEnname() {
		return this.evalInMethodEnname;
	}
	
	/**
	 * @param evalMethodReason
	 */
	public void setEvalMethodReason(String evalMethodReason) {
		this.evalMethodReason = evalMethodReason;
	}
	
    /**
     * @return evalMethodReason
     */
	public String getEvalMethodReason() {
		return this.evalMethodReason;
	}
	
	/**
	 * @param evalRealDate
	 */
	public void setEvalRealDate(String evalRealDate) {
		this.evalRealDate = evalRealDate;
	}
	
    /**
     * @return evalRealDate
     */
	public String getEvalRealDate() {
		return this.evalRealDate;
	}
	
	/**
	 * @param evalInAmt
	 */
	public void setEvalInAmt(java.math.BigDecimal evalInAmt) {
		this.evalInAmt = evalInAmt;
	}
	
    /**
     * @return evalInAmt
     */
	public java.math.BigDecimal getEvalInAmt() {
		return this.evalInAmt;
	}
	
	/**
	 * @param outEndDate
	 */
	public void setOutEndDate(String outEndDate) {
		this.outEndDate = outEndDate;
	}
	
    /**
     * @return outEndDate
     */
	public String getOutEndDate() {
		return this.outEndDate;
	}
	
	/**
	 * @param evalOutOrgName
	 */
	public void setEvalOutOrgName(String evalOutOrgName) {
		this.evalOutOrgName = evalOutOrgName;
	}
	
    /**
     * @return evalOutOrgName
     */
	public String getEvalOutOrgName() {
		return this.evalOutOrgName;
	}
	
	/**
	 * @param evalOutOrgNo
	 */
	public void setEvalOutOrgNo(String evalOutOrgNo) {
		this.evalOutOrgNo = evalOutOrgNo;
	}
	
    /**
     * @return evalOutOrgNo
     */
	public String getEvalOutOrgNo() {
		return this.evalOutOrgNo;
	}
	
	/**
	 * @param isOutPreReport
	 */
	public void setIsOutPreReport(String isOutPreReport) {
		this.isOutPreReport = isOutPreReport;
	}
	
    /**
     * @return isOutPreReport
     */
	public String getIsOutPreReport() {
		return this.isOutPreReport;
	}
	
	/**
	 * @param preOutAmt
	 */
	public void setPreOutAmt(java.math.BigDecimal preOutAmt) {
		this.preOutAmt = preOutAmt;
	}
	
    /**
     * @return preOutAmt
     */
	public java.math.BigDecimal getPreOutAmt() {
		return this.preOutAmt;
	}
	
	/**
	 * @param evalOutAmt
	 */
	public void setEvalOutAmt(java.math.BigDecimal evalOutAmt) {
		this.evalOutAmt = evalOutAmt;
	}
	
    /**
     * @return evalOutAmt
     */
	public java.math.BigDecimal getEvalOutAmt() {
		return this.evalOutAmt;
	}
	
	/**
	 * @param evalCurrency
	 */
	public void setEvalCurrency(String evalCurrency) {
		this.evalCurrency = evalCurrency;
	}
	
    /**
     * @return evalCurrency
     */
	public String getEvalCurrency() {
		return this.evalCurrency;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return exchangeRate
     */
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param evalValuePay
	 */
	public void setEvalValuePay(String evalValuePay) {
		this.evalValuePay = evalValuePay;
	}
	
    /**
     * @return evalValuePay
     */
	public String getEvalValuePay() {
		return this.evalValuePay;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}
	
    /**
     * @return evalDate
     */
	public String getEvalDate() {
		return this.evalDate;
	}
}