/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtDebitAssetMemo;
import cn.com.yusys.yusp.service.LmtDebitAssetMemoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitAssetMemoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 10:57:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtdebitassetmemo")
public class LmtDebitAssetMemoResource {
    @Autowired
    private LmtDebitAssetMemoService lmtDebitAssetMemoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtDebitAssetMemo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtDebitAssetMemo> list = lmtDebitAssetMemoService.selectAll(queryModel);
        return new ResultDto<List<LmtDebitAssetMemo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtDebitAssetMemo>> index(QueryModel queryModel) {
        List<LmtDebitAssetMemo> list = lmtDebitAssetMemoService.selectByModel(queryModel);
        return new ResultDto<List<LmtDebitAssetMemo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtDebitAssetMemo> show(@PathVariable("pkId") String pkId) {
        LmtDebitAssetMemo lmtDebitAssetMemo = lmtDebitAssetMemoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtDebitAssetMemo>(lmtDebitAssetMemo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtDebitAssetMemo> create(@RequestBody LmtDebitAssetMemo lmtDebitAssetMemo) throws URISyntaxException {
        lmtDebitAssetMemoService.insert(lmtDebitAssetMemo);
        return new ResultDto<LmtDebitAssetMemo>(lmtDebitAssetMemo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtDebitAssetMemo lmtDebitAssetMemo) throws URISyntaxException {
        int result = lmtDebitAssetMemoService.update(lmtDebitAssetMemo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtDebitAssetMemoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtDebitAssetMemoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/6/19 10:58
     * @注释 保存或者修改
     */
    @PostMapping("/saveorupdate")
    protected ResultDto<Integer> saveorupdate(@RequestBody LmtDebitAssetMemo lmtDebitAssetMemo) throws URISyntaxException {
        int insert=0;
        if (StringUtils.isEmpty(lmtDebitAssetMemo.getPkId())){
            //空 新增
            insert = lmtDebitAssetMemoService.insert(lmtDebitAssetMemo);
        }else {
            insert = lmtDebitAssetMemoService.update(lmtDebitAssetMemo);
        }

        return new ResultDto<Integer>(insert);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/19 11:12
     * @注释 分页条件查询
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtDebitAssetMemo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtDebitAssetMemo> list = lmtDebitAssetMemoService.selectByModel(queryModel);
        return new ResultDto<List<LmtDebitAssetMemo>>(list);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/6/19 15:17
     * @注释 删除单挑数据
     */
    @PostMapping("/deletebypkid")
    protected ResultDto deletebypkid(@RequestBody LmtDebitAssetMemo lmtDebitAssetMemo) {
        int i = lmtDebitAssetMemoService.deleteByPrimaryKey(lmtDebitAssetMemo.getPkId());
        return new ResultDto(i);
    }
}
