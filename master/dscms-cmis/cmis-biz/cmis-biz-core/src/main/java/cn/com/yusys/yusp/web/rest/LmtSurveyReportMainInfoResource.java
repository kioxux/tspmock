/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.service.LmtCrdReplyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.LmtSurveyReportMainInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportMainInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "客户授信调查主表")
@RequestMapping("/api/lmtsurveyreportmaininfo")
public class LmtSurveyReportMainInfoResource {
    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询-公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyReportMainInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyReportMainInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyReportMainInfo>> index(QueryModel queryModel) {
        List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyReportMainInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{surveySerno}")
    protected ResultDto<LmtSurveyReportMainInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyReportMainInfo>(lmtSurveyReportMainInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyReportMainInfo> create(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) throws URISyntaxException {
        lmtSurveyReportMainInfoService.insert(lmtSurveyReportMainInfo);
        return new ResultDto<LmtSurveyReportMainInfo>(lmtSurveyReportMainInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) throws URISyntaxException {
        int result = lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtSurveyReportMainInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyReportMainInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:HandleAgain
     * @函数描述:根据调查主表主键，自动生成一个新的流水号，赋值原来的信息，更改审批状态，公共API接口
     * @参数与返回说明: 重新办理接口
     * @算法描述:
     */
    @ApiOperation("重新办理接口，公共API接口")
    @PostMapping("/handlebyserno")
    protected ResultDto HandleAgain(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return   lmtSurveyReportMainInfoService.handleAgain(lmtSurveyReportMainInfo);

    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-16 16:46
     * @注释 根据插入字段新增一条数据
     */
    @ApiOperation("惠享待新增接口")
    @PostMapping("/installonedata")
    protected ResultDto<LmtSurveyReportMainInfo> installonedata(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.installonedata(lmtSurveyReportMainInfo);
    }

    /***
     * @author 王玉坤=>王浩
     * @date 2021年6月15日09:26:11 修改参数为对象 String=》
     * @version 1.0.0
     * @desc 调查报告提交审批校验
     * @修改历史: 王浩=>接入参数修改 Map->String
     */
    @ApiOperation("调查报告提交审批校验")
    @PostMapping(value = "/submit")
    public ResultDto<Boolean> submit(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.submit(lmtSurveyReportMainInfo.getSurveySerno());
    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-29 19:39
     * @注释 条件分页查询 -->增加了额外的参数 修改为post请求
     */
    @ApiOperation("分页条件查询")
    @PostMapping("/findlistbymodel")
    protected ResultDto<List<LmtSurveyReportMainInfoAndCrd>> findlistbymodel(@RequestBody QueryModel queryModel) {
        List<LmtSurveyReportMainInfoAndCrd> list = lmtSurveyReportMainInfoService.findlistbymodel(queryModel);
        return new ResultDto(list);
    }
    @ApiOperation("分页条件查询demo")
    @PostMapping("/findlistbymodel2")
    protected ResultDto<List<LmtSurveyReportMainInfo>> findlistbymodel2(@RequestBody QueryModel queryModel) {
        List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyReportMainInfo>>(list);
    }

    /**
     * @创建人 尚智勇
     * @创建时间 2021-04-29 19:39
     * @注释 根据调查流水号查询
     */
    @ApiOperation("根据调查流水号查询")
    @PostMapping("/findlistbysurveyserno/{surveyserno}")
    protected ResultDto<LmtSurveyReportMainInfoAndCrd> findlistbysurveyserno(@PathVariable String surveyserno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("surveySerno", surveyserno);
        List<LmtSurveyReportMainInfoAndCrd> list = lmtSurveyReportMainInfoService.findlistbymodel(queryModel);
        return new ResultDto(list != null && list.size() == 1 ? list.get(0) : null);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/5/20 15:11
     * @注释 测试接口 用于发送报文测试
     */
    @ApiOperation("测试接口")
    @PostMapping("/test0001")
    protected ResultDto demo00120(@RequestBody String serno){
        lmtSurveyReportMainInfoService.endPass(serno);
        return new ResultDto(null);
    }


    @PostMapping("/testydd")
    protected ResultDto testydd(@RequestBody String serno){
        lmtSurveyReportMainInfoService.yddEndPass(lmtSurveyReportMainInfoService.selectByPrimaryKey(serno));
        return new ResultDto(null);
    }
    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto
     * @author hubp
     * @date 2021/5/20 16:46
     * @version 1.0.0
     * @desc 无还本续贷普转，审批通过后，提交至风控二级准入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("无还本续贷普转，审批通过后，提交至风控二级准入")
    @PostMapping("/norepayinfosubmit")
    protected Fbxw01RespDto noRepayInfoSubmit(@RequestBody String surveySerno) {
        Fbxw01RespDto fbxw01RespDto = lmtSurveyReportMainInfoService.noRepayInfoSubmit(surveySerno);
        return fbxw01RespDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/25 11:11
     * @注释 删除单条数据
     */
    @ApiOperation("删除单条数据")
    @PostMapping("/delectdata")
    protected ResultDto delectdata(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return   lmtSurveyReportMainInfoService.delectdata(lmtSurveyReportMainInfo);

    }

    /**
     * @param replySerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/4 16:08
     * @version 1.0.0
     * @desc    授信批复 - 前往额度系统占用额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("授信批复 - 前往额度系统占用额度")
    @PostMapping("/selectreply")
    protected ResultDto selectReply(@RequestBody String replySerno) {
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replySerno);
        return   lmtSurveyReportMainInfoService.handleBizEnd(lmtCrdReplyInfo);
    }

    @PostMapping("/fbxw05")
    protected Fbxw05RespDto fbxw05(@RequestBody LmtCrdReplyInfo lmtCrdReplyInfo) {
        return   lmtSurveyReportMainInfoService.fbxw05(lmtCrdReplyInfo);

    }
    @PostMapping("/znsp04")
    protected Znsp04RespDto znsp04(@RequestBody LmtCrdReplyInfo lmtCrdReplyInfo) {
        return   lmtSurveyReportMainInfoService.znsp04(lmtCrdReplyInfo);

    }
    @PostMapping("/fbxwtest")
    protected Fbxw05RespDto fbxw05test(@RequestBody Fbxw05ReqDto fbxw05ReqDto) {
        return   lmtSurveyReportMainInfoService.fbxw05test(fbxw05ReqDto);

    }
    @PostMapping("/znsptest")
    protected Znsp04RespDto znsp04test(@RequestBody Znsp04ReqDto znsp04ReqDto){
        return lmtSurveyReportMainInfoService.znsp04test(znsp04ReqDto);
    }

    @PostMapping("/imageSum")
    protected ResultDto<Map<String, Integer>> imageSum(@RequestBody ImageDataReqDto imageDataReqDto) {
        return   lmtSurveyReportMainInfoService.imageSum(imageDataReqDto);

    }

    @PostMapping("/yqdEndPass")
    protected ResultDto yqdEndPass(@RequestBody LmtSurveyReportMainInfo mainInfo){
       return lmtSurveyReportMainInfoService.yqdEndPass(mainInfo,mainInfo.getApproveStatus());
    }
    /**
     * @param lmtSurveyReportMainInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     * @author hubp
     * @date 2021/7/15 10:15
     * @version 1.0.0
     * @desc 优抵贷调查报告提交审批校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("优抵贷调查报告提交审批校验")
    @PostMapping(value = "/submitydd")
    public ResultDto submitYdd(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.submitYdd(lmtSurveyReportMainInfo);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/7/23 10:16
     * @注释 优企贷 yqd 提交校验
     */
    @ApiOperation("优企贷调查报告提交审批校验")
    @PostMapping(value = "/submityqd")
    public ResultDto submitYqd(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.submitYqd(lmtSurveyReportMainInfo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/23 10:16
     * @注释 无还本 whb 提交校验
     */
    @ApiOperation("无还本调查报告提交审批校验")
    @PostMapping(value = "/submitwhb")
    public ResultDto submitwhb(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.submitwhb(lmtSurveyReportMainInfo);
    }

    /**
     * @创建人 尚智勇
     * @创建时间 2021/9/08 16:18
     * @注释 增享贷 zxd 提交校验
     */
    @ApiOperation("增享贷调查报告提交审批校验")
    @PostMapping(value = "/submitzxd")
    public ResultDto submitzxd(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.submitzxd(lmtSurveyReportMainInfo);
    }


    /**
     * @param lmtSurveyReportMainInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/20 9:58
     * @version 1.0.0
     * @desc    增享贷提交申请至风控
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("增享贷提交申请至风控")
    @PostMapping("/zxdsubmit")
    protected ResultDto zxdSubmit(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        ResultDto result = lmtSurveyReportMainInfoService.zxdSubmit(lmtSurveyReportMainInfo.getSurveySerno());
        return result;
    }

    /**
     * @param lmtSurveyReportMainInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/10/8 11:18
     * @version 1.0.0
     * @desc  根据调查流水号返回借据流水号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("增享贷借据流水号获取")
    @PostMapping(value = "/getbillno")
    public ResultDto getBillNo(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return lmtSurveyReportMainInfoService.getBillNo(lmtSurveyReportMainInfo.getSurveySerno());
    }
}


