package cn.com.yusys.yusp.service.client.bsp.rircp.fkyx01;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/20 9:08
 * @desc    参考利率测算接口
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Fkyx01Service {
    private static final Logger logger = LoggerFactory.getLogger(Fkyx01Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * 业务逻辑处理方法：参考利率测算接口
     *
     * @param fkyx01ReqDto
     * @return
     */
    @Transactional
    public Fkyx01RespDto fkyx01(Fkyx01ReqDto fkyx01ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value, JSON.toJSONString(fkyx01ReqDto));
        ResultDto<Fkyx01RespDto> fkyx01ResultDto = dscms2RircpClientService.fkyx01(fkyx01ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value, JSON.toJSONString(fkyx01ResultDto));
        String FKYX01Code = Optional.ofNullable(fkyx01ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String FKYX01Meesage = Optional.ofNullable(fkyx01ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fkyx01RespDto fkyx01RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fkyx01ResultDto.getCode())) {
            //  获取相关的值并解析
            fkyx01RespDto = fkyx01ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(FKYX01Code, FKYX01Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value);
        return fkyx01RespDto;
    }


}
