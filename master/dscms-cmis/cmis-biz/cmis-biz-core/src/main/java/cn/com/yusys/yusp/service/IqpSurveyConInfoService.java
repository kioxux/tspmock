/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpSurveyConInfo;
import cn.com.yusys.yusp.repository.mapper.IqpSurveyConInfoMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpSurveyConInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-05-07 10:27:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpSurveyConInfoService {

    @Autowired
    private IqpSurveyConInfoMapper iqpSurveyConInfoMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpSurveyConInfo selectByPrimaryKey(String iqpSerno) {
        return iqpSurveyConInfoMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpSurveyConInfo> selectAll(QueryModel model) {
        List<IqpSurveyConInfo> records = (List<IqpSurveyConInfo>) iqpSurveyConInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpSurveyConInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpSurveyConInfo> list = iqpSurveyConInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpSurveyConInfo record) {
        return iqpSurveyConInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpSurveyConInfo record) {
        return iqpSurveyConInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpSurveyConInfo record) {
        return iqpSurveyConInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpSurveyConInfo record) {
        return iqpSurveyConInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpSurveyConInfoMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpSurveyConInfoMapper.deleteByIds(ids);
    }

    /***
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.domain.IqpSurveyConInfo
     * @author hubp
     * @date 2021/5/7 17:25
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpSurveyConInfo selectByIqpSerno(LmtSurveyReportDto lmtSurveyReportDto) {
        //获取流水号
        String iqpSerno = lmtSurveyReportDto.getIqpSerno();
        IqpSurveyConInfo iqpSurveyConInfo = null;
        if (null == iqpSerno || "".equals(iqpSerno)) {
            iqpSurveyConInfo = new IqpSurveyConInfo();
        } else {
            //通过流水号查出主表信息
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            //通过主表信息中的原流水号查出原调查结论
            iqpSurveyConInfo = iqpSurveyConInfoMapper.selectByPrimaryKey(iqpLoanApp.getOldIqpSerno());
        }
        return iqpSurveyConInfo;
    }
}
