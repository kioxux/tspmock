package cn.com.yusys.yusp.service.server.xdxw0054;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0054.req.Xdxw0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0054.resp.Xdxw0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtPlListInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询优抵贷损益表明细
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0054Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0054Service.class);

    @Autowired
    private LmtPlListInfoMapper lmtPlListInfoMapper;

    /**
     * 查询优抵贷损益表明细
     * @param xdxw0054DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0054DataRespDto queryNetProfit(Xdxw0054DataReqDto xdxw0054DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataReqDto));
        Xdxw0054DataRespDto result = null;
        try {
            result = lmtPlListInfoMapper.queryNetProfit(xdxw0054DataReqDto);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(result));
        return result;
    }


}
