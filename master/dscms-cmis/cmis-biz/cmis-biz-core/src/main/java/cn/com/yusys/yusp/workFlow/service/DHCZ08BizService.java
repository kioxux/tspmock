package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 零售优惠利率申请审批流程业务处理类 --东海村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class DHCZ08BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ08BizService.class);

    @Autowired
    private SGCZ08BizService sGCZ08BizService;
    @Autowired
    private BGYW01BizService bGYW01BizService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //  DHF02 利率定价-二手房按揭贷款 东海村镇
        if(CmisFlowConstants.FLOW_TYPE_TYPE_DHF02.equals(bizType)){
            sGCZ08BizService.iRetailPrimeRateBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_DHH02.equals(bizType) ){
            bGYW01BizService.iqpRateChgAppBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.DHCZ08.equals(flowCode);
    }
}
