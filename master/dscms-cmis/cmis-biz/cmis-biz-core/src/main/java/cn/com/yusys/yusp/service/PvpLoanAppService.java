package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.req.Fb1147ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.resp.Fb1147RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1161.req.Fb1161ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.req.CmisLmt0054ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.resp.CmisLmt0054RespDto;
import cn.com.yusys.yusp.enums.common.MessageNoEnums;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.service.client.bsp.core.dp2021.Dp2021Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3110.Ln3110Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0010.CmisLmt0010Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0013.CmisLmt0013Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-01-05 10:57:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class PvpLoanAppService {

    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppService.class);

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private AccLoanService accLoanService;//借据服务

    @Autowired
    private CtrLoanContService ctrLoanContService;//合同服务

    @Autowired
    private IqpAcctService iqpAcctService;//账户信息

    @Autowired
    private IqpLoanAppApprModeService iqpLoanAppApprModeService;//审批模式服务

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;//授权信息

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;//合同与担保合同关系表服务

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;//担保合同与押品关系数据

    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同表服务

    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;

    @Autowired
    private Ln3110Service ln3110Service;

    @Autowired
    private ToppAcctSubMapper toppAcctSubMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private PvpEntrustLoanAppMapper pvpEntrustLoanAppMapper;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;


    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    @Resource
    private ICusClientService icusClientService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;
    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private ToppAcctSubService toppAcctSubService;


    @Autowired
    private PvpSehandEsInfoService pvpSehandEsInfoService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private PvpLoanAppRepayBillRellService pvpLoanAppRepayBillRellService;

    @Autowired
    private IqpHouseService iqpHouseService;

    @Autowired
    private LmtCobInfoMapper lmtCobInfoMapper;

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private CmisLmt0010Service cmisLmt0010Service;

    @Autowired
    private CmisLmt0013Service cmisLmt0013Service;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private Dp2021Service dp2021Service;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Value("${application.xwdxt.url}")
    private String url;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PvpLoanApp selectByPrimaryKey(String pvpSerno) {
        return pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PvpLoanApp> selectAll(QueryModel model) {
        List<PvpLoanApp> records = (List<PvpLoanApp>) pvpLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PvpLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpLoanApp> list = pvpLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpLoanApp record) {
        return pvpLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(PvpLoanApp record) {
        return pvpLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpLoanApp record) {
        return pvpLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(PvpLoanApp record) {
        return pvpLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pvpSerno) {
        return pvpLoanAppMapper.deleteByPrimaryKey(pvpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpLoanAppMapper.deleteByIds(ids);
    }


    /**
     * 校验前端传递的数据
     *
     * @param param
     * @return
     */
    private boolean checkPvpBizData(Map param) {
        boolean bizCheckFlag = false;
        try {
            String pvpSerno = (String) param.get("pvpSerno");
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key
                        , EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value);
            }
            String logPrefix = "放款申请" + pvpSerno;
            //1、针对放款到期日，需要控制 放款到期日不得超过合同到期日后半年
            String endDate = (String) param.get("endDate");
            if (StringUtils.isBlank(endDate)) {
                log.error(logPrefix + "放款到期日为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "放款到期日为空！");
            }
            log.info(logPrefix + "校验放款到期日与合同到期日");
            String contEndDate = (String) param.get("contEndDate");
            if (StringUtils.isBlank(contEndDate)) {
                log.error(logPrefix + "合同到期日为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "合同到期日为空！");
            }
            String compareDate = DateUtils.addMonth(contEndDate, DateFormatEnum.DEFAULT.getValue(), 6);
            log.info(logPrefix + "放款到期日：" + endDate + "合同到期日后半年：" + compareDate);
            int subDay = DateUtils.getDaysByTwoDates(compareDate, DateFormatEnum.DEFAULT.getValue(), endDate, DateFormatEnum.DEFAULT.getValue());
            if (subDay > 0) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_CONTDATESMALL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_CONTDATESMALL_EXCEPTION.value);
            }

            log.info(logPrefix + "校验放款到期日与授信额度到期日");
            String bizType = (String) param.get("bizType");
            if (StringUtils.isBlank(bizType)) {
                log.error(logPrefix + "业务类型为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "业务类型为空！");
            }
            log.info(logPrefix + "校验放款到期日与授信额度到期日-业务类型：" + bizType);

            //针对额度项下以及特殊业务申请，业务申请时一定会选取授信额度，因此需要校验放款到期日是否超过授信到期日
            if (!CmisBizConstants.BIZ_TYPE_SIGNLE.equals(bizType)) {
                String lmtEndDate = (String) param.get("lmtEndDate");
                if (StringUtils.isBlank(lmtEndDate)) {
                    log.error(logPrefix + "【授信到期日】为空！");
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                            EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "额度协议【授信到期日】为空！");
                }
                log.info(logPrefix + "放款到期日：" + endDate + "授信额度到期日：" + lmtEndDate);
                subDay = DateUtils.getDaysByTwoDates(lmtEndDate, DateFormatEnum.DEFAULT.getValue(), endDate, DateFormatEnum.DEFAULT.getValue());
                if (subDay > 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_LMTDATESMALL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_LMTDATESMALL_EXCEPTION.value);
                }
            }
            //针对单批单批以及额度项下业务，业务申请时可能选取第三方额度，因此需要校验放款到期日是否超过第三方额度到期日
            log.info(logPrefix + "校验放款到期日与第三方额度到期日");
            String thLmtEndDate = (String) param.get("th_limit_end_date");
            if (!CmisBizConstants.BIZ_TYPE_SPECIL.equals(bizType) && !StringUtils.isBlank(thLmtEndDate)) {
                log.info(logPrefix + "放款到期日：" + endDate + "第三方额度到期日：" + thLmtEndDate);
                subDay = DateUtils.getDaysByTwoDates(thLmtEndDate, DateFormatEnum.DEFAULT.getValue(), endDate, DateFormatEnum.DEFAULT.getValue());
                if (subDay > 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_LMTDATESMALL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_LMTDATESMALL_EXCEPTION.value);
                }
            }
            //获取放款金额
            log.info(logPrefix + "校验放款金额与合同余额");
            String pvpAmtStr = (String) param.get("pvpAmt");
            if (StringUtils.isBlank(pvpAmtStr)) {
                log.error(logPrefix + "【放款金额】为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "【放款金额】为空！");
            }
            BigDecimal pvpAmt = new BigDecimal((String) param.get("pvpAmt"));

            //获取合同余额-获取合同下所有放款未确认的借据金额
            log.info(logPrefix + "校验放款金额与合同余额");
            String contNo = (String) param.get("contNo");
            if (StringUtils.isBlank(contNo)) {
                log.error(logPrefix + "【合同编号】为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "【合同编号】为空！");
            }
            log.info(logPrefix + "校验放款金额与合同余额-获取合同余额数据");
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            if (ctrLoanCont == null) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_CONTNULL_EXCEPTION.value);
            }
            BigDecimal contBalance = ctrLoanCont.getContBalance();
            if (contBalance == null || contBalance.compareTo(new BigDecimal(0)) == 0) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_CONTAMTZERO_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_CONTAMTZERO_EXCEPTION.value);
            }
            log.info(logPrefix + "校验放款金额与合同余额-获取放款未确认的借据台账数据");
            Map accMap = new HashMap();
            accMap.put("contNo", contNo);//合同编号
            accMap.put("accStatus", CmisBizConstants.IQP_ACC_STATUS_0);//借据状态为【放款未确认】
            List<AccLoan> accLoanList = accLoanService.selectAccLoanByParams(accMap);
            if (CollectionUtils.nonEmpty(accLoanList) && accLoanList.size() > 0) {
                for (AccLoan accLoan : accLoanList) {
                    contBalance = contBalance.subtract(accLoan.getLoanAmt());
                }
            }

            log.info(logPrefix + "校验放款金额与合同余额-放款金额：" + pvpAmtStr + "合同余额：" + contBalance.toString());
            if (contBalance.compareTo(new BigDecimal(0)) < 0 || contBalance.compareTo(pvpAmt) < 0) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_CONTAMTSMALL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_CONTAMTSMALL_EXCEPTION.value);
            }

            //判断是否受托支付，若是受托支付，则获取业务流水号，通过业务流水号查询账户列表中，账户类型为受托支付的数据，并计算累加金额是否能够覆盖放款金额
            log.info(logPrefix + "校验放款金额与受托支付账户总金额");
            String payWay = (String) param.get("payWay");
            String iqpSerno = (String) param.get("iqpSerno");
            if (StringUtils.isBlank(payWay)) {
                log.error(logPrefix + "支付方式为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "支付方式为空！");
            }
            if (StringUtils.isBlank(iqpSerno)) {
                log.error(logPrefix + "业务申请流水号为空！");
                throw new YuspException(EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.key,
                        EcbEnum.E_PVP_UPDATE_PARAMS_EXCEPTION.value + "业务申请流水号为空！");
            }
            log.info(logPrefix + "校验放款金额与受托支付账户总金额-获取受托支付账户总金额");
            if (CmisBizConstants.PAY_WAY_ENTRUSTEDPAYMENT.equals(payWay)) {
                List<IqpAcct> iqpAcctList = iqpAcctService.selectAffectInfoByIqpSerno(iqpSerno);
                if (CollectionUtils.isEmpty(iqpAcctList) || iqpAcctList.size() == 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_ACCTNULL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_ACCTNULL_EXCEPTION.value);
                }
                BigDecimal totalAcctAmt = new BigDecimal(0);
                for (IqpAcct iqpAcct : iqpAcctList) {
                    //若是为【受托支付】，金额累加
                    if (CmisBizConstants.ID_ATTR_EP.equals(iqpAcct.getAcctAttr())) {
                        totalAcctAmt = totalAcctAmt.add(iqpAcct.getPayAmt());
                    }
                }
                log.info(logPrefix + "放款金额：" + pvpAmtStr + "受托支付账户总金额：" + totalAcctAmt.toString());
                if (pvpAmt.compareTo(totalAcctAmt) != 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_ACCTAMTNOTEQUALS_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_ACCTAMTNOTEQUALS_EXCEPTION.value);
                }
            }

            //20210309 放款申请提交流程前需要进行以下校验
            /* 申请提交时，需校验关联的权证是否入库，校验规则如下：
             * (1). 校验该放款申请关联的 担保合同下的每个押品都存在有效权证；
             * (2). 校验该放款申请关联的 担保合同下的每个押品都不存在 “未入库”、“入库在途”、“入库未记账”状态的权证；
             * (3). 校验该放款申请关联的 担保合同下的每个押品中“入库已记账”的他项权证（权证类别=他项权证）数=当前担保关联的有效业务数；
             *  因他项权证是只有存在合同的场景下才能办理，因此有效业务仅考虑业务申请通过的数据，不需要考虑在途业务
             *
             * 20210324 第三项校验调整为
             *  校验该放款申请关联的 担保合同下的每个押品中“入库已记账”的他项权证（权证类别=他项权证）数=该押品关联的有效担保合同数；
             */
            //4、逻辑判断
            //20210413  针对信用，未引入担保合同，无需校验
            String guarWay = ctrLoanCont.getGuarWay();
            if (!CmisBizConstants.GUAR_WAY_400.equals(guarWay)) {
                log.info(logPrefix + "获取合同下担保合同数据");
                Map queryMap = new HashMap();
                queryMap.put("contNo", (String) param.get("contNo"));
                queryMap.put("unSecure", "true");
                //1、通过放款申请中的合同编号查询合同与担保合同关系结果表数据，获取担保合同号-biz   grt_guar_biz_rst_rel
                List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectByParams(queryMap);
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList) || grtGuarBizRstRelList.size() == 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_GUARCONTNULL.key, EcbEnum.E_PVP_UPDATE_GUARCONTNULL.value);
                }
                String guarContNos = "";
                for (int i = 0; i < grtGuarBizRstRelList.size(); i++) {
                    guarContNos += grtGuarBizRstRelList.get(i).getGuarContNo() + ",";
                }
                log.info(logPrefix + "获取担保合同下押品数据");
                //2、通过担保合同号获取押品数据-biz  grt_guar_cont_rel
                queryMap.put("guarContNo", guarContNos);
                List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectGrtGuarContRelByGuarNo(queryMap);
                if (CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_GUARNULL.key, EcbEnum.E_PVP_UPDATE_GUARNULL.value);
                }
                List<Map> guarNos = new ArrayList<>();
                for (int i = 0; i < grtGuarContRelList.size(); i++) {
                    Map guarNoMap = new HashMap();
                    guarNoMap.put("guarNo", grtGuarContRelList.get(i).getGuarNo());
                }

                log.info(logPrefix + "调用押品服务接口获取押品下有效的权证数据");
                //3、通过押品编号获取权证数据-guar   guar_certi_rela
                GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
                guarCertiRelaClientDto.setList(guarNos);
                GuarClientRsDto guarClientRsDto = null;// iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
                if (guarClientRsDto == null || !CmisCommonConstants.INTERFACE_SUCCESS_CODE.equals(guarClientRsDto.getRtnCode())) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_CERTINULL.key, EcbEnum.E_PVP_UPDATE_CERTINULL.value);
                }

                List<GuarCertiRelaClientDto> certiDatas = (List<GuarCertiRelaClientDto>) guarClientRsDto.getData();
                if (CollectionUtils.isEmpty(certiDatas) || certiDatas.size() == 0) {
                    throw new YuspException(EcbEnum.E_PVP_UPDATE_CERTINULL.key, EcbEnum.E_PVP_UPDATE_CERTINULL.value);
                }

                log.info(logPrefix + "权证规则校验");
                //4、权证规则校验
                for (int i = 0; i < guarNos.size(); i++) {
                    String guarNo = (String) guarNos.get(i).get("guarNo");
                    int effectCount = 0;//有效权证数
                    int statusCount = 0;// “未入库”、“入库在途”、“入库未记账”状态的权证数
                    int statusOtherCount = 0;//入库已记账的他项权证数
                    int grtGuarConts = 0;//押品下生效的担保合同数
                    for (int j = 0; j < certiDatas.size(); j++) {
                        GuarCertiRelaClientDto rtnDto = certiDatas.get(j);
                        if (rtnDto.getGuarNo().equals(guarNo)) {
                            effectCount += 1;
                            String certStatus = rtnDto.getCertiState();
                            if (CmisBizConstants.IQP_CERTI_STATU_01.equals(certStatus) || CmisBizConstants.IQP_CERTI_STATU_02.equals(certStatus)
                                    || CmisBizConstants.IQP_CERTI_STATU_03.equals(certStatus)) {
                                statusCount += 1;
                            }

                            String certiCatalog = rtnDto.getCertiCatalog() == null ? "" : rtnDto.getCertiCatalog();
                            if (CmisBizConstants.IQP_CERTI_STATU_04.equals(certStatus) && CmisBizConstants.CERTI_CATALOG_02.equals(certiCatalog)) {
                                statusOtherCount += 1;
                            }
                        }
                    }

                    //逻辑判断，是否存在有效的权证数据
                    if (effectCount == 0) {
                        throw new YuspException(EcbEnum.E_PVP_UPDATE_CERTIINEFFECT.key, EcbEnum.E_PVP_UPDATE_CERTIINEFFECT.value);
                    }
                    //逻辑判断，是否存在未入库的数据
                    if (statusCount > 0) {
                        throw new YuspException(EcbEnum.E_PVP_UPDATE_CERTIINAPPR.key, EcbEnum.E_PVP_UPDATE_CERTIINAPPR.value);
                    }

                    //获取押品下生效的担保合同数
                    Map relMap = new HashMap();
                    relMap.put("guarNo", guarNo);
                    List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.selectGrtGuarContRelByGuarNo(relMap);
                    if (CollectionUtils.nonEmpty(grtGuarContRels) && grtGuarContRels.size() > 0) {
                        String guarConts = "";
                        for (int j = 0; j < grtGuarContRels.size(); j++) {
                            guarConts += grtGuarContRels.get(j).getGuarContNo();
                        }

                        //获取押品下的担保合同是否都为有效状态的数据
                        Map ggcMap = new HashMap();
                        ggcMap.put("guarContNos", guarConts);
                        ggcMap.put("guarContState", CmisBizConstants.GUAR_CONT_STATE_00 + CmisCommonConstants.COMMON_SPLIT_COMMA + CmisBizConstants.GUAR_CONT_STATE_01);
                        List<GrtGuarCont> grtGuarContList = grtGuarContService.queryByParams(ggcMap);
                        if (CollectionUtils.nonEmpty(grtGuarContList)) {
                            grtGuarConts = grtGuarContList.size();
                        }
                    }

                    //若是他项权证数不等于押品下生效的担保合同数据，则抛出异常
                    if (grtGuarConts != statusOtherCount) {
                        throw new YuspException(EcbEnum.E_PVP_UPDATE_OTHERCERTIINCHECK.key, EcbEnum.E_PVP_UPDATE_OTHERCERTIINCHECK.value);
                    }
                }
            }
            bizCheckFlag = true;
        } catch (Exception e) {
            log.info("保存放款申请数据-校验业务数据出现异常！", e);
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "，" + e.getMessage());
        }
        return bizCheckFlag;
    }

    /**
     * 通过放款申请流水号更新放款申请状态
     *
     * @param pvpSerno
     * @param updateApproveStatus
     */
    public int updateApproveStatus(String pvpSerno, String updateApproveStatus) {
        return pvpLoanAppMapper.updateApproveStatus(pvpSerno, updateApproveStatus);
    }

    /**
     * 放款申请提交流程，后续的业务处理
     * 0、根据业务类型判断是否【单笔单批申请】，若是单笔单批申请，更新业务审批模式表中的全流程状态
     * 1、更新放款申请主表的流程状态
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请发起流程-获取放款申请" + pvpSerno + "申请信息");
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
            if (pvpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            String bizType = "";
            if (Objects.equals(CmisCommonConstants.WF_STATUS_000, pvpLoanApp.getApproveStatus())) {
                //获取合同数据，并根据合同中的业务类型进行数据判断
                log.info("放款申请发起流程-获取放款申请" + pvpSerno + "获取业务合同数据");
                String serno = "";
                CtrLoanCont ctrLoanCont = null;
                CtrHighAmtAgrCont ctrHighAmtAgrCont = null;
                ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
                if (ctrLoanCont == null) {
                    ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(pvpLoanApp.getContNo());
                    if (ctrHighAmtAgrCont == null) {
                        throw new YuspException(EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.value);
                    } else {
                        serno = ctrHighAmtAgrCont.getSerno();
                        bizType = ctrHighAmtAgrCont.getBusiType();
                    }
                } else {
                    serno = ctrLoanCont.getIqpSerno();
                    bizType = ctrLoanCont.getBizType();
                }


                //更新流程审批状态
                log.info("放款申请发起流程-更新放款申请" + pvpSerno + "流程状态-审批中");
                pvpLoanApp.setIsAutoAppr(CmisCommonConstants.STD_ZB_YES_NO_0);
                pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                int updCount = this.updateSelective(pvpLoanApp);
                if (updCount <= 0) {
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }

                //判断是否低风险占额
                String guarMode = pvpLoanApp.getGuarMode();
                BigDecimal BizAmtCny = BigDecimal.ZERO;
                BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.STD_BUSI_TYPE_05.equals(bizType)) {
                    BizAmtCny = pvpLoanApp.getCvtCnyAmt();
                    bizSpacAmtCny = BigDecimal.ZERO;
                } else {
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                            || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                        BizAmtCny = pvpLoanApp.getCvtCnyAmt();
                        bizSpacAmtCny = BigDecimal.ZERO;
                    } else {
                        BizAmtCny = pvpLoanApp.getCvtCnyAmt();
                        bizSpacAmtCny = pvpLoanApp.getCvtCnyAmt();
                    }
                }
                // 判断是否无缝衔接
                String isFollowBiz = "";
                // 原交易业务编号
                String origiDealBizNo = "";
                // 原交易业务状态
                String origiDealBizStatus = "";
                // 原交易恢复类型
                String origiRecoverType = "";
                // 原交易属性
                String origiBizAttr = "";
                if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(pvpLoanApp.getLoanModal())
                        || CmisCommonConstants.STD_LOAN_MODAL_6.equals(pvpLoanApp.getLoanModal())
                        || CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())) {
                    isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_1;
                } else {
                    isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
                }

                //发台账占用接口占合同额度
                CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
                cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0013ReqDto.setSerno(pvpLoanApp.getIqpSerno());//交易流水号
                cmisLmt0013ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号
                cmisLmt0013ReqDto.setInputId(pvpLoanApp.getInputId());//登记人
                cmisLmt0013ReqDto.setInputBrId(pvpLoanApp.getInputBrId());//登记机构
                cmisLmt0013ReqDto.setInputDate(pvpLoanApp.getInputDate());//登记日期

                List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDto = new ArrayList<CmisLmt0013ReqDealBizListDto>();
                CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizDto = new CmisLmt0013ReqDealBizListDto();
                cmisLmt0013ReqDealBizDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
                cmisLmt0013ReqDealBizDto.setIsFollowBiz(isFollowBiz);//是否无缝衔接
                cmisLmt0013ReqDealBizDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0013ReqDealBizDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0013ReqDealBizDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0013ReqDealBizDto.setOrigiBizAttr(origiBizAttr);//原交易属性
                cmisLmt0013ReqDealBizDto.setCusId(pvpLoanApp.getCusId());//客户编号
                cmisLmt0013ReqDealBizDto.setCusName(pvpLoanApp.getCusName());//客户名称
                cmisLmt0013ReqDealBizDto.setPrdId(pvpLoanApp.getPrdId());//产品编号
                cmisLmt0013ReqDealBizDto.setPrdName(pvpLoanApp.getPrdName());//产品名称
                cmisLmt0013ReqDealBizDto.setPrdTypeProp(pvpLoanApp.getPrdTypeProp());//产品类型属性
                cmisLmt0013ReqDealBizDto.setDealBizAmtCny(BizAmtCny);//台账占用总额(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizSpacAmtCny(bizSpacAmtCny);//台账占用敞口(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                cmisLmt0013ReqDealBizDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
                cmisLmt0013ReqDealBizDto.setStartDate(pvpLoanApp.getLoanStartDate());//台账起始日
                cmisLmt0013ReqDealBizDto.setEndDate(pvpLoanApp.getLoanEndDate());//台账到期日
                cmisLmt0013ReqDealBizDto.setDealBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);//台账状态
                cmisLmt0013ReqDealBizListDto.add(cmisLmt0013ReqDealBizDto);
                cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDto);
                log.info("放款申请【{}】前往额度系统进行额度占用开始,请求报文为【{}】", pvpSerno, cmisLmt0013ReqDealBizDto.toString());
                ResultDto<CmisLmt0013RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
                log.info("放款申请【{}】前往额度系统进行额度占用结束,响应报文为【{}】", pvpSerno, resultDtoDto);
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("接口调用异常！");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(resultDtoDto.getData().getErrorCode())) {
                    log.error("业务申请占用额度异常,错误信息为【{}】！", resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null, resultDtoDto.getData().getErrorCode(), resultDtoDto.getData().getErrorMsg());
                }
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程发起业务处理异常！原因：" + e.getMessage());
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
    }

    /**
     * 通过放款申请流水号更新放款申请状态
     * 释放额度
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterCallBack(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + pvpSerno + "申请信息");
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);

            if (pvpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            //恢复额度后，更新流程审批状态
            int updCount = 0;
            log.info("放款申请发起流程-更新放款申请" + pvpSerno + "流程状态-退回");
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
            updCount = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            if (updCount <= 0) {
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
    }

    /**
     * 放款申请通过，后续的业务处理
     * 1、针对单笔单批业务，更新审批模式子表中的流程状态
     * 2、生成借据数据，状态为【放款待确认】
     * 0、放款申请主表申请状态修改为通过-997
     * 3、根据支付方式生成对应的授权记录
     * 4、若是受托支付，进行受托支付业务处理
     * 5、获取账户列表数据，生成账号信息与借据关系表
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ResultDto handleBusinessAfterEnd(String pvpSerno) {
        int updateCount = 0;
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款审批通过-获取放款申请" + pvpSerno + "申请信息");
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
            if (pvpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            //获取合同数据，并根据合同中的业务类型进行数据判断
            log.info("放款审批通过-获取放款申请" + pvpSerno + "获取业务合同数据");

//            Map seqMap = new HashMap();
//            seqMap.put("contNo", pvpLoanApp.getContNo());
//            String bill_no = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, seqMap);
            String bill_no = pvpLoanApp.getBillNo();
            log.info("放款审批通过-放款申请" + pvpSerno + "生成授权信息");
            PvpAuthorize pvpAuthorize = generatePvpAuthorizeInfo(pvpSerno, bill_no);

            log.info("放款审批通过-放款申请" + pvpSerno + "生成借据信息");
            AccLoan accLoan = generateAccLoanInfo(pvpLoanApp, bill_no);

            int resultAu = pvpAuthorizeService.insert(pvpAuthorize);
            int resultAc = accLoanService.insert(accLoan);

            log.info("流程发起-更新普通贷款申请" + pvpSerno + "流程审批状态为【997】-审批结束");
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            pvpLoanApp.setBillNo(accLoan.getBillNo());//借据编号
            updateCount = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);

            // 系统通知
            CmisCfg0001ReqDto cmisCfg0001ReqDto = new CmisCfg0001ReqDto();
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, new HashMap<>());
            cmisCfg0001ReqDto.setSerno(serno);
            cmisCfg0001ReqDto.setMessageType(CmisBizConstants.STD_WB_NOTICE_TYPE_2);//审批通过
            cmisCfg0001ReqDto.setPubTime(DateUtils.getCurrTime());
            cmisCfg0001ReqDto.setReceiverType(null);
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            cmisCfg0001ReqDto.setInputDate(openday);
            cmisCfg0001ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisCfg0001ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisCfg0001ReqDto.setContent(pvpLoanApp.getCusName() + "(" + pvpLoanApp.getCusId() + ")的" + pvpLoanApp.getPrdName() + "放款申请已完成，审批结果为【通过】");
            log.info("根据放款申请编号【{}】，审批操作生成首页提醒！", pvpSerno);
            ResultDto<CmisCfg0001RespDto> cmisCfg0001RespDto = dscmsCfgQtClientService.cmisCfg0001(cmisCfg0001ReqDto);
            log.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key,
                    DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, JSON.toJSONString(cmisCfg0001RespDto));
            if (cmisCfg0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisCfg0001RespDto.getData().getErrorCode())) {
                log.info("根据放款申请编号【{}】,生成首页提醒成功！", pvpSerno);
            } else {
                log.info("根据放款申请编号【{}】,生成首页提醒成功失败！", pvpSerno);
            }

            // 征信贷：在出账完成后，将借据起始日、到期日、金额、借款人名称信息自动推送给征信苏州
            String prdTypeProp = pvpLoanApp.getPrdTypeProp();
            if ("P009".equals(prdTypeProp)) {
                CusCorpDto cusCorpDto = icusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId()).getData();
                String mobile = "";
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    mobile = cusCorpDto.getFreqLinkmanTel();
                }
                MessageSendDto messageSendDto = new MessageSendDto();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                messageSendDto.setMessageType(CmisCommonConstants.PVP_SEND_MESSAGE_TYPE_0002);
                receivedUserDto.setReceivedUserType("2");
                receivedUserDto.setUserId(pvpLoanApp.getManagerId());
                receivedUserDto.setMobilePhone(mobile);
                messageSendDto.setReceivedUserList(Arrays.asList(receivedUserDto));
                Map<String, String> param = new HashMap<>();
                param.put("cusName", pvpLoanApp.getCusName());
                param.put("loanStartDate", pvpLoanApp.getLoanStartDate());
                param.put("loanEndDate", pvpLoanApp.getLoanEndDate());
                param.put("pvpAmt", pvpLoanApp.getPvpAmt().toString());
                messageSendDto.setParams(param);
                messageSendService.sendMessage(messageSendDto);
            }
            // 判断是否自动出账 STD_PVP_MODE
            try {
                if ("1".equals(pvpLoanApp.getPvpMode())) {
                    log.info("对公贷款自动出账" + pvpSerno);
                    ResultDto pvpResultDto = pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
                    if (Objects.nonNull(pvpResultDto)) {
                        if (Objects.equals("0", pvpResultDto.getCode())) {
                            log.info("对公贷款自动出账成功" + pvpSerno);
                            return new ResultDto(null).message("自动出账成功").code("0");
                        } else {
                            return pvpResultDto;
                        }
                    }
                }
                // 出账成功后微信通知客户经理
                MessageSendDto messageSendDto = new MessageSendDto();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                messageSendDto.setMessageType(CmisCommonConstants.PVP_SEND_MESSAGE_TYPE_0001);
                receivedUserDto.setReceivedUserType("1");
                receivedUserDto.setUserId(pvpLoanApp.getManagerId());
                messageSendDto.setReceivedUserList(Arrays.asList(receivedUserDto));
                Map<String, String> param = new HashMap<>();
                param.put("cusName", pvpLoanApp.getCusName());
                param.put("prdName", pvpLoanApp.getPrdName());
                param.put("time", DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                param.put("pvpAmt", pvpLoanApp.getPvpAmt().toString());
                messageSendDto.setParams(param);
                messageSendService.sendMessage(messageSendDto);
            } catch (BizException e) {
                log.info("对公贷款自动出账" + pvpSerno + "失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            } catch (Exception e) {
                log.info("对公贷款自动出账" + pvpSerno + "失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款审批通过-放款申请" + pvpSerno + "业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
        return new ResultDto(null).message("出账申请审批通过").code("0");
    }

    /**
     * 发送fb1161接口
     *
     * @param pvpLoanApp
     * @param openday
     * @return
     */
    public void sendFb1161(PvpLoanApp pvpLoanApp, String openday) {
        CusBaseClientDto cusBaseClientDto = icusClientService.queryCus(pvpLoanApp.getCusId());
        Fb1161ReqDto fb1161ReqDto = new Fb1161ReqDto();
        AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(pvpLoanApp.getManagerId());
        AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(pvpLoanApp.getManagerBrId());
        fb1161ReqDto.setBILL_NO(pvpLoanApp.getBillNo());
        fb1161ReqDto.setCN_CONT_NO("空"); // 许晓确认空
        fb1161ReqDto.setCHANNEL_TYPE("13");//线上
        fb1161ReqDto.setCERT_TYPE("10");//证件类型
        fb1161ReqDto.setCERT_CODE(cusBaseClientDto.getCertCode());//身份证号
        fb1161ReqDto.setLOAN_END_DATE(pvpLoanApp.getLoanEndDate());//贷款结束时间
        fb1161ReqDto.setREPAY_TYPE(pvpLoanApp.getRepayMode());//还款方式
        fb1161ReqDto.setMANAGER_NAME(Objects.isNull(adminSmUserDto) ? "" : adminSmUserDto.getUserName());//客户经理名称
        fb1161ReqDto.setFIXED_RATE_FLAG("Y");//是否固定利率
        fb1161ReqDto.setCUST_ID_CORE(pvpLoanApp.getCusId());//核心客户号
        fb1161ReqDto.setCONT_STATUS("105");//合同状态
        fb1161ReqDto.setCO_PLATFORM("2002");//合作平台
        fb1161ReqDto.setCURRENCY(pvpLoanApp.getCurType());//币种
        fb1161ReqDto.setLOAN_AMT(pvpLoanApp.getPvpAmt());//贷款金额
        fb1161ReqDto.setSIGN_TIME(openday);//签订时间
        fb1161ReqDto.setRATE_ADJ_TYPE("1");//利率调整方式
        fb1161ReqDto.setP_RATE_TYPE("1");//罚息类型
        fb1161ReqDto.setREPAY_CARD_NO(pvpLoanApp.getLoanPayoutAccno());//还款卡号
        fb1161ReqDto.setMANAGER_ID(pvpLoanApp.getManagerId());//客户经理工号
        fb1161ReqDto.setCRD_CONT_NO(pvpLoanApp.getContNo());//合同号
        fb1161ReqDto.setREPAY_DATE_TYPE("1");//还款日类型
        fb1161ReqDto.setP_RATE_FLOAT(pvpLoanApp.getOverdueRatePefloat());//逾期利率
        fb1161ReqDto.setREPAY_ACC(pvpLoanApp.getLoanPayoutAccno());//还款账号
        fb1161ReqDto.setPRD_CODE("2002000001");// 产品类型
        fb1161ReqDto.setMANAGER_ORG_NAME(Objects.isNull(adminSmOrgDto) ? "" : adminSmOrgDto.getOrgName());// 机构名称
        fb1161ReqDto.setLOAN_TERM(pvpLoanApp.getLoanTerm());// 还款期限
        fb1161ReqDto.setMANAGER_ORG_ID(pvpLoanApp.getManagerBrId());// 机构ID
        fb1161ReqDto.setREPAY_DAY(pvpLoanApp.getDeductDay());//扣款日期
        fb1161ReqDto.setEXEC_RATE(pvpLoanApp.getExecRateYear());// 执行年利率
        fb1161ReqDto.setINTEREST_CYCLE(pvpLoanApp.getEiIntervalCycle());//计息周期
        fb1161ReqDto.setLOAN_PURPOSE("10");//贷款用途
        fb1161ReqDto.setCUST_NAME(pvpLoanApp.getCusName());//客户姓名
        fb1161ReqDto.setLOAN_START_DATE(pvpLoanApp.getLoanStartDate());//开始时间
        dscms2CircpClientService.fb1161(fb1161ReqDto);
    }

    /**
     * 通过申请以及借据信息生成受托支付数据
     *
     * @param pvpLoanApp
     * @param accLoan
     * @param iqpAcct
     * @return
     */
    private PvpTruPayInfo generatePvpTruPayInfo(PvpLoanApp pvpLoanApp, AccLoan accLoan, IqpAcct iqpAcct) {
        PvpTruPayInfo pvpTruPayInfo = new PvpTruPayInfo();
        if (accLoan == null || pvpLoanApp == null || iqpAcct == null) {
            throw new YuspException(EcbEnum.E_PVP_WFHAND_GETPTPI_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_GETPTPI_EXCEPTION.value);
        }
        String pvpSerno = pvpLoanApp.getPvpSerno();
        String ptpiSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PTPI_TRAN_SERNO, new HashMap());
        log.info("放款审批通过-放款申请" + pvpSerno + "生成受托支付信息-受托支付交易流水号:" + ptpiSerno);
        pvpTruPayInfo.setTranSerno(ptpiSerno);//交易流水号
        pvpTruPayInfo.setPvpSerno(pvpSerno);//放款编号
        pvpTruPayInfo.setContNo(pvpLoanApp.getContNo());//合同编号
        pvpTruPayInfo.setBillNo(accLoan.getBillNo());//借据编号
        pvpTruPayInfo.setCusId(pvpLoanApp.getCusId());//客户编号
        pvpTruPayInfo.setPrdId(pvpLoanApp.getPrdId());//产品编号
        pvpTruPayInfo.setAcctNo(iqpAcct.getAcctNo());//账户编号
        pvpTruPayInfo.setAcctName(iqpAcct.getAcctName());//账户名称
        pvpTruPayInfo.setCurType(accLoan.getContCurType());//贷款发放币种
        pvpTruPayInfo.setTranAmt(iqpAcct.getPayAmt());//交易金额
        pvpTruPayInfo.setTranDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//交易日期
        pvpTruPayInfo.setManagerBrId(accLoan.getManagerBrId());//主办机构
        pvpTruPayInfo.setManagerId(accLoan.getManagerId());//主办人
        pvpTruPayInfo.setAcctBrId(pvpLoanApp.getFinaBrId());//放款机构  TODO 2021年7月5日15:49:26 这个字段重复要被删除掉了  WH
        pvpTruPayInfo.setPayStatus(CmisBizConstants.PVP_TRU_PAY_INFO_PAY_STATUS_00);//支付状态默认为【未支付】
        return pvpTruPayInfo;
    }

    /**
     * 通过放款申请以及借据信息生成授权信息数据
     *
     * @param pvpSerno
     * @param bill_no
     * @return
     */
    private PvpAuthorize generatePvpAuthorizeInfo(String pvpSerno, String bill_no) {
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        Map seqMap = new HashMap();
        String tran_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
        pvpAuthorize.setTranSerno(tran_serno);
        pvpAuthorize.setPvpSerno(pvpLoanApp.getPvpSerno());                    // 出账号
        String monthDiffer = pvpLoanApp.getLoanTerm();            //返回贷款到期日和起始日相差的月数
        pvpAuthorize.setAuthorizeNo("");
        pvpAuthorize.setAuthorizeType("");
        pvpAuthorize.setContNo(pvpLoanApp.getContNo());         //合同号
        pvpAuthorize.setBillNo(bill_no);
        pvpAuthorize.setUntruPayType("");
        pvpAuthorize.setCusId(pvpLoanApp.getCusId());                        //借款人客户号
        pvpAuthorize.setCusName(pvpLoanApp.getCusName());                   //借款人客户名称
        pvpAuthorize.setPrdId(pvpLoanApp.getPrdId());
        pvpAuthorize.setPrdName(pvpLoanApp.getPrdName());
        pvpAuthorize.setPrdTypeProp(pvpLoanApp.getPrdTypeProp());
        pvpAuthorize.setAcctNo(""); //账号
        pvpAuthorize.setAcctName("");//账号名称
        pvpAuthorize.setCurType(pvpLoanApp.getCurType());//币种
        pvpAuthorize.setTranAmt(pvpLoanApp.getPvpAmt());            //放款金额
        pvpAuthorize.setTranDate(stringRedisTemplate.opsForValue().get("openDay"));                        //交易日期
        pvpAuthorize.setSendTimes(new BigDecimal(0));   //发送次数
        pvpAuthorize.setReturnCode(""); //返回编码
        pvpAuthorize.setReturnDesc(""); //返回说明
        pvpAuthorize.setDisbOrgNo(pvpLoanApp.getDisbOrgNo());//放款机构
        pvpAuthorize.setInputId(pvpLoanApp.getManagerId());
        pvpAuthorize.setInputBrId(pvpLoanApp.getManagerBrId());
        pvpAuthorize.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        pvpAuthorize.setManagerId(pvpLoanApp.getManagerId()); //主办人
        pvpAuthorize.setManagerBrId(pvpLoanApp.getManagerBrId()); //主办机构
        pvpAuthorize.setFinaBrId(pvpLoanApp.getFinaBrId());//账务机构
        pvpAuthorize.setAuthStatus(CmisCommonConstants.STD_AUTH_STATUS_0); //授权状态
        pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1);                //核心状态未出账
        pvpAuthorize.setFldvalue01(pvpLoanApp.getLoanSubjectNo());            //贷款科目代码
        pvpAuthorize.setFldvalue02("bizType");    //***贷款类型
        pvpAuthorize.setFldvalue03(pvpLoanApp.getLoanPayoutAccno());                //收款人账号
        pvpAuthorize.setFldvalue04(pvpLoanApp.getRepayAccno());        //借款人结算账号
        pvpAuthorize.setFldvalue05(pvpLoanApp.getSbsyDepAccno());    //贴息人存款账号
        pvpAuthorize.setFldvalue08(pvpLoanApp.getLoanEndDate());             //到期日
        pvpAuthorize.setFldvalue09(String.valueOf(monthDiffer));            //期限代码
        pvpAuthorize.setFldvalue10(pvpLoanApp.getCurType());
        pvpAuthorize.setFldvalue11(pvpLoanApp.getCapAutobackFlag());        //本金自动归还标志    0否1是
        pvpAuthorize.setFldvalue12(pvpLoanApp.getIntAutobackFlag());         //利息自动归还标志     0否1是
        pvpAuthorize.setFldvalue13("");           //重定价代码
        pvpAuthorize.setFldvalue14(Optional.ofNullable(pvpLoanApp.getCurtLprRate()).orElse(new BigDecimal("0")).toString());           //基准利率代码
        pvpAuthorize.setFldvalue15(String.valueOf(pvpLoanApp.getRateFloatPoint()));    //正常贷款利率浮动比例
        pvpAuthorize.setFldvalue16(String.valueOf(pvpLoanApp.getOverdueRatePefloat()));    //逾期贷款利率浮动比例
        pvpAuthorize.setFldvalue17(String.valueOf(pvpLoanApp.getOverdueExecRate()));    //逾期贷款执行利率
        pvpAuthorize.setFldvalue18(String.valueOf(pvpLoanApp.getExecRateYear()));    //正常贷款执行利率
        pvpAuthorize.setFldvalue19(pvpLoanApp.getDeductType());                                        //还款方式       1-普通贷款 2-分期贷款
        pvpAuthorize.setFldvalue20(pvpLoanApp.getRepayMode());                //普通贷款结息方式代码
        pvpAuthorize.setFldvalue21("");                //分期贷款还款方式代码
        pvpAuthorize.setFldvalue22("");                //分期贷款总期数
        pvpAuthorize.setFldvalue23(String.valueOf(pvpLoanApp.getSbsyPerc()));   //贴息比例
        pvpAuthorize.setFldvalue24(pvpLoanApp.getLoanTerm());                        //借据期限
        pvpAuthorize.setFldvalue25(pvpLoanApp.getLoanPromiseFlag());                //贷款承诺标志
        pvpAuthorize.setFldvalue26("");                //经销商账户名
        //1大中型企业贷款承诺 2小企业贷款承诺3个人贷款承诺
        pvpAuthorize.setFldvalue27(pvpLoanApp.getRepayAcctName());                //放款账户名
        pvpAuthorize.setFldvalue28(pvpLoanApp.getRepaySubAccno());            //委托存款子户号
        pvpAuthorize.setFldvalue29("1");                                            //贷款类型 1新增，3借新还旧
        pvpAuthorize.setFldvalue30(pvpLoanApp.getIsMeterCi());                        //是否计复息   0不计算、1计算
        pvpAuthorize.setFldvalue31(pvpLoanApp.getLoanUseType());        //用途代码     00  流动资金   01  固定资金  02  技术改造  04  流资转期
        //05  固资转期 06  财政性直贷 07  财政性自营贷款 08  政府指令性贷款   09  其他
        //分期贷款：10  住房  11  汽车 12  商铺  13  工程机械       19  其他
        pvpAuthorize.setFldvalue32(pvpLoanApp.getIsBeEntrustedPay());    //是否受托支付
        pvpAuthorize.setFldvalue33("");    //委托贷款手续费收取比例
        pvpAuthorize.setFldvalue34("");   //委托贷款手续费
        pvpAuthorize.setFldvalue35(pvpLoanApp.getDeductDay());        //按揭还款日（缺省为放款日）
        pvpAuthorize.setFldvalue36("2");    //贷款利率浮动方式 1  加百分比 2 加点 3正常加点逾期加百分比
        pvpAuthorize.setFldvalue37(Optional.ofNullable(pvpLoanApp.getRateFloatPoint()).orElse(new BigDecimal("0")).toString());    //正常利率浮动点数(年利率)
        pvpAuthorize.setFldvalue38(Optional.ofNullable(pvpLoanApp.getOverdueRatePefloat()).orElse(new BigDecimal("0")).toString());    //逾期利率浮动点数(年利率)
        pvpAuthorize.setFldvalue39(pvpLoanApp.getLoanPayoutAccno());                            //业务品种
        pvpAuthorize.setFldvalue40("【贷款】");
        pvpAuthorize.setFldvalue41(pvpLoanApp.getManagerBrId());               //放贷机构
        //法人账户透支
        pvpAuthorize.setFldvalue42("");            //借用  执行利率方式
        pvpAuthorize.setFldvalue43("");           //复利年利率
        pvpAuthorize.setFldvalue44(pvpLoanApp.getLoanEndDate());                        //贷款到期日
        pvpAuthorize.setFldvalue45("");     //免息到期日
        pvpAuthorize.setTranId(CmisCommonConstants.DKCZ);                //贷款出账交易码
        pvpAuthorize.setBelgLine(pvpLoanApp.getBelgLine()); //所属条线

        return pvpAuthorize;
    }

    /**
     * 通过合同数据、放款申请数据生成对应状态的借据数据并进行保存操作
     *
     * @param pvpLoanApp
     * @param bill_no
     * @return
     */
    private AccLoan generateAccLoanInfo(PvpLoanApp pvpLoanApp, String bill_no) {
        AccLoan accLoan = new AccLoan();
        String pkid = StringUtils.uuid(true);
        accLoan.setPkId(pkid);//主键
        accLoan.setPvpSerno(pvpLoanApp.getPvpSerno());//出账流水号
        accLoan.setOverdueBalance(new BigDecimal("0"));//逾期余额
        accLoan.setRulingIr(pvpLoanApp.getCurtLprRate());//基准利率
        accLoan.setBillNo(bill_no);//借据编号
        accLoan.setContNo(pvpLoanApp.getContNo());//合同编号
        accLoan.setCusId(pvpLoanApp.getCusId());//客户编号
        accLoan.setCusName(pvpLoanApp.getCusName());//客户名称
        accLoan.setPrdId(pvpLoanApp.getPrdId());//产品编号
        accLoan.setPrdName(pvpLoanApp.getPrdName());//产品名称
        accLoan.setPrdTypeProp(pvpLoanApp.getPrdTypeProp());//产品属性
        accLoan.setGuarMode(pvpLoanApp.getGuarMode());//担保方式
        accLoan.setLoanModal(pvpLoanApp.getLoanModal());//贷款形式
        accLoan.setContCurType(pvpLoanApp.getCurType());//贷款发放币种
        accLoan.setExchangeRate(pvpLoanApp.getExchangeRate());//汇率
        accLoan.setLoanAmt(pvpLoanApp.getPvpAmt());//贷款金额
        accLoan.setLoanBalance(pvpLoanApp.getPvpAmt());//贷款余额
        accLoan.setZcbjAmt(pvpLoanApp.getPvpAmt());//贷款余额
        accLoan.setOverdueCapAmt(new BigDecimal("0.0"));//逾期本金
        accLoan.setDebitInt(new BigDecimal("0.0"));//欠息
        accLoan.setPenalInt(new BigDecimal("0.0")); //罚息
        accLoan.setCompoundInt(new BigDecimal("0.0"));//复息
        accLoan.setExchangeRmbAmt(pvpLoanApp.getCvtCnyAmt());//折合人民币金额
        accLoan.setExchangeRmbBal(pvpLoanApp.getCvtCnyAmt());//折合人民币余额
        accLoan.setLoanStartDate(pvpLoanApp.getLoanStartDate());//贷款起始日
        accLoan.setLoanEndDate(pvpLoanApp.getLoanEndDate());//贷款到期日
        accLoan.setOrigExpiDate(pvpLoanApp.getLoanEndDate());//原到期日期
        accLoan.setLoanTerm(pvpLoanApp.getLoanTerm());//贷款期限
        accLoan.setLoanTermUnit(pvpLoanApp.getLoanTermUnit());//贷款期限单位
        accLoan.setExtTimes("0");//展期次数
        accLoan.setOverdueDay(0);//逾期天数
        accLoan.setOverdueTimes("0");//逾期次数
        accLoan.setSettlDate("");//结清日期
        accLoan.setRateAdjMode(pvpLoanApp.getRateAdjMode());//利率调整方式
        accLoan.setIrFloatType(pvpLoanApp.getIrFloatType());
        accLoan.setRateAdjType(pvpLoanApp.getRateAdjType());//利率调整选项
        accLoan.setIsSegInterest(pvpLoanApp.getIsSegInterest());//是否分段计息
        accLoan.setLprRateIntval(pvpLoanApp.getLprRateIntval());//LPR授信利率区间
        accLoan.setCurtLprRate(pvpLoanApp.getCurtLprRate());//当前LPR利率
        accLoan.setRateFloatPoint(pvpLoanApp.getRateFloatPoint());//浮动点数
        accLoan.setExecRateYear(pvpLoanApp.getExecRateYear());//执行利率(年)
        accLoan.setOverdueRatePefloat(pvpLoanApp.getOverdueRatePefloat());//逾期利率浮动比
        accLoan.setOverdueExecRate(pvpLoanApp.getOverdueExecRate());//逾期执行利率(年利率)
        accLoan.setCiRatePefloat(pvpLoanApp.getCiRatePefloat());//复息利率浮动比
        accLoan.setCiExecRate(pvpLoanApp.getCiExecRate());//复息执行利率(年利率)
        accLoan.setNextRateAdjInterval(pvpLoanApp.getNextRateAdjInterval());//下一次利率调整间隔
        accLoan.setNextRateAdjUnit(pvpLoanApp.getNextRateAdjUnit());//下一次利率调整间隔单位
        accLoan.setFirstAdjDate(pvpLoanApp.getFirstAdjDate());//第一次调整日
        accLoan.setRepayMode(pvpLoanApp.getRepayMode());//还款方式
        accLoan.setEiIntervalCycle(pvpLoanApp.getEiIntervalCycle());//结息间隔周期
        accLoan.setEiIntervalUnit(pvpLoanApp.getEiIntervalUnit());//结息间隔周期单位
        accLoan.setDeductType(pvpLoanApp.getDeductType());//扣款方式
        accLoan.setDeductDay(pvpLoanApp.getDeductDay());//扣款日
        accLoan.setLoanPayoutAccno(pvpLoanApp.getLoanPayoutAccno());//贷款发放账号
        accLoan.setLoanPayoutSubNo(pvpLoanApp.getLoanPayoutSubNo());//贷款发放账号子序号
        accLoan.setPayoutAcctName(pvpLoanApp.getPayoutAcctName());//发放账号名称
        accLoan.setIsBeEntrustedPay(pvpLoanApp.getIsBeEntrustedPay());//是否受托支付
        accLoan.setRepayAccno(pvpLoanApp.getRepayAccno());//还款账号
        accLoan.setRepaySubAccno(pvpLoanApp.getRepaySubAccno());//还款账号子序号
        accLoan.setRepayAcctName(pvpLoanApp.getRepayAcctName());//还款账户名称
        accLoan.setLoanTer(pvpLoanApp.getLoanTer());//贷款投向
        accLoan.setAgriLoanTer(pvpLoanApp.getAgriLoanTer());//涉农贷款投向
        accLoan.setLoanPromiseFlag(pvpLoanApp.getLoanPromiseFlag());//贷款承诺标志
        accLoan.setLoanPromiseType(pvpLoanApp.getLoanPromiseType());//贷款承诺类型
        accLoan.setIsSbsy(pvpLoanApp.getIsSbsy());//是否贴息
        accLoan.setSbsyDepAccno(pvpLoanApp.getSbsyDepAccno());//贴息人存款账号
        accLoan.setSbsyPerc(pvpLoanApp.getSbsyPerc());//贴息比例
        accLoan.setSbysEnddate(pvpLoanApp.getSbysEnddate());//贴息到期日
        accLoan.setIsUtilLmt(pvpLoanApp.getIsUtilLmt());//是否使用授信额度
        accLoan.setBelgLine(pvpLoanApp.getBelgLine());//所属条线
        accLoan.setLmtAccNo(pvpLoanApp.getLmtAccNo());//授信额度编号
        accLoan.setReplyNo(pvpLoanApp.getReplyNo());//批复编号
        accLoan.setLoanTypeDetail(pvpLoanApp.getLoanTypeDetail());//贷款类别
        accLoan.setIsPactLoan(pvpLoanApp.getIsPactLoan());//是否落实贷款
        accLoan.setIsGreenIndustry(pvpLoanApp.getIsGreenIndustry());//是否绿色产业
        accLoan.setIsOperPropertyLoan(pvpLoanApp.getIsOperPropertyLoan());//是否经营性物业贷款
        accLoan.setIsSteelLoan(pvpLoanApp.getIsSteelLoan());//是否钢贸行业贷款
        accLoan.setIsStainlessLoan(pvpLoanApp.getIsStainlessLoan());//是否不锈钢行业贷款
        accLoan.setIsPovertyReliefLoan(pvpLoanApp.getIsPovertyReliefLoan());//是否扶贫贴息贷款
        accLoan.setIsLaborIntenSbsyLoan(pvpLoanApp.getIsLaborIntenSbsyLoan());//是否劳动密集型小企业贴息贷款
        accLoan.setGoverSubszHouseLoan(pvpLoanApp.getGoverSubszHouseLoan());//保障性安居工程贷款
        accLoan.setEngyEnviProteLoan(pvpLoanApp.getEngyEnviProteLoan());//项目贷款节能环保
        accLoan.setIsCphsRurDelpLoan(pvpLoanApp.getIsCphsRurDelpLoan());//是否农村综合开发贷款标志
        accLoan.setRealproLoan(pvpLoanApp.getIsCphsRurDelpLoan());//房地产贷款
        if (null != pvpLoanApp.getRealproLoanRate() && !pvpLoanApp.getRealproLoanRate().equals("")) {
            accLoan.setRealproLoanRate((pvpLoanApp.getRealproLoanRate().toString()).substring(0, 4));//房产开发贷款资本金比例
        } else {
            accLoan.setRealproLoanRate("0");//房产开发贷款资本金比例
        }
        accLoan.setGuarDetailMode(pvpLoanApp.getGuarDetailMode());//担保方式细分
        accLoan.setFinaBrId(pvpLoanApp.getFinaBrId());//账务机构编号
        accLoan.setFinaBrIdName(pvpLoanApp.getFinaBrIdName());//账务机构名称
        accLoan.setDisbOrgNo(pvpLoanApp.getDisbOrgNo());//放款机构编号
        accLoan.setDisbOrgName(pvpLoanApp.getDisbOrgName());//放款机构名称
        accLoan.setAccStatus(CmisCommonConstants.ACC_STATUS_6);//台账状态
        accLoan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
        //获取核心科目号及名称
        if ("01".equals(pvpLoanApp.getBelgLine())) {
            //accLoan.setSubjectNo("LNHS010");
        } else {
            CfgAccountClassChooseDto cfgAccountClassChooseDto = pvpAuthorizeService.calHxAccountClass(pvpLoanApp.getPvpSerno());
            accLoan.setSubjectNo(cfgAccountClassChooseDto.getAccountClass());
            accLoan.setSubjectName(cfgAccountClassChooseDto.getAccountClassName());
        }
        String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        accLoan.setInputId(pvpLoanApp.getManagerId());
        accLoan.setInputBrId(pvpLoanApp.getInputBrId());
        accLoan.setInputDate(openday);
        accLoan.setManagerId(pvpLoanApp.getManagerId());
        accLoan.setManagerBrId(pvpLoanApp.getManagerBrId());
        accLoan.setUpdId(pvpLoanApp.getManagerId());
        accLoan.setUpdDate(openday);
        accLoan.setUpdateTime(DateUtils.getCurrDate());
        accLoan.setCreateTime(DateUtils.getCurrDate());
        accLoan.setIsPool(pvpLoanApp.getIsPool());
        Map map = this.getFiveAndTenClass(pvpLoanApp.getCusId());
        // 五级分类
        accLoan.setFiveClass((String) map.get("fiveClass"));
        // 十级分类
        accLoan.setTenClass((String) map.get("tenClass"));
        return accLoan;
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【解除】，业务与担保合同关系结果表数据更新为【解除】
     * 20210112 该需求待确认
     * 20210113 放款申请拒绝后，仅将当前申请状态变更为“否决”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 拒绝
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterRefuse(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "申请信息");
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
            if (pvpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }

            //获取合同数据，并根据合同中的业务类型进行数据判断
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "获取业务合同数据");
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
            if (ctrLoanCont == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.value);
            }

            int updCount = 0;
            if (CmisBizConstants.BIZ_TYPE_SIGNLE.equals(ctrLoanCont.getBizType())) {
                log.info("放款申请拒绝流程-放款申请" + pvpSerno + "对应业务类型为：单笔单批业务。更新审批模式表【全流程状态】为【放款审批拒绝】");
                updCount = iqpLoanAppApprModeService.updateWfStatusByParams(pvpLoanApp.getIqpSerno(), CmisBizConstants.WF_STATUS_07);
                if (updCount < 0) {
                    throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDWFSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDWFSTATUS_EXCEPTION.value);
                }
            }

            log.info("放款申请拒绝流程-更新放款申请" + pvpSerno + "流程状态-拒绝");
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            updCount = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            if (updCount <= 0) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }

            String inputId = "";
            String inputBrId = "";
            String inputDate = "";
            // 获取当前的登录人信息
            User userInfo = SessionUtils.getUserInformation();
            if (Objects.nonNull(userInfo)) {
                inputId = userInfo.getLoginCode();
                inputBrId = userInfo.getOrg().getCode();
                inputDate = stringRedisTemplate.opsForValue().get("openDay");
            }
            // 根据担保方式判断是否低风险
            String guarMode = pvpLoanApp.getGuarMode();
            // 台账总余额
            BigDecimal balanceAmtCny = pvpLoanApp.getCvtCnyAmt();
            // 恢复敞口余额
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            // 恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;

            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverAmtCny = pvpLoanApp.getCvtCnyAmt();
                recoverSpacAmtCny = BigDecimal.ZERO;
            } else {
                recoverAmtCny = pvpLoanApp.getCvtCnyAmt();
                recoverSpacAmtCny = pvpLoanApp.getCvtCnyAmt();
            }
            // 组装报文进行台账恢复
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpLoanApp.getPvpSerno());//交易流水号
            cmisLmt0014ReqDto.setInputId(inputId);//登记人
            cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
            cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

            List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

            CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
            cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
            cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
            cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
            cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
            cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
            cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
            dealBizList.add(cmisLmt0014ReqdealBizListDto);
            cmisLmt0014ReqDto.setDealBizList(dealBizList);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpLoanApp.getBillNo(), cmisLmt0014ReqDto.toString());
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpLoanApp.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
                log.info("台账恢复，前往额度系统进行额度恢复异常");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpLoanApp.getBillNo());
                throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程拒绝业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }


    /**
     * @param oldpvpLoanApp
     * @return int
     * @author WH
     * @date 2021-04-16 15:31
     * @version 1.0.0
     * @desc 放款申请页面新增时插入的数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto addpvploanappxw(PvpLoanApp oldpvpLoanApp) {

        //生成流水号
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
        //传入的对象
        //获取传入的合同编号查询合同信息
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        //查询合同对象
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(oldpvpLoanApp.getContNo());
        // 根据所选合同判断是否为房抵循环贷的存量合同放款申请，若是则插入借新还旧试算表
        log.info("根据批复编号【{}】获取批复信息开始！", ctrLoanCont.getReplyNo());
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(ctrLoanCont.getReplyNo());
        log.info("根据批复编号【{}】获取批复信息结束！查询结果【{}】", Objects.nonNull(lmtCrdReplyInfo));
        if (Objects.isNull(lmtCrdReplyInfo)) {
            throw BizException.error(null, "999999", "未查询到批复信息！");
        }
        // 生成借据号
        String billSerno = cmisBizXwCommonService.getBillNo(ctrLoanCont.getContNo());

        //合同对象克隆给放款对象
        BeanUtils.copyProperties(ctrLoanCont, pvpLoanApp);
        // 是否使用授信额度 默认是
        pvpLoanApp.setIsUtilLmt(ctrLoanCont.getIsUtilLmt());
        // 币种
        pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
        // 产品编号
        pvpLoanApp.setPrdId(lmtCrdReplyInfo.getPrdId());
        // 产品名称
        pvpLoanApp.setPrdName(lmtCrdReplyInfo.getPrdName());
        //存入借据号
        pvpLoanApp.setBillNo(billSerno);
        //合同币种
        pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
        //默认币种 CNY
        pvpLoanApp.setCurType(ctrLoanCont.getCurType());
        //申请期限
        pvpLoanApp.setAppTerm(ctrLoanCont.getAppTerm().toString());
        //放入必填参数 操作类型 新增
        pvpLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);
        //放款流水号
        pvpLoanApp.setPvpSerno(serno);
        //合同起始日
        pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
        //渠道来源 小微 PC端
        pvpLoanApp.setChnlSour("02");
        //合同最高可用金额
        pvpLoanApp.setContHighDisb(ctrLoanCont.getContAmt());
        //合同到期日
        pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
        //利率调整方式 （3）最高额借款合同：默认授信批复中的，如果批复中为空，则手工选择。
        if ("1".equals(ctrLoanCont.getContType())) {
            //一般合同 选择合同带过来的
            //利率调整方式
            pvpLoanApp.setRateAdjMode(ctrLoanCont.getIrAdjustType());
            //执行年利率
            pvpLoanApp.setExecRateYear(ctrLoanCont.getExecRateYear());

        } else {
            //6、执行年利率： 9、LPR基准：
            if (lmtCrdReplyInfo.getExecRateYear() != null) {
                pvpLoanApp.setExecRateYear(lmtCrdReplyInfo.getExecRateYear());
            }
            // 7、利率调整方式：
            //批复没这个字段 不处理 一般合同就拿合同中的
            //10、基准年利率、浮动点数、逾期加罚比例、复利浮动比例：根据LPR基准自动回显且只读。

            //还款方式  默认取批复中的
            if (lmtCrdReplyInfo.getRepayMode() != null) {
                pvpLoanApp.setRepayMode(lmtCrdReplyInfo.getRepayMode());
            }

            //如果批复表中不为空 那么我们就给他赋值为批复中的 执行年利率
            if (lmtCrdReplyInfo.getExecRateYear() != null) {
                pvpLoanApp.setExecRateYear(lmtCrdReplyInfo.getExecRateYear());
            }

        }


        //正常利率浮动方式  默认赋值 lpr加点 00   加百分比 01
        pvpLoanApp.setIrFloatType(CmisBizConstants.IR_FLOAT_TYPE_00);

        //逾期浮动(年)
        pvpLoanApp.setOverdueExecRate(ctrLoanCont.getOverdueExecRate());
        //结息间隔周期单位 默认月
        pvpLoanApp.setEiIntervalUnit("M");
        //扣款扣息方式->扣款方式
        pvpLoanApp.setDeductType(ctrLoanCont.getDeductDeduType());
        //还款日->扣款日
        pvpLoanApp.setDeductDay(ctrLoanCont.getRepayDate());

        //借款用途类型
        pvpLoanApp.setLoanTypeDetail(null);
        //审批状态
        pvpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
        //担保方式
        pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());
        //贷款形式
        pvpLoanApp.setLoanModal(oldpvpLoanApp.getLoanModal());
        //贷款期限类型
        pvpLoanApp.setLoanTermUnit(ctrLoanCont.getTermType());
        //是否立即发起受托支付
        pvpLoanApp.setIsCfirmPay(ctrLoanCont.getIsCfirmPayWay());
        //工业转型升级标识
        pvpLoanApp.setIndtUpFlag(ctrLoanCont.getComUpIndtify());
        //文化产业标识
        pvpLoanApp.setCulIndustryFlag(ctrLoanCont.getIsCulEstate());
        //贷款放款账号名称
        pvpLoanApp.setPayoutAcctName(ctrLoanCont.getLoanPayoutAccName());

        // 获取账务机构信息，小贷特殊处理
        // 根据合同管护机构查询账务机构信息
        log.info("新增出账申请，合同号【{}】，根据管护机构【{}】查询账务机构开始", ctrLoanCont.getContNo(), ctrLoanCont.getManagerBrId());
        CfgXdLoanOrgDto xdLoanOrgQueryDto = new CfgXdLoanOrgDto();
        xdLoanOrgQueryDto.setXdOrg(ctrLoanCont.getManagerBrId());
        ResultDto<CfgXdLoanOrgDto> xdLoanOrgDtoResultDto = iCmisCfgClientService.queryCfgXdLoanOrg(xdLoanOrgQueryDto);
        if (Objects.isNull(xdLoanOrgDtoResultDto) || Objects.isNull(xdLoanOrgDtoResultDto.getData())) {
            throw BizException.error(null, "9999", "未查询到账务机构！");
        }
        CfgXdLoanOrgDto xdLoanOrgDto = xdLoanOrgDtoResultDto.getData();
        log.info("新增出账申请，合同号【{}】，根据管护机构【{}】查询账务机构结束，返回账务机构【{}】、放款机构【{}】",
                ctrLoanCont.getContNo(), ctrLoanCont.getManagerId(), xdLoanOrgDto.getXdFinaOrg(), xdLoanOrgDto.getXdPvpOrg());
        // 账务机构
        pvpLoanApp.setFinaBrId(xdLoanOrgDto.getXdFinaOrg());
        // 账务机构名称
        pvpLoanApp.setFinaBrIdName(xdLoanOrgDto.getXdFinaOrgName());
        // 放款机构
        pvpLoanApp.setDisbOrgNo(xdLoanOrgDto.getXdPvpOrg());
        // 放款机构名称
        pvpLoanApp.setDisbOrgName(xdLoanOrgDto.getXdPvpOrgName());

        //TODO 现在开始放一些基本的系统默认参数
        //产业结构类型
        pvpLoanApp.setEstateType("1");
        pvpLoanApp.setCulIndustryFlag("1");
        pvpLoanApp.setIndtUpFlag("1");
        pvpLoanApp.setStrategyNewLoan("1");
        pvpLoanApp.setCapAutobackFlag("1");
        //所属条线
        pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_XW_01);
        PvpLoanApp pvpLoanApp1 = this.setData(pvpLoanApp);
        int i = pvpLoanAppMapper.insertSelective(pvpLoanApp1);
        if (i != 1) {
            return new ResultDto(null).message("新增失败");
        }
        return new ResultDto(pvpLoanApp1);

    }

    /**
     * @param oldpvpLoanApp
     * @return int
     * @author WH
     * @date 2021-04-16 15:31
     * @version 1.0.0
     * @desc 放款申请页面新增时插入的数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto addpvploanappls(PvpLoanApp oldpvpLoanApp) {
        log.info("零售放款申请新增开始");
        PvpLoanApp pvpLoanApp = new PvpLoanApp();

        try {
            //查询合同对象
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(oldpvpLoanApp.getContNo());

            //产品编号
            String prdId = ctrLoanCont.getPrdId();

            //生成流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
            String billNo = cmisBizXwCommonService.getBillNo(ctrLoanCont.getContNo());
            //合同对象克隆给放款对象
            BeanUtils.copyProperties(ctrLoanCont, pvpLoanApp);

            //按揭类贷款迁移数据执行利率处理
            try {
                String irAdjustType = ctrLoanCont.getIrAdjustType() != null ? ctrLoanCont.getIrAdjustType() : "";
                BigDecimal execRateYear = ctrLoanCont.getExecRateYear() != null ? ctrLoanCont.getExecRateYear() : new BigDecimal(0);
                String signDate = ctrLoanCont.getSignDate() != null ? ctrLoanCont.getSignDate() : "";
                if (execRateYear.doubleValue() <= 0) {//迁移做不到为空，如果为空都刷成-999

                    if ("".equals(irAdjustType)) {
                        throw BizException.error(null, "999999", "利率调整类型不能为空");
                    }

                    if (ctrLoanCont.getRateFloatPoint() == null) {
                        throw BizException.error(null, "999999", "浮动点数不能为空");
                    }
                    BigDecimal rateFloatPoint = ctrLoanCont.getRateFloatPoint();

                    if ("".equals(signDate)) {
                        throw BizException.error(null, "999999", "签订日期不能为空");
                    }

                    if ("02".equals(irAdjustType)) { //利率调整类型 01-固定利率 02-浮动利率

                        signDate = DateUtils.addDay(signDate, "yyyy-MM-dd", -1); // 1天前日期

                        int contTerm = ctrLoanCont.getContTerm();
                        String lprRateIntval = "A1";//1年期

                        if (contTerm >= 60) {//大于等于5年
                            lprRateIntval = "A2"; //5年期
                        }

                        //当前lpr利率
                        BigDecimal lpr = null;

                        CfgLprRateDto cfgLprRateDto = new CfgLprRateDto();
                        cfgLprRateDto.setValidDate(signDate);
                        cfgLprRateDto.setRateTypeId(lprRateIntval);

                        ResultDto<CfgLprRateDto> cfgLprRateDtoAfter = iCmisCfgClientService.selectAfterLpr(cfgLprRateDto);
                        ResultDto<CfgLprRateDto> cfgLprRateDtoFront = iCmisCfgClientService.selectFrontLpr(cfgLprRateDto);

                        if (cfgLprRateDtoAfter.getData() == null && cfgLprRateDtoFront.getData() == null) {
                            throw BizException.error(null, "999999", "未找到LPR利率配置");
                        } else if (cfgLprRateDtoAfter.getData() == null || cfgLprRateDtoFront.getData() != null) {
                            lpr = cfgLprRateDtoFront.getData().getRate();
                        } else if (cfgLprRateDtoAfter.getData() != null || cfgLprRateDtoFront.getData() == null) {
                            lpr = cfgLprRateDtoAfter.getData().getRate();
                        } else {
                            if (signDate.equals(cfgLprRateDtoAfter.getData().getValidDate())) {
                                lpr = cfgLprRateDtoAfter.getData().getRate();
                            } else {
                                lpr = cfgLprRateDtoFront.getData().getRate();
                            }
                        }

                        //更新执行利率
                        pvpLoanApp.setExecRateYear(lpr.add(rateFloatPoint.divide(new BigDecimal(10000), 5, BigDecimal.ROUND_HALF_UP)));
                    }
                }
            } catch (Exception e) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "按揭类贷款迁移数据执行利率处理异常:" + e.getMessage());
            }

            // 是否使用授信额度 默认是
            pvpLoanApp.setIsUtilLmt(ctrLoanCont.getIsUtilLmt());
            // 币种
            pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
            //从前端参数取出是否涉农
            pvpLoanApp.setAgriType(oldpvpLoanApp.getAgriType());
            //放入必填参数 操作类型 新增
            pvpLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);

            pvpLoanApp.setContHighDisb(ctrLoanCont.getHighAvlAmt());
            //空位暂留 用于赋值
            //放款流水号
            pvpLoanApp.setPvpSerno(serno);
            //补充 克隆缺少数据
            //合同起始日
            pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
            //合同到期日
            pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
            //利率调整方式
            pvpLoanApp.setRateAdjMode(ctrLoanCont.getIrAdjustType());
            //执行年利率
            //利率浮动方式
            pvpLoanApp.setRateAdjType(ctrLoanCont.getIrFloatType());
            //LPR利率区间
            pvpLoanApp.setLprRateIntval(ctrLoanCont.getLprRateIntval());
            //浮动点数
            pvpLoanApp.setRateFloatPoint(ctrLoanCont.getRateFloatPoint());
            //结息间隔周期单位
            pvpLoanApp.setEiIntervalUnit(ctrLoanCont.getEiMode());
            //扣款扣息方式->扣款方式
            pvpLoanApp.setDeductType(ctrLoanCont.getDeductDeduType());
            //还款日->扣款日
            pvpLoanApp.setDeductDay(ctrLoanCont.getRepayDate());
            //逾期浮动
            pvpLoanApp.setOverdueRatePefloat(ctrLoanCont.getOverdueRatePefloat());
            //逾期浮动(年)
            pvpLoanApp.setOverdueExecRate(ctrLoanCont.getOverdueExecRate());
            //借款用途类型
            //pvpLoanApp.setLoanType(ctrLoanCont.getLoanPurp());
            //审批状态
            pvpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
            //担保方式
            pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());
            //贷款形式--默认新增
            pvpLoanApp.setLoanModal(CmisBizConstants.IQP_LOAN_MODAL_1);
            //贷款期限类型
            pvpLoanApp.setLoanTermUnit(ctrLoanCont.getTermType());
            //是否立即发起受托支付
            pvpLoanApp.setIsCfirmPay(ctrLoanCont.getIsCfirmPayWay());
            //工业转型升级标识
            pvpLoanApp.setIndtUpFlag(ctrLoanCont.getComUpIndtify());
            //文化产业标识
            pvpLoanApp.setCulIndustryFlag(ctrLoanCont.getIsCulEstate());
            //借据编号
            pvpLoanApp.setBillNo(billNo);
            //第一次调整日
            pvpLoanApp.setFirstAdjDate(ctrLoanCont.getLoanRateAdjDay());
            //借款利率调整日
            pvpLoanApp.setLoanRateAdjDay(ctrLoanCont.getLoanRateAdjDay());
            //放款金额
            pvpLoanApp.setPvpAmt(ctrLoanCont.getHighAvlAmt());
            //正常利率浮动方式  默认赋值 lpr加点 00   加百分比 01
            pvpLoanApp.setIrFloatType(CmisBizConstants.IR_FLOAT_TYPE_00);
            pvpLoanApp = this.setData(pvpLoanApp);
            // 渠道来源
            pvpLoanApp.setChnlSour("02");// 02 PC端
            //期限类型
            pvpLoanApp.setTermType("M");
            //是否分段计息
            pvpLoanApp.setIsSegInterest("0");//否
            // 额度分项编号
            pvpLoanApp.setLmtAccNo(ctrLoanCont.getLmtAccNo());

            //022040	个人二手住房按揭贷款（资金托管）
            //022051	个人二手商用房按揭贷款（资金托管）
            //022053	个人二手住房按揭贷款（连云港资金托管）
            //022054	个人二手商用房按揭贷款（连云港资金托管）
            //这四个产品无须合同交易对手信息赋值到出账
            if (!"022040".equals(prdId) && !"022051".equals(prdId) && !"022053".equals(prdId) && !"022054".equals(prdId)) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("bizSerno", pvpLoanApp.getIqpSerno());
                List<ToppAcctSub> toppAcctSubList = toppAcctSubMapper.selectByModel(queryModel);

                if (toppAcctSubList != null && toppAcctSubList.size() > 0) {
                    for (int j = 0; j < toppAcctSubList.size(); j++) {
                        ToppAcctSub toppAcctSub = toppAcctSubList.get(j);
                        toppAcctSub.setBizSerno(pvpLoanApp.getPvpSerno());
                        toppAcctSub.setBizSence("3");
                        toppAcctSub.setPkId(UUID.randomUUID().toString());
                        toppAcctSubMapper.insertSelective(toppAcctSub);
                    }
                }
            }


            int i = pvpLoanAppMapper.insertSelective(pvpLoanApp);
            if (i != 1) {
                throw BizException.error(null, "999999", "新增：零售-放款申请异常");
            }

            // TODO 添加征信信息
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());

            //村镇无需再查征信，放款阶段无需重新申请征信，带授信时征信信息过来
            if (!ctrLoanCont.getManagerBrId().startsWith("80") && !ctrLoanCont.getManagerBrId().startsWith("81")) {
                CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
                creditReportQryLstAndRealDto.setCusId(pvpLoanApp.getCusId());
                creditReportQryLstAndRealDto.setCusName(pvpLoanApp.getCusName());
                creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
                creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
                creditReportQryLstAndRealDto.setBorrowRel("001");
                String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
                creditReportQryLstAndRealDto.setQryCls(qryCls);
                creditReportQryLstAndRealDto.setApproveStatus("000");
                creditReportQryLstAndRealDto.setIsSuccssInit("0");
                creditReportQryLstAndRealDto.setQryFlag("02");
                creditReportQryLstAndRealDto.setQryStatus("001");
                //生成关联征信数据
                creditReportQryLstAndRealDto.setBizSerno(pvpLoanApp.getIqpSerno());
                creditReportQryLstAndRealDto.setScene("03");
                 ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
                 if (!doCreateCreditAuto.getCode().equals("0")) {
                     log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
                 }
            }

            try {
                /// 住房按揭放款申请征信报告校验只针对苏州地区
                if (!StringUtils.isBlank(cusBaseClientDto.getCertCode()) && cusBaseClientDto.getCertCode().startsWith("3205")) {
                    // 022001-个人一手住房按揭贷款 022002-个人二手住房按揭贷款 022040-个人二手住房按揭贷款（资金托管）022052-个人一手住房按揭贷款（常熟资金监管）
                    //022053-个人二手住房按揭贷款（连云港资金托管） 022055-个人一手住房按揭贷款（宿迁资金监管）
                    if ("022001,022002,022040,022052,022053,022055".indexOf(pvpLoanApp.getPrdId()) != -1) {

                        QueryModel querymodel = new QueryModel();
                        querymodel.addCondition("surveySerno", pvpLoanApp.getIqpSerno());
                        List<LmtCobInfo> lmtCobInfoLsit = lmtCobInfoMapper.selectByModel(querymodel);
                        for (int j = 0; j < lmtCobInfoLsit.size(); j++) {

                            LmtCobInfo lmtCobInfo = lmtCobInfoLsit.get(j);

                            if (!pvpLoanApp.getCusId().equals(lmtCobInfo.getCusId())) {
                                //生成征信流水号
                                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
                                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                                creditQryBizReal.setBizSerno(lmtCobInfo.getSurveySerno());
                                creditQryBizReal.setCrqlSerno(crqlSerno);
                                creditQryBizReal.setCusId(lmtCobInfo.getCusId());
                                creditQryBizReal.setCusName(lmtCobInfo.getCommonDebitName());
                                creditQryBizReal.setCertType(lmtCobInfo.getCommonDebitCertType());
                                creditQryBizReal.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                                creditQryBizReal.setBorrowRel("005");
                                creditQryBizReal.setScene("01");
                                if (creditQryBizRealMapper.insertSelective(creditQryBizReal) != 1) {
                                    log.error("征信查询与业务关联表插入异常");
                                }
                                //征信详情表
                                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                                //生成征信详情数据
                                creditReportQryLst.setCrqlSerno(crqlSerno);
                                creditReportQryLst.setCusId(lmtCobInfo.getCusId());
                                creditReportQryLst.setCusName(lmtCobInfo.getCommonDebitName());
                                creditReportQryLst.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                                creditReportQryLst.setCertType(lmtCobInfo.getCommonDebitCertType());
                                creditReportQryLst.setBorrowerCusId(pvpLoanApp.getCusId());
                                creditReportQryLst.setBorrowerCusName(pvpLoanApp.getCusName());
                                creditReportQryLst.setBorrowerCertCode(cusBaseClientDto.getCertCode());
                                creditReportQryLst.setCertType("A");
                                creditReportQryLst.setBorrowRel("005");
                                creditReportQryLst.setQryCls("0");
                                creditReportQryLst.setApproveStatus("000");
                                creditReportQryLst.setQryResn(creditReportQryLstService.getQryResnByPerIodAndReal("005", "01", "0"));
                                if (creditReportQryLstService.insertSelective(creditReportQryLst) != 1) {
                                    log.error("零售共同借款人征信详情表插入异常");
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("零售放款共同借款人征信详情表插入异常");
            }
            //这里加上特殊处理表  如果是个人二手房按揭贷款(资金托管)  022040  现在又来一个022051  二手商用房资金托管  022053连云港存量住房资金托管 022054连云港存量商用房资金托管
            //就在表中加载一个数据 pvp_esftuoguan_info 根据主键号关联
            if ("022040".equals(prdId) || "022051".equals(prdId) || "022052".equals(prdId) || "022053".equals(prdId) || "022054".equals(prdId)) {

                PvpSehandEsInfo pvpSehandEsInfo = new PvpSehandEsInfo();
                //放款申请流水号
                pvpSehandEsInfo.setPvpSerno(pvpLoanApp.getPvpSerno());
                //申请金额
                pvpSehandEsInfo.setApplyAmount(pvpLoanApp.getPvpAmt());

                //非常熟个人一手房资金托管022052项目从信贷中取信息
                if ("022040".equals(prdId) || "022051".equals(prdId)) {
                    //买房人姓名
                    pvpSehandEsInfo.setBuyerName(pvpLoanApp.getCusName());
                    //买房人身份证号码
                    pvpSehandEsInfo.setBuyerCertCode(cusBaseClientDto.getCertCode());
                    //资金托管协议编号
                    IqpHouse iqphouse = iqpHouseService.selectByIqpSernos(pvpLoanApp.getIqpSerno());
                    pvpSehandEsInfo.setTgxyNo(iqphouse.getTgxyNo());
                }


                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(pvpLoanApp.getIqpSerno());
                //是否公积金组合贷款 0：否  1：是
                String fundUnionFlag = iqpLoanApp.getFundUnionFlag();
                //3组合贷款  1商业贷款
                if ("022040".equals(prdId) || "022052".equals(prdId) || "022053".equals(prdId)) {
                    if ("1".equals(fundUnionFlag)) {
                        pvpSehandEsInfo.setCapitalType("3");
                    } else {
                        pvpSehandEsInfo.setCapitalType("1");
                    }
                } else {
                    //022051 这个好像不能用公积金
                    pvpSehandEsInfo.setCapitalType("1");
                }

                /****协议编号*****/
                //查看是否第二次放款，如果是的话就可以获取到协议编号了
//                String tgxy_no = imp.getStringByCondition("select a.tgxy_no from pvp_esftuoguan_info a left join pvp_loan_app b on a.pvp_serno = b.serno where a.tgxy_no is not null and b.cont_no='" + cont_no + "' and rownum=1", connection);
//                if(StringUtil.hasLength(tgxy_no)){
//                    KeyedCollectionUtil.setValue(esfKColl, "tgxy_no", tgxy_no);
//                }
//                if(tgxy_no.length() != 0) {
//                    //标记为非第一次出账申请
//                    KeyedCollectionUtil.setValue(esfKColl, "remark", "1");
//                }

                /**********/
                //0未发送核心  1已发送核心
                pvpSehandEsInfo.setSendStatus("0");
                pvpSehandEsInfoService.insert(pvpSehandEsInfo);

                //022052常熟的需要通过接口查询账号信息,查完之后插入,所以这边先不生成受托支付信息
                if ("022040".equals(prdId) || "022051".equals(prdId)) {
                    //插入一个固定的托管账号
                    ToppAcctSub toppAcctSub = new ToppAcctSub();
                    toppAcctSub.setBizSerno(pvpLoanApp.getPvpSerno());
                    toppAcctSub.setToppAcctNo("801000006838588");
                    toppAcctSub.setToppName("张家港市房产交易服务中心");
                    toppAcctSub.setToppAmt(pvpLoanApp.getPvpAmt());
                    toppAcctSub.setIsBankAcct("1");
                    toppAcctSub.setBizSence("3");
                    toppAcctSub.setIsOnline("1");
                    toppAcctSub.setOprType("01");
                    toppAcctSub.setPkId(StringUtils.uuid(true));
                    toppAcctSubService.insert(toppAcctSub);
                }
            }


            // 定制还款计划
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", ctrLoanCont.getIqpSerno());
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
            List<RepayCapPlan> list = repayCapPlanService.selectAll(queryModel);
            if (list.size() > 0) {
                for (int j = 0; j < list.size(); j++) {
                    RepayCapPlan repayCapPlan = list.get(j);
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setSerno(pvpLoanApp.getPvpSerno());
                    repayCapPlanService.insert(repayCapPlan);
                }
            }

        } catch (Exception e) {

            log.error("零售-放款申请新增异常：", e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("零售-放款申请新增结束");
        return new ResultDto(pvpLoanApp);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/21 14:30
     * @注释 公共的方法 补全固定参数
     */
    public PvpLoanApp setData(PvpLoanApp pvpLoanApp) {
        User user = SessionUtils.getUserInformation();
        if (user != null) {
            //登记人
            pvpLoanApp.setUpdId(user.getLoginCode());
            //最后修改人
            pvpLoanApp.setInputId(user.getLoginCode());
            //获取登记机构
            UserIdentity org = user.getOrg();
            //登记机构
            pvpLoanApp.setInputBrId(org.getCode());
            //最后修改机构
            pvpLoanApp.setUpdBrId(org.getCode());
            //责任人机构
            pvpLoanApp.setManagerBrId(org.getCode());
            Date date = new Date();
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String s = stringRedisTemplate.opsForValue().get("openDay");
            //登记日期
            pvpLoanApp.setInputDate(s);
            //最后修改日期
            pvpLoanApp.setUpdDate(s);
            //创建时间
            pvpLoanApp.setCreateTime(date);
            //修改日期
            pvpLoanApp.setUpdateTime(date);
        }
        return pvpLoanApp;

    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-17 16:45
     * @注释 查询基本页面
     */
    public ResultDto selectbasicpage(String ids) {
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(ids);//selectCtrLoanContAndPvPLoanApp
        CtrLoanContAndPvPLoanApp ctrLoanContAndPvPLoanApp = new CtrLoanContAndPvPLoanApp();
//      ctrLoanContService.selectCtrLoanContAndPvPLoanApp(pvpLoanApp.getContNo());
        ctrLoanContAndPvPLoanApp.setPvpAmt(pvpLoanApp.getPvpAmt());
        ctrLoanContAndPvPLoanApp.setBillNo(pvpLoanApp.getBillNo());
        ctrLoanContAndPvPLoanApp.setIsCfirmPay(pvpLoanApp.getIsCfirmPay());
        //ctrLoanContAndPvPLoanApp.setApproveTerm(pvpLoanApp.getApproveTerm());
        ctrLoanContAndPvPLoanApp.setApproveTerm(pvpLoanApp.getAppTerm());
        ctrLoanContAndPvPLoanApp.setAuthStatus(pvpLoanApp.getAuthStatus());
//        ctrLoanContAndPvPLoanApp.setAcctBrId(pvpLoanApp.getAcctBrId());
        ctrLoanContAndPvPLoanApp.setEndDate(pvpLoanApp.getEndDate());
        ctrLoanContAndPvPLoanApp.setPraType(pvpLoanApp.getOprType());
        ctrLoanContAndPvPLoanApp.setCustomerRatingFactor(pvpLoanApp.getCustomerRatingFactor());
        return new ResultDto(ctrLoanContAndPvPLoanApp);
    }

    public ResultDto selectbyids(String ids) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(ids);
        return new ResultDto(ctrLoanCont);
    }

    public ResultDto updateonedata(PvpLoanApp pvpLoanApp) {
        int i = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        return new ResultDto(i);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-20 11:14
     * @注释 贷款还款计划试算
     */
    public ResultDto selectcalculate(LmtSurveyReportDto lmtSurveyReportDto) {
        String pvpSerno = lmtSurveyReportDto.getSurveySerno();
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);//放款申请表
        Page<Lstdkhk> page = new Page();
        if (pvpLoanApp == null) {
            log.info("未查询到基本信息");
            return new ResultDto(null).code(-1).message("请补全放款要素");
        } else if (pvpLoanApp.getLoanStartDate() == null || pvpLoanApp.getLoanEndDate() == null) {
            log.info("基本信息未补全信息");
            return new ResultDto(null).code(-1).message("请补全放款要素");
        }
        log.info("贷款还款计划试算开始..................");
        Ln3110RespDto ln3110RespDto = null;
        try {
            Ln3110ReqDto ln3110ReqDto = new Ln3110ReqDto();//req对象

            //daikjine贷款金额
            ln3110ReqDto.setDaikjine(pvpLoanApp.getPvpAmt());
            //sftsdkbz是否特殊贷款标志  1是 0否
            ln3110ReqDto.setSftsdkbz("1");
            //tsdkjxqs特殊贷款计息总期数

            //jixiguiz计息规则
//            00--标准/标准(30/360)
//            01--对月对日(30/365,366)
//            02--标准/365(30/365)
//            10--实际/标准(31/360)
//            11--实际/实际(31/365,366)
//            12--实际/365(31/365)
            ln3110ReqDto.setJixiguiz("01");
            //zhchlilv正常利率
            ln3110ReqDto.setZhchlilv(pvpLoanApp.getExecRateYear());
            //hkzhouqi还款周期
            ln3110ReqDto.setHkzhouqi("1MA21");
            //hkqixian还款期限(月)
            ln3110ReqDto.setHkqixian(pvpLoanApp.getLoanTerm());
            //qixiriqi起息日期 TODO 存疑
            ln3110ReqDto.setQixiriqi(pvpLoanApp.getLoanStartDate().replaceAll("-", ""));
            //daoqriqi到期日期 TODO 存疑
            ln3110ReqDto.setDaoqriqi(pvpLoanApp.getLoanEndDate().replaceAll("-", ""));
            String repayMode = pvpLoanApp.getRepayMode();
            if (StringUtils.isBlank(repayMode)) {

            }
            if (repayMode.equals("A001")) {
                repayMode = "2";
            } else if (repayMode.equals("A002")) {
                repayMode = "3";
            } else if (repayMode.equals("A003")) {
                repayMode = "4";
            } else if (repayMode.equals("A009")) {
                repayMode = "1";
            } else {
                // TODO 其他的不支持计算 直接返回
                return new ResultDto().code(-1).message("暂不支持该种还款方式的还款计划试算");
            }
//            if (repayMode.equals("A012")||repayMode.equals("A013")||repayMode.equals("A014")||repayMode.equals("A015")||repayMode.equals("A016")
//                    ||repayMode.equals("A017")||repayMode.equals("A018")||repayMode.equals("A019")
//            ){
//                repayMode="7";
//            }else {
//                repayMode="7";
//            }

            //我们的字典项 [{"key":"A001","value":"按期付息到期还本"},{"key":"A002","value":"等额本息"},
            // {"key":"A003","value":"等额本金"},{"key":"A009","value":"利随本清"},
            // {"key":"A012","value":"按226比例还款"},{"key":"A013","value":"按月还息按季还本"},
            // {"key":"A014","value":"按月还息按半年还本"},{"key":"A015","value":"按月还息,按年还本"},
            // {"key":"A016","value":"新226"},{"key":"A017","value":"前6月按月还息，后6个月等额本息"},
            // {"key":"A018","value":"前4个月按月还息，后8个月等额本息"},{"key":"A019","value":"第一年按月还息，接下来等额本息"},
            // {"key":"A020","value":"334比例还款"},{"key":"A021","value":"433比例还款"},{"key":"A022","value":"10年期等额本息"},
            // {"key":"A023","value":"按月付息，定期还本"},{"key":"A030","value":"定制还款"},{"key":"A031","value":"5年期等额本息"},
            // {"key":"A040","value":"按期付息,按计划还本"},{"key":"A041","value":"其他方式"}]
            //huankfsh还款方式 TODO  1--利随本清  2--多次还息一次还本  3--等额本息  4--等额本金  5--等比累进  6--等额累进  7--定制还款
            ln3110ReqDto.setHuankfsh(repayMode);
            //qigscfsh期供生成方式  TODO 1--还款规则   2--还款计划书
            ln3110ReqDto.setQigscfsh("1");
            //qglxleix期供利息类型  1--贷款本金利息  2--本期本金利息  3--指定本金利息
            ln3110ReqDto.setQglxleix("3");
            //dechligz等额处理规则   TODO 0--不适用  1--标准+差额  2--标准  3--标准/实际
            ln3110ReqDto.setDechligz("2");
            //meiqhkze每期还款总额
            //meiqhbje每期还本金额

            //baoliuje保留金额
            //kuanxiqi宽限期
            //leijinzh累进值
            //leijqjsh累进区间期数
            //qishibis起始笔数
            //chxunbis查询笔数  默认20
            ln3110ReqDto.setChxunbis(1000);
            //shifoudy是否打印  1--是   0--否
            ln3110ReqDto.setShifoudy("0");
            //scihkrbz首次还款日模式  0--不适用  1--第一个还款日   2--次月还款日
            ln3110ReqDto.setScihkrbz("2");
            //mqihkfsh末期还款方式 TODO  1--在最后一期进行还款   2--到期日前一期进行还款   3--不足一期算做一期
            ln3110ReqDto.setMqihkfsh("3");
            //dzhhkjih定制还款计划 TODO 1--是 0--否
            ln3110ReqDto.setDzhhkjih("0");
            //ljsxqish累进首段期数
            //dzhhkzhl定制还款种类 TODO 1--正常还款 2--提前还款
            //xzuetqhk需足额提前还款
            //dzhkriqi定制还款日期
            //huanbjee还本金额
            //tqhkhxfs提前还款还息方式
            //huankzhh还款账号
            //hkzhhzxh还款账号子序号
            //benqqish本期期数
            //qishriqi起始日期
            //daoqriqi到期日期
            //huanbjee还本金额
            //hxijinee还息金额
            //zwhkriqi最晚还款日
            //设置查询条数

            ln3110ReqDto.setChxunbis(lmtSurveyReportDto.getSize());
            ln3110ReqDto.setQishibis((lmtSurveyReportDto.getPage() - 1) * lmtSurveyReportDto.getSize());
            ln3110RespDto = ln3110Service.ln3110(ln3110ReqDto);
//            PageHelper.startPage(lmtSurveyReportDto.getPage(), lmtSurveyReportDto.getSize());
//            PageHelper.clearPage();
        } catch (YuspException e) {
            log.info("贷款还款计划试算失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            log.info("贷款还款计划试算结束..................");
        }


        page.addAll(ln3110RespDto.getLstdkhk());
        page.setTotal(ln3110RespDto.getZongqish());
        page.setPages(lmtSurveyReportDto.getPage());
        page.setPageNum(lmtSurveyReportDto.getSize());
        return new ResultDto(page);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 13:39
     * @注释 作废/删除放款申请单
     */
    public ResultDto invalidpvploanapp(String pvpSerno) {
        // 根据主键取获取对象
        log.info("查询对象");
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        log.info("查询到的对象" + pvpLoanApp);
        if (pvpLoanApp == null) {
            //如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        if (!CmisCommonConstants.WF_STATUS_000.equals(pvpLoanApp.getApproveStatus()) && !CmisCommonConstants.WF_STATUS_992.equals(pvpLoanApp.getApproveStatus())) {
            //比对 如果进入到这里说明他的状态不是待发起  暂时只能删除/作废待发起的
            return new ResultDto(0).message("该条数据不是待发起或打回状态,禁止作废/删除");
        }

        //判断当前申请状态是否为退回，修改为自行退出
        int i = 0;
        if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(pvpLoanApp.getApproveStatus())){
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：",pvpSerno);
            // 删除流程实例
            workflowCoreClient.deleteByBizId(pvpSerno);
            // 2021年11月4日23:22:48 hubp恢复额度
            if("01".equals(pvpLoanApp.getBelgLine())){
                retailRestore(pvpSerno);
            }
            pvpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            i = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        }else{
            pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            i = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        }

        if (i != 1) {
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        return new ResultDto(i).message("删除/作废成功");
    }


    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:06
     * @注释 撤销申请单 状态->000
     */
    public ResultDto revokepvploanapp(String pvpSerno) {
        // 根据主键取获取对象
        log.info("查询对象");
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        log.info("查询到的对象" + pvpLoanApp);
        if (pvpLoanApp == null) {
            //如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        // 更改状态为 作废
        pvpLoanApp.setApproveStatus("000");
        log.info("修改开始,如果结果为1则修改成功 否则修改失败");
        int i = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        log.info("修改结束,修改条数=" + i);
        if (i != 1) {
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        return new ResultDto(i).message("撤销成功");

    }


    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:34
     * @注释 借据打印
     */
    public ResultDto pvploanprint(String pvpSerno) {
        // 参与者：在【数据列表区】界面，选择借据信息已创建，但未发起放款审批的单条记录，点击【借据打印】按钮；
        // 根据主键取获取对象
        log.info("查询对象");
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        log.info("查询到的对象" + pvpLoanApp);
        if (pvpLoanApp == null) {
            //如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        if (!CmisCommonConstants.WF_STATUS_000.equals(pvpLoanApp.getApproveStatus())) {
            //比对 如果进入到这里说明他的状态不是待发起  说明有问题 不能进行借据打印
            log.info("状态不是待发起  说明有问题 不能进行借据打印");
            return new ResultDto(0).message("该条数据不是待发起状态,禁止打印借据");
        }
        log.info("查询借据信息");
        AccLoan accLoan = accLoanService.selectByPrimaryKey(pvpLoanApp.getBillNo());
        //如果没有借据信息 也不打印
        if (accLoan == null) {
            return new ResultDto(0).message("该数据不存在借据信息,禁止打印借据");
        }
        //
        return new ResultDto(1).message("打印成功,挡板");
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:35
     * @注释 额度支用申请书
     */
    public ResultDto applybookprint(String pvpSerno) {
        // 在【数据列表区】界面，选择借据信息已创建，但未发起放款审批的且为白领易贷通为最高额合同的单条记录，
        // 点击【额度支用申请书打印】按钮；
        log.info("查询对象");
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        log.info("查询到的对象" + pvpLoanApp);
        if (pvpLoanApp == null) {
            //如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }

        //是否放款
        if (!CmisCommonConstants.WF_STATUS_000.equals(pvpLoanApp.getApproveStatus())) {
            //比对 如果进入到这里说明他的状态不是待发起  说明有问题 不能进行借据打印
            log.info("状态不是待发起  说明有问题 不能进行打印申请书");
            return new ResultDto(0).message("该条数据不是待发起状态,禁止打印申请书");
        }
        CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(pvpLoanApp.getContNo());
        if (ctrloancont == null) {
            //如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("合同信息异常!");
        } else if (!"2".equals(ctrloancont.getContType())) {
            return new ResultDto(0).message("非最高额合同，不支持打印!");
        }
        //TODO 且为白领易贷通为最高额合同 暂时不知道怎么做 这里直接给他返回不成功
//        if (pvpLoanApp.getPvpSerno() != null) {
//            return new ResultDto(0).message("该放款申请关联合同非白领易贷通产品或非最高额合同");
//        }

        return new ResultDto(1).message("打印成功");
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:35
     * @注释 手动发送至核心出账
     */
    public ResultDto manualsend(String pvpSerno) {
        // 在【数据列表区】界面，选择单条记录，点击【手动发送】按钮；
        // 根据主键取获取对象
        log.info("查询对象");
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        log.info("查询到的对象" + pvpLoanApp);
        if (pvpLoanApp == null) {
            // 如果没查到这个对象 说明传入的参数有问题 返回
            return new ResultDto(0).message("操作失败,请刷新页面或稍后重试!");
        }
        // 将借据信息、受托支付信息实时发送核心进行记账，
        AccLoan accLoan = accLoanService.selectByPrimaryKey(pvpLoanApp.getBillNo());
        if (accLoan == null) {
            return new ResultDto(0).message("未查询到相关借据信息");
        }
        // TODO 啥是受托支付信息??? 暂时当作没查到
        //
        if (pvpLoanApp.getIsBeEntrustedPay().equals("1")) {
            return new ResultDto(0).message("未查询到相关受托支付信息");
        }
        // 核心系统调二代支付系统做实时受托，核心系统实时返回记账结果。

        return new ResultDto(1).message("发送成功,挡板");

    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-04-24 15:35
     * @注释 贷款出账申请待发起
     */
    public List<PvpLoanApp> signlist(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<PvpLoanApp> list = pvpLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-04-24 15:35
     * @注释 贷款出账申请已处理
     */
    public List<PvpLoanApp> solvedSignlist(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<PvpLoanApp> list = pvpLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 贷款出账申请新增
     *
     * @param pvpLoanApp
     * @return PvpLoanApp pvpLoanApp
     * @author:zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map savePvploanapp(PvpLoanApp pvpLoanApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String pvpSerno = "";
        String curType = pvpLoanApp.getCurType();
        String loanModal = pvpLoanApp.getLoanModal();
        BigDecimal exchangeRate = BigDecimal.ZERO;
        //查询合同对象
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
        if (ctrLoanCont == null) {
            HashMap<String, String> queryData = new HashMap<String, String>();
            queryData.put("contNo", pvpLoanApp.getContNo());
            CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.queryCtrHighAmtAgrContDataByParams(queryData);
            BeanUtils.copyProperties(ctrHighAmtAgrCont, pvpLoanApp);
            pvpLoanApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            //申请状态
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            //操作类型
            pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //合同起始日
            pvpLoanApp.setStartDate(ctrHighAmtAgrCont.getStartDate());
            //合同到期日
            pvpLoanApp.setEndDate(ctrHighAmtAgrCont.getEndDate());

            //合同金额
            pvpLoanApp.setContAmt(ctrHighAmtAgrCont.getAgrAmt());
            //合同币种
            pvpLoanApp.setContCurType(ctrHighAmtAgrCont.getAgrType());

            //合同最高可放金额，取合同余额
            pvpLoanApp.setContHighDisb(ctrHighAmtAgrCont.getAgrContHighAvlAmt());

            pvpLoanApp.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);//节假日是否顺延

            //合同影像是否审核
            pvpLoanApp.setIsContImageAudit(ctrHighAmtAgrCont.getCtrBeginFlag());

            pvpLoanApp.setLoanModal(pvpLoanApp.getLoanModal());

            pvpLoanApp.setIqpSerno(ctrHighAmtAgrCont.getSerno());

            // 产品类型属性
            pvpLoanApp.setPrdTypeProp(this.getPrdTypeProp(ctrHighAmtAgrCont.getLmtAccNo(), pvpLoanApp.getPrdId()));
            // 贷款期限单位
            pvpLoanApp.setLoanTermUnit("M");
        } else {
            //合同对象克隆给放款对象
            BeanUtils.copyProperties(ctrLoanCont, pvpLoanApp);
            // 根据合同类型得到确定的贷款形式，一般合同以合同的贷款形式为准，最高额合同以选择的贷款形式为准
            // 2021-11-15 17:15由业务提出不做控制，出账的贷款形式与合同没有关系
            pvpLoanApp.setLoanModal(loanModal);
            //是否受托支付
            pvpLoanApp.setIsBeEntrustedPay(ctrLoanCont.getPayMode());
            pvpLoanApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            //申请状态
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            //操作类型
            pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //合同起始日
            pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
            //合同到期日
            pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
            //利率调整方式
            pvpLoanApp.setRateAdjMode(ctrLoanCont.getIrAdjustType());
            //利率调整选项
            pvpLoanApp.setRateAdjType(ctrLoanCont.getIrAdjustType());
            //LPR利率区间
            pvpLoanApp.setLprRateIntval(ctrLoanCont.getLprRateIntval());
            //浮动点数
            pvpLoanApp.setRateFloatPoint(ctrLoanCont.getRateFloatPoint());
            //扣款扣息方式->扣款方式
            pvpLoanApp.setDeductType(ctrLoanCont.getDeductDeduType());
            //还款日->扣款日
            pvpLoanApp.setDeductDay(ctrLoanCont.getRepayDate());
            //逾期浮动
            pvpLoanApp.setOverdueRatePefloat(ctrLoanCont.getOverdueRatePefloat());
            //逾期浮动(年)
            pvpLoanApp.setOverdueExecRate(ctrLoanCont.getOverdueExecRate());
            //借款用途类型
            //pvpLoanApp.setLoanType(ctrLoanCont.getLoanPurp());
            //审批状态
            pvpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
            //担保方式
            pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());
            //贷款期限类型
            if (ctrLoanCont.getTermType()!=null && !"".equals(ctrLoanCont.getTermType())) {
                pvpLoanApp.setLoanTermUnit(ctrLoanCont.getTermType());
            } else {
                pvpLoanApp.setLoanTermUnit("M");
            }
            //是否立即发起受托支付
            pvpLoanApp.setIsCfirmPay(ctrLoanCont.getIsCfirmPayWay());
            //工业转型升级标识
            pvpLoanApp.setIndtUpFlag(ctrLoanCont.getComUpIndtify());
            //文化产业标识
            pvpLoanApp.setCulIndustryFlag(ctrLoanCont.getIsCulEstate());
            //合同金额
            pvpLoanApp.setContAmt(ctrLoanCont.getContAmt());
            //合同币种
            pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
            //币种
            pvpLoanApp.setCurType(curType);
            //合同影像是否审核
            pvpLoanApp.setIsContImageAudit(ctrLoanCont.getCtrBeginFlag());
            //合同最高可放金额，取合同余额
            pvpLoanApp.setContHighDisb(ctrLoanCont.getHighAvlAmt());
            //业务流水号非空
            pvpLoanApp.setIqpSerno(ctrLoanCont.getIqpSerno());

            pvpLoanApp.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);//节假日是否顺延
        }
        //渠道来源
        pvpLoanApp.setChnlSour("02");//02PC端
        //所属条线
        pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
        try {
            if (pvpLoanApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                //当前LPR利率 CURT_LPR_RATE==>CURT_LPR_RATE
                //第一次调整日 ==>FIRST_ADJ_DATE/api/lmtreplyacc
                //浮动点数 RATE_FLOAT_POINT==>RATE_FLOAT_POINT
//                pvpLoanApp = this.setData(pvpLoanApp);
                pvpLoanApp.setInputId(userInfo.getLoginCode());
                pvpLoanApp.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanApp.setManagerId(userInfo.getLoginCode());
                pvpLoanApp.setManagerBrId(userInfo.getOrg().getCode());
            }
            //放款流水号
            pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
            Map seqMap = new HashMap();
            seqMap.put("contNo", pvpLoanApp.getContNo());
            //借据编号
//            String billNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO, seqMap);
            String billNo = cmisBizXwCommonService.getBillNo(pvpLoanApp.getContNo());
            pvpLoanApp.setBillNo(billNo);//借据编号
            pvpLoanApp.setPvpSerno(pvpSerno);//主键为放款流水号
            int insertCount = this.insertSelective(pvpLoanApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            //引入合同中的还本计划
            this.getRepayPlanFromCont(pvpSerno);

            //引入合同中的交易对手信息
            this.getToppAcctInfoFromCont(pvpSerno);

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());
            creditReportQryLstAndRealDto.setCusId(pvpLoanApp.getCusId());
            creditReportQryLstAndRealDto.setCusName(pvpLoanApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(pvpSerno);
            creditReportQryLstAndRealDto.setScene("03");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
            result.put("iqpSerno", pvpLoanApp.getIqpSerno());
            result.put("pvpSerno", pvpSerno);
            result.put("billNo", billNo);
            result.put("replyNo", pvpLoanApp.getReplyNo());
            log.info("贷款出账申请" + pvpSerno + "-新增成功！");
        } catch (YuspException e) {
            log.error("贷款出账申请新增异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("贷款出账申请异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 引入合同中的还本计划
     *
     * @param pvpSerno
     * @return
     */
    public void getRepayPlanFromCont(String pvpSerno) {
        PvpLoanApp pvpLoanApp = this.selectByPvpLoanSernoKey(pvpSerno);
        if (Objects.isNull(pvpLoanApp)) {
            log.info("根据出账流水号【{}】，未查询到对应的出账申请信息", pvpSerno);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        // 还款方式（按期付息按计划还本时引入合同中的还本计划）
        String repayMode = pvpLoanApp.getRepayMode();
        if (repayMode != null && !"".equals(repayMode) && CmisCommonConstants.STD_REPAY_MODE_A040.equals(repayMode)) {
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
            if (Objects.isNull(ctrLoanCont)) {
                log.error("根据合同号【{}】，未查询到对应的出账申请信息", pvpLoanApp.getContNo());
                throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
            }
            // 合同总金额（折合人民币）
            BigDecimal contTotalRepayAmt = ctrLoanCont.getCvtCnyAmt();
            // 出账总金额（折合人民币）
            BigDecimal pvpTotalRepayAmt = pvpLoanApp.getCvtCnyAmt();
            // 得到合同中的还本计划
            RepayCapPlan repayCapPlan = new RepayCapPlan();
            List<RepayCapPlan> repayCapPlanList = repayCapPlanService.selectBySerno(pvpLoanApp.getIqpSerno());

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                log.error("获取当前登录人信息异常！");
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            for (int i = 0; i < repayCapPlanList.size(); i++) {
                // 每一期的还本计划
                RepayCapPlan repayCapPlanData = repayCapPlanList.get(i);
                if (i == repayCapPlanList.size() - 1) {
                    // 除去最后一期的所有期数金额之和
                    BigDecimal excludeLastAmt = BigDecimal.ZERO;
                    // 最后一期金额
                    BigDecimal lastRepayAmt = BigDecimal.ZERO;
                    List<RepayCapPlan> repayCapPlans = repayCapPlanService.selectBySerno(pvpLoanApp.getPvpSerno());
                    for (RepayCapPlan capPlan : repayCapPlans) {
                        excludeLastAmt = excludeLastAmt.add(capPlan.getRepayAmt());
                        lastRepayAmt = pvpLoanApp.getCvtCnyAmt().subtract(excludeLastAmt);
                    }
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setBillNo(pvpLoanApp.getBillNo());
                    repayCapPlan.setSerno(pvpLoanApp.getPvpSerno());
                    repayCapPlan.setTerms(repayCapPlanData.getTerms());
                    repayCapPlan.setRepayDate(repayCapPlanData.getRepayDate());
                    repayCapPlan.setRepayAmt(lastRepayAmt);
                    repayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    repayCapPlan.setInputId(userInfo.getLoginCode());
                    repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                    repayCapPlan.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    int insertCount = repayCapPlanService.insert(repayCapPlan);
                    if (insertCount <= 0) {
                        log.error("引入还本计划【{}】异常", pvpLoanApp.getPvpSerno());
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                } else {
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setBillNo(pvpLoanApp.getBillNo());
                    repayCapPlan.setSerno(pvpLoanApp.getPvpSerno());
                    repayCapPlan.setTerms(repayCapPlanData.getTerms());
                    repayCapPlan.setRepayDate(repayCapPlanData.getRepayDate());
                    BigDecimal everyRepayAmt = (pvpTotalRepayAmt.multiply(repayCapPlanData.getRepayAmt())).divide(contTotalRepayAmt, 2, BigDecimal.ROUND_UP);
                    repayCapPlan.setRepayAmt(everyRepayAmt);
                    repayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    repayCapPlan.setInputId(userInfo.getLoginCode());
                    repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                    repayCapPlan.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    int insertCount = repayCapPlanService.insert(repayCapPlan);
                    if (insertCount <= 0) {
                        log.error("引入还本计划【{}】异常", pvpLoanApp.getPvpSerno());
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        }
    }

    /**
     * 引入合同中的还本计划
     *
     * @param pvpSerno
     * @return
     */
    public void getToppAcctInfoFromCont(String pvpSerno) {
        PvpLoanApp pvpLoanApp = this.selectByPvpLoanSernoKey(pvpSerno);
        if (Objects.isNull(pvpLoanApp)) {
            log.info("根据出账流水号【{}】，未查询到对应的出账申请信息", pvpSerno);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        // 支付方式（受托支付时引入合同中的交易对手信息）
        String isBeEntrustedPay = pvpLoanApp.getIsBeEntrustedPay();
        if (isBeEntrustedPay != null && !"".equals(isBeEntrustedPay) && CmisCommonConstants.STD_RAY_MODE_1.equals(isBeEntrustedPay)) {
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
            if (Objects.isNull(ctrLoanCont)) {
                log.error("根据合同号【{}】，未查询到对应的出账申请信息", pvpLoanApp.getContNo());
                throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
            }
            // 得到合同中的交易对手信息
            ToppAcctSub toppAcctSub = new ToppAcctSub();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", ctrLoanCont.getIqpSerno());
            List<ToppAcctSub> toppAcctSubList = toppAcctSubService.selectBySerno(queryModel);

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                log.error("获取当前登录人信息异常！");
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            for (ToppAcctSub acctSub : toppAcctSubList) {
                BeanUtils.copyProperties(acctSub, toppAcctSub);
                toppAcctSub.setBizSerno(pvpSerno);
                toppAcctSub.setPkId(StringUtils.uuid(true));
                toppAcctSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                toppAcctSub.setInputId(userInfo.getLoginCode());
                toppAcctSub.setInputBrId(userInfo.getOrg().getCode());
                toppAcctSub.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                int insertCount = toppAcctSubService.insertSelective(toppAcctSub);
                if (insertCount <= 0) {
                    log.error("引入交易对手【{}】异常", pvpLoanApp.getPvpSerno());
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
    }

    /**
     * 获取基本信息
     *
     * @param pvpSerno
     * @return
     */
    public PvpLoanApp selectByPvpLoanSernoKey(String pvpSerno) {
        //return pvpLoanAppMapper.selectByPvpLoanSernoKey(iqpSerno);
        return pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
    }

    /**
     * 贷款出账申请修改
     *
     * @param pvpLoanApp
     * @returnf
     * @author:zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateSavePvpLoanApp(PvpLoanApp pvpLoanApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpLoanApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpLoanApp.setUpdId(userInfo.getLoginCode());
                pvpLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                pvpLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
                pvpLoanApp.setOprType("01");
            }

            Map seqMap = new HashMap();
            // 王玉坤确定放默认值00  20211121
            pvpLoanApp.setIrFloatType("00");
            int updCount = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            if (updCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
        } catch (YuspException e) {
            log.error("贷款出账申请修改异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("贷款出账申请异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-27 19:39
     * @注释 传入的调查编号 查询借据编号
     */
    public ResultDto selectbilllist(String surveySerno) {
        List<AccLoan> list = accLoanService.selectbilllist(surveySerno);
        return new ResultDto(list);
    }

    /**
     * @方法名称: handInvaild
     * @方法描述: 根据主键手工作废银承出账
     * @参数与返回说明:
     * @算法描述: 无PvpLoanApp pvpLoanApp
     */
    public int handInvaild(PvpLoanApp pvpLoanApp) {
        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
        return pvpLoanAppMapper.updateByPrimaryKey(pvpLoanApp);
    }

    /**
     * @方法名称: selectDataByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: WH-适用于小微-零售
     * @算法描述: 无
     */

    public List<PvpLoanApp> selectDataByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpLoanApp> list = pvpLoanAppMapper.selectDataByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 9:18
     * @注释 放款提交通过后的方法 审核 小微
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto Xwyw02end(PvpLoanApp pvpLoanAppData) {
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpLoanAppData.getPvpSerno());
        //授权申请交易流水号
        String tranSerNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_NO, new HashMap<>());
        //借据编号 2021年8月6日15:14:12  由原来自动生成改成在新增的时候生成
        String accLoanBillNo = pvpLoanApp.getBillNo();
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        AccLoan accLoan = new AccLoan();
        log.info("放款通过后操作-----------------开始 ---- 生成accloan 和pvpAuthorize表数据--------------");
        //通过后 发起生成两条数据
        try {
            BeanUtils.copyProperties(pvpLoanApp, pvpAuthorize);
            BeanUtils.copyProperties(pvpLoanApp, accLoan);
            //空位 需手动补充详细字段映射
            String pkid = StringUtils.uuid(true);
            // 主键
            accLoan.setPkId(pkid);
            // 借据号
            accLoan.setBillNo(accLoanBillNo);
            // loanAmt 出账金额
            accLoan.setLoanAmt(pvpLoanApp.getPvpAmt());
            // 贷款余额
            accLoan.setLoanBalance(pvpLoanApp.getPvpAmt());
            // 人民币余额
            accLoan.setExchangeRmbBal(pvpLoanApp.getPvpAmt());
            // 人民币金额
            accLoan.setExchangeRmbAmt(pvpLoanApp.getPvpAmt());
            // 正常本金
            accLoan.setZcbjAmt(pvpLoanApp.getPvpAmt());
            // 逾期本金
            accLoan.setOverdueCapAmt(BigDecimal.ZERO);
            // 欠息
            accLoan.setDebitInt(BigDecimal.ZERO);
            // 罚息
            accLoan.setPenalInt(BigDecimal.ZERO);
            // 复息
            accLoan.setCompoundInt(BigDecimal.ZERO);
            // 币种
            accLoan.setContCurType(pvpLoanApp.getContCurType());
            // 汇率 默认人民币
            accLoan.setExchangeRate(BigDecimal.ONE);
            // 所属条线
            accLoan.setBelgLine("01");
            // 台账状态 未出帐6
            accLoan.setAccStatus("6");
            // 原到期日期
            accLoan.setOrigExpiDate(pvpLoanApp.getLoanEndDate());
            // 五级分类日期
            accLoan.setClassDate(pvpLoanApp.getLoanStartDate());
            // 科目号
            accLoan.setSubjectNo(pvpLoanApp.getLoanSubjectNo());
            // 科目名称
            accLoan.setSubjectName(pvpAuthorizeService.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getAccountClassName());
            accLoan.setIsSegInterest("0");//否
            accLoan.setDeductType(pvpLoanApp.getDeductType());
            Map map = this.getFiveAndTenClass(pvpLoanApp.getCusId());
            // 五级分类 10--正常
            accLoan.setFiveClass((String) map.get("fiveClass"));
            accLoan.setCreateTime(DateUtils.getCurrDate());
            accLoan.setUpdateTime(DateUtils.getCurrDate());
            log.info("放款通过后操作-----------------开始 ---- 插入accloan数据--------------");
            int i = accLoanService.insertSelective(accLoan);
            log.info("放款通过后操作-----------------结束 ---- 插入accloan数据结束 插入条数" + i);

            log.info("放款通过后操作-----------------开始 ---- 获取当前系统日期");
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            pvpAuthorize.setTranDate(openday);

            pvpAuthorize.setTranSerno(tranSerNo);
            //借据编号  出账状态  未授权
            pvpAuthorize.setBillNo(accLoanBillNo);
            //未出帐--0
            pvpAuthorize.setAuthStatus("0");
            //未出帐--1
            pvpAuthorize.setTradeStatus("1");
            //放款金额
            pvpAuthorize.setTranAmt(pvpLoanApp.getPvpAmt());
            //TODO 开始塞01-45字段
            log.info("放款通过后操作-----------------开始 ---- 塞值01-45");
            pvpAuthorize.setFldvalue01(pvpLoanApp.getLoanSubjectNo());            //贷款科目代码
            pvpAuthorize.setFldvalue02(loanType(Integer.parseInt(pvpLoanApp.getLoanTerm()), pvpLoanApp.getCurType(), "pvpLoanApp.getBizType()", "1"));    //***贷款类型  monthDiffer 期限
            pvpAuthorize.setContNo(pvpLoanApp.getContNo());                        //合同号
            pvpAuthorize.setCusId(pvpLoanApp.getCusId());                        //借款人客户号
            pvpAuthorize.setCusName(pvpLoanApp.getCusName());                   //借款人客户名称
            pvpAuthorize.setFldvalue03(pvpLoanApp.getLoanPayoutAccno());           //收款人账户
            pvpAuthorize.setFldvalue04(pvpLoanApp.getRepayAccno());        //借款人结算账号
            pvpAuthorize.setFldvalue05(pvpLoanApp.getSbsyDepAccno());    //贴息人存款账号
            pvpAuthorize.setFldvalue06("");            //融资业务编号
            pvpAuthorize.setFldvalue07("");            //委托人存款账号
            pvpAuthorize.setFldvalue08(pvpLoanApp.getLoanEndDate());            //到期日
            pvpAuthorize.setFldvalue09(String.valueOf(pvpLoanApp.getLoanTerm())); //期限代码 TODO 取期限
            pvpAuthorize.setFldvalue10(pvpLoanApp.getCurType());                                //币种
            pvpAuthorize.setFldvalue11(pvpLoanApp.getCapAutobackFlag());        //本金自动归还标志    0否1是
            pvpAuthorize.setFldvalue12(pvpLoanApp.getIntAutobackFlag());         //利息自动归还标志     0否1是
            pvpAuthorize.setFldvalue13("");           //重定价代码
            pvpAuthorize.setFldvalue14(pvpLoanApp.getCurtLprRate().toString());           //基准利率代码
            pvpAuthorize.setFldvalue15(String.valueOf(pvpLoanApp.getRateFloatPoint()));    //正常贷款利率浮动比例
            pvpAuthorize.setFldvalue16(String.valueOf(pvpLoanApp.getOverdueRatePefloat()));    //逾期贷款利率浮动比例
//if("010007".equals(bizType)){
//pvpAuthorize.setFldvalue17("0.18");   //法人账户透支逾期贷款执行利率 万分之五
//}else{
            pvpAuthorize.setFldvalue17(String.valueOf(String.valueOf(pvpLoanApp.getOverdueExecRate())));    //逾期贷款执行利率
//}
            pvpAuthorize.setFldvalue18(String.valueOf(String.valueOf(pvpLoanApp.getExecRateYear())));    //正常贷款执行利率
            pvpAuthorize.setFldvalue19("");                        //还款方式      1-普通贷款 2-按揭分期贷款3-非按揭分期贷款 TODO
            pvpAuthorize.setFldvalue20("");                //普通贷款结息方式代码
            pvpAuthorize.setFldvalue21("");                //分期贷款还款方式代码
            pvpAuthorize.setFldvalue22("");                //分期贷款总期数
            pvpAuthorize.setFldvalue23(String.valueOf(pvpLoanApp.getSbsyPerc()));   //贴息比例
            pvpAuthorize.setFldvalue24(pvpLoanApp.getLoanTerm());                        //借据期限
            pvpAuthorize.setFldvalue25(pvpLoanApp.getLoanPromiseFlag());                //贷款承诺标志
            pvpAuthorize.setFldvalue26("");                //经销商账户名
            //1大中型企业贷款承诺 2小企业贷款承诺3个人贷款承诺
            pvpAuthorize.setFldvalue27(pvpLoanApp.getRepayAcctName());                //放款账户名
            pvpAuthorize.setFldvalue28("");            //委托存款子户号 TODO
//            pvpAuthorize.setFldvalue29(pvpLoanApp.getLoanForm());  //     TODO 无名              //贷款类型 1新增，3借新还旧
            pvpAuthorize.setFldvalue29("");  //     TODO 无名    //贷款类型 1新增，3借新还旧
            pvpAuthorize.setManagerId(pvpLoanApp.getManagerId());                        //客户经理号码
            pvpAuthorize.setFldvalue30(pvpLoanApp.getIsMeterCi());                        //是否计复息   0不计算、1计算
            pvpAuthorize.setFldvalue31(pvpLoanApp.getLoanUseType());        //用途代码     00  流动资金   01  固定资金  02  技术改造  04  流资转期
            //05  固资转期 06  财政性直贷 07  财政性自营贷款 08  政府指令性贷款   09  其他
            //分期贷款：10  住房  11  汽车 12  商铺  13  工程机械       19  其他
            pvpAuthorize.setFldvalue32(pvpLoanApp.getIsBeEntrustedPay());    //是否受托支付
            pvpAuthorize.setFldvalue33("");    //委托贷款手续费收取比例
            pvpAuthorize.setFldvalue34("");   //委托贷款手续费
            pvpAuthorize.setFldvalue35("");        //按揭还款日（缺省为放款日）
            pvpAuthorize.setFldvalue36(pvpLoanApp.getIrFloatType());    //贷款利率浮动方式 1  加百分比 2 加点 3正常加点逾期加百分比
            pvpAuthorize.setFldvalue37(pvpLoanApp.getRateFloatPoint().toString());    //正常利率浮动点数(年利率)
            pvpAuthorize.setFldvalue38(pvpLoanApp.getOverdueRatePefloat().toString());    //逾期利率浮动点数(年利率)
            pvpAuthorize.setFldvalue39("");                            //业务品种
            pvpAuthorize.setFldvalue41(pvpLoanApp.getDisbOrgNo());               //放贷机构
            //法人账户透支
            pvpAuthorize.setFldvalue42("");            //借用  执行利率方式
            pvpAuthorize.setFldvalue43("");           //复利年利率
            pvpAuthorize.setFldvalue44(pvpLoanApp.getLoanEndDate());                        //贷款到期日
            pvpAuthorize.setFldvalue45("");     //免息到期日
            pvpAuthorize.setCreateTime(new Date());
            pvpAuthorize.setUpdateTime(new Date());

            log.info("放款通过后操作-----------------插入集中放款表 ---- ");
            int i1 = pvpAuthorizeService.insertSelective(pvpAuthorize);
            log.info("放款通过后操作-----------------插入集中放款表 ---- 插入条数" + i1);
            //空位 需手动补充详细字段映射
            pvpLoanApp.setBillNo(accLoanBillNo);
            pvpLoanApp.setApproveStatus("997");

            int i2 = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            if (i != i1 || i1 != i2 || i2 != 1) {
                //任何一个不成立都证明有操作失误  回滚
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);

            }

            // 发送审批通过短信 目前仅房抵押循环贷发送短信 您好，您管户下的${cusName}客户申请的${prdName}已通过出账审批，请关注！
            // 2021年11月15日17:38:31 hubp 问题编号：20211115-00130 放开限制，所有都会发短信
            log.info("******************发送放款审批通过短信开始******************");
            // 发送客户经理
            String receivedUserType = "1";// 接收人员类型 1--客户经理 2--借款人
            Map paramMap = new HashMap();// 短信填充参数
            paramMap.put("cusName", pvpLoanApp.getCusName());
            paramMap.put("prdName", pvpLoanApp.getPrdName());
            //执行发送客户经理操作
            messageCommonService.sendMessage(MessageNoEnums.MSG_XW_M_0013.key, paramMap, receivedUserType, pvpLoanApp.getManagerId(), "");
            log.info("******************发送放款审批通过短信结束******************");
            return new ResultDto(3).message("操作成功");
        } catch (Exception e) {
            throw new YuspException("未知异常 回滚", e);

        }
    }

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    /**
     * 返回贷款类型 LN001短期普通企业贷款  LN002中长期普通企业贷款  LN003短期普通个人贷款 LN004中长期普通个人贷款
     * LN005短期普通企业外汇贷款 LN006中长期普通企业外汇贷款 LN007委托贷款
     * LN008存贷通额度贷款 LN009个人自助循环额度贷款  LN010垫款其中
     * LN008/LN009/LN010，由核心系统自行放款，不通过本接口发起
     */
    public String loanType(int monthDiffer, String curType, String bizType, String cusType) throws Exception {
        String loanType = null;
        if (curType.equals("CNY")) {
            if (bizType.equals("042175")) {//委托贷款
                loanType = "LN007";
            } else {//普通贷款
                if (cusType.equals("2")) {//对公
                    if (monthDiffer <= 12) {
                        loanType = "LN001";
                    } else {
                        loanType = "LN002";
                    }
                } else if (cusType.equals("1")) {//对私
                    if (monthDiffer <= 12) {
                        loanType = "LN003";
                    } else {
                        loanType = "LN004";
                    }
                }
            }
        } else {//外汇贷款cusindivcontact/selectByPrimaryKey
            if (monthDiffer <= 12) {
                loanType = "LN005";
            } else {
                loanType = "LN006";
            }
        }
        return loanType;
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 9:18
     * @注释 放款提交通过后的方法 零售
     */
    @Transactional(rollbackFor = Exception.class)
    public void reviewend(PvpLoanApp pvpLoanAppData) {
        log.info("零售放款申请审批结束后业务处理逻辑开始【{}】", pvpLoanAppData.getPvpSerno());

        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpLoanAppData.getPvpSerno());
        //授权申请交易流水号
        String tranSerNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_NO, new HashMap<>());
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        AccLoan accLoan = new AccLoan();
        //通过后 发起生成两条数据
        try {

            BeanUtils.copyProperties(pvpLoanApp, accLoan);
            //未出帐6
            accLoan.setAccStatus("6");
            //出账金额
            accLoan.setLoanAmt(pvpLoanApp.getPvpAmt());
            //币种
            accLoan.setContCurType(pvpLoanApp.getCurType());
            // TODO 2021年7月13日09:54:05 WH  台账缺失字段  补全
            //基准利率  rulingIr   取 LPR利率
            accLoan.setRulingIr(pvpLoanApp.getCurtLprRate());
            // 汇率 默认人民币-1
            accLoan.setExchangeRate(BigDecimal.ONE);
            // 原到期日期
            accLoan.setOrigExpiDate(accLoan.getLoanEndDate());
            // 贷款余额
            accLoan.setLoanBalance(pvpLoanApp.getPvpAmt());
            // 人民币余额
            accLoan.setExchangeRmbBal(pvpLoanApp.getPvpAmt());
            // 人民币金额
            accLoan.setExchangeRmbAmt(pvpLoanApp.getPvpAmt());
            // 正常本金
            accLoan.setZcbjAmt(pvpLoanApp.getPvpAmt());
            // 逾期本金
            accLoan.setOverdueCapAmt(BigDecimal.ZERO);
            // 欠息
            accLoan.setDebitInt(BigDecimal.ZERO);
            // 罚息
            accLoan.setPenalInt(BigDecimal.ZERO);
            // 复息
            accLoan.setCompoundInt(BigDecimal.ZERO);
            // 币种
            accLoan.setContCurType(pvpLoanApp.getContCurType());
            // 汇率 默认人民币
            accLoan.setExchangeRate(BigDecimal.ONE);
            // 科目号
            accLoan.setSubjectNo(pvpLoanApp.getLoanSubjectNo());
            // 科目名称
            accLoan.setSubjectName(pvpAuthorizeService.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getAccountClassName());
            //是否分段计息
            accLoan.setIsSegInterest(pvpLoanApp.getIsSegInterest());
            //扣款方式
            accLoan.setDeductType(pvpLoanApp.getDeductType());
            // 额度分项编号
            accLoan.setLmtAccNo(pvpLoanApp.getLmtAccNo());
            //是否受托支付
            accLoan.setIsBeEntrustedPay(pvpLoanApp.getPayMode());
            // 五级分类日期
            accLoan.setClassDate(pvpLoanApp.getLoanStartDate());
            // 五级分类 10--正常
            Map map = this.getFiveAndTenClass(pvpLoanApp.getCusId());
            accLoan.setFiveClass((String) map.get("fiveClass"));
            // 插入台账信息
            if (accLoanService.insertSelective(accLoan) != 1) {
                //回滚
                throw new YuspException(EcbEnum.E_IQP_INSERT_PARAM_FAILED.key, EcbEnum.E_IQP_INSERT_PARAM_FAILED.value);
            }


            BeanUtils.copyProperties(pvpLoanApp, pvpAuthorize);
            pvpAuthorize.setBelgLine("02");
            pvpAuthorize.setTranSerno(tranSerNo);
            //未出帐0
            pvpAuthorize.setAuthStatus("0");
            //交易金额
            pvpAuthorize.setTranAmt(pvpLoanApp.getPvpAmt());
            //未出帐
            pvpAuthorize.setTradeStatus("6");
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            pvpAuthorize.setTranDate(openday);
            // 插入出账授权信息
            if (pvpAuthorizeService.insertSelective(pvpAuthorize) != 1) {
                //回滚
                throw new YuspException(EcbEnum.E_IQP_INSERT_PARAM_FAILED.key, EcbEnum.E_IQP_INSERT_PARAM_FAILED.value);
            }

            pvpLoanApp.setApproveStatus("997");
            // 更新出账申请状态
            if (pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp) != 1) {
                //回滚
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }

        } catch (Exception e) {
            throw e;
        }
        log.info("零售放款申请审批结束后业务处理逻辑结束【{}】", pvpLoanAppData.getPvpSerno());
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/9 10:30
     * @注释 小微 放款审批提交
     */
    public ResultDto submityesornoxw(PvpLoanApp pvpLoanApp) {
        // 填充出账申请信息
        log.info("出账流水号【{}】，计算科目信息开始", pvpLoanApp.getPvpSerno());
        ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(pvpLoanApp.getPrdId());
        if (Objects.isNull(cfgPrdBasicinfoDto) || Objects.isNull(cfgPrdBasicinfoDto.getData())) {
            //  抛出错误异常
            return new ResultDto(false).message("未查询到产品信息！");
        }
        String subjectNo = pvpAuthorizeService.getClassSubNoForXw(pvpLoanApp, cfgPrdBasicinfoDto.getData()).getAccountClass();
        log.info("出账流水号【{}】，计算科目信息结束,科目号【{}】", pvpLoanApp.getPvpSerno(), subjectNo);

        // 现在需要查询合同是否已经签订 否则不允许审批
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
        if (!ctrLoanCont.getContStatus().equals(CmisBizConstants.IQP_CONT_STS_200)) {
            return new ResultDto(false).message("合同未生效 禁止提交");

        }
        //支付方式为受托支付的时候  要求录入交易对手信息

        if ("1".equals(pvpLoanApp.getPayMode())) {
            List list = toppAcctSubMapper.selectByBizId(pvpLoanApp.getPvpSerno());
            if (list.size() == 0) {
                return new ResultDto(false).message("支付方式为受托支付 需要录入交易对手信息");
            }
        }

        // 根据所选合同判断是否为房抵循环贷的存量合同放款申请，若是则插入借新还旧试算表
        log.info("根据批复编号【{}】获取批复信息开始！", ctrLoanCont.getReplyNo());
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(ctrLoanCont.getReplyNo());
        log.info("根据批复编号【{}】获取批复信息结束！查询结果【{}】", Objects.nonNull(lmtCrdReplyInfo));
        if (Objects.isNull(lmtCrdReplyInfo)) {
            return new ResultDto(false).message("未查询到批复信息！");
        }

        if ("1".equals(lmtCrdReplyInfo.getIsWxbxd()) && "SC010010".equals(lmtCrdReplyInfo.getPrdId())) {
            // 获取原借据信息
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtCrdReplyInfo.getSurveySerno());
            if (Objects.isNull(lmtSurveyReportBasicInfo)) {
                return new ResultDto(false).message("未查询到调查基本信息！");
            }
            String billNo = lmtSurveyReportBasicInfo.getOldBillNo();

            if (StringUtils.isBlank(billNo)) {
                return new ResultDto(false).message("获取原借据信息失败！");
            }

            // 插入借新还旧信息表
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", pvpLoanApp.getPvpSerno());
            List<PvpJxhjRepayLoan> pvpJxhjRepayLoanTempList = pvpJxhjRepayLoanService.selectByModel(queryModel);

            if (CollectionUtils.isEmpty(pvpJxhjRepayLoanTempList)) {
                log.info("放款流水号【{}】,插入借新还旧信息开始", pvpLoanApp.getPvpSerno());
                PvpJxhjRepayLoan pvpJxhjRepayLoan = new PvpJxhjRepayLoan();
                pvpJxhjRepayLoan.setSerno(pvpLoanApp.getPvpSerno());
                pvpJxhjRepayLoan.setBillNo(billNo);
                pvpJxhjRepayLoan.setNewLoanAmt(pvpLoanApp.getPvpAmt().toPlainString());
                // 优转续贷未到期本金默认和借新金额一致
                pvpJxhjRepayLoan.setNextPrcp(pvpLoanApp.getPvpAmt().toPlainString());
                pvpJxhjRepayLoan.setSelfPrcp("0");
                pvpJxhjRepayLoan.setSelfInt("0");
                pvpJxhjRepayLoan.setInputId(pvpLoanApp.getInputId());
                pvpJxhjRepayLoan.setInputBrId(pvpLoanApp.getInputBrId());
                pvpJxhjRepayLoan.setInputDate(openDay);
                pvpJxhjRepayLoan.setUpdId(pvpLoanApp.getInputId());
                pvpJxhjRepayLoan.setUpdBrId(pvpLoanApp.getInputBrId());
                pvpJxhjRepayLoan.setUpdDate(openDay);
                pvpJxhjRepayLoanService.insert(pvpJxhjRepayLoan);
                log.info("放款流水号【{}】,插入借新还旧信息结束", pvpLoanApp.getPvpSerno());
            } else {
                PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanTempList.get(0);
                pvpJxhjRepayLoan.setSerno(pvpLoanApp.getPvpSerno());
                pvpJxhjRepayLoan.setBillNo(billNo);
                pvpJxhjRepayLoan.setNewLoanAmt(pvpLoanApp.getPvpAmt().toPlainString());
                // 优转续贷未到期本金默认和借新金额一致
                pvpJxhjRepayLoan.setNextPrcp(pvpLoanApp.getPvpAmt().toPlainString());
                pvpJxhjRepayLoan.setSelfPrcp("0");
                pvpJxhjRepayLoan.setSelfInt("0");
                pvpJxhjRepayLoan.setInputId(pvpLoanApp.getInputId());
                pvpJxhjRepayLoan.setInputBrId(pvpLoanApp.getInputBrId());
                pvpJxhjRepayLoan.setInputDate(openDay);
                pvpJxhjRepayLoan.setUpdId(pvpLoanApp.getInputId());
                pvpJxhjRepayLoan.setUpdBrId(pvpLoanApp.getInputBrId());
                pvpJxhjRepayLoan.setUpdDate(openDay);
                pvpJxhjRepayLoanService.update(pvpJxhjRepayLoan);
                log.info("放款流水号【{}】,更新借新还旧信息", pvpLoanApp.getPvpSerno());
            }

            // 插入PVP_LOAN_APP_REPAY_BILL_RELL
            PvpLoanAppRepayBillRell pvpLoanAppRepayBillRellTemp = pvpLoanAppRepayBillRellService.selectByPvpSerno(pvpLoanApp.getPvpSerno());
            if (Objects.isNull(pvpLoanAppRepayBillRellTemp)) {
                log.info("放款流水号【{}】,插入借新还旧信息关系表开始", pvpLoanApp.getPvpSerno());
                PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = new PvpLoanAppRepayBillRell();
                pvpLoanAppRepayBillRell.setSerno(pvpLoanApp.getPvpSerno());
                pvpLoanAppRepayBillRell.setBillNo(billNo);
                pvpLoanAppRepayBillRell.setOprType("01");
                pvpLoanAppRepayBillRell.setInputId(pvpLoanApp.getInputId());
                pvpLoanAppRepayBillRell.setInputBrId(pvpLoanApp.getInputBrId());
                pvpLoanAppRepayBillRell.setInputDate(openDay);
                pvpLoanAppRepayBillRell.setUpdId(pvpLoanApp.getInputId());
                pvpLoanAppRepayBillRell.setUpdBrId(pvpLoanApp.getInputBrId());
                pvpLoanAppRepayBillRell.setUpdDate(openDay);
                pvpLoanAppRepayBillRellService.insert(pvpLoanAppRepayBillRell);
                log.info("放款流水号【{}】,插入借新还旧信息关系表结束", pvpLoanApp.getPvpSerno());
            } else {
                PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = pvpLoanAppRepayBillRellTemp;
                pvpLoanAppRepayBillRell.setSerno(pvpLoanApp.getPvpSerno());
                pvpLoanAppRepayBillRell.setBillNo(billNo);
                pvpLoanAppRepayBillRell.setOprType("01");
                pvpLoanAppRepayBillRell.setInputId(pvpLoanApp.getInputId());
                pvpLoanAppRepayBillRell.setInputBrId(pvpLoanApp.getInputBrId());
                pvpLoanAppRepayBillRell.setInputDate(openDay);
                pvpLoanAppRepayBillRell.setUpdId(pvpLoanApp.getInputId());
                pvpLoanAppRepayBillRell.setUpdBrId(pvpLoanApp.getInputBrId());
                pvpLoanAppRepayBillRell.setUpdDate(openDay);
                pvpLoanAppRepayBillRellService.update(pvpLoanAppRepayBillRell);
                log.info("放款流水号【{}】,更新借新还旧信息关系表", pvpLoanApp.getPvpSerno());
            }

        }

        // 向额度系统去校验额度  CmisLmt0010
        try {
            CmisLmt0010RespDto cmisLmt0010RespDtoResultDto = this.cmisLmt0010ForXw(pvpLoanApp);
        } catch (Exception e) {
            log.error("***********调用cmisLmt0010 异常**************", pvpLoanApp.getPvpSerno());
            return new ResultDto(false).message("调用额度系统台账校验失败");

        }

        // 批复编号
        pvpLoanApp.setReplyNo(ctrLoanCont.getReplyNo());
        // 调查编号
        pvpLoanApp.setSurveySerno(ctrLoanCont.getSurveySerno());
        // 额度分项编号
        pvpLoanApp.setLmtAccNo(ctrLoanCont.getLmtAccNo());
        // 科目号
        pvpLoanApp.setLoanSubjectNo(subjectNo);
        // 折算人民币金额
        pvpLoanApp.setCvtCnyAmt(pvpLoanApp.getPvpAmt());
        // 汇率 默认1
        pvpLoanApp.setExchangeRate(BigDecimal.ONE);
        // 更新
        int count = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        if (count != 1) {
            return new ResultDto(false).message("数据更新失败");
        }

        return new ResultDto(true).message("提交成功");
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:10
     * @注释 小微-放款单是否可以提交审批 先做个挡板 根据后续需求补全逻辑
     */
    public ResultDto submityesorno(PvpLoanApp pvpLoanApp) {

        // 填充出账申请信息
        log.info("出账流水号【{}】，计算科目信息开始", pvpLoanApp.getPvpSerno());
        CfgAccountClassChooseDto cfgAccountClassChooseDto = cmisBizXwCommonService.caluAccountClass(pvpLoanApp);
        String subjectNo = cfgAccountClassChooseDto.getAccountClass();
        log.info("出账流水号【{}】，计算科目信息结束,科目号【{}】", pvpLoanApp.getPvpSerno(), subjectNo);
        // 科目号
        pvpLoanApp.setLoanSubjectNo(subjectNo);
        // 折算人民币金额
        pvpLoanApp.setCvtCnyAmt(pvpLoanApp.getPvpAmt());
        // 汇率 默认1
        pvpLoanApp.setExchangeRate(BigDecimal.ONE);
        int count = pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
        if (count != 1) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "数据更新失败");
        }

        //TODO 现在需要查询合同是否已经签订 否则不允许审批
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
        try {
            if (!ctrLoanCont.getContStatus().equals(CmisBizConstants.IQP_CONT_STS_200)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "合同未生效 不允许提交");
            }
        } catch (Exception e) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "合同未生效 不允许提交");
        }
        //支付方式为受托支付的时候  要求录入交易对手信息

        if ("1".equals(pvpLoanApp.getPayMode())) {
            List list = toppAcctSubMapper.selectByBizId(pvpLoanApp.getPvpSerno());
            if (list.size() == 0) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "支付方式为受托支付 需要录入交易对手信息");
            }
        }
        //征信校验
//        022001个人一手住房按揭贷款
//        022002个人二手住房按揭贷款
//        022040个人二手住房按揭贷款（资金托管）
//        022052个人一手住房按揭贷款（常熟资金监管）
//        022053个人二手住房按揭贷款（连云港资金托管）
//        022055个人一手住房按揭贷款（宿迁资金监管）
//        if("022001,022002,022040,022052,022053,022055".indexOf(pvpLoanApp.getPrdId()) != -1){
//            Boolean bool = true;
//            String openday = stringRedisTemplate.opsForValue().get("openDay");
//            String iqpSerno = pvpLoanApp.getIqpSerno();
//
//            QueryModel creditReportQryModel = new QueryModel();
//            creditReportQryModel.addCondition("bizSerno", iqpSerno);
//            List<CreditReportQryLstDto> creditReportQryLstDtoList = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(creditReportQryModel);
//            if(creditReportQryLstDtoList.size() >0){
//                for (int i=0;i<creditReportQryLstDtoList.size();i++){
//                    CreditReportQryLstDto creditReportQryLstDto = creditReportQryLstDtoList.get(i);
//                    //与主借款人关系：主借款人，征信查询状态：已查询
//                    if("001".equals(creditReportQryLstDto.getBorrowRel()) && "003".equals(creditReportQryLstDto.getQryStatus())){
//                        if(openday.equals(creditReportQryLstDto.getReportCreateTime().substring(0,10))){
//                            bool = false;
//                        }
//                    }
//                }
//            }
//            if(bool){
//                return new ResultDto(false).message("个人住房按揭类产品在放款当天不存在报告生成日期为当天的报告，不允许放款");
//            }
//        }


        return new ResultDto(true).message("提交成功");
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:29
     * @注释 查询单条数据 可能要新增其他的参数 返回对象请自行斟酌,适用于小微
     */
    public ResultDto selectByPvpSernoForXd(String pvpSerno) {
        PvpLoanAppPojo pvpLoanAppPojo = pvpLoanAppMapper.selectByPvpSerno(pvpSerno);
        // 存入还款日  ->如果当前借款人已经有合同申请记录 那么就按照以前的走  没有就按照新的走
        //查找以前的合同申请信息
        if (StringUtils.isBlank(pvpLoanAppPojo.getDeductDay())) {
            // 合同申请表中暂无该字段 略过  TODO 2021年7月1日23:50:26 坤说还款日==扣款日
            RepayDayRecordInfo newData = repayDayRecordInfoMapper.selectNewData();
            if (newData == null) {
                pvpLoanAppPojo.setDeductDay("5");
                return new ResultDto(pvpLoanAppPojo);
            }
            pvpLoanAppPojo.setDeductDay(newData.getRepayDate().toString());

        }
        return new ResultDto(pvpLoanAppPojo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:29
     * @注释 查询单条数据 可能要新增其他的参数 返回对象请自行斟酌
     */
    public ResultDto selectByPvpSernoForLs(String pvpSerno) {
        PvpLoanAppPojo pvpLoanApp = pvpLoanAppMapper.selectByPvpSerno(pvpSerno);
        return new ResultDto(pvpLoanApp);
    }

    /**
     * @方法名称: updateApproveStatusByPvpSerno
     * @方法描述: 作废出帐信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateApproveStatusByPvpSerno(String pvpSerno) {
        return pvpLoanAppMapper.updateApproveStatusByPvpSerno(pvpSerno);
    }

    /**
     * @方法名称: getPvpLoanAppList
     * @方法描述: 查询贷款出账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.xdht0012.resp.PvpList> getPvpLoanAppList(Map queryMap) {
        return pvpLoanAppMapper.getPvpLoanAppList(queryMap);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:29
     * @注释 保存单条数据
     */
    public ResultDto<Integer> saveandupdate(PvpLoanApp pvpLoanApp) {
        PvpLoanApp pvp = pvpLoanAppMapper.selectByPrimaryKey(pvpLoanApp.getPvpSerno());
        if (pvp == null) {
            return new ResultDto<Integer>(0).message("数据不存在");
        }
        if ("111".equals(pvp.getApproveStatus())) {
            return new ResultDto<>(0).message("只能修改审批状态为待发起或退回且责任人是自己的放款");
        }
        // 折算人民币金额
        pvpLoanApp.setCvtCnyAmt(pvpLoanApp.getPvpAmt());
        // 汇率
        pvpLoanApp.setExchangeRate(BigDecimal.ONE);
        int i = this.update(pvpLoanApp);
        return new ResultDto<>(i).message("操作成功");
    }

    /**
     * @方法名称: riskItem0014
     * @方法描述: 白领易贷通产品校验
     * @参数与返回说明:
     * @算法描述: 放款申请提交时点触发：
     * （1）还款方式=按月付息到期还本或利随本清，贷款期限必须小于等于1年，否则拦截
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:26:07
     * @修改记录: 修改时间：2021年8月10日08:42:10    修改人员：hubp    修改原因
     */
    public RiskResultDto riskItem0014(String serno) {
        log.info("授信申请担保信息校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(serno);
        if (Objects.isNull(pvpLoanApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001405);
            return riskResultDto;
        } else {
            if ("022028".equals(pvpLoanApp.getPrdId())) {
                if (StringUtils.isBlank(pvpLoanApp.getRepayMode()) || StringUtils.isBlank(pvpLoanApp.getLoanTerm()) || StringUtils.isBlank(pvpLoanApp.getPrdId())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001406);
                    return riskResultDto;
                } else {
                    //还款方式=按月付息到期还本或利随本清，贷款期限必须小于等于1年，否则拦截
                    String repayMode = pvpLoanApp.getRepayMode();//还款方式
                    int loanTerm = Integer.parseInt(pvpLoanApp.getLoanTerm());//贷款期限
                    //A009利随本清
                    //A001按期付息到期还本
                    if ("A001".equals(repayMode) || "A009".equals(repayMode)) {
                        if (loanTerm > 12) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001404);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        log.info("授信申请担保信息校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }


    /**
     * @方法名称: riskItem0031
     * @方法描述: 受托支付检验
     * @参数与返回说明:
     * @算法描述: 交易方式=受托支付是：
     * 1、受托支付账号拦截校验：
     * （1）交易对手账号不能为空
     * （2）交易对手账号不能重复
     * （3）交易对手账号不能与贷款发放账号重复
     * 2、交易对手名称含有以下字段，拦截：
     * （1）交易对手仅存在"即墨市汉裕应急扶持基金合伙企业"时，不拦截且不触发下述拦截
     * （2）不满足（1）时，同时执行以下判断：
     * （2-1）若交易对手名称存在"基金、证券、国债、账务公司、期货、经纪、担保、土地"任一一个，拦截。
     * （2-2）若交易对手存在"置业、地产、房地产、不动产、房产、房屋"且产品为园区置业贷、个人按揭类消费贷款、法人按揭类贷款时，不拦截，否则拦截
     * <p>
     * （3）交易对手账号不能与贷款发放账号重复
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0031(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("受托支付检验开始*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        // 对公贷款出账流程、小微放款申请、零售放款申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType) ||
                "SGE04".equals(bizType) || "DHE04".equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_DHD02.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(serno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            riskResultDto = riskItem0031Check(pvpLoanApp.getPvpSerno(), pvpLoanApp.getIsBeEntrustedPay(), pvpLoanApp.getLoanPayoutAccno(), pvpLoanApp.getPrdId(), pvpLoanApp.getCvtCnyAmt());
            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }

        // 对公贷款出账流程、小微放款申请、零售放款申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
            if (Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            riskResultDto = riskItem0031Check(iqpLoanApp.getIqpSerno(), iqpLoanApp.getPayMode(), iqpLoanApp.getLoanPayoutAccno(), iqpLoanApp.getPrdId(), iqpLoanApp.getCvtCnyAmt());
            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }

        // 委托贷款出账申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGD04.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_DHD04.equals(bizType)) {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(serno);
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            riskResultDto = riskItem0031Check(pvpEntrustLoanApp.getPvpSerno(), pvpEntrustLoanApp.getIsBeEntrustedPay(), pvpEntrustLoanApp.getLoanPayoutAccno(), pvpEntrustLoanApp.getPrdId(), pvpEntrustLoanApp.getPvpAmt());
            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param serno
     * @param isBeEntrustedPay
     * @return
     */
    public RiskResultDto riskItem0031Check(String serno, String isBeEntrustedPay, String loanPayoutAccno, String prdId, BigDecimal appAmt) {
        RiskResultDto riskResultDto = new RiskResultDto();
        // 支付方式，受托支付：1
        if (Objects.equals(CmisCommonConstants.STD_ZB_YES_NO_1, isBeEntrustedPay)) {
            // 查询受托支付名单
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("bizSerno", serno);
            List<ToppAcctSub> list = toppAcctSubService.queryToppAcctSub(queryModel);
            if (CollectionUtils.isEmpty(list)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03101);
                return riskResultDto;
            }
            Set<String> set = new HashSet<>();
            // 交易对手名称
            String toppName = "";
            // 交易对手总金额
            BigDecimal totalToppAcctAmt = BigDecimal.ZERO;
            for (ToppAcctSub toppAcctSub : list) {
                // 交易对手账号不能为空
                if (Objects.isNull(toppAcctSub) || StringUtils.isEmpty(toppAcctSub.getToppAcctNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03103);
                    return riskResultDto;
                }

                totalToppAcctAmt = totalToppAcctAmt.add(toppAcctSub.getToppAmt());

                // 交易对手账号不能与贷款发放账号重复
                if (Objects.equals(loanPayoutAccno, toppAcctSub.getToppAcctNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03104);
                    return riskResultDto;
                }
                toppName = toppAcctSub.getToppName();
                if (StringUtils.isEmpty(toppName)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03105);
                    return riskResultDto;
                }
                // 交易对手名称含有以下字段，交易对手仅存在"即墨市汉裕应急扶持基金合伙企业"时，不拦截且不触发下述拦截
                if (toppName.contains("即墨市汉裕应急扶持基金合伙企业") && list.size() == 1) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                }
                // 若交易对手名称存在"基金、证券、国债、账务公司、期货、经纪、担保、土地"任一一个，拦截。
                if (toppName.contains("基金") ||
                        toppName.contains("证券") ||
                        toppName.contains("国债") ||
                        toppName.contains("账务公司") ||
                        toppName.contains("期货") ||
                        toppName.contains("经纪") ||
                        toppName.contains("担保") ||
                        toppName.contains("土地")) {
                    AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
                    adminSmPropQueryDto.setPropName(CmisCommonConstants.TOPP_ACCT_NAME);
                    AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
                    if (Objects.isNull(adminSmPropDto)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_014);
                        return riskResultDto;
                    }
                    String toppAcctNames = adminSmPropDto.getPropValue();
                    String[] toppAcctNameArr = toppAcctNames.split(CmisCommonConstants.DEF_SPILIT_COMMMA);
                    for (String s : toppAcctNameArr) {
                        if(s.equals(toppName)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                            return riskResultDto;
                        }
                    }
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03106);
                    return riskResultDto;
                }
                // （2-2）若交易对手存在"置业、地产、房地产、不动产、房产、房屋"且产品为园区置业贷、个人按揭类消费贷款、法人按揭类贷款时，不拦截，否则拦截
                // 园区置业贷 620002
                // 以下是(个人按揭类消费贷款)
                //022001	个人一手住房按揭贷款
                //022002	个人二手住房按揭贷款
                //022020	个人一手商用房按揭贷款
                //022021	个人二手商用房按揭贷款
                //022024	接力贷
                //022031	拍卖贷
                //022040	个人二手住房按揭贷款（资金托管）
                //022051	个人二手商用房按揭贷款（资金托管）
                //022052	个人一手住房按揭贷款（常熟资金监管）
                //022053	个人二手住房按揭贷款（连云港资金托管）
                //022054	个人二手商用房按揭贷款（连云港资金托管）
                //022055	个人一手住房按揭贷款（宿迁资金监管）
                //022056	个人一手商用房按揭贷款（宿迁资金监管）
                // 法人按揭类贷款 (法人按揭贷款 012003、其他法人按揭贷款 012006)
                // 2021年8月24日14:51:04 在不为园区置业贷、个人按揭类消费贷款、法人按揭类贷款时，才去校验字段   hubp
                if (!Objects.equals("620002", prdId) &&
                        !Objects.equals("022001", prdId) &&
                        !Objects.equals("022002", prdId) &&
                        !Objects.equals("022020", prdId) &&
                        !Objects.equals("022021", prdId) &&
                        !Objects.equals("022024", prdId) &&
                        !Objects.equals("022031", prdId) &&
                        !Objects.equals("022040", prdId) &&
                        !Objects.equals("022051", prdId) &&
                        !Objects.equals("022052", prdId) &&
                        !Objects.equals("022053", prdId) &&
                        !Objects.equals("022054", prdId) &&
                        !Objects.equals("022055", prdId) &&
                        !Objects.equals("022056", prdId) &&
                        !Objects.equals("012003", prdId) &&
                        !Objects.equals("012006", prdId)) {
                    if (toppName.contains("置业") ||
                            toppName.contains("地产") ||
                            toppName.contains("房地产") ||
                            toppName.contains("不动产") ||
                            toppName.contains("房产") ||
                            toppName.contains("房屋")) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03107);
                        return riskResultDto;
                    }
                }
                set.add(toppAcctSub.getToppAcctNo());
            }
            if (list.size() > set.size()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03108);
                return riskResultDto;
            }

            if (totalToppAcctAmt.compareTo(appAmt) != 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03109);
                return riskResultDto;
            }
        } else if (Objects.equals(CmisCommonConstants.STD_ZB_YES_NO_0, isBeEntrustedPay)) {
            //信保贷,徐信保,无锡园区保,宿迁园区保,南通信保通,青岛即墨政银保产品,对公用信出账环节强制受托支付，受托支付只能为是
            if ("P010".equals(prdId) || "P017".equals(prdId) || "PO20".equals(prdId) || "PO21".equals(prdId) || "PO22".equals(prdId) || "PO23".equals(prdId)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04511);
            }
        }
        return riskResultDto;
    }


    /**
     * @方法名称: riskItem0026
     * @方法描述: 放款环节日期校验
     * @参数与返回说明:
     * @算法描述: 1.一般合同且授信额度为循环时：
     * 1)合同起始日<=放款起始日 && 放款到期日<=合同到期日
     * 2)放款起始日<放款到期日
     * <p>
     * 2.一般合同且授信额度为非循环时
     * 1)合同起始日<=放款起始日 && 放款到期日<=合同到期日
     * 2)放款起始日<放款到期日
     * 3）放款期限<=授信期限
     * 3.最高额合同且授信额度为循环时
     * 1)合同起始日<=放款起始日
     * 2)放款起始日<放款到期日
     * 3）放款期限<=授信期限
     * <p>
     * 4.最高额合同且授信额度为非循环时
     * 1)合同起始日<=放款起始日
     * 2)放款起始日<放款到期日
     * 3)放款到期日<=授信到期日+宽限期
     * 4）放款期限<=授信期限
     * 5.特定产品控制补充规则如下
     * 1)诚易融资：放款到期日<=授信到期日
     * 2）白领易贷通：放款到期日<=授信到期日
     * @创建人: liuqi
     * @创建时间: 2021-06-23
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0026(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("放款环节日期校验开始*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        // 是否循环额度
        String isRevolvLimit = null;
        int loanTerm, lmtTerm;
        // 对公贷款出账申请
        // 零售放款申请
        // 小微放款申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)) {
            PvpLoanApp pvpLoanApp = selectByPrimaryKey(serno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 对公贷款出账
            if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType)) {
                // 合同开始日
                String contStartDate = null;
                // 合同结束日
                String contEndDate = null;
                // 合同类型
                String contType = null;
                //根据合同编号获取合同详情
                Map map = new HashMap();
                LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
                CtrLoanCont ctrLoanCon = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if (Objects.isNull(ctrLoanCon)) {
                    CtrHighAmtAgrCont ctrhighamtagrcont = ctrHighAmtAgrContService.selectDataByContNo(pvpLoanApp.getContNo());
                    if (Objects.isNull(ctrhighamtagrcont)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                        return riskResultDto;
                    } else {
                        lmtReplyAccSubPrd = lmtReplyAccService.isExistAccSubOnHighAmtAgrCont(pvpLoanApp.getLmtAccNo(), pvpLoanApp.getPrdId());
                        if (Objects.isNull(lmtReplyAccSubPrd)) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02613);
                            return riskResultDto;
                        }
                        contStartDate = ctrhighamtagrcont.getStartDate();
                        contEndDate = ctrhighamtagrcont.getEndDate();
                        contType = ctrhighamtagrcont.getContType();
                    }
                } else {
                    try {
                        lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(pvpLoanApp.getLmtAccNo());
                    } catch (Exception e) {
                        e.printStackTrace();
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02612);
                        return riskResultDto;
                    }
                    if (Objects.isNull(lmtReplyAccSubPrd)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02601);
                        return riskResultDto;
                    }
                    contStartDate = ctrLoanCon.getContStartDate();
                    contEndDate = ctrLoanCon.getContEndDate();
                    contType = ctrLoanCon.getContType();

                }
                // 根据分项信息，查询起始日到期日期限
                CmisLmt0036RespDto cmisLmt0036RespDto = getCmisLmt0036ReqDto(pvpLoanApp.getLmtAccNo());
                if (Objects.isNull(cmisLmt0036RespDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02622);
                    return riskResultDto;
                }

                // 额度是否循环非循环(0:否 1：是)
                isRevolvLimit = lmtReplyAccSubPrd.getIsRevolvLimit();
                if (!Objects.equals("0", isRevolvLimit) && !Objects.equals("1", isRevolvLimit)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02602);
                    return riskResultDto;
                }
                // 日期不能为空判定
                if (StringUtils.isEmpty(contStartDate)
                        || StringUtils.isEmpty(contEndDate)
                        || StringUtils.isEmpty(pvpLoanApp.getLoanStartDate())
                        || StringUtils.isEmpty(pvpLoanApp.getLoanEndDate())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02603);
                    return riskResultDto;
                }
                // 一般合同
                if (CmisBizConstants.STD_CONT_TYPE_1.equals(contType)) {
                    // 一般合同且授信额度为循环或者非循环时，合同起始日<=放款起始日 并且 放款到期日<=合同到期日
                    // 一般合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    // 一般合同且授信额度为非循环时，放款期限<=授信期限
                    if (contStartDate.compareTo(pvpLoanApp.getLoanStartDate()) > 0
                            || contEndDate.compareTo(pvpLoanApp.getLoanEndDate()) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02604);
                        return riskResultDto;
                    }
                    // 放款起始日<放款到期日
                    if (pvpLoanApp.getLoanStartDate().compareTo(pvpLoanApp.getLoanEndDate()) >= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02605);
                        return riskResultDto;
                    }
                    // 一般合同且授信额度为非循环时，放款期限<=授信期限
                    if (Objects.equals("0", isRevolvLimit)) {
                        loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpLoanApp.getLoanTerm()) ? pvpLoanApp.getLoanTerm() : "0");
                        lmtTerm = Objects.isNull(cmisLmt0036RespDto.getTerm()) ? 0 : cmisLmt0036RespDto.getTerm();
                        if (loanTerm > lmtTerm) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                            return riskResultDto;
                        }
                    }
                }
                // 最高额合同
                if (CmisBizConstants.STD_CONT_TYPE_2.equals(contType)) {
                    // 最高额合同且授信额度为循环或者非循环时，合同起始日<=放款起始日
                    // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                    // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                    if (contStartDate.compareTo(pvpLoanApp.getLoanStartDate()) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                        return riskResultDto;
                    }
                    // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    if (pvpLoanApp.getLoanStartDate().compareTo(pvpLoanApp.getLoanEndDate()) >= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02608);
                        return riskResultDto;
                    }
                    // 获取授信到期日
                    Date appEndDate = null;
                    if (StringUtils.nonEmpty(cmisLmt0036RespDto.getEndDate())) {
                        appEndDate = DateUtils.parseDate(cmisLmt0036RespDto.getEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(appEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02610);
                        return riskResultDto;
                    }
                    Date LoanEndDate1 = DateUtils.parseDate(pvpLoanApp.getLoanEndDate(), "yyyy-MM-dd");
                    // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                    if (LoanEndDate1.compareTo(DateUtils.addMonth(appEndDate, cmisLmt0036RespDto.getTerm())) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02611);
                        return riskResultDto;
                    }
                    // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                    if (Objects.equals("0", isRevolvLimit)) {
                        loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpLoanApp.getLoanTerm()) ? pvpLoanApp.getLoanTerm() : "0");
                        lmtTerm = Objects.isNull(cmisLmt0036RespDto.getTerm()) ? 0 : cmisLmt0036RespDto.getTerm();
                        if (loanTerm > lmtTerm) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                            return riskResultDto;
                        }
                    }
                }
                // 特定产品控制补充规则如下
                //1、诚易融：放款到期日<=授信到期日
                //2、白领易贷通：放款到期日<=授信到期日
                if (Objects.equals("P016", lmtReplyAccSubPrd.getLmtBizTypeProp()) || Objects.equals("022028", pvpLoanApp.getPrdId())) {
                    // 获取授信到期日
                    Date appEndDate = null;
                    // 放款到期日
                    Date loanEndDate = null;
                    if (StringUtils.nonEmpty(cmisLmt0036RespDto.getEndDate())) {
                        appEndDate = DateUtils.parseDate(cmisLmt0036RespDto.getEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(appEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02615);
                        return riskResultDto;
                    }
                    if (StringUtils.nonEmpty(pvpLoanApp.getEndDate())) {
                        loanEndDate = DateUtils.parseDate(pvpLoanApp.getEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(loanEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02616);
                        return riskResultDto;
                    }
                    if (DateUtils.compare(loanEndDate, appEndDate) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                        return riskResultDto;
                    }
                }
                //3、结息贷、优税贷：单笔提款期限不超过1年
                //   单笔提款到期日，必须在合同到日期以内
                if (Objects.equals("P013", lmtReplyAccSubPrd.getLmtBizTypeProp()) || Objects.equals("P014", lmtReplyAccSubPrd.getLmtBizTypeProp())) {
                    if (StringUtils.isBlank(pvpLoanApp.getLoanTerm())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02618);
                        return riskResultDto;
                    }
                    if (Integer.parseInt(pvpLoanApp.getLoanTerm()) > 12) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02619);
                        return riskResultDto;
                    }
                    if (Objects.isNull(pvpLoanApp.getEndDate()) || Objects.isNull(pvpLoanApp.getLoanEndDate())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02620);
                        return riskResultDto;
                    }
                    if (pvpLoanApp.getEndDate().compareTo(pvpLoanApp.getLoanEndDate()) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02620);
                        return riskResultDto;
                    }
                }
            }
            if (CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType) ||
                    CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType) ||
                    CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)) {
                // 宽限期 小微、零售默认为0
                int lmtGraper = 0;
                //根据合同编号获取合同详情
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if (Objects.isNull(ctrLoanCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                }
                // 2021年9月10日16:24:04 hubp 修改调用批复方法
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(pvpLoanApp.getReplyNo());
                if (Objects.isNull(lmtCrdReplyInfo)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02601);
                    return riskResultDto;
                }
                // 额度是否循环非循环（[{"key":"01","value":"临时额度"},{"key":"02","value":"循环额度"}]）
                isRevolvLimit = lmtCrdReplyInfo.getLimitType();
                if (!Objects.equals("01", isRevolvLimit) && !Objects.equals("02", isRevolvLimit)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02602);
                    return riskResultDto;
                }
                // 一般合同
                if (CmisBizConstants.STD_CONT_TYPE_1.equals(ctrLoanCont.getContType())) {
                    // 一般合同且授信额度为循环或者非循环时，合同起始日<=放款起始日 并且 放款到期日<=合同到期日
                    // 一般合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    // 一般合同且授信额度为非循环时，放款期限<=授信期限
                    if (ctrLoanCont.getContStartDate().compareTo(pvpLoanApp.getLoanStartDate()) > 0
                            || ctrLoanCont.getContEndDate().compareTo(pvpLoanApp.getLoanEndDate()) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02604);
                        return riskResultDto;
                    }
                    // 放款起始日<放款到期日
                    if (pvpLoanApp.getLoanStartDate().compareTo(pvpLoanApp.getLoanEndDate()) >= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02605);
                        return riskResultDto;
                    }
                    // 放款期限<=授信期限
                    if (Objects.equals("01", isRevolvLimit)) {
                        loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpLoanApp.getLoanTerm()) ? pvpLoanApp.getLoanTerm() : "0");
                        lmtTerm = Objects.isNull(lmtCrdReplyInfo.getAppTerm()) ? 0 : lmtCrdReplyInfo.getAppTerm();
                        if (loanTerm > lmtTerm) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                            return riskResultDto;
                        }
                    }
                }
                // 最高额合同
                if (CmisBizConstants.STD_CONT_TYPE_2.equals(ctrLoanCont.getContType())) {
                    // 最高额合同且授信额度为循环或者非循环时，合同起始日<=放款起始日
                    // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                    // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                    if (ctrLoanCont.getContStartDate().compareTo(pvpLoanApp.getLoanStartDate()) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                        return riskResultDto;
                    }
                    // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                    if (pvpLoanApp.getLoanStartDate().compareTo(pvpLoanApp.getLoanEndDate()) >= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02608);
                        return riskResultDto;
                    }

                    // 获取授信到期日
                    Date appEndDate = null;
                    if (StringUtils.nonEmpty(lmtCrdReplyInfo.getReplyEndDate())) {
                        appEndDate = DateUtils.parseDate(lmtCrdReplyInfo.getReplyEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(appEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02610);
                        return riskResultDto;
                    }
                    Date loanEndDate = DateUtils.parseDate(pvpLoanApp.getLoanEndDate(), "yyyy-MM-dd");
                    // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                    if (loanEndDate.compareTo(DateUtils.addMonth(appEndDate, lmtGraper)) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02611);
                        return riskResultDto;
                    }
                    // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                    if (Objects.equals("01", isRevolvLimit)) {
                        loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpLoanApp.getLoanTerm()) ? pvpLoanApp.getLoanTerm() : "0");
                        lmtTerm = Objects.isNull(lmtCrdReplyInfo.getAppTerm()) ? 0 : lmtCrdReplyInfo.getAppTerm();
                        if (loanTerm > lmtTerm) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                            return riskResultDto;
                        }
                    }
                }

                // 特定产品控制补充规则如下
                //1、白领易贷通：放款到期日<=授信到期日
                if (Objects.equals("022028", pvpLoanApp.getPrdId())) {
                    // 获取授信到期日
                    Date appEndDate = null;
                    // 放款到期日
                    Date loanEndDate = null;
                    if (StringUtils.nonEmpty(lmtCrdReplyInfo.getReplyEndDate())) {
                        appEndDate = DateUtils.parseDate(lmtCrdReplyInfo.getReplyEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(appEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02615);
                        return riskResultDto;
                    }
                    if (StringUtils.nonEmpty(pvpLoanApp.getEndDate())) {
                        loanEndDate = DateUtils.parseDate(pvpLoanApp.getEndDate(), "yyyy-MM-dd");
                    }
                    if (Objects.isNull(loanEndDate)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02616);
                        return riskResultDto;
                    }
                    if (DateUtils.compare(loanEndDate, appEndDate) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                        return riskResultDto;
                    }
                }
            }

        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)) {
            // 委托贷款出账申请
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(pvpEntrustLoanApp.getContNo());
            if (Objects.isNull(ctrEntrustLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                return riskResultDto;
            }
            LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
            try {
                lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(pvpEntrustLoanApp.getLmtAccNo());
            } catch (Exception e) {
                e.printStackTrace();
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02612);
                return riskResultDto;
            }
            if (Objects.isNull(lmtReplyAccSubPrd)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02601);
                return riskResultDto;
            }
            // 根据分项信息，查询起始日到期日期限
            CmisLmt0036RespDto cmisLmt0036RespDto = getCmisLmt0036ReqDto(pvpEntrustLoanApp.getLmtAccNo());
            if (Objects.isNull(cmisLmt0036RespDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02622);
                return riskResultDto;
            }
            // 额度是否循环非循环
            isRevolvLimit = lmtReplyAccSubPrd.getIsRevolvLimit();
            if (!Objects.equals("0", isRevolvLimit) && !Objects.equals("1", isRevolvLimit)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02602);
                return riskResultDto;
            }
            // 日期不能为空判定
            if (StringUtils.isEmpty(ctrEntrustLoanCont.getStartDate())
                    || StringUtils.isEmpty(ctrEntrustLoanCont.getEndDate())
                    || StringUtils.isEmpty(pvpEntrustLoanApp.getLoanStartDate())
                    || StringUtils.isEmpty(pvpEntrustLoanApp.getLoanEndDate())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02603);
                return riskResultDto;
            }
            // 一般合同
            if (CmisBizConstants.STD_CONT_TYPE_1.equals(ctrEntrustLoanCont.getContType())) {
                // 一般合同且授信额度为循环或者非循环时，合同起始日<=放款起始日 并且 放款到期日<=合同到期日
                // 一般合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                // 一般合同且授信额度为非循环时，放款期限<=授信期限
                if (ctrEntrustLoanCont.getStartDate().compareTo(pvpEntrustLoanApp.getLoanStartDate()) > 0
                        || ctrEntrustLoanCont.getEndDate().compareTo(pvpEntrustLoanApp.getLoanEndDate()) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02604);
                    return riskResultDto;
                }
                // 放款起始日<放款到期日
                if (pvpEntrustLoanApp.getLoanStartDate().compareTo(pvpEntrustLoanApp.getLoanEndDate()) >= 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02605);
                    return riskResultDto;
                }
                // 放款期限<=授信期限
                if (Objects.equals("0", isRevolvLimit)) {
                    loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpEntrustLoanApp.getLoanTerm()) ? pvpEntrustLoanApp.getLoanTerm() : "0");
                    lmtTerm = Objects.isNull(cmisLmt0036RespDto.getTerm()) ? 0 : cmisLmt0036RespDto.getTerm();
                    if (loanTerm > lmtTerm) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                        return riskResultDto;
                    }
                }
            }
            // 最高额合同
            if (CmisBizConstants.STD_CONT_TYPE_2.equals(ctrEntrustLoanCont.getContType())) {
                // 最高额合同且授信额度为循环或者非循环时，合同起始日<=放款起始日
                // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                if (ctrEntrustLoanCont.getStartDate().compareTo(pvpEntrustLoanApp.getLoanStartDate()) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                    return riskResultDto;
                }
                // 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
                if (pvpEntrustLoanApp.getLoanStartDate().compareTo(pvpEntrustLoanApp.getLoanEndDate()) >= 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02608);
                    return riskResultDto;
                }
                // 获取授信到期日
                Date appEndDate = null;
                if (StringUtils.nonEmpty(cmisLmt0036RespDto.getEndDate())) {
                    appEndDate = DateUtils.parseDate(cmisLmt0036RespDto.getEndDate(), "yyyy-MM-dd");
                }
                if (Objects.isNull(appEndDate)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02610);
                    return riskResultDto;
                }
                Date loanEndDate = DateUtils.parseDate(pvpEntrustLoanApp.getLoanEndDate(), "yyyy-MM-dd");
                // 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
                if (loanEndDate.compareTo(DateUtils.addMonth(appEndDate, cmisLmt0036RespDto.getTerm())) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02611);
                    return riskResultDto;
                }
                // 最高额合同且授信额度为非循环时，放款期限<=授信期限
                if (Objects.equals("0", isRevolvLimit)) {
                    loanTerm = Integer.parseInt(StringUtils.nonEmpty(pvpEntrustLoanApp.getLoanTerm()) ? pvpEntrustLoanApp.getLoanTerm() : "0");
                    lmtTerm = Objects.isNull(cmisLmt0036RespDto.getTerm()) ? 0 : cmisLmt0036RespDto.getTerm();
                    if (loanTerm > lmtTerm) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02606);
                        return riskResultDto;
                    }
                }
            }
            // 特定产品控制补充规则如下
            //1、诚易融：放款到期日<=授信到期日
            //2、白领易贷通：放款到期日<=授信到期日
            if (Objects.equals("P016", lmtReplyAccSubPrd.getLmtBizTypeProp()) || Objects.equals("022028", pvpEntrustLoanApp.getPrdId())) {
                // 获取授信到期日
                Date appEndDate = null;
                // 放款到期日
                Date loanEndDate = null;
                if (StringUtils.nonEmpty(cmisLmt0036RespDto.getEndDate())) {
                    appEndDate = DateUtils.parseDate(cmisLmt0036RespDto.getEndDate(), "yyyy-MM-dd");
                }
                if (Objects.isNull(appEndDate)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02615);
                    return riskResultDto;
                }
                if (StringUtils.nonEmpty(pvpEntrustLoanApp.getEndDate())) {
                    loanEndDate = DateUtils.parseDate(pvpEntrustLoanApp.getEndDate(), "yyyy-MM-dd");
                }
                if (Objects.isNull(loanEndDate)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02616);
                    return riskResultDto;
                }
                if (DateUtils.compare(loanEndDate, appEndDate) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02607);
                    return riskResultDto;
                }
            }
            //3、结息贷、优税贷：单笔提款期限不超过1年
            //   单笔提款到期日，必须在合同到日期以内
            if (Objects.equals("P013", lmtReplyAccSubPrd.getLmtBizTypeProp()) || Objects.equals("P014", lmtReplyAccSubPrd.getLmtBizTypeProp())) {
                if (StringUtils.isBlank(pvpEntrustLoanApp.getLoanTerm())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02618);
                    return riskResultDto;
                }
                if (Integer.parseInt(pvpEntrustLoanApp.getLoanTerm()) > 12) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02619);
                    return riskResultDto;
                }
                if (Objects.isNull(pvpEntrustLoanApp.getEndDate()) || Objects.isNull(pvpEntrustLoanApp.getLoanEndDate())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02620);
                    return riskResultDto;
                }
                if (pvpEntrustLoanApp.getEndDate().compareTo(pvpEntrustLoanApp.getLoanEndDate()) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02620);
                    return riskResultDto;
                }
            }
        }
        log.info("放款环节日期校验结束*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 获取当前合同编号下的所有在途申请
     *
     * @param contNo
     * @return
     */
    public List<PvpLoanApp> selectByContNo(String contNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", contNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        queryModel.addCondition("approveStatus", CmisBizConstants.APPLY_STATE_APP);
        return selectAll(queryModel);
    }

    /**
     * 据分项信息，查询起始日到期日期限
     *
     * @param lmtAccNo
     * @return
     */
    public CmisLmt0036RespDto getCmisLmt0036ReqDto(String lmtAccNo) {
        CmisLmt0036ReqDto cmisLmt0036ReqDto = new CmisLmt0036ReqDto();
        cmisLmt0036ReqDto.setAccSubNo(lmtAccNo);
        ResultDto<CmisLmt0036RespDto> resultDto = cmisLmtClientService.cmislmt0036(cmisLmt0036ReqDto);
        if (Objects.isNull(resultDto)) {
            return null;
        }
        log.info("0025lmtAccNo:{} retVal===>{}", lmtAccNo, resultDto.toString());
        if (resultDto.getData().getErrorCode().equals(SuccessEnum.SUCCESS.key)) {
            return resultDto.getData();
        }
        return null;
    }

    /**
     * @param pvpSerno
     * @return
     * @author shenli
     * @date 2021-7-2 16:42:50
     * @version 1.0.0
     * @desc 零售-放款申请提交-额度占用
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void retaillimitOccupy(String pvpSerno) {

        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);

        try {
            //1. 向额度系统发送接口：校验额度
            CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
            cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0010ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0010ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号

            CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
            cmisLmt0010ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());
            cmisLmt0010ReqDealBizListDto.setIsFollowBiz("0");
            //cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo(pvpLoanApp.getBillNo());
            //cmisLmt0010ReqDealBizListDto.setOrigiRecoverType("06");
            //cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus("200");
            //cmisLmt0010ReqDealBizListDto.setOrigiBizAttr("2");
            cmisLmt0010ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0010ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0010ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0010ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0010ReqDealBizListDto.setDealBizAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
            cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);

            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账校验请求报文：" + cmisLmt0010ReqDto.toString());
            ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDto = cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto);
            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账校验返回报文：" + cmisLmt0010RespDto.toString());

            String code = cmisLmt0010RespDto.getData().getErrorCode();

            if ("0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-台账校验成功！", pvpSerno);
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统-台账校验失败！", pvpSerno);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-台账校验失败:" + cmisLmt0010RespDto.getData().getErrorMsg());
            }


            //2. 向额度系统发送接口：占用额度
            CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
            cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0013ReqDto.setSerno(pvpLoanApp.getPvpSerno());
            cmisLmt0013ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisLmt0013ReqDto.setInputDate(pvpLoanApp.getInputDate());
            cmisLmt0013ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisLmt0013ReqDto.setBizNo(pvpLoanApp.getContNo());

            List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();


            CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
            cmisLmt0013ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
            cmisLmt0013ReqDealBizListDto.setIsFollowBiz("0");//是否无缝衔接
            //cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo(pvpLoanApp.getBillNo());//原交易业务编号
            //cmisLmt0013ReqDealBizListDto.setDealBizStatus("200");//原交易业务状态
            //cmisLmt0013ReqDealBizListDto.setOrigiRecoverType("06");//原交易业务恢复类型
            //cmisLmt0013ReqDealBizListDto.setOrigiBizAttr("2");//原交易属性
            cmisLmt0013ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0013ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0013ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0013ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setStartDate(pvpLoanApp.getLoanStartDate());
            cmisLmt0013ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            cmisLmt0013ReqDealBizListDto.setDealBizStatus("100");
            cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
            cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);


            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账占用请求报文：" + cmisLmt0013ReqDto.toString());
            ResultDto<CmisLmt0013RespDto> cmisLmt0013RespDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账占用返回报文：" + cmisLmt0013RespDto.toString());
            code = cmisLmt0013RespDto.getData().getErrorCode();

            if ("0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-台账占用成功！", pvpSerno);
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统-台账占用异常！", pvpSerno);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-台账额度占用异常:" + cmisLmt0013RespDto.getData().getErrorMsg());
            }

        } catch (Exception e) {
            log.info("零售业务台账占用异常" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
    }

    /**
     * @param pvpSerno
     * @return
     * @author shenli
     * @date 2021-7-2 16:42:50
     * @version 1.0.0
     * @desc 零售-放款申请提交-台账恢复
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void retailRestore(String pvpSerno) {

        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);

        try {
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
            List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpLoanApp.getPvpSerno());
            cmisLmt0014ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisLmt0014ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisLmt0014ReqDto.setInputDate(pvpLoanApp.getInputDate());

            //update by lizx cmislmt0014 恢复接口，更改成list 此处修改
            dealBizList.setDealBizNo(pvpLoanApp.getBillNo());
            dealBizList.setRecoverType("06");
            dealBizList.setRecoverSpacAmtCny(pvpLoanApp.getCvtCnyAmt());
            dealBizList.setRecoverAmtCny(pvpLoanApp.getCvtCnyAmt());
            dealBizList.setDealBizBailPreRate(new BigDecimal("0"));
            dealBizList.setSecurityAmt(new BigDecimal("0"));
            cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
            cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账额度恢复请求报文：" + cmisLmt0014ReqDto.toString());
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账额度恢复返回报文：" + cmisLmt0014RespDto.toString());
            String code = cmisLmt0014RespDto.getData().getErrorCode();
            if ("0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统调台账额度恢复成功！", pvpSerno);
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统调台账额度恢复失败！", pvpSerno);
                throw new Exception("额度系统-台账额度恢复异常:" + cmisLmt0014RespDto.getData().getErrorMsg());
            }

        } catch (Exception e) {
            log.info("台账恢复异常" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
    }

    /**
     * @param pvpSerno
     * @return void
     * @author hubp
     * @date 2021/11/17 20:41
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void retailRestoreXw(String pvpSerno) {

        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);

        try {
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
            List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpLoanApp.getPvpSerno());
            cmisLmt0014ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisLmt0014ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisLmt0014ReqDto.setInputDate(pvpLoanApp.getInputDate());

            dealBizList.setDealBizNo(pvpLoanApp.getBillNo());
            dealBizList.setRecoverType("06");
            dealBizList.setRecoverSpacAmtCny(pvpLoanApp.getCvtCnyAmt());
            dealBizList.setRecoverAmtCny(pvpLoanApp.getCvtCnyAmt());
            dealBizList.setDealBizBailPreRate(new BigDecimal("0"));
            dealBizList.setSecurityAmt(new BigDecimal("0"));
            cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
            cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

            log.info("根据业务申请编号【{}】前往额度系统-台账额度恢复请求报文：【{}】",pvpSerno , JSON.toJSONString(cmisLmt0014ReqDto));
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("根据业务申请编号【{}】前往额度系统-台账额度恢复返回报文：【{}】",pvpSerno , JSON.toJSONString(cmisLmt0014RespDto));
            String code = cmisLmt0014RespDto.getData().getErrorCode();
            if ("0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统调台账额度恢复成功！", pvpSerno);
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统调台账额度恢复失败！", pvpSerno);
                throw new Exception("额度系统-台账额度恢复异常:" + cmisLmt0014RespDto.getData().getErrorMsg());
            }

        } catch (Exception e) {
            log.info("台账恢复异常" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
    }
    /**
     * 查询担保合同关联合同的在途出账记录数
     *
     * @param guarContNo
     * @return
     */
    public int countOnTheWayPvpLoanAppRecords(String guarContNo) {
        return pvpLoanAppMapper.countOnTheWayPvpLoanAppRecords(guarContNo);
    }

    /**
     * @方法名称: riskItem0067
     * @方法描述: 抵质押物入库校验
     * @参数与返回说明:
     * @算法描述: 零售放款申请时，判断是否先放款后抵押，若否，则判断合同项下的押品是否入库，若未入库，则拦截
     * @创建人: yfs
     * @创建时间: 2021-7-21 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0067(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("***************抵质押物入库校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        if ("LS004".equals(bizType) || "LS008".equals(bizType)) {
            // 零售合同申请进入，使用合同编号查
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            if (Objects.isNull(ctrLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            if (CmisCommonConstants.GUAR_MODE_10.equals(ctrLoanCont.getGuarWay()) || CmisCommonConstants.GUAR_MODE_20.equals(ctrLoanCont.getGuarWay())) {
                // 零售放款申请时，判断是否先放款后抵押，若否，则判断合同项下的押品是否入库，若未入库，则拦截
                if (Objects.isNull(ctrLoanCont.getBeforehandInd()) || Objects.equals("0", ctrLoanCont.getBeforehandInd())) {
                    //查询借款合同下的押品是否入库
                    String result = bizCommonService.checkWarrantIsInstoreByContNo(serno,bizType);

                    if (!"success".equals(result)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(result);
                        return riskResultDto;
                    }
                }
            }
        } else if ("LS005".equals(bizType) || "LS006".equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            PvpLoanApp pvpLoanApp = selectByPrimaryKey(serno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            if (CmisCommonConstants.GUAR_MODE_10.equals(pvpLoanApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_20.equals(pvpLoanApp.getGuarMode())) {
                // 零售放款申请时，判断是否先放款后抵押，若否，则判断合同项下的押品是否入库，若未入库，则拦截
                if (Objects.isNull(pvpLoanApp.getBeforehandInd()) || Objects.equals("0", pvpLoanApp.getBeforehandInd())) {
                    //查询借款合同下的押品是否入库
                    String result = bizCommonService.checkWarrantIsInstoreByContNo(pvpLoanApp.getContNo(),bizType);
                    log.info("************************抵质押物入库校验开始*******************成功标识：【{}】", result);
                    if (!"success".equals(result)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(result);
                        return riskResultDto;
                    }
                }
            }
        }
        log.info("************************抵质押物入库校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0059
     * @方法描述: 抵质押物入库校验 （适用小微、对公、委托贷款）
     * @参数与返回说明:
     * @算法描述: 判断合同项下的押品是否入库，若未入库，则拦截
     * @创建人: zrcbank-fengjj
     * @创建时间: 2021年8月7日
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0059(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("抵质押物入库校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = "";
        String guarMode = "";
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        // //小微、对公贷款业务
        if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX011, bizType) || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_XW002, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_SGD02, bizType) || Objects.equals(CmisFlowConstants.FLOW_TYPE_DHD02, bizType)) {
            PvpLoanApp pvpLoanApp = selectByPrimaryKey(serno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                return riskResultDto;
            }
            contNo = pvpLoanApp.getContNo();
            guarMode = pvpLoanApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX013, bizType)) {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);//委托贷款业务
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                return riskResultDto;
            }
            contNo = pvpEntrustLoanApp.getContNo();
            guarMode = pvpEntrustLoanApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX001, bizType)) {
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);//最高额授信协议
            if (Objects.isNull(iqpHighAmtAgrApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpHighAmtAgrApp.getContNo();
            guarMode = iqpHighAmtAgrApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX003, bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serno);//普通贷款、贸易融资
            if (Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpLoanApp.getContNo();
            guarMode = iqpLoanApp.getGuarWay();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005, bizType)) {
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);//开证
            if (Objects.isNull(iqpTfLocApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpTfLocApp.getContNo();
            guarMode = iqpTfLocApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006, bizType)) {
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByIqpSerno(serno);//银承
            if (Objects.isNull(iqpAccpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpAccpApp.getContNo();
            guarMode = iqpAccpApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007, bizType)) {
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectByCvrgSernoKey(serno);//保函
            if (Objects.isNull(iqpCvrgApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpCvrgApp.getContNo();
            guarMode = iqpCvrgApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX008, bizType)) {
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(serno);//委托贷款
            if (Objects.isNull(iqpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = iqpEntrustLoanApp.getContNo();
            guarMode = iqpEntrustLoanApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX012, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_SGD03, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_DHD03, bizType)) {
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);//银承出账
            if (Objects.isNull(pvpAccpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = pvpAccpApp.getContNo();
            guarMode = pvpAccpApp.getGuarMode();
        } else if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX013, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_SGD04, bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_DHD04, bizType)) {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);//委托贷款出账
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101);
                return riskResultDto;
            }
            contNo = pvpEntrustLoanApp.getContNo();
            guarMode = pvpEntrustLoanApp.getGuarMode();
        }
        if (StringUtils.nonBlank(contNo) && (Objects.equals(CmisCommonConstants.GUAR_MODE_10, guarMode)
                || Objects.equals(CmisCommonConstants.GUAR_MODE_20, guarMode)) || Objects.equals(CmisCommonConstants.GUAR_MODE_21, guarMode)) {
            //查询借款合同下的押品是否入库
            String result = bizCommonService.checkWarrantIsInstoreByContNo(contNo,bizType);

            if (!"success".equals(result)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(result);
                return riskResultDto;
            }
        }

        log.info("抵质押物入库校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }


    /**
     * @创建人 WH
     * @创建时间 2021/8/2 8:36
     * @注释 额度支用 CmisLmt0010 校验
     */
    public CmisLmt0010RespDto cmisLmt0010ForXw(PvpLoanApp pvpLoanApp) {

        //1. 向额度系统发送接口：占用额度
        // 判断是否无缝衔接
        String isFollowBiz = "0";
        // 原交易业务编号
        String origiDealBizNo = "";
        // 原交易业务状态
        String origiDealBizStatus = "";
        // 原交易恢复类型
        String origiRecoverType = "";
        // 原交易属性
        String origiBizAttr = "";
        if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(pvpLoanApp.getLoanModal())
                || CmisCommonConstants.STD_LOAN_MODAL_6.equals(pvpLoanApp.getLoanModal())
                || CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())) {
            isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_1;
            // 根据所选合同判断是否为房抵循环贷的存量合同放款申请，若是则插入借新还旧试算表
            log.info("根据批复编号【{}】获取批复信息开始！", pvpLoanApp.getReplyNo());
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(pvpLoanApp.getReplyNo());
            log.info("根据批复编号【{}】获取批复信息结束！查询结果【{}】", Objects.nonNull(lmtCrdReplyInfo));
            if (Objects.isNull(lmtCrdReplyInfo)) {
                throw BizException.error(null, "9999", "未查询到批复信息！");
            }

            if ("1".equals(lmtCrdReplyInfo.getIsWxbxd()) && "SC010010".equals(lmtCrdReplyInfo.getPrdId())) {// 房抵循环贷特殊处理
                // 获取原借据信息
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtCrdReplyInfo.getSurveySerno());
                if (Objects.isNull(lmtSurveyReportBasicInfo)) {
                    throw BizException.error(null, "9999", "未查询到调查基本信息！");
                }
                origiDealBizNo = lmtSurveyReportBasicInfo.getOldBillNo();

                if (StringUtils.isBlank(origiDealBizNo)) {
                    throw BizException.error(null, "9999", "获取原借据信息失败！");
                }
            } else {
                PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = pvpLoanAppRepayBillRellService.selectByPvpSerno(pvpLoanApp.getPvpSerno());
                if (Objects.isNull(pvpLoanAppRepayBillRell)) {
                    log.error("根据出账申请流水号：" + pvpLoanApp.getPvpSerno() + "未找到引入的借据");
                    throw BizException.error(null, EcbEnum.ECB020044.key, EcbEnum.ECB020044.value);
                }
                origiDealBizNo = pvpLoanAppRepayBillRell.getBillNo();
            }
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_2;
        } else {
            isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
        cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0010ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0010ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号
        CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
        cmisLmt0010ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());
        cmisLmt0010ReqDealBizListDto.setIsFollowBiz(isFollowBiz);
        cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo(origiDealBizNo);
        cmisLmt0010ReqDealBizListDto.setOrigiRecoverType(origiRecoverType);
        cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus(origiDealBizStatus);
        cmisLmt0010ReqDealBizListDto.setOrigiBizAttr(origiBizAttr);
        cmisLmt0010ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
        cmisLmt0010ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
        cmisLmt0010ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
        cmisLmt0010ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
        cmisLmt0010ReqDealBizListDto.setDealBizAmt(pvpLoanApp.getPvpAmt());
        cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(pvpLoanApp.getPvpAmt());
        cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
        cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
        cmisLmt0010ReqDealBizListDto.setEndDate(pvpLoanApp.getEndDate());
        List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
        cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
        cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);

        log.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验请求报文：" + cmisLmt0010ReqDto.toString());
        CmisLmt0010RespDto cmisLmt0010RespDto = cmisLmt0010Service.cmisLmt0010(cmisLmt0010ReqDto);
        log.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验返回报文：" + cmisLmt0010RespDto.toString());


        return cmisLmt0010RespDto;
    }


    /**
     * @创建人 WH
     * @创建时间 2021/8/2 8:36
     * @注释 额度支用 CmisLmt0013 支用
     */
    public ResultDto cmisLmt0013ForXw(String pvpSerno) {
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);

        CmisLmt0010RespDto cmisLmt0010RespDtoResultDto = this.cmisLmt0010ForXw(pvpLoanApp);

        //2. 向额度系统发送接口：占用额度
        // 判断是否无缝衔接
        String isFollowBiz = "0";
        // 原交易业务编号
        String origiDealBizNo = "";
        // 原交易业务状态
        String origiDealBizStatus = "";
        // 原交易恢复类型
        String origiRecoverType = "";
        // 原交易属性
        String origiBizAttr = "";
        if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(pvpLoanApp.getLoanModal())
                || CmisCommonConstants.STD_LOAN_MODAL_6.equals(pvpLoanApp.getLoanModal())
                || CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())) {
            isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_1;
        } else {
            isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
        cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0013ReqDto.setSerno(pvpLoanApp.getPvpSerno());
        cmisLmt0013ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
        cmisLmt0013ReqDto.setInputDate(pvpLoanApp.getInputDate());
        cmisLmt0013ReqDto.setInputId(pvpLoanApp.getInputId());
        cmisLmt0013ReqDto.setBizNo(pvpLoanApp.getContNo());
        List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();
        CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
        cmisLmt0013ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
        cmisLmt0013ReqDealBizListDto.setIsFollowBiz(isFollowBiz);//是否无缝衔接
        cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
        cmisLmt0013ReqDealBizListDto.setDealBizStatus(origiDealBizStatus);//原交易业务状态
        cmisLmt0013ReqDealBizListDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
        cmisLmt0013ReqDealBizListDto.setOrigiBizAttr(origiBizAttr);//原交易属性
        cmisLmt0013ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
        cmisLmt0013ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
        cmisLmt0013ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
        cmisLmt0013ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
        cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(pvpLoanApp.getPvpAmt());
        cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(pvpLoanApp.getPvpAmt());
        cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
        cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
        cmisLmt0013ReqDealBizListDto.setStartDate(pvpLoanApp.getLoanStartDate());
        cmisLmt0013ReqDealBizListDto.setEndDate(pvpLoanApp.getEndDate());
        cmisLmt0013ReqDealBizListDto.setDealBizStatus("100");// 未生效
        cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
        cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);


        log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账占用请求报文：" + cmisLmt0013ReqDto.toString());
        CmisLmt0013RespDto cmisLmt0013RespDto = cmisLmt0013Service.cmisLmt0013(cmisLmt0013ReqDto);
        log.info("根据业务申请编号【" + pvpSerno + "】前往额度系统-台账占用返回报文：" + cmisLmt0013RespDto.toString());

        return new ResultDto();
    }


    public int updateAuthStatusByPvpSerno(PvpLoanApp pvpLoanApp) {
        return pvpLoanAppMapper.updateAuthStatusByPvpSerno(pvpLoanApp);
    }

    /**
     * @方法名称: pvpLoanAppRevocationRetail
     * @方法描述: 零售出账申请撤消
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int pvpLoanAppRevocationRetail(String pvpSerno) {
        log.info("零售出账申请撤销开始：" + pvpSerno);
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
            String approveStatus = pvpLoanApp.getApproveStatus();

            if (!"997".equals(approveStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销审批通过的放款申请");
            }

            PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectPvpAuthorizeBySerno(pvpSerno);
            String authStatus = pvpAuthorize.getAuthStatus();

            //{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
            //{"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},{"key":"5","value":"已记帐已撤销"},
            // {"key":"6","value":"抹账被核心拒绝"},{"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},
            // {"key":"9","value":"已发送核心冲正成功"},{"key":"A","value":"冲正申请"}]

            if ("3".equals(authStatus)) {//已发送核心校验失败 去核心判断账户是否正常
                try {
                    pvpAuthorizeService.sendPvpCheckRetail(pvpAuthorize.getPvpSerno());
                } catch (Exception e) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "出账成功，不可撤销！");
                }
            }

            if (!"3".equals(authStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销核心出账失败的放款申请");
            }

            //台账额度恢复
            retailRestore(pvpSerno);

            pvpLoanApp.setApproveStatus("996");//自行退出
            pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            pvpAuthorize.setAuthStatus("4");//出账已撤销
            pvpAuthorizeService.updateSelective(pvpAuthorize);
            AccLoan accloan = accLoanService.selectByAccLoanPvpSerno(pvpLoanApp.getPvpSerno());
            accloan.setOprType("02");//台账逻辑删除
            accLoanService.updateSelective(accloan);
        } catch (Exception e) {
            log.info("零售出账申请撤销异常" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("零售出账申请撤销结束：" + pvpSerno);
        return 0;
    }

    /**
     * @return
     * @return
     * @方法名称: isPrdExist
     * @方法描述: 最高额授信协议做出账时，判断选择的产品是否存在于合同所选的分项下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<Boolean> isPrdExist(Map map) {
        Boolean result = true;
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo((String) map.get("contNo"));
        if (Objects.nonNull(ctrHighAmtAgrCont)) {
            CmisLmt0046ReqDto cmisLmt0046ReqDto = new CmisLmt0046ReqDto();
            cmisLmt0046ReqDto.setAccSubNo(ctrHighAmtAgrCont.getLmtAccNo());
            cmisLmt0046ReqDto.setPrdId((String) map.get("prdId"));
            ResultDto<CmisLmt0046RespDto> resultDto = cmisLmtClientService.cmislmt0046(cmisLmt0046ReqDto);
            String code = resultDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("查询分项" + ctrHighAmtAgrCont.getLmtAccNo() + "异常！" + "异常信息：" + resultDto.getData().getErrorMsg());
                throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(resultDto.getData().getHasBussFlag())) {
                result = true;
            } else {
                result = false;
            }
        }
        return new ResultDto<Boolean>(result);
    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 20:29
     **/
    public void createImageSpplInfo(String pvpSerno, String bizType, String instanceId, String spplBizType, String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(pvpSerno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            String cusName = "";// 客户名称
            String managerBrId = "";// 责任机构
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            if (Objects.equals(CmisCommonConstants.STD_SPPL_BIZ_TYPE_03, spplBizType)) {
                // 零售放款
                PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
                docImageSpplClientDto.setBizSerno(pvpLoanApp.getIqpSerno());// 关联业务流水号
                docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
                docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_01);// 影像补扫类型 01:放款影像补扫
                docImageSpplClientDto.setSpplBizType(spplBizType);
                docImageSpplClientDto.setCusId(pvpLoanApp.getCusId());// 客户号
                docImageSpplClientDto.setCusName(pvpLoanApp.getCusName());// 客户名称
                docImageSpplClientDto.setContNo(pvpLoanApp.getContNo());// 合同编号
                docImageSpplClientDto.setBillNo(pvpLoanApp.getBillNo());// 借据编号
                docImageSpplClientDto.setInputId(pvpLoanApp.getManagerId());// 登记人
                docImageSpplClientDto.setInputBrId(pvpLoanApp.getManagerBrId());// 登记机构
                docImageSpplClientDto.setPrdId(pvpLoanApp.getPrdId());
                docImageSpplClientDto.setPrdName(pvpLoanApp.getPrdName());
                docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
                cusName = pvpLoanApp.getCusName();
                managerBrId = pvpLoanApp.getManagerBrId();
            } else if (Objects.equals(CmisCommonConstants.STD_SPPL_BIZ_TYPE_08, spplBizType)) {
                // 委托贷款出账
                PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
                docImageSpplClientDto.setBizSerno(pvpEntrustLoanApp.getPvpSerno());// 关联业务流水号
                docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
                docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_01);// 影像补扫类型 01:放款影像补扫
                docImageSpplClientDto.setSpplBizType(spplBizType);
                docImageSpplClientDto.setCusId(pvpEntrustLoanApp.getCusId());// 客户号
                docImageSpplClientDto.setCusName(pvpEntrustLoanApp.getCusName());// 客户名称
                docImageSpplClientDto.setContNo(pvpEntrustLoanApp.getContNo());// 合同编号
                docImageSpplClientDto.setBillNo(pvpEntrustLoanApp.getBillNo());// 借据编号
                docImageSpplClientDto.setInputId(pvpEntrustLoanApp.getManagerId());// 登记人
                docImageSpplClientDto.setInputBrId(pvpEntrustLoanApp.getManagerBrId());// 登记机构
                docImageSpplClientDto.setPrdId(pvpEntrustLoanApp.getPrdId());
                docImageSpplClientDto.setPrdName(pvpEntrustLoanApp.getPrdName());
                docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
                cusName = pvpEntrustLoanApp.getCusName();
                managerBrId = pvpEntrustLoanApp.getManagerBrId();
            } else {
                // 一般出账或小微放款
                PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
                docImageSpplClientDto.setBizSerno(pvpLoanApp.getPvpSerno());// 关联业务流水号
                docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
                docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_01);// 影像补扫类型 01:放款影像补扫
                docImageSpplClientDto.setSpplBizType(spplBizType);
                docImageSpplClientDto.setCusId(pvpLoanApp.getCusId());// 客户号
                docImageSpplClientDto.setCusName(pvpLoanApp.getCusName());// 客户名称
                docImageSpplClientDto.setContNo(pvpLoanApp.getContNo());// 合同编号
                docImageSpplClientDto.setBillNo(pvpLoanApp.getBillNo());// 借据编号
                docImageSpplClientDto.setInputId(pvpLoanApp.getManagerId());// 登记人
                docImageSpplClientDto.setInputBrId(pvpLoanApp.getManagerBrId());// 登记机构
                docImageSpplClientDto.setPrdId(pvpLoanApp.getPrdId());
                docImageSpplClientDto.setPrdName(pvpLoanApp.getPrdName());
                docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
                cusName = pvpLoanApp.getCusName();
                managerBrId = pvpLoanApp.getManagerBrId();
            }


            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), managerBrId)) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", cusName);
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * @return
     * @return
     * @方法名称: isRepayCapitalReloan
     * @方法描述: 出账申请时，判断是否满足小企业无还本续贷业务要求
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<Boolean> isRepayCapitalReloan(Map map) throws Exception {
        Boolean result = true;
        String contNo = (String) map.get("contNo");
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        if (CmisCommonConstants.GUAR_MODE_10.equals(ctrLoanCont.getGuarWay())
                && CmisCommonConstants.STD_CONT_TYPE_2.equals(ctrLoanCont.getContType())
                && CmisCommonConstants.STD_LOAN_MODAL_8.equals(map.get("loanModal"))
                && !"P034".equals(ctrLoanCont.getPrdTypeProp())) {
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(ctrLoanCont.getLmtAccNo());
            if (Objects.isNull(lmtReplyAccSubPrd)) {
                throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
            }
            if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(lmtReplyAccSubPrd.getIsRevolvLimit())) {
                result = false;
            } else {
                CmisLmt0054ReqDto cmisLmt0054ReqDto = new CmisLmt0054ReqDto();
                cmisLmt0054ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0054ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
                cmisLmt0054ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//额度类型

                log.info("调用额度54接口查询客户额度开始" + "请求报文为：" + cmisLmt0054ReqDto.toString());
                ResultDto<CmisLmt0054RespDto> cmisLmt0054RespDto = cmisLmtClientService.cmislmt0054(cmisLmt0054ReqDto);
                log.info("调用额度54接口查询客户额度结束" + "返回报文为：" + cmisLmt0054RespDto.toString());
                if (!"0".equals(cmisLmt0054RespDto.getCode())) {
                    log.error("调用额度54接口查询: " + ctrLoanCont.getCusId() + " 客户额度异常");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(cmisLmt0054RespDto.getData().getErrorCode())) {
                    log.error("调用额度54接口查询: " + ctrLoanCont.getCusId() + " 客户额度异常，异常信息：" + cmisLmt0054RespDto.getData().getErrorMsg());
                    throw BizException.error(null, cmisLmt0054RespDto.getData().getErrorCode(), cmisLmt0054RespDto.getData().getErrorMsg());
                }
                // 获取客户名下授信总额
                BigDecimal avlAmt = cmisLmt0054RespDto.getData().getAvlAmt();
                if (avlAmt.compareTo(new BigDecimal(10000000)) > 0) {
                    result = false;
                } else {
                    result = true;
                }
            }
        } else {
            result = false;
        }
        return new ResultDto<Boolean>(result);
    }

    /**
     * @return
     * @return
     * @方法名称: isRepayCapitalReloan
     * @方法描述: 小企业无还本续贷后续的业务处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public Map pvpRepayCapitalReloan(Map map) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String pvpSerno = "";
        //放款金额
        BigDecimal pvpAmt = new BigDecimal((String) map.get("pvpAmt"));
        //币种
        String curType = (String) map.get("curType");
        try {
            //汇率
            Map cutTypeMap = new HashMap();
            cutTypeMap.put("curType", map.get("curType"));
            Map rateMap = iqpLoanAppService.getExchangeRate(cutTypeMap);
            BigDecimal exchangeRate = new BigDecimal((String) rateMap.get("rate"));
            //折算人民币金额
            BigDecimal cvtCnyAmt = pvpAmt.multiply(exchangeRate);
            // 生成贷款申请数据
            String contNo = (String) map.get("contNo");
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            PvpLoanApp pvpLoanApp = new PvpLoanApp();
            BeanUtils.copyProperties(ctrLoanCont, pvpLoanApp);
            pvpLoanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            //审批状态
            pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            //贷款形式(默认为小企业无还本续贷)
            pvpLoanApp.setLoanModal(CmisCommonConstants.STD_LOAN_MODAL_8);
            //操作类型
            pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //合同起始日
            pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
            //合同到期日
            pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
            //利率调整方式
            pvpLoanApp.setRateAdjMode(ctrLoanCont.getIrAdjustType());
            //利率调整选项
            pvpLoanApp.setRateAdjType(ctrLoanCont.getIrFloatType());
            //LPR利率区间
            pvpLoanApp.setLprRateIntval(ctrLoanCont.getLprRateIntval());
            //浮动点数
            pvpLoanApp.setRateFloatPoint(ctrLoanCont.getRateFloatPoint());
            //扣款扣息方式->扣款方式
            pvpLoanApp.setDeductType(ctrLoanCont.getDeductDeduType());
            //还款日->扣款日
            pvpLoanApp.setDeductDay(ctrLoanCont.getRepayDate());
            //逾期浮动
            pvpLoanApp.setOverdueRatePefloat(ctrLoanCont.getOverdueRatePefloat());
            //逾期浮动(年)
            pvpLoanApp.setOverdueExecRate(ctrLoanCont.getOverdueExecRate());
            //担保方式
            pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());
            //贷款期限类型
            pvpLoanApp.setLoanTermUnit(ctrLoanCont.getTermType());
            //是否立即发起受托支付
            pvpLoanApp.setIsCfirmPay(ctrLoanCont.getIsCfirmPayWay());
            //工业转型升级标识
            pvpLoanApp.setIndtUpFlag(ctrLoanCont.getComUpIndtify());
            //文化产业标识
            pvpLoanApp.setCulIndustryFlag(ctrLoanCont.getIsCulEstate());
            //合同金额
            pvpLoanApp.setContAmt(ctrLoanCont.getContAmt());
            //合同币种
            pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
            //放款金额
            pvpLoanApp.setPvpAmt(pvpAmt);
            //币种
            pvpLoanApp.setCurType(curType);
            //汇率
            pvpLoanApp.setExchangeRate(exchangeRate);
            //折算人民币金额
            pvpLoanApp.setCvtCnyAmt(cvtCnyAmt);
            //合同最高可放金额，取合同余额
            pvpLoanApp.setContHighDisb(ctrLoanCont.getHighAvlAmt());
            //业务条线
            pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
            //节假日是否顺延
            pvpLoanApp.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);
            //渠道来源 PC端
            pvpLoanApp.setChnlSour("02");

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return result;
            } else {
                pvpLoanApp.setInputId(userInfo.getLoginCode());
                pvpLoanApp.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanApp.setManagerId(userInfo.getLoginCode());
                pvpLoanApp.setManagerBrId(userInfo.getOrg().getCode());
            }
            pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());//放款流水号
            Map seqMap = new HashMap();
            seqMap.put("contNo", contNo);
            String billNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO, seqMap);//借据编号
            pvpLoanApp.setBillNo(billNo);//借据编号
            pvpLoanApp.setPvpSerno(pvpSerno);//主键为放款流水号
            pvpLoanApp.setIqpSerno(ctrLoanCont.getIqpSerno());//业务流水号非空
            int insertCount = this.insertSelective(pvpLoanApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                log.info("小企业无还本续贷出账申请出账申请" + pvpSerno + "-新增异常！");
                rtnCode = EcbEnum.ECB019999.key;
                rtnMsg = EcbEnum.ECB019999.value;
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            PvpLoanApp pvpLoanAppData = this.selectByPvpLoanSernoKey(pvpSerno);
            pvpLoanAppData.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
            pvpLoanAppData.setIsAutoAppr(CmisCommonConstants.STD_ZB_YES_NO_1);
            int updateCount = this.updateSelective(pvpLoanAppData);
            if (updateCount <= 0) {
                log.info("小企业无还本续贷出账申请出账申请" + pvpSerno + "-更新审批状态异常！");
                rtnCode = EcbEnum.ECB019999.key;
                rtnMsg = EcbEnum.ECB019999.value;
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }

            //续贷主体类型
            String xdType = "";
            // 企业客户
            String entCustId = "";//企业核心客户号
            String entName = "";//企业名称
            String orgCode = "";//企业统一社会信用代码

            // 企业法人
            String entCustName = "";//企业法人
            String entCustNo = "";//企业法人身份证
            String entCustNameId = "";//企业法人客户号

            // 个人客户
            String custId = "";//个人核心客户号
            String custNo = "";//个人身份证号码
            String custName = "";//个人客户名称
            String grantNo = "";//个人征信授权书编号

            // 管护信息
            String managerId = "";//管护客户经理
            String managerOrgId = "";//管护客户经理机构

            String substring = ((String) map.get("cusId")).substring(0, 1);
            // 对公客户
            if ("8".equals(substring)) {
                xdType = "2";
                log.info("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 调用客户服务接口开始，请求报文：" + pvpLoanApp.getCusId());
                ResultDto<CusCorpDto> cusCorpDtoResultDto = icusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId());
                log.info("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 调用客户服务接口结束，响应报文：" + cusCorpDtoResultDto.toString());
                if (!"0".equals(cusCorpDtoResultDto.getCode())) {
                    log.info("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 调用客户服务接口异常");
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                entCustId = (String) map.get("cusId");//企业核心客户号
                entName = (String) map.get("cusName");//企业名称
                // 获取企业统一社会信用代码
                Map mapData = new HashMap();
                mapData.put("cusId", map.get("cusId"));
                mapData.put("certType", CmisCusConstants.STD_ECIF_CERT_TYP_R);
                List<CusCorpCertDto> cusCorpCertDtoList = icusClientService.queryCusCorpCertDataByParams(mapData);
                if (CollectionUtils.isEmpty(cusCorpCertDtoList)) {
                    orgCode = cusCorpDtoResultDto.getData().getLoanCardId();//中证码
                } else {
                    orgCode = cusCorpCertDtoList.get(0).getCertCode();//企业统一社会信用代码
                }
                // 通过客户号和高管类别得到公司法人代表信息
                Map cusQueryMap = new HashMap();
                cusQueryMap.put("cusIdRel", map.get("cusId"));
                cusQueryMap.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
                ResultDto<CusCorpMgrDto> cusCorpMgrDtoResultDto = this.getCusInfo(cusQueryMap);
                if (Objects.isNull(cusCorpMgrDtoResultDto.getData())) {
                    log.info("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 调用客户服务接口异常");
                    rtnCode = EcbEnum.ECB010017.key;
                    rtnMsg = EcbEnum.ECB010017.value;
                    throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
                }
                entCustName = cusCorpMgrDtoResultDto.getData().getMrgName();//企业法人
                entCustNameId = cusCorpMgrDtoResultDto.getData().getCusId();//企业法人客户号
                entCustNo = cusCorpMgrDtoResultDto.getData().getMrgCertCode();//企业法人身份证

                // 通过客户号和高管类别得到公司实际控制人信息
                Map cusQueryMapData = new HashMap();
                cusQueryMapData.put("cusIdRel", map.get("cusId"));
                cusQueryMapData.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
                ResultDto<CusCorpMgrDto> cusCorpMgrDtoResultDtoData = this.getCusInfo(cusQueryMapData);
                custId = cusCorpMgrDtoResultDto.getData().getCusId();//个人核心客户号
                custName = cusCorpMgrDtoResultDto.getData().getMrgName();//个人客户名称
                custNo = cusCorpMgrDtoResultDto.getData().getMrgCertCode();//个人身份证号码
            } else {
                xdType = "1";
                log.info("通过个人客户：" + map.get("cusId") + " 调客户接口查询客户信息开始," + "请求报文：" + map.get("cusId"));
                CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(pvpLoanApp.getCusId());
                log.info("通过个人客户：" + map.get("cusId") + " 调客户接口查询客户信息结束," + "响应报文：" + cusBaseDtoResultDto.toString());
                custId = (String) map.get("cusId");
                custNo = cusBaseDtoResultDto.getCertCode();
                custName = cusBaseDtoResultDto.getCusName();
            }
            // 根据客户编号查询其管护客户经理信息
            log.info("通过客户编号：" + pvpLoanApp.getCusId() + " 调客户接口查询客户基本信息开始," + "请求报文：" + map.get("cusId"));
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(pvpLoanApp.getCusId());
            log.info("通过客户编号：" + pvpLoanApp.getCusId() + " 调客户接口查询客户基本信息开始," + "响应报文：" + cusBaseDtoResultDto.toString());
            managerId = cusBaseDtoResultDto.getManagerId();
            managerOrgId = cusBaseDtoResultDto.getManagerBrId();

            // 翻译管护经理名称和管护经理机构名称
            ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
            if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                log.error("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 查询用户数据异常");
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            String managerName = adminSmUserDtoResultDto.getData().getUserName();
            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerOrgId);
            if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                log.error("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 查询用户数据异常");
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();

            // 组装风控接口报文
            Fb1147ReqDto fb1147ReqDto = new Fb1147ReqDto();
            fb1147ReqDto.setCHANNEL_TYPE("13");//渠道来源
            fb1147ReqDto.setAPP_TYPE("5");//申请类型
            fb1147ReqDto.setXD_TYPE(xdType);//续贷主体类型
            fb1147ReqDto.setCRD_CONT_NO(contNo);//授信合同编号
            fb1147ReqDto.setBILL_NO(billNo);//借据编号

            fb1147ReqDto.setCUST_NAME(custName);//个人客户名称
            fb1147ReqDto.setCUST_NO(custNo);//个人身份证号码
            fb1147ReqDto.setCUST_ID(custId);//个人核心客户号
            fb1147ReqDto.setGRANT_NO(grantNo);//个人征信授权书编号

            fb1147ReqDto.setENT_NAME(entName);//企业名称
            fb1147ReqDto.setENT_CUST_ID(entCustId);//企业核心客户号
            fb1147ReqDto.setORG_CODE(orgCode);//企业统一社会信用代码

            fb1147ReqDto.setENT_CUST_NAME(entCustName);//企业法人
            fb1147ReqDto.setENT_CUST_NO(entCustNo);//企业法人身份证
            fb1147ReqDto.setENT_CUST_NAME_ID(entCustNameId);//企业法人客户号

            fb1147ReqDto.setPRD_TYPE("");//产品类别
            fb1147ReqDto.setPRD_CODE(pvpLoanApp.getPrdId());//产品代码
            fb1147ReqDto.setMANAGER_ID(managerId);//管户经理ID
            fb1147ReqDto.setMANAGER_NAME(managerName);//管户经理名称
            fb1147ReqDto.setMANAGER_ORG_ID(managerOrgId);//管户经理所属机构ID
            fb1147ReqDto.setMANAGER_ORG_NAME(managerBrName);//管户经理所属机构名称
            fb1147ReqDto.setAPP_AMT(pvpLoanAppData.getCvtCnyAmt().toString());//续贷金额
            fb1147ReqDto.setLOAN_CARD_NO("");//放款卡号
            fb1147ReqDto.setLOAN_START_DATE("");//新借据起始日
            fb1147ReqDto.setLOAN_TERM("");//新借据贷款期限
            fb1147ReqDto.setLOAN_END_DATE("");//新借据到日期
            fb1147ReqDto.setOLS_TRAN_NO(pvpSerno);//系统交易流水
            fb1147ReqDto.setOLS_DATE(stringRedisTemplate.opsForValue().get("openDay"));//系统交易日期
            log.info("无还本续贷：" + pvpLoanApp.getPvpSerno() + " 发智能风控1147无还本续贷申请接口开始," + "请求报文：" + fb1147ReqDto.toString());
            ResultDto<Fb1147RespDto> fb1147RespDtoResultDto = dscms2CircpClientService.fb1147(fb1147ReqDto);
            log.info("无还本续贷：" + pvpLoanApp.getPvpSerno() + " 发智能风控1147无还本续贷申请接口结束," + "响应报文：" + fb1147RespDtoResultDto.toString());
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(fb1147RespDtoResultDto.getCode())) {
                log.error("小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 调智能风控1147接口异常");
                rtnCode = fb1147RespDtoResultDto.getCode();
                rtnMsg = fb1147RespDtoResultDto.getMessage();
                throw BizException.error(null, fb1147RespDtoResultDto.getCode(), fb1147RespDtoResultDto.getMessage());
            }
            result.put("pvpSerno", pvpSerno);
            result.put("replyNo", pvpLoanAppData.getReplyNo());
            log.info("小企业无还本续贷出账申请：" + pvpSerno + "-新增成功！");
        } catch (BizException e) {
            log.error("小企业无还本续贷出账申请新增异常" + e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            log.error("小企业无还本续贷出账申请新增异常" + e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    // 获取高管信息公共方法
    public ResultDto getCusInfo(Map map) {
        ResultDto<CusCorpMgrDto> cusCorpMgrDtoResultDto = new ResultDto<>();
        log.info("通过对公客户：" + map.get("cusId") + " 调客户接口查询高管信息开始," + "请求报文：" + map.toString());
        cusCorpMgrDtoResultDto = cmisCusClientService.getCusCorpMgrByParams(map);
        log.info("通过对公客户：" + map.get("cusId") + " 调客户接口查询高管信息结束," + "响应报文：" + cusCorpMgrDtoResultDto.toString());
        if (!"0".equals(cusCorpMgrDtoResultDto.getCode())) {
            throw BizException.error(null, cusCorpMgrDtoResultDto.getCode(), cusCorpMgrDtoResultDto.getMessage());
        }
        return cusCorpMgrDtoResultDto;
    }

    /**
     * @param pvpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/9/6 14:04
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto annulPvpLoanApp(String pvpSerno) {
        log.info("************小微出账申请撤销开始，放款流水号：【{}】**********", pvpSerno);
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
            String approveStatus = pvpLoanApp.getApproveStatus();

            if (!"997".equals(approveStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销审批通过的放款申请！");
            }

            PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectPvpAuthorizeBySerno(pvpSerno);
            AccLoan accloan = accLoanService.selectByAccLoanPvpSerno(pvpLoanApp.getPvpSerno());
            String authStatus = pvpAuthorize.getAuthStatus();

            //{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
            //{"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},{"key":"5","value":"已记帐已撤销"},
            // {"key":"6","value":"抹账被核心拒绝"},{"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},
            // {"key":"9","value":"已发送核心冲正成功"},{"key":"A","value":"冲正申请"}]

            if ("3".equals(authStatus)) {//已发送核心校验失败 去核心判断账户是否正常
                try {
                    pvpAuthorizeService.sendPvpCheckRetail(pvpAuthorize.getPvpSerno());
                } catch (Exception e) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "出账成功，不可撤销！");
                }
            }

            if (!"0".equals(authStatus) && !"3".equals(authStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销尚未出账的放款申请！");
            }

            //台账额度恢复
            retailRestore(pvpSerno);

            pvpLoanApp.setApproveStatus("996");//自行退出
            pvpLoanAppMapper.updateByPrimaryKeySelective(pvpLoanApp);
            pvpAuthorize.setAuthStatus("4");//出账已撤销
            pvpAuthorizeService.updateSelective(pvpAuthorize);
            accloan.setOprType("02");//台账逻辑删除
            accLoanService.updateSelective(accloan);
        } catch (Exception e) {
            log.info("************小微出账申请撤销异常，放款流水号：【{}】，异常信息：【{}】**********", pvpSerno, e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("************小微出账申请撤销结束，放款流水号：【{}】**********", pvpSerno);
        return new ResultDto();
    }

    /**
     * @param pvpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author qw
     * @date 2021/9/11 14:04
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Integer onCancel(String pvpSerno) {
        PvpLoanApp pvpLoanApp = this.selectByPvpLoanSernoKey(pvpSerno);
        //删除流程实例
        ResultDto<ResultMessageDto> resultMessageDtoResultDto = workflowCoreClient.deleteByBizId(pvpSerno);
        int result = 0;
        // 先修改为自行退出再恢复额度，出现异常可以回滚
        ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(pvpLoanApp.getPvpSerno());
        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
        int updateCount = this.updateSelective(pvpLoanApp);
        if (updateCount <= 0) {
            log.error("贷款出账申请: " + pvpSerno + " 作废异常");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        result = updateCount;

        String inputId = "";
        String inputBrId = "";
        String inputDate = "";
        // 获取当前的登录人信息
        User userInfo = SessionUtils.getUserInformation();
        if (Objects.nonNull(userInfo)) {
            inputId = userInfo.getLoginCode();
            inputBrId = userInfo.getOrg().getCode();
            inputDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        // 根据担保方式判断是否低风险
        String guarMode = pvpLoanApp.getGuarMode();
        // 台账总余额
        BigDecimal balanceAmtCny = pvpLoanApp.getCvtCnyAmt();
        // 恢复敞口余额
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
        // 恢复总额
        BigDecimal recoverAmtCny = BigDecimal.ZERO;

        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            recoverAmtCny = pvpLoanApp.getCvtCnyAmt();
            recoverSpacAmtCny = BigDecimal.ZERO;
        } else {
            recoverAmtCny = pvpLoanApp.getCvtCnyAmt();
            recoverSpacAmtCny = pvpLoanApp.getCvtCnyAmt();
        }

        // 组装报文进行台账恢复
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(pvpLoanApp.getPvpSerno());//交易流水号
        cmisLmt0014ReqDto.setInputId(inputId);//登记人
        cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
        cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

        List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
        cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
        cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
        cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
        cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
        cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
        cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
        cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
        dealBizList.add(cmisLmt0014ReqdealBizListDto);
        cmisLmt0014ReqDto.setDealBizList(dealBizList);
        log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpLoanApp.getBillNo(), cmisLmt0014ReqDto.toString());
        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpLoanApp.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
        if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
            log.info("台账恢复，前往额度系统进行额度恢复异常");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
            log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpLoanApp.getBillNo());
            throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
        }
        return result;
    }

    /**
     * @param billNo
     * @return PvpLoanApp
     * @author qw
     * @date 2021/9/17 14:04
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public PvpLoanApp queryPvpLoanAppDataByBillNo(String billNo) {
        return pvpLoanAppMapper.queryPvpLoanAppDataByBillNo(billNo);
    }

    /**
     * @param model
     * @return java.util.List<cn.com.yusys.yusp.dto.server.xdtz0045.resp.List>
     * @author 王玉坤
     * @date 2021/9/25 20:12
     * @version 1.0.0
     * @desc 查询台账及房屋信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public java.util.List<cn.com.yusys.yusp.dto.server.xdtz0045.resp.List> querySd0007LoanInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        java.util.List<cn.com.yusys.yusp.dto.server.xdtz0045.resp.List> list = pvpLoanAppMapper.querySd0007LoanInfo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shangzy
     * @date 2021/9/30 15:31
     * @version 1.0.0
     * @desc 获取xwd批复报告视图
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getOtherViewUrl(PvpLoanApp pvpLoanApp) {
        String otherviewurl = url + "/#/otherView?vw=afc&reqId=" + pvpLoanApp.getSurveySerno();
        log.info("xwd视图URL为[{}]", otherviewurl);
        return otherviewurl;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shangzy
     * @date 2021/10/15 15:31
     * @version 1.0.0
     * @desc 获取xwd调查报告视图
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getReportViewUrl(PvpLoanApp pvpLoanApp) {
        String otherviewurl = url + "/#/otherView?vw=divrv&isReportModel=1&reqId=" + pvpLoanApp.getSurveySerno();
        log.info("xwd视图URL为[{}]", otherviewurl);
        return otherviewurl;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author qw
     * @date 2021/9/30 15:31
     * @version 1.0.0
     * @desc 获取结算账户
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<AccountDto> getAccountDtoData(QueryModel queryModel) {
        List<AccountDto> list = new LinkedList<>();
        String accountno = (String) queryModel.getCondition().get("loanPayoutAccno");
        // 调核心ib1253接口查询结算账户信息
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        ib1253ReqDto.setKehuzhao(accountno);//客户账号
        ib1253ReqDto.setZhhaoxuh("");// 账户序号
        ib1253ReqDto.setMimammmm("");//密码
        ib1253ReqDto.setYanmbzhi(CmisCommonConstants.STD_ZB_YES_NO_0);//密码校验方式
        ib1253ReqDto.setKehzhao2("");//客户账号2
        ib1253ReqDto.setShifoubz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否标志
        ib1253ReqDto.setZhufldm1("");//账户分类代码1
        ib1253ReqDto.setZhufldm2("");//账户分类代码2
        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        if (!"0".equals(resultDto.getCode())) {
            log.error("ib1253接口调用异常");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        if (Objects.nonNull(resultDto.getData())) {
            Ib1253RespDto respDto = resultDto.getData();
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(respDto.getShfojshu())) {
                AccountDto accountDto = new AccountDto();
                accountDto.setBeiyzd01(respDto.getBeiyzd01());//备用字段01
                accountDto.setBeiyzd02(respDto.getBeiyzd02());//备用字段02
                accountDto.setBeiyzd03(respDto.getBeiyzd03());//备用字段03
                accountDto.setChanpshm(respDto.getChanpshm());//产品说明
                accountDto.setChaohubz(respDto.getChaohubz());//账户钞汇标志
                accountDto.setChapbhao(respDto.getChapbhao());//产品编号
                accountDto.setCunkzlei(changeCunkzlei(respDto.getCunkzlei()));//存款种类
                accountDto.setCunqiiii(respDto.getCunqiiii());//存期
                accountDto.setDoqiriqi(respDto.getDoqiriqi());//到期日期
                accountDto.setDspzsyzt(respDto.getDspzsyzt());//凭证状态
                accountDto.setHuobdaih(changeCurrency(respDto.getHuobdaih()));//货币代号
                accountDto.setKaihguiy(respDto.getKaihguiy());//账户开户柜员
                accountDto.setKaihjigo(respDto.getZhkhjigo());//账户开户机构
                accountDto.setKaihriqi(respDto.getKaihriqi());////开户日期
                accountDto.setKaihuqud(respDto.getKaihuqud());//开户渠道
                accountDto.setKehuhaoo(respDto.getKehuhaoo());//客户号
                accountDto.setKehuzhao(accountno);//客户账号
                accountDto.setZhhaoxuh(respDto.getZhaoxhao());//账户子序号
                accountDto.setKeyonedu(new BigDecimal(respDto.getKeyonedu()));//可用额度
                accountDto.setKeyongye(new BigDecimal(respDto.getKeyongye()));//可用余额
                accountDto.setKhakjine(new BigDecimal(respDto.getKhakjine()));//可扣划金额
                accountDto.setKzfbdjbz(respDto.getKzfbdjbz());//客户账户封闭冻结标志
                accountDto.setKzhuztai(changezhuangtai(respDto.getKzhuztai()));//客户账户状态
                accountDto.setKzjedjbz(respDto.getKzjedjbz());//客户账户金额冻结标志
                accountDto.setKzzfbsbz(respDto.getKzzfbsbz());//客户账户只付不收标志
                accountDto.setKzzsbfbz(respDto.getKzzsbfbz());//客户账户只收不付标志
                accountDto.setPingzhma(respDto.getPingzhma());//凭证号码
                accountDto.setPngzzlei(respDto.getPngzzlei());//凭证种类
                accountDto.setQixiriqi(respDto.getQixiriqi());//起息日期
                accountDto.setSuoshudx(respDto.getSuoshudx());//产品所属对象
                accountDto.setTduibzhi(respDto.getTduibzhi());//通兑标志
                accountDto.setTonzriqi(respDto.getTonzriqi());//通知日期
                accountDto.setXiohriqi(respDto.getXiohriqi());//销户日期
                accountDto.setYegxriqi(respDto.getYegxriqi());//余额最近更新日期
                accountDto.setZhanghye(new BigDecimal(respDto.getZhhuyuee()));//账户余额
                accountDto.setZhcunfsh(respDto.getZhcunfsh());//转存方式
                accountDto.setZhfbdjbz(respDto.getZhfbdjbz());//账户封闭冻结标志
                accountDto.setZhfutojn(respDto.getZhfutojn());//支付条件
                accountDto.setZhhuztai(respDto.getZhhuztai());//账户状态
                accountDto.setZhhuzwmc(respDto.getZhhuzwmc());//账户名称
                accountDto.setZhjedjbz(respDto.getZhjedjbz());//账户金额冻结标志
                accountDto.setZhzfbsbz(respDto.getZhzfbsbz());
                ;//账户只付不收标志
                accountDto.setZhzsbfbz(respDto.getZhzsbfbz());//账户只收不付标志
                list.add(accountDto);
            }
        }
        return list;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @date 2021/9/30 15:31
     * @version 1.0.0
     * @desc 获取账户
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<AccountDto> getAccount(QueryModel queryModel) {
        List<AccountDto> list = new LinkedList<>();
        String accountno = (String) queryModel.getCondition().get("loanPayoutAccno");
        // 调核心ib1253接口查询结算账户信息
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        ib1253ReqDto.setKehuzhao(accountno);//客户账号
        ib1253ReqDto.setZhhaoxuh("");// 账户序号
        ib1253ReqDto.setMimammmm("");//密码
        ib1253ReqDto.setYanmbzhi(CmisCommonConstants.STD_ZB_YES_NO_0);//密码校验方式
        ib1253ReqDto.setKehzhao2("");//客户账号2
        ib1253ReqDto.setShifoubz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否标志
        ib1253ReqDto.setZhufldm1("");//账户分类代码1
        ib1253ReqDto.setZhufldm2("");//账户分类代码2
        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        if (!"0".equals(resultDto.getCode())) {
            log.error("ib1253接口调用异常");
            throw BizException.error(null, EcbEnum.ECB019999.key, resultDto.getMessage());
        }
        if (Objects.nonNull(resultDto.getData())) {
            Ib1253RespDto respDto = resultDto.getData();
            AccountDto accountDto = new AccountDto();
            accountDto.setBeiyzd01(respDto.getBeiyzd01());//备用字段01
            accountDto.setBeiyzd02(respDto.getBeiyzd02());//备用字段02
            accountDto.setBeiyzd03(respDto.getBeiyzd03());//备用字段03
            accountDto.setChanpshm(respDto.getChanpshm());//产品说明
            accountDto.setChaohubz(respDto.getChaohubz());//账户钞汇标志
            accountDto.setChapbhao(respDto.getChapbhao());//产品编号
            accountDto.setCunkzlei(changeCunkzlei(respDto.getCunkzlei()));//存款种类
            accountDto.setCunqiiii(respDto.getCunqiiii());//存期
            accountDto.setDoqiriqi(respDto.getDoqiriqi());//到期日期
            accountDto.setDspzsyzt(respDto.getDspzsyzt());//凭证状态
            accountDto.setHuobdaih(changeCurrency(respDto.getHuobdaih()));//货币代号
            accountDto.setKaihguiy(respDto.getKaihguiy());//账户开户柜员
            accountDto.setKaihjigo(respDto.getZhkhjigo());//账户开户机构
            accountDto.setKaihriqi(respDto.getKaihriqi());////开户日期
            accountDto.setKaihuqud(respDto.getKaihuqud());//开户渠道
            accountDto.setKehuhaoo(respDto.getKehuhaoo());//客户号
            accountDto.setKehuzhao(accountno);//客户账号
            accountDto.setZhhaoxuh(respDto.getZhaoxhao());//账户子序号
            accountDto.setKzfbdjbz(respDto.getKzfbdjbz());//客户账户封闭冻结标志
            accountDto.setKzhuztai(changezhuangtai(respDto.getKzhuztai()));//客户账户状态
            accountDto.setKzjedjbz(respDto.getKzjedjbz());//客户账户金额冻结标志
            accountDto.setKzzfbsbz(respDto.getKzzfbsbz());//客户账户只付不收标志
            accountDto.setKzzsbfbz(respDto.getKzzsbfbz());//客户账户只收不付标志
            accountDto.setPingzhma(respDto.getPingzhma());//凭证号码
            accountDto.setPngzzlei(respDto.getPngzzlei());//凭证种类
            accountDto.setQixiriqi(respDto.getQixiriqi());//起息日期
            accountDto.setSuoshudx(respDto.getSuoshudx());//产品所属对象
            accountDto.setTduibzhi(respDto.getTduibzhi());//通兑标志
            accountDto.setTonzriqi(respDto.getTonzriqi());//通知日期
            accountDto.setXiohriqi(respDto.getXiohriqi());//销户日期
            accountDto.setYegxriqi(respDto.getYegxriqi());//余额最近更新日期
            accountDto.setZhcunfsh(respDto.getZhcunfsh());//转存方式
            accountDto.setZhfbdjbz(respDto.getZhfbdjbz());//账户封闭冻结标志
            accountDto.setZhfutojn(respDto.getZhfutojn());//支付条件
            accountDto.setZhhuztai(respDto.getZhhuztai());//账户状态
            accountDto.setZhhuzwmc(respDto.getZhhuzwmc());//账户名称
            accountDto.setZhjedjbz(respDto.getZhjedjbz());//账户金额冻结标志
            accountDto.setZhzfbsbz(respDto.getZhzfbsbz());
            ;//账户只付不收标志
            accountDto.setZhzsbfbz(respDto.getZhzsbfbz());//账户只收不付标志
            list.add(accountDto);
        }
        return list;
    }

    //币种转换
    public String changeCunkzlei(String cunkzlei) {
        if ("00".equals(cunkzlei)) {
            cunkzlei = "单位活期存款";
        } else if ("01".equals(cunkzlei)) {
            cunkzlei = "财政性存款";
        } else if ("02".equals(cunkzlei)) {
            cunkzlei = "单位定期存款";
        } else if ("03".equals(cunkzlei)) {
            cunkzlei = "单位一般定期存款(自定义存期)";
        } else if ("04".equals(cunkzlei)) {
            cunkzlei = "国库定期存款";
        } else if ("05".equals(cunkzlei)) {
            cunkzlei = "单位通知存款";
        } else if ("06".equals(cunkzlei)) {
            cunkzlei = "单位保证金活期存款";
        } else if ("07".equals(cunkzlei)) {
            cunkzlei = "单位保证金定期存款";
        } else if ("08".equals(cunkzlei)) {
            cunkzlei = "单位大额存单";
        } else if ("09".equals(cunkzlei)) {
            cunkzlei = "单位协议存款";
        } else if ("10".equals(cunkzlei)) {
            cunkzlei = "单位结构性存款";
        } else if ("14".equals(cunkzlei)) {
            cunkzlei = "同业存放活期";
        } else if ("15".equals(cunkzlei)) {
            cunkzlei = "同业存放定期";
        } else if ("16".equals(cunkzlei)) {
            cunkzlei = "个人活期存款";
        } else if ("17".equals(cunkzlei)) {
            cunkzlei = "个人银行卡活期存款";
        } else if ("18".equals(cunkzlei)) {
            cunkzlei = "个人其他定期存款";
        } else if ("20".equals(cunkzlei)) {
            cunkzlei = "个人定活两便储蓄存款";
        } else if ("25".equals(cunkzlei)) {
            cunkzlei = "个人整存整取存款";
        } else if ("28".equals(cunkzlei)) {
            cunkzlei = "个人零存整取存款";
        } else if ("30".equals(cunkzlei)) {
            cunkzlei = "个人存本取息存款";
        } else if ("32".equals(cunkzlei)) {
            cunkzlei = "个人教育储蓄存款";
        } else if ("34".equals(cunkzlei)) {
            cunkzlei = "个人整存零取存款";
        } else if ("36".equals(cunkzlei)) {
            cunkzlei = "个人通知存款";
        } else if ("40".equals(cunkzlei)) {
            cunkzlei = "个人大额存单";
        } else if ("43".equals(cunkzlei)) {
            cunkzlei = "个人结构性存款";
        } else if ("80".equals(cunkzlei)) {
            cunkzlei = "单位组合产品(虚拟)";
        } else if ("57".equals(cunkzlei)) {
            cunkzlei = "单位零存整取";
        } else if ("58".equals(cunkzlei)) {
            cunkzlei = "个人添利存";
        } else if ("59".equals(cunkzlei)) {
            cunkzlei = "个人新年特刊";
        } else if ("60".equals(cunkzlei)) {
            cunkzlei = "个人步步为盈2号";
        } else if ("61".equals(cunkzlei)) {
            cunkzlei = "个人步步为盈5号";
        } else if ("62".equals(cunkzlei)) {
            cunkzlei = "个人步步为盈3号";
        } else if ("70".equals(cunkzlei)) {
            cunkzlei = "委托存款";
        } else if ("47".equals(cunkzlei)) {
            cunkzlei = "账户透支";
        } else if ("99".equals(cunkzlei)) {
            cunkzlei = "个人定增宝";
        } else if ("63".equals(cunkzlei)) {
            cunkzlei = "个人步步为盈6号";
        } else if ("90".equals(cunkzlei)) {
            cunkzlei = "单位步步为盈2号";
        } else if ("91".equals(cunkzlei)) {
            cunkzlei = "单位步步为盈5号";
        } else if ("92".equals(cunkzlei)) {
            cunkzlei = "单位步步为盈6号";
        } else if ("93".equals(cunkzlei)) {
            cunkzlei = "单位结算宝";
        } else if ("94".equals(cunkzlei)) {
            cunkzlei = "单位步步高";
        } else if ("95".equals(cunkzlei)) {
            cunkzlei = "个人定存宝";
        } else if ("96".equals(cunkzlei)) {
            cunkzlei = "个人电子账户智能存款";
        } else if ("97".equals(cunkzlei)) {
            cunkzlei = "七天利滚利";
        } else if ("98".equals(cunkzlei)) {
            cunkzlei = "个人智能通知存款";
        } else if ("66".equals(cunkzlei)) {
            cunkzlei = "利增宝";
        } else if ("26".equals(cunkzlei)) {
            cunkzlei = "升利宝";
        }
        return cunkzlei;

    }

    //存款种类
    public String changeCurrency(String cur) {
        if ("01".equals(cur)) {
            cur = "CNY";
        } else if ("12".equals(cur)) {
            cur = "GBP";
        } else if ("13".equals(cur)) {
            cur = "HKD";
        } else if ("14".equals(cur)) {
            cur = "USD";
        } else if ("15".equals(cur)) {
            cur = "CHF";
        } else if ("21".equals(cur)) {
            cur = "SEK";
        } else if ("27".equals(cur)) {
            cur = "JPY";
        } else if ("29".equals(cur)) {
            cur = "AUD";
        } else if ("38".equals(cur)) {
            cur = "EUR";
        }
        return cur;
    }

    //账户状态
    public String changezhuangtai(String zt) {
        if ("A".equals(zt)) {
            zt = "正常";
        } else if ("J".equals(zt)) {
            zt = "控制";
        } else if ("C".equals(zt)) {
            zt = "销户";
        } else if ("D".equals(zt)) {
            zt = "久悬户";
        } else if ("I".equals(zt)) {
            zt = "转营业外收入";
        } else if ("H".equals(zt)) {
            zt = "待启用";
        } else if ("G".equals(zt)) {
            zt = "未启用";
        } else if ("Y".equals(zt)) {
            zt = "预销户";
        }
        return zt;
    }

    // 最高额授信协议获取产品类型属性
    public String getPrdTypeProp(String lmtAccNo, String prdId) {
        String prdTypeProp = "";
        // 额度品种编号
        String lmtBizType = "";
        ResultDto<List<LmtSubPrdMappConfDto>> listResultDto = cmisLmtClientService.selectLimitSubNoByPrdId(prdId);
        if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode()) && CollectionUtils.nonEmpty(listResultDto.getData())) {
            lmtBizType = listResultDto.getData().get(0).getLimitSubNo();
        }
        List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(lmtAccNo);
        if (CollectionUtils.nonEmpty(lmtReplyAccSubPrdList)) {
            for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                if (Objects.equals(lmtBizType, lmtReplyAccSubPrd.getLmtBizType())) {
                    prdTypeProp = lmtReplyAccSubPrd.getLmtBizTypeProp();
                    return prdTypeProp;
                }
            }
        }
        log.info("获取产品类型属性【{}】成功！", prdTypeProp);
        return prdTypeProp;
    }

    /**
     * 根据合同号查询放款
     * wzy
     */
    public List<PvpLoanApp> selectByContNoAlive(String contNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", contNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectAll(queryModel);
    }

    /**
     * 资料未全生成影像补扫任务 房抵e点贷，省心快贷线上提款
     *
     * @author zhangliang15
     * @date 2021/10/19 20:29
     **/
    public void sxkdOnlinecreateImageSpplInfo(String pvpSerno) {
        PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPrimaryKey(pvpSerno);
        DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
        docImageSpplClientDto.setBizSerno(pvpSerno);// 关联业务流水号
        docImageSpplClientDto.setBizInstanceId("");// 原业务流程实例
        docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_01);// 影像补扫类型 01:放款影像补扫
        docImageSpplClientDto.setSpplBizType(CmisCommonConstants.STD_SPPL_TYPE_01);
        docImageSpplClientDto.setCusId(pvpLoanApp.getCusId());// 客户号
        docImageSpplClientDto.setCusName(pvpLoanApp.getCusName());// 客户名称
        docImageSpplClientDto.setContNo(pvpLoanApp.getContNo());// 合同编号
        docImageSpplClientDto.setBillNo(pvpLoanApp.getBillNo());// 借据编号
        docImageSpplClientDto.setInputId(pvpLoanApp.getManagerId());// 登记人
        docImageSpplClientDto.setInputBrId(pvpLoanApp.getManagerBrId());// 登记机构
        docImageSpplClientDto.setPrdId(pvpLoanApp.getPrdId());
        docImageSpplClientDto.setPrdName(pvpLoanApp.getPrdName());
        docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/10/22 16:40
     * @version 1.0.0
     * @desc 查询是否存在审批中的放款
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public PvpLoanApp selectRunByContNo(String contNo) {
        return pvpLoanAppMapper.selectRunByContNo(contNo);
    }

    /**
     * @param
     * @return 根据合同号查询合同信息
     * @author xs
     * @date 2021/10/22 16:40
     * @version 1.0.0
     * @desc 最高额合同 普通贷款合同 资产池协议
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<Map<String,String>>  selectContByContNo(String contNo) {
        log.info("根据合同号【{}】查询合同信息开始",contNo);
        // 合同对象信息
        CtrLoanCont ctrLoanCont = null;
        // 最高额授信协议对象信息
        CtrHighAmtAgrCont ctrHighAmtAgrCont = null;
        // 资产池协议
        CtrAsplDetails ctrAsplDetails = null;
        Map<String,String> map = new HashMap<String,String>();
        ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        if (Objects.isNull(ctrLoanCont)) {
            ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
            if (Objects.isNull(ctrHighAmtAgrCont)) {
                ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
                if (Objects.isNull(ctrAsplDetails)) {
                    return new ResultDto(null).message("未查询到合同信息").code("9999");
                }else{
                    map.put("iqpSerno",ctrAsplDetails.getSerno());
                }
            }else{
                map.put("iqpSerno",ctrHighAmtAgrCont.getSerno());
            }
        }else{
            map.put("iqpSerno",ctrLoanCont.getIqpSerno());
        }
        return new ResultDto(map);
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo>
     * @author qw
     * @date 2021/10/17 15:22
     * @version 1.0.0
     * @desc 根据客户编号查询结算账户信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<LstacctinfoDto> getAccNoListBycusId(QueryModel queryModel) {
        Dp2021ReqDto dp2021ReqDto = new Dp2021ReqDto();
        dp2021ReqDto.setChxunbis(100);
        dp2021ReqDto.setKehuhaoo(queryModel.getCondition().get("cusId").toString());
        dp2021ReqDto.setQishibis(1);
        Dp2021RespDto dp2021RespDto = Optional.ofNullable(dp2021Service.dp2021(dp2021ReqDto)).orElse(new Dp2021RespDto());
        List<Lstacctinfo> lstacctinfo = Optional.ofNullable(dp2021RespDto.getLstacctinfo()).orElse(new ArrayList<>());

        // 过滤结算账户
        lstacctinfo = lstacctinfo.stream().filter(s -> ("1".equals(s.getShfojshu()))).collect(Collectors.toList());
        List<LstacctinfoDto> list = new ArrayList<>();
        for (int i = 0; i < lstacctinfo.size(); i++) {
            Lstacctinfo record = lstacctinfo.get(i);
            // 过滤母户
            if("00001".equals(record.getZhhaoxuh())){
                LstacctinfoDto lstacctinfoDto = new LstacctinfoDto();
                cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(record, lstacctinfoDto);
                if (record.getKaihjigo().startsWith("80")) {
                    lstacctinfoDto.setKaihjigomc("寿光村镇银行");
                } else if (record.getKaihjigo().startsWith("81")) {
                    lstacctinfoDto.setKaihjigomc("东海村镇银行");
                } else {
                    lstacctinfoDto.setKaihjigomc("张家港农村商业银行");
                }
                list.add(lstacctinfoDto);
            }
        }
        // 筛选结算账户
        return list;
    }

    // 获取客户五十级分类
    public Map getFiveAndTenClass(String cusId){
        Map result = new HashMap();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("accStatusExist",CmisCommonConstants.ACC_STATUS_EXIST);
        queryModel.setSort("loanStartDate desc");
        List<AccLoan> accLoanList = accLoanService.selectByModel(queryModel);
        if(CollectionUtils.nonEmpty(accLoanList)){
            AccLoan accLoanData = accLoanList.get(0);
            result.put("fiveClass",accLoanData.getFiveClass());
            result.put("tenClass",accLoanData.getTenClass());
        }else {
            // 五级分类 10--正常
            result.put("fiveClass","10");
            // 十级分类 11--正常1
            result.put("tenClass","11");
        }
        return result;
    }
}