/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccp
 * @类描述: acc_accp数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 16:02:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_accp")
public class AccAccp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 出账流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;

	/** 核心银承编号 **/
	@Column(name = "CORE_BILL_NO", unique = false, nullable = true, length = 20)
	private String coreBillNo;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 是否电子票据 **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;

	/** 是否他行代签 **/
	@Column(name = "IS_OTHER_SIGN", unique = false, nullable = true, length = 5)
	private String isOtherSign;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 票据总金额 **/
	@Column(name = "DRFT_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftTotalAmt;

	/** 票据总余额 **/
	@Column(name = "DRFT_BALANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftBalanceAmt;
	
	/** 开票张数 **/
	@Column(name = "ISSE_CNT", unique = false, nullable = true, length = 10)
	private String isseCnt;
	
	/** 出票日期 **/
	@Column(name = "ISSE_DATE", unique = false, nullable = true, length = 20)
	private String isseDate;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 手续费率 **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;
	
	/** 敞口金额 **/
	@Column(name = "SPAC_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spacAmt;
	
	/** 手续费类型 **/
	@Column(name = "CHRG_TYPE", unique = false, nullable = true, length = 5)
	private String chrgType;
	
	/** 手续费金额 **/
	@Column(name = "CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgAmt;
	
	/** 逾期执款年利率 **/
	@Column(name = "OVERDUE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRate;
	
	/** 退票计息方式 **/
	@Column(name = "RETURN_DRAFT_INTEREST_TYPE", unique = false, nullable = true, length = 5)
	private String returnDraftInterestType;
	
	/** 承兑行类型 **/
	@Column(name = "AORG_TYPE", unique = false, nullable = true, length = 5)
	private String aorgType;
	
	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;
	
	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 放款机构编号 **/
	@Column(name = "DISB_ORG_NO", unique = false, nullable = true, length = 40)
	private String disbOrgNo;
	
	/** 放款机构名称 **/
	@Column(name = "DISB_ORG_NAME", unique = false, nullable = true, length = 80)
	private String disbOrgName;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;
	
	/** 分类日期 **/
	@Column(name = "CLASS_DATE", unique = false, nullable = true, length = 20)
	private String classDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 操作类型 STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 原始敞口金额 **/
	@Column(name = "ORIGI_OPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenAmt;
	
	/** 是否省心E票(STD_ZB_YES_NO) **/
	@Column(name = "IS_SXEP", unique = false, nullable = true, length = 5)
	private String isSxep;

	/** 是否资产池(STD_ZB_YES_NO) **/
	@Column(name = "IS_POOL", unique = false, nullable = true, length = 5)
	private String isPool;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param coreBillNo
	 */
	public void setCoreBillNo(String coreBillNo) {
		this.coreBillNo = coreBillNo;
	}
	
    /**
     * @return coreBillNo
     */
	public String getCoreBillNo() {
		return this.coreBillNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}
	
    /**
     * @return isEDrft
     */
	public String getIsEDrft() {
		return this.isEDrft;
	}

	/**
	 * @return isOtherSign
	 */
	public String getIsOtherSign() {
		return this.isOtherSign;
	}

	/**
	 * @param isOtherSign
	 */
	public void setIsOtherSign(String isOtherSign) {
		this.isOtherSign = isOtherSign;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param drftTotalAmt
	 */
	public void setDrftTotalAmt(java.math.BigDecimal drftTotalAmt) {
		this.drftTotalAmt = drftTotalAmt;
	}
	
    /**
     * @return drftTotalAmt
     */
	public java.math.BigDecimal getDrftTotalAmt() {
		return this.drftTotalAmt;
	}
	
	/**
	 * @param isseCnt
	 */
	public void setIsseCnt(String isseCnt) {
		this.isseCnt = isseCnt;
	}
	
    /**
     * @return isseCnt
     */
	public String getIsseCnt() {
		return this.isseCnt;
	}
	
	/**
	 * @param isseDate
	 */
	public void setIsseDate(String isseDate) {
		this.isseDate = isseDate;
	}
	
    /**
     * @return isseDate
     */
	public String getIsseDate() {
		return this.isseDate;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}
	
    /**
     * @return chrgRate
     */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(java.math.BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return spacAmt
     */
	public java.math.BigDecimal getSpacAmt() {
		return this.spacAmt;
	}
	
	/**
	 * @param chrgType
	 */
	public void setChrgType(String chrgType) {
		this.chrgType = chrgType;
	}
	
    /**
     * @return chrgType
     */
	public String getChrgType() {
		return this.chrgType;
	}
	
	/**
	 * @param chrgAmt
	 */
	public void setChrgAmt(java.math.BigDecimal chrgAmt) {
		this.chrgAmt = chrgAmt;
	}
	
    /**
     * @return chrgAmt
     */
	public java.math.BigDecimal getChrgAmt() {
		return this.chrgAmt;
	}
	
	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}
	
    /**
     * @return overdueRate
     */
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}
	
	/**
	 * @param returnDraftInterestType
	 */
	public void setReturnDraftInterestType(String returnDraftInterestType) {
		this.returnDraftInterestType = returnDraftInterestType;
	}
	
    /**
     * @return returnDraftInterestType
     */
	public String getReturnDraftInterestType() {
		return this.returnDraftInterestType;
	}
	
	/**
	 * @param aorgType
	 */
	public void setAorgType(String aorgType) {
		this.aorgType = aorgType;
	}
	
    /**
     * @return aorgType
     */
	public String getAorgType() {
		return this.aorgType;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}
	
    /**
     * @return aorgNo
     */
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}
	
    /**
     * @return aorgName
     */
	public String getAorgName() {
		return this.aorgName;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo;
	}
	
    /**
     * @return disbOrgNo
     */
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}
	
	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName;
	}
	
    /**
     * @return disbOrgName
     */
	public String getDisbOrgName() {
		return this.disbOrgName;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}
	
    /**
     * @return tenClass
     */
	public String getTenClass() {
		return this.tenClass;
	}
	
	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}
	
    /**
     * @return classDate
     */
	public String getClassDate() {
		return this.classDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param origiOpenAmt
	 */
	public void setOrigiOpenAmt(java.math.BigDecimal origiOpenAmt) {
		this.origiOpenAmt = origiOpenAmt;
	}
	
    /**
     * @return origiOpenAmt
     */
	public java.math.BigDecimal getOrigiOpenAmt() {
		return this.origiOpenAmt;
	}
	
	/**
	 * @param isSxep
	 */
	public void setIsSxep(String isSxep) {
		this.isSxep = isSxep;
	}
	
    /**
     * @return isSxep
     */
	public String getIsSxep() {
		return this.isSxep;
	}

	public String getIsPool() {
		return isPool;
	}

	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}

	public BigDecimal getDrftBalanceAmt() {
		return drftBalanceAmt;
	}

	public void setDrftBalanceAmt(BigDecimal drftBalanceAmt) {
		this.drftBalanceAmt = drftBalanceAmt;
	}
}