
/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.MapUtils;
import com.alibaba.fastjson.JSON;
import com.jcraft.jsch.UserInfo;
import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtReplySubMapper;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplySubService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:34:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplySubService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplySubService.class);

    @Resource
    private LmtReplySubMapper lmtReplySubMapper;

    // 单一客户授信审批分项
    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    // 单一客户授信批复变更分项
    @Autowired
    private LmtReplyChgSubService lmtReplyChgSubService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtReplyService lmtReplyService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplySub selectByPrimaryKey(String pkId) {
        return lmtReplySubMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplySub> selectAll(QueryModel model) {
        List<LmtReplySub> records = (List<LmtReplySub>) lmtReplySubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplySub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplySub> list = lmtReplySubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplySub record) {
        return lmtReplySubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplySub record) {
        return lmtReplySubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtReplySub record) {
        return lmtReplySubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplySub record) {
        return lmtReplySubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplySubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplySubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplySubByParams
     * @方法描述: 通过参数查询授信批复分项数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplySub> queryLmtReplySubByParams(Map queryMaps) {
        if(!MapUtils.checkExistsEmptyEnum(queryMaps)){
            return lmtReplySubMapper.queryLmtReplySubByParams(queryMaps);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }

    }

    /**
     * @方法名称: queryLmtReplySubByParams
     * @方法描述: 通过参数查询授信批复分项数据
     */
    public LmtReplySub queryLmtReplySubByReplySubSerno(String replySubNo) {
        return lmtReplySubMapper.queryLmtReplySubByReplySubSerno(replySubNo);
    }

    /**
     * @方法名称: generateNewLmtReplySubByLmtApprSub
     * @方法描述: 获取审批授信分项并生成授信批复分项和分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplySubByLmtAppr(LmtAppr lmtAppr, String currentUserId, String currentOrgId, String repaySerno) throws Exception {
        HashMap<String, String> subQueryMap = new HashMap<String, String>();
        subQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        subQueryMap.put("apprSerno", lmtAppr.getApproveSerno());
        List<LmtApprSub> lmtApprSubList = lmtApprSubService.selectLmtApprSubByParams(subQueryMap);
        log.info("查询授信审批中的分项信息:" + JSON.toJSONString(lmtApprSubList));
        if (lmtApprSubList != null && lmtApprSubList.size() > 0) {
            for (LmtApprSub lmtApprSub : lmtApprSubList) {
                String repaySubSerno = generateNewLmtReplySubByLmtApprSub(currentUserId, currentOrgId, repaySerno, lmtApprSub);
                lmtReplySubPrdService.generateNewLmtReplySubPrdByLmtApprSub(currentUserId, currentOrgId, lmtApprSub, repaySubSerno);
            }
        } else {
            throw new Exception("查询审批分项数据异常！");
        }
    }

    /**
     * @方法名称: generateNewLmtReplySubByLmtApprSub
     * @方法描述: 获取审批授信分项并生成授信批复分项和分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    private String generateNewLmtReplySubByLmtApprSub(String currentUserId, String currentOrgId, String replySerno, LmtApprSub lmtApprSub) {
        String repaySubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        LmtReplySub lmtReplySub = new LmtReplySub();
        BeanUtils.copyProperties(lmtApprSub, lmtReplySub);
        log.info("查询流水号项下是否存在分项明细品种为预授信的品种,分项流水号:" + lmtApprSub.getApproveSubSerno());
        List<LmtApproveSubPrdDto> prds = lmtApprSubService.selectPrdListByApproveSubSerno(lmtApprSub.getApproveSubSerno());
        if(!prds.isEmpty() && prds.size() > 0){
            for(LmtApproveSubPrdDto lmtApproveSubPrdDto : prds){
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtApproveSubPrdDto.getIsPreLmt())){
                    lmtReplySub.setIsPreLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                    break;
                }
            }
        }
        log.info("生成新的授信批复分项流水号:" + repaySubSerno);
        lmtReplySub.setReplySubSerno(repaySubSerno);
        lmtReplySub.setReplySerno(replySerno);
        lmtReplySub.setPkId(UUID.randomUUID().toString());
        lmtReplySub.setInputId(currentUserId);
        lmtReplySub.setInputBrId(currentOrgId);
        lmtReplySub.setInputDate(openday);
        lmtReplySub.setUpdId(currentUserId);
        lmtReplySub.setUpdBrId(currentOrgId);
        lmtReplySub.setUpdDate(openday);
        lmtReplySub.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        HashMap<String, String> subPrdQueryMap = new HashMap<String, String>();
        subPrdQueryMap.put("apprSubSerno", lmtApprSub.getApproveSubSerno());
        subPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdService.selectLmtApprSubPrdByParams(subPrdQueryMap);
        boolean isPre = true;
        StringBuilder subName = new StringBuilder();
        for(LmtApprSubPrd lmtApprSubPrd :lmtApprSubPrdList){
            subName.append(lmtApprSubPrd.getLmtBizTypeName()).append(",");
        }
        if(subName.length() >0 ){
            subName.deleteCharAt(subName.length()-1);
        }
        lmtReplySub.setSubName(subName.toString());
        if (lmtApprSubPrdList != null && lmtApprSubPrdList.size() > 0) {
            for (LmtApprSubPrd lmtApprSubPrd : lmtApprSubPrdList) {
                if(!CmisCommonConstants.YES_NO_0.equals(lmtApprSubPrd.getIsPreLmt())){
                    isPre = false;
                    break;
                }
            }
        }
        if(isPre){
            lmtReplySub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
        }
        lmtReplySubService.insert(lmtReplySub);
        log.info("生成新的授信批复分项:" + JSON.toJSONString(lmtReplySub));
        return repaySubSerno;
    }

    /**
     * @方法名称: generateNewLmtReplySubByLmtReplyChgSub
     * @方法描述: 获取批复变更授信分项并生成授信批复分项和分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplySubByLmtReplyChg(LmtReplyChg lmtReplyChg, String currentUserId, String currentOrgId) throws Exception {
        //申请流水号
        String serno = lmtReplyChg.getSerno();
        HashMap<String, String> subQueryMap = new HashMap<String, String>();
        subQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        subQueryMap.put("serno", serno);

        log.info("根据申请流水号【"+serno+"】查询批复变更分项明细记录");
        List<LmtReplyChgSub> lmtReplyChgSubList = lmtReplyChgSubService.queryLmtReplyChgSubByParams(subQueryMap);

        if (lmtReplyChgSubList != null && lmtReplyChgSubList.size() > 0) {
            //批复流水号
            String replySerno = lmtReplyChg.getReplySerno();
            HashMap<String, String> replyQueryMap = new HashMap<String, String>();
            replyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            replyQueryMap.put("replySerno", replySerno);
            log.info("根据批复流水号【"+replySerno+"】查询授信批复分项明细记录");
            List<LmtReplySub> lmtReplySubList = this.queryLmtReplySubByParams(replyQueryMap);

            for (LmtReplyChgSub lmtReplyChgSub : lmtReplyChgSubList) {
                LmtReplySub lmtReplySub = new LmtReplySub();
                boolean isNew = true;
                for (LmtReplySub item : lmtReplySubList) {
                    if (item.getReplySubSerno().equals(lmtReplyChgSub.getReplySubSerno())) {
                        lmtReplySub = item;
                        updateLmtReplySubByLmtReplyChgSub(currentUserId, currentOrgId, lmtReplyChgSub, item);
                        isNew = false;
                        break;
                    }
                }
                if (isNew) {
                    // 如果分项是新增的，则调用新增逻辑
                    String repaySubSerno = generateNewLmtReplySubByLmtReplyChgSub(currentUserId, currentOrgId, lmtReplyChgSub, lmtReplyChg.getReplySerno());
                    lmtReplySubPrdService.generateNewLmtReplySubPrdByLmtReplyChgSub(currentUserId, currentOrgId, lmtReplyChgSub, repaySubSerno);
                } else {
                    // 更新授信批复分项适用品种
                    lmtReplySubPrdService.updateLmtReplySubPrdByLmtReplyChgSub(currentUserId, currentOrgId, lmtReplyChgSub, lmtReplySub.getReplySerno());
                }
            }
        } else {
            throw new Exception("查询批复调整分项数据异常！");
        }
    }

    /**
     * @方法名称: updateLmtReplySubByLmtReplyChgSub
     * @方法描述: 通过授信批复变更分项修改批复分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplySubByLmtReplyChgSub(String currentUserId, String currentOrgId, LmtReplyChgSub lmtReplyChgSub, LmtReplySub lmtReplySub) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        log.info("根据授信批复明细变更信息更新对应的授信批复明细信息开始");
        LmtReplySub updateLmtReplySub = (LmtReplySub) SerializationUtils.clone(lmtReplySub);
        BeanUtils.copyProperties(lmtReplyChgSub, updateLmtReplySub);

        //主键、批复流水号、申请流水号、批复分项流水号、分项流水号、分项名称、登记人、登记机构、登记日期、创建时间不变
        updateLmtReplySub.setPkId(lmtReplySub.getPkId());
        updateLmtReplySub.setReplySerno(lmtReplySub.getReplySerno());
        updateLmtReplySub.setReplySubSerno(lmtReplySub.getReplySubSerno());
        updateLmtReplySub.setSerno(lmtReplySub.getSerno());
        updateLmtReplySub.setSubSerno(lmtReplySub.getSubSerno());
        updateLmtReplySub.setSubName(lmtReplySub.getSubName());
        updateLmtReplySub.setCreateTime(lmtReplySub.getCreateTime());
        updateLmtReplySub.setInputId(lmtReplySub.getInputId());
        updateLmtReplySub.setInputBrId(lmtReplySub.getInputBrId());
        updateLmtReplySub.setInputDate(lmtReplySub.getInputDate());

        updateLmtReplySub.setUpdId(currentUserId);
        updateLmtReplySub.setUpdBrId(currentOrgId);
        updateLmtReplySub.setUpdDate(openday);
        updateLmtReplySub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        this.update(updateLmtReplySub);
        log.info("根据授信批复明细变更信息更新对应的授信批复明细信息结束");
    }


    /**
     * @方法名称: updateLmtReplySubByLmtReplyChgSub
     * @方法描述: 通过授信批复变更分项修改批复分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateNewLmtReplySubByLmtReplyChgSub(String currentUserId, String currentOrgId, LmtReplyChgSub lmtReplyChgSub, String repaySerno) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String repaySubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("生成新的授信批复分项流水号:" + repaySubSerno);
        LmtReplySub lmtReplySub = new LmtReplySub();
        BeanUtils.copyProperties(lmtReplyChgSub, lmtReplySub);
        lmtReplySub.setReplySubSerno(repaySubSerno);
        lmtReplySub.setReplySerno(repaySerno);
        lmtReplySub.setPkId(UUID.randomUUID().toString());
        lmtReplySub.setInputId(currentUserId);
        lmtReplySub.setInputBrId(currentOrgId);
        lmtReplySub.setInputDate(openday);
        lmtReplySub.setUpdId(currentUserId);
        lmtReplySub.setUpdBrId(currentOrgId);
        lmtReplySub.setUpdDate(openday);
        lmtReplySub.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySubService.insert(lmtReplySub);
        log.info("生成新的授信批复分项:" + lmtReplySub.toString());
        return repaySubSerno;
    }


    /**
     * @方法名称: copyToLmtAppSub
     * @方法描述: 将原先的授信批复分项及对应的适用产品明细要挂载在新的授信流水号下
     * @创建人: zhuzr
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyToLmtAppSub(String originSerno, String curSerno) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        LmtApp lmtApp = lmtAppService.selectBySerno(curSerno);
        log.info("查询当前单一授信申请数据:"+JSON.toJSONString(lmtApp));
        HashMap<String, String> params = new HashMap();
        String subSerno = "";
        LmtAppSub lmtAppSub = new LmtAppSub();
        LmtReplySub lmtReplySub = new LmtReplySub();
        params.put("replySerno", originSerno);
        params.put("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtReplySub> subList = this.queryLmtReplySubByParams(params);
        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
        if (subList.size() > 0) {
            for (int i = 0; i < subList.size(); i++) {
                lmtReplySub = subList.get(i);
                String originReplySubSerno = lmtReplySub.getReplySubSerno();
                String originSubSerno = lmtReplySub.getSubSerno();
                subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                BeanUtils.copyProperties(lmtReplySub, lmtAppSub);
                lmtAppSub.setPkId(UUID.randomUUID().toString());
                lmtAppSub.setSerno(curSerno);
                lmtAppSub.setSubSerno(subSerno);
                HashMap<String,String> queryMap = new HashMap<String,String>();
                queryMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
                queryMap.put("replySubSerno",lmtReplySub.getReplySubSerno());
                List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.queryLmtReplyAccSubByParams(queryMap);
                if(lmtReplyAccSubList != null && lmtReplyAccSubList.size() == 1){
                    lmtReplyAccSub = lmtReplyAccSubList.get(0);
                    lmtAppSub.setOrigiLmtAccSubNo(lmtReplyAccSub.getAccSubNo());
                }
                lmtAppSub.setOrigiLmtAccSubAmt(lmtReplySub.getLmtAmt());
                lmtAppSub.setOrigiLmtAccSubTerm(lmtReplySub.getLmtTerm());
                // 如果当前登录人信息能够获取到 则同步更新
                User userInfo = SessionUtils.getUserInformation();
                if(userInfo != null){
                    lmtAppSub.setInputId(userInfo.getLoginCode());  //登记人
                    lmtAppSub.setInputBrId(userInfo.getOrg().getCode());  //登记机构
                    lmtAppSub.setUpdId(userInfo.getLoginCode());  //最近修改人
                    lmtAppSub.setUpdBrId(userInfo.getOrg().getCode());  //最近修改机构
                }
                lmtAppSub.setUpdDate(openday);  //登记日期
                lmtAppSub.setInputDate(openday);  //登记日期
                if(lmtApp.getIsGrp().equals(CmisCommonConstants.YES_NO_1)){
                    log.info("校验新纳入成员一级自有授信期限是否与修改后的成员授信期限保持一致,不一致则自动与成员授信申请数据保持一致-------start--------");
                    if(CmisCommonConstants.LMT_TYPE_01.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_03.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtApp.getLmtType())){
                        log.info("新增,续作,再议情况下,校验新纳入成员分项中自有授信期限与修改后的成员授信期限不一致,自动与保持一致");
                        if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit())){
                            log.info("校验新纳入成员一级自有授信期限是否与修改后的成员授信期限保持一致!");
                            lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                        }
                    }else{
                        if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit()) && lmtAppSub.getLmtTerm().compareTo(lmtApp.getLmtTerm())>0){
                            lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                        }
                    }
                    log.info("校验新纳入成员一级自有授信期限是否与修改后的成员授信期限保持一致,不一致则自动与成员授信申请数据保持一致--------end-------");
                }
                lmtAppSub.setCusName(lmtApp.getCusName());
                int count = lmtAppSubService.insert(lmtAppSub);
                BigDecimal openAmt = new BigDecimal(0);
                BigDecimal lowAmt = new BigDecimal(0);
                List<LmtAppSub> appSubList = lmtAppSubService.queryLmtAppSubBySerno(curSerno);
                for(LmtAppSub lmtAppSubItem: appSubList){
                    if(lmtAppSubItem.getLmtAmt() != null){
                        if ("60".equals(lmtAppSubItem.getGuarMode())) {
                            lowAmt = lowAmt.add(lmtAppSubItem.getLmtAmt());
                        } else {
                            openAmt = openAmt.add(lmtAppSubItem.getLmtAmt());
                        }
                    }
                }
                lmtApp.setLowRiskTotalLmtAmt(lowAmt);
                lmtApp.setOpenTotalLmtAmt(openAmt);
                int countApp = lmtAppService.update(lmtApp);
                if (countApp != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",申请信息更新失败！");
                }
                boolean copySubPrd = lmtReplySubPrdService.copyToBySubSerno(originReplySubSerno, subSerno);
                if (count != 1 || !copySubPrd) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",分项挂载失败！");
                }
                log.info("根据原授信申请流水分项号【{}】生成新的授信申请流水号【{}】的押品关系开始" , originSubSerno, subSerno);
                boolean copyGuarBizRel = lmtReplySubPrdService.copyToByGuarBizRel(lmtReplySub.getSubSerno(), subSerno);
                log.info("根据原授信申请流水分项号【{}】生成新的授信申请流水号【{}】的押品关系结束" , originSubSerno, subSerno);
                if (count != 1 || !copyGuarBizRel) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",分项挂载失败！");
                }
            }
        }
        return true;
    }


    /**
     * @方法名称：getReplySubInfo
     * @方法描述：批复额度分项
     * @创建人：zhangming12
     * @创建时间：2021/5/19 15:37
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<LmtReplySubDto> getReplySubInfo(String replySerno) {
        List<LmtReplySubDto> lmtReplySubDtoList = new ArrayList<>();
        if (StringUtils.isBlank(replySerno)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("replySerno", replySerno);
        List<LmtReplySub> replySubList = this.queryLmtReplySubByParams(queryMap);
        if (replySubList != null && replySubList.size() != 0) {
            lmtReplySubDtoList = (List<LmtReplySubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubList, LmtReplySubDto.class);
            lmtReplySubDtoList.forEach(lmtReplySubDto -> {
                String replySubSerno = lmtReplySubDto.getReplySubSerno();
                String subName = lmtReplySubDto.getSubName();
                if (StringUtils.nonBlank(subName)) {
                    lmtReplySubDto.setLmtDrawType(subName);
                }
                if (!StringUtils.isBlank(replySubSerno)) {
                    lmtReplySubDto.setLmtDrawNo(replySubSerno);
                    HashMap<String, String> queryPrdMap = new HashMap<>();
                    queryPrdMap.put("replySubSerno", replySubSerno);
                    List<LmtReplySubPrd> replySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(queryPrdMap);
                    List<LmtReplySubPrdDto> lmtReplySubPrdDtos = (List<LmtReplySubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubPrdList, LmtReplySubPrdDto.class);
                    for (LmtReplySubPrdDto lmtReplySubPrdDto : lmtReplySubPrdDtos) {
                        String bizTypeName = lmtReplySubPrdDto.getLmtBizTypeName();
                        String replySubPrdSerno = lmtReplySubPrdDto.getReplySubPrdSerno();
                        if (StringUtils.nonBlank(bizTypeName)) {
                            lmtReplySubPrdDto.setLmtDrawType(bizTypeName);
                        }
                        if (StringUtils.nonBlank(replySubPrdSerno)) {
                            lmtReplySubPrdDto.setLmtDrawNo(replySubPrdSerno);
                        }
                    }
                    lmtReplySubDto.setChildren(lmtReplySubPrdDtos);
                }
            });
        }
        return lmtReplySubDtoList;
    }

    /**
     * @方法名称: getReplySubInfoByGrpSerno
     * @方法描述: 通过授信审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtReplySubDto> getReplySubInfoByGrpSerno(String grpSerno) throws Exception {
        List<LmtReplySubDto> lmtReplySubDtoList = new ArrayList<>();
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        if (lmtGrpMemRels != null && lmtGrpMemRels.size() != 0) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                String serno = lmtGrpMemRel.getSingleSerno();
                if (StringUtils.isBlank(serno)) {
                    throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
                }
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("serno", serno);
                List<LmtReplySub> replySubList = this.queryLmtReplySubByParams(queryMap);
                if (replySubList != null && replySubList.size() != 0) {
                    lmtReplySubDtoList = (List<LmtReplySubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubList, LmtReplySubDto.class);
                    lmtReplySubDtoList.forEach(lmtReplySubDto -> {
                        String replySubSerno = lmtReplySubDto.getReplySubSerno();
                        String subName = lmtReplySubDto.getSubName();
                        if (StringUtils.nonBlank(subName)) {
                            lmtReplySubDto.setLmtDrawType(subName);
                        }
                        if (!StringUtils.isBlank(replySubSerno)) {
                            lmtReplySubDto.setLmtDrawNo(replySubSerno);
                            HashMap<String, String> queryPrdMap = new HashMap<>();
                            queryPrdMap.put("replySubSerno", replySubSerno);
                            List<LmtReplySubPrd> replySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(queryPrdMap);
                            List<LmtReplySubPrdDto> lmtReplySubPrdDtos = (List<LmtReplySubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubPrdList, LmtReplySubPrdDto.class);
                            for (LmtReplySubPrdDto lmtReplySubPrdDto : lmtReplySubPrdDtos) {
                                String bizTypeName = lmtReplySubPrdDto.getLmtBizTypeName();
                                String subPrdSerno = lmtReplySubPrdDto.getSubPrdSerno();
                                if (StringUtils.nonBlank(bizTypeName)) {
                                    lmtReplySubPrdDto.setLmtDrawType(bizTypeName);
                                }
                                if (StringUtils.nonBlank(subPrdSerno)) {
                                    lmtReplySubPrdDto.setLmtDrawNo(subPrdSerno);
                                }
                            }
                            lmtReplySubDto.setChildren(lmtReplySubPrdDtos);
                        }
                    });
                }
            }
        }
        return lmtReplySubDtoList;
    }
    public List<LmtReplySub> selectBySerno(String serno){
        return lmtReplySubMapper.selectBySerno(serno);
    }

    /**
     * 查询一般额度分项信息
     * @param serno
     * @return
     */
    public List<LmtReplySub> getNormal(String serno){
        return lmtReplySubMapper.getNormal(serno);
    }

    /**
     * 查询低风险额度分项信息
     * @param serno
     * @return
     */
    public List<LmtReplySub> getLow(String serno){
        return lmtReplySubMapper.getLow(serno);
    }

    /**
     * @方法名称: generateLmtReplySubByLmtAppSub
     * @方法描述: 获取授信审批分项并生成授信批复分项和分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-16 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtReplySubByLmtAppSub(LmtApp lmtApp, String currentUserId, String currentOrgId, String repaySerno) throws Exception {
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        log.info("查询授信申请中的分项信息:" + lmtAppSubList.toString());
        if (lmtAppSubList != null && lmtAppSubList.size() > 0) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String repaySubSerno = generateLmtReplySubByLmtAppSub(currentUserId, currentOrgId, repaySerno, lmtAppSub);
                lmtReplySubPrdService.generateLmtReplySubPrdByLmtAppSub(currentUserId, currentOrgId, lmtAppSub, repaySubSerno);
            }
        } else {
            throw new Exception("查询审批分项数据异常！");
        }
    }

    /**
     * @方法名称: generateLmtReplySubByLmtAppSub
     * @方法描述: 获取授信申请分项并生成授信批复分项和分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    private String generateLmtReplySubByLmtAppSub(String currentUserId, String currentOrgId, String replySerno, LmtAppSub lmtAppSub) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String repaySubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        LmtReplySub lmtReplySub = new LmtReplySub();
        BeanUtils.copyProperties(lmtAppSub, lmtReplySub);
        log.info("生成新的授信批复分项流水号:" + repaySubSerno);
        lmtReplySub.setReplySubSerno(repaySubSerno);
        lmtReplySub.setReplySerno(replySerno);
        lmtReplySub.setPkId(UUID.randomUUID().toString());
        lmtReplySub.setInputId(currentUserId);
        lmtReplySub.setInputBrId(currentOrgId);
        lmtReplySub.setInputDate(openday);
        lmtReplySub.setUpdId(currentUserId);
        lmtReplySub.setUpdBrId(currentOrgId);
        lmtReplySub.setUpdDate(openday);
        lmtReplySub.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySubService.insert(lmtReplySub);
        log.info("生成新的授信批复分项:" + lmtReplySub.toString());
        return repaySubSerno;
    }
}
