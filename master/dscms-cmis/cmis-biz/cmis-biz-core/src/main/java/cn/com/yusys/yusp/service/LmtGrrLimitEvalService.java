/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtGrrLimitEval;
import cn.com.yusys.yusp.repository.mapper.LmtGrrLimitEvalMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrrLimitEvalService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-31 17:50:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrrLimitEvalService {
    private static final Logger log = LoggerFactory.getLogger(LmtGrrLimitEvalService.class);
    @Autowired
    private LmtGrrLimitEvalMapper lmtGrrLimitEvalMapper;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtGrrLimitEval selectByPrimaryKey(String pkId) {
        return lmtGrrLimitEvalMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrrLimitEval> selectAll(QueryModel model) {
        List<LmtGrrLimitEval> records = (List<LmtGrrLimitEval>) lmtGrrLimitEvalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtGrrLimitEval> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrrLimitEval> list = lmtGrrLimitEvalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtGrrLimitEval record) {
        return lmtGrrLimitEvalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtGrrLimitEval record) {
        return lmtGrrLimitEvalMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtGrrLimitEval record) {
        return lmtGrrLimitEvalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtGrrLimitEval record) {
        return lmtGrrLimitEvalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrrLimitEvalMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrrLimitEvalMapper.deleteByIds(ids);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public LmtGrrLimitEval selectBySerno(String serno) {
        return lmtGrrLimitEvalMapper.selectBySerno(serno);
    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-05-05 15:35
     * @注释 授信保证人担保额度测算表查询
     */
    public List<LmtGrrLimitEval> queryLmtGrrLimitEval(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrrLimitEval> list = lmtGrrLimitEvalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 授信保证人担保额度测算表,新增
     *
     * @param lmtGrrLimitEval
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveLmtGrrLimitEval(LmtGrrLimitEval lmtGrrLimitEval) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (lmtGrrLimitEval == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                lmtGrrLimitEval.setInputId(userInfo.getLoginCode());
                lmtGrrLimitEval.setInputBrId(userInfo.getOrg().getCode());
                lmtGrrLimitEval.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrrLimitEval.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
            }
            Map seqMap = new HashMap();
            //String serno = StringUtils.uuid(true);
            // sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ,seqMap);
            String serno = lmtGrrLimitEval.getSerno();
            lmtGrrLimitEval.setPkId(StringUtils.uuid(true));
            int insertCount = lmtGrrLimitEvalMapper.insertSelective(lmtGrrLimitEval);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("serno", serno);
            log.info("保证人担保额度测算表" + serno + "-新增成功！");
        } catch (YuspException e) {
            log.error("保证人担保额度测算表新增异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保证人担保额度测算表异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 授信保证人担保额度测算表修改
     *
     * @param lmtGrrLimitEval
     * @returnf
     * @author:zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateLmtGrrLimitEval(LmtGrrLimitEval lmtGrrLimitEval) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (lmtGrrLimitEval == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                lmtGrrLimitEval.setUpdId(userInfo.getLoginCode());
                lmtGrrLimitEval.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrrLimitEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrrLimitEval.setOprType("01");
            }
            Map seqMap = new HashMap();
            int updCount = lmtGrrLimitEvalMapper.updateByPrimaryKey(lmtGrrLimitEval);
            if (updCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
        } catch (YuspException e) {
            log.error("授信保证人担保额度测算表修改异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("授信保证人担保额度测算表异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 初始化引入分项保证人数据
     *
     * @param serno
     * @return
     */
    public int initLmtGrrLimitEval(String serno) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.getGuarGuaranteeBySerno(serno);
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        List<LmtGrrLimitEval> lmtGrrLimitEvals = lmtGrrLimitEvalMapper.selectByModel(model);
        int count = 0;
        if (null != guarGuaranteeList && guarGuaranteeList.size() > 0) {
            for (GuarGuarantee guarGuarantee : guarGuaranteeList) {
                String cusId = guarGuarantee.getCusId();
                String assureName = guarGuarantee.getAssureName();
                boolean flag = false;
                if (null != lmtGrrLimitEvals && lmtGrrLimitEvals.size() > 0) {
                    for (LmtGrrLimitEval lmtGrrLimitEval : lmtGrrLimitEvals) {
                        //获取保证人客户ID
                        String guarCusId = lmtGrrLimitEval.getGuarCusId();
                        if (cusId.equals(guarCusId)) {
                            flag = true;
                            break;
                        }
                    }
                }
                if (!flag) {
                    //新增数据
                    LmtGrrLimitEval lmt = new LmtGrrLimitEval();
                    lmt.setSerno(serno);
                    lmt.setGuarCusId(cusId);
                    lmt.setGuarCusName(assureName);
                    User userInfo = SessionUtils.getUserInformation();
                    lmt.setInputId(userInfo.getLoginCode());
                    lmt.setInputBrId(userInfo.getOrg().getCode());
                    lmt.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmt.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                    lmt.setOprType("01");
                    lmt.setPkId(StringUtils.uuid(true));
                    count += lmtGrrLimitEvalMapper.insertSelective(lmt);
                }
            }
        }
        return count;
    }
}
