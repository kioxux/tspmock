/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyAppList
 * @类描述: doc_destroy_app_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-26 10:11:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_destroy_app_list")
public class DocDestroyAppList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 档案销毁流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DDAL_SERNO")
	private String ddalSerno;
	
	/** 销毁户数 **/
	@Column(name = "DESTROY_CUS", unique = false, nullable = true, length = 5)
	private String destroyCus;
	
	/** 销毁状态 **/
	@Column(name = "DESTROY_STATUS", unique = false, nullable = true, length = 5)
	private String destroyStatus;
	
	/** 销毁日期 **/
	@Column(name = "DESTROY_DATE", unique = false, nullable = true, length = 10)
	private String destroyDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 销毁类型 **/
	@Column(name = "DESTROY_TYPE", unique = false, nullable = true, length = 5)
	private String destroyType;
	
	
	/**
	 * @param ddalSerno
	 */
	public void setDdalSerno(String ddalSerno) {
		this.ddalSerno = ddalSerno;
	}
	
    /**
     * @return ddalSerno
     */
	public String getDdalSerno() {
		return this.ddalSerno;
	}
	
	/**
	 * @param destroyCus
	 */
	public void setDestroyCus(String destroyCus) {
		this.destroyCus = destroyCus;
	}
	
    /**
     * @return destroyCus
     */
	public String getDestroyCus() {
		return this.destroyCus;
	}
	
	/**
	 * @param destroyStatus
	 */
	public void setDestroyStatus(String destroyStatus) {
		this.destroyStatus = destroyStatus;
	}
	
    /**
     * @return destroyStatus
     */
	public String getDestroyStatus() {
		return this.destroyStatus;
	}
	
	/**
	 * @param destroyDate
	 */
	public void setDestroyDate(String destroyDate) {
		this.destroyDate = destroyDate;
	}
	
    /**
     * @return destroyDate
     */
	public String getDestroyDate() {
		return this.destroyDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param destroyType
	 */
	public void setDestroyType(String destroyType) {
		this.destroyType = destroyType;
	}
	
    /**
     * @return destroyType
     */
	public String getDestroyType() {
		return this.destroyType;
	}


}