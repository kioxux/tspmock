/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJqd
 * @类描述: rpt_spd_anys_jqd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:02:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jqd")
public class RptSpdAnysJqd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 主要供应商及销售客户简介 **/
	@Column(name = "MAIN_CUSTOMER_INTR", unique = false, nullable = true, length = 65535)
	private String mainCustomerIntr;
	
	/** 主要供应商及销售客户情况分析 **/
	@Column(name = "MAIN_CUSTOMER_ANYS", unique = false, nullable = true, length = 65535)
	private String mainCustomerAnys;
	
	/** 主要产品成本结构情况分析 **/
	@Column(name = "MAIN_PRODUCT_ANYS", unique = false, nullable = true, length = 65535)
	private String mainProductAnys;
	
	/** 财务情况 **/
	@Column(name = "FINA_SITU", unique = false, nullable = true, length = 65535)
	private String finaSitu;
	
	/** 商圈/商会/园区类销售收入属于层级 **/
	@Column(name = "STORE_SALE_LEVEL", unique = false, nullable = true, length = 200)
	private String storeSaleLevel;
	
	/** 商圈/商会/园区类销售收入类似客户的市场占比 **/
	@Column(name = "STORE_SALE_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal storeSalePrec;
	
	/** 商圈/商会/园区类经营年限属于层级 **/
	@Column(name = "STORE_SALE_YEAR_LEVEL", unique = false, nullable = true, length = 200)
	private String storeSaleYearLevel;
	
	/** 商圈/商会/园区类经营年限类似客户的市场占比 **/
	@Column(name = "STORE_SALE_YEAR_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal storeSaleYearPrec;
	
	/** 商圈/商会/园区类家庭净资产属于层级 **/
	@Column(name = "STORE_FAMILY_LEVEL", unique = false, nullable = true, length = 200)
	private String storeFamilyLevel;
	
	/** 商圈/商会/园区类家庭净资产类似客户的市场占比 **/
	@Column(name = "STORE_FAMILY_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal storeFamilyPrec;
	
	/** 核心企业供应链类销售收入属于层级 **/
	@Column(name = "ENTERPRISE_SALE_LEVEL", unique = false, nullable = true, length = 200)
	private String enterpriseSaleLevel;
	
	/** 核心企业供应链类销售收入类似客户的市场占比 **/
	@Column(name = "ENTERPRISE_SALE_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal enterpriseSalePrec;
	
	/** 核心企业供应链类经营年限属于层级 **/
	@Column(name = "ENTERPRISE_SALE_YEAR_LEVEL", unique = false, nullable = true, length = 200)
	private String enterpriseSaleYearLevel;
	
	/** 核心企业供应链类经营年限类似客户的市场占比 **/
	@Column(name = "ENTERPRISE_SALE_YEAR_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal enterpriseSaleYearPrec;
	
	/** 核心企业供应链类家庭净资产属于层级 **/
	@Column(name = "ENTERPRISE_FAMILY_LEVEL", unique = false, nullable = true, length = 200)
	private String enterpriseFamilyLevel;
	
	/** 核心企业供应链类家庭净资产类似客户的市场占比 **/
	@Column(name = "ENTERPRISE_FAMILY_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal enterpriseFamilyPrec;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainCustomerIntr
	 */
	public void setMainCustomerIntr(String mainCustomerIntr) {
		this.mainCustomerIntr = mainCustomerIntr;
	}
	
    /**
     * @return mainCustomerIntr
     */
	public String getMainCustomerIntr() {
		return this.mainCustomerIntr;
	}
	
	/**
	 * @param mainCustomerAnys
	 */
	public void setMainCustomerAnys(String mainCustomerAnys) {
		this.mainCustomerAnys = mainCustomerAnys;
	}
	
    /**
     * @return mainCustomerAnys
     */
	public String getMainCustomerAnys() {
		return this.mainCustomerAnys;
	}
	
	/**
	 * @param mainProductAnys
	 */
	public void setMainProductAnys(String mainProductAnys) {
		this.mainProductAnys = mainProductAnys;
	}
	
    /**
     * @return mainProductAnys
     */
	public String getMainProductAnys() {
		return this.mainProductAnys;
	}
	
	/**
	 * @param finaSitu
	 */
	public void setFinaSitu(String finaSitu) {
		this.finaSitu = finaSitu;
	}
	
    /**
     * @return finaSitu
     */
	public String getFinaSitu() {
		return this.finaSitu;
	}
	
	/**
	 * @param storeSaleLevel
	 */
	public void setStoreSaleLevel(String storeSaleLevel) {
		this.storeSaleLevel = storeSaleLevel;
	}
	
    /**
     * @return storeSaleLevel
     */
	public String getStoreSaleLevel() {
		return this.storeSaleLevel;
	}
	
	/**
	 * @param storeSalePrec
	 */
	public void setStoreSalePrec(java.math.BigDecimal storeSalePrec) {
		this.storeSalePrec = storeSalePrec;
	}
	
    /**
     * @return storeSalePrec
     */
	public java.math.BigDecimal getStoreSalePrec() {
		return this.storeSalePrec;
	}
	
	/**
	 * @param storeSaleYearLevel
	 */
	public void setStoreSaleYearLevel(String storeSaleYearLevel) {
		this.storeSaleYearLevel = storeSaleYearLevel;
	}
	
    /**
     * @return storeSaleYearLevel
     */
	public String getStoreSaleYearLevel() {
		return this.storeSaleYearLevel;
	}
	
	/**
	 * @param storeSaleYearPrec
	 */
	public void setStoreSaleYearPrec(java.math.BigDecimal storeSaleYearPrec) {
		this.storeSaleYearPrec = storeSaleYearPrec;
	}
	
    /**
     * @return storeSaleYearPrec
     */
	public java.math.BigDecimal getStoreSaleYearPrec() {
		return this.storeSaleYearPrec;
	}
	
	/**
	 * @param storeFamilyLevel
	 */
	public void setStoreFamilyLevel(String storeFamilyLevel) {
		this.storeFamilyLevel = storeFamilyLevel;
	}
	
    /**
     * @return storeFamilyLevel
     */
	public String getStoreFamilyLevel() {
		return this.storeFamilyLevel;
	}
	
	/**
	 * @param storeFamilyPrec
	 */
	public void setStoreFamilyPrec(java.math.BigDecimal storeFamilyPrec) {
		this.storeFamilyPrec = storeFamilyPrec;
	}
	
    /**
     * @return storeFamilyPrec
     */
	public java.math.BigDecimal getStoreFamilyPrec() {
		return this.storeFamilyPrec;
	}
	
	/**
	 * @param enterpriseSaleLevel
	 */
	public void setEnterpriseSaleLevel(String enterpriseSaleLevel) {
		this.enterpriseSaleLevel = enterpriseSaleLevel;
	}
	
    /**
     * @return enterpriseSaleLevel
     */
	public String getEnterpriseSaleLevel() {
		return this.enterpriseSaleLevel;
	}
	
	/**
	 * @param enterpriseSalePrec
	 */
	public void setEnterpriseSalePrec(java.math.BigDecimal enterpriseSalePrec) {
		this.enterpriseSalePrec = enterpriseSalePrec;
	}
	
    /**
     * @return enterpriseSalePrec
     */
	public java.math.BigDecimal getEnterpriseSalePrec() {
		return this.enterpriseSalePrec;
	}
	
	/**
	 * @param enterpriseSaleYearLevel
	 */
	public void setEnterpriseSaleYearLevel(String enterpriseSaleYearLevel) {
		this.enterpriseSaleYearLevel = enterpriseSaleYearLevel;
	}
	
    /**
     * @return enterpriseSaleYearLevel
     */
	public String getEnterpriseSaleYearLevel() {
		return this.enterpriseSaleYearLevel;
	}
	
	/**
	 * @param enterpriseSaleYearPrec
	 */
	public void setEnterpriseSaleYearPrec(java.math.BigDecimal enterpriseSaleYearPrec) {
		this.enterpriseSaleYearPrec = enterpriseSaleYearPrec;
	}
	
    /**
     * @return enterpriseSaleYearPrec
     */
	public java.math.BigDecimal getEnterpriseSaleYearPrec() {
		return this.enterpriseSaleYearPrec;
	}
	
	/**
	 * @param enterpriseFamilyLevel
	 */
	public void setEnterpriseFamilyLevel(String enterpriseFamilyLevel) {
		this.enterpriseFamilyLevel = enterpriseFamilyLevel;
	}
	
    /**
     * @return enterpriseFamilyLevel
     */
	public String getEnterpriseFamilyLevel() {
		return this.enterpriseFamilyLevel;
	}
	
	/**
	 * @param enterpriseFamilyPrec
	 */
	public void setEnterpriseFamilyPrec(java.math.BigDecimal enterpriseFamilyPrec) {
		this.enterpriseFamilyPrec = enterpriseFamilyPrec;
	}
	
    /**
     * @return enterpriseFamilyPrec
     */
	public java.math.BigDecimal getEnterpriseFamilyPrec() {
		return this.enterpriseFamilyPrec;
	}


}