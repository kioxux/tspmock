/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtReplyAccOperAppSubDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccOperAppSubMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:27:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccOperAppSubService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyAccOperAppSubService.class);

    @Resource
    private LmtReplyAccOperAppSubMapper lmtReplyAccOperAppSubMapper;

    @Autowired
    private LmtReplyAccOperAppSubPrdService lmtReplyAccOperAppSubPrdService;

    @Autowired
    private LmtReplyAccOperAppService lmtReplyAccOperAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyAccOperAppSub selectByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyAccOperAppSub> selectAll(QueryModel model) {
        List<LmtReplyAccOperAppSub> records = (List<LmtReplyAccOperAppSub>) lmtReplyAccOperAppSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyAccOperAppSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccOperAppSub> list = lmtReplyAccOperAppSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtReplyAccOperAppSub record) {
        return lmtReplyAccOperAppSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyAccOperAppSub record) {
        return lmtReplyAccOperAppSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReplyAccOperAppSub record) {
        return lmtReplyAccOperAppSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyAccOperAppSub record) {
        return lmtReplyAccOperAppSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyAccOperAppSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据授信申请流水号查询对应所有授信分项
     * @参数与返回说明:
     * @算法描述: 无
     * @更新人: ywl
     */
    public List<LmtReplyAccOperAppSub> selectByParams(Map queryMap) {
        return lmtReplyAccOperAppSubMapper.selectByParams(queryMap);
    }

    /**
     * @方法名称: queryBySerno
     * @方法描述: 根据授信申请流水号查询对应所有授信分项
     * @参数与返回说明:
     * @算法描述: 无
     * @更新人: ywl
     */
    public List<LmtReplyAccOperAppSub> queryBySerno(String serno) {
        return lmtReplyAccOperAppSubMapper.queryBySerno(serno);
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @创建人: yangwl
     * @创建时间: 2021-5-20
     * @算法描述:
     */

    public Map getSubAndPrd(String serno) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtReplyAccOperAppSubDto> lmtReplyAccOperAppSubDtoList = new ArrayList<>();

        try {
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("serno", serno);
            List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubList = lmtReplyAccOperAppSubMapper.selectByParams(queryMap);
            if (lmtReplyAccOperAppSubList != null) {
                for (LmtReplyAccOperAppSub lmtReplyAccOperAppSub : lmtReplyAccOperAppSubList) {
                    LmtReplyAccOperAppSubDto lmtReplyAccOperAppSubDto = new LmtReplyAccOperAppSubDto();
                    lmtReplyAccOperAppSubDto.setPkId(lmtReplyAccOperAppSub.getPkId());
                    lmtReplyAccOperAppSubDto.setAccSubNo(lmtReplyAccOperAppSub.getAccSubNo());
                    lmtReplyAccOperAppSubDto.setLmtBizTypeName(lmtReplyAccOperAppSub.getAccSubName());
                    lmtReplyAccOperAppSubDto.setIsPreLmt(lmtReplyAccOperAppSub.getIsPreLmt());
                    lmtReplyAccOperAppSubDto.setGuarMode(lmtReplyAccOperAppSub.getGuarMode());
                    lmtReplyAccOperAppSubDto.setLmtAmt(lmtReplyAccOperAppSub.getLmtAmt());
                    HashMap<String, String> queryPrdMap = new HashMap<>();
                    queryPrdMap.put("subSerno", lmtReplyAccOperAppSub.getSubSerno());
                    List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrdList = lmtReplyAccOperAppSubPrdService.selectByParams(queryPrdMap);
                    // 将每一个分项明细下的分项流水号的值 置为分项明细流水号 保证前端流水号字段显示正常
                    List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrdListNew = new ArrayList<LmtReplyAccOperAppSubPrd>();
                    if(lmtReplyAccOperAppSubPrdList.size()>0){
                        for(LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrdList){
                            lmtReplyAccOperAppSubPrd.setSubSerno(lmtReplyAccOperAppSubPrd.getSubPrdSerno());
                            lmtReplyAccOperAppSubPrdListNew.add(lmtReplyAccOperAppSubPrd);
                        }
                    }

                    lmtReplyAccOperAppSubDto.setChildren(lmtReplyAccOperAppSubPrdListNew);
                    lmtReplyAccOperAppSubDtoList.add(lmtReplyAccOperAppSubDto);
                 }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtReplyAccOperAppSubDtoList", lmtReplyAccOperAppSubDtoList);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @函数名称:getsubandprdbygrpSerno
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @创建人: yangwl
     * @创建时间: 2021-5-20
     * @算法描述:
     */

    public Map getSubandPrdByGrpSerno(String grpSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtReplyAccOperAppSubDto> lmtReplyAccOperAppSubDtoList = new ArrayList<>();

        try {
            if (StringUtils.isBlank(grpSerno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("grpSerno", grpSerno);
            List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubList = lmtReplyAccOperAppSubMapper.selectByParams(queryMap);
            if (lmtReplyAccOperAppSubList.size() > 0) {
                for (LmtReplyAccOperAppSub lmtReplyAccOperAppSub : lmtReplyAccOperAppSubList) {
                    LmtReplyAccOperAppSubDto lmtReplyAccOperAppSubDto = new LmtReplyAccOperAppSubDto();
                    BeanUtils.copyProperties(lmtReplyAccOperAppSub ,lmtReplyAccOperAppSubDto);
                    // 获取  成员客户名称
                    LmtReplyAccOperApp lmtReplyAccOperApp = lmtReplyAccOperAppService.queryLmtReplyAccOperAppBySerno(lmtReplyAccOperAppSub.getSerno());
                    lmtReplyAccOperAppSubDto.setCusId(lmtReplyAccOperApp.getCusId());
                    lmtReplyAccOperAppSubDto.setCusName(lmtReplyAccOperApp.getCusName());
                    // 授信品种名称比较特殊 选取品种表中的授信品种字段名称
                    lmtReplyAccOperAppSubDto.setLmtBizTypeName(lmtReplyAccOperAppSub.getAccSubName());
                    HashMap<String, String> queryPrdMap = new HashMap<>();
                    queryPrdMap.put("subSerno", lmtReplyAccOperAppSub.getSubSerno());
                    List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrdList = lmtReplyAccOperAppSubPrdService.selectByParams(queryPrdMap);
                    // 将每一个分项明细下的分项流水号的值 置为分项明细流水号 保证前端流水号字段显示正常
                    List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrdListNew = new ArrayList<LmtReplyAccOperAppSubPrd>();
                    if(lmtReplyAccOperAppSubPrdList.size()>0){
                        for(LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrdList){
                            lmtReplyAccOperAppSubPrd.setSubSerno(lmtReplyAccOperAppSubPrd.getSubPrdSerno());
                            lmtReplyAccOperAppSubPrdListNew.add(lmtReplyAccOperAppSubPrd);
                        }
                        lmtReplyAccOperAppSubDto.setChildren(lmtReplyAccOperAppSubPrdListNew);
                    }
                    lmtReplyAccOperAppSubDtoList.add(lmtReplyAccOperAppSubDto);
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtReplyAccOperAppSubDtoList", lmtReplyAccOperAppSubDtoList);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }
}
