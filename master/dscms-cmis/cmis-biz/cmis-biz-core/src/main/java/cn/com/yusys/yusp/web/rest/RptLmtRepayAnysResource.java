/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnys;
import cn.com.yusys.yusp.service.RptLmtRepayAnysService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-23 18:37:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanys")
public class RptLmtRepayAnysResource {
    @Autowired
    private RptLmtRepayAnysService rptLmtRepayAnysService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnys>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnys> list = rptLmtRepayAnysService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnys>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnys>> index(QueryModel queryModel) {
        List<RptLmtRepayAnys> list = rptLmtRepayAnysService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnys>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptLmtRepayAnys> show(@PathVariable("serno") String serno) {
        RptLmtRepayAnys rptLmtRepayAnys = rptLmtRepayAnysService.selectByPrimaryKey(serno);
        return new ResultDto<RptLmtRepayAnys>(rptLmtRepayAnys);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnys> create(@RequestBody RptLmtRepayAnys rptLmtRepayAnys) throws URISyntaxException {
        rptLmtRepayAnysService.insert(rptLmtRepayAnys);
        return new ResultDto<RptLmtRepayAnys>(rptLmtRepayAnys);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnys rptLmtRepayAnys) throws URISyntaxException {
        int result = rptLmtRepayAnysService.update(rptLmtRepayAnys);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptLmtRepayAnysService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptLmtRepayAnysService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询
     * @param map  RptLmtRepayAnys rptLmtRepayAnys
     * @return
     */
    @ApiOperation("根据流水号查看可行性分析和第一还款能力分析")
    @PostMapping("/selectBySerno")
    protected ResultDto<RptLmtRepayAnys> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptLmtRepayAnys>(rptLmtRepayAnysService.selectByPrimaryKey(serno));
    }

    /**
     * 保存信息
     */
    @PostMapping("/saveAnys")
    protected ResultDto<Integer> saveAnys(@RequestBody RptLmtRepayAnys rptLmtRepayAnys){
        return new ResultDto<Integer>(rptLmtRepayAnysService.saveAnys(rptLmtRepayAnys));
    }
}
