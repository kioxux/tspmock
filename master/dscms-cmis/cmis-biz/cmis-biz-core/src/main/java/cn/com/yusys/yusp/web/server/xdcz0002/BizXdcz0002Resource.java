package cn.com.yusys.yusp.web.server.xdcz0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0002.req.Xdcz0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.resp.Xdcz0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:电子保函开立
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0002:电子保函注销")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0002Resource.class);
    @Autowired
    private cn.com.yusys.yusp.service.server.xdcz0002.Xdcz0002Service xdcz0002Service;

    /**
     * 交易码：xdcz0002
     * 交易描述：电子保函开立
     *
     * @param xdcz0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子保函注销")
    @PostMapping("/xdcz0002")
    protected @ResponseBody
    ResultDto<Xdcz0002DataRespDto> xdcz0002(@Validated @RequestBody Xdcz0002DataReqDto xdcz0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataReqDto));
        Xdcz0002DataRespDto xdcz0002DataRespDto = new Xdcz0002DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0002DataRespDto> xdcz0002DataResultDto = new ResultDto<>();

        try {
            // 从xdcz0002DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataReqDto));
            xdcz0002DataRespDto = xdcz0002Service.xdcz0002(xdcz0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataRespDto));

            // 封装xdcz0002DataResultDto中正确的返回码和返回信息
            xdcz0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, e.getMessage());
            // 封装xdcz0002DataResultDto中异常返回码和返回信息
            xdcz0002DataResultDto.setCode(e.getErrorCode());
            xdcz0002DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, e.getMessage());
            // 封装xdcz0002DataResultDto中异常返回码和返回信息
            xdcz0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0002DataRespDto到xdcz0002DataResultDto中
        xdcz0002DataResultDto.setData(xdcz0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataRespDto));
        return xdcz0002DataResultDto;
    }
}
