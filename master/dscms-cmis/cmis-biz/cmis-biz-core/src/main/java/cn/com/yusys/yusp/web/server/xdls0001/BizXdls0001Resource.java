package cn.com.yusys.yusp.web.server.xdls0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0001.req.Xdls0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0001.resp.Xdls0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdls0001.Xdls0001Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:房贷要素查询
 *
 * @author code-generator
 * @version 1.0
 */
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0001Resource.class);

    @Autowired
    private Xdls0001Service xdls0001Service;

    /**
     * 交易码：xdls0001
     * 交易描述：房贷要素查询
     *
     * @param xdls0001DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdls0001")
    protected @ResponseBody
    ResultDto<Xdls0001DataRespDto> xdls0001(@Validated @RequestBody Xdls0001DataReqDto xdls0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, JSON.toJSONString(xdls0001DataReqDto));
        Xdls0001DataRespDto xdls0001DataRespDto = new Xdls0001DataRespDto();// 响应Dto:房贷要素查询
        ResultDto<Xdls0001DataRespDto> xdls0001DataResultDto = new ResultDto<>();
        try {
             xdls0001DataRespDto = xdls0001Service.xdls0001(xdls0001DataReqDto);
            // 封装xdls0001DataResultDto中正确的返回码和返回信息
            xdls0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, e.getMessage());
            // 封装xdls0001DataResultDto中异常返回码和返回信息
            xdls0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdls0001DataRespDto到xdls0001DataResultDto中
        xdls0001DataResultDto.setData(xdls0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, JSON.toJSONString(xdls0001DataResultDto));
        return xdls0001DataResultDto;
    }
}
