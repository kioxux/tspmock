/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.CreditCardLargeJudgInifoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardLargeJudgInifoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-25 21:08:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardLargeJudgInifoService {

    @Autowired
    private CreditCardLargeJudgInifoMapper creditCardLargeJudgInifoMapper;
    @Autowired
    private CreditCardLargeLoanAppService CreditCardLargeLoanAppService;
    @Autowired
    private CreditCardLargeAmtInfoService creditCardAdjustmentAmtInfo;
    @Autowired
    private CreditCtrLoanContService creditCtrLoanContService;
    @Autowired
    private CreditCardLargeJudgInifoService creditCardLargeJudgInifoService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardLargeJudgInifo selectByPrimaryKey(String pkId) {
        return creditCardLargeJudgInifoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardLargeJudgInifo> selectAll(QueryModel model) {
        List<CreditCardLargeJudgInifo> records = (List<CreditCardLargeJudgInifo>) creditCardLargeJudgInifoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardLargeJudgInifo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardLargeJudgInifo> list = creditCardLargeJudgInifoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardLargeJudgInifo record) {
        return creditCardLargeJudgInifoMapper.insert(record);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int save(CreditCardLargeJudgInifo record) {
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(record.getSerno());
        if(CreditCardLargeLoanApp == null){
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "未找到该大额分期申请！");
        }
        if(StringUtils.isNullOrEmpty(record.getApprovePostFlag())){
            throw BizException.error(null, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, "请传入审批岗位标识！");
        }else if("01".equals(record.getApprovePostFlag())){//如果审批岗位为二岗
            CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),record.getApprovePostFlag());
            if(creditCardLargeJudgInifo != null){
                creditCardLargeJudgInifo.setSerno(record.getSerno());
                creditCardLargeJudgInifo.setApprovePostFlag("01");
                creditCardLargeJudgInifo.setApproveAmt(record.getApproveAmt());
                creditCardLargeJudgInifo.setLoanFeeRate(record.getLoanFeeRate());
                creditCardLargeJudgInifo.setLoanTerm(record.getLoanTerm());
                creditCardLargeJudgInifo.setYearInterestRate(record.getYearInterestRate());
                creditCardLargeJudgInifo.setUpdateTime(record.getUpdateTime());
                return creditCardLargeJudgInifoMapper.updateByPrimaryKeySelective(creditCardLargeJudgInifo);
            }else {
                record.setPkId(UUID.randomUUID().toString());
                return creditCardLargeJudgInifoMapper.insert(record);
            }
        }else if("00".equals(record.getApprovePostFlag())){
            CreditCardLargeJudgInifo creditCardLargeJudgInifoTwo = creditCardLargeJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),"01");
            if(creditCardLargeJudgInifoTwo != null){
                creditCardLargeJudgInifoTwo.setApproveAmt(record.getApproveAmt());
                creditCardLargeJudgInifoTwo.setLoanFeeRate(record.getLoanFeeRate());
                creditCardLargeJudgInifoTwo.setLoanTerm(record.getLoanTerm());
                creditCardLargeJudgInifoTwo.setYearInterestRate(record.getYearInterestRate());
                creditCardLargeJudgInifoMapper.updateByPrimaryKeySelective(creditCardLargeJudgInifoTwo);
            }
            CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),record.getApprovePostFlag());
            if(creditCardLargeJudgInifo != null){
                creditCardLargeJudgInifo.setApproveConclusion(record.getApproveConclusion());
                creditCardLargeJudgInifo.setApproveAmt(record.getApproveAmt());
                creditCardLargeJudgInifo.setLoanFeeRate(record.getLoanFeeRate());
                creditCardLargeJudgInifo.setLoanTerm(record.getLoanTerm());
                creditCardLargeJudgInifo.setYearInterestRate(record.getYearInterestRate());
                creditCardLargeJudgInifo.setReturnReason(record.getReturnReason());
                creditCardLargeJudgInifo.setRefuseReason(record.getRefuseReason());
                creditCardLargeJudgInifo.setJudgRemark(record.getJudgRemark());
                return creditCardLargeJudgInifoMapper.updateByPrimaryKeySelective(creditCardLargeJudgInifo);
            }else {
                record.setPkId(UUID.randomUUID().toString());
                return creditCardLargeJudgInifoMapper.insert(record);
            }
        }else{
            CreditCardLargeJudgInifo creditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),record.getApprovePostFlag());
            if(creditCardLargeJudgInifo != null){
                return creditCardLargeJudgInifoMapper.updateByPrimaryKeySelective(record);
            }else {
                return creditCardLargeJudgInifoMapper.insert(record);
            }
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardLargeJudgInifo record) {
        return creditCardLargeJudgInifoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardLargeJudgInifo record) {
        return creditCardLargeJudgInifoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardLargeJudgInifo record) {
        return creditCardLargeJudgInifoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardLargeJudgInifoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardLargeJudgInifoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据流水号和审批人标识查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditCardLargeJudgInifo selectBySernoAndApprovePostFlag(String serno, String approvePostFlag) {
        return creditCardLargeJudgInifoMapper.selectBySernoAndApprovePostFlag(serno,approvePostFlag);
    }

    public CreditCardLargeJudgInifo selectFinalInfo(String serno) {
        Map map = new HashMap();
        map.put("serno",serno);
        return creditCardLargeJudgInifoMapper.selectFinalInfo(map);
    }

    public List<CreditCardLargeJudgInifo> selectBySerno(String serno) {
        return creditCardLargeJudgInifoMapper.selectBySerno(serno);
    }
}
