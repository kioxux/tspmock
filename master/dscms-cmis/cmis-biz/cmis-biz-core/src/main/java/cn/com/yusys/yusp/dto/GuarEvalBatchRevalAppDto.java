package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalBatchRevalApp
 * @类描述: guar_eval_batch_reval_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-30 16:59:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalBatchRevalAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 批量申请流水号 **/
	private String batchSerno;
	
	/** 押品统一编号 **/
	private java.math.BigDecimal evalFloatPercent;
	
	/** 流程审批状态 **/
	private String approveStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	
	/**
	 * @param batchSerno
	 */
	public void setBatchSerno(String batchSerno) {
		this.batchSerno = batchSerno == null ? null : batchSerno.trim();
	}
	
    /**
     * @return BatchSerno
     */	
	public String getBatchSerno() {
		return this.batchSerno;
	}
	
	/**
	 * @param evalFloatPercent
	 */
	public void setEvalFloatPercent(java.math.BigDecimal evalFloatPercent) {
		this.evalFloatPercent = evalFloatPercent;
	}
	
    /**
     * @return EvalFloatPercent
     */	
	public java.math.BigDecimal getEvalFloatPercent() {
		return this.evalFloatPercent;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}


}