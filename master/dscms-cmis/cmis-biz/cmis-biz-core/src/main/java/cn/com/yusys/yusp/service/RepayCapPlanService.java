/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import feign.QueryMap;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.RepayCapPlanMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RepayCapPlanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zhanyb
 * @创建时间: 2021-05-06 17:42:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RepayCapPlanService {

    @Resource
    private RepayCapPlanMapper repayCapPlanMapper;

    @Autowired
    private BizInvestCommonService bizInvestCommonService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    private static final Logger log = LoggerFactory.getLogger(RepayCapPlanService.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RepayCapPlan selectByPrimaryKey(String pkId) {
        return repayCapPlanMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RepayCapPlan> selectAll(QueryModel model) {
        List<RepayCapPlan> records = (List<RepayCapPlan>) repayCapPlanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RepayCapPlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("repayDate asc");
        List<RepayCapPlan> list = repayCapPlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RepayCapPlan record) {
        return repayCapPlanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RepayCapPlan record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        return repayCapPlanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RepayCapPlan record) {
        return repayCapPlanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RepayCapPlan record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return repayCapPlanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return repayCapPlanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return repayCapPlanMapper.deleteByIds(ids);
    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-05-05 15:35
     * @注释 还本计划分页查询
     */
    public List<RepayCapPlan> queryRepaycapplan(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("terms");
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<RepayCapPlan> list = repayCapPlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     *还本计划,新增
     * @param repayCapPlan
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveRepayCapPlan(RepayCapPlan repayCapPlan) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (repayCapPlan == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                repayCapPlan.setInputId(userInfo.getLoginCode());
                repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                repayCapPlan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                repayCapPlan.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
            }
            Map seqMap = new HashMap();
            //String serno = StringUtils.uuid(true);
            // sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ,seqMap);
            String serno = repayCapPlan.getSerno();
            repayCapPlan.setPkId(StringUtils.uuid(true));
            repayCapPlan.setOprType(CmisBizConstants.OPR_TYPE_01);
            int insertCount = repayCapPlanMapper.insertSelective(repayCapPlan);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("serno",serno);
            log.info("还本计划"+serno+"-保存成功！");
        }catch(YuspException e){
            log.error("还本计划新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("还本计划异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     *还本计划,修改
     * @param repayCapPlan
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateRepayCapPlan(RepayCapPlan repayCapPlan) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (repayCapPlan == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                repayCapPlan.setUpdId(userInfo.getLoginCode());
                repayCapPlan.setUpdBrId(userInfo.getOrg().getCode());
                repayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            Map seqMap = new HashMap();
            int updCount = repayCapPlanMapper.updateByPrimaryKeySelective(repayCapPlan);
            if(updCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
        }catch(YuspException e){
            log.error("还本计划修改异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("还本计划异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 还本计划逻辑删除
     * @param condition
     * @return
     */
    public Integer deleteLogicRepayCapPlan(Map condition) {
        String pkId = (String) condition.get("pkId");
        RepayCapPlan repayCapPlan = new RepayCapPlan();
        repayCapPlan.setPkId(pkId);
        repayCapPlan.setOprType(CmisBizConstants.OPR_TYPE_02);
        return updateSelective(repayCapPlan);
    }

    /**
     * 还款计划新增
     * @param repayCapPlan
     * @return
     */
    public Integer insertRepayCapPlan(RepayCapPlan repayCapPlan){
        bizInvestCommonService.initInsertDomainProperties(repayCapPlan);
        return insert(repayCapPlan);
    }

    /**
     * 还款计划列表
     * @param queryModel
     * @return
     */
    public List<RepayCapPlan> selectListByPageData(QueryModel queryModel){
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort("terms asc");
        return selectByModel(queryModel);
    }

    public List<RepayCapPlan> getAppSubPrdRepay(String serno){
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.queryAllLmtAppSubPrdBySerno(serno);
        List<RepayCapPlan> result = new ArrayList<>();
        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
            RepayCapPlan repayCapPlan = repayCapPlanMapper.selectByPrimaryKey(lmtAppSubPrd.getSubPrdSerno());
            result.add(repayCapPlan);
        }
        return result;
    }

    /**
     * 根据集团申请流水号查询还款计划
     * @param grpSerno
     * @return
     */
    public ArrayList<RepayCapPlan> queryRepayCapPlanByGrpSerno(String grpSerno) {
        return repayCapPlanMapper.queryRepayCapPlanByGrpSerno(grpSerno);
    }

    /**
     * 根据分项品种编号获取还款计划
     * @param map
     * @return
     */
    public List<RepayCapPlan> getRepayCapPlanData(Map map) throws Exception {
        // TODO: 2021/8/10  暂时先取申请中的还本计划，实际应该取的是批复中的还本计划，申请中的还本计划暂未落到批复中
        List<RepayCapPlan> repayCapPlanList = new ArrayList<>();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",map.get("serno"));
        repayCapPlanList = this.selectListByPageData(queryModel);
        if(CollectionUtils.nonEmpty(repayCapPlanList)){
            return repayCapPlanList;
        }else{
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo((String) map.get("accSubPrdNo"));
            if(Objects.isNull(lmtReplyAccSubPrd)){
                return repayCapPlanList;
            }
            QueryModel queryData = new QueryModel();
            queryData.addCondition("serno",lmtReplyAccSubPrd.getReplySubPrdSerno());
            repayCapPlanList = this.selectByModel(queryData);
            if(CollectionUtils.nonEmpty(repayCapPlanList)){
                List list = new ArrayList();
                for (RepayCapPlan repayCapPlan : repayCapPlanList) {
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setSerno((String)map.get("serno"));
                    int insertCount = this.insert(repayCapPlan);
                    if(insertCount<=0){
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    list.add(repayCapPlan);
                }
                return list;
            }
            return repayCapPlanList;
        }
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */

    public boolean addOrUpdateAllTable(List<RepayCapPlan> list) {
        for(RepayCapPlan RepayCapPlan : list){
            // 判断当前行是否已存在
            RepayCapPlan record = selectByPrimaryKey(RepayCapPlan.getPkId());
            if(record != null){
                // 更新操作
                User userInfo = SessionUtils.getUserInformation();
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                RepayCapPlan.setUpdId(userInfo.getLoginCode());
                RepayCapPlan.setUpdBrId(userInfo.getOrg().getCode());
                RepayCapPlan.setUpdDate(nowDate);
                RepayCapPlan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                updateSelective(RepayCapPlan);
            }else{
                // 新增操作
                insertSelective(RepayCapPlan);
            }
        }
        return true;
    }

    /**
     * @方法名称: saveRepayPlan
     * @方法描述: 新增还款计划
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map saveRepayPlan(RepayCapPlan repayCapPlan) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{

            // 校验是否存在同期数还款计划
            int existNum = isExistRepayCapPlan(repayCapPlan);
            if(existNum>0){
                throw BizException.error(null, EcbEnum.ECB010087.key, EcbEnum.ECB010087.value);
            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date repayDate = format.parse(repayCapPlan.getRepayDate());
            Date nowDate = format.parse(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            if(repayDate.compareTo(nowDate) < 0){
                throw BizException.error(null, EcbEnum.ECB010088.key, EcbEnum.ECB010088.value);
            }
            // 数据操作标志为新增
            repayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            repayCapPlan.setPkId(UUID.randomUUID().toString());
            log.info(String.format("保存授信申请数据%s-获取当前登录用户数据", (Object) null));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                repayCapPlan.setInputId(userInfo.getLoginCode());
                repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                repayCapPlan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                repayCapPlan.setUpdId(userInfo.getLoginCode());
                repayCapPlan.setUpdBrId(userInfo.getOrg().getCode());
                repayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            int count = repayCapPlanMapper.insert(repayCapPlan);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw BizException.error(null, EcbEnum.CTR_EXCEPTION_DEF.key, EcbEnum.CTR_EXCEPTION_DEF.value);
            }

        } catch (YuspException e) {
//            rtnCode = e.getCode();
//            rtnMsg = e.getMsg();
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("保存还款计划数据出现异常！", e);
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "," + e.getMessage());
//            rtnCode = EpbEnum.EPB099999.key;
//            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    public int isExistRepayCapPlan(RepayCapPlan repayCapPlan){
        boolean isExistFlg = false;
        QueryModel model = new QueryModel();
        model.addCondition("serno",repayCapPlan.getSerno());
        model.addCondition("terms",repayCapPlan.getTerms());
        model.addCondition("oprType",CmisCommonConstants.OP_TYPE_01);
        List<RepayCapPlan> repayCapPlans = selectAll(model);
        return repayCapPlans.size();
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 跟据流水号进行查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RepayCapPlan> selectBySerno(String serno) {
        Map map = new HashMap();
        map.put("serno", serno);
        map.put("oprType", CmisCommonConstants.ADD_OPR);
        map.put("sort","terms asc");
        List<RepayCapPlan> list = repayCapPlanMapper.selectByParams(map);
        return list;
    }

    /**
     * @方法名称: deleteByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map deleteByPkId(RepayCapPlan repayCapPlan){
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{
            int count = repayCapPlanMapper.updateByPkId(repayCapPlan);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除还款计划情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    };

    /**
     * @方法名称: copyLmtRepayPlan
     * @方法描述: 复议，复审，变更时将原担保关系挂靠在新的授信分项流水号下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyLmtRepayPlan(String originSerno,String newSerno) {
        RepayCapPlan repayCapPlan = null;
        Map map = new HashMap();
        map.put("serno",originSerno);
        List<RepayCapPlan> list = repayCapPlanMapper.selectByParams(map);
        if(list != null && list.size() >0){
            for (int i = 0; i < list.size(); i++) {
                repayCapPlan = list.get(i);
                repayCapPlan.setSerno(newSerno);
                int count = repayCapPlanMapper.insert(repayCapPlan);
                if (count != 1){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RepayCapPlan> selectByIqpSerno( QueryModel queryModel) {
        return repayCapPlanMapper.selectByIqpSerno(queryModel);
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据授信申请流水号初始化查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map> selectByLmtSerno(QueryModel queryModel) {
        if(StringUtils.isBlank((String) queryModel.getCondition().get("serno"))){
            return new ArrayList<>();
        }
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno((String) queryModel.getCondition().get("serno"));
        if(lmtAppr == null){
            return new ArrayList<>();
        }
        List<Map> repayCapPlanList = this.selectRepayCapPlanListByApproveSerno(lmtAppr.getApproveSerno());
        return repayCapPlanList;
    }

    /**
     * @方法名称: selectDataByApprSerno
     * @参数与返回说明: 根据审批中的审批流水号查询数据
     * @算法描述: 无
     */

    public List<Map> selectDataByApprSerno(QueryModel queryModel) {
        if(StringUtils.isBlank((String) queryModel.getCondition().get("apprSerno"))){
            return new ArrayList<>();
        }
        List<Map> repayCapPlanList = this.selectRepayCapPlanListByApproveSerno((String) queryModel.getCondition().get("apprSerno"));
        return repayCapPlanList;
    }

    /**
     * @方法名称: selectRepayCapPlanListByApproveSerno
     * @方法描述: 根据授信申请流水号初始化查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    private List<Map> selectRepayCapPlanListByApproveSerno(String approveSerno) {
        return repayCapPlanMapper.selectRepayCapPlanListByApproveSerno(approveSerno);
    }


    /***
     * @param repayCapPlan
     * @return int
     * @author shenli
     * @date 2021-8-18 10:26
     * @version 1.0.0
     * @desc 从主键是否有值来判断是插入还是更新
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveRepayCapPlanRetail(RepayCapPlan repayCapPlan) {
        log.info("开始保存还款信息----业务编号：" + repayCapPlan.getSerno());
        String pkId = repayCapPlan.getPkId();
        int row = 0;
        try {
            if (pkId == null || "".equals(pkId)) {
                repayCapPlan.setPkId(UUID.randomUUID().toString());
                row = this.insert(repayCapPlan);
            } else {
                row = this.updateSelective(repayCapPlan);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            log.info("还款信息保存成功----业务编号：" + repayCapPlan.getSerno());
        }
        return row;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteBySerno(String serno) {
        return repayCapPlanMapper.deleteBySerno(serno);
    }

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据流水号更新信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateBySerno(QueryModel model) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            model.addCondition("updId", userInfo.getLoginCode()); // 当前用户号
            model.addCondition("updBrId", userInfo.getOrg().getCode()); // 当前用户机构
        }
        model.addCondition("updDate", DateUtils.getCurrDateStr());  // 当前日期
        model.addCondition("updateTime", DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 当前时间
        return repayCapPlanMapper.updateBySerno(model);
    }

    /**
     * 根据授信流水号查询还款计划
     * @param serno
     * @return
     */
    public List<Map> selectRapByLmtSerno(String serno){
        return repayCapPlanMapper.selectByLmtSerno(serno);
    }
    /**
     * 根据集团授信流水号查询还款计划
     * @param grpSerno
     * @return
     */
    public List<Map> selectByGrpSerno(String grpSerno){
        return repayCapPlanMapper.selectByGrpSerno(grpSerno);
    }

    /***
     * @param repayCapPlanList
     * @desc    批量插入
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public int batchSaveRepayCapPlanDetail(List<RepayCapPlan> repayCapPlanList) {
        int resNum = 0;
        if(CollectionUtils.nonEmpty(repayCapPlanList)){
            for(RepayCapPlan repayCapPlan : repayCapPlanList){
                resNum += saveRepayCapPlanRetail(repayCapPlan);
            }
        }
        return resNum;
    }
}