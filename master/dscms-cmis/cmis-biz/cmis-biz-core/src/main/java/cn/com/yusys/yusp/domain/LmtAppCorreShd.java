/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppCorreShd
 * @类描述: lmt_app_corre_shd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 18:43:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_corre_shd")
public class LmtAppCorreShd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 关联股东客户编号 **/
	@Column(name = "SHD_CUS_ID", unique = false, nullable = true, length = 30)
	private String shdCusId;
	
	/** 关联股东客户名称 **/
	@Column(name = "SHD_CUS_NAME", unique = false, nullable = true, length = 80)
	private String shdCusName;
	
	/** 持股金额 **/
	@Column(name = "SHD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal shdAmt;
	
	/** 持股比例 **/
	@Column(name = "SHD_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal shdPerc;
	
	/** 企业性质 **/
	@Column(name = "CORP_CHA", unique = false, nullable = true, length = 5)
	private String corpCha;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 2000)
	private String remark;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param shdCusId
	 */
	public void setShdCusId(String shdCusId) {
		this.shdCusId = shdCusId;
	}
	
    /**
     * @return shdCusId
     */
	public String getShdCusId() {
		return this.shdCusId;
	}
	
	/**
	 * @param shdCusName
	 */
	public void setShdCusName(String shdCusName) {
		this.shdCusName = shdCusName;
	}
	
    /**
     * @return shdCusName
     */
	public String getShdCusName() {
		return this.shdCusName;
	}
	
	/**
	 * @param shdAmt
	 */
	public void setShdAmt(java.math.BigDecimal shdAmt) {
		this.shdAmt = shdAmt;
	}
	
    /**
     * @return shdAmt
     */
	public java.math.BigDecimal getShdAmt() {
		return this.shdAmt;
	}
	
	/**
	 * @param shdPerc
	 */
	public void setShdPerc(java.math.BigDecimal shdPerc) {
		this.shdPerc = shdPerc;
	}
	
    /**
     * @return shdPerc
     */
	public java.math.BigDecimal getShdPerc() {
		return this.shdPerc;
	}
	
	/**
	 * @param corpCha
	 */
	public void setCorpCha(String corpCha) {
		this.corpCha = corpCha;
	}
	
    /**
     * @return corpCha
     */
	public String getCorpCha() {
		return this.corpCha;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}