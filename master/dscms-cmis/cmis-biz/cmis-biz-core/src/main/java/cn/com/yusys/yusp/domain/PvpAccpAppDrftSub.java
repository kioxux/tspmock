/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppDrftSub
 * @类描述: pvp_accp_app_drft_sub数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-11-09 15:29:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_accp_app_drft_sub")
public class PvpAccpAppDrftSub extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	@Generated(KeyConstants.UUID)
	private String pkId;

	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;

	/** 票据号 **/
	@Column(name = "DRFT_NO", unique = false, nullable = true, length = 40)
	private String drftNo;

	/** 银承出账流水号 **/
	@Column(name = "ACCP_PVP_SEQ", unique = false, nullable = true, length = 40)
	private String accpPvpSeq;

	/** 票面金额 **/
	@Column(name = "DRFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftAmt;

	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;

	/** 出票人 **/
	@Column(name = "DRWR", unique = false, nullable = true, length = 80)
	private String drwr;

	/** 收款人 **/
	@Column(name = "PYEE", unique = false, nullable = true, length = 80)
	private String pyee;

	/** 承兑人 **/
	@Column(name = "ACCPTR", unique = false, nullable = true, length = 80)
	private String accptr;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;

	/** 收款人账号 **/
	@Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 80)
	private String pyeeAccno;

	/** 收款人开户行名称 **/
	@Column(name = "PYEE_ACCTSVCR_NAME", unique = false, nullable = true, length = 80)
	private String pyeeAcctsvcrName;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param drftNo
	 */
	public void setDrftNo(String drftNo) {
		this.drftNo = drftNo;
	}

	/**
	 * @return drftNo
	 */
	public String getDrftNo() {
		return this.drftNo;
	}

	/**
	 * @param accpPvpSeq
	 */
	public void setAccpPvpSeq(String accpPvpSeq) {
		this.accpPvpSeq = accpPvpSeq;
	}

	/**
	 * @return accpPvpSeq
	 */
	public String getAccpPvpSeq() {
		return this.accpPvpSeq;
	}

	/**
	 * @param drftAmt
	 */
	public void setDrftAmt(java.math.BigDecimal drftAmt) {
		this.drftAmt = drftAmt;
	}

	/**
	 * @return drftAmt
	 */
	public java.math.BigDecimal getDrftAmt() {
		return this.drftAmt;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param drwr
	 */
	public void setDrwr(String drwr) {
		this.drwr = drwr;
	}

	/**
	 * @return drwr
	 */
	public String getDrwr() {
		return this.drwr;
	}

	/**
	 * @param pyee
	 */
	public void setPyee(String pyee) {
		this.pyee = pyee;
	}

	/**
	 * @return pyee
	 */
	public String getPyee() {
		return this.pyee;
	}

	/**
	 * @param accptr
	 */
	public void setAccptr(String accptr) {
		this.accptr = accptr;
	}

	/**
	 * @return accptr
	 */
	public String getAccptr() {
		return this.accptr;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param pyeeAccno
	 */
	public void setPyeeAccno(String pyeeAccno) {
		this.pyeeAccno = pyeeAccno;
	}

	/**
	 * @return pyeeAccno
	 */
	public String getPyeeAccno() {
		return this.pyeeAccno;
	}

	/**
	 * @param pyeeAcctsvcrName
	 */
	public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
		this.pyeeAcctsvcrName = pyeeAcctsvcrName;
	}

	/**
	 * @return pyeeAcctsvcrName
	 */
	public String getPyeeAcctsvcrName() {
		return this.pyeeAcctsvcrName;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}