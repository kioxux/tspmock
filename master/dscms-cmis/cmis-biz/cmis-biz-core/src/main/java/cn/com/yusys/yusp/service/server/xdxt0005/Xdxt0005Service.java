package cn.com.yusys.yusp.service.server.xdxt0005;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.req.Xdxt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.resp.Xdxt0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AreaManagerMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 业务逻辑类:根据工号获取所辖区域
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxt0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0005Service.class);

    @Resource
    private AreaManagerMapper areaManagerMapper;

    @Autowired
    private CommonService commonService;
    /**
     * 查询客户经理所在分部编号
     * @param xdxt0005DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0005DataRespDto getXdxt0005(Xdxt0005DataReqDto xdxt0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataReqDto));
        Xdxt0005DataRespDto xdxt0005DataRespDto = new Xdxt0005DataRespDto();
        try {
            //客户经理号
            String loginCode = xdxt0005DataReqDto.getManagerId();
            logger.info("**********XDXT0005**根据客户经理号询客户经理信息开始,查询参数为:{}", JSON.toJSONString(loginCode));
            AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(loginCode);
            logger.info("**********XDXT0005**根据客户经理号询客户经理信息结束,查询结果为:{}", JSON.toJSONString(adminSmUserDto));
            //机构号
            String orgId =adminSmUserDto.getOrgId();
            //通过机构号查询分部编号
            String subBranch = areaManagerMapper.getSubBranchByorgId(orgId);
            xdxt0005DataRespDto.setBranchNo(subBranch);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataRespDto));
        return xdxt0005DataRespDto;
    }
}
