package cn.com.yusys.yusp.service.server.xdkh0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.RepayDayRecordInfo;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdkh0023.req.Xdkh0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0023.resp.Xdkh0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.RepayDayRecordInfoMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0023Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class Xdkh0023Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0023Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * 还款日查询(2021-11-15从cus模块迁来)
     *
     * @param xdkh0023DataReqDto
     * @return
     */
    public Xdkh0023DataRespDto xdkh0023(Xdkh0023DataReqDto xdkh0023DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value);
        Xdkh0023DataRespDto xdkh0023DataRespDto = new Xdkh0023DataRespDto();
        try {
            //参数获取
            String cusId = xdkh0023DataReqDto.getCusId();
            String certCode = xdkh0023DataReqDto.getCertNo();
            String contNo = xdkh0023DataReqDto.getContNo();//合同号
            String dueFlag = "0";
            String cusName = "";

            /**************
             * 取值规则：1、根据合同号取合同表里面的还款日repay_date
             *          2、如果合同表日期为空，取配置表日期
             * ****************/
            String repayDate = "";//还款日期
            String signDate = "";//签订日期
            String loanStartDate = "";//合同起始日期
            if (StringUtil.isNotEmpty(contNo)) {
                logger.info("***************xdkh0023*根据输入参数【{}】查询合同表信息还款日开始", JSON.toJSONString(contNo));
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
                logger.info("***************xdkh0023*根据输入参数【{}】查询合同表信息还款日结束", JSON.toJSONString(ctrLoanCont));
                if (ctrLoanCont != null) {
                    repayDate = ctrLoanCont.getRepayDate();//还款日期
                    signDate = ctrLoanCont.getSignDate();//签订日期
                    loanStartDate = ctrLoanCont.getContStartDate();//合同起始日期
                }
            }
            //如果还款日期为空，取配置表日期
            if (StringUtil.isEmpty(repayDate)) {
                logger.info("****************扣款日期获取开始*START**************");
                RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
                if (repayDayRecordInfo != null) {
                    repayDate = repayDayRecordInfo.getRepayDate().toString();
                }
                logger.info("****************扣款日期获取结束*END**************还款日" + repayDate);
            }
            if (StringUtil.isEmpty(repayDate)) {//以上判断日期皆为空，默认21日还款
                repayDate = "21";
            }
            //通过客户证件号查询客户信息
            logger.info("**************XDKH0023**根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certCode));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certCode)).orElse(new CusBaseClientDto());
            logger.info("**************XDKH0023**根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certCode), JSON.toJSONString(cusBaseClientDto));
            cusName = cusBaseClientDto.getCusName();
            //判断 dueFlag(渠道反映增享贷使用：1-渠道使用固定日期20；0-渠道使用信贷推送的还款日期)
            if ((signDate != null && signDate.compareTo("2021-09-16") > 0 || loanStartDate != null && loanStartDate.compareTo("2021-09-16") > 0)
                    && !("JKHT20210275610,JKHT20210275427,JKHT20210275388,JKHT20210275372,JKHT20210274995," +
                    "JKHT20210275026,JKHT20210274989,JKHT20210274805,JKHT20210274467,JKHT20210274643,JKHT20210274621,JKHT20210274558," +
                    "JKHT20210274442,JKHT20210274417,JKHT20210274359,JKHT20210274306,JKHT20210274202,JKHT20210274152,JKHT20210273871,JKHT20210275621").contains(contNo)) {
                dueFlag = "1";
            }
            //返回报文信息
            xdkh0023DataRespDto.setDueFlag(dueFlag);
            xdkh0023DataRespDto.setRepayDate(repayDate);
            xdkh0023DataRespDto.setCusName(cusName);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value);
        return xdkh0023DataRespDto;
    }
}
