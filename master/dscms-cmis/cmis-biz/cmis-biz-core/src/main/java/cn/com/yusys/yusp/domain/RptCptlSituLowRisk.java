/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituLowRisk
 * @类描述: rpt_cptl_situ_low_risk数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-26 15:24:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_cptl_situ_low_risk")
public class RptCptlSituLowRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 融资情况总额 **/
	@Column(name = "CPTL_SITU_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cptlSituAmt;
	
	/** 融资情况我行金额 **/
	@Column(name = "CPTL_SITU_SELF_BANK_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cptlSituSelfBankAmt;
	
	/** 信用情况 **/
	@Column(name = "CREDIT_SITUATION", unique = false, nullable = true, length = 65535)
	private String creditSituation;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cptlSituAmt
	 */
	public void setCptlSituAmt(java.math.BigDecimal cptlSituAmt) {
		this.cptlSituAmt = cptlSituAmt;
	}
	
    /**
     * @return cptlSituAmt
     */
	public java.math.BigDecimal getCptlSituAmt() {
		return this.cptlSituAmt;
	}
	
	/**
	 * @param cptlSituSelfBankAmt
	 */
	public void setCptlSituSelfBankAmt(java.math.BigDecimal cptlSituSelfBankAmt) {
		this.cptlSituSelfBankAmt = cptlSituSelfBankAmt;
	}
	
    /**
     * @return cptlSituSelfBankAmt
     */
	public java.math.BigDecimal getCptlSituSelfBankAmt() {
		return this.cptlSituSelfBankAmt;
	}
	
	/**
	 * @param creditSituation
	 */
	public void setCreditSituation(String creditSituation) {
		this.creditSituation = creditSituation;
	}
	
    /**
     * @return creditSituation
     */
	public String getCreditSituation() {
		return this.creditSituation;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}