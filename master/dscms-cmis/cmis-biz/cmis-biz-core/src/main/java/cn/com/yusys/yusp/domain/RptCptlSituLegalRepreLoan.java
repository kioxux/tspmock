/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituLegalRepreLoan
 * @类描述: rpt_cptl_situ_legal_repre_loan数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-17 14:18:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_cptl_situ_legal_repre_loan")
public class RptCptlSituLegalRepreLoan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 法人代表编号 **/
	@Column(name = "LEGAL_REPRE_CUS_ID", unique = false, nullable = true, length = 40)
	private String legalRepreCusId;
	
	/** 法人代表姓名 **/
	@Column(name = "LEGAL_REPRE_CUS_NAME", unique = false, nullable = true, length = 80)
	private String legalRepreCusName;
	
	/** 融资银行名称 **/
	@Column(name = "CPTL_BANK_NAME", unique = false, nullable = true, length = 80)
	private String cptlBankName;
	
	/** 融资金额 **/
	@Column(name = "CPTL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cptlAmt;
	
	/** 融资余额 **/
	@Column(name = "CPTL_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cptlBalance;
	
	/** 逾期次数 **/
	@Column(name = "OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private Integer overdueTimes;
	
	/** 欠息次数 **/
	@Column(name = "DEBT_TIMES", unique = false, nullable = true, length = 10)
	private Integer debtTimes;
	
	/** 与企业关系 **/
	@Column(name = "ENTERPRISES_RELATION", unique = false, nullable = true, length = 20)
	private String enterprisesRelation;
	
	/** 融资品种 **/
	@Column(name = "CPTL_TYPE", unique = false, nullable = true, length = 5)
	private String cptlType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param legalRepreCusId
	 */
	public void setLegalRepreCusId(String legalRepreCusId) {
		this.legalRepreCusId = legalRepreCusId;
	}
	
    /**
     * @return legalRepreCusId
     */
	public String getLegalRepreCusId() {
		return this.legalRepreCusId;
	}
	
	/**
	 * @param legalRepreCusName
	 */
	public void setLegalRepreCusName(String legalRepreCusName) {
		this.legalRepreCusName = legalRepreCusName;
	}
	
    /**
     * @return legalRepreCusName
     */
	public String getLegalRepreCusName() {
		return this.legalRepreCusName;
	}
	
	/**
	 * @param cptlBankName
	 */
	public void setCptlBankName(String cptlBankName) {
		this.cptlBankName = cptlBankName;
	}
	
    /**
     * @return cptlBankName
     */
	public String getCptlBankName() {
		return this.cptlBankName;
	}
	
	/**
	 * @param cptlAmt
	 */
	public void setCptlAmt(java.math.BigDecimal cptlAmt) {
		this.cptlAmt = cptlAmt;
	}
	
    /**
     * @return cptlAmt
     */
	public java.math.BigDecimal getCptlAmt() {
		return this.cptlAmt;
	}
	
	/**
	 * @param cptlBalance
	 */
	public void setCptlBalance(java.math.BigDecimal cptlBalance) {
		this.cptlBalance = cptlBalance;
	}
	
    /**
     * @return cptlBalance
     */
	public java.math.BigDecimal getCptlBalance() {
		return this.cptlBalance;
	}
	
	/**
	 * @param overdueTimes
	 */
	public void setOverdueTimes(Integer overdueTimes) {
		this.overdueTimes = overdueTimes;
	}
	
    /**
     * @return overdueTimes
     */
	public Integer getOverdueTimes() {
		return this.overdueTimes;
	}
	
	/**
	 * @param debtTimes
	 */
	public void setDebtTimes(Integer debtTimes) {
		this.debtTimes = debtTimes;
	}
	
    /**
     * @return debtTimes
     */
	public Integer getDebtTimes() {
		return this.debtTimes;
	}
	
	/**
	 * @param enterprisesRelation
	 */
	public void setEnterprisesRelation(String enterprisesRelation) {
		this.enterprisesRelation = enterprisesRelation;
	}
	
    /**
     * @return enterprisesRelation
     */
	public String getEnterprisesRelation() {
		return this.enterprisesRelation;
	}
	
	/**
	 * @param cptlType
	 */
	public void setCptlType(String cptlType) {
		this.cptlType = cptlType;
	}
	
    /**
     * @return cptlType
     */
	public String getCptlType() {
		return this.cptlType;
	}


}