/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAcc;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitReply;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.IntbankOrgAdmitAccMapper;
import cn.com.yusys.yusp.repository.mapper.IntbankOrgAdmitReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.IntbankOrgAdmitAccEmportVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAccService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:26:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IntbankOrgAdmitAccService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(IntbankOrgAdmitAccService.class);

    @Autowired
    private IntbankOrgAdmitAccMapper intbankOrgAdmitAccMapper;

    @Autowired
    private IntbankOrgAdmitReplyMapper intbankOrgAdmitReplyMapper;

    @Autowired
    private IntbankOrgAdmitReplyService intbankOrgAdmitReplyService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IntbankOrgAdmitAcc selectByPrimaryKey(String pkId) {
        return intbankOrgAdmitAccMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IntbankOrgAdmitAcc> selectAll(QueryModel model) {
        List<IntbankOrgAdmitAcc> records = (List<IntbankOrgAdmitAcc>) intbankOrgAdmitAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IntbankOrgAdmitAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IntbankOrgAdmitAcc> list = intbankOrgAdmitAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IntbankOrgAdmitAcc record) {
        return intbankOrgAdmitAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IntbankOrgAdmitAcc record) {
        return intbankOrgAdmitAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IntbankOrgAdmitAcc record) {
        return intbankOrgAdmitAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IntbankOrgAdmitAcc record) {
        return intbankOrgAdmitAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return intbankOrgAdmitAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return intbankOrgAdmitAccMapper.deleteByIds(ids);
    }

    /**
     * 获取最新批复台账信息
     * @param queryModel
     * @return
     */
    public List<IntbankOrgAdmitAcc> lastAccByApp(QueryModel queryModel) {
        PageHelper.startPage(1, 1);
        queryModel.setSort(" start_date desc ");
        List<IntbankOrgAdmitAcc> records = intbankOrgAdmitAccMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return records;
    }

    /**
     * 根据客户编号（CusId)获取最新台账详情
     * @param cusId
     * @return
     */
    public IntbankOrgAdmitAcc lastAccByCusId(String cusId){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        queryModel.setSort(" start_date desc ");
        queryModel.setSize(1);
        queryModel.setPage(1);
        //        queryModel.addCondition("");
        List<IntbankOrgAdmitAcc> intbankOrgAdmitAccs = selectByModel(queryModel);
        if (intbankOrgAdmitAccs!=null && intbankOrgAdmitAccs.size()>0){
            return intbankOrgAdmitAccs.get(0);
        }
        return null;
    }

    /**
     * 获取台账批复详情
     * @param replySerno
     * @return
     */
    public IntbankOrgAdmitAcc intbankOrgAdmitAccWithReplayDetail(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<IntbankOrgAdmitAcc> intbankOrgAdmitAccs = selectByModel(queryModel);
        if (intbankOrgAdmitAccs!=null && intbankOrgAdmitAccs.size()>0){
            IntbankOrgAdmitAcc intbankOrgAdmitAcc = intbankOrgAdmitAccs.get(0);
            if (intbankOrgAdmitAcc!=null){
                //添加批复状态（intbank_org_admit_reply.REPLY_STATUS），审批结论(intbank_org_admit_reply.APPR_RESULT)
                QueryModel queryModel2 = new QueryModel();//REPLY_SERNO
                queryModel2.addCondition("replySerno",replySerno);
                List<IntbankOrgAdmitReply> intbankOrgAdmitReplies = intbankOrgAdmitReplyMapper.selectByModel(queryModel2);
                if (intbankOrgAdmitReplies!=null && intbankOrgAdmitReplies.size() > 0){
//                    intbankOrgAdmitAcc.setApprResult(intbankOrgAdmitReplies.get(0).getApprResult());
//                    intbankOrgAdmitAcc.setReplayStatus(intbankOrgAdmitReplies.get(0).getReplyStatus());
                }
                return intbankOrgAdmitAcc;
            }
        }
        return null;
    }

    /**
     * 更新最新的批复信息到台账表中
     * 1.根据cusId获取最新的台账信息（有效）
     * 2.没有则新增台账信息
     * 3.有则更新台账信息
     * @param replySerno
     * @param cusId
     * @param currentUserId
     * @param currentOrgId
     */
    public void generateAdmitAccHandleByReply(String replySerno, String cusId, String currentUserId, String currentOrgId) {
        try {
            IntbankOrgAdmitAcc acc = getActiveAdmitAccByCusId(cusId);
            IntbankOrgAdmitReply reply = intbankOrgAdmitReplyService.selectByReplayNo(replySerno);
            if (acc != null){
                acc.setReplySerno(replySerno);
//                acc.setReplayStatus(reply.getReplyStatus());
                acc.setIndgtResult(reply.getIndgtResult());
//                acc.setsetIndgtResult(reply.getApprResult());

                //初始化（更新）通用domain信息
                initUpdateDomainProperties(acc,currentUserId,currentOrgId);

                //判断当前申请是否为机构准入调整 台账状态修改为 失效
                if(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_03.equals(reply.getAppType())){
                    acc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_02);
                }

                intbankOrgAdmitAccMapper.updateByPrimaryKey(acc);
            }else{
                // 1.生成台账流水号
                String accSerno = generateSerno(SeqConstant.INTBANK_ADMIT_ACC_SEQ);

                acc = new IntbankOrgAdmitAcc();
                //同名属性复制
                BeanUtils.copyProperties(reply, acc);

                acc.setAccNo(accSerno);
                //判断当前申请是否为机构准入调整 台账状态修改为 失效
                if(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_03.equals(reply.getAppType())){
                    acc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_02);
                }else {
                    acc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_01);//TODO 字典项--台账表状态
                }

                //初始化(新增)通用domain信息(不包含input)
                initInsertDomainPropertiesForWF(acc,currentUserId,currentOrgId);
                intbankOrgAdmitAccMapper.insert(acc);
            }
        } catch (Exception e) {
            log.error("同业机构准入台账信息保存失败！==》",e);
            throw BizException.error(null, EclEnum.INTBANK_ORG_APP_ACC_FAILED.key,EclEnum.INTBANK_ORG_APP_ACC_FAILED.value);
        }
    }

    /**
     * 根据cusId(客户编号)获取最新（有效）台账信息
     * @param cusId
     * @return
     */
    private IntbankOrgAdmitAcc getActiveAdmitAccByCusId(String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        BizInvestCommonService.checkParamsIsNull("cusId",cusId);
        queryModel.addCondition("accStatus",CmisBizConstants.STD_REPLY_STATUS_01);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<IntbankOrgAdmitAcc> intbankOrgAdmitAccs = selectByModel(queryModel);
        if (intbankOrgAdmitAccs!=null && intbankOrgAdmitAccs.size()>0){
            return intbankOrgAdmitAccs.get(0);
        }
        return null;
    }

    /**
     * 异步导出同业机构准入名单
     * @return 导出进度信息
     */
    public ProgressDto exportIntbankOrgAdmitAcc(QueryModel model) {
        model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        model.setSort(" createTime desc ");
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectNameByModel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(IntbankOrgAdmitAccEmportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 导出同业机构准入名单 责任人/责任机构转换
     * @param model
     * @return
     */
    public List<IntbankOrgAdmitAccEmportVo> selectNameByModel(QueryModel model) {
        List<IntbankOrgAdmitAccEmportVo> intbankOrgAdmitAccEmportVoList = new ArrayList<>() ;
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IntbankOrgAdmitAcc> list = intbankOrgAdmitAccMapper.selectByModel(model);
        PageHelper.clearPage();

        for (IntbankOrgAdmitAcc intbankOrgAdmitAcc:list){
            IntbankOrgAdmitAccEmportVo intbankOrgAdmitAccEmportVo = new IntbankOrgAdmitAccEmportVo();
            String orgName = OcaTranslatorUtils.getOrgName(intbankOrgAdmitAcc.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(intbankOrgAdmitAcc.getManagerId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(intbankOrgAdmitAcc, intbankOrgAdmitAccEmportVo);
            intbankOrgAdmitAccEmportVo.setManagerBrIdName(orgName);
            intbankOrgAdmitAccEmportVo.setManagerIdName(userName);
            intbankOrgAdmitAccEmportVoList.add(intbankOrgAdmitAccEmportVo);
        }
        return intbankOrgAdmitAccEmportVoList;
    }

}
