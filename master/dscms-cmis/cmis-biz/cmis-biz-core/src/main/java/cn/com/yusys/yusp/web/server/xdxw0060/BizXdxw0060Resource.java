package cn.com.yusys.yusp.web.server.xdxw0060;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0060.req.Xdxw0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0060.resp.Xdxw0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0060.Xdxw0060Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户及配偶信用类小微业务贷款授信金额
 *
 * @author zhangpeng
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDXW0060:客户及配偶信用类小微业务贷款授信金额")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0060Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0060Resource.class);
    @Autowired
    private Xdxw0060Service xdxw0060Service;

    /**
     * 交易码：xdxw0060
     * 交易描述：客户及配偶信用类小微业务贷款授信金额
     *
     * @param xdxw0060DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户及配偶信用类小微业务贷款授信金额")
    @PostMapping("/xdxw0060")
    protected @ResponseBody
    ResultDto<Xdxw0060DataRespDto> xdxw0060(@Validated @RequestBody Xdxw0060DataReqDto xdxw0060DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataReqDto));
        Xdxw0060DataRespDto xdxw0060DataRespDto = new Xdxw0060DataRespDto();// 响应Dto:客户及配偶信用类小微业务贷款授信金额
        ResultDto<Xdxw0060DataRespDto> xdxw0060DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataReqDto));
            xdxw0060DataRespDto = xdxw0060Service.xdxw0060(xdxw0060DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataRespDto));
            // 封装xdxw0060DataResultDto中正确的返回码和返回信息
            xdxw0060DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0060DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, e.getMessage());
            // 封装xdxw0060DataResultDto中异常返回码和返回信息
            xdxw0060DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0060DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0060DataRespDto到xdxw0060DataResultDto中
        xdxw0060DataResultDto.setData(xdxw0060DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataResultDto));
        return xdxw0060DataResultDto;
    }
}
