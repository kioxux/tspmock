/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import cn.com.yusys.yusp.repository.mapper.GuarContRelWarrantMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContRelWarrantService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-13 15:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarContRelWarrantService {

    @Autowired
    private GuarContRelWarrantMapper guarContRelWarrantMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarContRelWarrant selectByPrimaryKey(String pkId) {
        return guarContRelWarrantMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarContRelWarrant> selectAll(QueryModel model) {
        List<GuarContRelWarrant> records = (List<GuarContRelWarrant>) guarContRelWarrantMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarContRelWarrant> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarContRelWarrant> list = guarContRelWarrantMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarContRelWarrant record) {
        return guarContRelWarrantMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarContRelWarrant record) {
        return guarContRelWarrantMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarContRelWarrant record) {
        return guarContRelWarrantMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarContRelWarrant record) {
        return guarContRelWarrantMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return guarContRelWarrantMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarContRelWarrantMapper.deleteByIds(ids);
    }

    public List<GuarContRelWarrant> selectBySerno(@Param("serno") String serno){
        return guarContRelWarrantMapper.selectBySerno(serno);
    }

    /**
     * 根据核心担保编号查询
     * @param coreGuarantyNo
     * @return
     */
    public List<GuarContRelWarrant> selectByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo){
        return guarContRelWarrantMapper.selectByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 根据核心担保编号查询对应的抵押物的借款人客户项下存在有效的“诚易融”贷款合同的押品编号
     * @param coreGuarantyNo
     * @return
     */
    public String selectGuarNoByCoreGuarantyNo(String coreGuarantyNo){
        return guarContRelWarrantMapper.selectGuarNoByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 根据合同号查询权证入库核心担保编号列表
     * @param contNo
     * @return
     */
    public String selectCoreGuarantyNoByContNo(String contNo){
        return guarContRelWarrantMapper.selectCoreGuarantyNoByContNo(contNo);
    }

    /**
     * 根据核心担保编号查询押品编号
     * @param coreGuarantyNo
     * @return
     */
    public String selectGuarNosByCoreGuarantyNo(String coreGuarantyNo){
        return guarContRelWarrantMapper.selectGuarNosByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 根据担保合同编号查询已入库的核心担保编号
     * @param guarContNo
     * @return
     */
    public String selectCoreGuarantyNosByGuarContNo(String guarContNo){
        return guarContRelWarrantMapper.selectCoreGuarantyNosByGuarContNo(guarContNo);
    }

    /**
     * 根据押品编号查询最新的核心担保编号
     * @param guarNo
     * @return
     */
    public String selectCoreGuarantyNoByGuarNo(String guarNo){
        return guarContRelWarrantMapper.selectCoreGuarantyNoByGuarNo(guarNo);
    }

}
