package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeInfo
 * @类描述: major_grade_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-30 10:43:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class MajorGradeInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 评级申请流水号 **/
	private String gradeSerno;
	
	/** 授信申请流水号 **/
	private String lmtSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 评级年度 **/
	private String gradeYear;
	
	/** 评级发生类型 **/
	private String gradeHappenType;
	
	/** 评级模型 **/
	private String gradeMode;
	
	/** 专业贷款系统初始等级 **/
	private String initGrade;
	
	/** 专业贷款调整后等级 **/
	private String afterGrade;
	
	/** 专业贷款建议等级 **/
	private String adviceGrade;
	
	/** 专业贷款最终认定等级 **/
	private String finalGrade;
	
	/** 专业贷款预期损失率 **/
	private java.math.BigDecimal expectedLossRate;
	
	/** 风险大类 **/
	private String bigriskclass;
	
	/** 风险种类 **/
	private String midriskclass;
	
	/** 风险小类 **/
	private String fewriskclass;
	
	/** 风险划分日期 **/
	private String riskdividedate;
	
	/** 评级生效日 **/
	private String inureDate;
	
	/** 评级到期日 **/
	private String endDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 最近修改人 **/
	private java.util.Date updId;
	
	/** 最近修改机构 **/
	private java.util.Date updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private String createTime;
	
	/** 修改时间 **/
	private String updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param gradeSerno
	 */
	public void setGradeSerno(String gradeSerno) {
		this.gradeSerno = gradeSerno == null ? null : gradeSerno.trim();
	}
	
    /**
     * @return GradeSerno
     */	
	public String getGradeSerno() {
		return this.gradeSerno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param gradeYear
	 */
	public void setGradeYear(String gradeYear) {
		this.gradeYear = gradeYear == null ? null : gradeYear.trim();
	}
	
    /**
     * @return GradeYear
     */	
	public String getGradeYear() {
		return this.gradeYear;
	}
	
	/**
	 * @param gradeHappenType
	 */
	public void setGradeHappenType(String gradeHappenType) {
		this.gradeHappenType = gradeHappenType == null ? null : gradeHappenType.trim();
	}
	
    /**
     * @return GradeHappenType
     */	
	public String getGradeHappenType() {
		return this.gradeHappenType;
	}
	
	/**
	 * @param gradeMode
	 */
	public void setGradeMode(String gradeMode) {
		this.gradeMode = gradeMode == null ? null : gradeMode.trim();
	}
	
    /**
     * @return GradeMode
     */	
	public String getGradeMode() {
		return this.gradeMode;
	}
	
	/**
	 * @param initGrade
	 */
	public void setInitGrade(String initGrade) {
		this.initGrade = initGrade == null ? null : initGrade.trim();
	}
	
    /**
     * @return InitGrade
     */	
	public String getInitGrade() {
		return this.initGrade;
	}
	
	/**
	 * @param afterGrade
	 */
	public void setAfterGrade(String afterGrade) {
		this.afterGrade = afterGrade == null ? null : afterGrade.trim();
	}
	
    /**
     * @return AfterGrade
     */	
	public String getAfterGrade() {
		return this.afterGrade;
	}
	
	/**
	 * @param adviceGrade
	 */
	public void setAdviceGrade(String adviceGrade) {
		this.adviceGrade = adviceGrade == null ? null : adviceGrade.trim();
	}
	
    /**
     * @return AdviceGrade
     */	
	public String getAdviceGrade() {
		return this.adviceGrade;
	}
	
	/**
	 * @param finalGrade
	 */
	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade == null ? null : finalGrade.trim();
	}
	
    /**
     * @return FinalGrade
     */	
	public String getFinalGrade() {
		return this.finalGrade;
	}
	
	/**
	 * @param expectedLossRate
	 */
	public void setExpectedLossRate(java.math.BigDecimal expectedLossRate) {
		this.expectedLossRate = expectedLossRate;
	}
	
    /**
     * @return ExpectedLossRate
     */	
	public java.math.BigDecimal getExpectedLossRate() {
		return this.expectedLossRate;
	}
	
	/**
	 * @param bigriskclass
	 */
	public void setBigriskclass(String bigriskclass) {
		this.bigriskclass = bigriskclass == null ? null : bigriskclass.trim();
	}
	
    /**
     * @return Bigriskclass
     */	
	public String getBigriskclass() {
		return this.bigriskclass;
	}
	
	/**
	 * @param midriskclass
	 */
	public void setMidriskclass(String midriskclass) {
		this.midriskclass = midriskclass == null ? null : midriskclass.trim();
	}
	
    /**
     * @return Midriskclass
     */	
	public String getMidriskclass() {
		return this.midriskclass;
	}
	
	/**
	 * @param fewriskclass
	 */
	public void setFewriskclass(String fewriskclass) {
		this.fewriskclass = fewriskclass == null ? null : fewriskclass.trim();
	}
	
    /**
     * @return Fewriskclass
     */	
	public String getFewriskclass() {
		return this.fewriskclass;
	}
	
	/**
	 * @param riskdividedate
	 */
	public void setRiskdividedate(String riskdividedate) {
		this.riskdividedate = riskdividedate == null ? null : riskdividedate.trim();
	}
	
    /**
     * @return Riskdividedate
     */	
	public String getRiskdividedate() {
		return this.riskdividedate;
	}
	
	/**
	 * @param inureDate
	 */
	public void setInureDate(String inureDate) {
		this.inureDate = inureDate == null ? null : inureDate.trim();
	}
	
    /**
     * @return InureDate
     */	
	public String getInureDate() {
		return this.inureDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(java.util.Date updId) {
		this.updId = updId;
	}
	
    /**
     * @return UpdId
     */	
	public java.util.Date getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(java.util.Date updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return UpdBrId
     */	
	public java.util.Date getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}
	
    /**
     * @return CreateTime
     */	
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime == null ? null : updateTime.trim();
	}
	
    /**
     * @return UpdateTime
     */	
	public String getUpdateTime() {
		return this.updateTime;
	}


}