package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.OtherRecordSpecialLoanApp;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.OtherRecordSpecialLoanAppService;
import cn.com.yusys.yusp.service.RepayCapPlanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class RiskItem0108Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0108Service.class);

    @Autowired
    OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;
    @Autowired
    RepayCapPlanService repayCapPlanService;

    /**
     * @方法名称: riskItem0108
     * @方法描述: 用信审核备案申请金额校验
     * @参数与返回说明:
     * @算法描述:
     * 本次还款计划设置还款金额之和等于本次申请放款金额
     * @创建人: macm
     * @创建时间: 2021-09-01 23:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public  RiskResultDto riskItem0108(QueryModel queryModel) {
        // 业务流水号
        String serno = queryModel.getCondition().get("bizId").toString();
        // 本次申请放款金额
        BigDecimal curtAppAmt = BigDecimal.ZERO;
        // 是否存在还款计划默认0 不存在
        String isRepayPlan = "0";
        // 还款计划累加金额
        BigDecimal repayAmt =  BigDecimal.ZERO;
        log.info("用信审核备案申请金额校验*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 获取业务信息 other_record_special_loan_app
        OtherRecordSpecialLoanApp otherApp = otherRecordSpecialLoanAppService.selectByPrimaryKey(serno);
        if(null != otherApp){
            // 获取申请金额
            curtAppAmt = otherApp.getCurtAppAmt();
            isRepayPlan = otherApp.getIsRepayPlan();
        }
        // 是否存在还款计划
        if("1".equals(isRepayPlan)){
            // 获取还款计划 repay_cap_plan
            List<RepayCapPlan> list = repayCapPlanService.selectBySerno(serno);
            for (int i = 0; i < list.size(); i++) {
                RepayCapPlan repayCapPlan = list.get(i);
                repayAmt = repayAmt.add(repayCapPlan.getRepayAmt());
            }
        } else { // 无还款计划不校验
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }

        if(curtAppAmt.compareTo(repayAmt) != 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10406);
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}