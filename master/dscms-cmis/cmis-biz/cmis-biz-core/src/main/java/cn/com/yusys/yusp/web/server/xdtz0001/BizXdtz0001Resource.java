package cn.com.yusys.yusp.web.server.xdtz0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0001.req.Xdtz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.resp.Xdtz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0001.Xdtz0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户信息查询(贷款信息)
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0001:客户信息查询(贷款信息)")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0001Resource.class);

    @Autowired
    private Xdtz0001Service xdtz0001Service;

    /**
     * 交易码：xdtz0001
     * 交易描述：客户信息查询(贷款信息)
     *
     * @param xdtz0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0001")
    protected @ResponseBody
    ResultDto<Xdtz0001DataRespDto> xdtz0001(@Validated @RequestBody Xdtz0001DataReqDto xdtz0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataReqDto));
        Xdtz0001DataRespDto xdtz0001DataRespDto = new Xdtz0001DataRespDto();// 响应Dto:客户信息查询(贷款信息)
        ResultDto<Xdtz0001DataRespDto> xdtz0001DataResultDto = new ResultDto<>();
        // 从xdtz0001DataReqDto获取业务值进行业务逻辑处理
        String queryType = xdtz0001DataReqDto.getQueryType();//查询类型
        String cusId = xdtz0001DataReqDto.getCusId();//客户代码
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataReqDto));
            xdtz0001DataRespDto = xdtz0001Service.getXdtz0001(xdtz0001DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataRespDto));
            // 封装xdtz0001DataResultDto中正确的返回码和返回信息
            xdtz0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdtz0001DataResultDto中异常返回码和返回信息
            xdtz0001DataResultDto.setCode(e.getErrorCode());
            xdtz0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, e.getMessage());
            // 封装xdtz0001DataResultDto中异常返回码和返回信息
            xdtz0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0001DataRespDto到xdtz0001DataResultDto中
        xdtz0001DataResultDto.setData(xdtz0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataResultDto));
        return xdtz0001DataResultDto;
    }
}