package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:20
 * @desc 主动还款
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW12BizService implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW12BizService.class);

    @Autowired
    private IqpPrePaymentService iqpPrePaymentService;

    @Autowired
    private BGYW11BizService bGYW11BizService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private  CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // BG023主动还款（零售）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG023.equals(bizType)) {
            iqpBillAcctChgBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    // 集团客户处理
    private void iqpBillAcctChgBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {

            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            IqpPrePayment iqpPrePayment = iqpPrePaymentService.selectByPrimaryKey(iqpSerno);
            log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
            if (iqpPrePayment == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                //调用核心接口还款
//
                ResultDto<Ln3041RespDto> ln3041ResultDto = bGYW11BizService.sendHxToRepay(iqpSerno);
                String ln3041Meesage = Optional.ofNullable(ln3041ResultDto.getMessage())
                        .orElse(SuccessEnum.SUCCESS.value);
                if (!Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3041ResultDto.getCode())) {
                    iqpPrePayment.setStatus("0"); // 还款失败
                    iqpPrePayment.setResn("核心返回："+ ln3041Meesage);
                } else {
                    iqpPrePayment.setStatus("1"); // 还款成功
                    iqpPrePayment.setResn("核心返回流水号："+ ln3041Meesage);
                    //修改贷款余额：信贷发送ln3041交易成功后，esb会继续调用核心ln3100交易，将查询的内容返回给信贷（gxloan）xdtz0059，并且返回给国结
                   bGYW11BizService.changeAccLoan(iqpSerno);
                }

                if(null != ln3041ResultDto && null != ln3041ResultDto.getData()){
                    iqpPrePayment.setHxSerno(ln3041ResultDto.getData().getJiaoyils());
                    iqpPrePayment.setHxDate(ln3041ResultDto.getData().getJiaoyirq());
                }
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
                // 推送用印系统
                /*try {
                    cmisBizXwCommonService.sendYk(currentUserId,iqpSerno,iqpPrePayment.getCusId());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }*/
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpPrePaymentService.updateSelective(iqpPrePayment);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW12.equals(flowCode);
    }
}

