/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardTelvOtherInfo;
import cn.com.yusys.yusp.repository.mapper.CreditCardTelvOtherInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvOtherInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardTelvOtherInfoService {

    @Autowired
    private CreditCardTelvOtherInfoMapper creditCardTelvOtherInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardTelvOtherInfo selectByPrimaryKey(String serno) {
        return creditCardTelvOtherInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardTelvOtherInfo> selectAll(QueryModel model) {
        List<CreditCardTelvOtherInfo> records = (List<CreditCardTelvOtherInfo>) creditCardTelvOtherInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardTelvOtherInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardTelvOtherInfo> list = creditCardTelvOtherInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardTelvOtherInfo record) {
        return creditCardTelvOtherInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardTelvOtherInfo record) {
        return creditCardTelvOtherInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardTelvOtherInfo record) {
        return creditCardTelvOtherInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardTelvOtherInfo record) {
        return creditCardTelvOtherInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return creditCardTelvOtherInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardTelvOtherInfoMapper.deleteByIds(ids);
    }


/**
 * @param
 * @return
 * @author wzy
 * @date 2021/5/31 15:22
 * @version 1.0.0
 * @desc    问题其它信息新增
 * @修改历史: 修改时间    修改人员    修改原因
 */
    public int insertAuto(CreditCardTelvOtherInfo creditCardTelvOtherInfo) {
        int result=0;
        try {
            String serno = creditCardTelvOtherInfo.getSerno();
            CreditCardTelvOtherInfo record = this.selectByPrimaryKey(serno);
            if(record == null){
                result = this.insertSelective(creditCardTelvOtherInfo);
            }else{
                result = this.updateSelective(creditCardTelvOtherInfo);
            }
            if(result !=1){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成问题其它信息数据异常！");
            }
        }catch (Exception e){
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return result;
    }
}
