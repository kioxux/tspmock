package cn.com.yusys.yusp.web.server.xdxw0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0007.req.Xdxw0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0007.resp.Xdxw0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0007.Xdxw0007Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:批复信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0007:批复信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0007Resource.class);

    @Autowired
    private Xdxw0007Service xdxw0007Service;

    /**
     * 交易码：xdxw0007
     * 交易描述：批复信息查询
     *
     * @param xdxw0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("批复信息查询")
    @PostMapping("/xdxw0007")
    protected @ResponseBody
    ResultDto<Xdxw0007DataRespDto> xdxw0007(@Validated @RequestBody Xdxw0007DataReqDto xdxw0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007DataReqDto));
        Xdxw0007DataRespDto xdxw0007DataRespDto = new Xdxw0007DataRespDto();// 响应Dto:批复信息查询
        ResultDto<Xdxw0007DataRespDto> xdxw0007DataResultDto = new ResultDto<>();
        String contNo = xdxw0007DataReqDto.getContNo();
        // 从xdxw0007DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0007Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, contNo);
            xdxw0007DataRespDto = Optional.ofNullable(xdxw0007Service.queryByContNo(contNo))
                    .orElse(new Xdxw0007DataRespDto(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY,
                            new BigDecimal(CmisBizConstants.NUM_ZERO), StringUtils.EMPTY, StringUtils.EMPTY,
                            new BigDecimal(CmisBizConstants.NUM_ZERO), StringUtils.EMPTY));
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, xdxw0007DataRespDto);
            // 封装xdxw0007DataRespDto对象结束
            // 封装xdxw0007DataResultDto中正确的返回码和返回信息
            xdxw0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, e.getMessage());
            // 封装xdxw0007DataResultDto中异常返回码和返回信息
            xdxw0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0007DataRespDto到xdxw0007DataResultDto中
        xdxw0007DataResultDto.setData(xdxw0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007DataResultDto));
        return xdxw0007DataResultDto;
    }
}
