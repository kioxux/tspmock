/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarBaseInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarRegisterDetail;
import cn.com.yusys.yusp.service.GuarRegisterDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarRegisterDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 22:01:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarregisterdetail")
public class GuarRegisterDetailResource {
    @Autowired
    private GuarRegisterDetailService guarRegisterDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarRegisterDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarRegisterDetail> list = guarRegisterDetailService.selectAll(queryModel);
        return new ResultDto<List<GuarRegisterDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarRegisterDetail>> index(QueryModel queryModel) {
        List<GuarRegisterDetail> list = guarRegisterDetailService.selectByModel(queryModel);
        return new ResultDto<List<GuarRegisterDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarRegisterDetail> show(@PathVariable("serno") String serno) {
        GuarRegisterDetail guarRegisterDetail = guarRegisterDetailService.selectByPrimaryKey(serno);
        return new ResultDto<GuarRegisterDetail>(guarRegisterDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarRegisterDetail> create(@RequestBody GuarRegisterDetail guarRegisterDetail) throws URISyntaxException {
        guarRegisterDetailService.insert(guarRegisterDetail);
        return new ResultDto<GuarRegisterDetail>(guarRegisterDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarRegisterDetail guarRegisterDetail) throws URISyntaxException {
        int result = guarRegisterDetailService.update(guarRegisterDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarRegisterDetailService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarRegisterDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertGuarRegisterDetail
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertGuarRegisterDetail")
    protected ResultDto<Integer> insertGuarRegisterDetail(@RequestBody List<GuarRegisterDetail> guarRegisterDetail) {
        int result = guarRegisterDetailService.insertGuarRegisterDetail(guarRegisterDetail);
        //int result =0;
        return new ResultDto<Integer>(result);
    }

    /**
     * 全表查询.
     * @函数名称:
     * @函数描述:查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryInfoByRegSerno")
    protected ResultDto<List<GuarRegisterDetail>> queryInfoByRegSerno(@RequestBody GuarRegisterDetail record) {
        ResultDto<List<GuarRegisterDetail>> listResultDto = guarRegisterDetailService.selectByRegSernoModel(record.getRegSerno());
        return listResultDto;
    }
}
