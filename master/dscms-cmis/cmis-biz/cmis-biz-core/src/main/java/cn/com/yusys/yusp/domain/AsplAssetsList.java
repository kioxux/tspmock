/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAssetsList
 * @类描述: aspl_assets_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:45:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "aspl_assets_list")
public class AsplAssetsList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 资产类型 **/
	@Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String assetType;
	
	/** 资产价值 **/
	@Column(name = "ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetValue;
	
	/** 资产到期日 **/
	@Column(name = "ASSET_END_DATE", unique = false, nullable = true, length = 20)
	private String assetEndDate;
	
	/** 资产状态 **/
	@Column(name = "ASSET_STATUS", unique = false, nullable = true, length = 5)
	private String assetStatus;
	
	/** 资产来源 **/
	@Column(name = "ASSTE_SOUR", unique = false, nullable = true, length = 5)
	private String assteSour;
	
	/** 是否入池 **/
	@Column(name = "IS_POOL", unique = false, nullable = true, length = 5)
	private String isPool;
	
	/** 是否入池质押 **/
	@Column(name = "IS_PLEDGE", unique = false, nullable = true, length = 5)
	private String isPledge;
	
	/** 入池时间 **/
	@Column(name = "INP_TIME", unique = false, nullable = true, length = 20)
	private String inpTime;
	
	/** 出池时间 **/
	@Column(name = "OUTP_TIME", unique = false, nullable = true, length = 20)
	private String outpTime;
	
	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;
	
	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 统一押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;

	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param assetType
	 */
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	
    /**
     * @return assetType
     */
	public String getAssetType() {
		return this.assetType;
	}
	
	/**
	 * @param assetValue
	 */
	public void setAssetValue(java.math.BigDecimal assetValue) {
		this.assetValue = assetValue;
	}
	
    /**
     * @return assetValue
     */
	public java.math.BigDecimal getAssetValue() {
		return this.assetValue;
	}
	
	/**
	 * @param assetEndDate
	 */
	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}
	
    /**
     * @return assetEndDate
     */
	public String getAssetEndDate() {
		return this.assetEndDate;
	}
	
	/**
	 * @param assetStatus
	 */
	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}
	
    /**
     * @return assetStatus
     */
	public String getAssetStatus() {
		return this.assetStatus;
	}
	
	/**
	 * @param assteSour
	 */
	public void setAssteSour(String assteSour) {
		this.assteSour = assteSour;
	}
	
    /**
     * @return assteSour
     */
	public String getAssteSour() {
		return this.assteSour;
	}
	
	/**
	 * @param isPool
	 */
	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}
	
    /**
     * @return isPool
     */
	public String getIsPool() {
		return this.isPool;
	}
	
	/**
	 * @param isPledge
	 */
	public void setIsPledge(String isPledge) {
		this.isPledge = isPledge;
	}
	
    /**
     * @return isPledge
     */
	public String getIsPledge() {
		return this.isPledge;
	}
	
	/**
	 * @param inpTime
	 */
	public void setInpTime(String inpTime) {
		this.inpTime = inpTime;
	}
	
    /**
     * @return inpTime
     */
	public String getInpTime() {
		return this.inpTime;
	}
	
	/**
	 * @param outpTime
	 */
	public void setOutpTime(String outpTime) {
		this.outpTime = outpTime;
	}
	
    /**
     * @return outpTime
     */
	public String getOutpTime() {
		return this.outpTime;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}
	
    /**
     * @return aorgNo
     */
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}
	
    /**
     * @return aorgName
     */
	public String getAorgName() {
		return this.aorgName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
}