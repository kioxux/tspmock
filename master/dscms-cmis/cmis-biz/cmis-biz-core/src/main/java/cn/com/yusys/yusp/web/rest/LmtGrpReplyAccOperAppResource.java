/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtGrpReplyChg;
import cn.com.yusys.yusp.domain.LmtReplyAccOperApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpReplyAccOperApp;
import cn.com.yusys.yusp.service.LmtGrpReplyAccOperAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccOperAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:42:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrpreplyaccoperapp")
public class LmtGrpReplyAccOperAppResource {
    @Autowired
    private LmtGrpReplyAccOperAppService lmtGrpReplyAccOperAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpReplyAccOperApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpReplyAccOperApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpReplyAccOperApp>> index(QueryModel queryModel) {
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpReplyAccOperApp> show(@PathVariable("pkId") String pkId) {
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = lmtGrpReplyAccOperAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpReplyAccOperApp>(lmtGrpReplyAccOperApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpReplyAccOperApp> create(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws URISyntaxException {
        lmtGrpReplyAccOperAppService.insert(lmtGrpReplyAccOperApp);
        return new ResultDto<LmtGrpReplyAccOperApp>(lmtGrpReplyAccOperApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws URISyntaxException {
        int result = lmtGrpReplyAccOperAppService.update(lmtGrpReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpReplyAccOperAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpReplyAccOperAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:查询所有
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<LmtGrpReplyAccOperApp>> queryAll(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppService.queryAll(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<LmtGrpReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:queryHis
     * @函数描述:查询集团客户额度冻结/解冻/终止历史
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryHis")
    protected ResultDto<List<LmtGrpReplyAccOperApp>> queryHis(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppService.queryHis(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<LmtGrpReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws URISyntaxException {
        int result = lmtGrpReplyAccOperAppService.updateSelective(lmtGrpReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getsubandprd")
    protected ResultDto<Map> getSubAndPrd(@RequestBody Map map) {
        Object grpSerno = map.get("grpSerno");
        Map subAndPrd = lmtGrpReplyAccOperAppService.getSubAndPrd((String) grpSerno);
        return new ResultDto<>(subAndPrd);
    }

    /**
     * @函数名称:updateoperapp
     * @函数描述:客户额度冻结/解冻/终止申请暂存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoperapp")
    protected ResultDto<Integer> updateoperapp(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws URISyntaxException {
        int result = lmtGrpReplyAccOperAppService.updateoperapp(lmtGrpReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:showdetial
     * @函数描述:根据集团客户号查询授信成员信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showdetial")
    protected ResultDto<List<LmtGrpReplyAccOperApp>> showdetial(@RequestBody Map map) {
        String grpSerno =(String) map.get("serno");
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppService.showdetial(grpSerno);
        return new ResultDto<List<LmtGrpReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:guideSave
     * @函数描述:集团客户额度冻结解冻向导下一步保存
     * @创建人：ywl
     * @创建时间：2021/5/18
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/guideSave")
    protected ResultDto<String> guideSave(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws Exception {
        return new ResultDto<>(lmtGrpReplyAccOperAppService.guideSave(lmtGrpReplyAccOperApp));
    }

    /**
     * @函数名称:selectlmtreplyaccoperappbyparams
     * @函数描述:集团客户额度冻结/解冻/终止申请根据流水号查询
     * @创建者: yangwl
     * @创建时间: 2021/5/21
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "根据流水号查询客户额度冻结/解冻/终止申请详情")
    @PostMapping("/selectlmtgrpreplyaccoperappbygrpserno")
    protected  ResultDto<LmtGrpReplyAccOperApp> selectLmtReplyAccOperAppByGrpSerno(@RequestBody String grpSerno) {
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = lmtGrpReplyAccOperAppService.selectLmtGrpReplyAccOperAppByGrpSerno(grpSerno);
        return new ResultDto<LmtGrpReplyAccOperApp>(lmtGrpReplyAccOperApp);
    }

    /**
     * @函数名称:selectLmtGrpReplyAccOperApp
     * @函数描述:查询客户额度冻结/解冻/终止申请是否已存在
     * @创建者: yangwl
     * @创建时间: 2021/6/24
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectlmtgrpreplyaccoperapp")
    protected  ResultDto<Map> selectLmtGrpReplyAccOperApp(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) {
        Map rtnData = lmtGrpReplyAccOperAppService.selectLmtGrpReplyAccOperApp(lmtGrpReplyAccOperApp);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @函数名称:logicDelete
     * @函数描述:逻辑删除
     * @创建者: css
     * @创建时间: 2021/10/5
     */
    @PostMapping("/logicdelete")
    protected  ResultDto<Integer> logicDelete(@RequestBody LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) {
        int exit = 0;
        if(lmtGrpReplyAccOperApp.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_996)){
            exit = lmtGrpReplyAccOperAppService.updateSelective(lmtGrpReplyAccOperApp);
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(lmtGrpReplyAccOperApp.getGrpSerno());
        }else{
            lmtGrpReplyAccOperApp.setOprType(CmisCommonConstants.OP_TYPE_02);
            exit = lmtGrpReplyAccOperAppService.updateSelective(lmtGrpReplyAccOperApp);
        }
        return new ResultDto<Integer>(exit);
    }
}
