package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkCfrm
 * @类描述: iqp_risk_chk_cfrm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 15:04:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpRiskChkCfrmDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 场景 STD_ZB_BANK_KIND **/
	private String secen;
	
	/** 借款人收入认定意见 **/
	private String borrIncomeNote;
	
	/** 物业管理费认定 **/
	private java.math.BigDecimal pmfAr;
	
	/** 借款人负债认定意见 **/
	private String borrDebtNote;
	
	/** 信用风险评估意见 **/
	private String crdtRiskNote;
	
	/** 首付款支付及来源 **/
	private String firstAmtSrc;
	
	/** 首付比例执行依据 **/
	private String firstRateBasc;
	
	/** 所购房屋押品本次认定价值 **/
	private java.math.BigDecimal grtEvalAmt;
	
	/** 是否足值 STD_ZB_YES_NO **/
	private String grtIsEnfv;
	
	/** 是否足值认定说明 **/
	private String grtNote;
	
	/** 价值成数是否合理 STD_ZB_YES_NO **/
	private String grtAmtFit;
	
	/** 价值成数认定说明 **/
	private String grtFitNote;
	
	/** 是否具备处置变现能力 STD_ZB_YES_NO **/
	private String grtChab;
	
	/** 处置变现能力认定说明 **/
	private String grtChabNote;
	
	/** 购房用途评估意见 **/
	private String grtHousePopNote;
	
	/** I/I **/
	private java.math.BigDecimal mAmt;
	
	/** D/I **/
	private java.math.BigDecimal dAmt;
	
	/** D/I（折算后） **/
	private java.math.BigDecimal dAmtCvt;
	
	/** 借款人月收入总额 **/
	private java.math.BigDecimal mIncomeTot;
	
	/** 借款人已有负债月还款额汇总 **/
	private java.math.BigDecimal debtMRepayTot;
	
	/** 借款人已有负债月还款汇总(调整后) **/
	private java.math.BigDecimal debtMRepayTotCvt;
	
	/** 本笔贷款月负债总额 **/
	private java.math.BigDecimal mDebtTot;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param secen
	 */
	public void setSecen(String secen) {
		this.secen = secen == null ? null : secen.trim();
	}
	
    /**
     * @return Secen
     */	
	public String getSecen() {
		return this.secen;
	}
	
	/**
	 * @param borrIncomeNote
	 */
	public void setBorrIncomeNote(String borrIncomeNote) {
		this.borrIncomeNote = borrIncomeNote == null ? null : borrIncomeNote.trim();
	}
	
    /**
     * @return BorrIncomeNote
     */	
	public String getBorrIncomeNote() {
		return this.borrIncomeNote;
	}
	
	/**
	 * @param pmfAr
	 */
	public void setPmfAr(java.math.BigDecimal pmfAr) {
		this.pmfAr = pmfAr;
	}
	
    /**
     * @return PmfAr
     */	
	public java.math.BigDecimal getPmfAr() {
		return this.pmfAr;
	}
	
	/**
	 * @param borrDebtNote
	 */
	public void setBorrDebtNote(String borrDebtNote) {
		this.borrDebtNote = borrDebtNote == null ? null : borrDebtNote.trim();
	}
	
    /**
     * @return BorrDebtNote
     */	
	public String getBorrDebtNote() {
		return this.borrDebtNote;
	}
	
	/**
	 * @param crdtRiskNote
	 */
	public void setCrdtRiskNote(String crdtRiskNote) {
		this.crdtRiskNote = crdtRiskNote == null ? null : crdtRiskNote.trim();
	}
	
    /**
     * @return CrdtRiskNote
     */	
	public String getCrdtRiskNote() {
		return this.crdtRiskNote;
	}
	
	/**
	 * @param firstAmtSrc
	 */
	public void setFirstAmtSrc(String firstAmtSrc) {
		this.firstAmtSrc = firstAmtSrc == null ? null : firstAmtSrc.trim();
	}
	
    /**
     * @return FirstAmtSrc
     */	
	public String getFirstAmtSrc() {
		return this.firstAmtSrc;
	}
	
	/**
	 * @param firstRateBasc
	 */
	public void setFirstRateBasc(String firstRateBasc) {
		this.firstRateBasc = firstRateBasc == null ? null : firstRateBasc.trim();
	}
	
    /**
     * @return FirstRateBasc
     */	
	public String getFirstRateBasc() {
		return this.firstRateBasc;
	}
	
	/**
	 * @param grtEvalAmt
	 */
	public void setGrtEvalAmt(java.math.BigDecimal grtEvalAmt) {
		this.grtEvalAmt = grtEvalAmt;
	}
	
    /**
     * @return GrtEvalAmt
     */	
	public java.math.BigDecimal getGrtEvalAmt() {
		return this.grtEvalAmt;
	}
	
	/**
	 * @param grtIsEnfv
	 */
	public void setGrtIsEnfv(String grtIsEnfv) {
		this.grtIsEnfv = grtIsEnfv == null ? null : grtIsEnfv.trim();
	}
	
    /**
     * @return GrtIsEnfv
     */	
	public String getGrtIsEnfv() {
		return this.grtIsEnfv;
	}
	
	/**
	 * @param grtNote
	 */
	public void setGrtNote(String grtNote) {
		this.grtNote = grtNote == null ? null : grtNote.trim();
	}
	
    /**
     * @return GrtNote
     */	
	public String getGrtNote() {
		return this.grtNote;
	}
	
	/**
	 * @param grtAmtFit
	 */
	public void setGrtAmtFit(String grtAmtFit) {
		this.grtAmtFit = grtAmtFit == null ? null : grtAmtFit.trim();
	}
	
    /**
     * @return GrtAmtFit
     */	
	public String getGrtAmtFit() {
		return this.grtAmtFit;
	}
	
	/**
	 * @param grtFitNote
	 */
	public void setGrtFitNote(String grtFitNote) {
		this.grtFitNote = grtFitNote == null ? null : grtFitNote.trim();
	}
	
    /**
     * @return GrtFitNote
     */	
	public String getGrtFitNote() {
		return this.grtFitNote;
	}
	
	/**
	 * @param grtChab
	 */
	public void setGrtChab(String grtChab) {
		this.grtChab = grtChab == null ? null : grtChab.trim();
	}
	
    /**
     * @return GrtChab
     */	
	public String getGrtChab() {
		return this.grtChab;
	}
	
	/**
	 * @param grtChabNote
	 */
	public void setGrtChabNote(String grtChabNote) {
		this.grtChabNote = grtChabNote == null ? null : grtChabNote.trim();
	}
	
    /**
     * @return GrtChabNote
     */	
	public String getGrtChabNote() {
		return this.grtChabNote;
	}
	
	/**
	 * @param grtHousePopNote
	 */
	public void setGrtHousePopNote(String grtHousePopNote) {
		this.grtHousePopNote = grtHousePopNote == null ? null : grtHousePopNote.trim();
	}
	
    /**
     * @return GrtHousePopNote
     */	
	public String getGrtHousePopNote() {
		return this.grtHousePopNote;
	}
	
	/**
	 * @param mAmt
	 */
	public void setMAmt(java.math.BigDecimal mAmt) {
		this.mAmt = mAmt;
	}
	
    /**
     * @return MAmt
     */	
	public java.math.BigDecimal getMAmt() {
		return this.mAmt;
	}
	
	/**
	 * @param dAmt
	 */
	public void setDAmt(java.math.BigDecimal dAmt) {
		this.dAmt = dAmt;
	}
	
    /**
     * @return DAmt
     */	
	public java.math.BigDecimal getDAmt() {
		return this.dAmt;
	}
	
	/**
	 * @param dAmtCvt
	 */
	public void setDAmtCvt(java.math.BigDecimal dAmtCvt) {
		this.dAmtCvt = dAmtCvt;
	}
	
    /**
     * @return DAmtCvt
     */	
	public java.math.BigDecimal getDAmtCvt() {
		return this.dAmtCvt;
	}
	
	/**
	 * @param mIncomeTot
	 */
	public void setMIncomeTot(java.math.BigDecimal mIncomeTot) {
		this.mIncomeTot = mIncomeTot;
	}
	
    /**
     * @return MIncomeTot
     */	
	public java.math.BigDecimal getMIncomeTot() {
		return this.mIncomeTot;
	}
	
	/**
	 * @param debtMRepayTot
	 */
	public void setDebtMRepayTot(java.math.BigDecimal debtMRepayTot) {
		this.debtMRepayTot = debtMRepayTot;
	}
	
    /**
     * @return DebtMRepayTot
     */	
	public java.math.BigDecimal getDebtMRepayTot() {
		return this.debtMRepayTot;
	}
	
	/**
	 * @param debtMRepayTotCvt
	 */
	public void setDebtMRepayTotCvt(java.math.BigDecimal debtMRepayTotCvt) {
		this.debtMRepayTotCvt = debtMRepayTotCvt;
	}
	
    /**
     * @return DebtMRepayTotCvt
     */	
	public java.math.BigDecimal getDebtMRepayTotCvt() {
		return this.debtMRepayTotCvt;
	}
	
	/**
	 * @param mDebtTot
	 */
	public void setMDebtTot(java.math.BigDecimal mDebtTot) {
		this.mDebtTot = mDebtTot;
	}
	
    /**
     * @return MDebtTot
     */	
	public java.math.BigDecimal getMDebtTot() {
		return this.mDebtTot;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}