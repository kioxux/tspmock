/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtReViewPortDto;
import cn.com.yusys.yusp.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-09 10:54:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtappr")
public class LmtApprResource {
    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;
    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;
    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;



	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppr> list = lmtApprService.selectAll(queryModel);
        return new ResultDto<List<LmtAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAppr>> index(QueryModel queryModel) {
        List<LmtAppr> list = lmtApprService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppr> show(@PathVariable("pkId") String pkId) {
        LmtAppr lmtAppr = lmtApprService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppr>(lmtAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppr> create(@RequestBody LmtAppr lmtAppr) throws URISyntaxException {
        lmtApprService.insert(lmtAppr);
        return new ResultDto<LmtAppr>(lmtAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppr lmtAppr) throws URISyntaxException {
        int result = lmtApprService.update(lmtAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getAllApproveInfo
     * @方法描述：获取批复所有数据，包括分项、产品、用信条件
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-5-13 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getAllApproveInfo")
    protected ResultDto<Map> getAllReplyInfo(@RequestBody Map param) {
        Map map = lmtApprService.getAllApproveInfo((String) param.get("apprSerno"));
        return new ResultDto<Map>(map);
    }

    /**
     * @方法名称：getAllApproveInfo
     * @方法描述：获取批复所有数据，包括分项、产品、用信条件
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-5-13 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getapproveinfobyserno")
    protected ResultDto<Map> getApproveInfoBySerno(@RequestBody String serno) {
        Map map = lmtApprService.getApproveInfoBySerno(serno);
        return new ResultDto<Map>(map);
    }

    /**
     * @方法名称：getAllApproveInfoqueryGuarBaseInfoByApprSerno
     * @方法描述：获取批复所有数据，包括分项、产品、用信条件
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-5-13 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getapproveinfobysernoandreporttype")
    protected ResultDto<Map> getApproveInfoBySernoAndReportType(@RequestBody Map queryMap) {
        Map map = lmtApprService.getApproveInfoBySernoAndReportType(queryMap);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryInfoBySerno")
    protected ResultDto<LmtAppr> queryInfoBySerno(@RequestBody String serno) {
        LmtAppr lmtAppr = lmtApprService.queryInfoBySerno(serno);
        return  ResultDto.success(lmtAppr);
    }

    /**
     * @函数名称:selectByApproveSerno
     * @函数描述:根据授信审批流水号查询授信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByApproveSerno")
    protected ResultDto<LmtAppr> selectByApproveSerno(@RequestBody String approveSerno) {
        LmtAppr lmtAppr = lmtApprService.selectByApproveSerno(approveSerno);
        return  ResultDto.success(lmtAppr);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryInfoBySernoAndIssueReportType")
    protected ResultDto<LmtAppr> queryInfoBySernoAndIssueReportType(@RequestBody Map map) {
        LmtAppr lmtAppr = lmtApprService.queryInfoBySernoAndIssueReportType(map);
        return  ResultDto.success(lmtAppr);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtAppr> save(@RequestBody LmtAppr lmtAppr) throws URISyntaxException {
        LmtAppr lmtAppr1 = lmtApprService.selectByPrimaryKey(lmtAppr.getPkId());
        lmtAppr.setOprType("01");
        if(lmtAppr1!=null){
            lmtApprService.updateSelective(lmtAppr);
        }else{
            lmtApprService.insert(lmtAppr);
        }
        return ResultDto.success(lmtAppr);
    }

    /**
     * @函数名称: queryGuarBaseInfo
     * @函数描述: 根据申请流水号查询抵质押品信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号查询抵质押品信息")
    @PostMapping("/queryGuarBaseInfo")
    protected ResultDto<List<LmtReViewPortDto>> queryGuarBaseInfo(@RequestBody QueryModel params) {
        List<LmtReViewPortDto> list = guarBaseInfoService.queryGuarBaseInfo(params);
        return new ResultDto<List<LmtReViewPortDto>>(list);
    }

    /**
     * @函数名称: queryGuarBaseInfo
     * @函数描述: 根据申请流水号查询抵质押品信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据审批流水号查询抵质押品信息")
    @PostMapping("/queryGuarBaseInfoByApprSerno")
    protected ResultDto<List<LmtReViewPortDto>> queryGuarBaseInfoByApprSerno(@RequestBody QueryModel params) {
        List<LmtReViewPortDto> list = guarBaseInfoService.queryGuarBaseInfoByApprSerno(params);
        return new ResultDto<List<LmtReViewPortDto>>(list);
    }

    /**
     * @函数名称: queryGuarBaseInfo
     * @函数描述: 根据申请流水号查询保证人信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号查询保证人信息")
    @PostMapping("/queryGuaranteeInfo")
    protected ResultDto<List<GuarGuarantee>> queryGuaranteeInfo(@RequestBody Map<String,String> params) {
        ArrayList<GuarGuarantee> list = guarGuaranteeService.queryGuaranteeInfo(params.get("serno"));
        return new ResultDto<List<GuarGuarantee>>(list);
    }

    /**
     * @函数名称:queryEntrustSubDataByParams
     * @函数描述:根据申请流水号查询项下品种委托贷款
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryEntrustSubDataByParams")
    protected ResultDto<Map> queryEntrustSubDataByParams(@RequestBody Map map) {
        Map listMap = lmtApprService.queryEntrustSubDataByParams(map);
        return  ResultDto.success(listMap);
    }

    /**
     * @函数名称:queryEntrustSubDataByApprSerno
     * @函数描述:根据审批流水号查询项下品种委托贷款
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryEntrustSubDataByApprSerno")
    protected ResultDto<Map> queryEntrustSubDataByApprSerno(@RequestBody Map map) {
        Map listMap = lmtApprService.queryEntrustSubDataByApprSerno(map);
        return  ResultDto.success(listMap);
    }

    /**
     * @函数名称: queryGuarBaseInfo
     * @函数描述: 根据申请流水号查询用信条件信息
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号查询用信条件信息")
    @PostMapping("/queryLoanCond")
    protected ResultDto<List<LmtApprLoanCond>> queryLoanCond(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprLoanCond> list = lmtApprLoanCondService.queryLoanCond(params.get("serno"),params.get("condType"));
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }

    /**
     * @函数名称: queryLoanCondByApprSerno
     * @函数描述: 根据审批中的申请流水号查询用信条件信息
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据审批中的申请流水号查询用信条件信息")
    @PostMapping("/queryLoanCondByApprSerno")
    protected ResultDto<List<LmtApprLoanCond>> queryLoanCondByApprSerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprLoanCond> list = lmtApprLoanCondService.queryLoanCondByApprSerno(params.get("apprSerno"),params.get("condType"));
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }

    /**
     * @函数名称: queryLoanCondByApprSerno
     * @函数描述: 根据审批中的申请流水号查询用信条件信息
     * @创建人: css
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据审批中的申请流水号查询用信条件信息")
    @PostMapping("/queryLoanCondByGrpApprSerno")
    protected ResultDto<List<Map>> queryLoanCondByGrpApprSerno(@RequestBody Map<String,String> params) {
        ArrayList<Map> list = lmtApprLoanCondService.queryLoanCondByGrpApprSerno(params.get("grpApproveSerno"),params.get("condType"));
        return new ResultDto<List<Map>>(list);
    }

    /**
     * @函数名称: queryLoanCondByGrpSerno
     * @函数描述: 根据集团申请流水号查询用信条件信息
     * @创建人: css
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询用信条件信息")
    @PostMapping("/queryLoanCondByGrpSerno")
    protected ResultDto<List<Map>> queryLoanCondByGrpSerno(@RequestBody Map<String,String> params) {
        ArrayList<Map> list = lmtApprLoanCondService.queryLoanCondByGrpSerno(params.get("grpSerno"),params.get("condType"));
        return new ResultDto<List<Map>>(list);
    }

    /**
     * @函数名称: queryRepayPlanByGrpApproveSerno
     * @函数描述: 根据集团申请流水号查询还款计划信息
     * @创建人: css
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团申请流水号查询还款计划信息")
    @PostMapping("/queryRepayPlanByGrpApproveSerno")
    protected ResultDto<List<Map>> queryRepayPlanByGrpApproveSerno(@RequestBody Map<String,String> params) {
        ArrayList<Map> list = lmtApprLoanCondService.queryRepayPlanByGrpApproveSerno(params);
        return new ResultDto<List<Map>>(list);
    }

    /**
     * @函数名称: queryLmtApprSubBySerno
     * @函数描述: 根据集团申请流水号查询授信分项品种数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号查询授信分项品种数据")
    @PostMapping("/queryLmtApprSubBySerno")
    protected ResultDto<List<LmtApprSubPrd>> queryLmtApprSubBySerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprSubPrd> list = lmtApprSubPrdService.queryLmtApprSubBySerno(params.get("serno"),params.get("lmtBizType"));
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称: updatelmtapprchoose
     * @函数描述: 根据流水号更新审批选择条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updatelmtapprchoose")
    protected ResultDto<Integer> updateLmtApprChoose(@RequestBody Map<String, Object> params) throws URISyntaxException {
        int result = lmtApprService.updateLmtApprChoose(params);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: saveLmtReplySubData
     * @函数描述: 更新批复金额信息
     * @创建人: css
     * @创建时间: 2021-09-04 17:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/savelmtreplysubdata")
    protected ResultDto<Integer> saveLmtReplySubData(@RequestBody Map<String, Object> params) throws URISyntaxException {
        int result = lmtApprService.saveLmtReplySubData(params);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: batchsavelmtreplysubdata
     * @函数描述: 批量更新批复金额信息
     * @创建人: css
     * @创建时间: 2021-09-04 17:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/batchsavelmtreplysubdata")
    protected ResultDto<Integer> batchSaveLmtReplySubData(@RequestBody List<Map<String, String>> params) throws URISyntaxException {
        int result = lmtApprService.batchSaveLmtReplySubData(params);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: queryLmtApprSubDataBySerno
     * @函数描述: 根据申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */
    @ApiOperation("根据申请流水号查询授信审批过程中分项品种数据")
    @PostMapping("/queryLmtApprSubDataBySerno")
    protected ResultDto<List<LmtApprSubPrd>> queryLmtApprSubDataBySerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprSubPrd> list = lmtApprSubPrdService.queryLmtApprSubDataBySerno(params);
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称: queryLmtApprSubDataByApprSerno
     * @函数描述: 根据审批流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */
    @ApiOperation("根据申请流水号查询授信审批过程中分项品种数据")
    @PostMapping("/queryLmtApprSubDataByApprSerno")
    protected ResultDto<List<LmtApprSubPrd>> queryLmtApprSubDataByApprSerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprSubPrd> list = lmtApprSubPrdService.queryLmtApprSubDataByApprSerno(params);
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称: queryLmtApprSubDataByGrpApproveSerno
     * @函数描述: 根据申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */
    @ApiOperation("根据集团申请流水号查询授信审批过程中分项品种数据")
    @PostMapping("/queryLmtApprSubDataByGrpApproveSerno")
    protected ResultDto<List<LmtApprSubPrd>> queryLmtApprSubDataByGrpApproveSerno(@RequestBody Map<String,String> params) {
        ArrayList<LmtApprSubPrd> list = lmtApprSubPrdService.queryLmtApprSubDataByGrpApproveSerno(params);
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称: afterInitControlPanelShowOrHide
     * @函数描述: 复审表后处理控制显隐
     * @创建人: css
     */
    @ApiOperation("复审表后处理控制显隐")
    @PostMapping("/afterinitcontrolpanelshoworhide")
    protected ResultDto<Map> afterInitControlPanelShowOrHide(@RequestBody Map<String,String> params) throws Exception {
        Map list = lmtApprSubPrdService.afterInitControlPanelShowOrHide(params);
        return new ResultDto<Map>(list);
    }

    /**
     * @函数名称: refshAppData
     * @函数描述: 重新拉去授信申请数据
     * @创建人: css
     */
    @ApiOperation("重新拉去授信申请数据")
    @PostMapping("/refshAppData")
    protected ResultDto<LmtAppr> refshAppData(@RequestBody Map<String,String> params) throws Exception {
        LmtAppr lmtAppr = lmtApprSubPrdService.refshAppData(params);
        return new ResultDto<LmtAppr>(lmtAppr);
    }

    /**
     * 更新核查报告文件路径
     * @return
     */
    @PostMapping("/updateFilePath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtApprService.updateFilePath(condition);
        return new ResultDto<>(result);
    }

}
