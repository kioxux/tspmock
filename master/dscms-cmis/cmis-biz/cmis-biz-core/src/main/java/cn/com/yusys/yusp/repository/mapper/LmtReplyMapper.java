/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.dto.server.xdls0004.resp.Xdls0004DataRespDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplyMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtReply selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReply> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtReply record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtReply record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtReply record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtReply record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称：selectForChangeReply
     * @方法描述：查可变更的批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-17 下午 5:39
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtReply> selectForChangeableReply(HashMap<String, String> paramMap);


    /**
     * @方法名称：getOriginReplyNo
     * @方法描述：根据台账批复号查批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-19 下午 1:47
     * @修改记录：修改时间 修改人员  修改原因
     */
    LmtReply getReply(@Param("replySerno") String replyNo);


    /**
     * @方法名称：getReplyNO
     * @方法描述：用原复号获取历史批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-19 上午 10:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    LmtReply getOriginReply(@Param("originReplySerno") String origiReplySerno);

    /**
     * @方法名称：getReplyByReplyNo
     * @方法描述：根据条件查询授信批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-22 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtReply> queryLmtReplyDataByParams(HashMap<String, String> paramMap);

    Xdls0004DataRespDto hasLmtReplyByCusId(String cusId);

    /**
     * @方法名称：queryLmtReplyDataByLmtGrpSerno
     * @方法描述：根据集团授信流水号查询成员授信批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtReply> queryLmtReplyDataByLmtGrpSerno(@Param("grpSerno") String originReplySerno);

    /**
     * 查询是否存在有效批复
     * @param cusId
     * @return
     */
    int selectEffectiveByCusId(@Param("cusId") String cusId);

    /**
     * 根据授信流水更新授信额度
     * @param map
     * @return
     */
    int updateLmtReplyAmtSerno(Map map);

    LmtReply querySingleLmtReplyByReplySerno(@Param("replySerno") String replySerno);

    List<LmtReply> getLmtReplyByGrpSernoIsDeclare(String grpSerno);

    /**
     * 根据授信流水删除授信批复
     * @param serno
     * @return
     */
    int deleteBySerno(String serno);

    LmtReply selectUsedReply(QueryModel queryModel);
}