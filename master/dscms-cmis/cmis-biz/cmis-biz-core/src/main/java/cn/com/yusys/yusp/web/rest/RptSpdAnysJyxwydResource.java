/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtRecheckDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwyd;
import cn.com.yusys.yusp.service.RptSpdAnysJyxwydService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysjyxwyd")
public class RptSpdAnysJyxwydResource {
    @Autowired
    private RptSpdAnysJyxwydService rptSpdAnysJyxwydService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysJyxwyd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysJyxwyd> list = rptSpdAnysJyxwydService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysJyxwyd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysJyxwyd>> index(QueryModel queryModel) {
        List<RptSpdAnysJyxwyd> list = rptSpdAnysJyxwydService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysJyxwyd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysJyxwyd> show(@PathVariable("serno") String serno) {
        RptSpdAnysJyxwyd rptSpdAnysJyxwyd = rptSpdAnysJyxwydService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysJyxwyd>(rptSpdAnysJyxwyd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysJyxwyd> create(@RequestBody RptSpdAnysJyxwyd rptSpdAnysJyxwyd) throws URISyntaxException {
        rptSpdAnysJyxwydService.insert(rptSpdAnysJyxwyd);
        return new ResultDto<RptSpdAnysJyxwyd>(rptSpdAnysJyxwyd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysJyxwyd rptSpdAnysJyxwyd) throws URISyntaxException {
        int result = rptSpdAnysJyxwydService.update(rptSpdAnysJyxwyd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysJyxwydService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysJyxwydService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysJyxwyd> save(@RequestBody RptSpdAnysJyxwyd rptSpdAnysJyxwyd) throws URISyntaxException {
        RptSpdAnysJyxwyd rptSpdAnysJyxwyd1 = rptSpdAnysJyxwydService.selectByPrimaryKey(rptSpdAnysJyxwyd.getSerno());
        if(rptSpdAnysJyxwyd1!=null){
            rptSpdAnysJyxwydService.update(rptSpdAnysJyxwyd);
        }else{
            rptSpdAnysJyxwydService.insert(rptSpdAnysJyxwyd);
        }
        return ResultDto.success(rptSpdAnysJyxwyd);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysJyxwyd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysJyxwyd rptSpdAnysJyxwyd = rptSpdAnysJyxwydService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysJyxwyd);
    }
}
