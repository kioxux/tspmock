package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpLoanSuspension;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.IqpLoanSuspensionService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 贷款停息申请审批后处理-东海
 *
 * @author macm
 * @date 2021/10/08 23:20
 **/
@Service
public class DHCZ40BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DHCZ40BizService.class);

    @Autowired
    private IqpLoanSuspensionService iqpLoanSuspensionService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String ilsSerno = resultInstanceDto.getBizId();
        IqpLoanSuspension loanSuspension = iqpLoanSuspensionService.selectByPrimaryKey(ilsSerno);
        log.info("贷款停息申请审批后业务处理类型" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "流程提交操作，流程参数" + resultInstanceDto);
                loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpLoanSuspensionService.updateSelective(loanSuspension);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "流程结束操作，流程参数" + resultInstanceDto);
                // 审批通过后发送核心记账
                log.info("贷款停息申请" + ilsSerno + "发送核心记账开始");
                iqpLoanSuspensionService.sendToHX(loanSuspension);
                log.info("贷款停息申请" + ilsSerno + "发送核心记账结束");
                //针对流程到办结节点，进行以下处理-修改业务数据状态为通过
                loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpLoanSuspensionService.updateSelective(loanSuspension);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpLoanSuspensionService.updateSelective(loanSuspension);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpLoanSuspensionService.updateSelective(loanSuspension);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpLoanSuspensionService.updateSelective(loanSuspension);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpLoanSuspensionService.updateSelective(loanSuspension);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("贷款停息申请" + ilsSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                loanSuspension.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpLoanSuspensionService.updateSelective(loanSuspension);
            } else {
                log.warn("贷款停息申请" + ilsSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DHCZ40.equals(flowCode);
    }
}
