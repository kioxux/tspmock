/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppRelCusDetailsLimit;
import cn.com.yusys.yusp.service.LmtAppRelCusDetailsLimitService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusDetailsLimitResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-19 10:09:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapprelcusdetailslimit")
public class LmtAppRelCusDetailsLimitResource {
    @Autowired
    private LmtAppRelCusDetailsLimitService lmtAppRelCusDetailsLimitService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppRelCusDetailsLimit>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppRelCusDetailsLimit> list = lmtAppRelCusDetailsLimitService.selectAll(queryModel);
        return new ResultDto<List<LmtAppRelCusDetailsLimit>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtAppRelCusDetailsLimit>> index(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" subSerno desc");
        }
        //资金业务存量授信
        List<LmtAppRelCusDetailsLimit> list = lmtAppRelCusDetailsLimitService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppRelCusDetailsLimit>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppRelCusDetailsLimit> show(@PathVariable("pkId") String pkId) {
        LmtAppRelCusDetailsLimit lmtAppRelCusDetailsLimit = lmtAppRelCusDetailsLimitService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppRelCusDetailsLimit>(lmtAppRelCusDetailsLimit);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppRelCusDetailsLimit> create(@RequestBody LmtAppRelCusDetailsLimit lmtAppRelCusDetailsLimit) throws URISyntaxException {
        lmtAppRelCusDetailsLimitService.insert(lmtAppRelCusDetailsLimit);
        return new ResultDto<LmtAppRelCusDetailsLimit>(lmtAppRelCusDetailsLimit);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppRelCusDetailsLimit lmtAppRelCusDetailsLimit) throws URISyntaxException {
        int result = lmtAppRelCusDetailsLimitService.update(lmtAppRelCusDetailsLimit);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppRelCusDetailsLimitService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppRelCusDetailsLimitService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPkId")
    protected ResultDto<LmtAppRelCusDetailsLimit> selectByPkId(@RequestBody Map condition) {
        String pkId = (String) condition.get("pkId");
        LmtAppRelCusDetailsLimit lmtAppRelCusDetailsLimit = lmtAppRelCusDetailsLimitService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppRelCusDetailsLimit>(lmtAppRelCusDetailsLimit);
    }
}
