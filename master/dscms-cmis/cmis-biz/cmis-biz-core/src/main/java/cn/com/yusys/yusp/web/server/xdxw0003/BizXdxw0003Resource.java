package cn.com.yusys.yusp.web.server.xdxw0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0003.req.List;
import cn.com.yusys.yusp.dto.server.xdxw0003.req.Xdxw0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0003.resp.Xdxw0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:小微贷前调查信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0003:小微贷前调查信息维护")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0003Resource.class);

    /**
     * 交易码：xdxw0003
     * 交易描述：小微贷前调查信息维护
     *
     * @param xdxw0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微贷前调查信息维护")
    @PostMapping("/xdxw0003")
    protected @ResponseBody
    ResultDto<Xdxw0003DataRespDto> xdxw0003(@Validated @RequestBody Xdxw0003DataReqDto xdxw0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003DataReqDto));
        Xdxw0003DataRespDto xdxw0003DataRespDto = new Xdxw0003DataRespDto();// 响应Dto:小微贷前调查信息维护
        ResultDto<Xdxw0003DataRespDto> xdxw0003DataResultDto = new ResultDto<>();
		Xdxw0003DataRespDto xdxw0003RespDto = new Xdxw0003DataRespDto(); //响应Data:小微贷前调查信息维护
        try {
            // 从xdxw0003DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
			String cusName = xdxw0003DataReqDto.getCusName();//客户名称
			String managerId = xdxw0003DataReqDto.getManagerId();//客户经理编号
			String serno = xdxw0003DataReqDto.getSerno();//流水号
			String surveyType = xdxw0003DataReqDto.getSurveyType();//表格类型
			String prdType = xdxw0003DataReqDto.getPrdType();//产品细分
			String apprStatus = xdxw0003DataReqDto.getApprStatus();//客户名
			String cusId = xdxw0003DataReqDto.getCusId();//身份证号
			String certNo = xdxw0003DataReqDto.getCertNo();//经营地址
			String operAddr = xdxw0003DataReqDto.getOperAddr();//经营地址（定位）
			String mainBusi = xdxw0003DataReqDto.getMainBusi();//主营业务
			String applyAmt = xdxw0003DataReqDto.getApplyAmt();//申请金额
			String applyTerm = xdxw0003DataReqDto.getApplyTerm();//申请期限
			String loanResn = xdxw0003DataReqDto.getLoanResn();//贷款原因
			String loanUse = xdxw0003DataReqDto.getLoanUse();//贷款用途
			String repayMode = xdxw0003DataReqDto.getRepayMode();//还款方式
			String grtMode = xdxw0003DataReqDto.getGrtMode();//担保方式
			String chrem = xdxw0003DataReqDto.getChrem();//现金/银行存款/理财
			String actrec = xdxw0003DataReqDto.getActrec();//应收账款
			String depositAmt = xdxw0003DataReqDto.getDepositAmt();//存货
			String equip = xdxw0003DataReqDto.getEquip();//设备（净值）
			String ownRealpro = xdxw0003DataReqDto.getOwnRealpro();//自有房产
			String ownCar = xdxw0003DataReqDto.getOwnCar();//自有车辆
			String debtAmt = xdxw0003DataReqDto.getDebtAmt();//负债总额
			String bankLoan = xdxw0003DataReqDto.getBankLoan();//银行借款
			String otherDebt = xdxw0003DataReqDto.getOtherDebt();//其他负债
			String outguar = xdxw0003DataReqDto.getOutguar();//对外担保
			String salesAmt = xdxw0003DataReqDto.getSalesAmt();//销售收入-总额
			String costAmt = xdxw0003DataReqDto.getCostAmt();//可变成本-总额
			String expendAmt = xdxw0003DataReqDto.getExpendAmt();//固定支出-总额
			String profit = xdxw0003DataReqDto.getProfit();//净利润
			BigDecimal mearn = xdxw0003DataReqDto.getMearn();//月均收入
			BigDecimal cash = xdxw0003DataReqDto.getCash();//现金
			BigDecimal dep = xdxw0003DataReqDto.getDep();//存款
			BigDecimal realpro = xdxw0003DataReqDto.getRealpro();//房产
			BigDecimal car = xdxw0003DataReqDto.getCar();//汽车
			BigDecimal indivLoan = xdxw0003DataReqDto.getIndivLoan();//私人借款
			BigDecimal outguaramt = xdxw0003DataReqDto.getOutguaramt();//对外担保
			BigDecimal totalIncome = xdxw0003DataReqDto.getTotalIncome();//总收入
			BigDecimal paybill = xdxw0003DataReqDto.getPaybill();//工资收入
			BigDecimal operProfit = xdxw0003DataReqDto.getOperProfit();//经营利润
			BigDecimal yearHomeSpend = xdxw0003DataReqDto.getYearHomeSpend();//年家庭开支
			String workUnitName = xdxw0003DataReqDto.getWorkUnitName();//工作单位名称
			List list = xdxw0003DataReqDto.getList();//start

            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0003DataRespDto对象开始
			//  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
			xdxw0003DataRespDto.setSurveyNo(StringUtils.EMPTY);// 调查流水号
			//  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdxw0003DataRespDto对象结束
            // 封装xdxw0003DataResultDto中正确的返回码和返回信息
            xdxw0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, e.getMessage());
            // 封装xdxw0003DataResultDto中异常返回码和返回信息
            xdxw0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0003DataRespDto到xdxw0003DataResultDto中
        xdxw0003DataResultDto.setData(xdxw0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003DataResultDto));
        return xdxw0003DataResultDto;
    }
}
