/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mybatis.annotation.IdentitySequence;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDelayAgreement
 * @类描述: iqp_delay_agreement数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 15:07:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_delay_agreement")
public class IqpDelayAgreement extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 协议流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "AGR_SERNO")
	private String agrSerno;
	
	/** 延期流水号 **/
	@Column(name = "DELAY_SERNO", unique = false, nullable = true, length = 40)
	private String delaySerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 还款间隔 **/
	@Column(name = "REPAY_INTERVAL", unique = false, nullable = true, length = 10)
	private Integer repayInterval;
	
	/** 生效截止日 **/
	@Column(name = "DELAY_END_DATE", unique = false, nullable = true, length = 20)
	private String delayEndDate;
	
	/** 是否立即生效 **/
	@Column(name = "VALID_IMME_FLAG", unique = false, nullable = true, length = 5)
	private String validImmeFlag;
	
	/** 补充协议签订日期 **/
	@Column(name = "AGR_SIGN_DATE", unique = false, nullable = true, length = 20)
	private String agrSignDate;
	
	/** 申请变更理由 **/
	@Column(name = "DELAY_RESN", unique = false, nullable = true, length = 65535)
	private String delayResn;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 接口发送状态 **/
	@Column(name = "CORE_STATUS", unique = false, nullable = true, length = 10)
	private String coreStatus;
	
	/** 错误信息 **/
	@Column(name = "ERROR_MESSAGE", unique = false, nullable = true, length = 200)
	private String errorMessage;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param agrSerno
	 */
	public void setAgrSerno(String agrSerno) {
		this.agrSerno = agrSerno;
	}
	
    /**
     * @return agrSerno
     */
	public String getAgrSerno() {
		return this.agrSerno;
	}
	
	/**
	 * @param delaySerno
	 */
	public void setDelaySerno(String delaySerno) {
		this.delaySerno = delaySerno;
	}
	
    /**
     * @return delaySerno
     */
	public String getDelaySerno() {
		return this.delaySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param repayInterval
	 */
	public void setRepayInterval(Integer repayInterval) {
		this.repayInterval = repayInterval;
	}
	
    /**
     * @return repayInterval
     */
	public Integer getRepayInterval() {
		return this.repayInterval;
	}
	
	/**
	 * @param delayEndDate
	 */
	public void setDelayEndDate(String delayEndDate) {
		this.delayEndDate = delayEndDate;
	}
	
    /**
     * @return delayEndDate
     */
	public String getDelayEndDate() {
		return this.delayEndDate;
	}
	
	/**
	 * @param validImmeFlag
	 */
	public void setValidImmeFlag(String validImmeFlag) {
		this.validImmeFlag = validImmeFlag;
	}
	
    /**
     * @return validImmeFlag
     */
	public String getValidImmeFlag() {
		return this.validImmeFlag;
	}
	
	/**
	 * @param agrSignDate
	 */
	public void setAgrSignDate(String agrSignDate) {
		this.agrSignDate = agrSignDate;
	}
	
    /**
     * @return agrSignDate
     */
	public String getAgrSignDate() {
		return this.agrSignDate;
	}
	
	/**
	 * @param delayResn
	 */
	public void setDelayResn(String delayResn) {
		this.delayResn = delayResn;
	}
	
    /**
     * @return delayResn
     */
	public String getDelayResn() {
		return this.delayResn;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param coreStatus
	 */
	public void setCoreStatus(String coreStatus) {
		this.coreStatus = coreStatus;
	}
	
    /**
     * @return coreStatus
     */
	public String getCoreStatus() {
		return this.coreStatus;
	}
	
	/**
	 * @param errorMessage
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
    /**
     * @return errorMessage
     */
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}