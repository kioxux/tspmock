package cn.com.yusys.yusp.web.server.xdxw0085;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0085.req.Xdxw0085DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0085.resp.Xdxw0085DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0085.Xdxw0085Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:共借人声明文本生产PDF接口
 *
 * @author zrcb
 * @version 1.0
 */
@Api(tags = "XDXW0085:共借人声明文本生产PDF接口")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0085Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0085Resource.class);

    @Autowired
    private Xdxw0085Service xdxw0085Service;

    /**
     * 交易码：xdxw0085
     * 交易描述：共借人声明文本生产PDF接口
     *
     * @param xdxw0085DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("共借人声明文本生产PDF接口")
    @PostMapping("/xdxw0085")
    protected @ResponseBody
    ResultDto<Xdxw0085DataRespDto> xdxw0085(@Validated @RequestBody Xdxw0085DataReqDto xdxw0085DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataReqDto));
        Xdxw0085DataRespDto xdxw0085DataRespDto = new Xdxw0085DataRespDto();// 响应Dto:优企贷还款账号变更
        ResultDto<Xdxw0085DataRespDto> xdxw0085DataResultDto = new ResultDto<>();
        try {
            String cusName = xdxw0085DataReqDto.getCusName();
            String signDate = xdxw0085DataReqDto.getSignDate();

            if(StringUtil.isEmpty(cusName)||StringUtil.isEmpty(signDate)){
                xdxw0085DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0085DataResultDto.setMessage("参数不能为空");
                return xdxw0085DataResultDto;
            }
            // 从xdxw0085DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataReqDto));
            xdxw0085DataRespDto = xdxw0085Service.xdxw0085(xdxw0085DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataRespDto));
            // 封装xdxw0085DataResultDto中正确的返回码和返回信息
            xdxw0085DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0085DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch(BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, e.getMessage());
            // 封装xdxw0085DataResultDto中异常返回码和返回信息
            xdxw0085DataResultDto.setCode(e.getErrorCode());
            xdxw0085DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, e.getMessage());
            // 封装xdxw0085DataResultDto中异常返回码和返回信息
            xdxw0085DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0085DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0085DataRespDto到xdxw0085DataResultDto中
        xdxw0085DataResultDto.setData(xdxw0085DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataResultDto));
        return xdxw0085DataResultDto;
    }
}
