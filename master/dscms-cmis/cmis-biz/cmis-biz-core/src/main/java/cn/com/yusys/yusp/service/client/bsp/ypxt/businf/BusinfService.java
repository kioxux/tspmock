package cn.com.yusys.yusp.service.client.bsp.ypxt.businf;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.BusinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.resp.BusinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/28 14:21
 * @desc 信贷业务合同信息同步
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BusinfService {
    private static final Logger logger = LoggerFactory.getLogger(BusinfService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * @param [businfReqDto]
     * @return cn.com.yusys.yusp.dto.client.esb.ypxt.businf.businfRespDto
     * @author 王玉坤
     * @date 2021/6/28 22:12
     * @version 1.0.0
     * @desc 信贷授信协议信息同步
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public BusinfRespDto businf(BusinfReqDto businfReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value, JSON.toJSONString(businfReqDto));
        ResultDto<BusinfRespDto> businf2ResultDto = dscms2YpxtClientService.businf(businfReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value, JSON.toJSONString(businf2ResultDto));

        String businfCode = Optional.ofNullable(businf2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String businfMeesage = Optional.ofNullable(businf2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        BusinfRespDto businfRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, businf2ResultDto.getCode())) {
            //  获取相关的值并解析
            businfRespDto = businf2ResultDto.getData();
        } else {//未查询到相关信息
            businf2ResultDto.setCode(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value);
        return businfRespDto;
    }
}
