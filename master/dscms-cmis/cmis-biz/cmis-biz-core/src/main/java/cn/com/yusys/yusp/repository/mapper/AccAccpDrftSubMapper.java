/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.dto.AccAccpDrftSubDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpDrftSubMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 10:19:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccAccpDrftSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccAccpDrftSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccAccpDrftSub> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccAccpDrftSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccAccpDrftSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccAccpDrftSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccAccpDrftSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCountNumByDrftNo
     * @方法描述: 根据票号查询是否存在该笔票据台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectAccAccpSubCountNumByDrftNo(Map QueryMap);

    /**
     * @方法名称: updateCountNumByDrftNo
     * @方法描述: 更新银承台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateAccAccpSubCountNumByDrftNo(Map QueryMap);

    /**
     * @方法名称：selecByBillContNo
     * @方法描述：根据银承台账的合同编号查询
     * @创建人：xs
     * @创建时间：2021/6/7 16:39
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccAccpDrftSub> selectByBillContNo(@Param("contNo")String billContNo);

    /**
     * @方法名称：selectInfoByCusIdDate
     * @方法描述：根据客户号灵活查询
     * @创建人：xs
     * @创建时间：2021/6/7 16:39
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据核心银承编号回显银承台账票据明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    AccAccpDrftSubDto selectByCoreBillNo(AccAccpDrftSub accAccpDrftSub);

    /**
     * @方法名称: selectByContNo
     * @方法描述: 根据合同编号查询票据明细台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    AccAccpDrftSubDto selectByContNo(@Param("contNo") String contNo);

    /**
     * 根据查询条件查询台账信息并返回
     * @param model
     * @return
     */
    List<AccAccpDrftSub> querymodelByCondition(QueryModel model);

    /**
     * @方法名称：selecByBillContNo
     * @方法描述：根据银承台账的借据编号查询
     * @创建人：qw
     * @创建时间：2021/8/20 16:39
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccAccpDrftSub> selectByBillNo(@Param("billNo")String billNo);

    /**
     * 根据合同编号查询票据明细台账信息
     * @param model
     * @return
     */
    List<AccAccpDrftSub> selectByContNo(QueryModel model);

    /**
     * @方法名称: selectUnClearByBillContNo
     * @方法描述: 根据合同编号查询票据明细台账总金额(未结清)
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectUnClearByBillContNo(String contNo);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPorderNo(AccAccpDrftSub record);

    /**
     * @方法名称: selectDetailsByCoreBillNo
     * @方法描述: 根据银承核心编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccAccpDrftSub> selectDetailsByCoreBillNo(@Param("coreBillNo")String coreBillNo);

    int updateAccStatusByCoreBillNoAndBillNo(AccAccpDrftSub accAccpDrftSub);
}