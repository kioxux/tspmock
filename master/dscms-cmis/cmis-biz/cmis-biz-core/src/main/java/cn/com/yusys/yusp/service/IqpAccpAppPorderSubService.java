package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.IqpAccpAppPorderSub;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpAccpAppPorderSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.IqpAccpAppPorderSubImportVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.poi.ss.usermodel.Workbook;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAccpAppPorderSubService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-19 10:08:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAccpAppPorderSubService {

    private static final Logger log = LoggerFactory.getLogger(IqpAccpAppPorderSubService.class);

    @Autowired
    private IqpAccpAppPorderSubMapper iqpAccpAppPorderSubMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAccpAppPorderSub selectByPrimaryKey(String pkId) {
        return iqpAccpAppPorderSubMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpAccpAppPorderSub> selectAll(QueryModel model) {
        List<IqpAccpAppPorderSub> records = (List<IqpAccpAppPorderSub>) iqpAccpAppPorderSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpAccpAppPorderSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("pyeeName desc");
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<IqpAccpAppPorderSub> list = iqpAccpAppPorderSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpAccpAppPorderSub record) {
        return iqpAccpAppPorderSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpAccpAppPorderSub record) {
        return iqpAccpAppPorderSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpAccpAppPorderSub record) {
        return iqpAccpAppPorderSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpAccpAppPorderSub record) {
        return iqpAccpAppPorderSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAccpAppPorderSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAccpAppPorderSubMapper.deleteByIds(ids);
    }

    /**
     * 银承票据明细新增页面
     *
     * @param iqpAccpAppPorderSub
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIaaPorderSubInfo(IqpAccpAppPorderSub iqpAccpAppPorderSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpAccpAppPorderSub == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpAccpAppPorderSub.setInputId(userInfo.getLoginCode());
                iqpAccpAppPorderSub.setInputBrId(userInfo.getOrg().getCode());
                iqpAccpAppPorderSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();
            iqpAccpAppPorderSub.setPkId(StringUtils.uuid(true));
            iqpAccpAppPorderSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            int insertCount = iqpAccpAppPorderSubMapper.insertSelective(iqpAccpAppPorderSub);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

        } catch (YuspException e) {
            log.error("银承票据明细新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存银承票据明细异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(IqpAccpAppPorderSub iqpAccpAppPorderSub) {
        iqpAccpAppPorderSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return iqpAccpAppPorderSubMapper.updateByPrimaryKey(iqpAccpAppPorderSub);
    }

    /**
     * 批量插入票据明细
     *
     * @param perWhiteInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertIqpAccpAppPorderSub22(List<IqpAccpAppPorderSubImportVo> perWhiteInfoList) {
        List<IqpAccpAppPorderSub> iqpAccpAppPorderSubList = (List<IqpAccpAppPorderSub>) BeanUtils.beansCopy(perWhiteInfoList, IqpAccpAppPorderSub.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            IqpAccpAppPorderSubMapper iqpAccpAppPorderSubMapper = sqlSession.getMapper(IqpAccpAppPorderSubMapper.class);
            for (IqpAccpAppPorderSub iqpAccpAppPorderSub : iqpAccpAppPorderSubList) {
                QueryModel query = new QueryModel();
                query.addCondition("drftNo", iqpAccpAppPorderSub.getDrftNo()); //票据号
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);  //操作类型
                List<IqpAccpAppPorderSub> iqpAccpAppPorderSubs = iqpAccpAppPorderSubMapper.selectByModel(query);
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(iqpAccpAppPorderSubs)) {
                    IqpAccpAppPorderSub bizIqpAccpAppPorderSub = iqpAccpAppPorderSubs.get(0);
                    //到期日期
                    bizIqpAccpAppPorderSub.setEndDate(iqpAccpAppPorderSub.getEndDate());
                    //最近修改人
                    bizIqpAccpAppPorderSub.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    bizIqpAccpAppPorderSub.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    bizIqpAccpAppPorderSub.setUpdDate(sf.format(new Date()));
                    //修改时间
                    bizIqpAccpAppPorderSub.setUpdateTime(new Date());
                    iqpAccpAppPorderSubMapper.updateByPrimaryKeySelective(bizIqpAccpAppPorderSub);
                } else {//数据库中不存在，新增信息
                    //生成主键
                    Map paramMap = new HashMap<>();
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    iqpAccpAppPorderSub.setPkId(pkValue);
                    //申请流水号,塞入模板工厂里面的流水号
//                    iqpAccpAppPorderSub.setSerno();
                    //票据号
//                    iqpAccpAppPorderSub.setDrftNo();
                    //票面金额,转换
                    iqpAccpAppPorderSub.setDrftAmt(iqpAccpAppPorderSub.getDrftAmt().multiply(new BigDecimal(10000)));
                    //操作类型
                    iqpAccpAppPorderSub.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //开始日期
                    iqpAccpAppPorderSub.setStartDate(sf.format(new Date()));
                    //登记人
                    iqpAccpAppPorderSub.setInputId(userInfo.getLoginCode());
                    //登记机构
                    iqpAccpAppPorderSub.setInputBrId(userInfo.getOrg().getCode());
                    //登记日期
                    iqpAccpAppPorderSub.setInputDate(sf.format(new Date()));
                    //创建时间
                    iqpAccpAppPorderSub.setCreateTime(new Date());
                    iqpAccpAppPorderSubMapper.insertSelective(iqpAccpAppPorderSub);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perWhiteInfoList.size();
    }

    /**
     * 批量插入 票据明细
     *
     * @param perIqpAccpAppPorderSubList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertIqpAccpAppPorderSub(List<Object> perIqpAccpAppPorderSubList, String serno) {
        List<IqpAccpAppPorderSubImportVo> iqpAccpAppPorderSubList = (List<IqpAccpAppPorderSubImportVo>) BeanUtils.beansCopy(perIqpAccpAppPorderSubList, IqpAccpAppPorderSubImportVo.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
//            PlaPlanDetailMapper plaPlanDetailMapper = sqlSession.getMapper(PlaPlanDetailMapper.class);
            IqpAccpAppPorderSubMapper iqpAccpAppPorderSubMapper = sqlSession.getMapper(IqpAccpAppPorderSubMapper.class);
            for (IqpAccpAppPorderSubImportVo iqpAccpAppPorderSub : iqpAccpAppPorderSubList) {
                IqpAccpAppPorderSub iqpAccpAppPorder = new IqpAccpAppPorderSub();
                String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());
                iqpAccpAppPorder.setPkId(pkValue);
                iqpAccpAppPorder.setDrftAmt(Optional.ofNullable(new BigDecimal(iqpAccpAppPorderSub.getDrftAmt())).orElse(BigDecimal.ZERO));
                iqpAccpAppPorder.setDrftNo(iqpAccpAppPorderSub.getDrftNo());
                iqpAccpAppPorder.setStartDate(iqpAccpAppPorderSub.getStartDate());
                iqpAccpAppPorder.setEndDate(iqpAccpAppPorderSub.getEndDate());
                iqpAccpAppPorder.setPyeeName(iqpAccpAppPorderSub.getPyeeName());
                iqpAccpAppPorder.setPyeeAcctsvcrName(iqpAccpAppPorderSub.getPyeeAcctsvcrName());
                iqpAccpAppPorder.setPyeeAccno(iqpAccpAppPorderSub.getPyeeAccno());
                iqpAccpAppPorder.setSerno(serno);
                //操作类型
                iqpAccpAppPorder.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                //登记人
                iqpAccpAppPorder.setInputId(userInfo.getLoginCode());
                //登记机构
                iqpAccpAppPorder.setInputBrId(userInfo.getOrg().getCode());
                //登记日期
                iqpAccpAppPorder.setInputDate(sf.format(new Date()));
                //创建时间
                iqpAccpAppPorder.setCreateTime(new Date());

                iqpAccpAppPorderSubMapper.insertSelective(iqpAccpAppPorder);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return perIqpAccpAppPorderSubList.size();
    }

    /**
     * 异步下载票据明细模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncexportIqpAccpAppPorderSubTemp() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(IqpAccpAppPorderSubImportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 根据传入参数查询数据
     *
     * @return 票据明细列表
     */

    public List<IqpAccpAppPorderSub> selectPorderSubByParams(Map map) {
        return iqpAccpAppPorderSubMapper.selectPorderSubByParams(map);
    }
}
