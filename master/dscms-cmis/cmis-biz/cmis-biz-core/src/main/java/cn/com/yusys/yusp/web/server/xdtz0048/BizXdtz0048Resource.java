package cn.com.yusys.yusp.web.server.xdtz0048;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0048.req.Xdtz0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.resp.Xdtz0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0048.Xdtz0048Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小贷借款借据文本生成pdf
 *
 * @author zoubiao
 * @version 1.0
 */
@Api(tags = "XDTZ0048:小贷借款借据文本生成pdf")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0048Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0048Resource.class);

    @Autowired
    private Xdtz0048Service xdtz0048Service;

    /**
     * 交易码：xdtz0048
     * 交易描述：小贷借款借据文本生成pdf
     *
     * @param xdtz0048DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷借款借据文本生成pdf")
    @PostMapping("/xdtz0048")
    protected @ResponseBody
    ResultDto<Xdtz0048DataRespDto> xdtz0048(@Validated @RequestBody Xdtz0048DataReqDto xdtz0048DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataReqDto));
        Xdtz0048DataRespDto xdtz0048DataRespDto = new Xdtz0048DataRespDto();// 响应Dto:小贷借款借据文本生成pdf
        ResultDto<Xdtz0048DataRespDto> xdtz0048DataResultDto = new ResultDto<>();
        try {
            String cusName = xdtz0048DataReqDto.getCusName();//客户名称
            String contNo = xdtz0048DataReqDto.getContNo();//合同编号
            String billNo = xdtz0048DataReqDto.getBillNo();//借据编号
            if (StringUtils.isBlank(cusName)) {
                xdtz0048DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0048DataResultDto.setMessage("客户名称【cusName】不能为空！");
                return xdtz0048DataResultDto;
            } else if (StringUtils.isBlank(contNo)) {
                xdtz0048DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0048DataResultDto.setMessage("合同编号【contNo】不能为空！");
                return xdtz0048DataResultDto;
            } else if (StringUtils.isBlank(billNo)) {
//                xdtz0048DataResultDto.setCode(EpbEnum.EPB099999.key);
//                xdtz0048DataResultDto.setMessage("借据编号不能为空【billNo】不能为空！");
//                return xdtz0048DataResultDto;
            }
            // 从xdtz0048DataReqDto获取业务值进行业务逻辑处理
            // 调用xdtz0048Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataReqDto));
            xdtz0048DataRespDto = xdtz0048Service.getXdtz0048(xdtz0048DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataReqDto));
            // 封装xdtz0048DataResultDto中正确的返回码和返回信息
            xdtz0048DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0048DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            // 封装xdtz0048DataResultDto中异常返回码和返回信息
            xdtz0048DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0048DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0048DataRespDto到xdtz0048DataResultDto中
        xdtz0048DataResultDto.setData(xdtz0048DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataRespDto));
        return xdtz0048DataResultDto;
    }
}
