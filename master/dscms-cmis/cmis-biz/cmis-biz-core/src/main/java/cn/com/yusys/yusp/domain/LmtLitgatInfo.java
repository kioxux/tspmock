/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtLitgatInfo
 * @类描述: lmt_litgat_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-17 22:48:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_litgat_info")
public class LmtLitgatInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 业务角色 **/
	@Column(name = "BUSI_ROLE", unique = false, nullable = true, length = 5)
	private String busiRole;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 案号 **/
	@Column(name = "CASENO", unique = false, nullable = true, length = 40)
	private String caseno;
	
	/** 诉讼地位 **/
	@Column(name = "LAWSUIT_STATS", unique = false, nullable = true, length = 20)
	private String lawsuitStats;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param busiRole
	 */
	public void setBusiRole(String busiRole) {
		this.busiRole = busiRole;
	}
	
    /**
     * @return busiRole
     */
	public String getBusiRole() {
		return this.busiRole;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param caseno
	 */
	public void setCaseno(String caseno) {
		this.caseno = caseno;
	}
	
    /**
     * @return caseno
     */
	public String getCaseno() {
		return this.caseno;
	}
	
	/**
	 * @param lawsuitStats
	 */
	public void setLawsuitStats(String lawsuitStats) {
		this.lawsuitStats = lawsuitStats;
	}
	
    /**
     * @return lawsuitStats
     */
	public String getLawsuitStats() {
		return this.lawsuitStats;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}