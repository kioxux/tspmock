package cn.com.yusys.yusp.web.server.xdtz0057;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0057.req.Xdtz0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.resp.Xdtz0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0057.Xdtz0057Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据流水号查询客户调查的放款信息（在途需求）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDTZ0057:根据流水号查询客户调查的放款信息（在途需求）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0057Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0057Resource.class);

    @Autowired
    private Xdtz0057Service xdtz0057Service;
    /**
     * 交易码：xdtz0057
     * 交易描述：根据流水号查询客户调查的放款信息（在途需求）
     *
     * @param xdtz0057DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询客户调查的放款信息（在途需求）")
    @PostMapping("/xdtz0057")
    protected @ResponseBody
    ResultDto<Xdtz0057DataRespDto> xdtz0057(@Validated @RequestBody Xdtz0057DataReqDto xdtz0057DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataReqDto));
        Xdtz0057DataRespDto xdtz0057DataRespDto = new Xdtz0057DataRespDto();// 响应Dto:根据流水号查询客户调查的放款信息（在途需求）
        ResultDto<Xdtz0057DataRespDto> xdtz0057DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataReqDto));
            xdtz0057DataRespDto = xdtz0057Service.getXdtz0057(xdtz0057DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataRespDto));
            // 封装xdtz0057DataResultDto中正确的返回码和返回信息
            xdtz0057DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0057DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException e) {
            xdtz0057DataResultDto.setCode(e.getErrorCode());
            xdtz0057DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, e.getMessage());
            // 封装xdtz0057DataResultDto中异常返回码和返回信息
            xdtz0057DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0057DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0057DataRespDto到xdtz0057DataResultDto中
        xdtz0057DataResultDto.setData(xdtz0057DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataResultDto));
        return xdtz0057DataResultDto;
    }
}
