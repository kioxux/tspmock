/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:34:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPkId(LmtApp record);

    /**
     * 交易码：selectLmtApplyCountByCusNo
     * 交易描述：判断是否存在在途的授信申请
     *
     * @param paramMap
     * @return
     * @throws Exception
     */
    int selectOnloadLmtApplyCountByCusNo(HashMap<String, String> paramMap);

    /**
     * 交易码：selectNotFdeddLmtSubApplyCountByCusNo
     * 交易描述：判断是否存在授信申请记录
     *
     * @param paramMap
     * @return
     * @throws Exception
     */
    int selectNotFdeddLmtSubApplyCountByCusNo(HashMap<String, String> paramMap);

    /**
     * 通过入参查询授信申请信息
     *
     * @param queryMap
     * @return
     */
    List<LmtApp> selectByParams(Map queryMap);

    /**
     * 通过流水号授信申请信息
     *
     * @param
     * @return
     */
    LmtApp selectBySerno(@Param("serno") String serno);

    /**
     * 通过客户号查询在途授信申请信息
     *
     * @param
     * @return
     */
    LmtApp selectOnWayByCusid(@Param("cusId") String cusId);

    /**
     * @方法名称: getLmtAppByModel
     * @方法描述: 获取当前客户经理名下所以授信申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtApp> getLmtAppByModel(QueryModel queryModel);

    List<LmtApp> getLmtApp(QueryModel model);

    List<LmtApp> getLmtAppHis(QueryModel model);

    List<LmtApp> getLmtModify();

    List<LmtApp> getLmtModifyHis();

    /**
     * @方法名称：getReplyByReplyNo
     * @方法描述：根据条件查询授信批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtApp> queryLmtReplyDataByParams(HashMap<String, String> paramMap);

    /**
     * 获取对公申请
     * @param cusId
     * @return
     */

    List findComApplyListByCusId(@Param("cusId") String cusId);
    /**
     * @方法名称: updateSxkdAmtBySerno
     * @方法描述: 根据流水号更新省心快贷金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateSxkdAmtBySerno(LmtApp record);
    /**
     * 根据流水号更新授信额度
     * @param map
     * @return
     */
    int updateLmtAmtBySerno(Map map);
    /**
     * @函数名称:deleteBySerno
     * @函数描述:根据授信流水号删除授信申请
     * @参数与返回说明:serno 授信流水号
     * @算法描述:
     */
    int deleteBySerno(String serno);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);
	
    /**
     * 通过授信申报流水号 更新项下成员的审批状态信息
     * @param map
     * @return
     */
    int updateLmtAppBySingleSerno(HashMap map);

    /**
     * XDTZ0012查询授信审批状态是否“审批中”
     * @param cusId
     * @return
     */
    int selectAppingByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: selectLmtGrpAppDataByLmtAppSerno
     * @方法描述: 根据成员客户申请流水号 查询 对应的集团客户授信申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<LmtGrpApp> selectLmtGrpAppDataByLmtAppSerno(@Param("serno") String cusId);

    /**
     * @函数名称:getAllLmtByInputId
     * @函数描述:根据客户经理工号查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> getAllLmtByInputId(QueryModel model);

    /**
     * @函数名称:selectLmtAppDataBySubSerno
     * @函数描述: 根据分项流水号查询授信类型
     * @参数与返回说明:
     * @param
     * @算法描述:
     */

    LmtApp selectLmtAppDataBySubSerno(@Param("subSerno") String subSerno);

    /**
     * @函数名称:selectLmtAppDataBySubSerno
     * @函数描述:根据集团授信流水号查询名下的成员客户申请信息
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    
    List<LmtApp> selectLmtAppByGrpSerno(HashMap hashMap);

    /**
     * 根据申请流水号集合查询名授信申请信息
     * @param hashMap
     * @return
     */
    List<LmtApp> getLmtAppBySernos(HashMap hashMap);

    /**
     * 根据集团流水号查询成员授信申请信息
     * @param grpSerno
     * @return
     */
    List<LmtApp> getLmtAppByGrpSerno(String grpSerno);

    /**
     * 根据集团流水号查询本次申报成员授信申请信息
     * @param grpSerno
     * @return
     */
    List<LmtApp> getLmtAppByGrpSernoIsDeclare(String grpSerno);

    /**
     * 根据本次流水号查询上次申请信息
     * @param serno
     * @return
     */
    LmtApp getLastSerno(String serno);

    /**
     * 根据客户号查询最新的授信申请
     * @param cusId
     * @return
     */
    LmtApp getLastLmtApp(String cusId);

    /**
     *通过流水号获取当前客户相关信息
     */

    Map getGuarRingDataBySerno(@Param("serno") String serno);
}