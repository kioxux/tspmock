/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanAppPrtcptBankSub;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppPrtcptBankSubMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppPrtcptBankSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-05-11 15:49:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanAppPrtcptBankSubService {

    @Autowired
    private IqpLoanAppPrtcptBankSubMapper iqpLoanAppPrtcptBankSubMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpLoanAppPrtcptBankSub selectByPrimaryKey(String pkId) {
        return iqpLoanAppPrtcptBankSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanAppPrtcptBankSub> selectAll(QueryModel model) {
        List<IqpLoanAppPrtcptBankSub> records = (List<IqpLoanAppPrtcptBankSub>) iqpLoanAppPrtcptBankSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpLoanAppPrtcptBankSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanAppPrtcptBankSub> list = iqpLoanAppPrtcptBankSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpLoanAppPrtcptBankSub record) {
        return iqpLoanAppPrtcptBankSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanAppPrtcptBankSub record) {
        return iqpLoanAppPrtcptBankSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpLoanAppPrtcptBankSub record) {
        return iqpLoanAppPrtcptBankSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanAppPrtcptBankSub record) {
        return iqpLoanAppPrtcptBankSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpLoanAppPrtcptBankSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanAppPrtcptBankSubMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询单于行信息
     * @参数与返回说明:
     * @算法描述:
     */

    public List<IqpLoanAppPrtcptBankSub> selectBySerno(QueryModel model) {
        return iqpLoanAppPrtcptBankSubMapper.selectBySerno(model);
    }

    /**
     * @函数名称:deleteByPkId
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */

    public int deleteByPkId(IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) {
        // 逻辑删除
        iqpLoanAppPrtcptBankSub.setOprType(CommonConstance.OPR_TYPE_DELETE);
        //获取当前登录信息
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        iqpLoanAppPrtcptBankSub.setUpdId(userInfo.getLoginCode());
        iqpLoanAppPrtcptBankSub.setUpdBrId(userInfo.getOrg().getCode());
        iqpLoanAppPrtcptBankSub.setUpdDate(nowDate);
        int result = updateSelective(iqpLoanAppPrtcptBankSub);
        return result;
    }

    /**
     * @函数名称:insertBySerno
     * @函数描述:通过流水号新增参与行信息
     * @参数与返回说明:
     * @算法描述:
     */

    public int insertBySerno(IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) {
        // 获取当前登录信息
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        // 赋值
        iqpLoanAppPrtcptBankSub.setOprType(CommonConstance.OPR_TYPE_ADD);
        iqpLoanAppPrtcptBankSub.setManagerId(userInfo.getLoginCode());
        iqpLoanAppPrtcptBankSub.setManagerBrId(userInfo.getOrg().getCode());
        iqpLoanAppPrtcptBankSub.setInputId(userInfo.getLoginCode());
        iqpLoanAppPrtcptBankSub.setInputBrId(userInfo.getOrg().getCode());
        iqpLoanAppPrtcptBankSub.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        iqpLoanAppPrtcptBankSub.setUpdId(userInfo.getLoginCode());
        iqpLoanAppPrtcptBankSub.setUpdBrId(userInfo.getOrg().getCode());
        iqpLoanAppPrtcptBankSub.setUpdDate(nowDate);
        iqpLoanAppPrtcptBankSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return insertSelective(iqpLoanAppPrtcptBankSub);
    }
}
