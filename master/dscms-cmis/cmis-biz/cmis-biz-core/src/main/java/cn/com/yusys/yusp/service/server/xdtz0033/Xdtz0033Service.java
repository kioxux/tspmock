package cn.com.yusys.yusp.service.server.xdtz0033;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0033.req.Xdtz0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.resp.Xdtz0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0033Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0033Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：Xdtz0033
     * 交易描述：查询客户所担保的行内贷款五级分类非正常状态件数
     *
     * @param xdtz0033DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0033DataRespDto xdtz0033(Xdtz0033DataReqDto xdtz0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033DataReqDto));
        //定义返回信息
        Xdtz0033DataRespDto xdtz0033DataRespDto = new Xdtz0033DataRespDto();
        try {
            //请求字段
            String cusId = xdtz0033DataReqDto.getCusId();

            //查询客户所担保的行内贷款五级分类非正常状态件数
            Integer count = accLoanMapper.queryFiveclassByCusId(cusId);
            if(count >0){
                xdtz0033DataRespDto.setGrtLoanNotNormalCnt(count.toString());
            }else{
                xdtz0033DataRespDto.setGrtLoanNotNormalCnt(SuccessEnum.CMIS_SUCCSESS.key);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033DataRespDto));
        return xdtz0033DataRespDto;
    }
}
