/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.IqpApplAppt;
import cn.com.yusys.yusp.domain.IqpApplApptRel;
import cn.com.yusys.yusp.dto.IqpApplApptDetailDto;
import cn.com.yusys.yusp.dto.IqpApplApptDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.IqpApplApptRelService;
import cn.com.yusys.yusp.service.IqpApplApptService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:27:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "业务申请人信息")
@RequestMapping("/api/iqpapplappt")
public class IqpApplApptResource {
    @Autowired
    private IqpApplApptService iqpApplApptService;
    @Autowired
    private IqpApplApptRelService iqpApplApptRelService;

    private static final Logger log = LoggerFactory.getLogger(IqpApplApptResource.class);

	/**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpApplAppt>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpApplAppt> list = iqpApplApptService.selectAll(queryModel);
        return new ResultDto<List<IqpApplAppt>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpApplAppt>> index(QueryModel queryModel) {
        List<IqpApplAppt> list = iqpApplApptService.selectByModel(queryModel);
        return new ResultDto<List<IqpApplAppt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象")
    @GetMapping("/{apptCode}")
    protected ResultDto<IqpApplAppt> show(@PathVariable("apptCode") String apptCode) {
        log.info("根据申请人code查询申请人信息开始入参{}"+JSON.toJSONString(apptCode));
        IqpApplAppt iqpApplAppt = iqpApplApptService.selectByPrimaryKey(apptCode);
        return new ResultDto<IqpApplAppt>(iqpApplAppt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpApplAppt> create(@RequestBody IqpApplAppt iqpApplAppt) throws URISyntaxException {
        iqpApplApptService.insert(iqpApplAppt);
        return new ResultDto<IqpApplAppt>(iqpApplAppt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpApplAppt iqpApplAppt) throws URISyntaxException {
        int result = iqpApplApptService.update(iqpApplAppt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{apptCode}")
    protected ResultDto<Integer> delete(@PathVariable("apptCode") String apptCode) {
        int result = iqpApplApptService.deleteByPrimaryKey(apptCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpApplApptService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 保存主借款人信息
     * @param iqpApplApptDetailDto
     * @return
     */
    @ApiOperation("保存主借款人信息")
    @PostMapping("/saveIqpApplApptDetailDto")
    protected ResultDto<IqpApplApptDetailDto> saveIqpApplApptDetailDto(@RequestBody IqpApplApptDetailDto iqpApplApptDetailDto){
        log.info("单笔单批业务申请人详细信息保存【{}】"+ JSON.toJSONString(iqpApplApptDetailDto));
        iqpApplApptService.saveIqpApplApptDetailDto(iqpApplApptDetailDto);
        return new ResultDto<>(iqpApplApptDetailDto);
    }

    /**
     * 删除相关人详细信息by 相关人编号
     * @param apptCode
     * @return
     */
    @ApiOperation("删除相关人详细信息by，相关人编号")
    @PostMapping("/deleteIqpApptOtherDetailDto")
    protected ResultDto<String> deleteIqpApptOtherDetailDto(@RequestParam(value = "apptCode") String apptCode,
                                                            @RequestParam(value = "iqpSerno") String iqpSerno){
        iqpApplApptService.deleteIqpApptOtherDetailByApptCode(apptCode,iqpSerno);
        return new ResultDto<>(apptCode);
    }
    /**
     * 单笔单批业务相关人资产汇总信息
     * @param iqpSerno
     * @return
     */
    @ApiOperation("单笔单批业务相关人资产汇总信息")
    @GetMapping("/getSumInfo/{iqpSerno}")
    protected ResultDto<List<HashMap<String,String>>> getSumInfo(@PathVariable("iqpSerno") String iqpSerno){
        log.info("单笔单批业务相关人资产汇总信息【{}】"+ iqpSerno);
        if(StringUtils.isBlank(iqpSerno)){
            throw new YuspException(EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
        }
        ResultDto<List<HashMap<String,String>>> resultDto = new ResultDto<>();
        resultDto.setData(iqpApplApptService.getSumInfo(iqpSerno));
        return resultDto;
    }

    /**
     * 根据申请人编号,查询申请人配偶编号
     * @param apptCode
     * @param iqpSerno
     * @return
     */
    @ApiOperation("根据申请人编号,查询申请人配偶编号")
    @GetMapping("/findWifeApptInfo/{apptCode}/{iqpSerno}")
    protected ResultDto<IqpApplApptRel> findWifeApptInfo(@PathVariable("apptCode") String apptCode,
                                                         @PathVariable("iqpSerno") String iqpSerno){
        log.info("根据申请人编号,查询申请人配偶编号start：{}"+iqpSerno+"/"+apptCode);
        return new ResultDto<IqpApplApptRel>(iqpApplApptRelService.findWifeApptInfo(iqpSerno,apptCode));
    }

    /**
     * 根据申请人编号,查询申请人配偶编号
     * @param iqpApplAppt
     * @return
     */
    @ApiOperation("根据业务流水号查询申请人信息")
    @PostMapping("/querybyiqpSerno")
    protected ResultDto<IqpApplAppt> queryByIqpSerno(@RequestBody IqpApplAppt iqpApplAppt){
        return new ResultDto<IqpApplAppt>(iqpApplApptService.selectByPrimaryKey(iqpApplAppt.getIqpSerno()));
    }

}
