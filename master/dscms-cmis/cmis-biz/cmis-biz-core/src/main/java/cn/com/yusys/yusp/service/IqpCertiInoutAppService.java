/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.domain.IqpCertiInoutApp;
import cn.com.yusys.yusp.domain.IqpCertiInoutRel;
import cn.com.yusys.yusp.dto.GuarCertiRelaClientDto;
import cn.com.yusys.yusp.dto.GuarClientRsDto;
import cn.com.yusys.yusp.dto.IqpCertiInoutAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpCertiInoutAppMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-20 15:04:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpCertiInoutAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpCertiInoutAppService.class);
    @Autowired
    private IqpCertiInoutAppMapper iqpCertiInoutAppMapper;
    @Autowired
    private IqpCertiInoutRelService iqpCertiInoutRelService;
    // @Autowired
    // private IGuarClientService iGuarClientService;//押品端服务接口
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private GrtGuarContService grtGuarContService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpCertiInoutApp selectByPrimaryKey(String serno) {
        return iqpCertiInoutAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpCertiInoutApp> selectAll(QueryModel model) {
        List<IqpCertiInoutApp> records = (List<IqpCertiInoutApp>) iqpCertiInoutAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpCertiInoutApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpCertiInoutApp> list = iqpCertiInoutAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpCertiInoutApp record) {
        return iqpCertiInoutAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpCertiInoutApp record) {
        return iqpCertiInoutAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpCertiInoutApp record) {
        return iqpCertiInoutAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpCertiInoutApp record) {
        return iqpCertiInoutAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return iqpCertiInoutAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpCertiInoutAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateSelectByParmas
     * @方法描述: 保存权证出入库申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateSelectByParmas(Map params) throws Exception {
        int rtnData = 0;
        try {
            Map card = (Map) params.get("card");
            List list = (List) params.get("list");
            String serno = (String) card.get("serno");
            String opeType = (String) card.get("opeType");
            //权证待入库申请保存，需校验所选择入库的所有押品必须有一个 权证类别 = 他项权证 的权证
            if (CmisBizConstants.OPT_TYPE_ON_STORAGE.equals(opeType)) {
                log.info("权证出入库申请" + serno + "调用押品端接口开始");
                GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
                guarCertiRelaClientDto.setList(list);
                guarCertiRelaClientDto.setOpeType(opeType);
                log.info("接口请求入参" + JSON.toJSONString(serno));
                GuarClientRsDto guarClientRsDto = null;//iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
                log.info("权证出入库申请" + serno + "调用押品端接口结束");
                if (guarClientRsDto == null) {
                    throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
                }
                String rtnCode = guarClientRsDto.getRtnCode();
                if (!EcbEnum.IQP_CHG_SUCCESS_DEF.key.equals(rtnCode)) {
                    throw new YuspException(EcbEnum.GUAR_CERTI_RELA_SELECTCERTICATALOG_EXCEPTION.key, EcbEnum.GUAR_CERTI_RELA_SELECTCERTICATALOG_EXCEPTION.value);
                }
            }
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.IQP_CERI_GETSERNO_EXCEPTION.key, EcbEnum.IQP_CERI_GETSERNO_EXCEPTION.value);
            }
            IqpCertiInoutApp iqpCertiInoutApp = iqpCertiInoutAppMapper.selectBySernoKey(serno);
            IqpCertiInoutApp iqpCertiInfo = new IqpCertiInoutApp();
            iqpCertiInfo = JSONObject.parseObject(JSON.toJSONString(card), IqpCertiInoutApp.class);
            if (iqpCertiInoutApp == null) {
                iqpCertiInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                iqpCertiInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                iqpCertiInfo.setCertiStatu((String) card.get("certiState"));
                iqpCertiInoutAppMapper.insert(iqpCertiInfo);
                IqpCertiInoutRel iqpCertiRel = new IqpCertiInoutRel();
                BeanUtils.copyProperties(iqpCertiInfo, iqpCertiRel);
                for (int i = 0; i < list.size(); i++) {
                    Map newpkId = (Map) list.get(i);
                    iqpCertiRel.setPkId(StringUtils.uuid(true));
                    iqpCertiRel.setCertiPkId((String) newpkId.get("pkId"));
                    iqpCertiRel.setGuarNo((String) newpkId.get("guarNo"));
                    rtnData = iqpCertiInoutRelService.insert(iqpCertiRel);
                }
                return rtnData;
            }
            rtnData = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInfo);
            if (rtnData == 0) {
                throw new YuspException(EcbEnum.IQP_CERI_SAVE_EXCEPTION.key, EcbEnum.IQP_CERI_SAVE_EXCEPTION.value);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtnData;
    }


    /**
     * 审批流程初始节点提交操作
     * 1、更新权证出入库申请的权证出入库类型变更权证类型
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataRun(String serno) {
        try {
            log.info("流程审批更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            String certiPkIds = iqpCertiInoutRelService.querySelectBySerno(serno);
            if (certiPkIds == null) {
                throw new YuspException(EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.key, EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.value);
            }
            log.info("审批通过更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            IqpCertiInoutApp iqpCertiInoutApp = iqpCertiInoutAppMapper.selectByPrimaryKey(serno);
            if (iqpCertiInoutApp == null) {
                throw new YuspException(EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.key, EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.value);
            }
            iqpCertiInoutApp.setCertiPkId(certiPkIds);
            String inoutApptype = iqpCertiInoutApp.getInoutApptype();
            String certiStatu = iqpCertiInoutApp.getCertiStatu();
            //权证出入库类型为正常入库且权证状态为待入库，权证状态更新入库在途
            if (CmisBizConstants.IQP_INOUT_APPTYPE_05.equals(inoutApptype) && (CmisBizConstants.IQP_CERTI_STATU_01.equals(certiStatu) || CmisBizConstants.IQP_CERTI_STATU_02.equals(certiStatu))) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_02);
            }//权证出入库类型为正常出库、取出还贷且权证状态为入库已记账,权证状态更新出库在途
            else if ((CmisBizConstants.IQP_INOUT_APPTYPE_01.equals(inoutApptype) || CmisBizConstants.IQP_INOUT_APPTYPE_04.equals(inoutApptype)) && (CmisBizConstants.IQP_CERTI_STATU_04.equals(certiStatu) || CmisBizConstants.IQP_CERTI_STATU_08.equals(certiStatu))) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_08);
            }//权证出入库类型为换证出库且权证状态为入库已记账,权证状态更新换证在途
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_03.equals(inoutApptype) && (CmisBizConstants.IQP_CERTI_STATU_04.equals(certiStatu) || CmisBizConstants.IQP_CERTI_STATU_08.equals(certiStatu))) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_12);
            }//权证出入库类型为出借且权证状态为入库已记账,权证状态更新出借在途
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_02.equals(inoutApptype) && (CmisBizConstants.IQP_CERTI_STATU_04.equals(certiStatu)) || CmisBizConstants.IQP_CERTI_STATU_05.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_05);
            }//权证出入库类型为归还入库且权证状态为出借已记账,权证状态更新入库在途
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_06.equals(inoutApptype) && (CmisBizConstants.IQP_CERTI_STATU_04.equals(certiStatu)) || CmisBizConstants.IQP_CERTI_STATU_07.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_07);
            }//权证出入库类型为换证入库且权证状态为出库已记账,权证状态更新入库在途
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_07.equals(inoutApptype) && (CmisBizConstants.IQP_CERTI_STATU_10.equals(certiStatu)) || CmisBizConstants.IQP_CERTI_STATU_02.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_02);
            }
            iqpCertiInoutApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
            iqpCertiInoutApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            int result = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInoutApp);
            if (result < 0) {
                throw new YuspException(EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.key, EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("权证出入库申请流程业务处理发生异常！", e);
            throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
        }
    }

    /**
     * 审批通过后的业务流程操作
     * 1、更新权证出入库申请的权证出入库类型变更权证类型
     * 2、更新押品的担保权证表
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String serno) {
        try {
            log.info("流程审批更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            String certiPkIds = iqpCertiInoutRelService.querySelectBySerno(serno);
            if (certiPkIds == null) {
                throw new YuspException(EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.key, EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.value);
            }
            log.info("审批通过更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            IqpCertiInoutApp iqpCertiInoutApp = iqpCertiInoutAppMapper.selectByPrimaryKey(serno);
            if (iqpCertiInoutApp == null) {
                throw new YuspException(EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.key, EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.value);
            }
            iqpCertiInoutApp.setCertiPkId(certiPkIds);
            String inoutApptype = iqpCertiInoutApp.getInoutApptype();
            String certiStatu = iqpCertiInoutApp.getCertiStatu();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            //权证出入库类型为正常入库且权证状态为入库在途，权证状态更新入库未记账
            if (CmisBizConstants.IQP_INOUT_APPTYPE_05.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_02.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_03);
                iqpCertiInoutApp.setInDate(nowDate);
            }//权证出入库类型为正常出库、取出还贷且权证状态为出库在途,权证状态更新出库未记账
            else if ((CmisBizConstants.IQP_INOUT_APPTYPE_01.equals(inoutApptype) || CmisBizConstants.IQP_INOUT_APPTYPE_04.equals(inoutApptype)) && CmisBizConstants.IQP_CERTI_STATU_08.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_09);
                iqpCertiInoutApp.setOutDate(nowDate);
            }//权证出入库类型为换证出库且权证状态为换证在途,权证状态更新出库未记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_03.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_12.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_09);
                iqpCertiInoutApp.setOutDate(nowDate);
            }//权证出入库类型为出借且权证状态为出借在途,权证状态更新出借未记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_02.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_05.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_06);
                iqpCertiInoutApp.setOutDate(nowDate);
            }//权证出入库类型为归还入库且权证状态为入库在途,权证状态更新入库未记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_06.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_07.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_03);
                iqpCertiInoutApp.setInDate(nowDate);
                iqpCertiInoutApp.setRealBackDate(nowDate);
            }//权证出入库类型为换证入库且权证状态为入库在途,权证状态更新入库未记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_07.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_02.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_03);
                iqpCertiInoutApp.setInDate(nowDate);
                iqpCertiInoutApp.setRealBackDate(nowDate);
            } else {
                throw new YuspException(EcbEnum.AFTEREND_IQPCERTIINOUTAPP_INOUT_APPTYPE_EXCEPTION.key, EcbEnum.AFTEREND_IQPCERTIINOUTAPP_INOUT_APPTYPE_EXCEPTION.value);
            }
            iqpCertiInoutApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            iqpCertiInoutApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            int result = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInoutApp);
            if (result < 0) {
                throw new YuspException(EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.key, EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.value);
            }
            handleBusinessDataCertiStatus(iqpCertiInoutApp);
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("权证出入库申请流程业务处理发生异常！", e);
            throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
        }
    }

    /**
     * 审批退回至初始、打回至初始、否决节点后的业务流程操作
     * 1、更新权证出入库申请的权证出入库类型变更权证类型
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataReturnBack(String serno, String approveStatus) {
        try {
            log.info("流程审批更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            String certiPkIds = iqpCertiInoutRelService.querySelectBySerno(serno);
            if (certiPkIds == null) {
                throw new YuspException(EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.key, EcbEnum.AFTEREND_IQPCERTIINOUTREL_EXCEPTION.value);
            }
            log.info("审批通过更新权证出入库申请信息-获取权证信息" + serno + "获取权证信息");
            IqpCertiInoutApp iqpCertiInoutApp = iqpCertiInoutAppMapper.selectByPrimaryKey(serno);
            if (iqpCertiInoutApp == null) {
                throw new YuspException(EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.key, EcbEnum.AFTEREND_GUARCERTIRELA_EXCEPTION.value);
            }
            iqpCertiInoutApp.setCertiPkId(certiPkIds);
            String inoutApptype = iqpCertiInoutApp.getInoutApptype();
            String certiStatu = iqpCertiInoutApp.getCertiStatu();
            //权证出入库类型为正常入库且权证状态为入库在途，权证状态更新待入库
            if (CmisBizConstants.IQP_INOUT_APPTYPE_05.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_02.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_01);
            }//权证出入库类型为正常出库、取出还贷且权证状态为出库在途,权证状态更新入库已记账
            else if ((CmisBizConstants.IQP_INOUT_APPTYPE_01.equals(inoutApptype) || CmisBizConstants.IQP_INOUT_APPTYPE_04.equals(inoutApptype)) && CmisBizConstants.IQP_CERTI_STATU_08.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_04);
            }//权证出入库类型为换证出库且权证状态为换证在途,权证状态更新入库已记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_03.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_12.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_04);
            }
            //权证出入库类型为出借且权证状态为出借在途,权证状态更新入库已记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_02.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_05.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_04);
            }//权证出入库类型为归还入库且权证状态为入库在途,权证状态更新出借已记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_06.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_07.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_05);
            }
            //权证出入库类型为换证入库且权证状态为入库在途,权证状态更新出库已记账
            else if (CmisBizConstants.IQP_INOUT_APPTYPE_07.equals(inoutApptype) && CmisBizConstants.IQP_CERTI_STATU_11.equals(certiStatu)) {
                iqpCertiInoutApp.setCertiStatu(CmisBizConstants.IQP_CERTI_STATU_10);
            } else {
                throw new YuspException(EcbEnum.AFTEREND_IQPCERTIINOUTAPP_INOUT_APPTYPE_EXCEPTION.key, EcbEnum.AFTEREND_IQPCERTIINOUTAPP_INOUT_APPTYPE_EXCEPTION.value);
            }
            if (approveStatus.equals(CmisCommonConstants.WF_STATUS_992)) {
                iqpCertiInoutApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
            } else if (approveStatus.equals(CmisCommonConstants.WF_STATUS_991)) {
                iqpCertiInoutApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
            } else if (approveStatus.equals(CmisCommonConstants.WF_STATUS_998)) {
                iqpCertiInoutApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            }
            iqpCertiInoutApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            int result = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInoutApp);
            if (result < 0) {
                throw new YuspException(EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.key, EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("权证出入库申请流程业务处理发生异常！", e);
            throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
        }
    }

    /**
     * 审批流程业务操作
     * 1、更新押品的担保权证表信息
     *
     * @param iqpCertiInoutApp
     */
    public void handleBusinessDataCertiStatus(IqpCertiInoutApp iqpCertiInoutApp) {
        String certiPkIds = iqpCertiInoutApp.getCertiPkId();
        log.info("权证出入库申请" + certiPkIds + "调用押品端接口开始");
        //定义交互的dto
        GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
        GuarClientRsDto guarClientRsDto = new GuarClientRsDto();
        BeanUtils.copyProperties(iqpCertiInoutApp, guarCertiRelaClientDto);
        String[] certiPkIdAry = certiPkIds.split(",");
        //传参多个权证主键
        List<Map> list = new ArrayList<>();
        for (int j = 0; j < certiPkIdAry.length; j++) {
            String certiPkId = certiPkIdAry[j];
            //将json字符串转为map对象
            Map map = new HashMap();
            map.put("pkId", certiPkId);
            list.add(map);
        }
        guarCertiRelaClientDto.setList(list);
        guarCertiRelaClientDto.setDepositOrgNo(iqpCertiInoutApp.getKeepBrId());
        guarCertiRelaClientDto.setCertiState(iqpCertiInoutApp.getCertiStatu());
        guarClientRsDto.setData(guarCertiRelaClientDto);
        log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
        guarClientRsDto = null;//iGuarClientService.udpateGuarCertiRela(guarCertiRelaClientDto);
        log.info("权证出入库申请" + certiPkIds + "调用押品端接口结束");
        if (guarClientRsDto == null) {
            throw new YuspException(EcbEnum.GUAR_NULL_EXCEPTION.key, EcbEnum.GUAR_NULL_EXCEPTION.value);
        }
    }

    /**
     * 逻辑删除权证申请
     **/
    public int deleteSelectiveByParams(Map params) throws Exception {
        int rtData = 0;
        try {
            String serno = (String) params.get("serno");
            IqpCertiInoutApp iqpCertiInout = iqpCertiInoutAppMapper.selectBySernoKey(serno);
            if (iqpCertiInout == null) {
                throw new YuspException(EcbEnum.IQP_CERI_INSERT_EXCEPTION.key, EcbEnum.IQP_CERI_INSERT_EXCEPTION.value);
            }
            IqpCertiInoutApp iqpCertiInoutApp = new IqpCertiInoutApp();
            iqpCertiInoutApp = JSONObject.parseObject(JSON.toJSONString(params), IqpCertiInoutApp.class);
            iqpCertiInoutApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            rtData = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInoutApp);
            if (rtData == 0) {
                throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
            }
            IqpCertiInoutRel iqpCertiInoutRel = new IqpCertiInoutRel();
            BeanUtils.copyProperties(iqpCertiInoutApp, iqpCertiInoutRel);
            rtData = iqpCertiInoutRelService.updateSelective(iqpCertiInoutRel);
            if (rtData == 0) {
                throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtData;
    }

    /**
     * 通过入参查询符合条件的权证出入库申请
     *
     * @param params
     * @return
     */
    public String getQueryInfo(Map params) {
        String rtnData = "";
        String guarNo = (String) params.get("guarNo");
        String guarNoBiz = "";
        String guarNoGuar = "";
        //根据担保合同编号/借款人查询
        if (StringUtils.nonBlank((String) params.get("guarContNo")) || StringUtils.nonBlank((String) params.get("borrowerId"))) {
            guarNoBiz = queryByGuarContNo(params);
            if (StringUtils.isBlank(guarNoBiz)) {
                log.info("权证出入库申请" + params + "不存在有效的权证出入库申请");
                return rtnData;
            }
        }
        //根据担保分类代码查询
        if (StringUtils.nonBlank((String) params.get("guarTypeCd")) || StringUtils.nonBlank(guarNo) || StringUtils.nonBlank((String) params.get("pldimnMemo"))) {
            guarNoGuar = queryByGuarTypeCd(params);
            if (StringUtils.isBlank(guarNoGuar)) {
                log.info("权证出入库申请" + params + "不存在有效的权证出入库申请");
                return rtnData;
            }
        }
        if (StringUtils.nonBlank(guarNoGuar) && StringUtils.nonBlank(guarNoBiz)) {
            //取出相同元素的押品编号
            String[] arr1 = guarNoBiz.split(CmisCommonConstants.COMMON_SPLIT_COMMA);
            String[] arr2 = guarNoGuar.split(CmisCommonConstants.COMMON_SPLIT_COMMA);
            StringBuffer newGuarNo = new StringBuffer();

            for (int i = 0; i < arr2.length; i++) {
                for (int j = 0; j < arr1.length; j++) {
                    if (arr1[j].equals(arr2[i])) {
                        newGuarNo.append(arr1[j] + CmisCommonConstants.COMMON_SPLIT_COMMA);
                    }
                }
            }
            guarNo = newGuarNo.toString().substring(0, newGuarNo.toString().length() - 1);
        }
        if (StringUtils.nonBlank(guarNoGuar)) {
            guarNo = guarNoGuar;
        }
        if (StringUtils.nonBlank(guarNoBiz)) {
            guarNo = guarNoBiz;
        }
        List<Map> list = new ArrayList<>();
        Map guarNoMap = new HashMap();

        String[] guarNoAry = guarNo.split(CmisCommonConstants.COMMON_SPLIT_COMMA);
        for (int i = 0; i < guarNoAry.length; i++) {
            guarNoMap.put("guarNo", guarNoAry[i]);
            list.add(guarNoMap);
        }
        GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
        guarCertiRelaClientDto.setList(list);
        log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口开始");
        log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
        GuarClientRsDto guarClientRsDto = null;//iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
        log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口结束");
        String certiPkId = "";
        if (guarClientRsDto != null) {
            if (EcbEnum.IQP_CHG_SUCCESS_DEF.key.equals(guarClientRsDto.getRtnCode())) {
                List<Map> guarCertiRelaClientDtoList = (List<Map>) guarClientRsDto.getData();
                if (CollectionUtils.isEmpty(guarCertiRelaClientDtoList) || guarCertiRelaClientDtoList.size() == 0) {
                    log.info("权证出入库申请" + guarCertiRelaClientDto + "押品权证不存在");
                    return certiPkId;
                }

                for (Map guarCertiRelaClientDtos : guarCertiRelaClientDtoList) {
                    certiPkId = certiPkId + guarCertiRelaClientDtos.get("certiPkId") + CmisCommonConstants.COMMON_SPLIT_COMMA;
                }

                if (StringUtils.nonBlank(guarNo) || StringUtils.nonBlank(certiPkId)) {
                    certiPkId = certiPkId.substring(0, certiPkId.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
                }
            }
        }
        return iqpCertiInoutRelService.queryIqpCertiInOutApp(guarNo, (String) params.get("optType"), certiPkId);
    }

    /**
     * 通过担保合同编号/借款人编号查询符合条件的权证出入库申请
     *
     * @param
     * @return
     */

    public String queryByGuarContNo(Map params) {
        String rtnData = "";
        List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectGrtGuarCont(params);
        if (CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0) {
            log.info("权证出入库申请" + params + "担保合同项下不存在有效的权证信息");
            return rtnData;
        }
        for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
            rtnData = rtnData + grtGuarContRel.getGuarNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }
        if (StringUtils.nonBlank(rtnData)) {
            rtnData = rtnData.substring(0, rtnData.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }
        return rtnData;
    }

    /**
     * 通过担保分类代码/抵押物名称查询符合条件的权证出入库申请
     *
     * @param
     * @return
     */

    public String queryByGuarTypeCd(Map params) {
        String rtnData = "";
        log.info("权证出入库申请" + params + "调用押品端接口开始");
        log.info("接口请求入参" + JSON.toJSONString(params));
        GuarClientRsDto guarClientRsDto = null;//iGuarClientService.getGuarBaseInfoByParams(params);
        log.info("权证出入库申请" + params + "调用押品端接口结束");
        if (guarClientRsDto != null) {
            String rtnCode = guarClientRsDto.getRtnCode();
            if (EcbEnum.IQP_CHG_SUCCESS_DEF.key.equals(rtnCode)) {
                List<Map> guarClientRsDtoList = (List<Map>) guarClientRsDto.getData();
                if (CollectionUtils.isEmpty(guarClientRsDtoList) || guarClientRsDtoList.size() == 0) {
                    log.error("调用接口返回异常；异常编码：" + guarClientRsDto.getRtnCode() + "；异常描述：" + guarClientRsDto.getRtnMsg());
                    throw new YuspException(EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.key, EcbEnum.GUAR_GUARCERTIRELA_EXCEPTION.value);
                }
                for (Map guarClientRsDtos : guarClientRsDtoList) {
                    rtnData = rtnData + guarClientRsDtos.get("guarNo") + CmisCommonConstants.COMMON_SPLIT_COMMA;
                }
                if (StringUtils.nonBlank(rtnData)) {
                    rtnData = rtnData.substring(0, rtnData.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
                }
            }
        }
        return rtnData;
    }

    /**
     * 权证信息初始化
     */
    public IqpCertiInoutAppDto querySelectContNo(String guarContNo, String pkId) {
        IqpCertiInoutAppDto iqpCertiInoutAppDto = new IqpCertiInoutAppDto();
        List<Map> list = new ArrayList<>();
        Map map = new HashMap();
        map.put("pkId", pkId);
        list.add(map);
        GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
        guarCertiRelaClientDto.setList(list);
        log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口开始");
        log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
        GuarClientRsDto guarClientRsDto = null;// iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
        log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口结束");
        if (guarClientRsDto == null) {
            throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
        }
        IqpCertiInoutAppDto guarCertiRela = new IqpCertiInoutAppDto();
        String depositOrgNo = "";
        if (CmisCommonConstants.INTERFACE_SUCCESS_CODE.equals(guarClientRsDto.getRtnCode())) {
            List<Map> guarClientRsDtoList = (List<Map>) guarClientRsDto.getData();
            if (CollectionUtils.isEmpty(guarClientRsDtoList)) {
                throw new YuspException(EcbEnum.E_GETMAPPING_GUARCONTNONULL_EXCEPTION.key, EcbEnum.E_GETMAPPING_GUARCONTNONULL_EXCEPTION.value);
            }

            for (Map guarClientRsDtos : guarClientRsDtoList) {
                depositOrgNo = (String) guarClientRsDtos.get("depositOrgNo");
                guarCertiRela = JSONObject.parseObject(JSON.toJSONString(guarClientRsDtos), IqpCertiInoutAppDto.class);
            }
            BeanUtils.copyProperties(guarCertiRela, iqpCertiInoutAppDto);
            iqpCertiInoutAppDto.setKeepBrId(depositOrgNo);
        }
        log.info("权证出入库申请" + guarContNo + "信息查询-担保合同表查询");
        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
        if (grtGuarCont == null) {
            throw new YuspException(EcbEnum.QUERT_GUARCONTNO_EXCEPTION.key, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.value);
        }
        BeanUtils.copyProperties(grtGuarCont, iqpCertiInoutAppDto);
        return iqpCertiInoutAppDto;
    }

    /**
     * 通过入参查询符合条件的权证出入库申请
     *
     * @param
     * @return
     */
    public List<Map> selectIqpCertiInOutApp(Map parmas) {
        return iqpCertiInoutAppMapper.selectIqpCertiInOutApp(parmas);
    }

    /**
     * 更新押品权证状态（已记账）
     *
     * @param
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateGuarCertiStatus(Map params) throws Exception {
        int rtnData = 0;
        Map parmas = new HashMap();
        try {
            String certiStatu = "";
            //入库未记账更新为入库已记账
            if (CmisBizConstants.IQP_CERTI_STATU_03.equals(params.get("certiStatu"))) {
                certiStatu = CmisBizConstants.IQP_CERTI_STATU_04;
            }
            //出借未记账更新为出借已记账
            if (CmisBizConstants.IQP_CERTI_STATU_06.equals(params.get("certiStatu"))) {
                certiStatu = CmisBizConstants.IQP_CERTI_STATU_07;
            }
            //出库未记账更新为出库已记账
            if (CmisBizConstants.IQP_CERTI_STATU_09.equals(params.get("certiStatu"))) {
                certiStatu = CmisBizConstants.IQP_CERTI_STATU_10;
            }
            if (!CmisBizConstants.IQP_CERTI_STATU_03.equals(params.get("certiStatu")) && !CmisBizConstants.IQP_CERTI_STATU_06.equals(params.get("certiStatu")) && !CmisBizConstants.IQP_CERTI_STATU_09.equals(params.get("certiStatu"))) {
                throw new YuspException(EcbEnum.GET_CERTI_STATU_EXCEPTION.key, EcbEnum.GET_CERTI_STATU_EXCEPTION.value);
            }
            parmas.put("serno", params.get("serno"));
            parmas.put("operation", "true");
            parmas.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<Map> iqpCertiInOutAppList = iqpCertiInoutAppMapper.selectIqpCertiInOutApp(parmas);
            if (CollectionUtils.isEmpty(iqpCertiInOutAppList) || iqpCertiInOutAppList.size() == 0) {
                log.info(params.get("serno") + "当前流水号不存在！");
                return rtnData;
            }

            //发送核心系统进行权证入库，接收并处理成功
            IqpCertiInoutApp iqpCertiInoutApp = new IqpCertiInoutApp();
            iqpCertiInoutApp.setCertiStatu(certiStatu);
            iqpCertiInoutApp.setSerno((String) params.get("serno"));

            rtnData = iqpCertiInoutAppMapper.updateByPrimaryKeySelective(iqpCertiInoutApp);

            User userInfo = SessionUtils.getUserInformation();

            if (rtnData > 0) {
                List<Map> list = new ArrayList<>();
                for (int i = 0; i < iqpCertiInOutAppList.size(); i++) {
                    Map guarMap = new HashMap();
                    guarMap.put("pkId", iqpCertiInOutAppList.get(i).get("certiPkId"));
                    guarMap.put("certiState", certiStatu);
                    if (userInfo == null) {
                        throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
                    } else {
                        guarMap.put("updId", userInfo.getLoginCode());
                        guarMap.put("updBrId", userInfo.getOrg().getCode());
                        guarMap.put("updDate", DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    }
                    list.add(guarMap);
                }

                GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
                log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口开始");
                guarCertiRelaClientDto.setList(list);
                log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
                GuarClientRsDto guarClientRsDto = null;//iGuarClientService.udpateGuarCertiRela(guarCertiRelaClientDto);
                log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口结束");
                if (guarClientRsDto != null && EcbEnum.IQP_CHG_EXCEPTION_DEF.key.equals(guarClientRsDto.getRtnCode())) {
                    log.info("权证状态更新异常！");
                    throw new YuspException(EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.key, EcbEnum.IQP_CERI_UPDATRE_EXCEPTION.value);
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtnData;
    }
}
