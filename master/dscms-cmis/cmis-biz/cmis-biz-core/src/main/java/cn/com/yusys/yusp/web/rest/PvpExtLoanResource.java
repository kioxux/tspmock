/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpExtLoan;
import cn.com.yusys.yusp.service.PvpExtLoanService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpExtLoanResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-13 14:33:32
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvpextloan")
public class PvpExtLoanResource {
    private static  final Logger log = LoggerFactory.getLogger(PvpExtLoanResource.class);
    @Autowired
    private PvpExtLoanService pvpExtLoanService;

	/**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpExtLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpExtLoan> list = pvpExtLoanService.selectAll(queryModel);
        return new ResultDto<List<PvpExtLoan>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpExtLoan>> index(QueryModel queryModel) {
        List<PvpExtLoan> list = pvpExtLoanService.selectByModel(queryModel);
        return new ResultDto<List<PvpExtLoan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pvpSerno}")
    protected ResultDto<PvpExtLoan> show(@PathVariable("pvpSerno") String pvpSerno) {
        PvpExtLoan pvpExtLoan = pvpExtLoanService.selectByPrimaryKey(pvpSerno);
        return new ResultDto<PvpExtLoan>(pvpExtLoan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpExtLoan> create(@RequestBody PvpExtLoan pvpExtLoan) throws URISyntaxException {
        pvpExtLoanService.insert(pvpExtLoan);
        return new ResultDto<PvpExtLoan>(pvpExtLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpExtLoan pvpExtLoan) throws URISyntaxException {
        int result = pvpExtLoanService.update(pvpExtLoan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pvpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pvpSerno") String pvpSerno) {
        int result = pvpExtLoanService.deleteByPrimaryKey(pvpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpExtLoanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 合同撤销时,检查是否放款
     * 展期出账表存在状态不为否决998的记录 opr_type=01
     * @param extCtrNo
     * @return
     */
    @GetMapping("/chechExtAccLoan/{extCtrNo}")
    protected ResultDto<Integer> chechExtAccLoan(@PathVariable("extCtrNo") String extCtrNo){
        int result = pvpExtLoanService.chechExtAccLoan(extCtrNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增展期放款申请
     * @param pvpExtLoan
     * @return
     */
    @PostMapping("/addIPvpExtLoan")
    protected ResultDto<Integer> addIPvpExtLoan(@RequestBody PvpExtLoan pvpExtLoan){
        log.info("新增展期放款申请start"+ JSON.toJSONString(pvpExtLoan));
        int result = pvpExtLoanService.addIPvpExtLoan(pvpExtLoan);
        log.info("新增展期放款申请end"+ JSON.toJSONString(result));
        return new ResultDto<Integer>(result);
    }
}
