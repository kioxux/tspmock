package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: riskItem0067Service
 * @类描述: 抵质押物入库校验（适用小微、对公、委托贷款）
 * @功能描述: 抵质押物入库校验（适用小微、对公、委托贷款）
 * @创建人: zrcbank-fengjj
 * @创建时间: 2021年8月7日
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0059抵质押物入库校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0059")
public class RiskItem0059Resource {

    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    /**
     * @方法名称: riskItem0059
     * @方法描述: 抵质押物入库校验（适用小微、对公、委托贷款）
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021年8月7日
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "抵质押物入库校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0059(@RequestBody QueryModel queryModel) {
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(pvpLoanAppService.riskItem0059(queryModel));
    }
}
