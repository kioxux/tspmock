package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.RptOperAgriculture;
import cn.com.yusys.yusp.domain.RptOperProductionOper;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperAgriculture
 * @类描述: rpt_oper_agriculture数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 14:22:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptOperAgricultureDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private RptOperAgriculture rptOperAgriculture;

	private RptOperProductionOper rptOperProductionOper;

	public RptOperAgriculture getRptOperAgriculture() {
		return rptOperAgriculture;
	}

	public RptOperProductionOper getRptOperProductionOper() {
		return rptOperProductionOper;
	}

	public void setRptOperAgriculture(RptOperAgriculture rptOperAgriculture) {
		this.rptOperAgriculture = rptOperAgriculture;
	}

	public void setRptOperProductionOper(RptOperProductionOper rptOperProductionOper) {
		this.rptOperProductionOper = rptOperProductionOper;
	}
}