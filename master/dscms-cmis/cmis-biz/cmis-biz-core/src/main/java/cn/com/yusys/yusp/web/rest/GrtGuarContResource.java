/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.GrtGuarContAndContNoDto;
import cn.com.yusys.yusp.dto.GrtGuarContClientDto;
import cn.com.yusys.yusp.dto.GrtGuarContDto;
import cn.com.yusys.yusp.dto.GrtGuarContRelDto;
import cn.com.yusys.yusp.dto.resp.GrtGuarContBySernoDto;
import cn.com.yusys.yusp.service.GrtGuarContService;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-17 13:51:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/grtguarcont")
public class GrtGuarContResource {
    private  static final Logger log= LoggerFactory.getLogger(GrtGuarContResource.class);
    @Autowired
    private GrtGuarContService grtGuarContService;

    /**
     * 全表查询.
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GrtGuarCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<GrtGuarCont> list = grtGuarContService.selectAll(queryModel);
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GrtGuarCont>> index(QueryModel queryModel) {
        List<GrtGuarCont> list = grtGuarContService.selectByModel(queryModel);
        ResultDto<List<GrtGuarCont>> listzz = new ResultDto<List<GrtGuarCont>>(list);
        System.out.println(listzz.toString());
        return listzz;
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<GrtGuarCont>> queryList(@RequestBody QueryModel queryModel) {
        List<GrtGuarCont> list = grtGuarContService.selectByModel(queryModel);
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称:selectGuarContByContNo
     * @函数描述:根据合同号查询担保合同
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectguarcontbycontNo")
    protected ResultDto<List<GrtGuarCont>> selectGuarContByContNo(@RequestBody QueryModel queryModel) {
        List<GrtGuarCont> list = grtGuarContService.selectGuarContByContNo(queryModel);
        ResultDto<List<GrtGuarCont>> listzz = new ResultDto<List<GrtGuarCont>>(list);
        System.out.println(listzz.toString());
        return listzz;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{guarPkId}")
    protected ResultDto<GrtGuarCont> show(@PathVariable("guarPkId") String guarPkId) {
        GrtGuarCont grtGuarCont = grtGuarContService.selectByPrimaryKey(guarPkId);
        return new ResultDto<GrtGuarCont>(grtGuarCont);
    }

    /**
     * @函数名称:selectByGuarContNo
     * @函数描述:根据担保合同号查询担保合同
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByGuarContNo")
    @ApiOperation("根据担保合同号查询担保合同")
    protected ResultDto<GrtGuarCont> selectByGuarContNo(@RequestBody String guarContNo) {
        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
        return new ResultDto<GrtGuarCont>(grtGuarCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GrtGuarCont> create(@RequestBody GrtGuarCont grtGuarCont) throws URISyntaxException {
        grtGuarContService.insert(grtGuarCont);
        return new ResultDto<GrtGuarCont>(grtGuarCont);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口 小微用信数据新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<GrtGuarCont> insert(@RequestBody GrtGuarCont grtGuarCont) throws Exception {
        grtGuarContService.insertGrt(grtGuarCont);
        return new ResultDto<GrtGuarCont>(grtGuarCont);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口 零售用信数据新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertRetall")
    protected ResultDto<GrtGuarContBySernoDto> insertRetall(@RequestBody GrtGuarContBySernoDto grtGuarContBySernoDto) throws Exception {
        grtGuarContService.insertRetall(grtGuarContBySernoDto);
        return new ResultDto<GrtGuarContBySernoDto>(grtGuarContBySernoDto);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口 对公用信管理数据新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertPublic")
    protected ResultDto<GrtGuarContBySernoDto> insertPublic(@RequestBody GrtGuarContBySernoDto grtGuarContBySernoDto) throws Exception {
        grtGuarContService.insertPublic(grtGuarContBySernoDto);
        return new ResultDto<GrtGuarContBySernoDto>(grtGuarContBySernoDto);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GrtGuarCont grtGuarCont) throws URISyntaxException {
        int result = grtGuarContService.update(grtGuarCont);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:update1
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update1")
    protected ResultDto<Integer> update1(@RequestBody GrtGuarCont grtGuarCont) throws URISyntaxException {
        int result = grtGuarContService.update1(grtGuarCont);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebyguarcont")
    protected ResultDto<Integer> updatebyguarcont(@RequestBody GrtGuarCont grtGuarCont) throws URISyntaxException {
        int result = grtGuarContService.updateSelectiveByGuarContNo(grtGuarCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{guarPkId}")
    protected ResultDto<Integer> delete(@PathVariable("guarPkId") String guarPkId) {
        int result = grtGuarContService.deleteByPrimaryKey(guarPkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/deleteSelective{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = grtGuarContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(GrtGuarCont record) {
        return grtGuarContService.updateSelective(record);
    }
    /**
     * @函数名称: updateSelectiveByParams
     * @函数描述: 担保合同申请更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelectiveByParams")
    protected ResultDto<Integer> updateSelectiveByParams(@RequestBody Map parmas) {
        log.info("担保合同申请请求信息【{}】", JSONObject.toJSON(parmas));
        int result = grtGuarContService.updateSelectiveByParams(parmas);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称: getGrtGuarCont
     * @函数描述: 获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getGrtGuarCont")
    protected List<GrtGuarContClientDto> getGrtGuarCont(@RequestBody GrtGuarContClientDto grtGuarContClientDto) throws URISyntaxException {
        log.info("担保合同查询请求信息【{}】", JSONObject.toJSON(grtGuarContClientDto));
        return grtGuarContService.getGrtGuarCont(grtGuarContClientDto);
    }

    /**
     * 校验担保合同金额与押品的担保合同金额
     * @param params
     * @return
     */
    @PostMapping("/checkGuarAmtAndAppAmt")
    public ResultDto<Map> checkGuarAmtAndAppAmt(@RequestBody Map params) {
        Map rtnData = grtGuarContService.checkGuarAmtAndAppAmt(params);
        return new ResultDto<Map>(rtnData);
    }
    /**
     *  业务申请时 新增/修改  一般担保合同数据保存操作
     * 0、校验数据  校验押品的评估价值/保证人的保证
     * 1、新增/修改 保存担保合同数据
     * 2、新增 业务与担保合同关系数据
     * @param params
     * @return
     */

    @PostMapping("/saveIqpGrtGuarCont")
    public ResultDto<Map> saveIqpGrtGuarCont(@RequestBody Map params){
        Map rtnData = grtGuarContService.saveIqpGrtGuarCont(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @函数名称:showList
     * @函数描述:查询担保信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showlist")
    protected ResultDto<Object> showList(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        String contNo = (String) params.get("contNo");
        Integer page = (Integer) params.get("page");
        Integer size = (Integer) params.get("size");
        PageHelper.startPage(page, size);
        List<GrtGuarCont> list = grtGuarContService.selectGuarContByBizRst(contNo);
        PageHelper.clearPage();
        if (list.size()>0) {
            resultDto.setCode(200);
            resultDto.setData(list);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }


    /**
     * @方法名称：getStockGuarCont
     * @方法描述：合同下的担保合同
     * @创建人：zhangming12
     * @创建时间：2021/5/22 10:23
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/getstockcont")
    protected ResultDto<List<GrtGuarCont>> getStockGuarCont(@RequestBody QueryModel queryModel){
        List<GrtGuarCont> grtGuarContList = grtGuarContService.getStockGuarCont(queryModel);
        return new ResultDto<>(grtGuarContList);
    }

    /**
     * @方法名称：deleteGrtGuarContRelLink
     * @方法描述：删除担保合同和抵押物的关系
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @ApiOperation("删除担保合同和抵押物的关系")
    @PostMapping("/deleteGrtGuarContRelLink")
    protected ResultDto<Integer> deleteGrtGuarContRelLink(@RequestBody GrtGuarContRelDto grtGuarContRel){
        int result = grtGuarContService.deleteGrtGuarContRelLink(grtGuarContRel);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称：tempSave
     * @方法描述：担保合同暂存/保存
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @ApiOperation("担保合同暂存/保存")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody GrtGuarContDto grtGuarContDto){
        int result = grtGuarContService.tempSave(grtGuarContDto);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称：maxContCancelImport
     * @方法描述：最高额担保合同取消引入
     * @创建人：zfq
     * @创建时间：2021/6/8 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @ApiOperation("最高额担保合同取消引入")
    @PostMapping("/maxContCancelImport")
    protected ResultDto<Integer> maxContCancelImport(@RequestBody GrtGuarBizRstRel grtGuarBizRstRel){
        int result = grtGuarContService.maxContCancelImport(grtGuarBizRstRel);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称：deleteOnLogic
     * @方法描述：担保合同逻辑删除,同时逻辑删除关系表(担保合同和押品,担保合同和合同)
     * @创建人：zfq
     * @创建时间：2021/6/8 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @ApiOperation("担保合同逻辑删除")
    @PostMapping("/deleteOnLogic")
    protected ResultDto<Integer> deleteOnLogic(@RequestBody GrtGuarContDto grtGuarContDto){
        int result = grtGuarContService.deleteOnLogic(grtGuarContDto);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:selectByGuarContNo
     * @函数描述:根据借款合同号查询担保合同
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/queryGrtGuarContByContNo")
    @ApiOperation("根据借款合同号查询担保合同")
    protected ResultDto<List<GrtGuarContClientDto>> queryGrtGuarContByContNo(@RequestBody List<String> contNoList) {
        List<GrtGuarContClientDto> list= grtGuarContService.queryGrtGuarContByContNo(contNoList);
        return new ResultDto<List<GrtGuarContClientDto>>(list);
    }

    /**
     * @函数名称:getGrtGuarContByContNo
     * @函数描述:根据借款合同号查询担保合同
     * @参数与返回说明:
     * @算法描述:
     * @创建人：lyj
     */
    @PostMapping("/getGrtGuarContByContNo")
    @ApiOperation("对公根据借款合同号查询担保合同")
    protected ResultDto<List<GrtGuarContClientDto>> getGrtGuarContByContNo(@RequestBody List<String> contNoList) {
        List<GrtGuarContClientDto> list= grtGuarContService.queryGrtGuarContByContNo(contNoList);
        return new ResultDto<List<GrtGuarContClientDto>>(list);
    }

    /**
     * @函数名称:queryGrtGuarContByGuarNo
     * @函数描述:根据押品统一编号获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryGrtGuarContByGuarNo")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContByGuarNo(@RequestBody QueryModel queryModel) {
        List<GrtGuarCont> list = grtGuarContService.queryGrtGuarContByGuarNo(queryModel);
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称:queryGrtGuarContByCusId
     * @函数描述:根据客户编号获取为他人提供的担保
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryGrtGuarContByCusId")
    protected ResultDto<List<GrtGuarContAndContNoDto>> queryGrtGuarContByCusId(@RequestBody QueryModel queryModel) {
        List<GrtGuarContAndContNoDto> list = grtGuarContService.queryGrtGuarContByCusId(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:queryGrtGuarContByBorrowerCusId
     * @函数描述:根据客户编号获取他人为其提供的担保
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryGrtGuarContByBorrowerCusId")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContByBorrowerCusId(@RequestBody QueryModel queryModel) {
        List<GrtGuarCont> list = grtGuarContService.queryGrtGuarContByBorrowerCusId(queryModel);
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称:checkOnLogout
     * @函数描述:担保合同注销校验
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkOnLogout/{guarContNo}")
    protected ResultDto<String> checkOnLogout(@PathVariable("guarContNo") String guarContNo) {
        if(StringUtils.nonBlank(guarContNo) && grtGuarContService.checkOnLogout(guarContNo)){
            return ResultDto.success("担保合同校验通过");
        }else{
            return ResultDto.error("担保合同校验失败");
        }
    }

    /**
     * @函数名称:selectGuarContStateByGuarNo
     * @函数描述:根据担保合同编号查询对应的担保合同状态
     * @参数与返回说明:
     * @param guarContNo
     * @算法描述:
     */
    @GetMapping("/selectGuarContStateByGuarContNo/{guarContNo}")
    protected ResultDto<String> selectGuarContStateByGuarContNo(@PathVariable("guarContNo") String guarContNo) {
        String guarContState = grtGuarContService.selectGuarContStateByGuarContNo(guarContNo);
        return new ResultDto<>(guarContState);
    }

    @PostMapping("/queryGrtGuarContByContNohtdy")
    @ApiOperation("根据借款合同号查询担保合同")
    protected ResultDto<List<GrtGuarContClientDto>> queryGrtGuarContByContNohtdy(@RequestBody List<String> contNoList) {
        List<GrtGuarContClientDto> list= grtGuarContService.queryGrtGuarContByContNohtdy(contNoList);
        return new ResultDto<List<GrtGuarContClientDto>>(list);
    }
}
