/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfo;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoAppr;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoApprMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:22:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoApprService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoApprMapper lmtSigInvestBasicInfoApprMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoAppr selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoAppr> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoAppr> records = (List<LmtSigInvestBasicInfoAppr>) lmtSigInvestBasicInfoApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoAppr> list = lmtSigInvestBasicInfoApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoAppr record) {
        return lmtSigInvestBasicInfoApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoAppr record) {
        return lmtSigInvestBasicInfoApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoAppr record) {
        return lmtSigInvestBasicInfoApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoAppr record) {
        return lmtSigInvestBasicInfoApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoApprMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号获取
     * @param serno
     * @return
     */
    public LmtSigInvestBasicInfoAppr selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(serno)) throw new BizException(null,"",null,"申请流水号不允许为空！");
        queryModel.addCondition("serno",serno);
        List<LmtSigInvestBasicInfoAppr> lmtSigInvestBasicInfoApprs = lmtSigInvestBasicInfoApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoApprs!=null && lmtSigInvestBasicInfoApprs.size()>0){
            return lmtSigInvestBasicInfoApprs.get(0);
        }
        return null;
    }


    /**
     * @方法名称: initLmtSigInvestBasicInfoApprInfo
     * @方法描述: 根据申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoAppr initLmtSigInvestBasicInfoApprInfo(LmtSigInvestBasicInfo lmtSigInvestBasicInfo) {
        //初始化对象
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = new LmtSigInvestBasicInfoAppr() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtSigInvestBasicInfo, lmtSigInvestBasicInfoAppr);
        //初始化通用domain
        initInsertDomainPropertiesForWF(lmtSigInvestBasicInfoAppr);
        //主键
        lmtSigInvestBasicInfoAppr.setPkId(generatePkId());
        //返回对象信息
        return lmtSigInvestBasicInfoAppr;
    }

    /**
     * @方法名称: selectByApproveSerno
     * @方法描述: 根据审批流水号，获取对应的数据信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestBasicInfoAppr selectByApproveSerno(String approveSerno){
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(approveSerno)) throw new BizException(null,"",null,"审批编号不允许为空！");
        queryModel.addCondition("approveSerno",approveSerno);
        queryModel.addCondition("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtSigInvestBasicInfoAppr> lmtSigInvestBasicInfoApprs = lmtSigInvestBasicInfoApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoApprs!=null && lmtSigInvestBasicInfoApprs.size()>0){
            return lmtSigInvestBasicInfoApprs.get(0);
        }
        return null;
    }
}
