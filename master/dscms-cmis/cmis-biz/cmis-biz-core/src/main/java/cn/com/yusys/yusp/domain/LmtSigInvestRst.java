/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRst
 * @类描述: lmt_sig_invest_rst数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-20 10:39:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_rst")
public class LmtSigInvestRst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = false, length = 40)
	private String replySerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;
	
	/** 原授信期限 **/
	@Column(name = "ORIGI_LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtTerm;
	
	/** 原授信金额 **/
	@Column(name = "ORIGI_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLmtAmt;
	
	/** 原利率 **/
	@Column(name = "ORIGI_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiRate;
	
	/** 申请类型 **/
	@Column(name = "APP_TYPE", unique = false, nullable = true, length = 5)
	private String appType;
	
	/** 项目编号 **/
	@Column(name = "PRO_NO", unique = false, nullable = true, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 200)
	private String proName;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 底层资产类型 **/
	@Column(name = "BASIC_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String basicAssetType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户大类 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String cusCatalog;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 授信品种编号 **/
	@Column(name = "LMT_BIZ_TYPE", unique = false, nullable = true, length = 20)
	private String lmtBizType;
	
	/** 授信品种名称 **/
	@Column(name = "LMT_BIZ_TYPE_NAME", unique = false, nullable = true, length = 80)
	private String lmtBizTypeName;
	
	/** 授信产品 **/
	@Column(name = "LMT_PRD", unique = false, nullable = true, length = 20)
	private String lmtPrd;
	
	/** 是否大额授信 **/
	@Column(name = "IS_LARGE_LMT", unique = false, nullable = true, length = 5)
	private String isLargeLmt;
	
	/** 是否需报备董事长 **/
	@Column(name = "IS_REPORT_CHAIRMAN", unique = false, nullable = true, length = 5)
	private String isReportChairman;
	
	/** 大额主责任人 **/
	@Column(name = "LARGE_LMT_MAIN_MANAGER", unique = false, nullable = true, length = 5)
	private String largeLmtMainManager;
	
	/** 底层资产标准值 **/
	@Column(name = "BASIC_ASSET_NORMAL", unique = false, nullable = true, length = 5)
	private String basicAssetNormal;
	
	/** 资管计划申请业务类型 **/
	@Column(name = "ASSET_PLAN_APP_BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String assetPlanAppBusiType;
	
	/** 资管计划产品类型 **/
	@Column(name = "ASSET_MANA_PRD_TYPE", unique = false, nullable = true, length = 200)
	private String assetManaPrdType;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 利率 **/
	@Column(name = "RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rate;
	
	/** 项目投资期限（年） **/
	@Column(name = "PRO_INVEST_TERM", unique = false, nullable = true, length = 20)
	private String proInvestTerm;
	
	/** 最高授信投资剩余期限（年） **/
	@Column(name = "HIGH_LMT_INVEST_SURPLUS_TERM", unique = false, nullable = true, length = 80)
	private String highLmtInvestSurplusTerm;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 自营金额 **/
	@Column(name = "SOBS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sobsAmt;
	
	/** 资管金额 **/
	@Column(name = "ASSET_MANA_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetManaAmt;
	
	/** 拟/实际发行规模（原币） **/
	@Column(name = "INTEND_ACTUAL_ISSUED_SCALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intendActualIssuedScale;
	
	/** 拟/实际发行时间 **/
	@Column(name = "INTEND_ACTUAL_ISSUED_TIME", unique = false, nullable = true, length = 20)
	private String intendActualIssuedTime;
	
	/** 发行币种 **/
	@Column(name = "ISSUED_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String issuedCurType;
	
	/** 债项评级结果 **/
	@Column(name = "DEBT_EVAL_RESULT", unique = false, nullable = true, length = 5)
	private String debtEvalResult;
	
	/** 债项评级日期 **/
	@Column(name = "DEBT_EVAL_DATE", unique = false, nullable = true, length = 20)
	private String debtEvalDate;
	
	/** 评级机构名称 **/
	@Column(name = "DEBT_EVAL_ORG_NAME", unique = false, nullable = true, length = 5)
	private String debtEvalOrgName;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 2000)
	private String indgtResult;
	
	/** 终审机构类型 **/
	@Column(name = "FINAL_APPR_BR_TYPE", unique = false, nullable = true, length = 5)
	private String finalApprBrType;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;
	
	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;
	
	/** 批复生效日期 **/
	@Column(name = "REPLY_INURE_DATE", unique = false, nullable = true, length = 20)
	private String replyInureDate;
	
	/** 协办客户经理编号 **/
	@Column(name = "ASSIST_MANAGER_ID", unique = false, nullable = true, length = 20)
	private String assistManagerId;
	
	/** 投资类型 **/
	@Column(name = "INVEST_TYPE", unique = false, nullable = true, length = 5)
	private String investType;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}
	
    /**
     * @return origiLmtReplySerno
     */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(Integer origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}
	
    /**
     * @return origiLmtTerm
     */
	public Integer getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}
	
	/**
	 * @param origiLmtAmt
	 */
	public void setOrigiLmtAmt(java.math.BigDecimal origiLmtAmt) {
		this.origiLmtAmt = origiLmtAmt;
	}
	
    /**
     * @return origiLmtAmt
     */
	public java.math.BigDecimal getOrigiLmtAmt() {
		return this.origiLmtAmt;
	}
	
	/**
	 * @param origiRate
	 */
	public void setOrigiRate(java.math.BigDecimal origiRate) {
		this.origiRate = origiRate;
	}
	
    /**
     * @return origiRate
     */
	public java.math.BigDecimal getOrigiRate() {
		return this.origiRate;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType;
	}
	
    /**
     * @return basicAssetType
     */
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}
	
    /**
     * @return lmtBizType
     */
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName;
	}
	
    /**
     * @return lmtBizTypeName
     */
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param lmtPrd
	 */
	public void setLmtPrd(String lmtPrd) {
		this.lmtPrd = lmtPrd;
	}
	
    /**
     * @return lmtPrd
     */
	public String getLmtPrd() {
		return this.lmtPrd;
	}
	
	/**
	 * @param isLargeLmt
	 */
	public void setIsLargeLmt(String isLargeLmt) {
		this.isLargeLmt = isLargeLmt;
	}
	
    /**
     * @return isLargeLmt
     */
	public String getIsLargeLmt() {
		return this.isLargeLmt;
	}
	
	/**
	 * @param isReportChairman
	 */
	public void setIsReportChairman(String isReportChairman) {
		this.isReportChairman = isReportChairman;
	}
	
    /**
     * @return isReportChairman
     */
	public String getIsReportChairman() {
		return this.isReportChairman;
	}
	
	/**
	 * @param largeLmtMainManager
	 */
	public void setLargeLmtMainManager(String largeLmtMainManager) {
		this.largeLmtMainManager = largeLmtMainManager;
	}
	
    /**
     * @return largeLmtMainManager
     */
	public String getLargeLmtMainManager() {
		return this.largeLmtMainManager;
	}
	
	/**
	 * @param basicAssetNormal
	 */
	public void setBasicAssetNormal(String basicAssetNormal) {
		this.basicAssetNormal = basicAssetNormal;
	}
	
    /**
     * @return basicAssetNormal
     */
	public String getBasicAssetNormal() {
		return this.basicAssetNormal;
	}
	
	/**
	 * @param assetPlanAppBusiType
	 */
	public void setAssetPlanAppBusiType(String assetPlanAppBusiType) {
		this.assetPlanAppBusiType = assetPlanAppBusiType;
	}
	
    /**
     * @return assetPlanAppBusiType
     */
	public String getAssetPlanAppBusiType() {
		return this.assetPlanAppBusiType;
	}
	
	/**
	 * @param assetManaPrdType
	 */
	public void setAssetManaPrdType(String assetManaPrdType) {
		this.assetManaPrdType = assetManaPrdType;
	}
	
    /**
     * @return assetManaPrdType
     */
	public String getAssetManaPrdType() {
		return this.assetManaPrdType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return rate
     */
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param proInvestTerm
	 */
	public void setProInvestTerm(String proInvestTerm) {
		this.proInvestTerm = proInvestTerm;
	}
	
    /**
     * @return proInvestTerm
     */
	public String getProInvestTerm() {
		return this.proInvestTerm;
	}
	
	/**
	 * @param highLmtInvestSurplusTerm
	 */
	public void setHighLmtInvestSurplusTerm(String highLmtInvestSurplusTerm) {
		this.highLmtInvestSurplusTerm = highLmtInvestSurplusTerm;
	}
	
    /**
     * @return highLmtInvestSurplusTerm
     */
	public String getHighLmtInvestSurplusTerm() {
		return this.highLmtInvestSurplusTerm;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param sobsAmt
	 */
	public void setSobsAmt(java.math.BigDecimal sobsAmt) {
		this.sobsAmt = sobsAmt;
	}
	
    /**
     * @return sobsAmt
     */
	public java.math.BigDecimal getSobsAmt() {
		return this.sobsAmt;
	}
	
	/**
	 * @param assetManaAmt
	 */
	public void setAssetManaAmt(java.math.BigDecimal assetManaAmt) {
		this.assetManaAmt = assetManaAmt;
	}
	
    /**
     * @return assetManaAmt
     */
	public java.math.BigDecimal getAssetManaAmt() {
		return this.assetManaAmt;
	}
	
	/**
	 * @param intendActualIssuedScale
	 */
	public void setIntendActualIssuedScale(java.math.BigDecimal intendActualIssuedScale) {
		this.intendActualIssuedScale = intendActualIssuedScale;
	}
	
    /**
     * @return intendActualIssuedScale
     */
	public java.math.BigDecimal getIntendActualIssuedScale() {
		return this.intendActualIssuedScale;
	}
	
	/**
	 * @param intendActualIssuedTime
	 */
	public void setIntendActualIssuedTime(String intendActualIssuedTime) {
		this.intendActualIssuedTime = intendActualIssuedTime;
	}
	
    /**
     * @return intendActualIssuedTime
     */
	public String getIntendActualIssuedTime() {
		return this.intendActualIssuedTime;
	}
	
	/**
	 * @param issuedCurType
	 */
	public void setIssuedCurType(String issuedCurType) {
		this.issuedCurType = issuedCurType;
	}
	
    /**
     * @return issuedCurType
     */
	public String getIssuedCurType() {
		return this.issuedCurType;
	}
	
	/**
	 * @param debtEvalResult
	 */
	public void setDebtEvalResult(String debtEvalResult) {
		this.debtEvalResult = debtEvalResult;
	}
	
    /**
     * @return debtEvalResult
     */
	public String getDebtEvalResult() {
		return this.debtEvalResult;
	}
	
	/**
	 * @param debtEvalDate
	 */
	public void setDebtEvalDate(String debtEvalDate) {
		this.debtEvalDate = debtEvalDate;
	}
	
    /**
     * @return debtEvalDate
     */
	public String getDebtEvalDate() {
		return this.debtEvalDate;
	}
	
	/**
	 * @param debtEvalOrgName
	 */
	public void setDebtEvalOrgName(String debtEvalOrgName) {
		this.debtEvalOrgName = debtEvalOrgName;
	}
	
    /**
     * @return debtEvalOrgName
     */
	public String getDebtEvalOrgName() {
		return this.debtEvalOrgName;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}
	
    /**
     * @return finalApprBrType
     */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}
	
    /**
     * @return replyStatus
     */
	public String getReplyStatus() {
		return this.replyStatus;
	}
	
	/**
	 * @param replyInureDate
	 */
	public void setReplyInureDate(String replyInureDate) {
		this.replyInureDate = replyInureDate;
	}
	
    /**
     * @return replyInureDate
     */
	public String getReplyInureDate() {
		return this.replyInureDate;
	}
	
	/**
	 * @param assistManagerId
	 */
	public void setAssistManagerId(String assistManagerId) {
		this.assistManagerId = assistManagerId;
	}
	
    /**
     * @return assistManagerId
     */
	public String getAssistManagerId() {
		return this.assistManagerId;
	}
	
	/**
	 * @param investType
	 */
	public void setInvestType(String investType) {
		this.investType = investType;
	}
	
    /**
     * @return investType
     */
	public String getInvestType() {
		return this.investType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}