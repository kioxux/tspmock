package cn.com.yusys.yusp.web.server.xdzx0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzx0001.Xdzx0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:查询征信业务流水号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0001:查询征信业务流水号")
@RestController
@RequestMapping("/api/bizzx4bsp")
public class BizXdzx0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzx0001Resource.class);

    @Autowired
	private Xdzx0001Service xdzx0001Service;

    /**
     * 交易码：xdzx0001
     * 交易描述：查询征信业务流水号
     *
     * @param xdzx0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询征信业务流水号")
    @PostMapping("/xdzx0001")
    protected @ResponseBody
    ResultDto<Xdzx0001DataRespDto> xdzx0001(@Validated @RequestBody Xdzx0001DataReqDto xdzx0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataReqDto));
        Xdzx0001DataRespDto xdzx0001DataRespDto = new Xdzx0001DataRespDto();// 响应Dto:查询征信业务流水号
        ResultDto<Xdzx0001DataRespDto> xdzx0001DataResultDto = new ResultDto<>();
        String applySerno = xdzx0001DataReqDto.getApplySerno();//业务唯一编号
        try {
            // 从xdzx0001DataReqDto获取业务值进行业务逻辑处理
            // 调用xdzx0001Service层开始
			logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataReqDto));
			xdzx0001DataRespDto = Optional.ofNullable(xdzx0001Service.getZxSerno(xdzx0001DataReqDto)).orElseGet(() -> {
				Xdzx0001DataRespDto temp = new Xdzx0001DataRespDto();
				temp.setZxSerno(StringUtils.EMPTY);// 征信业务流水号
				return temp;
			});
			logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataRespDto));
            // 封装xdzx0001DataResultDto中正确的返回码和返回信息
            xdzx0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzx0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, e.getMessage());
            // 封装xdzx0001DataResultDto中异常返回码和返回信息
            xdzx0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzx0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzx0001DataRespDto到xdzx0001DataResultDto中
        xdzx0001DataResultDto.setData(xdzx0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataResultDto));
        return xdzx0001DataResultDto;
    }
}
