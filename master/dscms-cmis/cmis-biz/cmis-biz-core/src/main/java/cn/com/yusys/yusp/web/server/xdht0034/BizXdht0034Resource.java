package cn.com.yusys.yusp.web.server.xdht0034;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0034.req.Xdht0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0034.resp.Xdht0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0034.Xdht0034Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0034:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0034Resource.class);
    @Autowired
    private Xdht0034Service xdht0034Service;

    /**
     * 交易码：xdht0034
     * 交易描述：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
     *
     * @param xdht0034DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额")
    @PostMapping("/xdht0034")
    protected @ResponseBody
    ResultDto<Xdht0034DataRespDto> xdht0034(@Validated @RequestBody Xdht0034DataReqDto xdht0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataReqDto));
        Xdht0034DataRespDto xdht0034DataRespDto = new Xdht0034DataRespDto();// 响应Dto:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
        ResultDto<Xdht0034DataRespDto> xdht0034DataResultDto = new ResultDto<>();
        try {
            String cusId = xdht0034DataReqDto.getCusId();
            if (StringUtils.isEmpty(cusId)) {
                xdht0034DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0034DataResultDto.setMessage("客户号cusId不能为空");
                return xdht0034DataResultDto;
            }
            // 从xdht0034DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataReqDto));
            xdht0034DataRespDto = xdht0034Service.xdht0034(xdht0034DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataRespDto));
            // 封装xdht0034DataResultDto中正确的返回码和返回信息
            xdht0034DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0034DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, e.getMessage());
            // 封装xdht0034DataResultDto中异常返回码和返回信息
            xdht0034DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0034DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0034DataRespDto到xdht0034DataResultDto中
        xdht0034DataResultDto.setData(xdht0034DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataResultDto));
        return xdht0034DataResultDto;
    }
}