/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DataModify
 * @类描述: data_modify数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 17:25:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "data_modify")
public class DataModify extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 修改类型 **/
	@Column(name = "MODIFY_TYPE", unique = false, nullable = true, length = 2)
	private String modifyType;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 贷款类别 **/
	@Column(name = "LOAN_TYPE_DETAIL", unique = false, nullable = true, length = 5)
	private String loanTypeDetail;
	
	/** 是否落实贷款 **/
	@Column(name = "IS_PACT_LOAN", unique = false, nullable = true, length = 5)
	private String isPactLoan;
	
	/** 是否绿色产业 **/
	@Column(name = "IS_GREEN_INDUSTRY", unique = false, nullable = true, length = 5)
	private String isGreenIndustry;
	
	/** 是否经营性物业贷款 **/
	@Column(name = "IS_OPER_PROPERTY_LOAN", unique = false, nullable = true, length = 5)
	private String isOperPropertyLoan;
	
	/** 是否钢贸行业贷款 **/
	@Column(name = "IS_STEEL_LOAN", unique = false, nullable = true, length = 5)
	private String isSteelLoan;
	
	/** 是否不锈钢行业贷款 **/
	@Column(name = "IS_STAINLESS_LOAN", unique = false, nullable = true, length = 5)
	private String isStainlessLoan;
	
	/** 是否扶贫贴息贷款 **/
	@Column(name = "IS_POVERTY_RELIEF_LOAN", unique = false, nullable = true, length = 5)
	private String isPovertyReliefLoan;
	
	/** 是否劳动密集型小企业贴息贷款 **/
	@Column(name = "IS_LABOR_INTEN_SBSY_LOAN", unique = false, nullable = true, length = 5)
	private String isLaborIntenSbsyLoan;
	
	/** 保障性安居工程贷款 **/
	@Column(name = "GOVER_SUBSZ_HOUSE_LOAN", unique = false, nullable = true, length = 5)
	private String goverSubszHouseLoan;
	
	/** 项目贷款节能环保 **/
	@Column(name = "ENGY_ENVI_PROTE_LOAN", unique = false, nullable = true, length = 5)
	private String engyEnviProteLoan;
	
	/** 是否农村综合开发贷款标志 **/
	@Column(name = "IS_CPHS_RUR_DELP_LOAN", unique = false, nullable = true, length = 5)
	private String isCphsRurDelpLoan;
	
	/** 房地产贷款 **/
	@Column(name = "REALPRO_LOAN", unique = false, nullable = true, length = 5)
	private String realproLoan;
	
	/** 房产开发贷款资本金比例 **/
	@Column(name = "REALPRO_LOAN_RATE", unique = false, nullable = true, length = 5)
	private String realproLoanRate;
	
	/** 担保方式细分 **/
	@Column(name = "GUAR_DETAIL_MODE", unique = false, nullable = true, length = 5)
	private String guarDetailMode;
	
	/** 贷款类别 **/
	@Column(name = "OLD_LOAN_TYPE_DETAIL", unique = false, nullable = true, length = 5)
	private String oldLoanTypeDetail;
	
	/** 是否落实贷款 **/
	@Column(name = "OLD_IS_PACT_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsPactLoan;
	
	/** 是否绿色产业 **/
	@Column(name = "OLD_IS_GREEN_INDUSTRY", unique = false, nullable = true, length = 5)
	private String oldIsGreenIndustry;
	
	/** 是否经营性物业贷款 **/
	@Column(name = "OLD_IS_OPER_PROPERTY_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsOperPropertyLoan;
	
	/** 是否钢贸行业贷款 **/
	@Column(name = "OLD_IS_STEEL_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsSteelLoan;
	
	/** 是否不锈钢行业贷款 **/
	@Column(name = "OLD_IS_STAINLESS_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsStainlessLoan;
	
	/** 是否扶贫贴息贷款 **/
	@Column(name = "OLD_IS_POVERTY_RELIEF_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsPovertyReliefLoan;
	
	/** 是否劳动密集型小企业贴息贷款 **/
	@Column(name = "OLD_IS_LABOR_INTEN_SBSY_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsLaborIntenSbsyLoan;
	
	/** 保障性安居工程贷款 **/
	@Column(name = "OLD_GOVER_SUBSZ_HOUSE_LOAN", unique = false, nullable = true, length = 5)
	private String oldGoverSubszHouseLoan;
	
	/** 项目贷款节能环保 **/
	@Column(name = "OLD_ENGY_ENVI_PROTE_LOAN", unique = false, nullable = true, length = 5)
	private String oldEngyEnviProteLoan;
	
	/** 是否农村综合开发贷款标志 **/
	@Column(name = "OLD_IS_CPHS_RUR_DELP_LOAN", unique = false, nullable = true, length = 5)
	private String oldIsCphsRurDelpLoan;
	
	/** 房地产贷款 **/
	@Column(name = "OLD_REALPRO_LOAN", unique = false, nullable = true, length = 5)
	private String oldRealproLoan;
	
	/** 房产开发贷款资本金比例 **/
	@Column(name = "OLD_REALPRO_LOAN_RATE", unique = false, nullable = true, length = 5)
	private String oldRealproLoanRate;
	
	/** 担保方式细分 **/
	@Column(name = "OLD_GUAR_DETAIL_MODE", unique = false, nullable = true, length = 5)
	private String oldGuarDetailMode;

	/** 农户类型 **/
	@Column(name = "AGRI_TYPE", unique = false, nullable = true, length = 5)
	private String agriType;

	/** 涉农贷款投向 **/
	@Column(name = "AGRI_LOAN_TER", unique = false, nullable = true, length = 5)
	private String agriLoanTer;

	/** 农户类型 **/
	@Column(name = "OLD_AGRI_TYPE", unique = false, nullable = true, length = 5)
	private String oldAgriType;

	/** 涉农贷款投向 **/
	@Column(name = "OLD_AGRI_LOAN_TER", unique = false, nullable = true, length = 5)
	private String oldAgriLoanTer;
	
	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 5)
	private String loanTer;
	
	/** 贷款投向 **/
	@Column(name = "OLD_LOAN_TER", unique = false, nullable = true, length = 5)
	private String oldLoanTer;
	
	/** 科目号 **/
	@Column(name = "SUBJECT_NO", unique = false, nullable = true, length = 20)
	private String subjectNo;
	
	/** 科目名称 **/
	@Column(name = "SUBJECT_NAME", unique = false, nullable = true, length = 200)
	private String subjectName;
	
	/** 科目号 **/
	@Column(name = "OLD_SUBJECT_NO", unique = false, nullable = true, length = 20)
	private String oldSubjectNo;
	
	/** 科目名称 **/
	@Column(name = "OLD_SUBJECT_NAME", unique = false, nullable = true, length = 200)
	private String oldSubjectName;
	
	/** 修改内容 **/
	@Column(name = "MODIFICATION_CONTENT", unique = false, nullable = true, length = 500)
	private String modificationContent;
	
	/** 修改原因 **/
	@Column(name = "MODIFICATION_REASON", unique = false, nullable = true, length = 500)
	private String modificationReason;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 40)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	public String getAgriType() {
		return agriType;
	}

	public void setAgriType(String agriType) {
		this.agriType = agriType;
	}

	public String getAgriLoanTer() {
		return agriLoanTer;
	}

	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer;
	}

	public String getOldAgriType() {
		return oldAgriType;
	}

	public void setOldAgriType(String oldAgriType) {
		this.oldAgriType = oldAgriType;
	}

	public String getOldAgriLoanTer() {
		return oldAgriLoanTer;
	}

	public void setOldAgriLoanTer(String oldAgriLoanTer) {
		this.oldAgriLoanTer = oldAgriLoanTer;
	}



	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param modifyType
	 */
	public void setModifyType(String modifyType) {
		this.modifyType = modifyType;
	}
	
    /**
     * @return modifyType
     */
	public String getModifyType() {
		return this.modifyType;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail;
	}
	
    /**
     * @return loanTypeDetail
     */
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}
	
	/**
	 * @param isPactLoan
	 */
	public void setIsPactLoan(String isPactLoan) {
		this.isPactLoan = isPactLoan;
	}
	
    /**
     * @return isPactLoan
     */
	public String getIsPactLoan() {
		return this.isPactLoan;
	}
	
	/**
	 * @param isGreenIndustry
	 */
	public void setIsGreenIndustry(String isGreenIndustry) {
		this.isGreenIndustry = isGreenIndustry;
	}
	
    /**
     * @return isGreenIndustry
     */
	public String getIsGreenIndustry() {
		return this.isGreenIndustry;
	}
	
	/**
	 * @param isOperPropertyLoan
	 */
	public void setIsOperPropertyLoan(String isOperPropertyLoan) {
		this.isOperPropertyLoan = isOperPropertyLoan;
	}
	
    /**
     * @return isOperPropertyLoan
     */
	public String getIsOperPropertyLoan() {
		return this.isOperPropertyLoan;
	}
	
	/**
	 * @param isSteelLoan
	 */
	public void setIsSteelLoan(String isSteelLoan) {
		this.isSteelLoan = isSteelLoan;
	}
	
    /**
     * @return isSteelLoan
     */
	public String getIsSteelLoan() {
		return this.isSteelLoan;
	}
	
	/**
	 * @param isStainlessLoan
	 */
	public void setIsStainlessLoan(String isStainlessLoan) {
		this.isStainlessLoan = isStainlessLoan;
	}
	
    /**
     * @return isStainlessLoan
     */
	public String getIsStainlessLoan() {
		return this.isStainlessLoan;
	}
	
	/**
	 * @param isPovertyReliefLoan
	 */
	public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
		this.isPovertyReliefLoan = isPovertyReliefLoan;
	}
	
    /**
     * @return isPovertyReliefLoan
     */
	public String getIsPovertyReliefLoan() {
		return this.isPovertyReliefLoan;
	}
	
	/**
	 * @param isLaborIntenSbsyLoan
	 */
	public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
		this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
	}
	
    /**
     * @return isLaborIntenSbsyLoan
     */
	public String getIsLaborIntenSbsyLoan() {
		return this.isLaborIntenSbsyLoan;
	}
	
	/**
	 * @param goverSubszHouseLoan
	 */
	public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
		this.goverSubszHouseLoan = goverSubszHouseLoan;
	}
	
    /**
     * @return goverSubszHouseLoan
     */
	public String getGoverSubszHouseLoan() {
		return this.goverSubszHouseLoan;
	}
	
	/**
	 * @param engyEnviProteLoan
	 */
	public void setEngyEnviProteLoan(String engyEnviProteLoan) {
		this.engyEnviProteLoan = engyEnviProteLoan;
	}
	
    /**
     * @return engyEnviProteLoan
     */
	public String getEngyEnviProteLoan() {
		return this.engyEnviProteLoan;
	}
	
	/**
	 * @param isCphsRurDelpLoan
	 */
	public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
		this.isCphsRurDelpLoan = isCphsRurDelpLoan;
	}
	
    /**
     * @return isCphsRurDelpLoan
     */
	public String getIsCphsRurDelpLoan() {
		return this.isCphsRurDelpLoan;
	}
	
	/**
	 * @param realproLoan
	 */
	public void setRealproLoan(String realproLoan) {
		this.realproLoan = realproLoan;
	}
	
    /**
     * @return realproLoan
     */
	public String getRealproLoan() {
		return this.realproLoan;
	}
	
	/**
	 * @param realproLoanRate
	 */
	public void setRealproLoanRate(String realproLoanRate) {
		this.realproLoanRate = realproLoanRate;
	}
	
    /**
     * @return realproLoanRate
     */
	public String getRealproLoanRate() {
		return this.realproLoanRate;
	}
	
	/**
	 * @param guarDetailMode
	 */
	public void setGuarDetailMode(String guarDetailMode) {
		this.guarDetailMode = guarDetailMode;
	}
	
    /**
     * @return guarDetailMode
     */
	public String getGuarDetailMode() {
		return this.guarDetailMode;
	}
	
	/**
	 * @param oldLoanTypeDetail
	 */
	public void setOldLoanTypeDetail(String oldLoanTypeDetail) {
		this.oldLoanTypeDetail = oldLoanTypeDetail;
	}
	
    /**
     * @return oldLoanTypeDetail
     */
	public String getOldLoanTypeDetail() {
		return this.oldLoanTypeDetail;
	}
	
	/**
	 * @param oldIsPactLoan
	 */
	public void setOldIsPactLoan(String oldIsPactLoan) {
		this.oldIsPactLoan = oldIsPactLoan;
	}
	
    /**
     * @return oldIsPactLoan
     */
	public String getOldIsPactLoan() {
		return this.oldIsPactLoan;
	}
	
	/**
	 * @param oldIsGreenIndustry
	 */
	public void setOldIsGreenIndustry(String oldIsGreenIndustry) {
		this.oldIsGreenIndustry = oldIsGreenIndustry;
	}
	
    /**
     * @return oldIsGreenIndustry
     */
	public String getOldIsGreenIndustry() {
		return this.oldIsGreenIndustry;
	}
	
	/**
	 * @param oldIsOperPropertyLoan
	 */
	public void setOldIsOperPropertyLoan(String oldIsOperPropertyLoan) {
		this.oldIsOperPropertyLoan = oldIsOperPropertyLoan;
	}
	
    /**
     * @return oldIsOperPropertyLoan
     */
	public String getOldIsOperPropertyLoan() {
		return this.oldIsOperPropertyLoan;
	}
	
	/**
	 * @param oldIsSteelLoan
	 */
	public void setOldIsSteelLoan(String oldIsSteelLoan) {
		this.oldIsSteelLoan = oldIsSteelLoan;
	}
	
    /**
     * @return oldIsSteelLoan
     */
	public String getOldIsSteelLoan() {
		return this.oldIsSteelLoan;
	}
	
	/**
	 * @param oldIsStainlessLoan
	 */
	public void setOldIsStainlessLoan(String oldIsStainlessLoan) {
		this.oldIsStainlessLoan = oldIsStainlessLoan;
	}
	
    /**
     * @return oldIsStainlessLoan
     */
	public String getOldIsStainlessLoan() {
		return this.oldIsStainlessLoan;
	}
	
	/**
	 * @param oldIsPovertyReliefLoan
	 */
	public void setOldIsPovertyReliefLoan(String oldIsPovertyReliefLoan) {
		this.oldIsPovertyReliefLoan = oldIsPovertyReliefLoan;
	}
	
    /**
     * @return oldIsPovertyReliefLoan
     */
	public String getOldIsPovertyReliefLoan() {
		return this.oldIsPovertyReliefLoan;
	}
	
	/**
	 * @param oldIsLaborIntenSbsyLoan
	 */
	public void setOldIsLaborIntenSbsyLoan(String oldIsLaborIntenSbsyLoan) {
		this.oldIsLaborIntenSbsyLoan = oldIsLaborIntenSbsyLoan;
	}
	
    /**
     * @return oldIsLaborIntenSbsyLoan
     */
	public String getOldIsLaborIntenSbsyLoan() {
		return this.oldIsLaborIntenSbsyLoan;
	}
	
	/**
	 * @param oldGoverSubszHouseLoan
	 */
	public void setOldGoverSubszHouseLoan(String oldGoverSubszHouseLoan) {
		this.oldGoverSubszHouseLoan = oldGoverSubszHouseLoan;
	}
	
    /**
     * @return oldGoverSubszHouseLoan
     */
	public String getOldGoverSubszHouseLoan() {
		return this.oldGoverSubszHouseLoan;
	}
	
	/**
	 * @param oldEngyEnviProteLoan
	 */
	public void setOldEngyEnviProteLoan(String oldEngyEnviProteLoan) {
		this.oldEngyEnviProteLoan = oldEngyEnviProteLoan;
	}
	
    /**
     * @return oldEngyEnviProteLoan
     */
	public String getOldEngyEnviProteLoan() {
		return this.oldEngyEnviProteLoan;
	}
	
	/**
	 * @param oldIsCphsRurDelpLoan
	 */
	public void setOldIsCphsRurDelpLoan(String oldIsCphsRurDelpLoan) {
		this.oldIsCphsRurDelpLoan = oldIsCphsRurDelpLoan;
	}
	
    /**
     * @return oldIsCphsRurDelpLoan
     */
	public String getOldIsCphsRurDelpLoan() {
		return this.oldIsCphsRurDelpLoan;
	}
	
	/**
	 * @param oldRealproLoan
	 */
	public void setOldRealproLoan(String oldRealproLoan) {
		this.oldRealproLoan = oldRealproLoan;
	}
	
    /**
     * @return oldRealproLoan
     */
	public String getOldRealproLoan() {
		return this.oldRealproLoan;
	}
	
	/**
	 * @param oldRealproLoanRate
	 */
	public void setOldRealproLoanRate(String oldRealproLoanRate) {
		this.oldRealproLoanRate = oldRealproLoanRate;
	}
	
    /**
     * @return oldRealproLoanRate
     */
	public String getOldRealproLoanRate() {
		return this.oldRealproLoanRate;
	}
	
	/**
	 * @param oldGuarDetailMode
	 */
	public void setOldGuarDetailMode(String oldGuarDetailMode) {
		this.oldGuarDetailMode = oldGuarDetailMode;
	}
	
    /**
     * @return oldGuarDetailMode
     */
	public String getOldGuarDetailMode() {
		return this.oldGuarDetailMode;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
	
    /**
     * @return loanTer
     */
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param oldLoanTer
	 */
	public void setOldLoanTer(String oldLoanTer) {
		this.oldLoanTer = oldLoanTer;
	}
	
    /**
     * @return oldLoanTer
     */
	public String getOldLoanTer() {
		return this.oldLoanTer;
	}
	
	/**
	 * @param subjectNo
	 */
	public void setSubjectNo(String subjectNo) {
		this.subjectNo = subjectNo;
	}
	
    /**
     * @return subjectNo
     */
	public String getSubjectNo() {
		return this.subjectNo;
	}
	
	/**
	 * @param subjectName
	 */
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
    /**
     * @return subjectName
     */
	public String getSubjectName() {
		return this.subjectName;
	}
	
	/**
	 * @param oldSubjectNo
	 */
	public void setOldSubjectNo(String oldSubjectNo) {
		this.oldSubjectNo = oldSubjectNo;
	}
	
    /**
     * @return oldSubjectNo
     */
	public String getOldSubjectNo() {
		return this.oldSubjectNo;
	}
	
	/**
	 * @param oldSubjectName
	 */
	public void setOldSubjectName(String oldSubjectName) {
		this.oldSubjectName = oldSubjectName;
	}
	
    /**
     * @return oldSubjectName
     */
	public String getOldSubjectName() {
		return this.oldSubjectName;
	}
	
	/**
	 * @param modificationContent
	 */
	public void setModificationContent(String modificationContent) {
		this.modificationContent = modificationContent;
	}
	
    /**
     * @return modificationContent
     */
	public String getModificationContent() {
		return this.modificationContent;
	}
	
	/**
	 * @param modificationReason
	 */
	public void setModificationReason(String modificationReason) {
		this.modificationReason = modificationReason;
	}
	
    /**
     * @return modificationReason
     */
	public String getModificationReason() {
		return this.modificationReason;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}