package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.BizMustCheckDetails;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.XbdInfo;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.BizMustCheckDetailsService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.XbdInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0122Service {

    @Autowired
    private XbdInfoService xbdInfoService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    public RiskResultDto riskItem0122(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";
        if(Objects.nonNull(queryModel.getCondition().get("bizId"))){
            serno= queryModel.getCondition().get("bizId").toString();
        }
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        XbdInfo xbdInfo = xbdInfoService.selectByPrimaryKey(serno);
        if (Objects.nonNull(pvpLoanApp) && Objects.equals("P010", pvpLoanApp.getPrdTypeProp())) {
            if (Objects.isNull(xbdInfo)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012201);
                return riskResultDto;
            }
            if (StringUtils.isBlank(xbdInfo.getBdNo()) || StringUtils.isBlank(xbdInfo.getGuarContNo())
                    || StringUtils.isBlank(xbdInfo.getQrhNo()) || StringUtils.isBlank(xbdInfo.getInsuranceName())
                    || StringUtils.isBlank(xbdInfo.getInsuredName()) || Objects.isNull(xbdInfo.getCbLoanAmt())
                    || StringUtils.isBlank(xbdInfo.getBxStartDate()) || StringUtils.isBlank(xbdInfo.getBxEndDate())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012201);
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
