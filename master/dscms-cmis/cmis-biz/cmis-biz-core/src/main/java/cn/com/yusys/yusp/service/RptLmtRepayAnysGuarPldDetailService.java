/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.GuarBaseInfoRelDto;
import cn.com.yusys.yusp.dto.RptLmtRepayAnysGuarPldDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysGuarPldDetailMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPldDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-02 10:17:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysGuarPldDetailService {

    @Autowired
    private RptLmtRepayAnysGuarPldDetailMapper rptLmtRepayAnysGuarPldDetailMapper;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private RptLmtRepayAnysGuarZyService rptLmtRepayAnysGuarZyService;
    @Autowired
    private GuarInfLivingRoomService guarInfLivingRoomService;
    @Autowired
    private GuarBizRelService guarBizRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLmtRepayAnysGuarPldDetail selectByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarPldDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLmtRepayAnysGuarPldDetail> selectAll(QueryModel model) {
        List<RptLmtRepayAnysGuarPldDetail> records = (List<RptLmtRepayAnysGuarPldDetail>) rptLmtRepayAnysGuarPldDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLmtRepayAnysGuarPldDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnysGuarPldDetail> list = rptLmtRepayAnysGuarPldDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnysGuarPldDetail record) {
        return rptLmtRepayAnysGuarPldDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnysGuarPldDetail record) {
        return rptLmtRepayAnysGuarPldDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnysGuarPldDetail record) {
        return rptLmtRepayAnysGuarPldDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnysGuarPldDetail record) {
        return rptLmtRepayAnysGuarPldDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarPldDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * 初始化信息
     *
     * @param model
     * @return
     */
    public RptLmtRepayAnysGuarPldDto initPldDetail(QueryModel model) {
        RptLmtRepayAnysGuarPldDto rptLmtRepayAnysGuarPldDto = new RptLmtRepayAnysGuarPldDto();
        String serno = model.getCondition().get("serno").toString();
        List<RptLmtRepayAnysGuarPldDetail> rptLmtRepayAnysGuarPldDetails = rptLmtRepayAnysGuarPldDetailMapper.selectBySerno(serno);
        List<RptLmtRepayAnysGuarZy> rptLmtRepayAnysGuarZyList = rptLmtRepayAnysGuarZyService.selectBySerno(serno);
        if (CollectionUtils.nonEmpty(rptLmtRepayAnysGuarPldDetails)) {
            for (RptLmtRepayAnysGuarPldDetail temp : rptLmtRepayAnysGuarPldDetails) {
                String subSerno = temp.getSubSerno();
                String guarNo = temp.getGuarNo();
                QueryModel model1 = new QueryModel();
                model1.addCondition("serno", subSerno);
                List<GuarBaseInfoRelDto> guarBaseInfoRelDtos = guarBaseInfoService.queryGuarBaseInfoRelDtoByParams(model1);
                for (GuarBaseInfoRelDto guarBaseInfoRelDto : guarBaseInfoRelDtos) {
                    if (guarNo.equals(guarBaseInfoRelDto.getGuarNo())) {
                        temp.setCorreFinAmt(guarBaseInfoRelDto.getCorreFinAmt());
                        temp.setEvalAmt(guarBaseInfoRelDto.getEvalAmt());
                        //抵押率
                        BigDecimal correFinAmt = guarBaseInfoRelDto.getCorreFinAmt();
                        BigDecimal maxMortagageAmt = guarBaseInfoRelDto.getMaxMortagageAmt();
                        BigDecimal mortagageRate = BigDecimal.ZERO;
                        if(Objects.nonNull(correFinAmt)&&Objects.nonNull(maxMortagageAmt)&&maxMortagageAmt.compareTo(BigDecimal.ZERO)>0){
                            mortagageRate = correFinAmt.divide(maxMortagageAmt,4,BigDecimal.ROUND_HALF_UP);
                        }
                        temp.setMaxMortagageAmt(guarBaseInfoRelDto.getMaxMortagageAmt());
                        temp.setMortagageRate(mortagageRate);
                        rptLmtRepayAnysGuarPldDetailMapper.updateByPrimaryKey(temp);
                        break;
                    }
                }
            }
            rptLmtRepayAnysGuarPldDto.setRptLmtRepayAnysGuarPldDetailList(rptLmtRepayAnysGuarPldDetails);
        }
        if (CollectionUtils.nonEmpty(rptLmtRepayAnysGuarZyList)) {
            rptLmtRepayAnysGuarPldDto.setRptLmtRepayAnysGuarZyList(rptLmtRepayAnysGuarZyList);
        }
        if (CollectionUtils.nonEmpty(rptLmtRepayAnysGuarPldDto.getRptLmtRepayAnysGuarPldDetailList()) || CollectionUtils.nonEmpty(rptLmtRepayAnysGuarPldDto.getRptLmtRepayAnysGuarZyList())) {
            return rptLmtRepayAnysGuarPldDto;
        }
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        //获取抵押品信息
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                //分项额度为0时押品不引入显示
                BigDecimal amt = lmtAppSub.getLmtAmt();
                if(Objects.isNull(amt)||amt.compareTo(BigDecimal.ZERO) ==0){
                    continue;
                }
                if (CmisCommonConstants.GUAR_MODE_10.equals(lmtAppSub.getGuarMode()) || CmisCommonConstants.GUAR_MODE_20.equals(lmtAppSub.getGuarMode())) {
                    String subSerno = lmtAppSub.getSubSerno();
                    BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno", subSerno);
                    List<GuarBaseInfoRelDto> guarBaseInfoRelDtos = guarBaseInfoService.queryGuarBaseInfoRelDtoByParams(queryModel);
                    if (CollectionUtils.nonEmpty(guarBaseInfoRelDtos)) {
                        List<RptLmtRepayAnysGuarPldDetail> result1 = new ArrayList<>();
                        List<RptLmtRepayAnysGuarZy> result2 = new ArrayList<>();
                        for (GuarBaseInfoRelDto guarBaseInfoRelDto : guarBaseInfoRelDtos) {
                            if (CmisBizConstants.STD_GRT_FLAG_01.equals(guarBaseInfoRelDto.getGrtFlag())) {
                                RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail = new RptLmtRepayAnysGuarPldDetail();
                                rptLmtRepayAnysGuarPldDetail.setSerno(serno);
                                rptLmtRepayAnysGuarPldDetail.setSubSerno(subSerno);
                                rptLmtRepayAnysGuarPldDetail.setPldimnMemo(guarBaseInfoRelDto.getPldimnMemo());
                                rptLmtRepayAnysGuarPldDetail.setMaxMortagageAmt(guarBaseInfoRelDto.getMaxMortagageAmt());
                                rptLmtRepayAnysGuarPldDetail.setPkId(StringUtils.getUUID());
                                rptLmtRepayAnysGuarPldDetail.setGuarNo(guarBaseInfoRelDto.getGuarNo());
                                rptLmtRepayAnysGuarPldDetail.setGuarTypeCd(guarBaseInfoRelDto.getGuarTypeCd());
                                rptLmtRepayAnysGuarPldDetail.setGuarCusId(guarBaseInfoRelDto.getGuarCusId());
                                rptLmtRepayAnysGuarPldDetail.setGuarCusName(guarBaseInfoRelDto.getGuarCusName());
                                rptLmtRepayAnysGuarPldDetail.setEvalAmt(guarBaseInfoRelDto.getEvalAmt());
                                rptLmtRepayAnysGuarPldDetail.setCorreFinAmt(guarBaseInfoRelDto.getCorreFinAmt());
                                //抵押率
                                BigDecimal correFinAmt = guarBaseInfoRelDto.getCorreFinAmt();
                                BigDecimal maxMortagageAmt = guarBaseInfoRelDto.getMaxMortagageAmt();
                                BigDecimal mortagageRate = BigDecimal.ZERO;
                                if(Objects.nonNull(maxMortagageAmt)&&maxMortagageAmt.compareTo(BigDecimal.ZERO)>0){
                                    mortagageRate = correFinAmt.divide(maxMortagageAmt,4,BigDecimal.ROUND_HALF_UP);
                                }
                                rptLmtRepayAnysGuarPldDetail.setMortagageRate(mortagageRate);
                                //面积
                                if (StringUtils.nonBlank(guarBaseInfoRelDto.getSqu())) {
                                    rptLmtRepayAnysGuarPldDetail.setPldSqu(BigDecimal.valueOf(Double.parseDouble(guarBaseInfoRelDto.getSqu())));
                                }
                                //抵押物地址
                                rptLmtRepayAnysGuarPldDetail.setPldAddress(guarBaseInfoRelDto.getPldLocation());
                                rptLmtRepayAnysGuarPldDetail.setBorrowRel(guarBaseInfoRelDto.getPldimnDebitRelative());
                                int count1 = rptLmtRepayAnysGuarPldDetailMapper.insertSelective(rptLmtRepayAnysGuarPldDetail);
                                result1.add(rptLmtRepayAnysGuarPldDetail);
                            } else if (CmisBizConstants.STD_GRT_FLAG_02.equals(guarBaseInfoRelDto.getGrtFlag())) {
                                RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy = new RptLmtRepayAnysGuarZy();
                                rptLmtRepayAnysGuarZy.setPkId(StringUtils.getUUID());
                                rptLmtRepayAnysGuarZy.setSerno(serno);
                                rptLmtRepayAnysGuarZy.setPledInfor(guarBaseInfoRelDto.getPldimnMemo());
                                rptLmtRepayAnysGuarZy.setOwner(guarBaseInfoRelDto.getGuarCusName());
                                //质押率
                                BigDecimal correFinAmt = guarBaseInfoRelDto.getCorreFinAmt();
                                BigDecimal maxMortagageAmt = guarBaseInfoRelDto.getMaxMortagageAmt();
                                BigDecimal mortagageRate = BigDecimal.ZERO;
                                if(Objects.nonNull(maxMortagageAmt)&&maxMortagageAmt.compareTo(BigDecimal.ZERO)>0){
                                    mortagageRate = correFinAmt.divide(maxMortagageAmt,4,BigDecimal.ROUND_HALF_UP);
                                }
                                rptLmtRepayAnysGuarZy.setPledCurVal(maxMortagageAmt);
                                rptLmtRepayAnysGuarZy.setPledRate(mortagageRate);
                                int count2 = rptLmtRepayAnysGuarZyService.insertSelective(rptLmtRepayAnysGuarZy);
                                result2.add(rptLmtRepayAnysGuarZy);
                            }
                        }
                        rptLmtRepayAnysGuarPldDto.setRptLmtRepayAnysGuarPldDetailList(result1);
                        rptLmtRepayAnysGuarPldDto.setRptLmtRepayAnysGuarZyList(result2);
                    }
                }
            }
        }
        return rptLmtRepayAnysGuarPldDto;
    }

    /**
     * 更新押品信息
     *
     * @param model
     * @return
     */
    public RptLmtRepayAnysGuarPldDto updatePldDetail(QueryModel model) {
        String serno = model.getCondition().get("serno").toString();
        rptLmtRepayAnysGuarPldDetailMapper.deleteBySerno(serno);
        rptLmtRepayAnysGuarZyService.deleteBySerno(serno);
        return initPldDetail(model);
    }

}
