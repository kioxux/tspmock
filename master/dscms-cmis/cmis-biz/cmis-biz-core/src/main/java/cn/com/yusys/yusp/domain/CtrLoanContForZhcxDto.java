/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrHighAmtAgrCont
 * @类描述: ctr_high_amt_agr_cont数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 14:40:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrLoanContForZhcxDto extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;


	/** 合同编号 **/
	private String contNo;

	/** 中文合同编号 **/
	private String contCnNo;

	/** 全局流水号 **/
	private String serno;

	/** 调查编号 **/
	private String surveySerno;

	/** 业务申请流水号 **/
	private String iqpSerno;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 产品类型属性 **/
	private String prdTypeProp;

	/** 贷款用途 **/
	private String loanPurp;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 手机号码 **/
	private String phone;

	/** 业务类型 **/
	private String bizType;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 特殊业务类型 **/
	private String especBizType;

	/** 贷款形式 **/
	private String loanModal;

	/** 贷款性质 **/
	private String loanCha;

	/** 是否曾被拒绝 **/
	private String isHasRefused;

	/** 主担保方式 **/
	private String guarWay;

	/** 是否共同申请人 **/
	private String isCommonRqstr;

	/** 是否确认支付方式 **/
	private String isCfirmPayWay;

	/** 支付方式 **/
	private String payMode;

	/** 币种 **/
	private String curType;

	/** 合同金额 **/
	private java.math.BigDecimal contAmt;

	/** 已用金额 **/
	private java.math.BigDecimal useAmt;

	/** 合同余额 **/
	private java.math.BigDecimal contBalance;

	/** 合同汇率 **/
	private java.math.BigDecimal contRate;

	/** 合同类型 **/
	private String contType;

	/** 纸质合同签订日期 **/
	private String paperContSignDate;

	/** 当前LPR利率(%) **/
	private java.math.BigDecimal curtLprRate;

	/** LPR定价区间 **/
	private String lprPriceInterval;

	/** LPR浮动点(BP) **/
	private java.math.BigDecimal lprBp;

	/** 结息方式 **/
	private String eiMode;

	/** 结息具体说明 **/
	private String eiModeExpl;

	/** 提款方式 **/
	private String drawMode;

	/** 提款天数限制 **/
	private String dayLimit;

	/** 地址 **/
	private String addr;

	/** 传真 **/
	private String fax;

	/** 邮箱 **/
	private String email;

	/** QQ **/
	private String qq;

	/** 微信 **/
	private String wechat;

	/** 其他通讯方式及账号 **/
	private String otherPhone;

	/** 双录编号 **/
	private String doubleRecordNo;

	/** 签订方式 **/
	private String signMode;

	/** 签约渠道 **/
	private String signChannel;

	/** 所属条线 **/
	private String belgLine;

	/** 保证金来源 **/
	private String bailSour;

	/** 保证金汇率 **/
	private java.math.BigDecimal bailExchangeRate;

	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;

	/** 保证金币种 **/
	private String bailCurType;

	/** 保证金金额 **/
	private java.math.BigDecimal bailAmt;

	/** 保证金折算人民币金额 **/
	private java.math.BigDecimal bailCvtCnyAmt;

	/** 合同起始日期 **/
	private String contStartDate;

	/** 合同到期日期 **/
	private String contEndDate;

	/** 期限类型 **/
	private String termType;

	/** 申请期限 **/
	private java.math.BigDecimal appTerm;

	/** 利率依据方式 **/
	private String irAccordType;

	/** 利率种类 **/
	private String irType;

	/** 基准利率（年） **/
	private java.math.BigDecimal rulingIr;

	/** 对应基准利率(月) **/
	private java.math.BigDecimal rulingIrM;

	/** 计息方式 **/
	private String loanRatType;

	/** 利率调整类型 **/
	private String irAdjustType;

	/** 利率调整周期(月) **/
	private java.math.BigDecimal irAdjustTerm;

	/** 调息方式 **/
	private String praType;

	/** 利率形式 **/
	private String rateType;

	/** 正常利率浮动方式 **/
	private String irFloatType;

	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;

	/** 固定加点值 **/
	private java.math.BigDecimal irFloatPoint;

	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;

	/** 执行利率(月) **/
	private java.math.BigDecimal realityIrM;

	/** 逾期利率浮动方式 **/
	private String overdueFloatType;

	/** 逾期利率浮动加点值 **/
	private java.math.BigDecimal overduePoint;

	/** 逾期利率浮动百分比 **/
	private java.math.BigDecimal overdueRate;

	/** 逾期利率(年) **/
	private java.math.BigDecimal overdueRateY;

	/** 违约利率浮动方式 **/
	private String defaultFloatType;

	/** 违约利率浮动加点值 **/
	private java.math.BigDecimal defaultPoint;

	/** 违约利率浮动百分比 **/
	private java.math.BigDecimal defaultRate;

	/** 违约利率(年) **/
	private java.math.BigDecimal defaultRateY;

	/** 风险敞口金额 **/
	private java.math.BigDecimal riskOpenAmt;

	/** 还款方式 **/
	private String repayMode;

	/** 停本付息期间 **/
	private String stopPintTerm;

	/** 还款间隔周期 **/
	private String repayTerm;

	/** 还款间隔 **/
	private String repaySpace;

	/** 还款日确定规则 **/
	private String repayRule;

	/** 还款日类型 **/
	private String repayDtType;

	/** 还款日 **/
	private String repayDate;

	/** 本金宽限方式 **/
	private String capGraperType;

	/** 本金宽限天数 **/
	private String capGraperDay;

	/** 利息宽限方式 **/
	private String intGraperType;

	/** 利息宽限天数 **/
	private String intGraperDay;

	/** 扣款扣息方式 **/
	private String deductDeduType;

	/** 还款频率类型 **/
	private String repayFreType;

	/** 本息还款频率 **/
	private String repayFre;

	/** 提前还款违约金免除时间(月) **/
	private Integer liquFreeTime;

	/** 分段方式 **/
	private String subType;

	/** 保留期限 **/
	private Integer reserveTerm;

	/** 计算期限 **/
	private Integer calTerm;

	/** 保留金额 **/
	private java.math.BigDecimal reserveAmt;

	/** 第一阶段还款期数 **/
	private Integer repayTermOne;

	/** 第一阶段还款本金 **/
	private java.math.BigDecimal repayAmtOne;

	/** 第二阶段还款期数 **/
	private Integer repayTermTwo;

	/** 第二阶段还款本金 **/
	private java.math.BigDecimal repayAmtTwo;

	/** 利率选取日期种类 **/
	private String rateSelType;

	/** 贴息方式 **/
	private String sbsyMode;

	/** 贴息比例 **/
	private java.math.BigDecimal sbsyPerc;

	/** 贴息金额 **/
	private java.math.BigDecimal sbsyAmt;

	/** 贴息单位名称 **/
	private String sbsyUnitName;

	/** 贴息方账户 **/
	private String sbsyAcct;

	/** 贴息方账户户名 **/
	private String sbsyAcctName;

	/** 五级分类 **/
	private String fiveClass;

	/** 贷款投向 **/
	private String loanTer;

	/** 工业转型升级标识 **/
	private String comUpIndtify;

	/** 战略新兴产业类型 **/
	private String strategyNewLoan;

	/** 是否文化产业 **/
	private String isCulEstate;

	/** 贷款种类 **/
	private String loanType;

	/** 产业结构调整类型 **/
	private String estateAdjustType;

	/** 新兴产业贷款 **/
	private String newPrdLoan;

	/** 还款来源 **/
	private String repaySour;

	/** 是否委托人办理 **/
	private String isAuthorize;

	/** 委托人姓名 **/
	private String authedName;

	/** 委托人证件类型 **/
	private String consignorCertType;

	/** 委托人证件号 **/
	private String consignorCertCode;

	/** 委托人联系方式 **/
	private String authedTelNo;

	/** 签订日期 **/
	private String signDate;

	/** 注销日期 **/
	private String logoutDate;

	/** 渠道来源 **/
	private String chnlSour;

	/** 争议解决方式 **/
	private String billDispupeOpt;

	/** 法院所在地 **/
	private String courtAddr;

	/** 仲裁委员会 **/
	private String arbitrateBch;

	/** 仲裁委员会地点 **/
	private String arbitrateAddr;

	/** 其他方式 **/
	private String otherOpt;

	/** 合同份数 **/
	private java.math.BigDecimal contQnt;

	/** 公积金贷款合同编号 **/
	private String pundContNo;

	/** 补充条款 **/
	private String spplClause;

	/** 签约地点 **/
	private String signAddr;

	/** 营业网点 **/
	private String busiNetwork;

	/** 主要营业场所地址 **/
	private String mainBusiPalce;

	/** 合同模板 **/
	private String contTemplate;

	/** 合同打印次数 **/
	private java.math.BigDecimal contPrintNum;

	/** 合同状态 **/
	private String contStatus;

	/** 签章审批状态 **/
	private String signApproveStatus;

	/** 所属团队 **/
	private String team;

	/** 主管机构 **/
	private String managerBrId;

	/** 主管客户经理 **/
	private String managerId;

	/** 登记人 **/
	private String inputId;

	/** 登记日期 **/
	private String inputDate;

	/** 登记机构 **/
	private String inputBrId;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 其他约定 **/
	private String otherAgreed;

	/** 开户行名称 **/
	private String acctsvcrName;

	/** 贷款发放账号 **/
	private String loanPayoutAccno;

	/** 贷款发放账号名称 **/
	private String loanPayoutAccName;

	/** 合同模式 **/
	private String contMode;

	/** 是否线上提款 **/
	private String isOnlineDraw;

	/** 线上合同启用标识 **/
	private String ctrBeginFlag;

	/** 操作类型 **/
	private String oprType;

	/** 申请状态 **/
	private String approveStatus;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 本合同项下最高可用信金额 **/
	private java.math.BigDecimal highAvlAmt;

	/** 是否续签 **/
	private String isRenew;

	/** 原合同编号 **/
	private String origiContNo;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 是否在线抵押 **/
	private String isOlPld;

	/** 是否先放款后抵押 **/
	private String beforehandInd;

	/** 是否电子用印 **/
	private String isESeal;

	/** 借款利率调整日 **/
	private String loanRateAdjDay;

	/** 是否无缝对接 **/
	private String isSeajnt;

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getContCnNo() {
		return contCnNo;
	}

	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getSurveySerno() {
		return surveySerno;
	}

	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}

	public String getIqpSerno() {
		return iqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPrdTypeProp() {
		return prdTypeProp;
	}

	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}

	public String getLoanPurp() {
		return loanPurp;
	}

	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getLmtAccNo() {
		return lmtAccNo;
	}

	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	public String getEspecBizType() {
		return especBizType;
	}

	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType;
	}

	public String getLoanModal() {
		return loanModal;
	}

	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	public String getLoanCha() {
		return loanCha;
	}

	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha;
	}

	public String getIsHasRefused() {
		return isHasRefused;
	}

	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused;
	}

	public String getGuarWay() {
		return guarWay;
	}

	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}

	public String getIsCommonRqstr() {
		return isCommonRqstr;
	}

	public void setIsCommonRqstr(String isCommonRqstr) {
		this.isCommonRqstr = isCommonRqstr;
	}

	public String getIsCfirmPayWay() {
		return isCfirmPayWay;
	}

	public void setIsCfirmPayWay(String isCfirmPayWay) {
		this.isCfirmPayWay = isCfirmPayWay;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getCurType() {
		return curType;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public BigDecimal getUseAmt() {
		return useAmt;
	}

	public void setUseAmt(BigDecimal useAmt) {
		this.useAmt = useAmt;
	}

	public BigDecimal getContBalance() {
		return contBalance;
	}

	public void setContBalance(BigDecimal contBalance) {
		this.contBalance = contBalance;
	}

	public BigDecimal getContRate() {
		return contRate;
	}

	public void setContRate(BigDecimal contRate) {
		this.contRate = contRate;
	}

	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType;
	}

	public String getPaperContSignDate() {
		return paperContSignDate;
	}

	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}

	public BigDecimal getCurtLprRate() {
		return curtLprRate;
	}

	public void setCurtLprRate(BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	public String getLprPriceInterval() {
		return lprPriceInterval;
	}

	public void setLprPriceInterval(String lprPriceInterval) {
		this.lprPriceInterval = lprPriceInterval;
	}

	public BigDecimal getLprBp() {
		return lprBp;
	}

	public void setLprBp(BigDecimal lprBp) {
		this.lprBp = lprBp;
	}

	public String getEiMode() {
		return eiMode;
	}

	public void setEiMode(String eiMode) {
		this.eiMode = eiMode;
	}

	public String getEiModeExpl() {
		return eiModeExpl;
	}

	public void setEiModeExpl(String eiModeExpl) {
		this.eiModeExpl = eiModeExpl;
	}

	public String getDrawMode() {
		return drawMode;
	}

	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode;
	}

	public String getDayLimit() {
		return dayLimit;
	}

	public void setDayLimit(String dayLimit) {
		this.dayLimit = dayLimit;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getDoubleRecordNo() {
		return doubleRecordNo;
	}

	public void setDoubleRecordNo(String doubleRecordNo) {
		this.doubleRecordNo = doubleRecordNo;
	}

	public String getSignMode() {
		return signMode;
	}

	public void setSignMode(String signMode) {
		this.signMode = signMode;
	}

	public String getSignChannel() {
		return signChannel;
	}

	public void setSignChannel(String signChannel) {
		this.signChannel = signChannel;
	}

	public String getBelgLine() {
		return belgLine;
	}

	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	public String getBailSour() {
		return bailSour;
	}

	public void setBailSour(String bailSour) {
		this.bailSour = bailSour;
	}

	public BigDecimal getBailExchangeRate() {
		return bailExchangeRate;
	}

	public void setBailExchangeRate(BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}

	public BigDecimal getBailPerc() {
		return bailPerc;
	}

	public void setBailPerc(BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}

	public String getBailCurType() {
		return bailCurType;
	}

	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}

	public BigDecimal getBailAmt() {
		return bailAmt;
	}

	public void setBailAmt(BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	public BigDecimal getBailCvtCnyAmt() {
		return bailCvtCnyAmt;
	}

	public void setBailCvtCnyAmt(BigDecimal bailCvtCnyAmt) {
		this.bailCvtCnyAmt = bailCvtCnyAmt;
	}

	public String getContStartDate() {
		return contStartDate;
	}

	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}

	public String getContEndDate() {
		return contEndDate;
	}

	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate;
	}

	public String getTermType() {
		return termType;
	}

	public void setTermType(String termType) {
		this.termType = termType;
	}

	public BigDecimal getAppTerm() {
		return appTerm;
	}

	public void setAppTerm(BigDecimal appTerm) {
		this.appTerm = appTerm;
	}

	public String getIrAccordType() {
		return irAccordType;
	}

	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType;
	}

	public String getIrType() {
		return irType;
	}

	public void setIrType(String irType) {
		this.irType = irType;
	}

	public BigDecimal getRulingIr() {
		return rulingIr;
	}

	public void setRulingIr(BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

	public BigDecimal getRulingIrM() {
		return rulingIrM;
	}

	public void setRulingIrM(BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}

	public String getLoanRatType() {
		return loanRatType;
	}

	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType;
	}

	public String getIrAdjustType() {
		return irAdjustType;
	}

	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType;
	}

	public BigDecimal getIrAdjustTerm() {
		return irAdjustTerm;
	}

	public void setIrAdjustTerm(BigDecimal irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}

	public String getPraType() {
		return praType;
	}

	public void setPraType(String praType) {
		this.praType = praType;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getIrFloatType() {
		return irFloatType;
	}

	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}

	public BigDecimal getIrFloatRate() {
		return irFloatRate;
	}

	public void setIrFloatRate(BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	public BigDecimal getIrFloatPoint() {
		return irFloatPoint;
	}

	public void setIrFloatPoint(BigDecimal irFloatPoint) {
		this.irFloatPoint = irFloatPoint;
	}

	public BigDecimal getExecRateYear() {
		return execRateYear;
	}

	public void setExecRateYear(BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	public BigDecimal getRealityIrM() {
		return realityIrM;
	}

	public void setRealityIrM(BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}

	public String getOverdueFloatType() {
		return overdueFloatType;
	}

	public void setOverdueFloatType(String overdueFloatType) {
		this.overdueFloatType = overdueFloatType;
	}

	public BigDecimal getOverduePoint() {
		return overduePoint;
	}

	public void setOverduePoint(BigDecimal overduePoint) {
		this.overduePoint = overduePoint;
	}

	public BigDecimal getOverdueRate() {
		return overdueRate;
	}

	public void setOverdueRate(BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}

	public BigDecimal getOverdueRateY() {
		return overdueRateY;
	}

	public void setOverdueRateY(BigDecimal overdueRateY) {
		this.overdueRateY = overdueRateY;
	}

	public String getDefaultFloatType() {
		return defaultFloatType;
	}

	public void setDefaultFloatType(String defaultFloatType) {
		this.defaultFloatType = defaultFloatType;
	}

	public BigDecimal getDefaultPoint() {
		return defaultPoint;
	}

	public void setDefaultPoint(BigDecimal defaultPoint) {
		this.defaultPoint = defaultPoint;
	}

	public BigDecimal getDefaultRate() {
		return defaultRate;
	}

	public void setDefaultRate(BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

	public BigDecimal getDefaultRateY() {
		return defaultRateY;
	}

	public void setDefaultRateY(BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}

	public BigDecimal getRiskOpenAmt() {
		return riskOpenAmt;
	}

	public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}

	public String getRepayMode() {
		return repayMode;
	}

	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}

	public String getStopPintTerm() {
		return stopPintTerm;
	}

	public void setStopPintTerm(String stopPintTerm) {
		this.stopPintTerm = stopPintTerm;
	}

	public String getRepayTerm() {
		return repayTerm;
	}

	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm;
	}

	public String getRepaySpace() {
		return repaySpace;
	}

	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace;
	}

	public String getRepayRule() {
		return repayRule;
	}

	public void setRepayRule(String repayRule) {
		this.repayRule = repayRule;
	}

	public String getRepayDtType() {
		return repayDtType;
	}

	public void setRepayDtType(String repayDtType) {
		this.repayDtType = repayDtType;
	}

	public String getRepayDate() {
		return repayDate;
	}

	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}

	public String getCapGraperType() {
		return capGraperType;
	}

	public void setCapGraperType(String capGraperType) {
		this.capGraperType = capGraperType;
	}

	public String getCapGraperDay() {
		return capGraperDay;
	}

	public void setCapGraperDay(String capGraperDay) {
		this.capGraperDay = capGraperDay;
	}

	public String getIntGraperType() {
		return intGraperType;
	}

	public void setIntGraperType(String intGraperType) {
		this.intGraperType = intGraperType;
	}

	public String getIntGraperDay() {
		return intGraperDay;
	}

	public void setIntGraperDay(String intGraperDay) {
		this.intGraperDay = intGraperDay;
	}

	public String getDeductDeduType() {
		return deductDeduType;
	}

	public void setDeductDeduType(String deductDeduType) {
		this.deductDeduType = deductDeduType;
	}

	public String getRepayFreType() {
		return repayFreType;
	}

	public void setRepayFreType(String repayFreType) {
		this.repayFreType = repayFreType;
	}

	public String getRepayFre() {
		return repayFre;
	}

	public void setRepayFre(String repayFre) {
		this.repayFre = repayFre;
	}

	public Integer getLiquFreeTime() {
		return liquFreeTime;
	}

	public void setLiquFreeTime(Integer liquFreeTime) {
		this.liquFreeTime = liquFreeTime;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Integer getReserveTerm() {
		return reserveTerm;
	}

	public void setReserveTerm(Integer reserveTerm) {
		this.reserveTerm = reserveTerm;
	}

	public Integer getCalTerm() {
		return calTerm;
	}

	public void setCalTerm(Integer calTerm) {
		this.calTerm = calTerm;
	}

	public BigDecimal getReserveAmt() {
		return reserveAmt;
	}

	public void setReserveAmt(BigDecimal reserveAmt) {
		this.reserveAmt = reserveAmt;
	}

	public Integer getRepayTermOne() {
		return repayTermOne;
	}

	public void setRepayTermOne(Integer repayTermOne) {
		this.repayTermOne = repayTermOne;
	}

	public BigDecimal getRepayAmtOne() {
		return repayAmtOne;
	}

	public void setRepayAmtOne(BigDecimal repayAmtOne) {
		this.repayAmtOne = repayAmtOne;
	}

	public Integer getRepayTermTwo() {
		return repayTermTwo;
	}

	public void setRepayTermTwo(Integer repayTermTwo) {
		this.repayTermTwo = repayTermTwo;
	}

	public BigDecimal getRepayAmtTwo() {
		return repayAmtTwo;
	}

	public void setRepayAmtTwo(BigDecimal repayAmtTwo) {
		this.repayAmtTwo = repayAmtTwo;
	}

	public String getRateSelType() {
		return rateSelType;
	}

	public void setRateSelType(String rateSelType) {
		this.rateSelType = rateSelType;
	}

	public String getSbsyMode() {
		return sbsyMode;
	}

	public void setSbsyMode(String sbsyMode) {
		this.sbsyMode = sbsyMode;
	}

	public BigDecimal getSbsyPerc() {
		return sbsyPerc;
	}

	public void setSbsyPerc(BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

	public BigDecimal getSbsyAmt() {
		return sbsyAmt;
	}

	public void setSbsyAmt(BigDecimal sbsyAmt) {
		this.sbsyAmt = sbsyAmt;
	}

	public String getSbsyUnitName() {
		return sbsyUnitName;
	}

	public void setSbsyUnitName(String sbsyUnitName) {
		this.sbsyUnitName = sbsyUnitName;
	}

	public String getSbsyAcct() {
		return sbsyAcct;
	}

	public void setSbsyAcct(String sbsyAcct) {
		this.sbsyAcct = sbsyAcct;
	}

	public String getSbsyAcctName() {
		return sbsyAcctName;
	}

	public void setSbsyAcctName(String sbsyAcctName) {
		this.sbsyAcctName = sbsyAcctName;
	}

	public String getFiveClass() {
		return fiveClass;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	public String getLoanTer() {
		return loanTer;
	}

	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}

	public String getComUpIndtify() {
		return comUpIndtify;
	}

	public void setComUpIndtify(String comUpIndtify) {
		this.comUpIndtify = comUpIndtify;
	}

	public String getStrategyNewLoan() {
		return strategyNewLoan;
	}

	public void setStrategyNewLoan(String strategyNewLoan) {
		this.strategyNewLoan = strategyNewLoan;
	}

	public String getIsCulEstate() {
		return isCulEstate;
	}

	public void setIsCulEstate(String isCulEstate) {
		this.isCulEstate = isCulEstate;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getEstateAdjustType() {
		return estateAdjustType;
	}

	public void setEstateAdjustType(String estateAdjustType) {
		this.estateAdjustType = estateAdjustType;
	}

	public String getNewPrdLoan() {
		return newPrdLoan;
	}

	public void setNewPrdLoan(String newPrdLoan) {
		this.newPrdLoan = newPrdLoan;
	}

	public String getRepaySour() {
		return repaySour;
	}

	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour;
	}

	public String getIsAuthorize() {
		return isAuthorize;
	}

	public void setIsAuthorize(String isAuthorize) {
		this.isAuthorize = isAuthorize;
	}

	public String getAuthedName() {
		return authedName;
	}

	public void setAuthedName(String authedName) {
		this.authedName = authedName;
	}

	public String getConsignorCertType() {
		return consignorCertType;
	}

	public void setConsignorCertType(String consignorCertType) {
		this.consignorCertType = consignorCertType;
	}

	public String getConsignorCertCode() {
		return consignorCertCode;
	}

	public void setConsignorCertCode(String consignorCertCode) {
		this.consignorCertCode = consignorCertCode;
	}

	public String getAuthedTelNo() {
		return authedTelNo;
	}

	public void setAuthedTelNo(String authedTelNo) {
		this.authedTelNo = authedTelNo;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	public String getChnlSour() {
		return chnlSour;
	}

	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour;
	}

	public String getBillDispupeOpt() {
		return billDispupeOpt;
	}

	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt;
	}

	public String getCourtAddr() {
		return courtAddr;
	}

	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr;
	}

	public String getArbitrateBch() {
		return arbitrateBch;
	}

	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch;
	}

	public String getArbitrateAddr() {
		return arbitrateAddr;
	}

	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr;
	}

	public String getOtherOpt() {
		return otherOpt;
	}

	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt;
	}

	public BigDecimal getContQnt() {
		return contQnt;
	}

	public void setContQnt(BigDecimal contQnt) {
		this.contQnt = contQnt;
	}

	public String getPundContNo() {
		return pundContNo;
	}

	public void setPundContNo(String pundContNo) {
		this.pundContNo = pundContNo;
	}

	public String getSpplClause() {
		return spplClause;
	}

	public void setSpplClause(String spplClause) {
		this.spplClause = spplClause;
	}

	public String getSignAddr() {
		return signAddr;
	}

	public void setSignAddr(String signAddr) {
		this.signAddr = signAddr;
	}

	public String getBusiNetwork() {
		return busiNetwork;
	}

	public void setBusiNetwork(String busiNetwork) {
		this.busiNetwork = busiNetwork;
	}

	public String getMainBusiPalce() {
		return mainBusiPalce;
	}

	public void setMainBusiPalce(String mainBusiPalce) {
		this.mainBusiPalce = mainBusiPalce;
	}

	public String getContTemplate() {
		return contTemplate;
	}

	public void setContTemplate(String contTemplate) {
		this.contTemplate = contTemplate;
	}

	public BigDecimal getContPrintNum() {
		return contPrintNum;
	}

	public void setContPrintNum(BigDecimal contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getSignApproveStatus() {
		return signApproveStatus;
	}

	public void setSignApproveStatus(String signApproveStatus) {
		this.signApproveStatus = signApproveStatus;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getOtherAgreed() {
		return otherAgreed;
	}

	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}

	public String getAcctsvcrName() {
		return acctsvcrName;
	}

	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}

	public String getLoanPayoutAccno() {
		return loanPayoutAccno;
	}

	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}

	public String getLoanPayoutAccName() {
		return loanPayoutAccName;
	}

	public void setLoanPayoutAccName(String loanPayoutAccName) {
		this.loanPayoutAccName = loanPayoutAccName;
	}

	public String getContMode() {
		return contMode;
	}

	public void setContMode(String contMode) {
		this.contMode = contMode;
	}

	public String getIsOnlineDraw() {
		return isOnlineDraw;
	}

	public void setIsOnlineDraw(String isOnlineDraw) {
		this.isOnlineDraw = isOnlineDraw;
	}

	public String getCtrBeginFlag() {
		return ctrBeginFlag;
	}

	public void setCtrBeginFlag(String ctrBeginFlag) {
		this.ctrBeginFlag = ctrBeginFlag;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public BigDecimal getHighAvlAmt() {
		return highAvlAmt;
	}

	public void setHighAvlAmt(BigDecimal highAvlAmt) {
		this.highAvlAmt = highAvlAmt;
	}

	public String getIsRenew() {
		return isRenew;
	}

	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}

	public String getOrigiContNo() {
		return origiContNo;
	}

	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}

	public String getIsUtilLmt() {
		return isUtilLmt;
	}

	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	public String getIsOlPld() {
		return isOlPld;
	}

	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}

	public String getBeforehandInd() {
		return beforehandInd;
	}

	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}

	public String getIsESeal() {
		return isESeal;
	}

	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}

	public String getLoanRateAdjDay() {
		return loanRateAdjDay;
	}

	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay;
	}

	public String getIsSeajnt() {
		return isSeajnt;
	}

	public void setIsSeajnt(String isSeajnt) {
		this.isSeajnt = isSeajnt;
	}
}