package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtSigInvestAppService;
import cn.com.yusys.yusp.service.LmtSigInvestApprService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 资金业务授信申报审批流程（总行发起-债券池）（总行发起-债券池）申请流程
 * 作者：李召星
 */
@Service
public class TYSX06BizService implements ClientBizInterface {

    private final Logger logger = LoggerFactory.getLogger(TYSX06BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService ;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();

        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();

        logger.info("后业务处理类型{}", currentOpType);
        LmtSigInvestApp lmtSigInvestApp = null;
        try {
            lmtSigInvestApp = lmtSigInvestAppService.selectBySerno(serno);

            //加载路由条件 add by zhangjw 20210721
            Map<String,Object> varParam = investPut2VarParam(resultInstanceDto,serno);
            logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，路由条件加载-----：【{}】", serno,varParam);

            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
                //判断当前节点是否出具审查报告、授信批复节点
                String issueReportType = "";
                //获取下一审批节点信息
                String nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
                if(resultInstanceDto.getNextNodeInfos()!=null && resultInstanceDto.getNextNodeInfos().size()>0){
                    nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
                }
                //判断下一审批节点是否包含出具审查报告、出具批复页面
                if(CmisBizConstants.TYSX06_01.contains(nextNodeId+",")){
                    issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_01;
                }else if(CmisBizConstants.TYSX06_03.contains(nextNodeId+",")){
                    issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_03;
                }

                //如果当前节点是发起节点，则从申请表copy数据到审批表；如果当前节点非发起节点，则copy审批表中最新的数据至审批表
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                if(CmisBizConstants.TYSX06_START.equals(currNodeId)){
                    //TODO 从申请表生成对应的审批表信息
                    lmtSigInvestAppService.handleAfterStart(lmtSigInvestApp,issueReportType, varParam);
                }else{
                    //TODO 如果当前节点非发起节点，则copy审批表中最新的数据至审批表-根据create_time字段倒序取最新，将 issueReportType 出具报告类型一并带入
                    String cur_next_id = currNodeId+";"+nextNodeId;
                    lmtSigInvestApprService.generateSigInvestApprService(lmtSigInvestApp,issueReportType, cur_next_id, currentUserId, currentOrgId);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                lmtSigInvestAppService.handleAfterEnd(lmtSigInvestApp,CmisBizConstants.APPLY_STATE_PASS,resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedSigInvestApp(resultInstanceDto, serno, currentUserId, currentOrgId, lmtSigInvestApp);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedSigInvestApp(resultInstanceDto, serno, currentUserId, currentOrgId, lmtSigInvestApp);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                lmtSigInvestAppService.update(lmtSigInvestApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                lmtSigInvestAppService.update(lmtSigInvestApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("资金业务授信申报审批流程（总行发起-债券池）申请启用【{}】，流程否决操作，流程参数【{}】", serno, resultInstanceDto);
                lmtSigInvestAppService.handleAfterEnd(lmtSigInvestApp,CmisBizConstants.APPLY_STATE_REFUSE,resultInstanceDto);
            } else {
                logger.warn("资金业务授信申报审批流程（总行发起-债券池）申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("资金业务授信申报审批流程（总行发起-债券池）申请审批后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.TYSX06.equals(flowCode);
    }

    /**
     * @作者:lizx
     * @方法名称: extractedSigInvestApp
     * @方法描述:  投行业务授信审批打回或退回操作处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 15:27
     * @param resultInstanceDto:
     * @param grtSerno:
     * @param currentUserId:
     * @param currentOrgId:
     * @param lmtSigInvestApp:
     * @return: void
     * @算法描述: 无
     */
    private void extractedSigInvestApp(ResultInstanceDto resultInstanceDto, String grtSerno, String currentUserId, String currentOrgId, LmtSigInvestApp lmtSigInvestApp) throws Exception {
        logger.info("资金业务授信申报审批流程（总行发起-债券池）【{}】，流程打回或退回操作", grtSerno, resultInstanceDto);
        //审批打回至客户经理处，将审批表数据copy至申请表
//        List<String> nextNodeUserIds = lmtSigInvestAppService.getNextNodeUserIds(resultInstanceDto);
//        if (nextNodeUserIds!=null && nextNodeUserIds.contains(lmtSigInvestApp.getManagerId())){
//            lmtSigInvestAppService.handleAfterCallBack(lmtSigInvestApp, currentUserId, currentOrgId,CmisBizConstants.APPLY_STATE_CALL_BACK);
//        }else{
        //针对流程到办结节点，进行以下处理
        lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
        lmtSigInvestAppService.update(lmtSigInvestApp);
//        }
        //推送首页提醒事项 add by lizx 20210701 流程审批退回，推送
        lmtSigInvestAppService.sendWbMsgNotice(lmtSigInvestApp,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId(),"退回");
    }

    /**
     * @方法名称: investPut2VarParam
     * @方法描述: 重置流程参数-资金业务
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: zhangjw 20210719
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> investPut2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtSigInvestAppService.getRouterMapResult(resultInstanceDto,serno);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        return params;
    }
}
