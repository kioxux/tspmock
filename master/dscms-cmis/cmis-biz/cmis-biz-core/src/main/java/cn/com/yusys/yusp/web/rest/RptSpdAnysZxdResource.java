/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.RptSpdAnysYsd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysZxd;
import cn.com.yusys.yusp.service.RptSpdAnysZxdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysZxdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-25 10:31:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyszxd")
public class RptSpdAnysZxdResource {
    @Autowired
    private RptSpdAnysZxdService rptSpdAnysZxdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysZxd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysZxd> list = rptSpdAnysZxdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysZxd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysZxd>> index(QueryModel queryModel) {
        List<RptSpdAnysZxd> list = rptSpdAnysZxdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysZxd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysZxd> show(@PathVariable("serno") String serno) {
        RptSpdAnysZxd rptSpdAnysZxd = rptSpdAnysZxdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysZxd>(rptSpdAnysZxd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysZxd> create(@RequestBody RptSpdAnysZxd rptSpdAnysZxd) throws URISyntaxException {
        rptSpdAnysZxdService.insert(rptSpdAnysZxd);
        return new ResultDto<RptSpdAnysZxd>(rptSpdAnysZxd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysZxd rptSpdAnysZxd) throws URISyntaxException {
        int result = rptSpdAnysZxdService.update(rptSpdAnysZxd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysZxdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysZxdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysZxd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysZxd rptSpdAnysZxd = rptSpdAnysZxdService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysZxd);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysZxd> save(@RequestBody RptSpdAnysZxd rptSpdAnysZxd) throws URISyntaxException {
        RptSpdAnysZxd rptSpdAnysZxd1 = rptSpdAnysZxdService.selectByPrimaryKey(rptSpdAnysZxd.getSerno());
        if(rptSpdAnysZxd1!=null){
            rptSpdAnysZxdService.update(rptSpdAnysZxd);
        }else{
            rptSpdAnysZxdService.insert(rptSpdAnysZxd);
        }
        return ResultDto.success(rptSpdAnysZxd);
    }
}
