package cn.com.yusys.yusp.service.server.xdtz0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.req.Xdtz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.resp.Xdtz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.web.server.xdtz0018.BizXdtz0018Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

/**
 * 接口处理类:额度占用释放接口
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0018Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0018Service.class);

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private SequenceTemplateService sequenceTemplateService;

    /**
     * 额度占用释放
     * @param xdtz0018DataReqDto
     * @return
     */
    public Xdtz0018DataRespDto xdtz0018(Xdtz0018DataReqDto xdtz0018DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataReqDto));
        Xdtz0018DataRespDto xdtz0018DataRespDto = new Xdtz0018DataRespDto();
        try {
            String now = LocalDate.now().toString();
            CmisLmt0034ReqDto cmisLmt0034ReqDto = new CmisLmt0034ReqDto();
            cmisLmt0034ReqDto.setSerno(sequenceTemplateService.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>()));
            cmisLmt0034ReqDto.setSysNo(EsbEnum.SERVTP_PJP.key);//系统编号
            cmisLmt0034ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
            cmisLmt0034ReqDto.setInputBrId(xdtz0018DataReqDto.getInputBrId());
            cmisLmt0034ReqDto.setInputId(xdtz0018DataReqDto.getInputId());
            cmisLmt0034ReqDto.setInputDate(now);
            CmisLmt0034DealBizListReqDto cmisLmt0034DealBizListReqDto = new CmisLmt0034DealBizListReqDto();
            CmisLmt0034OccRelListReqDto cmisLmt0034OccRelListReqDto = new CmisLmt0034OccRelListReqDto();

            cmisLmt0034DealBizListReqDto.setDealBizNo(xdtz0018DataReqDto.getDrftNo());//台账编号
            cmisLmt0034DealBizListReqDto.setIsFollowBiz("0");//是否无缝衔接 1-是 0-否
            cmisLmt0034DealBizListReqDto.setPrdNo(xdtz0018DataReqDto.getPrdId());
            cmisLmt0034DealBizListReqDto.setPrdName("贴现业务");
            cmisLmt0034DealBizListReqDto.setDealBizAmtCny(xdtz0018DataReqDto.getRepasteTotlAmt());//台账总额
            cmisLmt0034DealBizListReqDto.setDealBizSpacAmtCny(xdtz0018DataReqDto.getRepasteTotlAmt());//台账敞口
            cmisLmt0034DealBizListReqDto.setDealBizStatus("100");//台账状态
            cmisLmt0034DealBizListReqDto.setStartDate(StringUtils.isBlank(xdtz0018DataReqDto.getStartDate()) ? now : xdtz0018DataReqDto.getStartDate());
            cmisLmt0034DealBizListReqDto.setEndDate(StringUtils.isBlank(xdtz0018DataReqDto.getEndDate()) ? now : xdtz0018DataReqDto.getEndDate());
            cmisLmt0034ReqDto.setDealBizList(Arrays.asList(cmisLmt0034DealBizListReqDto));

            cmisLmt0034OccRelListReqDto.setLmtCusId(xdtz0018DataReqDto.getAorgNo());
            cmisLmt0034OccRelListReqDto.setDealBizNo(xdtz0018DataReqDto.getDrftNo());
            cmisLmt0034OccRelListReqDto.setBizSpacAmtCny(xdtz0018DataReqDto.getRepasteTotlAmt());

            cmisLmt0034ReqDto.setOccRelList(Arrays.asList(cmisLmt0034OccRelListReqDto));
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(cmisLmt0034ReqDto));
            ResultDto<CmisLmt0034RespDto> resultDto = cmisLmtClientService.cmislmt0034(cmisLmt0034ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(resultDto));
            if (Objects.nonNull(resultDto)) {
                if (Objects.equals(resultDto.getCode(), SuccessEnum.SUCCESS.key)) {
                    xdtz0018DataRespDto.setOpFlag(SuccessEnum.SUCCESS.key);
                    xdtz0018DataRespDto.setOpMsg(SuccessEnum.SUCCESS.value);
                } else {
                    xdtz0018DataRespDto.setOpFlag(EpbEnum.EPB099999.key);
                    xdtz0018DataRespDto.setOpMsg(Objects.isNull(resultDto.getData()) ? "额度占用失败" : resultDto.getData().getErrorMsg());
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, e.getMessage());
            xdtz0018DataRespDto.setOpFlag(EpbEnum.EPB099999.key);
            xdtz0018DataRespDto.setOpMsg("系统异常");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataRespDto));
        return xdtz0018DataRespDto;
    }
}
