package cn.com.yusys.yusp.service.server.xdht0044;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdht0044.req.Xdht0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0044.resp.Xdht0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 业务逻辑类:根据核心客户号查询我行信用类合同金额汇总
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdht0044Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0044Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * 交易码：xdht0044
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0044DataReqDto
     * @return
     */
    @Transactional
    public Xdht0044DataRespDto xdht0044(Xdht0044DataReqDto xdht0044DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, JSON.toJSONString(xdht0044DataReqDto));
        Xdht0044DataRespDto xdht0044DataRespDto = new Xdht0044DataRespDto();

        try {
            // 从xdht0044DataReqDto获取业务值进行业务逻辑处理
            String certNo = xdht0044DataReqDto.getCertNo();//证件号码
            String queryType = xdht0044DataReqDto.getQueryType();//查询类型
            String isExcludeSettl = xdht0044DataReqDto.getIsExcludeSettl();
            Map queryMap = new HashMap<>();
            int loanHouseAmt = 0;
            int normalLoanHouseAmt = 0;
            String isHouseCus = DscmsBizHtEnum.ISHOUSECUS_N.key;
            //通过客户证件号查询客户信息
            logger.info("****************根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certNo)).orElse(new CusBaseClientDto());
            logger.info("****************根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusBaseClientDto));
            // 客户编号
            String cusId = cusBaseClientDto.getCusId();
            if (StringUtils.isBlank(cusId) && StringUtils.isBlank(certNo)) {
                xdht0044DataRespDto.setIsHouseCus(isHouseCus);
                return xdht0044DataRespDto;
            }
            //查询条件
            queryMap.put("cusId", cusId);//客户号
            queryMap.put("cert_code", certNo);//证件号码
            if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_LOANHOUSEFROMCMIS.key, queryType) || Objects.equals("N", isExcludeSettl)) {
                logger.info("根据证件号码查询客户是否是房群客户开始,查询参数为:{}", JSON.toJSONString(queryMap));
                loanHouseAmt = ctrLoanContMapper.queryLoanHouseFromCmis(queryMap);
                logger.info("根据证件号码查询客户是否是房群客户结束,返回结果为:{}", JSON.toJSONString(loanHouseAmt));
                if (!Objects.equals(loanHouseAmt, 0)) {
                    isHouseCus = DscmsBizHtEnum.ISHOUSECUS_Y.key;
                }
            } else if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_NORMALLOANHOUSEFROMCMIS.key, queryType) || Objects.equals("Y", isExcludeSettl)) {
                logger.info("根据证件号码查询客户是否是房群客户(排除已结清)开始,查询参数为:{}", JSON.toJSONString(queryMap));
                normalLoanHouseAmt = ctrLoanContMapper.queryNormalLoanHouseFromCmis(queryMap);
                logger.info("根据证件号码查询客户是否是房群客户(排除已结清)结束,返回结果为:{}", JSON.toJSONString(normalLoanHouseAmt));
                if (!Objects.equals(normalLoanHouseAmt, 0)) {
                    isHouseCus = DscmsBizHtEnum.ISHOUSECUS_Y.key;
                }
            }
            xdht0044DataRespDto.setIsHouseCus(isHouseCus);// 是否是房群客户字典项 Y-是, N-否
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, JSON.toJSONString(xdht0044DataRespDto));
        return xdht0044DataRespDto;
    }
}
