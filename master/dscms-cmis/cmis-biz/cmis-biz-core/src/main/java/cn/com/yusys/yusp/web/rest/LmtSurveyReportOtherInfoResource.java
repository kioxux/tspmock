/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyReportOtherInfo;
import cn.com.yusys.yusp.service.LmtSurveyReportOtherInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportOtherInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-25 19:48:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "调查报告其他信息")
@RequestMapping("/api/lmtsurveyreportotherinfo")
public class LmtSurveyReportOtherInfoResource {
    @Autowired
    private LmtSurveyReportOtherInfoService lmtSurveyReportOtherInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyReportOtherInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyReportOtherInfo> list = lmtSurveyReportOtherInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyReportOtherInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyReportOtherInfo>> index(QueryModel queryModel) {
        List<LmtSurveyReportOtherInfo> list = lmtSurveyReportOtherInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyReportOtherInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{surveySerno}")
    protected ResultDto<LmtSurveyReportOtherInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo = lmtSurveyReportOtherInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyReportOtherInfo>(lmtSurveyReportOtherInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyReportOtherInfo> create(@RequestBody LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo) throws URISyntaxException {
        lmtSurveyReportOtherInfoService.insert(lmtSurveyReportOtherInfo);
        return new ResultDto<LmtSurveyReportOtherInfo>(lmtSurveyReportOtherInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo) throws URISyntaxException {
        int result = lmtSurveyReportOtherInfoService.update(lmtSurveyReportOtherInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtSurveyReportOtherInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyReportOtherInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
    * @author zlf
    * @date 2021/6/7 17:01
    * @version 1.0.0
    * @desc    单查
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/selectone")
    protected ResultDto<LmtSurveyReportOtherInfo> showOne(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo = lmtSurveyReportOtherInfoService.selectByPrimaryKey(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtSurveyReportOtherInfo>(lmtSurveyReportOtherInfo);
    }
}
