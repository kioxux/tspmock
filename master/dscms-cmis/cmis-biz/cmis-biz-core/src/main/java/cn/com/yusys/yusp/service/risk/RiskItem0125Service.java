package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RiskItem0125Service {

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private IqpContExtService iqpContExtService;

    @Autowired
    private IqpGuarChgAppService iqpGuarChgAppService;

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    @Autowired
    private IqpDelayPaymentService iqpDelayPaymentService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    public RiskResultDto riskItem0125(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String cusId = (String)queryModel.getCondition().get("bizUserId");
        if (StringUtils.isBlank(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10003);
            return riskResultDto;
        }
        QueryModel model = new QueryModel();
        model.getCondition().put("cusId",cusId);
        model.getCondition().put("approveStatus", CmisCommonConstants.WF_STATUS_111);
        List<Map> listContApp = ctrLoanContService.selectOtherThingsAppLoan(model);
        if (listContApp != null && listContApp.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途合同申请");
            return riskResultDto;
        }
        List<IqpContExt> listIqpContExt = iqpContExtService.selectByModel(model);
        if (listIqpContExt != null && listIqpContExt.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途展期申请");
            return riskResultDto;
        }
        List<IqpGuarChgApp> listIqpGuarChgApp = iqpGuarChgAppService.selectByModel(model);
        if (listIqpGuarChgApp != null && listIqpGuarChgApp.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途担保变更申请");
            return riskResultDto;
        }
        List<IqpRepayWayChg> listIqpRepayWayChg = iqpRepayWayChgService.selectByModel(model);
        if (listIqpRepayWayChg != null && listIqpRepayWayChg.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途还款计划变更申请");
            return riskResultDto;
        }
        List<IqpDelayPayment> listIqpDelayPayment = iqpDelayPaymentService.selectByModel(model);
        if (listIqpDelayPayment != null && listIqpDelayPayment.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途延期还款申请");
            return riskResultDto;
        }
        List<PvpLoanApp> listPvpLoanApp = pvpLoanAppService.selectByModel(model);
        if (listPvpLoanApp != null && listPvpLoanApp.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途贷款出账申请");
            return riskResultDto;
        }
        List<PvpAccpApp> listPvpAccpApp = pvpAccpAppService.selectByModel(model);
        if (listPvpAccpApp != null && listPvpAccpApp.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途银承出账申请");
            return riskResultDto;
        }
        List<PvpEntrustLoanApp> listPvpEntrustLoanApp = pvpEntrustLoanAppService.selectByModel(model);
        if (listPvpEntrustLoanApp != null && listPvpEntrustLoanApp.size()>0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途委托贷款出账申请");
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
