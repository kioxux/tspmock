/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.domain.LmtSigInvestSubApp;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLimitAppMapper;
import cn.com.yusys.yusp.service.LmtSigInvestSubAppService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.service.LmtSigInvestBasicInfoSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:18:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasicinfosub")
public class LmtSigInvestBasicInfoSubResource {
    @Autowired
    private LmtSigInvestBasicInfoSubService lmtSigInvestBasicInfoSubService;

    @Autowired
    private LmtSigInvestAppMapper lmtSigInvestAppMapper;

    @Autowired
    private LmtSigInvestBasicLimitAppMapper lmtSigInvestBasicLimitAppMapper;

    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService ;
    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicInfoSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicInfoSub> list = lmtSigInvestBasicInfoSubService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicInfoSub>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicInfoSub>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicInfoSub> list = lmtSigInvestBasicInfoSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicInfoSub>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestBasicInfoSub>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestBasicInfoSub> list = lmtSigInvestBasicInfoSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicInfoSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicInfoSub> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub = lmtSigInvestBasicInfoSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicInfoSub>(lmtSigInvestBasicInfoSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicInfoSub> create(@RequestBody LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub) throws URISyntaxException {
        lmtSigInvestBasicInfoSubService.insert(lmtSigInvestBasicInfoSub);
        return new ResultDto<LmtSigInvestBasicInfoSub>(lmtSigInvestBasicInfoSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertBasicInfoSub")
    protected ResultDto<LmtSigInvestBasicInfoSub> insertBasicInfoSub(@RequestBody LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub) throws URISyntaxException {
        lmtSigInvestBasicInfoSubService.insertBasicInfoSub(lmtSigInvestBasicInfoSub);
        return new ResultDto<LmtSigInvestBasicInfoSub>(lmtSigInvestBasicInfoSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub) throws URISyntaxException {
        int result = lmtSigInvestBasicInfoSubService.update(lmtSigInvestBasicInfoSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/synchronizelmtamt")
    protected ResultDto<Integer> synchronizeLmtAmt(@RequestBody LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub) throws URISyntaxException {
        int result = 0;
        result = lmtSigInvestBasicInfoSubService.update(lmtSigInvestBasicInfoSub);
        QueryModel model = new QueryModel();
        model.addCondition("serno",lmtSigInvestBasicInfoSub.getSerno());
        model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppMapper.selectByModel(model);
        BigDecimal a = lmtSigInvestBasicInfoSub.getBasicAssetBalanceAmt().multiply(lmtSigInvestApps.get(0).getLmtAmt());
        LmtSigInvestSubApp lmtSigInvestSubApp = (LmtSigInvestSubApp) lmtSigInvestSubAppService.selectByModel(model).get(0);
        BigDecimal lmtAmt = a.divide(NumberUtils.nullDefaultZero(lmtSigInvestSubApp.getPrdTotalAmt()),2,BigDecimal.ROUND_HALF_UP);
        QueryModel model1 = new QueryModel();
        model1.addCondition("basicSerno",lmtSigInvestBasicInfoSub.getBasicSerno());
        model1.addCondition("basicCusId",lmtSigInvestBasicInfoSub.getBasicCusId());
        model1.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestBasicLimitApp> BasicLimitApps = lmtSigInvestBasicLimitAppMapper.selectByModel(model1);
        if (CollectionUtils.isNotEmpty(BasicLimitApps)) {
            LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp = BasicLimitApps.get(0);
            lmtSigInvestBasicLimitApp.setLmtAmt(lmtAmt);
            result = lmtSigInvestBasicLimitAppMapper.updateByPrimaryKey(lmtSigInvestBasicLimitApp);
        }
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicInfoSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicInfoSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);

    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody QueryModel queryModel ) {
        String pkId = (String) queryModel.getCondition().get("pkId");
        int result = lmtSigInvestBasicInfoSubService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }
}
