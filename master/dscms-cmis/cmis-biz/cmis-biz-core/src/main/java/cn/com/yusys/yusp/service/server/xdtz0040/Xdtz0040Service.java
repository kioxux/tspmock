package cn.com.yusys.yusp.service.server.xdtz0040;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.dto.server.xdtz0040.resp.Xdtz0040DataRespDto;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 接口处理类:申请人在本行当前逾期贷款数量
 *
 * @author zhugenrong
 * @version 1.0
 */
@Service
public class Xdtz0040Service {

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 申请人在本行当前逾期贷款数量
     * @param queryMap
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0040DataRespDto getContNumByCertCont(Map queryMap) {
        return accLoanMapper.getContNumByCertCont(queryMap);
    }
}
