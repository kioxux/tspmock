package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtSxkdPlusFksp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.LmtSxkdPlusFkspMapper;
import cn.com.yusys.yusp.service.LmtAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RiskItem0106Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0106Service.class);

    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtSxkdPlusFkspMapper lmtSxkdPlusFkspMapper;

    /**
     * @方法名称: riskItem0106
     * @方法描述: 省心快贷自动化审批风控返回审批结果校验
     * @参数与返回说明:
     * @算法描述:
     * 省心快贷自动化审批时，判断风控返回的自动化审批结果。
     * (1).若审批结果为空则进行拦截
     * @创建人: zhangliang15
     * @创建时间: 2021-08-27 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0106(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("省心快贷自动化审批风控返回审批结果校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();

        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 如果不是提交自动化审批且省心快贷自动化审批风险拦截通过，不校验
        if (!Objects.equals("1", lmtApp.getIsSubAutoAppr()) && !Objects.equals("1", lmtApp.getSxkdRiskResult())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_010602);
            return riskResultDto;
        }

        // 获取风控返回结果 pass:通过 1 reject:否决 0
        LmtSxkdPlusFksp lmtSxkdPlusFksp = lmtSxkdPlusFkspMapper.selectBySerno(lmtApp.getSerno());
        // 如果风控返回结果主表或返回结果字段为空,则拦截
        if (Objects.isNull(lmtSxkdPlusFksp) || StringUtils.isBlank(lmtSxkdPlusFksp.getApproveResult())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09905);
            return riskResultDto;
        }

        // 如果风控返回结果为reject:否决 0 ，则提示语为：省心快贷自动化审批未通过，提交人工审批。风险拦截通过，走人工审批流程。
        if (Objects.equals("reject", lmtSxkdPlusFksp.getApproveResult())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_010601);
            return riskResultDto;
        }

        log.info("省心快贷自动化审批风控返回审批结果校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}