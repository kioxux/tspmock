package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import cn.com.yusys.yusp.domain.GuarWarrantInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantManageApp
 * @类描述: guar_warrant_manage_app数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:24:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarWarrantManageAppDto implements Serializable{
	//private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 抵质押分类 **/
	private String grtFlag;
	
	/** 权证编号 **/
	private String warrantNo;
	
	/** 权证入库模式 **/
	private String warrantInType;
	
	/** 权证出入库申请类型 **/
	private String warrantAppType;
	
	/** 权证出库原因大类 **/
	private String warrantOutType;
	
	/** 权证出库原因细类 **/
	private String warrantOutTypeSub;
	
	/** 是否出库到集中作业 **/
	private String isZhblzx;
	
	/** 权证预计归还时间 **/
	private String preBackDate;
	
	/** 备注 **/
	private String remark;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 核心抵质押品出入库返回交易流水 **/
	private String hxSerno;
	
	/** 核心抵质押品出入库返回交易日期 **/
	private String hxDate;
	
	/** 核心担保编号 **/
	private String coreGuarantyNo;
	
	/** 核心担保品序号 **/
	private String coreGuarantySeq;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;

	/** 权证台账信息 **/
	private GuarWarrantInfo guarWarrantInfo;

	/** 担保合同编号 **/
	private String guarContNo;

	private String inputIdName;

	private String inputBrIdName;

	private String managerBrIdName;

	private String managerIdName;

	private String gagTyp;

	/** 是否还款即解押 **/
	private String isRepayRemoveGuar;

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}

	/**
	 * @return GuarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	public GuarWarrantInfo getGuarWarrantInfo() {
		return guarWarrantInfo;
	}

	public void setGuarWarrantInfo(GuarWarrantInfo guarWarrantInfo) {
		this.guarWarrantInfo = guarWarrantInfo;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag == null ? null : grtFlag.trim();
	}
	
    /**
     * @return GrtFlag
     */	
	public String getGrtFlag() {
		return this.grtFlag;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo == null ? null : warrantNo.trim();
	}
	
    /**
     * @return WarrantNo
     */	
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param warrantInType
	 */
	public void setWarrantInType(String warrantInType) {
		this.warrantInType = warrantInType == null ? null : warrantInType.trim();
	}
	
    /**
     * @return WarrantInType
     */	
	public String getWarrantInType() {
		return this.warrantInType;
	}
	
	/**
	 * @param warrantAppType
	 */
	public void setWarrantAppType(String warrantAppType) {
		this.warrantAppType = warrantAppType == null ? null : warrantAppType.trim();
	}
	
    /**
     * @return WarrantAppType
     */	
	public String getWarrantAppType() {
		return this.warrantAppType;
	}
	
	/**
	 * @param warrantOutType
	 */
	public void setWarrantOutType(String warrantOutType) {
		this.warrantOutType = warrantOutType == null ? null : warrantOutType.trim();
	}
	
    /**
     * @return WarrantOutType
     */	
	public String getWarrantOutType() {
		return this.warrantOutType;
	}
	
	/**
	 * @param warrantOutTypeSub
	 */
	public void setWarrantOutTypeSub(String warrantOutTypeSub) {
		this.warrantOutTypeSub = warrantOutTypeSub == null ? null : warrantOutTypeSub.trim();
	}
	
    /**
     * @return WarrantOutTypeSub
     */	
	public String getWarrantOutTypeSub() {
		return this.warrantOutTypeSub;
	}
	
	/**
	 * @param isZhblzx
	 */
	public void setIsZhblzx(String isZhblzx) {
		this.isZhblzx = isZhblzx == null ? null : isZhblzx.trim();
	}
	
    /**
     * @return IsZhblzx
     */	
	public String getIsZhblzx() {
		return this.isZhblzx;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate == null ? null : preBackDate.trim();
	}
	
    /**
     * @return PreBackDate
     */	
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param hxSerno
	 */
	public void setHxSerno(String hxSerno) {
		this.hxSerno = hxSerno == null ? null : hxSerno.trim();
	}
	
    /**
     * @return HxSerno
     */	
	public String getHxSerno() {
		return this.hxSerno;
	}
	
	/**
	 * @param hxDate
	 */
	public void setHxDate(String hxDate) {
		this.hxDate = hxDate == null ? null : hxDate.trim();
	}
	
    /**
     * @return HxDate
     */	
	public String getHxDate() {
		return this.hxDate;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo == null ? null : coreGuarantyNo.trim();
	}
	
    /**
     * @return CoreGuarantyNo
     */	
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param coreGuarantySeq
	 */
	public void setCoreGuarantySeq(String coreGuarantySeq) {
		this.coreGuarantySeq = coreGuarantySeq == null ? null : coreGuarantySeq.trim();
	}
	
    /**
     * @return CoreGuarantySeq
     */	
	public String getCoreGuarantySeq() {
		return this.coreGuarantySeq;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * 担保合同与押品及权证关系列表
	 */
	private List<GuarContRelWarrant> guarContRelWarrantList;

	public List<GuarContRelWarrant> getGuarContRelWarrantList() {
		return guarContRelWarrantList;
	}

	public String getInputIdName() {
		return inputIdName;
	}

	public void setInputIdName(String inputIdName) {
		this.inputIdName = inputIdName;
	}

	public String getInputBrIdName() {
		return inputBrIdName;
	}

	public void setInputBrIdName(String inputBrIdName) {
		this.inputBrIdName = inputBrIdName;
	}

	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	public String getManagerIdName() {
		return managerIdName;
	}

	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}

	/**
	 * @param gagTyp
	 */
	public void setGagTyp(String gagTyp) {
		this.gagTyp = gagTyp == null ? null : gagTyp.trim();
	}

	/**
	 * @return GagTyp
	 */
	public String getGagTyp() {
		return this.gagTyp;
	}
	
	/**
	 * @param isRepayRemoveGuar
	 */
	public void setIsRepayRemoveGuar(String isRepayRemoveGuar) {
		this.isRepayRemoveGuar = isRepayRemoveGuar == null ? null : isRepayRemoveGuar.trim();
	}
	
    /**
     * @return IsRepayRemoveGuar
     */	
	public String getIsRepayRemoveGuar() {
		return this.isRepayRemoveGuar;
	}


}