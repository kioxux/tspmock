/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtVerifInfo;
import cn.com.yusys.yusp.service.LmtVerifInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtVerifInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-15 22:24:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "核验信息")
@RequestMapping("/api/lmtverifinfo")
public class LmtVerifInfoResource {
    @Autowired
    private LmtVerifInfoService lmtVerifInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtVerifInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtVerifInfo> list = lmtVerifInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtVerifInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtVerifInfo>> index(QueryModel queryModel) {
        List<LmtVerifInfo> list = lmtVerifInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtVerifInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{surveySerno}")
    protected ResultDto<LmtVerifInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtVerifInfo lmtVerifInfo = lmtVerifInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtVerifInfo>(lmtVerifInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtVerifInfo> create(@RequestBody LmtVerifInfo lmtVerifInfo) throws URISyntaxException {
        lmtVerifInfoService.insert(lmtVerifInfo);
        return new ResultDto<LmtVerifInfo>(lmtVerifInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtVerifInfo lmtVerifInfo) throws URISyntaxException {
        int result = lmtVerifInfoService.update(lmtVerifInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtVerifInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtVerifInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

   /**
    * @创建人 WH
    * @创建时间 2021/6/18 19:27
    * @注释 查询单条数据 优企贷
    */
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<LmtVerifInfo> selectbysurveyserno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtVerifInfo lmtVerifInfo = lmtVerifInfoService.selectByPrimaryKey(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtVerifInfo>(lmtVerifInfo);
    }

}
