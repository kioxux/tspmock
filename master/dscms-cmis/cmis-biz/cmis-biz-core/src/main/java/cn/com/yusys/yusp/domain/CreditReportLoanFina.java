/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportLoanFina
 * @类描述: credit_report_loan_fina数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-07 16:49:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_report_loan_fina")
public class CreditReportLoanFina extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CRLF_SERNO")
	private String crlfSerno;
	
	/** 法定代表人姓名 **/
	@Column(name = "LEGAL_NAME", unique = false, nullable = true, length = 80)
	private String legalName;
	
	/** 法定代表人联系电话 **/
	@Column(name = "LEGAL_PHONE", unique = false, nullable = true, length = 20)
	private String legalPhone;
	
	/** 他行是否有融资 **/
	@Column(name = "OTHER_BANK_IS_FIN", unique = false, nullable = true, length = 5)
	private String otherBankIsFin;
	
	/** 本行是否已有融资 **/
	@Column(name = "OUR_BANK_IS_FIN", unique = false, nullable = true, length = 5)
	private String ourBankIsFin;
	
	/** 本次融资申请金额 **/
	@Column(name = "APPLY_FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal applyFinAmt;
	
	/** 本次融资期限（月） **/
	@Column(name = "APPLY_FIN_TERM", unique = false, nullable = true, length = 10)
	private String applyFinTerm;
	
	/** 本次融资担保方式 **/
	@Column(name = "APPLY_FIN_GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String applyFinGuarType;
	
	/** 本次融资原因 **/
	@Column(name = "APPLY_FIN_REMARK", unique = false, nullable = true, length = 5)
	private String applyFinRemark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param crlfSerno
	 */
	public void setCrlfSerno(String crlfSerno) {
		this.crlfSerno = crlfSerno;
	}
	
    /**
     * @return crlfSerno
     */
	public String getCrlfSerno() {
		return this.crlfSerno;
	}
	
	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	
    /**
     * @return legalName
     */
	public String getLegalName() {
		return this.legalName;
	}
	
	/**
	 * @param legalPhone
	 */
	public void setLegalPhone(String legalPhone) {
		this.legalPhone = legalPhone;
	}
	
    /**
     * @return legalPhone
     */
	public String getLegalPhone() {
		return this.legalPhone;
	}
	
	/**
	 * @param otherBankIsFin
	 */
	public void setOtherBankIsFin(String otherBankIsFin) {
		this.otherBankIsFin = otherBankIsFin;
	}
	
    /**
     * @return otherBankIsFin
     */
	public String getOtherBankIsFin() {
		return this.otherBankIsFin;
	}
	
	/**
	 * @param ourBankIsFin
	 */
	public void setOurBankIsFin(String ourBankIsFin) {
		this.ourBankIsFin = ourBankIsFin;
	}
	
    /**
     * @return ourBankIsFin
     */
	public String getOurBankIsFin() {
		return this.ourBankIsFin;
	}
	
	/**
	 * @param applyFinAmt
	 */
	public void setApplyFinAmt(java.math.BigDecimal applyFinAmt) {
		this.applyFinAmt = applyFinAmt;
	}
	
    /**
     * @return applyFinAmt
     */
	public java.math.BigDecimal getApplyFinAmt() {
		return this.applyFinAmt;
	}
	
	/**
	 * @param applyFinTerm
	 */
	public void setApplyFinTerm(String applyFinTerm) {
		this.applyFinTerm = applyFinTerm;
	}
	
    /**
     * @return applyFinTerm
     */
	public String getApplyFinTerm() {
		return this.applyFinTerm;
	}
	
	/**
	 * @param applyFinGuarType
	 */
	public void setApplyFinGuarType(String applyFinGuarType) {
		this.applyFinGuarType = applyFinGuarType;
	}
	
    /**
     * @return applyFinGuarType
     */
	public String getApplyFinGuarType() {
		return this.applyFinGuarType;
	}
	
	/**
	 * @param applyFinRemark
	 */
	public void setApplyFinRemark(String applyFinRemark) {
		this.applyFinRemark = applyFinRemark;
	}
	
    /**
     * @return applyFinRemark
     */
	public String getApplyFinRemark() {
		return this.applyFinRemark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}