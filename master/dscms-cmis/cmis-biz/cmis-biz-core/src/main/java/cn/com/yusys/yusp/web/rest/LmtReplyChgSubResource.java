/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyChgSub;
import cn.com.yusys.yusp.service.LmtReplyChgSubService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgSubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 10:22:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplychgsub")
public class LmtReplyChgSubResource {
    @Autowired
    private LmtReplyChgSubService lmtReplyChgSubService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyChgSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyChgSub> list = lmtReplyChgSubService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyChgSub>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyChgSub>> index(QueryModel queryModel) {
        List<LmtReplyChgSub> list = lmtReplyChgSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyChgSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyChgSub> show(@PathVariable("pkId") String pkId) {
        LmtReplyChgSub lmtReplyChgSub = lmtReplyChgSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyChgSub>(lmtReplyChgSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyChgSub> create(@RequestBody LmtReplyChgSub lmtReplyChgSub) throws URISyntaxException {
        lmtReplyChgSubService.insert(lmtReplyChgSub);
        return new ResultDto<LmtReplyChgSub>(lmtReplyChgSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyChgSub lmtReplyChgSub) throws URISyntaxException {
        int result = lmtReplyChgSubService.update(lmtReplyChgSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyChgSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyChgSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updatelmtreplychgsub
     * @函数描述:updateLmtReplyChgSub
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatelmtreplychgsub")
    protected ResultDto<Integer> updateLmtReplyChgSub(@RequestBody LmtReplyChgSub lmtReplyChgSub) {
        return ResultDto.success(lmtReplyChgSubService.updateLmtReplyChgSub(lmtReplyChgSub));
    }

}
