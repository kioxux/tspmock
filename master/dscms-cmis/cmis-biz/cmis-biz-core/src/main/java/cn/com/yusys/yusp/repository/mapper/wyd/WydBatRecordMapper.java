/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper.wyd;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.apache.ibatis.annotations.Param;
import cn.com.yusys.yusp.domain.WydBatRecord;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: WydBatRecordMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank-fjj
 * @创建时间: 2021-08-27 17:14:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface WydBatRecordMapper{

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    WydBatRecord selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(WydBatRecord record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(WydBatRecord record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(WydBatRecord record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: selectByBatDate
     * @方法描述: 根据批处理日期查询
     * @参数与返回说明:接口日期参数
     * @算法描述: 无
     */
    WydBatRecord selectByBatDate(@Param("batDate") String batDate);

    /**
     * 条件查询
     * @param model
     * @return
     */
    List<WydBatRecord> selectByCondition(QueryModel model);
}