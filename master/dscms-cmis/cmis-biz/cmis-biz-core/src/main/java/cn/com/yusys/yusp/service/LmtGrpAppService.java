/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.impl.RoleImpl;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.image.callimage.Para;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.LmtGrpAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.jcraft.jsch.UserInfo;
import io.netty.util.internal.ObjectUtil;
import org.apache.commons.lang.ObjectUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: LmtGrpAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xiamc
 * @创建时间: 2021-04-07 10:06:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpAppService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtGrpAppService.class);

    //集团授信批复台账表
    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpApprService lmtGrpApprService;

    // 单一客户授信批复
    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Resource
    private LmtGrpAppMapper lmtGrpAppMapper;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    // 流程自行退出服务
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    // 系统参数配置服务
    @Autowired
    private AdminSmPropService adminSmPropService;

    // 测算数据
    @Autowired
    private LmtHighCurfundEvalService lmtHighCurfundEvalService;

    @Autowired
    private LmtGrpSubPrdAdjustRelService lmtGrpSubPrdAdjustRelService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;
    @Autowired
    private LmtReplyService lmtReplyService;
    @Autowired
    private LmtApprService lmtApprService;
    @Autowired
    private LmtReplySubService lmtReplySubService;
    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;
    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;
    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;
    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;// 额度系统

    @Autowired
    private AdminSmUserService  adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private LmtSubPrdLowGuarRelService lmtSubPrdLowGuarRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MessageCommonService sendMessage;

    @Autowired
    private CommonService commonService;

    @Autowired
    private MessageSendService messageSendService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpApp selectByPrimaryKey(String pkId) {
        return lmtGrpAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */


    public List<LmtGrpApp> selectAll(QueryModel model) {
        List<LmtGrpApp> records = lmtGrpAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 -查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpApp> list = lmtGrpAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtGrpApp record) {
        record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        return lmtGrpAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpApp record) {
        return lmtGrpAppMapper.insertSelective(record);
    }


    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpApp record) {
        return lmtGrpAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称: update
     * @函数描述: 集团授信申请列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public int update(LmtGrpApp record) {
        return lmtGrpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @函数名称: updatelmtgrpappdicussdata
     * @函数描述: 集团授信申请再议数据申请向导保存接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-8 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public int updateLmtGrpAppDicussData(LmtGrpApp record) {
        // 再议时,同时更新成员客户审批状态信息
        if (CmisCommonConstants.LMT_TYPE_06.equals(record.getLmtType())) {
            HashMap hashMap = new HashMap();
            hashMap.put("serno", record.getGrpSerno());
            hashMap.put("lmtType", CmisCommonConstants.LMT_TYPE_06);
            hashMap.put("approveStatus", CmisCommonConstants.WF_STATUS_000);
            lmtAppService.updateLmtAppBySingleSerno(hashMap);
        }
        return lmtGrpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @函数名称: queryAllGrpAppListForPartMgr
     * @函数描述: 集团授信分开填报列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtGrpApp> queryAllGrpAppListForPartMgr(String managerId) {
        return lmtGrpAppMapper.queryAllGrpAppListForPartMgr(managerId);
    }

    /**
     * @函数名称: queryHisList
     * @函数描述: 集团授历史列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryHisList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 通过 否决 自行退出 再议
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_HIS_LIST.concat(",").concat("993"));
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryAppList
     * @函数描述: 集团授信申请列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryAppList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtTypeSets", CmisCommonConstants.LMT_TYPE_APP_LIST_STATUS);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_APP_LIST);
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryAppHisList
     * @函数描述: 集团授信申请历史列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryAppHisList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtTypeSets", CmisCommonConstants.LMT_TYPE_APP_LIST_STATUS);
        // 通过 否决 自行退出 再议
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_HIS_LIST.concat(",").concat("993"));
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryAppList
     * @函数描述: 集团授信变更列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryChgList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_02);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_APP_LIST);
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryChgHisList
     * @函数描述: 集团授信变更历史列表页面接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryChgHisList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_02);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryFinerList
     * @函数描述: 集团授信细化申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryFinerList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_07);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_APP_LIST);
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryFinerHisList
     * @函数描述: 集团授信细化申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryFinerHisList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_07);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return this.selectByModel(model);
    }


    /**
     * @函数名称: queryAdjustList
     * @函数描述: 集团授信调剂申请列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryAdjustList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_08);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_APP_LIST);
        return this.selectByModel(model);
    }

    /**
     * @函数名称: queryAdjustHisList
     * @函数描述: 集团授信调剂申请历史列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpApp> queryAdjustHisList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("lmtType", CmisCommonConstants.LMT_TYPE_08);
        model.getCondition().put("approveStatusSets", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return this.selectByModel(model);
    }

    /**
     * @方法名称: queryLmtGrpAppBySerno
     * @方法描述: 通过入参查询授信申请数据
     * @参数与返回说明:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpApp> selectLmtGrpAppByParams(HashMap<String, String> queryMap) {
        return lmtGrpAppMapper.selectLmtGrpAppByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtGrpAppBySerno
     * @方法描述: 通过流水号查询到集团授信申请数据
     * @参数与返回说明:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpApp queryLmtGrpAppByGrpSerno(String grpSerno) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("grpSerno", grpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpApp> lmtGrpAppList = this.selectLmtGrpAppByParams(queryMap);
        if (lmtGrpAppList != null && lmtGrpAppList.size() == 1) {
            return lmtGrpAppList.get(0);
        }
        return null;
    }

    /**
     * @方法名称: queryLmtGrpAppBySerno
     * @方法描述: 通过流水号查询到集团授信申请数据
     * @参数与返回说明:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpApp queryLmtGrpAppLmtType1ByGrpCusId(String grpCusId) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("grpCusId", grpCusId);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("lmtType", CmisCommonConstants.LMT_TYPE_01);
        List<LmtGrpApp> lmtGrpAppList = this.selectLmtGrpAppByParams(queryMap);
        if (lmtGrpAppList != null && lmtGrpAppList.size() >= 1) {
            return lmtGrpAppList.get(0);
        }
        return null;
    }

    /**
     * @函数名称: queryLmtGrpAppBySingleSerno
     * @函数描述: 根据单一客户申请流水号查询集团申请信息数据详情
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-20 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpApp queryLmtGrpAppBySingleSerno(String serno) {
        LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
        if (lmtGrpMemRel != null) {
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("grpSerno", lmtGrpMemRel.getGrpSerno());
            queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtGrpApp> lmtGrpAppList = this.selectLmtGrpAppByParams(queryMap);
            if (lmtGrpAppList != null && lmtGrpAppList.size() == 1) {
                return lmtGrpAppList.get(0);
            }
        }
        return null;
    }

    /**
     * @函数名称:logicdetele
     * @函数描述:集团申请数据逻辑删除接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Integer logicDelete(String grpSerno) {
        Integer result = 0;
        try {

            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                if (CmisCommonConstants.WF_STATUS_111.equals(lmtGrpMemRel.getManagerIdSubmitStatus())) {
                    throw BizException.error(null, EcbEnum.ECB010091.key, EcbEnum.ECB010091.value);
                }
            }

            LmtGrpApp lmtGrpApp = this.queryLmtGrpAppByGrpSerno(grpSerno);
            if (CmisCommonConstants.WF_STATUS_992.equals(lmtGrpApp.getApproveStatus())) {
                log.info("集团授信逻辑删除处理:" + grpSerno);
                lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                int exit = lmtGrpAppMapper.updateByPrimaryKeySelective(lmtGrpApp);
                ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(grpSerno);
                List<LmtApp> lmtApps = lmtAppService.getLmtAppByGrpSerno(grpSerno);
                for (LmtApp lmtApp : lmtApps) {
                    log.info("集团客户项下成员客户授信逻辑删除处理:" + lmtApp.getSerno());
                    lmtAppService.deleteByPkId(lmtApp);
                }
                if (exit == 1) {
                    return result;
                } else {
                    throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                }
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                lmtGrpApp.setUpdId(userInfo.getLoginCode());
                lmtGrpApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                result = this.update(lmtGrpApp);
                lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
                for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                    lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                    lmtGrpMemRelService.update(lmtGrpMemRel);
                    LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
                    if (lmtApp != null) {
                        lmtAppService.deleteByPkId(lmtApp);
                    }
                }
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("逻辑删除集团授信申请数据出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        } finally {

        }
        return result;
    }


    /**
     * @方法名称: guideSave
     * @方法描述: 集团授信申请向导页面保存
     * @参数与返回说明:
     * @算法描述: 1.根据不同的授信申请类型调用不同的方法
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String guideSave(LmtGrpApp lmtGrpApp) throws ParseException {

        Boolean judgeResult = judgeIsExistOnWayApp(lmtGrpApp.getGrpCusId());
        if (judgeResult) {
            throw BizException.error(null, EcbEnum.ECB010002.key, EcbEnum.ECB010002.value);
        }
        // 复议时,判断是否已复议成功三次
        if(lmtGrpApp.getLmtType().equals("05")){
            boolean isThreeFlg = isThreeFY(lmtGrpApp);
            if(!isThreeFlg){
                throw BizException.error(null, EcbEnum.ECB020000.key, EcbEnum.ECB020000.value);
            };
        }
        // 复审 规则判断
        if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)){
            boolean isRulesFlg = judgeOnReviewRules(lmtGrpApp);
            if (!isRulesFlg) {
                throw BizException.error(null, EcbEnum.ECB010098.key, EcbEnum.ECB010098.value);
            }
        }
        // 复议时 是否存在在途申请校验
        if (lmtGrpApp.getLmtType().equals("05") && String.valueOf(judgeResult).equals("true")) {
//            ResultDto resultDto = new ResultDto<String>();
//            resultDto.setCode(judgeResult.getCode());
//            resultDto.setMessage(judgeResult.getMessage());
//            resultDto.setData(judRes);
            return String.valueOf(judgeResult);
        }
        // 否决发起复议时
        if (lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_05) && CmisCommonConstants.WF_STATUS_998.equals(lmtGrpApp.getApproveStatus())) {
            return saveNewLmtAppUnderFYAndFJ(lmtGrpApp);
        }

        if (CmisCommonConstants.LMT_TYPE_01.equals(lmtGrpApp.getLmtType())) {
            return saveNewLmtAppForTypeNew(lmtGrpApp);
        } else {
            // 从批复台账中生成数据
            return saveNewLmtAppForTypeReNew(lmtGrpApp);
        }
    }

  /** @方法名称: judgeOnReviewRules
   *  @方法描述: 先判断当前授信期限是否大于一年;判断当前授信分项中是否存在循环授信的分项;
   *  @参数与返回说明: @算法描述: 无 */

    private boolean judgeOnReviewRules(LmtGrpApp lmtGrpApp) throws ParseException {
        // 1、先判断当前授信期限是否大于一年 手动发起去除
        // 2、判断集团项下成员的授信分项中是否存在循环授信的分项 （不包含低风险 -- add 20210829 马顺，周帅确认）
        // 3、不存在申请中的复审任务
        // 4、在有效期内
        boolean rulesFlg = false;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // 判断符合循环授信额度并且非低风险情况
        List<LmtAppSub> list = lmtAppSubService.queryLmtAppSubByGrpSerno(lmtGrpApp.getGrpSerno());
        if(!list.isEmpty() && list.size() > 0){
            for(LmtAppSub lmtAppSub : list){
                // 包含循环授信额度，并且不是低风险额度情况下，则需要复审
                if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit()) && !CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())){
                    log.info("授信项下存在当前分项符合循环授信额度并且非低风险情况,可发起复审:"+JSON.toJSONString(lmtAppSub));
                    rulesFlg = true;
                    break;
                }
            }
        }
        // 判断是否生效
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(lmtGrpApp.getGrpCusId());
        if(Objects.nonNull(lmtGrpReplyAcc)){
            // 获取营业日期
            Date openday = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
            Date inureDate = simpleDateFormat.parse(lmtGrpReplyAcc.getReplyInureDate());
            Date endDate = DateUtils.addMonth(inureDate,lmtGrpReplyAcc.getLmtTerm());
            if(endDate.before(openday)){
                log.info("当前日期已超过集团客户生效的台账生效日期!");
                rulesFlg = false;
            }
        }else{
            // 最新的生效的批复台账信息
            log.info("获取当前集团客户名下最新的生效的批复台账信息失败!");
            rulesFlg = false;
        }
        return rulesFlg;
    }

    /**
     * @方法名称: generateLmtGrpAppByLmtGrpReplyAcc
     * @方法描述: 通过集团授信台账生成一笔新的申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtGrpApp generateLmtGrpAppByLmtGrpReplyAcc(LmtGrpReplyAcc lmtGrpReplyAcc, String lmtType, User userInfo, String oldGrpSerno) throws Exception {
        // 判断当前集团是否解散
        boolean isGrpExistFlg = isGrpExist(lmtGrpReplyAcc.getGrpCusId());
        if(!isGrpExistFlg){
            throw BizException.error(null, EcbEnum.ECB010083.key, EcbEnum.ECB010083.value);
        }
        LmtGrpApp lmtGrpApp = new LmtGrpApp();
        BeanUtils.copyProperties(lmtGrpReplyAcc, lmtGrpApp);
        // 1.保存新增的集团授信申请数据
        String grpSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
        // 生成流水号异常 如空的情况
        if (StringUtils.isEmpty(grpSerno)) {
            throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
        }
        log.info(String.format("保存集团授信申请数据,生成流水号%s", grpSerno));
        lmtGrpApp.setPkId(UUID.randomUUID().toString());
        // 申请流水号
        lmtGrpApp.setGrpSerno(grpSerno);
        //原授信流水号
        lmtGrpApp.setOrigiLmtSerno(lmtGrpReplyAcc.getGrpSerno());
        // 数据操作标志为新增
        lmtGrpApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        // 流程状态
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        // 原批复编号
        lmtGrpApp.setOrigiLmtReplySerno(lmtGrpReplyAcc.getGrpReplySerno());
        // 原期限
        lmtGrpApp.setOrigiLmtTerm(lmtGrpReplyAcc.getLmtTerm());
        // 授信类型
        lmtGrpApp.setLmtType(lmtType);
        if (!CmisCommonConstants.LMT_TYPE_01.equals(lmtType) && !CmisCommonConstants.LMT_TYPE_06.equals(lmtType)) {
            // 除却新增和 再议的情况  凡是需要生成新数据的操作都要执行该操作 将需要获取上条数据的敞口.低风险
            LmtGrpReply oldLmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpSerno(oldGrpSerno);
            if (oldLmtGrpReply != null) {
                lmtGrpApp.setLowRiskTotalLmtAmt(oldLmtGrpReply.getLowRiskTotalLmtAmt());
                lmtGrpApp.setOpenTotalLmtAmt(oldLmtGrpReply.getOpenTotalLmtAmt());
            }
        }

        log.info(String.format("保存集团申请数据%s-获取当前登录用户数据", grpSerno));
        if(userInfo != null){
            lmtGrpApp.setInputId(userInfo.getLoginCode());
            lmtGrpApp.setInputBrId(userInfo.getOrg().getCode());
            lmtGrpApp.setUpdId(userInfo.getLoginCode());
            lmtGrpApp.setUpdBrId(userInfo.getOrg().getCode());
            lmtGrpApp.setManagerId(userInfo.getLoginCode());
            lmtGrpApp.setManagerBrId(userInfo.getOrg().getCode());
        }else{
            lmtGrpApp.setInputId(lmtGrpReplyAcc.getManagerId());
            lmtGrpApp.setInputBrId(lmtGrpReplyAcc.getManagerBrId());
            lmtGrpApp.setUpdId(lmtGrpReplyAcc.getManagerId());
            lmtGrpApp.setUpdBrId(lmtGrpReplyAcc.getManagerBrId());
            lmtGrpApp.setManagerId(lmtGrpReplyAcc.getManagerId());
            lmtGrpApp.setManagerBrId(lmtGrpReplyAcc.getManagerBrId());
        }

        lmtGrpApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtGrpApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(lmtGrpApp);

        // 根据当前申请流水号  复制一条新的信息测算信息数据
        log.info(String.format("根据新流水号 复制一条新的测算数据开始%s", grpSerno));
        LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectBySerno(oldGrpSerno);
        if (lmtHighCurfundEval != null) {
            lmtHighCurfundEval.setPkId(UUID.randomUUID().toString());
            lmtHighCurfundEval.setSerno(grpSerno);
            // 登录信息
            lmtHighCurfundEval.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            if(userInfo != null){
                lmtHighCurfundEval.setInputId(userInfo.getLoginCode());
                lmtHighCurfundEval.setInputBrId(lmtGrpReplyAcc.getManagerBrId());
                lmtHighCurfundEval.setUpdId(userInfo.getLoginCode());
                lmtHighCurfundEval.setUpdBrId(userInfo.getOrg().getCode());
            }else{
                lmtHighCurfundEval.setInputId(lmtGrpReplyAcc.getManagerId());
                lmtHighCurfundEval.setInputBrId(lmtGrpReplyAcc.getManagerBrId());
                lmtHighCurfundEval.setUpdId(lmtGrpReplyAcc.getManagerId());
                lmtHighCurfundEval.setUpdBrId(lmtGrpReplyAcc.getManagerBrId());
            }
            lmtHighCurfundEval.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtHighCurfundEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtHighCurfundEval.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtHighCurfundEval.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtHighCurfundEvalService.insertSelective(lmtHighCurfundEval);
        }
        log.info(String.format("根据新流水号 复制一条新的测算数据结束%s", grpSerno));
        // 复制授信申报填写的调剂数据
        log.info(String.format("根据新流水号 复制新的调剂数据开始%s", grpSerno));
        List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRelList = lmtGrpSubPrdAdjustRelService.queryLmtGrpSubPrdAdjustRelByGrpSerno(oldGrpSerno);
        for (LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel : lmtGrpSubPrdAdjustRelList) {
            lmtGrpSubPrdAdjustRel.setPkId(UUID.randomUUID().toString());
            lmtGrpSubPrdAdjustRel.setGrpSerno(grpSerno);
            lmtGrpSubPrdAdjustRelService.insert(lmtGrpSubPrdAdjustRel);
        }
        log.info(String.format("根据新流水号 复制新的调剂数据结束%s", grpSerno));

        return lmtGrpApp;
    }

    /*
    * 判断当前集团是否已解散
    * */
    private boolean isGrpExist(String grpCusId) {
        boolean result = true;
        CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
        cusGrpMemberRelDto.setGrpNo(grpCusId);
        List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = JSONArray.parseArray(JSON.toJSONString(iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData()), CusGrpMemberRelDto.class);
        if (cusGrpMemberRelDtoList == null || cusGrpMemberRelDtoList.size() == 0) {
            result = false;
        }
        return result;
    }

    /**
     * @方法名称: saveNewLmtAppForTypeReNew
     * @方法描述: 集团授信从集团授信台账中生成数据
     * @参数与返回说明:
     * @算法描述: 1.查询授信批复台账的数据
     * 2.根据授信台账生成授信申请数据
     * 3.根据现有的成员关系 对比原成员关系，
     * 如果是新增成员，则查看是否原授信台账是否存在，如果存在，则生成批复引入生成，如果不存在原批复，则只新增关系
     * 如果是剔除成员，则不引用关系
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public String saveNewLmtAppForTypeReNew(LmtGrpApp lmtGrpApp) {
        String grpSerno = "";
        try {
            User userInfo = SessionUtils.getUserInformation();
            // 再议校验 校验当前客户是否存在正在生效的台账
            if(CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpApp.getLmtType())){
                boolean isEffectAcc = judgeIsEffectAcc(lmtGrpApp);
                if (isEffectAcc) {
                    throw BizException.error(null, EcbEnum.ECB010093.key, EcbEnum.ECB010093.value);
                }
            }
            LmtGrpReplyAcc lmtGrpReplyAcc = new LmtGrpReplyAcc();
            if(CmisCommonConstants.LMT_TYPE_03.equals(lmtGrpApp.getLmtType())){
                // 根据集团客户号获取最新的一笔授信台账
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("grpCusId",lmtGrpApp.getGrpCusId());
                queryModel.setSort("inputDate desc");
                lmtGrpReplyAcc = CollectionUtils.nonEmpty(lmtGrpReplyAccService.queryByGrpCusId(queryModel)) ? lmtGrpReplyAccService.queryByGrpCusId(queryModel).get(0) : null;
                log.info("续作时.查询当前客户对应的最新的台账信息[{}],不带入台账状态字段",JSON.toJSONString(lmtGrpReplyAcc));
            }else{
                lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(lmtGrpApp.getGrpCusId());
            }
            LmtGrpApp newLmtGrpApp = generateLmtGrpAppByLmtGrpReplyAcc(lmtGrpReplyAcc, lmtGrpApp.getLmtType(), userInfo, lmtGrpReplyAcc.getGrpSerno());
            grpSerno = newLmtGrpApp.getGrpSerno();

            CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
            cusGrpMemberRelDto.setGrpNo(lmtGrpApp.getGrpCusId());
            List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = JSONArray.parseArray(JSON.toJSONString(iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData()), CusGrpMemberRelDto.class);
            log.info("根据集团编号【{}】查询最新的集团客户信息【{}】",lmtGrpApp.getGrpCusId(),cusGrpMemberRelDtoList);
            if(CollectionUtils.nonEmpty(cusGrpMemberRelDtoList) && !cusGrpMemberRelDtoList.get(0).getGrpName().equals(lmtGrpApp.getGrpCusName())){
                newLmtGrpApp.setGrpCusName(cusGrpMemberRelDtoList.get(0).getGrpName());
                this.update(newLmtGrpApp);
            }

            String resStr = lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpAppByLmtGrpReplyAcc(userInfo, newLmtGrpApp, lmtGrpReplyAcc);
            if (resStr.equals(EcbEnum.ECB010000.key)) {
                // 不做处理
            } else {
                //  事务回滚操作
                this.deleteDataByGrpSerno(grpSerno);
                throw new Exception(resStr);
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("保存集团授信申请数据出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return grpSerno;
    }

    private boolean judgeIsEffectAcc(LmtGrpApp lmtGrpApp) {
        QueryModel model = new QueryModel();
        model.addCondition("grpCusId",lmtGrpApp.getGrpCusId());
        model.addCondition("accStatus",CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        log.info("查询当前集团客户是否存在生效集团授信台账,集团客户号:"+lmtGrpApp.getGrpCusId());
        List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccService.queryByGrpCusId(model);
        if(!lmtGrpReplyAccList.isEmpty() && lmtGrpReplyAccList.size()>0){
            return true;
        }
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList){
            log.info("查询当前集团客户项下的成员客户是否存在生效成员授信台账");
            boolean isEffectAcc = lmtAppService.judgeIsEffectAcc(lmtGrpMemRel.getCusId());
            if(isEffectAcc){
                return true;
            }
        }
        return false;
    }

    /**
     * @方法名称: saveNewLmtAppForTypeReNew
     * @方法描述: 否决复议情况处理
     * @参数与返回说明:
     * @算法描述: 1.将本条数据复制为一条新的集团授信申请信息A
     * 2.根据本条数据查询集团授信关系表信息,并复制为新的数据B、C,关联A
     * 3.根据本条数据查询成员授信信息,复制为新的数据B、C，关联集团A
     * 4.终止原先的集团批复和成员客户批复
     * @创建人: css
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public String saveNewLmtAppUnderFYAndFJ(LmtGrpApp lmtGrpApp) {
        String grpSerno = "";
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            //校验当前集团是否已解散
            boolean isGrpExistFlg = isGrpExist(lmtGrpApp.getGrpCusId());
            if(!isGrpExistFlg){
                throw BizException.error(null, EcbEnum.ECB010083.key, EcbEnum.ECB010083.value);
            }
            LmtGrpApp newLmtGrpApp = new LmtGrpApp();
            HashMap map = new HashMap();
            map.put("grpSerno", lmtGrpApp.getGrpSerno());
            map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("获取当前流水号对应的授信申请,流水号为:" + lmtGrpApp.getGrpSerno());
            List<LmtGrpApp> lmtGrpApps = selectLmtGrpAppByParams(map);
            if (!lmtGrpApps.isEmpty() && lmtGrpApps.size() > 0) {
                newLmtGrpApp = lmtGrpApps.get(0);
            } else {
                throw BizException.error(null, EcbEnum.ECB010069.key, EcbEnum.ECB010069.value);
            }
            log.info("----------------------------生成新的的授信申请开始--------------------------");
            grpSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(grpSerno)) {
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("生成集团授信申请数据,生成流水号%s", grpSerno));
            log.info("根据集团编号【{}】查询最新的集团客户信息！",lmtGrpApp.getGrpCusId());
            CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
            cusGrpMemberRelDto.setGrpNo(lmtGrpApp.getGrpCusId());
            List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = JSONArray.parseArray(JSON.toJSONString(iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData()), CusGrpMemberRelDto.class);
            log.info("根据集团编号【{}】查询最新的集团客户信息【{}】",lmtGrpApp.getGrpCusId(),cusGrpMemberRelDtoList);
            if(CollectionUtils.nonEmpty(cusGrpMemberRelDtoList)  && !cusGrpMemberRelDtoList.get(0).getGrpName().equals(lmtGrpApp.getGrpCusName())){
                newLmtGrpApp.setGrpCusName(cusGrpMemberRelDtoList.get(0).getGrpName());
            }
            newLmtGrpApp.setPkId(UUID.randomUUID().toString());
            // 申请流水号
            newLmtGrpApp.setGrpSerno(grpSerno);
            // 数据操作标志为新增
            newLmtGrpApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            newLmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            // 授信类型
            newLmtGrpApp.setLmtType(CmisCommonConstants.LMT_TYPE_05);
            //原授信流水号
            newLmtGrpApp.setOrigiLmtSerno(StringUtils.isBlank(lmtGrpApp.getOrigiLmtSerno())?lmtGrpApp.getGrpSerno():lmtGrpApp.getOrigiLmtSerno());
            // 原授信期限
            newLmtGrpApp.setOrigiLmtTerm(lmtGrpApp.getLmtTerm());
            // 登录信息
            newLmtGrpApp.setInputId(userInfo.getLoginCode());
            newLmtGrpApp.setInputBrId(userInfo.getOrg().getCode());
            newLmtGrpApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            newLmtGrpApp.setUpdId(userInfo.getLoginCode());
            newLmtGrpApp.setUpdBrId(userInfo.getOrg().getCode());
            newLmtGrpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            newLmtGrpApp.setManagerId(userInfo.getLoginCode());
            newLmtGrpApp.setManagerBrId(userInfo.getOrg().getCode());
            newLmtGrpApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            newLmtGrpApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info(String.format("获取集团客户原授信申请批复信息,查询流水号%s", lmtGrpApp.getGrpSerno()));
            LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpSerno(lmtGrpApp.getGrpSerno());
            if(lmtGrpReply != null){
                newLmtGrpApp.setOrigiLmtReplySerno(lmtGrpReply.getGrpReplySerno());
                this.insert(newLmtGrpApp);
                log.info("----------------------------生成新的的授信申请结束--------------------------");
                log.info("----------------------------集团授信批复终止操作--------------------------,流水号:" + lmtGrpApp.getGrpSerno());
                lmtGrpReply.setReplyStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_02);
                lmtGrpReplyService.updateSelective(lmtGrpReply);
            }else{
                this.insert(newLmtGrpApp);
                log.info("----------------------------生成新的的授信申请结束--------------------------");
                log.info("----------------------------集团原批复数据不存在,原批复编号为空--------------------------,流水号:" + lmtGrpApp.getGrpSerno());
            }
            log.info("----------------------------生成新的的授信申请关系表数据开始--------------------------");
            List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.selectLmtGrpReplyByGrpSerno(lmtGrpApp.getGrpSerno());
            if (!lmtGrpMemRels.isEmpty() && lmtGrpMemRels.size() > 0) {
                log.info("----------------------------成员客户授信申请数据处理开始--------------------------");
                for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                    LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
                    String singleSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
                    lmtApp.setPkId(UUID.randomUUID().toString());
                    lmtApp.setOgrigiLmtSerno(lmtApp.getSerno());
                    lmtApp.setSerno(singleSerno);
                    lmtApp.setLmtType(CmisCommonConstants.LMT_TYPE_05);
                    // lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    // 登录信息
                    lmtApp.setInputId(userInfo.getLoginCode());
                    lmtApp.setInputBrId(userInfo.getOrg().getCode());
                    lmtApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtApp.setUpdId(userInfo.getLoginCode());
                    lmtApp.setUpdBrId(userInfo.getOrg().getCode());
                    lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtApp.setManagerId(userInfo.getLoginCode());
                    lmtApp.setManagerBrId(userInfo.getOrg().getCode());
                    lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    log.info("--生成新的成员客户授信申请------流水号:" + lmtApp.getSerno());
                    for(CusGrpMemberRelDto cusGrpMemberRelDto1 : cusGrpMemberRelDtoList){
                        if(cusGrpMemberRelDto1.getCusId().equals(lmtApp.getCusId())){
                            log.info("成员客户名称取最新的客户表数据！"+JSON.toJSONString(cusGrpMemberRelDto1));
                            lmtApp.setCusName(cusGrpMemberRelDto1.getCusName());
                            lmtGrpMemRel.setCusName(cusGrpMemberRelDto1.getCusName());
                            lmtGrpMemRel.setGrpCusName(cusGrpMemberRelDto1.getGrpName());
                        }
                    }
                    lmtAppService.insert(lmtApp);
                    //必输页签添加
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(CmisCommonConstants.LMT_TYPE_05);
                    String cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxsbjbxx,khxejzxpj,zydkxx,ldzjcsb,fysqxx";
                        pageList = "授信申报基本信息,客户限额及债项评级,专业贷款信息,流动资金测算表,复议申请信息";
                        log.info("--客户类型判断:公司客户------" + cusId);
                    } else {
                        idList = "sxsbjbxx,fysqxx";
                        pageList = "授信申报基本信息,复议申请信息";
                        log.info("--客户类型判断:单一客户------" + cusId);
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    log.info("--生成成员客户必输页签数据------流水号:" + lmtApp.getSerno());
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                    // 成员客户分项及明细信息
                    log.info("--成员客户授信申请原授信分项及分项明细处理------流水号:" + lmtApp.getSerno());
                    List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(lmtGrpMemRel.getSingleSerno());
                    if (!lmtAppSubs.isEmpty() && lmtAppSubs.size() > 0) {
                        String seqSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                        for (LmtAppSub lmtAppSub : lmtAppSubs) {
                            lmtAppSub.setCusName(lmtGrpMemRel.getCusName());
                            log.info("--成员客户授信申请原授信分项明细处理------分项流水号:" + lmtAppSub.getSubSerno());
                            List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                            if(lmtAppSubPrds.isEmpty() || lmtAppSubPrds.size() == 0){
                                continue;
                            }
                            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                                lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                                lmtAppSubPrd.setSubPrdSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>()));
                                lmtAppSubPrd.setSubSerno(seqSerno);
                                lmtAppSubPrd.setCusName(lmtGrpMemRel.getCusName());
                                // d登录信息
                                lmtAppSubPrd.setInputId(userInfo.getLoginCode());
                                lmtAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                                lmtAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                                lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
                                lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                                lmtAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                                lmtAppSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                log.info("--成员客户授信申请原授信分项明细处理------分项明细流水号:" + lmtAppSubPrd.getSubPrdSerno());
                                lmtAppSubPrdService.insert(lmtAppSubPrd);
                            }
                            lmtAppSub.setPkId(UUID.randomUUID().toString());
                            lmtAppSub.setSerno(singleSerno);
                            lmtAppSub.setSubSerno(seqSerno);
                            // d登录信息
                            lmtAppSub.setInputId(userInfo.getLoginCode());
                            lmtAppSub.setInputBrId(userInfo.getOrg().getCode());
                            lmtAppSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtAppSub.setUpdId(userInfo.getLoginCode());
                            lmtAppSub.setUpdBrId(userInfo.getOrg().getCode());
                            lmtAppSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtAppSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            lmtAppSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            log.info("--成员客户授信申请原授信分项处理------分项流水号:" + lmtAppSub.getSubSerno());
                            lmtAppSubService.insert(lmtAppSub);
                        }
                    }

                    // 成员客户批复信息
                    log.info("--成员客户授信申请批复信息终止操作------流水号:" + lmtGrpMemRel.getSingleSerno());
                    LmtReply lmtReplySingle = lmtReplyService.queryLmtReplyBySerno(lmtGrpMemRel.getSingleSerno());
                    if(lmtReplySingle != null ){
                        lmtReplySingle.setReplyStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_02);
                        lmtReplyService.updateSelective(lmtReplySingle);
                    }
                    log.info("-----生成新的成员客户授信申请------流水号:" + lmtApp.getSerno());
                    lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
//                    log.info("--判断是否预授信------流水号:"+lmtApp.getSerno());
//                    String isContainPreLmt = lmtGrpMemRelService.getOldLmtGrpAppIsContainPreLmt(lmtGrpMemRel.getSingleSerno());
//                    lmtGrpMemRel.setIsContainPreLmt(isContainPreLmt);
                    lmtGrpMemRel.setGrpSerno(grpSerno);
                    lmtGrpMemRel.setSingleSerno(lmtApp.getSerno());
                    // lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_000);
                    // d登录信息
                    lmtGrpMemRel.setInputId(userInfo.getLoginCode());
                    lmtGrpMemRel.setInputBrId(userInfo.getOrg().getCode());
                    lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                    lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                    lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtGrpMemRel.setManagerId(userInfo.getLoginCode());
                    lmtGrpMemRel.setManagerBrId(userInfo.getOrg().getCode());
                    lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtGrpMemRelService.insert(lmtGrpMemRel);
                }
                log.info("----------------------------成员客户授信申请数据处理结束--------------------------");

                // 根据当前申请流水号  复制一条新的信息测算信息数据
                log.info(String.format("根据新流水号复制一条新的测算数据%s", lmtGrpApp.getGrpSerno()));
                LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectBySerno(lmtGrpApp.getGrpSerno());
                if (lmtHighCurfundEval != null) {
                    lmtHighCurfundEval.setPkId(UUID.randomUUID().toString());
                    lmtHighCurfundEval.setSerno(grpSerno);
                    // 登录信息
                    lmtHighCurfundEval.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    lmtHighCurfundEval.setInputId(userInfo.getLoginCode());
                    lmtHighCurfundEval.setInputBrId(userInfo.getOrg().getCode());
                    lmtHighCurfundEval.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtHighCurfundEval.setUpdId(userInfo.getLoginCode());
                    lmtHighCurfundEval.setUpdBrId(userInfo.getOrg().getCode());
                    lmtHighCurfundEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtHighCurfundEval.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtHighCurfundEval.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtHighCurfundEvalService.insertSelective(lmtHighCurfundEval);
                }
            } else {
                throw BizException.error(null, EcbEnum.ECB010079.key, EcbEnum.ECB010079.value);
            }
            log.info("----------------------------生成新的的授信申请关系表数据结束--------------------------");
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("集团授信申请否决发起复议操作出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return grpSerno;
    }

    // 事务回滚操作
    public int deleteDataByGrpSerno(String grpSerno) {
        return lmtGrpAppMapper.deleteDataByGrpSerno(grpSerno);
    }

    /**
     * @方法名称: saveNewLmtAppForTypeNew
     * @方法描述: 集团授信新增的业务逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.保存新增的集团授信申请数据
     * 2.根据调用cmis_cus服务获取当前集团关联成员关系，插入集团业务申请成员关系表
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
        public String saveNewLmtAppForTypeNew(LmtGrpApp lmtGrpApp) {
        String grpSerno = "";
        try {

            // 1.保存新增的集团授信申请数据
            grpSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(grpSerno)) {
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("保存集团授信申请数据,生成流水号%s", grpSerno));

            lmtGrpApp.setPkId(UUID.randomUUID().toString());
            // 申请流水号
            lmtGrpApp.setGrpSerno(grpSerno);
            //原授信流水号
            lmtGrpApp.setOrigiLmtSerno(grpSerno);
            // 数据操作标志为新增
            lmtGrpApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            // 币种
            lmtGrpApp.setCurType(CmisCommonConstants.CUR_TYPE_CNY);

            log.info(String.format("保存集团申请数据%s-获取当前登录用户数据", grpSerno));
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                lmtGrpApp.setInputId(userInfo.getLoginCode());
                lmtGrpApp.setInputBrId(userInfo.getOrg().getCode());
                lmtGrpApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpApp.setUpdId(userInfo.getLoginCode());
                lmtGrpApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpApp.setManagerId(userInfo.getLoginCode());
                lmtGrpApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtGrpApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtGrpApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
            cusGrpMemberRelDto.setGrpNo(lmtGrpApp.getGrpCusId());
            List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData();
            log.info("根据集团编号【{}】查询最新的集团客户信息【{}】",lmtGrpApp.getGrpCusId(),cusGrpMemberRelDtoList);
            if(CollectionUtils.nonEmpty(cusGrpMemberRelDtoList)  && !cusGrpMemberRelDtoList.get(0).getGrpName().equals(lmtGrpApp.getGrpCusName())){
                lmtGrpApp.setGrpCusName(cusGrpMemberRelDtoList.get(0).getGrpName());
            }
            log.info(String.format("保存集团授信申请数据,流水号%s", grpSerno));
            int count = this.insert(lmtGrpApp);
            if (count != 1) {
                throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + ",保存失败！");
            }

            // 2.根据调用cmis_cus服务获取当前集团关联成员关系，插入集团业务申请成员关系表
            lmtGrpMemRelService.saveByQueryGrpMemRel(lmtGrpApp.getGrpCusId(), grpSerno); //传入的是集团编号
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("保存集团授信申请数据出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return grpSerno;
    }

    /**
     * @方法名称: getLmtTypeBySerno
     * @方法描述: 根据客户号查询从批复台账表中查询
     * @参数与返回说明:
     * @算法描述: 续作规则:
     * :帅
     * 1、授信到期后一年内（并且当前授信下用信余额为0）----- 续作
     * 2、授信到期后一年内（并且当前授信下用信余额不为0）------ 续作
     * 3、授信到期后一年之后并且当前授信下用信余额为0，------ 新增
     * 4、授信到期后一年之后并且当前授信下用信余额不为0，------ 续作
     * :雯
     * 1.授信到期后一年内，无需调用额度接口，发起的授信类型直接默认为授信续作
     * 2.授信到期后一年，调用额度接口，判断额度是否存在失效未结清的综合授信，若存则，则为授信续作；若不存在，则为授信新增
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String getLmtTypeByGrpNo(String grpCusId) {
        String lmtType = CmisCommonConstants.LMT_TYPE_01;
        try {
            if (StringUtils.isBlank(grpCusId)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团客户号%s,查询集团客户最新的授信批复台账信息", grpCusId));
            // 根据集团客户号获取最新的一笔授信台账
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("grpCusId",grpCusId);
            queryModel.setSort("inputDate desc");
            LmtGrpReplyAcc lastLmtGrpReplyAcc = CollectionUtils.nonEmpty(lmtGrpReplyAccService.queryByGrpCusId(queryModel)) ? lmtGrpReplyAccService.queryByGrpCusId(queryModel).get(0) : null;
            if (lastLmtGrpReplyAcc != null && lastLmtGrpReplyAcc.getPkId() != null) {
                    // 获取当前时间
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date nowDate = format.parse(stringRedisTemplate.opsForValue().get("openDay"));
                    // 授信期限+一年
                    int term = lastLmtGrpReplyAcc.getLmtTerm();
                    Date computeDate = format.parse(DateUtils.addMonth(lastLmtGrpReplyAcc.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue(), term));
                    // 如果台账生效日期加一年大于当前日期 则认为是授信续作
                    if (nowDate.compareTo(computeDate) <= 0) {
                        lmtType = CmisCommonConstants.LMT_TYPE_03;
                    } else if (nowDate.compareTo(computeDate) > -1 && nowDate.compareTo(DateUtils.addYear(computeDate, 1)) <= 0) {
                        lmtType = CmisCommonConstants.LMT_TYPE_03;
                    } else {
                        log.info("台账生效日期距当前时间超过一年,查询当前客户台账分项信息,台账编号:"+lastLmtGrpReplyAcc.getGrpAccNo());
                        // 调用额度接口，判断额度是否存在失效未结清的综合授信，若存则，则为授信续作；若不存在，则为授信新增
                        CmisLmt0047ReqDto cmisLmt0047ReqDto = new CmisLmt0047ReqDto();
                        cmisLmt0047ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_01);
                        List<CmisLmt0047LmtSubDtoList> lmtSubDtoList = new ArrayList<>();
                        List<LmtReplyAccSub> list = lmtReplyAccSubService.selectLmtReplyAccSubDataByGrpAccNo(lastLmtGrpReplyAcc.getGrpAccNo());
                        if(!list.isEmpty() && list.size() > 0){
                            for(LmtReplyAccSub lmtReplyAccSub : list){
                                log.info("发送额度系统校验分项下是否存在未结清的业务,分项编号:"+lmtReplyAccSub.getAccSubNo());
                                CmisLmt0047LmtSubDtoList cmisLmt0047LmtSubDtoList = new CmisLmt0047LmtSubDtoList();
                                cmisLmt0047LmtSubDtoList.setAccSubNo(lmtReplyAccSub.getAccSubNo());
                                lmtSubDtoList.add(cmisLmt0047LmtSubDtoList);
                            }
                            cmisLmt0047ReqDto.setLmtSubDtoList(lmtSubDtoList);
                            log.info("发送额度系统校验----------start-----------------,请求报文{}", JSON.toJSONString(cmisLmt0047ReqDto));
                            ResultDto<CmisLmt0047RespDto> cmisLmt0047RespDto = cmisLmtClientService.cmislmt0047(cmisLmt0047ReqDto);
                            log.info("--------------响应报文{}", JSON.toJSONString(cmisLmt0047RespDto));
                            if (cmisLmt0047RespDto != null && cmisLmt0047RespDto.getData() != null && "0000".equals(cmisLmt0047RespDto.getData().getErrorCode())) {
                                log.info("发送额度系统校验成功!");
                                List<CmisLmt0047ContRelDtoList> cmisLmt0047ContRelDtoLists = cmisLmt0047RespDto.getData().getContRelDtoList();
                                if(!cmisLmt0047ContRelDtoLists.isEmpty()&&cmisLmt0047ContRelDtoLists.size()>0){
                                    log.info("当前客户授信项下存在未结清的业务,执行续作操作!");
                                    lmtType = CmisCommonConstants.LMT_TYPE_03;
                                }else{
                                    log.info("当前客户授信项下不存在未结清的业务,还需校验当前客户是否存在生效的授信台账信息!");
                                    LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(grpCusId);
                                    if(lmtGrpReplyAcc != null && !"".equals(lmtGrpReplyAcc.getGrpAccNo())){
                                        log.info("当前客户存在生效的授信台账信息[{}],执行续作操作",JSON.toJSONString(lmtGrpReplyAcc));
                                        lmtType = CmisCommonConstants.LMT_TYPE_03;
                                    }else{
                                        log.info("当前客户不存在生效的授信台账信息[{}],,执行新增操作",JSON.toJSONString(lmtGrpReplyAcc));
                                        lmtType = CmisCommonConstants.LMT_TYPE_01;
                                    }
                                }
                            } else {
                                throw new Exception("同步额度系统异常");
                            }
                            log.info("发送额度系统校验----------end-----------------");
                        }else{
                            log.info("未查到当前台账项下分项信息,执行续作操作!");
                            lmtType = CmisCommonConstants.LMT_TYPE_03;
                        }
                    }
            } else {
                lmtType = CmisCommonConstants.LMT_TYPE_01;
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团客户号查询集团客户的授信台账表情况！失败", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return lmtType;
    }

    /**
     * @方法名称: judgeIsExistOnWayApp
     * @方法描述: 根据集团客户号查询集团客户是否存在在途其他的授信申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Boolean judgeIsExistOnWayApp(String grpCusId) {
        boolean isExist = false;
        try {
            if (StringUtils.isBlank(grpCusId)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团客户号%s,查询客户是否存在在途的授信申请", grpCusId));
            HashMap paramMap = new HashMap<String, Object>();
            paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramMap.put("grpCusId", grpCusId);
            paramMap.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            List<LmtGrpApp> lmtAppList = lmtGrpAppMapper.selectLmtGrpAppByParams(paramMap);
            if (lmtAppList.size() > 0) {
                isExist = true;
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团客户号查询客户是否存在在途的授信申请！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return isExist;
    }

    /**
     * @方法名称: queryRemindCountByManagerId
     * @方法描述: 通过客户经理号查询待处理的集团客户授信申报填报的数量
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer queryRemindCountByManagerId(String grpCusId) {
        Integer toDoAppCount = 0;
        try {
            if (StringUtils.isBlank(grpCusId)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团客户号%s,查询待处理的集团客户授信申报填报的数量", grpCusId));
            toDoAppCount = lmtGrpAppMapper.queryRemindCountByManagerId(grpCusId);

        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("通过客户经理号查询待处理的集团客户授信申报填报的数量异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return toDoAppCount;
    }


    /**
     * @方法名称: updateLmtAmtByGrpSerno
     * @方法描述: 通过集团授信申请号重新计算成员的金额汇总
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void updateLmtAmtByGrpSerno(String grpSerno) {
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        LmtGrpApp lmtGrpApp = this.queryLmtGrpAppByGrpSerno(grpSerno);
        BigDecimal openTotalLmtAmt = BigDecimal.ZERO;
        BigDecimal openLowRiskLmtAmt = BigDecimal.ZERO;
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            openTotalLmtAmt = openTotalLmtAmt.add(lmtGrpMemRel.getOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getOpenLmtAmt());
            openLowRiskLmtAmt = openLowRiskLmtAmt.add(lmtGrpMemRel.getLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getLowRiskLmtAmt());
        }
        lmtGrpApp.setOpenTotalLmtAmt(openTotalLmtAmt);
        lmtGrpApp.setLowRiskTotalLmtAmt(openLowRiskLmtAmt);
        this.update(lmtGrpApp);
        log.info(String.format("重新更新计算集团授信申请%s汇总金额成功", lmtGrpApp.toString()));
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 集团授信申请流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) throws Exception {
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
        String approveStatus = lmtGrpApp.getApproveStatus();
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        lmtGrpAppMapper.updateByPrimaryKey(lmtGrpApp);
        log.info("集团客户授信申报流程发起:" + lmtGrpApp.getGrpSerno());
        lmtGrpApprService.generateLmtGrpApprInfo(serno,approveStatus);

    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信申请流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决 2.生成否决批复 3.归档操作
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno, String flowCode) throws Exception {
        // 1.将审批状态更新为998
        log.info("修改集团授信申请审批状态为否决,流水号为:" + serno);
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        lmtGrpAppMapper.updateByPrimaryKey(lmtGrpApp);

        HashMap map = new HashMap();
        map.put("serno", serno);
        map.put("approveStatus", CmisCommonConstants.WF_STATUS_998);
        log.info("修改集团客户项下成员客户授信申请审批状态为否决,集团流水号为:" + serno);
        lmtAppService.updateLmtAppBySingleSerno(map);

        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_998);
            lmtGrpMemRelService.updateSelective(lmtGrpMemRel);
        }
        // 暂时注释流程以及接口相关
        // 2.从根据流水号获取最新的一笔授信审批记录中的数据
        log.info("根据流水号获取最新的一笔授信审批记录中的数据,流水号为:" + serno);
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryFinalLmtGrpApprBySerno(serno);
        if (lmtGrpAppr == null) {
            throw new Exception("查询审批中的授信结论异常");
        }
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new Exception("获取当前登录信息失败!");
        }
        // 3.根据最新的授信审批表中的数据生成批复数据
        log.info("根据最新的授信审批表中的数据生成批复数据,申请流水号为:" + serno);
        lmtGrpReplyService.generateLmtGrpReplyHandleByLmtGrpAppr(lmtGrpAppr, userInfo.getLoginCode(), userInfo.getOrg().getCode());

        log.info("根据申请流水号获取当前集团与成员客户关系,申请流水号为:" + serno);
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            // 4.修改成员客户授信申请审批状态为否决
            log.info("修改成员客户授信申请审批状态为否决,成员客户授信申请流水号为:" + lmtGrpMemRel.getSingleSerno());
            // 1).将审批状态更新为998
            LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
            lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            lmtAppService.updateSelective(lmtApp);
            // 2).从根据流水号获取最新的一笔授信审批记录中的数据
            log.info("查询成员客户最新的一条审批数据,成员客户授信申请流水号为:" + lmtGrpMemRel.getSingleSerno());
            LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(lmtGrpMemRel.getSingleSerno());
            if (lmtAppr == null) {
                throw new Exception("查询审批中的授信结论异常");
            }
            // 3).根据最新的授信审批表中的数据生成批复数据
            lmtReplyService.generateLmtReplyHandleByLmtAppr(lmtAppr, userInfo.getLoginCode(), userInfo.getOrg().getCode());
        }

        sendWbMsgNotice(lmtGrpApp, lmtGrpApp.getGrpCusName(), "集团授信申报", lmtGrpApp.getManagerId());
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信申请流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) throws Exception {
        // 1.将审批状态更新为992
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        lmtGrpAppMapper.updateByPrimaryKey(lmtGrpApp);

        //微信通知
        String managerId = lmtGrpApp.getManagerId();
        String mgrTel = "";
        if (StringUtil.isNotEmpty(managerId)) {
            log.info("调用AdminSmUserService用户信息查询服务开始*START***");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("调用AdminSmUserService用户信息查询服务结束*END******");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            try {
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", lmtGrpApp.getGrpCusName());
                if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_01)){
                    paramMap.put("prdName", "集团授信新增");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02)){
                    paramMap.put("prdName", "集团授信变更");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_03)){
                    paramMap.put("prdName", "集团授信续作");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)){
                    paramMap.put("prdName", "集团授信复审");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_05)){
                    paramMap.put("prdName", "集团授信复议");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_06)){
                    paramMap.put("prdName", "集团授信再议");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)){
                    paramMap.put("prdName", "集团预授信细化");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_08)){
                    paramMap.put("prdName", "集团授信额度调剂");
                }else{
                    log.info("当前授信申请[{}]类型未识别",JSON.toJSONString(lmtGrpApp));
                    paramMap.put("prdName", "集团授信申报");
                }
                paramMap.put("result", "退回");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
            } catch (Exception e) {
                throw new Exception("发送短信失败！");
            }
        }

    }

    /**
     * @方法名称: handleBusinessAfterReStart
     * @方法描述: 授信申请流程再议逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为99 否决
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterReStart(String serno) throws Exception {
        // 1.将审批状态更新为993
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_993);
        lmtGrpAppMapper.updateByPrimaryKey(lmtGrpApp);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信申请流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.从根据流水号获取最新的一笔授信审批记录中的数据
     * 3.根据最新的授信审批表中的数据生成批复数据
     * 4.根据不同的授信类型生成或者更新批复台账
     * 5.推送批复台账至额度系统
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno, String currentUserId, String currentOrgId, String flowCode) throws Exception {
        // 1.将审批状态更新为997 通过
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
        lmtGrpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.update(lmtGrpApp);
        log.info("更新集团授信申请" + serno + "的流程状态为997");
        // 将项下的成集团成员授信信息的审批状态都更新为997 通过 子流程自动处理
        // 暂时注释流程以及接口相关
        // 2.从根据流水号获取最新的一笔授信审批记录中的数据
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryFinalLmtGrpApprBySerno(serno);
        if (lmtGrpAppr == null) {
            throw new Exception("查询审批中的授信结论异常");
        }

        // 3.根据最新的授信审批表中的数据生成批复数据
        lmtGrpReplyService.generateLmtGrpReplyHandleByLmtGrpAppr(lmtGrpAppr, currentUserId, currentOrgId);

        // 4.根据申请流水号生成新的批复台账
        lmtGrpReplyAccService.generateLmtGrpReplyAccBySerno(serno);

        // 5 同步额度系统额度信息
        lmtGrpReplyAccService.synLmtReplyAccToLmtSys(lmtGrpApp.getGrpCusId());

        // 7.需要调用接口生成归档任务
        // 村镇银行不生成授信归档任务
        if(!Objects.equals(flowCode,CmisFlowConstants.SGCZ04) && !Objects.equals(flowCode,CmisFlowConstants.DHCZ04)){
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                // TODO 正常来说不应这么判断,需要根据关系表中是否参与本次申报的相关字段进行判断
                // 授信新增 如果不参与填报  不为集团成员客户生成审批数据 add 20210716 mashun
                if (CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpApp.getLmtType())) { // 变更
                    log.info("集团授信变更判断");
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpApp.getLmtType())) { // 预授信细化
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpApp.getLmtType())) { // 额度调剂
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                }
                if(StringUtil.isNotEmpty(lmtGrpMemRel.getSingleSerno())){
                    log.info("当前成员客户授信申请流水号{}", lmtGrpMemRel.getSingleSerno());
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
                    Map paramsSub = new HashMap();
                    paramsSub.put("serno", lmtGrpMemRel.getSingleSerno());
                    paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectLmtAppSubPrdByParams(paramsSub);
                    if(CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                        for(LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            // 判断是不是无还本续贷
                            if(Objects.equals("1",lmtAppSubPrd.getIsRwrop())) {
                                CusCorpDto cusCorpDto = icusClientService.queryCusCropDtoByCusId(lmtAppSubPrd.getCusId()).getData();
                                // 生成无还本续贷名单
                                CusLstWhbxdDto cusLstWhbxdDto = new CusLstWhbxdDto();
                                // 客户id
                                cusLstWhbxdDto.setCusId(lmtAppSubPrd.getCusId());
                                // 客户名称
                                cusLstWhbxdDto.setCusName(lmtAppSubPrd.getCusName());
                                // 客户规模
                                cusLstWhbxdDto.setCusScale(Objects.nonNull(cusBaseClientDto) ? cusCorpDto.getCorpScale() : "");
                                // 是否优良信贷客户
                                cusLstWhbxdDto.setIsFineCretCus("");
                                // 授信金额
                                cusLstWhbxdDto.setLmtAmt(lmtGrpMemRel.getOpenLmtAmt());
                                // 内部评级
                                cusLstWhbxdDto.setInnerEval(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getCusCrdGrade() : "");
                                // 名单归属
                                cusLstWhbxdDto.setListBelg("");
                                // 管户客户经理
                                cusLstWhbxdDto.setManagerId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getManagerId() : "");
                                // 所属机构
                                cusLstWhbxdDto.setBelgOrg(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getManagerBrId() : "");
                                // 状态
                                cusLstWhbxdDto.setStatus(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getCusState() : "");
                                // 登记人
                                cusLstWhbxdDto.setInputId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputId() : "");
                                // 登记机构
                                cusLstWhbxdDto.setInputBrId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputBrId() : "");
                                // 登记时间
                                cusLstWhbxdDto.setInputDate(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputDate() : "");
                                // 更新人
                                cusLstWhbxdDto.setUpdId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdId() : "");
                                // 更新机构
                                cusLstWhbxdDto.setUpdBrId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdBrId() : "");
                                // 更新时间
                                cusLstWhbxdDto.setUpdDate(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdDate() : "");
                                // 创建时间
                                cusLstWhbxdDto.setCreateTime(new Date());
                                // 修改时间
                                cusLstWhbxdDto.setUpdateTime(new Date());
                                cmisCusClientService.addCusLstWhbxd(cusLstWhbxdDto);
                                break;
                            }
                        }
                    }
                }
            }
        }



        //微信通知
        String managerId = lmtGrpApp.getManagerId();
        String mgrTel = "";
        if (StringUtil.isNotEmpty(managerId)) {
            log.info("调用AdminSmUserService用户信息查询服务开始*START***");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("调用AdminSmUserService用户信息查询服务结束*END******");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            try {
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", lmtGrpApp.getGrpCusName());
                if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_01)){
                    paramMap.put("prdName", "集团授信新增");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02)){
                    paramMap.put("prdName", "集团授信变更");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_03)){
                    paramMap.put("prdName", "集团授信续作");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)){
                    paramMap.put("prdName", "集团授信复审");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_05)){
                    paramMap.put("prdName", "集团授信复议");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_06)){
                    paramMap.put("prdName", "集团授信再议");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)){
                    paramMap.put("prdName", "集团预授信细化");
                }else if(lmtGrpApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_08)){
                    paramMap.put("prdName", "集团授信额度调剂");
                }else{
                    log.info("当前授信申请[{}]类型未识别",JSON.toJSONString(lmtGrpApp));
                    paramMap.put("prdName", "集团授信申报");
                }
                paramMap.put("result", "通过");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
            } catch (Exception e) {
                throw new Exception("发送短信失败！");
            }
        }


    }

    /**
     * @方法名称：queryByGrpCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-22 上午 10:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpApp> queryByGrpCusId(QueryModel queryModel) {
        return lmtGrpAppMapper.queryByGrpCusId(queryModel);
    }


    /**
     * @方法名称: riskItem0004
     * @方法描述: 客户标准化产品限额校验
     * @参数与返回说明:
     * @算法描述: 1）若当前集团客户所有成员的标准化产品的授信分项总额超过1500万，则拦截
     * 2）若当前单一客户所有标准化产品的授信分项总额超过1000万，则拦截
     * ---标准化产品类型为范围：信保贷、科技贷、南通信保通、徐州徐信保、即墨政银保、无锡园区保、宿迁园区保、征信贷、优税贷、高企贷、结息贷、诚易融、红领带、巾帼荣誉贷
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0004(String serno) {
        log.info("客户标准化产品限额校验开始********业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        BigDecimal stdPrdLmtTotalAmt;
        // 初始化匹配标准化产品的额度
        BigDecimal totalFiterAmt = new BigDecimal("0.0");
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.STD_PRD);
        // 获取标准化产品配置码值
        // 别看了代码没错，请更新最新的oca jar包
        AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        if (Objects.isNull(adminSmPropDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_014);
            return riskResultDto;
        }
        String bizTypeCond = adminSmPropDto.getPropValue();
        String[] bizTypes = bizTypeCond.split(CmisCommonConstants.DEF_SPILIT_COMMMA);
        // 查询是否为集团授信申请数据
        LmtGrpApp lmtGrpApp = this.queryLmtGrpAppByGrpSerno(serno);
        if (lmtGrpApp != null) {
            // 获取集团授信分项的所有额度，匹配是否为标准化产品，是则额度进行累加，然后与集团标准化额度进行比较
            adminSmPropQueryDto.setPropName(CmisCommonConstants.GRP_CUS_STD_PRD_LMT_TOTAL_AMT);
            // 获取集团客户标准化产品授信分项总额
            adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
            if (Objects.isNull(adminSmPropDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00401);
                return riskResultDto;
            }
            stdPrdLmtTotalAmt = new BigDecimal(StringUtils.isEmpty(adminSmPropDto.getPropValue()) ? "0" : adminSmPropDto.getPropValue());
            List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.queryAllLmtAppSubPrdByGrpSerno(serno);
            if (CollectionUtils.isEmpty(lmtAppSubPrdList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00402);
                return riskResultDto;
            }
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                for (String bizType : bizTypes) {
                    if (bizType.equals(lmtAppSubPrd.getLmtBizTypeProp())) {
                        totalFiterAmt = totalFiterAmt.add(Objects.isNull(lmtAppSubPrd.getLmtAmt()) ? new BigDecimal("0") : lmtAppSubPrd.getLmtAmt());
                    }
                }
            }
            if (totalFiterAmt.compareTo(stdPrdLmtTotalAmt) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00403);
                return riskResultDto;
            }
        } else {
            // 获取单一客户授信分项的所有额度，匹配是否为标准化产品，是则额度进行累加，然后与单一客户标准化额度进行比较
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            if (lmtApp != null) {
                adminSmPropQueryDto.setPropName(CmisCommonConstants.SINGLE_CUS_STD_PRD_LMT_TOTAL_AMT);
                // 获取单一客户标准化产品授信分项总额
                adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
                if (Objects.isNull(adminSmPropDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00404);
                    return riskResultDto;
                }
                stdPrdLmtTotalAmt = new BigDecimal(StringUtils.isEmpty(adminSmPropDto.getPropValue()) ? "0" : adminSmPropDto.getPropValue());
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
                if (CollectionUtils.isEmpty(lmtAppSubList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                    return riskResultDto;
                }
                for (LmtAppSub lmtAppSubs : lmtAppSubList) {
                    List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSubs.getSubSerno());
                    if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            for (String bizType : bizTypes) {
                                if (bizType.equals(lmtAppSubPrd.getLmtBizTypeProp())) {
                                    totalFiterAmt = totalFiterAmt.add(Objects.isNull(lmtAppSubPrd.getLmtAmt()) ? new BigDecimal("0") : lmtAppSubPrd.getLmtAmt());
                                }
                            }
                        }
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0013);
                        return riskResultDto;
                    }
                }
                if (totalFiterAmt.compareTo(stdPrdLmtTotalAmt) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00406);
                    return riskResultDto;
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        log.info("客户标准化产品限额校验结束********业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 根据集团申请流水号查询数据
     *
     * @param grpSerno
     * @return
     */
    public LmtGrpApp queryInfoByGrpSerno(String grpSerno) {
        return lmtGrpAppMapper.queryInfoByGrpSerno(grpSerno);
    }

    /**
     * 根据集团流水号查询成员授信申请信息
     *
     * @param grpSerno
     * @return
     */
    public List<LmtApp> getLmtAppByGrpSerno(String grpSerno) {
        return lmtAppService.getLmtAppByGrpSerno(grpSerno);
    }

    /**
     * 根据集团流水号查询本次申报成员授信申请信息
     *
     * @param grpSerno
     * @return
     */
    public List<LmtApp> getLmtAppByGrpSernoIsDeclare(String grpSerno){
        return lmtAppService.getLmtAppByGrpSernoIsDeclare(grpSerno);
    }

    /**
     * @方法名称: getApprMode
     * @方法描述: 获取
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-07-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String getApprMode(String serno) {
        // 默认单签节点
        String apprMode = "";
        // 查询审批数据
        LmtGrpApp lmtGrpApp = this.queryLmtGrpAppByGrpSerno(serno);
        if(lmtGrpApp.getOpenTotalLmtAmt().compareTo(new BigDecimal("5000000")) <= 0){
            apprMode = CmisFlowConstants.STD_APPR_MODE_02;
        } else if (lmtGrpApp.getOpenTotalLmtAmt().compareTo(new BigDecimal("5000000")) > 0
                && lmtGrpApp.getOpenTotalLmtAmt().compareTo(new BigDecimal("20000000")) <= 0) {
            apprMode = CmisFlowConstants.STD_APPR_MODE_03;
        } else if (lmtGrpApp.getOpenTotalLmtAmt().compareTo(new BigDecimal("20000000")) > 0
                && lmtGrpApp.getOpenTotalLmtAmt().compareTo(new BigDecimal("30000000")) <= 0) {
            apprMode = CmisFlowConstants.STD_APPR_MODE_04;
        } else {
            apprMode = CmisFlowConstants.STD_APPR_MODE_05;
        }
        // 新增成员客户项下低风险分项校验
        String cheskFlg = checkMemAppSubIsAllLowRisk(serno);
        if(StringUtils.isBlank(cheskFlg)){
            // 不做处理
        }else{
            apprMode = cheskFlg;
        }
        log.info("测算审批模式为【{}】", apprMode);
        return apprMode;
    }

    /**
     * @方法名称: getRouterMapResult
     * @方法描述: 获取路由结果集
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-07-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getRouterMapResult(String grpSerno) throws Exception {
        LmtGrpApp lmtGrpApp = this.queryInfoByGrpSerno(grpSerno);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // 提交状态
        resultMap.put("approveStatus", lmtGrpApp.getApproveStatus());
        // 敞口金额
        resultMap.put("openTotalLmtAmt", lmtGrpApp.getOpenTotalLmtAmt());
        // 低风险金额
        resultMap.put("lowRiskTotalLmtAmt", lmtGrpApp.getLowRiskTotalLmtAmt());
        // 授信金额
        resultMap.put("lmtAmt", BigDecimal.ZERO.add(lmtGrpApp.getLowRiskTotalLmtAmt() != null ? lmtGrpApp.getLowRiskTotalLmtAmt() : new BigDecimal(0)).add(lmtGrpApp.getOpenTotalLmtAmt() != null ? lmtGrpApp.getOpenTotalLmtAmt() : new BigDecimal(0)));
        // 上一笔授信申请流水号
        String origiLmtReplySerno = lmtGrpApp.getOrigiLmtReplySerno();
        if(StringUtils.nonBlank(origiLmtReplySerno)){
            LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(origiLmtReplySerno);
            if(lmtGrpReply != null){
                resultMap.put("lastSerno", lmtGrpReply.getGrpSerno());
            }
        }
        //判断当前登录人是否资产保全客户经理
        log.info("调用AdminSmUserService用户岗位信息查询服务开始*START***");
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        userAndDutyReqDto.setManagerId(lmtGrpApp.getManagerId());
        ResultDto<List<UserAndDutyRespDto>> resultDto = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
        log.info("调用AdminSmUserService用户岗位查询服务结束*END******");
        String code = resultDto.getCode();//返回结果
        String isZCBQB = CmisCommonConstants.YES_NO_0;
        List<UserAndDutyRespDto> userAndDutyRespDtos =  resultDto.getData();
        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
            for (UserAndDutyRespDto userAndDutyRespDto : userAndDutyRespDtos) {
                if("SGH21".equals(userAndDutyRespDto.getDutyNo()) || "DHH21".equals(userAndDutyRespDto.getDutyNo())){
                    isZCBQB = CmisCommonConstants.YES_NO_1;
                    break;
                }
            }
        }
        log.info("当前客户经理是否资产保全部-----"+isZCBQB);
        // 是否资产保全部提交
        resultMap.put("isZCBQB", isZCBQB);
        // 机构类型
        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(lmtGrpApp.getManagerBrId());
        String orgLevel = adminSmOrgDtoResultDto.getData().getOrgType().toString();
        resultMap.put("orgLevel", orgLevel);
        // 是否特资部审核
        String isTZBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否特资部审核
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            if (CmisBizConstants.LMT_TYPE_01.equals(lmtGrpApp.getLmtType()) && !"".equals(lmtGrpMemRel.getSingleSerno()) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                CusLstWtsxDto cusLstWtsxDto = iCusClientService.queryCuslstwtsxByCusId(lmtGrpMemRel.getCusId()).getData();
                if (cusLstWtsxDto != null && "01".equals(cusLstWtsxDto.getImportMode())) {
                    isTZBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                    break;
                }
            }
        }
        resultMap.put("isTZBAppr", isTZBAppr);
        log.info("判断总行部门审批权限开始*START***");
        // 是否贸金部审核
        String isMJBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否小企业审核
        String isXQYAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否为为小企业
        String isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_0;
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            if (CmisBizConstants.LMT_TYPE_01.equals(lmtGrpApp.getLmtType()) && !"".equals(lmtGrpMemRel.getSingleSerno()) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                // 是否对公客户，判断协办客户经理节点
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
                log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
                if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                    CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtGrpMemRel.getCusId()).getData();
                    if (cusCorpDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusCorpDto.getIsSmconCus())) {
                        isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
                        break;
                    }
                } else {
                    CusIndivAttrDto cusIndivAttrDto = iCusClientService.queryCusIndivAttrByCusId(lmtGrpMemRel.getCusId()).getData();
                    if (cusIndivAttrDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusIndivAttrDto.getIsSmconCus())) {
                        isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
                        break;
                    }
                }
            }
        }
        // 是否对公客户，判断协办客户经理节点
        resultMap.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
        // 是否公司部审核
        String isGSBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 存量授信金额（不包含委托贷款）
        BigDecimal originLmtAmtNoWT = BigDecimal.ZERO;
        // 新增贷款金额（不包括委托贷款）
        BigDecimal lmtAmtNoWT = BigDecimal.ZERO;
        // 存量授信金额（不包含委托贷款）
        BigDecimal originLmtAmtWT = BigDecimal.ZERO;
        // 新增贷款金额（不包括委托贷款）
        BigDecimal lmtAmtWT = BigDecimal.ZERO;
        // 低风险非关联金额
        BigDecimal lowRiskNoRelAmt = BigDecimal.ZERO;
        // 非小企业含无还本续贷
        String isNoSmconCusRwrop = CmisCommonConstants.STD_ZB_YES_NO_0;

        // 标准化产品属性
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.BANK_STD_PRD);
        AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        String stdPrdProp = String.valueOf(adminSmPropDto.getPropValue());
        String isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("计算分项中的金额开始开始*START***");
        boolean isChg = false;
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            if (!StringUtils.isBlank(lmtGrpMemRel.getSingleSerno())) {
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    // 除去低风险之外的品种
                    if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) { //非低风险分项
                        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {

                            // 判断是否为贸易融资类产品 或者币种是否可以调节
                            if (lmtAppSubPrd.getLmtBizType().startsWith("11") || CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getCurAdjustFlag())) {
                                isMJBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                            }
                            // 授信品种是否包含信保贷 或者 小企业客户授信分项无还本续贷
                            if ("P010".equals(lmtAppSubPrd.getLmtBizTypeProp())
                                    || (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isSmconCus) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getIsRwrop()))) {
                                isXQYAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                            }
                            // 是否非小企业含无还本续贷
                            if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isSmconCus) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getIsRwrop())) {
                                isNoSmconCusRwrop = CmisCommonConstants.STD_ZB_YES_NO_1;
                            }
                            // 计算本次不包含委托贷款金额 ，本次委托贷款金额
                            if ("140203".equals(lmtAppSubPrd.getLmtBizType()) || "14020301".equals(lmtAppSubPrd.getLmtBizType())
                                || "200401".equals(lmtAppSubPrd.getLmtBizType()) || "20040101".equals(lmtAppSubPrd.getLmtBizType())) { //委托贷款
                                lmtAmtWT = lmtAmtWT.add(lmtAppSubPrd.getLmtAmt());
                            }
                            if(StringUtils.isBlank(lmtAppSubPrd.getLmtBizTypeProp())
                                    || !stdPrdProp.contains(lmtAppSubPrd.getLmtBizTypeProp())){
                                isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_0;
                            }
                        }
                    } else { // 低风险
                        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            // 如果为低风险担保类型细分为：全额保证金、国债质押、存单质押则 将其算入
                            List<LmtSubPrdLowGuarRel> lmtSubPrdLowGuarRels = lmtSubPrdLowGuarRelService.queryLmtSubPrdLowGuarRelListByPrdSerno(lmtAppSubPrd.getSubPrdSerno());
                            for (LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel: lmtSubPrdLowGuarRels) {
                                if("10".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                        || "11".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                        || "13".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())){
                                    CusLstGlfDto cusLstGlfDto = iCusClientService.queryCusLstGlfByCusId(lmtGrpMemRel.getCusId()).getData();
                                    if(cusLstGlfDto != null && !StringUtils.isBlank(cusLstGlfDto.getCusId())){
                                        log.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfDto.toString());
                                        lowRiskNoRelAmt = lowRiskNoRelAmt.add(lmtSubPrdLowGuarRel.getLmtAmt());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            lmtAmtNoWT = lmtGrpApp.getOpenTotalLmtAmt().subtract(lmtAmtWT);

            // 判断是否申报金额发生改变
            if(isChg
                    || (lmtGrpMemRel.getLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getLowRiskLmtAmt()).compareTo(lmtGrpMemRel.getOrigiLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getOrigiLowRiskLmtAmt()) != 0
                    || (lmtGrpMemRel.getOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getOpenLmtAmt()).compareTo(lmtGrpMemRel.getOrigiOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getOrigiOpenLmtAmt()) != 0){
                isChg = true;
            }

        }
        log.info("计算分项中的金额结束*END***");
        // 是否是标准化产品
        if(lmtGrpApp.getOpenTotalLmtAmt().compareTo(BigDecimal.ZERO) <= 0){
            isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        resultMap.put("isAllStdPrd", isAllStdPrd);
        log.info("查询存量集团授信开始*START***");
        // 分支行审核规则
        String loanApprMode = "";
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(lmtGrpApp.getGrpCusId());
        BigDecimal originOpenAmt = BigDecimal.ZERO;
        if (lmtGrpReplyAcc != null && !StringUtils.isBlank(lmtGrpReplyAcc.getPkId())) {
            loanApprMode = lmtGrpReplyAcc.getLoanApprMode();
            List<LmtGrpMemRel> lmtGrpMemRelListForGrpAcc = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpReplyAcc.getGrpAccNo());
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelListForGrpAcc) {

                // 查询存量授信
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(lmtGrpMemRel.getSingleSerno());
                if (lmtReplyAcc != null) {
                    List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.selectByAccNo(lmtReplyAcc.getAccNo());
                    for (LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubList) {
                        if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSub.getGuarMode())) { // 低风险分项
                            List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(lmtReplyAccSub.getAccSubNo());
                            for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                                if ("140203".equals(lmtReplyAccSubPrd.getLmtBizType()) || "14020301".equals(lmtReplyAccSubPrd.getLmtBizType())
                                    || "200401".equals(lmtReplyAccSubPrd.getLmtBizType()) || "20040101".equals(lmtReplyAccSubPrd.getLmtBizType())) {
                                    originLmtAmtWT.add(lmtReplyAccSubPrd.getLmtAmt());
                                }
                            }
                        }
                    }
                    originOpenAmt = originOpenAmt.add(lmtReplyAcc.getOpenTotalLmtAmt());
                }
            }
            originLmtAmtNoWT = lmtGrpReplyAcc.getOpenTotalLmtAmt().subtract(originLmtAmtWT);
        }
        if(CmisCommonConstants.LMT_TYPE_05.equals(lmtGrpApp.getLmtType())
                && isChg){
            // 如果复审类型下，金额发生变化，则直接到总行审批
            resultMap.put("loanApprMode", "03");
            resultMap.put("lmtType", lmtGrpApp.getLmtType());
        }else {
            // 其他情况送参数
            resultMap.put("loanApprMode", loanApprMode);
            resultMap.put("lmtType", lmtGrpApp.getLmtType());
        }

        log.info("查询存量集团授信结束*END***");
        // 是否本次申报仅仅申请低风险，或者增加的紧急低风险
        if( lmtGrpApp.getOpenTotalLmtAmt().compareTo(BigDecimal.ZERO) > 0 ){
            resultMap.put("isAssignByTodoNum", CmisCommonConstants.STD_ZB_YES_NO_1);
        }else {
            resultMap.put("isAssignByTodoNum", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        log.info("公司部判断逻辑,入参为：原不包含委托贷款金额【{}】，原委托贷款金额【{}】，本次不包含委托贷款金额【{}】，本次委托贷款金额【{}】，是否非小企业含无还本续贷【{}】",
                originLmtAmtNoWT, originLmtAmtWT, lmtAmtNoWT, lmtAmtWT, isNoSmconCusRwrop);
        // 公司部判断逻辑
        if (originLmtAmtNoWT.compareTo(new BigDecimal("100000000")) >= 0
                || (lmtAmtNoWT.compareTo(new BigDecimal("15000000")) >= 0 && lmtAmtNoWT.compareTo(originLmtAmtNoWT) > 0)
                || originLmtAmtWT.compareTo(new BigDecimal("100000000")) >= 0
                || (lmtAmtWT.compareTo(new BigDecimal("15000000")) >= 0 && lmtAmtWT.compareTo(originLmtAmtWT) > 0)
                || CmisCommonConstants.STD_ZB_YES_NO_1.equals(isNoSmconCusRwrop)) {
            isGSBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
        }


        // 是否贸金部审核
        resultMap.put("isMJBAppr", isMJBAppr);
        // 是否小企业审核
        resultMap.put("isXQYAppr", isXQYAppr);
        // 是否公司部审核
        resultMap.put("isGSBAppr", isGSBAppr);
        LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryFinalLmtGrpApprBySerno(grpSerno);
        if (lmtGrpAppr != null) {
            // 公司部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptGSB", StringUtils.isBlank(lmtGrpAppr.getIsSubToOtherDeptCom()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getIsSubToOtherDeptCom());
            // 公司部审批部门获取
            resultMap.put("commitDeptTypeGSB", StringUtils.isBlank(lmtGrpAppr.getSubOtherDeptCom()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getSubOtherDeptCom());
            // 信贷管理部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptXDGLB", StringUtils.isBlank(lmtGrpAppr.getIsSubOtherDeptXd()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getIsSubOtherDeptXd());
            // 信贷管理部审批部门获取
            resultMap.put("commitDeptTypeXDGLB", StringUtils.isBlank(lmtGrpAppr.getSubOtherDeptXd()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getSubOtherDeptXd());
            // 审批模式
            resultMap.put("apprMode", lmtGrpAppr.getApprMode());
            // 是否大额业务授信备案
            resultMap.put("isDAELmtAppr", StringUtils.isBlank(lmtGrpAppr.getIsBigLmt()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getIsBigLmt());
            // 是否上调权限
            resultMap.put("isUpAppr", StringUtils.isBlank(lmtGrpAppr.getIsUpperApprAuth()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getIsUpperApprAuth());
            // 上调权限类型
            resultMap.put("upApprType", StringUtils.isBlank(lmtGrpAppr.getUpperApprAuthType()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getUpperApprAuthType());
            // 如果上调权限后，则将审批模式中的权限更新为上调后的权限
            if(!StringUtils.isBlank(lmtGrpAppr.getUpperApprAuthType())){
                resultMap.put("apprMode", lmtGrpAppr.getUpperApprAuthType());
            }
            // 是否下调权限
            resultMap.put("isDownAppr", StringUtils.isBlank(lmtGrpAppr.getIsLowerApprAuth()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtGrpAppr.getIsLowerApprAuth());
            // 如果下调权限后，则将审批模式中的权限更新为三人会签
            if(!StringUtils.isBlank(lmtGrpAppr.getIsLowerApprAuth()) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtGrpAppr.getIsLowerApprAuth())){
                resultMap.put("apprMode", CmisFlowConstants.STD_APPR_MODE_03);
            }

        } else {
            // 公司部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptGSB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 公司部审批部门获取
            resultMap.put("commitDeptTypeGSB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 信贷管理部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptXDGLB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 信贷管理部审批部门获取
            resultMap.put("commitDeptTypeXDGLB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 审批模式
            resultMap.put("apprMode", CmisFlowConstants.STD_APPR_MODE_01);
            // 是否大额业务授信备案
            resultMap.put("isDAELmtAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 是否上调权限
            resultMap.put("isUpAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 上调权限类型
            resultMap.put("upApprType", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 是否下调权限
            resultMap.put("isDownAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        log.info("判断总行部门审批权限结束*END***");
        // 是否关联审批
        String isGLAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否关联交易
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            CusLstGlfDto cusLstGlfDto = iCusClientService.queryCusLstGlfByCusId(lmtGrpMemRel.getCusId()).getData();
            if (cusLstGlfDto != null && !StringUtils.isBlank(cusLstGlfDto.getCusId())) {
                log.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfDto.toString());
                log.info("集团成员客户号【{}】为关联方", lmtGrpMemRel.getCusId());
                isGLAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                break;
            }else{
                log.info("根据客户号【{}】查询客户是不在关联客户名单", lmtGrpMemRel.getCusId());
            }
        }
        resultMap.put("isGLAppr", isGLAppr);
        // 低风险非关联方控制额度
        resultMap.put("lowRiskNoRelAmt", lowRiskNoRelAmt);
        // 如果为关联授信
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGLAppr)){
            // 获取本行资本净额
            AdminSmPropQueryDto adminSmPropQueryDto1 = new AdminSmPropQueryDto();
            adminSmPropQueryDto1.setPropName(CmisCommonConstants.BANK_CAPITAL_NET);
            String bankCapitalNet = adminSmPropService.getPropValue(adminSmPropQueryDto1).getData().getPropValue();
            BigDecimal bankCapitalNetAmt = new BigDecimal(bankCapitalNet);
            // 获取本行资本净值
            AdminSmPropQueryDto adminSmPropQueryDto2 = new AdminSmPropQueryDto();
            adminSmPropQueryDto2.setPropName(CmisCommonConstants.BANK_CAPITAL_VALUE);
            String bankCapitalValue = adminSmPropService.getPropValue(adminSmPropQueryDto2).getData().getPropValue();
            BigDecimal bankCapitalValueAmt = new BigDecimal(bankCapitalValue);
            // 本行资本净额于与行资本净值较低值
            BigDecimal minBankCapitalAmt = bankCapitalNetAmt.compareTo(bankCapitalValueAmt) > 0 ? bankCapitalNetAmt : bankCapitalValueAmt;
            // 是否超过关联方预计额度
            String isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_0;
            // 申报金额比例
            BigDecimal newLmtRelRate = BigDecimal.ZERO;
            // 存量敞口金额比例
            BigDecimal allLmtRelRate = BigDecimal.ZERO;
            // 关联交易名单中组别额度关联方预计额度
            BigDecimal relatedPartyGroupForeLmt = BigDecimal.ZERO;
            // 是否超过关联方预计额度
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                CusLstGlfGljyyjedDto cusLstGlfGljyyjedDto = iCusClientService.queryCusLstGlfGljyyjedDtoByCusId(lmtGrpMemRel.getCusId()).getData();
                if (cusLstGlfGljyyjedDto != null && !StringUtils.isBlank(cusLstGlfGljyyjedDto.getGroupForeTotlAmt())) {
                    log.info("根据客户号【{}】查询客户关联客户额度，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfGljyyjedDto.toString());
                    relatedPartyGroupForeLmt = new BigDecimal(cusLstGlfGljyyjedDto.getGroupForeTotlAmt());
                    log.info("集团成员客户号【{}】查询客户关联客户额度为【{}】", lmtGrpMemRel.getCusId(), relatedPartyGroupForeLmt);
                    break;
                }else{
                    log.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfGljyyjedDto);
                }

            }

            BigDecimal glfAppAmt = BigDecimal.ZERO;
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                CusLstGlfDto cusLstGlfDto = iCusClientService.queryCusLstGlfByCusId(lmtGrpMemRel.getCusId()).getData();
                if (cusLstGlfDto != null && !StringUtils.isBlank(cusLstGlfDto.getCusId())) {
                    log.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfDto.toString());
                    log.info("根据客户号【{}】计算授信金额【{}】", lmtGrpMemRel.getCusId(), lmtGrpMemRel.getOpenLmtAmt() == null ? lmtGrpMemRel.getOpenLmtAmt() : BigDecimal.ZERO
                            .add(lmtGrpMemRel.getLowRiskLmtAmt() == null ? lmtGrpMemRel.getLowRiskLmtAmt() : BigDecimal.ZERO));
                    glfAppAmt = glfAppAmt.add(lmtGrpMemRel.getOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getOpenLmtAmt())
                        .add(lmtGrpMemRel.getLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpMemRel.getLowRiskLmtAmt());
                }else{
                    log.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", lmtGrpMemRel.getCusId(), cusLstGlfDto);
                }
            }
            log.info("集团关联方客户授信金额为【{}】", glfAppAmt);
            // 集团中关联方申报金额比较金额 【授信金额】 - 【除去 全额保证金、国债质押、存单质押的金额】 - 【组别预计额度】
            BigDecimal newCompareAmt = glfAppAmt.subtract(lowRiskNoRelAmt).subtract(relatedPartyGroupForeLmt);
            newCompareAmt = newCompareAmt.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : newCompareAmt;
            // 超过关联方预计额度
            if(newCompareAmt.compareTo(BigDecimal.ZERO) > 0){
                isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            BigDecimal openGrpBalance = BigDecimal.ZERO;
            CmisLmt0052ReqDto cmisLmt0052ReqDto = new CmisLmt0052ReqDto();
            cmisLmt0052ReqDto.setCusId(lmtGrpApp.getGrpCusId());
            cmisLmt0052ReqDto.setIsQuryGrp(CmisCommonConstants.STD_ZB_YES_NO_1);
            cmisLmt0052ReqDto.setDealBizNo(UUID.randomUUID().toString());
            CmisLmt0052RespDto cmisLmt0052RespDto = cmisLmtClientService.cmislmt0052(cmisLmt0052ReqDto).getData();
            log.info("根据客户号【{}】查询集团客户存量敞口余额，返回为{}", lmtGrpApp.getGrpCusId(), cmisLmt0052RespDto);
            if (cmisLmt0052RespDto != null && cmisLmt0052RespDto.getSpcaBalanceAmt() != null) {
                openGrpBalance = cmisLmt0052RespDto.getSpcaBalanceAmt();
            }

            // 拟申报金额比例
            newLmtRelRate = newCompareAmt.divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);
            resultMap.put("newLmtRelRate", newLmtRelRate);
            // 拟申报金额 + 存量敞口  金额比例
            allLmtRelRate = newCompareAmt.add(openGrpBalance).divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);
            resultMap.put("allLmtRelRate", allLmtRelRate);
            // 是否超过关联方预计额度
            resultMap.put("isSupGLFLmtAmt", isSupGLFLmtAmt);
        }
        //省心快贷线上审批
        String sxkdRiskResult = ""; //省心快贷风险拦截结果 成功1 失败0
        String isSubAutoAppr = ""; //是否提交自动化审批 1是0否
        String approveResult = ""; //风控自动审批结果 1成功 2否决
        String isGrp = "";//是否集团  1是0否

        resultMap.put("sxkdRiskResult", "0");
        resultMap.put("isSubAutoAppr", "0");
        resultMap.put("approveResult", "0");
        resultMap.put("isGrp", "1");
        resultMap.put("isFCDY", "N");
        return resultMap;
    }

    /*
    * 新增成员客户项下低风险分项校验
    * */
    public String checkMemAppSubIsAllLowRisk(String grpSerno){
        log.info("检验当前集团授信申请流水号{}项下成员客户项下低风险分项校验--------start---------",grpSerno);
        boolean checkLowRiskFlg = true;
        String checkflg = "";
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(grpSerno);
        log.info("当前集团授信申请流水号{}对应的集团授信信息{}",grpSerno,JSON.toJSONString(lmtGrpApp));
        if(lmtGrpApp == null){
            throw BizException.error(null, EcbEnum.ECB010069.key, EcbEnum.ECB010069.value);
        }
        // 授信类型
        String lmtType = lmtGrpApp.getLmtType();
        //  根据集团授信申请流水号获取成员客户信息
        List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        log.info("当前集团授信申请流水号{}对应的集团授信与成员关系信息{}",grpSerno,JSON.toJSONString(list));
        if(list.isEmpty() || list.size() == 0){
            throw BizException.error(null, EcbEnum.ECB010092.key, EcbEnum.ECB010092.value);
        }
        BigDecimal lowRiskAmt = new BigDecimal(0);
        for (int i = 0; i < list.size() ; i++) {
            // 获取成员客户审批结果信息
            LmtGrpMemRel lmtGrpMemRel = list.get(i);
            log.info("当前成员授信信息{}", JSON.toJSONString(lmtGrpMemRel));
            if (CmisCommonConstants.LMT_TYPE_02.equals(lmtType)) { // 变更
                log.info("集团授信变更判断");
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                } else {
                    List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        if (!lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                            log.info("本次成员客户授信分项下存在非低风险的分项{}", lmtAppSub.getSubSerno());
                            checkLowRiskFlg = true;
                            break;
                        }else{
                            lowRiskAmt = lowRiskAmt.add(lmtAppSub.getLmtAmt());
                        }
                    }
                }
            } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtType)) { // 预授信细化
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                } else {
                    List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        if (!lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                            log.info("本次成员客户授信分项下存在非低风险的分项{}", lmtAppSub.getSubSerno());
                            checkLowRiskFlg = true;
                            break;
                        }else{
                            lowRiskAmt = lowRiskAmt.add(lmtAppSub.getLmtAmt());
                        }
                    }
                }
            } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtType)) { // 额度调剂
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                } else {
                    List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        if (!lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                            log.info("本次成员客户授信分项下存在非低风险的分项{}", lmtAppSub.getSubSerno());
                            checkLowRiskFlg = true;
                            break;
                        }else{
                            lowRiskAmt = lowRiskAmt.add(lmtAppSub.getLmtAmt());
                        }
                    }
                }
            } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                } else {
                    List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        if (!lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                            log.info("本次成员客户授信分项下存在非低风险的分项{}", lmtAppSub.getSubSerno());
                            checkLowRiskFlg = true;
                            break;
                        }else{
                            lowRiskAmt = lowRiskAmt.add(lmtAppSub.getLmtAmt());
                        }
                    }
                }

            }

        }
        if(checkLowRiskFlg){
            log.info("本次成员客户授信分项下存在非低风险的风险");
        }else{
            if(lowRiskAmt.compareTo(new BigDecimal(50000000))<=0){
                checkflg = CmisFlowConstants.STD_APPR_MODE_01;
            }else if(lowRiskAmt.compareTo(new BigDecimal(50000000))>0){
                checkflg = CmisFlowConstants.STD_APPR_MODE_02;
            }else{
                checkflg = "";
            }
        }

        return checkflg;
    }

    /**
     * 根据流水号获取原申请信息
     */
    public LmtGrpApp getLastGrpAppBySerno(String grpSerno) {
        return lmtGrpAppMapper.getLastGrpAppBySerno(grpSerno);
    }

    /**
     * 判断当前复议数据是否已复议3次
     */

    public Boolean isThreeFY(LmtGrpApp lmtGrpApp){
        boolean result = true;
        int curTimes = 1;
        try {
            // 获取复议次数
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(CmisCommonConstants.FY_TIMES);
            int fYTimes = Integer.parseInt(adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue());
            log.info("获取系统参数<复议次数>:"+fYTimes);
            // 获取复议天数 FY_DATE
            adminSmPropQueryDto.setPropName(CmisCommonConstants.FY_DATE);
            int fYDate = Integer.parseInt(adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue());
            log.info("获取系统参数<复议天数>:"+fYDate);
            // 查询当前客户的台账数据:批复生效日期+复议天数
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("grpCusId",lmtGrpApp.getGrpCusId());
            queryModel.setSort("updateTime desc");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date inureDate = new Date();
            log.info("区分否决与非否决状态的批复生效日期,当前数据审批状态为:"+lmtGrpApp.getApproveStatus());
            if(CmisCommonConstants.WF_STATUS_998.equals(lmtGrpApp.getApproveStatus())){
                LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpSerno(lmtGrpApp.getGrpSerno());
                log.info("否决发起复议时,取批复中的生效日期:"+JSON.toJSONString(lmtGrpReply));
                inureDate = DateUtils.addDay(simpleDateFormat.parse(lmtGrpReply.getReplyInureDate()),fYDate);
            }else{
                List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccService.queryByGrpCusId(queryModel);
                log.info("非否决发起复议时,取批复台账中的生效日期:"+JSON.toJSONString(lmtGrpReplyAccList.get(0)));
                inureDate = DateUtils.addDay(simpleDateFormat.parse(lmtGrpReplyAccList.get(0).getReplyInureDate().substring(0, 9)),fYDate);
            }
            // 当前时间 取营业日期
            Date nowDate = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
            if(nowDate.after(inureDate)){
                log.info("当前日期已超过当前数据复议的最大天数限制:"+fYDate);
                result = false;
            }else {
                if (!StringUtils.isBlank(lmtGrpApp.getOrigiLmtReplySerno())) {
                    String origiLmtReplySerno = lmtGrpApp.getOrigiLmtReplySerno();
                    while (curTimes < fYTimes) {
                        log.info("第{[]}次复议次数校验,批复编号为:", curTimes+1);
                        LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpReplySerno(origiLmtReplySerno);
                        if (lmtGrpReply != null) {
                            LmtGrpApp lmtGrpApp1 = lmtGrpAppMapper.queryInfoByGrpSerno(lmtGrpReply.getGrpSerno());
                            if (lmtGrpApp1 != null && CmisCommonConstants.LMT_TYPE_05.equals(lmtGrpApp1.getLmtType()) && !StringUtils.isBlank(lmtGrpApp1.getOrigiLmtReplySerno())) {
                                origiLmtReplySerno = lmtGrpApp1.getOrigiLmtReplySerno();
                                curTimes++;
                                result = false;
                            }else{
                                result = true;
                                break;
                            }
                        }else{
                            result = true;
                            break;
                        }
                    }

                }
            }
        } catch (Exception e) {
            // e.printStackTrace();
            throw BizException.error(null, String.valueOf(e.hashCode()), e.getMessage());
        }
        return result;
    }

    /**
     * 校验当前集团向下的成团客户是否全部审批通过
     */

    public Boolean checkIsApproveSuccess(String grpSerno) {
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(grpSerno);
        if(lmtGrpApp == null){
            throw BizException.error(null, EcbEnum.ECB010069.key, EcbEnum.ECB010069.value);
        }
        // 授信类型
        String lmtType = lmtGrpApp.getLmtType();
        //  根据集团授信申请流水号获取成员客户信息
        List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        if(list.isEmpty() || list.size() == 0){
            throw BizException.error(null, EcbEnum.ECB010092.key, EcbEnum.ECB010092.value);
        }
        for (int i = 0; i < list.size() ; i++) {
            // 获取成员客户审批结果信息
            LmtGrpMemRel lmtGrpMemRel = list.get(i);
            if(CmisCommonConstants.LMT_TYPE_02.equals(lmtType)){ // 变更
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        throw BizException.error(null, EcbEnum.ECB010095.key, EcbEnum.ECB010095.value+lmtGrpMemRel.getCusId());
                    }
                    LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
                    log.info("校验成员客户授信申请期限,授信信息:"+ JSON.toJSONString(lmtApp));
                    if(lmtApp.getLmtTerm()>lmtGrpApp.getLmtTerm()){
                        throw BizException.error(null, EcbEnum.ECB020067.key, EcbEnum.ECB020067.value+lmtGrpMemRel.getCusId());
                    }
                }
            }else if(CmisCommonConstants.LMT_TYPE_07.equals(lmtType)){ // 预授信细化
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        throw BizException.error(null, EcbEnum.ECB010095.key, EcbEnum.ECB010095.value+lmtGrpMemRel.getCusId());
                    }
                }
            }else if(CmisCommonConstants.LMT_TYPE_08.equals(lmtType)){ // 额度调剂
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        throw BizException.error(null, EcbEnum.ECB010095.key, EcbEnum.ECB010095.value+lmtGrpMemRel.getCusId());
                    }
                }
            }else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        throw BizException.error(null, EcbEnum.ECB010095.key, EcbEnum.ECB010095.value+lmtGrpMemRel.getCusId());
                    }
                    LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
                    log.info("校验成员客户授信申请期限,授信信息:"+ JSON.toJSONString(lmtApp));
                    if(lmtApp.getLmtTerm()>lmtGrpApp.getLmtTerm()){
                        throw BizException.error(null, EcbEnum.ECB020067.key, EcbEnum.ECB020067.value+lmtGrpMemRel.getCusId());
                    }
                }
            }

        }
        return true;
    }

    /**
     * 资料未全生成影像补扫任务
     * @author jijian_yx
     * @date 2021/8/27 21:04
     **/
    public void createImageSpplInfo(String serno, String bizType, String instanceId,String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(serno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(serno);
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
                docImageSpplClientDto.setBizSerno(lmtGrpMemRel.getSingleSerno());// 关联业务流水号
                docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
                docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_03);// 影像补扫类型 03:授信类型资料补录
                docImageSpplClientDto.setSpplBizType(CmisCommonConstants.STD_SPPL_BIZ_TYPE_06);
                docImageSpplClientDto.setCusId(lmtGrpMemRel.getCusId());// 客户号
                docImageSpplClientDto.setCusName(lmtGrpMemRel.getCusName());// 客户名称
                docImageSpplClientDto.setInputId(lmtGrpMemRel.getManagerId());// 登记人
                docImageSpplClientDto.setInputBrId(lmtGrpMemRel.getManagerBrId());// 登记机构
                docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

                //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
                getUserInfoByDutyCodeDto.setDutyCode("FZH03");
                getUserInfoByDutyCodeDto.setPageNum(1);
                getUserInfoByDutyCodeDto.setPageSize(300);
                List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
                if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                    for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                        if (Objects.equals(adminSmUserDto.getOrgId(), lmtGrpMemRel.getManagerBrId())) {
                            ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                            receivedUserDto.setReceivedUserType("1");// 发送人员类型
                            receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                            receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                            receivedUserList.add(receivedUserDto);
                        }
                    }
                }
                MessageSendDto messageSendDto = new MessageSendDto();
                Map<String, String> map = new HashMap<>();
                map.put("cusName", lmtGrpMemRel.getCusName());
                map.put("iqpName", iqpName);
                messageSendDto.setMessageType("MSG_DA_M_0001");
                messageSendDto.setParams(map);
                messageSendDto.setReceivedUserList(receivedUserList);
                messageSendService.sendMessage(messageSendDto);
            }
        }
    }

    /**
     * 校验授信调剂是否在授信申报的登记中
     */
    public Boolean checkAdjustOnLmtGrpApp(String grpSerno) {
        boolean resFlg = true;
        LmtGrpApp lmtGrpApp = queryLmtGrpAppByGrpSerno(grpSerno);
        if(lmtGrpApp == null){
            throw BizException.error(null, EcbEnum.ECB010069.key, EcbEnum.ECB010069.value);
        }
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRelList = lmtGrpSubPrdAdjustRelService.queryLmtGrpSubPrdAdjustRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            for (LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel : lmtGrpSubPrdAdjustRelList) {
                if (lmtGrpMemRel.getCusId().equals(lmtGrpSubPrdAdjustRel.getCusId())){
                    if(lmtGrpMemRel.getOpenLmtAmt().compareTo(lmtGrpSubPrdAdjustRel.getAdjustOpenLmtAmt()) > 0
                            || lmtGrpMemRel.getLowRiskLmtAmt().compareTo(lmtGrpSubPrdAdjustRel.getAdjustLowRiskLmtAmt()) > 0){
                        resFlg = false;
                        return resFlg;
                    }
                }
            }
        }
        return resFlg;
    }

    /**
     * @作者:lizx
     * @方法名称: sendWbMsgNotice
     * @方法描述:  推送首页提醒事项
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 15:36
     * @return:
     * @算法描述: 无
     */
    @Transactional(rollbackFor=Exception.class)
    public void sendWbMsgNotice(LmtGrpApp lmtGrpApp, String cusName, String bizTypeName, String managerId) throws Exception {
        try{
            log.info("业务类型【{}】流水号【{}】处理通知首页提醒事项处理", lmtGrpApp.getGrpSerno());
            Map<String, String> map = new HashMap<>();
            map.put("cusName", cusName);
            map.put("prdName", bizTypeName);
            AdminSmUserDto adminSmUserDto = adminSmUserService.getByLoginCode(managerId).getData();
            sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1", managerId, adminSmUserDto.getUserMobilephone());
        }catch (Exception e){
            log.error(e.getMessage());
        }

    }

    /**
     *  校验授信调剂前后的授信总额度是否一致
     */

    public Boolean checkAdjustAmtIsSame(Map map) {
        boolean returnFlg = true;
        BigDecimal oldOpenLmtAmt = new BigDecimal(0);
        BigDecimal oldLowRiskLmtAmt = new BigDecimal(0);
        BigDecimal nowOpenLmtAmt = new BigDecimal(0);
        BigDecimal nowLowRiskLmtAmt = new BigDecimal(0);
        try{
            LmtGrpApp lmtGrpApp = this.queryInfoByGrpSerno((String) map.get("grpSerno"));
            log.info("当前集团授信申请信息如下："+JSON.toJSONString(lmtGrpApp));
            if(lmtGrpApp != null){
                // 当前额度
                nowOpenLmtAmt = lmtGrpApp.getOpenTotalLmtAmt()==null?BigDecimal.ZERO:lmtGrpApp.getOpenTotalLmtAmt();
                nowLowRiskLmtAmt = lmtGrpApp.getLowRiskTotalLmtAmt()==null?BigDecimal.ZERO:lmtGrpApp.getLowRiskTotalLmtAmt();
                // 上一笔额度
                LmtGrpReply oldLmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(lmtGrpApp.getOrigiLmtReplySerno());
                log.info("原批复信息如下："+JSON.toJSONString(oldLmtGrpReply));
                if(oldLmtGrpReply != null ){
                    oldOpenLmtAmt = oldLmtGrpReply.getOpenTotalLmtAmt()==null?BigDecimal.ZERO:oldLmtGrpReply.getOpenTotalLmtAmt();
                    oldLowRiskLmtAmt = oldLmtGrpReply.getLowRiskTotalLmtAmt()==null?BigDecimal.ZERO:oldLmtGrpReply.getLowRiskTotalLmtAmt();
                }else{
                    log.info("获取上一笔授信申报批复信息失败！");
                    throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
                }
                // 额度对比
                // 敞口  3000000  9000000
                log.info("原敞口额度{}，现敞口额度{}",oldOpenLmtAmt,nowOpenLmtAmt);
                if(oldOpenLmtAmt.compareTo(nowOpenLmtAmt) < 0){
                    log.info("当前敞口额度{}超过上一笔授信敞口额度{}，请调整！",nowOpenLmtAmt,oldOpenLmtAmt);
                    returnFlg = false;
                    return returnFlg;
                }else if(oldOpenLmtAmt.compareTo(nowOpenLmtAmt) > 0){
                    log.info("当前敞口额度{}小于上一笔授信敞口额度{}，请调整！",nowOpenLmtAmt,oldOpenLmtAmt);
                    returnFlg = false;
                    return returnFlg;
                }else{
                    log.info("敞口额度校验通过！");
                    returnFlg = true;
                }
                // 低风险
                log.info("原低风险额度{}，现低风险额度{}",oldOpenLmtAmt,nowOpenLmtAmt);
                if(oldLowRiskLmtAmt.compareTo(nowLowRiskLmtAmt) < 0){
                    log.info("当前低风险额度{}超过上一笔授信低风险额度{}，请调整！",nowOpenLmtAmt,oldOpenLmtAmt);
                    returnFlg = false;
                }else if(oldLowRiskLmtAmt.compareTo(nowLowRiskLmtAmt) > 0){
                    log.info("当前低风险额度{}小于上一笔授信低风险额度{}，请调整！",nowOpenLmtAmt,oldOpenLmtAmt);
                    returnFlg = false;
                }else{
                    log.info("低风险额度校验通过！");
                    returnFlg = true;
                }
            }else{
                log.info("获取当前集团授信申报信息失败！");
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
        }catch(Exception e){
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        return returnFlg;
    }

}



