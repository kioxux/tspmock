package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class AccCommonDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 借据编号 **/
    private String billNo;
    /** 产品名称 **/
    private String prdName;
    /** 客户编号 **/
    private String cusId;
    /** 客户名称 **/
    private String cusName;
    /** 担保方式 **/
    private String guarMode;
    /** 币种 **/
    private String contCurType;
    /** 金额 **/
    private String loanAmt;
    /** 起始日 **/
    private String startDate;
    /** 到期日 **/
    private String endDate;
    /** 台账状态 **/
    private String accStatus;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

}
