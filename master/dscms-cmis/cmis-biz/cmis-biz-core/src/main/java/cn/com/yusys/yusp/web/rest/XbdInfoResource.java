/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.service.*;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: XbdInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-08-04 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "信保贷信息")
@RequestMapping("/api/xbdinfo")
public class XbdInfoResource {

    @Autowired
    private XbdInfoService xbdInfoService;

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showxbdddetails")
    protected ResultDto<Object> showXbdDdetails(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        XbdInfo xbdInfo = xbdInfoService.selectByPrimaryKey((String)params.get("pvpSerno"));
        if (xbdInfo != null) {
            resultDto.setCode(200);
            resultDto.setData(xbdInfo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<Map> create(@RequestBody XbdInfo xbdInfo) throws URISyntaxException {
        String pvp_serno = xbdInfo.getPvpSerno();
        XbdInfo xbdInfo1 = xbdInfoService.selectByPrimaryKey(pvp_serno);
        Map rtnData = null;
        if (xbdInfo1 == null) {
            rtnData = xbdInfoService.insert(xbdInfo);
        } else {
            rtnData = xbdInfoService.update(xbdInfo);
        }
        return new ResultDto<>(rtnData);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pvpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pvpSerno") String pvpSerno) {
        int result = xbdInfoService.deleteByPrimaryKey(pvpSerno);
        return new ResultDto<Integer>(result);
    }
}
