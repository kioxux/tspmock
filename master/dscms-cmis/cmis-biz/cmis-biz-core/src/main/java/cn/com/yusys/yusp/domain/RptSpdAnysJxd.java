/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RptSpdAnysJxd
 * @类描述: rpt_spd_anys_jxd数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-18 15:30:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jxd")
public class RptSpdAnysJxd extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 主办客户经理编号 **/
	@Column(name = "MAIN_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String mainManagerId;

	/** 协办客户经理编号 **/
	@Column(name = "ASSIST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String assistManagerId;

	/** 第一季度时间 **/
	@Column(name = "FQ_DATE", unique = false, nullable = true, length = 10)
	private String fqDate;

	/** 第一季度本行结息金额 **/
	@Column(name = "FQ_SELF_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fqSelfBankIrsmAmt;

	/** 第一季度他行结息金额 **/
	@Column(name = "FQ_OHTER_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fqOhterBankIrsmAmt;

	/** 第二季度时间 **/
	@Column(name = "SQ_DATE", unique = false, nullable = true, length = 10)
	private String sqDate;

	/** 第二季度本行结息金额 **/
	@Column(name = "SQ_SELF_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sqSelfBankIrsmAmt;

	/** 第二季度他行结息金额 **/
	@Column(name = "SQ_OHTER_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sqOhterBankIrsmAmt;

	/** 第三季度时间 **/
	@Column(name = "TQ_DATE", unique = false, nullable = true, length = 10)
	private String tqDate;

	/** 第三季度本行结息金额 **/
	@Column(name = "TQ_SELF_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tqSelfBankIrsmAmt;

	/** 第三季度他行结息金额 **/
	@Column(name = "TQ_OHTER_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tqOhterBankIrsmAmt;

	/** 第四季度时间 **/
	@Column(name = "LQ_DATE", unique = false, nullable = true, length = 10)
	private String lqDate;

	/** 第四季度本行结息金额 **/
	@Column(name = "LQ_SELF_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lqSelfBankIrsmAmt;

	/** 第四季度他行结息金额 **/
	@Column(name = "LQ_OHTER_BANK_IRSM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lqOhterBankIrsmAmt;

	/** 本行最大贷款额度 **/
	@Column(name = "SELF_BANK_MAX_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfBankMaxLoanAmt;

	/** 他行最大贷款额度 **/
	@Column(name = "OHTER_BANK_MAX_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ohterBankMaxLoanAmt;

	/** 去年销售前三客户一名称 **/
	@Column(name = "LAST_YEAR_TOPSELL1_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell1CusName;

	/** 去年销售前三客户一合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL1_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell1CoopTerm;

	/** 去年销售前三客户一上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell1Income;

	/** 去年销售前三客户二名称 **/
	@Column(name = "LAST_YEAR_TOPSELL2_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell2CusName;

	/** 去年销售前三客户二合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL2_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell2CoopTerm;

	/** 去年销售前三客户二上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell2Income;

	/** 去年销售前三客户三名称 **/
	@Column(name = "LAST_YEAR_TOPSELL3_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell3CusName;

	/** 去年销售前三客户三合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL3_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell3CoopTerm;

	/** 去年销售前三客户三上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell3Income;

	/** 去年企业供货总额占比前三的供应商一名称 **/
	@Column(name = "LAST_YEAR_TOPSELL1_PROVIDER_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell1ProviderName;

	/** 去年企业供货总额占比前三的供应商一合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL1_PROVIDER_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell1ProviderCoopTerm;

	/** 去年企业供货总额占比前三的供应商一上年度供应量 **/
	@Column(name = "LAST_YEAR_TOPSELL1_PROVIDER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell1Provider;

	/** 去年企业供货总额占比前三的供应商二名称 **/
	@Column(name = "LAST_YEAR_TOPSELL2_PROVIDER_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell2ProviderName;

	/** 去年企业供货总额占比前三的供应商二合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL2_PROVIDER_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell2ProviderCoopTerm;

	/** 去年企业供货总额占比前三的供应商二上年度供应量 **/
	@Column(name = "LAST_YEAR_TOPSELL2_PROVIDER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell2Provider;

	/** 去年企业供货总额占比前三的供应商三名称 **/
	@Column(name = "LAST_YEAR_TOPSELL3_PROVIDER_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell3ProviderName;

	/** 去年企业供货总额占比前三的供应商三合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL3_PROVIDER_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell3ProviderCoopTerm;

	/** 去年企业供货总额占比前三的供应商三上年度供应量 **/
	@Column(name = "LAST_YEAR_TOPSELL3_PROVIDER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell3Provider;

	/** 最近二年企业主要经营场所性质 **/
	@Column(name = "LAST_TWO_YEAR_MAIN_BUSINESS_PREMISES_NATURE", unique = false, nullable = true, length = 80)
	private String lastTwoYearMainBusinessPremisesNature;

	/** 近两年经营场所变更次数 **/
	@Column(name = "LAST_TWO_YEAR_BUSINESS_PREMISES_CHG_TIMES", unique = false, nullable = true, length = 80)
	private String lastTwoYearBusinessPremisesChgTimes;

	/** 企业去年收入占比不低于5%省份个数 **/
	@Column(name = "TNOPWTIOEAFNLT5PLY", unique = false, nullable = true, length = 80)
	private String tnopwtioeafnlt5ply;

	/** 企业上年度所有供应商总供应量 **/
	@Column(name = "TSOASITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tsoasitpy;

	/** 超过90天应收账款余额 **/
	@Column(name = "ARBO_90_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo90Days;

	/** 超过180天应收账款余额 **/
	@Column(name = "ARBO_180_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo180Days;

	/** 去年耗用电量能耗值 **/
	@Column(name = "LAST_YEAR_ELECTRICITY_CONSUMPTION", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearElectricityConsumption;

	/** 前年耗用电量能耗值 **/
	@Column(name = "ECVOPCITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ecvopcitpy;

	/** 去年发放工资总额 **/
	@Column(name = "TOTAL_WAGES_PAID_LAST_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWagesPaidLastYear;

	/** 前年发放工资总额 **/
	@Column(name = "TOTAL_WPITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWpitpy;

	/** 可核实营业收入 **/
	@Column(name = "VERIFIABLE_OPERATING_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal verifiableOperatingIncome;

	/** 去年在我行日均存款 **/
	@Column(name = "ADDIOBLY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal addiobly;

	/** 借款人在我行结算账户销售回款 **/
	@Column(name = "SCOTBSAIOB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal scotbsaiob;

	/** 去年在我行日均贷款 **/
	@Column(name = "ADLIOBLY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adliobly;

	/** 我行结算情况 **/
	@Column(name = "SETTLEMENT_OF_OUR_BANK", unique = false, nullable = true, length = 65535)
	private String settlementOfOurBank;

	/** 他行结算情况 **/
	@Column(name = "SETTLEMENT_OF_OTHER_BANKS", unique = false, nullable = true, length = 65535)
	private String settlementOfOtherBanks;

	/** 其他情况说明 **/
	@Column(name = "OTHER_INFORMATION", unique = false, nullable = true, length = 65535)
	private String otherInformation;

	/** 企业征信信用情况简要情况描述 **/
	@Column(name = "PFK_JXD_1_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd1Desc;

	/** 企业征信信用情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_1_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd1Grade1;

	/** 企业征信信用情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_1_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd1Grade2;

	/** 企业及实际控制人负债与核心资产比率简要情况描述 **/
	@Column(name = "PFK_JXD_2_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd2Desc;

	/** 企业及实际控制人负债与核心资产比率主办客户经理评分 **/
	@Column(name = "PFK_JXD_2_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd2Grade1;

	/** 企业及实际控制人负债与核心资产比率协办客户经理评分 **/
	@Column(name = "PFK_JXD_2_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd2Grade2;

	/** 企业及实际控制人负债与自有资产比率简要情况描述 **/
	@Column(name = "PFK_JXD_3_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd3Desc;

	/** 企业及实际控制人负债与自有资产比率主办客户经理评分 **/
	@Column(name = "PFK_JXD_3_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd3Grade1;

	/** 企业及实际控制人负债与自有资产比率协办客户经理评分 **/
	@Column(name = "PFK_JXD_3_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd3Grade2;

	/** 企业现有融资情况简要情况描述 **/
	@Column(name = "PFK_JXD_4_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd4Desc;

	/** 企业现有融资情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_4_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd4Grade1;

	/** 企业现有融资情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_4_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd4Grade2;

	/** 对外担保情况简要情况描述 **/
	@Column(name = "PFK_JXD_5_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd5Desc;

	/** 对外担保情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_5_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd5Grade1;

	/** 对外担保情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_5_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd5Grade2;

	/** 实际控制人从事本行业年限简要情况描述 **/
	@Column(name = "PFK_JXD_6_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd6Desc;

	/** 实际控制人从事本行业年限主办客户经理评分 **/
	@Column(name = "PFK_JXD_6_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd6Grade1;

	/** 实际控制人从事本行业年限协办客户经理评分 **/
	@Column(name = "PFK_JXD_6_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd6Grade2;

	/** 企业所属行业简要情况描述 **/
	@Column(name = "PFK_JXD_7_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd7Desc;

	/** 企业所属行业主办客户经理评分 **/
	@Column(name = "PFK_JXD_7_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd7Grade1;

	/** 企业所属行业协办客户经理评分 **/
	@Column(name = "PFK_JXD_7_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd7Grade2;

	/** 企业经营状况简要情况描述 **/
	@Column(name = "PFK_JXD_8_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd8Desc;

	/** 企业经营状况主办客户经理评分 **/
	@Column(name = "PFK_JXD_8_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd8Grade1;

	/** 企业经营状况协办客户经理评分 **/
	@Column(name = "PFK_JXD_8_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd8Grade2;

	/** 企业上下游合作情况简要情况描述 **/
	@Column(name = "PFK_JXD_9_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd9Desc;

	/** 企业上下游合作情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_9_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd9Grade1;

	/** 企业上下游合作情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_9_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd9Grade2;

	/** 法人或实际控制人其他产业经营情况简要情况描述 **/
	@Column(name = "PFK_JXD_10_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd10Desc;

	/** 法人或实际控制人其他产业经营情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_10_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd10Grade1;

	/** 法人或实际控制人其他产业经营情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_10_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd10Grade2;

	/** 企业及实际控人名下房产抵押情况简要情况描述 **/
	@Column(name = "PFK_JXD_11_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd11Desc;

	/** 企业及实际控人名下房产抵押情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_11_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd11Grade1;

	/** 企业及实际控人名下房产抵押情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_11_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd11Grade2;

	/** 申请金额与企业主名下可抵押房产比率简要情况描述 **/
	@Column(name = "PFK_JXD_12_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd12Desc;

	/** 申请金额与企业主名下可抵押房产比率主办客户经理评分 **/
	@Column(name = "PFK_JXD_12_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd12Grade1;

	/** 申请金额与企业主名下可抵押房产比率协办客户经理评分 **/
	@Column(name = "PFK_JXD_12_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd12Grade2;

	/** 法人面谈情况简要情况描述 **/
	@Column(name = "PFK_JXD_13_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd13Desc;

	/** 法人面谈情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_13_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd13Grade1;

	/** 法人面谈情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_13_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd13Grade2;

	/** 法人或实际控制人个人情况简要情况描述 **/
	@Column(name = "PFK_JXD_14_DESC", unique = false, nullable = true, length = 500)
	private String pfkJxd14Desc;

	/** 法人或实际控制人个人情况主办客户经理评分 **/
	@Column(name = "PFK_JXD_14_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkJxd14Grade1;

	/** 法人或实际控制人个人情况协办客户经理评分 **/
	@Column(name = "PFK_JXD_14_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkJxd14Grade2;

	/** 企业有无违规排污，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_JXD_1", unique = false, nullable = true, length = 65535)
	private String focusJxd1;

	/** 企业员工人数是否稳定，员工待遇是否合理 **/
	@Column(name = "FOCUS_JXD_2", unique = false, nullable = true, length = 65535)
	private String focusJxd2;

	/** 企业有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_JXD_3", unique = false, nullable = true, length = 65535)
	private String focusJxd3;

	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_JXD_4", unique = false, nullable = true, length = 65535)
	private String focusJxd4;

	/** 企业贷款银行是否超过3家 **/
	@Column(name = "FOCUS_JXD_5", unique = false, nullable = true, length = 65535)
	private String focusJxd5;

	/** 企业及实际控制人他行信用贷款授信额+我行信用贷款+信用卡用信额+结息贷申请额是否大于500万元 **/
	@Column(name = "FOCUS_JXD_6", unique = false, nullable = true, length = 65535)
	private String focusJxd6;

	/** 合规经营其它不利情况请简述 **/
	@Column(name = "FOCUS_JXD_7", unique = false, nullable = true, length = 65535)
	private String focusJxd7;

	/** 实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_JXD_8", unique = false, nullable = true, length = 65535)
	private String focusJxd8;

	/** 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_JXD_9", unique = false, nullable = true, length = 65535)
	private String focusJxd9;

	/** 实际控制人是否参与民间融资、投资高风险行业等行为 **/
	@Column(name = "FOCUS_JXD_10", unique = false, nullable = true, length = 65535)
	private String focusJxd10;

	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_JXD_11", unique = false, nullable = true, length = 65535)
	private String focusJxd11;

	/** 企业自制报表资产负债率是否超过75% **/
	@Column(name = "FOCUS_JXD_12", unique = false, nullable = true, length = 65535)
	private String focusJxd12;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param mainManagerId
	 */
	public void setMainManagerId(String mainManagerId) {
		this.mainManagerId = mainManagerId;
	}

	/**
	 * @return mainManagerId
	 */
	public String getMainManagerId() {
		return this.mainManagerId;
	}

	/**
	 * @param assistManagerId
	 */
	public void setAssistManagerId(String assistManagerId) {
		this.assistManagerId = assistManagerId;
	}

	/**
	 * @return assistManagerId
	 */
	public String getAssistManagerId() {
		return this.assistManagerId;
	}

	/**
	 * @param fqDate
	 */
	public void setFqDate(String fqDate) {
		this.fqDate = fqDate;
	}

	/**
	 * @return fqDate
	 */
	public String getFqDate() {
		return this.fqDate;
	}

	/**
	 * @param fqSelfBankIrsmAmt
	 */
	public void setFqSelfBankIrsmAmt(java.math.BigDecimal fqSelfBankIrsmAmt) {
		this.fqSelfBankIrsmAmt = fqSelfBankIrsmAmt;
	}

	/**
	 * @return fqSelfBankIrsmAmt
	 */
	public java.math.BigDecimal getFqSelfBankIrsmAmt() {
		return this.fqSelfBankIrsmAmt;
	}

	/**
	 * @param fqOhterBankIrsmAmt
	 */
	public void setFqOhterBankIrsmAmt(java.math.BigDecimal fqOhterBankIrsmAmt) {
		this.fqOhterBankIrsmAmt = fqOhterBankIrsmAmt;
	}

	/**
	 * @return fqOhterBankIrsmAmt
	 */
	public java.math.BigDecimal getFqOhterBankIrsmAmt() {
		return this.fqOhterBankIrsmAmt;
	}

	/**
	 * @param sqDate
	 */
	public void setSqDate(String sqDate) {
		this.sqDate = sqDate;
	}

	/**
	 * @return sqDate
	 */
	public String getSqDate() {
		return this.sqDate;
	}

	/**
	 * @param sqSelfBankIrsmAmt
	 */
	public void setSqSelfBankIrsmAmt(java.math.BigDecimal sqSelfBankIrsmAmt) {
		this.sqSelfBankIrsmAmt = sqSelfBankIrsmAmt;
	}

	/**
	 * @return sqSelfBankIrsmAmt
	 */
	public java.math.BigDecimal getSqSelfBankIrsmAmt() {
		return this.sqSelfBankIrsmAmt;
	}

	/**
	 * @param sqOhterBankIrsmAmt
	 */
	public void setSqOhterBankIrsmAmt(java.math.BigDecimal sqOhterBankIrsmAmt) {
		this.sqOhterBankIrsmAmt = sqOhterBankIrsmAmt;
	}

	/**
	 * @return sqOhterBankIrsmAmt
	 */
	public java.math.BigDecimal getSqOhterBankIrsmAmt() {
		return this.sqOhterBankIrsmAmt;
	}

	/**
	 * @param tqDate
	 */
	public void setTqDate(String tqDate) {
		this.tqDate = tqDate;
	}

	/**
	 * @return tqDate
	 */
	public String getTqDate() {
		return this.tqDate;
	}

	/**
	 * @param tqSelfBankIrsmAmt
	 */
	public void setTqSelfBankIrsmAmt(java.math.BigDecimal tqSelfBankIrsmAmt) {
		this.tqSelfBankIrsmAmt = tqSelfBankIrsmAmt;
	}

	/**
	 * @return tqSelfBankIrsmAmt
	 */
	public java.math.BigDecimal getTqSelfBankIrsmAmt() {
		return this.tqSelfBankIrsmAmt;
	}

	/**
	 * @param tqOhterBankIrsmAmt
	 */
	public void setTqOhterBankIrsmAmt(java.math.BigDecimal tqOhterBankIrsmAmt) {
		this.tqOhterBankIrsmAmt = tqOhterBankIrsmAmt;
	}

	/**
	 * @return tqOhterBankIrsmAmt
	 */
	public java.math.BigDecimal getTqOhterBankIrsmAmt() {
		return this.tqOhterBankIrsmAmt;
	}

	/**
	 * @param lqDate
	 */
	public void setLqDate(String lqDate) {
		this.lqDate = lqDate;
	}

	/**
	 * @return lqDate
	 */
	public String getLqDate() {
		return this.lqDate;
	}

	/**
	 * @param lqSelfBankIrsmAmt
	 */
	public void setLqSelfBankIrsmAmt(java.math.BigDecimal lqSelfBankIrsmAmt) {
		this.lqSelfBankIrsmAmt = lqSelfBankIrsmAmt;
	}

	/**
	 * @return lqSelfBankIrsmAmt
	 */
	public java.math.BigDecimal getLqSelfBankIrsmAmt() {
		return this.lqSelfBankIrsmAmt;
	}

	/**
	 * @param lqOhterBankIrsmAmt
	 */
	public void setLqOhterBankIrsmAmt(java.math.BigDecimal lqOhterBankIrsmAmt) {
		this.lqOhterBankIrsmAmt = lqOhterBankIrsmAmt;
	}

	/**
	 * @return lqOhterBankIrsmAmt
	 */
	public java.math.BigDecimal getLqOhterBankIrsmAmt() {
		return this.lqOhterBankIrsmAmt;
	}

	/**
	 * @param selfBankMaxLoanAmt
	 */
	public void setSelfBankMaxLoanAmt(java.math.BigDecimal selfBankMaxLoanAmt) {
		this.selfBankMaxLoanAmt = selfBankMaxLoanAmt;
	}

	/**
	 * @return selfBankMaxLoanAmt
	 */
	public java.math.BigDecimal getSelfBankMaxLoanAmt() {
		return this.selfBankMaxLoanAmt;
	}

	/**
	 * @param ohterBankMaxLoanAmt
	 */
	public void setOhterBankMaxLoanAmt(java.math.BigDecimal ohterBankMaxLoanAmt) {
		this.ohterBankMaxLoanAmt = ohterBankMaxLoanAmt;
	}

	/**
	 * @return ohterBankMaxLoanAmt
	 */
	public java.math.BigDecimal getOhterBankMaxLoanAmt() {
		return this.ohterBankMaxLoanAmt;
	}

	/**
	 * @param lastYearTopsell1CusName
	 */
	public void setLastYearTopsell1CusName(String lastYearTopsell1CusName) {
		this.lastYearTopsell1CusName = lastYearTopsell1CusName;
	}

	/**
	 * @return lastYearTopsell1CusName
	 */
	public String getLastYearTopsell1CusName() {
		return this.lastYearTopsell1CusName;
	}

	/**
	 * @param lastYearTopsell1CoopTerm
	 */
	public void setLastYearTopsell1CoopTerm(String lastYearTopsell1CoopTerm) {
		this.lastYearTopsell1CoopTerm = lastYearTopsell1CoopTerm;
	}

	/**
	 * @return lastYearTopsell1CoopTerm
	 */
	public String getLastYearTopsell1CoopTerm() {
		return this.lastYearTopsell1CoopTerm;
	}

	/**
	 * @param lastYearTopsell1Income
	 */
	public void setLastYearTopsell1Income(java.math.BigDecimal lastYearTopsell1Income) {
		this.lastYearTopsell1Income = lastYearTopsell1Income;
	}

	/**
	 * @return lastYearTopsell1Income
	 */
	public java.math.BigDecimal getLastYearTopsell1Income() {
		return this.lastYearTopsell1Income;
	}

	/**
	 * @param lastYearTopsell2CusName
	 */
	public void setLastYearTopsell2CusName(String lastYearTopsell2CusName) {
		this.lastYearTopsell2CusName = lastYearTopsell2CusName;
	}

	/**
	 * @return lastYearTopsell2CusName
	 */
	public String getLastYearTopsell2CusName() {
		return this.lastYearTopsell2CusName;
	}

	/**
	 * @param lastYearTopsell2CoopTerm
	 */
	public void setLastYearTopsell2CoopTerm(String lastYearTopsell2CoopTerm) {
		this.lastYearTopsell2CoopTerm = lastYearTopsell2CoopTerm;
	}

	/**
	 * @return lastYearTopsell2CoopTerm
	 */
	public String getLastYearTopsell2CoopTerm() {
		return this.lastYearTopsell2CoopTerm;
	}

	/**
	 * @param lastYearTopsell2Income
	 */
	public void setLastYearTopsell2Income(java.math.BigDecimal lastYearTopsell2Income) {
		this.lastYearTopsell2Income = lastYearTopsell2Income;
	}

	/**
	 * @return lastYearTopsell2Income
	 */
	public java.math.BigDecimal getLastYearTopsell2Income() {
		return this.lastYearTopsell2Income;
	}

	/**
	 * @param lastYearTopsell3CusName
	 */
	public void setLastYearTopsell3CusName(String lastYearTopsell3CusName) {
		this.lastYearTopsell3CusName = lastYearTopsell3CusName;
	}

	/**
	 * @return lastYearTopsell3CusName
	 */
	public String getLastYearTopsell3CusName() {
		return this.lastYearTopsell3CusName;
	}

	/**
	 * @param lastYearTopsell3CoopTerm
	 */
	public void setLastYearTopsell3CoopTerm(String lastYearTopsell3CoopTerm) {
		this.lastYearTopsell3CoopTerm = lastYearTopsell3CoopTerm;
	}

	/**
	 * @return lastYearTopsell3CoopTerm
	 */
	public String getLastYearTopsell3CoopTerm() {
		return this.lastYearTopsell3CoopTerm;
	}

	/**
	 * @param lastYearTopsell3Income
	 */
	public void setLastYearTopsell3Income(java.math.BigDecimal lastYearTopsell3Income) {
		this.lastYearTopsell3Income = lastYearTopsell3Income;
	}

	/**
	 * @return lastYearTopsell3Income
	 */
	public java.math.BigDecimal getLastYearTopsell3Income() {
		return this.lastYearTopsell3Income;
	}

	/**
	 * @param lastYearTopsell1ProviderName
	 */
	public void setLastYearTopsell1ProviderName(String lastYearTopsell1ProviderName) {
		this.lastYearTopsell1ProviderName = lastYearTopsell1ProviderName;
	}

	/**
	 * @return lastYearTopsell1ProviderName
	 */
	public String getLastYearTopsell1ProviderName() {
		return this.lastYearTopsell1ProviderName;
	}

	/**
	 * @param lastYearTopsell1ProviderCoopTerm
	 */
	public void setLastYearTopsell1ProviderCoopTerm(String lastYearTopsell1ProviderCoopTerm) {
		this.lastYearTopsell1ProviderCoopTerm = lastYearTopsell1ProviderCoopTerm;
	}

	/**
	 * @return lastYearTopsell1ProviderCoopTerm
	 */
	public String getLastYearTopsell1ProviderCoopTerm() {
		return this.lastYearTopsell1ProviderCoopTerm;
	}

	/**
	 * @param lastYearTopsell1Provider
	 */
	public void setLastYearTopsell1Provider(java.math.BigDecimal lastYearTopsell1Provider) {
		this.lastYearTopsell1Provider = lastYearTopsell1Provider;
	}

	/**
	 * @return lastYearTopsell1Provider
	 */
	public java.math.BigDecimal getLastYearTopsell1Provider() {
		return this.lastYearTopsell1Provider;
	}

	/**
	 * @param lastYearTopsell2ProviderName
	 */
	public void setLastYearTopsell2ProviderName(String lastYearTopsell2ProviderName) {
		this.lastYearTopsell2ProviderName = lastYearTopsell2ProviderName;
	}

	/**
	 * @return lastYearTopsell2ProviderName
	 */
	public String getLastYearTopsell2ProviderName() {
		return this.lastYearTopsell2ProviderName;
	}

	/**
	 * @param lastYearTopsell2ProviderCoopTerm
	 */
	public void setLastYearTopsell2ProviderCoopTerm(String lastYearTopsell2ProviderCoopTerm) {
		this.lastYearTopsell2ProviderCoopTerm = lastYearTopsell2ProviderCoopTerm;
	}

	/**
	 * @return lastYearTopsell2ProviderCoopTerm
	 */
	public String getLastYearTopsell2ProviderCoopTerm() {
		return this.lastYearTopsell2ProviderCoopTerm;
	}

	/**
	 * @param lastYearTopsell2Provider
	 */
	public void setLastYearTopsell2Provider(java.math.BigDecimal lastYearTopsell2Provider) {
		this.lastYearTopsell2Provider = lastYearTopsell2Provider;
	}

	/**
	 * @return lastYearTopsell2Provider
	 */
	public java.math.BigDecimal getLastYearTopsell2Provider() {
		return this.lastYearTopsell2Provider;
	}

	/**
	 * @param lastYearTopsell3ProviderName
	 */
	public void setLastYearTopsell3ProviderName(String lastYearTopsell3ProviderName) {
		this.lastYearTopsell3ProviderName = lastYearTopsell3ProviderName;
	}

	/**
	 * @return lastYearTopsell3ProviderName
	 */
	public String getLastYearTopsell3ProviderName() {
		return this.lastYearTopsell3ProviderName;
	}

	/**
	 * @param lastYearTopsell3ProviderCoopTerm
	 */
	public void setLastYearTopsell3ProviderCoopTerm(String lastYearTopsell3ProviderCoopTerm) {
		this.lastYearTopsell3ProviderCoopTerm = lastYearTopsell3ProviderCoopTerm;
	}

	/**
	 * @return lastYearTopsell3ProviderCoopTerm
	 */
	public String getLastYearTopsell3ProviderCoopTerm() {
		return this.lastYearTopsell3ProviderCoopTerm;
	}

	/**
	 * @param lastYearTopsell3Provider
	 */
	public void setLastYearTopsell3Provider(java.math.BigDecimal lastYearTopsell3Provider) {
		this.lastYearTopsell3Provider = lastYearTopsell3Provider;
	}

	/**
	 * @return lastYearTopsell3Provider
	 */
	public java.math.BigDecimal getLastYearTopsell3Provider() {
		return this.lastYearTopsell3Provider;
	}

	/**
	 * @param lastTwoYearMainBusinessPremisesNature
	 */
	public void setLastTwoYearMainBusinessPremisesNature(String lastTwoYearMainBusinessPremisesNature) {
		this.lastTwoYearMainBusinessPremisesNature = lastTwoYearMainBusinessPremisesNature;
	}

	/**
	 * @return lastTwoYearMainBusinessPremisesNature
	 */
	public String getLastTwoYearMainBusinessPremisesNature() {
		return this.lastTwoYearMainBusinessPremisesNature;
	}

	/**
	 * @param lastTwoYearBusinessPremisesChgTimes
	 */
	public void setLastTwoYearBusinessPremisesChgTimes(String lastTwoYearBusinessPremisesChgTimes) {
		this.lastTwoYearBusinessPremisesChgTimes = lastTwoYearBusinessPremisesChgTimes;
	}

	/**
	 * @return lastTwoYearBusinessPremisesChgTimes
	 */
	public String getLastTwoYearBusinessPremisesChgTimes() {
		return this.lastTwoYearBusinessPremisesChgTimes;
	}

	/**
	 * @param tnopwtioeafnlt5ply
	 */
	public void setTnopwtioeafnlt5ply(String tnopwtioeafnlt5ply) {
		this.tnopwtioeafnlt5ply = tnopwtioeafnlt5ply;
	}

	/**
	 * @return tnopwtioeafnlt5ply
	 */
	public String getTnopwtioeafnlt5ply() {
		return this.tnopwtioeafnlt5ply;
	}

	/**
	 * @param tsoasitpy
	 */
	public void setTsoasitpy(java.math.BigDecimal tsoasitpy) {
		this.tsoasitpy = tsoasitpy;
	}

	/**
	 * @return tsoasitpy
	 */
	public java.math.BigDecimal getTsoasitpy() {
		return this.tsoasitpy;
	}

	/**
	 * @param arbo90Days
	 */
	public void setArbo90Days(java.math.BigDecimal arbo90Days) {
		this.arbo90Days = arbo90Days;
	}

	/**
	 * @return arbo90Days
	 */
	public java.math.BigDecimal getArbo90Days() {
		return this.arbo90Days;
	}

	/**
	 * @param arbo180Days
	 */
	public void setArbo180Days(java.math.BigDecimal arbo180Days) {
		this.arbo180Days = arbo180Days;
	}

	/**
	 * @return arbo180Days
	 */
	public java.math.BigDecimal getArbo180Days() {
		return this.arbo180Days;
	}

	/**
	 * @param lastYearElectricityConsumption
	 */
	public void setLastYearElectricityConsumption(java.math.BigDecimal lastYearElectricityConsumption) {
		this.lastYearElectricityConsumption = lastYearElectricityConsumption;
	}

	/**
	 * @return lastYearElectricityConsumption
	 */
	public java.math.BigDecimal getLastYearElectricityConsumption() {
		return this.lastYearElectricityConsumption;
	}

	/**
	 * @param ecvopcitpy
	 */
	public void setEcvopcitpy(java.math.BigDecimal ecvopcitpy) {
		this.ecvopcitpy = ecvopcitpy;
	}

	/**
	 * @return ecvopcitpy
	 */
	public java.math.BigDecimal getEcvopcitpy() {
		return this.ecvopcitpy;
	}

	/**
	 * @param totalWagesPaidLastYear
	 */
	public void setTotalWagesPaidLastYear(java.math.BigDecimal totalWagesPaidLastYear) {
		this.totalWagesPaidLastYear = totalWagesPaidLastYear;
	}

	/**
	 * @return totalWagesPaidLastYear
	 */
	public java.math.BigDecimal getTotalWagesPaidLastYear() {
		return this.totalWagesPaidLastYear;
	}

	/**
	 * @param totalWpitpy
	 */
	public void setTotalWpitpy(java.math.BigDecimal totalWpitpy) {
		this.totalWpitpy = totalWpitpy;
	}

	/**
	 * @return totalWpitpy
	 */
	public java.math.BigDecimal getTotalWpitpy() {
		return this.totalWpitpy;
	}

	/**
	 * @param verifiableOperatingIncome
	 */
	public void setVerifiableOperatingIncome(java.math.BigDecimal verifiableOperatingIncome) {
		this.verifiableOperatingIncome = verifiableOperatingIncome;
	}

	/**
	 * @return verifiableOperatingIncome
	 */
	public java.math.BigDecimal getVerifiableOperatingIncome() {
		return this.verifiableOperatingIncome;
	}

	/**
	 * @param addiobly
	 */
	public void setAddiobly(java.math.BigDecimal addiobly) {
		this.addiobly = addiobly;
	}

	/**
	 * @return addiobly
	 */
	public java.math.BigDecimal getAddiobly() {
		return this.addiobly;
	}

	/**
	 * @param scotbsaiob
	 */
	public void setScotbsaiob(java.math.BigDecimal scotbsaiob) {
		this.scotbsaiob = scotbsaiob;
	}

	/**
	 * @return scotbsaiob
	 */
	public java.math.BigDecimal getScotbsaiob() {
		return this.scotbsaiob;
	}

	/**
	 * @param adliobly
	 */
	public void setAdliobly(java.math.BigDecimal adliobly) {
		this.adliobly = adliobly;
	}

	/**
	 * @return adliobly
	 */
	public java.math.BigDecimal getAdliobly() {
		return this.adliobly;
	}

	/**
	 * @param settlementOfOurBank
	 */
	public void setSettlementOfOurBank(String settlementOfOurBank) {
		this.settlementOfOurBank = settlementOfOurBank;
	}

	/**
	 * @return settlementOfOurBank
	 */
	public String getSettlementOfOurBank() {
		return this.settlementOfOurBank;
	}

	/**
	 * @param settlementOfOtherBanks
	 */
	public void setSettlementOfOtherBanks(String settlementOfOtherBanks) {
		this.settlementOfOtherBanks = settlementOfOtherBanks;
	}

	/**
	 * @return settlementOfOtherBanks
	 */
	public String getSettlementOfOtherBanks() {
		return this.settlementOfOtherBanks;
	}

	/**
	 * @param otherInformation
	 */
	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	/**
	 * @return otherInformation
	 */
	public String getOtherInformation() {
		return this.otherInformation;
	}

	/**
	 * @param pfkJxd1Desc
	 */
	public void setPfkJxd1Desc(String pfkJxd1Desc) {
		this.pfkJxd1Desc = pfkJxd1Desc;
	}

	/**
	 * @return pfkJxd1Desc
	 */
	public String getPfkJxd1Desc() {
		return this.pfkJxd1Desc;
	}

	/**
	 * @param pfkJxd1Grade1
	 */
	public void setPfkJxd1Grade1(Integer pfkJxd1Grade1) {
		this.pfkJxd1Grade1 = pfkJxd1Grade1;
	}

	/**
	 * @return pfkJxd1Grade1
	 */
	public Integer getPfkJxd1Grade1() {
		return this.pfkJxd1Grade1;
	}

	/**
	 * @param pfkJxd1Grade2
	 */
	public void setPfkJxd1Grade2(Integer pfkJxd1Grade2) {
		this.pfkJxd1Grade2 = pfkJxd1Grade2;
	}

	/**
	 * @return pfkJxd1Grade2
	 */
	public Integer getPfkJxd1Grade2() {
		return this.pfkJxd1Grade2;
	}

	/**
	 * @param pfkJxd2Desc
	 */
	public void setPfkJxd2Desc(String pfkJxd2Desc) {
		this.pfkJxd2Desc = pfkJxd2Desc;
	}

	/**
	 * @return pfkJxd2Desc
	 */
	public String getPfkJxd2Desc() {
		return this.pfkJxd2Desc;
	}

	/**
	 * @param pfkJxd2Grade1
	 */
	public void setPfkJxd2Grade1(Integer pfkJxd2Grade1) {
		this.pfkJxd2Grade1 = pfkJxd2Grade1;
	}

	/**
	 * @return pfkJxd2Grade1
	 */
	public Integer getPfkJxd2Grade1() {
		return this.pfkJxd2Grade1;
	}

	/**
	 * @param pfkJxd2Grade2
	 */
	public void setPfkJxd2Grade2(Integer pfkJxd2Grade2) {
		this.pfkJxd2Grade2 = pfkJxd2Grade2;
	}

	/**
	 * @return pfkJxd2Grade2
	 */
	public Integer getPfkJxd2Grade2() {
		return this.pfkJxd2Grade2;
	}

	/**
	 * @param pfkJxd3Desc
	 */
	public void setPfkJxd3Desc(String pfkJxd3Desc) {
		this.pfkJxd3Desc = pfkJxd3Desc;
	}

	/**
	 * @return pfkJxd3Desc
	 */
	public String getPfkJxd3Desc() {
		return this.pfkJxd3Desc;
	}

	/**
	 * @param pfkJxd3Grade1
	 */
	public void setPfkJxd3Grade1(Integer pfkJxd3Grade1) {
		this.pfkJxd3Grade1 = pfkJxd3Grade1;
	}

	/**
	 * @return pfkJxd3Grade1
	 */
	public Integer getPfkJxd3Grade1() {
		return this.pfkJxd3Grade1;
	}

	/**
	 * @param pfkJxd3Grade2
	 */
	public void setPfkJxd3Grade2(Integer pfkJxd3Grade2) {
		this.pfkJxd3Grade2 = pfkJxd3Grade2;
	}

	/**
	 * @return pfkJxd3Grade2
	 */
	public Integer getPfkJxd3Grade2() {
		return this.pfkJxd3Grade2;
	}

	/**
	 * @param pfkJxd4Desc
	 */
	public void setPfkJxd4Desc(String pfkJxd4Desc) {
		this.pfkJxd4Desc = pfkJxd4Desc;
	}

	/**
	 * @return pfkJxd4Desc
	 */
	public String getPfkJxd4Desc() {
		return this.pfkJxd4Desc;
	}

	/**
	 * @param pfkJxd4Grade1
	 */
	public void setPfkJxd4Grade1(Integer pfkJxd4Grade1) {
		this.pfkJxd4Grade1 = pfkJxd4Grade1;
	}

	/**
	 * @return pfkJxd4Grade1
	 */
	public Integer getPfkJxd4Grade1() {
		return this.pfkJxd4Grade1;
	}

	/**
	 * @param pfkJxd4Grade2
	 */
	public void setPfkJxd4Grade2(Integer pfkJxd4Grade2) {
		this.pfkJxd4Grade2 = pfkJxd4Grade2;
	}

	/**
	 * @return pfkJxd4Grade2
	 */
	public Integer getPfkJxd4Grade2() {
		return this.pfkJxd4Grade2;
	}

	/**
	 * @param pfkJxd5Desc
	 */
	public void setPfkJxd5Desc(String pfkJxd5Desc) {
		this.pfkJxd5Desc = pfkJxd5Desc;
	}

	/**
	 * @return pfkJxd5Desc
	 */
	public String getPfkJxd5Desc() {
		return this.pfkJxd5Desc;
	}

	/**
	 * @param pfkJxd5Grade1
	 */
	public void setPfkJxd5Grade1(Integer pfkJxd5Grade1) {
		this.pfkJxd5Grade1 = pfkJxd5Grade1;
	}

	/**
	 * @return pfkJxd5Grade1
	 */
	public Integer getPfkJxd5Grade1() {
		return this.pfkJxd5Grade1;
	}

	/**
	 * @param pfkJxd5Grade2
	 */
	public void setPfkJxd5Grade2(Integer pfkJxd5Grade2) {
		this.pfkJxd5Grade2 = pfkJxd5Grade2;
	}

	/**
	 * @return pfkJxd5Grade2
	 */
	public Integer getPfkJxd5Grade2() {
		return this.pfkJxd5Grade2;
	}

	/**
	 * @param pfkJxd6Desc
	 */
	public void setPfkJxd6Desc(String pfkJxd6Desc) {
		this.pfkJxd6Desc = pfkJxd6Desc;
	}

	/**
	 * @return pfkJxd6Desc
	 */
	public String getPfkJxd6Desc() {
		return this.pfkJxd6Desc;
	}

	/**
	 * @param pfkJxd6Grade1
	 */
	public void setPfkJxd6Grade1(Integer pfkJxd6Grade1) {
		this.pfkJxd6Grade1 = pfkJxd6Grade1;
	}

	/**
	 * @return pfkJxd6Grade1
	 */
	public Integer getPfkJxd6Grade1() {
		return this.pfkJxd6Grade1;
	}

	/**
	 * @param pfkJxd6Grade2
	 */
	public void setPfkJxd6Grade2(Integer pfkJxd6Grade2) {
		this.pfkJxd6Grade2 = pfkJxd6Grade2;
	}

	/**
	 * @return pfkJxd6Grade2
	 */
	public Integer getPfkJxd6Grade2() {
		return this.pfkJxd6Grade2;
	}

	/**
	 * @param pfkJxd7Desc
	 */
	public void setPfkJxd7Desc(String pfkJxd7Desc) {
		this.pfkJxd7Desc = pfkJxd7Desc;
	}

	/**
	 * @return pfkJxd7Desc
	 */
	public String getPfkJxd7Desc() {
		return this.pfkJxd7Desc;
	}

	/**
	 * @param pfkJxd7Grade1
	 */
	public void setPfkJxd7Grade1(Integer pfkJxd7Grade1) {
		this.pfkJxd7Grade1 = pfkJxd7Grade1;
	}

	/**
	 * @return pfkJxd7Grade1
	 */
	public Integer getPfkJxd7Grade1() {
		return this.pfkJxd7Grade1;
	}

	/**
	 * @param pfkJxd7Grade2
	 */
	public void setPfkJxd7Grade2(Integer pfkJxd7Grade2) {
		this.pfkJxd7Grade2 = pfkJxd7Grade2;
	}

	/**
	 * @return pfkJxd7Grade2
	 */
	public Integer getPfkJxd7Grade2() {
		return this.pfkJxd7Grade2;
	}

	/**
	 * @param pfkJxd8Desc
	 */
	public void setPfkJxd8Desc(String pfkJxd8Desc) {
		this.pfkJxd8Desc = pfkJxd8Desc;
	}

	/**
	 * @return pfkJxd8Desc
	 */
	public String getPfkJxd8Desc() {
		return this.pfkJxd8Desc;
	}

	/**
	 * @param pfkJxd8Grade1
	 */
	public void setPfkJxd8Grade1(Integer pfkJxd8Grade1) {
		this.pfkJxd8Grade1 = pfkJxd8Grade1;
	}

	/**
	 * @return pfkJxd8Grade1
	 */
	public Integer getPfkJxd8Grade1() {
		return this.pfkJxd8Grade1;
	}

	/**
	 * @param pfkJxd8Grade2
	 */
	public void setPfkJxd8Grade2(Integer pfkJxd8Grade2) {
		this.pfkJxd8Grade2 = pfkJxd8Grade2;
	}

	/**
	 * @return pfkJxd8Grade2
	 */
	public Integer getPfkJxd8Grade2() {
		return this.pfkJxd8Grade2;
	}

	/**
	 * @param pfkJxd9Desc
	 */
	public void setPfkJxd9Desc(String pfkJxd9Desc) {
		this.pfkJxd9Desc = pfkJxd9Desc;
	}

	/**
	 * @return pfkJxd9Desc
	 */
	public String getPfkJxd9Desc() {
		return this.pfkJxd9Desc;
	}

	/**
	 * @param pfkJxd9Grade1
	 */
	public void setPfkJxd9Grade1(Integer pfkJxd9Grade1) {
		this.pfkJxd9Grade1 = pfkJxd9Grade1;
	}

	/**
	 * @return pfkJxd9Grade1
	 */
	public Integer getPfkJxd9Grade1() {
		return this.pfkJxd9Grade1;
	}

	/**
	 * @param pfkJxd9Grade2
	 */
	public void setPfkJxd9Grade2(Integer pfkJxd9Grade2) {
		this.pfkJxd9Grade2 = pfkJxd9Grade2;
	}

	/**
	 * @return pfkJxd9Grade2
	 */
	public Integer getPfkJxd9Grade2() {
		return this.pfkJxd9Grade2;
	}

	/**
	 * @param pfkJxd10Desc
	 */
	public void setPfkJxd10Desc(String pfkJxd10Desc) {
		this.pfkJxd10Desc = pfkJxd10Desc;
	}

	/**
	 * @return pfkJxd10Desc
	 */
	public String getPfkJxd10Desc() {
		return this.pfkJxd10Desc;
	}

	/**
	 * @param pfkJxd10Grade1
	 */
	public void setPfkJxd10Grade1(Integer pfkJxd10Grade1) {
		this.pfkJxd10Grade1 = pfkJxd10Grade1;
	}

	/**
	 * @return pfkJxd10Grade1
	 */
	public Integer getPfkJxd10Grade1() {
		return this.pfkJxd10Grade1;
	}

	/**
	 * @param pfkJxd10Grade2
	 */
	public void setPfkJxd10Grade2(Integer pfkJxd10Grade2) {
		this.pfkJxd10Grade2 = pfkJxd10Grade2;
	}

	/**
	 * @return pfkJxd10Grade2
	 */
	public Integer getPfkJxd10Grade2() {
		return this.pfkJxd10Grade2;
	}

	/**
	 * @param pfkJxd11Desc
	 */
	public void setPfkJxd11Desc(String pfkJxd11Desc) {
		this.pfkJxd11Desc = pfkJxd11Desc;
	}

	/**
	 * @return pfkJxd11Desc
	 */
	public String getPfkJxd11Desc() {
		return this.pfkJxd11Desc;
	}

	/**
	 * @param pfkJxd11Grade1
	 */
	public void setPfkJxd11Grade1(Integer pfkJxd11Grade1) {
		this.pfkJxd11Grade1 = pfkJxd11Grade1;
	}

	/**
	 * @return pfkJxd11Grade1
	 */
	public Integer getPfkJxd11Grade1() {
		return this.pfkJxd11Grade1;
	}

	/**
	 * @param pfkJxd11Grade2
	 */
	public void setPfkJxd11Grade2(Integer pfkJxd11Grade2) {
		this.pfkJxd11Grade2 = pfkJxd11Grade2;
	}

	/**
	 * @return pfkJxd11Grade2
	 */
	public Integer getPfkJxd11Grade2() {
		return this.pfkJxd11Grade2;
	}

	/**
	 * @param pfkJxd12Desc
	 */
	public void setPfkJxd12Desc(String pfkJxd12Desc) {
		this.pfkJxd12Desc = pfkJxd12Desc;
	}

	/**
	 * @return pfkJxd12Desc
	 */
	public String getPfkJxd12Desc() {
		return this.pfkJxd12Desc;
	}

	/**
	 * @param pfkJxd12Grade1
	 */
	public void setPfkJxd12Grade1(Integer pfkJxd12Grade1) {
		this.pfkJxd12Grade1 = pfkJxd12Grade1;
	}

	/**
	 * @return pfkJxd12Grade1
	 */
	public Integer getPfkJxd12Grade1() {
		return this.pfkJxd12Grade1;
	}

	/**
	 * @param pfkJxd12Grade2
	 */
	public void setPfkJxd12Grade2(Integer pfkJxd12Grade2) {
		this.pfkJxd12Grade2 = pfkJxd12Grade2;
	}

	/**
	 * @return pfkJxd12Grade2
	 */
	public Integer getPfkJxd12Grade2() {
		return this.pfkJxd12Grade2;
	}

	/**
	 * @param pfkJxd13Desc
	 */
	public void setPfkJxd13Desc(String pfkJxd13Desc) {
		this.pfkJxd13Desc = pfkJxd13Desc;
	}

	/**
	 * @return pfkJxd13Desc
	 */
	public String getPfkJxd13Desc() {
		return this.pfkJxd13Desc;
	}

	/**
	 * @param pfkJxd13Grade1
	 */
	public void setPfkJxd13Grade1(Integer pfkJxd13Grade1) {
		this.pfkJxd13Grade1 = pfkJxd13Grade1;
	}

	/**
	 * @return pfkJxd13Grade1
	 */
	public Integer getPfkJxd13Grade1() {
		return this.pfkJxd13Grade1;
	}

	/**
	 * @param pfkJxd13Grade2
	 */
	public void setPfkJxd13Grade2(Integer pfkJxd13Grade2) {
		this.pfkJxd13Grade2 = pfkJxd13Grade2;
	}

	/**
	 * @return pfkJxd13Grade2
	 */
	public Integer getPfkJxd13Grade2() {
		return this.pfkJxd13Grade2;
	}

	/**
	 * @param pfkJxd14Desc
	 */
	public void setPfkJxd14Desc(String pfkJxd14Desc) {
		this.pfkJxd14Desc = pfkJxd14Desc;
	}

	/**
	 * @return pfkJxd14Desc
	 */
	public String getPfkJxd14Desc() {
		return this.pfkJxd14Desc;
	}

	/**
	 * @param pfkJxd14Grade1
	 */
	public void setPfkJxd14Grade1(Integer pfkJxd14Grade1) {
		this.pfkJxd14Grade1 = pfkJxd14Grade1;
	}

	/**
	 * @return pfkJxd14Grade1
	 */
	public Integer getPfkJxd14Grade1() {
		return this.pfkJxd14Grade1;
	}

	/**
	 * @param pfkJxd14Grade2
	 */
	public void setPfkJxd14Grade2(Integer pfkJxd14Grade2) {
		this.pfkJxd14Grade2 = pfkJxd14Grade2;
	}

	/**
	 * @return pfkJxd14Grade2
	 */
	public Integer getPfkJxd14Grade2() {
		return this.pfkJxd14Grade2;
	}

	/**
	 * @param focusJxd1
	 */
	public void setFocusJxd1(String focusJxd1) {
		this.focusJxd1 = focusJxd1;
	}

	/**
	 * @return focusJxd1
	 */
	public String getFocusJxd1() {
		return this.focusJxd1;
	}

	/**
	 * @param focusJxd2
	 */
	public void setFocusJxd2(String focusJxd2) {
		this.focusJxd2 = focusJxd2;
	}

	/**
	 * @return focusJxd2
	 */
	public String getFocusJxd2() {
		return this.focusJxd2;
	}

	/**
	 * @param focusJxd3
	 */
	public void setFocusJxd3(String focusJxd3) {
		this.focusJxd3 = focusJxd3;
	}

	/**
	 * @return focusJxd3
	 */
	public String getFocusJxd3() {
		return this.focusJxd3;
	}

	/**
	 * @param focusJxd4
	 */
	public void setFocusJxd4(String focusJxd4) {
		this.focusJxd4 = focusJxd4;
	}

	/**
	 * @return focusJxd4
	 */
	public String getFocusJxd4() {
		return this.focusJxd4;
	}

	/**
	 * @param focusJxd5
	 */
	public void setFocusJxd5(String focusJxd5) {
		this.focusJxd5 = focusJxd5;
	}

	/**
	 * @return focusJxd5
	 */
	public String getFocusJxd5() {
		return this.focusJxd5;
	}

	/**
	 * @param focusJxd6
	 */
	public void setFocusJxd6(String focusJxd6) {
		this.focusJxd6 = focusJxd6;
	}

	/**
	 * @return focusJxd6
	 */
	public String getFocusJxd6() {
		return this.focusJxd6;
	}

	/**
	 * @param focusJxd7
	 */
	public void setFocusJxd7(String focusJxd7) {
		this.focusJxd7 = focusJxd7;
	}

	/**
	 * @return focusJxd7
	 */
	public String getFocusJxd7() {
		return this.focusJxd7;
	}

	/**
	 * @param focusJxd8
	 */
	public void setFocusJxd8(String focusJxd8) {
		this.focusJxd8 = focusJxd8;
	}

	/**
	 * @return focusJxd8
	 */
	public String getFocusJxd8() {
		return this.focusJxd8;
	}

	/**
	 * @param focusJxd9
	 */
	public void setFocusJxd9(String focusJxd9) {
		this.focusJxd9 = focusJxd9;
	}

	/**
	 * @return focusJxd9
	 */
	public String getFocusJxd9() {
		return this.focusJxd9;
	}

	/**
	 * @param focusJxd10
	 */
	public void setFocusJxd10(String focusJxd10) {
		this.focusJxd10 = focusJxd10;
	}

	/**
	 * @return focusJxd10
	 */
	public String getFocusJxd10() {
		return this.focusJxd10;
	}

	/**
	 * @param focusJxd11
	 */
	public void setFocusJxd11(String focusJxd11) {
		this.focusJxd11 = focusJxd11;
	}

	/**
	 * @return focusJxd11
	 */
	public String getFocusJxd11() {
		return this.focusJxd11;
	}

	/**
	 * @param focusJxd12
	 */
	public void setFocusJxd12(String focusJxd12) {
		this.focusJxd12 = focusJxd12;
	}

	/**
	 * @return focusJxd12
	 */
	public String getFocusJxd12() {
		return this.focusJxd12;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}