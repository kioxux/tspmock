package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrContImageAuditApp;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param
 * @return 
 * @author shenli
 * @date 2021/6/7 20:49
 * @version 1.0.0
 * @desc 零售线上提款审批流
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service  //将实现类注入spring容器管理
public class LSYW05BizService implements ClientBizInterface {
    private final Logger logger = LoggerFactory.getLogger(LSYW05BizService.class);

    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;
    @Autowired
    private BGYW01BizService bgyw01Bizservice;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07Bizservice;
    @Autowired
    private BGYW09BizService bgyw09Bizservice;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private  CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private BizCommonService bizCommonService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        String currentOpType = resultInstanceDto.getCurrentOpType();
        logger.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        logger.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if("LS007".equals(bizType)){
            handlerLS007(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG027.equals(bizType)){
            //BG027展期协议签订审核（零售）
            bgyw02BizService.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG030.equals(bizType)){
            //BG030担保变更协议签订审核（零售）
            bgyw03Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG033.equals(bizType)){
            //BG033利率变更协议签订审核（零售）
            bgyw01Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG036.equals(bizType)){
            //BG036还款计划变更协议签订审核（零售）
            bgyw07Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG039.equals(bizType)){
            //BG039延期还款协议签订审核（零售）
            bgyw09Bizservice.bizOp(resultInstanceDto);
        }
        logger.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }


    //零售线上提款启用
    private void handlerLS007(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String grtSerno = resultInstanceDto.getBizId();
        String nodeId = resultInstanceDto.getNodeId();
        CtrContImageAuditApp ctrContImageAuditApp = null;
        try {
            ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(grtSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程发起操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程提交操作，流程参数【{}】", grtSerno, resultInstanceDto);
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
                if("229_5".equals(nodeId)){
                    cmisBizXwCommonService.sendYKTask(grtSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
                }
                // 资料未全生成影像补扫任务
                // logger.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,grtSerno);
                // ctrContImageAuditAppService.createImageSpplInfo(grtSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程跳转操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程结束操作，流程参数【{}】", grtSerno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                createCentralFileTask(grtSerno,resultInstanceDto);
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
                // 资料未全生成影像补扫任务
                logger.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,grtSerno);
                ctrContImageAuditAppService.createImageSpplInfo(grtSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"零售线上提款启用");
                sendImage(resultInstanceDto);
                if("229_5".equals(nodeId)){
                    cmisBizXwCommonService.sendYKTask(grtSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
                }
                sendMessage(resultInstanceDto,"通过");
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程退回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程打回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    //针对流程到办结节点，进行以下处理
                    ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    ctrContImageAuditAppService.update(ctrContImageAuditApp);
                    sendMessage(resultInstanceDto,"退回");
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程拿回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("线上提款启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("线上提款启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditApp.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                ctrContImageAuditAppService.update(ctrContImageAuditApp);
            } else {
                logger.warn("担保变更申请" + grtSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("后业务处理失败", e);
            try {
                logger.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
                logger.info("发送异常消息开始结束");
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }


    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.LSYW05.equals(flowCode);
    }

    public void createCentralFileTask (String  extSerno,ResultInstanceDto resultInstanceDto){
        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(extSerno);
        if(!"10".equals(ctrContImageAuditApp.getGuarMode())){

            try {
                logger.info("线上提款启用："+"流水号："+extSerno+"-归档开始");
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(ctrContImageAuditApp.getContNo());
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(ctrContImageAuditApp.getInputBrId());
                String orgType = resultDto.getData().getOrgType();
                //        0-总行部室
                //        1-异地支行（有分行）
                //        2-异地支行（无分行）
                //        3-异地分行
                //        4-中心支行
                //        5-综合支行
                //        6-对公支行
                //        7-零售支行
                if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) &&!"8".equals(orgType) && !"9".equals(orgType) && !"A".equals(orgType)){
                    //新增临时档案任务
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(ctrLoanCont.getIqpSerno());
                    centralFileTaskdto.setTraceId(ctrLoanCont.getContNo());
                    centralFileTaskdto.setCusId(ctrContImageAuditApp.getCusId());
                    centralFileTaskdto.setCusName(ctrContImageAuditApp.getCusName());
                    centralFileTaskdto.setBizType("LS007"); // LS007	零售线上提款启用
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    centralFileTaskdto.setInputId(ctrContImageAuditApp.getInputId());
                    centralFileTaskdto.setInputBrId(ctrContImageAuditApp.getInputBrId());
                    centralFileTaskdto.setOptType("01"); // 纯指令
                    centralFileTaskdto.setTaskType("02"); // 02	档案暂存
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                    logger.info("线上提款启用："+"流水号："+extSerno+"-归档成功");
                }
                logger.info("线上提款启用："+"流水号："+extSerno+"-归档结束");
            }catch (Exception e){
                logger.info("线上提款启用："+extSerno+"-归档异常------------------"+e);
            }
        }
    }

    /**
     * 发送短信
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void sendMessage(ResultInstanceDto resultInstanceDto,String result) {
        String serno = resultInstanceDto.getBizId();
        logger.info("线上提款启用："+"流水号："+serno+"-发送短信开始------------------");
        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(serno);
        String managerId = ctrContImageAuditApp.getManagerId();
        try {
            String mgrTel = "";
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }

            if (!StringUtils.isBlank(mgrTel)) {
                String messageType = "MSG_LS_M_0001";// 短信编号
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", resultInstanceDto.getBizUserName());
                paramMap.put("flowName", "零售线上提款启用审核");
                paramMap.put("result", result);

                //执行发送借款人操作
                logger.info("线上提款启用："+"流水号："+resultInstanceDto.getBizId()+"-发送短信------------------");
                ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            }
        } catch (Exception e) {
            logger.info("线上提款启用："+"流水号："+serno+"-发送短信失败------------------"+e);
        }
        logger.info("线上提款启用："+"流水号："+serno+"-发送短信结束------------------");
    }

    /**
     * 推送影像审批信息
     *
     * @author shenli
     * @date 2021-9-29 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {

        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();

            if ("229_5".equals(nodeId)){//集中作业零售合同审批岗
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }

        if(approveUserId.length() > 0){
            approveUserId = approveUserId.substring(1);
            Map<String, Object> params = new HashMap<>();
            params = resultInstanceDto.getParam();
            String contNo = (String) params.get("contNo");
            bizCommonService.sendImage(contNo,"GRXFDKCZJB;GRXFDKCZDY;GRXFDKCZZY;GRXFDKCZBZDB",approveUserId);
        }
    }
}
