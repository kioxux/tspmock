package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 住房按揭放款申请征信报告校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0034Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0034Service.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private LmtCobInfoService lmtCobInfoService;
    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    public RiskResultDto riskItem0034(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString(); // 流程编码
        log.info("*************征信报告校验开始***********【{}】", pvpSerno);
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        if ("LS005".equals(bizType) || "LS006".equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            // 零售放款进入
            User userInfo = SessionUtils.getUserInformation();
            String orgCode = userInfo.getOrg().getCode();
            if (orgCode.startsWith("80") || orgCode.startsWith("81") || (!orgCode.startsWith("01") // 张家港本地
                    && !orgCode.startsWith("12")  // 苏州分行，吴江支行
                    && !orgCode.startsWith("09")   // 常熟支行
                    && !orgCode.startsWith("10"))) { //昆山支行
                log.info("*************征信报告校验当前登录人机构号为：【{}】为寿光村镇银行***********【{}】", userInfo.getOrg().getCode());
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                log.info("*************征信报告校验结束***********【{}】", pvpSerno);
                return riskResultDto;
            }
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(pvpLoanApp.getIqpSerno());
            //征信校验
            /// 住房按揭放款申请征信报告校验只针对苏州地区，异地不强控   2021年8月26日14:57:37  hubp
            if (this.isToday(iqpLoanApp.getInputDate()) && "022001,022002,022040,022052,022053,022055".indexOf(pvpLoanApp.getPrdId()) != -1) {
                // 022001-个人一手住房按揭贷款 022002-个人二手住房按揭贷款 022040-个人二手住房按揭贷款（资金托管）022052-个人一手住房按揭贷款（常熟资金监管）
                //022053-个人二手住房按揭贷款（连云港资金托管） 022055-个人一手住房按揭贷款（宿迁资金监管）
                RiskResultDto riskResult =  iscredit(pvpLoanApp.getIqpSerno(),iqpLoanApp.getCertCode());
                if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult.getRiskResultType())) {
                    return riskResult;
                }
                // 开始查找共借人信息
                List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(pvpLoanApp.getIqpSerno());
                log.info("*************查询到共借人数量***********：【{}】", lmtCobInfoList.size());
                if (lmtCobInfoList.size() > 0) {
                    for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                        riskResult = iscredit(pvpLoanApp.getIqpSerno(), lmtCobInfo.getCommonDebitCertCode());
                        if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult.getRiskResultType())) {
                            return riskResult;
                        }
                    }
                }
            }
        } else if ("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)) {
            // 零售业务申请进入
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo.getOrg().getCode().startsWith("80") ) {// 寿光的不需要验证征信
                log.info("*************征信报告校验登当前录人机构号为：【{}】为寿光村镇银行***********【{}】", userInfo.getOrg().getCode());
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                log.info("*************征信报告校验结束***********【{}】", pvpSerno);
                return riskResultDto;
            }
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(pvpSerno);
            // 零售授信校验授信业务提交日期与征信报告生成日期超过一个月则进行拦截提示
            // 2021年9月10日14:03:37 hubp ,BUG 10910 修改征信判断逻辑
            // 根据主借款人信息去查询是否存在征信
            // 2021年10月21日22:12:40 hubp BUG 15262 修改征信判断逻辑 存单质押直接通过
            if(CmisCommonConstants.GUAR_MODE_20.equals(iqpLoanApp.getGuarWay()) && guarBaseInfoService.getBaseInfo(iqpLoanApp.getIqpSerno()) > 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                log.info("*************征信报告校验结束-- 存单质押直接通过***********【{}】", pvpSerno);
                return riskResultDto;
            } else {
                RiskResultDto riskResult = iscredit(pvpSerno, iqpLoanApp.getCertCode(), iqpLoanApp.getUpdateTime());
                if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult.getRiskResultType())) {
                    return riskResult;
                }
                // 开始查找共借人信息
                List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(pvpSerno);
                log.info("*************查询到共借人数量***********：【{}】", lmtCobInfoList.size());
                if (lmtCobInfoList.size() > 0) {
                    for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                        riskResult = iscredit(pvpSerno, lmtCobInfo.getCommonDebitCertCode(), iqpLoanApp.getUpdateTime());
                        if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult.getRiskResultType())) {
                            return riskResult;
                        }
                    }
                }
            }
        } else if ("XW001".equals(bizType) || "XW004".equals(bizType) || "XW005".equals(bizType)
                || "XW006".equals(bizType) || "XW007".equals(bizType) || "XW008".equals(bizType)) {
            // 小微授信进入
            LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(pvpSerno);
            if(Objects.isNull(mainInfo)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("未获取到调查信息！"); //流程编号获取失败
                return riskResultDto;
            }else {
                RiskResultDto riskResult = iscredit(pvpSerno, mainInfo.getCertCode(), mainInfo.getUpdateTime());
                if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult.getRiskResultType())) {
                    return riskResult;
                }
            }
            // 开始查找共借人信息
            // 2021年11月15日11:56:23 hubp 与小微业务确认，只有优企贷的共借人需要查询征信，因此加入控制
            if("XW008".equals(bizType)){
                List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(pvpSerno);
                log.info("*************查询到共借人数量***********：【{}】", lmtCobInfoList.size());
                if (lmtCobInfoList.size() > 0) {
                    for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                        RiskResultDto riskResult1 = iscredit(pvpSerno, lmtCobInfo.getCommonDebitCertCode(), mainInfo.getUpdateTime());
                        if (CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResult1.getRiskResultType())) {
                            return riskResult1;
                        }
                    }
                }
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //流程编号获取失败
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************征信报告校验结束***********【{}】", pvpSerno);
        return riskResultDto;
    }

    /**
     * @param date
     * @return boolean
     * @author hubp
     * @date 2021/8/26 15:11
     * @version 1.0.0
     * @desc 判断申请时间是否为今天
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public boolean isToday(String date) {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        if (openday.compareTo(date) == 0) {
            log.info("*************RiskItem0034征信报告校验，申请时间为今天***********");
            return false;
        }
        log.info("*************RiskItem0034征信报告校验，申请时间不为今天***********");
        return true;
    }

    /**
     * @param serno, certCode
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/10 14:55
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto iscredit(String serno, String certCode, Date updateTime) {
        RiskResultDto riskResultDto = new RiskResultDto();
        QueryModel creditModel = new QueryModel();
        creditModel.addCondition("bizSerno", serno);
        creditModel.addCondition("certCode", certCode);
        // 新增排序
        creditModel.setSort("reportCreateTime desc");
        List<CreditReportQryLstDto> creditReportQryLstDtoList = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(creditModel);
        if (creditReportQryLstDtoList.size() == 0) {
            log.info("*************征信报告校验结束，creditReportQryLstDtoList数量***********【{}】", creditReportQryLstDtoList.size());
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03403);
            return riskResultDto;
        } else {
            CreditReportQryLstDto creditReportQryLstDto = creditReportQryLstDtoList.get(0);
            if (!"003".equals(creditReportQryLstDto.getQryStatus())) {
                log.info("*************征信报告校验结束，校验失败***********【{}】", serno);
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03403); // 存在主借款人或共同借款人征信未查询
                return riskResultDto;
            } else {
                String creditReportDateTemp = DateUtils.addMonth(creditReportQryLstDto.getReportCreateTime().substring(0, 10), "yyyy-MM-dd", 1);
                Date creditReportDate = DateUtils.parseDate(creditReportDateTemp,"yyyy-MM-dd");
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                Date opendayDate = DateUtils.parseDate(openday,"yyyy-MM-dd");
                log.info("征信生成日期:【{}】，当前系统日期：【{}】", creditReportDateTemp, openday);
                if ( creditReportDate.compareTo(opendayDate)< 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03405);
                    return riskResultDto;
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param serno, certCode
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/15 14:28
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto iscredit(String serno, String certCode) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        QueryModel model = new QueryModel();
        model.addCondition("bizSerno", serno);
        model.addCondition("certCode", certCode);
        model.setSort("reportCreateTime desc");
        log.info("*************放款征信报告校验，证件号码：【{}】，流水号：【{}】", certCode,serno);
        List<CreditReportQryLstDto> creditReportQryLstDtoList = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(model);
        log.info("*************放款征信报告校验，证件号码：【{}】，流水号：【{}】,征信信息：【{}】", certCode,serno, JSON.toJSONString(creditReportQryLstDtoList));
        if (creditReportQryLstDtoList.size() > 0) {
            // 取最新的借款人征信
            CreditReportQryLstDto creditReportQryLstDto = creditReportQryLstDtoList.get(0);
            log.info("*************放款征信报告校验，当前日期：【{}】，征信日期：【{}】", openday,creditReportQryLstDto.getReportCreateTime());
            if (!openday.equals(creditReportQryLstDto.getReportCreateTime().substring(0, 10))) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03401); //请联系公司金融总部
                return riskResultDto;
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03403); //请联系公司金融总部
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
