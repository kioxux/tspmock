package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtPlListInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.domain.LmtSurveyReportOtherInfo;
import cn.com.yusys.yusp.dto.LmtPlListInfoReqDto;
import cn.com.yusys.yusp.repository.mapper.LmtPlListInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPlListInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-05-10 15:10:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtPlListInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private LmtPlListInfoMapper lmtPlListInfoMapper;

    @Autowired
    private LmtSurveyReportOtherInfoService lmtSurveyReportOtherInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtPlListInfo selectByPrimaryKey(String pkId) {
        return lmtPlListInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtPlListInfo> selectAll(QueryModel model) {
        List<LmtPlListInfo> records = (List<LmtPlListInfo>) lmtPlListInfoMapper.selectByModel(model);
        return records;
    }


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtPlListInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtPlListInfo> list = lmtPlListInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtPlListInfo record) {
        return lmtPlListInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtPlListInfo record) {
        return lmtPlListInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtPlListInfo record) {
        return lmtPlListInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtPlListInfo record) {
        return lmtPlListInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtPlListInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtPlListInfoMapper.deleteByIds(ids);
    }

    /**
     * @param lmtSurveyReportDto
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtPlListInfo>
     * @author hubp
     * @date 2021/5/18 16:32
     * @version 1.0.0
     * @desc 通过调查流水号查询损益明细信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly = true)
    public ResultDto selectBySurveySerno(LmtSurveyReportDto lmtSurveyReportDto) {
        LmtPlListInfoReqDto lmtPlListInfoReqDto = new LmtPlListInfoReqDto();
        String surveySerno = lmtSurveyReportDto.getSurveySerno();
        if (StringUtils.isBlank(surveySerno)) {
            return new ResultDto(null).code(9999).message("流水号不可为空");
        }

        List<LmtPlListInfo> list = lmtPlListInfoMapper.selectBySurveySerno(surveySerno);
        if (list.size() > 0) {
            lmtPlListInfoReqDto.setList(list);
        }

        LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo = lmtSurveyReportOtherInfoService.selectByPrimaryKey(surveySerno);
        if (null != lmtSurveyReportOtherInfo) {
            lmtPlListInfoReqDto.setOtherInfo(lmtSurveyReportOtherInfo);
        }
        return new ResultDto(lmtPlListInfoReqDto);
    }

    /**
     * @author hubp
     * @date 2021/5/18 19:48
     * @version 1.0.0
     * @desc 保存更新损益表相关信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveData(LmtPlListInfoReqDto lmtPlListInfoReqDto) {
        logger.info("************开始更新损益表相关信息，传入参数为：【{}】********" , JSON.toJSONString(lmtPlListInfoReqDto));
        int result = -1;
        List<LmtPlListInfo> list = null;
        LmtSurveyReportOtherInfo otherInfo = null;
        try {
            otherInfo = lmtPlListInfoReqDto.getOtherInfo();
            list = lmtPlListInfoReqDto.getList();
            if (Objects.nonNull(otherInfo) && StringUtils.nonBlank(otherInfo.getSurveySerno())) {
                if (null == lmtSurveyReportOtherInfoService.selectByPrimaryKey(otherInfo.getSurveySerno())) {
                    result = lmtSurveyReportOtherInfoService.insert(otherInfo);
                } else {
                    result = lmtSurveyReportOtherInfoService.updateSelective(otherInfo);
                }
            }
            if (null == list || list.size() == 0) {
                return result;
            } else {
                lmtPlListInfoReqDto.getList().forEach(lmtPlListInfo -> {
                    logger.info("开始更新信息，流水号为：.............." + lmtPlListInfo.getSurveySerno());
                    lmtPlListInfoMapper.updateByPrimaryKeySelective(lmtPlListInfo);
                });
            }
            result = 1;
        } catch (Exception e) {
            logger.error("************更新损益表相关信息异常，异常参数为：【{}】********" , JSON.toJSONString(e));
            throw e;
        } finally {
            logger.info("************更新损益表相关信息结束，返回参数为：【{}】********" , result);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public List<LmtPlListInfo> selectBylmtPlListInfoMap(Map lmtPlListInfoMap) {
        List<LmtPlListInfo> records = (List<LmtPlListInfo>) lmtPlListInfoMapper.selectBylmtPlListInfoMap(lmtPlListInfoMap);
        return records;
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/10/20 16:15
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly = true)
    public List<LmtPlListInfo> selectBySerno(String surveySerno) {
        return lmtPlListInfoMapper.selectBySurveySerno(surveySerno);
    }
}
