package cn.com.yusys.yusp.service.server.xdxw0068;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.req.Xdxw0068DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0068.resp.Xdxw0068DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0068Service {

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdxw0068.Xdxw0068Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 调查基本信息查询
     *
     * @param xdxw0068DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0068DataRespDto getComSurveyReportList(Xdxw0068DataReqDto xdxw0068DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataReqDto));
        Xdxw0068DataRespDto xdxw0068DataRespDto = new Xdxw0068DataRespDto();
        try {
            //查询优农批复信息
            java.util.List<List> result = lmtSurveyReportMainInfoMapper.xdxw0068(xdxw0068DataReqDto);
            //查询优农贷名单信息
            CmisCus0025ReqDto cmisCus0025ReqDto = new CmisCus0025ReqDto();
            cmisCus0025ReqDto.setCertCode(xdxw0068DataReqDto.getCert_code());
            cmisCus0025ReqDto.setCusName(xdxw0068DataReqDto.getPrd_name());
            cmisCus0025ReqDto.setSerno(xdxw0068DataReqDto.getSurvey_serno());
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025ReqDto));
            ResultDto<CmisCus0025RespDto> resultDto = cmisCusClientService.cmiscus0025(cmisCus0025ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(resultDto));
            //
            CmisCus0025RespDto cmisCus0025RespDto = new CmisCus0025RespDto();
            if (resultDto != null && CmisBizConstants.NUM_ZERO.equals(resultDto.getCode())) {
                cmisCus0025RespDto = resultDto.getData();
            }
            String isFarm = cmisCus0025RespDto.getAgriflag();
            String is_good_business = cmisCus0025RespDto.getCusOperIsNormal();
            String cus_list_serno = cmisCus0025RespDto.getSerno();
            //是否为农户\是否正常经营
            result = result.parallelStream().map(ret -> {
                cn.com.yusys.yusp.dto.server.xdxw0068.resp.List temp = new cn.com.yusys.yusp.dto.server.xdxw0068.resp.List();
                BeanUtils.copyProperties(ret, temp);
                //是否为农户
                temp.setIs_farm(isFarm);
                //是否正常经营
                temp.setIs_good_business(is_good_business);
                //名单流水
                temp.setCus_list_serno(cus_list_serno);
                //担保方式码值转换
                String guarType = temp.getAssure_means();//参数值
                String guarTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY", guarType);
                temp.setAssure_means_name(guarTypeName);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            //返回
            xdxw0068DataRespDto.setList(result);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value);

        return xdxw0068DataRespDto;
    }
}

