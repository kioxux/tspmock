/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpAppr
 * @类描述: lmt_grp_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-09 11:11:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_appr")
public class LmtGrpAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 集团审批流水号 **/
	@Column(name = "GRP_APPROVE_SERNO", unique = false, nullable = true, length = 40)
	private String grpApproveSerno;
	
	/** 集团申请流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;
	
	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;
	
	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;
	
	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 敞口额度合计 **/
	@Column(name = "OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openTotalLmtAmt;
	
	/** 低风险额度合计 **/
	@Column(name = "LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskTotalLmtAmt;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 核查报告模式 **/
	@Column(name = "INDGT_REPORT_MODE", unique = false, nullable = true, length = 5)
	private String indgtReportMode;
	
	/** 核查报告路径 **/
	@Column(name = "INDGT_REPORT_PATH", unique = false, nullable = true, length = 1000)
	private String indgtReportPath;
	
	/** 综合评价 **/
	@Column(name = "INTE_EVLU", unique = false, nullable = true, length = 65535)
	private String inteEvlu;
	
	/** 授信限额情况 **/
	@Column(name = "LMT_QUOTA_SITU", unique = false, nullable = true, length = 2000)
	private String lmtQuotaSitu;
	
	/** 风控建议 **/
	@Column(name = "RISK_ADVICE", unique = false, nullable = true, length = 2000)
	private String riskAdvice;
	
	/** 风险因素 **/
	@Column(name = "RISK_FACTOR", unique = false, nullable = true, length = 65535)
	private String riskFactor;
	
	/** 结论性描述 **/
	@Column(name = "REST_DESC", unique = false, nullable = true, length = 2000)
	private String restDesc;
	
	/** 用信审核方式 **/
	@Column(name = "LOAN_APPR_MODE", unique = false, nullable = true, length = 5)
	private String loanApprMode;
	
	/** 是否超限额管理要求 **/
	@Column(name = "IS_OUT_LMT_QUOTA_MANA", unique = false, nullable = true, length = 5)
	private String isOutLmtQuotaMana;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 2000)
	private String otherDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 贷后管理要求 **/
	@Column(name = "PSP_MANA_NEED", unique = false, nullable = true, length = 4000)
	private String pspManaNeed;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 审查内容 **/
	@Column(name = "REVIEW_CONTENT", unique = false, nullable = true, length = 65535)
	private String reviewContent;
	
	/** 授信用途合理性分析 **/
	@Column(name = "ANALYSIS_RATIONAL_LMT_USE", unique = false, nullable = true, length = 65535)
	private String analysisRationalLmtUse;
	
	/** 评审结论 **/
	@Column(name = "REVIEW_CONCLUSION", unique = false, nullable = true, length = 65535)
	private String reviewConclusion;
	
	/** 是否重组贷款 **/
	@Column(name = "IS_RESTRU_LOAN", unique = false, nullable = true, length = 5)
	private String isRestruLoan;
	
	/** 出具报告类型 **/
	@Column(name = "ISSUE_REPORT_TYPE", unique = false, nullable = true, length = 5)
	private String issueReportType;
	
	/** 公司是否提交其他部门审核 **/
	@Column(name = "IS_SUB_TO_OTHER_DEPT_COM", unique = false, nullable = true, length = 5)
	private String isSubToOtherDeptCom;
	
	/** 公司提交部门 **/
	@Column(name = "SUB_OTHER_DEPT_COM", unique = false, nullable = true, length = 20)
	private String subOtherDeptCom;
	
	/** 信贷是否提交其他部门审核 **/
	@Column(name = "IS_SUB_OTHER_DEPT_XD", unique = false, nullable = true, length = 5)
	private String isSubOtherDeptXd;
	
	/** 信贷提交部门 **/
	@Column(name = "SUB_OTHER_DEPT_XD", unique = false, nullable = true, length = 20)
	private String subOtherDeptXd;
	
	/** 是否大额授信 **/
	@Column(name = "IS_BIG_LMT", unique = false, nullable = true, length = 5)
	private String isBigLmt;
	
	/** 是否下调审批权限 **/
	@Column(name = "IS_LOWER_APPR_AUTH", unique = false, nullable = true, length = 5)
	private String isLowerApprAuth;
	
	/** 是否上调审批权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = true, length = 5)
	private String isUpperApprAuth;

	/** 上调审批权限类型 **/
	@Column(name = "UPPER_APPR_AUTH_TYPE", unique = false, nullable = true, length = 5)
	private String upperApprAuthType;

	/** 终审机构类型 **/
	@Column(name = "FINAL_APPR_BR_TYPE", unique = false, nullable = true, length = 5)
	private String finalApprBrType;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;

	/** 审批退回原因 **/
	@Column(name = "APPR_BACK_REASON_TYPE", unique = false, nullable = true, length = 5)
	private String apprBackReasonType;

	/** 保证担保其他事项说明  **/
	@Column(name = "GENERAL_ASSURE_ACC", unique = false, nullable = true)
	private String generalAssureAcc;

	/** 抵质押担保其他事项说明  **/
	@Column(name = "GUAR_ASSURE_ACC", unique = false, nullable = true)
	private String guarAssureAcc;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param grpApproveSerno
	 */
	public void setGrpApproveSerno(String grpApproveSerno) {
		this.grpApproveSerno = grpApproveSerno;
	}
	
    /**
     * @return grpApproveSerno
     */
	public String getGrpApproveSerno() {
		return this.grpApproveSerno;
	}
	
	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}
	
    /**
     * @return grpSerno
     */
	public String getGrpSerno() {
		return this.grpSerno;
	}
	
	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}
	
    /**
     * @return grpCusId
     */
	public String getGrpCusId() {
		return this.grpCusId;
	}
	
	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}
	
    /**
     * @return grpCusName
     */
	public String getGrpCusName() {
		return this.grpCusName;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}
	
    /**
     * @return origiLmtReplySerno
     */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param openTotalLmtAmt
	 */
	public void setOpenTotalLmtAmt(java.math.BigDecimal openTotalLmtAmt) {
		this.openTotalLmtAmt = openTotalLmtAmt;
	}
	
    /**
     * @return openTotalLmtAmt
     */
	public java.math.BigDecimal getOpenTotalLmtAmt() {
		return this.openTotalLmtAmt;
	}
	
	/**
	 * @param lowRiskTotalLmtAmt
	 */
	public void setLowRiskTotalLmtAmt(java.math.BigDecimal lowRiskTotalLmtAmt) {
		this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
	}
	
    /**
     * @return lowRiskTotalLmtAmt
     */
	public java.math.BigDecimal getLowRiskTotalLmtAmt() {
		return this.lowRiskTotalLmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param indgtReportMode
	 */
	public void setIndgtReportMode(String indgtReportMode) {
		this.indgtReportMode = indgtReportMode;
	}
	
    /**
     * @return indgtReportMode
     */
	public String getIndgtReportMode() {
		return this.indgtReportMode;
	}
	
	/**
	 * @param indgtReportPath
	 */
	public void setIndgtReportPath(String indgtReportPath) {
		this.indgtReportPath = indgtReportPath;
	}
	
    /**
     * @return indgtReportPath
     */
	public String getIndgtReportPath() {
		return this.indgtReportPath;
	}
	
	/**
	 * @param inteEvlu
	 */
	public void setInteEvlu(String inteEvlu) {
		this.inteEvlu = inteEvlu;
	}
	
    /**
     * @return inteEvlu
     */
	public String getInteEvlu() {
		return this.inteEvlu;
	}
	
	/**
	 * @param lmtQuotaSitu
	 */
	public void setLmtQuotaSitu(String lmtQuotaSitu) {
		this.lmtQuotaSitu = lmtQuotaSitu;
	}
	
    /**
     * @return lmtQuotaSitu
     */
	public String getLmtQuotaSitu() {
		return this.lmtQuotaSitu;
	}
	
	/**
	 * @param riskAdvice
	 */
	public void setRiskAdvice(String riskAdvice) {
		this.riskAdvice = riskAdvice;
	}
	
    /**
     * @return riskAdvice
     */
	public String getRiskAdvice() {
		return this.riskAdvice;
	}
	
	/**
	 * @param riskFactor
	 */
	public void setRiskFactor(String riskFactor) {
		this.riskFactor = riskFactor;
	}
	
    /**
     * @return riskFactor
     */
	public String getRiskFactor() {
		return this.riskFactor;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc;
	}
	
    /**
     * @return restDesc
     */
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param loanApprMode
	 */
	public void setLoanApprMode(String loanApprMode) {
		this.loanApprMode = loanApprMode;
	}
	
    /**
     * @return loanApprMode
     */
	public String getLoanApprMode() {
		return this.loanApprMode;
	}
	
	/**
	 * @param isOutLmtQuotaMana
	 */
	public void setIsOutLmtQuotaMana(String isOutLmtQuotaMana) {
		this.isOutLmtQuotaMana = isOutLmtQuotaMana;
	}
	
    /**
     * @return isOutLmtQuotaMana
     */
	public String getIsOutLmtQuotaMana() {
		return this.isOutLmtQuotaMana;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param pspManaNeed
	 */
	public void setPspManaNeed(String pspManaNeed) {
		this.pspManaNeed = pspManaNeed;
	}
	
    /**
     * @return pspManaNeed
     */
	public String getPspManaNeed() {
		return this.pspManaNeed;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param reviewContent
	 */
	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}
	
    /**
     * @return reviewContent
     */
	public String getReviewContent() {
		return this.reviewContent;
	}
	
	/**
	 * @param analysisRationalLmtUse
	 */
	public void setAnalysisRationalLmtUse(String analysisRationalLmtUse) {
		this.analysisRationalLmtUse = analysisRationalLmtUse;
	}
	
    /**
     * @return analysisRationalLmtUse
     */
	public String getAnalysisRationalLmtUse() {
		return this.analysisRationalLmtUse;
	}
	
	/**
	 * @param reviewConclusion
	 */
	public void setReviewConclusion(String reviewConclusion) {
		this.reviewConclusion = reviewConclusion;
	}
	
    /**
     * @return reviewConclusion
     */
	public String getReviewConclusion() {
		return this.reviewConclusion;
	}
	
	/**
	 * @param isRestruLoan
	 */
	public void setIsRestruLoan(String isRestruLoan) {
		this.isRestruLoan = isRestruLoan;
	}
	
    /**
     * @return isRestruLoan
     */
	public String getIsRestruLoan() {
		return this.isRestruLoan;
	}
	
	/**
	 * @param issueReportType
	 */
	public void setIssueReportType(String issueReportType) {
		this.issueReportType = issueReportType;
	}
	
    /**
     * @return issueReportType
     */
	public String getIssueReportType() {
		return this.issueReportType;
	}
	
	/**
	 * @param isSubToOtherDeptCom
	 */
	public void setIsSubToOtherDeptCom(String isSubToOtherDeptCom) {
		this.isSubToOtherDeptCom = isSubToOtherDeptCom;
	}
	
    /**
     * @return isSubToOtherDeptCom
     */
	public String getIsSubToOtherDeptCom() {
		return this.isSubToOtherDeptCom;
	}
	
	/**
	 * @param subOtherDeptCom
	 */
	public void setSubOtherDeptCom(String subOtherDeptCom) {
		this.subOtherDeptCom = subOtherDeptCom;
	}
	
    /**
     * @return subOtherDeptCom
     */
	public String getSubOtherDeptCom() {
		return this.subOtherDeptCom;
	}
	
	/**
	 * @param isSubOtherDeptXd
	 */
	public void setIsSubOtherDeptXd(String isSubOtherDeptXd) {
		this.isSubOtherDeptXd = isSubOtherDeptXd;
	}
	
    /**
     * @return isSubOtherDeptXd
     */
	public String getIsSubOtherDeptXd() {
		return this.isSubOtherDeptXd;
	}
	
	/**
	 * @param subOtherDeptXd
	 */
	public void setSubOtherDeptXd(String subOtherDeptXd) {
		this.subOtherDeptXd = subOtherDeptXd;
	}
	
    /**
     * @return subOtherDeptXd
     */
	public String getSubOtherDeptXd() {
		return this.subOtherDeptXd;
	}
	
	/**
	 * @param isBigLmt
	 */
	public void setIsBigLmt(String isBigLmt) {
		this.isBigLmt = isBigLmt;
	}
	
    /**
     * @return isBigLmt
     */
	public String getIsBigLmt() {
		return this.isBigLmt;
	}
	
	/**
	 * @param isLowerApprAuth
	 */
	public void setIsLowerApprAuth(String isLowerApprAuth) {
		this.isLowerApprAuth = isLowerApprAuth;
	}
	
    /**
     * @return isLowerApprAuth
     */
	public String getIsLowerApprAuth() {
		return this.isLowerApprAuth;
	}
	
	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) {
		this.isUpperApprAuth = isUpperApprAuth;
	}
	
    /**
     * @return isUpperApprAuth
     */
	public String getIsUpperApprAuth() {
		return this.isUpperApprAuth;
	}

	public String getUpperApprAuthType() {
		return upperApprAuthType;
	}

	public void setUpperApprAuthType(String upperApprAuthType) {
		this.upperApprAuthType = upperApprAuthType;
	}

	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}
	
    /**
     * @return finalApprBrType
     */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}

	/**
	 * @return apprBackReasonType
	 */
	public String getApprBackReasonType() {
		return apprBackReasonType;
	}


	/**
	 * @param apprBackReasonType
	 */
	public void setApprBackReasonType(String apprBackReasonType) {
		this.apprBackReasonType = apprBackReasonType;
	}

	public String getGeneralAssureAcc() {
		return generalAssureAcc;
	}

	public void setGeneralAssureAcc(String generalAssureAcc) {
		this.generalAssureAcc = generalAssureAcc;
	}

	public String getGuarAssureAcc() {
		return guarAssureAcc;
	}

	public void setGuarAssureAcc(String guarAssureAcc) {
		this.guarAssureAcc = guarAssureAcc;
	}
}