package cn.com.yusys.yusp.web.server.xdxw0056;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0056.req.Xdxw0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.resp.Xdxw0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0056.Xdxw0056Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:根据流水号取得优企贷信息的客户经理ID和客户经理名
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0056:根据流水号取得优企贷信息的客户经理ID和客户经理名")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0056Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0056Resource.class);

    @Autowired
    private Xdxw0056Service xdxw0056Service;

    /**
     * 交易码：xdxw0056
     * 交易描述：根据流水号取得优企贷信息的客户经理ID和客户经理名
     *
     * @param xdxw0056DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号取得优企贷信息的客户经理ID和客户经理名")
    @PostMapping("/xdxw0056")
    protected @ResponseBody
    ResultDto<Xdxw0056DataRespDto> xdxw0056(@Validated @RequestBody Xdxw0056DataReqDto xdxw0056DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataReqDto));
        Xdxw0056DataRespDto xdxw0056DataRespDto = new Xdxw0056DataRespDto();// 响应Dto:根据流水号取得优企贷信息的客户经理ID和客户经理名
        ResultDto<Xdxw0056DataRespDto> xdxw0056DataResultDto = new ResultDto<>();
        String serno = xdxw0056DataReqDto.getSerno();//流水号
        try {
            // 从xdxw0056DataReqDto获取业务值进行Xdxw0056Resource.java业务逻辑处理
            // 调用xdxw0056Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataReqDto));
            xdxw0056DataRespDto = Optional.ofNullable(xdxw0056Service.getManagerInfo(xdxw0056DataReqDto)).orElseGet(() -> {
                Xdxw0056DataRespDto temp = new Xdxw0056DataRespDto();
                temp.setCusId(StringUtils.EMPTY);// 客户号
                temp.setManagerId(StringUtils.EMPTY);// 客户经理ID
                temp.setManagerName(StringUtils.EMPTY);// 客户经理姓名
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataRespDto));

            // 封装xdxw0056DataResultDto中正确的返回码和返回信息
            xdxw0056DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0056DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, e.getMessage());
            // 封装xdxw0056DataResultDto中异常返回码和返回信息
            xdxw0056DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0056DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0056DataRespDto到xdxw0056DataResultDto中
        xdxw0056DataResultDto.setData(xdxw0056DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataResultDto));
        return xdxw0056DataResultDto;
    }
}
