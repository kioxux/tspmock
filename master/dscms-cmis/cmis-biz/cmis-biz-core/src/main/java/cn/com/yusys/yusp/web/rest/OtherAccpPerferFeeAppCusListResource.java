/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherAccpPerferFeeAppCusList;
import cn.com.yusys.yusp.service.OtherAccpPerferFeeAppCusListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherAccpPerferFeeAppCusListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-03 13:56:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otheraccpperferfeeappcuslist")
public class OtherAccpPerferFeeAppCusListResource {
    @Autowired
    private OtherAccpPerferFeeAppCusListService otherAccpPerferFeeAppCusListService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherAccpPerferFeeAppCusList>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherAccpPerferFeeAppCusList> list = otherAccpPerferFeeAppCusListService.selectAll(queryModel);
        return new ResultDto<List<OtherAccpPerferFeeAppCusList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<OtherAccpPerferFeeAppCusList>> index(@RequestBody Map<String,Object> map) {
        List<OtherAccpPerferFeeAppCusList> list = otherAccpPerferFeeAppCusListService.selectByModel(map);
        return new ResultDto<List<OtherAccpPerferFeeAppCusList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherAccpPerferFeeAppCusList> show(@PathVariable("serno") String serno) {
        OtherAccpPerferFeeAppCusList otherAccpPerferFeeAppCusList = otherAccpPerferFeeAppCusListService.selectByPrimaryKey(serno);
        return new ResultDto<OtherAccpPerferFeeAppCusList>(otherAccpPerferFeeAppCusList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherAccpPerferFeeAppCusList> create(@RequestBody OtherAccpPerferFeeAppCusList otherAccpPerferFeeAppCusList) throws URISyntaxException {
    	otherAccpPerferFeeAppCusListService.insert(otherAccpPerferFeeAppCusList);
        return new ResultDto<OtherAccpPerferFeeAppCusList>(otherAccpPerferFeeAppCusList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherAccpPerferFeeAppCusList otherAccpPerferFeeAppCusList) throws URISyntaxException {
        int result = otherAccpPerferFeeAppCusListService.update(otherAccpPerferFeeAppCusList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherAccpPerferFeeAppCusListService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherAccpPerferFeeAppCusListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
