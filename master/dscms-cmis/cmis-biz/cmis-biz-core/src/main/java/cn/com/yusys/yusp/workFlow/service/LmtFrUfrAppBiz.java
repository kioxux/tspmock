package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtFrUfrApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtFrUfrAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className LmtFrUfrAccRelBiz
 * @Description 额度冻结/解冻申请
 * @Date 2020/12/21 : 10:48
 */
@Service
public class LmtFrUfrAppBiz implements ClientBizInterface {
	
	private final Logger log = LoggerFactory.getLogger(LmtFrUfrAppBiz.class);//定义log
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	@Autowired
	private LmtFrUfrAppService lmtFrUfrAppService;
	
	@Override
	public void bizOp(ResultInstanceDto resultInstanceDto) {
		String currentOpType = resultInstanceDto.getCurrentOpType();
		String frUfrSerno = resultInstanceDto.getBizId();
		log.info("额度冻结/解冻申请"+frUfrSerno+"流程操作:" + currentOpType+"后业务处理");
		try {
			LmtFrUfrApp lmtFrUfrApp = lmtFrUfrAppService.selectByPrimaryKey(frUfrSerno);
			if (OpType.STRAT.equals(currentOpType)) {
				log.info("额度冻结/解冻申请"+frUfrSerno+"流程发起操作，流程参数：" + resultInstanceDto.toString());
			}else if (OpType.RUN.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "流程提交操作，流程参数：" + resultInstanceDto.toString());
                //申请主表中的流程审批状态修改为【审批中111】
                lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                //针对流程到办结节点，进行以下处理
                //1.更新额度冻结/解冻申请状态由审批中111 -> 审批通过 997；2、更新授信协议、额度台账信息
                lmtFrUfrAppService.handleBusinessDataAfterEnd(frUfrSerno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "退回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
				log.info("权证出入库申请"+frUfrSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
                }
			} else if (OpType.TACK_BACK.equals(currentOpType)) {
				log.info("额度冻结/解冻申请"+frUfrSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
                }
			} else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务

                lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("额度冻结/解冻申请" + frUfrSerno + "否决操作，流程参数：" + resultInstanceDto.toString());
                //调用流程拒绝业务处理
                lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                lmtFrUfrAppService.updateApproveStatus(lmtFrUfrApp);
            } else {
                log.warn("额度冻结/解冻申请" + frUfrSerno + "未知操作:" + resultInstanceDto);
            }
		}  catch (Exception e) {
			log.error("流程提交后业务处理失败", e);
			try {
				BizCommonUtils bizCommonUtils = new BizCommonUtils();
				bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
			} catch (Exception e1) {
				log.error("发送异常消息失败", e1);
			}
		}
	}
	
	/**
	 * 判断当前流程类型是否匹配
	 * @param resultInstanceDto
	 * @return
	 */
	@Override
	public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.BIZ_TYPE_LMT_FR_UFR_APP.equals(flowCode);
    }
	
	
}

