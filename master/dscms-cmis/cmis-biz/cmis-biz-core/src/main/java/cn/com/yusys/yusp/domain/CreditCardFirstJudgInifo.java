/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFirstJudgInifo
 * @类描述: credit_card_first_judg_inifo数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:45:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_card_first_judg_inifo")
public class CreditCardFirstJudgInifo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 是否有增信 **/
	@Column(name = "IS_HAVE_CREDIT_INCREASE", unique = false, nullable = true, length = 5)
	private String isHaveCreditIncrease;
	
	/** 退回原因 **/
	@Column(name = "RETURN_REASON", unique = false, nullable = true, length = 5)
	private String returnReason;
	
	/** 拒绝原因 **/
	@Column(name = "REFUSE_REASON", unique = false, nullable = true, length = 5)
	private String refuseReason;
	
	/** 补件多选 **/
	@Column(name = "MULTI_SELECT_PROVE", unique = false, nullable = true, length = 100)
	private String multiSelectProve;
	
	/** 初审备注 **/
	@Column(name = "FIRST_JUDG_REMARK", unique = false, nullable = true, length = 500)
	private String firstJudgRemark;
	
	/** 审批结论 **/
	@Column(name = "APPROVE_CONCLUSION", unique = false, nullable = true, length = 5)
	private String approveConclusion;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param isHaveCreditIncrease
	 */
	public void setIsHaveCreditIncrease(String isHaveCreditIncrease) {
		this.isHaveCreditIncrease = isHaveCreditIncrease;
	}
	
    /**
     * @return isHaveCreditIncrease
     */
	public String getIsHaveCreditIncrease() {
		return this.isHaveCreditIncrease;
	}
	
	/**
	 * @param returnReason
	 */
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	
    /**
     * @return returnReason
     */
	public String getReturnReason() {
		return this.returnReason;
	}
	
	/**
	 * @param refuseReason
	 */
	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}
	
    /**
     * @return refuseReason
     */
	public String getRefuseReason() {
		return this.refuseReason;
	}
	
	/**
	 * @param multiSelectProve
	 */
	public void setMultiSelectProve(String multiSelectProve) {
		this.multiSelectProve = multiSelectProve;
	}
	
    /**
     * @return multiSelectProve
     */
	public String getMultiSelectProve() {
		return this.multiSelectProve;
	}
	
	/**
	 * @param firstJudgRemark
	 */
	public void setFirstJudgRemark(String firstJudgRemark) {
		this.firstJudgRemark = firstJudgRemark;
	}
	
    /**
     * @return firstJudgRemark
     */
	public String getFirstJudgRemark() {
		return this.firstJudgRemark;
	}
	
	/**
	 * @param approveConclusion
	 */
	public void setApproveConclusion(String approveConclusion) {
		this.approveConclusion = approveConclusion;
	}
	
    /**
     * @return approveConclusion
     */
	public String getApproveConclusion() {
		return this.approveConclusion;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}