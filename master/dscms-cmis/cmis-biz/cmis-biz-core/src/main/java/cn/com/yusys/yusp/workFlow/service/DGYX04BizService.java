package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.domain.PvpEntrustLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.PvpEntrustLoanAppMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
public class DGYX04BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX04BizService.class);//定义log

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private BizCommonService bizCommonService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType)){
            handleYX011Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)) {
            handleYX013Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 对公贷款出账申请
    private void handleYX011Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("对公贷款出账申请"+pvpSerno+"流程操作:" + currentOpType+"后业务处理");
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)){
                    pvpLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                    if(CmisCommonConstants.GUAR_MODE_21.equals(pvpLoanApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(pvpLoanApp.getGuarMode())){
                        TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
                        taskUrgentAppDto.setBizType(CmisFlowConstants.FLOW_TYPE_TYPE_YX011);
                        taskUrgentAppDto.setCusId(pvpLoanApp.getCusId());
                        taskUrgentAppDto.setCusName(pvpLoanApp.getCusName());
                        taskUrgentAppDto.setSerno(pvpSerno);
                        taskUrgentAppDto.setUrgentType("3");
                        taskUrgentAppDto.setPwbrSerno(resultInstanceDto.getInstanceId());
                        taskUrgentAppDto.setUrgentResn("系统加急");
                        taskUrgentAppDto.setManagerBrId(pvpLoanApp.getManagerBrId());
                        taskUrgentAppDto.setManagerId(pvpLoanApp.getManagerId());
                        taskUrgentAppDto.setInputBrId(pvpLoanApp.getInputBrId());
                        taskUrgentAppDto.setInputId(pvpLoanApp.getInputId());
                        taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
                        taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
                        ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
                        if(!"0".equals(result.getCode()) || 1 != result.getData()){
                            throw BizException.error(null, "999999","新增系统加急记录失败！");
                        }
                    }
                }
                if ("244_7".equals(resultInstanceDto.getCurrentNodeId())){
                    createCentralFileTask4PvpLoanApp(pvpSerno, CmisFlowConstants.FLOW_TYPE_TYPE_YX011,resultInstanceDto) ;
                }
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,pvpSerno);
                // pvpLoanAppService.createImageSpplInfo(pvpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_01);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"流程结束操作，流程参数："+ resultInstanceDto.toString());
                // 推送影像审批信息
                sendImage(resultInstanceDto);
                //放款申请通过，调用后续的业务处理过程
                pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,pvpSerno);
                pvpLoanAppService.createImageSpplInfo(pvpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_01,"对公贷款出账申请");
                //微信通知
                String managerId = pvpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpLoanApp.getCusName());
                        paramMap.put("prdName", "一般贷款出账");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                log.info("对公贷款出账申请"+pvpSerno+"发送用印，流程参数："+ resultInstanceDto.toString());
                cmisBizXwCommonService.sendYKTask(pvpSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"退回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                    //微信通知
                    String managerId = pvpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpLoanApp.getCusName());
                            paramMap.put("prdName", "一般贷款出账");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //针对客户经理拿回操作，应当将业务申请主表的业务状态更新为【打回-992】
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"拿回初始节点操作，流程参数："+ resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"否决操作，流程参数："+ resultInstanceDto.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                pvpLoanAppService.handleBusinessAfterRefuse(pvpSerno);
            } else {
                log.warn("对公贷款出账申请"+pvpSerno+"未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 委托贷款出账申请
    private void handleYX013Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("委托贷款出账申请" + currentOpType);
        try {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
            this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                if ("244_7".equals(resultInstanceDto.getCurrentNodeId())){
                    createCentralFileTask4PvpEntrustLoanApp(pvpSerno, CmisFlowConstants.FLOW_TYPE_TYPE_YX013,resultInstanceDto) ;
                }
                // 推送影像审批信息
                pvpEntrustLoanAppService.handleBusinessDataAfterStart(pvpSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                // 推送影像审批信息
                sendImage(resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                pvpEntrustLoanAppService.handleBusinessDataAfterEnd(pvpSerno);

                //微信通知
                String managerId = pvpEntrustLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpEntrustLoanApp.getCusName());
                        paramMap.put("prdName", "委托贷款出账");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,pvpSerno);
                pvpLoanAppService.createImageSpplInfo(pvpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_08,"委托贷款出账");
                log.info("委托出账申请"+pvpSerno+"发送用印，流程参数："+ resultInstanceDto.toString());
                cmisBizXwCommonService.sendYKTask(pvpSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
                    this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("委托出账申请"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    //pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    //pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpEntrustLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
                    this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
                    //微信通知
                    String managerId = pvpEntrustLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpEntrustLoanApp.getCusName());
                            paramMap.put("prdName", "委托贷款出账");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("委托出账申请"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("委托出账申请" + pvpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pvpEntrustLoanAppService.handleBusinessAfterRefuse(pvpSerno);
            } else {
                log.warn("委托出账申请" + pvpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGYX04.equals(flowCode);
    }

    /**
     * @作者:lizx
     * @方法名称: createCentralFileTask4PvpLoanApp
     * @方法描述:  
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/6 21:50
     * @param pvpSerno: 
     * @param bizType: 
     * @return: void
     * @算法描述: 无
    */
    public void createCentralFileTask4PvpEntrustLoanApp(String pvpSerno,String bizType,ResultInstanceDto resultInstanceDto){
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
        //3、新增临时档案任务
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(pvpSerno);
        centralFileTaskdto.setTraceId(pvpEntrustLoanApp.getContNo());
        centralFileTaskdto.setCusId(pvpEntrustLoanApp.getCusId());
        centralFileTaskdto.setCusName(pvpEntrustLoanApp.getCusName());
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(pvpEntrustLoanApp.getInputId());
        centralFileTaskdto.setInputBrId(pvpEntrustLoanApp.getInputBrId());
        centralFileTaskdto.setOptType("02"); // 非纯指令
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        centralFileTaskdto.setTaskType("02"); // 档案暂存
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    public void createCentralFileTask4PvpLoanApp(String pvpSerno,String bizType,ResultInstanceDto resultInstanceDto){
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        //3、新增临时档案任务
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(pvpSerno);
        centralFileTaskdto.setTraceId(pvpLoanApp.getContNo());
        centralFileTaskdto.setCusId(pvpLoanApp.getCusId());
        centralFileTaskdto.setCusName(pvpLoanApp.getCusName());
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(pvpLoanApp.getInputId());
        centralFileTaskdto.setInputBrId(pvpLoanApp.getInputBrId());
        centralFileTaskdto.setOptType("02"); // 非纯指令
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        centralFileTaskdto.setTaskType("02"); // 档案暂存
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 对公贷款出账申请 参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamForPT(ResultInstanceDto resultInstanceDto, PvpLoanApp pvpLoanApp, String pvpSerno) {
        log.info("对公贷款出账申请【{}】审批流程参数更新开始", pvpSerno);
        String approveStatus = pvpLoanApp.getApproveStatus();
        BigDecimal pvpAmt =  pvpLoanApp.getPvpAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        params.put("approveStatus", approveStatus);
        params.put("pvpAmt", pvpAmt);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("对公贷款出账申请【{}】审批流程参数更新结束", pvpSerno);
    }

    /**
     * @方法名称: put2VarParamForWT
     * @方法描述: 委托出账申请 参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamForWT(ResultInstanceDto resultInstanceDto, PvpEntrustLoanApp pvpEntrustLoanApp, String pvpSerno) {
        log.info("委托出账申请【{}】审批流程参数更新开始", pvpSerno);
        String approveStatus = pvpEntrustLoanApp.getApproveStatus();
        BigDecimal pvpAmt =  pvpEntrustLoanApp.getPvpAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        params.put("approveStatus", approveStatus);
        params.put("pvpAmt", pvpAmt);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("委托出账申请【{}】审批流程参数更新结束", pvpSerno);
    }

    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();
            if ("244_9".equals(nodeId) || "244_8".equals(nodeId)){//【集中作业放款初审岗、集中作业放款复审岗员工号】 这两个岗位的人审核影像
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }
        if(approveUserId.length() > 0){
            // 一般情况下，直接获取流程实列中的参数。
            Map<String, Object> map = (Map) JSON.parse(resultInstanceDto.getFlowParam());
            String topOutsystemCode = (String) map.get("topOutsystemCode");
            if(StringUtils.isEmpty(topOutsystemCode)){
                throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
            }
            Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
            String docid = imageParams.get("contid");
            String pvpSreno = imageParams.get("businessid");
            if(StringUtils.isEmpty(docid)){
                throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
            }
            approveUserId = approveUserId.substring(1);
            String[] arr = topOutsystemCode.split(";");
            for (int i = 0; i < arr.length; i++) {
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(docid);//任务编号
                imageApprDto.setApproval("同意");//审批意见
                imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                imageApprDto.setOutcode(arr[i]);//文件类型根节点
                imageApprDto.setOpercode(approveUserId);//审批人员
                imageApprDto.setBatchNo(pvpSreno);
                cmisBizXwCommonService.sendImage(imageApprDto);
            }
        }
    }
}
