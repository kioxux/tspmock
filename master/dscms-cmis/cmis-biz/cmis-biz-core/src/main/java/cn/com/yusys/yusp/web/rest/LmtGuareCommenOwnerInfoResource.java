/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGuareCommenOwnerInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.service.LmtGuareCommenOwnerInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCommenOwnerInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-25 17:22:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "抵质押物共有人信息")
@RequestMapping("/api/lmtguarecommenownerinfo")
public class LmtGuareCommenOwnerInfoResource {
    @Autowired
    private LmtGuareCommenOwnerInfoService lmtGuareCommenOwnerInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGuareCommenOwnerInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGuareCommenOwnerInfo> list = lmtGuareCommenOwnerInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtGuareCommenOwnerInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGuareCommenOwnerInfo>> index(QueryModel queryModel) {
        List<LmtGuareCommenOwnerInfo> list = lmtGuareCommenOwnerInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareCommenOwnerInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGuareCommenOwnerInfo> show(@PathVariable("pkId") String pkId) {
        LmtGuareCommenOwnerInfo lmtGuareCommenOwnerInfo = lmtGuareCommenOwnerInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGuareCommenOwnerInfo>(lmtGuareCommenOwnerInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGuareCommenOwnerInfo> create(@RequestBody LmtGuareCommenOwnerInfo lmtGuareCommenOwnerInfo) throws URISyntaxException {
        lmtGuareCommenOwnerInfoService.insert(lmtGuareCommenOwnerInfo);
        return new ResultDto<LmtGuareCommenOwnerInfo>(lmtGuareCommenOwnerInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGuareCommenOwnerInfo lmtGuareCommenOwnerInfo) throws URISyntaxException {
        int result = lmtGuareCommenOwnerInfoService.update(lmtGuareCommenOwnerInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGuareCommenOwnerInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGuareCommenOwnerInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @author zlf
     * @date 2021/4/28 20:23
     * @version 1.0.0
     * @desc 编辑信息
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("保存编辑信息")
    @PostMapping("/savecommenownerinfo")
    protected ResultDto saveCommenOwnerInfo(@RequestBody LmtGuareCommenOwnerInfo lmtGuareCommenOwnerInfo) {
        return  lmtGuareCommenOwnerInfoService.saveCommenOwnerInfo(lmtGuareCommenOwnerInfo);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.LmtGuareCommenOwnerInfo>>
     * @author hubp
     * @date 2021/6/8 10:44
     * @version 1.0.0
     * @desc    通过抵押物编号，查询共有人信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过抵押物编号，查询共有人信息列表")
    @PostMapping("/selectbypldimnno")
    protected ResultDto<List<LmtGuareCommenOwnerInfo>> selectByPldimnNo(@RequestBody QueryModel queryModel) {
        List<LmtGuareCommenOwnerInfo> list = lmtGuareCommenOwnerInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareCommenOwnerInfo>>(list);
    }
    
    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/6/15 15:59
     * @version 1.0.0
     * @desc 
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletebypkid")
    protected ResultDto<Integer> deleteByPkId(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        int result = lmtGuareCommenOwnerInfoService.deleteByPrimaryKey(lmtSurveyReportDto.getPkId());
        return new ResultDto<Integer>(result);
    }
}
