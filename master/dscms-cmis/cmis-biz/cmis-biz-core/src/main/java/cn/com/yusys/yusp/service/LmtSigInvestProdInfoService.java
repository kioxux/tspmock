/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSigInvestProdInfo;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestProdInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 11:05:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestProdInfoService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestProdInfoMapper lmtSigInvestProdInfoMapper;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestProdInfo selectByPrimaryKey(String pkId) {
        return lmtSigInvestProdInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestProdInfo> selectAll(QueryModel model) {
        List<LmtSigInvestProdInfo> records = (List<LmtSigInvestProdInfo>) lmtSigInvestProdInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestProdInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestProdInfo> list = lmtSigInvestProdInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestProdInfo record) {
        return lmtSigInvestProdInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestProdInfo record) {
        return lmtSigInvestProdInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestProdInfo record) {
        LmtSigInvestProdInfo lmtSigInvestProdInfo = lmtSigInvestProdInfoMapper.selectBySerno(record.getSerno());
        if (lmtSigInvestProdInfo != null) {
            return lmtSigInvestProdInfoMapper.updateByPrimaryKey(record);
        } else {
            record.setOprType(CmisBizConstants.OPR_TYPE_01);
            return lmtSigInvestProdInfoMapper.insert(record);
        }
    }


    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestProdInfo record) {
        return lmtSigInvestProdInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestProdInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestProdInfoMapper.deleteByIds(ids);
    }

    /**
     * 保存产品分析
     * @param lmtSigInvestProdInfo
     * @return
     */
    public int updateCpfx(LmtSigInvestProdInfo lmtSigInvestProdInfo) {
        String serno = lmtSigInvestProdInfo.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"cpfx");
        return update(lmtSigInvestProdInfo);
    }
}
