package cn.com.yusys.yusp.web.server.xdxw0038;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0038.req.Xdxw0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0038.resp.Xdxw0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0038.Xdxw0038Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:查询调查表和其他关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0038:查询调查表和其他关联信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0038Resource.class);

    @Autowired
    private Xdxw0038Service xdxw0038Service;

    /**
     * 交易码：xdxw0038
     * 交易描述：查询调查表和其他关联信息
     *
     * @param xdxw0038DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询调查表和其他关联信息")
    @PostMapping("/xdxw0038")
    protected @ResponseBody
    ResultDto<Xdxw0038DataRespDto> xdxw0038(@Validated @RequestBody Xdxw0038DataReqDto xdxw0038DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataReqDto));
        Xdxw0038DataRespDto xdxw0038DataRespDto = new Xdxw0038DataRespDto();// 响应Dto:查询调查表和其他关联信息
        ResultDto<Xdxw0038DataRespDto> xdxw0038DataResultDto = new ResultDto<>();
        String cusId = xdxw0038DataReqDto.getCusId();//客户号
        String cert_code = xdxw0038DataReqDto.getCert_code();//证件号码
        try {
            // 从xdxw0038DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataReqDto));
            xdxw0038DataRespDto = xdxw0038Service.xdxw0038(xdxw0038DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataRespDto));
            // 封装xdxw0038DataResultDto中正确的返回码和返回信息
            xdxw0038DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0038DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, e.getMessage());
            // 封装xdxw0038DataResultDto中异常返回码和返回信息
            xdxw0038DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0038DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0038DataRespDto到xdxw0038DataResultDto中
        xdxw0038DataResultDto.setData(xdxw0038DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataResultDto));
        return xdxw0038DataResultDto;
    }
}
