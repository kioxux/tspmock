/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubAppr;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLmtAppr;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLmtApprMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:22:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicLmtApprService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicLmtApprMapper lmtSigInvestBasicLmtApprMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    @Autowired
    private LmtSigInvestBasicInfoSubApprService lmtSigInvestBasicInfoSubApprService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAppr selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtApprMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicLmtAppr> selectAll(QueryModel model) {
        List<LmtSigInvestBasicLmtAppr> records = (List<LmtSigInvestBasicLmtAppr>) lmtSigInvestBasicLmtApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicLmtAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicLmtAppr> list = lmtSigInvestBasicLmtApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicLmtAppr record) {
        return lmtSigInvestBasicLmtApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicLmtAppr record) {
        return lmtSigInvestBasicLmtApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicLmtAppr record) {
        return lmtSigInvestBasicLmtApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicLmtAppr record) {
        return lmtSigInvestBasicLmtApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicLmtApprMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号获取
     * @param serno
     * @return
     */
    public List<LmtSigInvestBasicLmtAppr> selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(serno)) throw new BizException(null,"",null,"申请流水号不允许为空！");
        queryModel.addCondition("serno",serno);
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprs = lmtSigInvestBasicLmtApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicLmtApprs!=null && lmtSigInvestBasicLmtApprs.size()>0){
            return lmtSigInvestBasicLmtApprs ;
        }
        return null;
    }

    /**
     * @方法名称: initLmtSigInvestBasicLmtApprInfo
     * @方法描述: 初始化审批表数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAppr initLmtSigInvestBasicLmtApprInfo(LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp) {
        //主键
        String pkValue = generatePkId();
        //初始化数据
        LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = new LmtSigInvestBasicLmtAppr() ;
        //数据拷贝
        BeanUtils.copyProperties(lmtSigInvestBasicLimitApp, lmtSigInvestBasicLmtAppr);
        //主键
        lmtSigInvestBasicLmtAppr.setPkId(pkValue);
        //最新更新日期
        lmtSigInvestBasicLmtAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicLmtAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicLmtAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //返回对象信息
        return lmtSigInvestBasicLmtAppr;
    }

    public List<Map<String,Object>> selectbasic(QueryModel model){
        List<Map<String,Object>> result = new ArrayList<>();
        List<LmtSigInvestBasicLmtAppr> list = lmtSigInvestBasicLmtApprMapper.selectByModel(model);
        if(list!=null&&list.size()>0){
            for(LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr :list){
                Map<String,Object> map = new HashMap<>();
                String basicSerno = lmtSigInvestBasicLmtAppr.getBasicSerno();
                String approveSerno = lmtSigInvestBasicLmtAppr.getApproveSerno();
                map.put("basicSerno",basicSerno);
                map.put("approveSerno",approveSerno);
                LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr=lmtSigInvestBasicInfoSubApprService.selectByBasicSernoAndApproveSerno(basicSerno,approveSerno);
                if (lmtSigInvestBasicInfoSubAppr != null){
                    map.put("basicCusName",lmtSigInvestBasicInfoSubAppr.getBasicCusName());
                    map.put("basicCusId",lmtSigInvestBasicInfoSubAppr.getBasicCusId());
                    map.put("basicAssetType",lmtSigInvestBasicInfoSubAppr.getBasicAssetType());
                    map.put("basicLmtBizTypeName",lmtSigInvestBasicLmtAppr.getBasicLmtBizTypeName());
                    map.put("basicAssetBalanceAmt",lmtSigInvestBasicInfoSubAppr.getBasicAssetBalanceAmt());
                    map.put("lmtAmt",lmtSigInvestBasicLmtAppr.getLmtAmt());
                }else{
                    throw BizException.error(null,"99999","数据获取失败！");
                }
                result.add(map);
            }
        }
        return result;
    }


    /**
     * 根据申请流水号获取
     * @param approveSerno
     * @return
     */
    public List<LmtSigInvestBasicLmtAppr> selectByApproveSerno(String approveSerno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(approveSerno)) throw new BizException(null,"",null,"审批流水号不允许为空！");
        queryModel.addCondition("approveSerno",approveSerno);
        queryModel.addCondition("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprs = lmtSigInvestBasicLmtApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicLmtApprs!=null && lmtSigInvestBasicLmtApprs.size()>0){
            return lmtSigInvestBasicLmtApprs ;
        }
        return null;
    }

    /**
     * @方法名称: selectByBasicSerno
     * @方法描述: 根据底层申请流水号获取底层额度信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAppr selectByBasicSerno(String basicSerno) {
        return lmtSigInvestBasicLmtApprMapper.selectByBasicSerno(basicSerno);
    }
}
