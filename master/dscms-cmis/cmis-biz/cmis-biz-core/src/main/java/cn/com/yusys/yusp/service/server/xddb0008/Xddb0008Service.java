package cn.com.yusys.yusp.service.server.xddb0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0008.req.Xddb0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0008.resp.List;
import cn.com.yusys.yusp.dto.server.xddb0008.resp.Xddb0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddb0008Service
 * @类描述: #服务类
 * @功能描述:通过入参信息查找抵押物信息表中，其中抵押物分类编号是'DY0401001'的押品存放地址需通过抵押物编号查询
 * @创建人: xull2
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0008Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xddb0008.Xddb0008Service.class);

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    /**
     * 查询在线抵押信息
     *
     * @param xdkh0008DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0008DataRespDto selcetGuarBaseInfoByManagerId(Xddb0008DataReqDto xdkh0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value);
        Xddb0008DataRespDto xddb0008DataRespDto = new Xddb0008DataRespDto();
        String guarCusName = xdkh0008DataReqDto.getPldManName();//抵押人名称
        String managerId = xdkh0008DataReqDto.getManagerId();//客户经理号
        String pageSize = xdkh0008DataReqDto.getPageSize();//每页条数
        String pageNum = xdkh0008DataReqDto.getStartPageNum();//开始页码
        try {
            //分页查询
            int startPageNum = 1;//页码
            int pageSizeNum = 10;//每页条数
            if (StringUtil.isNotEmpty(pageSize) && StringUtil.isNotEmpty(pageNum)) {
                startPageNum = Integer.parseInt(pageNum);
                pageSizeNum = Integer.parseInt(pageSize);
            }
            logger.info("**************************查询在线抵押信息所属押品信息**************************");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarCusName",guarCusName);//抵押人名称
            queryModel.addCondition("managerId", managerId);//客户经理号
            queryModel.addCondition("pageNum", pageNum);//页码
            queryModel.addCondition("pageSize", pageSize);//每页条数
            PageHelper.startPage(startPageNum, pageSizeNum);
            java.util.List<List> lists = guarBaseInfoMapper.selectGuarBaseInfoByManagerId(queryModel);
            PageHelper.clearPage();
            xddb0008DataRespDto.setList(lists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value);
        return xddb0008DataRespDto;
    }
}
