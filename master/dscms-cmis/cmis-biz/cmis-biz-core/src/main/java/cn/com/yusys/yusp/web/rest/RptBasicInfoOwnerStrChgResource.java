/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoOwnerStrChg;
import cn.com.yusys.yusp.service.RptBasicInfoOwnerStrChgService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoOwnerStrChgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:55:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinfoownerstrchg")
public class RptBasicInfoOwnerStrChgResource {
    @Autowired
    private RptBasicInfoOwnerStrChgService rptBasicInfoOwnerStrChgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoOwnerStrChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoOwnerStrChg> list = rptBasicInfoOwnerStrChgService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoOwnerStrChg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoOwnerStrChg>> index(QueryModel queryModel) {
        List<RptBasicInfoOwnerStrChg> list = rptBasicInfoOwnerStrChgService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoOwnerStrChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptBasicInfoOwnerStrChg> show(@PathVariable("pkId") String pkId) {
        RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg = rptBasicInfoOwnerStrChgService.selectByPrimaryKey(pkId);
        return new ResultDto<RptBasicInfoOwnerStrChg>(rptBasicInfoOwnerStrChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoOwnerStrChg> create(@RequestBody RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg) throws URISyntaxException {
        rptBasicInfoOwnerStrChgService.insert(rptBasicInfoOwnerStrChg);
        return new ResultDto<RptBasicInfoOwnerStrChg>(rptBasicInfoOwnerStrChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg) throws URISyntaxException {
        int result = rptBasicInfoOwnerStrChgService.update(rptBasicInfoOwnerStrChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoOwnerStrChgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoOwnerStrChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected  ResultDto<List<RptBasicInfoOwnerStrChg>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptBasicInfoOwnerStrChg>>(rptBasicInfoOwnerStrChgService.selectByModel(model));
    }

    @PostMapping("/updateOwnerStrChg")
    protected ResultDto<Integer> updateOwnerStrChg(@RequestBody RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg){
        return new ResultDto<Integer>(rptBasicInfoOwnerStrChgService.updateSelective(rptBasicInfoOwnerStrChg));
    }

    @PostMapping("/insertOwnerStrChg")
    protected ResultDto<Integer> insertOwnerStrChg(@RequestBody RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg){
        rptBasicInfoOwnerStrChg.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptBasicInfoOwnerStrChgService.insertSelective(rptBasicInfoOwnerStrChg));
    }

    @PostMapping("/deleteOwnerStrChg")
    protected ResultDto<Integer> deleteOwnerStrChg(@RequestBody RptBasicInfoOwnerStrChg rptBasicInfoOwnerStrChg){
        String pkId= rptBasicInfoOwnerStrChg.getPkId();
        return new ResultDto<Integer>(rptBasicInfoOwnerStrChgService.deleteByPrimaryKey(pkId));
    }

}
