/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.req.CmisLmt0030ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.resp.CmisLmt0030RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.req.CmisLmt0031ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.resp.CmisLmt0031RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.req.CmisLmt0037ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.resp.CmisLmt0037RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.req.CmisLmt0043ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.resp.CmisLmt0043RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.req.CmisLmt0055ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.CmisLmt0055RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.LmtDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.resp.CmisLmt0064RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.req.CmisLmt0065ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.resp.CmisLmt0065RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-17 16:02:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestAppService extends BizInvestCommonService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtSigInvestAppService.class);

    @Autowired
    private LmtSigInvestAppMapper lmtSigInvestAppMapper;

    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService;

    @Autowired
    private LmtSigInvestRstService lmtSigInvestRstService;

    /**
     * 申请关联客户信息Service
     */
    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    /**
     * 申请关联客户所属集团额度信息Service
     */
    @Autowired
    private LmtAppRelGrpLimitService lmtAppRelGrpLimitService;

    /**
     * 单笔投资授信台账表
     */
    @Autowired
    private LmtSigInvestAccService lmtSigInvestAccService;

    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService;

    @Autowired
    private LmtSigInvestApprMapper lmtSigInvestApprMapper;

    @Autowired
    private LmtSigInvestRstMapper lmtSigInvestRstMapper;

    @Autowired
    private LmtSigInvestAccMapper lmtSigInvestAccMapper;

    @Autowired
    private LmtSigInvestBasicInfoRstService lmtSigInvestBasicInfoRstService;

    @Autowired
    private LmtSigInvestBasicInfoRstMapper lmtSigInvestBasicInfoRstMapper;

    @Autowired
    private LmtSigInvestBasicLmtRstService lmtSigInvestBasicLmtRstService;

    @Autowired
    private LmtSigInvestBasicLmtRstMapper lmtSigInvestBasicLmtRstMapper;

    @Autowired
    private LmtSigInvestBasicInfoSubApprService lmtSigInvestBasicInfoSubApprService;

    @Autowired
    private LmtSigInvestBasicInfoSubApprMapper lmtSigInvestBasicInfoSubApprMapper;

    @Autowired
    private LmtSigInvestBasicInfoApprService lmtSigInvestBasicInfoApprService;

    @Autowired
    private LmtSigInvestBasicInfoApprMapper lmtSigInvestBasicInfoApprMapper;

    @Autowired
    private LmtSigInvestBasicLmtApprService lmtSigInvestBasicLmtApprService;

    @Autowired
    private LmtSigInvestBasicLmtApprMapper lmtSigInvestBasicLmtApprMapper;

    @Autowired
    private LmtSigInvestBasicInfoSubRstService lmtSigInvestBasicInfoSubRstService;

    @Autowired
    private LmtSigInvestBasicInfoSubRstMapper lmtSigInvestBasicInfoSubRstMapper;

    @Autowired
    private LmtSigInvestBasicInfoSubAccService lmtSigInvestBasicInfoSubAccService;

    @Autowired
    private LmtSigInvestBasicInfoSubAccMapper lmtSigInvestBasicInfoSubAccMapper;

    @Autowired
    private LmtSigInvestBasicInfoAccService lmtSigInvestBasicInfoAccService;

    @Autowired
    private LmtSigInvestBasicInfoAccMapper lmtSigInvestBasicInfoAccMapper;

    @Autowired
    private LmtSigInvestBasicLmtAccService lmtSigInvestBasicLmtAccService;

    @Autowired
    private LmtSigInvestBasicLmtAccMapper lmtSigInvestBasicLmtAccMapper;

    @Autowired
    private LmtSigInvestBasicInfoService lmtSigInvestBasicInfoService;

    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService;

    @Autowired
    private LmtSigInvestRiskAnlyService lmtSigInvestRiskAnlyService;

    @Autowired
    private LmtSigInvestBasicInfoSubService lmtSigInvestBasicInfoSubService;

    @Autowired
    private LmtSigInvestBasicLimitAppMapper lmtSigInvestBasicLimitAppMapper;

    @Autowired
    private LmtSigInvestBasicInfoSubMapper lmtSigInvestBasicInfoSubMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 授信变更申请从表 保存授信变更申请书
     */
    @Autowired
    private LmtChgDetailService lmtChgDetailService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private LmtAppRelGuarService lmtAppRelGuarService;

    @Autowired
    private LmtSigInvestRelFinaInfoService lmtSigInvestRelFinaInfoService;

    @Autowired
    private LmtSigInvestRelFinaDetailsService lmtSigInvestRelFinaDetailsService;

    @Autowired
    private LmtSigInvestRelMainBussService lmtSigInvestRelMainBussService;

    @Autowired
    private LmtSigInvestRelCapitInfoService lmtSigInvestRelCapitInfoService;

    @Autowired
    private LmtSigInvestRelCusOutGuarService lmtSigInvestRelCusOutGuarService;

    @Autowired
    private LmtSigInvestProdLevelDetailsService lmtSigInvestProdLevelDetailsService;

    @Autowired
    private LmtSigInvestProdInfoService lmtSigInvestProdInfoService;

    @Autowired
    private LmtSigInvestProdRateService lmtSigInvestProdRateService;

    @Autowired
    private LmtSigInvestBasicCptAnlyService lmtSigInvestBasicCptAnlyService;

    @Autowired
    private LmtSigInvestBasicFncAnlyService lmtSigInvestBasicFncAnlyService;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 授信复议申请从表 保存授信复议申请书
     */
    @Autowired
    private LmtReconsideDetailService lmtReconsideDetailService;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    @Autowired
    private LmtReplyAccLoanCondService lmtReplyAccLoanCondService;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;



    private BigDecimal lmtAmt;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestApp selectByPrimaryKey(String pkId) {
        return lmtSigInvestAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtSigInvestApp> selectAll(QueryModel model) {
        List<LmtSigInvestApp> records = (List<LmtSigInvestApp>) lmtSigInvestAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * 01.申请：审批状态为【待发起】、【审批中】、【退回】的业务数据
     * 02.历史：审批状态为【审批通过】、【否决】、【自行退出】的业务数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSigInvestApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        String listtype = (String) model.getCondition().get("listtype");
        String showType = (String) model.getCondition().get("showType");
        String cusCatalogType = (String) model.getCondition().get("cusCatalogType");
        if (!StringUtils.isBlank(listtype)) {
            //01.申请：审批状态为【待发起】、【审批中】、【退回】的业务数据
            if (CmisBizConstants.LIST_TYPE.APPLY.equals(listtype)) {
                List status = new ArrayList();
                status.add(CmisBizConstants.APPLY_STATE_APP);//111
                status.add(CmisBizConstants.APPLY_STATE_TODO);//000
                status.add(CmisBizConstants.APPLY_STATE_CALL_BACK);//992
                model.addCondition("noApproveStatus", status);
            }
            //02.历史：审批状态为【审批通过】、【否决】、【自行退出】的业务数据
            if (CmisBizConstants.LIST_TYPE.HISTORY.equals(listtype)) {
                List status = new ArrayList();
                status.add(CmisBizConstants.APPLY_STATE_PASS);//997
                status.add(CmisBizConstants.APPLY_STATE_REFUSE);//998
                status.add(CmisBizConstants.APPLY_STATE_QUIT);//996
                model.addCondition("noApproveStatus", status);
            }
        }
        //授信列表
        if (!StringUtils.isBlank(showType)) {
            List appTypes = new ArrayList();
            //主体授信申请列表（授信新增、授信续作、授信复议）
            if (CmisBizConstants.INVEST_SHOW_TYPE.APPLY.equals(showType)) {
                appTypes.add(CmisBizConstants.STD_SX_LMT_TYPE_01);
                appTypes.add(CmisBizConstants.STD_SX_LMT_TYPE_03);
                appTypes.add(CmisBizConstants.STD_SX_LMT_TYPE_05);
            }
            if (CmisBizConstants.INVEST_SHOW_TYPE.CHANGE.equals(showType)) {
                //否则
                appTypes.add(CmisBizConstants.STD_SX_LMT_TYPE_02);
            }
            model.addCondition("showTypes", appTypes);
        }
        //主体授信、产品授信区分
        if (StringUtils.isBlank(cusCatalogType)) {

        }
        //TODO 业务类型？
        List<LmtSigInvestApp> list = lmtSigInvestAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 查询在途的单笔投资业务申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSigInvestApp> selectOnSideAppByModel(QueryModel model) {
        List<LmtSigInvestApp> list = lmtSigInvestAppMapper.selectOnSideAppByModel(model);
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtSigInvestApp record) {
        return lmtSigInvestAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestApp record) {
        return lmtSigInvestAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtSigInvestApp record) {
        return lmtSigInvestAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(LmtSigInvestApp record) {
        return lmtSigInvestAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestAppMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号获取
     *
     * @param serno
     * @return
     */
    public LmtSigInvestApp selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppMapper.selectByModel(queryModel);
        if (lmtSigInvestApps != null && lmtSigInvestApps.size() > 0) {
            return lmtSigInvestApps.get(0);
        }
        return null;
    }

    /**
     * 根据批复流水号获取
     *
     * @param replySerno
     * @return
     */
    public LmtSigInvestApp selectByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno", replySerno);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppMapper.selectByModel(queryModel);
        if (lmtSigInvestApps != null && lmtSigInvestApps.size() > 0) {
            return lmtSigInvestApps.get(0);
        }
        return null;
    }

    /**
     * 逻辑删除
     * 1.审批状态为【待发起】则删除数据，提示信息“删除成功”
     * 2.审批状态为【退回】则后台将该条数据的审批状态更新为【自行退出】，提示信息“自行退出成功”，并将该笔申请所对应的流程否决结束
     *
     * @param serno
     * @return
     */
    @Transactional
    public int deleteLogicBySerno(String serno) {
        log.info("授信申请表数据 {} 逻辑删除。。。", serno);
        LmtSigInvestApp lmtSigInvestApp = selectBySerno(serno);
        if (lmtSigInvestApp != null) {
            //审批状态为【待发起】则删除数据，提示信息“删除成功”
            if (CmisBizConstants.APPLY_STATE_TODO.equals(lmtSigInvestApp.getApproveStatus())) {
                lmtSigInvestApp.setOprType(CmisBizConstants.OPR_TYPE_02);
            }
            //审批状态为【退回】则后台将该条数据的审批状态更新为【自行退出】,提示信息“自行退出成功”，并将该笔申请所对应的流程否决结束
            if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtSigInvestApp.getApproveStatus())) {
                lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                //TODO 流程否决结束 2021-05-18
                log.info("授信申请流程删除 bizId: {}", serno);
                workflowCoreClient.deleteByBizId(serno);
            }
            return lmtSigInvestAppMapper.updateByPrimaryKey(lmtSigInvestApp);
        } else {
            throw BizException.error(null, EclEnum.INTBANK_ERROR_000005.key, EclEnum.INTBANK_ERROR_000005.value);
        }
    }

    /**
     * 添加（主体授信）记录
     *
     * @param info
     */
    @Transactional
    public Map addProjectBasicInfo(Map info) throws ParseException {
        Instant stime = Instant.now();
        int result = 0;
        try {
            Map dataInfo = (Map) info.get("map");
            String cusId = (String) dataInfo.get("cusId");
            String cusType = (String) dataInfo.get("cusType");
            String cusCatalog = (String) dataInfo.get("cusCatalog");
            String lmtBizType = (String) dataInfo.get("lmtBizType");
            String appType = (String) dataInfo.get("appType");
            String assetNo = (String) dataInfo.get("assetNo");

            //检测该客户是否存在正在流转的申请
            checkCanRestart("", appType, lmtBizType, cusId, null, cusCatalog);

            changeValueToBigDecimal(dataInfo, "lmtAmt");
            changeValueToBigDecimal(dataInfo, "intendActualIssuedScale");
            changeValueToBigDecimal(dataInfo, "origiLmtAmt");
            changeValueToBigDecimal(dataInfo, "sobsAmt");
            changeValueToBigDecimal(dataInfo, "assetManaAmt");
            changeValueToBigDecimal(dataInfo, "prdTotalAmt");
            changeValueToBigDecimal(dataInfo, "rate");
            changeValueToBigDecimal(dataInfo, "origiRate");
            //TODO 添加Rate
            dataInfo.remove("updateTime");
            dataInfo.remove("createTime");

            //校验 serno、pkId前台是否生成
            if (StringUtils.isBlank(String.valueOf(dataInfo.get("newSerno"))) || "null".equals(String.valueOf(dataInfo.get("newSerno")))
                    || StringUtils.isBlank(String.valueOf(dataInfo.get("newPkId"))) || "null".equals(String.valueOf(dataInfo.get("newPkId")))){
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
            }

            //生成申请流水号  从前台获取 ，防止重复提交
            String newSerno = (String) dataInfo.get("newSerno");
            log.info(String.format("保存主体授信申请数据,生成流水号%s", newSerno));
            dataInfo.put("serno", newSerno);

            //添加授信主表数据
            result = addOrUpdateLmtSigInvestApp(newSerno, dataInfo, false);
            if (result == 1) {
                //添加授信从表数据
                result = lmtSigInvestSubAppService.addOrUpdateLmtSigInvestSubApp(newSerno, dataInfo, false);
            }

            //TODO 添加企业相关信息（lmt_app_rel_cus_info、lmt_app_corre_shd、lmt_sig_invest_rel_fina_info（授信主体及增信人财务信息总表））
            //result = insertCusInfo(serno,(String) dataInfo.get("cusId"), (String) dataInfo.get("cusCatalog"));
            lmtAppRelCusInfoService.insertCusInfoApp(newSerno, cusId, cusType, cusCatalog);

            //TODO 添加集团额度相关信息（lmt_app_rel_grp_limit、lmt_app_rel_cus_limit、lmt_app_rel_cus_details_limit）
            lmtAppRelGrpLimitService.insertCusLmtInfo(newSerno, cusId, cusType, cusCatalog);

            //默认添加  判断当前业务类型是否为（结构化融资&其他非标投资&资产证券化产品（标准）&资产证券化产品（非标））增加单笔投资授信关联风险分析（lmt_sig_invest_risk_anly）
            lmtSigInvestRiskAnlyService.insertRiskAnly(newSerno);

            //判断当前业务类型是否为（ 资产-其他）初始化 （单笔投资产品对应底层资产基本情况—lmt_sig_invest_basic_info）
            if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4003.equals(lmtBizType)) {
                lmtSigInvestBasicInfoService.insertInvestBasicInfo(newSerno);
            }

        } catch (Exception e) {
            log.error("主体授信新增失败==>", e);
            if (e instanceof BizException) {
                throw e;
            }
            result = 0;
        }
        if (result == 0) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_INSERT_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_INSERT_FAILD.value);
        }

        Instant etime = Instant.now();
        log.info("=====addProjectBasicInfo执行时间===》 {} ms", Duration.between(stime, etime).toMillis());
        return info;
    }


    /**
     * 更新主体授信信息
     *
     * @param info
     * @return
     */
    @Transactional
    public Integer updateProjectBasicInfo(Map info) {
        String serno = (String) info.get("serno");
        int result = 0;
        try {
            changeValueToBigDecimal(info, "lmtAmt");
            changeValueToBigDecimal(info, "intendActualIssuedScale");
            changeValueToBigDecimal(info, "origiLmtAmt");
            changeValueToBigDecimal(info, "sobsAmt");
            changeValueToBigDecimal(info, "assetManaAmt");
            changeValueToBigDecimal(info, "rate");
            changeValueToBigDecimal(info, "origiRate");
            changeValueToBigDecimal(info, "prdTotalAmt");
            changeValueToInteger(info, "origiLmtTerm");
            //TODO 添加Rate
            info.remove("updateTime");
            info.remove("createTime");

            String assetNo = (String) info.get("assetNo");
            String proNo = (String) info.get("proNo");
            //查询台账表中是否已存在当前资产编号
            lmtSigInvestAccService.check_isExistAssetNo(assetNo,proNo);

            /**
             * 判断当前申请类型是否为授信变更
             * 规则：
             *  支持对单笔投资业务的有效批复发起变更申请，调整申报信息，授信金额上只能调减，不能调增，期限只能缩短，不能延长
             */
            if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(info.get("appType"))) {
                BigDecimal lmtAmt = (BigDecimal) info.get("lmtAmt");
                BigDecimal origiLmtAmt = (BigDecimal) info.get("origiLmtAmt");
                Integer lmtTerm = (Integer) info.get("lmtTerm");
                Integer origiLmtTerm = (Integer) info.get("origiLmtTerm");

                //校验授信金额 （授信金额上只能调减，不能调增）
                if (lmtAmt.compareTo(origiLmtAmt) > 0) {
                    throw BizException.error(null, EclEnum.LMT_INVEST_CHG_LMTAMT_FAILED.key, EclEnum.LMT_INVEST_CHG_LMTAMT_FAILED.value);
                }
                //校验授信期限 （期限只能缩短，不能延长）
                if (lmtTerm.compareTo(origiLmtTerm) > 0) {
                    throw BizException.error(null, EclEnum.LMT_INVEST_CHG_LMTTERM_FAILED.key, EclEnum.LMT_INVEST_CHG_LMTTERM_FAILED.value);
                }
            }
            //更新授信主表
            result = addOrUpdateLmtSigInvestApp(serno, info, true);
            if (result == 1) {
                //更新授信从表
                result = lmtSigInvestSubAppService.addOrUpdateLmtSigInvestSubApp(serno, info, true);
            }
            //更新底层资产授信金额
            if (result == 1) {
                //底层资产授信金额 = 底层资产余额 * 项目授信金额 / 项目总金额
                BigDecimal lmtAmt = (BigDecimal) info.get("lmtAmt");
                BigDecimal prdTotalAmt = (BigDecimal) info.get("prdTotalAmt");
                QueryModel model = new QueryModel();
                model.addCondition("serno", serno);
                model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                model.addCondition("isAppBasicLmt", CmisBizConstants.STD_ZB_YES_NO_Y);
                List<LmtSigInvestBasicInfoSub> basicInfoSubs = lmtSigInvestBasicInfoSubMapper.selectByModel(model);
                if (basicInfoSubs != null) {
                    for (LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub : basicInfoSubs) {
                        QueryModel model1 = new QueryModel();
                        model1.addCondition("basicSerno", lmtSigInvestBasicInfoSub.getBasicSerno());
                        BizInvestCommonService.checkParamsIsNull("basicSerno",lmtSigInvestBasicInfoSub.getBasicSerno());
                        model1.addCondition("basicCusId", lmtSigInvestBasicInfoSub.getBasicCusId());
                        BizInvestCommonService.checkParamsIsNull("basicCusId",lmtSigInvestBasicInfoSub.getBasicCusId());
                        model1.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                        List<LmtSigInvestBasicLimitApp> BasicLimitApps = lmtSigInvestBasicLimitAppMapper.selectByModel(model1);
                        if (CollectionUtils.isNotEmpty(BasicLimitApps)) {
                            LmtSigInvestBasicLimitApp basicLimitApp = BasicLimitApps.get(0);
                            BigDecimal a = lmtSigInvestBasicInfoSub.getBasicAssetBalanceAmt().multiply(lmtAmt);
                            if (BigDecimal.ZERO.compareTo(prdTotalAmt) != 0){
                                basicLimitApp.setLmtAmt(a.divide(prdTotalAmt, 2, BigDecimal.ROUND_HALF_UP));
                            }else{
                                throw BizException.error(null, EclEnum.LMT_INVEST_CHG_LMTTERM_FAILED.key, "项目总金额不能为空");
                            }
                            basicLimitApp.setLmtTerm((Integer) info.get("lmtTerm"));
                            lmtSigInvestBasicLimitAppMapper.updateByPrimaryKey(basicLimitApp);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("主体授信更新失败==>", e);
            if (e instanceof BizException) {
                throw e;
            }
            result = 0;
        }
        if (result == 0) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
        }
        return result;
    }

    /**
     * 添加或修改授信主表数据
     * appType为授信变更时，增加校验规则
     *
     * @param serno
     * @param info
     * @param isUpdate
     */
    private int addOrUpdateLmtSigInvestApp(String serno, Map info, boolean isUpdate) {
        LmtSigInvestApp app = null;

        //修改前保存原pkid
        String origiPkId = null;
        if (isUpdate) {
            app = selectBySerno(serno);
            if (app == null) {
                return 0;
            } else {
                origiPkId = app.getPkId();
            }
        } else {
            app = new LmtSigInvestApp();
        }
        //修改String为BigDecimal
        mapToBean(info, app);
        //使用前台生成 防止重复提交
        app.setSerno(serno);

        app.setManagerBrId(getCurrentUser().getOrg().getCode());
        app.setManagerId(getCurrentUser().getLoginCode());
        //判断是否为null并置为0
        app.setLmtAmt(changeValueToBigDecimal(app.getLmtAmt()));

        //获取计算  是否大额授信  是否报备董事长   审批权限
        Map resultMap = this.getLargeLmtInfo(app);
        String isLargeLmt =(String) resultMap.get("isLargeLmt");
        String isReportChairman =(String) resultMap.get("isReportChairman");
        String apprMode =(String) resultMap.get("apprMode");
        app.setIsLargeLmt(isLargeLmt);
        app.setIsReportChairman(isReportChairman);
        app.setApprMode(apprMode);

        if (isUpdate) {
            app.setPkId(origiPkId);
            //初始化（更新）通用domain信息
            initUpdateDomainProperties(app);
            return lmtSigInvestAppMapper.updateByPrimaryKey(app);
        } else {
            //债券池默认为12月 其余默认为3个月
            if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(app.getLmtBizType())) {
                app.setLmtTerm(CmisBizConstants.LMT_TERM_12);
                //自营金额（默认为0 隐藏）
                app.setSobsAmt(BigDecimal.ZERO);
                //资管金额（默认为0 隐藏）
                app.setAssetManaAmt(BigDecimal.ZERO);
            } else {
                app.setLmtTerm(CmisBizConstants.LMT_TERM_3);
            }
            //流程状态为未发起
            app.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
            //业务类型为授信新增 - 根据向导页 客户选择决定
            //app.setAppType(CmisBizConstants.STD_SX_LMT_TYPE_01);
            //根据业务类型 ，当业务类型为授信新增时，自动生成项目编号 TODO 项目编号 暂时无法添加 先使用申请流水号代替
            if (CmisBizConstants.STD_SX_LMT_TYPE_01.equals(app.getAppType())) {
                String proNo = generateSerno(SeqConstant.INVEST_PRO_SEQ);
                app.setProNo(proNo);
            }
            //是否大额授信 默认否
            app.setIsLargeLmt(CmisBizConstants.STD_ZB_YES_NO_N);
            //是否报备董事长 默认否
            app.setIsReportChairman(CmisBizConstants.STD_ZB_YES_NO_N);
            //增加默认值0
            app.setLmtAmt(BigDecimal.ZERO);
            app.setIntendActualIssuedScale(BigDecimal.ZERO);
            //默认循环为是(页面不可修改)
            app.setIsRevolv(CmisBizConstants.STD_ZB_YES_NO_Y);
            //发行币种(默认为人民币)
            app.setIssuedCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
            //币种(默认为人民币)
            app.setCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
            //初始化(新增)通用domain信息
            initInsertDomainProperties(app);
            //防止重复提交
            String newPkId = (String) info.get("newPkId");
            app.setPkId(newPkId);
            //初始化 必填信息校验页面
            initMustCheckItem(app);
            return lmtSigInvestAppMapper.insert(app);
        }
    }

    /**
     * 初始化 必填信息校验页面
     * @param app
     */
    private void initMustCheckItem(LmtSigInvestApp app) {
        //初始化必填信息校验
        //判断是否为 授信复议
        if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(app.getAppType())){
            insertMustCheckItem(app.getSerno(),"fysqb","复议申请表",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            return;
        }

        //判断是否为 授信变更
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(app.getAppType())){
            insertMustCheckItem(app.getSerno(),"bgsqb","变更申请书",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            return;
        }

        //债券池  - 企业基本信息、 总体调查结论 -
        if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"qyxx","企业信息",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //债券投资、其他标准化债权投资 - 企业基本信息、主体分析、 总体调查结论 -
        else if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4002.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4008.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"qyxx","企业基本信息",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //资产其他、净值型产品 - 主体分析、产品分析、底层资产分析、 总体调查结论 -
        else if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4003.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4005.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"cpfx","产品分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"dczcfx","底层资产分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //资产-债权融资计划、理财直融工具 、理财直融工具（投资）、债务融资工具（投资）  -  总体调查结论 -
        else if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4004.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4009.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4010.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //资产证券化产品（标准）、资产证券化产品（非标） -  企业信息  主体分析  总体情况分析 总体调查结论
        else if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4006.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4007.equals(app.getLmtBizType())){
            //主体授信 企业信息必填
            LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(app.getSerno(), app.getCusId());
            if (lmtAppRelCusInfo != null && !StringUtils.isBlank(lmtAppRelCusInfo.getIntbankOrgType()) && lmtAppRelCusInfo.getIntbankOrgType().indexOf("1C1") == 0) {
                insertMustCheckItem(app.getSerno(), "qyxx", "企业信息", app.getAppType(), CmisBizConstants.STD_ZB_YES_NO_N);
            }
            insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztqkfx","总体情况分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //结构化融资、其他非标债权投资  - 企业信息、财务状况、 总体调查结论
        else if (CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4011.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4012.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"qyxx","企业信息",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"cwzk","财务状况",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
        //16010101	债权融资计划（承销）、16020101	理财直融工具（承销） - 企业信息、主体分析、总体调查结论
        else if(CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(app.getLmtBizType())
                ||CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(app.getLmtBizType())){
            insertMustCheckItem(app.getSerno(),"qyxx","企业信息",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztfx","主体分析",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
            insertMustCheckItem(app.getSerno(),"ztdcjl","总体调查结论",app.getAppType(),CmisBizConstants.STD_ZB_YES_NO_N);
        }
    }

    /**
     * 判断大额授信和是否报备董事长
     * @param app
     * @return
     */
    public Map<String, String> checkIsLargeLmt(LmtSigInvestApp app) {
        Map<String, String> resultMap = new HashMap<>();

        String serno = app.getSerno();

        //大额值
        Long largeLmt = 0L;
        //是否大额授信 默认为 N
        String isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_N;
        //是否报备董事长默认为N
        String isReportChairman = CmisBizConstants.STD_ZB_YES_NO_N;
        //客户大类
        String cusCatalog = app.getCusCatalog();
        //授信品种
        String lmtBizType = app.getLmtBizType();

        if(lmtBizType.startsWith("16")){
            resultMap.put("isLargeLmt", isLargeLmt);
            resultMap.put("isReportChairman", isReportChairman);
            //是否有信贷类业务
            resultMap.put("hasLoanBuss",  CmisBizConstants.STD_ZB_YES_NO_N);
            log.info("【checkIsLargeLmt】【{}】大额授信处理结果，lmtBizType为承销额度，默认非大额，非报备董事长，不走贷审会--------> end 【{}】", app.getSerno(), JSON.toJSONString(resultMap));
            return resultMap;
        }

        //拟投资金额
        BigDecimal lmtAmt = app.getLmtAmt();
        log.info("【checkIsLargeLmt】【"+serno+"】本笔业务授信金额lmtAmt【{}】-------->  ",lmtAmt);
        //是否有信贷类业务(对公客户)
        String hasLoanBuss = getHasLoanBuss(app);
        log.info("【checkIsLargeLmt】【"+serno+"】本笔业务有信贷类业务(对公客户)hasLoanBuss【{}】-------->  ",hasLoanBuss);
        //否存在有效的非标额度
        String hasActiveNoStandardLmt = getHasAvailableNoStandardLmt(app);
        log.info("【checkIsLargeLmt】 申请【"+serno+"】的客户【"+app.getCusId()+"】是否存在有效的非标额度hasActiveNoStandardLmt【{}】-------->  ",hasActiveNoStandardLmt);

        String cusId = app.getCusId();//获取本次申请的申请客户号

        //特定目的投资产品(根据授信申报时选择的”大额主要责任人“和”底层资产判断“)
        log.info("【checkIsLargeLmt】【"+serno+"】本笔业务授信品种lmtBizType【{}】-------->  ",lmtBizType);
        if (CmisBizConstants.TOU_RONG_LEI_UNIQUE.contains(lmtBizType)) {
            //判断 大额责任人 （银行类）
            String largeLmtMainManager = app.getLargeLmtMainManager();
            log.info("【checkIsLargeLmt】【"+serno+"】本笔大额责任人largeLmtMainManager【{}】-------->  ",largeLmtMainManager);
            largeLmt = this.getMinTdmdztDc(largeLmtMainManager,app.getBasicAssetNormal());
        }

        //投融类业务（标准）：2亿
        if (CmisBizConstants.TOU_RONG_LEI_STANDARD.contains(lmtBizType)) {
            largeLmt = 2_0000_0000L;
        }

        //投融类业务（非标）：5000w
        if (CmisBizConstants.TOU_RONG_LEI_NO_STANDARD.contains(lmtBizType)) {
            largeLmt = 5000_0000L;
        }

        //是否为 产品授信中的特定目的载体
        boolean isUniqueAndPrd = CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)&&CmisBizConstants.TOU_RONG_LEI_UNIQUE.contains(lmtBizType);
        //如果有非标业务，则以5000w判断大额（产品授信-特定目的载体 不判断）
        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(hasActiveNoStandardLmt) && !isUniqueAndPrd) {
            largeLmt = 5000_0000L;
        }

        //(对公客户)则判断客户是否有信贷类业务，若有，则按5000万标准值判断
        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(hasLoanBuss) && CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)) {
            largeLmt = 5000_0000L;
        }

        /**
         *  add by zhangjw 20210908判断是否有特定目的载体投资------>资产证券化产品投资（标准）\资产-其他\净值型产品
         *  规则：
         *      法人客户的是否大额最低标准是5000万，如果标准值计算出来已经是5000万了，无需进行此步骤判断；
         *      此步骤判断规则为：
         *          对公客户的大额标准值需要综合本笔业务、是否存在信贷类业务、是否存在非标业务和特定目的载体投资的底层大额标准值进行孰低判断
         */
        if(largeLmt > 5000_0000L && CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){
            List<LmtDto> lmtDtoList = this.getApprSernoTDMD(cusId);
            if(lmtDtoList!=null && lmtDtoList.size()>0){
                Long minTdmdztDc = 0L;//特定目的载体投资的最小大额标准值
                for(LmtDto lmtDto : lmtDtoList){
                    String apprSerno = lmtDto.getApprSerno();
                    if(StringUtils.isBlank(apprSerno)){
                        continue;
                    }else{
                        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByAccNo(apprSerno);
                        if(!StringUtils.isBlank(lmtSigInvestAcc.getLargeLmtMainManager())){
                            minTdmdztDc = this.getMinTdmdztDc(lmtSigInvestAcc.getLargeLmtMainManager(),lmtSigInvestAcc.getBasicAssetNormal());
                            log.info("【checkIsLargeLmt】客户特殊目的载体投资批复台账编号【" + apprSerno + "】 最小大额标准值 【{}】-------->  ", minTdmdztDc);
                        }
                    }
                }

                log.info("【checkIsLargeLmt】大额标准值【{}】 与  客户最小特殊目的载体投资大额标准值 【{}】-------->  ", largeLmt,minTdmdztDc);
                if( largeLmt.compareTo(minTdmdztDc) >= 0){
                    largeLmt = minTdmdztDc;//取最小大额标准值
                }
            }
        }


        log.info("【checkIsLargeLmt】【"+serno+"】本笔业务大额标准值largeLmt【{}】-------->  ",largeLmt);

        //特定载体和对公客户（非同业客户）：使用当前授信金额比较
        //同业客户=》投融类业务标准（非标）：需要汇总客户标准化业务、非标业务授信金额之和+本次授信金额
        /**
         * add by zhangjw 20210903
         * 非同业客户：无论标、非标、或者特定目的载体投资，都需要将客户及其所在集团的授信余额之和累加，与本笔业务申请金额累加，做比较
         *            累加客户授信时，注意，非同业客户，综合授信累加时，不包含低风险额度、穿透化额度、承销类额度；已到期的额度使用用信余额累加
         *  同业客户：标和非标业务，需要汇总客户标准化业务、非标业务授信金额之和+本次授信判断，与最低限制比较
         *          特定目的载体投资，只看当前申报的金额是否超过标准值
         */
        log.info("【checkIsLargeLmt】【"+serno+"】 lmtAmt 【{}】-------->  ",lmtAmt);

        //资产证券化（标准）、资管计划、净值化产品  在判断大额时是单个产品独立判断的，不用和其它业务加总，且其它业务判断大额时也不必加总这三个品种
        if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)) {
            lmtAmt = app.getLmtAmt().add(getCusGrpLmtBalance(app));
            log.info("【checkIsLargeLmt】【" + serno + "】非同业客户累加 客户非低风险、非穿透化、非承销的授信余额 lmtAmt 后 【{}】-------->  ", lmtAmt);
        } else {
            //（产品授信-特定目的载体 不计算）
            if (!CmisBizConstants.TOU_RONG_LEI_UNIQUE.contains(lmtBizType)) {
                lmtAmt = app.getLmtAmt().add(getLmtBal(app));
                log.info("【checkIsLargeLmt】【" + serno + "】同业客户累加 标和非标 授信余额 lmtAmt 后【{}】-------->  ", lmtAmt);
            }
        }

        /**
         * add by zhangjw 20211028  累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额
         */
        CmisLmt0065ReqDto cmisLmt0065ReqDto = new CmisLmt0065ReqDto();
        cmisLmt0065ReqDto.setCusId(cusId);
        cmisLmt0065ReqDto.setLimitSubNo(lmtBizType);
        if(!CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.contains(lmtBizType)){
            //其他场景  根据项目编号匹配  获取项下余额  进行扣减
            cmisLmt0065ReqDto.setProNo(app.getProNo());
        }
        ResultDto<CmisLmt0065RespDto> cmisLmt0065RespDto= cmisLmtClientService.cmislmt0065(cmisLmt0065ReqDto);
        log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额：-----> 返回信息: cmisLmt0065RespDto -------------------------->" + cmisLmt0065RespDto.toString() );
        String cmisLmt0065Code = cmisLmt0065RespDto.getData().getErrorCode();
        if (!"0000".equals(cmisLmt0065Code)) {
            log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额！");
            throw new YuspException(EcbEnum.ECB019999.key, "获取客户本笔授信项下用信余额异常！");
        }
        BigDecimal cmislmt0065LmtAmt = cmisLmt0065RespDto.getData().getLmtAmt() == null ? BigDecimal.ZERO : cmisLmt0065RespDto.getData().getLmtAmt() ;
        String cmislmt0065Status = cmisLmt0065RespDto.getData().getStatus() == null ? "" : cmisLmt0065RespDto.getData().getStatus() ;
        if(cmislmt0065LmtAmt.compareTo(BigDecimal.ZERO) >=0  && cmislmt0065Status.equals(CmisLmtConstants.STD_ZB_APPR_ST_90) ){
            log.error("累加客户存量授信余额时，还需要剔除掉本笔授信（非失效额度）项下用信余额：-----> lmtAmt【"+lmtAmt+"】 cmislmt0065LmtAmt【"+cmislmt0065LmtAmt+"】 " );
            lmtAmt = lmtAmt.subtract(cmislmt0065LmtAmt);
        }

        /**
         * 如果授信申请存在原批复流水号，则查询原批复流水号是否对应有批复台账，如果有且状态为生效，则扣减掉原批复台账金额
         * (产品授信-特定目的载体 不计算  -- lixy 2021年9月28日)
         */
        String origiLmtReplySerno = app.getOrigiLmtReplySerno();
        if(!StringUtils.isBlank(origiLmtReplySerno) && !isUniqueAndPrd){
            log.info("【checkIsLargeLmt】【" + serno + "】原-------->  origiLmtReplySerno【{}】", origiLmtReplySerno);
            LmtSigInvestAcc sigAcc = lmtSigInvestAccService.selectByReplySerno(origiLmtReplySerno);
            if(sigAcc!=null && !StringUtils.isBlank(sigAcc.getAccNo())){
                String accStatus = sigAcc.getAccStatus();
                //授信续作  台账生效
                if(CmisCommonConstants.STD_XD_REPLY_STATUS_01.equals(accStatus) && CmisBizConstants.STD_SX_LMT_TYPE_03.equals(app.getAppType())){
                    log.info("【checkIsLargeLmt】【"+serno+"】 剔除客户本笔授信续作原授信金额：-------->"+sigAcc.getLmtAmt());
                    lmtAmt = lmtAmt.subtract(sigAcc.getLmtAmt());
                }
                //授信变更
                if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(app.getAppType())){
                    log.info("【checkIsLargeLmt】【"+serno+"】 剔除客户本笔授信变更原授信金额：-------->"+sigAcc.getLmtAmt());
                    lmtAmt = lmtAmt.subtract(sigAcc.getLmtAmt());
                }

            }
        }

        log.info("【checkIsLargeLmt】【"+serno+"】 判断使用的金额  lmtAmt：-------->"+lmtAmt);

        //判断是否大额授信
        log.info("【checkIsLargeLmt】【{}】判断是否大额授信isLargeLmt-------->  lmtAmt【{}】   largeLmt【{}】", app.getSerno(), lmtAmt, largeLmt);
        if (lmtAmt.compareTo(BigDecimal.valueOf(largeLmt)) >= 0) {
            isLargeLmt = CmisBizConstants.STD_ZB_YES_NO_Y;
        }
        resultMap.put("isLargeLmt", isLargeLmt);

        //判断是否报备董事长(大额值两倍)
        log.info("【checkIsLargeLmt】【{}】判断是否报备董事长isReportChairman-------->  lmtAmt【{}】   largeLmt【{}】", app.getSerno(), lmtAmt, largeLmt);
        if (lmtAmt.compareTo(BigDecimal.valueOf(largeLmt * 2)) >= 0) {
            isReportChairman = CmisBizConstants.STD_ZB_YES_NO_Y;
        }
        resultMap.put("isReportChairman", isReportChairman);

        //是否有信贷类业务
        resultMap.put("hasLoanBuss", hasLoanBuss);

        log.info("【checkIsLargeLmt】【{}】大额授信处理结果--------> end 【{}】", app.getSerno(), JSON.toJSONString(resultMap));
        return resultMap;
    }

    /**
     * 判断当前客户是否存在信贷类业务(对公客户)
     * @param app
     * @return
     */
    public String getHasLoanBuss(LmtSigInvestApp app) {
        //是否有信贷类业务
        String hasLoanBuss = CmisBizConstants.STD_ZB_YES_NO_N;
        //判断是否为对公客户
        if (!CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(app.getCusCatalog())) {
            return hasLoanBuss;
        }

        CmisLmt0019ReqDto cmisLmt0019ReqDto = new CmisLmt0019ReqDto();
        cmisLmt0019ReqDto.setCusId(app.getCusId());
        cmisLmt0019ReqDto.setInstuCde(getInstuCde(app.getInputBrId()));
        cmisLmt0019ReqDto.setIsQuryGrp(CmisBizConstants.STD_ZB_YES_NO_Y);
        cmisLmt0019ReqDto.setQueryType(CmisLmtConstants.CMIS_QUERY_TYPE_01);

        log.info("根据客户号【{}】获取客户是否有有效的综合授信额度调用cmisLmt0019 请求报文--------> start 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0019ReqDto));
        ResultDto<CmisLmt0019RespDto> cmisLmt0019RespDto = cmisLmtClientService.cmisLmt0019(cmisLmt0019ReqDto);
        log.info("根据客户号【{}】获取客户是否有有效的综合授信额度调用cmisLmt0019 响应结果--------> end 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0019RespDto));
        String code = cmisLmt0019RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据客户号【{}】获取客户是否有有效的综合授信额度失败！", app.getCusId());
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0019RespDto.getMessage());
        }
        String result = cmisLmt0019RespDto.getData().getResult();
        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(result)) {
            hasLoanBuss = CmisBizConstants.STD_ZB_YES_NO_Y;
        }
        log.info("根据客户号【{}】获取客户是否有有效的综合授信额度结果-------->【{}】", app.getCusId(), result);

        return hasLoanBuss;
    }

    /**
     * 获取客户 标准化业务+非标业务授信金额 之和
     * @param app
     * @return
     */
    public BigDecimal getLmtBal(LmtSigInvestApp app){
        CmisLmt0043ReqDto cmisLmt0043ReqDto = new CmisLmt0043ReqDto();
        cmisLmt0043ReqDto.setCusId(app.getCusId());
        cmisLmt0043ReqDto.setInstuCde(getInstuCde(app.getInputBrId()));
        log.info("根据客户号【{}】获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）余额cmislmt0043 请求报文--------> start 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0043ReqDto));
        ResultDto<CmisLmt0043RespDto> cmisLmt0043RespDto = cmisLmtClientService.cmislmt0043(cmisLmt0043ReqDto);
        log.info("根据客户号【{}】获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）cmislmt0043 响应结果--------> end 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0043RespDto));
        String code = cmisLmt0043RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据客户号【{}】获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）失败！", app.getCusId());
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0043RespDto.getMessage());
        }
        BigDecimal lmtBal = cmisLmt0043RespDto.getData().getLmtBal();
        log.info("根据客户号【{}】获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）结果-------->【{}】", app.getCusId(), lmtBal);
        return lmtBal;
    }

    /**
     * 获取客户 标准化业务+非标业务授信金额 之和
     * @param app
     * @return
     */
    public BigDecimal getCusGrpLmtBalance(LmtSigInvestApp app){
        CmisLmt0032ReqDto cmisLmt0032ReqDto = new CmisLmt0032ReqDto();
        cmisLmt0032ReqDto.setDealBizNo(generatePkId());
        cmisLmt0032ReqDto.setCusId(app.getCusId());
        cmisLmt0032ReqDto.setInstuCde(getInstuCde(app.getInputBrId()));
        cmisLmt0032ReqDto.setIsQuryGrp(CmisLmtConstants.YES_NO_Y);
        cmisLmt0032ReqDto.setIsQueryCth(CmisLmtConstants.YES_NO_N);
        cmisLmt0032ReqDto.setIsQueryDfx(CmisLmtConstants.YES_NO_N);
        cmisLmt0032ReqDto.setIsQueryWt(CmisLmtConstants.YES_NO_Y);
        log.info("根据客户号【{}】获取客户及其集团成员的除低风险、承销、穿透化额度外的授信余额 cmislmt0032 请求报文--------> start 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0032ReqDto));
        ResultDto<CmisLmt0032RespDto> cmisLmt0032RespDto = cmisLmtClientService.cmislmt0032(cmisLmt0032ReqDto);
        log.info("根据客户号【{}】获取客户及其集团成员的除低风险、承销、穿透化额度外的授信余额 cmislmt0032 响应结果--------> end 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0032RespDto));
        String code = cmisLmt0032RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据客户号【{}】获取客户及其集团成员的单笔投资业务授信余额失败！", app.getCusId());
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0032RespDto.getMessage());
        }
        BigDecimal lmtBalanceAmt = cmisLmt0032RespDto.getData().getLmtBalanceAmt();
        log.info("根据客户号【{}】获取客户及其集团成员的除低风险、承销、穿透化额度外的授信余额 结果-------->【{}】", app.getCusId(), lmtBalanceAmt);
        return lmtBalanceAmt;
    }

    /**
     * 查询客户是否存在有效的非标额度
     * @param app
     * @return
     */
    public String getHasAvailableNoStandardLmt(LmtSigInvestApp app){
        CmisLmt0031ReqDto cmisLmt0031ReqDto = new CmisLmt0031ReqDto();
        cmisLmt0031ReqDto.setCusId(app.getCusId());
        cmisLmt0031ReqDto.setDealBizNo(app.getSerno());
        log.info("根据客户号【{}】查询客户及其集团成员是否存在有效的非标额度调用cmislmt0031 请求报文--------> start 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0031ReqDto));
        ResultDto<CmisLmt0031RespDto> cmisLmt0031RespDto = cmisLmtClientService.cmislmt0031(cmisLmt0031ReqDto);
        log.info("根据客户号【{}】获取客户获取客户及其集团成员的是否存在有效的非标额度调用cmislmt0031 响应结果--------> end 【{}】", app.getCusId(), JSON.toJSONString(cmisLmt0031RespDto));
        String code = cmisLmt0031RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据客户号【{}】查询客户及其集团成员是否存在有效的非标额度失败！", app.getCusId());
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0031RespDto.getMessage());
        }
        String isExists = cmisLmt0031RespDto.getData().getIsExists();
        log.info("根据客户号【{}】查询客户及其集团成员是否存在有效的非标额度结果-------->【{}】", app.getCusId(), isExists);
        return isExists;
    }

    /**
     * 获取客户有效的特定目的载体投资额度批复台账编号
     * @param cusId
     * @return
     */
    public  List<LmtDto> getApprSernoTDMD(String cusId) {
        CmisLmt0055ReqDto cmisLmt0055ReqDto = new CmisLmt0055ReqDto();
        cmisLmt0055ReqDto.setCusId(cusId);

        log.info("根据客户号【{}】获取客户有效的特定目的载体投资额度批复台账编号 cmisLmt0055 请求报文--------> start 【{}】", cusId, JSON.toJSONString(cmisLmt0055ReqDto));
        ResultDto<CmisLmt0055RespDto> cmisLmt0055RespDto = cmisLmtClientService.cmislmt0055(cmisLmt0055ReqDto);
        log.info("根据客户号【{}】获取客户有效的特定目的载体投资额度批复台账编号 cmisLmt0055 响应结果--------> end 【{}】", cusId, JSON.toJSONString(cmisLmt0055RespDto));
        String code = cmisLmt0055RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据客户号【{}】获取客户有效的特定目的载体投资额度批复台账编号失败！", cusId);
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0055RespDto.getMessage());
        }
        List<LmtDto> lmtDtoList = cmisLmt0055RespDto.getData().getLmtDtoList();
        log.info("根据客户号【{}】获取客户是否有有效的综合授信额度结果-------->【{}】", cusId, lmtDtoList);

        return lmtDtoList;
    }

    /**
     * 获取客户有效的特定目的载体投资额度批复台账编号
     * @param largeLmtMainManager,basicAssetNormal
     * @return
     */
    public Long getMinTdmdztDc(String largeLmtMainManager,String basicAssetNormal) {
        Long minTdmdztDc = 0L;
        if(!StringUtils.isBlank(largeLmtMainManager)){
            //大额主责任人类型 银行类机构
            if (CmisBizConstants.STD_ZB_LARGE_MAIN_MANAGER_TYPE_1.equals(largeLmtMainManager)) {
                //底层资产标准值 3亿元
                if (CmisBizConstants.STD_ZB_BASIC_ASSET_NORMAL_TYPE_03.equals(basicAssetNormal)) {
                    minTdmdztDc = CmisBizConstants.STD_ZB_BASIC_ASSET_NORMAL_TYPE_03_VAL;
                }
                //底层资产标准值 5亿元
                else if (CmisBizConstants.STD_ZB_BASIC_ASSET_NORMAL_TYPE_05.equals(basicAssetNormal)) {
                    minTdmdztDc = CmisBizConstants.STD_ZB_BASIC_ASSET_NORMAL_TYPE_05_VAL;
                }
            } //大额主责任人类型 （非金融企业债券投资、非银类机构同业业务） 2亿
            else if (CmisBizConstants.STD_ZB_LARGE_MAIN_MANAGER_TYPE_2.equals(largeLmtMainManager)
                    || CmisBizConstants.STD_ZB_LARGE_MAIN_MANAGER_TYPE_3.equals(largeLmtMainManager)) {
                minTdmdztDc = 2_0000_0000L;
            }
            //大额主责任人类型 （企业信用、非标资产） 5000W
            else if (CmisBizConstants.STD_ZB_LARGE_MAIN_MANAGER_TYPE_4.equals(largeLmtMainManager)
                    || CmisBizConstants.STD_ZB_LARGE_MAIN_MANAGER_TYPE_5.equals(largeLmtMainManager)) {
                minTdmdztDc = 5000_0000L;
            }
        }
        return minTdmdztDc;
    }


    /**
     * 获取金融机构代码
     *
     * @param inputBrId
     * @return
     */
    public String getInstuCde(String inputBrId) {
        String instuCde = CmisCommonConstants.INSTUCDE_001;
        if (inputBrId.startsWith("80")) {
            instuCde = CmisCommonConstants.INSTUCDE_002;
        } else if (inputBrId.startsWith("81")) {
            instuCde = CmisCommonConstants.INSTUCDE_003;
        }
        return instuCde;
    }

    /**
     * 授信续作：
     * 主体授信：选择的是授信批复台账列表时，默认为授信续作
     *
     * @param lmtSigInvestAcc
     * @return
     */
    @Transactional
    public LmtSigInvestApp onXuzuo(Map lmtSigInvestAcc) throws ParseException {
        return optInvestAppByAcc(lmtSigInvestAcc, CmisBizConstants.STD_SX_LMT_TYPE_03);
    }

    /**
     * 授信变更
     *
     * @param lmtSigInvestAcc
     * @return
     */
    @Transactional
    public LmtSigInvestApp onInvestChange(Map lmtSigInvestAcc) throws ParseException {
        return optInvestAppByAcc(lmtSigInvestAcc, CmisBizConstants.STD_SX_LMT_TYPE_02);
    }

    /**
     * 授信复议：
     * 主体授信：从主体授信申请历史列表选择一笔数据发起复议，判断选择的申请数据（以申请流水号为关联）是否关联有授信批复台账，如果有，则将授信批复台账表数据copy一份至授信复议申请表；
     * 如果没有关联授信批复台账，则copy申请流水号关联的授信批复结果表  至  授信复议申请表；授信复议生成新的申请流水号
     *
     * @param lmtSigInvestApp
     * @return
     */
    @Transactional
    public ResultDto<LmtSigInvestApp> onReconside(LmtSigInvestApp lmtSigInvestApp) throws ParseException {
        ResultDto<LmtSigInvestApp> resultDto = new ResultDto<LmtSigInvestApp>();
        int result = 0;
        LmtSigInvestApp newApp = new LmtSigInvestApp();
        LmtSigInvestSubApp newSubApp = new LmtSigInvestSubApp();
        //采用批复表中的serno
        String origiSerno = lmtSigInvestApp.getSerno();
        String origiLmtReplySerno = "";
        String cudId = lmtSigInvestApp.getCusId();
        String lmtBizType = "";
        BigDecimal origiLmtAmt = null;
        Integer origiLmtTerm = null;
        BigDecimal origiRate = null;

        String serno = generateSerno(CmisBizConstants.BIZ_SERNO);
        log.info(String.format("保存主体授信复议申请数据,生成流水号%s", serno));

        try {
            //如果不是否决数据，进行判断该批复是否存在有效占用关系，如果存在不允许复议
            if (CmisFlowConstants.WF_STATUS_997.equals(lmtSigInvestApp.getApproveStatus())) {
                //根据申请流水号查询批复台账信息
                LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectBySerno(lmtSigInvestApp.getSerno());
                if(lmtSigInvestAcc == null){
                    throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_EORROR000028.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000028.value);
                }
                //获取批复台账信息
                String accNo = lmtSigInvestAcc.getAccNo();
                CmisLmt0037ReqDto cmisLmt0037reqDto = new CmisLmt0037ReqDto();
                //查询类型：1 批复台账编号查询
                cmisLmt0037reqDto.setQueryType(CmisBizConstants.FLOW_QUERY_TYPE_1);
                cmisLmt0037reqDto.setAccNo(accNo);
                //调用接口，查询批复向下是否存在有效占用关系
                ResultDto<CmisLmt0037RespDto> cmisLmt0037RespDtoResultDto = cmisLmtClientService.cmislmt0037(cmisLmt0037reqDto);
                //接口结果处理
                if (cmisLmt0037RespDtoResultDto.getCode().equals(SuccessEnum.CMIS_SUCCSESS.key)) {
                    CmisLmt0037RespDto cmisLmt0037RespDto = cmisLmt0037RespDtoResultDto.getData();
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(cmisLmt0037RespDto.getIsExists())) {
                        resultDto.setCode(EcbEnum.ECB020005.key);
                        resultDto.setMessage(EcbEnum.ECB020005.value);
                        return resultDto;
                    }
                }
            }

            /**
             * 判断该项目是否有待发起的或者在途的申请，若存在，则不允许发起复议
             */
            String proNo = lmtSigInvestApp.getProNo(); //获取项目编号
            if(StringUtils.isBlank(proNo)){
                resultDto.setCode(EcbEnum.ECB020045.key);
                resultDto.setMessage(EcbEnum.ECB020045.value);
                return resultDto;
            }
            QueryModel model = new QueryModel();
            model.addCondition("proNo",proNo);
            List<LmtSigInvestApp> onsideAppList = this.selectOnSideAppByModel(model);
            if(onsideAppList!=null && onsideAppList.size()>0){
                LmtSigInvestApp onsideApp = onsideAppList.get(0);
                resultDto.setCode(EcbEnum.ECB020046.key);
                resultDto.setMessage(EcbEnum.ECB020046.value + "申请流水号为："+ onsideApp.getSerno());
                return resultDto;
            }

            /**
             * 校验选中要复议的这笔申请，是否是当前客户最新的批复对应的申请
             */
            String origSerno = lmtSigInvestApp.getSerno();
            QueryModel lmtSigInvestRstQueryMap = new QueryModel();
            lmtSigInvestRstQueryMap.addCondition("serno", origSerno);
            BizInvestCommonService.checkParamsIsNull("serno",origSerno);
            lmtSigInvestRstQueryMap.addCondition("replyStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            lmtSigInvestRstQueryMap.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtSigInvestRst> list  = lmtSigInvestRstService.selectByModel(lmtSigInvestRstQueryMap);
            if(list == null || list.size() <=0){
                resultDto.setCode(EcbEnum.ECB020009.key);
                resultDto.setMessage(EcbEnum.ECB020009.value);
                return resultDto;
            }


            /**
             * 复议发起规则
             * 1.判断当前记录复议是否超过三次，超过不能发起复议
             * 2.判断当前记录的复议记录是否完结，未完结则不能发起复议
             */
            LmtSigInvestApp origilmtSigInvestApp = selectBySerno(origiSerno);
            //获取原批复
            LmtSigInvestRst origiLmtSigInvestRst = lmtSigInvestRstService.selectBySerno(origiSerno);
            if (origilmtSigInvestApp != null && origiLmtSigInvestRst != null) {
                checkCanRestart(lmtSigInvestApp.getSerno(), CmisBizConstants.STD_SX_LMT_TYPE_05, lmtBizType, cudId, origiLmtSigInvestRst, origilmtSigInvestApp.getCusCatalog());
            }

            //根据申请表serno获取台账表记录（有效）
            LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectBySerno(origiSerno);
            if (lmtSigInvestAcc != null) {
                //授信主表
                copyProperties(lmtSigInvestAcc, newApp);
                //授信从表
                copyProperties(lmtSigInvestAcc, newSubApp);
                //获取批复流水号
                origiLmtReplySerno = lmtSigInvestAcc.getReplySerno();
                origiLmtAmt = lmtSigInvestAcc.getLmtAmt();
                origiLmtTerm = lmtSigInvestAcc.getLmtTerm();
                origiRate = lmtSigInvestAcc.getRate();
                lmtBizType = lmtSigInvestAcc.getLmtBizType();

                //复议、续作、变更 相关表复制（台账不存在）
                copyInvestOtherTables(origiSerno, serno, origiLmtReplySerno);
                //复议、变更、续作 （台账表）copy至新的申请表
                copyInvestAccTables(origiLmtReplySerno, serno);
            } else {
                //台账表未找到，从批复表中查找结果
                LmtSigInvestRst replyApp = lmtSigInvestRstService.selectBySerno(origiSerno);
                if (replyApp != null) {
                    //授信主表
                    copyProperties(replyApp, newApp);
                    //授信从表
                    copyProperties(replyApp, newSubApp);
                    //获取批复流水号
                    origiLmtReplySerno = replyApp.getReplySerno();
                    origiLmtAmt = replyApp.getLmtAmt();
                    origiLmtTerm = replyApp.getLmtTerm();
                    origiRate = replyApp.getRate();
                    lmtBizType = replyApp.getLmtBizType();


                    //复议、续作、变更 相关表复制（台账不存在）
                    copyInvestOtherTables(origiSerno, serno, origiLmtReplySerno);
                    //复议、变更、续作 （台账表）copy至新的申请表
                    copyInvestRstTables(origiLmtReplySerno, serno);

                } else {
                    //台账表、批复表 未查找到 抛出异常
                    throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_FUYI_ORIGIINFO_GET_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_FUYI_ORIGIINFO_GET_FAILD.value);
                }
            }

            newApp.setSerno(serno);
            //原授信批复申请流水号
            newApp.setOrigiLmtReplySerno(origiLmtReplySerno);
            //原授信批复金额
            newApp.setOrigiLmtAmt(origiLmtAmt);
            //原授信批复期限
            newApp.setOrigiLmtTerm(origiLmtTerm);
            //TODO 增加原授信批复利率
            newApp.setOrigiRate(origiRate);
            //修改申请状态为未发起
            newApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
            //修改操作状态为新增
            newApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //修改业务类型为复议
            newApp.setAppType(CmisBizConstants.STD_SX_LMT_TYPE_05);

            newApp.setManagerId(getCurrentUser().getLoginCode());
            newApp.setManagerBrId(getCurrentUser().getOrg().getCode());
            //初始化(新增)通用domain信息
            initInsertDomainProperties(newApp);

            //初始化 必填信息校验页面
            initMustCheckItem(newApp);

            result = lmtSigInvestAppMapper.insert(newApp);

            if (result == 1) {

                //获取原申请中的交易结构、交易市场
                LmtSigInvestSubApp subApp = lmtSigInvestSubAppService.selectBySerno(origiSerno);
                if (subApp != null) {
                    copyProperties(subApp, newSubApp);
                }
                //批复从表
                newSubApp.setSerno(serno);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newSubApp);

                lmtSigInvestSubAppService.insert(newSubApp);

                //获取企业信息 、copy原主体分析、获取额度相关信息
                copyRelCusInfo(origiSerno,serno,lmtSigInvestApp.getCusId(),
                        lmtSigInvestApp.getCusType(),lmtSigInvestApp.getCusCatalog(),true);

                //申请类型为授信复议，主体授信-授信复议-授信复议申请书
//                lmtRediDetailService.insertLmtRediDetail(serno);
                LmtReconsideDetail lmtReconsideDetail = new LmtReconsideDetail();
                lmtReconsideDetail.setLmtSerno(serno);
                initInsertDomainProperties(lmtReconsideDetail);
                lmtReconsideDetailService.insert(lmtReconsideDetail);
            }
        } catch (Exception e) {
            log.error("生成复议申请出现异常！==》", e);
            if (e instanceof BizException || e instanceof ParseException ) {
                throw e;
            }
            result = 0;
        }
        if (result == 0) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_FUYI_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_FUYI_FAILD.value);
        }
        resultDto.setData(newApp);
        return resultDto;
    }


    /**
     * （复议、续作、变更）发起规则检测
     * NEW:
     * 1. 同业客户 需要同时判断同业客户综合授信中是否存在审核中的数据
     * 2. 判断当前客户是否存在正在流转中的记录
     * 3. 复议条件判断
     * 4. 授信新增：判断当前客户授信类型 是否存在新增记录(不包含 自行退出、) 不存在 可以新增
     *
     * @param origSerno
     * @param appType
     * @param lmtBizType
     * @param cusId
     * @param lmtSigInvestRst
     * @param cusCatalog
     * @return
     */
    private int checkCanRestart(String origSerno, String appType, String lmtBizType, String cusId, LmtSigInvestRst lmtSigInvestRst, String cusCatalog) throws ParseException {
        int result = 1;

        String startDate = lmtSigInvestRst != null ? lmtSigInvestRst.getReplyInureDate() : null;
        String origiReplySerno = lmtSigInvestRst != null ? lmtSigInvestRst.getReplySerno() : null;

        //1. 同业客户 需要同时判断同业客户综合授信中是否存在审核中的数据
        if (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)) {
            boolean isExistOnWayApp = lmtIntbankAppService.judgeIsExistOnWayApp(cusId);
            if (isExistOnWayApp) {
                throw BizException.error(null, EcbEnum.ECB020003.key, EcbEnum.ECB020003.value);
            }
        }

        //2. 判断当前客户是否存在正在流转中的记录
        List<LmtSigInvestApp> lmtSigInvestApps1 = selectRunningInvestListByCusId(cusId);
        if (lmtSigInvestApps1 != null && lmtSigInvestApps1.size() > 0) {
            for (LmtSigInvestApp lmtSigInvestApp : lmtSigInvestApps1) {
                //判断流转中的数据是否为当前新增的业务类型
                if (lmtBizType.equals(lmtSigInvestApp.getLmtBizType())) {
                    throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_REAPPLY_TODO.key, EclEnum.LMT_SIG_INVESTAPP_REAPPLY_TODO.value);
                }
            }
        }

        //3. 复议条件判断
        if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(appType) && startDate != null) {
            //授信复议可发起天数
            int allowDelayDays = 0;
            //获取复议天数
            String allowDelayDaysSys = getSysParameterByName("FY_DAYS");
            if (!StringUtils.isBlank(allowDelayDaysSys)) {
                allowDelayDays = Integer.parseInt(allowDelayDaysSys);
                if (allowDelayDays == 0) {
                    throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000022.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000022.value);
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = DateUtils.addDay(simpleDateFormat.parse(startDate), allowDelayDays);
                //复议最后日期
                int deadLine = Integer.parseInt(DateUtils.formatDate(date, "yyyyMMdd"));
                //当前日期
                int curDay = Integer.parseInt(getCurrrentDateStr().replaceAll("-",""));
                if (deadLine < curDay) {
                    throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000023.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000023.value + allowDelayDays + "天！");
                }
            }

            //授信复议次数
            int allowFuyiCount = 3;
            //获取复议次数
            String fuiyiCount = "1";//getSysParameterByName("FY_COUNT");
            if (!StringUtils.isBlank(fuiyiCount)) {
                allowFuyiCount = Integer.parseInt(fuiyiCount);
            }

            //获取当前申请发起的复议记录
            String realOriginReplySerno = findRealOriginLmtSigInvestRst(origiReplySerno);
            Integer fuyiCount = getFyCount(realOriginReplySerno);
            if (fuyiCount >= allowFuyiCount) {
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_FUYI.key, EclEnum.LMT_SIG_INVESTAPP_FUYI.value + allowFuyiCount + "次！");
            }
        }

        //4. 授信新增：判断当前客户授信类型 是否存在新增记录(不包含 自行退出、) 不存在 可以新增
        // ===>债券池新增通过后 只能续作 其余类型不做限制
        if (CmisBizConstants.STD_SX_LMT_TYPE_01.equals(appType) && CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtBizType)) {
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);
            BizInvestCommonService.checkParamsIsNull("cusId",cusId);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
            queryModel.addCondition("lmtBizType", lmtBizType);
            BizInvestCommonService.checkParamsIsNull("lmtBizType",lmtBizType);
            queryModel.addCondition("appType", CmisBizConstants.STD_SX_LMT_TYPE_01);
            //是否存在新增 默认不存在
            boolean isExist = false;
            //获取当前客户和业务类型 的新增记录
            List<LmtSigInvestApp> lmtSigInvestApps = selectByModel(queryModel);
            if (lmtSigInvestApps != null && lmtSigInvestApps.size() > 0) {
                for (LmtSigInvestApp lmtSigInvestApp : lmtSigInvestApps) {
                    //判断当前记录是否为自行退出、否决      update by lizx 审批通过了，有有效额度进行续作，没有进行新增，此处不应该拦截
                    if (CmisBizConstants.APPLY_STATE_QUIT.equals(lmtSigInvestApp.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_REFUSE.equals(lmtSigInvestApp.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_PASS.equals(lmtSigInvestApp.getApproveStatus())) {
                        continue;
                    }
                    //存在新增记录
                    isExist = true;
                }
            }
            //存在新增记录
            if (isExist) {
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000007.key, EclEnum.LMT_SIG_INVESTAPP_EORROR000007.value);
            }
        }
        return result;
    }



    /**
     * 获取当前客户正在流转中的授信记录
     *
     * @param cusId
     * @return
     */
    public List<LmtSigInvestApp> selectRunningInvestListByCusId(String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("listtype", CmisBizConstants.LIST_TYPE.APPLY);
        queryModel.addCondition("cusId", cusId);
        BizInvestCommonService.checkParamsIsNull("cusId",cusId);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectByModel(queryModel);
    }

    /**
     * 更新对应字段
     *
     * @param lmtSigInvestApp
     * @return
     */
    public int updateByKeyAndVal(LmtSigInvestApp lmtSigInvestApp) {
        String serno = lmtSigInvestApp.getSerno();
        LmtSigInvestApp lmtSigInvestApp1 = selectBySerno(serno);
        if (lmtSigInvestApp1 != null) {
            lmtSigInvestApp.setPkId(lmtSigInvestApp1.getPkId());
            return updateSelective(lmtSigInvestApp);
        }
        return 0;
    }

    /**
     * 存量业务tabs显示状态
     *
     * @param condition
     * @return
     */
    public Map cunLiangYeWuTabsStatus(Map condition) {
        Map result = new HashMap();
        String serno = (String) condition.get("serno");
        //tab2(机构归属集团存量授信) 默认不显示
        boolean tab2 = false;
        LmtSigInvestApp lmtSigInvestApp = selectBySerno(serno);
        if (lmtSigInvestApp != null) {
            String cusId = lmtSigInvestApp.getCusId();
            //判断是否属于法人客户
            if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtSigInvestApp.getCusCatalog())) {
                //判断是否属于集团客户 判断法人客户是否为集团成员
                LmtAppRelGrpLimit grpLmtInfoBySerno = lmtAppRelGrpLimitService.getGrpLmtInfoBySerno(serno);
                if (grpLmtInfoBySerno != null && !StringUtils.isBlank(grpLmtInfoBySerno.getGrpCusId())) {
                    tab2 = true;
                }
            }
        }
        result.put("tab1", true);
        result.put("tab2", tab2);
        return result;
    }

    /**
     * 资金同业准入申请(通过、否决 录入批复表中)
     *
     * @param lmtSigInvestApp
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterEnd(LmtSigInvestApp lmtSigInvestApp, String approveStatus, ResultInstanceDto resultInstanceDto) throws Exception {
        log.info("单笔投资授信流程审批处理结束开始.....");
        String noticeType = CmisBizConstants.STD_WB_NOTICE_TYPE_2;
        if (CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)) {
            this.flowPassHandleAfterEnd(lmtSigInvestApp, resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId());
        } else {
            this.flowRefuseHandleAfterEnd(lmtSigInvestApp);
            noticeType = CmisBizConstants.STD_WB_NOTICE_TYPE_3;
        }

        lmtSigInvestApp.setApproveStatus(approveStatus);
        this.update(lmtSigInvestApp);
        String result = "否决";
        if(CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
            result = "通过";
        }
        //推送首页提醒事项 add by zhangjw 20210630
        this.sendWbMsgNotice(lmtSigInvestApp, noticeType,
                resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId(),result);
        log.info("单笔投资授信流程审批处理结束结束.....");
    }

    /**
     * 单笔投资授信申请(同意 录入批复表台账表中)
     *
     * @param lmtSigInvestApp
     */
    @Transactional(rollbackFor = Exception.class)
    public void flowPassHandleAfterEnd(LmtSigInvestApp lmtSigInvestApp, String currentUserId, String currentOrgId) throws Exception {
        log.info("通过逻辑处理开始.................start");
        //01 授信新增 02 授信变更 03 授信续作 05 授信复议
        String lmtType = lmtSigInvestApp.getAppType();
        /**
         * 补充逻辑：
         *      如果申请类型是05-授信复议，则关联查找真正的授信申请类型是什么
         * add by zhangjw 20210722
         */
        if (CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(lmtType)) {
            lmtType = bizCommonService.getLmtSigInvestRelLmtType(lmtSigInvestApp);
        }

        /**4001 债券池
         *     债券池授信新增、授信续作时，要将符合条件的债券投资额度纳入到债券池授信项下
         *     此方法：将符合条件的债券投资额度状态置为失效已结清
         */
        String lmtBizType = lmtSigInvestApp.getLmtBizType();
        if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtBizType)) {
            this.generateSigInvestToPool(lmtSigInvestApp);
        }
        //原授信台账编号
        String origiAccNo = "";
        /* 如果是授信复议，获取原授信批复台账*/
        if (CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(lmtType)) {
            origiAccNo = bizCommonService.getLmtSigInvestRelAccNo(lmtSigInvestApp.getOrigiLmtReplySerno());
        }

        //获取生申请流水号
        String serno = lmtSigInvestApp.getSerno();

        //如果是授信变更，则调用更新操作
        if (CmisLmtConstants.STD_SX_LMT_TYPE_02.equals(lmtType)) {
            updateLmtSigInvestAcc(lmtSigInvestApp, serno);
        } else {
            //授信续作，原协议对象的台账信息更新为失效已结清
            if (CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(lmtType)) {
                origiAccNo = extractedLoseLmtSIAcc(lmtSigInvestApp);
            }
            //新增操作
            insertLmtSigInvestAcc(lmtSigInvestApp, serno);
        }

        lmtSigInvestAccService.lmtSigInvestSendCmisLmt0005(serno, lmtType, origiAccNo);
        log.info("通过逻辑处理开始.................start");
    }

    /**
     * @param lmtSigInvestApp
     * @param serno
     * @throws Exception
     * @方法名称：insertLmtSigInvestAcc
     * @方法描述：资金同业新增操作操作
     */
    private void insertLmtSigInvestAcc(LmtSigInvestApp lmtSigInvestApp, String serno) throws Exception {
        log.info("资金同业新增操作操作.................start");
        //根据申请流水号查询最新的
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectBySerno(serno);

        /**单笔投资授信申请审批,根据单笔投资授信申请审批表 生成 同业授信批复表（lmt_sig_invest_rst） 同业授信台账表（lmt_sig_invest_acc） 信息**/
        if (lmtSigInvestAppr == null) {
            throw new Exception(EclEnum.ECL070077.value);
        }

        //审批流水号
        String approveSerno = lmtSigInvestAppr.getApproveSerno();

        //生成批复主表信息
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.initLmtSigInvestRstInfo(lmtSigInvestAppr);
        lmtSigInvestRstMapper.insert(lmtSigInvestRst);

        //批复流水号
        String replySerno = lmtSigInvestRst.getReplySerno();
        //生成台账信息
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.initLmtSigInvestAccInfo(lmtSigInvestAppr);
        lmtSigInvestAcc.setReplySerno(replySerno);
        //插入台账数据信息
        lmtSigInvestAccMapper.insert(lmtSigInvestAcc);

        String accNo = lmtSigInvestAcc.getAccNo();

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_rst  **/
        //获取审批或者申请中的 单笔投资对应底层资产基本情况 信息 LmtSigInvestBasicInfoAppr
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno);
        if (lmtSigInvestBasicInfoAppr != null) {
            initLmtSigInvestBasicInfo(lmtSigInvestBasicInfoAppr, replySerno, accNo);
        }

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_sub_rst 单笔投资关联底层信息明细台账表 lmt_sig_invest_basic_info_sub_acc **/
        //单笔投资关联底层信息明细审批表 lmt_sig_invest_basic_info_sub_appr
        /**底层授信额度批复表 lmt_sig_invest_basic_lmt_rst **/
        //获取审批或申请中 底层授信额度批复表 信息 初始化批复底层授信额度批复表
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno);
        if (lmtSigInvestBasicInfoSubApprList != null && lmtSigInvestBasicInfoSubApprList.size() > 0) {
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr : lmtSigInvestBasicInfoSubApprList) {
                initLmtSigInvestBasicSubInfo(sigInvestBasicInfoSubAppr, replySerno, accNo);
            }
        }

        /**
         * 拷贝用信条件
         */
        List<LmtApprLoanCond> lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByApprSerno(approveSerno);
        if (CollectionUtils.isNotEmpty(lmtApprLoanConds)){
            for (LmtApprLoanCond lmtApprLoanCond : lmtApprLoanConds) {
                //生成批复信息
                LmtReplyLoanCond lmtReplyLoanCond = new LmtReplyLoanCond();
                BeanUtils.copyProperties(lmtApprLoanCond, lmtReplyLoanCond);
                lmtReplyLoanCond.setPkId(generatePkId());
                lmtReplyLoanCond.setReplySerno(replySerno);
                lmtReplyLoanCondService.insert(lmtReplyLoanCond);

                //生成台账信息
                LmtReplyAccLoanCond lmtReplyAccLoanCond = new LmtReplyAccLoanCond();
                BeanUtils.copyProperties(lmtReplyLoanCond, lmtReplyAccLoanCond);
                lmtReplyAccLoanCond.setPkId(generatePkId());
                lmtReplyAccLoanCond.setAccNo(accNo);
                lmtReplyAccLoanCondService.insert(lmtReplyAccLoanCond);

                log.info("生成其他条件信息:" + lmtReplyLoanCond.toString());
            }
        }
        // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
        //lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(approveSernoNew, lmtSigInvestBasicInfoApprOld.getApproveSerno());
        log.info("资金同业新增操作操作.................start");
    }

    /**
     * @param lmtSigInvestApp
     * @param serno
     * @throws Exception
     * @方法名称：updateLmtSigInvestAcc
     * @方法描述：资金同业变更操作
     */
    private void updateLmtSigInvestAcc(LmtSigInvestApp lmtSigInvestApp, String serno) throws Exception {
        log.info("资金同业变更操作{}---------------strart", serno);
        //根据申请流水号查询最新的
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectBySerno(serno);

        /**单笔投资授信申请审批,根据单笔投资授信申请审批表 生成 同业授信批复表（lmt_sig_invest_rst） 同业授信台账表（lmt_sig_invest_acc） 信息**/
        if (lmtSigInvestAppr == null) {
            throw new Exception(EclEnum.ECL070077.value);
        }
        //审批流水号
        String approveSerno = lmtSigInvestAppr.getApproveSerno();
        //生成批复主表信息
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.initLmtSigInvestRstInfo(lmtSigInvestAppr);
        lmtSigInvestRstMapper.insert(lmtSigInvestRst);

        //批复流水号
        String replySerno = lmtSigInvestRst.getReplySerno();
        //原批复编号信息
        String origiLmtReplySerno = lmtSigInvestAppr.getOrigiLmtReplySerno();
        //生成台账信息
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.updaetLmtSigInvestAccInfo(lmtSigInvestAppr, origiLmtReplySerno,lmtSigInvestAppr.getAppType());
        lmtSigInvestAcc.setReplySerno(replySerno);
        //插入台账数据信息
        lmtSigInvestAccMapper.updateByPrimaryKey(lmtSigInvestAcc);

        String accNo = lmtSigInvestAcc.getAccNo();

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_sub_rst 单笔投资关联底层信息明细台账表 lmt_sig_invest_basic_info_sub_acc **/
        //单笔投资关联底层信息明细审批表 lmt_sig_invest_basic_info_sub_appr
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno);
        if (CollectionUtils.isNotEmpty(lmtSigInvestBasicInfoSubApprList)) {
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr : lmtSigInvestBasicInfoSubApprList) {
                //生成批复结果 - 底层明细信息
                LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst = lmtSigInvestBasicInfoSubRstService.initLmtSigInvestBasicInfoSubRstInfo(sigInvestBasicInfoSubAppr);
                lmtSigInvestBasicInfoSubRst.setReplySerno(replySerno);
                lmtSigInvestBasicInfoSubRstService.insert(lmtSigInvestBasicInfoSubRst) ;
                String basicReplySerno = lmtSigInvestBasicInfoSubRst.getBasicReplySerno() ;
                //根据批复信息更新 台账信息  --授信变更
                LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc = lmtSigInvestBasicInfoSubAccService.initLmtSigInvestBasicInfoSubAccInfo(sigInvestBasicInfoSubAppr, true);
                lmtSigInvestBasicInfoSubAcc.setAccNo(accNo);
                lmtSigInvestBasicInfoSubAcc.setReplySerno(replySerno);
                lmtSigInvestBasicInfoSubAcc.setBasicReplySerno(basicReplySerno);
                lmtSigInvestBasicInfoSubAccService.update(lmtSigInvestBasicInfoSubAcc) ;

                //是否申报底层额度，是的话进行资产额度处理
                if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(sigInvestBasicInfoSubAppr.getIsAppBasicLmt())){
                    //底层审批流水号
                    //底层申请流水号
                    String basicSerno = sigInvestBasicInfoSubAppr.getBasicSerno() ;
                    LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = lmtSigInvestBasicLmtApprService.selectByBasicSerno(basicSerno) ;
                    LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst =
                            lmtSigInvestBasicLmtRstService.initLmtSigInvestBasicLmtRstInfo(lmtSigInvestBasicLmtAppr) ;
                    lmtSigInvestBasicLmtRst.setReplySerno(replySerno);
                    lmtSigInvestBasicLmtRst.setBasicReplySerno(basicReplySerno);
                    lmtSigInvestBasicLmtRstService.insert(lmtSigInvestBasicLmtRst) ;

                    //处理台账底层关系
                    String origibasicAccNo = sigInvestBasicInfoSubAppr.getOrigiBasicAccNo() ;
                    LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc = lmtSigInvestBasicLmtAccService.initLmtSigInvestBasicLmtAccInfo(lmtSigInvestBasicLmtRst) ;
                    LmtSigInvestBasicLmtAcc origiLmtSigInvestBasicLmtAcc = lmtSigInvestBasicLmtAccService.selectBasicAccNo(origibasicAccNo) ;
                    if (origiLmtSigInvestBasicLmtAcc != null) {
                        lmtSigInvestBasicLmtAcc.setPkId(origiLmtSigInvestBasicLmtAcc.getPkId());
                        lmtSigInvestBasicLmtAcc.setAccNo(origiLmtSigInvestBasicLmtAcc.getAccNo());
                        lmtSigInvestBasicLmtAcc.setBasicAccNo(origiLmtSigInvestBasicLmtAcc.getBasicAccNo());
                    }
                    lmtSigInvestBasicLmtAcc.setBasicReplySerno(basicReplySerno);
                    lmtSigInvestBasicLmtAccService.update(lmtSigInvestBasicLmtAcc) ;
                }
            }
        }

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_rst  **/
        //获取审批或者申请中的 单笔投资对应底层资产基本情况 信息 LmtSigInvestBasicInfoAppr
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno);
        //根据批复编号，删除数据
        if (lmtSigInvestBasicInfoAppr != null) {
            //根据审批表中的 单笔投资对应底层资产基本情况 生成批复表中对应的数据
            initLmtSigInvestBasicInfoRst(lmtSigInvestBasicInfoAppr, replySerno, accNo) ;
            //根据台账编号查询
            LmtSigInvestBasicInfoAcc origiLmtSigInvestBasicInfoAcc = lmtSigInvestBasicInfoAccService.selectByAccNo(accNo) ;
            LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = initLmtSigInvestBasicInfoAcc(lmtSigInvestBasicInfoAppr, replySerno, accNo) ;
            String pkId = origiLmtSigInvestBasicInfoAcc.getPkId() ;
            lmtSigInvestBasicInfoAcc.setPkId(pkId);
            lmtSigInvestBasicInfoAccService.update(lmtSigInvestBasicInfoAcc) ;
        }
        log.info("资金同业变更操作{}---------------end", serno);
    }

    /**
     * @param lmtSigInvestApp
     * @方法名：extractedLoseLmtSIAcc
     * @描述：复议、续作操作，失效原先台账
     */
    private String extractedLoseLmtSIAcc(LmtSigInvestApp lmtSigInvestApp) throws Exception {
        String origiLmtReplySerno = lmtSigInvestApp.getOrigiLmtReplySerno();

        //失效原授信批复  add by zhangjw 20210722
        lmtSigInvestRstService.invalidatedOldAppr(origiLmtReplySerno);

        //不根据原批复编号，查找对应的台账主表信息以及台账分项信息，并且更改其状态
        //直接获取最新的批复台账，因为原批复信息可能审批为否决，不存在批复台账
        QueryModel model = new QueryModel();
        model.addCondition("cusId", lmtSigInvestApp.getCusId());
        BizInvestCommonService.checkParamsIsNull("cusId",lmtSigInvestApp.getCusId());
        model.addCondition("proNo", lmtSigInvestApp.getProNo());
        BizInvestCommonService.checkParamsIsNull("proNo",lmtSigInvestApp.getProNo());
        model.addCondition("accStatus", CmisBizConstants.STD_REPLY_STATUS_01);
        model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestAcc> lmtSigInvestAccs = lmtSigInvestAccService.selectByModel(model);
        if (lmtSigInvestAccs == null || lmtSigInvestAccs.size() <= 0) {
            throw new Exception("获取原批复台账信息异常");
        }
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccs.get(0);
        lmtSigInvestAcc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_02);
        lmtSigInvestAccMapper.updateByPrimaryKey(lmtSigInvestAcc);

        List<LmtSigInvestBasicLmtAcc> lmtSigInvestBasicLmtAccList = lmtSigInvestBasicLmtAccService.selectByReplySerno(lmtSigInvestAcc.getReplySerno());
        for (LmtSigInvestBasicLmtAcc sigInvestBasicLmtAcc : lmtSigInvestBasicLmtAccList) {
            sigInvestBasicLmtAcc.setAccStatus(CmisLmtConstants.STD_REPLY_STATUS_02);
            lmtSigInvestBasicLmtAccMapper.updateByPrimaryKey(sigInvestBasicLmtAcc);
        }

        return lmtSigInvestAcc.getAccNo();
    }

    /**
     * 单笔投资授信申请(拒绝录入批复表中)
     *
     * @param lmtSigInvestApp
     */
    @Transactional(rollbackFor = Exception.class)
    public void flowRefuseHandleAfterEnd(LmtSigInvestApp lmtSigInvestApp) {
        List<LmtIntbankAccSub> lmtIntbankAccSubs = new ArrayList<LmtIntbankAccSub>();
        //获取本次申请类型
        //01 授信新增 02 授信变更 03 授信续作 05 授信复议
        String lmtType = lmtSigInvestApp.getAppType();
        //获取生申请流水号
        String serno = lmtSigInvestApp.getSerno();
        //根据申请流水号查询最新的
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectBySerno(serno);

        String approveSerno = lmtSigInvestAppr.getApproveSerno();

        /**单笔投资授信申请审批表为空 则证明该申请中无 审批表信息， 数据从申请表中产生
         * 根据单笔投资授信申请审批表 生成 同业授信批复表（lmt_sig_invest_rst） 同业授信台账表（lmt_sig_invest_acc） 信息
         * 根据单笔投资授信申请审批表 生成 同业授信批复表（lmt_sig_invest_rst） 同业授信台账表（lmt_sig_invest_acc） 信息**/
        //生成批复主表信息
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.initLmtSigInvestRstInfo(lmtSigInvestAppr);
        lmtSigInvestRstMapper.insert(lmtSigInvestRst);

        //批复流水号
        String replySerno = lmtSigInvestRst.getReplySerno();
        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_sub_rst 单笔投资关联底层信息明细台账表 lmt_sig_invest_basic_info_sub_acc **/
        //单笔投资关联底层信息明细审批表 lmt_sig_invest_basic_info_sub_appr
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno);
        if (CollectionUtils.isNotEmpty(lmtSigInvestBasicInfoSubApprList)) {
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr : lmtSigInvestBasicInfoSubApprList) {
                LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst =
                        lmtSigInvestBasicInfoSubRstService.initLmtSigInvestBasicInfoSubRstInfo(sigInvestBasicInfoSubAppr);
                //批复流水号
                lmtSigInvestBasicInfoSubRst.setReplySerno(replySerno);
                lmtSigInvestBasicInfoSubRstMapper.insert(lmtSigInvestBasicInfoSubRst);
            }
        }

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_rst  **/
        //获取审批或者申请中的 单笔投资对应底层资产基本情况 信息 LmtSigInvestBasicInfoAppr
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno);
        //根据审批表中的 单笔投资对应底层资产基本情况 生成批复表中对应的数据
        if (lmtSigInvestBasicInfoAppr != null) {
            LmtSigInvestBasicInfoRst lmtSigInvestBasicInfoRst = lmtSigInvestBasicInfoRstService.initLmtSigInvestBasicInfoRstInfo(lmtSigInvestBasicInfoAppr);
            //批复流水赋值
            lmtSigInvestBasicInfoRst.setReplySerno(replySerno);
            lmtSigInvestBasicInfoRstMapper.insert(lmtSigInvestBasicInfoRst);
        }

        /**底层授信额度批复表 lmt_sig_invest_basic_lmt_rst **/
        //获取审批或申请中 底层授信额度批复表 信息 初始化批复底层授信额度批复表
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprList = lmtSigInvestBasicLmtApprService.selectByApproveSerno(approveSerno);
        if (CollectionUtils.isNotEmpty(lmtSigInvestBasicLmtApprList)) {
            for (LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr : lmtSigInvestBasicLmtApprList) {
                //根据审批表中的 底层授信额度批复表 生成批复表中对应的数据
                LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst = lmtSigInvestBasicLmtRstService.initLmtSigInvestBasicLmtRstInfo(lmtSigInvestBasicLmtAppr);
                //批复流水号
                lmtSigInvestBasicLmtRst.setReplySerno(replySerno);
                lmtSigInvestBasicLmtRstMapper.insert(lmtSigInvestBasicLmtRst);
            }
        }

        /**
         * 拷贝用信条件
         */
        List<LmtApprLoanCond> lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByApprSerno(approveSerno);
        if (CollectionUtils.isNotEmpty(lmtApprLoanConds)){
            for (LmtApprLoanCond lmtApprLoanCond : lmtApprLoanConds) {
                //生成批复信息
                LmtReplyLoanCond lmtReplyLoanCond = new LmtReplyLoanCond();
                BeanUtils.copyProperties(lmtApprLoanCond, lmtReplyLoanCond);
                lmtReplyLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyLoanCond.setReplySerno(replySerno);
                lmtReplyLoanCondService.insert(lmtReplyLoanCond);

                log.info("生成其他条件信息:" + lmtReplyLoanCond.toString());
            }
        }
    }

    /**
     * 初始化单笔投资关联底层信息明细批复表 和 单笔投资关联底层信息明细台账表 ，并插入数据
     *
     * @param subAppBean
     * @param replySerno
     */
    public void initLmtSigInvestBasicSubInfo(LmtSigInvestBasicInfoSubAppr subAppBean, String replySerno, String accNo) throws Exception {
        LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst =
                lmtSigInvestBasicInfoSubRstService.initLmtSigInvestBasicInfoSubRstInfo(subAppBean);
        //批复流水号
        lmtSigInvestBasicInfoSubRst.setReplySerno(replySerno);
        lmtSigInvestBasicInfoSubRstMapper.insert(lmtSigInvestBasicInfoSubRst);

        //创建台账信息
        LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc =
                lmtSigInvestBasicInfoSubAccService.initLmtSigInvestBasicInfoSubAccInfo(subAppBean, false);
        String basicReplySerno = lmtSigInvestBasicInfoSubRst.getBasicReplySerno() ;

        lmtSigInvestBasicInfoSubAcc.setReplySerno(replySerno);
        //台账编号
        lmtSigInvestBasicInfoSubAcc.setAccNo(accNo);
        //底层批复流水号
        lmtSigInvestBasicInfoSubAcc.setBasicReplySerno(basicReplySerno);
        lmtSigInvestBasicInfoSubAccMapper.insert(lmtSigInvestBasicInfoSubAcc);

        //是否生成穿透化额度，如果是，则生成底层额度信息
        if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(subAppBean.getIsAppBasicLmt())){
            //底层申请流水号
            String basicSerno = subAppBean.getBasicSerno() ;
            String basicAccNo = lmtSigInvestBasicInfoSubAcc.getBasicAccNo() ;
            //获取底层对应资产额度信息
            LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = lmtSigInvestBasicLmtApprService.selectByBasicSerno(basicSerno) ;
            if(lmtSigInvestBasicLmtAppr==null){
                throw new Exception("生成底层额度信息，未查询到底层申请流水号【"+basicSerno+"】对应的底层资产额度信息") ;
            }
            //初始化批复信息
            LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst =
                    lmtSigInvestBasicLmtRstService.initLmtSigInvestBasicLmtRstInfo(lmtSigInvestBasicLmtAppr);
            lmtSigInvestBasicLmtRst.setBasicReplySerno(basicReplySerno);
            lmtSigInvestBasicLmtRst.setReplySerno(replySerno);
            lmtSigInvestBasicLmtRstService.insert(lmtSigInvestBasicLmtRst) ;

            //初始化台账信息
            LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc =
                    lmtSigInvestBasicLmtAccService.initLmtSigInvestBasicLmtAccInfo(lmtSigInvestBasicLmtRst);
            lmtSigInvestBasicLmtAcc.setAccNo(accNo);
            lmtSigInvestBasicLmtAcc.setBasicReplySerno(basicReplySerno);
            lmtSigInvestBasicLmtAcc.setBasicAccNo(basicAccNo);
            lmtSigInvestBasicLmtAcc.setReplySerno(replySerno);
            lmtSigInvestBasicLmtAccService.insert(lmtSigInvestBasicLmtAcc) ;
        }
    }

    /**
     * 初始化单笔投资授信批复表 和 单笔投资授信台账表 ，并插入数据
     *
     * @param sigInvestBean
     * @param replySerno
     */
    public void initLmtSigInvestBasicInfo(Object sigInvestBean, String replySerno, String accNo) {
        //根据审批表中的 单笔投资对应底层资产基本情况 生成批复表中对应的数据
        initLmtSigInvestBasicInfoRst(sigInvestBean, replySerno, accNo) ;
        //获取审批或者申请中的 单笔投资对应底层资产基本情况台账表 lmt_sig_invest_basic_info_acc
        LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = initLmtSigInvestBasicInfoAcc(sigInvestBean, replySerno, accNo) ;
        lmtSigInvestBasicInfoAccService.insert(lmtSigInvestBasicInfoAcc);
    }

    /**
     * 初始化单笔投资授信批复表 和 单笔投资授信台账表 ，并插入数据
     *
     * @param sigInvestBean
     * @param replySerno
     */
    public void initLmtSigInvestBasicInfoRst(Object sigInvestBean, String replySerno, String accNo) {
        //根据审批表中的 单笔投资对应底层资产基本情况 生成批复表中对应的数据
        LmtSigInvestBasicInfoRst lmtSigInvestBasicInfoRst = lmtSigInvestBasicInfoRstService.initLmtSigInvestBasicInfoRstInfo(sigInvestBean);
        //批复流水赋值
        lmtSigInvestBasicInfoRst.setReplySerno(replySerno);
        lmtSigInvestBasicInfoRstMapper.insert(lmtSigInvestBasicInfoRst);
    }

    /**
     * 初始化单笔投资授信批复表 和 单笔投资授信台账表 ，并插入数据
     *
     * @param sigInvestBean
     * @param replySerno
     */
    public LmtSigInvestBasicInfoAcc initLmtSigInvestBasicInfoAcc(Object sigInvestBean, String replySerno, String accNo) {
        //获取审批或者申请中的 单笔投资对应底层资产基本情况台账表 lmt_sig_invest_basic_info_acc
        LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = lmtSigInvestBasicInfoAccService.initLmtSigInvestBasicInfoAccInfo(sigInvestBean);
        lmtSigInvestBasicInfoAcc.setReplySerno(replySerno);
        lmtSigInvestBasicInfoAcc.setAccNo(accNo);
        return lmtSigInvestBasicInfoAcc;
    }


    /**
     * 初始化单笔投资授信批复表 和 单笔投资授信台账表 ，并插入数据
     *
     * @param sigInvestBean
     * @param replySerno
     */
    public void initLmtSigInvestBasicLmtInfo(Object sigInvestBean, String replySerno, String accNo) {
        //根据审批表中的 底层授信额度批复表 生成批复表中对应的数据
        LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst = lmtSigInvestBasicLmtRstService.initLmtSigInvestBasicLmtRstInfo(sigInvestBean);
        //批复流水号
        lmtSigInvestBasicLmtRst.setReplySerno(replySerno);
        lmtSigInvestBasicLmtRstMapper.insert(lmtSigInvestBasicLmtRst);

        //底层授信额度台账表 lmt_sig_invest_basic_lmt_acc
        LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc =
                lmtSigInvestBasicLmtAccService.initLmtSigInvestBasicLmtAccInfo(sigInvestBean);
        lmtSigInvestBasicLmtAcc.setReplySerno(replySerno);
        lmtSigInvestBasicLmtAcc.setAccNo(accNo);
        lmtSigInvestBasicLmtAccMapper.insert(lmtSigInvestBasicLmtAcc);
    }

    /**
     * 授信操作（授信变更、授信续作）
     *
     * @param lmtSigInvestAccMap
     * @param appType         CmisBizConstants.STD_SX_LMT_TYPE_02（授信变更）
     *                        CmisBizConstants.STD_SX_LMT_TYPE_03（授信续作）
     * @return
     */
    public LmtSigInvestApp optInvestAppByAcc(Map lmtSigInvestAccMap, String appType) throws ParseException {
        Instant stime = Instant.now();

        //校验前台pkId 和serno 是否生成
        if (StringUtils.isBlank(String.valueOf(lmtSigInvestAccMap.get("newSerno")))
                || StringUtils.isBlank(String.valueOf(lmtSigInvestAccMap.get("newPkId")))){
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
        }

        String accNo = (String) lmtSigInvestAccMap.get("accNo");
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByAccNo(accNo);
        if (lmtSigInvestAcc == null) {
            throw BizException.error(null, "99999", "台账数据获取失败！");
        }

        String newPkId = (String) lmtSigInvestAccMap.get("newPkId");
        String newSerno = (String) lmtSigInvestAccMap.get("newSerno");

        int result = 0;
        LmtSigInvestApp newApp = null;
        String origiSerno = null;
        String replySerno = null;
        try {
            replySerno = lmtSigInvestAcc.getReplySerno();
            origiSerno = lmtSigInvestAcc.getSerno();
            //判断当前申请是否存在已经发起（复议、变更、续作）
            checkCanRestart(origiSerno, appType, lmtSigInvestAcc.getLmtBizType(), lmtSigInvestAcc.getCusId(), null, lmtSigInvestAcc.getCusCatalog());
            //获取申请流水号
            log.info(String.format("保存主体授信续作数据,生成流水号%s", newSerno));
            //1.复制台账表到 授信申请主表和从表
            newApp = new LmtSigInvestApp();
            //1.1 赋值相关属性到授信主表
            copyProperties(lmtSigInvestAcc, newApp);

            newApp.setOrigiLmtAmt(lmtSigInvestAcc.getLmtAmt());
            newApp.setOrigiLmtReplySerno(replySerno);
            newApp.setOrigiLmtTerm(lmtSigInvestAcc.getLmtTerm());
            newApp.setOrigiRate(lmtSigInvestAcc.getRate());
            //TODO 增加原授信批复利率
            //申请类型为授信变更
            newApp.setAppType(appType);

            newApp.setManagerBrId(getCurrentUser().getOrg().getCode());
            newApp.setManagerId(getCurrentUser().getLoginCode());

            //初始化(新增)通用domain信息
            initInsertDomainProperties(newApp);
            //防止重复提交
            newApp.setPkId(newPkId);
            newApp.setSerno(newSerno);
            //流程状态为未发起
            newApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);

            //初始化 必填信息校验页面
            initMustCheckItem(newApp);

            result = insert(newApp);
            //判断主表是否添加成功
            if (result == 1) {
                //添加从表信息
                LmtSigInvestSubApp newSubApp = new LmtSigInvestSubApp();
                copyProperties(lmtSigInvestAcc, newSubApp);
                newSubApp.setSerno(newSerno);
                //获取原申请中
                LmtSigInvestSubApp subApp = lmtSigInvestSubAppService.selectBySerno(origiSerno);
                if (subApp != null) {
                    //交易市场
                    newSubApp.setTransMarket(subApp.getTransMarket());
                    //交易结构
                    newSubApp.setTranStr(subApp.getTranStr());
                    //是否存在担保及增信情况
                    newSubApp.setIsExistGuarCreditect(subApp.getIsExistGuarCreditect());
                    //其他担保/增信情况说明
                    newSubApp.setGuarDescExt(subApp.getGuarDescExt());
                    //产品总金额
                    newSubApp.setPrdTotalAmt(subApp.getPrdTotalAmt());
                }
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newSubApp);
                lmtSigInvestSubAppService.insert(newSubApp);

                //获取企业信息 、copy原主体分析、获取额度相关信息
                copyRelCusInfo(origiSerno,newSerno,lmtSigInvestAcc.getCusId(),
                        lmtSigInvestAcc.getCusType(),lmtSigInvestAcc.getCusCatalog(),true);

                System.out.println("复议、续作、变更 相关表复制（台账不存在）=====>");
                //复议、续作、变更 相关表复制（台账不存在）
                copyInvestOtherTables(origiSerno, newSerno, replySerno);
                System.out.println("复议、变更、续作 （台账表）copy至新的申请表=====>");
                //复议、变更、续作 （台账表）copy至新的申请表
                copyInvestAccTables(replySerno, newSerno);

                //判断申请类型是否为授信变更，初始化变更申请表数据
                if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType)) {
                    lmtChgDetailService.insertLmtChgDetail(newSerno);
                }
            }
        } catch (Exception e) {
            log.error("授信保存报错===》", e);
            //判断是否为BizException 抛出
            if (e instanceof BizException) {
                throw e;
            }
            //系统报错
            result = 0;
        }
        if (result == 0) {
            //判断申请类型，抛出对应报错
            if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType))
                throw BizException.error(null, EclEnum.LMT_INVEST_CHG_FIALED.key, EclEnum.LMT_INVEST_CHG_FIALED.value);
            if (CmisBizConstants.STD_SX_LMT_TYPE_03.equals(appType))
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_XUZUO_FAILED.key, EclEnum.LMT_SIG_INVESTAPP_XUZUO_FAILED.value);
        }

        Instant etime = Instant.now();
        log.info("optInvestAppByAcc ===>执行结束 消耗时间===》{} ms", Duration.between(stime, etime).toMillis());

        return newApp;
    }

    /**
     * 获取企业信息 、copy原主体分析、获取额度相关信息
     * @param origiSerno
     * @param serno
     * @param cusId
     * @param cusType
     * @param cusCatalog
     * @param needCusLmtInfo 是否获取额度相关信息
     */
    private void copyRelCusInfo(String origiSerno, String serno, String cusId, String cusType, String cusCatalog,boolean needCusLmtInfo) {
        //2.获取集团成员(股东信息)不包含主体分析
        lmtAppRelCusInfoService.insertCusInfoApp(serno, cusId, cusType, cusCatalog, false);

        //拷贝原主体分析
        lmtSigInvestRelFinaInfoService.copyRelFinaInfo(origiSerno, serno, cusId);

        if (needCusLmtInfo){
            //3.获取客户额度相关信息
            lmtAppRelGrpLimitService.insertCusLmtInfo(serno, cusId, cusType, cusCatalog);
        }
    }

    /**
     * 复议（批复表）copy至新的申请表
     *
     * @param replySerno
     * @param serno
     */
    private void copyInvestRstTables(String replySerno, String serno) {

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno", replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

        //lmt_sig_invest_basic_info_acc
        List<LmtSigInvestBasicInfoRst> lmtSigInvestBasicInfoRsts = lmtSigInvestBasicInfoRstService.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoRsts != null && lmtSigInvestBasicInfoRsts.size() > 0) {
            for (LmtSigInvestBasicInfoRst lmtSigInvestBasicInfoRst : lmtSigInvestBasicInfoRsts) {
                LmtSigInvestBasicInfo newLmtSigInvestBasicInfo = new LmtSigInvestBasicInfo();
                copyProperties(lmtSigInvestBasicInfoRst, newLmtSigInvestBasicInfo);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestBasicInfo);
                newLmtSigInvestBasicInfo.setSerno(serno);
                lmtSigInvestBasicInfoService.insert(newLmtSigInvestBasicInfo);
            }
        }

        //缓存原底层申请序列号对应生成的新的底层申请序列号（lmt_sig_invest_basic_info_sub.basic_serno <-> lmt_sig_invest_basic_limit_app.basic_serno）
        Map basicSernoMap = new HashMap();
        //lmt_sig_invest_basic_info_sub_rst
        List<LmtSigInvestBasicInfoSubRst> lmtSigInvestBasicInfoSubRsts = lmtSigInvestBasicInfoSubRstService.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoSubRsts != null && lmtSigInvestBasicInfoSubRsts.size() > 0) {
            for (LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst : lmtSigInvestBasicInfoSubRsts) {
                LmtSigInvestBasicInfoSub newLmtSigInvestBasicInfoSub = new LmtSigInvestBasicInfoSub();
                copyProperties(lmtSigInvestBasicInfoSubRst, newLmtSigInvestBasicInfoSub);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestBasicInfoSub);
                newLmtSigInvestBasicInfoSub.setSerno(serno);

                //底层申请流水号
                String newBasicSerno = generateSerno(CmisLmtConstants.LMT_SERNO);
                basicSernoMap.put(lmtSigInvestBasicInfoSubRst.getBasicSerno(), newBasicSerno);
                newLmtSigInvestBasicInfoSub.setBasicSerno(newBasicSerno);

                lmtSigInvestBasicInfoSubService.insert(newLmtSigInvestBasicInfoSub);

                //获取企业信息 、copy原主体分析、获取额度相关信息
                String basicCusId = lmtSigInvestBasicInfoSubRst.getBasicCusId();
                String basicCusType = lmtSigInvestBasicInfoSubRst.getBasicCusType();
                String basicCusCatalog = lmtSigInvestBasicInfoSubRst.getBasicCusCatalog();
                String basicSerno = lmtSigInvestBasicInfoSubRst.getBasicSerno();
                copyRelCusInfo(basicSerno,newBasicSerno,basicCusId,basicCusType,basicCusCatalog,true);
                //担保及增信情况
                lmtAppRelGuarService.copyLmtAppRelGuarList(basicSerno, newBasicSerno);
            }
        }
        //lmt_sig_invest_basic_lmt_acc
        List<LmtSigInvestBasicLmtRst> lmtSigInvestBasicLmtRsts = lmtSigInvestBasicLmtRstService.selectByModel(queryModel);
        if (lmtSigInvestBasicLmtRsts != null && lmtSigInvestBasicLmtRsts.size() > 0) {
            for (LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst : lmtSigInvestBasicLmtRsts) {
                //lmt_sig_invest_basic_limit_app
                LmtSigInvestBasicLimitApp newLmtSigInvestBasicLimitApp = new LmtSigInvestBasicLimitApp();
                copyProperties(lmtSigInvestBasicLmtRst, newLmtSigInvestBasicLimitApp);

                initInsertDomainProperties(newLmtSigInvestBasicLimitApp);
                newLmtSigInvestBasicLimitApp.setSerno(serno);
                //底层申请流水号
                String newBasicSerno = "";
                if (basicSernoMap.get(lmtSigInvestBasicLmtRst.getBasicSerno()) != null) {
                    newBasicSerno = (String) basicSernoMap.get(lmtSigInvestBasicLmtRst.getBasicSerno());
                }
                newLmtSigInvestBasicLimitApp.setBasicSerno(newBasicSerno);

                lmtSigInvestBasicLimitAppService.insert(newLmtSigInvestBasicLimitApp);
            }
        }
    }

    /**
     * 变更、续作 （台账表）copy至新的申请表
     *
     * @param replySerno
     * @param serno
     */
    private void copyInvestAccTables(String replySerno, String serno) {
        //lmt_sig_invest_basic_info_acc
        LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = lmtSigInvestBasicInfoAccService.selectByReplySerno(replySerno);
        if (lmtSigInvestBasicInfoAcc != null) {
            LmtSigInvestBasicInfo newLmtSigInvestBasicInfo = new LmtSigInvestBasicInfo();
            copyProperties(lmtSigInvestBasicInfoAcc, newLmtSigInvestBasicInfo);
            //初始化(新增)通用domain信息
            initInsertDomainProperties(newLmtSigInvestBasicInfo);
            newLmtSigInvestBasicInfo.setSerno(serno);
            lmtSigInvestBasicInfoService.insert(newLmtSigInvestBasicInfo);
        }

        //缓存原底层申请序列号对应生成的新的底层申请序列号（lmt_sig_invest_basic_info_sub.basic_serno <-> lmt_sig_invest_basic_limit_app.basic_serno）
        Map basicSernoMap = new HashMap();
        //lmt_sig_invest_basic_info_sub_acc
        List<LmtSigInvestBasicInfoSubAcc> lmtSigInvestBasicInfoSubAccs = lmtSigInvestBasicInfoSubAccService.selectByReplySerno(replySerno);
        if (lmtSigInvestBasicInfoSubAccs != null && lmtSigInvestBasicInfoSubAccs.size() > 0) {
            for (LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc : lmtSigInvestBasicInfoSubAccs) {
                LmtSigInvestBasicInfoSub newLmtSigInvestBasicInfoSub = new LmtSigInvestBasicInfoSub();
                copyProperties(lmtSigInvestBasicInfoSubAcc, newLmtSigInvestBasicInfoSub);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestBasicInfoSub);
                newLmtSigInvestBasicInfoSub.setSerno(serno);
                //底层申请流水号
                String newBasicSerno = generateSerno(CmisLmtConstants.LMT_SERNO);
                basicSernoMap.put(lmtSigInvestBasicInfoSubAcc.getBasicSerno(), newBasicSerno);
                newLmtSigInvestBasicInfoSub.setBasicSerno(newBasicSerno);
                newLmtSigInvestBasicInfoSub.setOrigiBasicAccNo(lmtSigInvestBasicInfoSubAcc.getBasicAccNo());
                lmtSigInvestBasicInfoSubService.insert(newLmtSigInvestBasicInfoSub);

                //获取企业信息 、copy原主体分析、获取额度相关信息
                String basicCusId = lmtSigInvestBasicInfoSubAcc.getBasicCusId();
                String basicCusType = lmtSigInvestBasicInfoSubAcc.getBasicCusType();
                String basicCusCatalog = lmtSigInvestBasicInfoSubAcc.getBasicCusCatalog();
                String basicSerno = lmtSigInvestBasicInfoSubAcc.getBasicSerno();
                copyRelCusInfo(basicSerno,newBasicSerno,basicCusId,basicCusType,basicCusCatalog,true);
                //担保及增信情况
                lmtAppRelGuarService.copyLmtAppRelGuarList(basicSerno, newBasicSerno);
            }
        }
        //lmt_sig_invest_basic_lmt_acc
        List<LmtSigInvestBasicLmtAcc> lmtSigInvestBasicLmtAccs = lmtSigInvestBasicLmtAccService.selectByReplySerno(replySerno);
        if (lmtSigInvestBasicLmtAccs != null && lmtSigInvestBasicLmtAccs.size() > 0) {
            for (LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc : lmtSigInvestBasicLmtAccs) {
                //lmt_sig_invest_basic_limit_app
                LmtSigInvestBasicLimitApp newLmtSigInvestBasicLimitApp = new LmtSigInvestBasicLimitApp();
                copyProperties(lmtSigInvestBasicLmtAcc, newLmtSigInvestBasicLimitApp);
                initInsertDomainProperties(newLmtSigInvestBasicLimitApp);
                //底层申请流水号
                String newBasicSerno = "";
                if (basicSernoMap.get(lmtSigInvestBasicLmtAcc.getBasicSerno()) != null) {
                    newBasicSerno = (String) basicSernoMap.get(lmtSigInvestBasicLmtAcc.getBasicSerno());
                }
                newLmtSigInvestBasicLimitApp.setBasicSerno(newBasicSerno);
                newLmtSigInvestBasicLimitApp.setSerno(serno);
                newLmtSigInvestBasicLimitApp.setOrigiBasicAccNo(lmtSigInvestBasicLmtAcc.getAccNo());
                lmtSigInvestBasicLimitAppService.insert(newLmtSigInvestBasicLimitApp);
            }
        }
    }

    /**
     * 复议、续作、变更 相关表复制（台账不存在）
     *
     * @param origiSerno
     * @param serno
     * @param replySerno
     */
    private void copyInvestOtherTables(String origiSerno, String serno, String replySerno) {

        //4.担保人增信人信息 、抵质押品信息  copy (lmt_sig_invest_rel_fina_info、lmt_app_corre_shd、lmt_app_rel_cus_info)
        lmtAppRelGuarService.copyLmtAppRelGuarList(origiSerno, serno);

        //单笔投资授信主体及增信人财务信息明细表 lmt_sig_invest_rel_fina_details
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", origiSerno);
        BizInvestCommonService.checkParamsIsNull("origiSerno",origiSerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestRelFinaDetails> lmtSigInvestRelFinaDetails = lmtSigInvestRelFinaDetailsService.selectByModel(queryModel);
        if (lmtSigInvestRelFinaDetails != null && lmtSigInvestRelFinaDetails.size() > 0) {
            for (LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetail : lmtSigInvestRelFinaDetails) {
                LmtSigInvestRelFinaDetails newLmtSigInvestRelFinaDetail = new LmtSigInvestRelFinaDetails();
                copyProperties(lmtSigInvestRelFinaDetail, newLmtSigInvestRelFinaDetail);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestRelFinaDetail);
                newLmtSigInvestRelFinaDetail.setSerno(serno);
                lmtSigInvestRelFinaDetailsService.insert(newLmtSigInvestRelFinaDetail);
            }
        }
        //前三大主营业务占比情况 lmt_sig_invest_rel_main_buss
        List<LmtSigInvestRelMainBuss> lmtSigInvestRelMainBusses = lmtSigInvestRelMainBussService.selectByModel(queryModel);
        if (lmtSigInvestRelMainBusses != null && lmtSigInvestRelMainBusses.size() > 0) {
            for (LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss : lmtSigInvestRelMainBusses) {
                LmtSigInvestRelMainBuss newLmtSigInvestRelMainBuss = new LmtSigInvestRelMainBuss();
                copyProperties(lmtSigInvestRelMainBuss, newLmtSigInvestRelMainBuss);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestRelMainBuss);
                newLmtSigInvestRelMainBuss.setSerno(serno);
                lmtSigInvestRelMainBussService.insert(newLmtSigInvestRelMainBuss);
            }
        }

        //单笔投资企业融资情况 lmt_sig_invest_rel_capit_info
        List<LmtSigInvestRelCapitInfo> lmtSigInvestRelCapitInfos = lmtSigInvestRelCapitInfoService.selectByModel(queryModel);
        if (lmtSigInvestRelCapitInfos != null && lmtSigInvestRelCapitInfos.size() > 0) {
            for (LmtSigInvestRelCapitInfo lmtSigInvestRelCapitInfo : lmtSigInvestRelCapitInfos) {
                LmtSigInvestRelCapitInfo newLmtSigInvestRelCapitInfo = new LmtSigInvestRelCapitInfo();
                copyProperties(lmtSigInvestRelCapitInfo, newLmtSigInvestRelCapitInfo);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestRelCapitInfo);
                newLmtSigInvestRelCapitInfo.setSerno(serno);
                lmtSigInvestRelCapitInfoService.insert(newLmtSigInvestRelCapitInfo);
            }
        }

        //单笔投资企业对外担保情况 lmt_sig_invest_rel_cus_out_guar
        List<LmtSigInvestRelCusOutGuar> lmtSigInvestRelCusOutGuars = lmtSigInvestRelCusOutGuarService.selectByModel(queryModel);
        if (lmtSigInvestRelCusOutGuars != null && lmtSigInvestRelCusOutGuars.size() > 0) {
            for (LmtSigInvestRelCusOutGuar lmtSigInvestRelCusOutGuar : lmtSigInvestRelCusOutGuars) {
                LmtSigInvestRelCusOutGuar newLmtSigInvestRelCusOutGuar = new LmtSigInvestRelCusOutGuar();
                copyProperties(lmtSigInvestRelCusOutGuar, newLmtSigInvestRelCusOutGuar);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestRelCusOutGuar);
                newLmtSigInvestRelCusOutGuar.setSerno(serno);
                lmtSigInvestRelCusOutGuarService.insert(newLmtSigInvestRelCusOutGuar);
            }
        }

        //产品各级别投资情况 lmt_sig_invest_prod_level_details
        List<LmtSigInvestProdLevelDetails> lmtSigInvestProdLevelDetails = lmtSigInvestProdLevelDetailsService.selectByModel(queryModel);
        if (lmtSigInvestProdLevelDetails != null && lmtSigInvestProdLevelDetails.size() > 0) {
            for (LmtSigInvestProdLevelDetails lmtSigInvestProdLevelDetail : lmtSigInvestProdLevelDetails) {
                LmtSigInvestProdLevelDetails newLmtSigInvestProdLevelDetails = new LmtSigInvestProdLevelDetails();
                copyProperties(lmtSigInvestProdLevelDetail, newLmtSigInvestProdLevelDetails);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestProdLevelDetails);
                newLmtSigInvestProdLevelDetails.setSerno(serno);
                lmtSigInvestProdLevelDetailsService.insert(newLmtSigInvestProdLevelDetails);
            }
        }

        //单笔投资授信关联风险分析 lmt_sig_invest_risk_anly
        List<LmtSigInvestRiskAnly> lmtSigInvestRiskAnlies = lmtSigInvestRiskAnlyService.selectByModel(queryModel);
        if (lmtSigInvestRiskAnlies != null && lmtSigInvestRiskAnlies.size() > 0) {
            for (LmtSigInvestRiskAnly lmtSigInvestRiskAnly : lmtSigInvestRiskAnlies) {
                LmtSigInvestRiskAnly newLmtSigInvestRiskAnly = new LmtSigInvestRiskAnly();
                copyProperties(lmtSigInvestRiskAnly, newLmtSigInvestRiskAnly);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestRiskAnly);
                newLmtSigInvestRiskAnly.setSerno(serno);
                lmtSigInvestRiskAnlyService.insert(newLmtSigInvestRiskAnly);
            }
        }

        //单笔投资产品基本信息 lmt_sig_invest_prod_info
        List<LmtSigInvestProdInfo> lmtSigInvestProdInfos = lmtSigInvestProdInfoService.selectByModel(queryModel);
        if (lmtSigInvestProdInfos != null && lmtSigInvestProdInfos.size() > 0) {
            for (LmtSigInvestProdInfo lmtSigInvestProdInfo : lmtSigInvestProdInfos) {
                LmtSigInvestProdInfo newLmtSigInvestProdInfo = new LmtSigInvestProdInfo();
                copyProperties(lmtSigInvestProdInfo, newLmtSigInvestProdInfo);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestProdInfo);
                newLmtSigInvestProdInfo.setSerno(serno);
                lmtSigInvestProdInfoService.insert(newLmtSigInvestProdInfo);
            }
        }

        //单笔投资产品费率 lmt_sig_invest_prod_rate
        List<LmtSigInvestProdRate> lmtSigInvestProdRates = lmtSigInvestProdRateService.selectByModel(queryModel);
        if (lmtSigInvestProdRates != null && lmtSigInvestProdRates.size() > 0) {
            for (LmtSigInvestProdRate lmtSigInvestProdRate : lmtSigInvestProdRates) {
                LmtSigInvestProdRate newLmtSigInvestProdRate = new LmtSigInvestProdRate();
                copyProperties(lmtSigInvestProdRate, newLmtSigInvestProdRate);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestProdRate);
                newLmtSigInvestProdRate.setSerno(serno);
                lmtSigInvestProdRateService.insert(newLmtSigInvestProdRate);
            }
        }

        //单笔投资底层入池性质分析 lmt_sig_invest_basic_cpt_anly
        List<LmtSigInvestBasicCptAnly> lmtSigInvestBasicCptAnlies = lmtSigInvestBasicCptAnlyService.selectByModel(queryModel);
        if (lmtSigInvestBasicCptAnlies != null && lmtSigInvestBasicCptAnlies.size() > 0) {
            for (LmtSigInvestBasicCptAnly lmtSigInvestBasicCptAnly : lmtSigInvestBasicCptAnlies) {
                LmtSigInvestBasicCptAnly newLmtSigInvestBasicCptAnly = new LmtSigInvestBasicCptAnly();
                copyProperties(lmtSigInvestBasicCptAnly, newLmtSigInvestBasicCptAnly);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestBasicCptAnly);
                newLmtSigInvestBasicCptAnly.setSerno(serno);
                lmtSigInvestBasicCptAnlyService.insert(newLmtSigInvestBasicCptAnly);
            }
        }

        //底层授信关联财务情况分析 lmt_sig_invest_basic_fnc_anly
        List<LmtSigInvestBasicFncAnly> lmtSigInvestBasicFncAnlies = lmtSigInvestBasicFncAnlyService.selectByModel(queryModel);
        if (lmtSigInvestBasicFncAnlies != null && lmtSigInvestBasicFncAnlies.size() > 0) {
            for (LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly : lmtSigInvestBasicFncAnlies) {
                LmtSigInvestBasicFncAnly newLmtSigInvestBasicFncAnly = new LmtSigInvestBasicFncAnly();
                copyProperties(lmtSigInvestBasicFncAnly, newLmtSigInvestBasicFncAnly);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtSigInvestBasicFncAnly);
                newLmtSigInvestBasicFncAnly.setSerno(serno);
                lmtSigInvestBasicFncAnlyService.insert(newLmtSigInvestBasicFncAnly);
            }
        }
    }


    /**
     * 流程提交后，数据由申请表拷贝到审批表中
     *
     * @param lmtSigInvestApp
     * @param issueReportType
     * @param varParam        审批路由参数
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterStart(LmtSigInvestApp lmtSigInvestApp, String issueReportType, Map<String, Object> varParam) throws Exception {
        log.info("资金业务授信流程审批，申请表生成审批表数据开始.....");
        //申请流水号
        String serno = lmtSigInvestApp.getSerno();
        //根据申请表生成单笔投资授信申请审批表（lmt_sig_invest_appr） 开始
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.initLmtSigInvestApprInfo(lmtSigInvestApp);

        //终身机构 -- 授信终审机构类型 03 总行
        lmtSigInvestAppr.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //审批模式
        /**
         *  判断这笔申请数据是不是正在信贷管理部负责人权限内，若是，则审批模式为51-信贷管理部负责人权限；
         *  判断申请数据有没有在信贷管理部分管行长权限内，若在，则审批模式为52-信贷管理部分管行长权限
         *  若不在，则为53-投委会权限;
         */
        String approveMode = "";
        Integer rightXDGL = (Integer) varParam.get("rightXDGL");
        Integer rightFHZ = (Integer) varParam.get("rightFHZ");

        if (rightXDGL == 1) {
            approveMode = CmisBizConstants.STD_APPR_MODE_51;
        } else if (rightFHZ == 1) {
            approveMode = CmisBizConstants.STD_APPR_MODE_52;
        } else {
            approveMode = CmisBizConstants.STD_APPR_MODE_53;
        }
        lmtSigInvestAppr.setApprMode(approveMode);
        lmtSigInvestAppr.setIssueReportType(issueReportType);

        lmtSigInvestApprMapper.insert(lmtSigInvestAppr);

        String approveSerno = lmtSigInvestAppr.getApproveSerno();
        //查询单笔投资对应底层资产基本情况申请表 根据申请表生成审批表 数据信息
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = lmtSigInvestBasicInfoService.selectBySerno(serno);
        //底层资产基本情况不为空，处理
        if (lmtSigInvestBasicInfo != null) {
            LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.initLmtSigInvestBasicInfoApprInfo(lmtSigInvestBasicInfo);
            //审批流水号
            lmtSigInvestBasicInfoAppr.setApproveSerno(approveSerno);
            //插入数据
            lmtSigInvestBasicInfoApprMapper.insert(lmtSigInvestBasicInfoAppr);
        }

        //单笔投资关联底层信息明细审批表 数据生成
        List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubList = lmtSigInvestBasicInfoSubService.selectListBySerno(serno);
        if (lmtSigInvestBasicInfoSubList != null && lmtSigInvestBasicInfoSubList.size() > 0) {
            for (LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub : lmtSigInvestBasicInfoSubList) {
                LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr = lmtSigInvestBasicInfoSubApprService.initLmtSigInvestBasicInfoSubApprInfo(lmtSigInvestBasicInfoSub);
                lmtSigInvestBasicInfoSubAppr.setApproveSerno(approveSerno);
                lmtSigInvestBasicInfoSubApprMapper.insert(lmtSigInvestBasicInfoSubAppr);
            }
        }

        //底层资产额度审批表(lmt_sig_invest_basic_lmt_appr)
        List<LmtSigInvestBasicLimitApp> lmtSigInvestBasicLimitAppList = lmtSigInvestBasicLimitAppService.selectBySerno(serno);
        if (lmtSigInvestBasicLimitAppList != null && lmtSigInvestBasicLimitAppList.size() > 0) {
            for (LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp : lmtSigInvestBasicLimitAppList) {
                LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = lmtSigInvestBasicLmtApprService.initLmtSigInvestBasicLmtApprInfo(lmtSigInvestBasicLimitApp);
                lmtSigInvestBasicLmtAppr.setApproveSerno(approveSerno);
                lmtSigInvestBasicLmtApprMapper.insert(lmtSigInvestBasicLmtAppr);
            }
        }

        lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
        this.update(lmtSigInvestApp);

        log.info("资金业务授信流程审批，申请表生成审批表数据结束.....");
    }

    /**
     * （审批打回）至客户经理处，将审批表数据copy至申请表（打回操作不需要生成审批数据）
     *
     * @param lmtSigInvestApp
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterCallBack(LmtSigInvestApp lmtSigInvestApp, String currentUserId, String currentOrgId, String approveStatus) throws Exception {

//        update(lmtSigInvestApp);

        //获取申请流水号
        String serno = lmtSigInvestApp.getSerno();
        //根据申请流水号查询最新的审批表数据
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectBySerno(serno);
        if (lmtSigInvestAppr == null) {
            throw new Exception(EclEnum.ECL070077.value);
        }
        //copy审批表数据到申请表中
        copyProperties(lmtSigInvestAppr, lmtSigInvestApp, "pkId");
        //针对流程到办结节点，进行以下处理
        lmtSigInvestApp.setApproveStatus(approveStatus);
        //初始化（更新）通用domain信息
        initUpdateDomainProperties(lmtSigInvestApp);
        update(lmtSigInvestApp);

        LmtSigInvestSubApp subApp = lmtSigInvestSubAppService.selectBySerno(serno);
        if (subApp != null) {
            copyProperties(lmtSigInvestAppr, subApp, "pkId");
            lmtSigInvestSubAppService.update(subApp);
        } else {
            log.error("资金同业-主体授信{}从表信息未找到！", serno);
        }

        //删除原有申请表信息
        List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubs = lmtSigInvestBasicInfoSubService.selectBySerno(serno);
        if (lmtSigInvestBasicInfoSubs != null && lmtSigInvestBasicInfoSubs.size() > 0) {
            for (LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub : lmtSigInvestBasicInfoSubs) {
                //标记删除
                lmtSigInvestBasicInfoSub.setOprType(CmisBizConstants.OPR_TYPE_02);
                lmtSigInvestBasicInfoSubService.update(lmtSigInvestBasicInfoSub);
            }
        }
        //删除原有申请表信息
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo1 = lmtSigInvestBasicInfoService.selectBySerno(serno);
        if (lmtSigInvestBasicInfo1 != null) {
            lmtSigInvestBasicInfo1.setOprType(CmisBizConstants.OPR_TYPE_02);
            lmtSigInvestBasicInfoService.update(lmtSigInvestBasicInfo1);
        }
        //删除原有申请表信息
        List<LmtSigInvestBasicLimitApp> lmtSigInvestBasicLimitApps = lmtSigInvestBasicLimitAppService.selectBySerno(serno);
        if (lmtSigInvestBasicLimitApps != null && lmtSigInvestBasicLimitApps.size() > 0) {
            for (LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp : lmtSigInvestBasicLimitApps) {
                lmtSigInvestBasicLimitApp.setOprType(CmisBizConstants.OPR_TYPE_02);
                lmtSigInvestBasicLimitAppService.update(lmtSigInvestBasicLimitApp);
            }
        }

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_sub_rst 单笔投资关联底层信息明细台账表 lmt_sig_invest_basic_info_sub_acc **/
        //单笔投资关联底层信息明细审批表 lmt_sig_invest_basic_info_sub_appr
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectBySerno(serno);
        if (lmtSigInvestBasicInfoSubApprList != null && lmtSigInvestBasicInfoSubApprList.size() > 0) {
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr : lmtSigInvestBasicInfoSubApprList) {
                LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub = new LmtSigInvestBasicInfoSub();
                copyProperties(sigInvestBasicInfoSubAppr, lmtSigInvestBasicInfoSub);
                //初始化(新增)通用domain信息(不包含input)
                initInsertDomainPropertiesForWF(lmtSigInvestBasicInfoSub);
                lmtSigInvestBasicInfoSubService.insert(lmtSigInvestBasicInfoSub);
            }
        }

        /**生成批复单笔投资对应底层资产基本情况批复表 lmt_sig_invest_basic_info_rst  **/
        //获取审批或者申请中的 单笔投资对应底层资产基本情况 信息 LmtSigInvestBasicInfoAppr
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.selectBySerno(serno);
        if (lmtSigInvestBasicInfoAppr != null) {
            LmtSigInvestBasicInfo lmtSigInvestBasicInfo = new LmtSigInvestBasicInfo();
            copyProperties(lmtSigInvestBasicInfoAppr, lmtSigInvestBasicInfo);
            //初始化(新增)通用domain信息(不包含input)
            initInsertDomainPropertiesForWF(lmtSigInvestBasicInfo);
            lmtSigInvestBasicInfoService.insert(lmtSigInvestBasicInfo);
        }

        /**底层授信额度批复表 lmt_sig_invest_basic_lmt_rst **/
        //获取审批或申请中 底层授信额度批复表 信息 初始化批复底层授信额度批复表
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprList = lmtSigInvestBasicLmtApprService.selectBySerno(serno);
        if (lmtSigInvestBasicLmtApprList != null && lmtSigInvestBasicLmtApprList.size() > 0) {
            for (LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr : lmtSigInvestBasicLmtApprList) {
                LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp = new LmtSigInvestBasicLimitApp();
                copyProperties(lmtSigInvestBasicLmtAppr, lmtSigInvestBasicLimitApp);
                //初始化(新增)通用domain信息(不包含input);
                initInsertDomainPropertiesForWF(lmtSigInvestBasicLimitApp);
                lmtSigInvestBasicLimitAppService.insert(lmtSigInvestBasicLimitApp);
            }
        }
    }

    /**
     * @param lmtSigInvestApp 描述：
     *                        检查同一客户不允许发起多笔债券池授信
     *                        如该客户同时存在满足条件的债券投资授信，系统自动将其额度纳入本次债券池授信额度，债券池授信金额以投资经理录入为准，审批通过后原债券投资授信额度自动失效。满足条件的单笔债券投资指：
     *                        ①债券投资的标准化 投资类型为：企业债、公司债、中期票据债、PPN、短期融资券、超短期融资券、项目收益券、项目收益票据、绿色债务融资工具；
     *                        ②债券投资的额度未到期
     *                        在债券池额度审批通过后，将符合条件的债券投资额度状态调整为“失效已结清”，原债券投资额度项下的未结清的业务关联至新审批通过的债券池额度项下。
     *                        同时通知ComStar系统将单笔债券投资产品授信金额调整为0，新增债券池额度。
     *                        授信主体客户有债券池授信后，允许发起全类型的单笔债券投资授信额度，并与债券池授信额度无关。后续客户再次新增或调整债券池授信额度时，将符合条件的单笔债券投资额度纳入债券池授信额度中。
     *                        4 企业债
     *                        E 公司债
     *                        N 中期票据
     *                        N1 PPN
     *                        6 短期融资券
     *                        O 超短期融资券
     *                        G 项目收益债券
     *                        J 绿色债务融资工具
     */
    public void generateSigInvestToPool(LmtSigInvestApp lmtSigInvestApp) {
        log.info("检查同一客户不允许发起多笔债券池授信 ---------------start");
        //获取客户信息
        String cusId = lmtSigInvestApp.getCusId();
        //债券投资授信查询：①债券投资的标准化 投资类型为：企业债、公司债、中期票据债、PPN、短期融资券、超短期融资券、项目收益券、项目收益票据、绿色债务融资工具；
        //     ②债券投资的额度未到期
        //update by lizx 2021-11-03 ②债券投资的额度未到期 该田间剔除
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("lmtBizType", CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4002);
        queryModel.addCondition("investType", CmisLmtConstants.STD_ZB_NORM_INVEST_TYPE_DMD);
        queryModel.addCondition("accStatus", CmisLmtConstants.STD_REPLY_STATUS_01);
        List<LmtSigInvestAcc> lmtSigInvestAccs = lmtSigInvestAccService.selectSigInvestInvestType(queryModel);

        if (lmtSigInvestAccs != null && lmtSigInvestAccs.size() > 0) {
            for (LmtSigInvestAcc lmtSigInvestAcc : lmtSigInvestAccs) {
                //将符合条件的债券投资额度状态调整为失效状态
                lmtSigInvestAcc.setAccStatus(CmisLmtConstants.STD_REPLY_STATUS_02);
                lmtSigInvestAccMapper.updateByPrimaryKey(lmtSigInvestAcc);

                /**债券投资不存在穿透到底层，故该处代码注释掉  20210722 zhangjw*/
//                //台账编号
//                String accNo = lmtSigInvestAcc.getAccNo() ;
//                HashMap<String, String > paramMap = new HashMap<>() ;
//                paramMap.put("accNo", accNo) ;
//                paramMap.put("accStatus", CmisLmtConstants.STD_REPLY_STATUS_02) ;
//                //根据台账标号，更新底层授信额度台账表 状态为失效 STD_REPLY_STATUS
//                lmtSigInvestBasicLmtAccService.updateLmtSigInvestBasicAccStatus(paramMap) ;
            }
        }

        //授信批复表数据改为失效状态
        queryModel.addCondition("replyStatus", CmisLmtConstants.STD_REPLY_STATUS_01);
        List<LmtSigInvestRst> lmtSigInvestRstList = lmtSigInvestRstService.selectSigInvestInvestType(queryModel);
        if (lmtSigInvestRstList != null && lmtSigInvestRstList.size() > 0) {
            for (LmtSigInvestRst lmtSigInvestRst : lmtSigInvestRstList) {
                //将符合条件的债券投资额度状态调整为失效状态
                lmtSigInvestRst.setReplyStatus(CmisLmtConstants.STD_REPLY_STATUS_02);
                lmtSigInvestRstMapper.updateByPrimaryKey(lmtSigInvestRst);
            }
        }
        log.info("检查同一客户不允许发起多笔债券池授信 ---------------start");
    }

    /**
     * 推送首页提醒事项
     *
     * @param lmtSigInvestApp,messageType,comment,inputId,inputBrId add by zhangjw 20210630
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendWbMsgNotice(LmtSigInvestApp lmtSigInvestApp, String messageType, String comment, String inputId, String inputBrId,String result) throws Exception {
        log.info("根据资金业务业务申请编号【{}】，审批操作生成首页提醒！", lmtSigInvestApp.getSerno());

        Map<String, String> map = new HashMap<>();
        map.put("cusName", lmtSigInvestApp.getCusName());
        map.put("prdName", "主体授信/产品授信申报");
        map.put("result", result);
        ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(lmtSigInvestApp.getManagerId());
        sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1",lmtSigInvestApp.getManagerId(),byLoginCode.getData().getUserMobilephone());
    }

    /**
     * 更新图片路径
     *
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        LmtSigInvestSubApp lmtSigInvestSubApp = new LmtSigInvestSubApp();
        lmtSigInvestSubApp.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/" + getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)) {
            picAbsolutePath = getFileAbsolutePath(fileId, serverPath, relativePath, fanruanFileTemplate);
        }
        setValByKey(lmtSigInvestSubApp, key, picAbsolutePath);
        lmtSigInvestSubAppService.updateSelective(lmtSigInvestSubApp);
        return picAbsolutePath;
    }



    /**
     * @方法名称: getRouterMapResult
     * @方法描述: 获取路由结果集-资金业务审批流程
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: zhangjw
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getRouterMapResult(ResultInstanceDto resultInstanceDto, String serno) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        LmtSigInvestApp lmtSigInvestApp = this.selectBySerno(serno);

        //流程路由1：发起人岗位判断  -- 前端提交时将参数放入，无需这里重新判断

        //流程路由2：金融市场总部风险合规部负责人是否等于金融市场总部总裁
        String isEqualJRSCBZC = commonService.getIsEqualJRSCBZC();
        resultMap.put("isEqualJRSCBZC", isEqualJRSCBZC);

        //授信变更单独判断(债券池除外)
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getAppType())){
            //授信变更数据 对比当前利率和原利率差值 是20BP以下，则在信贷管理部负责人权限内，如果超了20BP，则上会
            lmtSigInvestApp.setRate(changeValueToBigDecimal(lmtSigInvestApp.getRate()));
            lmtSigInvestApp.setOrigiRate(changeValueToBigDecimal(lmtSigInvestApp.getOrigiRate()));
            BigDecimal subtract = lmtSigInvestApp.getOrigiRate().subtract(lmtSigInvestApp.getRate());
            if (subtract.compareTo(BigDecimal.valueOf(0.002)) <= 0) {
                resultMap.put("rightXDGL", 1);
                resultMap.put("rightFHZ", 1);
            }else{
                resultMap.put("rightXDGL", 0);
                resultMap.put("rightFHZ", 0);
            }
        }else{

            Integer rightXDGL =  0; //默认不在信贷管理部权限范围内
            Integer rightFHZ =  0; //默认不在信分管行长权限范围内
            if(!"164".equals(resultInstanceDto.getFlowId())) {//非分支机构发起的业务，分支机构发起的业务 非授信变更都走投委会
                //流程路由3：是否信贷管理部负责人权限范围内
                rightXDGL = this.hasAuthorityForXindaiManageDept(serno, lmtSigInvestApp.getDebtEvalResult());
                //流程路由5：是否分管行长权限
                rightFHZ = this.hasAuthorityForFuHangzhang(serno, lmtSigInvestApp.getDebtEvalResult());
            }
            resultMap.put("rightXDGL", rightXDGL);
            resultMap.put("rightFHZ", rightFHZ);
        }


        //流程路由4：是否上调权限
        resultMap.put("isUpAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectLastBySerno(serno);
        if (lmtSigInvestAppr != null) {
            String isUpAppr = lmtSigInvestAppr.getIsUpperApprAuth() == null ? "0" : lmtSigInvestAppr.getIsUpperApprAuth()  ;
            resultMap.put("isUpAppr", isUpAppr);
        }


        //流程路由6：是否大额授信  授信变更场景下默认不走大额授信流程  、承销额度不走大额授信流程
        Map<String, String> isLargeLmtMap = new HashMap<>();
        isLargeLmtMap.put("isLargeLmt",CmisCommonConstants.STD_ZB_YES_NO_0);
        isLargeLmtMap.put("isReportChairman",CmisCommonConstants.STD_ZB_YES_NO_0);
        isLargeLmtMap.put("hasLoanBuss",CmisCommonConstants.STD_ZB_YES_NO_0);

        resultMap.put("isLargeLmt", CmisCommonConstants.STD_ZB_YES_NO_0);
        if (!CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getAppType()) && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(lmtSigInvestApp.getLmtBizType())
                && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(lmtSigInvestApp.getLmtBizType())
                ){
            isLargeLmtMap = this.checkIsLargeLmt(lmtSigInvestApp);
            resultMap.put("isLargeLmt", isLargeLmtMap.get("isLargeLmt"));
        }

        //流程路由7：是否关联方交易
        // 是否关联交易  授信变更默认不走关联交易流程 、承销额度不走关联交易流程
        String isGLAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        resultMap.put("isGLAppr", isGLAppr);
        if (!CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getAppType()) && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(lmtSigInvestApp.getLmtBizType())
                && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(lmtSigInvestApp.getLmtBizType())
            ){
            isGLAppr = bizCommonService.checkCusIsGlf(lmtSigInvestApp.getCusId());
            resultMap.put("isGLAppr", isGLAppr);
        }

        // 如果是关联交易
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGLAppr)){
            Map<String,Object> gljyMap = bizCommonService.calGljyType(lmtSigInvestApp.getCusId(),lmtSigInvestApp.getLmtAmt(),BigDecimal.ZERO,BigDecimal.ZERO,CmisBizConstants.STD_ZB_INSTU_CDE_ZJG);

            BigDecimal newLmtRelRate = (BigDecimal)gljyMap.get("newLmtRelRate");
            BigDecimal allLmtRelRate = (BigDecimal)gljyMap.get("allLmtRelRate");
            String isSupGLFLmtAmt = (String)gljyMap.get("isSupGLFLmtAmt");
            resultMap.put("newLmtRelRate",newLmtRelRate);//单笔拟申报金额比例
            resultMap.put("allLmtRelRate",allLmtRelRate);//关联方贷款余额（存量敞口）+拟申报授信金额之和占我行资本净额
            resultMap.put("isSupGLFLmtAmt",isSupGLFLmtAmt);//是否超过关联方预计额度

            //关联交易类型
            String gLType = bizCommonService.getGLType(gljyMap);
            resultMap.put("gLType",gLType);
        }

        //流程路由8：是否超两倍大额标准值（是否报备董事长）
        resultMap.put("isReportChairman", isLargeLmtMap.get("isReportChairman"));

        //流程路由9：是否同时开展信贷业务
        resultMap.put("isHasLoanBuss", isLargeLmtMap.get("hasLoanBuss"));
        if(CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(lmtSigInvestApp.getLmtBizType())
                || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(lmtSigInvestApp.getLmtBizType())){
            resultMap.put("isHasLoanBuss", CmisCommonConstants.STD_ZB_YES_NO_0);
        }

        return resultMap;
    }

    /**
     * 信贷管理部权限-路由权限判断
     * 授信金额（(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)）
     *
     * @param serno
     * @param debtEvalResult
     * @return
     */
    public Integer hasAuthorityForXindaiManageDept(String serno, String debtEvalResult) {
        Integer result = 0;
        try {
            LmtSigInvestApp lmtSigInvestApp = this.selectBySerno(serno);
            if (lmtSigInvestApp != null) {
                /**
                 * 1、（主体授信）如果客户名下有非标（理财直融、债融、结构化融资、其他非标资产、资产证券化非标）、贷款业务，审批权限直接不在权限范围内（集团成员名下如果存在也算）
                 * （产品授信）（资产-债权融资计划、理财直融工具、结构化融资、资产证券化非标、资管计划、净值型产品）
                 */
                //产品类型
                String lmtBizType = lmtSigInvestApp.getLmtBizType();
                if(CmisBizConstants.SIG_INVEST_XDGLB_ROTE.contains(lmtBizType)){
                    log.info("判断1：信贷管理部负责人权限-路由权限判断----->"+serno+",存在非标业务类型，直接上会，不在信贷管理部负责人权限范围内");
                    //如果本身的授信产品就为此部分品种，则直接上会
                    return result;
                }
                //判断客户或者所属集团，成员项下是否存在以上业务类型，若存在，则直接上会 cmislmt0019
                CmisLmt0019ReqDto cmis0019Req = new CmisLmt0019ReqDto();
                cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_001);
                cmis0019Req.setQueryType(CmisLmtConstants.CMIS_QUERY_TYPE_02);//查询类型：01 贷款  02 贷款和非标
                cmis0019Req.setIsQuryGrp(CmisBizConstants.STD_ZB_YES_NO_Y);//是否查询所属集团项下
                if(lmtSigInvestApp.getInputBrId().startsWith("80")){
                    cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_002);
                }else if(lmtSigInvestApp.getInputBrId().startsWith("81")){
                    cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_003);
                }
                cmis0019Req.setCusId(lmtSigInvestApp.getCusId());
                ResultDto<CmisLmt0019RespDto> cmis0019ResultDto = cmisLmtClientService.cmisLmt0019(cmis0019Req);
                //请求成功
                if (SuccessEnum.SUCCESS.key.equals(cmis0019ResultDto.getData().getErrorCode())) {
                    String hasAsset = cmis0019ResultDto.getData().getResult();
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(hasAsset)) {
                        log.info("判断2：信贷管理部负责人权限-路由权限判断----->"+serno+",客户及其集团成员存在非标业务类型，直接上会，不在信贷管理部负责人权限范围内");
                        //授信主体及其集团成员如果也有此业务则直接上会，默认不在权限内
                        return result;
                    }
                }

                //投资类型
                String investType = lmtSigInvestApp.getInvestType();

                /**
                 * 2、（主体授信）【债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资】授信判断时，需要获取客户此部分品种授信余额（调用额度接口），
                 * 累加总额判断（集团成员也要累加，并且累加时，未到期累加授信总额，已到期的累加业务余额）；
                 * （产品授信）同上
                 * 如果授信申请存在原批复流水号，则查询原批复流水号是否对应有批复台账，如果有且状态为生效，则扣减掉原批复台账金额
                 */
                //获取授信金额
                BigDecimal lmtAmt = getLmtAmt() == null ? lmtSigInvestApp.getLmtAmt() : getLmtAmt();
                //法人客户  产品授信（不包含特定目的载体）
                if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtSigInvestApp.getCusCatalog())
                        || (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(lmtSigInvestApp.getCusCatalog())
                                && !CmisBizConstants.TOU_RONG_LEI_UNIQUE.contains(lmtBizType))) {
                    log.info("判断3-1：信贷管理部负责人权限-路由权限判断----->【" + serno + "】 lmtAmt 【{}】-------->  ", lmtAmt);
                    if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtSigInvestApp.getCusCatalog())) {
                        lmtAmt = lmtAmt.add(getCusGrpLmtBalance(lmtSigInvestApp));
                        log.info("判断3-2：信贷管理部负责人权限-路由权限判断----->【" + serno + "】非同业客户累加 客户非低风险、非穿透化、非承销的授信余额 lmtAmt 后 【{}】-------->  ", lmtAmt);
                    } else {
                        lmtAmt = lmtAmt.add(getLmtBal(lmtSigInvestApp));
                        log.info("判断3-3：信贷管理部负责人权限-路由权限判断----->【" + serno + "】同业客户累加 标和非标 授信余额 lmtAmt 后【{}】-------->  ", lmtAmt);
                    }

                    /**
                     * add by zhangjw 20211028  累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额
                     */
                    CmisLmt0065ReqDto cmisLmt0065ReqDto = new CmisLmt0065ReqDto();
                    cmisLmt0065ReqDto.setCusId(lmtSigInvestApp.getCusId());
                    cmisLmt0065ReqDto.setLimitSubNo(lmtBizType);
                    if(!CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.contains(lmtBizType)){
                        //其他场景  根据项目编号匹配  获取项下余额  进行扣减
                        cmisLmt0065ReqDto.setProNo(lmtSigInvestApp.getProNo());
                    }
                    ResultDto<CmisLmt0065RespDto> cmisLmt0065RespDto= cmisLmtClientService.cmislmt0065(cmisLmt0065ReqDto);
                    log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额：-----> 返回信息: cmisLmt0065RespDto -------------------------->" + cmisLmt0065RespDto.toString() );
                    String cmisLmt0065Code = cmisLmt0065RespDto.getData().getErrorCode();
                    if (!"0000".equals(cmisLmt0065Code)) {
                        log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额！");
                        throw new YuspException(EcbEnum.ECB019999.key, "获取客户本笔授信项下用信余额异常！");
                    }
                    BigDecimal cmislmt0065LmtAmt = cmisLmt0065RespDto.getData().getLmtAmt() == null ? BigDecimal.ZERO : cmisLmt0065RespDto.getData().getLmtAmt() ;
                    String cmislmt0065Status = cmisLmt0065RespDto.getData().getStatus() == null ? "" : cmisLmt0065RespDto.getData().getStatus() ;
                    if(cmislmt0065LmtAmt.compareTo(BigDecimal.ZERO) >=0   && cmislmt0065Status.equals(CmisLmtConstants.STD_ZB_APPR_ST_90)  ){
                        log.error("累加客户存量授信余额时，还需要剔除掉本笔授信（非失效额度）项下用信余额：-----> lmtAmt【"+lmtAmt+"】 cmislmt0065LmtAmt【"+cmislmt0065LmtAmt+"】 " );
                        lmtAmt = lmtAmt.subtract(cmislmt0065LmtAmt);
                    }
                }


                log.info("判断3：信贷管理部负责人权限-路由权限判断----->授信金额："+lmtAmt);
                CmisLmt0030ReqDto cmisLmt0030ReqDto = new CmisLmt0030ReqDto();
                cmisLmt0030ReqDto.setCusId(lmtSigInvestApp.getCusId());
                cmisLmt0030ReqDto.setIsQuryGrp(CmisBizConstants.STD_ZB_YES_NO_Y);
                //查询客户标准资产授信余额:客户标准化资产未到期的授信金额+已到期的业务余额
                ResultDto<CmisLmt0030RespDto> resultDto = cmisLmtClientService.cmislmt0030(cmisLmt0030ReqDto);
                //请求成功
                if (SuccessEnum.SUCCESS.key.equals(resultDto.getData().getErrorCode())) {
                    BigDecimal lmtBalanceAmt = resultDto.getData().getLmtBalanceAmt();
                    if (lmtBalanceAmt != null) {
                        log.info("判断4：信贷管理部负责人权限-路由权限判断----->客户授信余额："+lmtBalanceAmt);
                        //获取授信金额
                        lmtAmt = lmtAmt.add(lmtBalanceAmt);
                    }
                }

                /**
                 * 如果授信申请存在原批复流水号，则查询原批复流水号是否对应有批复台账，如果有且状态为生效，则扣减掉原批复台账金额
                 */
                String origiLmtReplySerno = lmtSigInvestApp.getOrigiLmtReplySerno();
                if(!StringUtils.isBlank(origiLmtReplySerno)){
                    LmtSigInvestAcc sigAcc = lmtSigInvestAccService.selectByReplySerno(origiLmtReplySerno);
                    if(sigAcc!=null && !StringUtils.isBlank(sigAcc.getAccNo())){
                        String accStatus = sigAcc.getAccStatus();
                        if(CmisCommonConstants.STD_XD_REPLY_STATUS_01.equals(accStatus)){
                            log.info("判断5：信贷管理部负责人权限-路由权限判断----->剔除客户本笔授信变更原授信金额："+sigAcc.getLmtAmt());
                            lmtAmt = lmtAmt.subtract(sigAcc.getLmtAmt());
                        }
                    }

                    /**
                     * 授信变更场景下，利率下调超过20BP,则默认上会，即：不在副行长权限内，也不在信贷管理部负责人权限内（同法人主体授信）
                     */
                    if(CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getLmtBizType())){
                        BigDecimal origiRate = lmtSigInvestApp.getOrigiRate() == null ? BigDecimal.ZERO : lmtSigInvestApp.getOrigiRate();
                        BigDecimal rate = lmtSigInvestApp.getRate()  == null ? BigDecimal.ZERO : lmtSigInvestApp.getRate();
                        if(origiRate.subtract(rate).compareTo(new BigDecimal(0.002))>0){
                            log.info("判断6：信贷管理部负责人权限-路由权限判断----->授信变更场景下，利率下调超过20BP,则默认上会：原利率【"+origiRate + "】,新利率【"+rate+"】");
                            return result;
                        }
                    }
                }

                /**
                 * 3、优先判断债项评级，若不存在债项评级则以授信主体外部评级为准（授信主体的评级取集团最低）
                 */
                debtEvalResult = StringUtils.isBlank(debtEvalResult) ? lmtSigInvestApp.getDebtEvalResult() : debtEvalResult;
                log.info("判断7：信贷管理部负责人权限-路由权限判断----->债项评级："+debtEvalResult);

                Map condition = new HashMap();
                condition.put("cusId", lmtSigInvestApp.getCusId());
                condition.put("serno", serno);
                condition.put("oprType", CmisBizConstants.OPR_TYPE_01);
                //5. 获取客户类型
                String cusType = "default";
                //6. 外部评级或债项评级
                String evalResultOuter = "";
                LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(condition);

                if (lmtAppRelCusInfo != null) {

                    if(StringUtils.isBlank(debtEvalResult) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_017.equals(debtEvalResult)){
                        //此处需要改造，获取判断是否集团成员，获取所在集团最低的客户评级 TODO
                        //queryMinBankLoanLevel

                        ResultDto<String> stringResultDto = null;
                        try {
                            Map<String, String> paramMap = new HashMap<>();
                            paramMap.put("cusId", lmtSigInvestApp.getCusId()) ;
                            paramMap.put("cusCatalog", lmtSigInvestApp.getCusCatalog()) ;
                            stringResultDto = cmisCusClientService.queryMinBankLoanLevel(paramMap);
                            log.info("判断8：信贷管理部负责人权限-路由权限判断----->授信主体外部最低评级，接口返回值【{}】",JSON.toJSONString(stringResultDto));
                            if (stringResultDto == null || StringUtils.isBlank(stringResultDto.getData()) || "null".equals(stringResultDto.getData())){
//                                throw BizException.error(null,"9999","获取授信主体外部最低评级失败！");
                                log.info("判断8：信贷管理部负责人权限-路由权限判断----->授信主体外部最低评级失败：使用当前客户的外部评级：【{}】",lmtAppRelCusInfo.getEvalResultOuter());
                                evalResultOuter = lmtAppRelCusInfo.getEvalResultOuter();
                            }else{
                                log.info("判断8：信贷管理部负责人权限-路由权限判断----->授信主体外部最低评级，【{}】",stringResultDto.getData());
                                evalResultOuter = stringResultDto.getData();
                            }
                            //剔除个人客户，且外部评级机构为【中诚信、联合资信、新世纪、大公国际、东方金城、其他】

                            log.info("判断8信贷管理部负责人权限-路由权限判断----->授信主体外部最低评级：【{}】 evalResultOuter【{}】"+JSON.toJSONString(stringResultDto),evalResultOuter);
                        } catch (Exception e) {
                            log.info("判断8：信贷管理部负责人权限-路由权限判断----->授信主体外部最低评级：",e);
                            throw BizException.error(null,"99999","获取授信主体外部最低评级失败！");
                        }
                    }


                    //外部评级或债项评级：AA+    11=>AAA
                    boolean isEval_UP_AA_Plus = "AAA".equals(evalResultOuter) || "AA+".equals(evalResultOuter)
                            || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_001.equals(debtEvalResult) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_002.equals(debtEvalResult);
                    //标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                    boolean isStandardBond = CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtBizType) || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4002.equals(lmtBizType)
                            || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4006.equals(lmtBizType) || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4012.equals(lmtBizType);
                    //外部评级或债项评级：AAA
                    boolean isEval_AAA = "AAA".equals(evalResultOuter) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_001.equals(debtEvalResult) || "11".equals(evalResultOuter);
                    //可转债/可交换债券(根据授信申报时[投资类型]区分)
                    boolean is_ExchangeBond = CmisBizConstants.STD_ZB_NORM_INVEST_TYPE_V.equals(investType) || CmisBizConstants.STD_ZB_NORM_INVEST_TYPE_Z1.equals(investType);
                    log.info("信贷管理部负责人权限-路由权限判断------>外部评级或债项评级isEval_UP_AA_Plus==>" + isEval_UP_AA_Plus);
                    log.info("信贷管理部负责人权限-路由权限判断------>标准化债券isStandardBond==>" + isStandardBond);
                    log.info("信贷管理部负责人权限-路由权限判断------>外部评级或债项评级isEval_AAA==>" + isEval_AAA);
                    log.info("信贷管理部负责人权限-路由权限判断------>可转债/可交换债券is_ExchangeBond==>" + is_ExchangeBond);
                    log.info("信贷管理部负责人权限-路由权限判断------>判断金额 ==>" + lmtAmt);

                    /**如果是城投企业，则以城投的权限判断，若存在在权限范围内的，直接认为权限内；
                     * 如果是国有企业，则以国有企业的权限判断，若存在权限范围内的，则直接认为权限内；
                     * add by zhangjw 20210829
                     */
                    //城投企业
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(lmtAppRelCusInfo.getIsCtinve())) {
                        cusType = "Ctinve";
                        log.info("判断9：信贷管理部负责人权限-路由权限判断----->客户类别："+cusType);

                        /**
                         * 客户类型：城投企业
                         * 授信金额：<=3亿(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)
                         * 外部评级或债项评级：AA+
                         * 产品类型：标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                         */
                        if ("Ctinve".equals(cusType) && compare(lmtAmt, 3_0000_0000L) <= 0
                                && isEval_UP_AA_Plus && isStandardBond) {
                            log.info("判断10：信贷管理部负责人权限-路由权限判断----->城投企业&AA+及以上评级&债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资&<=3亿：true");
                            result = 1;
                        }

                    }
                    if (    //国有企业(modify by zhangjw 20210826 蔡俊确认，通过法人客户信息中的注册登记类型区分，
                            // 国有全资、国有联营、国有独资（公司），这三个认定为国有企业)
                                (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtAppRelCusInfo.getCusCatalog())
                                    && (CmisCusConstants.STD_ZB_REG_TYPE_110.equals(lmtAppRelCusInfo.getRegiType())
                                        || CmisCusConstants.STD_ZB_REG_TYPE_141.equals(lmtAppRelCusInfo.getRegiType())
                                         || CmisCusConstants.STD_ZB_REG_TYPE_151.equals(lmtAppRelCusInfo.getRegiType())
                                      )
                                )
                                ||
                                //同业客户通过控股类型(国有绝对控股/国有相对控股)区分
                                (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(lmtAppRelCusInfo.getCusCatalog())
                                        && (CmisCusConstants.STD_ZB_HOLD_TYPE_111.equals(lmtAppRelCusInfo.getHoldType())
                                        || CmisCusConstants.STD_ZB_HOLD_TYPE_112.equals(lmtAppRelCusInfo.getHoldType()))
                                )) {
                        log.info("判断9：信贷管理部负责人权限-路由权限判断----->[HoldType]：" + lmtAppRelCusInfo.getHoldType() + "[RegiType]:" + lmtAppRelCusInfo.getRegiType());
                        cusType = "belongCountry";
                        log.info("判断9：信贷管理部负责人权限-路由权限判断----->客户类别：" + cusType);
                        /**
                         * 客户类型：国有企业(法人客户通过法人客户信息中企业性质区分,同业客户通过控股类型(国有绝对控股/国有相对控股)区分)
                         * 授信金额：<=5亿(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)
                         * 外部评级或债项评级：AAA
                         * 产品类型：标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                         */
                        if ("belongCountry".equals(cusType) && compare(lmtAmt, 5_0000_0000L) <= 0
                                && isEval_AAA && isStandardBond) {
                            log.info("判断11：信贷管理部负责人权限-路由权限判断----->国有企业&AAA及以上评级&债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资&<=5亿：true");
                            result = 1;
                        }
                    }
                    if("default".equals(cusType) ){
                        /**
                         * 客户类型：default
                         * 授信金额：<=1000万(本次授信金额)
                         * 外部评级或债项评级：AA+(及以上)
                         * 产品类型：可转债/可交换债券(根据授信申报时[投资类型]区分)
                         */
                        lmtAmt = getLmtAmt()==null ? lmtSigInvestApp.getLmtAmt():getLmtAmt();
                        if (compare(lmtAmt, 1000_0000L) <= 0
                                && isEval_UP_AA_Plus && is_ExchangeBond) {
                            log.info("判断12：信贷管理部负责人权限-路由权限判断----->AA+及以上评级&可转债/可交换债券&<=1000万：true");
                            result = 1;
                        }
                    }
                } else {
                    log.info("客户信息获取失败！流水号【serno】"+ serno);
                    return result;
                }
            } else {
                result = 0;
            }
        } catch (Exception e) {
            log.error("hasAuthorityForXindaiManageDept======>", e);
            if (e instanceof BizException){
                throw e;
            }
            result = 0;
        }
        return result;
    }

    /**
     * 副行长权限-路由权限判断
     * 授信金额（(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)）
     *
     * @param serno
     * @param debtEvalResult
     * @return
     */
    public Integer hasAuthorityForFuHangzhang(String serno, String debtEvalResult) {
        Integer result = 0;
        try {
            LmtSigInvestApp lmtSigInvestApp = this.selectBySerno(serno);

            if (lmtSigInvestApp != null) {
                /**
                 * 1、（主体授信）如果客户名下有非标（理财直融、债融、结构化融资、其他非标资产、资产证券化非标）、贷款业务，审批权限直接不在权限范围内（集团成员名下如果存在也算）
                 * （产品授信）（资产-债权融资计划、理财直融工具、结构化融资、资产证券化非标、资管计划、净值型产品）
                 */
                //产品类型
                String lmtBizType = lmtSigInvestApp.getLmtBizType();
                if(CmisBizConstants.SIG_INVEST_XDGLB_ROTE.contains(lmtBizType)){
                    log.info("判断1：副行长权限-路由权限判断----->"+serno+",存在非标业务类型，直接上会，不在副行长权限范围内");
                    //如果本身的授信产品就为此部分品种，则直接上会
                    return result;
                }

                //判断客户或者所属集团，成员项下是否存在以上业务类型，若存在，则直接上会 cmislmt0019
                CmisLmt0019ReqDto cmis0019Req = new CmisLmt0019ReqDto();
                cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_001);
                cmis0019Req.setQueryType(CmisLmtConstants.CMIS_QUERY_TYPE_02);//查询类型：01 贷款  02 贷款和非标
                cmis0019Req.setIsQuryGrp(CmisBizConstants.STD_ZB_YES_NO_Y);//是否查询所属集团项下
                if(lmtSigInvestApp.getInputBrId().startsWith("80")){
                    cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_002);
                }else if(lmtSigInvestApp.getInputBrId().startsWith("81")){
                    cmis0019Req.setInstuCde(CmisCommonConstants.INSTUCDE_003);
                }

                cmis0019Req.setCusId(lmtSigInvestApp.getCusId());
                ResultDto<CmisLmt0019RespDto> cmis0019ResultDto = cmisLmtClientService.cmisLmt0019(cmis0019Req);
                //请求成功
                if (SuccessEnum.SUCCESS.key.equals(cmis0019ResultDto.getData().getErrorCode())) {
                    String hasAsset = cmis0019ResultDto.getData().getResult();
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(hasAsset)) {
                        log.info("判断2：副行长权限-路由权限判断----->"+serno+",客户及其集团成员存在非标业务类型，直接上会，不在副行长权限范围内");
                        //授信主体及其集团成员如果也有此业务则直接上会，默认不在权限内
                        return result;
                    }
                }
                //投资类型
                String investType = lmtSigInvestApp.getInvestType();

                /**
                 * 2、（主体授信）【债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资】授信判断时，需要获取客户此部分品种授信余额（调用额度接口），
                 * 累加总额判断（集团成员也要累加，并且累加时，未到期累加授信总额，已到期的累加业务余额）；
                 * （产品授信）同上
                 * 如果授信申请存在原批复流水号，则查询原批复流水号是否对应有批复台账，如果有且状态为生效，则扣减掉原批复台账金额
                 */
                //获取授信金额
                BigDecimal lmtAmt = getLmtAmt() == null ? lmtSigInvestApp.getLmtAmt() : getLmtAmt();
                //法人客户  产品授信（特定目的载体）
                if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtSigInvestApp.getCusCatalog())
                        || (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(lmtSigInvestApp.getCusCatalog())
                                && CmisBizConstants.TOU_RONG_LEI_UNIQUE.contains(lmtBizType))) {
                    log.info("判断3-1：副行长权限-路由权限判断----->【" + serno + "】 lmtAmt 【{}】-------->  ", lmtAmt);
                    if (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtSigInvestApp.getCusCatalog())) {
                        lmtAmt = lmtAmt.add(getCusGrpLmtBalance(lmtSigInvestApp));
                        log.info("判断3-2：副行长权限-路由权限判断----->【" + serno + "】非同业客户累加 客户非低风险、非穿透化、非承销的授信余额 lmtAmt 后 【{}】-------->  ", lmtAmt);
                    } else {
                        lmtAmt = lmtAmt.add(getLmtBal(lmtSigInvestApp));
                        log.info("判断3-3：副行长权限-路由权限判断----->【" + serno + "】同业客户累加 标和非标 授信余额 lmtAmt 后【{}】-------->  ", lmtAmt);
                    }

                    /**
                     * add by zhangjw 20211028  累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额
                     */
                    CmisLmt0065ReqDto cmisLmt0065ReqDto = new CmisLmt0065ReqDto();
                    cmisLmt0065ReqDto.setCusId(lmtSigInvestApp.getCusId());
                    cmisLmt0065ReqDto.setLimitSubNo(lmtBizType);
                    if(!CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.contains(lmtBizType)){
                        //其他场景  根据项目编号匹配  获取项下余额  进行扣减
                        cmisLmt0065ReqDto.setProNo(lmtSigInvestApp.getProNo());
                    }
                    ResultDto<CmisLmt0065RespDto> cmisLmt0065RespDto= cmisLmtClientService.cmislmt0065(cmisLmt0065ReqDto);
                    log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额：-----> 返回信息: cmisLmt0065RespDto -------------------------->" + cmisLmt0065RespDto.toString() );
                    String cmisLmt0065Code = cmisLmt0065RespDto.getData().getErrorCode();
                    if (!"0000".equals(cmisLmt0065Code)) {
                        log.error("累加客户存量授信余额时，还需要剔除掉本笔授信项下用信余额！");
                        throw new YuspException(EcbEnum.ECB019999.key, "获取客户本笔授信项下用信余额异常！");
                    }
                    BigDecimal cmislmt0065LmtAmt = cmisLmt0065RespDto.getData().getLmtAmt() == null ? BigDecimal.ZERO : cmisLmt0065RespDto.getData().getLmtAmt() ;
                    String cmislmt0065Status = cmisLmt0065RespDto.getData().getStatus() == null ? "" : cmisLmt0065RespDto.getData().getStatus() ;
                    if(cmislmt0065LmtAmt.compareTo(BigDecimal.ZERO) >=0  && cmislmt0065Status.equals(CmisLmtConstants.STD_ZB_APPR_ST_90)  ){
                        log.error("累加客户存量授信余额时，还需要剔除掉本笔授信（非失效额度）项下用信余额：-----> lmtAmt【"+lmtAmt+"】 cmislmt0065LmtAmt【"+cmislmt0065LmtAmt+"】 " );
                        lmtAmt = lmtAmt.subtract(cmislmt0065LmtAmt);
                    }

                }
                log.info("判断3：副行长权限-路由权限判断----->授信金额："+lmtAmt);
                CmisLmt0030ReqDto cmisLmt0030ReqDto = new CmisLmt0030ReqDto();
                cmisLmt0030ReqDto.setCusId(lmtSigInvestApp.getCusId());
                cmisLmt0030ReqDto.setIsQuryGrp(CmisBizConstants.STD_ZB_YES_NO_Y);
                ResultDto<CmisLmt0030RespDto> resultDto = cmisLmtClientService.cmislmt0030(cmisLmt0030ReqDto);
                //请求成功
                if (SuccessEnum.SUCCESS.key.equals(resultDto.getData().getErrorCode())) {
                    BigDecimal lmtBalanceAmt = resultDto.getData().getLmtBalanceAmt();
                    if (lmtBalanceAmt != null) {
                        log.info("判断3：副行长权限-路由权限判断----->客户授信余额："+lmtBalanceAmt);
                        //获取授信金额
                        lmtAmt = lmtAmt.add(lmtBalanceAmt);
                    }
                }
                /**
                 * 如果授信申请存在原批复流水号，则查询原批复流水号是否对应有批复台账，如果有且状态为生效，则扣减掉原批复台账金额
                 */
                String origiLmtReplySerno = lmtSigInvestApp.getOrigiLmtReplySerno();
                if(!StringUtils.isBlank(origiLmtReplySerno)){
                    LmtSigInvestAcc sigAcc = lmtSigInvestAccService.selectByReplySerno(origiLmtReplySerno);
                    if(sigAcc!=null && !StringUtils.isBlank(sigAcc.getAccNo())){
                        String accStatus = sigAcc.getAccStatus();
                        if(CmisCommonConstants.STD_XD_REPLY_STATUS_01.equals(accStatus)){
                            log.info("判断3：副行长权限-路由权限判断----->剔除客户本笔授信变更原授信金额："+sigAcc.getLmtAmt());
                            lmtAmt = lmtAmt.subtract(sigAcc.getLmtAmt());
                        }
                    }

                    /**
                     * 授信变更场景下，利率下调超过20BP,则默认上会，即：不在副行长权限内，也不在信贷管理部负责人权限内（同法人主体授信）
                     */
                    if(CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getLmtBizType())){
                        BigDecimal origiRate = lmtSigInvestApp.getOrigiRate() == null ? BigDecimal.ZERO : lmtSigInvestApp.getOrigiRate();
                        BigDecimal rate = lmtSigInvestApp.getRate()  == null ? BigDecimal.ZERO : lmtSigInvestApp.getRate();
                        if(origiRate.subtract(rate).compareTo(new BigDecimal(0.002))>0){
                            log.info("判断3：信贷管理部负责人权限-路由权限判断----->授信变更场景下，利率下调超过20BP,则默认上会：原利率【"+origiRate + "】,新利率【"+rate+"】");
                            return result;
                        }
                    }
                }

                /**
                 * 3、优先判断债项评级，若不存在债项评级则以授信主体外部评级为准（授信主体的评级取集团最低）
                 */
                debtEvalResult = StringUtils.isBlank(debtEvalResult) ? lmtSigInvestApp.getDebtEvalResult() : debtEvalResult;
                log.info("判断4：副行长权限-路由权限判断----->债项评级："+debtEvalResult);
                Map condition = new HashMap();
                condition.put("cusId", lmtSigInvestApp.getCusId());
                condition.put("serno", serno);
                condition.put("oprType", CmisBizConstants.OPR_TYPE_01);
                //5. 获取客户类型
                String cusType = "default";
                //6. 外部评级或债项评级
                String evalResultOuter = "";

                LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(condition);
                if (lmtAppRelCusInfo != null) {
                    if(StringUtils.isBlank(debtEvalResult) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_017.equals(debtEvalResult)){
                        //此处需要改造，获取判断是否集团成员，获取所在集团最低的客户评级 TODO
                        ResultDto<String> stringResultDto = null;
                        Map<String, String> paramMap = new HashMap<>();
                        paramMap.put("cusId", lmtSigInvestApp.getCusId()) ;
                        paramMap.put("cusCatalog", lmtSigInvestApp.getCusCatalog()) ;
                        stringResultDto = cmisCusClientService.queryMinBankLoanLevel(paramMap);
                        log.info("副行长权限-路由权限判断-路由权限判断----->授信主体外部最低评级，接口返回值【{}】",JSON.toJSONString(stringResultDto));
                        if (stringResultDto == null || StringUtils.isBlank(stringResultDto.getData()) || "null".equals(stringResultDto.getData())){
//                                throw BizException.error(null,"9999","获取授信主体外部最低评级失败！");
                            log.info("副行长权限-路由权限判断-路由权限判断----->授信主体外部最低评级失败：使用当前客户的外部评级：【{}】",lmtAppRelCusInfo.getEvalResultOuter());
                            evalResultOuter = lmtAppRelCusInfo.getEvalResultOuter();
                        }else{
                            log.info("副行长权限-路由权限判断-路由权限判断----->授信主体外部最低评级，【{}】",stringResultDto.getData());
                            evalResultOuter = stringResultDto.getData();
                        }
                    }

                    //外部评级或债项评级：AA+
                    boolean isEval_UP_AA_Plus = "AAA".equals(evalResultOuter) || "AA+".equals(evalResultOuter)
                            || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_001.equals(debtEvalResult) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_002.equals(debtEvalResult);
                    //标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                    boolean isStandardBond = CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtBizType) || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4002.equals(lmtBizType)
                            || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4006.equals(lmtBizType) || CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4012.equals(lmtBizType);
                    //外部评级或债项评级：AAA
                    boolean isEval_AAA = "AAA".equals(evalResultOuter) || CmisBizConstants.STD_ZB_DEBT_EVAL_RESULT_001.equals(debtEvalResult);
                    //可转债/可交换债券(根据授信申报时[投资类型]区分)
                    boolean is_ExchangeBond = CmisBizConstants.STD_ZB_NORM_INVEST_TYPE_V.equals(investType) || CmisBizConstants.STD_ZB_NORM_INVEST_TYPE_Z1.equals(investType);
                    log.info("副行长权限-路由权限判断------>外部评级或债项评级isEval_UP_AA_Plus===>" + isEval_UP_AA_Plus);
                    log.info("副行长权限-路由权限判断------>标准化债券isStandardBond===>" + isStandardBond);
                    log.info("副行长权限-路由权限判断------>外部评级或债项评级isEval_AAA===>" + isEval_AAA);
                    log.info("副行长权限-路由权限判断------>可转债/可交换债券is_ExchangeBond===>" + is_ExchangeBond);
                    log.info("副行长权限-路由权限判断------>判断金额 ==>" + lmtAmt);

                    //城投企业
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(lmtAppRelCusInfo.getIsCtinve())) {
                        cusType = "Ctinve";
                        log.info("判断3：副行长权限-路由权限判断----->客户类别："+cusType);
                        /**
                         * 客户类型：城投企业
                         * 授信金额：<=4亿(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)
                         * 外部评级或债项评级：AA+
                         * 产品类型：标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                         */
                        if ("Ctinve".equals(cusType) && compare(lmtAmt, 4_0000_0000L) <= 0
                                //外部评级或债项评级：AA+
                                && isEval_UP_AA_Plus
                                //标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                                && isStandardBond) {
                            log.info("判断4：副行长权限-路由权限判断----->"+serno+"城投企业&<=4亿&AA+评级以上&标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)：true");
                            result = 1;
                        }

                    }
                    if (    //国有企业(modify by zhangjw 20210826 蔡俊确认，通过法人客户信息中的注册登记类型区分，国有全资、国有联营、国有独资（公司），这三个认定为国有企业)
                        (CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(lmtAppRelCusInfo.getCusCatalog())
                            && (CmisCusConstants.STD_ZB_REG_TYPE_110.equals(lmtAppRelCusInfo.getRegiType())
                                || CmisCusConstants.STD_ZB_REG_TYPE_141.equals(lmtAppRelCusInfo.getRegiType())
                                || CmisCusConstants.STD_ZB_REG_TYPE_151.equals(lmtAppRelCusInfo.getRegiType())
                            )
                        ) ||
                        //同业客户通过控股类型(国有绝对控股/国有相对控股)区分
                        (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(lmtAppRelCusInfo.getCusCatalog())
                                && (CmisCusConstants.STD_ZB_HOLD_TYPE_111.equals(lmtAppRelCusInfo.getHoldType())
                                || CmisCusConstants.STD_ZB_HOLD_TYPE_112.equals(lmtAppRelCusInfo.getHoldType()))
                        )) {
                        cusType = "belongCountry";
                        log.info("判断3：副行长权限-路由权限判断----->客户类别："+cusType);
                            /**
                             * 客户类型：国有企业(法人客户通过法人客户信息中企业性质区分,同业客户通过控股类型(国有绝对控股/国有相对控股)区分)
                             * 授信金额：<=6亿(客户名下有效标准化资产授信(未到期)+标准化资产余额(已到期)+本次授信金额)
                             * 外部评级或债项评级：AAA
                             * 产品类型：标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)
                             */
                            if ("belongCountry".equals(cusType) && compare(lmtAmt, 6_0000_0000L) <= 0
                                    && isEval_AAA && isStandardBond) {
                                log.info("判断4：副行长权限-路由权限判断----->"+serno+"国有企业&<=6亿&AAA评级以上&标准化债券(债券池/债券投资/资产证券化产品投资标准/其他标准化债券投资)：true");
                                result = 1;
                            }
                    }
                    if("default".equals(cusType)){
                        /**
                         * 客户类型：default
                         * 授信金额：<=1000万(本次授信金额)
                         * 外部评级或债项评级：AA+(及以上)
                         * 产品类型：可转债/可交换债券(根据授信申报时[投资类型]区分)
                         */
                        lmtAmt = getLmtAmt() == null ? lmtSigInvestApp.getLmtAmt() : getLmtAmt();
                        if ("default".equals(cusType) && compare(lmtAmt, 1000_0000L) <= 0
                                && isEval_UP_AA_Plus && is_ExchangeBond) {
                            log.info("判断4：副行长权限-路由权限判断----->"+serno+"<=1000万&AA+评级以上&可转债/可交换债券：true");
                            result = 1;
                        }
                    }

                } else {
                    log.info("客户信息获取失败！[serno]:"+serno);
                }
                log.info("判断3：副行长权限-路由权限判断----->客户类别："+cusType);
            } else {

                log.info("副行长权限-路由权限判断-----> 【{}】未找到该申请记录",lmtSigInvestApp.getSerno());
                result = 0;
            }
        } catch (Exception e) {
            result = 0;
            log.error("hasAuthorityForFuHangzhang====>", e);
        }
        return result;
    }

    //BigDecimal的比较方法：a相比于b,返回值1是大于，0是等于，-1是小于
    public int compare(BigDecimal a, long b) {
        int i = a.compareTo(BigDecimal.valueOf(b));
        return i;
    }

    /**
     * @方法名称: findRealLmtType
     * @方法描述: 根据授信批复查询原始批复的授信类型
     * @参数与返回说明:
     * @算法描述: 递归查询最近一笔授信类型不为复议的数据
     * @创建人: zhangjw
     * @创建时间: 2021-07-22
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String findRealLmtType(LmtSigInvestRst lmtSigInvestRst) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("replySerno", lmtSigInvestRst.getOrigiLmtReplySerno());
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtSigInvestRst> lastLmtSigInvestRstList = lmtSigInvestRstService.queryLmtSigInvestRstDataByParams(queryMap);
        if (lastLmtSigInvestRstList != null && lastLmtSigInvestRstList.size() == 1) {
            LmtSigInvestRst lastLmtSigInvestRst = lastLmtSigInvestRstList.get(0);
            if (CmisCommonConstants.LMT_TYPE_05.equals(lastLmtSigInvestRst.getAppType())) {
                return findRealLmtType(lastLmtSigInvestRst);
            } else {
                return lastLmtSigInvestRst.getAppType();
            }
        } else {
            throw new Exception("授信批复查询异常!");
        }
    }

    /**
     * 获取当前复议的初始记录
     * @param replySerno
     * @return
     */
    private String findRealOriginLmtSigInvestRst(String replySerno) {
        log.info("findRealOriginLmtSigInvestRst ===> "+replySerno);
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("replySerno", replySerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtSigInvestRst> lastLmtSigInvestRstList = lmtSigInvestRstService.queryLmtSigInvestRstDataByParams(queryMap);
        if (lastLmtSigInvestRstList != null && lastLmtSigInvestRstList.size() == 1) {
            LmtSigInvestRst lastLmtSigInvestRst = lastLmtSigInvestRstList.get(0);
            if (CmisCommonConstants.LMT_TYPE_05.equals(lastLmtSigInvestRst.getAppType())) {
                return findRealOriginLmtSigInvestRst(lastLmtSigInvestRst.getOrigiLmtReplySerno());
            } else {
                return lastLmtSigInvestRst.getReplySerno();
            }
        } else {
            throw BizException.error(null,"99999","授信批复查询异常!");
        }
    }


    /**
     * 获取复议次数 包含审批中的数据
     * @param replySerno
     * @return
     */
    public Integer getFyCount(String replySerno) {
        int count = 0;
        //获取审批完成的复议次数
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("origiLmtReplySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.addCondition("appType",CmisBizConstants.STD_SX_LMT_TYPE_05);//只过滤复议记录
        List<LmtSigInvestRst> lmtSigInvestRsts = lmtSigInvestRstService.selectByModel(queryModel);
        //获取审批中的复议次数
        List status = new ArrayList();
        status.add(CmisBizConstants.APPLY_STATE_APP);//111
        status.add(CmisBizConstants.APPLY_STATE_TODO);//000
        status.add(CmisBizConstants.APPLY_STATE_CALL_BACK);//992
        queryModel.addCondition("noApproveStatus", status);
        List<LmtSigInvestApp> lmtSigInvestApps = selectAll(queryModel);
        if (CollectionUtils.isNotEmpty(lmtSigInvestApps)){
            List<String> collect = lmtSigInvestApps.stream().map(LmtSigInvestApp::getSerno).collect(Collectors.toList());
            log.info("origiLmtReplySerno【"+replySerno+"】 == > app ==> "+collect.toString());
            count += lmtSigInvestApps.size();
        }
        if (CollectionUtils.isNotEmpty(lmtSigInvestRsts)){
            List<String> collect = lmtSigInvestRsts.stream().map(LmtSigInvestRst::getSerno).collect(Collectors.toList());
            log.info("origiLmtReplySerno【"+replySerno+"】 == > rst ==> "+collect.toString());
            log.info("==========================");
            count += lmtSigInvestRsts.size();
            for (LmtSigInvestRst lmtSigInvestRst : lmtSigInvestRsts) {
                //获取当前批复记录的复议次数 2
                count += getFyCount(lmtSigInvestRst.getReplySerno());
            }
        }
        return count;
    }

    /**
     * 获取发起复议次数
     * add by zhangjw 20210724
     */
    public Integer getFyCount1(String serno) {
        Integer count = 0;
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.selectBySerno(serno);
        if (lmtSigInvestRst != null) {
            String lmtType = lmtSigInvestRst.getAppType();
            if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(lmtType)) {
                count++;

                String origiLmtReplySerno = lmtSigInvestRst.getOrigiLmtReplySerno();
                if (!StringUtils.isBlank(origiLmtReplySerno)) {
                    LmtSigInvestRst newLmtSigInvestRst = lmtSigInvestRstService.selectByReplySerno(origiLmtReplySerno);
                    if (newLmtSigInvestRst != null) {
                        if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(newLmtSigInvestRst.getAppType())) {
                            count = count + this.getFyCount(newLmtSigInvestRst.getSerno());
                        }
                    }
                }
            }
        }
        return count;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    /**
     * 获取大额授信相关信息
     * @param lmtSigInvestApp
     * @return
     */
    public Map getLargeLmtInfo(LmtSigInvestApp lmtSigInvestApp) {
        log.info("单笔投资业务判断是否大额授信、是否报备董事长、审批权限-------------------->start");
        Map resultMap = new HashMap();
        String serno = lmtSigInvestApp.getSerno();

        BigDecimal lmtAmt = changeValueToBigDecimal(lmtSigInvestApp.getLmtAmt());
        lmtSigInvestApp.setLmtAmt(lmtAmt);
        setLmtAmt(lmtAmt);

        log.info("单笔投资业务判断是否大额授信：isLargeLmt-------------------->start");
        //是否大额授信
        if(!CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getAppType()) && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(lmtSigInvestApp.getLmtBizType())
                && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(lmtSigInvestApp.getLmtBizType())){
            Map<String, String> isLargeLmtMap = this.checkIsLargeLmt(lmtSigInvestApp);
            resultMap.put("isLargeLmt", isLargeLmtMap.get("isLargeLmt"));
            log.info("单笔投资业务判断是否大额授信、是否报备董事长：isLargeLmt【"+isLargeLmtMap.get("isLargeLmt")+"】-------------------->end");
            //是否超两倍大额标准值（是否报备董事长）
            resultMap.put("isReportChairman", isLargeLmtMap.get("isReportChairman"));
            log.info("单笔投资业务判断是否大额授信、是否报备董事长：isReportChairman【"+isLargeLmtMap.get("isReportChairman")+"】-------------------->end");
        }else{
            log.info("单笔投资业务判断是否大额授信、是否报备董事长：授信变更场景下默认不走大额授信流程  、承销额度不走大额授信流程");
            resultMap.put("isLargeLmt",CmisCommonConstants.STD_ZB_YES_NO_0);
            resultMap.put("isReportChairman",CmisCommonConstants.STD_ZB_YES_NO_0);
        }

        //非分支机构发起的业务，分支机构发起的业务 非授信变更都走投委会
        String approveMode = CmisBizConstants.STD_APPR_MODE_53;
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(lmtSigInvestApp.getAppType())) {
            log.info("单笔投资业务判断审批权限（信贷管理部负责人）：change-------------------->start");
            lmtSigInvestApp.setRate(changeValueToBigDecimal(lmtSigInvestApp.getRate()));
            lmtSigInvestApp.setOrigiRate(changeValueToBigDecimal(lmtSigInvestApp.getOrigiRate()));
            //授信变更数据 对比当前利率和原利率差值 是20BP以下，则在信贷管理部负责人权限内，如果超了20BP，则上会
            BigDecimal subtract = lmtSigInvestApp.getOrigiRate().subtract(lmtSigInvestApp.getRate());
            if (subtract.compareTo(BigDecimal.valueOf(0.002)) <= 0) {
                approveMode = CmisBizConstants.STD_APPR_MODE_51;
            }else{
                approveMode = CmisBizConstants.STD_APPR_MODE_53;
            }
            log.info("单笔投资业务判断审批权限（信贷管理部负责人）：change ------------------>end【{}】",approveMode);
            resultMap.put("apprMode", approveMode);
            return resultMap;
        }else if(!isSubOrg(lmtSigInvestApp.getInputBrId())){
            log.info("单笔投资业务判断审批权限（信贷管理部负责人）：rightXDGL-------------------->start");
            //是否信贷管理部负责人权限范围内
            int rightXDGL = this.hasAuthorityForXindaiManageDept(serno,lmtSigInvestApp.getDebtEvalResult());
            log.info("单笔投资业务判断审批权限（信贷管理部负责人）：rightXDGL【"+rightXDGL+"】-------------------->end");
            int rightFHZ = 0;
            if(rightXDGL == 0){
                log.info("单笔投资业务判断审批权限（副行长）：rightFHZ-------------------->start");
                //是否分管行长权限
                rightFHZ = this.hasAuthorityForFuHangzhang(serno,lmtSigInvestApp.getDebtEvalResult());
                log.info("单笔投资业务判断审批权限（副行长）：rightFHZ【"+rightFHZ+"】-------------------->end");
            }
            if (rightXDGL == 1) {
                approveMode = CmisBizConstants.STD_APPR_MODE_51;
            } else if (rightFHZ == 1) {
                approveMode = CmisBizConstants.STD_APPR_MODE_52;
            } else {
                approveMode = CmisBizConstants.STD_APPR_MODE_53;
            }
        }

        resultMap.put("apprMode", approveMode);

        return resultMap;
    }

    /**
     * 判断是否为分支机构
     * 如果是 非006035 且  非006025  且  非006041
     * 就认为是分支机构发起的投行业务
     * @return
     */
    public boolean isSubOrg(String inputBrId) {
        boolean isSubOrg;
        AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(inputBrId);
        if (adminSmOrgDto != null) {
            String orgType = adminSmOrgDto.getOrgType();
            if ("0".equals(orgType)) {
                isSubOrg = false;
            } else {
                isSubOrg = true;
            }
        } else {
            throw BizException.error(null, "9999", "发起机构类型获取失败！");
        }
        log.info("是否为分支机构===========》【{}】", isSubOrg);
        return isSubOrg;
    }

    /**
     * 更新总体调查结论
     * @param lmtSigInvestApp
     * @return
     */
    public int updateZtdcjl(LmtSigInvestApp lmtSigInvestApp) {
        String serno = lmtSigInvestApp.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"ztdcjl");
        return updateByKeyAndVal(lmtSigInvestApp);
    }

    /**
     * 更新资产编号
     * @param serno
     * @param assetNo
     */
    public int updateAssetNo(String serno, String assetNo) {
        LmtSigInvestApp lmtSigInvestApp = selectBySerno(serno);
        if (lmtSigInvestApp!=null){
            lmtSigInvestApp.setAssetNo(assetNo);
            return update(lmtSigInvestApp);
        }
        return 0;
    }

    /**
     * 补录资产编号
     * @param condition
     * @return
     */
    @Transactional
    public int changeAssetNo(Map condition) {
        String serno = condition.get("serno").toString();
        String proNo = condition.get("proNo").toString();
        String assetNo = condition.get("assetNo").toString();
        //1.检测该资产编号是否已生效
        lmtSigInvestAccService.check_isExistAssetNo(assetNo,proNo);

        //更新审批表数据
        lmtSigInvestApprMapper.updateAssetNoBySerno(assetNo,serno);

        //更新申请表
        return updateAssetNo(serno,assetNo);
    }

    /**
     * 获取客户在途的资金授信金额
     * @return
     */
    public BigDecimal getSigInvestZtAmt(String cusId){
        BigDecimal ztInvestLmtAmt = lmtSigInvestAppMapper.getSigInvestZtAmt(cusId);
        return ztInvestLmtAmt;
    }

    /**
     * 更新企业信息，重新计算审批权限
     * @param serno
     */
    public int reCalculateApprMode(String serno) {
        LmtSigInvestApp lmtSigInvestApp = selectBySerno(serno);
        Map largeLmtInfo = getLargeLmtInfo(lmtSigInvestApp);
        String apprMode = (String) largeLmtInfo.get("apprMode");
        lmtSigInvestApp.setApprMode(apprMode);
        return update(lmtSigInvestApp);
    }
}
