package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportQryLst
 * @类描述: credit_report_qry_lst数据实体类
 * @功能描述: 
 * @创建人: xx
 * @创建时间: 2021-05-18 20:47:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CreditReportQryLstDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 征信与业务关联流水号 **/
	private String cqbrSerno;

	/** 业务流水号 **/
	private String bizSerno;

	/** 征信查询场景 **/
	private String scene;

	/** 征信查询流水号 **/
	private String crqlSerno;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 主借款人客户号 **/
	private String borrowerCusId;
	
	/** 主借款人证件号码 **/
	private String borrowerCertCode;
	
	/** 主借款人名称 **/
	private String borrowerCusName;
	
	/** 与主借款人关系 **/
	private String borrowRel;
	
	/** 征信查询类别 **/
	private String qryCls;
	
	/** 征信查询原因 **/
	private String qryResn;
	
	/** 查询原因描述 **/
	private String qryResnDec;
	
	/** 授权书编号 **/
	private String authbookNo;
	
	/** 授权书内容 **/
	private String authbookContent;
	
	/** 其他授权书内容 **/
	private String otherAuthbookExt;
	
	/** 授权书日期 **/
	private String authbookDate;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 成功发起时间 **/
	private String sendTime;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 是否成功发起 **/
	private String isSuccssInit;
	
	/** 征信报告编号 **/
	private String reportNo;
	
	/** 征信查询状态 **/
	private String qryStatus;
	
	/** 征信返回地址 **/
	private String creditUrl;
	
	/** 报告生成时间 **/
	private String reportCreateTime;
	
	/** 征信查询标识 **/
	private String qryFlag;
	
	/** 影像编号 **/
	private String imageNo;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 授权模式 **/
	private String authWay;

	/**
	 * @param authWay
	 */
	public void setAuthWay(String authWay) {
		this.authWay = authWay == null ? null : authWay.trim();
	}

	/**
	 * @return authWay
	 */
	public String getAuthWay() {
		return this.authWay;
	}

	/**
	 * @param cqbrSerno
	 */
	public void setCqbrSerno(String cqbrSerno) {
		this.cqbrSerno = cqbrSerno == null ? null : cqbrSerno.trim();
	}

	/**
	 * @return cqbrSerno
	 */
	public String getCqbrSerno() {
		return this.cqbrSerno;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}

	/**
	 * @param scene
	 */
	public void setScene(String scene) {
		this.scene = scene == null ? null : scene.trim();
	}

	/**
	 * @return scene
	 */
	public String getScene() {
		return this.scene;
	}

	/**
	 * @param crqlSerno
	 */
	public void setCrqlSerno(String crqlSerno) {
		this.crqlSerno = crqlSerno == null ? null : crqlSerno.trim();
	}
	
    /**
     * @return CrqlSerno
     */	
	public String getCrqlSerno() {
		return this.crqlSerno;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param borrowerCusId
	 */
	public void setBorrowerCusId(String borrowerCusId) {
		this.borrowerCusId = borrowerCusId == null ? null : borrowerCusId.trim();
	}
	
    /**
     * @return BorrowerCusId
     */	
	public String getBorrowerCusId() {
		return this.borrowerCusId;
	}
	
	/**
	 * @param borrowerCertCode
	 */
	public void setBorrowerCertCode(String borrowerCertCode) {
		this.borrowerCertCode = borrowerCertCode == null ? null : borrowerCertCode.trim();
	}
	
    /**
     * @return BorrowerCertCode
     */	
	public String getBorrowerCertCode() {
		return this.borrowerCertCode;
	}
	
	/**
	 * @param borrowerCusName
	 */
	public void setBorrowerCusName(String borrowerCusName) {
		this.borrowerCusName = borrowerCusName == null ? null : borrowerCusName.trim();
	}
	
    /**
     * @return BorrowerCusName
     */	
	public String getBorrowerCusName() {
		return this.borrowerCusName;
	}
	
	/**
	 * @param borrowRel
	 */
	public void setBorrowRel(String borrowRel) {
		this.borrowRel = borrowRel == null ? null : borrowRel.trim();
	}
	
    /**
     * @return BorrowRel
     */	
	public String getBorrowRel() {
		return this.borrowRel;
	}
	
	/**
	 * @param qryCls
	 */
	public void setQryCls(String qryCls) {
		this.qryCls = qryCls == null ? null : qryCls.trim();
	}
	
    /**
     * @return QryCls
     */	
	public String getQryCls() {
		return this.qryCls;
	}
	
	/**
	 * @param qryResn
	 */
	public void setQryResn(String qryResn) {
		this.qryResn = qryResn == null ? null : qryResn.trim();
	}
	
    /**
     * @return QryResn
     */	
	public String getQryResn() {
		return this.qryResn;
	}
	
	/**
	 * @param qryResnDec
	 */
	public void setQryResnDec(String qryResnDec) {
		this.qryResnDec = qryResnDec == null ? null : qryResnDec.trim();
	}
	
    /**
     * @return QryResnDec
     */	
	public String getQryResnDec() {
		return this.qryResnDec;
	}
	
	/**
	 * @param authbookNo
	 */
	public void setAuthbookNo(String authbookNo) {
		this.authbookNo = authbookNo == null ? null : authbookNo.trim();
	}
	
    /**
     * @return AuthbookNo
     */	
	public String getAuthbookNo() {
		return this.authbookNo;
	}
	
	/**
	 * @param authbookContent
	 */
	public void setAuthbookContent(String authbookContent) {
		this.authbookContent = authbookContent == null ? null : authbookContent.trim();
	}
	
    /**
     * @return AuthbookContent
     */	
	public String getAuthbookContent() {
		return this.authbookContent;
	}
	
	/**
	 * @param otherAuthbookExt
	 */
	public void setOtherAuthbookExt(String otherAuthbookExt) {
		this.otherAuthbookExt = otherAuthbookExt == null ? null : otherAuthbookExt.trim();
	}
	
    /**
     * @return OtherAuthbookExt
     */	
	public String getOtherAuthbookExt() {
		return this.otherAuthbookExt;
	}
	
	/**
	 * @param authbookDate
	 */
	public void setAuthbookDate(String authbookDate) {
		this.authbookDate = authbookDate == null ? null : authbookDate.trim();
	}
	
    /**
     * @return AuthbookDate
     */	
	public String getAuthbookDate() {
		return this.authbookDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param sendTime
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime == null ? null : sendTime.trim();
	}
	
    /**
     * @return SendTime
     */	
	public String getSendTime() {
		return this.sendTime;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param isSuccssInit
	 */
	public void setIsSuccssInit(String isSuccssInit) {
		this.isSuccssInit = isSuccssInit == null ? null : isSuccssInit.trim();
	}
	
    /**
     * @return IsSuccssInit
     */	
	public String getIsSuccssInit() {
		return this.isSuccssInit;
	}
	
	/**
	 * @param reportNo
	 */
	public void setReportNo(String reportNo) {
		this.reportNo = reportNo == null ? null : reportNo.trim();
	}
	
    /**
     * @return ReportNo
     */	
	public String getReportNo() {
		return this.reportNo;
	}
	
	/**
	 * @param qryStatus
	 */
	public void setQryStatus(String qryStatus) {
		this.qryStatus = qryStatus == null ? null : qryStatus.trim();
	}
	
    /**
     * @return QryStatus
     */	
	public String getQryStatus() {
		return this.qryStatus;
	}
	
	/**
	 * @param creditUrl
	 */
	public void setCreditUrl(String creditUrl) {
		this.creditUrl = creditUrl == null ? null : creditUrl.trim();
	}
	
    /**
     * @return CreditUrl
     */	
	public String getCreditUrl() {
		return this.creditUrl;
	}
	
	/**
	 * @param reportCreateTime
	 */
	public void setReportCreateTime(String reportCreateTime) {
		this.reportCreateTime = reportCreateTime == null ? null : reportCreateTime.trim();
	}
	
    /**
     * @return ReportCreateTime
     */	
	public String getReportCreateTime() {
		return this.reportCreateTime;
	}
	
	/**
	 * @param qryFlag
	 */
	public void setQryFlag(String qryFlag) {
		this.qryFlag = qryFlag == null ? null : qryFlag.trim();
	}
	
    /**
     * @return QryFlag
     */	
	public String getQryFlag() {
		return this.qryFlag;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo == null ? null : imageNo.trim();
	}
	
    /**
     * @return ImageNo
     */	
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}