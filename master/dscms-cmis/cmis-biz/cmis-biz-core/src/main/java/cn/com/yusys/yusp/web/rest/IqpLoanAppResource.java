/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.dto.IqpLoanAppLsUpdatDto;
import cn.com.yusys.yusp.dto.IqpLoanAppRetailDto;
import cn.com.yusys.yusp.dto.IqpLoanAppSLDto;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.service.LmtAppSubService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-03 15:22:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "业务申请信息")
@RequestMapping("/api/iqploanapp")
public class IqpLoanAppResource {
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppResource.class);


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanApp> list = iqpLoanAppService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanApp>> index(QueryModel queryModel) {
        List<IqpLoanApp> list = iqpLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppXw>>
     * @author hubp
     * @date 2021/5/31 9:43
     * @version 1.0.0
     * @desc 重写GET请求，请求参数自带
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/select")
    protected ResultDto<List<IqpLoanAppXw>> select(@RequestBody QueryModel queryModel) {
        List<IqpLoanAppXw> list = iqpLoanAppService.xwSelectByModel(queryModel);
        return new ResultDto<List<IqpLoanAppXw>>(list);
    }


    /**
     * @函数名称:selectIqpLoanAppListData
     * @函数描述:重写查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("重写查询对象列表")
    @PostMapping("/selectIqpLoanAppListData")
    protected ResultDto<List<IqpLoanApp>> selectIqpLoanAppListData(@RequestBody QueryModel queryModel) {
        List<IqpLoanApp> list = iqpLoanAppService.selectIqpLoanAppListData(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:selectIqpLoanAppHisListData
     * @函数描述:重写查询对象历史列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("重写查询对象历史列表")
    @PostMapping("/selectIqpLoanAppHisListData")
    protected ResultDto<List<IqpLoanApp>> selectIqpLoanAppHisListData(@RequestBody QueryModel queryModel) {
        List<IqpLoanApp> list = iqpLoanAppService.selectIqpLoanAppHisListData(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:aftercommitop02
     * @函数描述:跳过流程直接启用流程后处理方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/aftercommitop02")
    protected ResultDto<IqpLoanApp> afterCommitOp02(@RequestBody IqpLoanApp iqpLoanApp) throws Exception {
        iqpLoanAppService.handleBusinessDataAfterEnd(iqpLoanApp.getIqpSerno());
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }

    /**
     * @函数名称:aftercommitop05
     * @函数描述:跳过流程直接启用流程后处理方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/aftercommitop05")
    protected ResultDto<IqpLoanApp> afterCommitOp05(@RequestBody IqpLoanApp iqpLoanApp) throws Exception {
        iqpLoanAppService.handleBusinessDataAfterEnd(iqpLoanApp.getIqpSerno());
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }

    /***
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpLoanApp>>
     * @author hubp
     * @date 2021/4/27 21:56
     * @version 1.0.0
     * @desc    根据申请状态查询业务申请历史信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请状态查询业务申请历史信息")
    @GetMapping("/selectbystatus/")
    protected ResultDto<List<IqpLoanApp>> selectByStatus(QueryModel queryModel) {
        List<IqpLoanApp> list = iqpLoanAppService.selectByStatus(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpLoanApp> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }


    /**
     * @函数名称:queryByiqpSerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByiqpSerno")
    protected ResultDto<IqpLoanApp> queryByiqpSerno(@RequestBody String iqpSerno) {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }


    /**
     * @函数名称:show
     * @函数描述:重写show方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/show")
    protected ResultDto<IqpLoanApp> reWriteShow(@RequestBody String iqpSerno) {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }
    /**
     * @函数名称:selectByIqpSerno
     * @函数描述:根据流水号查询单个对象
     * @参数与返回说明:
     * @param iqpLoanApp
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbyiqpsernoxw")
    protected ResultDto<IqpLoanApp> selectByIqpSernoXw(@RequestBody IqpLoanApp iqpLoanApp) {
        IqpLoanApp iqpLoanAppTemp = iqpLoanAppService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
        return new ResultDto<IqpLoanApp>(iqpLoanAppTemp);
    }
    /**
     * 获取基本信息
     * @param iqpSerno
     * @return
     */
    @ApiOperation("获取基本信息")
    @GetMapping("/getBaseInfoDto/{iqpSerno}")
    protected ResultDto<HashMap<String,Object>> getBaseInfoDto(@PathVariable("iqpSerno") String iqpSerno){
        log.info("单笔单批业务申请信息查询【{}】"+ iqpSerno);
        HashMap<String,Object> retrunData = iqpLoanAppService.getBaseInfoDto(iqpSerno);
        return new ResultDto<>(retrunData);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanApp> create(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        iqpLoanAppService.insert(iqpLoanApp);
        return new ResultDto<IqpLoanApp>(iqpLoanApp);
    }
    /**
     * @函数名称:insertIqpLoanApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建")
    @PostMapping("/insertIqpLoanApp")
    protected ResultDto<IqpLoanApp> insertIqpLoanApp(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        IqpLoanApp result= iqpLoanAppService.insertIqpLoanApp(iqpLoanApp);
        return new ResultDto<IqpLoanApp>(result);
    }

    /**
     * @函数名称:insertIqpLoanApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷合同申请新增")
    @PostMapping("/insertIqpLoanAppForftin")
    protected ResultDto<IqpLoanApp> insertIqpLoanAppForftin(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        IqpLoanApp result= iqpLoanAppService.insertIqpLoanAppForftin(iqpLoanApp);
        return new ResultDto<IqpLoanApp>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        int result = iqpLoanAppService.updateSelective(iqpLoanApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/12 9:34
     * @version 1.0.0
     * @desc  小微合同申请修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updatexw")
    protected ResultDto<Integer> updateXw(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        int result = iqpLoanAppService.updateXw(iqpLoanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpLoanAppService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 通过业务主表的主键进行主表跟子表的逻辑删除
     */
    @ApiOperation("通过业务主表的主键进行主表跟子表的逻辑删除")
    @PostMapping("/deleteIqpInfo")
    protected ResultDto<Integer> delete(@RequestBody Map param) {
        log.info("业务申请数据删除！{}，入参信息为："+param.toString());
        int result = iqpLoanAppService.deleteIqpInfoByIqpSerno(param);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getDoReconsidFromBk
     * @函数描述:查询单笔单批业务复议是否存在
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/getDoReconsidFromBk/{iqpSerno}")
    public ResultDto<HashMap<String,String>> getDoReconsidFromBk(@PathVariable("iqpSerno") String iqpSerno){
        log.info("查询单笔单批业务复议是否存在 正在审批的流程开始【{}】"+ iqpSerno);
        ResultDto resultDto = new ResultDto();
        HashMap<String,String> map = iqpLoanAppService.getDoReconsidFromBk(iqpSerno);
        resultDto.setData(map);
        return resultDto;
    }
    /***
     * @param param
     * @return
     * @author hubp
     * @date 2021/4/26 21:43insertIqpLoanApp
     * @version 1.0.0
     * @desc 零售业务申请复议
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售业务申请复议")
    @PostMapping("/addreconsidiqploanapp")
    public ResultDto<String> reconsidIqpLoanApp(@RequestBody Map param) throws URISyntaxException {
        String iqpSerno = (String) param.get("iqpSerno");
        log.info("单笔单批业务复议信息旧流水号【{}】"+ iqpSerno);
        String iqpSernoNew = iqpLoanAppService.reconsidIqpLoanApp(iqpSerno);
        log.info("单笔单批业务复议信息保存完成,新流水号!"+iqpSernoNew);
        return new ResultDto<String>(iqpSernoNew);
    }
    /***
     * @param param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.HashMap<java.lang.String,java.lang.String>>
     * @author hubp
     * @date 2021/4/26 21:43
     * @version 1.0.0
     * @desc 零售业务申请复议-通流水号查询其是否存在待发起和审批中的复议申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通流水号查询其是否存在待发起和审批中的复议申请")
    @PostMapping("/getdoreconsidfrombk")
    public ResultDto<HashMap<String, String>> getDoReconsidFromBk(@RequestBody Map param) {
        String iqpSerno = (String) param.get("iqpSerno");
        log.info("查询单笔单批业务复议是否存在 正在审批的流程开始【{}】" + iqpSerno);
        ResultDto resultDto = new ResultDto();
        HashMap<String, String> map = iqpLoanAppService.getDoReconsidFromBk(iqpSerno);
        resultDto.setData(map);
        return resultDto;
    }


    /**
     * 校验申请金额与担保合同金额
     * @param params
     * @return
     */
    @ApiOperation("校验申请金额与担保合同金额")
    @PostMapping("/checkGuarAmtAndAppAmt")
    public ResultDto<Map> checkGuarAmtAndAppAmt(@RequestBody Map params){
        Map rtnData = iqpLoanAppService.checkGuarAmtAndAppAmt(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 查看拒绝记录
     * @param cusId
     * @return
     */
    @ApiOperation("查看拒绝记录")
    @GetMapping("/getCusRejectByCusId/{cusId}")
    protected ResultDto<Integer> getCusRejectByCusId(@PathVariable("cusId") String cusId){
        int count = iqpLoanAppService.getCusRejectByCusId(cusId);
        return new ResultDto<>(count);
    }

    /**
     * 单笔单批提交前校验
     * @return
     */
    @ApiOperation("单笔单批提交前校验")
    @GetMapping("/checkBeforeCommit/{iqpSerno}")
    protected ResultDto<String> checkBeforeCommit(@PathVariable("iqpSerno") String iqpSerno){
        ResultDto<String> resultDto = new ResultDto();
        try {
            String ok = iqpLoanAppService.checkBeforeCommit(iqpSerno);
            resultDto.setData(ok);
        }catch (YuspException e){
            resultDto.setCode(e.getCode());
            resultDto.setMessage(e.getMessage());
        }
        return resultDto;
    }


    /**
     * 根据协议编号查询所有下属合同的业务申请流水号
     * @param lmtCtrNo
     * @return
     */
    @ApiOperation("根据协议编号查询所有下属合同的业务申请流水号")
    @PostMapping("/getIqpSernoNumbers")
    protected List<String> getIqpSernoNumbers(@RequestBody String  lmtCtrNo){
        return this.iqpLoanAppService.getIqpSernoNumbers(lmtCtrNo);
    }

    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     * @param lmtCtrNo
     * @return
     */
    @ApiOperation("注销合作方授信协议，校验逻辑，返回生效状态的合同数量")
    @PostMapping("/getZaiTuAppCount")
    public Integer getZaiTuAppCount(@RequestBody String lmtCtrNo) {
        return this.iqpLoanAppService.getZaiTuAppCount(lmtCtrNo);
    }



    /**
     * 根据个人授信协议编号 查询在途的业务申请（特殊业务、额度项下业务）申请流水号
     * @param map
     * @return
     */
    @ApiOperation("根据个人授信协议编号 查询在途的业务申请（特殊业务、额度项下业务）申请流水号")
    @PostMapping("/getIqpsernoByLmtCtrNo")
    protected List<String> getIqpsernoByLmtCtrNo(@RequestBody Map map){
        log.info("根据个人授信协议编号查询在途的业务申请（特殊业务、额度项下业务）申请流水号..............");
        return iqpLoanAppService.getIqpsernoByLmtCtrNo(map);
    }


    /**
     * 贸易融资合同申请保存操作
     * @param iqpTcontApp
     * @return
     */
    @ApiOperation("贸易融资合同申请保存操作")
    @PostMapping("/savetcontappinfo")
    public ResultDto<Map> saveIqpTcontAppInfo(@RequestBody IqpLoanApp iqpTcontApp){
        Map result = iqpLoanAppService.saveIqpTcontAppInfo(iqpTcontApp);
        return new ResultDto<>(result);
    }

    /**
     * 贸易融资合同申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("贸易融资合同申请通用的保存方法")
    @PostMapping("/commonsavetcontappinfo")
    public ResultDto<Map> commonSaveIqpTcontAppInfoInfo(@RequestBody Map params){
        Map rtnData = iqpLoanAppService.commonSaveIqpTcontAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @param
     * @return String
     * @author shenli
     * @date 2021/4/20 0020 10:05
     * @version 1.0.0
     * @desc 根据贷款发放账号获取放款账号名称
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据贷款发放账号获取放款账号名称")
    @PostMapping("/getLoanPayoutAccName/{loanPayoutAccNo}")
    protected HashMap<String, String> getLoanPayoutAccName(@PathVariable String loanPayoutAccNo){
        log.info("根据贷款发放账号获取放款账号名称..............");
        return iqpLoanAppService.getLoanPayoutAccName(loanPayoutAccNo);
    }

    /**
     * 福费廷合同申请保存操作
     * @param iqpForftinApp
     * @return
     */
    @ApiOperation("福费廷合同申请保存")
    @PostMapping("/saveforftinappinfo")
    public ResultDto<Map> saveIqpForftinAppInfo(@RequestBody IqpLoanApp iqpForftinApp){
        Map result = iqpLoanAppService.saveIqpForftinAppInfo(iqpForftinApp);
        return new ResultDto<>(result);
    }

    /**
     * 福费廷合同申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("福费廷合同申请通用保存操作")
    @PostMapping("/commonsaveforftinapp")
    public ResultDto<Map> commonSaveIqpForftinAppInfo(@RequestBody Map params){
        Map rtnData = iqpLoanAppService.commonSaveIqpForftinAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 福费廷合同申请数据反显方法
     * @param params
     * @return
     */
    @ApiOperation("福费廷合同申请数据反显")
    @PostMapping("/forftinshowdetial")
    protected ResultDto<Object> forftinShowDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpLoanApp temp = new IqpLoanApp();
        IqpLoanApp studyDemo = iqpLoanAppService.selectByPrimaryKey((String)params.get("iqpSerno"));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:delete
     * @函数描述:物理删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷合同申请逻辑删除")
    @PostMapping("/forftinlogicdelete")
    public ResultDto<Map> forftinLogicDelete(@RequestBody IqpLoanApp iqpLoanApp) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = iqpLoanAppService.forftinLogicDelete(iqpLoanApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:forftinToSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷合同申请列表")
    @PostMapping("/forftintosignlist")
    protected ResultDto<List<IqpLoanApp>> forftinToSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpLoanApp> list = iqpLoanAppService.forftinToSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:forftinDoneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷合同申请历史列表")
    @PostMapping("/forftindonesignlist")
    protected ResultDto<List<IqpLoanApp>> forftinDoneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpLoanApp> list = iqpLoanAppService.forftinDoneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * 根据【LPR定价区间】的选择，自动从核心获取当前LPR利率。
     * @param newVal
     * @return
     */
    @ApiOperation("根据【LPR定价区间】的选择，自动从核心获取当前LPR利率")
    @PostMapping("/nextIqoLoanAppSell/{newVal}")
    protected ResultDto<Map> getLprRate(@PathVariable("cusId") String newVal){
        Map rtnData = iqpLoanAppService.getLprRate(newVal);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 保存零售业务申请基本信息
     * @param iqpLoanAppRetailDto
     * @return
     */
    @ApiOperation("保存零售业务申请基本信息")
    @PostMapping("/saveiqploanappsell")
    protected ResultDto<IqpLoanApp> saveIqpLoanAppSell(@RequestBody IqpLoanAppRetailDto iqpLoanAppRetailDto){
        return new ResultDto<IqpLoanApp>( iqpLoanAppService.saveIqpLoanAppSell(iqpLoanAppRetailDto));
    }
    /**
     * 保存零售业务申请基本信息
     * @param iqpLoanAppSLDto
     * @return
     */
    @ApiOperation("保存零售业务申请基本信息")
    @PostMapping("/updateiqpLoanappsell")
    protected ResultDto updateIqpLoanAppSell(@RequestBody IqpLoanAppSLDto iqpLoanAppSLDto){
        return new ResultDto<>(iqpLoanAppService.updateIqpLoanAppAndAssit(iqpLoanAppSLDto));
    }

    /**
     * 保存零售业务申请基本信息
     * @param iqpLoanAppLsUpdatDto
     * @return
     */
    @ApiOperation("保存零售业务申请基本信息-移动作业")
    @PostMapping("/updateIqpLoanAppSellOam")
    protected ResultDto updateIqpLoanAppSellOam(@RequestBody IqpLoanAppLsUpdatDto iqpLoanAppLsUpdatDto){
        return new ResultDto<>(iqpLoanAppService.updateIqpLoanAppSellOam(iqpLoanAppLsUpdatDto));
    }


    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author shenli
     * @date 2021/4/28 0028 20:54
     * @version 1.0.0
     * @desc 小微合同申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("小微合同申请")
    @PostMapping("/xw/createIqpLoanApp")
    protected ResultDto<Map<String,Object>> createIqpLoanAppXw(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        return new ResultDto<Map<String,Object>>(iqpLoanAppService.createIqpLoanAppXw(iqpLoanApp));
    }

    /**
     * @函数名称:tcontToSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资合同申请列表")
    @PostMapping("/tconttosignlist")
    protected ResultDto<List<IqpLoanApp>> tcontToSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpLoanApp> list = iqpLoanAppService.tcontToSignlist(queryModel);
        ResultDto<List<IqpLoanApp>> resultDto = new ResultDto<List<IqpLoanApp>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @函数名称:tcontDoneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资合同申请历史列表")
    @PostMapping("/tcontdonesignlist")
    protected ResultDto<List<IqpLoanApp>> tcontDoneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpLoanApp> list = iqpLoanAppService.tcontDoneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpLoanApp>>(list);
    }
    /***
     * @param
     * @return cn.com.yusys.yusp.domain.CtrLoanCont
     * @author hubp
     * @date 2021/5/11 16:39
     * @version 1.0.0
     * @desc    通过申请流水号，查询合同信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过申请流水号，查询合同信息")
    @PostMapping("/selectbyiqpserno")
    protected ResultDto<CtrLoanCont> selectByIqpSerno(@RequestBody CtrLoanCont ctrLoanCont){
        String iqpSerno = ctrLoanCont.getIqpSerno();
        CtrLoanCont ctrLoanContOne = iqpLoanAppService.selectContByIqpSerno(iqpSerno);  //
        return new ResultDto<CtrLoanCont>(ctrLoanContOne);
    }
    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author wzy
     * @date 2021/4/28 0028 20:54
     * @version 1.0.0
     * @desc 零售业务删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售业务删除")
    @PostMapping("/deleteiqpbyiqpserno")
    protected ResultDto<Integer> deleteIqpbyIqpSerno(@RequestBody String iqpSerno) throws URISyntaxException {
        return new ResultDto<Integer>(iqpLoanAppService.deleteIqpbyIqpSerno(iqpSerno));
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author wzy
     * @date 2021/4/28 0028 20:54
     * @version 1.0.0
     * @desc 零售业务查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售业务查询")
    @PostMapping("/selectbymodel")
    protected ResultDto<List<IqpLoanApp>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<IqpLoanApp> list = iqpLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanApp>>(list);
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author wzy
     * @date 2021/4/28 0028 20:54
     * @version 1.0.0
     * @desc 零售业务查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售业务信息校验")
    @PostMapping("/confirmiqploanapp")
    protected  ResultDto<IqpLoanAppSLDto> ocnfirmIqpLoanApp(@RequestBody IqpLoanAppSLDto iqpLoanAppSLDto) {
        return new ResultDto<IqpLoanAppSLDto>(iqpLoanAppService.confirmIqpLoanApp(iqpLoanAppSLDto));
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请明细展示")
    @PostMapping("/showtcontdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno((String)map.get("serno"));
        if (iqpLoanApp != null) {
            resultDto.setCode(0);
            resultDto.setData(iqpLoanApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(0);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpLoanAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }
    /**
     * @param iqpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/5/27 14:44
     * @version 1.0.0
     * @desc    小微合同申请-合同信息级联删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("小微合同申请-合同信息级联删除")
    @PostMapping("/deletecontiqpinfo")
    protected  ResultDto deleteContIqpInfo(@RequestBody String iqpSerno) {
        return iqpLoanAppService.deleteContIqpInfo(iqpSerno);
    }
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/4 10:02
     * @version 1.0.0
     * @desc    小微合同申请-额度批复信息查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("小微合同申请-额度批复信息查询")
    @PostMapping("/selectdata")
    protected  ResultDto selectData(@RequestBody QueryModel queryModel) {
        return iqpLoanAppService.selectData(queryModel);
    }

    /**
     * 贸易融资合同申请逻辑删除方法
     * @param iqpLoanApp
     * @return
     */
    @ApiOperation("贸易融资合同申请逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpLoanApp iqpLoanApp){
        Integer result = iqpLoanAppService.logicDelete(iqpLoanApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据【LPR定价区间】的选择，自动从核心获取当前LPR利率。
     * @param params
     * @return
     */
    @ApiOperation("根据【LPR定价区间】的选择，自动从核心获取当前LPR利率")
    @PostMapping("/getlprrate")
    protected ResultDto<Map> getLprRate(@RequestBody Map params){
        String newVal = (String)params.get("lprRate");
        Map rtnData = iqpLoanAppService.getLprRate(newVal);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map>
     * @author hubp
     * @date 2021/8/18 17:15
     * @version 1.0.0
     * @desc  新增为移动OA测试 -- LPR
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getlprrateoa")
    protected ResultDto<Map> getLprRate(@RequestBody IqpLoanApp iqpLoanApp){
        Map rtnData = iqpLoanAppService.getLprRate(iqpLoanApp.getLprRateIntval());
        return new ResultDto<Map>(rtnData);
    }

    @PostMapping("/getcfgtfrate")
    protected ResultDto<Map> getCfgTfRate(@RequestBody CfgTfRateQueryDto cfgTfRateQueryDto) {
        Map rtnData = iqpLoanAppService.getCfgTfRate(cfgTfRateQueryDto);
        return new ResultDto<Map>(rtnData);
    }
    /**
     * @param
     * @return
     * @author shenli
     * @date 2021/6/15 13:55
     * @version 1.0.0
     * @desc 零售删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售删除")
    @PostMapping("/deleteByIqpSerno")
    protected ResultDto<Integer> deleteByIqpSerno(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        return new ResultDto<Integer>(iqpLoanAppService.deleteIqpbyIqpSerno(iqpLoanApp.getIqpSerno()));
    }


    /**
     * @函数名称:queryByiqpSerno
     * @函数描述:根据流水查询业务申请详细信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByiqpSernoDetail")
    protected ResultDto<IqpLoanApp> queryByiqpSernoDetail(@RequestBody IqpLoanApp iqpLoanApp) {
        return new ResultDto<IqpLoanApp>(iqpLoanAppService.selectByPrimaryKey(iqpLoanApp.getIqpSerno()));
    }

    /**
     * @函数名称:getAllIqpByInputId
     * @函数描述:根据客户经理工号查询合同申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询合同待签订数据")
    @PostMapping("/getAllIqpByInputId")
    protected ResultDto<List<Map<String, Object>>> getAllIqpByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = iqpLoanAppService.getAllIqpByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:selectfdccount
     * @函数描述:根据流水号查询"房地产抵押"的方式条数
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据流水号查询房地产抵押的方式条数")
    @PostMapping("/selectfdccount")
    protected ResultDto<Integer> selectFdcCount(@RequestBody String iqpSerno) {
        return new ResultDto<Integer>(iqpLoanAppService.selectFdcCount(iqpSerno));
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-6-30 10:14:28
     * @注释 还款测算-第一个月应还款
     */
    @ApiOperation("还款测算-第一个月应还款")
    @PostMapping("/selectcalculate")
    protected ResultDto selectcalculate(@RequestBody IqpLoanApp iqpLoanApp) {
        return iqpLoanAppService.selectcalculate(iqpLoanApp);
    }

    /**
     * @函数名称:updateDelete
     * @函数描述: 普通贷款合同申请 逻辑删除更新
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateDelete")
    protected ResultDto<Integer> updateDelete(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        int result = iqpLoanAppService.updateDelete(iqpLoanApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateDelete
     * @函数描述: 根据业务品种判断合同模式
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/changecontmode")
    protected ResultDto changeContMode(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        return iqpLoanAppService.changeContMode(iqpLoanApp.getIqpSerno(),iqpLoanApp.getContMode());
    }


    /**
     * @函数名称:selectfdccount
     * @函数描述:根据客户号查询是否有在途业务-小微客户批量移交
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户号查询是否有在途业务-小微客户批量移交")
    @PostMapping("/selectXwztCount")
    protected ResultDto<Integer> selectXwztCount(@RequestBody String cusId) {
        int result = iqpLoanAppService.selectXwztCount(cusId);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectByIqpSernoRetail
     * @函数描述:根据流水号查询单个对象
     * @参数与返回说明:
     * @param iqpLoanApp
     * @算法描述: 零售业务申请根据流水号查询基本信息
     */
    @PostMapping("/selectbyiqpsernoretail")
    protected ResultDto<IqpLoanApp> selectByIqpSernoRetail(@RequestBody IqpLoanApp iqpLoanApp) {
        IqpLoanApp iqpLoanAppTemp = iqpLoanAppService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
        return new ResultDto<IqpLoanApp>(iqpLoanAppTemp);
    }

    /**
     * @函数名称:getExchangeRate
     * @函数描述:根据币种获取实时汇率
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @ApiOperation("根据币种获取实时汇率")
    @PostMapping("/getexchangerate")
    protected ResultDto<Map> getExchangeRate(@RequestBody Map map) {
        Map rtnData = iqpLoanAppService.getExchangeRate(map);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @函数名称:selectAppAmtBuGuarRelSerno
     * @函数描述:根据流水号查询单个对象
     * @参数与返回说明:
     * @param iqpLoanApp
     * @算法描述: 根据担保信息关系流水号查询关联业务的申请金额
     */
    @PostMapping("/selectappamtbuguarrelserno")
    protected ResultDto<Map> selectAppAmtBuGuarRelSerno(@RequestBody IqpLoanApp iqpLoanApp) {
        Map rtnData = new HashMap();
        IqpLoanApp iqpLoanAppTemp = iqpLoanAppService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
        if(iqpLoanAppTemp != null && !StringUtils.isBlank(iqpLoanAppTemp.getIqpSerno())){
            rtnData.put("appAmt", iqpLoanAppTemp.getAppAmt());
        }else{
            LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(iqpLoanApp.getIqpSerno());
            rtnData.put("appAmt", lmtAppSub.getLmtAmt());
        }
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @函数名称:queryCtrLoanContDataBySerno
     * @函数描述:零售-业务申请历史-作废
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("零售-业务申请-作废 ")
    @PostMapping("/canceliqpretail")
    protected ResultDto<Integer> cancelIqpRetail(@RequestBody String IqpSerno) {
        return new ResultDto<Integer>(iqpLoanAppService.cancelIqpRetail(IqpSerno));
    }

    /**
     * @函数名称:isAllowContApp
     * @函数描述:判断所选分项是否能做最高额授信协议或其他合同
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/isallowcontapp")
    protected ResultDto<Boolean> isAllowContApp(@RequestBody Map map) {
        return iqpLoanAppService.isAllowContApp(map);
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.String>
     * @author hubp
     * @date 2021/9/3 16:35
     * @version 1.0.0
     * @desc  根据产品ID获取产品相关信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getprdinfo")
    protected ResultDto<String> getPrdInfo(@RequestBody IqpLoanApp iqpLoanApp) {
        return iqpLoanAppService.getPrdInfo(iqpLoanApp.getIqpSerno());
    }

    /**
     * @函数名称:submitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/submitNoFlow")
    protected ResultDto<Map> submitNoFlow(@RequestBody IqpLoanApp iqpLoanApp) throws URISyntaxException {
        Map rtnData = iqpLoanAppService.submitNoFlow(iqpLoanApp.getIqpSerno());
        return new ResultDto<>(rtnData);
    }


    @PostMapping("/handleBizEnd")
    protected ResultDto<Map> handleBizEnd(@RequestBody IqpLoanApp iqpLoanApp) throws Exception {
        iqpLoanAppService.handleBizEnd(iqpLoanAppService.selectByPrimaryKey(iqpLoanApp.getIqpSerno()));
        return new ResultDto<>();
    }

    @PostMapping("/sendmessage")
    protected ResultDto<Map> sendmessage(@RequestBody IqpLoanApp iqpLoanApp) throws Exception {
        iqpLoanAppService.sendMessage(iqpLoanApp);
        return new ResultDto<>();
    }

    /**
     * @函数名称:isAllowContApp
     * @函数描述:判断所选分项是否能做最高额授信协议或其他合同
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/isexistaspl")
    protected ResultDto<Boolean> isExistAspl(@RequestBody Map map) {
        return iqpLoanAppService.isExistAspl(map);
    }

    /**
     * @函数名称:isAllowContApp
     * @函数描述:判断是否允许债项评级测算
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/isallowladeval")
    protected ResultDto<Map> isAllowLadEval(@RequestBody Map map) {
        Map result =  iqpLoanAppService.isAllowLadEval(map);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:isOnWayAppFlow
     * @函数描述:判断客户名下是否存在在途的合同申请流程
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/isonwayappflow")
    protected ResultDto<Boolean> isOnWayAppFlow(@RequestBody Map map) {
        Boolean isOnWayFlag =  iqpLoanAppService.isOnWayAppFlow(map);
        return new ResultDto<Boolean>(isOnWayFlag);
    }
}
