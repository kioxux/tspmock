/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.IqpDiscAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiscAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:03:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpDiscAppService {

    private static final Logger log = LoggerFactory.getLogger(IqpDiscAppService.class);
    @Autowired
    private IqpDiscAppMapper iqpDiscAppMapper;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private CtrDiscContService ctrDiscContService;//贴现合同主表

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;//BSP封装调用核心系统的接口

    @Resource
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private IqpDiscAppPorderSubService iqpDiscAppPorderSubService;

    @Autowired
    private CtrDiscPorderSubService ctrDiscPorderSubService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpDiscApp selectByPrimaryKey(String pkId) {
        return iqpDiscAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<IqpDiscApp> selectAll(QueryModel model) {
        List<IqpDiscApp> records = (List<IqpDiscApp>) iqpDiscAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<IqpDiscApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDiscApp> list = iqpDiscAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(IqpDiscApp record) {
        return iqpDiscAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(IqpDiscApp record) {
        return iqpDiscAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(IqpDiscApp record) {
        return iqpDiscAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(IqpDiscApp record) {
        return iqpDiscAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return iqpDiscAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return iqpDiscAppMapper.deleteByIds(ids);
    }


    /**
     * 贴现协议申请新增页面点击下一步
     *
     * @param iqpDiscApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpDiscAppInfo(IqpDiscApp iqpDiscApp) throws Exception {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpDiscApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpDiscApp.getCusId();
            // 将 地址与联系方式 信息更新值贷款申请表中
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",cusId);
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",cusId, cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpDiscApp.setPhone(CusIndivAllDto.getMobile());
                    iqpDiscApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpDiscApp.setEmail(CusIndivAllDto.getEmail());
                    iqpDiscApp.setQq(CusIndivAllDto.getQq());
                    iqpDiscApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpDiscApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpDiscApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpDiscApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpDiscApp.setFax(cusCorpDto.getFax());
                    iqpDiscApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpDiscApp.setQq(cusCorpDto.getQq());
                    iqpDiscApp.setWechat(cusCorpDto.getWechatNo());
                    iqpDiscApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpDiscApp.setInputId(userInfo.getLoginCode());
                iqpDiscApp.setInputBrId(userInfo.getOrg().getCode());
                iqpDiscApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }

            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            HashMap<String, String> param = new HashMap<>();
            String dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_03);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);

            iqpDiscApp.setSerno(serno);
            iqpDiscApp.setContNo(contNo);
            if(CmisCommonConstants.STD_DRFT_TYPE_1.equals(iqpDiscApp.getDrftType())){
                iqpDiscApp.setGuarMode(CmisCommonConstants.GUAR_MODE_21);
            }
//            if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
//                iqpDiscApp.setContType(CmisCommonConstants.STD_CONT_TYPE_1);
//            }else {
//                iqpDiscApp.setContType(CmisCommonConstants.STD_CONT_TYPE_2);
//            }
            iqpDiscApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpDiscApp.setPkId(StringUtils.uuid(true));
            iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpDiscApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpDiscApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_03);
            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpDiscApp.getLmtAccNo())){
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpDiscApp.getLmtAccNo());
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",iqpDiscApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpDiscApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            int insertCount = iqpDiscAppMapper.insertSelective(iqpDiscApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("serno", serno);
            log.info("贴现协议申请" + serno + "-新增成功！");

            // 生成用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpDiscApp.getReplyNo());
            queryMap.put("contNo",iqpDiscApp.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(iqpDiscApp.getCusId());
            creditReportQryLstAndRealDto.setCusId(iqpDiscApp.getCusId());
            creditReportQryLstAndRealDto.setCusName(iqpDiscApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            log.error("保证金账户信息新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存保证金账户信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 贴现申请提交保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpDiscAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "贴现申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpDiscApp iqpDiscApp = JSONObject.parseObject(JSON.toJSONString(params), IqpDiscApp.class);
            if (iqpDiscApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }

            log.info(logPrefix + "保存贴现额度申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpDiscApp.setUpdId(userInfo.getLoginCode());
                iqpDiscApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpDiscApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存贴现申请数据");
            int updCount = iqpDiscAppMapper.updateByPrimaryKeySelective(iqpDiscApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存贴现额度申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("流程发起-获取贴现合同申请" + iqpSerno + "申请主表信息");
        IqpDiscApp iqpDiscApp = iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);
        if (iqpDiscApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        //占额成功后，更新审批状态
        int updateCount = 0;
        log.info("流程发起-更新贴现合同申请" + iqpSerno + "流程审批状态为【111】-审批中");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
        if (updateCount <= 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }

        //向额度系统发送接口,占用额度
        this.sendToLmt(iqpDiscApp);
    }

    public void sendToLmt(IqpDiscApp iqpDiscApp) {
        /**
         * 流程发起的占额处理
         * 1.根据票据类型，进行不同的占额处理
         * 2.银行承兑汇票时：
         * <1>该客户名下存在低风险品种分项，直接按照普通低风险占额处理;
         * <2>不存在低风险品种分项时，先反向生成低风险额度，再按照普通低风险占额处理（在流程结束时处理）;
         * 3.商业承兑汇票(双占)：
         * <1>申请客户只能选到商票贴现额度产品，承兑企业客户只能选到商票保贴额度产品（赋值担保方式）
         * <2>申请客户的占额为一般额度
         * <3>承兑企业客户的占额直接根据担保方式判断是否为低风险额度
         */
        String guarMode = iqpDiscApp.getGuarMode();
        String drftType = iqpDiscApp.getDrftType();

        String dealBizType = "";
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            dealBizType = "1";
        }else{
            dealBizType = "2";
        }
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        //实际占额金额
        BigDecimal actualAmt = BigDecimal.ZERO;
        /*
         * 1.一般贴现协议 实际占额金额为票面总金额
         * 2.贴现额度协议 实际占额金额为合同金额
         */
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            //一般贴现协议
            actualAmt = iqpDiscApp.getDrftTotalAmt();
        }else{
            //贴现额度协议
            actualAmt = iqpDiscApp.getContAmt();
        }
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpDiscApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpDiscApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //银行承兑贴现
        if (CmisCommonConstants.STD_DRFT_TYPE_1.equals(drftType)) {
            //自动挂接低风险产品分项，没有自动挂接的情况会反向生成低风险额度（流程结束，反向生成后占用额度）
            if (iqpDiscApp.getLmtAccNo() != null && !"".equals(iqpDiscApp.getLmtAccNo())) {
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
                ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(resultDtoDto));
                if(!"0".equals(resultDtoDto.getCode())){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if(!"0000".equals(code)){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
        //商业承兑汇票(双占)
        else {
            //判断承兑企业客户是否为低风险额度
            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
                    CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                //调额度接口占额
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<>();

                //申请人客户信息
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDto.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);

                //承兑企业客户信息
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo(iqpDiscApp.getAcptCrpLmtNo());//额度分项编号
                cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);

                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
                ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(resultDtoDto));
                if(!"0".equals(resultDtoDto.getCode())){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if(!"0000".equals(code)){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else {
                //调额度接口占额
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<>();

                //申请人客户信息
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDto.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);

                //承兑企业客户信息
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo(iqpDiscApp.getAcptCrpLmtNo());//额度分项编号
                cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);

                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
                ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(resultDtoDto));
                if(!"0".equals(resultDtoDto.getCode())){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if(!"0000".equals(code)){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + iqpSerno + "申请信息");
            IqpDiscApp iqpDiscApp = iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);

            if (iqpDiscApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpDiscAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("贴现协议申请否决流程-获取贴现协议申请" + iqpSerno + "申请信息");
            IqpDiscApp iqpDiscApp = iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);
            if (iqpDiscApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (iqpDiscApp.getLmtAccNo() != null && !"".equals(iqpDiscApp.getLmtAccNo())) {
                //实际占额金额
                BigDecimal actualAmt = BigDecimal.ZERO;
                /*
                 * 1.一般贴现协议 实际占额金额为票面总金额
                 * 2.贴现额度协议 实际占额金额为合同金额
                 */
                if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
                    //一般贴现协议
                    actualAmt = iqpDiscApp.getDrftTotalAmt();
                }else{
                    //贴现额度协议
                    actualAmt = iqpDiscApp.getContAmt();
                }
                String drftType = iqpDiscApp.getDrftType();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.STD_DRFT_TYPE_1.equals(drftType)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = actualAmt;
                } else {
                    recoverSpacAmtCny = actualAmt;
                    recoverAmtCny = actualAmt;
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpDiscApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpDiscApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("贴现协议申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("贴现协议申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    //修改审批状态方法
    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpDiscAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     * @param iqpSerno
     */
    @Transactional
    public void handleBusinessDataAfterEnd(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取贴现合同申请" + iqpSerno + "申请主表信息");
        IqpDiscApp iqpDiscApp = iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);
        if (iqpDiscApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        log.info("审批通过-更新贴现业务申请" + iqpSerno + "流程审批状态为【997】-通过");
        int count = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
        iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        if(count <= 0){
            log.info("审批通过-更新贴现业务申请" + iqpSerno + "流程审批状态为【{}】异常，更新笔数为【{}】", CmisCommonConstants.WF_STATUS_997, count);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        String drftType = iqpDiscApp.getDrftType();

        // 根据贴现协议类型获取交易业务类型
        String dealBizType = "";
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            dealBizType = "1";
        }else {
            dealBizType = "2";
        }

        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";

        //实际占额金额
        BigDecimal actualAmt = BigDecimal.ZERO;
        /*
         * 1.一般贴现协议 实际占额金额为票面总金额
         * 2.贴现额度协议 实际占额金额为合同金额
         */
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            //一般贴现协议
            actualAmt = iqpDiscApp.getDrftTotalAmt();
        }else{
            //贴现额度协议
            actualAmt = iqpDiscApp.getContAmt();
        }

        // 判断是否合同续签
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpDiscApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpDiscApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        String guarMode = iqpDiscApp.getGuarMode();

        // 判断是否需要反向生成低风险额度,反向生成后占这笔新生成的低风险额度
        if (CmisCommonConstants.STD_DRFT_TYPE_1.equals(drftType) && (iqpDiscApp.getLmtAccNo() == null || "".equals(iqpDiscApp.getLmtAccNo()))) {
            log.info("贴现协议申请: " + iqpSerno + " 反向生成低风险额度开始");
            BigDecimal bailPerc = BigDecimal.ZERO;//保证金比例
            BigDecimal chrgRate = BigDecimal.ZERO;//手续费率
            String isDisc = "";
            String endDate = "";
            if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
                isDisc = CmisCommonConstants.STD_ZB_YES_NO_1;
                endDate = "";
            }else {
                isDisc = CmisCommonConstants.STD_ZB_YES_NO_0;
                endDate = iqpDiscApp.getEndDate();
            }
            boolean result = lmtReplyAccService.generaLmtReplyAccForLowRisk(actualAmt, iqpDiscApp.getCusId(), iqpDiscApp.getBusiType(), bailPerc, chrgRate,endDate,isDisc);
            if (!result) {
                log.error("贴现协议申请: " + iqpSerno + " 反向生成低风险额度分项异常");
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            log.info("贴现协议申请: " + iqpSerno + " 反向生成低风险额度结束");
            String replySerno = "";
            String lmtAccNo = "";
            log.info("贴现协议申请: "+iqpSerno+"获取批复编号和授信额度编号开始");
            // 根据客户号以及产品编号查询生成的分项下对应的低风险分项明细
            CmisLmt0056ReqDto cmisLmt0056ReqDto = new CmisLmt0056ReqDto();
            cmisLmt0056ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));
            cmisLmt0056ReqDto.setPrdId(iqpDiscApp.getPrdId());
            cmisLmt0056ReqDto.setCusId(iqpDiscApp.getCusId());
            log.info("贴现协议业务申请【{}】，前往额度系统查询低风险分项明细,请求报文为:【{}】", iqpSerno, cmisLmt0056ReqDto.toString());
            ResultDto<CmisLmt0056RespDto> resultDto = cmisLmtClientService.cmislmt0056(cmisLmt0056ReqDto);
            log.info("贴现协议业务申请【{}】，前往额度系统查询低风险分项明细,响应报文为:【{}】", iqpSerno, resultDto.toString());
            if(!"0".equals(resultDto.getCode())){
                log.error("额度0056接口调用异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(resultDto.getData().getErrorCode())){
                log.error("查询低风险分项明细失败！异常信息为【{}】",resultDto.getData().getErrorMsg());
                throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
            }
            lmtAccNo = resultDto.getData().getApprSubSerno();
            log.info("贴现协议申请【{}】的授信额度编号为【{}】",iqpSerno,lmtAccNo);
            // 根据授信额度编号获取批复流水号
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                log.error("根据授信额度编号【{}】查询授信产品明细为空！",lmtAccNo);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }

            Map map = new HashMap();
            map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
            if(Objects.isNull(lmtReplyAccSub)){
                log.error("根据分项额度编号【{}】查询授信分项明细为空！",lmtReplyAccSubPrd.getAccSubNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            replySerno = lmtReplyAccSub.getReplySerno();
            log.info("贴现协议申请【{}】的批复编号为【{}】",iqpSerno,replySerno);

            //生成低风险分项后，更新申请数据
            int updateCount = 0;
            log.info("更新贴现协议业务申请" + iqpSerno + "开始");
            IqpDiscApp iqpDiscAppData = iqpDiscAppMapper.selectByIqpSerno(iqpSerno);
            iqpDiscAppData.setLmtAccNo(lmtAccNo);
            iqpDiscAppData.setReplyNo(replySerno);
            log.info("批复编号为【{}】,授信额度编号为【{}】",lmtAccNo,replySerno);
            updateCount = this.updateSelective(iqpDiscAppData);
            if (updateCount < 0) {
                log.error("更新贴现协议业务申请【{}】异常",iqpSerno);
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            log.info("更新贴现协议业务申请" + iqpSerno + "结束");
            //发额度接口占用额度
            IqpDiscApp iqpDiscAppObj = iqpDiscAppMapper.selectByIqpSerno(iqpSerno);
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpDiscAppObj.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpDiscAppObj.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpDiscAppObj.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpDiscAppObj.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpDiscAppObj.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpDiscAppObj.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpDiscAppObj.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpDiscAppObj.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpDiscAppObj.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpDiscAppObj.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscAppObj.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String responseCode = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(responseCode)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }else{
            // 银行承兑汇票且足额--直接走低风险占额
            if(CmisCommonConstants.STD_DRFT_TYPE_1.equals(drftType) && (iqpDiscApp.getLmtAccNo()!=null || !"".equals(iqpDiscApp.getLmtAccNo()))){
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
                ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
                if(!"0".equals(resultDtoDto.getCode())){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String responseCode = resultDtoDto.getData().getErrorCode();
                if(!"0000".equals(responseCode)){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
            if (CmisCommonConstants.STD_DRFT_TYPE_2.equals(drftType)) {
                //商业承兑汇票(双占)
                //判断承兑企业客户是否为低风险额度
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
                        CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    //调额度接口占额
                    CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                    cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                    cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                    cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                    cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                    cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                    cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                    cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                    cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                    cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
                    cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                    cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                    cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                    cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                    cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                    cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                    cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                    cmisLmt0011ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                    cmisLmt0011ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
                    cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                    cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                    cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                    cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                    cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                    cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                    List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<>();

                    //申请人客户信息
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                    cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                    cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                    cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                    cmisLmt0011OccRelListDto.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                    cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                    cmisLmt0011OccRelListDto.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);

                    //承兑企业客户信息
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo(iqpDiscApp.getAcptCrpLmtNo());//额度分项编号
                    cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp("");//授信品种类型
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(actualAmt);//占用总额(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);

                    cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                    log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
                    ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                    log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
                    if(!"0".equals(resultDtoDto.getCode())){
                        log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    String code = resultDtoDto.getData().getErrorCode();
                    if(!"0000".equals(code)){
                        log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                } else {
                    //调额度接口占额
                    CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                    cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                    cmisLmt0011ReqDto.setDealBizNo(iqpDiscApp.getContNo());//合同编号
                    cmisLmt0011ReqDto.setCusId(iqpDiscApp.getCusId());//客户编号
                    cmisLmt0011ReqDto.setCusName(iqpDiscApp.getCusName());//客户名称
                    cmisLmt0011ReqDto.setDealBizType(dealBizType);//交易业务类型
                    cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                    cmisLmt0011ReqDto.setPrdId(iqpDiscApp.getPrdId());//产品编号
                    cmisLmt0011ReqDto.setPrdName(iqpDiscApp.getPrdName());//产品名称
                    cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
                    cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
                    cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                    cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                    cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                    cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                    cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                    cmisLmt0011ReqDto.setDealBizAmt(actualAmt);//交易业务金额
                    cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                    cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                    cmisLmt0011ReqDto.setStartDate(iqpDiscApp.getStartDate());//合同起始日
                    cmisLmt0011ReqDto.setEndDate(iqpDiscApp.getEndDate());//合同到期日
                    cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                    cmisLmt0011ReqDto.setInputId(iqpDiscApp.getInputId());//登记人
                    cmisLmt0011ReqDto.setInputBrId(iqpDiscApp.getInputBrId());//登记机构
                    cmisLmt0011ReqDto.setInputDate(iqpDiscApp.getInputDate());//登记日期

                    List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<>();

                    //申请人客户信息
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                    cmisLmt0011OccRelListDto.setLmtSubNo(iqpDiscApp.getLmtAccNo());//额度分项编号
                    cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
                    cmisLmt0011OccRelListDto.setBizTotalAmt(actualAmt);//占用总额(原币种)
                    cmisLmt0011OccRelListDto.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                    cmisLmt0011OccRelListDto.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                    cmisLmt0011OccRelListDto.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);

                    //承兑企业客户信息
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo(iqpDiscApp.getAcptCrpLmtNo());//额度分项编号
                    cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp("");//授信品种类型
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(actualAmt);//占用总额(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(actualAmt);//占用敞口(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(actualAmt);//占用总额(折人民币)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(actualAmt);//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);

                    cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                    log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
                    ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                    log.info("贴现协议业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
                    if(!"0".equals(resultDtoDto.getCode())){
                        log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    String code = resultDtoDto.getData().getErrorCode();
                    if(!"0000".equals(code)){
                        log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        }



        log.info("审批通过生成获取贴现合同信息-合同填充业务申请" + iqpSerno + "信息");
        //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
        //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
        CtrDiscCont ctrDiscCont = new CtrDiscCont();
        IqpDiscApp iqpDiscAppData = iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);
        BeanUtils.copyProperties(iqpDiscAppData, ctrDiscCont);
        ctrDiscCont.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_03);
        ctrDiscCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);
        ctrDiscCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);//合同状态默认【未生效】
        int insertCount = ctrDiscContService.insertSelective(ctrDiscCont);
        if (insertCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }

        // 生成贴现协议后，把贴现申请汇票明细copy到贴现协议汇票明细表
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",iqpDiscApp.getSerno());
        queryModel.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<IqpDiscAppPorderSub> iqpDiscAppPorderSubList = iqpDiscAppPorderSubService.selectByModel(queryModel);
        if(CollectionUtils.nonEmpty(iqpDiscAppPorderSubList)){
            for (IqpDiscAppPorderSub iqpDiscAppPorderSub : iqpDiscAppPorderSubList) {
                CtrDiscPorderSub ctrDiscPorderSub = new CtrDiscPorderSub();
                BeanUtils.copyProperties(iqpDiscAppPorderSub, ctrDiscPorderSub);
                ctrDiscPorderSub.setContNo(iqpDiscApp.getContNo());
                int insertCountData = ctrDiscPorderSubService.insert(ctrDiscPorderSub);
                if(insertCountData<=0){
                    throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                }
            }
        }

        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpDiscApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpDiscApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrDiscCont origCtrDiscCont = ctrDiscContService.selectByContNo(origiContNo);
                if(Objects.nonNull(origCtrDiscCont)) {
                    origCtrDiscCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    int updateCount = ctrDiscContService.updateSelective(origCtrDiscCont);
                    if (updateCount < 1) {
                        throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                    }
                }
            }
        }
    }

    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpDiscApp selectByDiscSernoKey(String iqpSerno) {
        return iqpDiscAppMapper.selectByDiscSernoKey(iqpSerno);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpDiscApp> toSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return iqpDiscAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpDiscApp> doneSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return iqpDiscAppMapper.selectByModel(model);
    }

    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpDiscApp> selectForLmtAccNo(String lmtAccNo) {
        return iqpDiscAppMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(IqpDiscApp iqpDiscApp) {
        int updateCount = 0;
        if (CmisCommonConstants.WF_STATUS_000.equals(iqpDiscApp.getApproveStatus())) {
            iqpDiscApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            updateCount = this.updateSelective(iqpDiscApp);

        } else if (CmisCommonConstants.WF_STATUS_992.equals(iqpDiscApp.getApproveStatus())) {
            iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            updateCount = this.updateSelective(iqpDiscApp);
            //删除流程实例
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpDiscApp.getSerno());
            if (iqpDiscApp.getLmtAccNo() != null && !"".equals(iqpDiscApp.getLmtAccNo())) {
                String drftType = iqpDiscApp.getDrftType();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.STD_DRFT_TYPE_1.equals(drftType)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpDiscApp.getDrftTotalAmt();
                } else {
                    recoverSpacAmtCny = iqpDiscApp.getDrftTotalAmt();
                    recoverAmtCny = iqpDiscApp.getDrftTotalAmt();
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpDiscApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpDiscApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpDiscApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("贴现协议申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("贴现协议申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpDiscApp.getSerno(), JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请【{}】恢复额度异常！",iqpDiscApp.getSerno());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请【{}】恢复额度异常！",iqpDiscApp.getSerno());
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        }
        return updateCount;
    }

    /**
     * @方法名称: getAccNoInfo
     * @方法描述: 发送核心查询客户账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto<Ib1253RespDto> getAccNoInfo(Map map) {
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        ib1253ReqDto.setKehuzhao((String) map.get("rqstrAccNo"));//客户账号
        ib1253ReqDto.setZhhaoxuh("");//子账户序号
        ib1253ReqDto.setYanmbzhi("0");//密码校验方式 0--不校验1--校验查询密码 2--校验交易密码

        ib1253ReqDto.setMimammmm("");//密码
        ib1253ReqDto.setKehzhao2("");//客户账号2
        ib1253ReqDto.setShifoubz("0");//是否标志 1--是 0--否

        ib1253ReqDto.setZhufldm1("");//账户分类代码1
        ib1253ReqDto.setZhufldm2("");//账户分类代码2

        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        return resultDto;
    }

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpDiscApp selectBySerno(String serno) {
        return iqpDiscAppMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 贴现贷款申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuzz
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("贴现贷款申请流程参数更新,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpDiscApp iqpDiscApp = this.selectBySerno(serno);
        Map<String, Object> params = new HashMap<>();


        if(CmisCommonConstants.STD_DRFT_TYPE_1.equals(iqpDiscApp.getDrftType())){
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
                params.put("lowRiskAmtTotal", iqpDiscApp.getDrftTotalAmt().add(iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpDiscApp.getCusId(), iqpDiscApp.getManagerBrId())));
            }else {
                params.put("lowRiskAmtTotal", iqpDiscApp.getContAmt().add(iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpDiscApp.getCusId(), iqpDiscApp.getManagerBrId())));
            }
        }else{
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", new BigDecimal("0"));
        }
        // 是否足额
        if(iqpDiscApp.getLmtAccNo()==null || "".equals(iqpDiscApp.getLmtAccNo())){
            params.put("isAmtEnough", CmisCommonConstants.STD_ZB_YES_NO_0);
        }else {
            params.put("isAmtEnough", checkLmtAmtIsEnough(iqpDiscApp));
        }
        // 申请金额
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            params.put("amt", iqpDiscApp.getDrftTotalAmt());
        }else {
            params.put("amt", iqpDiscApp.getContAmt());
        }
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }


    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 贴现协议申请判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpDiscApp iqpDiscApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("贴现协议申请判断是否足额,流水号【{}】", iqpDiscApp.getSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpDiscApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpDiscApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpDiscApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" + iqpDiscApp.getLmtAccNo());
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        BigDecimal amt = BigDecimal.ZERO;
        if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
            amt = iqpDiscApp.getDrftTotalAmt();
        }else{
            amt = iqpDiscApp.getContAmt();
        }
        if(cmisLmt0026RespDto != null && amt.compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: iqpDiscAppSubmitNoFlow
     * @方法描述: 村镇银行无流程提交后续处理
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public Map iqpDiscAppSubmitNoFlow(String iqpSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            log.info("村镇银行贴现贷款申请提交审批无流程处理开始：" + iqpSerno);
            //发起节点执行下面的逻辑
            this.handleBusinessDataAfterStart(iqpSerno);  //发起节点执行下面的逻辑
            //针对流程到办结节点，进行以下处理
            //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
            this.handleBusinessDataAfterEnd(iqpSerno);
            //微信通知
            IqpDiscApp iqpDiscApp = this.selectBySerno(iqpSerno);
            String managerId = iqpDiscApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", iqpDiscApp.getCusName());
                paramMap.put("prdName", "贴现协议申请");
                paramMap.put("result", "通过");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            }
            log.info("村镇银行贴现贷款申请提交审批无流程处理结束：" + iqpSerno);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("村镇银行贴现贷款申请提交审批无流程处理提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

}
