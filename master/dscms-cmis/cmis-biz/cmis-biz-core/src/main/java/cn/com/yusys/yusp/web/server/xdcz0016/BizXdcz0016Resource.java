package cn.com.yusys.yusp.web.server.xdcz0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0016.req.Xdcz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.resp.Xdcz0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0016.Xdcz0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:企业网银查询影像补录批次
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0016:企业网银查询影像补录批次")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0016Resource.class);

    @Autowired
    private Xdcz0016Service xdcz0016Service;

    /**
     * 交易码：xdcz0016
     * 交易描述：企业网银银票查询影像补录批次
     *
     * @param xdcz0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银查询影像补录批次")
    @PostMapping("/xdcz0016")
    protected @ResponseBody
    ResultDto<Xdcz0016DataRespDto> xdcz0016(@Validated @RequestBody Xdcz0016DataReqDto xdcz0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataReqDto));
        Xdcz0016DataRespDto xdcz0016DataRespDto = new Xdcz0016DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0016DataRespDto> xdcz0016DataResultDto = new ResultDto<>();

        String cusId = xdcz0016DataReqDto.getCusId();//客户号
        String qryType = xdcz0016DataReqDto.getQryType();//查询类型

        try {
            // 从xdcz0016DataReqDto获取业务值进行业务逻辑处理
            xdcz0016DataRespDto = xdcz0016Service.xdcz0016(xdcz0016DataReqDto);
            // 封装xdcz0016DataResultDto中正确的返回码和返回信息
            xdcz0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, e.getMessage());
            // 封装xdcz0016DataResultDto中异常返回码和返回信息
            xdcz0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0016DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, e.getMessage());
            // 封装xdcz0016DataResultDto中异常返回码和返回信息
            xdcz0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0016DataRespDto到xdcz0016DataResultDto中
        xdcz0016DataResultDto.setData(xdcz0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataRespDto));
        return xdcz0016DataResultDto;
    }
}
