/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardTelvPerDetail;
import cn.com.yusys.yusp.service.CreditCardTelvPerDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvPerDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "电话调查")
@RestController
@RequestMapping("/api/creditcardtelvperdetail")
public class CreditCardTelvPerDetailResource {
    @Autowired
    private CreditCardTelvPerDetailService creditCardTelvPerDetailService;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardTelvPerDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardTelvPerDetail> list = creditCardTelvPerDetailService.selectAll(queryModel);
        return new ResultDto<List<CreditCardTelvPerDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardTelvPerDetail>> index(QueryModel queryModel) {
        List<CreditCardTelvPerDetail> list = creditCardTelvPerDetailService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardTelvPerDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardTelvPerDetail> show(@PathVariable("pkId") String pkId) {
        CreditCardTelvPerDetail creditCardTelvPerDetail = creditCardTelvPerDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardTelvPerDetail>(creditCardTelvPerDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("保存")
    @PostMapping("/")
    protected ResultDto<CreditCardTelvPerDetail> create(@RequestBody CreditCardTelvPerDetail creditCardTelvPerDetail) throws URISyntaxException {
        creditCardTelvPerDetailService.insert(creditCardTelvPerDetail);
        return new ResultDto<CreditCardTelvPerDetail>(creditCardTelvPerDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardTelvPerDetail creditCardTelvPerDetail) throws URISyntaxException {
        int result = creditCardTelvPerDetailService.update(creditCardTelvPerDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardTelvPerDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardTelvPerDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return ResultDto
     * @author wzy
     * @date 2021/5/25 21:04
     * @version 1.0.0
     * @desc  分页查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据条件进行分页查询")
    @PostMapping("/querybyconditon")
    protected ResultDto<List<CreditCardTelvPerDetail>> queryByConditon(@RequestBody QueryModel queryModel) {
        List<CreditCardTelvPerDetail> list = creditCardTelvPerDetailService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardTelvPerDetail>>(list);
    }

    /**
     * @param list
     * @return ResultDto
     * @author wzy
     * @version 1.0.0
     * @date 2021/5/25 21:04
     * @desc  分页查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("电话调查可编辑列表保存")
    @PostMapping("/savetellist")
    protected ResultDto<Integer> saveTelList(@RequestBody List<CreditCardTelvPerDetail> list) {
        Integer result = creditCardTelvPerDetailService.saveTelList(list);
        return new ResultDto<Integer>(result);
    }




}
