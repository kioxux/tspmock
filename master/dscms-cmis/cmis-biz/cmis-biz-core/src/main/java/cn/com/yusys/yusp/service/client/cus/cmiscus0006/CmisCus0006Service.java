package cn.com.yusys.yusp.service.client.cus.cmiscus0006;


import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询客户基本信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisCus0006Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0006Service.class);

    // 1）注入：封装的接口类:客户管理模块
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 业务逻辑处理方法：查询客户基本信息
     *
     * @param cmisCus0006ReqDto
     * @return
     */
    @Transactional
    public CmisCus0006RespDto cmisCus0006(CmisCus0006ReqDto cmisCus0006ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
        ResultDto<CmisCus0006RespDto> cmisCus0006ResultDto = cmisCusClientService.cmiscus0006(cmisCus0006ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ResultDto));

        String cmisCus0006Code = Optional.ofNullable(cmisCus0006ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisCus0006Meesage = Optional.ofNullable(cmisCus0006ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisCus0006RespDto cmisCus0006RespDto = null;
        List<CusBaseDto> cusBaseList = new ArrayList<>();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCus0006ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisCus0006RespDto = cmisCus0006ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(cmisCus0006Code, cmisCus0006Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value);
        return cmisCus0006RespDto;
    }
}
