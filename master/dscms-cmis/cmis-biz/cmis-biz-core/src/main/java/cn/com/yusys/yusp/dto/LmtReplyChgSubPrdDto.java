package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class LmtReplyChgSubPrdDto {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 批复变更分项适用产品流水号 **/
    private String subPrdSerno;

    /** 批复变更授信分项流水号 **/
    private String subSerno;

    /** 批复分项流水号 **/
    private String replySubSerno;

    /** 批复分项适用产品流水号 **/
    private String replySubPrdSerno;

    /** 客户名称 **/
    private String cusName;

    /** 客户编号 **/
    private String cusId;

    /** 授信品种编号 **/
    private String lmtBizType;

    /** 授信品种名称 **/
    private String lmtBizTypeName;

    /** 是否循环额度 **/
    private String isRevolvLimit;

    /** 担保方式 **/
    private String guarMode;

    /** 币种 **/
    private String curType;

    /** 授信额度 **/
    private java.math.BigDecimal lmtAmt;

    /** 授信起始日 **/
    private String startDate;

    /** 授信到期日 **/
    private String endDate;

    /** 保证金预留比例 **/
    private java.math.BigDecimal bailPreRate;

    /** 授信期限 **/
    private Integer lmtTerm;

    /** 授信宽限期 **/
    private Integer lmtGraperTerm;

    /** 年利率 **/
    private java.math.BigDecimal rateYear;

    /** 还款方式 **/
    private String repayMode;

    /** 结息方式 **/
    private String eiMode;

    /** 是否借新还旧 **/
    private String isRefinance;

    /** 是否无还本续贷 **/
    private String isRwrop;

    /** 手续费率 **/
    private java.math.BigDecimal chrgRate;

    /** 手续费收取方式 **/
    private String chrgCollectMode;

    /** 委托人类型 **/
    private String consignorType;

    /** 委托人客户号 **/
    private String consignorCusId;

    /** 委托人客户名称 **/
    private String consignorCusName;

    /** 委托人证件号码 **/
    private String consignorCertCode;

    /** 委托人证件类型 **/
    private String consignorCertType;

    /** 是否预授信额度 **/
    private String isPreLmt;

    /** 还款计划描述 **/
    private String repayPlanDesc;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    /**
     * 页面渲染用的授信分项额度编号
     **/
    private String lmtChgDrawNo;

    /**
     * 页面渲染用的授信品种
     **/
    private String lmtChgDrawType;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSubPrdSerno() {
        return subPrdSerno;
    }

    public void setSubPrdSerno(String subPrdSerno) {
        this.subPrdSerno = subPrdSerno;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getReplySubPrdSerno() {
        return replySubPrdSerno;
    }

    public void setReplySubPrdSerno(String replySubPrdSerno) {
        this.replySubPrdSerno = replySubPrdSerno;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public Integer getLmtGraperTerm() {
        return lmtGraperTerm;
    }

    public void setLmtGraperTerm(Integer lmtGraperTerm) {
        this.lmtGraperTerm = lmtGraperTerm;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getIsRefinance() {
        return isRefinance;
    }

    public void setIsRefinance(String isRefinance) {
        this.isRefinance = isRefinance;
    }

    public String getIsRwrop() {
        return isRwrop;
    }

    public void setIsRwrop(String isRwrop) {
        this.isRwrop = isRwrop;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public String getChrgCollectMode() {
        return chrgCollectMode;
    }

    public void setChrgCollectMode(String chrgCollectMode) {
        this.chrgCollectMode = chrgCollectMode;
    }

    public String getConsignorType() {
        return consignorType;
    }

    public void setConsignorType(String consignorType) {
        this.consignorType = consignorType;
    }

    public String getConsignorCusId() {
        return consignorCusId;
    }

    public void setConsignorCusId(String consignorCusId) {
        this.consignorCusId = consignorCusId;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getRepayPlanDesc() {
        return repayPlanDesc;
    }

    public void setRepayPlanDesc(String repayPlanDesc) {
        this.repayPlanDesc = repayPlanDesc;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLmtChgDrawNo() {
        return lmtChgDrawNo;
    }

    public void setLmtChgDrawNo(String lmtChgDrawNo) {
        this.lmtChgDrawNo = lmtChgDrawNo;
    }

    public String getLmtChgDrawType() {
        return lmtChgDrawType;
    }

    public void setLmtChgDrawType(String lmtChgDrawType) {
        this.lmtChgDrawType = lmtChgDrawType;
    }

    public String getReplySubSerno() {
        return replySubSerno;
    }

    public void setReplySubSerno(String replySubSerno) {
        this.replySubSerno = replySubSerno;
    }

    public String getLmtBizType() {
        return lmtBizType;
    }

    public void setLmtBizType(String lmtBizType) {
        this.lmtBizType = lmtBizType;
    }

    public String getLmtBizTypeName() {
        return lmtBizTypeName;
    }

    public void setLmtBizTypeName(String lmtBizTypeName) {
        this.lmtBizTypeName = lmtBizTypeName;
    }

    public String getIsRevolvLimit() {
        return isRevolvLimit;
    }

    public void setIsRevolvLimit(String isRevolvLimit) {
        this.isRevolvLimit = isRevolvLimit;
    }

    public String getConsignorCusName() {
        return consignorCusName;
    }

    public void setConsignorCusName(String consignorCusName) {
        this.consignorCusName = consignorCusName;
    }

    public String getConsignorCertCode() {
        return consignorCertCode;
    }

    public void setConsignorCertCode(String consignorCertCode) {
        this.consignorCertCode = consignorCertCode;
    }

    public String getConsignorCertType() {
        return consignorCertType;
    }

    public void setConsignorCertType(String consignorCertType) {
        this.consignorCertType = consignorCertType;
    }
}
