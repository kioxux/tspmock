/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateLoanRelOldRate;
import cn.com.yusys.yusp.service.OtherCnyRateLoanRelOldRateService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateLoanRelOldRateResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-05 14:43:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othercnyrateloanreloldrate")
public class OtherCnyRateLoanRelOldRateResource {
    @Autowired
    private OtherCnyRateLoanRelOldRateService otherCnyRateLoanRelOldRateService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherCnyRateLoanRelOldRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherCnyRateLoanRelOldRate> list = otherCnyRateLoanRelOldRateService.selectAll(queryModel);
        return new ResultDto<List<OtherCnyRateLoanRelOldRate>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherCnyRateLoanRelOldRate>> index(QueryModel queryModel) {
        List<OtherCnyRateLoanRelOldRate> list = otherCnyRateLoanRelOldRateService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateLoanRelOldRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<OtherCnyRateLoanRelOldRate> show(@PathVariable("pkId") String pkId) {
        OtherCnyRateLoanRelOldRate otherCnyRateLoanRelOldRate = otherCnyRateLoanRelOldRateService.selectByPrimaryKey(pkId);
        return new ResultDto<OtherCnyRateLoanRelOldRate>(otherCnyRateLoanRelOldRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherCnyRateLoanRelOldRate> create(@RequestBody OtherCnyRateLoanRelOldRate otherCnyRateLoanRelOldRate) throws URISyntaxException {
        otherCnyRateLoanRelOldRateService.insert(otherCnyRateLoanRelOldRate);
        return new ResultDto<OtherCnyRateLoanRelOldRate>(otherCnyRateLoanRelOldRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherCnyRateLoanRelOldRate otherCnyRateLoanRelOldRate) throws URISyntaxException {
        int result = otherCnyRateLoanRelOldRateService.update(otherCnyRateLoanRelOldRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = otherCnyRateLoanRelOldRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherCnyRateLoanRelOldRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<OtherCnyRateLoanRelOldRate>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherCnyRateLoanRelOldRate> list = otherCnyRateLoanRelOldRateService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateLoanRelOldRate>>(list);
    }

    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody OtherCnyRateLoanRelOldRate otherCnyRateLoanRelOldRate) throws URISyntaxException {
        int result = otherCnyRateLoanRelOldRateService.updateSelective(otherCnyRateLoanRelOldRate);
        return new ResultDto<Integer>(result);
    }
}
