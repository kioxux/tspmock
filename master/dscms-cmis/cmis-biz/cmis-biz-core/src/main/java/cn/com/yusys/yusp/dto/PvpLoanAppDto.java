package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanApp
 * @类描述: pvp_loan_app数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-05 10:57:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpLoanAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 放款流水号 **/
	private String pvpSerno;
	
	/** 业务流水号 **/
	private String iqpSerno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 中文合同编号 **/
	private String cnContNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 本金宽限方式 **/
	private String capGraperType;
	
	/** 本金宽限天数 **/
	private java.math.BigDecimal capGraperDay;
	
	/** 利息宽限方式 **/
	private String intGraperType;
	
	/** 本金宽限天数 **/
	private java.math.BigDecimal intGraperDay;
	
	/** 扣款扣息方式 **/
	private String deductDeduType;
	
	/** 特殊业务类型 STD_ZB_SPEBS_TYP **/
	private String especBizType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 合同金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 放款金额 **/
	private java.math.BigDecimal pvpAmt;
	
	/** 支付方式 STD_ZB_RAY_MODE **/
	private String payWay;
	
	/** 是否立即发起受托支付 STD_ZB_YES_NO **/
	private String isCfirmPay;
	
	/** 期限类型 STD_ZB_TERM_TYP **/
	private String termType;
	
	/** 申请期限 **/
	private String approveTerm;
	
	/** 授权状态 STD_ZB_AUTH_ST **/
	private String authStatus;
	
	/** 渠道来源 STD_ZB_CHNL_SOUR **/
	private String chnlSour;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 放款机构 **/
	private String acctBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/**业务申请类型**/
	private String bizType;

	/** 客户评级系数 **/
	private java.math.BigDecimal customerRatingFactor;

	/** 利率 **/
	private java.math.BigDecimal realityIrY;

	public BigDecimal getCustomerRatingFactor() {
		return customerRatingFactor;
	}

	public void setCustomerRatingFactor(BigDecimal customerRatingFactor) {
		this.customerRatingFactor = customerRatingFactor;
	}


	public BigDecimal getRealityIrY() {
		return realityIrY;
	}

	public void setRealityIrY(BigDecimal realityIrY) {
		this.realityIrY = realityIrY;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cnContNo
	 */
	public void setCnContNo(String cnContNo) {
		this.cnContNo = cnContNo == null ? null : cnContNo.trim();
	}
	
    /**
     * @return CnContNo
     */	
	public String getCnContNo() {
		return this.cnContNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param capGraperType
	 */
	public void setCapGraperType(String capGraperType) {
		this.capGraperType = capGraperType == null ? null : capGraperType.trim();
	}
	
    /**
     * @return CapGraperType
     */	
	public String getCapGraperType() {
		return this.capGraperType;
	}
	
	/**
	 * @param capGraperDay
	 */
	public void setCapGraperDay(java.math.BigDecimal capGraperDay) {
		this.capGraperDay = capGraperDay;
	}
	
    /**
     * @return CapGraperDay
     */	
	public java.math.BigDecimal getCapGraperDay() {
		return this.capGraperDay;
	}
	
	/**
	 * @param intGraperType
	 */
	public void setIntGraperType(String intGraperType) {
		this.intGraperType = intGraperType == null ? null : intGraperType.trim();
	}
	
    /**
     * @return IntGraperType
     */	
	public String getIntGraperType() {
		return this.intGraperType;
	}
	
	/**
	 * @param intGraperDay
	 */
	public void setIntGraperDay(java.math.BigDecimal intGraperDay) {
		this.intGraperDay = intGraperDay;
	}
	
    /**
     * @return IntGraperDay
     */	
	public java.math.BigDecimal getIntGraperDay() {
		return this.intGraperDay;
	}
	
	/**
	 * @param deductDeduType
	 */
	public void setDeductDeduType(String deductDeduType) {
		this.deductDeduType = deductDeduType == null ? null : deductDeduType.trim();
	}
	
    /**
     * @return DeductDeduType
     */	
	public String getDeductDeduType() {
		return this.deductDeduType;
	}
	
	/**
	 * @param especBizType
	 */
	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType == null ? null : especBizType.trim();
	}
	
    /**
     * @return EspecBizType
     */	
	public String getEspecBizType() {
		return this.especBizType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param pvpAmt
	 */
	public void setPvpAmt(java.math.BigDecimal pvpAmt) {
		this.pvpAmt = pvpAmt;
	}
	
    /**
     * @return PvpAmt
     */	
	public java.math.BigDecimal getPvpAmt() {
		return this.pvpAmt;
	}
	
	/**
	 * @param payWay
	 */
	public void setPayWay(String payWay) {
		this.payWay = payWay == null ? null : payWay.trim();
	}
	
    /**
     * @return PayWay
     */	
	public String getPayWay() {
		return this.payWay;
	}
	
	/**
	 * @param isCfirmPay
	 */
	public void setIsCfirmPay(String isCfirmPay) {
		this.isCfirmPay = isCfirmPay == null ? null : isCfirmPay.trim();
	}
	
    /**
     * @return IsCfirmPay
     */	
	public String getIsCfirmPay() {
		return this.isCfirmPay;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}
	
    /**
     * @return TermType
     */	
	public String getTermType() {
		return this.termType;
	}
	
	/**
	 * @param approveTerm
	 */
	public void setApproveTerm(String approveTerm) {
		this.approveTerm = approveTerm == null ? null : approveTerm.trim();
	}
	
    /**
     * @return ApproveTerm
     */	
	public String getApproveTerm() {
		return this.approveTerm;
	}
	
	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus == null ? null : authStatus.trim();
	}
	
    /**
     * @return AuthStatus
     */	
	public String getAuthStatus() {
		return this.authStatus;
	}
	
	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour == null ? null : chnlSour.trim();
	}
	
    /**
     * @return ChnlSour
     */	
	public String getChnlSour() {
		return this.chnlSour;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param acctBrId
	 */
	public void setAcctBrId(String acctBrId) {
		this.acctBrId = acctBrId == null ? null : acctBrId.trim();
	}
	
    /**
     * @return AcctBrId
     */	
	public String getAcctBrId() {
		return this.acctBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}