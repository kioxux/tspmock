/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRenewLoanInfo
 * @类描述: pvp_renew_loan_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-05 17:13:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_renew_loan_info")
public class PvpRenewLoanInfo extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;

	/** 原借据号 **/
	@Id
	@Column(name = "OLD_BILL_NO")
	private String oldBillNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 5)
	private String belgLine;

	/** 第一次续贷借据号 **/
	@Column(name = "FIRST_BILL_NO", unique = false, nullable = true, length = 100)
	private String firstBillNo;

	/** 第二次续贷借据号 **/
	@Column(name = "SECOND_BILL_NO", unique = false, nullable = true, length = 100)
	private String secondBillNo;

	/** 第一次续贷时间 **/
	@Column(name = "FIRST_DATE", unique = false, nullable = true, length = 20)
	private String firstDate;

	/** 第二次续贷时间 **/
	@Column(name = "SECOND_DATE", unique = false, nullable = true, length = 20)
	private String secondDate;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo;
	}

	/**
	 * @return oldBillNo
	 */
	public String getOldBillNo() {
		return this.oldBillNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param firstBillNo
	 */
	public void setFirstBillNo(String firstBillNo) {
		this.firstBillNo = firstBillNo;
	}

	/**
	 * @return firstBillNo
	 */
	public String getFirstBillNo() {
		return this.firstBillNo;
	}

	/**
	 * @param secondBillNo
	 */
	public void setSecondBillNo(String secondBillNo) {
		this.secondBillNo = secondBillNo;
	}

	/**
	 * @return secondBillNo
	 */
	public String getSecondBillNo() {
		return this.secondBillNo;
	}

	/**
	 * @param firstDate
	 */
	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}

	/**
	 * @return firstDate
	 */
	public String getFirstDate() {
		return this.firstDate;
	}

	/**
	 * @param secondDate
	 */
	public void setSecondDate(String secondDate) {
		this.secondDate = secondDate;
	}

	/**
	 * @return secondDate
	 */
	public String getSecondDate() {
		return this.secondDate;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}