package cn.com.yusys.yusp.service.server.xdxw0032;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0032.req.Xdxw0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0032.resp.ApprHisList;
import cn.com.yusys.yusp.dto.server.xdxw0032.resp.Xdxw0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0032Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021-06-04 14:12:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0032Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0032Service.class);

    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Resource
    private AdminSmUserService adminSmUserService; // 用户信息
//    @Resource
////    private AdminSmDutyService  adminSmDutyService; // 岗位信息
    @Resource
    private AdminSmOrgService adminSmOrgService; // 机构信息

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 根据证件号查询信贷系统的申请信息
     * 智能审贷系统贷款时,查询关系人（借款人、配偶、保证人）的信贷申请历史信息列表
     * @param xdxw0032DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0032DataRespDto xdxw0032(Xdxw0032DataReqDto xdxw0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataReqDto));
        Xdxw0032DataRespDto xdxw0032DataRespDto = new Xdxw0032DataRespDto();
        try {

            if (StringUtils.isEmpty(xdxw0032DataReqDto.getPkValue())) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            } else {
                // 业务编号
                String pkValue = xdxw0032DataReqDto.getPkValue();
                String url = "http://yusp-app-oca/api/core/getAllCommentsByBizId?bizId={1}";
                ResponseEntity<ResultDto> responseEntity = restTemplate.getForEntity(url, ResultDto.class, pkValue);
                ResultDto<List<Map<String, String>>> resultDto = responseEntity.getBody();
                java.util.List<ApprHisList> apprHisLists = new ArrayList<>();
                if (null != resultDto && CollectionUtils.nonEmpty(resultDto.getData())) {
                    List<Map<String, String>> list = resultDto.getData();
                    apprHisLists = Optional.ofNullable(list.parallelStream().map(resultCommentDto -> {
                        ApprHisList apprHisList = new ApprHisList();
                        apprHisList.setApprNode(resultCommentDto.get("nodeId"));
                        apprHisList.setApprRes(resultCommentDto.get("commentSign"));
                        apprHisList.setApprAdvice(resultCommentDto.get("userComment"));
                        apprHisList.setApprId(resultCommentDto.get("userId"));
                        apprHisList.setApprName(resultCommentDto.get("userName"));
                        apprHisList.setApprDuty(resultCommentDto.get("nodeName"));
                        String orgId = "";
                        ResultDto<AdminSmUserDto> resultUserDto = adminSmUserService.getByLoginCode(resultCommentDto.get("userId"));
                        if (ResultDto.success().getCode().equals(resultUserDto.getCode())) {
                            AdminSmUserDto adminSmUserDto = resultUserDto.getData();
                            if (!Objects.isNull(adminSmUserDto)) {
                                orgId = adminSmUserDto.getOrgId();
                            }
                        }
                        apprHisList.setApprOrg(orgId);
                        apprHisList.setApprTime(resultCommentDto.get("startTime"));
                        return apprHisList;
                    }).collect(Collectors.toList())).orElse(new ArrayList<>());
                }
//                java.util.List<ApprHisList> apprHisLists = lmtSurveyReportMainInfoMapper.selectWfiAppAdviceByPkValue(pkValue);
//                // 开始新增
//                for (int i = 0; i < apprHisLists.size(); i++) {
//                    ApprHisList appHisInfo = apprHisLists.get(i);
//                    // 客户名称翻译
//                    String userName = "";
//                    ResultDto<AdminSmUserDto> resultUserDto = adminSmUserService.getByLoginCode(appHisInfo.getApprId());
//                    if (ResultDto.success().getCode().equals(resultUserDto.getCode())) {
//                        AdminSmUserDto adminSmUserDto = resultUserDto.getData();
//                        if (!Objects.isNull(adminSmUserDto)) {
//                            userName = adminSmUserDto.getUserName();
//                        }
//                    }
//                    appHisInfo.setApprName(userName);
//
//                    // TODO 岗位表
//                    // 岗位名称翻译
//                    String dutyName = "";
////                    ResultDto<AdminSmDutyDto> resultDutyDto = adminSmDutyService.xx(appHisInfo.getApprDuty());
////                    if (ResultDto.success().getCode().equals(resultDutyDto.getCode())) {
////                        AdminSmDutyDto adminSmDutyDto = resultDutyDto.getData();
////                        if (!Objects.isNull(adminSmDutyDto)) {
////                            dutyName = adminSmDutyDto.getDutyName();
////                        }
////                    }
////                    appHisInfo.setApprDuty(dutyName);
//                    // 机构名称翻译
//                    String orgName = "";
//                    ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(appHisInfo.getApprOrg());
//                    if (ResultDto.success().getCode().equals(resultOrgDto.getCode())) {
//                        AdminSmOrgDto adminSmOrgrDto = resultOrgDto.getData();
//                        if (!Objects.isNull(adminSmOrgrDto)) {
//                            orgName = adminSmOrgrDto.getOrgName();
//                        }
//                    }
//                    appHisInfo.setApprOrg(orgName);
//                    // 查询对应的字典
//                    logger.info("从Redis中获取字典项缓存开始");
//                    // 码值转换
//                    String apprRes = DictTranslatorUtils.findValueByDictKey("SCSPJL", appHisInfo.getApprRes());
//                    appHisInfo.setApprRes(apprRes);
//                }
                xdxw0032DataRespDto.setApprHisLists(apprHisLists);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataRespDto));
        return xdxw0032DataRespDto;
    }
}
