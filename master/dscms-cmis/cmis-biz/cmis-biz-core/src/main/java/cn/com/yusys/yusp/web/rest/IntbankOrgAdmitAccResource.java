/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAcc;
import cn.com.yusys.yusp.service.IntbankOrgAdmitAccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:26:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/intbankorgadmitacc")
public class IntbankOrgAdmitAccResource {
    @Autowired
    private IntbankOrgAdmitAccService intbankOrgAdmitAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IntbankOrgAdmitAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<IntbankOrgAdmitAcc> list = intbankOrgAdmitAccService.selectAll(queryModel);
        return new ResultDto<List<IntbankOrgAdmitAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<IntbankOrgAdmitAcc>> index(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" createTime desc ");
        }
        List<IntbankOrgAdmitAcc> list = intbankOrgAdmitAccService.selectByModel(queryModel);
        return new ResultDto<List<IntbankOrgAdmitAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{pkId}")
    protected ResultDto<IntbankOrgAdmitAcc> show(@PathVariable("pkId") String pkId) {
        IntbankOrgAdmitAcc intbankOrgAdmitAcc = intbankOrgAdmitAccService.selectByPrimaryKey(pkId);
        return new ResultDto<IntbankOrgAdmitAcc>(intbankOrgAdmitAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IntbankOrgAdmitAcc> create(@RequestBody IntbankOrgAdmitAcc intbankOrgAdmitAcc) throws URISyntaxException {
        intbankOrgAdmitAccService.insert(intbankOrgAdmitAcc);
        return new ResultDto<IntbankOrgAdmitAcc>(intbankOrgAdmitAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IntbankOrgAdmitAcc intbankOrgAdmitAcc) throws URISyntaxException {
        int result = intbankOrgAdmitAccService.update(intbankOrgAdmitAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = intbankOrgAdmitAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) { 
        int result = intbankOrgAdmitAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 获取当前机构最新批复台账信息
     * @param queryModel
     * @return
     */
    @PostMapping("/lastAccByApp")
    protected ResultDto<List<IntbankOrgAdmitAcc>> lastAccByApp(@RequestBody QueryModel queryModel){
        List<IntbankOrgAdmitAcc> list = intbankOrgAdmitAccService.lastAccByApp(queryModel);
        return new ResultDto<List<IntbankOrgAdmitAcc>>(list);
    }

    /**
     * 获取台账批复详情
     * @param condition
     * @return
     */
    @PostMapping("/intbankOrgAdmitAccWithReplayDetail")
    protected ResultDto<IntbankOrgAdmitAcc> intbankOrgAdmitAccWithReplayDetail(@RequestBody Map condition){
        String replySerno = (String) condition.get("replySerno");
        IntbankOrgAdmitAcc intbankOrgAdmitAcc = intbankOrgAdmitAccService.intbankOrgAdmitAccWithReplayDetail(replySerno);
        return new ResultDto<IntbankOrgAdmitAcc>(intbankOrgAdmitAcc);
    }

    /**
     * 异步导出金融机构准入名单
     */
    @PostMapping("/exportIntbankOrgAdmitAcc")
    public ResultDto<ProgressDto> exportIntbankOrgAdmitAcc(@RequestBody QueryModel model) {
        ProgressDto progressDto = intbankOrgAdmitAccService.exportIntbankOrgAdmitAcc(model);
        return ResultDto.success(progressDto);
    }

}
