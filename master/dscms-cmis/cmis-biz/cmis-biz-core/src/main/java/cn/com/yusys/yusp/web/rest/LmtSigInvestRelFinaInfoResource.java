/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.service.BizInvestCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaInfo;
import cn.com.yusys.yusp.service.LmtSigInvestRelFinaInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-19 10:26:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestrelfinainfo")
public class LmtSigInvestRelFinaInfoResource {
    @Autowired
    private LmtSigInvestRelFinaInfoService lmtSigInvestRelFinaInfoService;


    @Value("${yusp.file-server.home-path}")
    private String serverPath;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestRelFinaInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestRelFinaInfo> list = lmtSigInvestRelFinaInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestRelFinaInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestRelFinaInfo>> index(QueryModel queryModel) {
        List<LmtSigInvestRelFinaInfo> list = lmtSigInvestRelFinaInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestRelFinaInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestRelFinaInfo> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = lmtSigInvestRelFinaInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestRelFinaInfo>(lmtSigInvestRelFinaInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestRelFinaInfo> create(@RequestBody LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo) throws URISyntaxException {
        lmtSigInvestRelFinaInfoService.insert(lmtSigInvestRelFinaInfo);
        return new ResultDto<LmtSigInvestRelFinaInfo>(lmtSigInvestRelFinaInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo) throws URISyntaxException {
        int result = lmtSigInvestRelFinaInfoService.update(lmtSigInvestRelFinaInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestRelFinaInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestRelFinaInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 主题授信-主体分析详情获取
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtSigInvestRelFinaInfo> selectBySerno(@RequestBody Map condition){
        String serno = (String) condition.get("serno");
        String cusId = (String) condition.get("cusId");
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = lmtSigInvestRelFinaInfoService.selectBySerno(serno,cusId);
        return new ResultDto<>(lmtSigInvestRelFinaInfo);
    }


    /**
     * 根据cusid和serno更新字段
     * @param lmtSigInvestRelFinaInfo
     * @return
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo){
        int result = lmtSigInvestRelFinaInfoService.updateByKeyAndVal(lmtSigInvestRelFinaInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * 图片
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtSigInvestRelFinaInfoService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }

    /**
     * 获取图片信息
     * @param condition
     * @return
     */
    @PostMapping("/selectFileInfo")
    protected ResultDto<BizInvestCommonService.TempFileInfo> selectFileInfo(@RequestBody Map condition){
        BizInvestCommonService.TempFileInfo tempFileInfo = lmtSigInvestRelFinaInfoService.selectFileInfo(condition);
        return new ResultDto<>(tempFileInfo);
    }


    /**
     * 根据cusid和serno更新字段
     * @param lmtSigInvestRelFinaInfo
     * @return
     */
    @PostMapping("/updateZtfxOrCwzk")
    protected ResultDto<Integer> updateZtfxOrCwzk(@RequestBody LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo){
        int result = lmtSigInvestRelFinaInfoService.updateZtfxOrCwzk(lmtSigInvestRelFinaInfo);
        return new ResultDto<Integer>(result);
    }

}
