/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.GrtGuarBizRstRelDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarBizRstRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-18 19:41:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GrtGuarBizRstRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    GrtGuarBizRstRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarBizRstRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(GrtGuarBizRstRel record);
    /**
     * @方法名称: selectByGuarContNoKeys
     * @方法描述: 根据担保合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarBizRstRel> selectByGuarContNoKeys(Map grtGuarContDto);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(GrtGuarBizRstRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(GrtGuarBizRstRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(GrtGuarBizRstRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: queryContNoKey
     * @方法描述: 根据合同编号查主键
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarBizRstRel> queryContNoKey(@Param("contNo") String contNo,@Param("oprType") String oprType);

    /**
     * @方法名称: insertBizResRel
     * @方法描述: 根据入参插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertBizResRel(GrtGuarBizRstRel grtGuarBizRstRel);

    /**
     * 通过入参信息查询数据是否存在
     * @param params
     * @return
     */
    List<GrtGuarBizRstRel> selectByParams(Map params);

    /**
     * 根据入参查询
     * @param grtGuarBizRstRel
     * @return
     */
    GrtGuarBizRstRel selectBySernoKeys(GrtGuarBizRstRel grtGuarBizRstRel);

    /**
     * 批量添加数据
     * @param grtGuarMapList
     * @return
     */
    int insertForeach(List<Map> grtGuarMapList);

    /**
     * 根据担保编号查询
     * @param guarContNo
     * @return
     */
    GrtGuarBizRstRel selectByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * @方法名称：selectByLmtSerno
     * @方法描述：跟据授信业务流水号获取
     * @创建人：zhangming12
     * @创建时间：2021/5/18 10:29
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<GrtGuarBizRstRel> selectByLmtSerno(@Param("serno") String serno);

    /**
     * @方法名称：selectByContNo
     * @方法描述：根据合同查询
     * @创建人：zhangming12
     * @创建时间：2021/5/22 10:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<GrtGuarBizRstRel> selectByContNo(@Param("contNo") String contNo);


    /**
     * @方法名称：getGuarPkIdsByContNo
     * @方法描述：根据合同查询担保合同流水号
     * @创建人：fengjj
     * @创建时间：2021/10/19 10:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<String> getGuarPkIdsByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称：insertGrtGuarBizRstRelList
     * @方法描述：批量插入
     * @创建人：xs
     * @创建时间：2021/5/22 10:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    int insertGrtGuarBizRstRelList(@Param("list")List<GrtGuarBizRstRel> grtGuarBizRstRelList);

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/5/27 15:19
     * @version 1.0.0
     * @desc    删除合同关联信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int deleteBySerno(@Param("serno") String serno);

    /**
     * @param map
     * @return int
     * @author xs
     * @date 2021/06/01 15:19
     * @version 1.0.0
     * @desc   修改担保合同 根据资产台账的合同编号去 担保合同和业务的关系 修改关联关系 为生效
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int updateActiveRelByContNo(Map<String, String> map);

    /**
     * @方法名称：selectByContNo
     * @方法描述：根据借款合同号查询项下的押品编号
     * @创建人：zoubiao
     * @创建时间：2021/06/10 20:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    String selectGuarantyIdByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称：selectContManagerId
     * 获取合同管户客户经理及机构manager_id manager_br_id，根据请求的担保合同编号GUAR_CONT_NO
     * @param guarContCnNo
     * @return
     */
    GrtGuarBizRstRel selectContManagerId(@Param("guarContCnNo") String guarContCnNo);

    /**
     * 根据流水号查询物联产贷的已签订的合同
     * @param serno
     * @return
     */
    GrtGuarBizRstRel selectGrtGuarBizRstRelDataBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectDetailBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarBizRstRel> selectDetailBySerno(String serno);

    /**
     * 根据担保编号查询
     * @param guarContNo
     * @return
     */
    List<GrtGuarBizRstRel> selectListByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据资产编号删除担保合同和押品信息
     * @param assetNo
     * @return
     */
    int deleteByAssetNo(@Param("assetNo") String assetNo);

    /**
     * 通过担保合同查询关系数据
     * @param params
     * @return
     */
    List<GrtGuarBizRstRel> selectAmtDataByParams(Map params);

    /**
     * 根据担保合同编号获取完整合同信息
     * @param model
     * @return
     */
    List<GrtGuarBizRstRelDto> getGrtContAndBizRel(QueryModel model);

    /**
     * 根据担保合同编号获取押品信息
     * @param model
     * @return
     */
    List<GuarBaseInfo> getGrtBaseByGarContNo(QueryModel model);

    /**
     * 根据担保合同号编号获取最早的合同编号
     * @param model
     * @return
     */
    String selectContNoByGuarContNo(QueryModel model);

    /**
     * 根据担保合同号编号和合同编号查询最新的流水号
     * @param model
     * @return
     */
    String selectSernoByModel(QueryModel model);

    /**
     * 根据合同编号查询
     * @param contNo
     * @return
     */
    String selectGuarContNoBycontNo(@Param("contNo") String contNo);

    /**
     * 查询担保合同对应的借款合同数
     * @param guarContNo
     * @return
     */
    int countCtrLoanContRecordsByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     *
     * @param contNo
     * @return
     */
    String selectGuarContNosByContNo(@Param("contNo") String contNo);

    /**
     * 根据合同编号查询关联的担保合同
     * @param contNos
     * @return
     */
    String selectGuarContNosByContNos(@Param("contNos") String contNos);

    /**
     *
     * @param contNo
     * @return
     */
    String selectGuarContNosByContNoForCreditQuery(@Param("contNo") String contNo);

    /**
     * 获取关联业务合同信息
     * @param model
     * @return
     */
    List<GrtGuarBizRstRelDto> getGrtContBizRelInfo(QueryModel model);

    /**
     * 根据担保合同编号查询担保合同与业务关系数据
     * @param guarContNo
     * @return
     */
    List<GrtGuarBizRstRel> queryGrtGuarBizRstRelDataByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据担保合同编号查询关联的合同编号
     * @param guarContNo
     * @return
     */
    String selectContNosByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据流水号查询担保合同集合
     * @param serno
     * @return
     */
    List<String> selectguarContNosBySerno(@Param("serno") String serno);

    /**
     * 根据担保编号删除
     * @param guarContNo
     * @return
     */
    int deleteByGuarNo(String guarContNo);
}