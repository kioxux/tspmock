/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constant.CommonConstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanAppPrtcptBankSub;
import cn.com.yusys.yusp.service.IqpLoanAppPrtcptBankSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppPrtcptBankSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-05-11 15:49:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploanappprtcptbanksub")
public class IqpLoanAppPrtcptBankSubResource {
    @Autowired
    private IqpLoanAppPrtcptBankSubService iqpLoanAppPrtcptBankSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanAppPrtcptBankSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanAppPrtcptBankSub> list = iqpLoanAppPrtcptBankSubService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanAppPrtcptBankSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanAppPrtcptBankSub>> index(QueryModel queryModel) {
        List<IqpLoanAppPrtcptBankSub> list = iqpLoanAppPrtcptBankSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanAppPrtcptBankSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpLoanAppPrtcptBankSub> show(@PathVariable("pkId") String pkId) {
        IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub = iqpLoanAppPrtcptBankSubService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpLoanAppPrtcptBankSub>(iqpLoanAppPrtcptBankSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanAppPrtcptBankSub> create(@RequestBody IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) throws URISyntaxException {
        iqpLoanAppPrtcptBankSubService.insert(iqpLoanAppPrtcptBankSub);
        return new ResultDto<IqpLoanAppPrtcptBankSub>(iqpLoanAppPrtcptBankSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) throws URISyntaxException {
        int result = iqpLoanAppPrtcptBankSubService.updateSelective(iqpLoanAppPrtcptBankSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpLoanAppPrtcptBankSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanAppPrtcptBankSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询单于行信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<List<IqpLoanAppPrtcptBankSub>> selectBySerno(@RequestBody QueryModel model) {
        List<IqpLoanAppPrtcptBankSub> iqpLoanAppPrtcptBankSub = iqpLoanAppPrtcptBankSubService.selectBySerno(model);
        return new ResultDto<List<IqpLoanAppPrtcptBankSub>>(iqpLoanAppPrtcptBankSub);
    }

    /**
     * @函数名称:insertBySerno
     * @函数描述:通过流水号新增参与行信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertBySerno")
    protected ResultDto<Integer> insertBySerno(@RequestBody IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) {
        int result = iqpLoanAppPrtcptBankSubService.insertBySerno(iqpLoanAppPrtcptBankSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPkId")
    protected ResultDto<Integer> deleteByPkId(@RequestBody IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub) {
        int result = iqpLoanAppPrtcptBankSubService.deleteByPkId(iqpLoanAppPrtcptBankSub);
        return new ResultDto<Integer>(result);
    }

}
