package cn.com.yusys.yusp.service.server.xdxw0016;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0016.req.Xdxw0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0016.resp.Xdxw0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0016Service
 * @类描述: #服务类
 * @功能描述:根据客户号查询查询统一管控额度接口（总额度）
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0016Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0016Service.class);

    @Autowired
    private CmisLmtClientService cmisLmtClientService;//额度信息模块

    /**
     * 根据客户号查询查询统一管控额度接口（总额度）
     *
     * @param xdxw0016DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0016DataRespDto xdxw0016(Xdxw0016DataReqDto xdxw0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value);
        //返回信息
        Xdxw0016DataRespDto Xdxw0016DataRespDto = new Xdxw0016DataRespDto();
        try {
            //请求字段
            String cusId = xdxw0016DataReqDto.getCusId();//客户号

            //调用额度系统服务接口
            CmisLmt0023ReqDto cmisLmt0023ReqDto = new CmisLmt0023ReqDto();//请求
            CmisLmt0023RespDto cmisLmt0023RespDto = new CmisLmt0023RespDto();//响应

            logger.info("***********调用额度服务cmislmt0023查询服务开始*START,查询参数为:{}", JSON.toJSONString(cusId));
            cmisLmt0023ReqDto.setCusId(cusId);
            ResultDto<CmisLmt0023RespDto> ResultDto = cmisLmtClientService.cmislmt0023(cmisLmt0023ReqDto);
            //调用额度系统成功,返回信息
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ResultDto.getCode())) {
                //  获取相关的值并解析
                cmisLmt0023RespDto = ResultDto.getData();
            }
            logger.info("***********调用额度服务cmislmt0023查询服务结束*END,返回结果为:{}", JSON.toJSONString(cmisLmt0023RespDto));

            BigDecimal lmtAmt = cmisLmt0023RespDto.getLmtAmt();//授信总额度
            BigDecimal lmtValAmt =  cmisLmt0023RespDto.getLmtValAmt();//总授信可用额度
            List<CmisLmt0023LmtListRespDto> cmisLmt0023RespDtoLmtLists = cmisLmt0023RespDto.getLmtList();
            //定义返回列表信息
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0016.resp.LmtList> LmtLists = new ArrayList<>();

            if (CollectionUtils.nonEmpty(cmisLmt0023RespDtoLmtLists)) {//返回列表不为空
                LmtLists = cmisLmt0023RespDtoLmtLists.parallelStream().map(cmisLmt0023LmtListRespDto -> {
                            //返回信息
                            cn.com.yusys.yusp.dto.server.xdxw0016.resp.LmtList lmtList = new cn.com.yusys.yusp.dto.server.xdxw0016.resp.LmtList();
                            lmtList.setCusId(cmisLmt0023LmtListRespDto.getCusId());// 客户号
                            lmtList.setCusName(cmisLmt0023LmtListRespDto.getCusName());// 客户名称
                            lmtList.setApprSubSerno(cmisLmt0023LmtListRespDto.getApprSubSerno());// 额度分项流水号
                            lmtList.setLmtBizType(cmisLmt0023LmtListRespDto.getLmtBizType());// 授信品种编号
                            lmtList.setLmtBizTypeName(cmisLmt0023LmtListRespDto.getLmtBizTypeName());// 授信品种名称
                            lmtList.setSpacAmt(cmisLmt0023LmtListRespDto.getSpacAmt());// 授信敞口金额
                            lmtList.setSpacOutstandAmt(cmisLmt0023LmtListRespDto.getSpacOutstandAmt());// 授信敞口已用
                            lmtList.setGuarMode(cmisLmt0023LmtListRespDto.getGuarMode());// 担保方式
                            lmtList.setAvlAmt(cmisLmt0023LmtListRespDto.getAvlAmt());// 授信总额
                            lmtList.setOutstandAmt(cmisLmt0023LmtListRespDto.getOutstandAmt());// 授信总额已用
                            lmtList.setIsRevolv(cmisLmt0023LmtListRespDto.getIsRevolv());// 是否循环
                            lmtList.setStartDate(cmisLmt0023LmtListRespDto.getStartDate());// 起始日期
                            lmtList.setEndDate(cmisLmt0023LmtListRespDto.getEndDate());// 到期日期
                            return lmtList;
                        }).collect(Collectors.toList());
            }
            //返回报文
            Xdxw0016DataRespDto.setCrdAmtTotal(lmtAmt);
            Xdxw0016DataRespDto.setLmtValAmt(lmtValAmt);
            Xdxw0016DataRespDto.setLmtList(LmtLists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value);
        return Xdxw0016DataRespDto;
    }
}
