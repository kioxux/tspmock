package cn.com.yusys.yusp.service.server.xdqt0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.domain.bat.BatBizWydPojo;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdqt0009.req.Xdqt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.resp.Xdqt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.repository.mapper.wyd.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Service
public class Xdqt0009Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdqt0009Service.class);

    @Resource
    private WydBatRecordMapper wydBatRecordMapper;
    @Resource
    private CfgSftpInfoMapper cfgSftpInfoMapper;

    @Resource
    private WydBatchService wydBatchService;

    private static ExecutorService taskThreadPool = Executors.newFixedThreadPool(20);

    private String batchDate;//批处理日期

    /**
     * 微业贷信贷文件处理通知
     *
     * @author zrcbank-fengjj
     * @param xdqt0009DataReqDto 接口输入dto
     * @return xdqt0009DataRespDto
     */
    @Transactional
    public Xdqt0009DataRespDto xdqt0009(Xdqt0009DataReqDto xdqt0009DataReqDto){
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value);
        //返回对象
        Xdqt0009DataRespDto xdqt0009DataRespDto = new Xdqt0009DataRespDto();
        CountDownLatch failedTaskCd = new CountDownLatch(1);
        batchDate = xdqt0009DataReqDto.getBuzdate();
        taskThreadPool.submit(() -> {
            logger.info("微业贷落数开始>>>");
            WydBatRecord wydBatRecord = null;
            boolean wydBatRecordExistFlag = false;
            CfgSftpInfo cfgSftpInfo  = cfgSftpInfoMapper.selectByPrimaryKey("wyd");
            try {
                wydBatRecord = wydBatRecordMapper.selectByBatDate(batchDate);

                if(wydBatRecord == null){
                    String pkId = cn.com.yusys.yusp.commons.util.StringUtils.uuid(false);
                    wydBatRecord = new WydBatRecord();
                    wydBatRecord.setPkId(pkId);
                    wydBatRecord.setMsgBuzdate(batchDate);
                    wydBatRecord.setCreateTime(DateUtils.getCurrDate());
                }else{
                    wydBatRecordExistFlag = true;
                }
                String opflag = xdqt0009DataReqDto.getOpflag();
                wydBatRecord.setMsgOpflag(opflag);
                wydBatRecord.setBatStartTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                wydBatRecord.setIsArtificial("0");//自动

                if("02".equals(opflag)){
                    throw BizException.error("", EpbEnum.EPB099999.value,"接口入参buzdate【" + opflag + "】，为不可处理状态，批处理失败！");
                }

                String username = cfgSftpInfo.getUsername();
                String password = cfgSftpInfo.getPassword();
                String host = cfgSftpInfo.getHost();
                String localPath = cfgSftpInfo.getLocalpath();
                String remotePath = cfgSftpInfo.getServerpath();
                String port = cfgSftpInfo.getPort();
                String batchDt = batchDate.replace("-", "");//去除- 得到yyyyMMdd格式
                wydBatchService.downRemoteFiles(username,password,host,localPath, remotePath,port, batchDt);

                //TODO 1、下载数据文件 2、数据落库 3、向业务表插入数据 4、删除文件
                //String localPath = "C:\\Users\\zrcbank\\Desktop\\EXT-COOPEZIP_20250904\\";
                List<BatBizWydPojo> tableList = wydBatchService.initLoadTableList();
                for(int i=0;i<tableList.size();i++){
                    BatBizWydPojo bbd = tableList.get(i);
                    String fileName = localPath + bbd.getDataFileName()+".txt";
                    File file = new File(fileName);
                    String primary = bbd.getPrimary();
                    String tableA = bbd.getTableA();
                    String tableB = bbd.getTableB();
                    String columns = bbd.getColumns();
                    logger.info("处理文件开始:" + fileName);
                    wydBatchService.loandDateFromFile(tableB,columns,file);
                    logger.info("处理文件结束:" + fileName);
                    String [] primaryArr = primary.split(",");
                    String indexCondition = "";
                    for(int j=0;j<primaryArr.length;j++){
                        indexCondition += " and "+ tableA +"."+primaryArr[j]+ "=" +tableB +"." + primaryArr[j];
                    }
                    logger.info("处理删除冗余数据开始:" + tableA );
                    wydBatchService.deleleExistsData(tableA,tableB,indexCondition);
                    logger.info("处理删除冗余数据结束:" + tableA );
                    logger.info(tableB+"往:" + tableA + "插数据开始" );
                    wydBatchService.insertIntoDataFromB2A(tableA,tableB,columns);
                    logger.info(tableB+"往:" + tableA + "插数据结束" );
                }
                //更新公共八个字段
                wydBatchService.batUpdate8Colum("cmis_psp.psp_wyd_task_list",batchDate);
                //业务表落数
                wydBatchService.saveOrUpdateWydBusinessInfo(batchDate);
                //风险分类
                if(wydBatchService.isLastDayOfMonth(batchDate)){
                    wydBatchService.riskClass1ForWyd();
                    wydBatchService.riskClass2ForWyd();
                    wydBatchService.riskClass3ForWyd();
                }

                wydBatRecord.setBatStatus("100");//成功
                wydBatRecord.setBatFailedMsg(StringUtils.EMPTY);

                //校验微业贷借据合规信息（客户号不合法，科目号为空）
                wydBatchService.checkWydAccLoanInfo(batchDate,cfgSftpInfo.getSendMsgPhones());

                logger.info("微业贷落数结束<<<");

            } catch (Exception ex) {
                ex.printStackTrace();
                wydBatRecord.setBatStatus("101");//失败
                wydBatRecord.setBatFailedMsg(ex.toString());
                logger.info("微业贷落数失败！");
                logger.info("微业贷落数异常日志", ex);
                logger.error("微业贷落数异常日志", ex);
            }finally {
                failedTaskCd.countDown();
                if(wydBatRecord != null){
                    wydBatRecord.setBatEndTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    wydBatRecord.setUpdateTime(DateUtils.getCurrDate());
                    try {
                        if (wydBatRecordExistFlag) {
                            wydBatRecordMapper.updateByPrimaryKeySelective(wydBatRecord);
                        } else {
                            wydBatRecordMapper.insertSelective(wydBatRecord);
                        }
                    }catch(Exception e){
                        logger.info("保存跑批日志失败：", e);
                    }
                    //发送跑批短信
                    wydBatchService.sendWydBatchMsg(cfgSftpInfo.getSendMsgPhones(),wydBatRecord.getBatStatus(),batchDate);
                }
            }
        });

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value);
        return xdqt0009DataRespDto;
    }

    /**
     * 微业贷人工触发批处理
     * @author zrcbank-fengjj
     * @param pkId 微业贷批处理日志主键
     * @throws Exception
     */
    public void  batArtificial(String pkId) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, "batArtificial", "微业贷人工触发批处理");
        WydBatRecord wydBatRecord = wydBatRecordMapper.selectByPrimaryKey(pkId);
        if(wydBatRecord == null){
            throw BizException.error("", EpbEnum.EPB099999.value,"微业贷跑批日志pkId【" + pkId + "】不存在！");
        }
        if("010".equals(wydBatRecord.getBatStatus())){
            throw BizException.error("", EpbEnum.EPB099999.value,"微业贷跑批任务pkId【" + pkId + "】，正在执行中，请勿重新发起！");
        }
        if("100".equals(wydBatRecord.getBatStatus())){
            throw BizException.error("", EpbEnum.EPB099999.value,"微业贷跑批任务pkId【" + pkId + "】，已执行成功，请勿重新发起！");
        }
        wydBatRecord.setBatStatus("010");//状态设置为执行中
        wydBatRecord.setBatStartTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        wydBatRecord.setIsArtificial("1");//手动
        wydBatRecord.setUpdateTime(DateUtils.getCurrDate());
        wydBatRecordMapper.updateByPrimaryKeySelective(wydBatRecord);//先更新状态
        batchDate = wydBatRecord.getMsgBuzdate();
        CountDownLatch failedTaskCd = new CountDownLatch(1);

        taskThreadPool.submit(() -> {
            logger.info("微业贷落数开始>>>");
            CfgSftpInfo cfgSftpInfo  = cfgSftpInfoMapper.selectByPrimaryKey("wyd");
            try {
                String username = cfgSftpInfo.getUsername();
                String password = cfgSftpInfo.getPassword();
                String host = cfgSftpInfo.getHost();
                String localPath = cfgSftpInfo.getLocalpath();
                String remotePath = cfgSftpInfo.getServerpath();
                String port = cfgSftpInfo.getPort();
                String batchDt = batchDate.replace("-", "");//去除- 得到yyyyMMdd格式
                wydBatchService.downRemoteFiles(username,password,host,localPath, remotePath,port, batchDt);

                //TODO 1、下载数据文件 2、数据落库 3、向业务表插入数据 4、删除文件
                //String localPath = "C:\\Users\\zrcbank\\Desktop\\EXT-COOPEZIP_20250904\\";
                List<BatBizWydPojo> tableList = wydBatchService.initLoadTableList();
                for(int i=0;i<tableList.size();i++){
                    BatBizWydPojo bbd = tableList.get(i);
                    String fileName = localPath + bbd.getDataFileName()+".txt";
                    File file = new File(fileName);

                    String primary = bbd.getPrimary();
                    String tableA = bbd.getTableA();
                    String tableB = bbd.getTableB();
                    String columns = bbd.getColumns();
                    logger.info("处理文件开始:" + fileName);
                    wydBatchService.loandDateFromFile(tableB,columns,file);
                    logger.info("处理文件结束:" + fileName);
                    String [] primaryArr = primary.split(",");
                    String indexCondition = "";
                    for(int j=0;j<primaryArr.length;j++){
                        indexCondition += " and "+ tableA +"."+primaryArr[j]+ "=" +tableB +"." + primaryArr[j];
                    }
                    logger.info("处理删除冗余数据开始:" + tableA );
                    wydBatchService.deleleExistsData(tableA,tableB,indexCondition);
                    logger.info("处理删除冗余数据结束:" + tableA );
                    logger.info(tableB+"往:" + tableA + "插数据开始" );
                    wydBatchService.insertIntoDataFromB2A(tableA,tableB,columns);
                    logger.info(tableB+"往:" + tableA + "插数据结束" );
                }
                //更新公共八个字段
                wydBatchService.batUpdate8Colum("cmis_psp.psp_wyd_task_list",batchDate);
                wydBatchService.saveOrUpdateWydBusinessInfo(batchDate);

                //风险分类
                if(wydBatchService.isLastDayOfMonth(batchDate)){
                    wydBatchService.riskClass1ForWyd();
                    wydBatchService.riskClass2ForWyd();
                    wydBatchService.riskClass3ForWyd();
                }

                wydBatRecord.setBatStatus("100");//成功
                wydBatRecord.setBatFailedMsg(StringUtils.EMPTY);

                //校验微业贷借据合规信息（客户号不合法，科目号为空）
                wydBatchService.checkWydAccLoanInfo(batchDate,cfgSftpInfo.getSendMsgPhones());

                logger.info("微业贷落数结束<<<");

            } catch (Exception ex) {
                ex.printStackTrace();
                wydBatRecord.setBatStatus("101");//失败
                wydBatRecord.setBatFailedMsg(ex.getLocalizedMessage());
                logger.info("微业贷落数失败！");
            }finally {
                failedTaskCd.countDown();
                wydBatRecord.setBatEndTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                wydBatRecord.setUpdateTime(DateUtils.getCurrDate());
                try {
                    wydBatRecordMapper.updateByPrimaryKeySelective(wydBatRecord);
                }catch(Exception e){
                    logger.info("保存跑批日志失败：", e);
                }
                //发送跑批短信
                wydBatchService.sendWydBatchMsg(cfgSftpInfo.getSendMsgPhones(),wydBatRecord.getBatStatus(),batchDate);
            }
        });

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, "batArtificial", "微业贷人工触发批处理");
    }


}
