package cn.com.yusys.yusp.web.server.xdxw0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0019.req.Xdxw0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0019.resp.Xdxw0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0019.Xdxw0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:推送决策审批结果（产生信贷批复）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0019:推送决策审批结果（产生信贷批复）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0019Resource.class);

    @Autowired
    private Xdxw0019Service xdxw0019Service;

    /**
     * 交易码：xdxw0019
     * 交易描述：推送决策审批结果（产生信贷批复）
     *
     * @param xdxw0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送决策审批结果（产生信贷批复）")
    @PostMapping("/xdxw0019")
    protected @ResponseBody
    ResultDto<Xdxw0019DataRespDto> xdxw0019(@Validated @RequestBody Xdxw0019DataReqDto xdxw0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataReqDto));
        Xdxw0019DataRespDto xdxw0019DataRespDto = null;// 响应Dto:推送决策审批结果（产生信贷批复）
        ResultDto<Xdxw0019DataRespDto> xdxw0019DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0019DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataReqDto));
            xdxw0019DataRespDto = xdxw0019Service.xdxw0019(xdxw0019DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataRespDto));
            // 封装xdxw0019DataResultDto中正确的返回码和返回信息
            xdxw0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            xdxw0019DataRespDto = new Xdxw0019DataRespDto();
            xdxw0019DataRespDto.setSendStatus("F");
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            // 封装xdxw0019DataResultDto中异常返回码和返回信息
            xdxw0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0019DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            xdxw0019DataRespDto = new Xdxw0019DataRespDto();
            xdxw0019DataRespDto.setSendStatus("F");
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            // 封装xdxw0019DataResultDto中异常返回码和返回信息
            xdxw0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0019DataRespDto到xdxw0019DataResultDto中
        xdxw0019DataResultDto.setData(xdxw0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataResultDto));
        return xdxw0019DataResultDto;
    }
}
