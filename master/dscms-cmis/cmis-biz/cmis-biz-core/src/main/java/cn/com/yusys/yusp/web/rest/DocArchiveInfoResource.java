/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardAppInfo;
import cn.com.yusys.yusp.domain.DocArchiveInfo;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.service.CreditCardAppInfoService;
import cn.com.yusys.yusp.service.DocArchiveInfoService;
import cn.com.yusys.yusp.service.DocImageSpplInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docarchiveinfo")
public class DocArchiveInfoResource {
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private CreditCardAppInfoService creditCardAppInfoService;
    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DocArchiveInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocArchiveInfo> list = docArchiveInfoService.selectAll(queryModel);
        return new ResultDto<List<DocArchiveInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<DocArchiveInfo>> index(@RequestBody QueryModel queryModel) {
        List<DocArchiveInfo> list = docArchiveInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocArchiveInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{docSerno}")
    protected ResultDto<DocArchiveInfo> show(@PathVariable("docSerno") String docSerno) {
        DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docSerno);
        return new ResultDto<DocArchiveInfo>(docArchiveInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<DocArchiveInfo> create(@RequestBody DocArchiveInfo docArchiveInfo) throws URISyntaxException {
        docArchiveInfoService.insert(docArchiveInfo);
        return new ResultDto<DocArchiveInfo>(docArchiveInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocArchiveInfo docArchiveInfo) throws URISyntaxException {
        int result = docArchiveInfoService.update(docArchiveInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{docSerno}")
    protected ResultDto<Integer> delete(@PathVariable("docSerno") String docSerno) {
        int result = docArchiveInfoService.deleteByPrimaryKey(docSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docArchiveInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据档案类型和客户号获取业务流水号
     *
     * @author jijian_yx
     * @date 2021/6/21 16:35
     **/
    @PostMapping("/queryserno")
    protected ResultDto<List<DocArchiveInfo>> selectSernoByDocType(@RequestBody QueryModel queryModel) {
        List<DocArchiveInfo> list = docArchiveInfoService.selectSernoByDocType(queryModel);
        return new ResultDto(list);
    }

    /**
     * 提交入库
     *
     * @author jijian_yx
     * @date 2021/6/21 16:37
     **/
    @PostMapping("/archiveopt")
    protected ResultDto<Map<String, String>> archiveOpt(@RequestBody DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = docArchiveInfoService.archiveOpt(docArchiveInfo);
        return new ResultDto<>(map);
    }

    /**
     * 作废档案归档信息
     *
     * @author jijian_yx
     * @date 2021/6/21 19:59
     **/
    @PostMapping("/invalid")
    protected ResultDto<Map<String, String>> invalid(@RequestBody DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = docArchiveInfoService.invalid(docArchiveInfo);
        return new ResultDto<>(map);
    }

    /**
     * 档案接收
     *
     * @author jijian_yx
     * @date 2021/6/22 22:30
     **/
    @PostMapping("/receive")
    protected ResultDto<Map<String, String>> receive(@RequestBody DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = docArchiveInfoService.receive(docArchiveInfo);
        return new ResultDto<>(map);
    }

    /**
     * 去档案系统查询档案状态
     *
     * @author jijian_yx
     * @date 2021/6/25 14:04
     **/
    @PostMapping("/searchstatus")
    protected ResultDto<Map<String, String>> searchStatus(@RequestBody DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = docArchiveInfoService.searchStatus(docArchiveInfo);
        return new ResultDto<>(map);
    }

    /**
     * 系统生成档案归档信息
     *
     * @author jijian_yx
     * @date 2021/6/29 20:23
     **/
    @PostMapping("/createDocArchiveBySys")
    protected ResultDto<Integer> createDocArchiveBySys(@RequestBody DocArchiveClientDto docArchiveClientDto) {
        int result = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据业务流水号作废"等待入库"状态归档任务和台账
     *
     * @author jijian_yx
     * @date 2021/7/22 16:18
     **/
    @PostMapping("/invalidByBizSerno")
    protected ResultDto<Integer> invalidByBizSerno(@RequestBody String serno) {
        int result = docArchiveInfoService.invalidByBizSerno(serno,"");
        return new ResultDto<>(result);
    }
    
    /**
     * 获取档案池关联零售业务申请流水号
     * @author jijian_yx
     * @date 2021/9/8 16:50
     **/
    @PostMapping("/getLSIqpserno")
    protected ResultDto<String> getLSIqpserno(@RequestBody String bizSerno){
        String result = docArchiveInfoService.getLSIqpserno(bizSerno);
        return new ResultDto<>(result);
    }

    /**
     * 获取合作方准入/协议影像目录
     *
     * @author jijian_yx
     * @date 2021/9/8 14:33
     **/
    @PostMapping("/getCoopImageCode")
    protected ResultDto<String> getCoopImageCode(@RequestBody String bizSerno) {
        String result = docImageSpplInfoService.getCoopImageCodeByBizSerno(bizSerno);
        return new ResultDto<>(result);
    }

    /**
     * 根据业务流水号获取信用卡业务信息
     * @author jijian_yx
     * @date 2021/9/15 23:48
     **/
    @PostMapping("/getXYKImageCode")
    protected ResultDto<CreditCardAppInfo> getXYKImageCode(@RequestBody String bizSerno){
        CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(bizSerno);
        return new ResultDto<CreditCardAppInfo>(creditCardAppInfo);
    }

    /**
     * 提交入库有未知事务问题,暂时在提交入库后回调中处理更新失败的情况
     * @author jijian_yx
     * @date 2021/9/27 17:30
     **/
    @PostMapping("/updatedocafteropt")
    protected ResultDto<Integer> updateDocAfterOpt(@RequestBody String docSerno){
        int result = docArchiveInfoService.updateDocAfterOpt(docSerno);
        return new ResultDto<>(result);
    }
}
