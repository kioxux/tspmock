package cn.com.yusys.yusp.web.server.xdda0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdda0001.req.Xdda0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdda0001.resp.Xdda0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdda0001.Xdda0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 接口处理类:扫描人信息登记
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDA0001:扫描人信息登记")
@RestController
@RequestMapping("/api/bizda4bsp")
public class BizXdda0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdda0001Resource.class);

    @Autowired
    private Xdda0001Service xdda0001Service;
    /**
     * 交易码：xdda0001
     * 交易描述：扫描人信息登记
     *
     * @param xdda0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("扫描人信息登记")
    @PostMapping("/xdda0001")
    protected @ResponseBody
    ResultDto<Xdda0001DataRespDto> xdda0001(@Validated @RequestBody Xdda0001DataReqDto xdda0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataReqDto));
        Xdda0001DataRespDto xdda0001DataRespDto = new Xdda0001DataRespDto();// 响应Dto:扫描人信息登记
        ResultDto<Xdda0001DataRespDto> xdda0001DataResultDto = new ResultDto<>();
        // 档案状态
        String docStatus = "";
        try {
            // 操作类型是01的时候，表示档案系统对档案内容已进行入库操作
            if(Objects.equals("01",xdda0001DataReqDto.getOprType())) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataReqDto));
                xdda0001DataRespDto = xdda0001Service.insertDocImageSppi(xdda0001DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataRespDto));
                // 操作类型01，代表已入库。档案台账以及档案归档中的档案状态修改为03：表示已入库
                docStatus = "03";
            } else if (Objects.equals("02",xdda0001DataReqDto.getOprType())) {// 操作类型是02的时候，表示档案系统对档案内容进行退回操作
                // 操作类型02，代表已退回。档案台账以及档案归档中的档案状态修改为04：表示退回在途
                docStatus = "04";
            } else if (Objects.equals("03",xdda0001DataReqDto.getOprType())) {// 操作类型是03的时候，表示档案系统对档案内容进行出库操作
                // 操作类型03，代表出库在途。表示档案系统对档案内容进行出库操作（此时借阅的记录将从出库在途变成已出库）
                docStatus = "08";
            } else if (Objects.equals("04",xdda0001DataReqDto.getOprType())) {// 操作类型是04的时候，表示档案系统对档案内容进行再入库操作（此时借阅的记录将从归还再途变成（已入库））
                docStatus = "03";
            }
            // 需要同时同步更新（档案台账以及档案归档中的档案状态）
            if(Objects.equals("01",xdda0001DataReqDto.getOprType()) || Objects.equals("02",xdda0001DataReqDto.getOprType())) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataReqDto));
                xdda0001Service.updateStatus(xdda0001DataReqDto,docStatus);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataRespDto));
            }else
            // 需要同时同步更新（档案台账以及档案归档中的档案状态）
            if(Objects.equals("03",xdda0001DataReqDto.getOprType()) || Objects.equals("04",xdda0001DataReqDto.getOprType())) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataReqDto));
                xdda0001Service.updateDocStatus(xdda0001DataReqDto,docStatus);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataRespDto));
            }
            // 封装xdda0001DataResultDto中正确的返回码和返回信息
            xdda0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdda0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, e.getMessage());
            // 封装xdda0001DataResultDto中异常返回码和返回信息
            xdda0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdda0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdda0001DataRespDto到xdda0001DataResultDto中
        xdda0001DataResultDto.setData(xdda0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataResultDto));
        return xdda0001DataResultDto;
    }
}
