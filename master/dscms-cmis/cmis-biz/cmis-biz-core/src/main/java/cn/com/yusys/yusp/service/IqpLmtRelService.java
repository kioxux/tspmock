/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLmtRel;
import cn.com.yusys.yusp.repository.mapper.IqpLmtRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLmtRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-08 17:12:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpLmtRelService {

    @Autowired
    private IqpLmtRelMapper iqpLmtRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLmtRel selectByPrimaryKey(String pkId) {
        return iqpLmtRelMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLmtRel> selectAll(QueryModel model) {
        List<IqpLmtRel> records = (List<IqpLmtRel>) iqpLmtRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpLmtRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLmtRel> list = iqpLmtRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpLmtRel record) {
        return iqpLmtRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLmtRel record) {
        return iqpLmtRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpLmtRel record) {
        return iqpLmtRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpLmtRel record) {
        return iqpLmtRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpLmtRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLmtRelMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新授信关系表数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpLmtRelMapper.updateByParams(delMap);
    }

    /**
     * 通过入参查询信息
     * @param params
     * @return
     */
    public IqpLmtRel selectByParams(Map params) {
        return iqpLmtRelMapper.selectByParams(params);
    }

    /**
     * 通过业务流水号查询
     * @param oldIqpserno
     * @return
     */
    public List<IqpLmtRel> selectByIqpSerno(String oldIqpserno) {
        return iqpLmtRelMapper.selectByIqpSerno(oldIqpserno);
    }

    /**
     * 针对自有额度，通过额度编号获取数据并更新
     * @param updateMap
     * @return
     */
    public int updateLmtLimitNo(Map updateMap) {
        return iqpLmtRelMapper.updateLmtLimitNo(updateMap);
    }
}
