package cn.com.yusys.yusp.web.server.xdht0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0013.req.Xdht0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0013.resp.Xdht0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0013.Xdht0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同信息查询(微信小程序)
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0013:合同信息查询(微信小程序)")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0013Resource.class);

    @Autowired
    private Xdht0013Service xdht0013Service;

    /**
     * 交易码：xdht0013
     * 交易描述：合同信息查询(微信小程序)
     *
     * @param xdht0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息查询(微信小程序)")
    @PostMapping("/xdht0013")
    protected @ResponseBody
    ResultDto<Xdht0013DataRespDto> xdht0013(@Validated @RequestBody Xdht0013DataReqDto xdht0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataReqDto));
        Xdht0013DataRespDto xdht0013DataRespDto = new Xdht0013DataRespDto();// 响应Dto:合同信息查询(微信小程序)
        ResultDto<Xdht0013DataRespDto> xdht0013DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataReqDto));
            xdht0013DataRespDto = xdht0013Service.getXdht0013(xdht0013DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataRespDto));
            // 封装xdht0013DataResultDto中正确的返回码和返回信息
            xdht0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, e.getMessage());
            // 封装xdht0013DataResultDto中异常返回码和返回信息
            xdht0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0013DataRespDto到xdht0013DataResultDto中
        xdht0013DataResultDto.setData(xdht0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataRespDto));
        return xdht0013DataResultDto;
    }
}
