/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarPro
 * @类描述: lmt_car_pro数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 16:34:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_car_pro")
public class LmtCarPro extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 授信协议编号 **/
	@Column(name = "lmt_ctr_no", unique = false, nullable = true, length = 40)
	private String lmtCtrNo;
	
	/** 项目编号 **/
	@Column(name = "pro_no", unique = false, nullable = false, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "pro_name", unique = false, nullable = true, length = 80)
	private String proName;
	
	/** 销售网点数量 **/
	@Column(name = "sale_brh_qnt", unique = false, nullable = true, length = 80)
	private String saleBrhQnt;
	
	/** 合作品牌 **/
	@Column(name = "coop_brand", unique = false, nullable = true, length = 80)
	private String coopBrand;
	
	/** 汽车使用类别 STD_ZB_CAR_UTIL_TYPE **/
	@Column(name = "car_util_type", unique = false, nullable = true, length = 5)
	private String carUtilType;
	
	/** 联系人姓名 **/
	@Column(name = "linkman_name", unique = false, nullable = true, length = 80)
	private String linkmanName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 32)
	private String certCode;
	
	/** 联系方式 **/
	@Column(name = "link_mode", unique = false, nullable = true, length = 35)
	private String linkMode;
	
	/** 备注 **/
	@Column(name = "memo", unique = false, nullable = true, length = 500)
	private String memo;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param saleBrhQnt
	 */
	public void setSaleBrhQnt(String saleBrhQnt) {
		this.saleBrhQnt = saleBrhQnt;
	}
	
    /**
     * @return saleBrhQnt
     */
	public String getSaleBrhQnt() {
		return this.saleBrhQnt;
	}
	
	/**
	 * @param coopBrand
	 */
	public void setCoopBrand(String coopBrand) {
		this.coopBrand = coopBrand;
	}
	
    /**
     * @return coopBrand
     */
	public String getCoopBrand() {
		return this.coopBrand;
	}
	
	/**
	 * @param carUtilType
	 */
	public void setCarUtilType(String carUtilType) {
		this.carUtilType = carUtilType;
	}
	
    /**
     * @return carUtilType
     */
	public String getCarUtilType() {
		return this.carUtilType;
	}
	
	/**
	 * @param linkmanName
	 */
	public void setLinkmanName(String linkmanName) {
		this.linkmanName = linkmanName;
	}
	
    /**
     * @return linkmanName
     */
	public String getLinkmanName() {
		return this.linkmanName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkMode
	 */
	public void setLinkMode(String linkMode) {
		this.linkMode = linkMode;
	}
	
    /**
     * @return linkMode
     */
	public String getLinkMode() {
		return this.linkMode;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}