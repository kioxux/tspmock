/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfo;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoRst;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoRstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoRstService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:23:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoRstService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoRstMapper lmtSigInvestBasicInfoRstMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoRst selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoRstMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoRst> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoRst> records = (List<LmtSigInvestBasicInfoRst>) lmtSigInvestBasicInfoRstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoRst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoRst> list = lmtSigInvestBasicInfoRstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoRst record) {
        return lmtSigInvestBasicInfoRstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoRst record) {
        return lmtSigInvestBasicInfoRstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoRst record) {
        return lmtSigInvestBasicInfoRstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoRst record) {
        return lmtSigInvestBasicInfoRstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoRstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoRstMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: initLmtSigInvestBasicInfoRstInfo
     * @方法描述: 初始化 单笔投资对应底层资产基本情况审批 数据信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoRst initLmtSigInvestBasicInfoRstInfo(Object lmtSigInvestBasicBean) {
        //主键
        String pkValue = generatePkId();
        LmtSigInvestBasicInfoRst lmtSigInvestBasicInfoRst = new LmtSigInvestBasicInfoRst() ;
        //数据拷贝
        BeanUtils.copyProperties(lmtSigInvestBasicBean, lmtSigInvestBasicInfoRst);
        //生成主键
        lmtSigInvestBasicInfoRst.setPkId(pkValue);
        //最新更新日期
        lmtSigInvestBasicInfoRst.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicInfoRst.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicInfoRst.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestBasicInfoRst ;
    }

    /**
     * 批复变更-生成批复信息
     * @param lmtSigInvestBasicInfo
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     */
    public void insertBasicInfoRst(LmtSigInvestBasicInfo lmtSigInvestBasicInfo, String oldSerno, String replySerno, String currentOrgId, String currentUserId) {
        LmtSigInvestBasicInfoRst lmtSigInvestBasicInfoRst = new LmtSigInvestBasicInfoRst();
        copyProperties(lmtSigInvestBasicInfo,lmtSigInvestBasicInfoRst);

        lmtSigInvestBasicInfoRst.setSerno(oldSerno);
        lmtSigInvestBasicInfoRst.setReplySerno(replySerno);
        //初始化新增通用属性信息
        initInsertDomainProperties(lmtSigInvestBasicInfoRst,currentUserId,currentOrgId);
        insert(lmtSigInvestBasicInfoRst);
    }
}
