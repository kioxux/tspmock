package cn.com.yusys.yusp.web.server.xdxw0083;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0083.req.Xdxw0083DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0083.resp.Xdxw0083DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0083.Xdxw0083Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:优企贷还款账号变更
 *
 * @author zrcb
 * @version 1.0
 */
@Api(tags = "XDXW0083:优企贷还款账号变更")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0083Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0083Resource.class);

    @Autowired
    private Xdxw0083Service xdxw0083Service;

    /**
     * 交易码：xdxw0083
     * 交易描述：优企贷还款账号变更
     *
     * @param xdxw0083DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷还款账号变更")
    @PostMapping("/xdxw0083")
    protected @ResponseBody
    ResultDto<Xdxw0083DataRespDto> xdxw0083(@Validated @RequestBody Xdxw0083DataReqDto xdxw0083DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataReqDto));
        Xdxw0083DataRespDto xdxw0083DataRespDto = new Xdxw0083DataRespDto();// 响应Dto:优企贷还款账号变更
        ResultDto<Xdxw0083DataRespDto> xdxw0083DataResultDto = new ResultDto<>();
        try {
            String billNo = xdxw0083DataReqDto.getBillNo();
            String repayNo = xdxw0083DataReqDto.getRepayAcctNo();

            if(StringUtil.isEmpty(billNo)||StringUtil.isEmpty(repayNo)){
                xdxw0083DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0083DataResultDto.setMessage("参数不能为空");
                return xdxw0083DataResultDto;
            }
            // 从xdxw0083DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataReqDto));
            xdxw0083DataRespDto = xdxw0083Service.xdxw0083(xdxw0083DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataRespDto));
            // 封装xdxw0083DataResultDto中正确的返回码和返回信息
            xdxw0083DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0083DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch(BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, e.getMessage());
            // 封装xdxw0083DataResultDto中异常返回码和返回信息
            xdxw0083DataResultDto.setCode(e.getErrorCode());
            xdxw0083DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, e.getMessage());
            // 封装xdxw0083DataResultDto中异常返回码和返回信息
            xdxw0083DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0083DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0083DataRespDto到xdxw0083DataResultDto中
        xdxw0083DataResultDto.setData(xdxw0083DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataResultDto));
        return xdxw0083DataResultDto;
    }
}
