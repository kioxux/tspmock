/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;


import cn.com.yusys.yusp.domain.BailDepositInfo;
import cn.com.yusys.yusp.domain.CtrAccpCont;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailDepositInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-09 21:06:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BailDepositInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    BailDepositInfo selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(BailDepositInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(BailDepositInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(BailDepositInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: deleteByContNo
     * @方法描述: 根据借据编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称: deleteByContNo
     * @方法描述: 更新保证金金额
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updatebailAmt(BailDepositInfo record);

    /**
     * @方法名称: updatebailAmtAndStatus
     * @方法描述: 通过借据号将保证金金额小于等于零的相关信息状态置为'1'失效状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updatebailAmtAndStatus(BailDepositInfo record);

    /**
     * 有效台账下当天补交保证金总额
     *
     * @param bdiMap
     * @return
     */
    BigDecimal queryDtbjBailAmtByBdiMap(Map bdiMap);

    /**
     * 已缴存保证金总额
     *
     * @param bdiMap
     * @return
     */
    BigDecimal queryYjcBailAmtByBdiMap(Map bdiMap);
    /**
     * selectBailDepositInfoByStatus
     * 查询有效的保证金金额
     * @param contNo
     * @return
     */
    BigDecimal selectBailDepositInfoByStatus(@Param("contNo") String contNo);

    /**
     * @方法名称: insertOneInfo
     * @方法描述: 插入 -保证金信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertOneInfo(BailDepositInfo record);

    /**
     * @方法名称: insertTwoInfo
     * @方法描述: 插入 -保证金信息2
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertTwoInfo(BailDepositInfo record);

    /**
     * @方法名称: selectBzjInfo
     * @方法描述: 保证金金额查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    String selectBzjInfo(String hxSecurityHandNo);

    /**
     * @方法名称: updateBail_amt
     * @方法描述: 更新保证金金额
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBailamt(Map map);

    /**
     * @方法名称: selectBzjInfo
     * @方法描述: 保证金金额查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    String selectSfcz(String hxSecurityHandNo);

    /**
     * @方法名称: selectSxedInfo
     * @方法描述: 查询授信额度相关信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    CtrAccpCont selectSxedInfo(String contno);

    /**
     * @方法名称: selectNewBailamt
     * @方法描述: 台账中已出账批次在保证金登记表中新的补交保证金记录
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<BailDepositInfo> selectNewBailamt(Map map);

    //查询总金额
    String selectSumAmt(String contno);
}