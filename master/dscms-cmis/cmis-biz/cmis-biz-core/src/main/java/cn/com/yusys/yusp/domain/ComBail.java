/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ComBail
 * @类描述: com_bail数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 11:01:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "com_bail")
public class ComBail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 保证金序号 **/
	@Column(name = "SERIAL_NUMBER", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal serialNumber;
	
	/** 保证金账户编号 **/
	@Column(name = "BAIL_ACCT_NO", unique = false, nullable = true, length = 40)
	private String bailAcctNo;
	
	/** 保证金账户名称 **/
	@Column(name = "BAIL_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String bailAcctName;
	
	/** 保证金币种 STD_ZB_CUR_TYP **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;
	
	/** 账户余额 **/
	@Column(name = "ACCT_AMT_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal acctAmtBalance;
	
	/** 保证金来源 STD_ZB_BAIL_SOUR **/
	@Column(name = "BAIL_SOUR", unique = false, nullable = true, length = 5)
	private String bailSour;
	
	/** 保证金类型 **/
	@Column(name = "BAIL_TYPE", unique = false, nullable = true, length = 5)
	private String bailType;
	
	/** 存期 **/
	@Column(name = "DEP_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal depTerm;
	
	/** 开户机构编号 **/
	@Column(name = "OPEN_ORG", unique = false, nullable = true, length = 40)
	private String openOrg;
	
	/** 保证金汇率 **/
	@Column(name = "EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRate;
	
	/** 类型 STD_ZB_COM_BAIL_TP **/
	@Column(name = "TYPE", unique = false, nullable = true, length = 5)
	private String type;
	
	/** 开户起始日期 **/
	@Column(name = "DEP_STAR_DATE", unique = false, nullable = true, length = 10)
	private String depStarDate;
	
	/** 开户到期日期 **/
	@Column(name = "DEP_END_DATE", unique = false, nullable = true, length = 10)
	private String depEndDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param serialNumber
	 */
	public void setSerialNumber(java.math.BigDecimal serialNumber) {
		this.serialNumber = serialNumber;
	}
	
    /**
     * @return serialNumber
     */
	public java.math.BigDecimal getSerialNumber() {
		return this.serialNumber;
	}
	
	/**
	 * @param bailAcctNo
	 */
	public void setBailAcctNo(String bailAcctNo) {
		this.bailAcctNo = bailAcctNo;
	}
	
    /**
     * @return bailAcctNo
     */
	public String getBailAcctNo() {
		return this.bailAcctNo;
	}
	
	/**
	 * @param bailAcctName
	 */
	public void setBailAcctName(String bailAcctName) {
		this.bailAcctName = bailAcctName;
	}
	
    /**
     * @return bailAcctName
     */
	public String getBailAcctName() {
		return this.bailAcctName;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}
	
    /**
     * @return bailCurType
     */
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param acctAmtBalance
	 */
	public void setAcctAmtBalance(java.math.BigDecimal acctAmtBalance) {
		this.acctAmtBalance = acctAmtBalance;
	}
	
    /**
     * @return acctAmtBalance
     */
	public java.math.BigDecimal getAcctAmtBalance() {
		return this.acctAmtBalance;
	}
	
	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour;
	}
	
    /**
     * @return bailSour
     */
	public String getBailSour() {
		return this.bailSour;
	}
	
	/**
	 * @param bailType
	 */
	public void setBailType(String bailType) {
		this.bailType = bailType;
	}
	
    /**
     * @return bailType
     */
	public String getBailType() {
		return this.bailType;
	}
	
	/**
	 * @param depTerm
	 */
	public void setDepTerm(java.math.BigDecimal depTerm) {
		this.depTerm = depTerm;
	}
	
    /**
     * @return depTerm
     */
	public java.math.BigDecimal getDepTerm() {
		return this.depTerm;
	}
	
	/**
	 * @param openOrg
	 */
	public void setOpenOrg(String openOrg) {
		this.openOrg = openOrg;
	}
	
    /**
     * @return openOrg
     */
	public String getOpenOrg() {
		return this.openOrg;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return exchangeRate
     */
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param depStarDate
	 */
	public void setDepStarDate(String depStarDate) {
		this.depStarDate = depStarDate;
	}
	
    /**
     * @return depStarDate
     */
	public String getDepStarDate() {
		return this.depStarDate;
	}
	
	/**
	 * @param depEndDate
	 */
	public void setDepEndDate(String depEndDate) {
		this.depEndDate = depEndDate;
	}
	
    /**
     * @return depEndDate
     */
	public String getDepEndDate() {
		return this.depEndDate;
	}


}