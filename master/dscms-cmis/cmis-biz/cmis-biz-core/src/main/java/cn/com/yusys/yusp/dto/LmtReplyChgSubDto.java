package cn.com.yusys.yusp.dto;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class LmtReplyChgSubDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 批复分项流水号 **/
    private String replySubSerno;

    /** 批复变更分项流水号 **/
    private String subSerno;

    /** 批复变更分项名称 **/
    private String subName;

    /** 批复变更流水号 **/
    private String serno;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 币种 **/
    private String curType;

    /** 授信金额 **/
    private java.math.BigDecimal lmtAmt;

    /** 担保方式 **/
    private String guarMode;

    /** 是否预授信额度 **/
    private String isPreLmt;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    /**
     * 分项下的分项产品
     **/
    private List<LmtReplyChgSubPrdDto> children;

    /**
     * 页面用的授信分项额度编号
     **/
    private String lmtChgDrawNo;

    /**
     * 页面渲染用的授信品种
     **/
    private String lmtChgDrawType;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getReplySubSerno() {
        return replySubSerno;
    }

    public void setReplySubSerno(String replySubSerno) {
        this.replySubSerno = replySubSerno;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<LmtReplyChgSubPrdDto> getChildren() {
        return children;
    }

    public void setChildren(List<LmtReplyChgSubPrdDto> children) {
        this.children = children;
    }

    public String getLmtChgDrawNo() {
        return lmtChgDrawNo;
    }

    public void setLmtChgDrawNo(String lmtChgDrawNo) {
        this.lmtChgDrawNo = lmtChgDrawNo;
    }

    public String getLmtChgDrawType() {
        return lmtChgDrawType;
    }

    public void setLmtChgDrawType(String lmtChgDrawType) {
        this.lmtChgDrawType = lmtChgDrawType;
    }
}
