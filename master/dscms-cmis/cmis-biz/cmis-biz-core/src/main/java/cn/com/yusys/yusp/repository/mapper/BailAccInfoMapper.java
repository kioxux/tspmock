/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.AccAccpDto;
import cn.com.yusys.yusp.dto.BailAccInfoDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.BailAccInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailAccInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:23:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BailAccInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    BailAccInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<BailAccInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(BailAccInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(BailAccInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(BailAccInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(BailAccInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据入参serno查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<BailAccInfo> selectBySerno(String serno);

    /**
     * 根据合同号查询有效保证金
     * @param contNo
     * @return
     */
    BigDecimal getEffectBail(String contNo);

    String getTfBailAccNo(String contNo);

    String getCvrsBailAccNo(String contNo);

    /**
     * @方法名称: updateBailAccInfoBySerno
     * @方法描述: 根据流水号保证金信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateBailAccInfoBySerno(BailAccInfo record);

    /**
     * @方法名称: selectAccNoBySerno
     * @方法描述: 根据流水号获取保证金账号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectAccNoBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectInfoBySerno
     * @方法描述: 根据流水号获取保证金账账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    BailAccInfo selectInfoBySerno(@Param("serno")String serno);

    /**
     * @方法名称: queryBySerno
     * @方法描述: 根据流水号回显银承台账保证金信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<BailAccInfoDto> queryBySerno(QueryModel model);

    /**
     * 根据资产池流水号查询保证金账户信息
     * @param serno
     * @return
     */
    Map<String, String> selectAsplBailBySerno(@Param("serno")String serno);

    int deleteBySerno(@Param("serno") String serno);
}