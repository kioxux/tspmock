package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrTfLocContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpTfLocAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrTfLocContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-14 11:16:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrTfLocContService {

    private static final Logger log = LoggerFactory.getLogger(CtrEntrustLoanContService.class);

    @Autowired
    private CtrTfLocContMapper ctrTfLocContMapper;

    @Autowired
    private AccTfLocService accTfLocService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private IqpTfLocAppMapper iqpTfLocAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrTfLocCont selectByPrimaryKey(String pkId) {
        return ctrTfLocContMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrTfLocCont> selectAll(QueryModel model) {
        List<CtrTfLocCont> records = (List<CtrTfLocCont>) ctrTfLocContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrTfLocCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrTfLocCont> list = ctrTfLocContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CtrTfLocCont record) {
        return ctrTfLocContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CtrTfLocCont record) {
        return ctrTfLocContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CtrTfLocCont record) {
        return ctrTfLocContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CtrTfLocCont record) {
        return ctrTfLocContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 通过合同号更新LMTACCNO
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateLmtAccNoByContNo(CtrTfLocCont record) {
        return ctrTfLocContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return ctrTfLocContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return ctrTfLocContMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrTfLocCont> toSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatusOther", CmisCommonConstants.CONT_STATUS_100);
        return ctrTfLocContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrTfLocCont> doneSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatusOther", CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrTfLocContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrTfLocCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrTfLocContMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: onSign
     * @方法描述: 开证合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int onSign(CtrTfLocCont ctrTfLocCont) {
        ctrTfLocCont.setContStatus(CmisCommonConstants.CONT_STATUS_200);
        if(!CmisCommonConstants.GUAR_MODE_40.equals(ctrTfLocCont.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(ctrTfLocCont.getGuarMode())
                && !CmisCommonConstants.GUAR_MODE_00.equals(ctrTfLocCont.getGuarMode())){
            List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.getByContNo(ctrTfLocCont.getContNo());
            if (CollectionUtils.isEmpty(list)) {
                log.error("主合同【{}】未关联担保合同！",ctrTfLocCont.getContNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            //担保合同签订生效
            for (GrtGuarBizRstRel grtGuarBizRstRel : list) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                grtGuarCont.setSignDate(ctrTfLocCont.getPaperContSignDate());
                if(org.apache.commons.lang.StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);
            }
        }

        CtrTfLocCont ctrTfLocContObj = ctrTfLocContMapper.selectByContNo(ctrTfLocCont.getContNo());
        // 判断是否为合同续签,若为续签合同则恢复原合同的占额
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrTfLocContObj.getIsRenew())){
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrTfLocContObj.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrTfLocContObj.getOrigiContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
            cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
            cmisLmt0012ReqDto.setInputId(ctrTfLocContObj.getInputId());//登记人
            cmisLmt0012ReqDto.setInputBrId(ctrTfLocContObj.getInputBrId());//登记机构
            cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
            log.info("开证合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrTfLocContObj.getContNo(), ctrTfLocContObj.getOrigiContNo(), JSON.toJSONString(cmisLmt0012ReqDto));
            ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("开证合同【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrTfLocContObj.getContNo(), ctrTfLocContObj.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
            if (!"0".equals(resultDtoDto.getCode())) {
                log.error("开证合同【{}】签订异常",ctrTfLocContObj.getContNo());
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
            }
        }

        guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_06, ctrTfLocCont.getContNo());
        if("412265".equals(ctrTfLocCont.getPrdId()) || "410002".equals(ctrTfLocCont.getPrdId())){
            // 生成归档任务
            log.info("开始系统生成档案归档信息");
            String cusId = ctrTfLocCont.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setDocClass("03");
            docArchiveClientDto.setDocType("15");// 15国际业务
            docArchiveClientDto.setDocBizType("06");// 06开证
            docArchiveClientDto.setBizSerno(ctrTfLocCont.getSerno());
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(ctrTfLocCont.getManagerId());
            docArchiveClientDto.setManagerBrId(ctrTfLocCont.getManagerBrId());
            docArchiveClientDto.setInputId(ctrTfLocCont.getInputId());
            docArchiveClientDto.setInputBrId(ctrTfLocCont.getInputBrId());
            docArchiveClientDto.setContNo(ctrTfLocCont.getContNo());
            docArchiveClientDto.setLoanAmt(ctrTfLocCont.getContAmt());
            docArchiveClientDto.setStartDate(ctrTfLocCont.getStartDate());
            docArchiveClientDto.setEndDate(ctrTfLocCont.getEndDate());
            docArchiveClientDto.setPrdId(ctrTfLocCont.getPrdId());
            docArchiveClientDto.setPrdName(ctrTfLocCont.getPrdName());
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if (num < 1) {
                log.info("信用证开证系统生成档案归档信息失败");
            }
        }

        return ctrTfLocContMapper.updateByPrimaryKey(ctrTfLocCont);
    }

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:54
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrTfLocCont selectByIqpSerno(String serno) {
        return ctrTfLocContMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:54
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrTfLocCont selectByContNo(String contNo) {
        return ctrTfLocContMapper.selectByContNo(contNo);
    }

    /**
     * 合同注销保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateLogout(@RequestBody Map params) throws ParseException {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        int pvpLoanAppCount = 0;
        int accTfLocCount = 0;
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            String logPrefix = "合同签订" + serno;

            log.info(logPrefix + "获取申请数据");
            CtrTfLocCont ctrTfLocCont = ctrTfLocContMapper.selectByContNo((String) params.get("contNo"));

            if (ctrTfLocCont == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            // 先判断合同是否存在未结清的业务
            String responseResult = ctrLoanContService.isContUncleared(ctrTfLocCont.getContNo());
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                rtnCode = EcbEnum.ECB020055.key;
                rtnMsg = EcbEnum.ECB020055.value;
                return result;
            }
            // 获取合同注销后的合同状态
            String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrTfLocCont.getEndDate(),ctrTfLocCont.getContStatus());

            log.info(logPrefix + "合同注销-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                log.info(logPrefix + "保存开证贷款合同数据");
                int updCount = ctrTfLocContMapper.updateByPrimaryKeySelective(ctrTfLocCont);
                if (updCount < 0) {
                    throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
                }

                ctrTfLocCont.setInputId(userInfo.getLoginCode());
                ctrTfLocCont.setInputBrId(userInfo.getOrg().getCode());
                ctrTfLocCont.setManagerId(userInfo.getLoginCode());
                ctrTfLocCont.setManagerBrId(userInfo.getOrg().getCode());
                ctrTfLocCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ctrTfLocCont.setContStatus(finalContStatus);
                update(ctrTfLocCont);
            }
            // 折算金额和保证金比例
            BigDecimal contHighAmt = ctrTfLocCont.getContHighAvlAmt();
            BigDecimal cvtCnyAmt = ctrTfLocCont.getCvtCnyAmt();
            BigDecimal bailPerc = ctrTfLocCont.getBailPerc();
            // 保证金金额和实际占额金额
            BigDecimal bailAmt = BigDecimal.ZERO;
            String guarMode = ctrTfLocCont.getGuarMode();
            //恢复敞口
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            //恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;
            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverSpacAmtCny = BigDecimal.ZERO;
                bailAmt = contHighAmt.multiply(bailPerc);
                recoverAmtCny = cvtCnyAmt;
            } else {
                recoverSpacAmtCny = cvtCnyAmt;
                bailAmt = bailPerc.multiply(contHighAmt.divide(BigDecimal.ONE.subtract(bailPerc),2,BigDecimal.ROUND_HALF_UP));
                recoverAmtCny = cvtCnyAmt.add(bailAmt);
            }

            // 根据合是否存在业务，得到恢复类型
            String recoverType = "";
            List<AccTfLoc> accTfLocList = accTfLocService.selectByContNo(ctrTfLocCont.getContNo());
            if(CollectionUtils.nonEmpty(accTfLocList)){
                //结清注销
                for (AccTfLoc accTfLoc : accTfLocList) {
                    if(CmisCommonConstants.ACC_STATUS_7.equals(accTfLoc.getAccStatus())){
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                    }else{
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                        break;
                    }
                }
            }else {
                //未用注销
                recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
            }
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrTfLocCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrTfLocCont.getContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
            cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
            cmisLmt0012ReqDto.setInputId(ctrTfLocCont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrTfLocCont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            if(!"0".equals(resultDto.getCode())){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,code, resultDto.getData().getErrorMsg());
            }

            //不存在电子用印的情况下，需要作废档案归档任务
            if (!"1".equals(ctrTfLocCont.getIsESeal())) {
                log.info("根据业务申请编号【" + ctrTfLocCont.getSerno() + "】需要作废档案归档任务开始");
                try {
                    docArchiveInfoService.invalidByBizSerno(ctrTfLocCont.getSerno(),"");
                } catch (Exception e) {
                    log.info("根据业务申请编号【" + ctrTfLocCont.getSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                }
                log.info("根据业务申请编号【" + ctrTfLocCont.getSerno() + "】需要作废档案归档任务结束");
            }
        } catch (BizException e){
            log.error("开证合同注销异常", e.getMessage(), e);
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
        } catch (Exception e) {
            log.error("开证合同注销异常" + serno + "异常", e.getMessage(), e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrTfLocCont> selectByQuerymodel(QueryModel model) {
        return ctrTfLocContMapper.selectByModel(model);
    }

    /**
     * @param contNo
     * @return BigDecimal
     * @author qw
     * @date 2021/8/20 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可出账金额
     * @修改历史:
     */
    public BigDecimal getCanOutAccountAmt(String contNo) {
        //可出账金额
        BigDecimal canOutAccountAmt = BigDecimal.ZERO;
        CtrTfLocCont ctrTfLocCont = ctrTfLocContMapper.selectByContNo(contNo);
        if(Objects.isNull(ctrTfLocCont)){
            throw BizException.error(null, EcbEnum.ECB020022.key, EcbEnum.ECB020022.value);
        }
        //合同最高可用信金额折算人民币金额
        BigDecimal contHighAmt = ctrTfLocCont.getCvtCnyAmt();

        //业务敞口余额（未扣减保证金）
        BigDecimal totalSpacBalance = BigDecimal.ZERO;
        //业务敞口初始总金额（未扣减保证金）
        BigDecimal totalOrigiSpacAmt = BigDecimal.ZERO;

        //业务敞口余额（扣减保证金）
        BigDecimal totalSpacBalanceSubtractBailAmt = BigDecimal.ZERO;
        //业务敞口初始总金额（扣减保证金）
        BigDecimal totalOrigiSpacSubtractBailAmt = BigDecimal.ZERO;

        //保证金币种
        String bailCurType = "";
        //保证金折算人民币金额
        BigDecimal bailAmtCny = BigDecimal.ZERO;
        List<AccTfLoc> accTfLocList = accTfLocService.selectByContNo(ctrTfLocCont.getContNo());
        if(CollectionUtils.nonEmpty(accTfLocList)){
            for (AccTfLoc accTfLoc : accTfLocList) {
                totalSpacBalance = totalSpacBalance.add(accTfLoc.getCvtCnySpac());//业务敞口余额（未扣减保证金）
                totalOrigiSpacAmt = totalOrigiSpacAmt.add(accTfLoc.getOrigiOpenAmt().multiply(accTfLoc.getExchgRate()));//业务敞口初始总金额（未扣减保证金）

                bailCurType = accTfLoc.getBailCurType();
                Map map = new HashMap();
                map.put("curType", bailCurType);
                Map result = iqpLoanAppService.getExchangeRate(map);
                BigDecimal rate = (BigDecimal) result.get("rate");
                bailAmtCny = accTfLoc.getBailAmt().multiply(rate);
                totalSpacBalanceSubtractBailAmt = totalSpacBalanceSubtractBailAmt.add(accTfLoc.getCvtCnySpac()).subtract(bailAmtCny);//业务敞口余额（扣减保证金）
                totalOrigiSpacSubtractBailAmt = (totalOrigiSpacSubtractBailAmt.add(accTfLoc.getOrigiOpenAmt().multiply(accTfLoc.getExchgRate()).subtract(bailAmtCny)));//业务敞口初始总金额（扣减保证金）
            }
            //低风险
            if(CmisCommonConstants.GUAR_MODE_21.equals(ctrTfLocCont.getGuarMode())||
                    CmisCommonConstants.GUAR_MODE_40.equals(ctrTfLocCont.getGuarMode())||
                    CmisCommonConstants.GUAR_MODE_60.equals(ctrTfLocCont.getGuarMode())){
                //一般合同
                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrTfLocCont.getContType())){
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalance);
                }else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacAmt);
                }
            }else{
                //一般合同
                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrTfLocCont.getContType())){
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalanceSubtractBailAmt);
                }else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacSubtractBailAmt);
                }
            }
        }else{
            canOutAccountAmt = contHighAmt;
        }
        return canOutAccountAmt;
    }

    /**
     * @方法名称: getSumContAmt
     * @方法描述: 根据额度编号查询合同金额总和
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BigDecimal getSumContAmt(String lmtAccNo) {
        return ctrTfLocContMapper.getSumContAmt(lmtAccNo);
    }
}
