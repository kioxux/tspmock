package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.dto.client.esb.com001.req.List;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuareInfo
 * @类描述: guare_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-16 15:29:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PfkInfosDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	private String serno;

	/** 评分卡信息 **/
	private ArrayList<PfkInfoDto> pfkInfoDtoList;

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public ArrayList<PfkInfoDto> getPfkInfoDtoList() {
		return pfkInfoDtoList;
	}

	public void setPfkInfoDtoList(ArrayList<PfkInfoDto> pfkInfoDtoList) {
		this.pfkInfoDtoList = pfkInfoDtoList;
	}
}