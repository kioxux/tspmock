package cn.com.yusys.yusp.web.server.xdtz0052;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0052.req.Xdtz0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.resp.Xdtz0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0052.Xdtz0052Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:根据借据号查询申请人行内还款（利息、本金）该笔借据次数
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0052:根据借据号查询申请人行内还款（利息、本金）该笔借据次数")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0052Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0052Resource.class);

    @Autowired
    private Xdtz0052Service xdtz0052Service;

    /**
     * 交易码：xdtz0052
     * 交易描述：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
     *
     * @param xdtz0052DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据号查询申请人行内还款（利息、本金）该笔借据次数")
    @PostMapping("/xdtz0052")
    protected @ResponseBody
    ResultDto<Xdtz0052DataRespDto> xdtz0052(@Validated @RequestBody Xdtz0052DataReqDto xdtz0052DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataReqDto));
        Xdtz0052DataRespDto xdtz0052DataRespDto = new Xdtz0052DataRespDto();// 响应Dto:根据借据号查询申请人行内还款（利息、本金）该笔借据次数
        ResultDto<Xdtz0052DataRespDto> xdtz0052DataResultDto = new ResultDto<>();
        String billNo = xdtz0052DataReqDto.getBillNo();//借据号
        try {
            if (StringUtil.isEmpty(billNo)) {
                xdtz0052DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0052DataResultDto.setMessage("借据号【billNo】不能为空！");
                return xdtz0052DataResultDto;
            }
            // 从xdtz0052DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataReqDto));
            xdtz0052DataRespDto = xdtz0052Service.xdtz0052(xdtz0052DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataRespDto));
            // 封装xdtz0052DataResultDto中正确的返回码和返回信息
            xdtz0052DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0052DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, e.getMessage());
            // 封装xdtz0052DataResultDto中异常返回码和返回信息
            xdtz0052DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0052DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0052DataRespDto到xdtz0052DataResultDto中
        xdtz0052DataResultDto.setData(xdtz0052DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataResultDto));
        return xdtz0052DataResultDto;
    }
}
