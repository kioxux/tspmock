package cn.com.yusys.yusp.web.server.xdzc0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0016.req.Xdzc0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.resp.Xdzc0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0016.Xdzc0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:购销合同查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0016:购销合同查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0016Resource.class);

    @Autowired
    private Xdzc0016Service xdzc0016Service;
  /**
     * 交易码：xdzc0016
     * 交易描述：购销合同查询
     *
     * @param xdzc0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("购销合同查询")
    @PostMapping("/xdzc0016")
    protected @ResponseBody
    ResultDto<Xdzc0016DataRespDto> xdzc0016(@Validated @RequestBody Xdzc0016DataReqDto xdzc0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016DataReqDto));
        Xdzc0016DataRespDto xdzc0016DataRespDto = new Xdzc0016DataRespDto();// 响应Dto:购销合同查询
        ResultDto<Xdzc0016DataRespDto> xdzc0016DataResultDto = new ResultDto<>();

        try {
            xdzc0016DataRespDto = xdzc0016Service.xdzc0016Service(xdzc0016DataReqDto);

            // TODO 封装xdzc0016DataRespDto对象结束
            // 封装xdzc0016DataResultDto中正确的返回码和返回信息
            xdzc0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, e.getMessage());
            // 封装xdzc0016DataResultDto中异常返回码和返回信息
            xdzc0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0016DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0016DataRespDto到xdzc0016DataResultDto中
        xdzc0016DataResultDto.setData(xdzc0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016DataResultDto));
        return xdzc0016DataResultDto;
    }
}
