package cn.com.yusys.yusp.web.server.xddh0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0003.req.Xddh0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0003.resp.List;
import cn.com.yusys.yusp.dto.server.xddh0003.resp.Xddh0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;

/**
 * 接口处理类:主动还款申请记录列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0003:主动还款申请记录列表查询")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0003Resource.class);

    /**
     * 交易码：xddh0003
     * 交易描述：主动还款申请记录列表查询
     *
     * @param xddh0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("主动还款申请记录列表查询")
    @PostMapping("/xddh0003")
    protected @ResponseBody
    ResultDto<Xddh0003DataRespDto> xddh0003(@Validated @RequestBody Xddh0003DataReqDto xddh0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003DataReqDto));
        Xddh0003DataRespDto xddh0003DataRespDto = new Xddh0003DataRespDto();// 响应Dto:主动还款申请记录列表查询
        ResultDto<Xddh0003DataRespDto> xddh0003DataResultDto = new ResultDto<>();
        String billNo = xddh0003DataReqDto.getBillNo();//借据编号
        String managerId = xddh0003DataReqDto.getManagerId();//主管客户经理
        try {
            // 从xddh0003DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0003DataRespDto对象开始
            List list = new List();
            list.setCusName(StringUtils.EMPTY);// 客户名称
            list.setContNo(StringUtils.EMPTY);// 合同号
            list.setCurType(StringUtils.EMPTY);// 币种
            list.setBillBal(new BigDecimal(0L));// 借据余额
            list.setStartDate(StringUtils.EMPTY);// 发生日期
            list.setEndDate(StringUtils.EMPTY);// 到期日期
            list.setCusId(StringUtils.EMPTY);// 客户号
            list.setFinaBrId(StringUtils.EMPTY);// 账务机构
            list.setAccountMgr(StringUtils.EMPTY);// 管户人
            list.setBizType(StringUtils.EMPTY);// 业务品种
            list.setSerno(StringUtils.EMPTY);// 交易流水号
            list.setApproveStatus(StringUtils.EMPTY);// 审批状态
            // TODO 封装xddh0003DataRespDto对象结束
            // 封装xddh0003DataResultDto中正确的返回码和返回信息
            xddh0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, e.getMessage());
            // 封装xddh0003DataResultDto中异常返回码和返回信息
            xddh0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0003DataRespDto到xddh0003DataResultDto中
        xddh0003DataResultDto.setData(xddh0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003DataResultDto));
        return xddh0003DataResultDto;
    }
}
