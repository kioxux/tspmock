/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RetailPrimeRateReplyInfo;
import cn.com.yusys.yusp.service.RetailPrimeRateReplyInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateReplyInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:21:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/retailprimeratereplyinfo")
public class RetailPrimeRateReplyInfoResource {
    @Autowired
    private RetailPrimeRateReplyInfoService retailPrimeRateReplyInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RetailPrimeRateReplyInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<RetailPrimeRateReplyInfo> list = retailPrimeRateReplyInfoService.selectAll(queryModel);
        return new ResultDto<List<RetailPrimeRateReplyInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RetailPrimeRateReplyInfo>> index(QueryModel queryModel) {
        List<RetailPrimeRateReplyInfo> list = retailPrimeRateReplyInfoService.selectByModel(queryModel);
        return new ResultDto<List<RetailPrimeRateReplyInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{replySerno}")
    protected ResultDto<RetailPrimeRateReplyInfo> show(@PathVariable("replySerno") String replySerno) {
        RetailPrimeRateReplyInfo retailPrimeRateReplyInfo = retailPrimeRateReplyInfoService.selectByPrimaryKey(replySerno);
        return new ResultDto<RetailPrimeRateReplyInfo>(retailPrimeRateReplyInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RetailPrimeRateReplyInfo> create(@RequestBody RetailPrimeRateReplyInfo retailPrimeRateReplyInfo) throws URISyntaxException {
        retailPrimeRateReplyInfoService.insert(retailPrimeRateReplyInfo);
        return new ResultDto<RetailPrimeRateReplyInfo>(retailPrimeRateReplyInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RetailPrimeRateReplyInfo retailPrimeRateReplyInfo) throws URISyntaxException {
        int result = retailPrimeRateReplyInfoService.update(retailPrimeRateReplyInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{replySerno}")
    protected ResultDto<Integer> delete(@PathVariable("replySerno") String replySerno) {
        int result = retailPrimeRateReplyInfoService.deleteByPrimaryKey(replySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = retailPrimeRateReplyInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @param retailPrimeRateReplyInfo
     * @return
     * @author wzy
     * @date 2021/9/15 11:02
     * @version 1.0.0
     * @desc  根据批复编号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyreplyserno")
    protected ResultDto<RetailPrimeRateReplyInfo> selectbyreplyserno(@RequestBody RetailPrimeRateReplyInfo retailPrimeRateReplyInfo) {
        RetailPrimeRateReplyInfo record = retailPrimeRateReplyInfoService.selectByPrimaryKey(retailPrimeRateReplyInfo.getReplySerno());
        return new ResultDto<RetailPrimeRateReplyInfo>(record);
    }
}
