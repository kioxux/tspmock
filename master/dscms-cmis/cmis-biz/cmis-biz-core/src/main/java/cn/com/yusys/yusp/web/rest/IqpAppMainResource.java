/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.IqpMainDiscDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.IqpAppMainService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpAppMainResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-03 15:23:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpappmain")
public class IqpAppMainResource {
    @Autowired
    private IqpAppMainService iqpAppMainService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAppMain>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAppMain> list = iqpAppMainService.selectAll(queryModel);
        return new ResultDto<List<IqpAppMain>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAppMain>> index(QueryModel queryModel) {
        List<IqpAppMain> list = iqpAppMainService.selectByModel(queryModel);
        return new ResultDto<List<IqpAppMain>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAppMain> show(@PathVariable("pkId") String pkId) {
        IqpAppMain iqpAppMain = iqpAppMainService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAppMain>(iqpAppMain);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAppMain> create(@RequestBody IqpAppMain iqpAppMain) throws URISyntaxException {
        iqpAppMainService.insert(iqpAppMain);
        return new ResultDto<IqpAppMain>(iqpAppMain);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAppMain iqpAppMain) throws URISyntaxException {
        int result = iqpAppMainService.update(iqpAppMain);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAppMainService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAppMainService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 贴现申请新增保存
     *
     *
     */
    @PostMapping("/saveiqpappdiscinfo")
    public ResultDto<Integer> saveIqpAppMainInfo(@RequestBody IqpMainDiscDto iqpMainDiscDto){

        int result = iqpAppMainService.insertAll(iqpMainDiscDto);

        return new ResultDto<Integer>(result);
    }

}
