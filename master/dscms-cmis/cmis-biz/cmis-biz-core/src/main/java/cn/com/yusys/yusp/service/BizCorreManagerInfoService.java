/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.BizCorreManagerInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.BizCorreManagerInfoMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: BizCorreManagerInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-18 20:20:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class BizCorreManagerInfoService {
    private static final Logger log = LoggerFactory.getLogger(BizCorreManagerInfoService.class);
    @Autowired
    private BizCorreManagerInfoMapper bizCorreManagerInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizCorreManagerInfo selectByPrimaryKey(String pkId) {
        return bizCorreManagerInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizCorreManagerInfo> selectAll(QueryModel model) {
        List<BizCorreManagerInfo> records = (List<BizCorreManagerInfo>) bizCorreManagerInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizCorreManagerInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizCorreManagerInfo> list = bizCorreManagerInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizCorreManagerInfo record) {
        return bizCorreManagerInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizCorreManagerInfo record) {
        return bizCorreManagerInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizCorreManagerInfo record) {
        return bizCorreManagerInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizCorreManagerInfo record) {
        return bizCorreManagerInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return bizCorreManagerInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return bizCorreManagerInfoMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     *
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return bizCorreManagerInfoMapper.updateByParams(delMap);
    }

    /**
     * 新增办理人员数据
     * 针对业务申请，主板人员需要控制只有一条
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveBizCorreManagerInfo(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.BIZ_CORRE_MAN_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.BIZ_CORRE_MAN_SUCCESS.value;//初始化信息
        try {
            String bizSerno = (String) params.get("bizSerno");
            String pageOpFlag = (String) params.get("pageOpFlag");
            if (StringUtils.isBlank(bizSerno) || StringUtils.isBlank(pageOpFlag)) {
                rtnCode = EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.key;
                rtnMsg = EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.value;
                return rtnData;
            }

            if (CmisBizConstants.CORRE_PAGE_OP_FLAG_INSERT.equals(pageOpFlag)) {
                //新增时，应不存在主办人员数据
                log.info("业务申请" + bizSerno + "新增办理人员-校验是否该人员是否已存在对应人员类型的数据");
                Map existsMap = new HashMap();
                existsMap.put("bizSerno", (String) params.get("bizSerno"));
                existsMap.put("correMgrType", (String) params.get("correMgrType"));
                List<BizCorreManagerInfo> bizCorreManagerInfoList = bizCorreManagerInfoMapper.selectBizCorreInfoByParams(existsMap);
                if (bizCorreManagerInfoList != null && bizCorreManagerInfoList.size() > 0) {
                    rtnCode = EcbEnum.BIZ_CORRE_MAN_ADD_EXISTS_EXP.key;
                    rtnMsg = EcbEnum.BIZ_CORRE_MAN_ADD_EXISTS_EXP.value;
                    return rtnData;
                }
                //新增时校验是否存在主办人记录
                log.info("业务申请" + bizSerno + "新增办理人员开始！");

                String correMgrType = (String) params.get("correMgrType");
                log.info("业务申请" + bizSerno + "新增办理人员-人员类型：" + correMgrType);

                if (CmisBizConstants.CORRE_REL_TYPE_MAIN.equals(correMgrType)) {
                    log.info("业务申请" + bizSerno + "新增办理人员-校验是否存在主办人员");
                    Map mainMap = new HashMap();
                    mainMap.put("bizSerno", (String) params.get("bizSerno"));
                    mainMap.put("correMgrType", correMgrType);
                    List<BizCorreManagerInfo> mainList = bizCorreManagerInfoMapper.selectBizCorreInfoByParams(mainMap);
                    if (mainList != null && mainList.size() > 0) {
                        rtnCode = EcbEnum.BIZ_CORRE_MAN_ADD_MAINEXISTS_EXP.key;
                        rtnMsg = EcbEnum.BIZ_CORRE_MAN_ADD_MAINEXISTS_EXP.value;
                        return rtnData;
                    }
                }

                log.info("业务申请" + bizSerno + "新增办理人员-保存数据");
                BizCorreManagerInfo bizCorreManagerInfo = JSONObject.parseObject(JSON.toJSONString(params), BizCorreManagerInfo.class);

                int insertCount = bizCorreManagerInfoMapper.insertSelective(bizCorreManagerInfo);
                if (insertCount < 0) {
                    throw new YuspException(EcbEnum.BIZ_CORRE_MAN_ADD_FAILED_EXP.key,
                            EcbEnum.BIZ_CORRE_MAN_ADD_FAILED_EXP.value);
                }
                log.info("业务申请" + bizSerno + "新增办理人员-保存数据成功！");
            } else {
                //修改时需要查询是否存在不等于当前申请的办理人员记录
                log.info("业务申请" + bizSerno + "修改办理人员-校验是否该人员是否已存在对应人员类型的数据");
                Map existsMap = new HashMap();
                existsMap.put("bizSerno", (String) params.get("bizSerno"));
                existsMap.put("correMgrType", (String) params.get("correMgrType"));
                existsMap.put("unEqualPkId", (String) params.get("pkId"));
                List<BizCorreManagerInfo> bizCorreManagerInfoList = bizCorreManagerInfoMapper.selectBizCorreInfoByParams(existsMap);
                if (bizCorreManagerInfoList != null && bizCorreManagerInfoList.size() > 0) {
                    rtnCode = EcbEnum.BIZ_CORRE_MAN_UPDATE_MAINEXISTS_EXP.key;
                    rtnMsg = EcbEnum.BIZ_CORRE_MAN_UPDATE_MAINEXISTS_EXP.value;
                    return rtnData;
                }

                log.info("业务申请" + bizSerno + "修改办理人员信息开始！");
                BizCorreManagerInfo bizCorreManagerInfo = JSONObject.parseObject(JSON.toJSONString(params), BizCorreManagerInfo.class);

                int updCount = bizCorreManagerInfoMapper.updateByPrimaryKey(bizCorreManagerInfo);
                if (updCount < 0) {
                    throw new YuspException(EcbEnum.BIZ_CORRE_MAN_ADD_FAILED_EXP.key, EcbEnum.BIZ_CORRE_MAN_ADD_FAILED_EXP.value);
                }
                log.info("业务申请" + bizSerno + "修改办理人员-保存数据成功！");
            }
        } catch (YuspException e) {
            log.error("新增办理人员信息异常", e);
            throw e;
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 通过业务流水号查询办理人员
     *
     * @param queryMap
     * @return
     */
    public List<BizCorreManagerInfo> selectBizCorreManagerInfoByBizSerno(Map queryMap) {
        return bizCorreManagerInfoMapper.selectBizCorreInfoByParams(queryMap);
    }

    /**
     * 批量添加办理人员数据
     *
     * @param bizCorreManagerInfoList
     * @return
     */
    public int insertForeach(List<BizCorreManagerInfo> bizCorreManagerInfoList) {
        return bizCorreManagerInfoMapper.insertForeach(bizCorreManagerInfoList);
    }
}

