/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailAccInfo
 * @类描述: bail_acc_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:23:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bail_acc_info")
public class BailAccInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 账户类型 **/
	@Column(name = "ACCOUNT_TYPE", unique = false, nullable = true, length = 40)
	private String accountType;
	
	/** 保证金账号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 40)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB", unique = false, nullable = true, length = 40)
	private String bailAccNoSub;
	
	/** 保证金账号名称 **/
	@Column(name = "BAIL_ACC_NAME", unique = false, nullable = true, length = 100)
	private String bailAccName;
	
	/** 保证金开户行号 **/
	@Column(name = "ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String acctsvcrNo;
	
	/** 保证金开户行名称 **/
	@Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 40)
	private String acctsvcrName;
	
	/** 保证金币种 **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_RATE", unique = false, nullable = true, length = 16,scale = 4)
	private java.math.BigDecimal bailRate;
	
	/** 保证金计息方式 **/
	@Column(name = "BAIL_INTEREST_MODE", unique = false, nullable = true, length = 5)
	private String bailInterestMode;
	
	/** 母户序号 **/
	@Column(name = "FIRST_ACCOUNT", unique = false, nullable = true, length = 40)
	private String firstAccount;
	
	/** 结算账号 **/
	@Column(name = "SETTL_ACCNO", unique = false, nullable = true, length = 40)
	private String settlAccno;
	
	/** 结算户名 **/
	@Column(name = "SETTL_ACCNAME", unique = false, nullable = true, length = 100)
	private String settlAccname;
	
	/** 结算账号子序号 **/
	@Column(name = "SETTL_ACCNO_SUB", unique = false, nullable = true, length = 40)
	private String settlAccnoSub;
	
	/** 待清算账号 **/
	@Column(name = "CLEAR_ACCNO", unique = false, nullable = true, length = 40)
	private String clearAccno;
	
	/** 待清算账户名 **/
	@Column(name = "CLEAR_ACCNAME", unique = false, nullable = true, length = 100)
	private String clearAccname;

	/** 待清算账号子序号 **/
	@Column(name = "CLEAR_ACCNO_SUB", unique = false, nullable = true, length = 40)
	private String clearAccnoSub;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 利率 **/
	@Column(name = "RATE", unique = false, nullable = true, length = 20)
	private String rate;
	
	/** 支付方式 **/
	@Column(name = "ZHFUTOJN", unique = false, nullable = true, length = 5)
	private String zhfutojn;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param accountType
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
    /**
     * @return accountType
     */
	public String getAccountType() {
		return this.accountType;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSub
	 */
	public void setBailAccNoSub(String bailAccNoSub) {
		this.bailAccNoSub = bailAccNoSub;
	}
	
    /**
     * @return bailAccNoSub
     */
	public String getBailAccNoSub() {
		return this.bailAccNoSub;
	}
	
	/**
	 * @param bailAccName
	 */
	public void setBailAccName(String bailAccName) {
		this.bailAccName = bailAccName;
	}
	
    /**
     * @return bailAccName
     */
	public String getBailAccName() {
		return this.bailAccName;
	}
	
	/**
	 * @param acctsvcrNo
	 */
	public void setAcctsvcrNo(String acctsvcrNo) {
		this.acctsvcrNo = acctsvcrNo;
	}
	
    /**
     * @return acctsvcrNo
     */
	public String getAcctsvcrNo() {
		return this.acctsvcrNo;
	}
	
	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}
	
    /**
     * @return acctsvcrName
     */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}
	
    /**
     * @return bailCurType
     */
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return bailRate
     */
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param bailInterestMode
	 */
	public void setBailInterestMode(String bailInterestMode) {
		this.bailInterestMode = bailInterestMode;
	}
	
    /**
     * @return bailInterestMode
     */
	public String getBailInterestMode() {
		return this.bailInterestMode;
	}
	
	/**
	 * @param firstAccount
	 */
	public void setFirstAccount(String firstAccount) {
		this.firstAccount = firstAccount;
	}
	
    /**
     * @return firstAccount
     */
	public String getFirstAccount() {
		return this.firstAccount;
	}
	
	/**
	 * @param settlAccno
	 */
	public void setSettlAccno(String settlAccno) {
		this.settlAccno = settlAccno;
	}
	
    /**
     * @return settlAccno
     */
	public String getSettlAccno() {
		return this.settlAccno;
	}
	
	/**
	 * @param settlAccname
	 */
	public void setSettlAccname(String settlAccname) {
		this.settlAccname = settlAccname;
	}
	
    /**
     * @return settlAccname
     */
	public String getSettlAccname() {
		return this.settlAccname;
	}
	
	/**
	 * @param settlAccnoSub
	 */
	public void setSettlAccnoSub(String settlAccnoSub) {
		this.settlAccnoSub = settlAccnoSub;
	}
	
    /**
     * @return settlAccnoSub
     */
	public String getSettlAccnoSub() {
		return this.settlAccnoSub;
	}
	
	/**
	 * @param clearAccno
	 */
	public void setClearAccno(String clearAccno) {
		this.clearAccno = clearAccno;
	}
	
    /**
     * @return clearAccno
     */
	public String getClearAccno() {
		return this.clearAccno;
	}
	
	/**
	 * @param clearAccname
	 */
	public void setClearAccname(String clearAccname) {
		this.clearAccname = clearAccname;
	}
	
    /**
     * @return clearAccname
     */
	public String getClearAccname() {
		return this.clearAccname;
	}

	/**
	 * @param clearAccnoSub
	 */
	public void setClearAccnoSub(String clearAccnoSub) {
		this.clearAccnoSub = clearAccnoSub;
	}

	/**
	 * @return clearAccnoSub
	 */
	public String getClearAccnoSub() {
		return this.clearAccnoSub;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}
	
    /**
     * @return rate
     */
	public String getRate() {
		return this.rate;
	}
	
	/**
	 * @param zhfutojn
	 */
	public void setZhfutojn(String zhfutojn) {
		this.zhfutojn = zhfutojn;
	}
	
    /**
     * @return zhfutojn
     */
	public String getZhfutojn() {
		return this.zhfutojn;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}