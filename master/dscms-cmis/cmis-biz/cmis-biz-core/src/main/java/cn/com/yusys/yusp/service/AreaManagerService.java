package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AreaManager;
import cn.com.yusys.yusp.repository.mapper.AreaManagerMapper;
import cn.com.yusys.yusp.repository.mapper.AreaOrgMapper;
import cn.com.yusys.yusp.repository.mapper.AreaUserMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaManagerService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zlf
 * @创建时间: 2021-05-11 16:47:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AreaManagerService {

    private static final Logger log = LoggerFactory.getLogger(AreaManagerService.class);
    @Autowired
    private AreaManagerMapper areaManagerMapper;
    @Autowired
    private AreaOrgMapper areaOrgMapper;
    @Autowired
    private AreaUserMapper areaUserMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public AreaManager selectByPrimaryKey(String areaNo) {
        return areaManagerMapper.selectByPrimaryKey(areaNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<AreaManager> selectAll(QueryModel model) {
        List<AreaManager> records = (List<AreaManager>) areaManagerMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AreaManager> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AreaManager> list = areaManagerMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(AreaManager record) {
        record.setAreaNo(StringUtils.uuid(true));
        return areaManagerMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(AreaManager record) {
        return areaManagerMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(AreaManager record) {
        return areaManagerMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(AreaManager record) {
        return areaManagerMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String areaNo) {
        return areaManagerMapper.deleteByPrimaryKey(areaNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return areaManagerMapper.deleteByIds(ids);
    }

    /**
     * @author zlf
     * @date 2021/5/26 14:29
     * @version 1.0.0
     * @desc 信息维护
     * @修改历史 修改时间 修改人员 修改原因
     */
    public int save(AreaManager record) {
        int result = 0;
        User userInfo = SessionUtils.getUserInformation();
        if (StringUtils.isEmpty(record.getAreaNo())) {
            record.setAreaNo(StringUtils.uuid(true));
            result = areaManagerMapper.insert(record);
        } else {
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            result = areaManagerMapper.updateByPrimaryKey(record);
        }
        return result;
    }


    @Transactional(readOnly = true)
    public List<AreaManager> selectExceptOther(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AreaManager> records = (List<AreaManager>) areaManagerMapper.selectExceptOther(model);
        PageHelper.clearPage();
        return records;
    }

    /**
     * @author zlf
     * @date 2021/6/3 9:37
     * @version 1.0.0
     * @desc 删除区域所有关联信息
     * @修改历史 修改时间 修改人员 修改原因
     */
    public ResultDto deleteAll(String areaNo) {
        int result = -1;
        try {
            if (!"".equals(areaNo)) {
                areaOrgMapper.deleteByAreaNo(areaNo);
                result = areaManagerMapper.deleteByPrimaryKey(areaNo);
                areaUserMapper.deleteByPrimaryAreaNo(areaNo);
                if (result < 1) {
                    return new ResultDto(null).code("0").message("删除失败");
                }
            }
        } catch (YuspException e) {
            log.error("删除区域所有关联信息失败..................");
            return new ResultDto(null).code("0").message("删除失败");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).code("0").message("删除失败");
        }
        return new ResultDto(null).code("1").message("删除成功");
    }
}
