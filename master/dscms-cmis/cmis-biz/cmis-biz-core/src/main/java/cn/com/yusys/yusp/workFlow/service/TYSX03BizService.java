package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChg;
import cn.com.yusys.yusp.domain.LmtSigInvestChgReply;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtIntbankReplyChgService;
import cn.com.yusys.yusp.service.LmtSigInvestChgReplyService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 资金业务授信批复变更审批申请流程
 * 作者：李召星
 */
@Service
public class TYSX03BizService implements ClientBizInterface {

    private final Logger logger = LoggerFactory.getLogger(TYSX03BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private LmtIntbankReplyChgService lmtIntbankReplyChgService;

    @Autowired
    private LmtSigInvestChgReplyService lmtSigInvestChgReplyService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();

        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        logger.info("后业务处理类型{}", currentOpType);
        //同业授信批复变更
        LmtIntbankReplyChg lmtIntbankReplyChg = null;
        //（主体授信、产品授信）批复变更
        LmtSigInvestChgReply lmtSigInvestChgReply = null;

        try {
            lmtIntbankReplyChg = lmtIntbankReplyChgService.selectBySerno(serno);
            lmtSigInvestChgReply = lmtSigInvestChgReplyService.selectBySerno(serno);

            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
                if (lmtIntbankReplyChg != null){
                    lmtIntbankReplyChgService.updateReplyChgStatus(lmtIntbankReplyChg,CmisBizConstants.APPLY_STATE_APP,currentUserId,currentOrgId);
                }
                if (lmtSigInvestChgReply != null){
                    lmtSigInvestChgReplyService.updateReplyChgStatus(lmtSigInvestChgReply,CmisBizConstants.APPLY_STATE_APP,currentUserId,currentOrgId);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                if (lmtIntbankReplyChg != null){
                    lmtIntbankReplyChgService.handleAfterEnd(lmtIntbankReplyChg,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_PASS);
                    //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
                    lmtIntbankReplyChgService.sendWbMsgNotice(lmtIntbankReplyChg,CmisBizConstants.STD_WB_NOTICE_TYPE_2,
                            resultInstanceDto.getComment().getUserComment(),resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"通过");
                }
                //TODO  代码待实现
                if (lmtSigInvestChgReply != null){
                    lmtSigInvestChgReplyService.handleAfterEnd(lmtSigInvestChgReply,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_PASS);
                    //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
                    lmtSigInvestChgReplyService.sendWbMsgNotice(lmtSigInvestChgReply,CmisBizConstants.STD_WB_NOTICE_TYPE_2,
                            resultInstanceDto.getComment().getUserComment(),resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"通过");
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedChgReplyBack(resultInstanceDto, lmtIntbankReplyChg, lmtSigInvestChgReply);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedChgReplyBack(resultInstanceDto, lmtIntbankReplyChg, lmtSigInvestChgReply);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                if (lmtIntbankReplyChg != null) {
                    lmtIntbankReplyChg.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                    lmtIntbankReplyChgService.update(lmtIntbankReplyChg);
                }
                if (lmtSigInvestChgReply != null){
                    lmtSigInvestChgReply.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                    lmtSigInvestChgReplyService.update(lmtSigInvestChgReply);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("资金业务授信批复变更审批申请启用【{}】，流程拒绝操作，流程参数【{}】", serno, resultInstanceDto);
                if (lmtIntbankReplyChg != null) {
                    lmtIntbankReplyChgService.handleAfterEnd(lmtIntbankReplyChg, currentUserId, currentOrgId, CmisBizConstants.APPLY_STATE_REFUSE);
                    //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
                    lmtIntbankReplyChgService.sendWbMsgNotice(lmtIntbankReplyChg,CmisBizConstants.STD_WB_NOTICE_TYPE_3,
                            null,resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"否决");
                }
                if (lmtSigInvestChgReply != null){
                    lmtSigInvestChgReplyService.handleAfterEnd(lmtSigInvestChgReply, currentUserId, currentOrgId, CmisBizConstants.APPLY_STATE_REFUSE);
                    //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
                    lmtSigInvestChgReplyService.sendWbMsgNotice(lmtSigInvestChgReply,CmisBizConstants.STD_WB_NOTICE_TYPE_3,
                            null,resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"否决");
                }

            } else {
                logger.warn("资金业务授信批复变更审批申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("资金业务授信批复变更审批申请审批后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: extractedChgReplyBack
     * @方法描述:  资金投行批复授信变更打回或退回业务处理
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/1 15:38
     * @param resultInstanceDto: 
     * @param lmtIntbankReplyChg: 
     * @param lmtSigInvestChgReply: 
     * @return: void
     * @算法描述: 无
    */
    private void extractedChgReplyBack(ResultInstanceDto resultInstanceDto, LmtIntbankReplyChg lmtIntbankReplyChg, LmtSigInvestChgReply lmtSigInvestChgReply) throws Exception {
        logger.info("资金同业批复变更【{}】流程，打回或退回业务处理...", lmtSigInvestChgReply.getSerno());
        //针对流程到办结节点，进行以下处理
        if (lmtIntbankReplyChg != null) {
            lmtIntbankReplyChg.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
            lmtIntbankReplyChgService.update(lmtIntbankReplyChg);
            //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
            lmtIntbankReplyChgService.sendWbMsgNotice(lmtIntbankReplyChg,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                    resultInstanceDto.getComment().getUserComment(),resultInstanceDto.getCurrentUserId(),resultInstanceDto.getCurrentOrgId(),"打回");
        }
        if (lmtSigInvestChgReply != null){
            lmtSigInvestChgReply.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
            lmtSigInvestChgReplyService.update(lmtSigInvestChgReply);
            //推送首页提醒事项 add by lizx 20210701 流程审批通过，推送
            lmtSigInvestChgReplyService.sendWbMsgNotice(lmtSigInvestChgReply,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                    resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId(),"打回");
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.TYSX03).equals(flowCode);
    }
}
