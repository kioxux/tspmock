/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPartnerLstInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerLstInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerLstInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 09:38:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerLstInfoService {

    @Autowired
    private CoopPartnerLstInfoMapper coopPartnerLstInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPartnerLstInfo selectByPrimaryKey(String partnerNo) {
        return coopPartnerLstInfoMapper.selectByPrimaryKey(partnerNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPartnerLstInfo> selectAll(QueryModel model) {
        List<CoopPartnerLstInfo> records = (List<CoopPartnerLstInfo>) coopPartnerLstInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPartnerLstInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCoopPartnerInfo
     * @方法描述: 条件查询 - 查询进行分页,不适用管护条件
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopPartnerLstInfo> queryCoopPartnerInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoMapper.queryCoopPartnerInfo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopPartnerLstInfo record) {
        return coopPartnerLstInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPartnerLstInfo record) {
        return coopPartnerLstInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopPartnerLstInfo record) {
        return coopPartnerLstInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPartnerLstInfo record) {
        return coopPartnerLstInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String partnerNo) {
        return coopPartnerLstInfoMapper.deleteByPrimaryKey(partnerNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPartnerLstInfoMapper.deleteByIds(ids);
    }
}
