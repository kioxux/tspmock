package cn.com.yusys.yusp.web.server.xdht0028;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0028.req.Xdht0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0028.resp.Xdht0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0028.Xdht0028Service;
import cn.com.yusys.yusp.service.server.xdtz0017.Xdtz0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据合同号取得客户经理电话号码
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0028:根据合同号取得客户经理电话号码")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0028Resource.class);
    @Autowired
    private Xdht0028Service xdht0028Service;

    /**
     * 交易码：xdht0028
     * 交易描述：根据合同号取得客户经理电话号码
     *
     * @param xdht0028DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据合同号取得客户经理电话号码")
    @PostMapping("/xdht0028")
    protected @ResponseBody
    ResultDto<Xdht0028DataRespDto> xdht0028(@Validated @RequestBody Xdht0028DataReqDto xdht0028DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataReqDto));
        Xdht0028DataRespDto xdht0028DataRespDto = new Xdht0028DataRespDto();// 响应Dto:根据合同号取得客户经理电话号码
        ResultDto<Xdht0028DataRespDto> xdht0028DataResultDto = new ResultDto<>();
        try {
            String contNo = xdht0028DataReqDto.getContNo();//合同号
            // 从xdht0028DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataReqDto));
            xdht0028DataRespDto = xdht0028Service.xdht0028(xdht0028DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataRespDto));
            // 封装xdht0028DataResultDto中正确的返回码和返回信息
            xdht0028DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0028DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, e.getMessage());
            // 封装xdht0028DataResultDto中异常返回码和返回信息
            xdht0028DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0028DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0028DataRespDto到xdht0028DataResultDto中
        xdht0028DataResultDto.setData(xdht0028DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataRespDto));
        return xdht0028DataResultDto;
    }
}
