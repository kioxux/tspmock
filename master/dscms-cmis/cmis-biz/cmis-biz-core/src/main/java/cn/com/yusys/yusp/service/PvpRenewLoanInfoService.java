/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpRenewLoanInfo;
import cn.com.yusys.yusp.repository.mapper.PvpRenewLoanInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRenewLoanInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpRenewLoanInfoService {

    @Autowired
    private PvpRenewLoanInfoMapper pvpRenewLoanInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpRenewLoanInfo selectByPrimaryKey(String pkId, String oldBillNo) {
        return pvpRenewLoanInfoMapper.selectByPrimaryKey(pkId, oldBillNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpRenewLoanInfo> selectAll(QueryModel model) {
        List<PvpRenewLoanInfo> records = (List<PvpRenewLoanInfo>) pvpRenewLoanInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpRenewLoanInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpRenewLoanInfo record) {
        return pvpRenewLoanInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpRenewLoanInfo record) {
        return pvpRenewLoanInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpRenewLoanInfo record) {
        return pvpRenewLoanInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpRenewLoanInfo record) {
        return pvpRenewLoanInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String oldBillNo) {
        return pvpRenewLoanInfoMapper.deleteByPrimaryKey(pkId, oldBillNo);
    }

    /**
     * @方法名称: selectBySecondLoanNo
     * @方法描述: 根据secondBillNo查询数目
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String selectBySecondLoanNo(String secondBillNo) {
        return pvpRenewLoanInfoMapper.selectBySecondLoanNo(secondBillNo);
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpRenewLoanInfo> selectByParams(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("inputDate desc");
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoMapper.selectByParams(model);
        PageHelper.clearPage();
        return list;
    }

}
