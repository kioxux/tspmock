package cn.com.yusys.yusp.web.server.cmisbiz0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.req.CmisBiz0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.resp.CmisBiz0002RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:集团客户授信复审
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "CMISBIZ0002:集团客户授信复审")
@RestController
@RequestMapping("/api/biz4inner")
public class CmisBiz0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0002Resource.class);
    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    /**
     * 交易码：cmisbiz0002
     * 交易描述：集团客户授信复审
     *
     * @param cmisbiz0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("集团客户授信复审")
    @PostMapping("/cmisbiz0002")
    protected @ResponseBody
    ResultDto<CmisBiz0002RespDto> cmisbiz0002(@Validated @RequestBody CmisBiz0002ReqDto cmisbiz0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisbiz0002ReqDto));
        CmisBiz0002RespDto cmisbiz0002RespDto = new CmisBiz0002RespDto();// 响应Dto:集团客户授信复审
        ResultDto<CmisBiz0002RespDto> cmisbiz0002ResultDto = new ResultDto<>();
        try {
            LmtGrpApp lmtGrpApp = new LmtGrpApp();
            BeanUtils.copyProperties(cmisbiz0002ReqDto, lmtGrpApp);

            // 从cmisbiz0002ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisbiz0002ReqDto));
            String result = lmtGrpAppService.guideSave(lmtGrpApp);
            CmisBiz0002RespDto cmisBiz0002RespDto = new CmisBiz0002RespDto();
            cmisBiz0002RespDto.setResult(result);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisbiz0002RespDto));
            // 封装cmisbiz0002ResultDto中正确的返回码和返回信息
            cmisbiz0002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbiz0002ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, e.getMessage());
            // 封装cmisbiz0002ResultDto中异常返回码和返回信息
            cmisbiz0002ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbiz0002ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbiz0002RespDto到cmisbiz0002ResultDto中
        cmisbiz0002ResultDto.setData(cmisbiz0002RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisbiz0002ResultDto));
        return cmisbiz0002ResultDto;
    }
}