package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.yusys.yusp.domain.RptLmtRepayAnys;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class RiskItem0084Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0084Service.class);

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Resource
    private ICusClientService iCusClientService;//注入客户服务接口

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private RptBasicInfoAssoService rptBasicInfoAssoService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private RptSpdAnysSxkdService rptSpdAnysSxkdService;

    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;

    @Autowired
    private  ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private RptLmtRepayAnysService rptLmtRepayAnysService;
    /**
     * @方法名称: riskItem0084
     * @方法描述: 省心快贷自动化审批校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批校验：
     * @创建人: yuanfs
     * @创建时间: 2021-08-4
     * @修改记录: 修改时间    修改人员    修改原因
     * @修改记录:2021/08/31 zhangliang15
     */
    public RiskResultDto riskItem0084(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
//        // 是否省心快贷
//        String type = queryModel.getCondition().get("type").toString();
        log.info("省心快贷自动化审批要素校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 如果不是提交自动化审批，不校验
        if (!Objects.equals("1", lmtApp.getIsSubAutoAppr())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }

        // 省心快贷自动化审批要素校验
        riskResultDto = riskItem008401(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 省心快贷自动化审批单一上报校验
        riskResultDto = riskItem008402(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
//        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
//            return riskResultDto;
//        }
        // 省心快贷自动化审批抵押物校验
        riskResultDto = riskItem008403(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }

        // 省心快贷自动化审批贷款主体校验
        riskResultDto = riskItem008404(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }

        // 省心快贷自动化审批征信引入校验
        riskResultDto = riskItem008405(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 省心快贷自动化审批评级校验
        riskResultDto = riskItem008406(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 省心快贷自动化审批公司财报校验
        riskResultDto = riskItem008407(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 省心快贷评分卡校验
        riskResultDto = riskItem008408(lmtApp);
        if((CmisRiskConstants.RISK_RESULT_TYPE_1).equals(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        log.info("省心快贷自动化审批要素校验结束*******************业务流水号：【{}】", serno);
        // 更新授信申请表省心快贷风险拦截结果 1成功0失败
        lmtApp.setSxkdRiskResult("1");
        log.info("更细授信申请表省心快贷风险拦截结果****************业务流水号：【{}】", serno);
        lmtAppService.updateBySerno(lmtApp);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0084
     * @方法描述: 省心快贷自动化审批要素校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批要素校验：
     * （1）省心快贷自动化审批仅支持企业；
     * （2）省心快贷自动化审批仅支持申请额度小于等于500万；
     * （3）省心快贷自动化审批仅支持授信3年以内（含）；
     * （4）4、省心快贷自动化审批无宽限期
     * @创建人: yuanfs
     * @创建时间: 2021-08-4
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008401(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        // 是否省心快贷
        log.info("省心快贷自动化审批要素校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 获取客户模块中的客户基本信息
            ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                return riskResultDto;
            }
            CusBaseDto cusBaseDtoDo = resultDto.getData();
            String cusCatalog = cusBaseDtoDo.getCusCatalog();
            // 借款人必须是企业（客户大类 2-对公 1-对私）
            if (!Objects.equals("2", cusCatalog)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840101);
                return riskResultDto;
            }
            // 省心快贷自动化审批仅支持授信3年以内（含）；
            if (36 < lmtApp.getLmtTerm()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840103);
                return riskResultDto;
            }

            // 省心快贷自动化审批无宽限期
            if (0 != lmtApp.getLmtGraperTerm()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840104);
                return riskResultDto;
            }
            log.info("省心快贷自动化审批要素校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem0082
     * @方法描述: 省心快贷自动化审批单一上报校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批要素校验：
     * （1）上报授信仅包含：单一的1条省心快贷录入，且为循环额度，担保方式录入为抵押；
     *     注：判断上报授信中仅包含单一一条省心快贷授信分项，存在以下场景：
     *      （1.1）只存在一条省心快贷的分项信息，分项中也仅录入了一个产品品种（授信品种为流动资金贷款，短期和中长期均可；品种类型属性为省心快贷；是否循环额度为是；担保方式为抵押），满足省心快贷自动化审批的条件；
     *      （1.2）存在多条分项信息，其中不属于省心快贷的分项信息（分项未被删除的，分项授信额度必须为0，相当于该条授信是删除了的），
     *             属于省心快贷的分项信息，分项中也仅录入了一个产品品种（授信品种为流动资金贷款，短期和中长期均可；品种类型属性为省心快贷；是否循环额度为是；担保方式为抵押）；
     * （2）授信品种为流动资金贷款；
     * （3）无其他任何临时、低风险、非抵押、非流贷类上报授信
     * @创建人: yuanfs
     * @创建时间: 2021-08-5
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008402(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷自动化审批单一上报校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
            Map paramsSub = new HashMap();
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramsSub.put("lmtAmt", "0");
            List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
            if (Objects.isNull(subList) || subList.isEmpty()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                return riskResultDto;
            }
            // 省心快贷必须只有一个授信品种
            if (CollectionUtils.nonEmpty(subList)) {
                // 只存在一条省心快贷的分项信息，分项中也仅录入了一个产品品种（授信品种为流动资金贷款，短期和中长期均可；品种类型属性为省心快贷；是否循环额度为是；担保方式为抵押），满足省心快贷自动化审批的条件；
                if (subList.size() == 1) {
                    LmtAppSub lmtAppSub = subList.get(0);
                    // 是否循环额度
                    if (!Objects.equals("1", lmtAppSub.getIsRevolvLimit())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840202);
                        return riskResultDto;
                    }
                    // 担保方式录入为抵押
                    if (!Objects.equals(CmisCommonConstants.GUAR_MODE_10, lmtAppSub.getGuarMode())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840203);
                        return riskResultDto;
                    }
                    // 2、省心快贷自动化审批仅支持申请额度小于等于500万；
                    if (new BigDecimal("5000000").compareTo(lmtAppSub.getLmtAmt()) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840102);
                        return riskResultDto;
                    }
                    Map paramsSubPrd = new HashMap();
                    // 省心快贷
                    paramsSubPrd.put("subSerno", subList.get(0).getSubSerno());
                    paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                    if (CollectionUtils.nonEmpty(subListPrd) && subListPrd.size() == 1) {
                        LmtAppSubPrd lmtAppSubPrd = subListPrd.get(0);
                        // 1001:授信品种为流动资金贷款（短期流动资金贷款、中长期流动资金贷款）
                        if (!Objects.equals("10010101", lmtAppSubPrd.getLmtBizType()) && !Objects.equals("10010102", lmtAppSubPrd.getLmtBizType())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840204);
                            return riskResultDto;
                        }
                        // P011:省心快贷
                        if (!Objects.equals("P011", lmtAppSubPrd.getLmtBizTypeProp())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840206);
                            return riskResultDto;
                        }
    //                // 省心快贷自动化审批无宽限期
    //                if (0 != lmtAppSubPrd.getLmtGraperTerm()) {
    //                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
    //                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840104);
    //                    return riskResultDto;
    //                }
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840205);
                        return riskResultDto;
                    }
               // 存在多条分项信息，其中不属于省心快贷的分项信息（分项未被删除的，分项授信额度必须为0，相当于该条授信是删除了的），
                    // 属于省心快贷的分项信息，分项中也仅录入了一个产品品种（授信品种为流动资金贷款，短期和中长期均可；品种类型属性为省心快贷；是否循环额度为是；担保方式为抵押）；
                } else if (subList.size() > 1){
                    boolean isExitSxkdFlag = false; //判断是否存在省心快贷分项标识
                    for (LmtAppSub lmtAppSub : subList){
                        boolean isSxkdFlag = false; //判断是否为省心快贷标识
                        Map paramsSubPrd = new HashMap();
                        // 获取分项产品明细
                        paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
                        paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                        List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                        for (LmtAppSubPrd lmtAppSubPrd : subListPrd){
                            // P011:省心快贷
                            if (Objects.equals("P011", lmtAppSubPrd.getLmtBizTypeProp())) {
                                isSxkdFlag = true;
                                isExitSxkdFlag = true;
                            } else {
                                isSxkdFlag = false;
                            }
                        }
                        if(!isSxkdFlag){
                            // 若不为省心快贷，则授信分项金额不能大于0；
                            if (new BigDecimal("0").compareTo(lmtAppSub.getLmtAmt()) < 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840207);
                                return riskResultDto;
                            }
                        } else {
                            // 属于省心快贷的分项信息，分项中也仅录入了一个产品品种（授信品种为流动资金贷款，短期和中长期均可；品种类型属性为省心快贷；是否循环额度为是；担保方式为抵押）；
                            // 是否循环额度
                            if (!Objects.equals("1", lmtAppSub.getIsRevolvLimit())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840202);
                                return riskResultDto;
                            }
                            // 担保方式录入为抵押
                            if (!Objects.equals(CmisCommonConstants.GUAR_MODE_10, lmtAppSub.getGuarMode())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840203);
                                return riskResultDto;
                            }
                            // 2、省心快贷自动化审批仅支持申请额度小于等于500万；
                            if (new BigDecimal("5000000").compareTo(lmtAppSub.getLmtAmt()) < 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840102);
                                return riskResultDto;
                            }

                            if (CollectionUtils.nonEmpty(subListPrd) && subListPrd.size() == 1) {
                                LmtAppSubPrd lmtAppSubPrd = subListPrd.get(0);
                                // 1001:授信品种为流动资金贷款（短期流动资金贷款、中长期流动资金贷款）
                                if (!Objects.equals("10010101", lmtAppSubPrd.getLmtBizType()) && !Objects.equals("10010102", lmtAppSubPrd.getLmtBizType())) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840204);
                                    return riskResultDto;
                                }
                                // P011:省心快贷
                                if (!Objects.equals("P011", lmtAppSubPrd.getLmtBizTypeProp())) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840206);
                                    return riskResultDto;
                                }
                            } else {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840205);
                                return riskResultDto;
                            }
                        }
                    }
                    if(!isExitSxkdFlag){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840206);
                        return riskResultDto;
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840201);
                return riskResultDto;
            }
            log.info("省心快贷自动化审批单一上报校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem008403
     * @方法描述: 省心快贷自动化审批抵押物校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批抵押物校验：
     * （1）引入抵押物或调查报告中引入抵押物，是否顺位抵押标识为否
     * （2）引入抵押物类型在准入范围内
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008403(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷自动化审批抵押物校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
            Map paramsSub = new HashMap();
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
            if (Objects.isNull(subList) || subList.isEmpty()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                return riskResultDto;
            }
            // 省心快贷必须只有一个授信品种
            if (CollectionUtils.nonEmpty(subList)) {
                for (LmtAppSub lmtAppSub : subList) {
                    boolean isSxkdFlag = false; //判断是否为省心快贷标识
                    Map paramsSubPrd = new HashMap();
                    // 获取分项产品明细
                    paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
                    paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                    for (LmtAppSubPrd lmtAppSubPrd : subListPrd) {
                        // P011:省心快贷
                        if (Objects.equals("P011", lmtAppSubPrd.getLmtBizTypeProp())) {
                            isSxkdFlag = true;
                        } else {
                            isSxkdFlag = false;
                        }
                    }
                    if(isSxkdFlag){
                        // 引入抵押物
                        QueryModel model = new QueryModel();
                        model.addCondition("serno", serno);
                        List<RptLmtRepayAnysGuarPldDetail> list = rptLmtRepayAnysGuarPldDetailService.selectAll(model);
                        if (list != null && list.size() != 0) {
                            for (RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail : list) {
                                // 抵押顺位: 0:一抵, 1:二抵
                                if (!Objects.equals("0", rptLmtRepayAnysGuarPldDetail.getIsPldOrder())) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840301);
                                    return riskResultDto;
                                }
                                // 引入抵押物类型在准入范围内
                                String guarTypeCd = rptLmtRepayAnysGuarPldDetail.getGuarTypeCd();
                                // 根据抵押物担保分类代码判断抵押物类型是否在准入范围内
                                ResultDto<CfgSxkdGuarDiscountDto> cfgSxkdGuarDiscountDto = iCmisCfgClientService.selectCfgSxkdGuarDiscountByGuarTypeCd(guarTypeCd);
                                // 若查询结果为空，则该抵押物类型不在准入范围内
                                if (Objects.isNull(cfgSxkdGuarDiscountDto) || Objects.isNull(cfgSxkdGuarDiscountDto.getData())) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840302);
                                    return riskResultDto;
                                }
                            }
                        } else {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840303);
                            return riskResultDto;
                        }
                    }
                }
            }
            log.info("省心快贷自动化审批抵押物校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem008404
     * @方法描述: 省心快贷自动化审批贷款主体校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批贷款主体校验
     * （1）对公客户信息-高管人员信息模块，校验：录入的法人代表和实际控制人必须是同一个人
     * （2）高管人员信息表中法人代表婚姻情况，校验：
     * 1、已婚类型（字典项：已婚、初婚、再婚、复婚，以新信贷字典项为准），校验社会关系信息中，必须包含配偶数据；
     * 2、未婚类型（字典项：单身、丧偶、离婚、未说明的婚姻情况），不校验
     * （3）个人客户信息，校验法人代表信息，手机号码不得为空
     * （4）个人客户信息，如果有配偶校验配偶信息手机号码不得为空
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008404(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷自动化审批贷款主体校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 获取客户模块中的客户基本信息
            ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                return riskResultDto;
            }
            CusBaseDto cusBaseDtoDo = resultDto.getData();
            String cusCatalog = cusBaseDtoDo.getCusCatalog();
            // 借款人必须是企业（客户大类 2-对公 1-对私）
            if (!Objects.equals("2", cusCatalog)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840101);
                return riskResultDto;
            }
            //获取实际控制人
            Map<String, Object> map1 = new HashMap<>();
            map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
            map1.put("cusIdRel", lmtApp.getCusId());
            ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType = cmisCusClientService.getCusCorpMgrByParams(map1);
            if (Objects.isNull(cusCorpMgrByMrgType) || Objects.isNull(cusCorpMgrByMrgType.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840402);
                return riskResultDto;
            }
            //获取法人代表
            Map<String, Object> map2 = new HashMap<>();
            map2.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
            map2.put("cusIdRel", lmtApp.getCusId());
            ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType1 = cmisCusClientService.getCusCorpMgrByParams(map2);
            if (Objects.isNull(cusCorpMgrByMrgType1) || Objects.isNull(cusCorpMgrByMrgType1.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840401);
                return riskResultDto;
            }
            if (!Objects.equals(cusCorpMgrByMrgType.getData().getMrgCertCode(), cusCorpMgrByMrgType1.getData().getMrgCertCode())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840504);
                return riskResultDto;
            }
            // 根据证件号获取客户基本信息
            CusBaseClientDto cusInfoDto = cmisCusClientService.queryCusByCertCode(cusCorpMgrByMrgType1.getData().getMrgCertCode());

    //        // 获取客户信息
    //        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusCorpMgrByMrgType1.getData().getCusId());
            CusIndivDto cusIndivDto = null;
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusInfoDto.getCusCatalog())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840403);
                return riskResultDto;
            } else {
                cusIndivDto = cmisCusClientService.queryCusindivByCusid(cusInfoDto.getCusId()).getData();
            }
            if (Objects.isNull(cusIndivDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840404);
                return riskResultDto;
            }
            // 判断法人代表婚姻情况
            // 婚姻状况 已婚 :20
            if (Objects.equals("20", cusIndivDto.getMarStatus())) {
                //根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号
                ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(cusCorpMgrByMrgType1.getData().getMrgCertCode());
                if (Objects.isNull(cmisCus0013RespDtoResultDto) || Objects.isNull(cmisCus0013RespDtoResultDto.getData())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840405);
                    return riskResultDto;
                }
                // 获取客户的配偶联系信息
                CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(cmisCus0013RespDtoResultDto.getData().getCusId());
                if (Objects.isNull(cusIndivContactDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840408);
                    return riskResultDto;
                }
                // 个人客户信息，校验法人代表信息，手机号码不得为空
                if (StringUtils.isBlank(cusIndivContactDto.getMobileNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840409);
                    return riskResultDto;
                }
            }

            // 获取客户的联系信息
            CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(cusIndivDto.getCusId());
            if (Objects.isNull(cusIndivContactDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840406);
                return riskResultDto;
            }
            // 个人客户信息，校验法人代表信息，手机号码不得为空
            if (StringUtils.isBlank(cusIndivContactDto.getMobileNo())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840407);
                return riskResultDto;
            }

            log.info("省心快贷自动化审批贷款主体校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem0085
     * @方法描述: 省心快贷自动化审批征信引入校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批征信引入校验
     * （1）信贷系统已发起企业征信查询，且为一个月内有效征信查询
     * （2）信贷系统已发起法人代表征信查询，且为一个月内有效征信查询
     * （3）如果存在配偶，信贷系统已发起法人代表配偶征信查询，且为一个月内有效征信查询
     * （4）征信分析中引入征信校验：
     * 法人和企业征信必有；
     * 若存在配偶，配偶必有
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008405(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷自动化审批征信引入校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 获取客户模块中的客户基本信息
            ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                return riskResultDto;
            }
            CusBaseDto cusBaseDtoDo = resultDto.getData();
            String cusCatalog = cusBaseDtoDo.getCusCatalog();
            // 借款人必须是企业（客户大类 2-对公 1-对私）
            if (!Objects.equals("2", cusCatalog)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840101);
                return riskResultDto;
            }
    //        // 根据证件号去查询
    //        QueryModel model = new QueryModel();
    //        model.addCondition("certCode", cusBaseDtoDo.getCertCode());
    //        // 1，代表企业查询
    //        model.addCondition("qryCls", "1");
    //        // 01:贷前审查（企业）
    //        model.addCondition("qryResn", "01");
    //        // 取得所有的企业征信查询
    //        List<CreditReportQryLst> list = creditReportQryLstService.selectCreditByCertCode(model);
            // 根据流水号获取引入的征信信息
            boolean reportFlag = false;
            QueryModel model = new QueryModel();
            model.addCondition("bizSerno",serno);
            List<CreditReportQryLstDto> list = creditReportQryLstService.selectCreditReportQryLstByCrqlSerno(model);
            if(CollectionUtils.isEmpty(list)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09201);
                return riskResultDto;
            }

            if (list != null && list.size() > 0) {
                for (CreditReportQryLstDto creditReportQryLst: list) {
                    //判断是否一个月内
                    String reportCreateTime = creditReportQryLst.getReportCreateTime();
                    if (StringUtils.nonBlank(reportCreateTime)) {
                        if (DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(reportCreateTime, "yyyy-MM-dd"), 1), DateUtils.getCurrDate()) >= 0) {
                            reportFlag = true;
                            break;
                        }
                    }
                }
            }
            if (!reportFlag) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840501);
                return riskResultDto;
            }

            //获取法人代表
            Map<String, Object> map2 = new HashMap<>();
            map2.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
            map2.put("cusIdRel", lmtApp.getCusId());
            ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType1 = cmisCusClientService.getCusCorpMgrByParams(map2);
            if (Objects.isNull(cusCorpMgrByMrgType1) || Objects.isNull(cusCorpMgrByMrgType1.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840401);
                return riskResultDto;
            }

            //法人和配偶征信关系
            //与主借款人关系：共同借款人，查询原因：贷款审批    或者
            //与主借款人关系：担保人，查询原因：担保资格审查    或者
            //与主借款人关系：法人代表、出资人及关联人等，查询原因：法人代表、出资人及关联人等资信审查(22)
            // 根据证件号去查询
            QueryModel model1 = new QueryModel();
            model1.addCondition("certCode", cusCorpMgrByMrgType1.getData().getMrgCertCode());
            // 0，代表个人征信查询
            model1.addCondition("qryCls", "0");
            // 22:法人代表、出资人及关联人等资信审查(22)
            model1.addCondition("qryResn", "22");
            reportFlag = false;
            // 取得所有的法人征信查询
            List<CreditReportQryLst> list1 = creditReportQryLstService.selectCreditByCertCode(model1);
            if (list1 != null && list1.size() > 0) {
                for (CreditReportQryLst creditReportQryLst : list1) {
                    //判断是否一个月内
                    String reportCreateTime = creditReportQryLst.getReportCreateTime();
                    if (StringUtils.nonBlank(reportCreateTime)) {
                        if (DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(reportCreateTime, "yyyy-MM-dd"), 1), DateUtils.getCurrDate()) >= 0) {
                            reportFlag = true;
                            break;
                        }
                    }
                }
            }
            // 信贷系统已发起法人代表征信查询，且为一个月内有效征信查询
            if (!reportFlag) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840502);
                return riskResultDto;
            }

            // 获取法人客户信息
            CusBaseClientDto cusBaseClientDto = cmisCusClientService.queryCusByCertCode(cusCorpMgrByMrgType1.getData().getMrgCertCode());
            CusIndivDto cusIndivDto = null;
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840403);
                return riskResultDto;
            } else {
                cusIndivDto = cmisCusClientService.queryCusindivByCusid(cusBaseClientDto.getCusId()).getData();
            }
            if (Objects.isNull(cusIndivDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840404);
                return riskResultDto;
            }
            // 判断法人代表婚姻情况
            // 婚姻状况 已婚 :20
            if (Objects.equals("20", cusIndivDto.getMarStatus())) {
                //根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号
                ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(cusCorpMgrByMrgType1.getData().getMrgCertCode());
                if (Objects.isNull(cmisCus0013RespDtoResultDto) || Objects.isNull(cmisCus0013RespDtoResultDto.getData())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840405);
                    return riskResultDto;
                }
                //法人和配偶征信关系
                //与主借款人关系：共同借款人，查询原因：贷款审批    或者
                //与主借款人关系：担保人，查询原因：担保资格审查    或者
                //与主借款人关系：法人代表、出资人及关联人等，查询原因：法人代表、出资人及关联人等资信审查(22)
                // 根据证件号去查询
                // 获取法人客户信息
                CusBaseClientDto cusBaseMateClientDto = iCusClientService.queryCus(cmisCus0013RespDtoResultDto.getData().getCusId());
                if(Objects.isNull(cusBaseClientDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840505);
                    return riskResultDto;
                }
                QueryModel model2 = new QueryModel();
                model2.addCondition("certCode", cusBaseMateClientDto.getCertCode());
                // 0，代表个人征信查询
                model2.addCondition("qryCls", "0");
                // 22:法人代表、出资人及关联人等资信审查(22)
    //            model2.addCondition("qryResn", "22");
                reportFlag = false;
                // 取得所有的法人配偶征信查询
                List<CreditReportQryLst> list2 = creditReportQryLstService.selectCreditByCertCode(model2);
                if (list2 != null && list2.size() > 0) {
                    for (CreditReportQryLst creditReportQryLst : list2) {
                        //判断是否一个月内
                        String reportCreateTime = creditReportQryLst.getReportCreateTime();
                        if (StringUtils.nonBlank(reportCreateTime)) {
                            if (DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(reportCreateTime, "yyyy-MM-dd"), 1), DateUtils.getCurrDate()) >= 0) {
                                reportFlag = true;
                                break;
                            }
                        }
                    }
                }
                // 信贷系统已发起法人代表配偶征信查询，且为一个月内有效征信查询
                if (!reportFlag) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840503);
                    return riskResultDto;
                }
            }

            log.info("省心快贷自动化审批征信引入校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem008406
     * @方法描述: 省心快贷自动化审批评级校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批评级校验
     * （1）借款企业的内部评级不得低于BB级
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008406(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷自动化审批评级校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 查询法人客户的信用评级信息
            ResultDto<Map<String, String>> result = iCusClientService.selectGradeInfoByCusId(lmtApp.getCusId());
            if (Objects.isNull(result) || Objects.isNull(result.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840601);
                return riskResultDto;
            }
            Map<String, String> gradeInfo = result.getData();
            if (Objects.nonNull(gradeInfo)) {
                String finalRank = gradeInfo.get("finalRank");
                if (StringUtils.isBlank(finalRank) || "110".compareTo(finalRank) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840602);
                    return riskResultDto;
                }
            }
            log.info("省心快贷自动化审批评级校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem008407
     * @方法描述: 省心快贷自动化审批公司财报校验
     * @参数与返回说明:
     * @算法描述: 省心快贷自动化审批公司财报校验
     * （1）自制报表近两年内不得连续亏损。
     * 财务模块财务报表未录入的拦截提示；
     * 若成立满6个月，但未满1年的，取近一期的报表数据的值判断；
     * 若成立满1年，但不满2年的，取上一年12月份数据报表和当期报表数据的值判断；
     * 若成立满2年的，取近两年的12月份报表数据的值判断
     * （2）自制报表资产负债率不得超过75%
     * （3）单一客户销贷比规则中销售指标取值（接口字段）
     * （4）征信担保规则指标中净资产指标取值（接口字段）
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008407(LmtApp lmtApp) {
            String serno = lmtApp.getSerno();
            log.info("省心快贷自动化审批公司财报校验开始*******************业务流水号：【{}】", serno);
            RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 获取客户模块中的客户基本信息
            ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                return riskResultDto;
            }
            CusBaseDto cusBaseDtoDo = resultDto.getData();
            String cusCatalog = cusBaseDtoDo.getCusCatalog();
            // 借款人必须是企业（客户大类 2-对公 1-对私）
            if (!Objects.equals("2", cusCatalog)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840101);
                return riskResultDto;
            }

            //查询对公客户基本信息
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
            if (Objects.isNull(cusCorpDtoResultDto) || Objects.isNull(cusCorpDtoResultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840701);
                return riskResultDto;
            }
            CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
            // 判断公司成立日期是否为空
            if (StringUtils.isBlank(cusCorpDto.getBuildDate())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840702);
                return riskResultDto;
            }
            // 获取公司成立日期
            Date buildDate = DateUtils.parseDate(cusCorpDto.getBuildDate(), "yyyy-MM-dd");
            log.info("省心快贷自动化审批评级校验*******************公司成立日期：【{}】", buildDate);
            String cusId = lmtApp.getCusId();
            String statPrd = "";
            ResultDto<Map<String, Object>> result = null;
            Map<String, Object> map = null;

            // 1）自制报表近两年内不得连续亏损。
            // 获取财报科目净利润与上一年度或者当期资产负债表的资产负债率信息
            ResultDto<Map<String, FinanIndicAnalyDto>> finanIndicAnalyDto =  iCusClientService.getFinRepRetProAndRatOfLia(cusId);
            Map<String, FinanIndicAnalyDto> resultMap = finanIndicAnalyDto.getData();
            log.info("当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(finanIndicAnalyDto)+"】}");
            // 财务模块财务报表未录入的拦截提示
            if (Objects.isNull(Optional.ofNullable(resultMap.get("retainedProfits")).get().getInputYear())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840703);
                return riskResultDto;
            }
            String rptYear = (Optional.ofNullable(resultMap.get("retainedProfits")).get().getInputYear());
            log.info("省心快贷自动化审批评级校验*******************获取最新财报日期{【"+rptYear+"】}");
            int year = Integer.parseInt(rptYear.substring(0, 4));
            int month = Integer.parseInt(rptYear.substring(4));
            log.info("省心快贷自动化审批评级校验*******************获取最新财报月份{【"+month+"】}");
            // 若成立满6个月，但未满1年的，取近一期的报表数据的值判断；
            if (DateUtils.compare(DateUtils.addMonth(buildDate, 6), DateUtils.getCurrDate()) <= 0
                    && DateUtils.compare(DateUtils.addMonth(buildDate, 12), DateUtils.getCurrDate()) > 0) {
                    // 获取财报为0时,未查询到符合条件的财务报表信息
                    if (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840706);
                        return riskResultDto;
                    }

                    // 报科目净利润连续2年小于等于0
                    if (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) < 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840704);
                        return riskResultDto;
                    }
            }

            // 若成立满1年，但不满2年的，取上一年12月份数据报表和当期报表数据的值判断；
            if (DateUtils.compare(DateUtils.addMonth(buildDate, 12), DateUtils.getCurrDate()) <= 0
                    && DateUtils.compare(DateUtils.addMonth(buildDate, 24), DateUtils.getCurrDate()) > 0) {
                    // 获取财报为0时,未查询到符合条件的财务报表信息
                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)
                            || (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840706);
                        return riskResultDto;
                    }

                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)
                            && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840704);
                        return riskResultDto;
                    }

            }
            // 若成立满2年的，取近两年的12月份年报数据的值判断
            if (DateUtils.compare(DateUtils.addMonth(buildDate, 24), DateUtils.getCurrDate()) <= 0) {
                // 若最新期财报为12月份,则取最新期年报与上一期财报。否则，取上两年的年报
                if (month == 12) {
                    // 获取财报为0时,未查询到符合条件的财务报表信息
                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)
                            && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840706);
                        return riskResultDto;
                    }

                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)
                            && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840704);
                        return riskResultDto;
                    }

                } else {
                    // 获取财报为0时,未查询到符合条件的财务报表信息
                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)
                            && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastBeforeYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840706);
                        return riskResultDto;
                    }

                    if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)
                            && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastBeforeYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840704);
                        return riskResultDto;
                    }
                }
            }
            //（2）自制报表资产负债率不得超过75%
            if (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("saleProfitRate")).get().getCurYmValue()).compareTo(new BigDecimal(0)) == 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840707);
                return riskResultDto;
            }

            // 当期资产负债表的资产负债率大于75%
            if (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("saleProfitRate")).get().getCurYmValue()).compareTo(new BigDecimal(75)) >= 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840705);
                return riskResultDto;
            }

            log.info("省心快贷自动化审批公司财报校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }

    /**
     * @方法名称: riskItem008408
     * @方法描述: 省心快贷评分卡校验
     * @参数与返回说明:
     * @算法描述: 省心快贷评分卡校验
     * （1） 校验：是否9类易燃易爆行业为 否
     * 是否两高一剩行业为  否
     * （2）合规经营校验中：
     * 企业有无违规排污，有无被环保部门查处和处罚为  否
     * 企业员工人数是否稳定，员工待遇是否合理为       是
     * 企业有无被税务机关查处和处罚为      否
     * 有无异常工商股权变更情况为    否
     * 实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等为   否
     * 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为     否
     * 实际控制人是否参与民间融资、投资高风险行业等行为为   否
     * 有无其他影响企业稳定经营的情况为   否
     * （3）流动资金测算校验：
     * 若流动资金测算不满足计算公式，调查报告中流动资金测算说明必须不为空
     * @创建人: yuanfs
     * @创建时间: 2021-08-7
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem008408(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        log.info("省心快贷评分卡校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        try{
            // 获取客户模块中的客户基本信息
            ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                return riskResultDto;
            }
            // 调查报告特定产品分析省心快贷
            RptBasicInfoAsso rptBasicInfoAsso = rptBasicInfoAssoService.selectByPrimaryKey(serno);
            if (Objects.isNull(resultDto) || Objects.isNull(rptBasicInfoAsso)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840801);
                return riskResultDto;
            }
            RptSpdAnysSxkd rptSpdAnysSxkd = rptSpdAnysSxkdService.selectByPrimaryKey(serno);
            if (Objects.isNull(rptSpdAnysSxkd)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840801);
                return riskResultDto;
            }
            // 评分卡分数未满70分不予准入
            if (70 > (Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd1Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd2Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd3Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd4Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd5Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd6Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd7Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd8Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd9Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysSxkd.getPfkKsxd10Grade1()).orElse(0))) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840810);
                return riskResultDto;
            }
            // 是否为我行规定的9类易燃易爆行业
            if (!Objects.equals("0", rptBasicInfoAsso.getBurnExplodeIndustryInd())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840811);
                return riskResultDto;
            }
            // 是否两高一剩行业为
            if (!Objects.equals("0", rptBasicInfoAsso.getTwoHightOneLeftInd())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840812);
                return riskResultDto;
            }
            // 企业有无违规排污，有无被环保部门查处和处罚
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd1())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
                return riskResultDto;
            }
            // 企业员工人数是否稳定，员工待遇是否合理为
            if (!Objects.equals("1", rptSpdAnysSxkd.getFocusSxkd2())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840803);
                return riskResultDto;
            }
            // 企业有无被税务机关查处和处罚
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd3())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840804);
                return riskResultDto;
            }
            // 有无异常工商股权变更情况
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd4())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840805);
                return riskResultDto;
            }
            // 实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd6())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840806);
                return riskResultDto;
            }
            // 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd7())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840807);
                return riskResultDto;
            }
            //  实际控制人是否参与民间融资、投资高风险行业等行为为   否
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd8())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840808);
                return riskResultDto;
            }
            // 有无其他影响企业稳定经营的情况
            if (!Objects.equals("0", rptSpdAnysSxkd.getFocusSxkd9())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840809);
                return riskResultDto;
            }

            //（3）流动资金测算校验：
            // 若流动资金测算不满足计算公式，调查报告中流动资金测算说明必须不为空
            // 根据授信流水获取流动资金测算额度，及说明
            // 测算最高额流动资金贷款额度必须 >= 敞口额度合计
            // 如果不满足该计算公式，则测算最高额流动资金是否覆盖贷款申请额度，且测算最高额流动资金是否覆盖贷款申请额度说明，必须录入说明原因
            log.info("省心快贷评分卡校验*******************流动资金测算校验开始：【{}】", serno);
            RptLmtRepayAnys rptLmtRepayAnys = rptLmtRepayAnysService.selectByPrimaryKey(serno);
            if (Objects.isNull(rptLmtRepayAnys) || org.springframework.util.StringUtils.isEmpty(rptLmtRepayAnys.getNewWorkingCapitalCal())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840813);
                return riskResultDto;
            } else {
                //获取最高额流动资金贷款额度
                BigDecimal newWorkingCapitalCal = new BigDecimal(0);
                //获取敞口额度合计
                BigDecimal openTotalLmtAmt = new BigDecimal(0);
                newWorkingCapitalCal = rptLmtRepayAnys.getNewWorkingCapitalCal();
                openTotalLmtAmt = lmtApp.getOpenTotalLmtAmt();
                // 如果测算最高额流动资金贷款额度必须 < 敞口额度合计
                if (openTotalLmtAmt.compareTo(newWorkingCapitalCal) > 0) {
                    // 判断测算最高额流动资金是否覆盖贷款申请额度说明，必须录入说明原因
                    if (org.springframework.util.StringUtils.isEmpty(rptLmtRepayAnys.getOtherInstructions())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840814);
                        return riskResultDto;
                    }
                }
            }
            log.info("省心快贷评分卡校验*******************流动资金测算校验结束：【{}】", serno);
            log.info("省心快贷评分卡校验结束*******************业务流水号：【{}】", serno);
            return riskResultDto;
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
    }}
