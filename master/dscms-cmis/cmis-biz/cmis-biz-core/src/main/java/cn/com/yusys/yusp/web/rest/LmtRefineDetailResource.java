/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtChgDetail;
import cn.com.yusys.yusp.domain.LmtRefineDetail;
import cn.com.yusys.yusp.service.LmtRefineDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRefineDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 11:23:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtrefinedetail")
public class LmtRefineDetailResource {
    @Autowired
    private LmtRefineDetailService lmtRefineDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRefineDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRefineDetail> list = lmtRefineDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtRefineDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRefineDetail>> index(QueryModel queryModel) {
        List<LmtRefineDetail> list = lmtRefineDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtRefineDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtRefineDetail> show(@PathVariable("pkId") String pkId) {
        LmtRefineDetail lmtRefineDetail = lmtRefineDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtRefineDetail>(lmtRefineDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtRefineDetail> create(@RequestBody LmtRefineDetail lmtRefineDetail) throws URISyntaxException {
        lmtRefineDetailService.insert(lmtRefineDetail);
        return new ResultDto<LmtRefineDetail>(lmtRefineDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRefineDetail lmtRefineDetail) throws URISyntaxException {
        int result = lmtRefineDetailService.update(lmtRefineDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtRefineDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtRefineDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtRefineDetail> save(@RequestBody LmtRefineDetail lmtRefineDetail) throws URISyntaxException {
        LmtRefineDetail lmtRefineDetail1 = lmtRefineDetailService.selectByLmtSerno(lmtRefineDetail.getLmtSerno());
        lmtRefineDetail.setOprType("01");
        if(lmtRefineDetail1!=null){
            lmtRefineDetailService.update(lmtRefineDetail);
        }else{
            lmtRefineDetailService.insert(lmtRefineDetail);
        }
        return ResultDto.success(lmtRefineDetail);
    }

    /**
     * @函数名称:show
     * @函数描述:进入页面查询数据返回前台
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtRefineDetail> selectBySerno(@RequestBody String lmtSerno) {
        LmtRefineDetail lmtRefineDetail = lmtRefineDetailService.selectByLmtSerno(lmtSerno);
        return  ResultDto.success(lmtRefineDetail);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetailByLmtSerno")
    protected ResultDto<List<Map<String,String>>> queryDetailByLmtSerno(@RequestBody Map<String,String> map) {
        List<Map<String,String>> lmtRefineDetails = lmtRefineDetailService.queryDetailByLmtSerno(map.get("lmtSerno"));
        return  ResultDto.success(lmtRefineDetails);
    }

}
