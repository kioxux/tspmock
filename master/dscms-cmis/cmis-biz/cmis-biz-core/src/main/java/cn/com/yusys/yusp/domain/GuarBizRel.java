/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRel
 * @类描述: GUAR_BIZ_REL数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "GUAR_BIZ_REL")
public class GuarBizRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 50)
	private String guarNo;
	
	/** 授信场景 **/
	@Column(name = "IS_UNDER_LMT", unique = false, nullable = true, length = 5)
	private String isUnderLmt;

	/** 是否追加担保 **/
	@Column(name = "IS_ADD_GUAR", unique = false, nullable = true, length = 5)
	private String isAddGuar;

	/** 对应融资金额 **/
	@Column(name = "CORRE_FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal correFinAmt;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private String createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String updateTime;

	/** 设定抵押率（%） **/
	@Column(name = "MORTAGAGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mortagageRate;

	/** 我行可用价值 **/
	@Column(name = "MAX_MORTAGAGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maxMortagageAmt;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param isUnderLmt
	 */
	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt;
	}
	
    /**
     * @return isUnderLmt
     */
	public String getIsUnderLmt() {
		return this.isUnderLmt;
	}

	public String getIsAddGuar() {
		return isAddGuar;
	}

	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar;
	}

	public BigDecimal getCorreFinAmt() {
		return correFinAmt;
	}

	public void setCorreFinAmt(BigDecimal correFinAmt) {
		this.correFinAmt = correFinAmt;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public String getUpdateTime() {
		return this.updateTime;
	}

	public BigDecimal getMortagageRate() {
		return mortagageRate;
	}

	public void setMortagageRate(BigDecimal mortagageRate) {
		this.mortagageRate = mortagageRate;
	}

	public BigDecimal getMaxMortagageAmt() {
		return maxMortagageAmt;
	}

	public void setMaxMortagageAmt(BigDecimal maxMortagageAmt) {
		this.maxMortagageAmt = maxMortagageAmt;
	}
}