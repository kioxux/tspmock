/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpTermChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpTermChgMapper;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpTermChgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xuchao
 * @创建时间: 2021-01-19 21:19:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpTermChgService {

    @Autowired
    private IqpTermChgMapper iqpTermChgMapper;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    private static final Logger log = LoggerFactory.getLogger(IqpTermChgService.class);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpTermChg selectByPrimaryKey(String iqpSerno) {
        return iqpTermChgMapper.selectByPrimaryKey(iqpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpTermChg> selectAll(QueryModel model) {
        List<IqpTermChg> records = (List<IqpTermChg>) iqpTermChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpTermChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpTermChg> list = iqpTermChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpTermChg record) {
        return iqpTermChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpTermChg record) {
        return iqpTermChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpTermChg record) {
        return iqpTermChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpTermChg record) {
        return iqpTermChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpTermChgMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpTermChgMapper.deleteByIds(ids);
    }

    /**
     * 新增期限调整申请数据
     * @param iqpTermChg
     * @return
     */
    public int insertIqpRateChgAppInfo(IqpTermChg iqpTermChg) throws Exception {
        int rtnId = 0;
        try{
            log.info("期限调整申请-引导页数据保存-开始！");
            log.info("借据号请求信息【{}】", JSONObject.toJSON(iqpTermChg));
            //操作类型赋初始值
            iqpTermChg.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //设置申请状态未待发起
            iqpTermChg.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            rtnId = iqpTermChgMapper.insertIqpTermChgAppInfo(iqpTermChg);
            if(rtnId == 0){
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value);
            }
            log.info("利率调整申请-引导页数据保存-结束！");
        }catch (Exception e) {
            log.error(e.getMessage() ,e);
        }
        return rtnId;
    }

    /**
     * 查询时否存在在途期限调整变更
     * @param param
     * @return
     */
    public int checkIsExistTermChgBiz(HashMap param){
        return iqpTermChgMapper.checkIsExistIqpTermChgBizByBillNo(param);
    }

    /**
     * 检验是否存在在途变更业务
     * @param iqpSerno
     * @return
     */
    public int checkIsExistChgBizByIqpSerno(String iqpSerno,String billNo){
//        return iqpRepayWayChgService.checkIsExistChgBizByBillNo(iqpSerno,billNo);
        return 0;
    }

    /**
     * 流程处理后更新期限申请表数据
     * @param iqpSerno
     * @param wfStatus
     */
    public void handleBusinessDataAfterStart(String iqpSerno, String wfStatus) throws Exception {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpTermChg iqpTermChg = iqpTermChgMapper.selectByPrimaryKey(iqpSerno);
            if(iqpTermChg==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为"+wfStatus);
            iqpTermChg.setApproveStatus(wfStatus);
            updateCount = iqpTermChgMapper.updateByPrimaryKeySelective(iqpTermChg);
            if(updateCount<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            //审批通过后更新借据表信息
            if (wfStatus == CmisCommonConstants.WF_STATUS_997) {
                log.info("更新借据表信息开始");
                AccLoan accloan = new AccLoan();
                /**
                 * zhanyb修改20210427。以下为原内容
                 accloan.setAppTerm(iqpTermChg.getTerm());//申请期限
                 accloan.setStartDate(iqpTermChg.getNewEndDate());//期限类型
                 accloan.setTermType(iqpTermChg.getTermTyp());
                 */
                accloan.setLoanTerm(iqpTermChg.getTerm().toString()); //贷款期限
                accloan.setLoanStartDate(iqpTermChg.getNewEndDate());
                accloan.setLoanTermUnit(iqpTermChg.getTermTyp());//贷款期限单位
                accloan.setBillNo(iqpTermChg.getBillNo());
                int rtnDate = accLoanService.updateSelective(accloan);
                if(rtnDate < 0){
                    throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, EcbEnum.IQP_EXCEPTION_DEF.value);
                }
            }
        }catch(Exception e){
            log.error(e.getMessage() ,e);
        }

    }
}
