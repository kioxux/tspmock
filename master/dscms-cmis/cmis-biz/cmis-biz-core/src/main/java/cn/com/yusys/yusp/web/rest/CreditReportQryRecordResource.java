/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditReportQryRecord;
import cn.com.yusys.yusp.service.CreditReportQryRecordService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportQryRecordResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 09:30:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditreportqryrecord")
public class CreditReportQryRecordResource {
    @Autowired
    private CreditReportQryRecordService creditReportQryRecordService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditReportQryRecord>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditReportQryRecord> list = creditReportQryRecordService.selectAll(queryModel);
        return new ResultDto<List<CreditReportQryRecord>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CreditReportQryRecord>> index(@RequestBody QueryModel queryModel) {
        List<CreditReportQryRecord> list = creditReportQryRecordService.selectByModel(queryModel);
        return new ResultDto<List<CreditReportQryRecord>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{crqrSerno}")
    protected ResultDto<CreditReportQryRecord> show(@PathVariable("crqrSerno") String crqrSerno) {
        CreditReportQryRecord creditReportQryRecord = creditReportQryRecordService.selectByPrimaryKey(crqrSerno);
        return new ResultDto<CreditReportQryRecord>(creditReportQryRecord);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CreditReportQryRecord> create(@RequestBody CreditReportQryRecord creditReportQryRecord) throws URISyntaxException {
        creditReportQryRecordService.insert(creditReportQryRecord);
        return new ResultDto<CreditReportQryRecord>(creditReportQryRecord);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditReportQryRecord creditReportQryRecord) throws URISyntaxException {
        int result = creditReportQryRecordService.update(creditReportQryRecord);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{crqrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("crqrSerno") String crqrSerno) {
        int result = creditReportQryRecordService.deleteByPrimaryKey(crqrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditReportQryRecordService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:createCreditReport
     * @函数描述:创建征信报告查看记录
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createCreditReport")
    protected ResultDto<CreditReportQryRecord> createCreditReport(@RequestBody CreditReportQryRecord creditReportQryRecord) throws URISyntaxException {
        creditReportQryRecordService.createCreditReport(creditReportQryRecord);
        return new ResultDto<CreditReportQryRecord>(creditReportQryRecord);
    }
}
