package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 押品公共功能DTO
 */
public class GuarInfoCommDto implements Serializable {
    private static final long serialVersionUID = 1L;

    //表名
    private String tableName;

    //押品统一编号
    private String guarNo;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }
}
