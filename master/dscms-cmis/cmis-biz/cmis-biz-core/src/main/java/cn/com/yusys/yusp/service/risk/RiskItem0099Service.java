package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CreditCardLargeLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.req.CmisLmt0050ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.resp.CmisLmt0050RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * 业务条线限额校验
 */
@Service
public class RiskItem0099Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0099Service.class);

    @Autowired
    private CreditCardLargeLoanAppService creditCardLargeLoanAppService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private ICmisCfgClientService cmisCfgClientService;

    /**
     * 1、贷款放款申请时，判断本次贷款放款申请金额<=业务条线及机构类型对应当月剩余可投放贷款余额
     * @param queryModel
     * @return
     */
    public RiskResultDto riskItem0099(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("【riskItem0099】业务条线限额校验开始*******************业务流水号：【{}】", serno);
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        //对公贷款出账申请  小微放款申请  零售放款申请（空白合同模式） 零售放款申请（生成打印模式）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)
                ||CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)){
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null){
                //是否需要校验（默认为是）
                boolean needCheck = true;
                //对公贷款出账申请  小微放款申请  零售放款申请
                if (CmisCommonConstants.STD_BELG_LINE_06.equals(pvpLoanApp.getBelgLine())){
                    log.info("【riskItem0099】业务条线限额校验======>1");
                    needCheck = false;
                }
                //币种不为人民币，直接通过
                if (!CmisBizConstants.STD_ZB_CUR_TYP_CNY.equals(pvpLoanApp.getContCurType())) {
                    log.info("【riskItem0099】业务条线限额校验======>2");
                    needCheck = false;
                }
                //申请机构以8开头，直接通过
                if (pvpLoanApp.getFinaBrId().startsWith("8")) {
                    log.info("【riskItem0099】业务条线限额校验======>3");
                    needCheck = false;
                }
                //需要校验
                if (needCheck){
                    log.info("RiskItem0099Service ==[OrgCode-->reqDto] ===> 【{}】", JSONObject.toJSON(pvpLoanApp.getManagerBrId()));
                    AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(pvpLoanApp.getManagerBrId());
                    log.info("RiskItem0099Service ==[OrgCode-->AdminSmOrgDto] ===> 【{}】", JSONObject.toJSON(adminSmOrgDto));
                    //机构信息获取失败
                    if (adminSmOrgDto == null) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09901); //机构信息获取失败
                        return riskResultDto;
                    }
                    String orgType = adminSmOrgDto.getOrgType();
                    //默认本地
                    String isOtherArea = "01";
                    //机构类型如果是村镇银行，则直接默认通过；机构类型为1-异地支行（有分行）、3-异地分行、2-异地支行（无分行）   则为异地机构
                    if ("1".equals(orgType) || "3".equals(orgType)||"2".equals(orgType)){
                        isOtherArea = "02";
                    }
                    BigDecimal pvpAmt = pvpLoanApp.getPvpAmt();

                    /**
                     * case when a.fina_br_id not like '0160%' and (a.fina_br_id not like '%06' or a.fina_br_id='019806') then '小微'
                     * when a.fina_br_id='019806' then '网金'
                     * when 产品为消费贷款 then '零售'
                     * else '对公'
                     */
                    //默认对公条线
                    String newBelgLine = CmisCommonConstants.STD_BELG_LINE_03;
                    String finaBrId = pvpLoanApp.getFinaBrId();
                    String prdId = pvpLoanApp.getPrdId();
                    //小微
                    if (finaBrId.startsWith("0160") || (!finaBrId.endsWith("06") && !"019806".equals(finaBrId))){
                        log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>1");
                        newBelgLine = CmisCommonConstants.STD_BELG_LINE_01;
                    }
                    //网络金融条线
                    else if (finaBrId.equals("019806")){
                        log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>2");
                        newBelgLine = CmisCommonConstants.STD_BELG_LINE_05;
                    }else{
                        //产品为消费贷款===>零售(prdType=10、11)
                        log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>3===>queryCfgPrdBasicInfo params【{}】",prdId);
                        ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = cmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                        log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>4===>queryCfgPrdBasicInfo Result【{}】",JSONObject.toJSON(cfgPrdBasicinfoDtoResultDto));
                        CfgPrdBasicinfoDto data = cfgPrdBasicinfoDtoResultDto.getData();
                        if (data == null){
                            //授信数据获取失败 RISK_ERROR_0005
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc("产品类型获取失败");
                            return riskResultDto;
                        }
                        if ("10".equals(data.getPrdType()) || "11".equals(data.getPrdType()) || "9".equals(data.getPrdType())
                                || "12".equals(data.getPrdType())){
                            //此处把9、12也加上，为了在上面已经判断过小微的了，下面如果有机构不对的小微产品，则归属到零售条线  add by zhangjw 20211118(同张洪波确认)
                            log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>5");
                            newBelgLine = CmisCommonConstants.STD_BELG_LINE_02;
                        }

//                        if (prdId.indexOf("0220")!=-1 && !"022044,022011,022045,022004,022099,022041,".contains(prdId+",")){
//                            log.info("【riskItem0099】业务条线限额校验【getNewBelgLine】======>5");
//                            newBelgLine = CmisCommonConstants.STD_BELG_LINE_02;
//                        }

                    }
                    return getCurrMonthAllowLoanBalance(adminSmOrgDto, pvpAmt,newBelgLine,isOtherArea);
                }
            }else{
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        //信用卡大额分期申请(默认为零售业务条线)
        if (CmisFlowConstants.FLOW_ID_XK005.equals(bizType)){
            log.info("【riskItem0099】业务条线限额校验======>4");
            CreditCardLargeLoanApp creditCardLargeLoanApp = creditCardLargeLoanAppService.selectByPrimaryKey(serno);
            if (!Objects.isNull(creditCardLargeLoanApp)){
                //是否需要校验（默认为是）
                boolean needCheck = true;
                //申请机构以8开头，直接通过
                if (creditCardLargeLoanApp.getManagerBrId().startsWith("8")) {
                    needCheck = false;
                }
                log.info("【riskItem0099】业务条线限额校验======>5 【{}】",needCheck);
                if (needCheck){
                    log.info("RiskItem0099Service ==[OrgCode-->reqDto] ===> 【{}】", JSONObject.toJSON(creditCardLargeLoanApp.getManagerBrId()));
                    AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(creditCardLargeLoanApp.getManagerBrId());
                    log.info("RiskItem0099Service ==[OrgCode-->AdminSmOrgDto] ===> 【{}】", JSONObject.toJSON(adminSmOrgDto));
                    if (adminSmOrgDto == null) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09901); //机构信息获取失败
                        return riskResultDto;
                    }
                    String orgType = adminSmOrgDto.getOrgType();
                    //默认本地
                    String isOtherArea = "01";
                    //机构类型如果是村镇银行，则直接默认通过；机构类型为1-异地支行（有分行）、3-异地分行、2-异地支行（无分行）   则为异地机构
                    if ("1".equals(orgType) || "3".equals(orgType)||"2".equals(orgType)){
                        isOtherArea = "02";
                    }
                    BigDecimal loanAmount = creditCardLargeLoanApp.getLoanAmount();
                    riskResultDto = getCurrMonthAllowLoanBalance(adminSmOrgDto, loanAmount,CmisCommonConstants.STD_BELG_LINE_02, isOtherArea);
                    return riskResultDto;
                }
            }else{
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        //校验通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 贷款放款申请时，判断本次贷款放款申请金额<=业务条线及机构类型对应当月剩余可投放贷款余额
     * @param adminSmOrgDto
     * @param lmtAmt
     * @param belgLine
     * @param isOtherArea 01本地 02异地
     * @return
     */
    private RiskResultDto getCurrMonthAllowLoanBalance(AdminSmOrgDto adminSmOrgDto, BigDecimal lmtAmt, String belgLine, String isOtherArea) {
        RiskResultDto riskResultDto = new RiskResultDto();

        BigDecimal allowLoanBalance = null;
        //机构类型 1、中心支行  2、异地支行（有分行） 3、异地支行（无分行）
        String orgType = adminSmOrgDto.getOrgType();
        //机构层级
        Integer orgLevel = adminSmOrgDto.getOrgLevel();
        //上月末贷款余额
        BigDecimal lastMonthLoanBalance;
        //当月可净新增贷款投放金额
        BigDecimal currMonthAllowAddAmt;
        CmisLmt0050ReqDto cmisLmt0050ReqDto = new CmisLmt0050ReqDto();
        cmisLmt0050ReqDto.setBelgLine(belgLine);
        cmisLmt0050ReqDto.setOrgAreaType(isOtherArea);
        log.info("【riskItem0099】cmislmt0050 交易描述：根据条线部门和区域获取业务条线额度管控信息********【cmisLmt0050ReqDto】***********【{}】", JSONObject.toJSON(cmisLmt0050ReqDto));
        ResultDto<CmisLmt0050RespDto> resultDto = cmisLmtClientService.cmislmt0050(cmisLmt0050ReqDto);
        log.info("【riskItem0099】cmislmt0050 交易描述：根据条线部门和区域获取业务条线额度管控信息*********【cmisLmt0050RespDtoResultDto】**********【{}】", JSONObject.toJSON(resultDto));
        if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            //根据机构编号获取分支机构额度管控信息 获取失败
            riskResultDto.setRiskResultDesc(resultDto.getMessage());
            return riskResultDto;
        }
        CmisLmt0050RespDto resultDtoData = resultDto.getData();
        if (SuccessEnum.SUCCESS.key.equals(resultDtoData.getErrorCode())) {
            lastMonthLoanBalance = resultDtoData.getLastMonthLoanBalance() == null ? BigDecimal.ZERO : resultDtoData.getLastMonthLoanBalance();
            lastMonthLoanBalance = lastMonthLoanBalance.multiply(BigDecimal.valueOf(10000L));
            currMonthAllowAddAmt = resultDtoData.getCurrMonthAllowAddAmt() == null ? BigDecimal.ZERO : resultDtoData.getCurrMonthAllowAddAmt();
            currMonthAllowAddAmt = currMonthAllowAddAmt.multiply(BigDecimal.valueOf(10000L));
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            //根据机构编号获取分支机构额度管控信息 获取失败
            riskResultDto.setRiskResultDesc(resultDtoData.getErrorMsg());
            return riskResultDto;
        }
        List<String> orgIds = new ArrayList<>();
        //1-异地支行（有分行） 2-异地支行（无分行）  3-异地分行
        if ("2".equals(orgType) || "3".equals(orgType) || "1".equals(orgType)) {
            orgIds.addAll(adminSmOrgService.getRemoteOrgList("1").getData());
            if (CollectionUtils.isEmpty(orgIds)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                //业务条线及机构类型对应当月剩余可投放贷款余额获取失败
                riskResultDto.setRiskResultDesc("异地机构列表获取失败！");
                return riskResultDto;
            }
        } else {
            orgIds.addAll(adminSmOrgService.getRemoteOrgList("0").getData());
            if (CollectionUtils.isEmpty(orgIds)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                //业务条线及机构类型对应当月剩余可投放贷款余额获取失败
                riskResultDto.setRiskResultDesc("本地机构列表获取失败！");
                return riskResultDto;
            }
        }
        //零售产品类型
        ResultDto<List<CfgPrdBasicinfoDto>> listResultDto = cmisCfgClientService.queryCfgPrdBasicInfoByPrdType("09,10,11,12");
        log.info("【riskItem0099】获取零售业务条线产品Id=====>【{}】",JSONObject.toJSON(listResultDto));
        List<CfgPrdBasicinfoDto> data = listResultDto.getData();
        if (data == null){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("零售条线产品类型获取失败！");
            return riskResultDto;
        }
        List<String> linshouPrdIds = new ArrayList<>();
        for (Object datum : data) {
            Map<String,Object> map = (Map<String, Object>) datum;
            String prdId = map.get("prdId").toString();
            linshouPrdIds.add(prdId);
        }
        log.info("【riskItem0099】获取零售业务条线产品Ids=====>【{}】",linshouPrdIds);

        log.info("【riskItem0099】业务条线限额校验=====>>6 【{}】",orgIds);
        allowLoanBalance = accLoanService.getCurrMonthAllowLoanBalance(orgIds,belgLine,linshouPrdIds);
        log.info("【riskItem0099】业务条线限额校验=====>>8 【{}】",allowLoanBalance);
//        if (allowLoanBalance == null) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
//            //业务条线及机构类型对应当月剩余可投放贷款余额获取失败
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09902);
//            return riskResultDto;
//        }
        if (allowLoanBalance == null) {
            allowLoanBalance = BigDecimal.ZERO;
        }
        //贷款放款申请时，判断本次贷款放款申请金额>业务条线及机构类型对应当月剩余可投放贷款余额(拦截)
        BigDecimal lmt = lastMonthLoanBalance.add(currMonthAllowAddAmt).subtract(allowLoanBalance);
        log.info("【riskItem0099】业务条线限额校验=====>>9 【{}】",lmt);
        if (lmtAmt.compareTo(lmt) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            //本次贷款放款申请金额 超过了 业务条线及机构类型对应当月剩余可投放贷款余额！
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09903);
            return riskResultDto;
        }
        //校验通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
