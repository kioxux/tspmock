package cn.com.yusys.yusp.web.server.xdtz0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0005.req.Xdtz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.resp.Xdtz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0005.Xdtz0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据流水号查询是否放款标记
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0005:根据流水号查询是否放款标记")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0005Resource.class);

    @Autowired
    private Xdtz0005Service xdtz0005Service;

    /**
     * 交易码：xdtz0005
     * 交易描述：根据流水号查询是否放款标记
     *
     * @param xdtz0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询是否放款标记")
    @PostMapping("/xdtz0005")
    protected @ResponseBody
    ResultDto<Xdtz0005DataRespDto> xdtz0005(@Validated @RequestBody Xdtz0005DataReqDto xdtz0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataReqDto));
        Xdtz0005DataRespDto xdtz0005DataRespDto = new Xdtz0005DataRespDto();// 响应Dto:根据流水号查询是否放款标记
        ResultDto<Xdtz0005DataRespDto> xdtz0005DataResultDto = new ResultDto<>();
        // 从xdtz0005DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataReqDto));
            xdtz0005DataRespDto = xdtz0005Service.xdtz0005(xdtz0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataRespDto));
            // 封装xdtz0005DataResultDto中正确的返回码和返回信息
            xdtz0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, e.getMessage());
            xdtz0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0005DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, e.getMessage());
            xdtz0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0005DataRespDto到xdtz0005DataResultDto中
        xdtz0005DataResultDto.setData(xdtz0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataResultDto));
        return xdtz0005DataResultDto;
    }
}
