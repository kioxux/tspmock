/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanFirstPayInfo;
import cn.com.yusys.yusp.repository.mapper.IqpLoanFirstPayInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanFirstPayInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-27 16:46:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanFirstPayInfoService {

    @Autowired
    private IqpLoanFirstPayInfoMapper iqpLoanFirstPayInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpLoanFirstPayInfo selectByPrimaryKey(String bizNo) {
        return iqpLoanFirstPayInfoMapper.selectByPrimaryKey(bizNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanFirstPayInfo> selectAll(QueryModel model) {
        List<IqpLoanFirstPayInfo> records = (List<IqpLoanFirstPayInfo>) iqpLoanFirstPayInfoMapper.selectByModel(model);
        return records;
    }
    /***
     * @param iqpSerno
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanFirstPayInfo>
     * @author hubp
     * @date 2021/4/27 16:50
     * @version 1.0.0
     * @desc 通过申请流水号查询购房首付款来源信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly=true)
    public List<IqpLoanFirstPayInfo> selectByIqpSerno(String iqpSerno) {
        List<IqpLoanFirstPayInfo> list = iqpLoanFirstPayInfoMapper.selectBySerno(iqpSerno);
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpLoanFirstPayInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanFirstPayInfo> list = iqpLoanFirstPayInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpLoanFirstPayInfo record) {
        return iqpLoanFirstPayInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanFirstPayInfo record) {
        return iqpLoanFirstPayInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpLoanFirstPayInfo record) {
        return iqpLoanFirstPayInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanFirstPayInfo record) {
        return iqpLoanFirstPayInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bizNo) {
        return iqpLoanFirstPayInfoMapper.deleteByPrimaryKey(bizNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanFirstPayInfoMapper.deleteByIds(ids);
    }

    /***
     * @param iqpLoanFirstPayInfo
     * @return int
     * @author wzy
     * @date 2021/4/24 9:37
     * @version 1.0.0
     * @desc 从主键是否有值来判断是插入还是更新首付款来源情况核实
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public int saveiqploanfirstpayinfo(IqpLoanFirstPayInfo iqpLoanFirstPayInfo) {
        String pkId = iqpLoanFirstPayInfo.getPkId();
        int row = 0;
        if (pkId == null || "".equals(pkId)) {
            iqpLoanFirstPayInfo.setPkId(UUID.randomUUID().toString());
            row = this.insert(iqpLoanFirstPayInfo);
        } else {
            row = this.updateSelective(iqpLoanFirstPayInfo);
        }
        return row;
    }

    /***
     * @param serno
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub>
     * @author hubp
     * @date 2021/4/23 16:27
     * @version 1.0.0
     * @desc 根据流水号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<IqpLoanFirstPayInfo> selectBySerno(String serno) {
        List<IqpLoanFirstPayInfo> list = iqpLoanFirstPayInfoMapper.selectBySerno(serno);
        return list;
    }
}
