package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuareInfo
 * @类描述: guare_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-16 15:29:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LatestLmtInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 上期授信情况-客户名称 **/
	private String grpCusName;

	/** 授信总额 **/
	private java.math.BigDecimal lmtAmt;

	/** 用信额度 **/
	private java.math.BigDecimal contAmt;

	/** 担保方式 **/
	private String guarMode;

	/** 复审授信金额 **/
	private java.math.BigDecimal reconsideAmt;

	public String getGrpCusName() {
		return grpCusName;
	}

	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getGuarMode() {
		return guarMode;
	}

	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	public BigDecimal getReconsideAmt() {
		return reconsideAmt;
	}

	public void setReconsideAmt(BigDecimal reconsideAmt) {
		this.reconsideAmt = reconsideAmt;
	}
}