/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarGcfBook;
import cn.com.yusys.yusp.service.GuarGcfBookService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGcfBookResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-10 22:21:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guargcfbook")
public class GuarGcfBookResource {
    @Autowired
    private GuarGcfBookService guarGcfBookService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarGcfBook>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarGcfBook> list = guarGcfBookService.selectAll(queryModel);
        return new ResultDto<List<GuarGcfBook>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarGcfBook>> index(QueryModel queryModel) {
        List<GuarGcfBook> list = guarGcfBookService.selectByModel(queryModel);
        return new ResultDto<List<GuarGcfBook>>(list);
    }

    /**
     * @函数名称:queryGuarGcfBookList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：zl
     */
    @ApiOperation(value = "省心快贷查封登记簿")
    @PostMapping("/queryGuarGcfBookList")
    protected ResultDto<List<GuarGcfBook>> queryGuarGcfBookList(@RequestBody QueryModel queryModel) {
        List<GuarGcfBook> list = guarGcfBookService.selectByModel(queryModel);
        return new ResultDto<List<GuarGcfBook>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        GuarGcfBook guarGcfBook = guarGcfBookService.selectBySerno((String) params.get("serno"));
        if (null != guarGcfBook) {
            resultDto.setData(guarGcfBook);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarGcfBook> show(@PathVariable("serno") String serno) {
        GuarGcfBook guarGcfBook = guarGcfBookService.selectByPrimaryKey(serno);
        return new ResultDto<GuarGcfBook>(guarGcfBook);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarGcfBook> create(@RequestBody GuarGcfBook guarGcfBook) throws URISyntaxException {
        guarGcfBookService.insert(guarGcfBook);
        return new ResultDto<GuarGcfBook>(guarGcfBook);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarGcfBook guarGcfBook) throws URISyntaxException {
        int result = guarGcfBookService.update(guarGcfBook);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarGcfBookService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarGcfBookService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
