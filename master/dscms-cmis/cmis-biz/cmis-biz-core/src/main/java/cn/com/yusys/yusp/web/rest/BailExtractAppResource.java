/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BailExtractApp;
import cn.com.yusys.yusp.service.BailExtractAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailExtractAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 14:50:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/bailextractapp")
public class BailExtractAppResource {
    @Autowired
    private BailExtractAppService bailExtractAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BailExtractApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BailExtractApp> list = bailExtractAppService.selectAll(queryModel);
        return new ResultDto<List<BailExtractApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BailExtractApp>> index(QueryModel queryModel) {
        List<BailExtractApp> list = bailExtractAppService.selectByModel(queryModel);
        return new ResultDto<List<BailExtractApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BailExtractApp> show(@PathVariable("pkId") String pkId) {
        BailExtractApp bailExtractApp = bailExtractAppService.selectByPrimaryKey(pkId);
        return new ResultDto<BailExtractApp>(bailExtractApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BailExtractApp> create(@RequestBody BailExtractApp bailExtractApp) throws URISyntaxException {
        bailExtractAppService.insert(bailExtractApp);
        return new ResultDto<BailExtractApp>(bailExtractApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BailExtractApp bailExtractApp) throws URISyntaxException {
        int result = bailExtractAppService.update(bailExtractApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = bailExtractAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = bailExtractAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:保证金提取列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("保证金提取列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<BailExtractApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<BailExtractApp> list = bailExtractAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<BailExtractApp>>(list);
    }

    /**
     * @函数名称:queryBailExtractAppDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querybailextractappdatabyparams")
    protected ResultDto<BailExtractApp> queryBailExtractAppDataByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        queryData.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        BailExtractApp bailExtractApp = bailExtractAppService.queryBailExtractAppDataByParams(queryData);
        return new ResultDto<BailExtractApp>(bailExtractApp);
    }
}
