package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpHouseService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021年7月23日11:00:20
 * @desc 借款人客户状态校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0047Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0047Service.class);
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private CmisCusClientService cmisCusClientService;


    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021年7月23日11:00:20
     * @version 1.0.0
     * @desc    借款人客户状态校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0047(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        log.info("*************借款人客户状态校验***********【{}】", iqpSerno);
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if (null == iqpLoanApp || StringUtils.isBlank(iqpLoanApp.getCusId())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人客户ID为空");
            return riskResultDto;
        } else {
            ResultDto<CusBaseDto>  resultDto = cmisCusClientService.cusBaseInfo(iqpLoanApp.getCusId());
            if(null == resultDto || null == resultDto.getData()){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0108);
                return riskResultDto;
            }
            CusBaseDto cusBaseDtoDo = resultDto.getData();
            if(cusBaseDtoDo == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0108);
                return riskResultDto;
            }
            String cusState = cusBaseDtoDo.getCusState();
            if("1".equals(cusState)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc("个人客户维护中，业务不允许提交审批");
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************借款人客户状态校验***********【{}】", iqpSerno);
        return riskResultDto;
    }
}
