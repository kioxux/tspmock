package cn.com.yusys.yusp.service.server.xdzc0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0024.req.Xdzc0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.List;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.Xdzc0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAccpTaskService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * 接口处理类:票据池资料补全查询
 *
 * @Author xs
 * @Date 2021/6/17 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0024Service {

    @Autowired
    private AsplAccpTaskService asplAccpTaskService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0024.Xdzc0024Service.class);

    /**
     * 交易码：xdzc0024
     * 交易描述: 票据池资料补全查询
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0024DataRespDto xdzc0024Service(Xdzc0024DataReqDto xdzc0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value);
        Xdzc0024DataRespDto xdzc0024DataRespDto = new Xdzc0024DataRespDto();
        String cusId = xdzc0024DataReqDto.getCusId();// 客户号
        int startPage = xdzc0024DataReqDto.getStartPage();// 起始页数
        int pageSize = xdzc0024DataReqDto.getPageSize();// 分页大小
        String taskCreateDateS = xdzc0024DataReqDto.getTaskCreateDateS();// 任务生成日期开始
        String taskCreateDateE = xdzc0024DataReqDto.getTaskCreateDateE();// 任务生成日期截止
        String needFinishDateS = xdzc0024DataReqDto.getNeedFinishDateS();// 要求完成日期开始
        String needFinishDateE = xdzc0024DataReqDto.getNeedFinishDateE();// 要求完成日期截止
        String collectDateS = xdzc0024DataReqDto.getCollectDateS();// 收集日期开始
        String collectDateE = xdzc0024DataReqDto.getCollectDateE();// 收集日期截止
        BigDecimal batchDrftAmtS = xdzc0024DataReqDto.getBatchDrftAmtS();// 票面总金额下限
        BigDecimal batchDrftAmtE = xdzc0024DataReqDto.getBatchDrftAmtE();// 票面总金额上限
        String pvpSerno = xdzc0024DataReqDto.getPvpSerno();// 出账流水（批次号）
        String taskId = xdzc0024DataReqDto.getTaskId();// 任务编号(补录流水）
        String queryType = xdzc0024DataReqDto.getQueryType();// 查询类型
        try {
            // 根据银承编号并联查询 资产池贸易背景收集任务表 和 银承台账票据明细
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);// 客户号
            model.addCondition("taskCreateDateS",taskCreateDateS);// 任务生成日期开始
            model.addCondition("taskCreateDateE",taskCreateDateE);// 任务生成日期截止
            model.addCondition("needFinishDateS",needFinishDateS);// 要求完成日期开始
            model.addCondition("needFinishDateE",needFinishDateE);// 要求完成日期截止
            model.addCondition("collectDateS",collectDateS);// 收集日期开始
            model.addCondition("collectDateE",collectDateE);// 收集日期截止
            model.addCondition("batchDrftAmtS",batchDrftAmtS);// 票面总金额下限
            model.addCondition("batchDrftAmtE",batchDrftAmtE);// 票面总金额上限
            model.addCondition("pvpSerno",pvpSerno);// // 出账流水（批次号）
            model.addCondition("taskId",taskId);// // 任务编号(补录流水）
            java.util.List<cn.com.yusys.yusp.dto.server.xdzc0024.resp.List> list = new ArrayList<>();
            int count = 0;
            if("2".equals(queryType)){
                list = asplAccpTaskService.xdzc0024(model);
                count = list.size();
            }else{
                if("0".equals(queryType)){
                    model.addCondition("approveStatus", CmisCommonConstants.WF_STATUS_000992111 );// // 任务编号(补录流水）
                }else if("1".equals(queryType)){
                    model.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_996997998);
                }else if("3".equals(queryType)){
                    model.addCondition("approveStatus", CmisCommonConstants.WF_STATUS_000992111 );
                    model.addCondition("isAddBill",CmisCommonConstants.YES_NO_0);// 未上传影像
                }
                PageHelper.startPage(startPage, pageSize);
                list = asplAccpTaskService.xdzc0024(model);
                PageInfo<List> pageinfo = new PageInfo<>(list);
                count = (int) pageinfo.getTotal();
                PageHelper.clearPage();
            }
            xdzc0024DataRespDto.setCount(Long.toString(count));
            xdzc0024DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value);
        return xdzc0024DataRespDto;
    }
}
