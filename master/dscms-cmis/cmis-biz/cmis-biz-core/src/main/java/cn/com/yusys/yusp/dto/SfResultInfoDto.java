package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-client模块
 * @类名称: SfResultInfo
 * @类描述: sf_result_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-20 23:43:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class SfResultInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 核心客户号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 手机银行录入信息是否准确 **/
	private String isMobileBankInfo;
	
	/** 是否收集现场照片 **/
	private String isScenePhoto;
	
	/** 是否完成抵押物勘验表 **/
	private String isPldimnInspect;
	
	/** 房产抵押物情况 **/
	private String housePldCase;
	
	/** 是否完成经营场所勘验表 **/
	private String isOperInspect;
	
	/** 企业经营情况 **/
	private String operCase;
	
	/** 经营企业近2年无逾期贷款 **/
	private String isCorpOverdueLoan;
	
	/** 经营企业无风险分类后三类贷款 **/
	private String isCorpThreeLoan;
	
	/** 借款人是否有配偶 **/
	private String isCusSpouse;
	
	/** 配偶近2年无逾期经营性贷款 **/
	private String isSpouseOperLoan;
	
	/** 配偶近2年房贷、消费贷逾期不超过5次 **/
	private String isSpouseOverdueTimes;
	
	/** 配偶无风险分类后三类贷款 **/
	private String isSpouseThreeLoan;
	
	/** 是否予以业务准入 **/
	private String isAdmit;
	
	/** 提交时间 **/
	private String submitDate;
	
	/** 流程状态 **/
	private String approveStatus;
	
	/** 处理人 **/
	private String managerId;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 保存提交区分标识 **/
	private String isSaveOrsubmit;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param isMobileBankInfo
	 */
	public void setIsMobileBankInfo(String isMobileBankInfo) {
		this.isMobileBankInfo = isMobileBankInfo == null ? null : isMobileBankInfo.trim();
	}
	
    /**
     * @return IsMobileBankInfo
     */	
	public String getIsMobileBankInfo() {
		return this.isMobileBankInfo;
	}
	
	/**
	 * @param isScenePhoto
	 */
	public void setIsScenePhoto(String isScenePhoto) {
		this.isScenePhoto = isScenePhoto == null ? null : isScenePhoto.trim();
	}
	
    /**
     * @return IsScenePhoto
     */	
	public String getIsScenePhoto() {
		return this.isScenePhoto;
	}
	
	/**
	 * @param isPldimnInspect
	 */
	public void setIsPldimnInspect(String isPldimnInspect) {
		this.isPldimnInspect = isPldimnInspect == null ? null : isPldimnInspect.trim();
	}
	
    /**
     * @return IsPldimnInspect
     */	
	public String getIsPldimnInspect() {
		return this.isPldimnInspect;
	}
	
	/**
	 * @param housePldCase
	 */
	public void setHousePldCase(String housePldCase) {
		this.housePldCase = housePldCase == null ? null : housePldCase.trim();
	}
	
    /**
     * @return HousePldCase
     */	
	public String getHousePldCase() {
		return this.housePldCase;
	}
	
	/**
	 * @param isOperInspect
	 */
	public void setIsOperInspect(String isOperInspect) {
		this.isOperInspect = isOperInspect == null ? null : isOperInspect.trim();
	}
	
    /**
     * @return IsOperInspect
     */	
	public String getIsOperInspect() {
		return this.isOperInspect;
	}
	
	/**
	 * @param operCase
	 */
	public void setOperCase(String operCase) {
		this.operCase = operCase == null ? null : operCase.trim();
	}
	
    /**
     * @return OperCase
     */	
	public String getOperCase() {
		return this.operCase;
	}
	
	/**
	 * @param isCorpOverdueLoan
	 */
	public void setIsCorpOverdueLoan(String isCorpOverdueLoan) {
		this.isCorpOverdueLoan = isCorpOverdueLoan == null ? null : isCorpOverdueLoan.trim();
	}
	
    /**
     * @return IsCorpOverdueLoan
     */	
	public String getIsCorpOverdueLoan() {
		return this.isCorpOverdueLoan;
	}
	
	/**
	 * @param isCorpThreeLoan
	 */
	public void setIsCorpThreeLoan(String isCorpThreeLoan) {
		this.isCorpThreeLoan = isCorpThreeLoan == null ? null : isCorpThreeLoan.trim();
	}
	
    /**
     * @return IsCorpThreeLoan
     */	
	public String getIsCorpThreeLoan() {
		return this.isCorpThreeLoan;
	}
	
	/**
	 * @param isCusSpouse
	 */
	public void setIsCusSpouse(String isCusSpouse) {
		this.isCusSpouse = isCusSpouse == null ? null : isCusSpouse.trim();
	}
	
    /**
     * @return IsCusSpouse
     */	
	public String getIsCusSpouse() {
		return this.isCusSpouse;
	}
	
	/**
	 * @param isSpouseOperLoan
	 */
	public void setIsSpouseOperLoan(String isSpouseOperLoan) {
		this.isSpouseOperLoan = isSpouseOperLoan == null ? null : isSpouseOperLoan.trim();
	}
	
    /**
     * @return IsSpouseOperLoan
     */	
	public String getIsSpouseOperLoan() {
		return this.isSpouseOperLoan;
	}
	
	/**
	 * @param isSpouseOverdueTimes
	 */
	public void setIsSpouseOverdueTimes(String isSpouseOverdueTimes) {
		this.isSpouseOverdueTimes = isSpouseOverdueTimes == null ? null : isSpouseOverdueTimes.trim();
	}
	
    /**
     * @return IsSpouseOverdueTimes
     */	
	public String getIsSpouseOverdueTimes() {
		return this.isSpouseOverdueTimes;
	}
	
	/**
	 * @param isSpouseThreeLoan
	 */
	public void setIsSpouseThreeLoan(String isSpouseThreeLoan) {
		this.isSpouseThreeLoan = isSpouseThreeLoan == null ? null : isSpouseThreeLoan.trim();
	}
	
    /**
     * @return IsSpouseThreeLoan
     */	
	public String getIsSpouseThreeLoan() {
		return this.isSpouseThreeLoan;
	}
	
	/**
	 * @param isAdmit
	 */
	public void setIsAdmit(String isAdmit) {
		this.isAdmit = isAdmit == null ? null : isAdmit.trim();
	}
	
    /**
     * @return IsAdmit
     */	
	public String getIsAdmit() {
		return this.isAdmit;
	}
	
	/**
	 * @param submitDate
	 */
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate == null ? null : submitDate.trim();
	}
	
    /**
     * @return SubmitDate
     */	
	public String getSubmitDate() {
		return this.submitDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param isSaveOrsubmit
	 */
	public void setIsSaveOrsubmit(String isSaveOrsubmit) {
		this.isSaveOrsubmit = isSaveOrsubmit == null ? null : isSaveOrsubmit.trim();
	}

	/**
	 * @return isSaveOrsubmit
	 */
	public String getIsSaveOrsubmit() {
		return this.isSaveOrsubmit;
	}



}