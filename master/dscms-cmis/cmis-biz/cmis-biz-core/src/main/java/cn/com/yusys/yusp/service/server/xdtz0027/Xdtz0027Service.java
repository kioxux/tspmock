package cn.com.yusys.yusp.service.server.xdtz0027;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0025.resp.Xdtz0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.req.Xdtz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.resp.Xdtz0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.AccLoanList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.server.xdtz0031.Xdtz0031Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0027Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0027Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 是否有贷款指标值
     *
     * @param xdtz0027DataReqDto
     * @return
     */
    public Xdtz0027DataRespDto xdtz0027(Xdtz0027DataReqDto xdtz0027DataReqDto) throws Exception {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataReqDto));
        //定义返回信息
        Xdtz0027DataRespDto xdtz0027DataRespDto = new Xdtz0027DataRespDto();
        try {
            String cusId = xdtz0027DataReqDto.getCusId();
            String flag = xdtz0027DataReqDto.getSftqzz();//是否提前周转
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//营业日期

            Map queryMap = new HashMap();
            queryMap.put("cusId", cusId);
            queryMap.put("openDay", openDay);
            if ("Y".equals(flag)) {//Y代表提前周转走无还本续贷sql
                xdtz0027DataRespDto = accLoanMapper.getYseIsHavingLoanByCusIds(queryMap);
            } else {//N代表非提前周转走还本续贷sql
                xdtz0027DataRespDto = accLoanMapper.getIsHavingLoanByCusIds(queryMap);
            }
            return xdtz0027DataRespDto;
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataRespDto));
        return xdtz0027DataRespDto;
    }
}
