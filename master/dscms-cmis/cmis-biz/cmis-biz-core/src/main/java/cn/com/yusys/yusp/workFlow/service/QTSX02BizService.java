package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateAppSubMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 人民币利率定价申请流程业务处理类
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class QTSX02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(QTSX02BizService.class);

    @Autowired
    private OtherCnyRateAppService otherCnyRateAppService;

    @Autowired
    private BGYW01BizService bgyw01BizService;

    @Autowired
    private OtherCnyRateAppFinDetailsService otherCnyRateAppFinDetailsService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private OtherCnyRateAppSubMapper otherCnyRateAppSubMapper;
    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;
    @Autowired
    private OtherCnyRateApprSubService otherCnyRateApprSubService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //对公人民币利率定价
        if (BizFlowConstant.QT002.equals(bizType)) {
            // 流程路由参数
            WFBizParamDto param = new WFBizParamDto();
            // 流程路由参数
        	Map<String, Object> params = resultInstanceDto.getParam();
            /**
             * 机构类型:
             * 1-异地支行（有分行）
             * 2-异地支行（无分行）
             * 3-异地分行
             * 4-中心支行
             * 5-综合支行
             * 6-对公支行
             */
            /*String orgType = "6"; // 机构类型默认 6-对公支行
            CommonService commonService = new CommonService();
            AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(currentOrgId);
            if(Objects.nonNull(adminSmOrgDto) && null != adminSmOrgDto.getOrgType()){
                orgType = adminSmOrgDto.getOrgType();
            }
            params.put("orgType",orgType);*/

            /**
             * 是否权限内 : Y-是,N-否
             * 支行行长 authorZHHZ
             * 分行行长 authorFHHZ
             * 负责人 authorFZR
             *
             * 获取 人民币利率定价权限表中 other_cny_rate_app_sub 对应审批权限 rulingApprAuth  取列表中最大权限
             * 1 支行权限
             * 2 分行权限
             * 3 负责人
             */
            // 权限默认 N-权限外
            String authorZHHZ = CmisBizConstants.AUTHORITY_N;
            String authorFHHZ = CmisBizConstants.AUTHORITY_N;
            String authorFZR = CmisBizConstants.AUTHORITY_N;
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", serno);
            queryModel.addCondition("oprType",CmisBizConstants.GGCR_OPRTYPE_ADD);
            List<OtherCnyRateAppSub> records = (List<OtherCnyRateAppSub>) otherCnyRateAppSubMapper.selectByModel(queryModel);
            // 默认审批权限 负责人
            String rulingApprAuth = CmisBizConstants.AUTHORITY_FZR;
            for (int i = 0; i < records.size(); i++) {
                OtherCnyRateAppSub otherCnyRateAppSub = records.get(i);
                if (CmisBizConstants.AUTHORITY_FHHZ.equals(otherCnyRateAppSub.getRulingApprAuth())) {
                    rulingApprAuth = CmisBizConstants.AUTHORITY_FHHZ;// 分行权限
                    break;
                }
                if (CmisBizConstants.AUTHORITY_ZHHZ.equals(otherCnyRateAppSub.getRulingApprAuth())) {
                    rulingApprAuth = CmisBizConstants.AUTHORITY_ZHHZ; // 支行权限
                }
            }
            if(CmisBizConstants.AUTHORITY_ZHHZ.equals(rulingApprAuth)){
                authorZHHZ = CmisBizConstants.AUTHORITY_Y; // 支行权限内
            } else if (CmisBizConstants.AUTHORITY_FHHZ.equals(rulingApprAuth)) {
                authorFHHZ = CmisBizConstants.AUTHORITY_Y;// 分行权限内
            } else {
                authorFZR = CmisBizConstants.AUTHORITY_Y;// 负责人权限内
            }
            params.put("authorZHHZ",authorZHHZ); // 支行权限
            params.put("authorFHHZ",authorFHHZ); // 分行权限
            params.put("authorFZR",authorFZR);// 负责人权限
            
            param.setParam(params);
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            workflowCoreClient.updateFlowParam(param);

            QT002biz(resultInstanceDto);
        }if (CmisFlowConstants.FLOW_TYPE_TYPE_BG001.equals(bizType)) {
            //BG001利率变更（对公人民币）
            bgyw01BizService.bizOp(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void QT002biz(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //利率反显
                String bizType = instanceInfo.getBizType();
                // 结束后处理
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                // 同步更新 人民币利率定价申请融资信息明细  other_cny_rate_app_fin_details
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno",serno);
                queryModel.addCondition("isSelfBank","1");
                queryModel.addCondition("oprType","01");
                List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsService.selectByModel(queryModel);
                for (int i = 0; i < list.size(); i++) {
                    OtherCnyRateAppFinDetails otherDetails =  list.get(i);
                    otherDetails.setStatus(CmisBizConstants.STD_CUS_LIST_STATUS_01);
                    otherCnyRateAppFinDetailsService.updateSelective(otherDetails);
                    //更新利率
                    //分项品种流水号
                    String subPrdSerno = otherDetails.getSubPrdSerno();
                    if(!"".equals(subPrdSerno) && null != subPrdSerno){
                        //执行年利率 授信批复台账分项适用品种明细表  LMT_REPLY_ACC_SUB_PRD
                        BigDecimal execRateYear = otherDetails.getExecRateYear();
                        // 查询定价审批信息 other_cny_rate_appr_sub  otherCnyRateApprSub
                        QueryModel apprSubModel = new QueryModel();
                        apprSubModel.addCondition("subPrdSerno",subPrdSerno);
                        apprSubModel.addCondition("serno",serno);
                        apprSubModel.addCondition("oprType","01");
                        List<OtherCnyRateApprSub> otherCnyRateApprSubList  = otherCnyRateApprSubService.selectByModel(apprSubModel);
                        for (int j = 0; j < otherCnyRateApprSubList.size() ; j++) {
                            OtherCnyRateApprSub otherCnyRateApprSub = otherCnyRateApprSubList.get(j);
                            if(!"".equals(otherCnyRateApprSub.getApprLoanRate()) && null != otherCnyRateApprSub.getApprLoanRate()){
                                execRateYear = otherCnyRateApprSub.getApprLoanRate();
                                break;
                            }
                        }
                        QueryModel queryModelLra = new QueryModel();
                        queryModelLra.addCondition("subPrdSerno",subPrdSerno);
                        List<LmtReplyAccSubPrd> lmtReplyAccSubPrds = lmtReplyAccSubPrdService.selectAll(queryModelLra);
                        for (int j = 0; j < lmtReplyAccSubPrds.size(); j++) {
                            LmtReplyAccSubPrd LmtReplyAccSubPrd = lmtReplyAccSubPrds.get(j);
                            LmtReplyAccSubPrd.setRateYear(execRateYear);
                            lmtReplyAccSubPrdService.update(LmtReplyAccSubPrd);
                        }
                        //执行年利率 授信批复分项适用品种明细表   lmt_reply_sub_prd
                        List<LmtReplySubPrd> lmtReplySubPrds = lmtReplySubPrdService.selectAll(queryModelLra);
                        for (int j = 0; j < lmtReplySubPrds.size(); j++) {
                            LmtReplySubPrd lmtReplySubPrd = lmtReplySubPrds.get(j);
                            lmtReplySubPrd.setRateYear(execRateYear);
                            lmtReplySubPrdService.update(lmtReplySubPrd);
                        }
                        //  执行年利率 授信申请分项适用品种明细表  lmtAppSubPrd
                        List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectAll(queryModelLra);
                        for (int j = 0; j < lmtAppSubPrds.size(); j++) {
                            LmtAppSubPrd lmtAppSubPrd = lmtAppSubPrds.get(j);
                            lmtAppSubPrd.setRateYear(execRateYear);
                            lmtAppSubPrdService.update(lmtAppSubPrd);
                        }
                        //  执行年利率 授信审批分项适用品种明细表 lmtApprSubPrd
                        List<LmtApprSubPrd> lmtApprSubPrds = lmtApprSubPrdService.selectAll(queryModelLra);
                        for (int j = 0; j < lmtApprSubPrds.size(); j++) {
                            LmtApprSubPrd lmtApprSubPrd = lmtApprSubPrds.get(j);
                            lmtApprSubPrd.setRateYear(execRateYear);
                            lmtApprSubPrdService.update(lmtApprSubPrd);
                        }
                    }
                }

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.QTSX02.equals(flowCode);
    }
}
