package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarChgApp
 * @类描述: grt_guar_chg_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-16 17:04:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GrtGuarChgAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String grtSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	private String guarWay;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 合同金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 汇率 **/
	private java.math.BigDecimal exchangeRate;
	
	/** 保证金比例 **/
	private java.math.BigDecimal bailRate;
	
	/** 风险敞口金额 **/
	private java.math.BigDecimal riskOpenAmt;
	
	/** 起始日期 **/
	private String contStartDate;
	
	/** 到期日期 **/
	private String contEndDate;
	
	/** 修改后担保方式 STD_ZB_GUAR_WAY **/
	private String newGuarWay;
	
	/** 备注 **/
	private String remark;
	
	/** 主办人 **/
	private String managerId;
	
	/** 管理机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param grtSerno
	 */
	public void setGrtSerno(String grtSerno) {
		this.grtSerno = grtSerno == null ? null : grtSerno.trim();
	}
	
    /**
     * @return GrtSerno
     */	
	public String getGrtSerno() {
		return this.grtSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}
	
    /**
     * @return GuarWay
     */	
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return ExchangeRate
     */	
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return BailRate
     */	
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param riskOpenAmt
	 */
	public void setRiskOpenAmt(java.math.BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}
	
    /**
     * @return RiskOpenAmt
     */	
	public java.math.BigDecimal getRiskOpenAmt() {
		return this.riskOpenAmt;
	}
	
	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate == null ? null : contStartDate.trim();
	}
	
    /**
     * @return ContStartDate
     */	
	public String getContStartDate() {
		return this.contStartDate;
	}
	
	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate == null ? null : contEndDate.trim();
	}
	
    /**
     * @return ContEndDate
     */	
	public String getContEndDate() {
		return this.contEndDate;
	}
	
	/**
	 * @param newGuarWay
	 */
	public void setNewGuarWay(String newGuarWay) {
		this.newGuarWay = newGuarWay == null ? null : newGuarWay.trim();
	}
	
    /**
     * @return NewGuarWay
     */	
	public String getNewGuarWay() {
		return this.newGuarWay;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}