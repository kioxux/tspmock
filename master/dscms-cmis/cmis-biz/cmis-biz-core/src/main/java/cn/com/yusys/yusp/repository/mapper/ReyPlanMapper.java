/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.ReyPlan;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 还款计划变更mapper
 */
public interface ReyPlanMapper {
    /**
     * 插入从借据表中插入的数据
     * @param record
     * @return
     */
    int insertSelective(ReyPlan record);

    /**
     * 根据借据编号查询是否存在在途的变更业务
     * @param billNo
     * @return
     */
    int checkIsExistWFByBillNo(@Param("billNo") String billNo);

    /**
     * 根据主键查询数据
     * @param iqpSerno
     * @return
     */
    ReyPlan selectByPrimaryKey(String iqpSerno);

    /**
     * 根据传入的主键宇状态更改审批状态
     * @param iqpSerno
     * @param wfStatus111
     * @return
     */
    int updateApproveStatusByIqpSerno(String iqpSerno, String wfStatus111);

    /**
     * @方法名称: updateAccLoanRepayWayByBillNo
     * @方法描述: 通过借据号更新贷款台账还款方式
     * @参数与返回说明:iqpRepayWayChg
     * @算法描述:
     */
    void updateAccLoanRepayWayByBillNo(ReyPlan reyPlan);

    /**
     * 向iqpRepayPlan表中插入数据
     * @param reyPlan
     */
    void insertIntoIqpRepayPlan(ReyPlan reyPlan);

    //根据借据号查询借据信息
    AccLoan selectPvpLoanAppByBillNo(String billNo);

    /**
     * 根据主键更改审批状态
     * @param reyPlan
     * @return
     */
    int updateByPrimaryKeySelective(ReyPlan reyPlan);

    /**
     * 将借据信息插入计划表
     * @param reyPlan
     * @return
     */
    int insertLoanInfoByBillNoIntoIqpRepayPlanChg(ReyPlan reyPlan);
}