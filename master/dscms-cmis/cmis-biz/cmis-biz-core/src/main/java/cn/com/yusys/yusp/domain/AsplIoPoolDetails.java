/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolDetails
 * @类描述: aspl_io_pool_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:15:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "aspl_io_pool_details")
public class AsplIoPoolDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 资产类型 **/
	@Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String assetType;
	
	/** 资产价值 **/
	@Column(name = "ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetValue;
	
	/** 资产到期日 **/
	@Column(name = "ASSET_END_DATE", unique = false, nullable = true, length = 20)
	private String assetEndDate;
	
	/** 资产状态 **/
	@Column(name = "ASSET_STATUS", unique = false, nullable = true, length = 5)
	private String assetStatus;
	
	/** 是否入池 **/
	@Column(name = "IS_POOL", unique = false, nullable = true, length = 5)
	private String isPool;
	
	/** 是否入池质押 （是否出池解质押，根据出入池状态，入池为质押 ，出池为解质押）**/
	@Column(name = "IS_PLEDGE", unique = false, nullable = true, length = 5)
	private String isPledge;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param assetType
	 */
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	
    /**
     * @return assetType
     */
	public String getAssetType() {
		return this.assetType;
	}
	
	/**
	 * @param assetValue
	 */
	public void setAssetValue(java.math.BigDecimal assetValue) {
		this.assetValue = assetValue;
	}
	
    /**
     * @return assetValue
     */
	public java.math.BigDecimal getAssetValue() {
		return this.assetValue;
	}
	
	/**
	 * @param assetEndDate
	 */
	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}
	
    /**
     * @return assetEndDate
     */
	public String getAssetEndDate() {
		return this.assetEndDate;
	}
	
	/**
	 * @param assetStatus
	 */
	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}
	
    /**
     * @return assetStatus
     */
	public String getAssetStatus() {
		return this.assetStatus;
	}
	
	/**
	 * @param isPool
	 */
	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}
	
    /**
     * @return isPool
     */
	public String getIsPool() {
		return this.isPool;
	}
	
	/**
	 * @param isPledge
	 */
	public void setIsPledge(String isPledge) {
		this.isPledge = isPledge;
	}
	
    /**
     * @return isPledge
     */
	public String getIsPledge() {
		return this.isPledge;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}