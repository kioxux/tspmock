package cn.com.yusys.yusp.service.server.xdxw0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.server.xdxw0019.req.Xdxw0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0019.resp.Xdxw0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CmisBizXwCommonService
 * @类描述: 推送决策审批结果（产生信贷批复）
 * @功能描述: 推送决策审批结果（产生信贷批复）
 * @创建人: 王玉坤
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0019Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0019Service.class);

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0019DataRespDto xdxw0019(Xdxw0019DataReqDto xdxw0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataReqDto));
        Xdxw0019DataRespDto xdxw0019DataRespDto = new Xdxw0019DataRespDto();
        // 返回推送状态
        String sendStatus = "S";
        // 目前缺少是否无还本续贷和续贷原合同编号字段
        try {
            LmtCrdReplyInfo lmtCrdReplyInfo = new LmtCrdReplyInfo();
            //拷贝数据
            BeanUtils.copyProperties(xdxw0019DataReqDto, lmtCrdReplyInfo);
            lmtCrdReplyInfo.setPrdName(xdxw0019DataReqDto.getPrdName());
            lmtCrdReplyInfo.setReplySerno(xdxw0019DataReqDto.getSurveyNo());
            lmtCrdReplyInfo.setPrdId(xdxw0019DataReqDto.getPrdId());
            lmtCrdReplyInfo.setCusId(xdxw0019DataReqDto.getCusId());
            lmtCrdReplyInfo.setCertCode(xdxw0019DataReqDto.getCertCode());
            lmtCrdReplyInfo.setCertType(xdxw0019DataReqDto.getCertType());
            lmtCrdReplyInfo.setLmtGraper(0);
            lmtCrdReplyInfo.setCurType("CNY");
            // 调查编号
            lmtCrdReplyInfo.setSurveySerno(xdxw0019DataReqDto.getSurveyNo());
            //担保方式
            lmtCrdReplyInfo.setGuarMode(xdxw0019DataReqDto.getGrtMode());
            //批复金额
            lmtCrdReplyInfo.setReplyAmt(new BigDecimal(xdxw0019DataReqDto.getContAmt()));
            //申请期限
            lmtCrdReplyInfo.setAppTerm(Integer.parseInt(xdxw0019DataReqDto.getApplyTerm()));
            //批复起始日
            lmtCrdReplyInfo.setReplyStartDate(xdxw0019DataReqDto.getContStartDate());
            //批复到期日
            lmtCrdReplyInfo.setReplyEndDate(xdxw0019DataReqDto.getContEndDate());
            //执行年利率
            lmtCrdReplyInfo.setExecRateYear(new BigDecimal(xdxw0019DataReqDto.getRate()));
            //本次用信金额
            lmtCrdReplyInfo.setCurtLoanAmt(new BigDecimal(xdxw0019DataReqDto.getCurtLoanAmt()));
            // 是否无还本续贷缺少字段
            lmtCrdReplyInfo.setIsWxbxd(xdxw0019DataReqDto.getIswxbxd());
            // 续贷原合同编号
            lmtCrdReplyInfo.setXdOrigiContNo(xdxw0019DataReqDto.getXdOrigiContNo());
            // 续贷原借据号
            lmtCrdReplyInfo.setXdOrigiBillNo(xdxw0019DataReqDto.getXdOrigiBillNo());
            //是否受托支付
            lmtCrdReplyInfo.setIsBeEntrustedPay(xdxw0019DataReqDto.getTruPayFlg());
            //批复状态 {"key":"01","value":"生效"},{"key":"02","value":"失效"},{"key":"03","value":"重新办理"}
            lmtCrdReplyInfo.setReplyStatus(DscmsBizXwEnum.CONFIRMSTATUS_01.key);
            // 贷款形式 1--新增 3--无还本  6--借新还旧
            lmtCrdReplyInfo.setLoanModal(xdxw0019DataReqDto.getLoanModal());
            //所属条线 01-小微；02-零售；03-对公；04-资产池
            lmtCrdReplyInfo.setBelgLine(DscmsBizXwEnum.BELG_LINE_01.key);

            //主管机构
            // 插LMT_CRD_REPLY_INFO小贷批复表
            lmtCrdReplyInfoMapper.insert(lmtCrdReplyInfo);

            // 前往额度系统建立额度
            cmisBizXwCommonService.sendCmisLmt0001(lmtCrdReplyInfo);

            xdxw0019DataRespDto.setSendStatus(sendStatus);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataRespDto));
        return xdxw0019DataRespDto;
    }
}
