/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.dto.LmtGrpMemDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpMemRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-08 21:01:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpMemRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    LmtGrpMemRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpMemRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtGrpMemRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtGrpMemRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtGrpMemRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtGrpMemRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    List<LmtGrpMemRel> getGrpHighCurfund(String serno);

    List<Map> getGrpEvelList(@Param("grpSerno")  String grpSerno);

    List<LmtGrpMemRel> getGrpLadEvalList(String grpCusId);

    List<LmtGrpMemRel> selectLmtGrpReplyByGrpSerno(String grpSerno);

    /**
     * @函数名称:queryLmtGrpMemRelByParams
     * @函数描述:通过组合参数查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtGrpMemRel> queryLmtGrpMemRelByParams(HashMap<String, String> params);

    /**
     * 根据流水号更新代码
     * @param grpSerno
     * @return
     */
    int intiGrpMemRelStatus(@Param("grpSerno") String grpSerno);


    /**
     * 查询返回所有成员信息
     */
    List<LmtGrpMemRel> getAllMemRelList();


    /**
     * 根据流水号查询mem信息表
     */
    List<LmtGrpMemRel> selectByLmtSerno(@Param("sernno") String serno);
    /**
     *  根据流水号查询mem信息表
     *
     */
    List<LmtGrpMemRel> selectByGrpSerno(@Param("grpSerno") String grpSerno);

    /**
     * 根据集团申请流水号获取单人客户申请流水号
     * @param grpSerno
     * @return
     */
    List<String> querySernoByGrpSerno(String grpSerno);

    /**
     * @函数名称: selectLmtGrpAppIsInFlow
     * @函数描述: 校验当前授信申报是否已进入流程
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-10 20:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    int selectLmtGrpAppIsInFlow(@Param("serno") String serno);

    /**
     * @函数名称: queryLmtGrpMemRelByGrpSerno
     * @函数描述: 查询当前集团名下的客户
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-18 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    String queryCusByGrpSerno(@Param("grpSerno") String grpSerno);

    /**
     * @函数名称: querySingleSernoByGrpSerno
     * @函数描述: 查询当前集团名下的客户授信申报流水号
     * @创建人: css
     * @创建时间: 2021-10-4 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    String querySingleSernoByGrpSerno(@Param("grpSerno") String grpSerno);

    List<Map> selectGrpLmtGuarRingDataByGrpSerno(String grpSerno);

    /**
     * 根据集团申请流水号获取集团项下的成员客户授信申报流水号
     * @param grpSerno
     * @return
     */
    List<String> getSingleSernosByGrpSerno(@Param("grpSerno") String grpSerno);

    /**
     * 修改关系表中金额字段
     * @param lmtGrpMemRel
     * @return
     */

    void updateLmtGrpMemRelLmtAmt(LmtGrpMemRel lmtGrpMemRel);
}