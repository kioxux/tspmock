package cn.com.yusys.yusp.web.server.xdtz0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0020.Xdtz0020Service;
import cn.com.yusys.yusp.service.server.xdtz0022.Xdtz0022Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0020.req.Xdtz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.resp.Xdtz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;

/**
 * 接口处理类:台账入账（保函）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0020:台账入账（保函）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0020Resource.class);

    @Autowired
    private Xdtz0020Service xdtz0020Service;
    /**
     * 交易码：xdtz0020
     * 交易描述：台账入账（保函）
     *
     * @param xdtz0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账（保函）")
    @PostMapping("/xdtz0020")
    protected @ResponseBody
    ResultDto<Xdtz0020DataRespDto> xdtz0020(@Validated @RequestBody Xdtz0020DataReqDto xdtz0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020DataReqDto));
        Xdtz0020DataRespDto xdtz0020DataRespDto = new Xdtz0020DataRespDto();// 响应Dto:台账入账（保函）
        ResultDto<Xdtz0020DataRespDto> xdtz0020DataResultDto = new ResultDto<>();
        String bizType = xdtz0020DataReqDto.getBizType();//业务类型
        String oprtype = xdtz0020DataReqDto.getOprtype();//操作类型
        String billNo = xdtz0020DataReqDto.getBillNo();//借据号
        String contNo = xdtz0020DataReqDto.getContNo();//合同编号
        String guarantType = xdtz0020DataReqDto.getGuarantType();//保函种类
        String guarantTypeName = xdtz0020DataReqDto.getGuarantTypeName();//保函种类名称
        String cusId = xdtz0020DataReqDto.getCusId();//客户号
        String cusName = xdtz0020DataReqDto.getCusName();//客户名称
        String assureMeans = xdtz0020DataReqDto.getAssureMeans();//担保方式
        BigDecimal guarantAmt = xdtz0020DataReqDto.getGuarantAmt();//保函金额
        String guarantAmtCurType = xdtz0020DataReqDto.getGuarantAmtCurType();//保函金额币种
        BigDecimal guarantBal = xdtz0020DataReqDto.getGuarantBal();//保函余额
        String startDate = xdtz0020DataReqDto.getStartDate();//生效日期
        String endDate = xdtz0020DataReqDto.getEndDate();//失效日期
        String settlAcctNo = xdtz0020DataReqDto.getSettlAcctNo();//结算账号
        String settlAcctNoName = xdtz0020DataReqDto.getSettlAcctNoName();//结算账号名称
        String riskSpacPerc = xdtz0020DataReqDto.getRiskSpacPerc();//风险敞口比例
        BigDecimal riskSpacAmt = xdtz0020DataReqDto.getRiskSpacAmt();//风险敞口金额
        BigDecimal chrgRate = xdtz0020DataReqDto.getChrgRate();//手续费率
        BigDecimal chrgAmt = xdtz0020DataReqDto.getChrgAmt();//手续费金额
        BigDecimal overduePadYearRate = xdtz0020DataReqDto.getOverduePadYearRate();//逾期垫款年利率
        String guarantPayType = xdtz0020DataReqDto.getGuarantPayType();//保函付款方式
        String beneficiar = xdtz0020DataReqDto.getBeneficiar();//受益人
        String beneficiarAcctb = xdtz0020DataReqDto.getBeneficiarAcctb();//受益人开户行
        String beneficiarAcctNo = xdtz0020DataReqDto.getBeneficiarAcctNo();//受益人账号
        String accStatus = xdtz0020DataReqDto.getAccStatus();//台账状态
        String inputId = xdtz0020DataReqDto.getInputId();//登记人
        String finaBrId = xdtz0020DataReqDto.getFinaBrId();//账务机构
        String managerId = xdtz0020DataReqDto.getManagerId();//责任人
        String managerBrId = xdtz0020DataReqDto.getManagerBrId();//责任机构
        BigDecimal cretQuotationExchgRate = xdtz0020DataReqDto.getCretQuotationExchgRate();//信贷牌价汇率
        try {
            // 从xdtz0020DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdtz0020DataRespDto = xdtz0020Service.xdtz0020(xdtz0020DataReqDto);
            // TODO 调用XXXXXService层结束
            // 封装xdtz0020DataResultDto中正确的返回码和返回信息
            xdtz0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, e.getMessage());
            // 封装xdtz0020DataResultDto中异常返回码和返回信息
            xdtz0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0020DataRespDto到xdtz0020DataResultDto中
        xdtz0020DataResultDto.setData(xdtz0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020DataResultDto));
        return xdtz0020DataResultDto;
    }
}
