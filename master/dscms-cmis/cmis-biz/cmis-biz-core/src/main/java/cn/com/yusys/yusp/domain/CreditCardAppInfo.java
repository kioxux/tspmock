/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAppInfo
 * @类描述: credit_card_app_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-10-27 23:46:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_card_app_info")
public class CreditCardAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 第三方申请流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 5)
	private String appChnl;
	
	/** 申请类型 **/
	@Column(name = "APPLY_TYPE", unique = false, nullable = true, length = 5)
	private String applyType;
	
	/** 申请卡产品 **/
	@Column(name = "APPLY_CARD_PRD", unique = false, nullable = true, length = 6)
	private String applyCardPrd;
	
	/** 卡种类 **/
	@Column(name = "CARD_TYPE", unique = false, nullable = true, length = 5)
	private String cardType;
	
	/** 是否复议 **/
	@Column(name = "IS_RECONSID", unique = false, nullable = true, length = 5)
	private String isReconsid;
	
	/** 原业务流水号 **/
	@Column(name = "OLD_SERNO", unique = false, nullable = true, length = 40)
	private String oldSerno;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 信用卡客户类型 **/
	@Column(name = "CARD_CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cardCusType;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 3)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 35)
	private String phone;
	
	/** 是否同时申请普通信用卡 **/
	@Column(name = "IS_APPLY_COMMON_CARD", unique = false, nullable = true, length = 5)
	private String isApplyCommonCard;
	
	/** 普通信用卡申请卡产品 **/
	@Column(name = "APPLY_COMMON_CARD_PRD", unique = false, nullable = true, length = 6)
	private String applyCommonCardPrd;
	
	/** 是否发送审批拒绝短信 **/
	@Column(name = "IS_SEND_MSG", unique = false, nullable = true, length = 5)
	private String isSendMsg;
	
	/** 是否同意卡片降级 **/
	@Column(name = "IS_AGREE_CARD_DOWN", unique = false, nullable = true, length = 5)
	private String isAgreeCardDown;
	
	/** 是否自选卡号 **/
	@Column(name = "IS_CUSTOMIZE_CARD_NO", unique = false, nullable = true, length = 5)
	private String isCustomizeCardNo;
	
	/** 自选卡号 **/
	@Column(name = "CUSTOMIZE_CARD_NO", unique = false, nullable = true, length = 19)
	private String customizeCardNo;
	
	/** 是否实时发卡 **/
	@Column(name = "IS_REAL_TIME_CARD_ISSUE", unique = false, nullable = true, length = 5)
	private String isRealTimeCardIssue;
	
	/** 征信授权日期 **/
	@Column(name = "CREDIT_AUTH_DATE", unique = false, nullable = true, length = 10)
	private String creditAuthDate;
	
	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 国籍 **/
	@Column(name = "NATION", unique = false, nullable = true, length = 5)
	private String nation;
	
	/** 民族 **/
	@Column(name = "FOLK", unique = false, nullable = true, length = 5)
	private String folk;
	
	/** 拼音 **/
	@Column(name = "PINYIN", unique = false, nullable = true, length = 80)
	private String pinyin;
	
	/** 客户出生日期 **/
	@Column(name = "BIRTHDAY", unique = false, nullable = true, length = 10)
	private String birthday;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 教育程度 **/
	@Column(name = "QUALIFICATION", unique = false, nullable = true, length = 5)
	private String qualification;
	
	/** 证件是否长期有效 **/
	@Column(name = "IS_LONG_VLD", unique = false, nullable = true, length = 5)
	private String isLongVld;
	
	/** 证件起始日期 **/
	@Column(name = "CERT_START_DATE", unique = false, nullable = true, length = 10)
	private String certStartDate;
	
	/** 证件到期日 **/
	@Column(name = "CERT_END_DATE", unique = false, nullable = true, length = 10)
	private String certEndDate;
	
	/** 发证机关 **/
	@Column(name = "REGI_ORG", unique = false, nullable = true, length = 80)
	private String regiOrg;
	
	/** 住宅状况 **/
	@Column(name = "RESI_STATUS", unique = false, nullable = true, length = 5)
	private String resiStatus;
	
	/** 车辆状况 **/
	@Column(name = "CAR_STATUS", unique = false, nullable = true, length = 5)
	private String carStatus;
	
	/** 年收入（税后）（万元） **/
	@Column(name = "YEAR_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearIncome;
	
	/** 现职年限 **/
	@Column(name = "WORK_YEARS", unique = false, nullable = true, length = 10)
	private Integer workYears;
	
	/** 单位全称 **/
	@Column(name = "CORP_NAME", unique = false, nullable = true, length = 500)
	private String corpName;
	
	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 5)
	private String duty;
	
	/** 公司单位性质 **/
	@Column(name = "CORP_CATEGORY", unique = false, nullable = true, length = 5)
	private String corpCategory;
	
	/** 职业 **/
	@Column(name = "OCCU", unique = false, nullable = true, length = 5)
	private String occu;
	
	/** 行业 **/
	@Column(name = "TRADE", unique = false, nullable = true, length = 5)
	private String trade;
	
	/** 预留问题 **/
	@Column(name = "RESERVE_QUESTION", unique = false, nullable = true, length = 500)
	private String reserveQuestion;
	
	/** 答案 **/
	@Column(name = "ANSWER", unique = false, nullable = true, length = 500)
	private String answer;
	
	/** 家庭地址省 **/
	@Column(name = "FAMILY_ADDR_PROVINCE", unique = false, nullable = true, length = 40)
	private String familyAddrProvince;
	
	/** 家庭地址市 **/
	@Column(name = "FAMILY_ADDR_CITY", unique = false, nullable = true, length = 40)
	private String familyAddrCity;
	
	/** 家庭地址区 **/
	@Column(name = "FAMILY_ADDR_ZONE", unique = false, nullable = true, length = 40)
	private String familyAddrZone;
	
	/** 家庭详细地址 **/
	@Column(name = "FAMILY_DETAIL_ADDR", unique = false, nullable = true, length = 500)
	private String familyDetailAddr;
	
	/** 家庭地址邮编 **/
	@Column(name = "FAMILY_ADDR_POST", unique = false, nullable = true, length = 6)
	private String familyAddrPost;
	
	/** 家庭电话 **/
	@Column(name = "FAMILY_PHONE", unique = false, nullable = true, length = 20)
	private String familyPhone;
	
	/** 单位地址省 **/
	@Column(name = "UNIT_ADDR_PROVINCE", unique = false, nullable = true, length = 40)
	private String unitAddrProvince;
	
	/** 单位地址市 **/
	@Column(name = "UNIT_ADDR_CITY", unique = false, nullable = true, length = 40)
	private String unitAddrCity;
	
	/** 单位地址区 **/
	@Column(name = "UNIT_ADDR_ZONE", unique = false, nullable = true, length = 40)
	private String unitAddrZone;
	
	/** 单位地址详细地址 **/
	@Column(name = "UNIT_DETAIL_ADDR", unique = false, nullable = true, length = 500)
	private String unitDetailAddr;
	
	/** 单位地址邮编 **/
	@Column(name = "UNIT_ADDR_POST", unique = false, nullable = true, length = 6)
	private String unitAddrPost;
	
	/** 单位电话 **/
	@Column(name = "UNIT_PHONE", unique = false, nullable = true, length = 20)
	private String unitPhone;
	
	/** 卡片邮寄地址 **/
	@Column(name = "CARD_POST_ADDR", unique = false, nullable = true, length = 5)
	private String cardPostAddr;
	
	/** 电子邮件地址 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 50)
	private String email;
	
	/** 亲属联系人中文姓名 **/
	@Column(name = "RELA_LINKMAN_NAME", unique = false, nullable = true, length = 80)
	private String relaLinkmanName;
	
	/** 亲属联系人性别 **/
	@Column(name = "RELA_LINKMAN_SEX", unique = false, nullable = true, length = 5)
	private String relaLinkmanSex;
	
	/** 亲属与申请人关系 **/
	@Column(name = "RELA_CUS_CORRE", unique = false, nullable = true, length = 6)
	private String relaCusCorre;
	
	/** 亲属联系人手机 **/
	@Column(name = "RELA_LINKMAN_PHONE", unique = false, nullable = true, length = 20)
	private String relaLinkmanPhone;
	
	/** 亲属联系人固定电话 **/
	@Column(name = "RELA_LINKMAN_FIXED_PHONE", unique = false, nullable = true, length = 20)
	private String relaLinkmanFixedPhone;
	
	/** 其他联系人中文姓名 **/
	@Column(name = "OTHER_LINKMAN_NAME", unique = false, nullable = true, length = 80)
	private String otherLinkmanName;
	
	/** 其他联系人性别 **/
	@Column(name = "OTHER_LINKMAN_SEX", unique = false, nullable = true, length = 5)
	private String otherLinkmanSex;
	
	/** 其他与申请人关系 **/
	@Column(name = "OTHER_LINKMAN_CORRE", unique = false, nullable = true, length = 6)
	private String otherLinkmanCorre;
	
	/** 其他联系人手机 **/
	@Column(name = "OTHER_LINKMAN_PHONE", unique = false, nullable = true, length = 35)
	private String otherLinkmanPhone;
	
	/** 其他联系人固定电话 **/
	@Column(name = "OTHER_LINKMAN_FIXED_PHONE", unique = false, nullable = true, length = 35)
	private String otherLinkmanFixedPhone;
	
	/** 主卡卡号 **/
	@Column(name = "MAIN_CARD_NO", unique = false, nullable = true, length = 40)
	private String mainCardNo;
	
	/** 附卡是否自选卡号 **/
	@Column(name = "IS_SUB_CUSTOMIZE_CARD_NO", unique = false, nullable = true, length = 5)
	private String isSubCustomizeCardNo;
	
	/** 附卡自选卡号 **/
	@Column(name = "SUB_CUSTOMIZE_CARD_NO", unique = false, nullable = true, length = 19)
	private String subCustomizeCardNo;
	
	/** 附卡申请人中文姓名 **/
	@Column(name = "SUB_CARD_CUS_NAME", unique = false, nullable = true, length = 80)
	private String subCardCusName;
	
	/** 附卡申请人性别 **/
	@Column(name = "SUB_CARD_CUS_SEX", unique = false, nullable = true, length = 5)
	private String subCardCusSex;
	
	/** 附卡申请人国籍 **/
	@Column(name = "SUB_CARD_CUS_NATION", unique = false, nullable = true, length = 5)
	private String subCardCusNation;
	
	/** 附卡申请人拼音 **/
	@Column(name = "SUB_CARD_CUS_PINYIN", unique = false, nullable = true, length = 80)
	private String subCardCusPinyin;
	
	/** 附卡申请人证件类型 **/
	@Column(name = "SUB_CARD_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String subCardCertType;
	
	/** 附卡申请人证件号码 **/
	@Column(name = "SUB_CARD_CERT_CODE", unique = false, nullable = true, length = 40)
	private String subCardCertCode;
	
	/** 附卡申请人证件是否长期有效 **/
	@Column(name = "IS_SUB_CARD_CERT_LONG_VLD", unique = false, nullable = true, length = 5)
	private String isSubCardCertLongVld;
	
	/** 附卡申请人证件起始日期 **/
	@Column(name = "SUB_CARD_START_DATE", unique = false, nullable = true, length = 20)
	private String subCardStartDate;
	
	/** 附卡申请人证件到期日 **/
	@Column(name = "SUB_CARD_END_DATE", unique = false, nullable = true, length = 20)
	private String subCardEndDate;
	
	/** 附卡申请人发证机关 **/
	@Column(name = "SUB_CARD_REGI_ORG", unique = false, nullable = true, length = 80)
	private String subCardRegiOrg;
	
	/** 附卡申请人民族 **/
	@Column(name = "SUB_CARD_INDIV_FOLK", unique = false, nullable = true, length = 5)
	private String subCardIndivFolk;
	
	/** 附卡申请人手机 **/
	@Column(name = "SUB_CARD_CUS_PHONE", unique = false, nullable = true, length = 36)
	private String subCardCusPhone;
	
	/** 附卡申请人职业 **/
	@Column(name = "SUB_CARD_OCCU", unique = false, nullable = true, length = 5)
	private String subCardOccu;
	
	/** 附卡申请人家庭地址省 **/
	@Column(name = "SUB_CARD_FAMILY_ADDR_PROVINCE", unique = false, nullable = true, length = 40)
	private String subCardFamilyAddrProvince;
	
	/** 附卡申请人家庭地址市 **/
	@Column(name = "SUB_CARD_FAMILY_ADDR_CITY", unique = false, nullable = true, length = 40)
	private String subCardFamilyAddrCity;
	
	/** 附卡申请人家庭地址区 **/
	@Column(name = "SUB_CARD_FAMILY_ADDR_ZONE", unique = false, nullable = true, length = 40)
	private String subCardFamilyAddrZone;
	
	/** 附卡申请人家庭详细地址 **/
	@Column(name = "SUB_CARD_FAMILY_DETAIL_ADDR", unique = false, nullable = true, length = 500)
	private String subCardFamilyDetailAddr;
	
	/** 附卡申请人单位地址省 **/
	@Column(name = "SUB_CARD_UNIT_ADDR_PROVINCE", unique = false, nullable = true, length = 40)
	private String subCardUnitAddrProvince;
	
	/** 附卡申请人单位地址市 **/
	@Column(name = "SUB_CARD_UNIT_ADDR_CITY", unique = false, nullable = true, length = 40)
	private String subCardUnitAddrCity;
	
	/** 附卡申请人单位地址区 **/
	@Column(name = "SUB_CARD_UNIT_ADDR_ZONE", unique = false, nullable = true, length = 40)
	private String subCardUnitAddrZone;
	
	/** 附卡申请人单位地址详细地址 **/
	@Column(name = "SUB_CARD_UNIT_DETAIL_ADDR", unique = false, nullable = true, length = 500)
	private String subCardUnitDetailAddr;
	
	/** 附卡申请人卡片邮寄地址 **/
	@Column(name = "SUB_CARD_POST_ADDR", unique = false, nullable = true, length = 5)
	private String subCardPostAddr;
	
	/** 约定还款类型 **/
	@Column(name = "AGREED_REPAY_TYPE", unique = false, nullable = true, length = 5)
	private String agreedRepayType;
	
	/** 约定还款扣款账号 **/
	@Column(name = "AGREED_REPAY_CARD_NO", unique = false, nullable = true, length = 40)
	private String agreedRepayCardNo;
	
	/** 约定还款人扣款姓名 **/
	@Column(name = "AGREED_REPAY_CUS_NAME", unique = false, nullable = true, length = 80)
	private String agreedRepayCusName;
	
	/** 推广渠道 **/
	@Column(name = "RECOM_CHNL", unique = false, nullable = true, length = 5)
	private String recomChnl;
	
	/** 建议额度 **/
	@Column(name = "SUGGEST_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal suggestLmt;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 退回原因 **/
	@Column(name = "RETURN_REASON", unique = false, nullable = true, length = 5)
	private String returnReason;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 申请业务阶段 **/
	@Column(name = "BUSINESS_STAGE", unique = false, nullable = true, length = 5)
	private String businessStage;
	
	/** 通联进件编号 **/
	@Column(name = "TL_APP_NO", unique = false, nullable = true, length = 40)
	private String tlAppNo;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 80)
	private String imageNo;
	
	/** 客户经理号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 客户经理姓名 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 80)
	private String managerName;
	
	/** 客户经理手机 **/
	@Column(name = "MANAGER_PHONE", unique = false, nullable = true, length = 35)
	private String managerPhone;
	
	/** 推荐人工号 **/
	@Column(name = "RECOM_ID", unique = false, nullable = true, length = 20)
	private String recomId;
	
	/** 推荐人名称 **/
	@Column(name = "RECOM_NAME", unique = false, nullable = true, length = 80)
	private String recomName;
	
	/** 推荐人手机号 **/
	@Column(name = "RECOM_PHONE", unique = false, nullable = true, length = 35)
	private String recomPhone;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 客户经理机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param applyType
	 */
	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}
	
    /**
     * @return applyType
     */
	public String getApplyType() {
		return this.applyType;
	}
	
	/**
	 * @param applyCardPrd
	 */
	public void setApplyCardPrd(String applyCardPrd) {
		this.applyCardPrd = applyCardPrd;
	}
	
    /**
     * @return applyCardPrd
     */
	public String getApplyCardPrd() {
		return this.applyCardPrd;
	}
	
	/**
	 * @param cardType
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
    /**
     * @return cardType
     */
	public String getCardType() {
		return this.cardType;
	}
	
	/**
	 * @param isReconsid
	 */
	public void setIsReconsid(String isReconsid) {
		this.isReconsid = isReconsid;
	}
	
    /**
     * @return isReconsid
     */
	public String getIsReconsid() {
		return this.isReconsid;
	}
	
	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno;
	}
	
    /**
     * @return oldSerno
     */
	public String getOldSerno() {
		return this.oldSerno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cardCusType
	 */
	public void setCardCusType(String cardCusType) {
		this.cardCusType = cardCusType;
	}
	
    /**
     * @return cardCusType
     */
	public String getCardCusType() {
		return this.cardCusType;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param isApplyCommonCard
	 */
	public void setIsApplyCommonCard(String isApplyCommonCard) {
		this.isApplyCommonCard = isApplyCommonCard;
	}
	
    /**
     * @return isApplyCommonCard
     */
	public String getIsApplyCommonCard() {
		return this.isApplyCommonCard;
	}
	
	/**
	 * @param applyCommonCardPrd
	 */
	public void setApplyCommonCardPrd(String applyCommonCardPrd) {
		this.applyCommonCardPrd = applyCommonCardPrd;
	}
	
    /**
     * @return applyCommonCardPrd
     */
	public String getApplyCommonCardPrd() {
		return this.applyCommonCardPrd;
	}
	
	/**
	 * @param isSendMsg
	 */
	public void setIsSendMsg(String isSendMsg) {
		this.isSendMsg = isSendMsg;
	}
	
    /**
     * @return isSendMsg
     */
	public String getIsSendMsg() {
		return this.isSendMsg;
	}
	
	/**
	 * @param isAgreeCardDown
	 */
	public void setIsAgreeCardDown(String isAgreeCardDown) {
		this.isAgreeCardDown = isAgreeCardDown;
	}
	
    /**
     * @return isAgreeCardDown
     */
	public String getIsAgreeCardDown() {
		return this.isAgreeCardDown;
	}
	
	/**
	 * @param isCustomizeCardNo
	 */
	public void setIsCustomizeCardNo(String isCustomizeCardNo) {
		this.isCustomizeCardNo = isCustomizeCardNo;
	}
	
    /**
     * @return isCustomizeCardNo
     */
	public String getIsCustomizeCardNo() {
		return this.isCustomizeCardNo;
	}
	
	/**
	 * @param customizeCardNo
	 */
	public void setCustomizeCardNo(String customizeCardNo) {
		this.customizeCardNo = customizeCardNo;
	}
	
    /**
     * @return customizeCardNo
     */
	public String getCustomizeCardNo() {
		return this.customizeCardNo;
	}
	
	/**
	 * @param isRealTimeCardIssue
	 */
	public void setIsRealTimeCardIssue(String isRealTimeCardIssue) {
		this.isRealTimeCardIssue = isRealTimeCardIssue;
	}
	
    /**
     * @return isRealTimeCardIssue
     */
	public String getIsRealTimeCardIssue() {
		return this.isRealTimeCardIssue;
	}
	
	/**
	 * @param creditAuthDate
	 */
	public void setCreditAuthDate(String creditAuthDate) {
		this.creditAuthDate = creditAuthDate;
	}
	
    /**
     * @return creditAuthDate
     */
	public String getCreditAuthDate() {
		return this.creditAuthDate;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param nation
	 */
	public void setNation(String nation) {
		this.nation = nation;
	}
	
    /**
     * @return nation
     */
	public String getNation() {
		return this.nation;
	}
	
	/**
	 * @param folk
	 */
	public void setFolk(String folk) {
		this.folk = folk;
	}
	
    /**
     * @return folk
     */
	public String getFolk() {
		return this.folk;
	}
	
	/**
	 * @param pinyin
	 */
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	
    /**
     * @return pinyin
     */
	public String getPinyin() {
		return this.pinyin;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param qualification
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
    /**
     * @return qualification
     */
	public String getQualification() {
		return this.qualification;
	}
	
	/**
	 * @param isLongVld
	 */
	public void setIsLongVld(String isLongVld) {
		this.isLongVld = isLongVld;
	}
	
    /**
     * @return isLongVld
     */
	public String getIsLongVld() {
		return this.isLongVld;
	}
	
	/**
	 * @param certStartDate
	 */
	public void setCertStartDate(String certStartDate) {
		this.certStartDate = certStartDate;
	}
	
    /**
     * @return certStartDate
     */
	public String getCertStartDate() {
		return this.certStartDate;
	}
	
	/**
	 * @param certEndDate
	 */
	public void setCertEndDate(String certEndDate) {
		this.certEndDate = certEndDate;
	}
	
    /**
     * @return certEndDate
     */
	public String getCertEndDate() {
		return this.certEndDate;
	}
	
	/**
	 * @param regiOrg
	 */
	public void setRegiOrg(String regiOrg) {
		this.regiOrg = regiOrg;
	}
	
    /**
     * @return regiOrg
     */
	public String getRegiOrg() {
		return this.regiOrg;
	}
	
	/**
	 * @param resiStatus
	 */
	public void setResiStatus(String resiStatus) {
		this.resiStatus = resiStatus;
	}
	
    /**
     * @return resiStatus
     */
	public String getResiStatus() {
		return this.resiStatus;
	}
	
	/**
	 * @param carStatus
	 */
	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}
	
    /**
     * @return carStatus
     */
	public String getCarStatus() {
		return this.carStatus;
	}
	
	/**
	 * @param yearIncome
	 */
	public void setYearIncome(java.math.BigDecimal yearIncome) {
		this.yearIncome = yearIncome;
	}
	
    /**
     * @return yearIncome
     */
	public java.math.BigDecimal getYearIncome() {
		return this.yearIncome;
	}
	
	/**
	 * @param workYears
	 */
	public void setWorkYears(Integer workYears) {
		this.workYears = workYears;
	}
	
    /**
     * @return workYears
     */
	public Integer getWorkYears() {
		return this.workYears;
	}
	
	/**
	 * @param corpName
	 */
	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}
	
    /**
     * @return corpName
     */
	public String getCorpName() {
		return this.corpName;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param corpCategory
	 */
	public void setCorpCategory(String corpCategory) {
		this.corpCategory = corpCategory;
	}
	
    /**
     * @return corpCategory
     */
	public String getCorpCategory() {
		return this.corpCategory;
	}
	
	/**
	 * @param occu
	 */
	public void setOccu(String occu) {
		this.occu = occu;
	}
	
    /**
     * @return occu
     */
	public String getOccu() {
		return this.occu;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}
	
    /**
     * @return trade
     */
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param reserveQuestion
	 */
	public void setReserveQuestion(String reserveQuestion) {
		this.reserveQuestion = reserveQuestion;
	}
	
    /**
     * @return reserveQuestion
     */
	public String getReserveQuestion() {
		return this.reserveQuestion;
	}
	
	/**
	 * @param answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
    /**
     * @return answer
     */
	public String getAnswer() {
		return this.answer;
	}
	
	/**
	 * @param familyAddrProvince
	 */
	public void setFamilyAddrProvince(String familyAddrProvince) {
		this.familyAddrProvince = familyAddrProvince;
	}
	
    /**
     * @return familyAddrProvince
     */
	public String getFamilyAddrProvince() {
		return this.familyAddrProvince;
	}
	
	/**
	 * @param familyAddrCity
	 */
	public void setFamilyAddrCity(String familyAddrCity) {
		this.familyAddrCity = familyAddrCity;
	}
	
    /**
     * @return familyAddrCity
     */
	public String getFamilyAddrCity() {
		return this.familyAddrCity;
	}
	
	/**
	 * @param familyAddrZone
	 */
	public void setFamilyAddrZone(String familyAddrZone) {
		this.familyAddrZone = familyAddrZone;
	}
	
    /**
     * @return familyAddrZone
     */
	public String getFamilyAddrZone() {
		return this.familyAddrZone;
	}
	
	/**
	 * @param familyDetailAddr
	 */
	public void setFamilyDetailAddr(String familyDetailAddr) {
		this.familyDetailAddr = familyDetailAddr;
	}
	
    /**
     * @return familyDetailAddr
     */
	public String getFamilyDetailAddr() {
		return this.familyDetailAddr;
	}
	
	/**
	 * @param familyAddrPost
	 */
	public void setFamilyAddrPost(String familyAddrPost) {
		this.familyAddrPost = familyAddrPost;
	}
	
    /**
     * @return familyAddrPost
     */
	public String getFamilyAddrPost() {
		return this.familyAddrPost;
	}
	
	/**
	 * @param familyPhone
	 */
	public void setFamilyPhone(String familyPhone) {
		this.familyPhone = familyPhone;
	}
	
    /**
     * @return familyPhone
     */
	public String getFamilyPhone() {
		return this.familyPhone;
	}
	
	/**
	 * @param unitAddrProvince
	 */
	public void setUnitAddrProvince(String unitAddrProvince) {
		this.unitAddrProvince = unitAddrProvince;
	}
	
    /**
     * @return unitAddrProvince
     */
	public String getUnitAddrProvince() {
		return this.unitAddrProvince;
	}
	
	/**
	 * @param unitAddrCity
	 */
	public void setUnitAddrCity(String unitAddrCity) {
		this.unitAddrCity = unitAddrCity;
	}
	
    /**
     * @return unitAddrCity
     */
	public String getUnitAddrCity() {
		return this.unitAddrCity;
	}
	
	/**
	 * @param unitAddrZone
	 */
	public void setUnitAddrZone(String unitAddrZone) {
		this.unitAddrZone = unitAddrZone;
	}
	
    /**
     * @return unitAddrZone
     */
	public String getUnitAddrZone() {
		return this.unitAddrZone;
	}
	
	/**
	 * @param unitDetailAddr
	 */
	public void setUnitDetailAddr(String unitDetailAddr) {
		this.unitDetailAddr = unitDetailAddr;
	}
	
    /**
     * @return unitDetailAddr
     */
	public String getUnitDetailAddr() {
		return this.unitDetailAddr;
	}
	
	/**
	 * @param unitAddrPost
	 */
	public void setUnitAddrPost(String unitAddrPost) {
		this.unitAddrPost = unitAddrPost;
	}
	
    /**
     * @return unitAddrPost
     */
	public String getUnitAddrPost() {
		return this.unitAddrPost;
	}
	
	/**
	 * @param unitPhone
	 */
	public void setUnitPhone(String unitPhone) {
		this.unitPhone = unitPhone;
	}
	
    /**
     * @return unitPhone
     */
	public String getUnitPhone() {
		return this.unitPhone;
	}
	
	/**
	 * @param cardPostAddr
	 */
	public void setCardPostAddr(String cardPostAddr) {
		this.cardPostAddr = cardPostAddr;
	}
	
    /**
     * @return cardPostAddr
     */
	public String getCardPostAddr() {
		return this.cardPostAddr;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param relaLinkmanName
	 */
	public void setRelaLinkmanName(String relaLinkmanName) {
		this.relaLinkmanName = relaLinkmanName;
	}
	
    /**
     * @return relaLinkmanName
     */
	public String getRelaLinkmanName() {
		return this.relaLinkmanName;
	}
	
	/**
	 * @param relaLinkmanSex
	 */
	public void setRelaLinkmanSex(String relaLinkmanSex) {
		this.relaLinkmanSex = relaLinkmanSex;
	}
	
    /**
     * @return relaLinkmanSex
     */
	public String getRelaLinkmanSex() {
		return this.relaLinkmanSex;
	}
	
	/**
	 * @param relaCusCorre
	 */
	public void setRelaCusCorre(String relaCusCorre) {
		this.relaCusCorre = relaCusCorre;
	}
	
    /**
     * @return relaCusCorre
     */
	public String getRelaCusCorre() {
		return this.relaCusCorre;
	}
	
	/**
	 * @param relaLinkmanPhone
	 */
	public void setRelaLinkmanPhone(String relaLinkmanPhone) {
		this.relaLinkmanPhone = relaLinkmanPhone;
	}
	
    /**
     * @return relaLinkmanPhone
     */
	public String getRelaLinkmanPhone() {
		return this.relaLinkmanPhone;
	}
	
	/**
	 * @param relaLinkmanFixedPhone
	 */
	public void setRelaLinkmanFixedPhone(String relaLinkmanFixedPhone) {
		this.relaLinkmanFixedPhone = relaLinkmanFixedPhone;
	}
	
    /**
     * @return relaLinkmanFixedPhone
     */
	public String getRelaLinkmanFixedPhone() {
		return this.relaLinkmanFixedPhone;
	}
	
	/**
	 * @param otherLinkmanName
	 */
	public void setOtherLinkmanName(String otherLinkmanName) {
		this.otherLinkmanName = otherLinkmanName;
	}
	
    /**
     * @return otherLinkmanName
     */
	public String getOtherLinkmanName() {
		return this.otherLinkmanName;
	}
	
	/**
	 * @param otherLinkmanSex
	 */
	public void setOtherLinkmanSex(String otherLinkmanSex) {
		this.otherLinkmanSex = otherLinkmanSex;
	}
	
    /**
     * @return otherLinkmanSex
     */
	public String getOtherLinkmanSex() {
		return this.otherLinkmanSex;
	}
	
	/**
	 * @param otherLinkmanCorre
	 */
	public void setOtherLinkmanCorre(String otherLinkmanCorre) {
		this.otherLinkmanCorre = otherLinkmanCorre;
	}
	
    /**
     * @return otherLinkmanCorre
     */
	public String getOtherLinkmanCorre() {
		return this.otherLinkmanCorre;
	}
	
	/**
	 * @param otherLinkmanPhone
	 */
	public void setOtherLinkmanPhone(String otherLinkmanPhone) {
		this.otherLinkmanPhone = otherLinkmanPhone;
	}
	
    /**
     * @return otherLinkmanPhone
     */
	public String getOtherLinkmanPhone() {
		return this.otherLinkmanPhone;
	}
	
	/**
	 * @param otherLinkmanFixedPhone
	 */
	public void setOtherLinkmanFixedPhone(String otherLinkmanFixedPhone) {
		this.otherLinkmanFixedPhone = otherLinkmanFixedPhone;
	}
	
    /**
     * @return otherLinkmanFixedPhone
     */
	public String getOtherLinkmanFixedPhone() {
		return this.otherLinkmanFixedPhone;
	}
	
	/**
	 * @param mainCardNo
	 */
	public void setMainCardNo(String mainCardNo) {
		this.mainCardNo = mainCardNo;
	}
	
    /**
     * @return mainCardNo
     */
	public String getMainCardNo() {
		return this.mainCardNo;
	}
	
	/**
	 * @param isSubCustomizeCardNo
	 */
	public void setIsSubCustomizeCardNo(String isSubCustomizeCardNo) {
		this.isSubCustomizeCardNo = isSubCustomizeCardNo;
	}
	
    /**
     * @return isSubCustomizeCardNo
     */
	public String getIsSubCustomizeCardNo() {
		return this.isSubCustomizeCardNo;
	}
	
	/**
	 * @param subCustomizeCardNo
	 */
	public void setSubCustomizeCardNo(String subCustomizeCardNo) {
		this.subCustomizeCardNo = subCustomizeCardNo;
	}
	
    /**
     * @return subCustomizeCardNo
     */
	public String getSubCustomizeCardNo() {
		return this.subCustomizeCardNo;
	}
	
	/**
	 * @param subCardCusName
	 */
	public void setSubCardCusName(String subCardCusName) {
		this.subCardCusName = subCardCusName;
	}
	
    /**
     * @return subCardCusName
     */
	public String getSubCardCusName() {
		return this.subCardCusName;
	}
	
	/**
	 * @param subCardCusSex
	 */
	public void setSubCardCusSex(String subCardCusSex) {
		this.subCardCusSex = subCardCusSex;
	}
	
    /**
     * @return subCardCusSex
     */
	public String getSubCardCusSex() {
		return this.subCardCusSex;
	}
	
	/**
	 * @param subCardCusNation
	 */
	public void setSubCardCusNation(String subCardCusNation) {
		this.subCardCusNation = subCardCusNation;
	}
	
    /**
     * @return subCardCusNation
     */
	public String getSubCardCusNation() {
		return this.subCardCusNation;
	}
	
	/**
	 * @param subCardCusPinyin
	 */
	public void setSubCardCusPinyin(String subCardCusPinyin) {
		this.subCardCusPinyin = subCardCusPinyin;
	}
	
    /**
     * @return subCardCusPinyin
     */
	public String getSubCardCusPinyin() {
		return this.subCardCusPinyin;
	}
	
	/**
	 * @param subCardCertType
	 */
	public void setSubCardCertType(String subCardCertType) {
		this.subCardCertType = subCardCertType;
	}
	
    /**
     * @return subCardCertType
     */
	public String getSubCardCertType() {
		return this.subCardCertType;
	}
	
	/**
	 * @param subCardCertCode
	 */
	public void setSubCardCertCode(String subCardCertCode) {
		this.subCardCertCode = subCardCertCode;
	}
	
    /**
     * @return subCardCertCode
     */
	public String getSubCardCertCode() {
		return this.subCardCertCode;
	}
	
	/**
	 * @param isSubCardCertLongVld
	 */
	public void setIsSubCardCertLongVld(String isSubCardCertLongVld) {
		this.isSubCardCertLongVld = isSubCardCertLongVld;
	}
	
    /**
     * @return isSubCardCertLongVld
     */
	public String getIsSubCardCertLongVld() {
		return this.isSubCardCertLongVld;
	}
	
	/**
	 * @param subCardStartDate
	 */
	public void setSubCardStartDate(String subCardStartDate) {
		this.subCardStartDate = subCardStartDate;
	}
	
    /**
     * @return subCardStartDate
     */
	public String getSubCardStartDate() {
		return this.subCardStartDate;
	}
	
	/**
	 * @param subCardEndDate
	 */
	public void setSubCardEndDate(String subCardEndDate) {
		this.subCardEndDate = subCardEndDate;
	}
	
    /**
     * @return subCardEndDate
     */
	public String getSubCardEndDate() {
		return this.subCardEndDate;
	}
	
	/**
	 * @param subCardRegiOrg
	 */
	public void setSubCardRegiOrg(String subCardRegiOrg) {
		this.subCardRegiOrg = subCardRegiOrg;
	}
	
    /**
     * @return subCardRegiOrg
     */
	public String getSubCardRegiOrg() {
		return this.subCardRegiOrg;
	}
	
	/**
	 * @param subCardIndivFolk
	 */
	public void setSubCardIndivFolk(String subCardIndivFolk) {
		this.subCardIndivFolk = subCardIndivFolk;
	}
	
    /**
     * @return subCardIndivFolk
     */
	public String getSubCardIndivFolk() {
		return this.subCardIndivFolk;
	}
	
	/**
	 * @param subCardCusPhone
	 */
	public void setSubCardCusPhone(String subCardCusPhone) {
		this.subCardCusPhone = subCardCusPhone;
	}
	
    /**
     * @return subCardCusPhone
     */
	public String getSubCardCusPhone() {
		return this.subCardCusPhone;
	}
	
	/**
	 * @param subCardOccu
	 */
	public void setSubCardOccu(String subCardOccu) {
		this.subCardOccu = subCardOccu;
	}
	
    /**
     * @return subCardOccu
     */
	public String getSubCardOccu() {
		return this.subCardOccu;
	}
	
	/**
	 * @param subCardFamilyAddrProvince
	 */
	public void setSubCardFamilyAddrProvince(String subCardFamilyAddrProvince) {
		this.subCardFamilyAddrProvince = subCardFamilyAddrProvince;
	}
	
    /**
     * @return subCardFamilyAddrProvince
     */
	public String getSubCardFamilyAddrProvince() {
		return this.subCardFamilyAddrProvince;
	}
	
	/**
	 * @param subCardFamilyAddrCity
	 */
	public void setSubCardFamilyAddrCity(String subCardFamilyAddrCity) {
		this.subCardFamilyAddrCity = subCardFamilyAddrCity;
	}
	
    /**
     * @return subCardFamilyAddrCity
     */
	public String getSubCardFamilyAddrCity() {
		return this.subCardFamilyAddrCity;
	}
	
	/**
	 * @param subCardFamilyAddrZone
	 */
	public void setSubCardFamilyAddrZone(String subCardFamilyAddrZone) {
		this.subCardFamilyAddrZone = subCardFamilyAddrZone;
	}
	
    /**
     * @return subCardFamilyAddrZone
     */
	public String getSubCardFamilyAddrZone() {
		return this.subCardFamilyAddrZone;
	}
	
	/**
	 * @param subCardFamilyDetailAddr
	 */
	public void setSubCardFamilyDetailAddr(String subCardFamilyDetailAddr) {
		this.subCardFamilyDetailAddr = subCardFamilyDetailAddr;
	}
	
    /**
     * @return subCardFamilyDetailAddr
     */
	public String getSubCardFamilyDetailAddr() {
		return this.subCardFamilyDetailAddr;
	}
	
	/**
	 * @param subCardUnitAddrProvince
	 */
	public void setSubCardUnitAddrProvince(String subCardUnitAddrProvince) {
		this.subCardUnitAddrProvince = subCardUnitAddrProvince;
	}
	
    /**
     * @return subCardUnitAddrProvince
     */
	public String getSubCardUnitAddrProvince() {
		return this.subCardUnitAddrProvince;
	}
	
	/**
	 * @param subCardUnitAddrCity
	 */
	public void setSubCardUnitAddrCity(String subCardUnitAddrCity) {
		this.subCardUnitAddrCity = subCardUnitAddrCity;
	}
	
    /**
     * @return subCardUnitAddrCity
     */
	public String getSubCardUnitAddrCity() {
		return this.subCardUnitAddrCity;
	}
	
	/**
	 * @param subCardUnitAddrZone
	 */
	public void setSubCardUnitAddrZone(String subCardUnitAddrZone) {
		this.subCardUnitAddrZone = subCardUnitAddrZone;
	}
	
    /**
     * @return subCardUnitAddrZone
     */
	public String getSubCardUnitAddrZone() {
		return this.subCardUnitAddrZone;
	}
	
	/**
	 * @param subCardUnitDetailAddr
	 */
	public void setSubCardUnitDetailAddr(String subCardUnitDetailAddr) {
		this.subCardUnitDetailAddr = subCardUnitDetailAddr;
	}
	
    /**
     * @return subCardUnitDetailAddr
     */
	public String getSubCardUnitDetailAddr() {
		return this.subCardUnitDetailAddr;
	}
	
	/**
	 * @param subCardPostAddr
	 */
	public void setSubCardPostAddr(String subCardPostAddr) {
		this.subCardPostAddr = subCardPostAddr;
	}
	
    /**
     * @return subCardPostAddr
     */
	public String getSubCardPostAddr() {
		return this.subCardPostAddr;
	}
	
	/**
	 * @param agreedRepayType
	 */
	public void setAgreedRepayType(String agreedRepayType) {
		this.agreedRepayType = agreedRepayType;
	}
	
    /**
     * @return agreedRepayType
     */
	public String getAgreedRepayType() {
		return this.agreedRepayType;
	}
	
	/**
	 * @param agreedRepayCardNo
	 */
	public void setAgreedRepayCardNo(String agreedRepayCardNo) {
		this.agreedRepayCardNo = agreedRepayCardNo;
	}
	
    /**
     * @return agreedRepayCardNo
     */
	public String getAgreedRepayCardNo() {
		return this.agreedRepayCardNo;
	}
	
	/**
	 * @param agreedRepayCusName
	 */
	public void setAgreedRepayCusName(String agreedRepayCusName) {
		this.agreedRepayCusName = agreedRepayCusName;
	}
	
    /**
     * @return agreedRepayCusName
     */
	public String getAgreedRepayCusName() {
		return this.agreedRepayCusName;
	}
	
	/**
	 * @param recomChnl
	 */
	public void setRecomChnl(String recomChnl) {
		this.recomChnl = recomChnl;
	}
	
    /**
     * @return recomChnl
     */
	public String getRecomChnl() {
		return this.recomChnl;
	}
	
	/**
	 * @param suggestLmt
	 */
	public void setSuggestLmt(java.math.BigDecimal suggestLmt) {
		this.suggestLmt = suggestLmt;
	}
	
    /**
     * @return suggestLmt
     */
	public java.math.BigDecimal getSuggestLmt() {
		return this.suggestLmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param returnReason
	 */
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	
    /**
     * @return returnReason
     */
	public String getReturnReason() {
		return this.returnReason;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param businessStage
	 */
	public void setBusinessStage(String businessStage) {
		this.businessStage = businessStage;
	}
	
    /**
     * @return businessStage
     */
	public String getBusinessStage() {
		return this.businessStage;
	}
	
	/**
	 * @param tlAppNo
	 */
	public void setTlAppNo(String tlAppNo) {
		this.tlAppNo = tlAppNo;
	}
	
    /**
     * @return tlAppNo
     */
	public String getTlAppNo() {
		return this.tlAppNo;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerPhone
	 */
	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}
	
    /**
     * @return managerPhone
     */
	public String getManagerPhone() {
		return this.managerPhone;
	}
	
	/**
	 * @param recomId
	 */
	public void setRecomId(String recomId) {
		this.recomId = recomId;
	}
	
    /**
     * @return recomId
     */
	public String getRecomId() {
		return this.recomId;
	}
	
	/**
	 * @param recomName
	 */
	public void setRecomName(String recomName) {
		this.recomName = recomName;
	}
	
    /**
     * @return recomName
     */
	public String getRecomName() {
		return this.recomName;
	}
	
	/**
	 * @param recomPhone
	 */
	public void setRecomPhone(String recomPhone) {
		this.recomPhone = recomPhone;
	}
	
    /**
     * @return recomPhone
     */
	public String getRecomPhone() {
		return this.recomPhone;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}