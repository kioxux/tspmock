/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanAppr;
import cn.com.yusys.yusp.service.IqpLoanApprService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-23 20:06:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploanappr")
public class IqpLoanApprResource {
    @Autowired
    private IqpLoanApprService iqpLoanApprService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanAppr> list = iqpLoanApprService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanAppr>> index(QueryModel queryModel) {
        List<IqpLoanAppr> list = iqpLoanApprService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanAppr>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanAppr> create(@RequestBody IqpLoanAppr iqpLoanAppr) throws URISyntaxException {
        iqpLoanApprService.insert(iqpLoanAppr);
        return new ResultDto<IqpLoanAppr>(iqpLoanAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanAppr iqpLoanAppr) throws URISyntaxException {
        int result = iqpLoanApprService.update(iqpLoanAppr);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String iqpSerno) {
        int result = iqpLoanApprService.deleteByPrimaryKey(pkId, iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param iqpLoanAppr
     * @return
     * @author wzy
     * @date 2021/8/23 20:14
     * @version 1.0.0
     * @desc  新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/saveorupdate")
    protected ResultDto<IqpLoanAppr> saveorupdate(@RequestBody IqpLoanAppr iqpLoanAppr) {
        iqpLoanApprService.saveOrUpdate(iqpLoanAppr);
        return new ResultDto<>(iqpLoanAppr);
    }

    /**
     * @param iqpLoanAppr
     * @return
     * @author wzy
     * @date 2021/8/23 20:14
     * @version 1.0.0
     * @desc  新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querybysernoandnode")
    protected ResultDto<IqpLoanAppr> querybysernoandnode(@RequestBody IqpLoanAppr iqpLoanAppr) {
        IqpLoanAppr result = iqpLoanApprService.queryBySernoAndNode(iqpLoanAppr);
        return new ResultDto<>(result);
    }
}
