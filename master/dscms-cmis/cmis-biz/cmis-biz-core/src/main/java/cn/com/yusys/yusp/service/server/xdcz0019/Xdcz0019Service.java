package cn.com.yusys.yusp.service.server.xdcz0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.req.Xdcz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.resp.Xdcz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0019Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-06-15 20:32:46
 * @修改备注:小贷额度支用申请书生成pdf
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0019Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0019Service.class);

    @Resource
    private ICusClientService icusClientService;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * 小贷额度支用申请书生成pdf
     *
     * @param xdcz0019DataReqDto
     * @return
     */
    public Xdcz0019DataRespDto xdcz0019(Xdcz0019DataReqDto xdcz0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value);
        Xdcz0019DataRespDto xdcz0019DataRespDto = new Xdcz0019DataRespDto();

        String cusName = xdcz0019DataReqDto.getCusName();//客户名称
        String contNo = xdcz0019DataReqDto.getContNo();//合同编号
        String cnContNo = xdcz0019DataReqDto.getCnContNo();//中文合同编号
        BigDecimal loanAmt = xdcz0019DataReqDto.getLoanAmt();//放款金额
        String curType = xdcz0019DataReqDto.getCurType();//币种
        String loanStartDate = xdcz0019DataReqDto.getLoanStartDate();//借款起始日期
        String loanEndDate = xdcz0019DataReqDto.getLoanEndDate();//借款到期日期
        String loanType = xdcz0019DataReqDto.getLoanType();//借款种类
        String loanUseType = xdcz0019DataReqDto.getLoanUseType();//借款用途
        String rateExeMode = xdcz0019DataReqDto.getRateExeMode();//利率执行方式
        String lprInterzone = xdcz0019DataReqDto.getLprInterzone();//lpr区间
        BigDecimal lprBasePoint = xdcz0019DataReqDto.getLprBasePoint();//lpr基点
        BigDecimal yearRate = Optional.ofNullable(xdcz0019DataReqDto.getYearRate()).orElse(BigDecimal.ZERO);//年利率
        String floatRateAdjMode = xdcz0019DataReqDto.getFloatRateAdjMode();//浮动利率调整方式
        String drawIntervalDate = xdcz0019DataReqDto.getDrawIntervalDate();//提款间隔日
        String eiMode = xdcz0019DataReqDto.getEiMode();//结息方式
        String acctNo = xdcz0019DataReqDto.getAcctNo();//账号
        String acctsvcr = xdcz0019DataReqDto.getAcctsvcr();//开户行
        String payType = xdcz0019DataReqDto.getPayType();//支付方式
        String repayType = xdcz0019DataReqDto.getRepayType();//还款方式
        String applyDate = xdcz0019DataReqDto.getApplyDate();//申请日期
        String isdzqy = xdcz0019DataReqDto.getIsdzqy();//是否电子签约：1-是 0-否


        try {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            //***********************************合同模板名字；待生成文件名称***********************************
            String TempleteName = "xwyx-edzyht.cpt";// 合同模板名字 xdedjkzysqs2020.docx
            String saveFileName = "xdedjkzysqs2021_" + contNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
            //***********************************查询服务器配置***********************************
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            //**********************************查询存储地址信息**********************************
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();
            // ********************************************查询合同信息********************************************
            logger.info("根据合同编号【{}】查询合同信息开始", contNo);
            CtrLoanCont ctrLoanCont = Optional.ofNullable(ctrLoanContMapper.selectByPrimaryKey(contNo)).orElse(new CtrLoanCont());
            logger.info("***************根据合同编号【{}】查询合同信息结束,查询结果信息【{}】", contNo, JSON.toJSONString(ctrLoanCont));

            // ********************************************传入帆软报表生成需要的参数********************************************
            HashMap<String, Object> parameterMap = new HashMap<>();
            parameterMap.put("contNo", contNo);//合同编号
            parameterMap.put("cnContNo", ctrLoanCont.getContCnNo());
            String contType = ctrLoanCont.getContType();
            if ("1".equals(contType)) {
                contType = "一般借款合同";
            } else {
                contType = "最高额借款合同";
            }
            parameterMap.put("contType", contType);//合同类型-最高额借款合同；一般借款合同
            parameterMap.put("loanAmt", loanAmt);//借款金额
            parameterMap.put("curType", curType);//币种
            parameterMap.put("loanStartDate", loanStartDate);//借款起始日
            parameterMap.put("loanEndDate", loanEndDate);//借款到期日期
            /**********************借款用途码值转换**
             * 经营性贷款：资金周转
             * 消费性贷款：循环消费是综合消费，一次性用信是按实际借款用途填，抓取合同上的借款用途
             * 无还本续贷是：归还贷款
             * *******************/
            String prdId = ctrLoanCont.getPrdId();
            String repayModel = "";//还款方式
            if (StringUtil.isNotEmpty(prdId)) {
                LmtCrdReplyInfo lmtCrdReplyInfo = Optional.ofNullable(lmtCrdReplyInfoMapper.selectBySurveySerno(ctrLoanCont.getSurveySerno())).orElse(new LmtCrdReplyInfo());
                String isWxbxd = lmtCrdReplyInfo.getIsWxbxd();
                String limitType = lmtCrdReplyInfo.getLimitType();//[{"key":"01","value":"临时额度"},{"key":"02","value":"循环额度"}]
                repayModel = lmtCrdReplyInfo.getRepayMode();
                if ("1".equals(isWxbxd)) {//无还本续贷
                    loanUseType = "归还贷款";
                } else {
                    logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
                    ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                    String prdCode = prdresultDto.getCode();//返回结果
                    String prdType = net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils.EMPTY;
                    if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                        CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                        if (CfgPrdBasicinfoDto != null) {
                            prdType = CfgPrdBasicinfoDto.getPrdType();
                        }
                    }
                    if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType)) {//经营
                        loanUseType = "资金周转";
                    } else if (DscmsBizTzEnum.PRDTYPE_09.key.equals(prdType)) {//消费
                        if ("02".equals(limitType)) {
                            loanUseType = "综合消费";
                        } else {
                            loanUseType = ctrLoanCont.getLoanPurp();
                        }
                    }
                    logger.info("***********调用iCmisCfgClientService查询产品类别*END**************");
                }
                parameterMap.put("loanUseType", loanUseType);//借款用途
            } else {
                parameterMap.put("loanUseType", loanUseType);//借款用途
            }
            /******************************* G：固定 F：浮动****************************/
            if ("G".equals(rateExeMode)) {
                parameterMap.put("rateExeMode", "(1)");//利率执行方式
            } else if ("F".equals(rateExeMode)) {
                parameterMap.put("rateExeMode", "(2)");//利率执行方式
            }
            /******************************* lpr区间 A1-1年期  A2-5年期********************/
            if ("A1".equals(lprInterzone)) {
                parameterMap.put("lprInterzone", "1年期 ");//利率执行方式
                parameterMap.put("rateType", "A1 ");//利率执行方式
            } else if ("A2".equals(lprInterzone)) {
                parameterMap.put("lprInterzone", "5年期");//利率执行方式
                parameterMap.put("rateType", "A2 ");//利率执行方式
            } else {//如果为空
                //根据起始日到期日计算借据期数
                int term = cmisBizXwCommonService.getBetweenMonth(loanStartDate, loanEndDate);
                //根据期数判断lpr区间
                if (term > 60) {
                    lprInterzone = "A2";
                    parameterMap.put("lprInterzone", "5年期");//lpr区间 A1-1年期  A2-5年期
                    parameterMap.put("rateType", "A2 ");//利率执行方式
                } else {
                    lprInterzone = "A1";
                    parameterMap.put("lprInterzone", "1年期 ");//lpr区间 A1-1年期  A2-5年期
                    parameterMap.put("rateType", "A1 ");//利率执行方式
                }
            }
            /******************************年利率********************/
            BigDecimal ExecRateYear = Optional.ofNullable(ctrLoanCont.getExecRateYear()).orElse(BigDecimal.ZERO);
            if (yearRate.compareTo(BigDecimal.ZERO) != 0) {
                parameterMap.put("ExecRateYear", yearRate);//年利率
                ExecRateYear = yearRate.divide(new BigDecimal("100"),8, BigDecimal.ROUND_HALF_UP);
            } else {
                parameterMap.put("ExecRateYear", ExecRateYear.multiply(new BigDecimal("100")));//年利率
            }
            /******************************* bp利率加减判断*
             * 执行利率（年）*100 - lpr利率*100
             * *******************/
            parameterMap.put("lpr_base_point", lprBasePoint);
            BigDecimal curtLprRate = queryLprRate(lprInterzone);
            BigDecimal lprBp = ExecRateYear.multiply(new BigDecimal("10000")).subtract(curtLprRate.multiply(new BigDecimal("10000")));
            lprBp = lprBp.divide(new BigDecimal("1"), 0, BigDecimal.ROUND_HALF_UP);
            if (lprBp.compareTo(BigDecimal.ZERO) < 0) {
                parameterMap.put("lprType", "减");//
                parameterMap.put("lprBp", lprBp);//
            } else {
                parameterMap.put("lprType", "加");//
                parameterMap.put("lprBp", lprBp);//
            }
            /**************************浮动利率调整方式*******************/
            if ("0".equals(floatRateAdjMode)) {
                parameterMap.put("floatRateAdjMode", "①");//浮动利率调整方式
            }
            /********************外币借款利率执行方式*******************/
            parameterMap.put("waibiType1", "/");
            parameterMap.put("waibiType2", "/");
            parameterMap.put("waibiType3", "/");
            parameterMap.put("waibiType4", "/");
            /********************提款方式*******************/
            parameterMap.put("drawIntervalDate", drawIntervalDate);//提款间隔日
            if ("01".equals(eiMode)) {
                eiMode = "1";
            } else if ("02".equals(eiMode)) {
                eiMode = "2";
            } else if ("03".equals(eiMode)) {
                eiMode = "3";
            } else if ("04".equals(eiMode)) {
                eiMode = "4";
            } else if ("05".equals(eiMode)) {
                eiMode = "5";
            }
            parameterMap.put("eiMode", eiMode);//结息方式
            /********************支付方式*******************/
            parameterMap.put("acctNo", acctNo);//账户
            if ("0".equals(acctsvcr)) {// 码值关系
                acctsvcr = "张家港农村商业银行";
            } else {
                acctsvcr = ctrLoanCont.getAcctsvcrName();
            }
            parameterMap.put("acctBank", acctsvcr);//开户行
            if ("01".equals(payType)) {//支付方式 01受托支付 02自主支付 传空不勾选
                payType = "1";
            } else {
                payType = "2";
            }
            parameterMap.put("payType", payType);//支付方式
            /********************还款方式 A001按期付息到期还本 A002等额本息 A003等额本金 A009利随本清*******************/
//            String repayMode = ctrLoanCont.getRepayMode();
//            if (StringUtils.isEmpty(repayType)) {
//                repayType = repayMode;
//            }
            //如果合同表还款方式还是空,则取批复表
            if(StringUtils.isEmpty(repayType)){//20211118直接取批复
                repayType = repayModel;
            }
            //还款方式码值转换
            if ("A001".equals(repayType)) {//按期付息到期还本
                repayType = "1";
            } else if ("A002".equals(repayType)) {//等额本息
                repayType = "2";
            } else if ("A003".equals(repayType)) {//等额本金
                repayType = "3";
            } else if ("A009".equals(repayType)) {//利随本清
                repayType = "4";
            } else if ("A031".equals(repayType)) {//"5年期等额本息
                repayType = "5";
            } else if ("A040".equals(repayType)) {//按期付息,按计划还本
                repayType = "6";
            } else if ("A022".equals(repayType)) {//10年期等额本息
                repayType = "7";
            }
            parameterMap.put("repayType", repayType);
            parameterMap.put("cus_name", cusName);
            parameterMap.put("loanType", loanType);
            parameterMap.put("yearRate", yearRate);//年利率
            parameterMap.put("applyDate", applyDate);
            parameterMap.put("openDay", openDay);
            parameterMap.put("isdzqy", isdzqy);//是否电子签约:1-是 0-否

            logger.info("*****xdcz0019**小贷额度支用申请书生成pdf,生成参数:{}", JSON.toJSONString(parameterMap));
            /********************************************************参数准备完毕,开始调用公共方法生成pdf**********************************************/
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(contNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
            //***********************************生成pdf结束，准备返回值***********************************
            xdcz0019DataRespDto.setPdfDepoAddr(path);
            xdcz0019DataRespDto.setPdfFileName(saveFileName + ".pdf");
            xdcz0019DataRespDto.setFtpAddr(ip);
            xdcz0019DataRespDto.setPort(port);
            xdcz0019DataRespDto.setUserName(username);
            xdcz0019DataRespDto.setPassword(password);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value);
        return xdcz0019DataRespDto;
    }

    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(String newVal) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0.0485");
        try {
            logger.info("根据贷款期限查询lpr利率开始{}", newVal);
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
                logger.info("根据贷款期限查询lpr利率开始{}", curtLprRate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }
}
