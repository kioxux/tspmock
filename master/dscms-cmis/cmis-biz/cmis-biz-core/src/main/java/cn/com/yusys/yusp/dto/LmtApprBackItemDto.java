package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprBackItem
 * @类描述: lmt_appr_back_item数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 11:03:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtApprBackItemDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 申请流水号 **/
	private String serno;
	
	/** 扣分项目编号 **/
	private String backItemId;
	
	/** 扣分项目名称 **/
	private String backItemName;
	
	/** 扣分标准 **/
	private Integer stdBackItemPoint;
	
	/** 扣分分值 **/
	private Integer backItemPoint;
	
	/** 是否存在问题 **/
	private String isExistIssue;
	
	/** 存在问题描述 **/
	private String existIssueDesc;
	
	/** 扣分项目排序 **/
	private Integer backItemOrder;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param backItemId
	 */
	public void setBackItemId(String backItemId) {
		this.backItemId = backItemId == null ? null : backItemId.trim();
	}
	
    /**
     * @return BackItemId
     */	
	public String getBackItemId() {
		return this.backItemId;
	}
	
	/**
	 * @param backItemName
	 */
	public void setBackItemName(String backItemName) {
		this.backItemName = backItemName == null ? null : backItemName.trim();
	}
	
    /**
     * @return BackItemName
     */	
	public String getBackItemName() {
		return this.backItemName;
	}
	
	/**
	 * @param stdBackItemPoint
	 */
	public void setStdBackItemPoint(Integer stdBackItemPoint) {
		this.stdBackItemPoint = stdBackItemPoint;
	}
	
    /**
     * @return StdBackItemPoint
     */	
	public Integer getStdBackItemPoint() {
		return this.stdBackItemPoint;
	}
	
	/**
	 * @param backItemPoint
	 */
	public void setBackItemPoint(Integer backItemPoint) {
		this.backItemPoint = backItemPoint;
	}
	
    /**
     * @return BackItemPoint
     */	
	public Integer getBackItemPoint() {
		return this.backItemPoint;
	}
	
	/**
	 * @param isExistIssue
	 */
	public void setIsExistIssue(String isExistIssue) {
		this.isExistIssue = isExistIssue == null ? null : isExistIssue.trim();
	}
	
    /**
     * @return IsExistIssue
     */	
	public String getIsExistIssue() {
		return this.isExistIssue;
	}
	
	/**
	 * @param existIssueDesc
	 */
	public void setExistIssueDesc(String existIssueDesc) {
		this.existIssueDesc = existIssueDesc == null ? null : existIssueDesc.trim();
	}
	
    /**
     * @return ExistIssueDesc
     */	
	public String getExistIssueDesc() {
		return this.existIssueDesc;
	}
	
	/**
	 * @param backItemOrder
	 */
	public void setBackItemOrder(Integer backItemOrder) {
		this.backItemOrder = backItemOrder;
	}
	
    /**
     * @return BackItemOrder
     */	
	public Integer getBackItemOrder() {
		return this.backItemOrder;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}