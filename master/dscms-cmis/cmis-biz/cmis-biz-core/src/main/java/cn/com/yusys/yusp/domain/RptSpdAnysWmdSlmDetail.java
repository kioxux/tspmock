/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWmdSlmDetail
 * @类描述: rpt_spd_anys_wmd_slm_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-27 21:22:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_wmd_slm_detail")
public class RptSpdAnysWmdSlmDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 交易对手 **/
	@Column(name = "TOPP", unique = false, nullable = true, length = 80)
	private String topp;
	
	/** 交易对手名称 **/
	@Column(name = "TOPP_NAME", unique = false, nullable = true, length = 80)
	private String toppName;
	
	/** 上年所占业务比例 **/
	@Column(name = "LAST_YEAR_BUSI_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearBusiRate;
	
	/** 交易商品 **/
	@Column(name = "TOPP_GOODS", unique = false, nullable = true, length = 80)
	private String toppGoods;
	
	/** 结算方式 **/
	@Column(name = "SETT_TYPE", unique = false, nullable = true, length = 60)
	private String settType;
	
	/** 帐期 **/
	@Column(name = "ACCT_PERIOD", unique = false, nullable = true, length = 40)
	private String acctPeriod;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param topp
	 */
	public void setTopp(String topp) {
		this.topp = topp;
	}
	
    /**
     * @return topp
     */
	public String getTopp() {
		return this.topp;
	}
	
	/**
	 * @param toppName
	 */
	public void setToppName(String toppName) {
		this.toppName = toppName;
	}
	
    /**
     * @return toppName
     */
	public String getToppName() {
		return this.toppName;
	}
	
	/**
	 * @param lastYearBusiRate
	 */
	public void setLastYearBusiRate(java.math.BigDecimal lastYearBusiRate) {
		this.lastYearBusiRate = lastYearBusiRate;
	}
	
    /**
     * @return lastYearBusiRate
     */
	public java.math.BigDecimal getLastYearBusiRate() {
		return this.lastYearBusiRate;
	}
	
	/**
	 * @param toppGoods
	 */
	public void setToppGoods(String toppGoods) {
		this.toppGoods = toppGoods;
	}
	
    /**
     * @return toppGoods
     */
	public String getToppGoods() {
		return this.toppGoods;
	}
	
	/**
	 * @param settType
	 */
	public void setSettType(String settType) {
		this.settType = settType;
	}
	
    /**
     * @return settType
     */
	public String getSettType() {
		return this.settType;
	}
	
	/**
	 * @param acctPeriod
	 */
	public void setAcctPeriod(String acctPeriod) {
		this.acctPeriod = acctPeriod;
	}
	
    /**
     * @return acctPeriod
     */
	public String getAcctPeriod() {
		return this.acctPeriod;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}