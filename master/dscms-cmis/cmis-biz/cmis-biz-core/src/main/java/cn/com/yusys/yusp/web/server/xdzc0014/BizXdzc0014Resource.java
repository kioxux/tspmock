package cn.com.yusys.yusp.web.server.xdzc0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0014.req.Xdzc0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.resp.Xdzc0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0014.Xdzc0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0014:资产池主动还款接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0014Resource.class);

    @Autowired
    private Xdzc0014Service xdzc0014Service;
    /**
     * 交易码：xdzc0014
     * 交易描述：资产池主动还款接口
     *
     * @param xdzc0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池主动还款接口")
    @PostMapping("/xdzc0014")
    protected @ResponseBody
    ResultDto<Xdzc0014DataRespDto> xdzc0014(@Validated @RequestBody Xdzc0014DataReqDto xdzc0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014DataReqDto));
        Xdzc0014DataRespDto xdzc0014DataRespDto = new Xdzc0014DataRespDto();// 响应Dto:资产池主动还款接口
        ResultDto<Xdzc0014DataRespDto> xdzc0014DataResultDto = new ResultDto<>();

        try {
            xdzc0014DataRespDto = xdzc0014Service.xdzc0014Service(xdzc0014DataReqDto);
            xdzc0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, e.getMessage());
            // 封装xdzc0014DataResultDto中异常返回码和返回信息
            xdzc0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0014DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0014DataRespDto到xdzc0014DataResultDto中
        xdzc0014DataResultDto.setData(xdzc0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014DataResultDto));
        return xdzc0014DataResultDto;
    }
}
