/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpLoanTerChg;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 贷款投向调整mapper
 */
public interface IqpLoanTerChgMapper {
    /**
     * 插入从借据表中插入的数据
     *
     * @param iqpLoanTerChg
     * @return
     */
    int insertSelective(IqpLoanTerChg iqpLoanTerChg);

    /**
     * 根据借据编号查询是否存在在途的变更业务
     * @param contNo
     * @return
     */
    int checkIsExistWFByContNo(@Param("contNo") String contNo);

    /**
     * 根据主键查询数据
     * @param iqpSerno
     * @return
     */
    IqpLoanTerChg selectByPrimaryKey(String iqpSerno);

    /**
     * 根据传入的主键宇状态更改审批状态
     * @param iqpSerno
     * @param wfStatus111
     * @return
     */
    int updateApproveStatusByIqpSerno(String iqpSerno, String wfStatus111);

    /**
     * @方法名称: updateAccLoanRepayWayByBillNo
     * @方法描述: 通过借据号更新贷款台账还款方式
     * @参数与返回说明:iqpRepayWayChg
     * @算法描述:
     */
    void updateAccLoanRepayWayByBillNo(IqpLoanTerChg iqpLoanTerChg);

    /**
     * 向iqpRepayPlan表中插入数据
     * @param iqpLoanTerChg
     */
    void insertIntoIqpRepayPlan(IqpLoanTerChg iqpLoanTerChg);

    //根据借据号查询借据信息
    AccLoan selectPvpLoanAppByBillNo(String billNo);

    /**
     * 根据主键更改审批状态
     *
     * @param iqpLoanTerChg
     * @return
     */
    int updateByPrimaryKeySelective(IqpLoanTerChg iqpLoanTerChg);

    List<IqpLoanTerChg> selectByModel(QueryModel model);
}