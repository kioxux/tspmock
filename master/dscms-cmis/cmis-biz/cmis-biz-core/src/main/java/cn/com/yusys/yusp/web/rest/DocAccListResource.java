/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.util.List;
import java.util.Map;
import java.util.Queue;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocAccList;
import cn.com.yusys.yusp.service.DocAccListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAccListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 17:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "档案台账")
@RestController
@RequestMapping("/api/docAccList")
public class DocAccListResource {

    @Autowired
    private DocAccListService docAccListService;

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("档案台账_查询档案台账列表（对应档案销毁）（分页查询）")
    @PostMapping("/query/docDestoryPageList")
    protected ResultDto<List<DocAccList>> docDestoryPageList(@RequestBody QueryModel queryModel) {
        List<DocAccList> list = docAccListService.docDestroyPageList(queryModel);
        return new ResultDto<List<DocAccList>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("档案台账_查询档案台账列表（分页查询）")
    @PostMapping("/query/pageList")
    protected ResultDto<List<DocAccList>> pageList(@RequestBody QueryModel queryModel) {
        List<DocAccList> list = docAccListService.selectByModel(queryModel);
        return new ResultDto<List<DocAccList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案台账_查询档案台账明细内容")
    @PostMapping("/showDocAccList")
    protected ResultDto<DocAccList> showDocAccList(@RequestBody String docSerno) {
        DocAccList docAccList = docAccListService.selectByPrimaryKey(docSerno);
        return new ResultDto<DocAccList>(docAccList);
    }

    /**
     * @函数名称:selectByCondition
     * @函数描述:根据条件查询
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * @author cainingbo_yx
     */
    @GetMapping("/selectByCondition")
    protected ResultDto<List<DocAccList>> selectByCondition(QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" doc_serno desc");
        }
        List<DocAccList> list = docAccListService.selectByModel(queryModel);
        return new ResultDto<List<DocAccList>>(list);
    }

    /**
     * 导出档案台账列表
     * @param queryModel
     * @return
     */
    @PostMapping("/exportDocAccList")
    public ResultDto<ProgressDto> exportDocAccList(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = docAccListService.exportDocAccList(queryModel);
        return ResultDto.success(progressDto);
    }

    /**
     * 获取各类型档案目录年份数据
     * @author jijian_yx
     * @date 2021/7/9 19:19
     **/
    @PostMapping("/getAccTimeList")
    protected ResultDto<Map<String,List<String>>> getAccTimeList(@RequestBody String cusId){
        Map<String,List<String>> map = docAccListService.getAccTimeList(cusId);
        return new ResultDto<Map<String,List<String>>>(map);
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/7/10 14:13
     **/
    @PostMapping("/queryAccListByDocTypeAndYear")
    protected ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(@RequestBody QueryModel queryModel){
        List<DocAccSearchDto> list = docAccListService.queryAccListByDocTypeAndYear(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:getSequences
     * @函数描述:用于获取序列号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSequences")
    public ResultDto<String> getSequences() {
        String daSerno = docAccListService.getSequences();
        return new ResultDto<String>(daSerno);
    }

    /**
     * 更新客户名下未结清的银承、保函、开证、委托贷款台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:00
     **/
    @PostMapping("/updateOtherLoanByCusId")
    public ResultDto<Integer> updateOtherLoanByCusId(@RequestBody Map<String, String> map){
        int result = docAccListService.updateOtherLoanByCusId(map);
        return new ResultDto<>(result);
    }
}
