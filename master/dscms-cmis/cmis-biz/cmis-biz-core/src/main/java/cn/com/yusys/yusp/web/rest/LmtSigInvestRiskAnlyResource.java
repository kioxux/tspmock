/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestRiskAnly;
import cn.com.yusys.yusp.service.LmtSigInvestRiskAnlyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRiskAnlyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 18:59:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestriskanly")
public class LmtSigInvestRiskAnlyResource {
    @Autowired
    private LmtSigInvestRiskAnlyService lmtSigInvestRiskAnlyService;
    @Value("${yusp.file-server.home-path}")
    private String serverPath;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestRiskAnly>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestRiskAnly> list = lmtSigInvestRiskAnlyService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestRiskAnly>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestRiskAnly>> index(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestRiskAnly> list = lmtSigInvestRiskAnlyService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestRiskAnly>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestRiskAnly> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestRiskAnly lmtSigInvestRiskAnly = lmtSigInvestRiskAnlyService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestRiskAnly>(lmtSigInvestRiskAnly);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestRiskAnly> create(@RequestBody LmtSigInvestRiskAnly lmtSigInvestRiskAnly) throws URISyntaxException {
        lmtSigInvestRiskAnlyService.insert(lmtSigInvestRiskAnly);
        return new ResultDto<LmtSigInvestRiskAnly>(lmtSigInvestRiskAnly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestRiskAnly lmtSigInvestRiskAnly) throws URISyntaxException {
        int result = lmtSigInvestRiskAnlyService.update(lmtSigInvestRiskAnly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestRiskAnlyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestRiskAnlyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据serno获取对象
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtSigInvestRiskAnly> selectBySerno(@RequestBody Map condition){
        String serno = (String) condition.get("serno");
        LmtSigInvestRiskAnly lmtSigInvestRiskAnly = lmtSigInvestRiskAnlyService.selectBySerno(serno);
        return new ResultDto<>(lmtSigInvestRiskAnly);
    }


    /**
     * 图片
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtSigInvestRiskAnlyService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:保存主体分析
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateZtfx")
    protected ResultDto<Integer> updateZtfx(@RequestBody LmtSigInvestRiskAnly lmtSigInvestRiskAnly) throws URISyntaxException {
        int result = lmtSigInvestRiskAnlyService.updateZtfx(lmtSigInvestRiskAnly);
        return new ResultDto<Integer>(result);
    }
}
