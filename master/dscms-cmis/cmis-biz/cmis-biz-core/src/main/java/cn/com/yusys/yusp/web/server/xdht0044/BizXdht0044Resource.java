package cn.com.yusys.yusp.web.server.xdht0044;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0044.req.Xdht0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0044.resp.Xdht0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0044.Xdht0044Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:房群客户查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0044:房群客户查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0044Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0044Resource.class);
    @Autowired
    private Xdht0044Service xdht0044Service;

    /**
     * 交易码：xdht0044
     * 交易描述：房群客户查询
     *
     * @param xdht0044DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdht0044:房群客户查询")
    @PostMapping("/xdht0044")
    protected @ResponseBody
    ResultDto<Xdht0044DataRespDto> xdht0044(@Validated @RequestBody Xdht0044DataReqDto xdht0044DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, JSON.toJSONString(xdht0044DataReqDto));
        Xdht0044DataRespDto xdht0044DataRespDto = new Xdht0044DataRespDto();// 响应Dto:房群客户查询
        ResultDto<Xdht0044DataRespDto> xdht0044DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0044DataReqDto));
            xdht0044DataRespDto = xdht0044Service.xdht0044(xdht0044DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0044DataResultDto));

            // 封装xdht0044DataResultDto中正确的返回码和返回信息
            xdht0044DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0044DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, e.getMessage());
            // 封装xdht0044DataResultDto中异常返回码和返回信息
            xdht0044DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0044DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0044DataRespDto到xdht0044DataResultDto中
        xdht0044DataResultDto.setData(xdht0044DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0044.key, DscmsEnum.TRADE_CODE_XDHT0044.value, JSON.toJSONString(xdht0044DataResultDto));
        return xdht0044DataResultDto;
    }
}
