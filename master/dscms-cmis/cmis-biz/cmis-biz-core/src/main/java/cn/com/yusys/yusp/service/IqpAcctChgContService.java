/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpAcctChgCont;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpAcctChgContMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctChgContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-14 09:16:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAcctChgContService {
    private static final Logger log = LoggerFactory.getLogger(IqpAcctChgContService.class);

    @Autowired
    private IqpAcctChgContMapper iqpAcctChgContMapper;

    @Autowired
    private IqpAcctChgDtlService iqpAcctChgDtlService;

    @Autowired
    private IqpAcctService iqpAcctService;

    // 借据账号变更
    @Autowired
    private IqpBillAcctChgAppService iqpBillAcctChgAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAcctChgCont selectByPrimaryKey(String iqpSerno) {
        return iqpAcctChgContMapper.selectByPrimaryKey(iqpSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAcctChgCont> selectAll(QueryModel model) {
        List<IqpAcctChgCont> records = (List<IqpAcctChgCont>) iqpAcctChgContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpAcctChgCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAcctChgCont> list = iqpAcctChgContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpAcctChgCont record) {
        return iqpAcctChgContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpAcctChgCont record) {
        return iqpAcctChgContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpAcctChgCont record) {
        return iqpAcctChgContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpAcctChgCont record) {
        return iqpAcctChgContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpAcctChgContMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAcctChgContMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map updateIqpAcctChgCont(IqpAcctChgCont record) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            int flag = iqpAcctChgContMapper.updateByPrimaryKeySelective(record);
            Map map = JSONObject.parseObject(JSON.toJSONString(record, SerializerFeature.WriteMapNullValue), Map.class);
            if (flag < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATE_FAILED.key, EcbEnum.E_IQP_UPDATE_FAILED.value);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @description 流程发起操作
     * 页面提交后更新审批状态为待发起，查询出当前合同下所有帐号信息（iqp_acct），新增到详情表（iqp_acct_chg_dtl）
     * @return
     * @author yxl
     * @date 2021/1/18 19:40
     */

    public void handleBusinessDataAfterStart(Map paramMap) {
        //更新审批状态
        paramMap.put("approve_status", CmisCommonConstants.WF_STATUS_111);
        int count =iqpAcctChgContMapper.updateApproveStatus(paramMap);
    }

    /**
     *
     * @description 流程结束操作
     * 暂时更新审批状态，TODO 后期要加发送核心或者核算的修改帐号信息的接口
     * @return
     * @author yxl
     * @date 2021/1/18 20:13
     *
     */
    public void handleBusinessDataAfterEnd(String iqpSerno) {
        //1.更新流程审批状态
        Map paramMap =new HashMap<String,Object>();
        IqpAcctChgCont iqpAcctChgCont = iqpAcctChgContMapper.selectByPrimaryKey(iqpSerno);
        QueryModel queryModelByContNo = new QueryModel();

        paramMap.put("approve_status", CmisCommonConstants.WF_STATUS_997);
        paramMap.put("iqp_serno", iqpSerno);
        int count =iqpAcctChgContMapper.updateApproveStatus(paramMap);
        log.info("更新了"+count+"条合同账号信息(通过)！");
        //2.流水号查询合同下所有的账户信息，并更新到iqp_acct表
        if (count>0) {
            paramMap.put("contNo", iqpAcctChgCont.getContNo());
            paramMap.put("acctStatus", '2');
            int numVoid = iqpAcctService.updateAcctStatusByContNoSelective(paramMap);
            log.info("更新了"+numVoid+"条账号信息(作废)！");
            int numAdd = iqpAcctService.insertByContNoAfterApp(paramMap);
            log.info("更新了"+numAdd+"条账号信息(新增)！");
        }


        //3.接口同步到核心或者核算

    }

    /**
     * @return
     * @descripion  打回流程操作
     * 暂时更改审批状态为待发起
     * @verson V1.0
     * @className
     * @methodName
     * @autor yxl
     * @date 2021/1/18 20:18
     */
    public void handleBusinessDataAfteCallBack(String iqpSerno) {
        Map paramMap =new HashMap<String,Object>();
        paramMap.put("approve_status", CmisCommonConstants.WF_STATUS_992);
        paramMap.put("iqp_serno", iqpSerno);
        iqpAcctChgContMapper.updateApproveStatus(paramMap);
    }

    /**
     * @return
     * @descripion 拿回流程操作
     * @verson
     * @classNme
     * @methodNme
     * @autor yxl
     * @dat 2021/1/18 20:47
     */
    public void handleBusinessDataAfteTackBack(String iqpSerno) {
        Map paramMap =new HashMap<String,Object>();
        paramMap.put("approve_status", CmisCommonConstants.WF_STATUS_991);
        paramMap.put("iqp_serno", iqpSerno);
        iqpAcctChgContMapper.updateApproveStatus(paramMap);
    }

    /**
     *
     * @description  否决流程
     * @version  V1.0
     * @className
     * @methodName
     * @return
     * @author
     * @date 2021/1/18 20:48
     *
     */
    public void handleBusinessDataAfterRefuse(String iqpSerno) {
        Map paramMap =new HashMap<String,Object>();
        paramMap.put("approve_status", CmisCommonConstants.WF_STATUS_998);
        paramMap.put("iqp_serno", iqpSerno);
        iqpAcctChgContMapper.updateApproveStatus(paramMap);
    }


    public boolean  queryIsExisitsOrderIntransit(String cont_no){

        return  false;
    }

    /**
     *
     * @description 新增待变更的 合同帐号信息
     * @version
     * @className
     * @methodName
     * @return
     * @author
     * @date 2021/1/21 15:26
     *
     */
    public void addIqpAcctChgDtl(Map map){
        iqpAcctChgContMapper.addIqpAcctChgDtl(map);
    }

    /**
     *
     * @description
     * @version
     * @className
     * @methodName
     * @return
     * @author
     * @date 2021/1/22 11:30
     *
     */
    public boolean checkExistIqpAcctChg(IqpAcctChgCont iqpAcctChgCont){
        Boolean result = false;
        HashMap param = new HashMap();
        // 放入审批中  标志
        param.put("wfExistsFlag", CmisFlowConstants.REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME);
        // 放入需要的操作类型
        param.put("opr_type", CmisCommonConstants.OPR_TYPE_ADD);
        // 放入合同号
        param.put("cont_no",iqpAcctChgCont.getContNo());
        // 放入申请流水号
        param.put("iqp_serno",iqpAcctChgCont.getIqpSerno());
        int numCont = iqpAcctChgContMapper.checkIqpAcctChgContByBillNo(param);
        if(numCont > 0){
            throw new YuspException(EcbEnum.IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_9.key, EcbEnum.IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_9.value);
        }
//        int numLoan = iqpBillAcctChgAppService.checkIqpBillAcctChgAppByBillNo(param);
//        if(numLoan > 0){
//            throw new YuspException(EcbEnum.IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_10.key, EcbEnum.IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_10.value);
//        }
        result = true;
        return result;
    };


    public int updateApproveStatus(Map map){

        return iqpAcctChgContMapper.updateApproveStatus(map);
    }

    /**
     * @方法名称: checkIqpAcctChgContByBillNo
     * @方法描述: 校验是否已存在 在途的合同账号变更信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int checkIqpAcctChgContByBillNo(HashMap param) {
        return iqpAcctChgContMapper.checkIqpAcctChgContByBillNo(param);
    }

    /**
     * @方法名称: insertIqpAcctChgCont
     * @方法描述: 新增合同帐号变更申请表数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Boolean insertIqpAcctChgCont(IqpAcctChgCont iqpAcctChgCont) {
        Boolean result = false;
        int numCont = iqpAcctChgContMapper.insertSelective(iqpAcctChgCont);
        if(numCont < 1){
            throw new YuspException(EcbEnum.IQP_ACCT_CHG_CONT_ADD_EXCEPTION_3.key, EcbEnum.IQP_ACCT_CHG_CONT_ADD_EXCEPTION_3.value);
        }
        int numDtl = iqpAcctChgDtlService.insertDefValueAfterContAdd(iqpAcctChgCont.getIqpSerno());
        if(numDtl < 1){
            throw new YuspException(EcbEnum.IQP_ACCT_CHG_CONT_ADD_EXCEPTION_4.key, EcbEnum.IQP_ACCT_CHG_CONT_ADD_EXCEPTION_4.value);
        }
        result = true;
        return result;
    }

    /**
     * 检查是否存在 合同账号变更 账号
     *
     * @param contNo
     * @param acctNo
     * @return
     */
    public Integer checkAcctUpdApp(String contNo, String acctNo) {
        return iqpAcctChgContMapper.checkAcctUpdApp(contNo, acctNo);
    }

    /*** 
     * @param param 查询参数
     * @return java.lang.Integer
     * @author tangxun
     * @date 2021/4/15 下午9:33
     * @version 1.0.0
     * @desc 查询是否有行外账号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Integer queryOutBankAccount(HashMap<String, String> param) {
        return iqpAcctChgContMapper.queryOutBankAccount(param);
    }
}
