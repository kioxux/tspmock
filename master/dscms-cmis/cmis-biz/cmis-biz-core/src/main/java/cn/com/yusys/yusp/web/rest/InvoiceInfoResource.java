package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.InvoiceInfo;
import cn.com.yusys.yusp.service.InvoiceInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InvoiceInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-31 15:43:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/invoiceinfo")
public class InvoiceInfoResource {
    @Autowired
    private InvoiceInfoService invoiceInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<InvoiceInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<InvoiceInfo> list = invoiceInfoService.selectAll(queryModel);
        return new ResultDto<List<InvoiceInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<InvoiceInfo>> index(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<InvoiceInfo> list = invoiceInfoService.selectByModel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<InvoiceInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/show/{serno}")
    protected ResultDto<InvoiceInfo> show(@PathVariable("serno") String serno) {
        InvoiceInfo invoiceInfo = invoiceInfoService.selectByPrimaryKey(serno);
        return new ResultDto<InvoiceInfo>(invoiceInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<InvoiceInfo> create(@RequestBody InvoiceInfo invoiceInfo) throws URISyntaxException {
        invoiceInfoService.insert(invoiceInfo);
        return new ResultDto<InvoiceInfo>(invoiceInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody InvoiceInfo invoiceInfo) throws URISyntaxException {
        int result = invoiceInfoService.updateSelective(invoiceInfo);
        if (result > 0) {
            return new ResultDto<Integer>(result);
        } else {
            return new ResultDto<Integer>(result).code(EpbEnum.EPB099999.key);
        }
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = invoiceInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = invoiceInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insert
     * @函数描述: 新增发票信息，并且返回
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertReturn")
    protected ResultDto<InvoiceInfo> insertReturn(@RequestBody InvoiceInfo invoiceInfo) {
        ResultDto<InvoiceInfo> result = invoiceInfoService.insertReturn(invoiceInfo);
        return result;
    }
}
