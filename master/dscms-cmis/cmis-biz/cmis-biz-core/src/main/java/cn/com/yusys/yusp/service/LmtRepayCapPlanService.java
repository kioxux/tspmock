/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtRepayCapPlan;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtRepayCapPlanMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.management.Query;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRepayCapPlanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-13 14:10:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRepayCapPlanService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtRepayCapPlanService.class);

    @Resource
    private LmtRepayCapPlanMapper lmtRepayCapPlanMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRepayCapPlan selectByPrimaryKey(String pkId) {
        return lmtRepayCapPlanMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRepayCapPlan> selectAll(QueryModel model) {
        List<LmtRepayCapPlan> records = (List<LmtRepayCapPlan>) lmtRepayCapPlanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRepayCapPlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRepayCapPlan> list = lmtRepayCapPlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRepayCapPlan record) {
        return lmtRepayCapPlanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRepayCapPlan record) {
        return lmtRepayCapPlanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtRepayCapPlan record) {
        return lmtRepayCapPlanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRepayCapPlan record) {
        return lmtRepayCapPlanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtRepayCapPlanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtRepayCapPlanMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveRepayPlan
     * @方法描述: 新增还款计划
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map saveRepayPlan(LmtRepayCapPlan lmtRepayCapPlan) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{

            // 校验是否存在同期数还款计划
            int existNum = isExistLmtRepayCapPlan(lmtRepayCapPlan);
            if(existNum>0){
                throw BizException.error(null, EcbEnum.ECB010087.key, EcbEnum.ECB010087.value);
            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date repayDate = format.parse(lmtRepayCapPlan.getRepayDate());
            Date nowDate = format.parse(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            if(repayDate.compareTo(nowDate) < 0){
                throw BizException.error(null, EcbEnum.ECB010088.key, EcbEnum.ECB010088.value);
            }
            // 数据操作标志为新增
            lmtRepayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtRepayCapPlan.setPkId(UUID.randomUUID().toString());
            log.info(String.format("保存授信申请数据%s-获取当前登录用户数据", (Object) null));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtRepayCapPlan.setInputId(userInfo.getLoginCode());
                lmtRepayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                lmtRepayCapPlan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtRepayCapPlan.setUpdId(userInfo.getLoginCode());
                lmtRepayCapPlan.setUpdBrId(userInfo.getOrg().getCode());
                lmtRepayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            int count = lmtRepayCapPlanMapper.insert(lmtRepayCapPlan);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw BizException.error(null, EcbEnum.CTR_EXCEPTION_DEF.key, EcbEnum.CTR_EXCEPTION_DEF.value);
            }

        } catch (YuspException e) {
//            rtnCode = e.getCode();
//            rtnMsg = e.getMsg();
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("保存还款计划数据出现异常！", e);
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "," + e.getMessage());
//            rtnCode = EpbEnum.EPB099999.key;
//            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    public int isExistLmtRepayCapPlan(LmtRepayCapPlan lmtRepayCapPlan){
        boolean isExistFlg = false;
        QueryModel model = new QueryModel();
        model.addCondition("serno",lmtRepayCapPlan.getSerno());
        model.addCondition("times",lmtRepayCapPlan.getTimes());
        model.addCondition("oprType",CmisCommonConstants.OP_TYPE_01);
        List<LmtRepayCapPlan> lmtRepayCapPlans = selectAll(model);
        return lmtRepayCapPlans.size();
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 跟据流水号进行查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtRepayCapPlan> selectBySerno(String serno) {
        Map map = new HashMap();
        map.put("serno", serno);
        map.put("oprType", CmisCommonConstants.ADD_OPR);
        map.put("sort","times asc");
        List<LmtRepayCapPlan> list = lmtRepayCapPlanMapper.selectByParams(map);
        return list;
    }

    /**
     * @方法名称: deleteByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map deleteByPkId(LmtRepayCapPlan lmtRepayCapPlan){
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{
            int count = lmtRepayCapPlanMapper.updateByPkId(lmtRepayCapPlan);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除还款计划情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    };

    /**
     * @方法名称: copyLmtRepayPlan
     * @方法描述: 复议，复审，变更时将原担保关系挂靠在新的授信分项流水号下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyLmtRepayPlan(String originSerno,String newSerno) {
        LmtRepayCapPlan lmtRepayCapPlan = null;
        Map map = new HashMap();
        map.put("serno",originSerno);
        List<LmtRepayCapPlan> list = lmtRepayCapPlanMapper.selectByParams(map);
        if(list != null && list.size() >0){
            for (int i = 0; i < list.size(); i++) {
                lmtRepayCapPlan = list.get(i);
                lmtRepayCapPlan.setSerno(newSerno);
                int count = lmtRepayCapPlanMapper.insert(lmtRepayCapPlan);
                if (count != 1){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtRepayCapPlan> selectByIqpSerno( QueryModel queryModel) {
        return lmtRepayCapPlanMapper.selectByIqpSerno(queryModel);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */

    public boolean addOrUpdateAllTable(List<LmtRepayCapPlan> list) {
        for(LmtRepayCapPlan lmtRepayCapPlan : list){
            // 判断当前行是否已存在
            LmtRepayCapPlan record = selectByPrimaryKey(lmtRepayCapPlan.getPkId());
            if(record != null){
                // 更新操作
                User userInfo = SessionUtils.getUserInformation();
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                lmtRepayCapPlan.setUpdId(userInfo.getLoginCode());
                lmtRepayCapPlan.setUpdBrId(userInfo.getOrg().getCode());
                lmtRepayCapPlan.setUpdDate(nowDate);
                lmtRepayCapPlan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                updateSelective(lmtRepayCapPlan);
            }else{
                // 新增操作
                insertSelective(lmtRepayCapPlan);
            }
        }
        return true;
    }
}
