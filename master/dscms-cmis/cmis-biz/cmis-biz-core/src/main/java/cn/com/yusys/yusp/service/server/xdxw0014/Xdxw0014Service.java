package cn.com.yusys.yusp.service.server.xdxw0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0014.req.Xdxw0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.resp.Xdxw0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.web.server.xdxw0014.BizXdxw0014Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 优企贷共借人签订
 *
 * @author sunzhen
 * @version 1.0
 */
@Service
public class Xdxw0014Service {

    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0014Resource.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易描述：优企贷共借人签订
     *
     * @param xdxw0014DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0014DataRespDto updateSignStatByCommonCertNo(Xdxw0014DataReqDto xdxw0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataReqDto));
        //返回对象
        Xdxw0014DataRespDto xdxw0014DataRespDto = new Xdxw0014DataRespDto();
        int result = 0;//执行结果
        try {
            String commonCertNo = xdxw0014DataReqDto.getCommonCertNo();//共借人证件号
            String taskSerno = xdxw0014DataReqDto.getTaskSerno();//优企贷待办流水
            String imageSerno1 = xdxw0014DataReqDto.getImageSerno1();//影像流水号1
            String imageSerno2 = xdxw0014DataReqDto.getImageSerno2();//影像流水号2
            if (StringUtils.isBlank(commonCertNo)) {
                throw new Exception("共借人证件号为空！");
            }
            if (StringUtils.isBlank(taskSerno)) {
                throw new Exception("优企贷待办流水为空！");
            }
            if (StringUtils.isBlank(imageSerno1)) {
                throw new Exception("影像流水号1为空！");
            }
            if (StringUtils.isBlank(imageSerno2)) {
                throw new Exception("影像流水号2为空！");
            }

            result = ctrLoanContMapper.updateSignStatByCommonCertNo(xdxw0014DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, result);
            if (result > 0) {//优企贷共借人签订成功
                xdxw0014DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                xdxw0014DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
            } else {//优企贷共借人签订失败
                xdxw0014DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
                xdxw0014DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);// 描述信息
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdxw0014DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
            xdxw0014DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value + ":" + e.getMessage());// 描述信息
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value);
        return xdxw0014DataRespDto;
    }
}
