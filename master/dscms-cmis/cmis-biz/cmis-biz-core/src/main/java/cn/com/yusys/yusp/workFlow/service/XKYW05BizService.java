package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CreditCardLargeLoanApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CreditCardLargeLoanAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @className SingleBatchApp
 * @Description 业务展期
 * @Date 2020/12/21 : 10:48
 */
@Service
public class XKYW05BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(XKYW05BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CreditCardLargeLoanAppService CreditCardLargeLoanAppService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String extSerno = instanceInfo.getBizId();
        updatFlowParam(instanceInfo);
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
//                CreditCardLargeLoanAppService.checkNameAndStatus(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                CreditCardLargeLoanAppService.handleBusinessDataAfterEnd(extSerno);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 以流程标识-进入流程
     * @param instanceInfo
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.XKYW05.equals(flowCode);
    }

    //更新状态
    public void updateStatus(String serno,String state){
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(serno);
        CreditCardLargeLoanApp.setApproveStatus(state);
        CreditCardLargeLoanAppService.updateSelective(CreditCardLargeLoanApp);
    }

    /**
     * 更新流程变量-审批状态
     * @author wzy
     * @date 2021-9-6 09:34:28
     **/
    private void updatFlowParam (ResultInstanceDto resultInstanceDto) {
        String serno = resultInstanceDto.getBizId();
        log.info("更新流程"+serno+"变量开始");
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = resultInstanceDto.getParam();
        log.info("流程变量前:" + params.toString());
        params.put("certCode",CreditCardLargeLoanApp.getCertCode());
        params.put("cardNo",CreditCardLargeLoanApp.getCardNo());
        params.put("loanAmount",CreditCardLargeLoanApp.getLoanAmount());
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        }
        log.info("流程变量后:" + params.toString());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }
}
