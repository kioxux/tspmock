package cn.com.yusys.yusp.dto;


import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSubPrd;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class LmtReplyAccOperAppSubDto {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    private String pkId;

    /** 申请流水号 **/
    private String serno;

    /** 分项流水号 **/
    private String subSerno;

    /** 成员客户编号 **/
    private String cusId;

    /** 成员客户名称 **/
    private String cusName;

    /** 授信台账编号 **/
    private String lmtAccNo;

    /** 授信品种编号 **/
    private String accSubNo;

    /** 授信品种名称  分项品种表保持一致 **/
    private String lmtBizTypeName;

    /** 是否预授信额度 **/
    private String isPreLmt;

    /** 担保方式 **/
    private String guarMode;

    /** 授信额度 **/
    private java.math.BigDecimal lmtAmt;

    /**
     * 分项下的分项产品
     **/
    private List<LmtReplyAccOperAppSubPrd> children;

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param subSerno
     */
    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    /**
     * @return subSerno
     */
    public String getSubSerno() {
        return this.subSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param accSubNo
     */
    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    /**
     * @return accSubNo
     */
    public String getAccSubNo() {
        return this.accSubNo;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    /**
     * @param lmtBizTypeName
     */
    public void setLmtBizTypeName(String lmtBizTypeName) {
        this.lmtBizTypeName = lmtBizTypeName;
    }

    /**
     * @return lmtBizTypeName
     */
    public String getLmtBizTypeName() {
        return this.lmtBizTypeName;
    }

    /**
     * @param isPreLmt
     */
    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    /**
     * @return isPreLmt
     */
    public String getIsPreLmt() {
        return this.isPreLmt;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param lmtAmt
     */
    public void setLmtAmt(java.math.BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    /**
     * @return lmtAmt
     */
    public java.math.BigDecimal getLmtAmt() {
        return this.lmtAmt;
    }

    public List<LmtReplyAccOperAppSubPrd> getChildren() {
        return children;
    }

    public void setChildren(List<LmtReplyAccOperAppSubPrd> children) {
        this.children = children;
    }
}
