package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0082Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0082Resource
 * @类描述: 受托账号校验
 * @功能描述: 受托账号校验
 * @创建人: hubp
 * @创建时间:
 * @修改备注: 2021年8月11日08:41:17
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0082受托账号校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0082")
public class RiskItem0082Resource {

    @Autowired
    private RiskItem0082Service riskItem0082Service;
    /**
     * @方法名称: riskItem0082
     * @方法描述: 受托账号校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: hubp
     * @创建时间: 2021年8月11日08:41:17
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "受托账号校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0082(@RequestBody QueryModel queryModel) {
        if (queryModel.getCondition().size() == 0) {
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0082Service.riskItem0082(queryModel));
    }
}
