/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CtrEntrustLoanContShowDto;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.LmtGrpSubDto;
import cn.com.yusys.yusp.service.CtrEntrustLoanContService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrEntrustLoanContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-15 14:31:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "委托贷款合同管理")
@RequestMapping("/api/ctrentrustloancont")
public class CtrEntrustLoanContResource {
    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrEntrustLoanCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.selectAll(queryModel);
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrEntrustLoanCont>> index(QueryModel queryModel) {
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrEntrustLoanCont> show(@PathVariable("pkId") String pkId) {
        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrEntrustLoanCont>(ctrEntrustLoanCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrEntrustLoanCont> create(@RequestBody CtrEntrustLoanCont ctrEntrustLoanCont) throws URISyntaxException {
        ctrEntrustLoanContService.insert(ctrEntrustLoanCont);
        return new ResultDto<CtrEntrustLoanCont>(ctrEntrustLoanCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrEntrustLoanCont ctrEntrustLoanCont) throws URISyntaxException {
        int result = ctrEntrustLoanContService.update(ctrEntrustLoanCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrEntrustLoanContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrEntrustLoanContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * 合同签订保存方法
     * @param params
     * @return
     */
    @ApiOperation("合同签订保存方法")
    @PostMapping("/updateSign")
    protected ResultDto<Map> updateSign(@RequestBody Map params) {
        Map rtnData   = ctrEntrustLoanContService.updateSign(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 合同注销保存方法
     * @param params
     * @return
     */
    @ApiOperation("合同注销保存方法")
    @PostMapping("/updateLogout")
    protected ResultDto<Map> updateLogout(@RequestBody Map params) throws ParseException {
        Map rtnData   = ctrEntrustLoanContService.updateLogout(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 合同注销保存方法
     * @param params
     * @return
     */
    @ApiOperation("合同注销保存方法")
    @PostMapping("/updateLogouthis")
    protected ResultDto<Map> updateLogouthis(@RequestBody Map params) throws ParseException {
        Map rtnData   = ctrEntrustLoanContService.updateLogoutHis(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 合同选择框查询方法
     * @param queryModel
     * @return
     */
    @ApiOperation("合同选择框查询方法")
    @PostMapping("/selectctrloan")
    protected ResultDto<List<CtrEntrustLoanContShowDto>> getSubInfo(@RequestBody QueryModel queryModel) throws URISyntaxException {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        List<CtrEntrustLoanContShowDto> result = ctrEntrustLoanContService.selectCtrCont(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrEntrustLoanContShowDto>>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrEntrustLoanCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.toSignlist(queryModel);
        ResultDto<List<CtrHighAmtAgrCont>> resultDto = new ResultDto<List<CtrHighAmtAgrCont>>();// 方法返回对象
        PageHelper.clearPage();
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrEntrustLoanCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlistforzhcx")
    protected ResultDto<List<CtrEntrustLoanCont>> toSignlistForZhcx(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.toSignlistForZhcx(queryModel);
        ResultDto<List<CtrHighAmtAgrCont>> resultDto = new ResultDto<List<CtrHighAmtAgrCont>>();// 方法返回对象
        PageHelper.clearPage();
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrEntrustLoanCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrEntrustLoanCont>>(list);
    }

    /**
     * @函数名称:queryCtrCvrgContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrEntrustloancontdatabyserno")
    protected ResultDto<CtrEntrustLoanCont> queryctrEntrustLoanContDataBySerno(@RequestBody String serno) {
        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByIqpSerno(serno);
        return new ResultDto<CtrEntrustLoanCont>(ctrEntrustLoanCont);
    }

    /**
     * @函数名称:queryCtrEntrustContDataByContNo
     * @函数描述:根据合同号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据合同号查询合同信息")
    @PostMapping("/queryctrentrustcontdatabycontno")
    protected ResultDto<CtrEntrustLoanCont> queryCtrEntrustContDataByContNo(@RequestBody String contNo) {
        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(contNo);
        return new ResultDto<CtrEntrustLoanCont>(ctrEntrustLoanCont);
    }

    /**
     * @函数名称:queryCtrEntrustContDataByMap
     * @函数描述:根据合同号查询合同信息
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCtrEntrustContDataByMap")
    protected ResultDto<CtrEntrustLoanCont> queryCtrEntrustContDataByMap(@RequestBody Map map) {
        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo((String)map.get("contNo"));
        return new ResultDto<CtrEntrustLoanCont>(ctrEntrustLoanCont);
    }
}
