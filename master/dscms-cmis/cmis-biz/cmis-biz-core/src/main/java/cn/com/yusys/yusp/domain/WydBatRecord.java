/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;


import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: WydBatRecord
 * @类描述: wyd_bat_record数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-27 17:14:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "wyd_bat_record")
public class WydBatRecord {
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 是否可处理标志 01：可处理 02：不可处理 **/
	@Column(name = "MSG_OPFLAG", unique = false, nullable = true, length = 2)
	private String msgOpflag;
	
	/** 业务日期 **/
	@Column(name = "MSG_BUZDATE", unique = false, nullable = true, length = 10)
	private String msgBuzdate;
	
	/** 是否人工 **/
	@Column(name = "IS_ARTIFICIAL", unique = false, nullable = true, length = 1)
	private String isArtificial;
	
	/** 批处理状态 **/
	@Column(name = "BAT_STATUS", unique = false, nullable = true, length = 30)
	private String batStatus;
	
	/** 跑批开始时间 **/
	@Column(name = "BAT_START_TIME", unique = false, nullable = true, length = 30)
	private String batStartTime;
	
	/** 跑批结束时间 **/
	@Column(name = "BAT_END_TIME", unique = false, nullable = true, length = 30)
	private String batEndTime;
	
	/** 跑批失败信息 **/
	@Column(name = "BAT_FAILED_MSG", unique = false, nullable = true, length = 512)
	private String batFailedMsg;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param msgOpflag
	 */
	public void setMsgOpflag(String msgOpflag) {
		this.msgOpflag = msgOpflag;
	}
	
    /**
     * @return msgOpflag
     */
	public String getMsgOpflag() {
		return this.msgOpflag;
	}
	
	/**
	 * @param msgBuzdate
	 */
	public void setMsgBuzdate(String msgBuzdate) {
		this.msgBuzdate = msgBuzdate;
	}
	
    /**
     * @return msgBuzdate
     */
	public String getMsgBuzdate() {
		return this.msgBuzdate;
	}
	
	/**
	 * @param isArtificial
	 */
	public void setIsArtificial(String isArtificial) {
		this.isArtificial = isArtificial;
	}
	
    /**
     * @return isArtificial
     */
	public String getIsArtificial() {
		return this.isArtificial;
	}
	
	/**
	 * @param batStatus
	 */
	public void setBatStatus(String batStatus) {
		this.batStatus = batStatus;
	}
	
    /**
     * @return batStatus
     */
	public String getBatStatus() {
		return this.batStatus;
	}
	
	/**
	 * @param batStartTime
	 */
	public void setBatStartTime(String batStartTime) {
		this.batStartTime = batStartTime;
	}
	
    /**
     * @return batStartTime
     */
	public String getBatStartTime() {
		return this.batStartTime;
	}
	
	/**
	 * @param batEndTime
	 */
	public void setBatEndTime(String batEndTime) {
		this.batEndTime = batEndTime;
	}
	
    /**
     * @return batEndTime
     */
	public String getBatEndTime() {
		return this.batEndTime;
	}
	
	/**
	 * @param batFailedMsg
	 */
	public void setBatFailedMsg(String batFailedMsg) {
		this.batFailedMsg = batFailedMsg;
	}
	
    /**
     * @return batFailedMsg
     */
	public String getBatFailedMsg() {
		return this.batFailedMsg;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}