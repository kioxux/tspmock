/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCloestInfo
 * @类描述: lmt_guare_cloest_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 20:01:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_guare_cloest_info")
public class LmtGuareCloestInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 楼盘名称 **/
	@Column(name = "BUILDING_NAME", unique = false, nullable = true, length = 200)
	private String buildingName;
	
	/** 楼栋 **/
	@Column(name = "BUILDING", unique = false, nullable = true, length = 100)
	private String building;
	
	/** 楼层 **/
	@Column(name = "FLOOR", unique = false, nullable = true, length = 20)
	private String floor;
	
	/** 房间号 **/
	@Column(name = "ROOM_NO", unique = false, nullable = true, length = 60)
	private String roomNo;
	
	/** 估价 **/
	@Column(name = "ASS_EVA_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assEvaAmt;
	
	/** 总价 **/
	@Column(name = "TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalAmt;
	
	/** 面积 **/
	@Column(name = "SQU", unique = false, nullable = true, length = 100)
	private String squ;
	
	/** 地址 **/
	@Column(name = "ADDR", unique = false, nullable = true, length = 500)
	private String addr;
	
	/** 查询人员 **/
	@Column(name = "QRY_USER", unique = false, nullable = true, length = 80)
	private String qryUser;
	
	/** 查询日期 **/
	@Column(name = "QRY_DATE", unique = false, nullable = true, length = 20)
	private String qryDate;
	
	/** 查询人员工号 **/
	@Column(name = "QRY_ID", unique = false, nullable = true, length = 20)
	private String qryId;
	
	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 11)
	private String phone;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	
    /**
     * @return buildingName
     */
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param building
	 */
	public void setBuilding(String building) {
		this.building = building;
	}
	
    /**
     * @return building
     */
	public String getBuilding() {
		return this.building;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
    /**
     * @return floor
     */
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param roomNo
	 */
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	
    /**
     * @return roomNo
     */
	public String getRoomNo() {
		return this.roomNo;
	}
	
	/**
	 * @param assEvaAmt
	 */
	public void setAssEvaAmt(java.math.BigDecimal assEvaAmt) {
		this.assEvaAmt = assEvaAmt;
	}
	
    /**
     * @return assEvaAmt
     */
	public java.math.BigDecimal getAssEvaAmt() {
		return this.assEvaAmt;
	}
	
	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	
    /**
     * @return totalAmt
     */
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(String squ) {
		this.squ = squ;
	}
	
    /**
     * @return squ
     */
	public String getSqu() {
		return this.squ;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
    /**
     * @return addr
     */
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param qryUser
	 */
	public void setQryUser(String qryUser) {
		this.qryUser = qryUser;
	}
	
    /**
     * @return qryUser
     */
	public String getQryUser() {
		return this.qryUser;
	}
	
	/**
	 * @param qryDate
	 */
	public void setQryDate(String qryDate) {
		this.qryDate = qryDate;
	}
	
    /**
     * @return qryDate
     */
	public String getQryDate() {
		return this.qryDate;
	}
	
	/**
	 * @param qryId
	 */
	public void setQryId(String qryId) {
		this.qryId = qryId;
	}
	
    /**
     * @return qryId
     */
	public String getQryId() {
		return this.qryId;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}