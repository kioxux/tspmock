/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.IqpRepayPlan;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.service.CtrLoanContService;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.service.RepayCapPlanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RepayCapPlanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 72908
 * @创建时间: 2021-05-06 17:42:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/repaycapplan")
public class RepayCapPlanResource {
    @Autowired
    private RepayCapPlanService repayCapPlanService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RepayCapPlan>> query() {
        QueryModel queryModel = new QueryModel();
        List<RepayCapPlan> list = repayCapPlanService.selectAll(queryModel);
        return new ResultDto<List<RepayCapPlan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<RepayCapPlan>> index(@RequestBody QueryModel queryModel) {
        List<RepayCapPlan> list = repayCapPlanService.selectByModel(queryModel);
        return new ResultDto<List<RepayCapPlan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RepayCapPlan> show(@PathVariable("pkId") String pkId) {
        RepayCapPlan repayCapPlan = repayCapPlanService.selectByPrimaryKey(pkId);
        return new ResultDto<RepayCapPlan>(repayCapPlan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<RepayCapPlan> create(@RequestBody RepayCapPlan repayCapPlan) throws URISyntaxException {
        repayCapPlanService.insertSelective(repayCapPlan);
        return new ResultDto<RepayCapPlan>(repayCapPlan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RepayCapPlan repayCapPlan) throws URISyntaxException {
        int result = repayCapPlanService.updateSelective(repayCapPlan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = repayCapPlanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = repayCapPlanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryModel
     * @函数描述:贷款出账申请还本计划分页查询
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("还本计划分页查询")
    @PostMapping("/queryRepaycapplan")
    protected ResultDto<List<RepayCapPlan>> queryRepaycapplan(@RequestBody QueryModel queryModel) {
        List<RepayCapPlan> list = repayCapPlanService.queryRepaycapplan(queryModel);
        return new ResultDto<List<RepayCapPlan>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("还本计划新增")
    @PostMapping("/saverepaycapplan")
    protected ResultDto<Map> saveRepayCapPlan(@RequestBody RepayCapPlan repayCapPlan) throws URISyntaxException {
        ResultDto<RepayCapPlan> resultDto = new ResultDto<RepayCapPlan>();
        Map result = repayCapPlanService.saveRepayCapPlan(repayCapPlan);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:params
     * @函数描述:还本计划查看
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("还本计划查看")
    @PostMapping("/showdetialrepay")
    protected ResultDto<Object> showDetialRepay(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        RepayCapPlan studyDemo = repayCapPlanService.selectByPrimaryKey((String)params.get(("pkId")));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:params
     * @函数描述:还本计划修改
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("还本计划修改")
    @PostMapping("/updaterepaycapplan")
    public ResultDto<Map> updateRepayCapPlan(@RequestBody RepayCapPlan repayCapPlan){
        Map rtnData = repayCapPlanService.updateRepayCapPlan(repayCapPlan);
        return new ResultDto<>(rtnData);
    }

    /**
     * 还本计划逻辑删除
     * @param condition
     * @return
     */
    @ApiOperation("还本计划逻辑删除")
    @PostMapping("/deleteLogicRepayCapPlan")
    public ResultDto<Integer> deleteLogicRepayCapPlan(@RequestBody Map condition){
        Integer result = repayCapPlanService.deleteLogicRepayCapPlan(condition);
        return new ResultDto<>(result);
    }

    /**
     * 还本计划新增
     * @param repayCapPlan
     * @return
     */
    @ApiOperation("还本计划新增")
    @PostMapping("/insertRepayCapPlan")
    public ResultDto<Integer> insertRepayCapPlan(@RequestBody RepayCapPlan repayCapPlan){
        Integer result = repayCapPlanService.insertRepayCapPlan(repayCapPlan);
        return new ResultDto<>(result);
    }

    /**
     * 还本计划分页
     * @param queryModel
     * @return
     */
    @ApiOperation("还本计划分页")
    @PostMapping("/selectListByPageData")
    public ResultDto<List<RepayCapPlan>> selectListByPageData(@RequestBody QueryModel queryModel){
        List<RepayCapPlan> repayCapPlans = repayCapPlanService.selectListByPageData(queryModel);
        return new ResultDto<>(repayCapPlans);
    }


    /**
     * 修改还本计划分页
     * @param
     * @return
     */
    @ApiOperation("还本计划分页")
    @PostMapping("/changeselectListByPageData")
    public ResultDto<List<RepayCapPlan>> changeselectListByPageData(@RequestBody Map<String,String> params){
        String htSerno = params.get("serno");//流水号
        String contNo = params.get("contNo");//合同号
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        // 按比例计算金额
        // 获取本次申请金额
        String appAmt = params.get("appAmt");
        // 合同金额
        BigDecimal contAmt = ctrLoanCont.getContAmt();
        // 获取比例
        BigDecimal rate = BigDecimal.ZERO;
        // 申请金额
        BigDecimal amt = BigDecimal.ZERO;
        if(!"".equals(appAmt) && null != appAmt){
            amt = new BigDecimal(appAmt);
            rate = amt.divide(contAmt,4,BigDecimal.ROUND_HALF_UP);
        }
        String serno = ctrLoanCont.getIqpSerno();//引入合同的流水号
        List<RepayCapPlan> repayCapPlans = repayCapPlanService.selectBySerno(serno);//查出合同的还款计划
        repayCapPlanService.deleteBySerno(htSerno);
        // 比例大于0时
        BigDecimal sumAmt = BigDecimal.ZERO; // 记录金额
        if(rate.compareTo(new BigDecimal("0")) == 1){
            for (int i = 0; i <repayCapPlans.size() ; i++) {
                // 金额计算
                BigDecimal repayAmt = (repayCapPlans.get(i).getRepayAmt().multiply(rate)).setScale(0,BigDecimal.ROUND_DOWN);
                sumAmt = sumAmt.add(repayAmt);
            }
        } else {
            //直接插入
            for (int i = 0; i <repayCapPlans.size() ; i++) {
                // 金额计算
                RepayCapPlan repayCapPlanNow = new RepayCapPlan();
                repayCapPlanNow.setPkId(repayCapPlans.get(i).getPkId());
                repayCapPlanNow.setSerno(htSerno);
                repayCapPlanNow.setTerms(repayCapPlans.get(i).getTerms());
                repayCapPlanNow.setRepayDate(repayCapPlans.get(i).getRepayDate());
                repayCapPlanNow.setRepayAmt(repayCapPlans.get(i).getRepayAmt());
                repayCapPlanNow.setOprType(repayCapPlans.get(i).getOprType());
                repayCapPlanNow.setInputId(repayCapPlans.get(i).getInputId());
                repayCapPlanNow.setInputBrId(repayCapPlans.get(i).getInputBrId());
                repayCapPlanNow.setInputDate(repayCapPlans.get(i).getInputDate());
                repayCapPlanNow.setUpdId(repayCapPlans.get(i).getUpdId());
                repayCapPlanNow.setUpdBrId(repayCapPlans.get(i).getUpdBrId());
                repayCapPlanNow.setUpdDate(repayCapPlans.get(i).getUpdDate());
                repayCapPlanNow.setCreateTime(repayCapPlans.get(i).getCreateTime());
                repayCapPlanNow.setUpdateTime(repayCapPlans.get(i).getUpdateTime());
                repayCapPlanService.insert(repayCapPlanNow);
            }
        }
        // 计算还款累计金额大于0 并且不等于申请金额
        if(sumAmt.compareTo(new BigDecimal("0")) == 1){
                // 申请金额减去 累计金额 = 差额
            BigDecimal endAmt  = amt.subtract(sumAmt);
            for (int i = 0; i <repayCapPlans.size() ; i++) {
                // 金额计算
                BigDecimal repayAmt = (repayCapPlans.get(i).getRepayAmt().multiply(rate)).setScale(0,BigDecimal.ROUND_DOWN);
                if("1".equals(repayCapPlans.get(i).getTerms().toString())){ // 第一期
                    repayAmt = repayAmt.add(endAmt);
                }
                RepayCapPlan repayCapPlanNow = new RepayCapPlan();
                repayCapPlanNow.setPkId(repayCapPlans.get(i).getPkId());
                repayCapPlanNow.setSerno(htSerno);
                repayCapPlanNow.setTerms(repayCapPlans.get(i).getTerms());
                repayCapPlanNow.setRepayDate(repayCapPlans.get(i).getRepayDate());
                repayCapPlanNow.setRepayAmt(repayAmt);
                repayCapPlanNow.setOprType(repayCapPlans.get(i).getOprType());
                repayCapPlanNow.setInputId(repayCapPlans.get(i).getInputId());
                repayCapPlanNow.setInputBrId(repayCapPlans.get(i).getInputBrId());
                repayCapPlanNow.setInputDate(repayCapPlans.get(i).getInputDate());
                repayCapPlanNow.setUpdId(repayCapPlans.get(i).getUpdId());
                repayCapPlanNow.setUpdBrId(repayCapPlans.get(i).getUpdBrId());
                repayCapPlanNow.setUpdDate(repayCapPlans.get(i).getUpdDate());
                repayCapPlanNow.setCreateTime(repayCapPlans.get(i).getCreateTime());
                repayCapPlanNow.setUpdateTime(repayCapPlans.get(i).getUpdateTime());
                repayCapPlanService.insert(repayCapPlanNow);
            }
        }
        List<RepayCapPlan> repayCapPlans4Other = repayCapPlanService.selectBySerno(htSerno);// 查出还款计划
        return new ResultDto<List<RepayCapPlan>>(repayCapPlans4Other);
    }


    /**
     * 查看授信分项明细还本计划
     */
    @PostMapping("/getAppSubPrdRepay")
    protected ResultDto<List<RepayCapPlan>> getAppSubPrdRepay(@RequestBody Map map) throws Exception {
        String serno = map.get("serno").toString();
        return new ResultDto<List<RepayCapPlan>>(repayCapPlanService.getRepayCapPlanData(map));
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addOrUpdateAllTable")
    protected ResultDto<Boolean> addOrUpdateAllTable(@RequestBody List<RepayCapPlan> list) {
        boolean result = repayCapPlanService.addOrUpdateAllTable(list);
        return new ResultDto<Boolean>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPkId")
    protected ResultDto<Integer> deleteByPkId(@RequestBody Map map) {
        int result = 0;
        RepayCapPlan repayCapPlan = repayCapPlanService.selectByPrimaryKey((String) map.get("pkId"));
        if(repayCapPlan != null){
            repayCapPlan.setOprType(CommonConstance.OPR_TYPE_DELETE);
            result = repayCapPlanService.updateSelective(repayCapPlan);
        }else{
            // 不做操作  直接返回
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveRepayPlan
     * @函数描述:新增还款计划
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saverepayplan")
    protected ResultDto<Map> saveRepayPlan(@RequestBody RepayCapPlan repayCapPlan) {
        Map result = repayCapPlanService.saveRepayPlan(repayCapPlan);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:saveRepayPlan
     * @函数描述:新增还款计划
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<List> selectBySerno(@RequestBody String serno) {
        List result = repayCapPlanService.selectBySerno(serno.replaceAll("\"", ""));
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:deleterepayCapPlan
     * @函数描述:逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */

    @PostMapping("/deletelmtrepaycapplan")
    protected ResultDto<Map> deleterepayCapPlan(@RequestBody RepayCapPlan repayCapPlan) {
        Map rtnData = new HashMap();
        rtnData = repayCapPlanService.deleteByPkId(repayCapPlan);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:selectByIqpSerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByIqpSerno")
    protected ResultDto<List<RepayCapPlan>> selectByIqpSerno(@RequestBody QueryModel queryModel) {
        List<RepayCapPlan> result = repayCapPlanService.selectByIqpSerno(queryModel);
        return new ResultDto<List<RepayCapPlan>>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RepayCapPlan>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RepayCapPlan>>(repayCapPlanService.selectByModel(model));
    }

    /**
     * @函数名称:selectByLmtSerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbylmtserno")
    protected ResultDto<List<Map>> selectByLmtSerno(@RequestBody QueryModel queryModel) {
        List<Map> result = repayCapPlanService.selectByLmtSerno(queryModel);
        return new ResultDto<List<Map>>(result);
    }

    /**
     * @函数名称:selectDataByApprSerno
     * @函数描述:根据审批审批流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectdatabyapprserno")
    protected ResultDto<List<Map>> selectDataByApprSerno(@RequestBody QueryModel queryModel) {
        List<Map> result = repayCapPlanService.selectDataByApprSerno(queryModel);
        return new ResultDto<List<Map>>(result);
    }

    /***
     * @param repayCapPlan
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021-8-18 10:32:33
     * @version 1.0.0
     * @desc    根据信息判断是插入还是更新还款信息selectByModel
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/saverepaycapplanretail")
    protected ResultDto<Integer> saveRepayCapPlanRetail(@RequestBody RepayCapPlan repayCapPlan) throws URISyntaxException {
        int result = repayCapPlanService.saveRepayCapPlanRetail(repayCapPlan);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateBySerno
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateBySerno")
    protected ResultDto<Integer> updateBySerno(@RequestBody QueryModel model) throws URISyntaxException {
        int result = repayCapPlanService.updateBySerno(model);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据授信流水号查询还款计划
     * @param map
     * @return
     */
    @PostMapping("/selectByLmtSerno")
    protected ResultDto<List<Map>> selectByLmtSerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<Map>>(repayCapPlanService.selectRapByLmtSerno(serno));
    }
    /**
     * 根据集团授信流水号查询还款计划
     * @param map
     * @return
     */
    @PostMapping("/selectByGrpSerno")
    protected ResultDto<List<Map>> selectByGrpSerno(@RequestBody Map map){
        String grpSerno = map.get("grpSerno").toString();
        return new ResultDto<List<Map>>(repayCapPlanService.selectByGrpSerno(grpSerno));
    }

    /***
     * @param repayCapPlanList
     * @desc    批量插入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/batchsaverepaycapplandetail")
    protected ResultDto<Integer> batchSaveRepayCapPlanDetail(@RequestBody List<RepayCapPlan> repayCapPlanList) throws URISyntaxException {
        int result = repayCapPlanService.batchSaveRepayCapPlanDetail(repayCapPlanList);
        return new ResultDto<Integer>(result);
    }
}