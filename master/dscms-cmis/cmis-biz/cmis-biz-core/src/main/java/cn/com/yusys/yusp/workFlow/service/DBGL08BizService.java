package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantManageAppMapper;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.GuarContRelWarrantService;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @className GuarWarrantManageAppBiz
 * @Description 权证出入库、续借审批流程
 * @author zhengfq
 * @Date 2020/05/19
 */
@Service
public class DBGL08BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DBGL08BizService.class);//定义log

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String logPrefix = resultInstanceDto.getFlowName()+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            // 加载路由条件
            put2VarParam(resultInstanceDto, serno);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto.toString());
                guarWarrantManageAppService.handleBusinessAfterStart(serno);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto.toString());
                guarWarrantManageAppService.handleBusinessAfterEnd(serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarWarrantManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarWarrantManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                guarWarrantManageAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DBGL08.equals(flowCode);
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: dumingdi
     * @创建时间: 2021-08-02 10:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);

        //查询权证对应的核销及转让贷款台账记录数
        int count = accLoanService.countHxAndZrAccLoanRecords(guarWarrantManageApp.getGuarContNo());

        Map<String, Object> params = new HashMap<>();

        if (count>0){
            //涉及核销或转让，走特资部调阅审核岗
            params.put("is","1");
        }else {
            //否则走集中作业放款复审岗
            params.put("is","0");
        }

        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }
}