package cn.com.yusys.yusp.web.server.xdxw0062;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0062.req.Xdxw0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.resp.Xdxw0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0062.Xdxw0062Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0062:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0062Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0062Resource.class);

    @Autowired
	private Xdxw0062Service xdxw0062Service;

    /**
     * 交易码：xdxw0062
     * 交易描述：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
     *
     * @param xdxw0062DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微平台提交推送待办消息，信贷生成批复，客户经理继续审核")
    @PostMapping("/xdxw0062")
    protected @ResponseBody
    ResultDto<Xdxw0062DataRespDto> xdxw0062(@Validated @RequestBody Xdxw0062DataReqDto xdxw0062DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataReqDto));
        Xdxw0062DataRespDto xdxw0062DataRespDto = new Xdxw0062DataRespDto();// 响应Dto:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
        ResultDto<Xdxw0062DataRespDto> xdxw0062DataResultDto = new ResultDto<>();
        try {
			logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataReqDto));
			xdxw0062DataRespDto = xdxw0062Service.saveSurveyAndSendMessage(xdxw0062DataReqDto);
			logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataRespDto));
            // 封装xdxw0062DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0062DataRespDto.getOpFlag();
            String opMessage = xdxw0062DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0062DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0062DataResultDto.setMessage(opMessage);
            } else {
                xdxw0062DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0062DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, e.getMessage());
            // 封装xdxw0062DataResultDto中异常返回码和返回信息
            xdxw0062DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0062DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, e.getMessage());
            // 封装xdxw0062DataResultDto中异常返回码和返回信息
            xdxw0062DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0062DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdxw0062DataRespDto到xdxw0062DataResultDto中
        xdxw0062DataResultDto.setData(xdxw0062DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataRespDto));
        return xdxw0062DataResultDto;
    }
}
