/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.IqpGuarBizRelApp;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpGuarBizRelAppMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarBizRelAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:55:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpGuarBizRelAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpGuarBizRelAppService.class);

    @Autowired
    private IqpGuarBizRelAppMapper iqpGuarBizRelAppMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;//注入业务申请主表的服务
    @Autowired
    private GrtGuarContService grtGuarContService;//注入担保合同的服务

//    @Autowired
//    private UserService userService;//注入通用的用户服务接口
@Autowired
private CtrLoanContService ctrLoanContService;//合同服务
    @Autowired
    private LmtSubService lmtSubService;//额度分项服务

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpGuarBizRelApp selectByPrimaryKey(String pkId) {
        return iqpGuarBizRelAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpGuarBizRelApp> selectAll(QueryModel model) {
        List<IqpGuarBizRelApp> records = (List<IqpGuarBizRelApp>) iqpGuarBizRelAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpGuarBizRelApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpGuarBizRelApp> list = iqpGuarBizRelAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpGuarBizRelApp record) {
        return iqpGuarBizRelAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpGuarBizRelApp record) {
        return iqpGuarBizRelAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpGuarBizRelApp record) {
        return iqpGuarBizRelAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpGuarBizRelApp record) {
        return iqpGuarBizRelAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: selectByGuarContNoKey
     * @方法描述: 根据担保合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectByGuarContNoKey(Map params) {
        int result = 0;
        List<IqpGuarBizRelApp> IqpGuarBizRelAppList = iqpGuarBizRelAppMapper.selectByGuarContNoKey(params);
        if (IqpGuarBizRelAppList != null) {
            result = IqpGuarBizRelAppList.size();
        }
        if (result > 0) {
            throw new YuspException(EcbEnum.GRT_GUAR_CONT_NO_DELETE.key, EcbEnum.GRT_GUAR_CONT_NO_DELETE.value);
        }
        return result;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpGuarBizRelAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpGuarBizRelAppMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpGuarBizRelAppMapper.updateByParamsBySerno(delMap);
    }

    /**
     * 通过入参批量更新数据
     * @param delMap
     * @return
     */
    public int batchUpdateByParams(Map delMap) {
        return iqpGuarBizRelAppMapper.batchUpdateByParams(delMap);
    }

    /**
     * 0、校验该笔担保合同是否存在其他的业务关系
     * 1、校验担保合同是否已经引入
     * 2、校验担保合同的可用金额是否为空
     *
     * @param params
     * @return
     */
    public Map queryIqpGuarBizRelAppExists(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        try {
            String iqpSerno = (String) params.get("iqpSerno");//业务流水号
            String guarContNo = (String) params.get("guarContNo");//担保合同编号
            if (StringUtils.isBlank(iqpSerno) || StringUtils.isBlank(guarContNo)) {
                throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_PARAM_EXCEPTION.value);
            }
            //担保合同类型
            String guarContType = (String) params.get("guarContType");

            //20201230 针对一般担保合同引入，需要校验该笔担保合同是否存在其他业务申请的有效关系数据
            //20210202 针对业务申请校验业务申请是否引用，针对额度申请校验授信关系数据
            if (StringUtils.nonBlank(guarContType) && CmisBizConstants.GTR_CONT_TYPE_NORMAL.equals(guarContType)) {
                Map otherMap = new HashMap();
                String bizTyp = "";
                if (params.containsKey("bizTyp")) {
                    bizTyp = (String) params.get("bizTyp");
                }
                if (StringUtils.nonBlank(bizTyp) && CmisBizConstants.BIZ_TYP_LMT.equals(bizTyp)) {
                    otherMap.put("rType", CmisBizConstants.R_TYPE_1);//查询与授信关联数据
                } else {
                    otherMap.put("rType", CmisBizConstants.R_TYPE_2);//查询与业务关联数据
                }
                otherMap.put("guarContNo", guarContNo);
                otherMap.put("iqpSerno", iqpSerno);

                log.info("业务申请" + iqpSerno + "校验担保合同" + guarContNo + "是否可用-1、是否存在其他有效的业务关系数据");
                List<IqpGuarBizRelApp> otherDataList = iqpGuarBizRelAppMapper.selectOtherDataByParams(otherMap);
                if (CollectionUtils.nonEmpty(otherDataList) && otherDataList.size() > 0) {
                    log.error("校验业务申请" + iqpSerno + "是否存在担保合同" + guarContNo + "-->该笔担保合同存在其他有效的关系数据！");
                    throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_EXISTSOTHER_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_EXISTSOTHER_EXCEPTION.value);
                }
            }


            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);//操作类型为新增的数据
            log.info("校验业务申请" + iqpSerno + "担保合同" + guarContNo + "是否可用-1、关系是否已存在");
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppMapper.selectDataByParams(params);

            if (iqpGuarBizRelAppList != null && iqpGuarBizRelAppList.size() > 0) {
                log.error("校验业务申请" + iqpSerno + "是否存在担保合同" + guarContNo + "-->关系已存在！");
                throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_EXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_EXISTS_EXCEPTION.value);
            }

            log.info("校验业务申请" + iqpSerno + "担保合同" + guarContNo + "是否可用-2、校验可用金额是否为空");
            Map amtMap = getAmtInfoByGuarContNo(params);
            if (EcbEnum.IQP_GBRA_SUCCESS.key.equals((String) amtMap.get("rtnCode"))) {
                BigDecimal usedAmt = (BigDecimal) amtMap.get("usedAmt");
                String guarAmtString = (String) params.get("guarAmt");
                guarAmtString = guarAmtString.replace(",", "");
                BigDecimal guarAmt = new BigDecimal(guarAmtString);
                BigDecimal availableAmt = guarAmt.subtract(usedAmt);
                log.info("校验业务申请" + iqpSerno + "担保合同" + guarContNo + "是否可用-可用金额:" + availableAmt.toString());
                if (availableAmt.compareTo(new BigDecimal(0)) <= 0) {
                    log.error("校验业务申请" + iqpSerno + "担保合同" + guarContNo + "是否可用-可用金额为零，担保合同不可用");
                    throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_ZEROAMT_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_ZEROAMT_EXCEPTION.value);
                }
            }
        } catch (YuspException e) {
            log.error("校验担保合同是否可用出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 通过入参信息获取担保合同金额信息
     *
     * @param params
     * @return
     */
    public Map getAmtInfoByGuarContNo(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        BigDecimal usedAmt = new BigDecimal(0);
        try {
            String guarContNo = (String) params.get("guarContNo");
            if (StringUtils.isBlank(guarContNo)) {
                throw new YuspException(EcbEnum.IQP_GBRA_GETAMT_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_GETAMT_PARAM_EXCEPTION.value);
            }

            String iqpSerno = (String) params.get("iqpSerno");

            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);//操作类型为新增的数据
            params.put("rType", CmisBizConstants.R_TYPE_2);//业务类型为【与业务关系】

            log.info("业务申请" + iqpSerno + "获取担保合同" + guarContNo + "额度信息开始");
            List<IqpGuarBizRelApp> amtDataList = iqpGuarBizRelAppMapper.selectAmtDataByParams(params);

            //关系数据为空的场景直接返回
            if (amtDataList == null) {
                log.info("业务申请" + iqpSerno + "获取担保合同" + guarContNo + "额度信息，该笔担保合同未引用！");
                return rtnData;
            }

            //循环处理
            for (IqpGuarBizRelApp iqpGuarBizRelApp : amtDataList) {
                usedAmt = usedAmt.add(iqpGuarBizRelApp.getThisGuarAmt());
            }

        } catch (YuspException e) {
            log.error("获取担保合同额度信息出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("usedAmt", usedAmt);
        }

        return rtnData;
    }

    /**
     * 通过入参更新担保合同引用关系状态
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map delImportRel(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        try {
            //获取关系表的主键信息
            String pkId = (String) params.get("pkId");
            if (StringUtils.isBlank(pkId)) {
                throw new YuspException(EcbEnum.IQP_GBRA_DEL_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_DEL_PARAM_EXCEPTION.value);
            }

            log.info("解除担保合同关系开始！关系主键：" + pkId);

            int delCount = iqpGuarBizRelAppMapper.updateByParams(params);

            if (delCount < 0) {
                log.error("解除" + pkId + "担保合同关系失败！");
                throw new YuspException(EcbEnum.IQP_GBRA_DEL_FAILED.key, EcbEnum.IQP_GBRA_DEL_FAILED.value);
            }

        } catch (YuspException e) {
            log.error("解除担保合同关系引用出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 更新担保合同以及关系数据
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateImportRel(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        try {
            //校验担保信息页签是否已引入押品，并校验押品总价值包含本次担保金额
            Map grtContCont = grtGuarContService.checkGuarAmtAndAppAmt(params);
            if (grtContCont != null) {
                if (grtContCont.get("rtnCode").equals(EcbEnum.IQP_SUCCESS_DEF.key)) {

                    String iqpSerno = (String) params.get("iqpSerno");//业务申请主键

                    //是否阶段担保为【否】时，需要校验当前担保合同的期限是否能够覆盖业务申请的申请期限
                    checkGuarContTerm(params);

                    log.info("业务申请" + iqpSerno + "落地担保合同以及关系数据-获取操作标识");
                    //因为最高额引入/修改，一般担保引入/新增/修改共用一个界面，后端操作时根据操作标识进行区分操作
                    String optType = (String) params.get("optType");
                    if (StringUtils.isBlank(optType)) {
                        throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_PARAM_EXCEPTION.value);
                    }
                    log.info("业务申请" + iqpSerno + "落地担保合同以及关系数据-获取操作标识-操作标识为：" + optType);
                    if (CmisBizConstants.OPT_TYPE_IMPORTN.equals(optType) || CmisBizConstants.OPT_TYPE_IMPORTMAX.equals(optType)) {
                        //将入参对象转化为实体对象
                        IqpGuarBizRelApp iqpGuarBizRelApp = JSONObject.parseObject(JSON.toJSONString(params), IqpGuarBizRelApp.class);
                        log.info("业务申请" + iqpSerno + "【引入】操作数据落地！入参数据：" + params.toString() + "，新增关系数据");
                        int insertCount = iqpGuarBizRelAppMapper.insertSelective(iqpGuarBizRelApp);
                        if (insertCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_IMPORTADD_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_IMPORTADD_EXCEPTION.value);
                        }
                    } else if (CmisBizConstants.OPT_TYPE_UPDATEM.equals(optType)) {
                        Map updateMap = new HashMap();
                        updateMap.put("pkId", params.get("pkId"));
                        updateMap.put("thisGuarAmt", params.get("thisGuarAmt"));
                        updateMap.put("isAddGuar", params.get("isAddGuar"));
                        updateMap.put("isPerGur", params.get("isPerGur"));
                        log.info("【最高额担保合同-修改担保金额】操作数据落地！入参数据：" + updateMap.toString() + "，更新关系数据");
                        int updateCount = iqpGuarBizRelAppMapper.updateByParams(params);
                        if (updateCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_IMPORTUPDATE_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_IMPORTUPDATE_EXCEPTION.value);
                        }
                    } else if (CmisBizConstants.OPT_TYPE_ADDN.equals(optType)) {
                        log.info("【一般担保合同-新增】操作数据落地！入参数据：" + params.toString() + "，新增担保合同数据以及新增关系数据");
                        GrtGuarCont grtGuarCont = new GrtGuarCont();
                        grtGuarCont = JSONObject.parseObject(JSON.toJSONString(params), GrtGuarCont.class);
                        if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                            log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                        }
                        int insertCount = grtGuarContService.insertSelective(grtGuarCont);
                        if (insertCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_GRTCONTADD_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_GRTCONTADD_EXCEPTION.value);
                        }
                        IqpGuarBizRelApp iqpGuarBizRelApp = JSONObject.parseObject(JSON.toJSONString(params), IqpGuarBizRelApp.class);

                        insertCount = iqpGuarBizRelAppMapper.insertSelective(iqpGuarBizRelApp);
                        if (insertCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_IMPORTADD_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_IMPORTADD_EXCEPTION.value);
                        }
                    } else if (CmisBizConstants.OPT_TYPE_UPDATEN.equals(optType)) {
                        log.info("【一般担保合同-修改】操作数据落地！入参数据：" + params.toString() + "，修改担保合同数据以及修改关系数据");
                        GrtGuarCont grtGuarCont = JSONObject.parseObject(JSON.toJSONString(params), GrtGuarCont.class);
                        if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                            log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                        }
                        int updateCount = grtGuarContService.updateSelective(grtGuarCont);
                        if (updateCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_CONT_FAILED.key, EcbEnum.IQP_GBRA_UPDATE_CONT_FAILED.value);
                        }
                        updateCount = iqpGuarBizRelAppMapper.updateByParams(params);
                        if (updateCount < 0) {
                            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_REL_FAILED.key, EcbEnum.IQP_GBRA_UPDATE_REL_FAILED.value);
                        }
                    }
                }
//                else {
//                    throw new YuspException((String) grtContCont.get("rtnCode"),(String) grtContCont.get("rtnMsg"));
//                }
            }

        } catch (YuspException e) {
            log.error("保存担保合同以及关系引用出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();

        } catch (Exception e) {
            log.error("保存担保合同以及关系引用出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "原因：" + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 是否阶段担保为【否】时，需要校验当前担保合同的期限是否能够覆盖业务申请的申请期限
     *
     * @param params
     * @return
     */
    private void checkGuarContTerm(Map params) {
        String guarContNo = (String) params.get("guarContNo");//担保合同编号
        String iqpSerno = (String) params.get("iqpSerno");//业务申请主键
        String isPerGur = (String) params.get("isPerGur");//是否阶段担保
        String guarEndDate = (String) params.get("guarEndDate");//担保终止日
        String bizTyp = (String)params.get("bizTyp");//业务类型，用于区分业务申请以及担保变更申请
        if (StringUtils.isBlank(iqpSerno) || StringUtils.isBlank(isPerGur) || StringUtils.isBlank(guarEndDate)
            || StringUtils.isBlank(guarContNo) || StringUtils.isBlank(bizTyp)) {
            throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_PARAM_EXCEPTION.value);
        }

        if (CmisBizConstants.IS_PER_GUR_N.equals(isPerGur)) {
            //若为担保变更申请，查询合同的期限进行校验
            if (CmisBizConstants.BIZ_TYP_CHG.equals(bizTyp)) {
                String contNo = (String) params.get("contNo");
                log.info("担保变更申请" + iqpSerno + "落地担保合同以及关系数据-校验担保期限");
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                if (ctrLoanCont == null) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.value);
                }
                String contStartDate = ctrLoanCont.getContStartDate();
                String contendDate = ctrLoanCont.getContEndDate();
                if (contStartDate == null || contendDate == null) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.value);
                }
                log.info("合同申请" + contNo + "落地担保合同以及关系数据-校验担保期限-合同起始日期：" + contStartDate + "合同结束日期：" + contendDate);
                //比较合同终止日期与担保终止时间
                if (contendDate.compareTo(guarEndDate) > 0) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.value);
                }
            } else if (CmisBizConstants.BIZ_TYP_IQP.equals(bizTyp)) {
                log.info("业务申请" + iqpSerno + "落地担保合同以及关系数据-校验担保期限");
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
                if (iqpLoanApp == null) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.value);
                }
                BigDecimal appTerm = iqpLoanApp.getAppTerm();
                String termType = iqpLoanApp.getTermType();
                String appDate = iqpLoanApp.getAppDate();

                if (appTerm == null || StringUtils.isBlank(termType) || StringUtils.isBlank(appDate)) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.value);
                }
                log.info("业务申请" + iqpSerno + "落地担保合同以及关系数据-校验担保期限-申请时间：" + appDate + "申请期限：" + appTerm.intValue() + "申请期限类型：" + termType);
                String appEndDate = this.calDateByTermTypeAndTerm(appDate,termType,appTerm.intValue());
                if (StringUtils.isBlank(appEndDate)) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.value);
                }
                log.info("业务申请" + iqpSerno + "落地担保合同以及关系数据-校验担保期限-申请终止时间：" + appEndDate + "，担保终止时间：" + guarEndDate);
                //处理时间字符串转化为相同格式
                appEndDate = formatDateStr(appEndDate);
                guarEndDate = formatDateStr(guarEndDate);
                //比较申请终止时间与担保终止时间
                if (appEndDate.compareTo(guarEndDate) > 0) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.value);
                }
            } else if (CmisBizConstants.BIZ_TYP_LMT.equals(bizTyp)) {
                String logPrefix = "额度分项" + iqpSerno + "落地担保合同以及关系数据-";
                log.info(logPrefix + "获取额度分项申请数据");
                Map lmtSubDataMap = (Map) params.get("appData");
                if (CollectionUtils.isEmpty(lmtSubDataMap)) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION.value);
                }
                log.info(logPrefix + "获取额度分项到期日");
                String lmtEndDate = "";
                try {
                    lmtEndDate = this.calLmtEndDate(lmtSubDataMap);
                } catch (Exception e) {
                    log.error("获取额度分项到期日异常！", e);
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_GETAPPTERM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_GETAPPTERM_EXCEPTION.value);
                }

                if(StringUtils.isBlank(lmtEndDate)){
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERM_EXCEPTION.value);
                }
                log.info(logPrefix+"校验担保期限-申请终止时间：" + lmtEndDate + "，担保终止时间：" + guarEndDate);

                //处理时间字符串转化为相同格式
                lmtEndDate = formatDateStr(lmtEndDate);
                guarEndDate = formatDateStr(guarEndDate);

                //比较申请终止时间与担保终止时间
                if (lmtEndDate.compareTo(guarEndDate) > 0) {
                    throw new YuspException(EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.key, EcbEnum.IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION.value);
                }
            }
        }
    }

    /**
     * 计算额度分项到期日
     * @param lmtSubDataMap
     * @return
     */
    private String calLmtEndDate(Map lmtSubDataMap) {
        String lmtEndDate = "";
        String oldLmtStarDate = (String)lmtSubDataMap.get("lmtStartDate");
        String oldLmtEndDate = (String)lmtSubDataMap.get("lmtEndDate");
        String isAdjTerm = (String)lmtSubDataMap.get("isAdjTerm");
        String lmtType = (String)lmtSubDataMap.get("lmtType");
        String lmtTermType = (String)lmtSubDataMap.get("lmtTermType");
        int lmtTerm = Integer.parseInt((String)lmtSubDataMap.get("lmtTerm"));

        String lmtSubStarDate = "";//额度分项起始日
        if (CmisBizConstants.LMT_TYPE_01.equals(lmtType) || CmisBizConstants.LMT_TYPE_03.equals(lmtType)) {
            lmtSubStarDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            lmtEndDate = this.calDateByTermTypeAndTerm(lmtSubStarDate, lmtTermType, lmtTerm);
        } else if (CmisBizConstants.LMT_TYPE_02.equals(lmtType) || CmisBizConstants.LMT_TYPE_04.equals(lmtType)) {
            lmtSubStarDate = oldLmtStarDate;
            if (CmisCommonConstants.YES_NO_1.equals(isAdjTerm)) {//是否调整期限为是时，计算额度到期日
                lmtEndDate = this.calDateByTermTypeAndTerm(lmtSubStarDate, lmtTermType, lmtTerm);
            } else {
                lmtEndDate = oldLmtEndDate;
            }
        }

        return lmtEndDate;
    }

    /**
     * 通过期限类型，期限计算日期
     * @param dateStr
     * @param termType
     * @param term
     */
    private String calDateByTermTypeAndTerm(String dateStr,String termType,int term) {
        String calDate = "";
        if (CmisBizConstants.LMT_TERM_TYPE_001.equals(termType)) {//期限类型为年
            calDate = DateUtils.addYear(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_002.equals(termType)) {//期限类型为月
            calDate = DateUtils.addMonth(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_003.equals(termType)) {//期限类型为日
            calDate = DateUtils.addDay(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        }

        return calDate;
    }


    /**
     * 格式化时间字符串为固定的时间字符串格式  yyyy-MM-dd
     *
     * @return
     */
    private String formatDateStr(String oldDateStr) {
        String newDateStr = "";
        String[] oldDateStrs = oldDateStr.split("-");
        for (int i = 0; i < oldDateStrs.length; i++) {
            if (i == 0) {
                newDateStr += oldDateStrs[i];
                continue;
            }

            if (oldDateStrs[i].length() == 1) {
                newDateStr = newDateStr + "-" + "0" + oldDateStrs[i];
            } else {
                newDateStr = newDateStr + "-" + oldDateStrs[i];
            }
        }

        return newDateStr;
    }

    /**
     * 通过入参信息查询业务与担保合同关系数据
     *
     * @param params
     * @return
     */
    public List<IqpGuarBizRelApp> selectIGBRAByParams(Map params) {
        return iqpGuarBizRelAppMapper.selectDataByParams(params);
    }

    /**
     * 校验担保合同是否已经引入
     *
     * @param params
     * @return
     */
    public Map queryIqpGuarBizRelAppContNo(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        try {
            String contNo = (String) params.get("contNo");
            String guarContNo = (String) params.get("guarContNo");
            String iqpSerno = (String) params.get("grtSerno");
            if (StringUtils.isBlank(guarContNo) && StringUtils.isBlank(contNo) && StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_NOGUAR_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_NOGUAR_EXCEPTION.value);
            }

            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);//操作类型为新增的数据

            log.info("校验担保合同" + guarContNo + "是否可用-1、关系是否已存在");
            //定义合同查询条件
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarContNo", guarContNo);
            queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppMapper.selectByModel(queryModel);

            if (iqpGuarBizRelAppList != null && iqpGuarBizRelAppList.size() > 0) {
                log.error("校验保合同" + guarContNo + "-->关系已存在！");
                throw new YuspException(EcbEnum.IQP_GBRA_IMPORT_EXISTSOTHER_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_EXISTSOTHER_EXCEPTION.value);
            }
        } catch (YuspException e) {
            log.error("校验合同是否存在出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 通过入参信息查询合同关系数据
     *
     * @param params
     * @return
     */
    public List<IqpGuarBizRelApp> selectByParams(Map params) {
        return iqpGuarBizRelAppMapper.selectByParams(params);
    }
    /**
     * 重置担保合同
     *
     * @param newpkId
     * @return
     */

    public int updateByPkIdKey(String newpkId,String oprType)throws Exception {
        int rtnResult = 0;
        try {
            int iqpGuarBizRelAppList = iqpGuarBizRelAppMapper.updateByPkIdKey(newpkId,oprType);
            if (iqpGuarBizRelAppList < 0) {
                throw new YuspException(EcbEnum.GRT_CONT_IMPORT_NOGUAR_EXCEPTION.key, EcbEnum.GRT_CONT_IMPORT_NOGUAR_EXCEPTION.value);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtnResult;
    }
    /**
     * @方法名称: queryContNoKey
     * @方法描述: 根据合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @return
     */
    public BigDecimal queryContNoKey(String contNo, String oprType) {
        BigDecimal thisGuarAmt = new BigDecimal(0);
        try {
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppMapper.queryContNoKey(contNo ,oprType);
            if (CollectionUtils.isEmpty(iqpGuarBizRelAppList) && iqpGuarBizRelAppList.size() <= 0 ){
                return thisGuarAmt;
            }
            //获取总的本次担保金额
            for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
                thisGuarAmt = thisGuarAmt.add(iqpGuarBizRelApp.getThisGuarAmt());
            }
        } catch (Exception e) {
            log.error("通过担保合同编号查询异常", e);
        }
        return thisGuarAmt;
    }

    /**
     * @方法名称: updateByContNoKey
     * @方法描述: 根据合同编号更新原数据信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByContNoKey(String pkId, String contNo, String correlRel) {
        int rtnResult = 0;
        try {
            int result = iqpGuarBizRelAppMapper.updateByContNoKey(pkId, contNo, correlRel);
            if (result < 0) {
                rtnResult = -1;
            }
        } catch (Exception e) {
            log.error("担保与业务申请表更新异常", e);
            rtnResult = -1;
        }
        return rtnResult;
    }
    /**
     * @方法名称: querySelectIqpSerno
     * @方法描述: 根据入参复制原合同的业务子表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int querySelectIqpSerno(IqpGuarBizRelApp iqpGuarBizRel) throws Exception{
        int rtnData = 0;
        try{
            String contNo = iqpGuarBizRel.getContNo();
            String iqpSerno = iqpGuarBizRel.getIqpSerno();
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppMapper.queryContNoKey(contNo, CmisCommonConstants.OPR_TYPE_ADD);
            if (CollectionUtils.isEmpty(iqpGuarBizRelAppList)){
                for(IqpGuarBizRelApp iqpGuarBizRelApp:iqpGuarBizRelAppList){
                    iqpGuarBizRelApp.setPkId(StringUtils.uuid(true));
                    iqpGuarBizRelApp.setIqpSerno(iqpSerno);
                    rtnData=iqpGuarBizRelAppMapper.insert(iqpGuarBizRelApp);
                    if (rtnData == 0){
                        throw new YuspException(EcbEnum.GRTDE_IQPGUARBIZRELAPP_EXCEPTION.key, EcbEnum.GRTDE_IQPGUARBIZRELAPP_EXCEPTION.value);
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return  rtnData;
    }

    /**
     * 批量添加业务与担保合同关系数据
     * @param iqpGuarBizRelAppList
     * @return
     */
    public int insertForech(List<IqpGuarBizRelApp> iqpGuarBizRelAppList) {
        return iqpGuarBizRelAppMapper.insertForeach(iqpGuarBizRelAppList);
    }
    
}

