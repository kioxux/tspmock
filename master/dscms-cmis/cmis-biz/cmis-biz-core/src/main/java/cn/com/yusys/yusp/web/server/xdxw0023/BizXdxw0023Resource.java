package cn.com.yusys.yusp.web.server.xdxw0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0023.req.Xdxw0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0023.resp.Xdxw0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0023.Xdxw0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:在小贷是否有调查申请记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0023:在小贷是否有调查申请记录")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0023Resource.class);

    @Autowired
    private Xdxw0023Service xdxw0023Service;

    /**
     * 交易码：xdxw0023
     * 交易描述：在小贷是否有调查申请记录
     *
     * @param xdxw0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("在小贷是否有调查申请记录")
    @PostMapping("/xdxw0023")
    protected @ResponseBody
    ResultDto<Xdxw0023DataRespDto> xdxw0023(@Validated @RequestBody Xdxw0023DataReqDto xdxw0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataReqDto));
        Xdxw0023DataRespDto xdxw0023DataRespDto = new Xdxw0023DataRespDto();// 响应Dto:在小贷是否有调查申请记录
        ResultDto<Xdxw0023DataRespDto> xdxw0023DataResultDto = new ResultDto<>();
        // 证件号
        String certNo = xdxw0023DataReqDto.getCertNo();
        try {
            if (StringUtils.isEmpty(certNo)) {
                xdxw0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
                return xdxw0023DataResultDto;
            }
            xdxw0023DataRespDto = xdxw0023Service.xdxw0023(xdxw0023DataReqDto);
            xdxw0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, e.getMessage());
            // 封装xdxw0023DataResultDto中异常返回码和返回信息
            xdxw0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0023DataRespDto到xdxw0023DataResultDto中
        xdxw0023DataResultDto.setData(xdxw0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataResultDto));
        return xdxw0023DataResultDto;
    }
}
