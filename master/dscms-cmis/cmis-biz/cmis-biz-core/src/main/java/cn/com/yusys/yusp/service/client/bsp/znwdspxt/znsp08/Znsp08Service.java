package cn.com.yusys.yusp.service.client.bsp.znwdspxt.znsp08;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req.Znsp08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.resp.Znsp08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2ZnwdspxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：惠享贷规则审批申请接口
 *
 * @author 胡兵鹏
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Znsp08Service {
    private static final Logger logger = LoggerFactory.getLogger(Znsp08Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2ZnwdspxtClientService dscms2ZnwdspxtClientService;

    /**
     * 业务逻辑处理方法：客户调查撤销接口
     *  胡兵鹏
     * @param
     * @return
     */
    @Transactional
    public Znsp08RespDto znsp08(Znsp08ReqDto znsp08ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value, JSON.toJSONString(znsp08ReqDto));
        ResultDto<Znsp08RespDto> znsp08ResultDto = dscms2ZnwdspxtClientService.znsp08(znsp08ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value, JSON.toJSONString(znsp08ResultDto));
        String znsp08Code = Optional.ofNullable(znsp08ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String znsp08Meesage = Optional.ofNullable(znsp08ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Znsp08RespDto znsp08RespDto = new Znsp08RespDto();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, znsp08ResultDto.getCode())) {
            //  获取相关的值并解析
            znsp08RespDto = znsp08ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(znsp08Code, znsp08Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value);
        return znsp08RespDto;
    }


}
