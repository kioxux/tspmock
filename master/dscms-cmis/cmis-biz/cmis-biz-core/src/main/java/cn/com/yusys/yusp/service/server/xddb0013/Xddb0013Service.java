package cn.com.yusys.yusp.service.server.xddb0013;


import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgSftpDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.server.hyy.common.CertificateInfo;
import cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm;
import cn.com.yusys.yusp.dto.server.xddb0013.req.Xddb0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0013.resp.Xddb0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.util.FtpUtils;
import cn.com.yusys.yusp.util.PUBUtilTools;
import io.netty.util.internal.StringUtil;
import main.com.mingtech.domain.DocNode;
import main.com.mingtech.domain.FileBean;
import main.com.mingtech.domain.ResultBean;
import main.com.mingtech.service.ECMClient;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:
 *
 * @author xs
 * @version 1.0
 */

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddb0013Service
 * @类描述: #抵押登记不动产登记证明入库
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-12 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0013Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xddb0013.Xddb0013Service.class);
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Resource
    private Dscms2CoreCoClientService dscms2CoreCoClientService;
    @Resource
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Resource
    private Dscms2YpqzxtClientService Dscms2YpqzxtClientService;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
    @Resource
    private GrtGuarContMapper grtGuarContMapper;
    @Resource
    private GrtGuarContRelMapper grtGuarContRelMapper;
    @Resource
    private GuarMortgageRegisterAppMapper guarMortgageRegisterAppMapper;

    @Resource
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Resource
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private CtrAccpContMapper ctrAccpContMapper;
    @Resource
    private CtrCvrgContMapper ctrCvrgContMapper;
    @Resource
    private CtrTfLocContMapper ctrTfLocContMapper;
    @Resource
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;
    @Resource
    private IqpHighAmtAgrAppMapper iqpHighAmtAgrAppMapper;
    @Resource
    private SenddxService senddxService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private GuarContRelWarrantMapper guarContRelWarrantMapper;
    @Resource
    private ICmisCfgClientService iCmisCfgClientService;
    @Resource
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;
    @Resource
    private MessageSendService messageSendService;
    @Resource
    private GuarContRelWarrantService guarContRelWarrantService;
    @Value("${application.image.url}")
    String imageSysUrl;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 查询押品是否按揭
     *
     * @param xddb0013DataReqDto
     * @return
     */
    public Xddb0013DataRespDto xddb0013(Xddb0013DataReqDto xddb0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value);
        Xddb0013DataRespDto xddb0013DataRespDto = new Xddb0013DataRespDto();
        xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0013DataRespDto.setOpMsg(StringUtils.EMPTY);
        UpdateCollateralElectronicCertificateForm form = new UpdateCollateralElectronicCertificateForm();
        CertificateInfo certificateInfo = new CertificateInfo();
        try{
            String guarid = xddb0013DataReqDto.getGuarid();//押品编号
            form = xddb0013DataReqDto.getUpdateCollateralElectronicCertificateForm();
            certificateInfo = form.getCertificate_info();
            String certif = form.getCertificate();//押品登记证明
            String certve = certificateInfo.getVersion();//押品登记证明-版本
            String certlo = certificateInfo.getLocation();//押品登记证明-坐落
            String cereun = certificateInfo.getReal_estate_unit_number();//押品登记证明-不动产单元号
            String cecenu = certificateInfo.getCertificate_number();//押品登记证明-不动产权证书号
            String ceihcn = certificateInfo.getHouse_certificate_number();//押品登记证明-房产证号
            String ceilcn = certificateInfo.getLand_certificate_number();//押品登记证明-土地证号
            String ceimcn = certificateInfo.getMortgage_certificate_number();//押品登记证明-不动产登记证明号
            String ceribn = certificateInfo.getBusiness_number();//押品登记证明-报件编号
            String ceripn = certificateInfo.getPrint_number();//押品登记证明-证书印刷编号
            String cetibd = certificateInfo.getBooking_date();//押品登记证明-登簿日期
            String cetiob = certificateInfo.getObligee();//押品登记证明-权利人
            String cetobl = certificateInfo.getObligor();//押品登记证明-义务人
            String cetirt = certificateInfo.getRight_type();//押品登记证明-证明权利类型
            String ceidst = certificateInfo.getDebt_start_time();//押品登记证明-债务履行期限起
            String ceidet = certificateInfo.getDebt_end_time();//押品登记证明-债务履行期限止
            String ceimom = certificateInfo.getMortgage_method();//押品登记证明-抵押方式
            String ceigsc = certificateInfo.getGuarantee_scope();//押品登记证明-担保范围
            String ceidam = certificateInfo.getDebt_amount();//押品登记证明-被担保债权数额
            String djzlxmc = certificateInfo.getDjzlxmc();

            if (ceidam.indexOf("万元") > -1) {
                ceidam = ceidam.replace("万元", "");
                ceidam = new BigDecimal(ceidam).multiply(new BigDecimal("10000")).toString();
            } else if (ceidam.indexOf("元") > -1) {
                ceidam = ceidam.replace("元", "");
            }
            String ceirem = certificateInfo.getRemark();//押品登记证明-附记
            String cemocn = certificateInfo.getMortgage_contract_no();//押品登记证明-抵押合同编号
            String cedzlx = certificateInfo.getDjzlx();//押品登记证明-登记子类型
            String ypstatus = form.getOrder();//顺位

            //状态、证明权利或事项（01-抵押权、02-预告登记、03-居住权、04-其他）
            String status_code = "";
            String pro_certi_item = "";
            String guarContNo = cemocn;
            if(cedzlx!=null || !"".equals(cedzlx)){
                if("91012".equals(cedzlx)){
                    status_code = "10006";
                    pro_certi_item = "01";
                }else if("91017".equals(cedzlx)){//预抵押模式，根据对应担保合同（合同）号查询出押品编号
                    status_code = "10011";
                    pro_certi_item = "02";
                    if(!"".equals(guarContNo) || guarContNo != null){
                        //若是传合同号，则需查询出对应担保合同号
                        String guarContNoDBHT = guarContNo.substring(0,2);
                        if(!"DB".equals(guarContNoDBHT)){
                            guarContNo =  grtGuarBizRstRelMapper.selectGuarContNoBycontNo(cemocn);
                        }
                        GrtGuarContRel grtGuarContRel = grtGuarContRelMapper.selectDetailByGuarContNo(guarContNo);
                        if(grtGuarContRel==null){
                            xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                            xddb0013DataRespDto.setOpMsg(DscmsBizDbEnum.NO_GUAR_BASE_INFO.value);
                            return xddb0013DataRespDto;
                        }else{
                            guarid = grtGuarContRel.getGuarNo();
                        }
                    }
                }
            }

            ypstatus = "0" + ypstatus;
            String[] str = guarid.split("#");
            guarid = str[0];

            GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.queryBaseInfoByGuarId(guarid);//押品编号
            if(guarBaseInfo==null){
                xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0013DataRespDto.setOpMsg(DscmsBizDbEnum.NO_GUAR_BASE_INFO.value);
                return xddb0013DataRespDto;
            }
            String islocal= guarBaseInfo.getAreaCode();
            if(islocal!=null&&!"".equals(islocal)){
                islocal=islocal.substring(0, 6);
            }
            if("320582".equals(islocal)){
                islocal="1";
            }else{
                islocal="0";
            }
            String accountManagerBr = guarBaseInfo.getManagerBrId();// 获取责任机构(待定)
            String accountManager = guarBaseInfo.getAccountManager(); // 获取责任人Id(管户人)
            String time = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);// 获取当前时间（待定）先这样写到时候再说
            String pldimnMemo = guarBaseInfo.getPldimnMemo(); // 抵押物名称 |抵质押物名称（抵质押物类型名称）
            String guarTypeCd = guarBaseInfo.getGuarTypeCd(); // 抵押物类型
            String gagetype = CmisBizConstants.gageTypeMap.get(guarTypeCd);
            String gagetype2="";//信贷与核心抵质押种类对应
            if("20001".equals(gagetype)){
                gagetype2 = "49";
            }else if("20002".equals(gagetype)){
                gagetype2 = "50";
            }else{
                gagetype2 = gagetype.substring(3);
            }
            String grpNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>()); // 生成核心担保编号 老信贷（权证编号）
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());  // 生成出入库流水号
            String cusID = guarBaseInfo.getGuarCusId(); //客户号
            String cusName = guarBaseInfo.getGuarCusName(); //客户名称
//            String curType = guarBaseInfo.getCurType();//币种 需要转换新核心对应码值
//            curType = PUBUtilTools.currencyType(curType);
            String yewusx01 =PUBUtilTools.changeloanGlRoleCde(gagetype);  //核心赊账码值
            //押品登记证明-抵押合同编号 验证
            //String guarContNo = grtGuarContMapper.selectGuarContByCnNo(cemocn);
            if("".equals(guarContNo) || guarContNo == null){
                //该合同编号不存在
                xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0013DataRespDto.setOpMsg(DscmsBizDbEnum.NOEXIT_GUAR_CONT_NO.value);
                return xddb0013DataRespDto;
            }
            //若是传合同号，则需查询出对应担保合同号
            String guarContNoDB = guarContNo.substring(0,2);
            if(!"DB".equals(guarContNoDB)){
                guarContNo =  grtGuarBizRstRelMapper.selectGuarContNoBycontNo(guarContNo);
            }
            GrtGuarCont grtGuarCont = grtGuarContMapper.selectContNo(guarContNo);
            // 判断有没有入库 GUARCONTGRTREL
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarNo",guarid);//押品统一编号
            queryModel.addCondition("guarContNo",guarContNo);//担保合同编号
            List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelMapper.selectByModel(queryModel);
            if(grtGuarContRelList!=null && grtGuarContRelList.size()>0){
                //如果查不到(理论只有测试环境会出现)  查看具体押品的状态
                //String statuscode = guarBaseInfo.getGuarState();
                //查询押品的对应抵押顺位的出入库状态
                QueryModel querymodel = new QueryModel();
                querymodel.addCondition("guarNo",guarid);
                querymodel.addCondition("mortOrderFlag",ypstatus);
                String statuscode = guarBaseInfoMapper.queryYpRegState(querymodel);
                if("04".equals(statuscode)){
                    //该押品已经入库
                    xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xddb0013DataRespDto.setOpMsg(DscmsBizDbEnum.GUAR_STATE_IN.value);
                    return xddb0013DataRespDto;
                }else{
                    // 抵押登记不动产登记证明登记表插入数据 guarMortgageRegisterAppMapper
                    /**
                     * 调用核心入库接口：co3200
                     **/
                    //String inputid =grtGuarContRelList.get(0).getInputId();
                    //String inputbrid =grtGuarContRelList.get(0).getInputBrId();
                    //String finabrid=grtGuarContRelList.get(0).getManagerId();
                    //String orgidid=grtGuarContRelList.get(0).getManagerBrId();
                    //String newid=inputbrid;
                    //取对应担保合同中的客户经理号及机构
                    String inputid = "";
                    String inputbrid = "";
                    String newid = "";
                    //GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelMapper.selectContManagerId(guarContNo);
                    inputid = grtGuarCont.getManagerId();
                    inputbrid = grtGuarCont.getManagerBrId();
                    newid = inputbrid.substring(0, inputbrid.length()-1) + "1";
                    //发送co3200接口自动入库
                    Co3200ReqDto co3200ReqDto = new Co3200ReqDto();
                    co3200ReqDto.setDkkhczbz("4");//开户操作标志1--录入、2--修改、3--复核、4--直通
                    co3200ReqDto.setDzywbhao(grpNo);//抵质押物编号bookSerno
                    co3200ReqDto.setDzywminc(pldimnMemo);//抵质押物名称
                    co3200ReqDto.setYngyjigo(newid);//营业机构
                    co3200ReqDto.setDzywzlei(gagetype2);//抵质押物种类
                    co3200ReqDto.setDizyfshi("1");//抵质押方式1--抵押
                    co3200ReqDto.setSyrkhhao(cusID);//受益人客户号
                    co3200ReqDto.setSyrkhmin(cusName);//受益人客户名
                    co3200ReqDto.setSyqrkehh(cusID);//所有权人客户号
                    co3200ReqDto.setSyqrkehm(cusName);//所有权人客户名
                    co3200ReqDto.setHuobdhao("01");//货币代号 默认01--人民币
                    co3200ReqDto.setMinyjiaz(new BigDecimal(ceidam));//名义价值
                    co3200ReqDto.setShijjiaz(new BigDecimal(ceidam));//实际价值
                    co3200ReqDto.setPingjiaz(new BigDecimal(ceidam));//评估价值
                    co3200ReqDto.setShengxrq(time.replaceAll("-",""));//生效日期
                    co3200ReqDto.setDaoqriqi(ceidet.replaceAll("-",""));//到期日期
                    co3200ReqDto.setGlywbhao(ceimcn);//关联业务编号  --传权证号(理财传理财产品代码)
                    co3200ReqDto.setYewusx01(yewusx01);//记账余额属性
                    ResultDto<Co3200RespDto> co3200ResultDto = dscms2CoreCoClientService.co3200(co3200ReqDto);
                    if(!"0".equals(co3200ResultDto.getCode())){
                        //失败直接返回
                        xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0013DataRespDto.setOpMsg(co3200ResultDto.getMessage());
                        return xddb0013DataRespDto;
                    }
                    //插入权证信息表 GUARWARRANTINFO

                    //插入权证信息与押品关联关系表 GRTGPINOUTDETAILS已经废弃
                    //更新权证与担保合同关联关系  新信贷没有bookserno
                    //更新押品状态 10006
                    guarBaseInfo.setGuarState("04");
                    guarBaseInfo.setCoreGuarantyNo(grpNo);
                    guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);

                    GuarWarrantInfo guarWarrantInfo = new GuarWarrantInfo();

                    guarWarrantInfo.setFinaBrId(guarBaseInfo.getFinaBrId());//账务机构
                    guarWarrantInfo.setGrtFlag(guarBaseInfo.getGrtFlag());//抵质押分类
                    guarWarrantInfo.setWarrantNo(ceimcn);//权证编号
                    guarWarrantInfo.setCoreGuarantyNo(grpNo);//核心担保编号
                    guarWarrantInfo.setIsEWarrant(CmisBizConstants.STD_ZB_YES_NO_Y);//是否电子权证
                    guarWarrantInfo.setMortOrderFlag(ypstatus);//押品顺位标识
                    guarWarrantInfo.setCertiAmt(new BigDecimal(ceidam));//权利金额(权利价值)
                    guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_04);//权证状态
                    guarWarrantInfo.setInDate(time);//权证入库日期
                    guarWarrantInfo.setInputId(accountManager);//登记人
                    guarWarrantInfo.setInputBrId(accountManagerBr);//登记机构
                    guarWarrantInfo.setManagerId(accountManager);//主管客户经理
                    guarWarrantInfo.setManagerBrId(accountManagerBr);//主管机构
                    guarWarrantInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    guarWarrantInfo.setProCertiItem(pro_certi_item);
                    guarWarrantInfo.setWarrantName(cetobl+"-住宅");//权证名称
                    guarWarrantInfo.setCertiCatalog("1");//权证类别 默认1--权利证明类型
                    guarWarrantInfo.setCertiTypeCd("03");//权证类型 默认 03--抵押登记证明
                    guarWarrantInfo.setProCertiItem("01");//证明权利或事项 默认 01 抵押权
                    guarWarrantInfo.setCertiStartDate(ceidst);//权证发证日期
                    guarWarrantInfo.setCertiEndDate(ceidet);//权证到期日期
                    guarWarrantInfo.setFinaBrId(accountManagerBr);//账务机构

                    if ("预购商品房抵押权预告登记".equals(djzlxmc)){
                        //如果是预购商品房抵押权预告登记，则证明权利或事项为02--预告登记
                        guarWarrantInfo.setProCertiItem("02");
                    }
                    guarWarrantInfoMapper.insertSelective(guarWarrantInfo);

                    //新增担保合同关联押品及权证关系
                    GuarContRelWarrant guarContRelWarrant = new GuarContRelWarrant();
                    //流水号取报件编号
                    guarContRelWarrant.setSerno(ceribn);
                    guarContRelWarrant.setGuarContNo(guarContNo);
                    guarContRelWarrant.setCoreGuarantyNo(grpNo);
                    guarContRelWarrant.setWarrantNo(ceimcn);
                    guarContRelWarrant.setGuarNo(guarid);
                    guarContRelWarrant.setPldimnMemo(pldimnMemo);
                    guarContRelWarrant.setGuarType(guarBaseInfo.getGuarType());
                    guarContRelWarrant.setGuarCusId(guarBaseInfo.getGuarCusId());
                    guarContRelWarrant.setGuarCusName(guarBaseInfo.getGuarCusName());
                    guarContRelWarrant.setOprType(CmisBizConstants.OPR_TYPE_01);
                    guarContRelWarrantMapper.insert(guarContRelWarrant);

                    logger.info("新增权证入库记录开始，流水号【"+ceribn+"】");
                    GuarWarrantManageApp guarWarrantManageApp = new GuarWarrantManageApp();
                    guarWarrantManageApp.setSerno(ceribn);//申请流水号
                    guarWarrantManageApp.setGuarContNo(guarContNo);//担保合同编号
                    guarWarrantManageApp.setGrtFlag(CmisBizConstants.STD_GRT_FLAG_01);//抵押/质押标识 默认抵押
                    guarWarrantManageApp.setGagTyp(CmisBizConstants.STD_ZB_GUAR_TYPE_CD_03);//抵质押物种类 默认不动产
                    guarWarrantManageApp.setWarrantNo(ceimcn);//权证编号
                    guarWarrantManageApp.setWarrantInType(CmisBizConstants.STD_WARRANT_IN_TYPE_01);//权证入库模式  01-电子权证自动入库模式
                    guarWarrantManageApp.setWarrantAppType(CmisBizConstants.STD_WARRANT_APP_TYPE_01);//权证出入库申请类型 01--入库申请
                    guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);//审批状态 默认通过
                    guarWarrantManageApp.setHxSerno(co3200ResultDto.getData().getJiaoyils());//核心抵质押品出入库返回交易流水
                    guarWarrantManageApp.setHxDate(co3200ResultDto.getData().getJiaoyirq());//核心抵质押品出入库返回交易日期

                    if (StringUtils.isNotEmpty(ceirem) && ceirem.length()>167){
                        ceirem = ceirem.substring(0,166);//截取前166个，防止字段过长报错
                    }
                    guarWarrantManageApp.setRemark(ceirem);//备注
                    guarWarrantManageApp.setCoreGuarantyNo(grpNo);//核心担保编号
                    guarWarrantManageApp.setManagerId(accountManager);// 主管客户经理
                    guarWarrantManageApp.setManagerBrId(accountManagerBr);//主管机构
                    guarWarrantManageApp.setOprType(CmisBizConstants.OPR_TYPE_01);//操作类型
                    guarWarrantManageApp.setInputId(accountManager);//登记人
                    guarWarrantManageApp.setInputBrId(accountManagerBr);//登记机构
                    guarWarrantManageApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                    guarWarrantManageApp.setUpdId(accountManager);//最后修改人
                    guarWarrantManageApp.setUpdBrId(accountManagerBr);//最后修改机构
                    guarWarrantManageApp.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));//	最后修改日期
                    guarWarrantManageApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));//创建时间
                    guarWarrantManageAppMapper.insert(guarWarrantManageApp);
                    logger.info("新增权证入库记录结束，流水号【"+ceribn+"】");

                    CetinfReqDto cetinfReqDto = new CetinfReqDto();
                    List<CetinfListInfo> cetinfList = new LinkedList<CetinfListInfo>();
                    CetinfListInfo cetinfListInfo = new CetinfListInfo();
                    cetinfListInfo.setYptybh(guarid);//
                    cetinfList.add(cetinfListInfo);
                    cetinfReqDto.setList(cetinfList); // 押品统一编号
                    cetinfReqDto.setDbhtbh(cemocn);// 担保合同编号
                    cetinfReqDto.setSernoy(grpNo);// 核心担保编号
                    cetinfReqDto.setDyswbs(ypstatus);// 抵押顺位标识
                    cetinfReqDto.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(guarWarrantInfo.getCertiTypeCd()));// 权证类型
                    cetinfReqDto.setQlpzhm(ceimcn);// 权利凭证号
                    cetinfReqDto.setQzfzjg("苏州不动产登记中心");// 权证发证机关名称
                    cetinfReqDto.setQzffrq(cetibd);// 权证发证日期
                    cetinfReqDto.setQzdqrq(ceidet);// 权证到期日期
                    cetinfReqDto.setQljeyp(ceidam);// 权利金额
                    cetinfReqDto.setQzztyp(status_code);// 权证状态
                    cetinfReqDto.setQzbzxx(StringUtil.EMPTY_STRING);// 权证备注信息
                    cetinfReqDto.setDjrmyp(inputid);// 登记人
                    cetinfReqDto.setDjjgyp(inputbrid);// 登记机构
                    cetinfReqDto.setDjrqyp(time);// 登记日期
                    cetinfReqDto.setOperat("05"); // 操作
                    //发送cetinf接口
                    ResultDto<CetinfRespDto> cetinfResultDto = dscms2YpxtClientService.cetinf(cetinfReqDto);
                    if(!"0".equals(cetinfResultDto.getCode())){
                        //失败直接返回
                        xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0013DataRespDto.setOpMsg(cetinfResultDto.getMessage());
                        return xddb0013DataRespDto;
                    }
                    //发送certis接口
                    CertisReqDto certisReqDto = new CertisReqDto();
                    List<CertisListInfo> certisListInfoList = new LinkedList<CertisListInfo>();
                    CertisListInfo certisListInfo = new CertisListInfo();
                    certisListInfo.setSernoy(grpNo);//核心担保编号
                    certisListInfo.setQlpzhm(ceimcn);//权利凭证号
                    certisListInfo.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(guarWarrantInfo.getCertiTypeCd()));//权证类型
                    certisListInfo.setQzztyp(status_code);//权证状态
                    certisListInfo.setQzrkrq(time);//权证入库日期
                    certisListInfo.setQzckrq(StringUtil.EMPTY_STRING);//权证正常出库日期
                    certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称
                    certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期
                    certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期
                    certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期
                    certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因
                    certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
                    certisListInfoList.add(certisListInfo);
                    certisReqDto.setList(certisListInfoList);
                    ResultDto<CertisRespDto> certisResultDto =  dscms2YpxtClientService.certis(certisReqDto);
                    if(!"0".equals(certisResultDto.getCode())){
                        xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0013DataRespDto.setOpMsg(certisResultDto.getMessage());
                        return xddb0013DataRespDto;
                    }
                    //发送cwm003接口 对应老信贷yprksq接口
                    Cwm003ReqDto cwm003ReqDto = new Cwm003ReqDto();
                    cwm003ReqDto.setApplyTime(time);// 申请时间
                    cwm003ReqDto.setGageId(grpNo);// 核心担保品编号
                    cwm003ReqDto.setGageType(gagetype);// 抵质押类型
                    cwm003ReqDto.setGageTypeName(pldimnMemo);// 抵质押类型名称
                    cwm003ReqDto.setMaxAmt(ceidam);// 权利价值
                    cwm003ReqDto.setGageUser(guarBaseInfo.getCusId()); // 抵押人
                    cwm003ReqDto.setAcctBrchNo(guarBaseInfo.getFinaBrId());// 账务机构
                    cwm003ReqDto.setAcctBrchName(OcaTranslatorUtils.getOrgName(inputbrid));// 账务机构名称
                    cwm003ReqDto.setApplyBrchNo(inputbrid);// 申请支行号
                    cwm003ReqDto.setApplyBrchName(OcaTranslatorUtils.getOrgName(inputbrid));// 申请支行名称
                    cwm003ReqDto.setApplyUserCode(inputid);// 申请人操作号
                    cwm003ReqDto.setApplyUserName(OcaTranslatorUtils.getUserName(inputid));// 申请人名称
                    cwm003ReqDto.setIsMortgage(StringUtil.EMPTY_STRING); // 是否住房按揭
                    cwm003ReqDto.setRemark(StringUtil.EMPTY_STRING);// 描述
                    cwm003ReqDto.setGageName(pldimnMemo);// 抵押物名称
                    cwm003ReqDto.setIsLocal("1");// 是否张家港地区不动产 不是柜面复核送1
                    cwm003ReqDto.setIsElectronic("1");// 押品类型
                    cwm003ReqDto.setIszhRegist(StringUtil.EMPTY_STRING);// 是否总行办理注销登记
                    cwm003ReqDto.setAcctBrch(inputbrid);
                    cwm003ReqDto.setUserName(inputid);

                    //是否柜面审核 默认 Y：柜面审核
//                    cwm003ReqDto.setIsgm("Y");

                    ResultDto<Cwm003RespDto> cwm003ResultDto= Dscms2YpqzxtClientService.cwm003(cwm003ReqDto);
                    if(!"0".equals(cwm003ResultDto.getCode())){
                        xddb0013DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0013DataRespDto.setOpMsg(cwm003ResultDto.getMessage());
                        return xddb0013DataRespDto;
                    }
                    //接口没有完成 详情请参照老信贷Baec01Action.java(影像先不做)
                    //根据担保合同编号查询合同编号
                    GrtGuarBizRstRel grtguarBizRstRel = grtGuarBizRstRelMapper.selectByGuarContNo(guarContNo);
                    String contNo = grtguarBizRstRel.getContNo();
                    String htserno="";//合同流水号
                    String cus_id="";//客户号
                    String yxdjd = "DKDY";//默认权证目录
                    String yxxjd = "DKDY6";
                    String biz_type="";//贷款类别
                    String manager_br_id="";//所属机构

                    getSendPath(ceimcn,certif,grpNo,cus_id,contNo,yxdjd,yxxjd);
                    String actorname = "";//分配人姓名
                    String telnum = "";//分配人电话
                    ResultDto<AdminSmUserDto> userResultDto = adminSmUserService.getByLoginCode(accountManager);
                    String userCode = userResultDto.getCode();//返回结果
                    if (tk.mybatis.mapper.util.StringUtil.isNotEmpty(userCode) && CmisBizConstants.NUM_ZERO.equals(userCode)) {
                        AdminSmUserDto adminSmUserDto = userResultDto.getData();
                        actorname = adminSmUserDto.getUserName();
                        telnum = adminSmUserDto.getUserMobilephone();
                    }

                    MessageSendDto messageSendDto = new MessageSendDto();
                    List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                    ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                    receivedUserDto.setReceivedUserType("1");
                    receivedUserDto.setUserId(accountManager);
                    ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(accountManager);
                    receivedUserDto.setMobilePhone(byLoginCode.getData().getUserMobilephone());
                    receivedUserList.add(receivedUserDto);
                    messageSendDto.setMessageType("MSG_DB_M_0001");
                    Map<String,String> map = new HashMap<>();

                    List<GuarContRelWarrant> list = guarContRelWarrantService.selectByCoreGuarantyNo(grpNo);

                    if(CollectionUtils.isNotEmpty(list)){
                        for (GuarContRelWarrant gcrw:list) {
                            map.put("guarNo",gcrw.getGuarNo());
                            messageSendDto.setParams(map);
                            messageSendDto.setReceivedUserList(receivedUserList);
                            messageSendService.sendMessage(messageSendDto);
                        }
                    }
                }
            }
        }
        catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value);
        return xddb0013DataRespDto;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            senddxService.senddx(senddxReqDto);
        } catch (Exception e) {
            logger.info("errmsg：" + e.getMessage());
            throw new Exception("发送短信失败！");
        }
    }

    private String getSendPath(String ceimcn,String certif,String grpNo,String cus_id,String cont_no,String yxdjd,String yxxjd){
        String savelocalPath = "";

        try {
            savelocalPath = "/home/dscms/sharetx";
            savelocalPath = savePic(certif, ceimcn,savelocalPath);
            uploadFile(grpNo, cus_id,cont_no, yxdjd, yxxjd,ceimcn,savelocalPath);
            //关闭FTP连接
//            ftpUtils.closeConnect();

        }catch (Exception e){}

        return savelocalPath;
    }

    private String savePic(String certif,String ceimcn,String localPath) throws Exception{
        BufferedInputStream bin = null;
        FileOutputStream fout = null;
        BufferedOutputStream bout = null;
        BASE64Decoder decoder = new sun.misc.BASE64Decoder();
        localPath = localPath+"/"+ceimcn+ ".pdf";
        try{
            byte[] bytes = decoder.decodeBuffer(certif);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            bin = new BufferedInputStream(bais);
            File file = new File(localPath);
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
            }
            fout = new FileOutputStream(file);
            bout = new BufferedOutputStream(fout);
            byte[] buffers = new byte[1024];
            int len= bin.read(buffers);
            while(len!=-1){
                bout.write(buffers,0,len);
                len = bin.read(buffers);
            }
            bout.flush();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                bin.close();
                fout.close();
                bout.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        logger.info("localPath:"+localPath);
        return localPath;
    }

    private void uploadFile(String grpNo,String cus_id,String cont_no,String yxdjd,String yxxjd,String ceimcn,String savelocalPath){
        String ip = imageSysUrl;
        logger.info("ip:"+ip);
        ip = ip.substring(7);
        FileBean fileBean = new FileBean();
        Map<String, String> map = new HashMap<String, String>();
        map.put("businessid", grpNo);//之前的业务的主键
        map.put("custid", cus_id);
        map.put("contid", grpNo);
//        if("XD_ZGESXXY".equals(yxdjd)){//最高额授信协议（在线抵押）影像目录是新增的，Index中必须要传docid
//            map.put("docid", grpNo);
//        }
        map.put("docid", grpNo);
        fileBean.setIndex(map);
        fileBean.setIp(ip);//
        fileBean.setUsername("admin");//写死
        fileBean.setPassword("f6fdffe48c908deb0f4c3bd36c032e72");//写死
        fileBean.setDocId(grpNo);//pdf对应的主键
        fileBean.setTopOutCode(yxdjd);//节点
        fileBean.setThumbnail(true);
        DocNode doc = new DocNode(yxxjd);//上传的节点
        File file1 = new File(savelocalPath);//
        doc.getDocList().add(file1);
        fileBean.getDocNodeList().add(doc);
        ECMClient.getInstance();
        logger.info("fileBean:"+fileBean);
        ResultBean result = ECMClient.uploadFile(fileBean);
        logger.info("~~~~~~~~~~~~~~~~~" +result.getCode());
    }
}
