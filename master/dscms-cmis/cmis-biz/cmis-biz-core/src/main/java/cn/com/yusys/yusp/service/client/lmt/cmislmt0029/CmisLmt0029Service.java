package cn.com.yusys.yusp.service.client.lmt.cmislmt0029;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class cmisLmt0029Service
 * @date 2021/8/25 19:10
 * @desc 更新台账编号接口
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0029Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0029Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0029ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.cmisLmt0029RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 占用台账额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0029RespDto cmisLmt0029(CmisLmt0029ReqDto cmisLmt0029ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(cmisLmt0029ReqDto));
        ResultDto<CmisLmt0029RespDto> cmisLmt0029ResultDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(cmisLmt0029ResultDto));

        String cmisLmt0029Code = Optional.ofNullable(cmisLmt0029ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0029Meesage = Optional.ofNullable(cmisLmt0029ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0029RespDto cmisLmt0029RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0029ResultDto.getCode()) && cmisLmt0029ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0029ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0029RespDto = cmisLmt0029ResultDto.getData();
        } else {
            if (cmisLmt0029ResultDto.getData() != null) {
                cmisLmt0029Code = cmisLmt0029ResultDto.getData().getErrorCode();
                cmisLmt0029Meesage = cmisLmt0029ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0029Code = EpbEnum.EPB099999.key;
                cmisLmt0029Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0029Code, cmisLmt0029Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value);
        return cmisLmt0029RespDto;
    }
}
