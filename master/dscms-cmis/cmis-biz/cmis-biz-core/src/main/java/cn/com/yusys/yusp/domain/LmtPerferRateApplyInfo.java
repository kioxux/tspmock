/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPerferRateApplyInfo
 * @类描述: lmt_perfer_rate_apply_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 19:31:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_perfer_rate_apply_info")
public class LmtPerferRateApplyInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = false, length = 40)
	private String surveySerno;

	/** 第三方业务申请流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 申请利率 **/
	@Column(name = "APP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appRate;
	
	/** 上期贷款方式 **/
	@Column(name = "PRE_LOAN_MODE", unique = false, nullable = true, length = 5)
	private String preLoanMode;
	
	/** 上期贷款金额 **/
	@Column(name = "PRE_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preLoanAmt;
	
	/** 上期执行年利率 **/
	@Column(name = "PRE_EXE_YEAR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preExeYearRate;
	
	/** 上期贷款期限 **/
	@Column(name = "PRE_LOAN_TERM", unique = false, nullable = true, length = 20)
	private String preLoanTerm;
	
	/** 客户经理意见 **/
	@Column(name = "MANAGER_ID_ADVICE", unique = false, nullable = true, length = 500)
	private String managerIdAdvice;
	
	/** 客户经理其他意见 **/
	@Column(name = "MANAGER_ID_OTHER_ADVICE", unique = false, nullable = true, length = 500)
	private String managerIdOtherAdvice;
	
	/** 其他原因 **/
	@Column(name = "OTHER_RESN", unique = false, nullable = true, length = 500)
	private String otherResn;
	
	/** 批复利率 **/
	@Column(name = "REPLY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyRate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 拒绝原因 **/
	@Column(name = "REFUSE_REASONS", unique = false, nullable = true, length = 500)
	private String refuseReasons;
	
	/** 审批节点类型 **/
	@Column(name = "APPROVE_NODE_TYPE", unique = false, nullable = true, length = 5)
	private String approveNodeType;
	
	/** 当前审批节点 **/
	@Column(name = "CURT_APPROVE_NODE", unique = false, nullable = true, length = 10)
	private String curtApproveNode;
	
	/** 当前审批人工号 **/
	@Column(name = "CURT_APPR_ID_JOB_NO", unique = false, nullable = true, length = 20)
	private String curtApprIdJobNo;
	
	/** 申请时间 **/
	@Column(name = "APP_TIME", unique = false, nullable = true, length = 20)
	private String appTime;
	
	/** 是否存量用户  **/
	@Column(name = "WHETHER_STOCK_USER", unique = false, nullable = true, length = 1)
	private String whetherStockUser;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}
	
    /**
     * @return appRate
     */
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}
	
	/**
	 * @param preLoanMode
	 */
	public void setPreLoanMode(String preLoanMode) {
		this.preLoanMode = preLoanMode;
	}
	
    /**
     * @return preLoanMode
     */
	public String getPreLoanMode() {
		return this.preLoanMode;
	}
	
	/**
	 * @param preLoanAmt
	 */
	public void setPreLoanAmt(java.math.BigDecimal preLoanAmt) {
		this.preLoanAmt = preLoanAmt;
	}
	
    /**
     * @return preLoanAmt
     */
	public java.math.BigDecimal getPreLoanAmt() {
		return this.preLoanAmt;
	}
	
	/**
	 * @param preExeYearRate
	 */
	public void setPreExeYearRate(java.math.BigDecimal preExeYearRate) {
		this.preExeYearRate = preExeYearRate;
	}
	
    /**
     * @return preExeYearRate
     */
	public java.math.BigDecimal getPreExeYearRate() {
		return this.preExeYearRate;
	}
	
	/**
	 * @param preLoanTerm
	 */
	public void setPreLoanTerm(String preLoanTerm) {
		this.preLoanTerm = preLoanTerm;
	}
	
    /**
     * @return preLoanTerm
     */
	public String getPreLoanTerm() {
		return this.preLoanTerm;
	}
	
	/**
	 * @param managerIdAdvice
	 */
	public void setManagerIdAdvice(String managerIdAdvice) {
		this.managerIdAdvice = managerIdAdvice;
	}
	
    /**
     * @return managerIdAdvice
     */
	public String getManagerIdAdvice() {
		return this.managerIdAdvice;
	}
	
	/**
	 * @param managerIdOtherAdvice
	 */
	public void setManagerIdOtherAdvice(String managerIdOtherAdvice) {
		this.managerIdOtherAdvice = managerIdOtherAdvice;
	}
	
    /**
     * @return managerIdOtherAdvice
     */
	public String getManagerIdOtherAdvice() {
		return this.managerIdOtherAdvice;
	}
	
	/**
	 * @param otherResn
	 */
	public void setOtherResn(String otherResn) {
		this.otherResn = otherResn;
	}
	
    /**
     * @return otherResn
     */
	public String getOtherResn() {
		return this.otherResn;
	}
	
	/**
	 * @param replyRate
	 */
	public void setReplyRate(java.math.BigDecimal replyRate) {
		this.replyRate = replyRate;
	}
	
    /**
     * @return replyRate
     */
	public java.math.BigDecimal getReplyRate() {
		return this.replyRate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param refuseReasons
	 */
	public void setRefuseReasons(String refuseReasons) {
		this.refuseReasons = refuseReasons;
	}
	
    /**
     * @return refuseReasons
     */
	public String getRefuseReasons() {
		return this.refuseReasons;
	}
	
	/**
	 * @param approveNodeType
	 */
	public void setApproveNodeType(String approveNodeType) {
		this.approveNodeType = approveNodeType;
	}
	
    /**
     * @return approveNodeType
     */
	public String getApproveNodeType() {
		return this.approveNodeType;
	}
	
	/**
	 * @param curtApproveNode
	 */
	public void setCurtApproveNode(String curtApproveNode) {
		this.curtApproveNode = curtApproveNode;
	}
	
    /**
     * @return curtApproveNode
     */
	public String getCurtApproveNode() {
		return this.curtApproveNode;
	}
	
	/**
	 * @param curtApprIdJobNo
	 */
	public void setCurtApprIdJobNo(String curtApprIdJobNo) {
		this.curtApprIdJobNo = curtApprIdJobNo;
	}
	
    /**
     * @return curtApprIdJobNo
     */
	public String getCurtApprIdJobNo() {
		return this.curtApprIdJobNo;
	}
	
	/**
	 * @param appTime
	 */
	public void setAppTime(String appTime) {
		this.appTime = appTime;
	}
	
    /**
     * @return appTime
     */
	public String getAppTime() {
		return this.appTime;
	}
	
	/**
	 * @param whetherStockUser
	 */
	public void setWhetherStockUser(String whetherStockUser) {
		this.whetherStockUser = whetherStockUser;
	}
	
    /**
     * @return whetherStockUser
     */
	public String getWhetherStockUser() {
		return this.whetherStockUser;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}