package cn.com.yusys.yusp.service.server.xdsx0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.DiscountBp;
import cn.com.yusys.yusp.domain.FdyddWhbxdApproval;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.req.Xdsx0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.resp.Xdsx0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.req.Xdsx0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.resp.Xdsx0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.DiscountBpMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.RiskXdGuarantyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:查询优惠（省心E付）
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdsx0011Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0011Service.class);
    @Autowired
    private CommonService commonService;
    @Autowired
    private DiscountBpMapper discountBpMapper;
    /**
     * 查询优惠（省心E付）
     *
     * @param xdsx0011DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0011DataRespDto xdsx0011(Xdsx0011DataReqDto xdsx0011DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value);
        //返回对象
        Xdsx0011DataRespDto xdsx0011DataRespDto = new Xdsx0011DataRespDto();
        try {
            //赋值剩余优惠次数
            xdsx0011DataRespDto.setPreferTimes("");
            //赋值优惠点数
            xdsx0011DataRespDto.setPreferPoints("");
            xdsx0011DataRespDto.setErorcd("0000");
            xdsx0011DataRespDto.setErortx("交易成功");
            if("".equals(xdsx0011DataReqDto.getContNo())){
                xdsx0011DataRespDto.setErorcd("9999");
                xdsx0011DataRespDto.setErortx("贷款合同编号不能为空");
                return xdsx0011DataRespDto;
            }
            if("".equals(xdsx0011DataReqDto.getCusId())){
                xdsx0011DataRespDto.setErorcd("9999");
                xdsx0011DataRespDto.setErortx("客户编号不能为空");
                return xdsx0011DataRespDto;
            }
            DiscountBp discountBp = new DiscountBp();
            //获取系统当前日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            QueryModel model = new QueryModel();
            model.addCondition("openday",openDay);
            model.addCondition("cus_id",xdsx0011DataReqDto.getCusId());
            discountBp = discountBpMapper.queryDiscountBpByCusIdAndDate(model);
            if(discountBp!=null && !"".equals(discountBp.getAppMiniRate())){
                //赋值优惠点数
                xdsx0011DataRespDto.setPreferPoints(discountBp.getAppMiniRate());

                //贷款总条数
                String sxefNumber = discountBp.getSxefNumber();
                //起始时间
                String bpDiscountStartDate = discountBp.getDiscountStartDate();
                //结束时间
                String discountEndDate = discountBp.getDiscountEndDate();
                if(!"".equals(bpDiscountStartDate) && !"".equals(discountEndDate)){
                    model.addCondition("discount_start_date",bpDiscountStartDate);
                    model.addCondition("discount_end_date",discountEndDate);
                    model.addCondition("cont_no",xdsx0011DataReqDto.getContNo());
                    //查询优惠历史条数
                    BigDecimal hisCount = discountBpMapper.queryContHisDiscountBpConStr(model);
                    //计算剩余优惠次数
                    BigDecimal syCount = new BigDecimal(sxefNumber).subtract(hisCount);
                    //赋值剩余优惠次数
                    xdsx0011DataRespDto.setPreferTimes(syCount+"");

                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value);
        return xdsx0011DataRespDto;
    }
}
