/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuilProject
 * @类描述: guar_inf_buil_project数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_buil_project")
public class GuarInfBuilProject extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 在建工程关联的土地证 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 40)
	private String landNo;
	
	/** 工程项目名称 **/
	@Column(name = "PROJECT_NAME", unique = false, nullable = true, length = 100)
	private String projectName;
	
	/** 建设用地规划许可证号 **/
	@Column(name = "LAND_LICENCE", unique = false, nullable = true, length = 40)
	private String landLicence;
	
	/** 建设工程规划许可证号 **/
	@Column(name = "LAYOUT_LICENCE", unique = false, nullable = true, length = 40)
	private String layoutLicence;
	
	/** 施工许可证号 **/
	@Column(name = "CONS_LICENCE", unique = false, nullable = true, length = 40)
	private String consLicence;
	
	/** 工程启动日期 **/
	@Column(name = "PROJ_START_DATE", unique = false, nullable = true, length = 10)
	private String projStartDate;
	
	/** 工程竣工预计交付日期 **/
	@Column(name = "PLAN_FINISH_DATE", unique = false, nullable = true, length = 10)
	private String planFinishDate;
	
	/** 工程预计总造价（元） **/
	@Column(name = "PROJ_PLAN_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal projPlanAccnt;
	
	/** 工程契约合同编号 **/
	@Column(name = "BUSINESS_CONTRACT_NO", unique = false, nullable = true, length = 40)
	private String businessContractNo;
	
	/** 合同签订日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 合同签订金额（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal buildArea;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 详细地址 **/
	@Column(name = "DETAILS_ADDRESS", unique = false, nullable = true, length = 100)
	private String detailsAddress;
	
	/** 所得税率 **/
	@Column(name = "GET_TAX", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal getTax;
	
	/** 房产类别 STD_ZB_HOUSE_TYPE **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 10)
	private String houseType;
	
	/** 建筑年份 **/
	@Column(name = "ARCH_YEAR", unique = false, nullable = true, length = 10)
	private String archYear;
	
	/** 已缴纳土地出让金（元） **/
	@Column(name = "LAND_SELL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landSellAmt;
	
	/** 已投入工程款（元） **/
	@Column(name = "USE_PROJECT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal useProjectAmt;
	
	/** 已完成工程量 **/
	@Column(name = "FINISH_PROJECT_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finishProjectQnt;
	
	/** 在建工程类型 STD_ZB_PROJECT_TYPE **/
	@Column(name = "BUILT_PROJECT_TYPE", unique = false, nullable = true, length = 10)
	private String builtProjectType;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal landUseYears;
	
	/** 土地用途 STD_ZB_LANDYT **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 500)
	private String landExplain;
	
	/** 是否欠工程款 **/
	@Column(name = "IS_DELAY_PROJECT_AMT", unique = false, nullable = true, length = 10)
	private String isDelayProjectAmt;
	
	/** 欠工程款金额 **/
	@Column(name = "DELAY_PROJECT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal delayProjectAmt;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
    /**
     * @return projectName
     */
	public String getProjectName() {
		return this.projectName;
	}
	
	/**
	 * @param landLicence
	 */
	public void setLandLicence(String landLicence) {
		this.landLicence = landLicence;
	}
	
    /**
     * @return landLicence
     */
	public String getLandLicence() {
		return this.landLicence;
	}
	
	/**
	 * @param layoutLicence
	 */
	public void setLayoutLicence(String layoutLicence) {
		this.layoutLicence = layoutLicence;
	}
	
    /**
     * @return layoutLicence
     */
	public String getLayoutLicence() {
		return this.layoutLicence;
	}
	
	/**
	 * @param consLicence
	 */
	public void setConsLicence(String consLicence) {
		this.consLicence = consLicence;
	}
	
    /**
     * @return consLicence
     */
	public String getConsLicence() {
		return this.consLicence;
	}
	
	/**
	 * @param projStartDate
	 */
	public void setProjStartDate(String projStartDate) {
		this.projStartDate = projStartDate;
	}
	
    /**
     * @return projStartDate
     */
	public String getProjStartDate() {
		return this.projStartDate;
	}
	
	/**
	 * @param planFinishDate
	 */
	public void setPlanFinishDate(String planFinishDate) {
		this.planFinishDate = planFinishDate;
	}
	
    /**
     * @return planFinishDate
     */
	public String getPlanFinishDate() {
		return this.planFinishDate;
	}
	
	/**
	 * @param projPlanAccnt
	 */
	public void setProjPlanAccnt(java.math.BigDecimal projPlanAccnt) {
		this.projPlanAccnt = projPlanAccnt;
	}
	
    /**
     * @return projPlanAccnt
     */
	public java.math.BigDecimal getProjPlanAccnt() {
		return this.projPlanAccnt;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo;
	}
	
    /**
     * @return businessContractNo
     */
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(java.math.BigDecimal buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public java.math.BigDecimal getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param detailsAddress
	 */
	public void setDetailsAddress(String detailsAddress) {
		this.detailsAddress = detailsAddress;
	}
	
    /**
     * @return detailsAddress
     */
	public String getDetailsAddress() {
		return this.detailsAddress;
	}
	
	/**
	 * @param getTax
	 */
	public void setGetTax(java.math.BigDecimal getTax) {
		this.getTax = getTax;
	}
	
    /**
     * @return getTax
     */
	public java.math.BigDecimal getGetTax() {
		return this.getTax;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param archYear
	 */
	public void setArchYear(String archYear) {
		this.archYear = archYear;
	}
	
    /**
     * @return archYear
     */
	public String getArchYear() {
		return this.archYear;
	}
	
	/**
	 * @param landSellAmt
	 */
	public void setLandSellAmt(java.math.BigDecimal landSellAmt) {
		this.landSellAmt = landSellAmt;
	}
	
    /**
     * @return landSellAmt
     */
	public java.math.BigDecimal getLandSellAmt() {
		return this.landSellAmt;
	}
	
	/**
	 * @param useProjectAmt
	 */
	public void setUseProjectAmt(java.math.BigDecimal useProjectAmt) {
		this.useProjectAmt = useProjectAmt;
	}
	
    /**
     * @return useProjectAmt
     */
	public java.math.BigDecimal getUseProjectAmt() {
		return this.useProjectAmt;
	}
	
	/**
	 * @param finishProjectQnt
	 */
	public void setFinishProjectQnt(java.math.BigDecimal finishProjectQnt) {
		this.finishProjectQnt = finishProjectQnt;
	}
	
    /**
     * @return finishProjectQnt
     */
	public java.math.BigDecimal getFinishProjectQnt() {
		return this.finishProjectQnt;
	}
	
	/**
	 * @param builtProjectType
	 */
	public void setBuiltProjectType(String builtProjectType) {
		this.builtProjectType = builtProjectType;
	}
	
    /**
     * @return builtProjectType
     */
	public String getBuiltProjectType() {
		return this.builtProjectType;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(java.math.BigDecimal landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public java.math.BigDecimal getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param isDelayProjectAmt
	 */
	public void setIsDelayProjectAmt(String isDelayProjectAmt) {
		this.isDelayProjectAmt = isDelayProjectAmt;
	}
	
    /**
     * @return isDelayProjectAmt
     */
	public String getIsDelayProjectAmt() {
		return this.isDelayProjectAmt;
	}
	
	/**
	 * @param delayProjectAmt
	 */
	public void setDelayProjectAmt(java.math.BigDecimal delayProjectAmt) {
		this.delayProjectAmt = delayProjectAmt;
	}
	
    /**
     * @return delayProjectAmt
     */
	public java.math.BigDecimal getDelayProjectAmt() {
		return this.delayProjectAmt;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}