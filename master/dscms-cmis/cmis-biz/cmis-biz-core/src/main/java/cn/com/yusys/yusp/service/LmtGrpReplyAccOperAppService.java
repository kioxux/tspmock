package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.domain.LmtGrpReplyAcc;
import cn.com.yusys.yusp.domain.LmtGrpReplyAccOperApp;
import cn.com.yusys.yusp.domain.LmtReplyAcc;
import cn.com.yusys.yusp.domain.LmtReplyAccOperApp;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSub;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSubPrd;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.resp.CmisLmt0006RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.resp.CmisLmt0007RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015GrpLmtAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGrpReplyAccOperAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccOperAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:42:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpReplyAccOperAppService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtGrpReplyAccOperAppService.class);

    @Resource
    private LmtGrpReplyAccOperAppMapper lmtGrpReplyAccOperAppMapper;

    @Autowired
    private LmtReplyAccOperAppSubService lmtReplyAccOperAppSubService;

    @Autowired
    private LmtReplyAccOperAppSubPrdService lmtReplyAccOperAppSubPrdService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService; // 集团批复

    @Autowired
    private LmtReplyAccService lmtReplyAccService; // 单一批复

    @Autowired
    private LmtReplyAccOperAppService lmtReplyAccOperAppService; // 单一额度授信操作申请

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpReplyAccOperApp selectByPrimaryKey(String pkId) {
        return lmtGrpReplyAccOperAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtGrpReplyAccOperApp> selectAll(QueryModel model) {
        List<LmtGrpReplyAccOperApp> records = (List<LmtGrpReplyAccOperApp>) lmtGrpReplyAccOperAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpReplyAccOperApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpReplyAccOperApp> list = lmtGrpReplyAccOperAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtGrpReplyAccOperApp record) {
        return lmtGrpReplyAccOperAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpReplyAccOperApp record) {
        return lmtGrpReplyAccOperAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(LmtGrpReplyAccOperApp record) {
        return lmtGrpReplyAccOperAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpReplyAccOperApp record) {
        return lmtGrpReplyAccOperAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpReplyAccOperAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpReplyAccOperAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: guideSave
     * @方法描述: 集团客户额度冻结/解冻/终止向导申请新增保存
     * @参数与返回说明:
     * @算法描述: 1.根据集团客户号查询额度系统的集团客户额度查询接口
     * 2.根据返回结果保存数据到授信额度冻结/解冻/终止相关表
     * 2.1 加工批复台账操作表数据并插入数据库
     * 2.2 把批复台账操作表的数据插入授信台账额度操作分项表
     * 2.2.1 额度系统的客户额度查询接口查询出父节点并判断是否为null，为null则插入到授信台账额度操作分项表，不为null则插入到插入授信台账额度操作分项适用品种明细表
     * 2.2.1通过客户号查询批复台账操作表并将查询出来的数据插入授信台账额度操作分项表
     * 2.3 把批复台账操作表的数据插入授信台账额度操作分项适用品种明细表
     * 2.3.1
     * @创建人: yangwl
     * @创建时间: 2021-05-13 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String guideSave(LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) throws Exception {
        User userInfo = SessionUtils.getUserInformation();
        CmisLmt0015ReqDto cmisLmt0015ReqDto = new CmisLmt0015ReqDto();
        cmisLmt0015ReqDto.setSerno(UUID.randomUUID().toString());
        cmisLmt0015ReqDto.setInstuCde(CmisCommonUtils.getInstucde(userInfo.getOrg().getCode()));
        cmisLmt0015ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_02);
        cmisLmt0015ReqDto.setCusId(lmtGrpReplyAccOperApp.getGrpCusId());
        cmisLmt0015ReqDto.setStartNum(0);
        cmisLmt0015ReqDto.setPageCount(1);
        //根据客户号查询额度系统的客户额度查询接口
        log.info("根据客户号查询额度系统的客户额度查询接口,请求报文:"+ JSON.toJSONString(cmisLmt0015ReqDto));
        ResultDto<CmisLmt0015RespDto> cmisLmt0015RespDtoResultDto = cmisLmtClientService.cmisLmt0015(cmisLmt0015ReqDto);
        log.info("根据客户号查询额度系统的客户额度查询接口,响应报文:"+ JSON.toJSONString(cmisLmt0015RespDtoResultDto));
        if (cmisLmt0015RespDtoResultDto != null && cmisLmt0015RespDtoResultDto.getData() != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0015RespDtoResultDto.getData().getErrorCode())) {
            log.info("根据客户号查询额度系统的客户额度查询接口成功");
        } else {
            throw new Exception("根据客户号查询额度系统的客户额度查询接口异常");
        }
        log.info("根据额度系统的客户额度查询接口获取 集团授信额度操作申请表数据------------start-----------");
        List<CmisLmt0015GrpLmtAccListRespDto> grpLmtAccList = cmisLmt0015RespDtoResultDto.getData().getGrpLmtAccList();
        if (grpLmtAccList.size() > 0) {
            log.info("根据额度系统的客户额度查询接口获取 集团授信额度操作申请表数据------------集团客户号:" + grpLmtAccList.get(0).getGrpCusNo() + "-----------");
            // 根据集团客户号去查询集团授信台账编号
            log.info("根据集团客户号去查询集团授信台账编号:" + grpLmtAccList.get(0).getGrpCusNo() + "-----------");
            QueryModel model = new QueryModel();
            model.getCondition().put("grpCusId", lmtGrpReplyAccOperApp.getGrpCusId());
            model.getCondition().put("replyStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            // 正常来说一个客户只会存在一条生效的批复数据
            List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccService.queryByGrpCusId(model);
            if (lmtGrpReplyAccList.size() == 0) {
                throw new Exception("根据集团客户号查询授信台账编号异常");
            }
            // 去第一个数据的  批复编号
            lmtGrpReplyAccOperApp.setGrpAccNo(lmtGrpReplyAccList.get(0).getGrpAccNo());
            // 获取调额度系统查出的一条数据
            String grpSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            //赋值
            lmtGrpReplyAccOperApp.setPkId(UUID.randomUUID().toString());
            lmtGrpReplyAccOperApp.setGrpSerno(grpSerno);
            lmtGrpReplyAccOperApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtGrpReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);

            if (userInfo == null) {
                throw new Exception("当前登录用户为空或不存在");
            } else {
                lmtGrpReplyAccOperApp.setInputId(userInfo.getLoginCode());
                lmtGrpReplyAccOperApp.setInputBrId(userInfo.getOrg().getCode());
                lmtGrpReplyAccOperApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                lmtGrpReplyAccOperApp.setUpdId(userInfo.getLoginCode());
                lmtGrpReplyAccOperApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpReplyAccOperApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpReplyAccOperApp.setManagerId(userInfo.getLoginCode());
                lmtGrpReplyAccOperApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtGrpReplyAccOperApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                lmtGrpReplyAccOperApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                //批复台账操作表数据并插入数据库
                int result = lmtGrpReplyAccOperAppMapper.insertSelective(lmtGrpReplyAccOperApp);
                if (result < 0) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",批复台账操作表数据插入数据库失败！");
                }
            }
        }
        log.info("根据额度系统的客户额度查询接口获取 集团授信额度操作申请表数据------------end-----------");
        log.info("根据额度系统的客户额度查询接口获取 集团名下个人授信额度操作申请表数据------------start-----------");
        LmtReplyAccOperApp lmtReplyAccOperApp = new LmtReplyAccOperApp();
        List<CmisLmt0015LmtAccListRespDto> lmtAccList = cmisLmt0015RespDtoResultDto.getData().getLmtAccList();
        if (lmtAccList != null && lmtAccList.size() > 0) {
            for (CmisLmt0015LmtAccListRespDto cmisLmt0015LmtAccListRespDto : lmtAccList) {
                // 获取调额度系统查出的一条数据
                String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
                //赋值
                lmtReplyAccOperApp.setPkId(UUID.randomUUID().toString());
                lmtReplyAccOperApp.setSerno(serno);
                lmtReplyAccOperApp.setLmtAccOperType(lmtGrpReplyAccOperApp.getLmtAccOperType());
                lmtReplyAccOperApp.setCusId(cmisLmt0015LmtAccListRespDto.getCusId());
                lmtReplyAccOperApp.setCusName(cmisLmt0015LmtAccListRespDto.getCusName());

                log.info("根据集团名下客户号去查询单一授信台账编号,客户号" + cmisLmt0015LmtAccListRespDto.getCusId() + "-----------");
                HashMap map = new HashMap();
                map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                map.put("accStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
                map.put("cusId", cmisLmt0015LmtAccListRespDto.getCusId());
                // 正常来说一个客户只会存在一条生效的批复数据
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                if (lmtReplyAcc == null) {
                    throw new Exception("根据集团客户名下单一客户号查询授信台账编号异常");
                }
                log.info("根据集团名下客户号去查询单一授信台账编号:" + lmtReplyAcc.getAccNo() + "-----------");
                lmtReplyAccOperApp.setLmtAccNo(lmtReplyAcc.getAccNo());
                lmtReplyAccOperApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);

                //User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    throw new Exception("当前登录用户为空或不存在");
                } else {
                    lmtReplyAccOperApp.setInputId(userInfo.getLoginCode());
                    lmtReplyAccOperApp.setInputBrId(userInfo.getOrg().getCode());
                    lmtReplyAccOperApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtReplyAccOperApp.setUpdId(userInfo.getLoginCode());
                    lmtReplyAccOperApp.setUpdBrId(userInfo.getOrg().getCode());
                    lmtReplyAccOperApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtReplyAccOperApp.setManagerId(userInfo.getLoginCode());
                    lmtReplyAccOperApp.setManagerBrId(userInfo.getOrg().getCode());
                    lmtReplyAccOperApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                    lmtReplyAccOperApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                    //批复台账操作表数据并插入数据库
                    int result = lmtReplyAccOperAppService.insert(lmtReplyAccOperApp);
                    if (result < 0) {
                        //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                        throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",集团项下单一客户额度冻结/解冻操作表数据插入数据库失败！");
                    }
                }
                log.info("继续处理当前客户名下的 分项及分项明细 信息-----------");
                //根据客户号查询额度系统的客户额度分项列表
                List<CmisLmt0015LmtSubAccListRespDto> lmtSubAccList = cmisLmt0015RespDtoResultDto.getData().getLmtSubAccList();
                if (lmtSubAccList != null && lmtSubAccList.size() != 0) {
                    log.info("根据客户号查询额度系统的客户额度额度分项成功");
                } else {
                    throw new Exception("根据客户号查询额度系统的客户额度分项适用品种不存在!");
                }
                log.info("根据客户号查询额度系统的客户额度额度分项中处理没有父节点的数据----------------------");
                for (CmisLmt0015LmtSubAccListRespDto cmisLmt0015LmtSubAccListRespDto : lmtSubAccList) {
                    if (!cmisLmt0015LmtSubAccListRespDto.getCusId().equals(cmisLmt0015LmtAccListRespDto.getCusId())) {
                        continue;
                    }
                    // 获取父节点
                    String parentId = cmisLmt0015LmtSubAccListRespDto.getParentId();
                    if (parentId == null || "".equals(parentId)) {
                        LmtReplyAccOperAppSub lmtReplyAccOperAppSub = new LmtReplyAccOperAppSub();
                        lmtReplyAccOperAppSub.setPkId(UUID.randomUUID().toString());
                        lmtReplyAccOperAppSub.setSerno(lmtReplyAccOperApp.getSerno()); // serno 获取当前客户的申请流水号
                        lmtReplyAccOperAppSub.setSubSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()));// cmisLmt0015LmtSubAccListRespDto.getSubSerno()  写死 测试
                        lmtReplyAccOperAppSub.setLmtAccNo(cmisLmt0015LmtSubAccListRespDto.getAccNo());
                        // 授信品种编号
                        lmtReplyAccOperAppSub.setAccSubNo(cmisLmt0015LmtSubAccListRespDto.getSubSerno());
                        // 授信品种名称
                        lmtReplyAccOperAppSub.setAccSubName(cmisLmt0015LmtSubAccListRespDto.getLimitSubName());
                        lmtReplyAccOperAppSub.setIsPreLmt(cmisLmt0015LmtSubAccListRespDto.getIsPreCrd());// 是否预授信
                        lmtReplyAccOperAppSub.setGuarMode(cmisLmt0015LmtSubAccListRespDto.getSuitGuarWay());// 担保方式
                        lmtReplyAccOperAppSub.setLmtAmt(cmisLmt0015LmtSubAccListRespDto.getAvlAmt());// 授信额度

                        // 登录信息
                        lmtReplyAccOperAppSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtReplyAccOperAppSub.setInputId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSub.setInputBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSub.setUpdId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSub.setUpdBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSubService.insert(lmtReplyAccOperAppSub);
                    }
                }
                log.info("根据客户号查询额度系统的客户额度额度分项中处理有父节点的数据-----------------------");
                //根据客户号查询额度系统的客户额度分项列表
                for (CmisLmt0015LmtSubAccListRespDto cmisLmt0015LmtSubAccListRespDto : lmtSubAccList) {
                    if (!cmisLmt0015LmtSubAccListRespDto.getCusId().equals(cmisLmt0015LmtAccListRespDto.getCusId())) {
                        continue;
                    }
                    // 获取父节点
                    String parentId = cmisLmt0015LmtSubAccListRespDto.getParentId();
                    if (parentId != null && !"".equals(parentId)) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("serno", serno);
                        hashMap.put("accSubNo", parentId);
                        List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubs = lmtReplyAccOperAppSubService.selectByParams(hashMap);
                        // 正常来说 查出的分项值可能存在一笔数据  则取当前第一条
                        log.info("根据锁定的分项信息,插入分项项下的适用品种明细信息-----------------------");
                        for (LmtReplyAccOperAppSub lmtReplyAccOperAppSub : lmtReplyAccOperAppSubs) {
                            LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd = new LmtReplyAccOperAppSubPrd();
                            lmtReplyAccOperAppSubPrd.setPkId(UUID.randomUUID().toString());
                            lmtReplyAccOperAppSubPrd.setSubSerno(lmtReplyAccOperAppSub.getSubSerno());
                            lmtReplyAccOperAppSubPrd.setSubPrdSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()));
                            lmtReplyAccOperAppSubPrd.setAccSubNo(cmisLmt0015LmtSubAccListRespDto.getParentId());
                            lmtReplyAccOperAppSubPrd.setAccSubPrdNo(cmisLmt0015LmtSubAccListRespDto.getSubSerno());
                            lmtReplyAccOperAppSubPrd.setCusId(cmisLmt0015LmtAccListRespDto.getCusId());
                            lmtReplyAccOperAppSubPrd.setCusName(cmisLmt0015LmtAccListRespDto.getCusName());
                            lmtReplyAccOperAppSubPrd.setLmtBizType(cmisLmt0015LmtSubAccListRespDto.getLimitSubNo());
                            lmtReplyAccOperAppSubPrd.setLmtBizTypeName(cmisLmt0015LmtSubAccListRespDto.getLimitSubName());
                            lmtReplyAccOperAppSubPrd.setIsRevolvLimit(cmisLmt0015LmtSubAccListRespDto.getIsRevolv());
                            lmtReplyAccOperAppSubPrd.setGuarMode(cmisLmt0015LmtSubAccListRespDto.getSuitGuarWay());
                            lmtReplyAccOperAppSubPrd.setCurType(cmisLmt0015LmtSubAccListRespDto.getCny());
                            lmtReplyAccOperAppSubPrd.setLmtAmt(cmisLmt0015LmtSubAccListRespDto.getAvlAmt());
                            lmtReplyAccOperAppSubPrd.setStartDate(cmisLmt0015LmtSubAccListRespDto.getStartDate());
                            lmtReplyAccOperAppSubPrd.setEndDate(cmisLmt0015LmtSubAccListRespDto.getEndDate());
                            lmtReplyAccOperAppSubPrd.setLmtTerm(cmisLmt0015LmtSubAccListRespDto.getTerm());
                            lmtReplyAccOperAppSubPrd.setOrigiLmtStatus(cmisLmt0015LmtSubAccListRespDto.getStatus());
                            // 修改后额度状态
                            lmtReplyAccOperAppSubPrd.setAfterLmtStatus(lmtReplyAccOperApp.getLmtAccOperType());
                            lmtReplyAccOperAppSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                            lmtReplyAccOperAppSubPrd.setInputId(userInfo.getLoginCode());
                            lmtReplyAccOperAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                            lmtReplyAccOperAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtReplyAccOperAppSubPrd.setUpdId(userInfo.getLoginCode());
                            lmtReplyAccOperAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                            lmtReplyAccOperAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtReplyAccOperAppSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                            lmtReplyAccOperAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                            lmtReplyAccOperAppSubPrdService.insert(lmtReplyAccOperAppSubPrd);
                        }
                    }
                }
                log.info("处理集团客户及其项下的单一客户的关系表信息-----start------");
                LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
                lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
                lmtGrpMemRel.setGrpSerno(lmtGrpReplyAccOperApp.getGrpSerno());
                lmtGrpMemRel.setSingleSerno(lmtReplyAccOperApp.getSerno());
                lmtGrpMemRel.setGrpCusId(lmtGrpReplyAccOperApp.getGrpCusId());
                lmtGrpMemRel.setGrpCusName(lmtGrpReplyAccOperApp.getGrpCusName());
                lmtGrpMemRel.setCusId(lmtReplyAccOperApp.getCusId());
                lmtGrpMemRel.setCusName(lmtReplyAccOperApp.getCusName());
                //lmtGrpMemRel.setCusType();// 客户类型
                lmtGrpMemRel.setOpenLmtAmt(cmisLmt0015LmtAccListRespDto.getSpacAmt());//敞口额度
                lmtGrpMemRel.setLowRiskLmtAmt(cmisLmt0015LmtAccListRespDto.getLriskLmt());//低风险额度
                // 登录信息
                lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtGrpMemRel.setInputId(userInfo.getLoginCode());
                lmtGrpMemRel.setInputBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpMemRel.setManagerId(userInfo.getLoginCode());
                lmtGrpMemRel.setManagerBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                lmtGrpMemRelService.insertSelective(lmtGrpMemRel);
                log.info("处理集团客户及其项下的单一客户的关系表信息-----end------");
            }
            log.info("根据额度系统的客户额度查询接口获取 集团名下个人授信额度操作申请表数据------------end-----------");
        } else {
            throw new Exception("客户额度台账查询异常，查询为空");
        }
        return lmtGrpReplyAccOperApp.getGrpSerno();
    }

    /**
     * @方法名称: queryAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtGrpReplyAccOperApp> queryAll(QueryModel model) {
        List<LmtGrpReplyAccOperApp> records = (List<LmtGrpReplyAccOperApp>) lmtGrpReplyAccOperAppMapper.queryAll(model);
        return records;
    }

    /**
     * @方法名称: queryAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtGrpReplyAccOperApp> queryHis(QueryModel model) {
        List<LmtGrpReplyAccOperApp> records = (List<LmtGrpReplyAccOperApp>) lmtGrpReplyAccOperAppMapper.queryHis(model);
        return records;
    }

    /**
     * @方法名称: getSubAndPrd
     * @方法描述: 集团客户额度冻结/解冻/终止额度分项品种列表展示
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map getSubAndPrd(String grpSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        String subSerno = "";
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        LmtReplyAccOperAppSub lmtReplyAccOperAppSub = new LmtReplyAccOperAppSub();
        try {
            paramsSub.put("grpSerno", grpSerno);
            List<LmtReplyAccOperAppSub> subList = lmtReplyAccOperAppSubService.selectByParams(paramsSub);
            if (subList.size() > 0) {
                for (int i = 0; i < subList.size(); i++) {
                    lmtReplyAccOperAppSub = subList.get(i);
                    subSerno = lmtReplyAccOperAppSub.getSubSerno();
                    paramsSubPrd.put("subSerno", subSerno);
                    List<LmtReplyAccOperAppSubPrd> subListPrd = lmtReplyAccOperAppSubPrdService.selectByParams(paramsSubPrd);
                    tempMap = new HashMap();
                    tempMap.put("subSerno", lmtReplyAccOperAppSub.getSubSerno());
                    tempMap.put("bizTypeName", lmtReplyAccOperAppSub.getAccSubName());
                    tempMap.put("isPreLmt", lmtReplyAccOperAppSub.getIsPreLmt());
                    tempMap.put("guarMode", lmtReplyAccOperAppSub.getGuarMode());
                    tempMap.put("lmtAmt", lmtReplyAccOperAppSub.getLmtAmt());
                    tempMap.put("children", subListPrd);
                    list.add(tempMap);
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subList", list);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateoperapp
     * @方法描述: 集团客户额度冻结/解冻/终止申请暂存
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateoperapp(LmtGrpReplyAccOperApp record) {
        User userInfo = SessionUtils.getUserInformation();
        record.setUpdId(userInfo.getLoginCode());
        record.setUpdBrId(userInfo.getOrg().getCode());
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        return lmtGrpReplyAccOperAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: showdetial
     * @方法描述: 根据集团客户号查询授信成员信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtGrpReplyAccOperApp> showdetial(String grpSerno) {
        List<LmtGrpReplyAccOperApp> showdetial = lmtGrpReplyAccOperAppMapper.showdetial(grpSerno);
        return showdetial;
    }

    /**
     * @方法名称: queryLmtReplyAccOperAppByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-016 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpReplyAccOperApp> queryLmtgrpReplyAccOperAppByParams(HashMap<String, String> queryMap) {
        return lmtGrpReplyAccOperAppMapper.queryLmtgrpReplyAccOperAppByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtGrpReplyAccOperAppByGrpSerno
     * @方法描述: 通过申请流水号查询申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-016 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReplyAccOperApp queryLmtGrpReplyAccOperAppByGrpSerno(String grpSerno) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("grpSerno", grpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpReplyAccOperApp> lmtGrpReplyAccOperAppList = queryLmtgrpReplyAccOperAppByParams(queryMap);
        if (lmtGrpReplyAccOperAppList != null && lmtGrpReplyAccOperAppList.size() == 1) {
            return lmtGrpReplyAccOperAppList.get(0);
        } else {
            throw new Exception("查询授信台账操作申请表异常");
        }
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 授信冻结解冻终止流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: yangwl
     * @创建时间: 2021-05-16 15:30:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String grpSerno) throws Exception {
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = queryLmtGrpReplyAccOperAppByGrpSerno(grpSerno);
        lmtGrpReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        this.update(lmtGrpReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信冻结解冻终止流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: yangwl
     * @创建时间: 2021-05-16 15:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String grpSerno) throws Exception {
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = queryLmtGrpReplyAccOperAppByGrpSerno(grpSerno);
        lmtGrpReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        this.update(lmtGrpReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信冻结解冻终止流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String grpSerno) throws Exception {
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = queryLmtGrpReplyAccOperAppByGrpSerno(grpSerno);
        lmtGrpReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        this.update(lmtGrpReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信冻结解冻终止流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.根据冻结解冻状态更新台账中的状态
     * 5.推送批复台账至额度系统
     * @创建人: yangwl
     * @创建时间: 2021-05-16 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String GrpSerno, String currentUserId, String currentOrgId) throws Exception {
        // 将审批状态更新为997
        LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp = queryLmtGrpReplyAccOperAppByGrpSerno(GrpSerno);
        lmtGrpReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.updateSelective(lmtGrpReplyAccOperApp);
        User userInfo = SessionUtils.getUserInformation();

        // 通过集团申请流水号查询关系表数据
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(GrpSerno);
        if (lmtGrpMemRels != null & lmtGrpMemRels.size() > 0) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                String serno = lmtGrpMemRel.getSingleSerno();
                // 通过授信台账操作流水号查询授信台账分项适用品种数据 并将修改后的额度状态更新到台账中
                lmtReplyAccSubPrdService.updateLmtReplyAccSubPrdBySerno(serno);
                log.info("通过流水号查询授信批复台账额度申请信息:" + serno);
                LmtReplyAccOperApp lmtReplyAccOperApp = lmtReplyAccOperAppService.queryLmtReplyAccOperAppBySerno(serno);
                log.info("通过流水号查询授信批复台账额度申请分项信息:" + serno);
                List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubs = lmtReplyAccOperAppSubService.queryBySerno(serno);
                // 发送额度报文拼接
                CmisLmt0006ReqDto cmisLmt0006ReqDto = new CmisLmt0006ReqDto();
                List<CmisLmt0006ApprListReqDto> cmisLmt0006ApprListReqDtos = new ArrayList<CmisLmt0006ApprListReqDto>();
                List<CmisLmt0006ApprSubListReqDto> cmisLmt0006ApprSubListReqDtos = new ArrayList<CmisLmt0006ApprSubListReqDto>();
                // 组装额度系统报文 字段部分
                cmisLmt0006ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
                cmisLmt0006ReqDto.setInputId(userInfo.getLoginCode());
                cmisLmt0006ReqDto.setInputBrId(userInfo.getOrg().getCode());
                cmisLmt0006ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                cmisLmt0006ReqDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_02);
                for (LmtReplyAccOperAppSub lmtReplyAccOperAppSub : lmtReplyAccOperAppSubs) {
                    log.info("通过台账分项流水号查询授信批复台账额度申请分项品种信息:" + lmtReplyAccOperAppSub.getSubSerno());
                    HashMap<String, String> map = new HashMap();
                    map.put("subSerno", lmtReplyAccOperAppSub.getSubSerno());
                    List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrds = lmtReplyAccOperAppSubPrdService.selectByParams(map);
                    for (LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrds) {
                        log.info("拼接额度报文中授信分项明细列表,分项明细流水号为:" + lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                        CmisLmt0006ApprSubListReqDto cmisLmt0006ApprSubListReqDto = new CmisLmt0006ApprSubListReqDto();
                        if (!StringUtils.isBlank(lmtReplyAccOperAppSubPrd.getAfterLmtStatus())) {
                            cmisLmt0006ApprSubListReqDto.setApprSubSerno(lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                            cmisLmt0006ApprSubListReqDto.setCusId(lmtReplyAccOperAppSubPrd.getCusId());
                            // 加入 列表
                            cmisLmt0006ApprSubListReqDtos.add(cmisLmt0006ApprSubListReqDto);
                        }
                    }
                }
                CmisLmt0006ApprListReqDto cmisLmt0006ApprListReqDto = new CmisLmt0006ApprListReqDto();
                cmisLmt0006ApprListReqDto.setApprSerno(lmtReplyAccOperApp.getLmtAccNo());
                cmisLmt0006ApprListReqDto.setOptType(CmisCommonConstants.LMT_OPER_OPT_TYPE_01);
                cmisLmt0006ApprListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                cmisLmt0006ApprListReqDtos.add(cmisLmt0006ApprListReqDto);
                cmisLmt0006ReqDto.setApprList(cmisLmt0006ApprListReqDtos);
                cmisLmt0006ReqDto.setApprSubList(cmisLmt0006ApprSubListReqDtos);
                if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_01).equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    log.info("额度冻结【cmislmt0006】操作开始,请求报文:{}", JSON.toJSONString(cmisLmt0006ReqDto));
                    ResultDto<CmisLmt0006RespDto> cmisLmt0006RespDtoResultDto = cmisLmtClientService.cmisLmt0006(cmisLmt0006ReqDto);
                    log.info("额度系统返回：{}", JSON.toJSONString(cmisLmt0006RespDtoResultDto));
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0006RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0006RespDtoResultDto.getCode(), cmisLmt0006RespDtoResultDto.getMessage());
                    }
                } else if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_02).equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    CmisLmt0007ReqDto cmisLmt0007ReqDto = new CmisLmt0007ReqDto();
                    BeanUtils.copyProperties(cmisLmt0006ReqDto, cmisLmt0007ReqDto);
                    log.info("额度解冻操作开始,请求报文:{}", JSON.toJSONString(cmisLmt0007ReqDto));
                    ResultDto<CmisLmt0007RespDto> cmisLmt0007RespDtoResultDto = cmisLmtClientService.cmisLmt0007(cmisLmt0007ReqDto);
                    log.info("额度系统返回：{}", JSON.toJSONString(cmisLmt0007RespDtoResultDto));
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0007RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0007RespDtoResultDto.getCode(), cmisLmt0007RespDtoResultDto.getMessage());
                    }
                } else if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_03).equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
                    BeanUtils.copyProperties(cmisLmt0006ReqDto, cmisLmt0008ReqDto);
                    log.info("额度终止操作开始,流水号为:{}", JSON.toJSONString(cmisLmt0008ReqDto));
                    ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDtoResultDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
                    log.info("额度系统返回：{}", JSON.toJSONString(cmisLmt0008RespDtoResultDto));
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0008RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0008RespDtoResultDto.getCode(), cmisLmt0008RespDtoResultDto.getMessage());
                    }
                } else {
                    throw BizException.error(null, EcbEnum.LMT_RE_ACC_OPER_APP_TYPE_EXCEPTION_4.key, EcbEnum.LMT_RE_ACC_OPER_APP_TYPE_EXCEPTION_4.value);
                }
            }
        }
    }

    /**
     * @函数名称:selectLmtReplyAccOperAppByGrpSerno
     * @函数描述:集团客户额度冻结/解冻/终止申请根据流水号查询
     * @创建者: yangwl
     * @创建时间: 2021/5/21
     * @参数与返回说明:
     * @算法描述:
     */
    public LmtGrpReplyAccOperApp selectLmtGrpReplyAccOperAppByGrpSerno(String grpSerno) {
        return lmtGrpReplyAccOperAppMapper.selectLmtGrpReplyAccOperAppByGrpSerno(grpSerno);
    }

    /**
     * @函数名称:selectLmtGrpReplyAccOperApp
     * @函数描述:查询客户额度冻结/解冻/终止申请是否已存在
     * @创建者: yangwl
     * @创建时间: 2021/6/24
     * @参数与返回说明:
     * @算法描述:
     */
    public Map selectLmtGrpReplyAccOperApp(LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            List<LmtGrpReplyAccOperApp> lmtGrpReplyAccOperApps = lmtGrpReplyAccOperAppMapper.selectLmtGrpReplyAccOperApp(lmtGrpReplyAccOperApp);
            if (lmtGrpReplyAccOperApps.size() > 0) {
                if ("01".equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_FRO_EXISTS_EXCEPTION.key;
                    rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_FRO_EXISTS_EXCEPTION.value;
                } else if ("02".equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_THAW_EXISTS_EXCEPTION.key;
                    rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_THAW_EXISTS_EXCEPTION.value;
                } else if ("03".equals(lmtGrpReplyAccOperApp.getLmtAccOperType())) {
                    rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_TERM_EXISTS_EXCEPTION.key;
                    rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_TERM_EXISTS_EXCEPTION.value;
                }
            } else {
                serno = this.guideSave(lmtGrpReplyAccOperApp);
            }
        } catch (YuspException e) {
            log.error("查询额度冻结解冻异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("查询额度冻结解冻异常！", e.getMessage());

        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
            result.put("serno", serno);
        }
        return result;
    }
}
