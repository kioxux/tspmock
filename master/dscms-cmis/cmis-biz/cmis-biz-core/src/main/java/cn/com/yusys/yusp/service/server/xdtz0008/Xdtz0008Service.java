package cn.com.yusys.yusp.service.server.xdtz0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0008.req.Xdtz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.resp.Xdtz0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:根据客户号获取正常周转次数
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdtz0008Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0008Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：xdtz0008
     * 交易描述：根据客户号获取正常周转次数
     *
     * @param xdtz0008DataReqDo
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0008DataRespDto xdtz0008(Xdtz0008DataReqDto xdtz0008DataReqDo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataReqDo));
        Xdtz0008DataRespDto xdtz0008DataRespDto = new Xdtz0008DataRespDto();//响应Data：根据客户号获取正常周转次数
        String cusNo = xdtz0008DataReqDo.getCusNo();//客户号
        Map queryMap = new HashMap();
        queryMap.put("cus_id", cusNo);//客户号
        logger.info("根据客户号获取正常周转次数开始,查询参数为:{}", JSON.toJSONString(queryMap));
        int turnovTimes = accLoanMapper.queryTurnovTimes(queryMap);//周转次数
        logger.info("根据客户号获取正常周转次数结束,返回结果为:{}", JSON.toJSONString(turnovTimes));
        xdtz0008DataRespDto.setTurnovTimes(String.valueOf(turnovTimes));//周转次数
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataRespDto));
        return xdtz0008DataRespDto;
    }


}
