/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.LmtApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtChgSubAccLoanRel;
import cn.com.yusys.yusp.service.LmtChgSubAccLoanRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtChgSubAccLoanRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:17:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtchgsubaccloanrel")
public class LmtChgSubAccLoanRelResource {
    @Autowired
    private LmtChgSubAccLoanRelService lmtChgSubAccLoanRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtChgSubAccLoanRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtChgSubAccLoanRel> list = lmtChgSubAccLoanRelService.selectAll(queryModel);
        return new ResultDto<List<LmtChgSubAccLoanRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtChgSubAccLoanRel>> index(QueryModel queryModel) {
        List<LmtChgSubAccLoanRel> list = lmtChgSubAccLoanRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtChgSubAccLoanRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtChgSubAccLoanRel> show(@PathVariable("pkId") String pkId) {
        LmtChgSubAccLoanRel lmtChgSubAccLoanRel = lmtChgSubAccLoanRelService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtChgSubAccLoanRel>(lmtChgSubAccLoanRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtChgSubAccLoanRel> create(@RequestBody LmtChgSubAccLoanRel lmtChgSubAccLoanRel) throws URISyntaxException {
        lmtChgSubAccLoanRelService.insert(lmtChgSubAccLoanRel);
        return new ResultDto<LmtChgSubAccLoanRel>(lmtChgSubAccLoanRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtChgSubAccLoanRel lmtChgSubAccLoanRel) throws URISyntaxException {
        int result = lmtChgSubAccLoanRelService.update(lmtChgSubAccLoanRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtChgSubAccLoanRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtChgSubAccLoanRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySubPrdSerno
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querybysubprdserno")
    protected ResultDto<List<AccLoan>> queryBySubPrdSerno(@RequestBody String subPrdSerno) {
        List<AccLoan> list = lmtChgSubAccLoanRelService.queryBySubPrdSerno(subPrdSerno.replaceAll("\"", ""));
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * @函数名称:deleteByBillNo
     * @函数描述:根据借据编号逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletebybillno")
    protected ResultDto<Integer> deleteByBillNo(@RequestBody String billNo) {
        int result = lmtChgSubAccLoanRelService.deleteByBillNo(billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getByPkId
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savelmtchgsubaccloanrel")
    protected ResultDto<Map> saveLmtChgSubAccLoanRel(@RequestBody Map map) {
        Map rtnMap = lmtChgSubAccLoanRelService.saveLmtChgSubAccLoanRel(map);
        return new ResultDto<Map>(rtnMap);
    }

    /**
     * @函数名称:updateByBillNo
     * @函数描述:根据借据编号修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebybillno")
    protected ResultDto<Integer> updateByBillNo(@RequestBody Map map) {
        int result = lmtChgSubAccLoanRelService.updateByBillNo(map);
        return new ResultDto<Integer>(result);
    }
}
