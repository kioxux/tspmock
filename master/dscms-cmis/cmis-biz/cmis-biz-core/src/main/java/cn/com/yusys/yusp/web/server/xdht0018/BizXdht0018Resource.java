package cn.com.yusys.yusp.web.server.xdht0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0018.req.Xdht0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0018.resp.Xdht0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdht0018.Xdht0018Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:担保合同文本生成pdf
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0018:担保合同文本生成pdf")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0018Resource.class);

    @Autowired
    private Xdht0018Service xdht0018Service;

    /**
     * 交易码：xdht0018
     * 交易描述：担保合同文本生成pdf
     *
     * @param xdht0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("担保合同文本生成pdf")
    @PostMapping("/xdht0018")
    protected @ResponseBody
    ResultDto<Xdht0018DataRespDto> xdht0018(@Validated @RequestBody Xdht0018DataReqDto xdht0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataReqDto));
        Xdht0018DataRespDto xdht0018DataRespDto = new Xdht0018DataRespDto();// 响应Dto:担保合同文本生成pdf
        ResultDto<Xdht0018DataRespDto> xdht0018DataResultDto = new ResultDto<>();
        try {
            String cusId = xdht0018DataReqDto.getCusId();//签订人id
            String contNo = xdht0018DataReqDto.getContNo();//合同编号
            String grtContNo = xdht0018DataReqDto.getGrtContNo();//担保合同号
            String applyDate = xdht0018DataReqDto.getApplyDate();//申请日期
            /***************************************必输字段非空校验***************************************/
            if (StringUtil.isEmpty(grtContNo)) {
                xdht0018DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0018DataResultDto.setMessage("担保合同号【grtContNo】不能为空!");
                return xdht0018DataResultDto;
            }
            if (StringUtil.isEmpty(contNo)) {
                xdht0018DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0018DataResultDto.setMessage("借款合同号【contNo】不能为空！");
                return xdht0018DataResultDto;
            }
            if (StringUtil.isEmpty(cusId)) {
                xdht0018DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0018DataResultDto.setMessage("签订人id【cusId】不能为空！");
                return xdht0018DataResultDto;
            }
            // 从xdht0018DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataReqDto));
            xdht0018DataRespDto = xdht0018Service.xdht0018(xdht0018DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataRespDto));
            // 封装xdht0018DataResultDto中正确的返回码和返回信息
            String Password = xdht0018DataRespDto.getPassword();
            if("0123456".equals(Password)){
                xdht0018DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0018DataResultDto.setMessage(xdht0018DataRespDto.getUserName());
                return xdht0018DataResultDto;
            }
            xdht0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, e.getMessage());
            // 封装xdht0018DataResultDto中异常返回码和返回信息
            xdht0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0018DataRespDto到xdht0018DataResultDto中
        xdht0018DataResultDto.setData(xdht0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataRespDto));
        return xdht0018DataResultDto;
    }
}
