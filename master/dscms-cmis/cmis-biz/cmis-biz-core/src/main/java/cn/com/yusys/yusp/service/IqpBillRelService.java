/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpBillRel;
import cn.com.yusys.yusp.repository.mapper.IqpBillRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-08 17:11:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpBillRelService {

    @Autowired
    private IqpBillRelMapper iqpBillRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpBillRel selectByPrimaryKey(String pkId) {
        return iqpBillRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpBillRel> selectAll(QueryModel model) {
        List<IqpBillRel> records = (List<IqpBillRel>) iqpBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpBillRel> list = iqpBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpBillRel record) {
        return iqpBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpBillRel record) {
        return iqpBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpBillRel record) {
        return iqpBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpBillRel record) {
        return iqpBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpBillRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpBillRelMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpBillRelMapper.updateByParams(delMap);
    }

    /**
     * 通过关联主键查询借据关系数据
     * @param iqpSerno
     * @return
     */
    public IqpBillRel selectByIqpSerno(String iqpSerno) {
        return iqpBillRelMapper.selectByIqpSerno(iqpSerno);
    }


    /**
     * 通过入参查询是否存在数据
     * @param params
     * @return
     */
    public List<IqpBillRel> selectByParams(Map params) {
        return iqpBillRelMapper.selectByParams(params);
    }

    /**
     * 批量添加借据关系数据
     * @param insertMap
     * @return
     */
    public int insertForeach(Map insertMap) {
        return iqpBillRelMapper.insertForeach(insertMap);
    }

    /**
     * 通过主键逻辑删除
     * @param pkId
     * @return
     */
    public int deleteOnLogic(String pkId) {
        IqpBillRel iqpBillRel = new IqpBillRel();
        iqpBillRel.setPkId(pkId);
        iqpBillRel.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return iqpBillRelMapper.updateByPrimaryKeySelective(iqpBillRel);
    }
}
