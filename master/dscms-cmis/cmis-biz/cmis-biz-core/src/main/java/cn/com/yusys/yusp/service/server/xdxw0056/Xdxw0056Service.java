package cn.com.yusys.yusp.service.server.xdxw0056;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.req.Xdxw0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.resp.Xdxw0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 接口处理类:根据流水号取得优企贷信息的客户经理ID和客户经理名
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0056Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0056Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    @Autowired
    private CommonService commonService;

    /**
     * 根据流水号取得优企贷信息的客户经理ID和客户经理名
     * @param xdxw0056DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0056DataRespDto getManagerInfo(Xdxw0056DataReqDto xdxw0056DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataReqDto));
        Xdxw0056DataRespDto result = null;
        try {
            result = lmtSurveyReportMainInfoMapper.getManagerInfo(xdxw0056DataReqDto);
            if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getManagerId())) {
                AdminSmUserDto userDto = commonService.getByLoginCode(result.getManagerId());
                result.setManagerName(userDto.getUserName());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(result));
        return result;
    }
}
