package cn.com.yusys.yusp.service.server.xddh0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpPrePaymentMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0021Service
 * @类描述: #服务类
 * @功能描述:新增主动还款申请记录
 * @创建人:
 * @创建时间: 2021-06-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddh0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0002Service.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;
    @Autowired
    private IqpPrePaymentMapper iqpPrePaymentMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3108.Ln3108Service ln3108Service;// 业务逻辑处理类：贷款组合查询
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3100.Ln3100Service ln3100Service;// 业务逻辑处理类：贷款信息查询
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3102.Ln3102Service ln3102Service;// 业务逻辑处理类：贷款期供查询试算
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3041.Ln3041Service ln3041Service;//业务逻辑处理类：贷款归还

    /**
     * 新增还款记录</br>
     * <p>
     * 信贷提供还款交易，根据借据号判断业务类型为消费还是经营，
     * 消费贷款直接发往核心，
     * 经营类需审批。
     *
     * @param xddh0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0002DataRespDto xddh0002(Xddh0002DataReqDto xddh0002DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value);
        Xddh0002DataRespDto xddh0002DataRespDto = new Xddh0002DataRespDto();
        try {
            String billNo = xddh0002DataReqDto.getBillNo();//借据编号
            String yjhkms = xddh0002DataReqDto.getFstRepayMode();//一级还款模式
            String ejhkmx = xddh0002DataReqDto.getSedRepayMode();//二级还款模式
            BigDecimal hkje = xddh0002DataReqDto.getRepayAmt();//还款金额
            BigDecimal hkbj = xddh0002DataReqDto.getHkbj();//还款本金
            String dkhsfs = xddh0002DataReqDto.getLoanReclaimType();//贷款回收方式
            String acctno = xddh0002DataReqDto.getRepayAcctNo();//还款账户
            String acctna = xddh0002DataReqDto.getAcctName();//还款账户名称
            String curType = xddh0002DataReqDto.getCurType();
            String acctxh = xddh0002DataReqDto.getAcctSeqNo();//还款账户子序号
            String repayChannel = xddh0002DataReqDto.getRepayChannel();//还款渠道
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前营业日期

            // 判断线上还款是否为灵活还款模式，若是，则拒绝
            if ("08".equals(ejhkmx)) {
                throw BizException.error("", "9999", "线上还款暂不支持灵活还款模式！");
            }
            // 主管客户经理不好传，改为由信贷根据传的借据号查询acc_loan表中的manager_id
            AccLoan accLoan = accLoanMapper.selectByBillNo(billNo);

            String managerId = "";//客户经理
            String prdId = "";//业务品种
            String belgLine = "";//业务条线
            if (accLoan != null) {
                managerId = accLoan.getManagerId();
                prdId = accLoan.getPrdId();
                belgLine = accLoan.getBelgLine();
            } else {
                xddh0002DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0002DataRespDto.setOpMsg("数据错误，通过借据编号查询不到借据，借据编号为：" + billNo);
                return xddh0002DataRespDto;
            }
            /*****************************根据产品编号查询************************************/
            String prdType = StringUtils.EMPTY;//贷款品种类型
            String advRepayEndDate = cn.com.yusys.yusp.commons.util.StringUtils.EMPTY;//提前还款截止日期
            logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
            ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            String prdCode = prdresultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                if (CfgPrdBasicinfoDto != null) {
                    prdType = CfgPrdBasicinfoDto.getPrdType();
                    advRepayEndDate = CfgPrdBasicinfoDto.getAdvRepayEndDate();
                }
            } else {
                xddh0002DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0002DataRespDto.setOpMsg("通过产品编号查询不到产品信息，产品编号为：" + prdId);
                return xddh0002DataRespDto;
            }
            
            // 房抵e点贷默认直接发送核心 P034--房抵e点贷
            if ("P034".equals(accLoan.getPrdTypeProp())) {
                prdType = DscmsBizTzEnum.PRDTYPE_02.key;
            } else if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType) || "C".equals(repayChannel)) {//经营贷款
                prdType = DscmsBizTzEnum.PRDTYPE_01.key;
            } else if ("09".equals(prdType) || "10".equals(prdType) || "11".equals(prdType) || "12".equals(prdType)) {//消费贷款
                prdType = DscmsBizTzEnum.PRDTYPE_02.key;
            }
            logger.info("***********调用iCmisCfgClientService查询产品类别*END**************贷款品种类型为:" + prdType);

            if ("02".equals(prdType)) {//消费类贷款;直接发核心（直接送还款金额给核心）

                if ("01".equals(belgLine)) {//小微产品有还款时间和日期限制
                    //最晚还款时间校验
                    String time = DateUtils.getCurrTime();
                    Integer hour = Integer.valueOf(time.substring(0, 2));
                    if (hour >= 17 || hour <= 4) {
                        // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                        logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010030.value);//每日17点到凌晨5点,贷款不能支用及归还
                        throw BizException.error(null, EcbEnum.ECB010030.key, EcbEnum.ECB010030.value);
                    }
                    //昨晚还款日期校验
                    if (StringUtil.isNotEmpty(advRepayEndDate)) {//提前还款截止日期不为空
                        Integer today = Integer.valueOf(openDay.substring(8, 10));
                        Integer endday = Integer.valueOf(advRepayEndDate);
                        if (today > endday) {
                            logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010029.value);//每月25日至月底,贷款不能支用及归还
                            throw BizException.error(null, EcbEnum.ECB010029.key, EcbEnum.ECB010029.value);
                        }
                    }
                }

                //报文
                Xddh0005DataReqDto xddh0005DataReqDto = new Xddh0005DataReqDto();
                // 借据编号
                xddh0005DataReqDto.setBillNo(billNo);
                // 币种
                xddh0005DataReqDto.setCurType(curType);
                // 还款账号
                xddh0005DataReqDto.setRepayAcctNo(acctno);
                // 还款金额
                xddh0005DataReqDto.setRepayAmt(hkje);
                // 还款总本金
                xddh0005DataReqDto.setRepayTotlCap(hkje);
                // 还款模式
                xddh0005DataReqDto.setRepayType(yjhkms);
                // 交易流水号
                xddh0005DataReqDto.setTranSerno(billNo);
                // 一级还款模式
                xddh0005DataReqDto.setFstRepayMode(yjhkms);
                // 二级还款模式
                xddh0005DataReqDto.setSedRepayMode(ejhkmx);
                /* ****************************调用 ln3100贷款信息查询 ****************************/
                // 组装ln3100 贷款信息查询 接口请求报文
                Ln3100ReqDto ln3100ReqDto = ln3100Service.buildLn3100ReqDto(xddh0005DataReqDto);
                Ln3100RespDto ln3100RespDto = ln3100Service.ln3100(ln3100ReqDto);
                String huankzhh = ln3100RespDto.getHuankzhh();// 还款账号
                Optional.ofNullable(huankzhh).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010031.key, EcbEnum.ECB010031.value));//还款账号为空
                if (!Objects.equals(acctno, huankzhh)) {
                    // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010028.value);//还款账号与放款时预留不一致
                    if (!"022097".equals(prdId) && !"022099".equals(prdId) && !"022100".equals(prdId) && !"022101".equals(prdId)) {
                        throw BizException.error(null, EcbEnum.ECB010028.key, EcbEnum.ECB010028.value);//还款账号与放款时预留不一致
                    }
                }

                /* 调用  ln3041 贷款归还*/
                // 组装 ln3041 贷款归还 接口请求报文
                Ln3041ReqDto ln3041ReqDto = ln3041Service.buildLn3041ReqDto(xddh0005DataReqDto);
                ln3041ReqDto.setBrchno(accLoan.getFinaBrId());// 部门号,取账务机构号
                Ln3041RespDto ln3041RespDto = ln3041Service.ln3041(ln3041ReqDto);
                /* 调用 ln3108 贷款组合查询*/

            } else {//经营类贷款
                List<AccLoan> ptdkIColl = accLoanMapper.selectRepayAccLoanByParam(billNo, managerId);
                List<AccEntrustLoan> wtdkIColl = accEntrustLoanMapper.selectRepayAccEntrustLoanByParam(billNo, managerId);

                ptdkIColl = Optional.ofNullable(ptdkIColl).orElse(new ArrayList<AccLoan>());
                wtdkIColl = Optional.ofNullable(wtdkIColl).orElse(new ArrayList<AccEntrustLoan>());
                int totalSize = ptdkIColl.size() + wtdkIColl.size();
                //查出来的数据塞入 iqp_pre_payment 表中
                if (totalSize > 0) {
                    if (totalSize > 1) {
                        throw new Exception("数据错误，通过借据编号查询借据信息有" + totalSize + "条，借据编号为：" + billNo);
                    }
                    String cusName = "";//客户名称
                    String contNo = "";//合同号
                    BigDecimal loanBalance = BigDecimal.ZERO;//借据余额
                    String intStartDt = "";//发生日期
                    String lastDueDt = "";//到期日期
                    String cusId = "";//客户号
                    String finaBrId = "";//账务机构
                    managerId = "";//管户人
                    String prdName = "";//产品名称
                    String inputId = "";
                    //通过借据编号查询借据信息的数据应该只有一条的
                    if (ptdkIColl.size() > 0) {
                        AccLoan loan = ptdkIColl.get(0);
                        cusName = loan.getCusName();
                        contNo = loan.getContNo();
                        loanBalance = loan.getLoanBalance();
                        intStartDt = loan.getLoanStartDate();
                        lastDueDt = loan.getLoanEndDate();
                        cusId = loan.getCusId();
                        finaBrId = loan.getFinaBrId();
                        managerId = loan.getManagerId();
                        prdId = loan.getPrdId();
                        inputId = loan.getInputId();
                        prdName = loan.getPrdName();
                    } else {
                        AccEntrustLoan loan = wtdkIColl.get(0);
                        cusName = loan.getCusName();
                        contNo = loan.getContNo();
                        loanBalance = loan.getLoanBalance();
                        intStartDt = loan.getLoanStartDate();
                        lastDueDt = loan.getLoanEndDate();
                        cusId = loan.getCusId();
                        finaBrId = loan.getFinaBrId();
                        managerId = loan.getManagerId();
                        prdId = loan.getPrdId();
                        inputId = loan.getInputId();
                        prdName = loan.getPrdName();
                    }

                    IqpPrePayment iqpPrePayment = new IqpPrePayment();
                    iqpPrePayment.setBillNo(billNo);
                    iqpPrePayment.setCusName(cusName);
                    iqpPrePayment.setContNo(contNo);
                    iqpPrePayment.setLoanBalance(loanBalance);
                    iqpPrePayment.setLoanStartDate(intStartDt);
                    iqpPrePayment.setLoanEndDate(lastDueDt);
                    iqpPrePayment.setCusId(cusId);
                    iqpPrePayment.setInputBrId(finaBrId);
                    iqpPrePayment.setManagerBrId(finaBrId);
                    iqpPrePayment.setInputId(managerId);
                    iqpPrePayment.setManagerId(managerId);
                    iqpPrePayment.setPrdId(prdId);
                    iqpPrePayment.setPrdName(prdName);
                    iqpPrePayment.setRepayMode(yjhkms);
                    //信贷系统还款模式细分码值： 01：提前归还本金及全部利息；02：提前归还本金及本金对应利息；03：灵活还款；04：归还拖欠的本息
                    if ("03".equals(ejhkmx)) {//提前归还本金及全部利息
                        iqpPrePayment.setRepayModeDesc("01");
                    } else if ("04".equals(ejhkmx)) {//提前归还本金及本金对应利息
                        iqpPrePayment.setRepayModeDesc("02");
                    } else if ("05".equals(ejhkmx)) {//归还拖欠本息
                        iqpPrePayment.setRepayModeDesc("04");
                    } else if ("08".equals(ejhkmx)) {//灵活还款
                        iqpPrePayment.setRepayModeDesc("03");
                    } else {//未找到对应关系
                        iqpPrePayment.setRepayModeDesc(ejhkmx);
                    }
                    iqpPrePayment.setRepayAmt(hkje);//还款金额
                    iqpPrePayment.setRepayPriAmt(hkbj);//还本金额
                    iqpPrePayment.setRepayType(dkhsfs);
                    iqpPrePayment.setApproveStatus("000");
                    iqpPrePayment.setLoanAppChnl(repayChannel);
                    iqpPrePayment.setAccStatus(accLoan.getAccStatus());
                    Map seqMap = new HashMap();
                    String zdhkSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, seqMap);
                    iqpPrePayment.setIqpSerno(zdhkSerno);
                    iqpPrePayment.setRepayAcctNo(acctno);
                    iqpPrePayment.setRepayAcctName(acctna);
                    iqpPrePayment.setRepaySubAccno(acctxh);
                    iqpPrePayment.setInputDate(openDay);

                    int count = iqpPrePaymentMapper.insert(iqpPrePayment);
                    if (count != 1) {
                        throw new Exception("数据新增失败，借据编号为：" + billNo);
                    }
                }
            }
            // 给xddh0002DataRespDto赋值
            xddh0002DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
            xddh0002DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);
            // 短信通知
            if (StringUtil.isNotEmpty(managerId)) {
                sendMsg(managerId);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            xddh0002DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0002DataRespDto.setOpMsg(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            xddh0002DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0002DataRespDto.setOpMsg(e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddh0002DataRespDto));
        return xddh0002DataRespDto;
    }


    /**
     * 发送短信
     *
     * @param managerId
     * @throws Exception
     */
    private void sendMsg(String managerId) throws Exception {
        String message = "当前有笔客户线上还款申请，请及时处理";
        logger.info("发送的短信内容：" + message);

        try {
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
            ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
            AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());
            //号码不能为空
            if (StringUtil.isNotEmpty(adminSmUserDto.getUserMobilephone())) {
                SenddxReqDto senddxReqDto = new SenddxReqDto();
                senddxReqDto.setInfopt("dx");
                SenddxReqList senddxReqList = new SenddxReqList();
                senddxReqList.setMobile(adminSmUserDto.getUserMobilephone());
                senddxReqList.setSmstxt(message);
                ArrayList<SenddxReqList> list = new ArrayList<>();
                list.add(senddxReqList);
                senddxReqDto.setSenddxReqList(list);

                logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

                logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
                ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
                logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

                String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                SenddxRespDto senddxRespDto = null;
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                    //  获取相关的值并解析
                    senddxRespDto = senddxResultDto.getData();
                } else {
                    //  抛出错误异常
                    throw new YuspException(senddxCode, senddxMeesage);
                }
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.info("errmsg：" + e.getMessage());
        }
    }
}
