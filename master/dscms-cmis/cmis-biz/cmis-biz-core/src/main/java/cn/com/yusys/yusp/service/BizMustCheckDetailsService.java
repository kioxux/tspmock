/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.BizMustCheckDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.BizMustCheckDetailsMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: BizMustCheckDetailsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-12 15:37:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizMustCheckDetailsService {

    @Autowired
    private BizMustCheckDetailsMapper bizMustCheckDetailsMapper;

    @Autowired
    private RptBasicService rptBasicService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private RptBasicInfoAssoService rptBasicInfoAssoService;
    @Autowired
    private RptLmtRepayAnysService rptLmtRepayAnysService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizMustCheckDetails selectByPrimaryKey(String pkId) {
        return bizMustCheckDetailsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizMustCheckDetails> selectAll(QueryModel model) {
        List<BizMustCheckDetails> records = (List<BizMustCheckDetails>) bizMustCheckDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizMustCheckDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizMustCheckDetails> list = bizMustCheckDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizMustCheckDetails record) {
        return bizMustCheckDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizMustCheckDetails record) {
        return bizMustCheckDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizMustCheckDetails record) {
        return bizMustCheckDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizMustCheckDetails record) {
        return bizMustCheckDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return bizMustCheckDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return bizMustCheckDetailsMapper.deleteByIds(ids);
    }

    /**
     * 根据流水号查询必输页签
     *
     * @param serno
     * @return
     */
    public List<BizMustCheckDetails> selectBySerno(String serno) {
        return bizMustCheckDetailsMapper.selectBySerno(serno);
    }

    public int insertMustCheck(BizMustCheckDetailsDto bizMustCheckDetailsDto) {
        String page = bizMustCheckDetailsDto.getPageList();
        String idList = bizMustCheckDetailsDto.getIdList();
        String[] idArr = idList.split(",");
        String[] pageArr = page.split(",");
        String serno = bizMustCheckDetailsDto.getSerno();
        String bizType = bizMustCheckDetailsDto.getBizType();
        int count = 0;
        User userInfo = SessionUtils.getUserInformation();
        for (int i = 0; i < pageArr.length; i++) {
            BizMustCheckDetails bizMustCheckDetails = new BizMustCheckDetails();
            bizMustCheckDetails.setPkId(StringUtils.getUUID());
            bizMustCheckDetails.setSerno(serno);
            bizMustCheckDetails.setBizType(bizType);
            bizMustCheckDetails.setPageId(idArr[i]);
            bizMustCheckDetails.setPageName(pageArr[i]);
            bizMustCheckDetails.setFinFlag(CmisCommonConstants.YES_NO_0);
            bizMustCheckDetails.setInputId(userInfo.getLoginCode());
            bizMustCheckDetails.setInputBrId(userInfo.getOrg().getCode());
            bizMustCheckDetails.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            bizMustCheckDetails.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
            count += insertSelective(bizMustCheckDetails);
        }
        return count;
    }

    /**
     * 页签保存完成
     */
    public int doMustCheck(BizMustCheckDetails bizMustCheckDetails) {
        String serno = bizMustCheckDetails.getSerno();
        String pageId = bizMustCheckDetails.getPageId();
        String pageName = bizMustCheckDetails.getPageName();
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        model.addCondition("pageId", pageId);
        model.addCondition("pageName", pageName);
        User userInfo = SessionUtils.getUserInformation();
        int count = 0;
        List<BizMustCheckDetails> bizMustCheckDetailsList = selectByModel(model);
        if (CollectionUtils.nonEmpty(bizMustCheckDetailsList)) {
            for (BizMustCheckDetails temp : bizMustCheckDetailsList) {
                temp.setFinFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                temp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                temp.setUpdBrId(userInfo.getOrg().getCode());
                temp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                temp.setUpdId(userInfo.getLoginCode());
                count = updateSelective(temp);
            }
        }
        return count;
    }

    /**
     * @return
     */
    public BizMustCheckDetails selectBySernoAndPageId(String serno, String pageId) {
        if (StringUtils.isBlank(serno) || StringUtils.isBlank(pageId)) {
            return null;
        }
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        queryModel.addCondition("pageId", pageId);
        List<BizMustCheckDetails> bizMustCheckDetails = selectByModel(queryModel);
        if (CollectionUtils.nonEmpty(bizMustCheckDetails)) {
            return bizMustCheckDetails.get(0);
        }
        return null;
    }

    /**
     * 单一授信调查报告完整性检验
     *
     * @param bizMustCheckDetails
     * @return
     */
    public int rptCheck(BizMustCheckDetails bizMustCheckDetails) {
        String serno = bizMustCheckDetails.getSerno();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        String lmtType = lmtApp.getLmtType();
        String rptType = lmtApp.getRptType();
        RptBasic rptBasic = rptBasicService.selectByPrimaryKey(serno);
        boolean flag = true;
        if (Objects.nonNull(rptBasic)) {
            if (CmisBizConstants.LMT_TYPE_01.equals(lmtType)) {
                if (StringUtils.isBlank(rptBasic.getNewFinaReason()) || StringUtils.isBlank(rptBasic.getLmtRationAnaly())) {
                    flag = false;
                }
            } else {
                if (StringUtils.isBlank(rptBasic.getNewFinaReason()) || StringUtils.isBlank(rptBasic.getLmtRationAnaly()) || StringUtils.isBlank(rptBasic.getLastLmtNeedPactCase()) || StringUtils.isBlank(rptBasic.getLastPdoActCaseMemo())) {
                    flag = false;
                }
            }
            if (StringUtils.isBlank(rptBasic.getMainAdvant()) || StringUtils.isBlank(rptBasic.getRiskMainPacmDesc()) || StringUtils.isBlank(rptBasic.getCreditConditions()) || StringUtils.isBlank(rptBasic.getRiskControl())) {
                flag = false;
            }
        } else {
            flag = false;
        }
        RptBasicInfoAsso rptBasicInfoAsso = rptBasicInfoAssoService.selectByPrimaryKey(serno);
        if (!("I02".equals(rptType) || "I03".equals(rptType) || "I05".equals(rptType))) {
            if (Objects.nonNull(rptBasicInfoAsso)) {
                if (CmisBizConstants.STD_RPT_TYPE_C03.equals(rptType)) {
                    if (StringUtils.isBlank(rptBasicInfoAsso.getEnvironStandInd()) || StringUtils.isBlank(rptBasicInfoAsso.getAgrInd())) {
                        flag = false;
                    }
                } else {
                    if (StringUtils.isBlank(rptBasicInfoAsso.getEnvironStandInd()) || StringUtils.isBlank(rptBasicInfoAsso.getCompanyHistory()) || StringUtils.isBlank(rptBasicInfoAsso.getMainBusiness()) || StringUtils.isBlank(rptBasicInfoAsso.getCusEiaSafeCase()) || StringUtils.isBlank(rptBasicInfoAsso.getAgrInd())) {
                        flag = false;
                    }
                }
            } else {
                flag = false;
            }
        }
        if (!CmisBizConstants.STD_RPT_TYPE_C03.equals(rptType)) {
            RptLmtRepayAnys rptLmtRepayAnys = rptLmtRepayAnysService.selectByPrimaryKey(serno);
            if (Objects.nonNull(rptLmtRepayAnys)) {
                if (StringUtils.isBlank(rptLmtRepayAnys.getFirstRepaymentAbilityAnalysis())) {
                    flag = false;
                }
                if (StringUtils.isBlank(rptLmtRepayAnys.getOtherInstructions())) {
                    flag = false;
                }
            } else {
                flag = false;
            }
        }
        if (flag) {
            return doMustCheck(bizMustCheckDetails);
        } else {
            return 0;
        }
    }
}
