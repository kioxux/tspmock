package cn.com.yusys.yusp.web.server.xdcz0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0017.req.Xdcz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.resp.Xdcz0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0017.Xdcz0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:支用(微信小程序)
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0017:支用(微信小程序)")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0017Resource.class);

    @Autowired
    private Xdcz0017Service xdcz0017Service;

    /**
     * 交易码：xdcz0017
     * 交易描述：支用(微信小程序)
     *
     * @param xdcz0017DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("支用(微信小程序)")
    @PostMapping("/xdcz0017")
    protected @ResponseBody
    ResultDto<Xdcz0017DataRespDto>  xdcz0017(@Validated @RequestBody Xdcz0017DataReqDto xdcz0017DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataReqDto));
        Xdcz0017DataRespDto  xdcz0017DataRespDto  = new Xdcz0017DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0017DataRespDto>xdcz0017DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0017DataReqDto获取业务值进行业务逻辑处理
             xdcz0017DataRespDto = xdcz0017Service.xdcz0017(xdcz0017DataReqDto);
            // 封装xdcz0017DataResultDto中正确的返回码和返回信息
            xdcz0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value,e.getMessage());
            xdcz0017DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdcz0017DataRespDto.setOpMsg(e.getMessage());
            // 封装xdcz0017DataResultDto中异常返回码和返回信息
            xdcz0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0017DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value,e.getMessage());
            xdcz0017DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdcz0017DataRespDto.setOpMsg(EpbEnum.EPB099999.value);
            // 封装xdcz0017DataResultDto中异常返回码和返回信息
            xdcz0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0017DataRespDto到xdcz0017DataResultDto中
        xdcz0017DataResultDto.setData(xdcz0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataRespDto));
        return xdcz0017DataResultDto;
    }
}
