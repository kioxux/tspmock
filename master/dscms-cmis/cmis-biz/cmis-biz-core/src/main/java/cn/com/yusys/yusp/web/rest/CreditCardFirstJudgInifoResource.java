/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardFirstJudgInifo;
import cn.com.yusys.yusp.service.CreditCardFirstJudgInifoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFirstJudgInifoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:45:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 * @version 1.0.0
 */
@Api(tags = "初审意见")
@RestController
@RequestMapping("/api/creditcardfirstjudginifo")
public class CreditCardFirstJudgInifoResource {
    @Autowired
    private CreditCardFirstJudgInifoService creditCardFirstJudgInifoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardFirstJudgInifo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardFirstJudgInifo> list = creditCardFirstJudgInifoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardFirstJudgInifo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardFirstJudgInifo>> index(QueryModel queryModel) {
        List<CreditCardFirstJudgInifo> list = creditCardFirstJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardFirstJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardFirstJudgInifo> show(@PathVariable("pkId") String pkId) {
        CreditCardFirstJudgInifo creditCardFirstJudgInifo = creditCardFirstJudgInifoService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardFirstJudgInifo>(creditCardFirstJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("初审意见新增")
    @PostMapping("/save")
    protected ResultDto<CreditCardFirstJudgInifo> create(@RequestBody CreditCardFirstJudgInifo creditCardFirstJudgInifo) throws URISyntaxException {
        creditCardFirstJudgInifoService.insert(creditCardFirstJudgInifo);
        return new ResultDto<CreditCardFirstJudgInifo>(creditCardFirstJudgInifo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("初审意见修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardFirstJudgInifo creditCardFirstJudgInifo) throws URISyntaxException {
        int result = creditCardFirstJudgInifoService.update(creditCardFirstJudgInifo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardFirstJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardFirstJudgInifoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param creditCardFirstJudgInifo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/26 14:52
     * @version 1.0.0
     * @desc 根据主键查询信用卡初审意见
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querybypkid")
    protected ResultDto<CreditCardFirstJudgInifo> queryByPkId(@RequestBody CreditCardFirstJudgInifo creditCardFirstJudgInifo) {
        CreditCardFirstJudgInifo creditCardFirstJudgInifo2 = creditCardFirstJudgInifoService.selectByPrimaryKey(creditCardFirstJudgInifo.getPkId());
        return new ResultDto<CreditCardFirstJudgInifo>(creditCardFirstJudgInifo2);
    }

    /**
     * @param creditCardFirstJudgInifo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/26 14:52
     * @version 1.0.0
     * @desc 根据业务流水号查询信用卡初审意见
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号查询信用卡初审意见")
    @PostMapping("/querybyserno")
    protected ResultDto<CreditCardFirstJudgInifo> queryBySerno(@RequestBody CreditCardFirstJudgInifo creditCardFirstJudgInifo) {
        CreditCardFirstJudgInifo creditCardFirstJudgInifo2 = creditCardFirstJudgInifoService.selectBySerno(creditCardFirstJudgInifo.getSerno());
        return new ResultDto<CreditCardFirstJudgInifo>(creditCardFirstJudgInifo2);
    }
}
