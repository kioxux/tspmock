package cn.com.yusys.yusp.web.server.xdht0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0017.req.Xdht0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0017.resp.Xdht0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0017.Xdht0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:借款、担保合同PDF生成
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0017:借款、担保合同PDF生成")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0017Resource.class);

    @Autowired
    private Xdht0017Service xdht0017Service;

    /**
     * 交易码：xdht0017
     * 交易描述：借款、担保合同PDF生成
     *
     * @param xdht0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款、担保合同PDF生成")
    @PostMapping("/xdht0017")
    protected @ResponseBody
    ResultDto<Xdht0017DataRespDto> xdht0017(@Validated @RequestBody Xdht0017DataReqDto xdht0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataReqDto));
        Xdht0017DataRespDto xdht0017DataRespDto = new Xdht0017DataRespDto();// 响应Dto:借款、担保合同PDF生成
        ResultDto<Xdht0017DataRespDto> xdht0017DataResultDto = new ResultDto<>();
        try {
            // 从xdht0017DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataReqDto));
            xdht0017DataRespDto = xdht0017Service.xdht0017(xdht0017DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataRespDto));
            // 封装xdht0017DataResultDto中正确的返回码和返回信息
            xdht0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, e.getMessage());
            // 封装xdht0017DataResultDto中异常返回码和返回信息
            xdht0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0017DataRespDto到xdht0017DataResultDto中
        xdht0017DataResultDto.setData(xdht0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataRespDto));
        return xdht0017DataResultDto;
    }
}
