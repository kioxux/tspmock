package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAppr;
import cn.com.yusys.yusp.domain.LmtIntbankAppr;
import cn.com.yusys.yusp.domain.LmtSigInvestAppr;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IntbankOrgAdmitAppService;
import cn.com.yusys.yusp.service.IntbankOrgAdmitApprService;
import cn.com.yusys.yusp.service.LmtIntbankApprService;
import cn.com.yusys.yusp.service.LmtSigInvestApprService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * riskItem0066 审查核查报告校验
 */
@Service
public class RiskItem0066Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0066Service.class);

    @Autowired
    private IntbankOrgAdmitApprService intbankOrgAdmitApprService;

    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService;

    @Autowired
    private LmtIntbankApprService lmtIntbankApprService;

    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService;

    /**
     *
     */
    /**
     * @方法名称: riskItem0066
     * @方法描述: 审查核查报告校验
     * @参数与返回说明:
     * @算法描述: 同业机构准入、同业授信、主体授信、产品授信在审批过程中
     * 出具审查报告节点、出具核查报告节点、出具批复报告节点，
     * 如果信息未录入，则进行拦截，不允许提交至下一节点
     * @创建人: lixy
     * @创建时间: 2021年8月3日 14点27分
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0066(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        String node_id = queryModel.getCondition().get("nodeId").toString();
        log.info("风险拦截RiskItem0066 校验开始=====》 serno【{}】 bizType【{}】 node_id【{}】", serno, bizType, node_id);

        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno) || StringUtils.isEmpty(bizType)) {
            log.info("风险拦截RiskItem0066 校验1=====》");
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }

        //同业机构验证主体分析是否填写
        if (CmisBizConstants.STD_ZB_ZJTY_TY001.equals(bizType)) {
            log.info("风险拦截RiskItem0066 校验2=====》");
            //INTBANK_ORG_ADMIT
            //INDGT_RESULT
            IntbankOrgAdmitApp intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(serno);
            if (intbankOrgAdmitApp != null) {
                log.info("风险拦截RiskItem0066 校验3=====》");
                if (StringUtils.isBlank(intbankOrgAdmitApp.getOperFinaSitu())
                        || StringUtils.isBlank(intbankOrgAdmitApp.getIndgtResult())) {
                    log.info("风险拦截RiskItem0066 校验4=====》");
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06605);
                    return riskResultDto;
                }
            } else {
                log.info("风险拦截RiskItem0066 校验5=====》");
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc("同业机构准入数据获取失败！");
                return riskResultDto;
            }
        }

        //同业机构准入
        if (CmisBizConstants.STD_ZB_ZJTY_TY001.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY002.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY003.equals(bizType)) {
            log.info("风险拦截RiskItem0066 校验6=====》");
            if ("170_14".equals(node_id) || "170_19".equals(node_id)) {
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("serno", serno);
                queryModel1.addCondition("issueReportType", CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
                queryModel1.setSort(" createTime desc ");
                List<IntbankOrgAdmitAppr> intbankOrgAdmitApprs = intbankOrgAdmitApprService.selectByModel(queryModel1);
                if (CollectionUtils.nonEmpty(intbankOrgAdmitApprs)) {
                    log.info("风险拦截RiskItem0066 校验7=====》");
                    //审查报告
                    if ("170_14".equals(node_id) && StringUtils.isBlank(intbankOrgAdmitApprs.get(0).getInteAnaly())) {
                        log.info("风险拦截RiskItem0066 校验9=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                    if ("170_19".equals(node_id) && StringUtils.isBlank(intbankOrgAdmitApprs.get(0).getInteAnalyZh())) {
                        log.info("风险拦截RiskItem0066 校验10=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                } else {
                    log.info("风险拦截RiskItem0066 校验8=====》");
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("审查报告获取失败！");
                    return riskResultDto;
                }
            }
        }
        //同业授信
        if (CmisBizConstants.STD_ZB_ZJTY_TY004.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY005.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY006.equals(bizType)) {
            log.info("风险拦截RiskItem0066 校验11=====》");

            if ("137_18".equals(node_id) || "137_30".equals(node_id)) {
                log.info("风险拦截RiskItem0066 校验12=====》");
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("serno", serno);
                queryModel1.addCondition("issueReportType", CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
                queryModel1.setSort(" createTime desc ");
                List<LmtIntbankAppr> lmtIntbankApprs = lmtIntbankApprService.selectByModel(queryModel1);
                if (CollectionUtils.nonEmpty(lmtIntbankApprs)) {
                    log.info("风险拦截RiskItem0066 校验13=====》");
                    //137_18	金融市场总部风险合规部信评岗
                    if ("137_18".equals(node_id) && StringUtils.isBlank(lmtIntbankApprs.get(0).getInteAnaly())) {
                        log.info("风险拦截RiskItem0066 校验14=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                    //137_30	信贷管理部风险派驻岗
                    if ("137_30".equals(node_id) && StringUtils.isBlank(lmtIntbankApprs.get(0).getInteAnalyZh())) {
                        log.info("风险拦截RiskItem0066 校验15=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                } else {
                    log.info("风险拦截RiskItem0066 校验16=====》");
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("审查报告获取失败！");
                    return riskResultDto;
                }
            }
        }

        //主体授信、产品授信
        if (CmisBizConstants.STD_ZB_ZJTY_TY007.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY008.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY009.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY010.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY011.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY012.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY013.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY014.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY015.equals(bizType)
                || CmisBizConstants.STD_ZB_ZJTY_TY016.equals(bizType)) {
            LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectLastBySerno(serno);
            log.info("风险拦截RiskItem0066 校验17=====》");

            //164_36	信贷管理部风险派驻岗   禅道BUG8933 去除债券池（信贷管理部风险派驻岗出具综合分析）填写校验 -- 2021年9月11日 by lixy
            //161_30	信贷管理部风险派驻岗
            boolean isXdglbFxpzg = ("164_36".equals(node_id) || "161_30".equals(node_id) || "137_30".equals(node_id))
                    && !CmisBizConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtSigInvestAppr.getLmtBizType());

            //164_32	金融市场总部风险合规部信评岗
            if (("164_32".equals(node_id) || "137_18".equals(node_id)) || isXdglbFxpzg) {
                log.info("风险拦截RiskItem0066 校验18=====》issueReportType【{}】", CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("serno", serno);
                queryModel1.addCondition("issueReportType", CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
                queryModel1.setSort(" createTime desc ");
                List<LmtSigInvestAppr> lmtSigInvestApprs = lmtSigInvestApprService.selectByModel(queryModel1);
                if (CollectionUtils.nonEmpty(lmtSigInvestApprs)) {
                    log.info("风险拦截RiskItem0066 校验19=====》");
                    if (("164_32".equals(node_id) || "137_18".equals(node_id)) && StringUtils.isBlank(lmtSigInvestApprs.get(0).getInteAnaly())) {
                        log.info("风险拦截RiskItem0066 校验20=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                    //164_36	信贷管理部风险派驻岗   禅道BUG8933 去除债券池（信贷管理部风险派驻岗出具综合分析）填写校验 -- 2021年9月11日 by lixy
                    //161_30	信贷管理部风险派驻岗
                    if (isXdglbFxpzg && StringUtils.isBlank(lmtSigInvestApprs.get(0).getInteAnalyZh())) {
                        log.info("风险拦截RiskItem0066 校验24=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06601);
                        return riskResultDto;
                    }
                } else {
                    log.info("风险拦截RiskItem0066 校验21=====》");
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("审查报告获取失败！");
                    return riskResultDto;
                }
            }
            //核查报告  lmtSigInvestApprCheckReport.vue
            if ("164_13".equals(node_id)) {
                log.info("风险拦截RiskItem0066 校验26=====》IndgtReportMode【{}】 IndgtReportPath【{}】",
                        lmtSigInvestAppr.getIndgtReportMode(), lmtSigInvestAppr.getIndgtReportPath());
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("serno", serno);
                queryModel1.addCondition("issueReportType", CmisBizConstants.STD_ISSUE_REPORT_TYPE_02);
                queryModel1.setSort(" createTime desc ");
                List<LmtSigInvestAppr> lmtSigInvestApprs = lmtSigInvestApprService.selectByModel(queryModel1);
                if (CollectionUtils.nonEmpty(lmtSigInvestApprs)) {
                    log.info("风险拦截RiskItem0066 校验27=====》IndgtReportMode【{}】 IndgtReportPath【{}】",
                            lmtSigInvestApprs.get(0).getIndgtReportMode(), lmtSigInvestApprs.get(0).getIndgtReportPath());
                    if (StringUtils.isBlank(lmtSigInvestApprs.get(0).getIndgtReportMode())) {
                        log.info("风险拦截RiskItem0066 校验28=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06602);
                        return riskResultDto;
                    }
                    //核查报告模式 2 上传WORD版核查报告
                    if (CmisBizConstants.STD_INDGT_REPORT_MODE_2.equals(lmtSigInvestApprs.get(0).getIndgtReportMode())
                            && StringUtils.isBlank(lmtSigInvestApprs.get(0).getIndgtReportPath())) {
                        log.info("风险拦截RiskItem0066 校验29=====》");
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06604);
                        return riskResultDto;
                    }
                } else {
                    log.info("风险拦截RiskItem0066 校验30=====》");
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("核查报告获取失败！");
                    return riskResultDto;
                }
            }
            //批复报告
            if (lmtSigInvestAppr != null && CmisBizConstants.STD_ISSUE_REPORT_TYPE_03.equals(lmtSigInvestAppr.getIssueReportType())) {

            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
