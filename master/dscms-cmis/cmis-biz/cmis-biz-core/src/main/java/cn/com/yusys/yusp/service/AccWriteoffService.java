/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccWriteoff;
import cn.com.yusys.yusp.domain.IqpWriteoffApp;
import cn.com.yusys.yusp.domain.IqpWriteoffDetl;
import cn.com.yusys.yusp.repository.mapper.AccWriteoffMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.BizUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz模块
 * @类名称: AccWriteoffService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:41:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class AccWriteoffService {

    private static final Logger log = LoggerFactory.getLogger(AccWriteoffService.class);

    @Autowired
    private AccWriteoffMapper accWriteoffMapper;

    @Autowired
    private IqpWriteoffDetlService iqpWriteoffDetlService;

    @Autowired
    private IqpWriteoffAppService iqpWriteoffAppService;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AccWriteoff selectByPrimaryKey(String accNo) {
        return accWriteoffMapper.selectByPrimaryKey(accNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AccWriteoff> selectAll(QueryModel model) {
        List<AccWriteoff> records = (List<AccWriteoff>) accWriteoffMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AccWriteoff> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccWriteoff> list = accWriteoffMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AccWriteoff record) {
        return accWriteoffMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AccWriteoff record) {
        return accWriteoffMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AccWriteoff record) {
        return accWriteoffMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AccWriteoff record) {
        return accWriteoffMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String accNo) {
        return accWriteoffMapper.deleteByPrimaryKey(accNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accWriteoffMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertAccWriteOffs
     * @方法描述: 根据申请流水号插入多笔台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor=Exception.class)
    public void insertAccWriteOffs(String iqpSerno){

        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("iqpSerno", iqpSerno);
        paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        IqpWriteoffApp iqpWriteoffApp = iqpWriteoffAppService.selectByPrimaryKey(iqpSerno);
        List<IqpWriteoffDetl> iqpWriteoffDetlList = iqpWriteoffDetlService.selectIqpWriteOffDetailsListByParams(paramMap);
        if(iqpWriteoffDetlList != null && iqpWriteoffDetlList.size() > 0){
            log.info(String.format("保存核销台账信息,查询核销明细数据%d笔", iqpWriteoffDetlList.size()));
            for (IqpWriteoffDetl IqpWriteoffDetl  : iqpWriteoffDetlList) {

                log.info(String.format("保存核销台账信息,借据号%s,数据-转化入参数据", IqpWriteoffDetl.getBillNo()));
//                log.info(String.format("保存恢复计息新增申请,借据号%s,数据映射处理-调用cmis-cfg服务接口获取映射关系-开始，源表:%s,目标表:%s",
//                        IqpWriteoffDetl.getBillNo(), EcbEnum.IQP_WRITEOFF_DETL, EcbEnum.ACC_WRITEOFF));

                //调用工具类
                BizUtils bizUtils = new BizUtils();
                AccWriteoff accWriteoff = new AccWriteoff();
                accWriteoff = (AccWriteoff) bizUtils.getMappingValueBySourceAndDisTable(IqpWriteoffDetl, accWriteoff);

//                log.info(String.format("保存恢复计息新增申请,借据号%s,数据映射处理-调用cmis-cfg服务接口获取映射关系-成功，源表:%s,目标表:%s",
//                        IqpWriteoffDetl.getBillNo(), EcbEnum.IQP_WRITEOFF_DETL, EcbEnum.ACC_WRITEOFF));

                // 通过流水号生成器生成业务流水号
                String accNo = null;//sequenceTemplateClient.getSequenceTemplate(EcbEnum.ACC_WRITEOFF_NO, new HashMap<>());

                accWriteoff.setAccNo(accNo);
                accWriteoff.setManagerId(iqpWriteoffApp.getManagerId());
                accWriteoff.setManagerBrId(iqpWriteoffApp.getManagerBrId());
                accWriteoffMapper.insert(accWriteoff);
                log.info(String.format("保存核销台账信息,台账编号%s,保存成功", accWriteoff.getAccNo()));
            }
        }
    }
}
