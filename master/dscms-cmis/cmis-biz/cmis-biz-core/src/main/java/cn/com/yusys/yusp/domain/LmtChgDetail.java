/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtChgDetail
 * @类描述: lmt_chg_detail数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:18:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_chg_detail")
public class LmtChgDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = true, length = 40)
	private String lmtSerno;

	/** 集团变更申请概述 **/
	@Column(name = "GRP_CHG_APP_MEMO", unique = false, nullable = true, length = 4000)
	private String grpChgAppMemo;

	/** 原授信情况 **/
	@Column(name = "ORIGI_LMT_SURVEY", unique = false, nullable = true, length = 4000)
	private String origiLmtSurvey;

	/** 本次授信变更内容 **/
	@Column(name = "LMT_CHG_CONTENT", unique = false, nullable = true, length = 4000)
	private String lmtChgContent;

	/** 授信变更理由 **/
	@Column(name = "LMT_CHG_RESN", unique = false, nullable = true, length = 4000)
	private String lmtChgResn;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}

	/**
	 * @return lmtSerno
	 */
	public String getLmtSerno() {
		return this.lmtSerno;
	}

	/**
	 * @param origiLmtSurvey
	 */
	public void setOrigiLmtSurvey(String origiLmtSurvey) {
		this.origiLmtSurvey = origiLmtSurvey;
	}

	/**
	 * @return origiLmtSurvey
	 */
	public String getOrigiLmtSurvey() {
		return this.origiLmtSurvey;
	}

	/**
	 * @param lmtChgContent
	 */
	public void setLmtChgContent(String lmtChgContent) {
		this.lmtChgContent = lmtChgContent;
	}

	/**
	 * @return lmtChgContent
	 */
	public String getLmtChgContent() {
		return this.lmtChgContent;
	}

	/**
	 * @param lmtChgResn
	 */
	public void setLmtChgResn(String lmtChgResn) {
		this.lmtChgResn = lmtChgResn;
	}

	/**
	 * @return lmtChgResn
	 */
	public String getLmtChgResn() {
		return this.lmtChgResn;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGrpChgAppMemo() {
		return grpChgAppMemo;
	}

	public void setGrpChgAppMemo(String grpChgAppMemo) {
		this.grpChgAppMemo = grpChgAppMemo;
	}
}