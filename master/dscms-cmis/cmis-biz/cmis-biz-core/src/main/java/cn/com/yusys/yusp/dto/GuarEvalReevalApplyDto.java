package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalReevalApply
 * @类描述: guar_eval_reeval_apply数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-27 09:15:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalReevalApplyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 审批状态 **/
	private String wfApprSts;
	
	/** 重估类型 STD_ZB_REEVAL_TYPE **/
	private String reevalType;
	
	/** 发起岗确认价值 **/
	private java.math.BigDecimal evalAmt;
	
	/** 评估方式 STD_ZB_EVAL_INOUT_TYPE **/
	private String evalInOutType;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 价值确认日期 **/
	private String evalDate;
	
	/** 认定岗确认价值 **/
	private java.math.BigDecimal finalEvalAmt;
	
	/** 确认价值币种 STD_ZB_CUR_TYP **/
	private String evalCurrency;
	
	/** 折算人民币价值 **/
	private java.math.BigDecimal evalConvertedAmt;
	
	/** 是否延期  STD_ZB_YES_NO **/
	private String isStartReeval;
	
	/** 延期重估原因 **/
	private String delayReevalReason;
	
	/** 延期到期日 **/
	private String delayReevalDate;
	
	/** 是否特殊流程审批  STD_ZB_YES_NO **/
	private String special;
	
	/** 审批状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 是否超阈值  STD_ZB_YES_NO **/
	private String beyond;
	
	/** 价值重估岗认定价值 **/
	private java.math.BigDecimal assertValue;
	
	/** 浮动比例/差额比率 **/
	private java.math.BigDecimal rate;
	
	/** 押品重复抵押类型 **/
	private String agaginType;
	
	/** 评估方法 STD_ZB_EVAL_ENNAME **/
	private String evalEnname;
	
	/** 评估类型 STD_ZB_EVAL_TYPE **/
	private String evalType;
	
	/** 操作类型 STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param wfApprSts
	 */
	public void setWfApprSts(String wfApprSts) {
		this.wfApprSts = wfApprSts == null ? null : wfApprSts.trim();
	}
	
    /**
     * @return WfApprSts
     */	
	public String getWfApprSts() {
		return this.wfApprSts;
	}
	
	/**
	 * @param reevalType
	 */
	public void setReevalType(String reevalType) {
		this.reevalType = reevalType == null ? null : reevalType.trim();
	}
	
    /**
     * @return ReevalType
     */	
	public String getReevalType() {
		return this.reevalType;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param evalInOutType
	 */
	public void setEvalInOutType(String evalInOutType) {
		this.evalInOutType = evalInOutType == null ? null : evalInOutType.trim();
	}
	
    /**
     * @return EvalInOutType
     */	
	public String getEvalInOutType() {
		return this.evalInOutType;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate == null ? null : evalDate.trim();
	}
	
    /**
     * @return EvalDate
     */	
	public String getEvalDate() {
		return this.evalDate;
	}
	
	/**
	 * @param finalEvalAmt
	 */
	public void setFinalEvalAmt(java.math.BigDecimal finalEvalAmt) {
		this.finalEvalAmt = finalEvalAmt;
	}
	
    /**
     * @return FinalEvalAmt
     */	
	public java.math.BigDecimal getFinalEvalAmt() {
		return this.finalEvalAmt;
	}
	
	/**
	 * @param evalCurrency
	 */
	public void setEvalCurrency(String evalCurrency) {
		this.evalCurrency = evalCurrency == null ? null : evalCurrency.trim();
	}
	
    /**
     * @return EvalCurrency
     */	
	public String getEvalCurrency() {
		return this.evalCurrency;
	}
	
	/**
	 * @param evalConvertedAmt
	 */
	public void setEvalConvertedAmt(java.math.BigDecimal evalConvertedAmt) {
		this.evalConvertedAmt = evalConvertedAmt;
	}
	
    /**
     * @return EvalConvertedAmt
     */	
	public java.math.BigDecimal getEvalConvertedAmt() {
		return this.evalConvertedAmt;
	}
	
	/**
	 * @param isStartReeval
	 */
	public void setIsStartReeval(String isStartReeval) {
		this.isStartReeval = isStartReeval == null ? null : isStartReeval.trim();
	}
	
    /**
     * @return IsStartReeval
     */	
	public String getIsStartReeval() {
		return this.isStartReeval;
	}
	
	/**
	 * @param delayReevalReason
	 */
	public void setDelayReevalReason(String delayReevalReason) {
		this.delayReevalReason = delayReevalReason == null ? null : delayReevalReason.trim();
	}
	
    /**
     * @return DelayReevalReason
     */	
	public String getDelayReevalReason() {
		return this.delayReevalReason;
	}
	
	/**
	 * @param delayReevalDate
	 */
	public void setDelayReevalDate(String delayReevalDate) {
		this.delayReevalDate = delayReevalDate == null ? null : delayReevalDate.trim();
	}
	
    /**
     * @return DelayReevalDate
     */	
	public String getDelayReevalDate() {
		return this.delayReevalDate;
	}
	
	/**
	 * @param special
	 */
	public void setSpecial(String special) {
		this.special = special == null ? null : special.trim();
	}
	
    /**
     * @return Special
     */	
	public String getSpecial() {
		return this.special;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param beyond
	 */
	public void setBeyond(String beyond) {
		this.beyond = beyond == null ? null : beyond.trim();
	}
	
    /**
     * @return Beyond
     */	
	public String getBeyond() {
		return this.beyond;
	}
	
	/**
	 * @param assertValue
	 */
	public void setAssertValue(java.math.BigDecimal assertValue) {
		this.assertValue = assertValue;
	}
	
    /**
     * @return AssertValue
     */	
	public java.math.BigDecimal getAssertValue() {
		return this.assertValue;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return Rate
     */	
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param agaginType
	 */
	public void setAgaginType(String agaginType) {
		this.agaginType = agaginType == null ? null : agaginType.trim();
	}
	
    /**
     * @return AgaginType
     */	
	public String getAgaginType() {
		return this.agaginType;
	}
	
	/**
	 * @param evalEnname
	 */
	public void setEvalEnname(String evalEnname) {
		this.evalEnname = evalEnname == null ? null : evalEnname.trim();
	}
	
    /**
     * @return EvalEnname
     */	
	public String getEvalEnname() {
		return this.evalEnname;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType == null ? null : evalType.trim();
	}
	
    /**
     * @return EvalType
     */	
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}