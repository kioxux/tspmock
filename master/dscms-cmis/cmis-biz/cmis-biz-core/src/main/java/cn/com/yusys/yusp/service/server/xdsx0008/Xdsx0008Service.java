package cn.com.yusys.yusp.service.server.xdsx0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0008.req.Xdsx0008DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtLadEvalMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 接口处理类:单一客户人工限额同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Service
public class Xdsx0008Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0008Service.class);

    @Autowired
    private LmtLadEvalMapper lmtLadEvalMapper;

    /**
     * 单一客户人工限额同步
     * @param xdsx0008DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public void updateByCusId(Xdsx0008DataReqDto xdsx0008DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataReqDto));
        if (Objects.nonNull(xdsx0008DataReqDto.getSingleCusQuota())) {
            xdsx0008DataReqDto.setSingleCusQuota(new BigDecimal(xdsx0008DataReqDto.getSingleCusQuota().toPlainString()));
            lmtLadEvalMapper.updateByCusId(xdsx0008DataReqDto);
        }
    }
}
