package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CfgBankInfoDto;
import cn.com.yusys.yusp.dto.CfgCreditLevelDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.req.CmisCus0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.resp.CmisCus0023RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AsplAssetsListMapper;
import cn.com.yusys.yusp.repository.mapper.BailAccInfoMapper;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageHelper;
import io.netty.util.internal.StringUtil;
import org.hibernate.validator.internal.engine.constraintvalidation.PredefinedScopeConstraintValidatorManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAssetsListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:45:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplAssetsListService {

    private static final Logger logger = LoggerFactory.getLogger(AsplAssetsListService.class);

    @Autowired
    private AsplAssetsListMapper asplAssetsListMapper;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AsplAorgListService asplAorgListService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AsplAssetsList selectByPrimaryKey(String pkId) {
        return asplAssetsListMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AsplAssetsList> selectAll(QueryModel model) {
        List<AsplAssetsList> records = (List<AsplAssetsList>) asplAssetsListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AsplAssetsList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplAssetsList> list = asplAssetsListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AsplAssetsList record) {
        return asplAssetsListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AsplAssetsList record) {
        return asplAssetsListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AsplAssetsList record) {
        return asplAssetsListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AsplAssetsList record) {
        return asplAssetsListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplAssetsListMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplAssetsListMapper.deleteByIds(ids);
    }

    /**
     * @param ctrAsplDetails
     * @方法名称: getAssetPoolBailAmt
     * @方法描述: 资产池保证金核心获取
     * @参数与返回说明:
     * @算法描述: 实时计算:
     *
     */
    @Transactional
    public BigDecimal getAssetPoolBailAmt(CtrAsplDetails ctrAsplDetails) {
        /**
         *  资产池保证金账户余额
         *  1、根据资产池协议台账的申请流水号 去保证金账户表 获取保证金账户(资产池只有一个保证金账号)
         *  2、发核心 dp2099（查询保证金账户余额）
         */
        String contNo = ctrAsplDetails.getContNo();
        String serno = ctrAsplDetails.getSerno();
        List<BailAccInfo> bailAccInfoList = bailAccInfoService.selectBySerno(serno);
        if (CollectionUtils.isEmpty(bailAccInfoList)) {
            return null;
        }
        BailAccInfo bailAccInfo = bailAccInfoList.get(0);
        BigDecimal assetPoolBailAmt = BigDecimal.ZERO;
        // 发接口 dp2099
        Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
        // 保证金账号
        dp2099ReqDto.setKehuzhao(bailAccInfo.getBailAccNo());
        // 子账户序号
        dp2099ReqDto.setZhhaoxuh(bailAccInfo.getBailAccNoSub());
        ResultDto<Dp2099RespDto> Dp2099ResultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);
        Dp2099RespDto dp2099RespDto = new Dp2099RespDto();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,Dp2099ResultDto.getCode())) {
            dp2099RespDto = Dp2099ResultDto.getData();
            // 资产池保证金账户余额(累加所有保证金账户的账户与余额)
            if(CollectionUtils.nonEmpty(dp2099RespDto.getList())){
                assetPoolBailAmt = dp2099RespDto.getList()
                        .stream()
                        .map(e -> e.getZhanghye())
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                logger.info("资产池："+contNo+"保证金账户余额总额："+ Objects.toString(assetPoolBailAmt));
            }
        }
        return assetPoolBailAmt;
    }
    /**
     * @param ctrAsplDetails
     * @方法名称: getAssetPoolFinAmt
     * @方法描述: 资产池融资额度计算
     * @参数与返回说明:
     * @算法描述: 实时计算:
     * 资产池融资额度 = 低风险池融资额度 + 一般风险池融资额度
     * 低风险池融资额度 = ∑池内质押资产价值*抵（质）押率 + 资产池保证金账户余额（其中银票需占额成功才纳入低风险池融资额度计算范围）
     * （目前没有商票）
     * （其中商票需占额成功才纳入一般风险池融资额度计算范围）
     * 一般风险池融资额度 = ∑池内可质押资产价值*抵（质）押率
     * ********抵（质）押率(来自 信用等级配置表(与资产抵押率设置公用))
     * MIN（资产池合作协议签订金额 ， 低风险池融资额度 ）
     */
    @Transactional
    public BigDecimal getAssetPoolFinAmt(CtrAsplDetails ctrAsplDetails,BigDecimal assetPoolBailAmt) {
        String contNo = ctrAsplDetails.getContNo();
        String cusId = ctrAsplDetails.getCusId();
        logger.info("资产池融资额度计算开始,资产池协议编号："+contNo+",客户编号："+cusId);
        // 资产池融资额度
        BigDecimal assetPoolFinAmt = BigDecimal.ZERO;
        // 低风险池融资额度
        BigDecimal lowAssetPoolFinAmt = BigDecimal.ZERO;
        // 一般风险池融资额度
        BigDecimal comAssetPoolFinAmt = BigDecimal.ZERO;
        // 资产池保证金账户余额
        // BigDecimal bailBalance = BigDecimal.ZERO;
        try {
            // 获取该资产池下所有的资产清单(满足条件：1、是入池 2、是入池质押)
            QueryModel model = new QueryModel();
            model.addCondition("contNo", ctrAsplDetails.getContNo());
            model.addCondition("isPool", CommonConstance.STD_ZB_YES_NO_1);//入池
            model.addCondition("isPledge", CommonConstance.STD_ZB_YES_NO_1);//入池质押
            model.addCondition("oprType", CommonConstance.OPR_TYPE_ADD);//新增操作类型
            List<AsplAssetsList> asplAssetsListList = selectAll(model);
            // 质押物抵质押率 01
            Map<String, BigDecimal> pldimnRateMap = cfgCreditLevelResult("01");
            // 银承分类 02
            Map<String, BigDecimal> cfgCreditLevelMap = cfgCreditLevelResult("02");
            /**
             * 根据资产类型获取对应的抵质押率进行计算
             * 资产类型
             * 01银行承兑汇票（电子）
             * 02本行存单（电子）
             * 03已承兑出口信用证向下单据
             * 04已承兑国内信用证向下单据
             * 目前没有商票(05)
             */
            // ∑池内质押资产价值*抵（质）押率(低风险池融资额度)

            BigDecimal lowAssetPoolFinAmt1 = BigDecimal.ZERO;
            if (CollectionUtils.nonEmpty(asplAssetsListList)) {
                lowAssetPoolFinAmt1 = asplAssetsListList.stream()
                        .map(e -> {
                            // 该笔资产价值
                            BigDecimal sumPldimnAmt = BigDecimal.ZERO;
                            try{
                                // 资产类型质押率
                                BigDecimal assetPldimnRate = pldimnRateMap.get(e.getAssetType());
                                // 判断是否银票
                                if (Objects.equals("01",e.getAssetType())){
                                    // 每一张银票需要单独计算 1、查寻总行分类 2、根据总行信用等级换算质押率
                                    String creditLevel = getPldimnRate(e);
                                    // 承兑行质押率
                                    BigDecimal orgPldimnRate = cfgCreditLevelMap.get(creditLevel);
                                    // 该笔资产质押价值
                                    sumPldimnAmt = Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO).multiply(assetPldimnRate).multiply(orgPldimnRate);

                                    logger.info("资产池："+contNo
                                            +",资产类型："+Objects.toString(e.getAssetType())
                                            +",资产类型质押率:"+Objects.toString(assetPldimnRate)
                                            +",承兑行质押率:"+Objects.toString(orgPldimnRate)
                                            +",资产质押价值:"+Objects.toString(sumPldimnAmt));
                                }else{
                                    // 该笔资产质押价值
                                    sumPldimnAmt = Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO).multiply(assetPldimnRate);
                                    logger.info("资产池："+contNo
                                            +",资产类型："+Objects.toString(e.getAssetType())
                                            +",资产类型质押率:"+Objects.toString(assetPldimnRate)
                                            +",资产质押价值:"+Objects.toString(sumPldimnAmt));
                                }
                            }catch (BizException bizException){
                                throw BizException.error(null,"9999",bizException.getMessage());
                            }catch (Exception exception){
                                throw BizException.error(null,"9999","资产池融资额度计算失败[逻辑错误]，请联系管理员！！！ ");
                            }
                            return sumPldimnAmt;
                        }).reduce(BigDecimal.ZERO, BigDecimal::add);
            }
            logger.info("资产池："+contNo+" ∑池内质押资产价值 * 抵（质）押率: "+Objects.toString(lowAssetPoolFinAmt1));

            // 池内质押资产价值*抵（质）押率 + 资产池保证金账户余额
            lowAssetPoolFinAmt = lowAssetPoolFinAmt1.add(assetPoolBailAmt);
            // 资产池融资额度 = 低风险池融资额度 + 一般风险池融资额度
            assetPoolFinAmt = lowAssetPoolFinAmt.add(comAssetPoolFinAmt);
            logger.info("资产池："+contNo+"资产池融资额度计算值："+ Objects.toString(assetPoolFinAmt));
            // MIN（资产池合作协议签订金额 ， 低风险池融资额度 ）
            return assetPoolFinAmt.min(ctrAsplDetails.getContAmt());
        } catch (BizException e) {
            logger.info(e.getMessage());
            throw BizException.error(null,"9999","资产池融资额度计算失败:"+e.getMessage());
        }
    }
    /**
     * @param ctrAsplDetails
     * @方法名称: getAssetPoolAvaAmt
     * @方法描述: 资产池可用融资额度
     * @参数与返回说明:
     * @算法描述: 实时计算:
     * 可用池融资额度自动计算：
     * ►资产池融资额度为可循环额度，在额度有效期内：
     * 资产池可用融资额度 = 低风险池可用融资额度 + 一般风险池可用融资额度 –最低保证金留存金额
     * 低风险池可用融资额度
     * = 低风险池融资额度 – 低风险已用池融资额度
     * = 低风险池融资额度 - （∑低风险池下借据余额）
     * 一般风险池可用融资额度
     * = 一般风险池融资额度 – 一般风险已用池融资额度
     * = 一般风险资产池融资额度 - （∑一般风险池下借据余额）
     * 池下借据余额 = 委托贷款台账的贷款余额 + 银承台账票据总额
     * 最低保证金留存金额 = 资产池授信金额(资产池融资额度)*保证金比例
     * 贷款状态 1未生效  2正常 3结清 4付垫款 5作废
     */
    public BigDecimal getAssetPoolAvaAmt(CtrAsplDetails ctrAsplDetails,BigDecimal assetPoolFinAmt,BigDecimal assetPoolBailAmt) {
        logger.info("资产池可用融资额度,资产池协议编号："+ctrAsplDetails.getContNo());
        BigDecimal assetPoolAvaAmt = BigDecimal.ZERO;
        // 资产池协议编号
        String contNo = ctrAsplDetails.getContNo();
        // 客户编号
        String cusId = ctrAsplDetails.getCusId();
        try{
            // 获取该用户下所有的资产池协议编号
            List<String> contNoList = ctrAsplDetailsService.selectContNoListByCusId(cusId);

            // ∑低风险池下借据余额 = 普通贷款台账的贷款余额（超短贷） + 银承台账票据总额(查询分项 未结清)
            // ∑低风险池下借据总额
            BigDecimal lowRiskAmt = BigDecimal.ZERO;

            for(String contNoNow : contNoList){
                /**
                 * 累加所有的贷款台账的贷款余额 （只计算未结清的贷款）
                 * * 贷款台账的 贷款余额 1，2，3，4，5，6，8
                 */
                BigDecimal lowRiskLoanAmtn = Optional.ofNullable(accLoanService.selectUnClearByContNo(contNoNow)).orElse(BigDecimal.ZERO);
                logger.info("资产池协议："+contNoNow+", 普通贷款剩余总额："+Objects.toString(lowRiskLoanAmtn));
                /**
                 * 银承台账票据总额（未结清） 0，1，4，5
                 * 银承台账票据明细（根据 银承台账借据编号 查询分项）
                 */
                BigDecimal lowRiskBillAmtn = Optional.ofNullable(accAccpService.selectSumAmtByContNo(contNoNow)).orElse(BigDecimal.ZERO);
                logger.info("资产池协议："+contNoNow+", 银承台账剩余银票总额："+Objects.toString(lowRiskBillAmtn));
                lowRiskAmt = lowRiskAmt.add(lowRiskLoanAmtn).add(lowRiskBillAmtn);
            }
            // 资产池融资额度
            logger.info("资产池："+contNo+", 资产池融资额度："+Objects.toString(assetPoolFinAmt)+", 借据总额:"+Objects.toString(lowRiskAmt) );
            // 最低保证金留存金额 = 保证金账户余额*保证金留存比例
            //todo 业务突然说不用了
//            BigDecimal BailMinAmt = assetPoolBailAmt.multiply(Optional.ofNullable(bailAccInfo.getBailRate()).orElse(BigDecimal.ONE));
//            logger.info("资产池："+contNo+", 最低保证金留存金额："+Objects.toString(BailMinAmt));
            // 可用池融资额度
            BigDecimal resultDecimal = assetPoolFinAmt.subtract(lowRiskAmt);
            logger.info("资产池："+contNo+", 可用池融资额度："+Objects.toString(resultDecimal));
            return resultDecimal;
        }catch (BizException e) {
            throw e;
        }
    }


    /**
     * @方法名称: isPoolVaild
     * @方法描述: 出池校验
     * @参数与返回说明: contNo(待出池的资产池协议编号)
     * reqAssetNoList（带出池的资产编号集合）
     * map<String,String> opflag(成功失败标志) msg(描述信息)
     * @算法描述: 实时计算:
     * 校验可用池融资额度是否大于0
     * 资产池授信金额 = 资产池融资额度 = 低风险池融资额度 + 一般风险池融资额度
     * 低风险池融资额度 = ∑池内质押资产价值*抵（质）押率 + 资产池保证金账户余额（其中银票需占额成功才纳入低风险池融资额度计算范围）
     * （其中商票需占额成功才纳入一般风险池融资额度计算范围）（目前没有商票）
     * 一般风险池融资额度 = ∑池内可质押资产价值*抵（质）押率 （其中商票需占额成功才纳入一般风险池融资额度计算范围）
     * 抵（质）押率(来自 信用等级配置表(与资产抵押率设置公用))
     * 资产池融资额度为可循环额度，在额度有效期内：
     * 资产池可用融资额度 = 低风险池可用融资额度 + 一般风险池可用融资额度 –最低保证金留存金额
     * 低风险池可用融资额度 = 低风险池融资额度 – 低风险已用池融资额度
     * = 低风险池融资额度 - （∑低风险池下借据余额）
     * 一般风险池可用融资额度 = 一般风险池融资额度 – 一般风险已用池融资额度
     * = 一般风险资产池融资额度 - （∑一般风险池下借据余额）
     * 低风险池可用融资额度 + 一般风险池可用融资额度(池融资额度) = 低风险池融资额度 - （∑低风险池下借据余额） +一般风险资产池融资额度 - （∑一般风险池下借据余额）
     * 池下借据余额 = 委托贷款台账的贷款余额（未结清） + 银承台账票据总额（分项总和）（未结清）（这里为2正常）
     * 最低保证金留存金额 = 资产池授信金额(资产池融资额度)*保证金比例
     * 贷款状态  1未生效  2正常 3结清  4付垫款  5作废
     */
    public OpRespDto isOupPoolVaild(String contNo, List<String> reqAssetNoList) {
        OpRespDto opRespDto = new OpRespDto();
        String opFlag = CmisBizConstants.SUCCESS;
        String opMsg = "出池校验成功";
        try {
            // 1、获取该协议编号下所有的池内资产清单（已经入池的）
            QueryModel model = new QueryModel();
            model.addCondition("contNo", contNo);
            model.addCondition("isPool", CommonConstance.STD_ZB_YES_NO_1);//入池
            List<AsplAssetsList> asplAssetsListList = this.selectAll(model);
            if (CollectionUtils.isEmpty(asplAssetsListList)) {
                opFlag = CmisBizConstants.FAIL;// 成功失败标志
                opMsg = "未查询到对应的资产信息，资产池协议编号：" + contNo;// 描述信息
                throw BizException.error(null,"9999", opMsg);
            }
            // 2、判断需要出池的资产清单是否都在池内（入池状态）
            List<String> assetsNoList = asplAssetsListList.parallelStream()
                    .map(e -> e.getAssetNo())
                    .collect(Collectors.toList());
            // 3、计算在不存在池内的待出池集合
            List<String> noReqAssetList = reqAssetNoList.stream().filter(e -> {
                return !assetsNoList.contains(e);
            }).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(noReqAssetList)) {
                opFlag = CmisBizConstants.FAIL;// 成功失败标志
                opMsg = "池内不存在该资产（没有入池），资产编号：" + noReqAssetList.toString();// 描述信息
                throw BizException.error(null,"9999", opMsg);
            }
            // 计算出池后，剩余没有出池的入池抵押资产清单(去除将要出池的资产清单)
            assetsNoList.removeAll(reqAssetNoList);
            List<AsplAssetsList> resultAsplList = asplAssetsListList.stream().filter(e -> {
                return CommonConstance.STD_ZB_YES_NO_1.equals(e.getIsPledge()) && assetsNoList.contains(e.getAssetNo());//1、入池质押 && 存在剩余的资产清单里
            }).collect(Collectors.toList());
            /**
             * 3、校验可用池融资额度是否大于0
             */
            // 质押物抵质押率 01
            Map<String, BigDecimal> pldimnRateMap = cfgCreditLevelResult("01");
            // 银承分类 02
            Map<String, BigDecimal> cfgCreditLevelMap = cfgCreditLevelResult("02");
            /**
             * 根据资产类型获取对应的抵质押率进行计算
             * 资产类型
             * 01银行承兑汇票（电子）
             * 02本行存单（电子）
             * 03已承兑出口信用证向下单据
             * 04已承兑国内信用证向下单据
             * 目前没有商票(05)
             */
            // ∑池内质押资产价值*抵（质）押率(低风险池融资额度)
            BigDecimal lowAssetPoolFinAmt1 = BigDecimal.ZERO;
            if (CollectionUtils.nonEmpty(resultAsplList)) {
                lowAssetPoolFinAmt1 = resultAsplList.stream()
                        .map(e -> {
                            // 资产类型质押率
                            BigDecimal assetPldimnRate = pldimnRateMap.get(e.getAssetType());
                            // 该笔资产价值
                            BigDecimal sumPldimnAmt = BigDecimal.ZERO;
                            // 判断是否银票
                            if (Objects.equals("01",e.getAssetType())){
                                // 每一张银票需要单独计算 1、查寻总行分类 2、根据总行信用等级换算质押率
                                String creditLevel = getPldimnRate(e);
                                // 承兑行质押率
                                BigDecimal orgPldimnRate = cfgCreditLevelMap.get(creditLevel);
                                // 该笔资产质押价值
                                sumPldimnAmt = Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO).multiply(assetPldimnRate).multiply(orgPldimnRate);

                                logger.info("资产池："+contNo
                                        +",资产类型："+Objects.toString(e.getAssetType())
                                        +",资产编号："+e.getAssetNo()
                                        +",资产类型质押率:"+Objects.toString(assetPldimnRate)
                                        +",承兑行质押率:"+Objects.toString(orgPldimnRate)
                                        +",资产质押价值:"+Objects.toString(sumPldimnAmt));
                                // 资产价值*资产类型质押率*承兑行质押率
                                return sumPldimnAmt;
                            }else{
                                // 该笔资产质押价值
                                sumPldimnAmt = Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO).multiply(assetPldimnRate);
                                logger.info("资产池："+contNo
                                        +",资产类型："+Objects.toString(e.getAssetType())
                                        +",资产编号："+e.getAssetNo()
                                        +",资产类型质押率:"+Objects.toString(assetPldimnRate)
                                        +",资产质押价值:"+Objects.toString(sumPldimnAmt));
                                // 资产价值*资产类型质押率
                                return sumPldimnAmt;
                            }
                        }).reduce(BigDecimal.ZERO, BigDecimal::add);
            }
            logger.info("资产池："+contNo+" 出池后，池内质押资产价值 : "+Objects.toString(lowAssetPoolFinAmt1));

            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
            /**
             *  资产池保证金账户余额
             *  1、根据资产池协议台账的申请流水号 去保证金账户表 获取保证金账户
             *  2、发核心 dp2099（查询保证金账户余额）
             */
            String serno = ctrAsplDetails.getSerno();
            // 资产池保证金账户余额
            BigDecimal lowAssetPoolFinAmt2 = asplAssetsListService.getAssetPoolBailAmt(ctrAsplDetails);

            // 低风险池融资额度 = 池内质押资产价值*抵（质）押率 + 资产池保证金账户余额
            BigDecimal lowAssetPoolFinAmt = lowAssetPoolFinAmt1.add(lowAssetPoolFinAmt2);
            // 一般风险池融资额度(商票目前没有先留着，其实在上面 低风险池融资额度中已经加上了商票的融资额度了)
            BigDecimal comAssetPoolFinAmt = BigDecimal.ZERO;
            // 资产池融资额度 = 低风险池融资额度 + 一般风险池融资额度
            BigDecimal assetPoolFinAmt = lowAssetPoolFinAmt.add(comAssetPoolFinAmt);
            // 资产池融资额度 = MIN（资产池合作协议签订金额 ， 资产池融资额度 ）这是原本计算 用户可用融资额度的规则，这里不适用
            // 出池后的资产池融资额度
            // assetPoolFinAmt = assetPoolFinAmt.min(ctrAsplDetails.getContAmt());

            BigDecimal resultAmt = getAssetPoolAvaAmt(ctrAsplDetails,assetPoolFinAmt,lowAssetPoolFinAmt2);
            // 出池后资产池可用融资额度>0 (如果不大于说明出池额度校验无法通过)
            if (resultAmt.compareTo(BigDecimal.ZERO) < 0) {
                opFlag = CmisBizConstants.FAIL;// 成功失败标志
                opMsg = "出池后该资产池下可用的额度不足，出池后额度：" + Objects.toString(resultAmt);// 描述信息
                throw BizException.error(null,"9999", opMsg);
            }
        }catch(Exception e){
            throw BizException.error(null,"9999", e.getMessage());
        }finally {
            opRespDto.setOpFlag(opFlag);// 成功失败标志
            opRespDto.setOpMsg(opMsg);//描述信息
            return opRespDto;
        }
    }

    public int updateAsplAssets(AsplAssetsList asplAssetsList) {
        return asplAssetsListMapper.updateAsplAssets(asplAssetsList);
    }

    /**
     * @方法名称: asplAssetsList
     * @方法描述: 查询列表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplAssetsList> asplAssetsList(QueryModel model) {
        return asplAssetsListMapper.selectByModel(model);
    }

    /**
     * @方法名称: updateAsplAssetsDataAfterOutPoolFlow
     * @方法描述: 更新 客户资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void updateAsplAssetsDataAfterOutPoolFlow(String cusId) {
        HashMap map = new HashMap();
        map.put("cusId", cusId);
        map.put("isPool", CmisBizConstants.STD_ZB_YES_NO_N);
        map.put("isPledge", CmisBizConstants.STD_ZB_YES_NO_N);
        map.put("outpTime", DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        map.put("oprType", CmisBizConstants.OPR_TYPE_01);
        asplAssetsListMapper.updateAsplAssetsDataAfterOutPoolFlow();
    }

    /**
     * @方法名称: insertAsplAssetsList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertAsplAssetsList(List<AsplAssetsList> asplAssetsListList) {
        return asplAssetsListMapper.insertAsplAssetsList(asplAssetsListList);
    }

    /**
     * @方法名称: isAssetNoList
     * @方法描述: 查询已存在的资产清单编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<String> isAssetNoList(List<java.lang.String> assetNoList) {
        return asplAssetsListMapper.isAssetNoList(assetNoList);
    }

    /**
     * @方法名称: selectAsplAssestsByAssestsNos
     * @方法描述: 根据资产编号集合查询资产集合
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AsplAssetsList> selectAsplAssestsByAssestsNos(List<String> assetNoList) {
        return asplAssetsListMapper.selectAsplAssestsByAssestsNos(assetNoList);
    }

    /**
     * @方法名称: selectByModelDate
     * @方法描述: 根据时间范围灵活查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AsplAssetsList> selectByModelDate(QueryModel queryModel) {
        return asplAssetsListMapper.selectByModelDate(queryModel);
    }
    /**
     * @方法名称: selectByModelDate
     * @方法描述: 根据银票资产获取承兑分类
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String getPldimnRate(AsplAssetsList asplAssetsList){
        // 获取承兑行行号
        String aorgNo = asplAssetsList.getAorgNo();
        // 承兑行总行号
        String superBankNo = getHeadBankInfo(aorgNo);
        // 获取分类 aspl_aorg_list
        AsplAorgList asplAorgList =  asplAorgListService.selectByHeadBankNo(superBankNo);
        if(Objects.isNull(asplAorgList)){
            throw BizException.error(null, "9999", "未查询到该承兑行【"+aorgNo+"】,总行的配置信息，请联系信贷管理员");
        }
        String creditLevel = asplAorgList.getCreditLevel();
        return creditLevel;
    }

    /**
     * @方法名称: getHeadBankInfo
     * @方法描述: 根据 承兑行行号 查询总行行号
     * @参数与返回说明: aorgNo
     * @算法描述: 无
     */
    public String getHeadBankInfo(String aorgNo){
        // 承兑行总行号
        String superBankNo = StringUtils.EMPTY;
        CmisCfg0003ReqDto cmisCfg0003ReqDto = new CmisCfg0003ReqDto();
        cmisCfg0003ReqDto.setBankNo(aorgNo);
        CmisCfg0003RespDto cfgData = dscmsCfgQtClientService.cmisCfg0003(cmisCfg0003ReqDto).getData();
        if(SuccessEnum.SUCCESS.key.equals(cfgData.getErrorCode())){
            superBankNo = cfgData.getSuperBankNo();
        }else{
            throw BizException.error(null, "9999", "未查询到该承兑行信息，请联系信贷管理员！");
        }
        return superBankNo;
    }

    /**
     * @方法名称: selectByModelDate
     * @方法描述: 获取分类质押率 02
     * @参数与返回说明:02 02信用等级配置 01质押物类型
     * @算法描述: 无
     */
    public Map<String, BigDecimal> cfgCreditLevelResult(String cfgType){
        CfgCreditLevelDto cfgCreditLevelDto = new CfgCreditLevelDto();
        cfgCreditLevelDto.setCfgType(cfgType);// 查询信用等级配置类型
        ResultDto<List<CfgCreditLevelDto>> cfgCreditLevelResult = iCmisCfgClientService.queryCfgCreditLevelByType(cfgCreditLevelDto);
        List<CfgCreditLevelDto> cfgCreditLevelList = new ArrayList<CfgCreditLevelDto>();
        if ("0".equals(cfgCreditLevelResult.getCode())) {
            cfgCreditLevelList = cfgCreditLevelResult.getData();
            if (CollectionUtils.isEmpty(cfgCreditLevelList)){
                throw BizException.error("9999","质押物类型质押率列表获取失败");
            }
            cfgCreditLevelList = JSON.parseObject(JSON.toJSONString(cfgCreditLevelList), new TypeReference<List<CfgCreditLevelDto>>() {
            });
            logger.info("类型："+cfgType+",质押率配置："+ JSON.toJSONString(cfgCreditLevelList));
            // 创建Map映射
            Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
            if(Objects.equals("01",cfgType)){
                // 01质押物类型
                cfgCreditLevelList.forEach(e -> {
                    map.put(e.getAssetType() , e.getPldimnRate());
                });
            }else{
                //02信用等级配置
                cfgCreditLevelList.forEach(e -> {
                    map.put(e.getCreditLevel() , e.getPldimnRate());
                });
            }
            logger.info("类型："+cfgType+",质押率配置："+ JSON.toJSONString(map));
            return map;
        }else{
            throw BizException.error("9999","质押率配置CFG出错");
        }
    }
    /**
     * @方法名称: selectByAssetNo
     * @方法描述: 根据资产编号查询资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */
    public AsplAssetsList selectByAssetNo(String contNo,String assetNo) {
        return asplAssetsListMapper.selectByAssetNo(contNo , assetNo);
    }

    /**
     * 根据资产编号出池
     * @param assetNo
     * @return
     */
    public int updateOutPollAsplAssets(String assetNo,String contNo) {
        String outpTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
        return asplAssetsListMapper.updateOutPollAsplAssets(assetNo,outpTime,contNo);
    }

    /**
     * 根据资产编号入池
     * @param assetNo
     * @return
     */
    public int updateInpPollAsplAssets(String assetNo,String contNo) {
        String inpTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
        return asplAssetsListMapper.updateInpPollAsplAssets(assetNo,inpTime,contNo);
    }
    public List<AsplAssetsList> inPoolAssetsListAll(QueryModel queryModel) {
        return asplAssetsListMapper.inPoolAssetsListAll(queryModel);
    }

    /**
     * 可出池资产清单
     * @param queryModel
     * @return
     */
    public ResultDto<List<AsplAssetsList>> outPoolAssetsList(QueryModel queryModel) {
        if(Objects.isNull(queryModel.getCondition().get("contNo"))){
            return new ResultDto<List<AsplAssetsList>>(null).code("9999").message("资产池协议参数【contNo】为空");
        }
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        if(StringUtils.isEmpty(openday)){
            return new ResultDto<List<AsplAssetsList>>(null).code("9999").message("获取营业时间【openday】失败");
        }
        // 可出池资产清单 (出池日<=到期日前一天) => 营业日期+1 <= 到期日
        queryModel.addCondition("assetStartDate", DateUtils.addDay(DateUtils.parseDate(openday,DateFormatEnum.DEFAULT.getValue()),DateFormatEnum.DEFAULT.getValue(),1));//资产到期日止
        queryModel.setSort("INP_TIME DESC");// 后进先出法
        queryModel.addCondition("isPool",CommonConstance.STD_ZB_YES_NO_1);
        queryModel.addCondition("isPledge",CommonConstance.STD_ZB_YES_NO_1);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<AsplAssetsList> asplAssetsLists = selectByModelDate(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAssetsList>>(asplAssetsLists);
    }

    public int deleteByContNo(String contNo) {
        return asplAssetsListMapper.deleteByContNo(contNo);
    }

    public ResultDto<Map<String, Object>> doSynchronize(AsplAssetsList asplAssetsList) {
        Map map = new HashMap<String,String>();
        String code = "0";
        String msg = "补发成功";
        // 根据统一押品编号查询押品
        logger.info("核心出库同步:"+asplAssetsList.getAssetNo()+"开始！！");
        try{
            GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectInfoByGuarNo(asplAssetsList.getGuarNo());
            // 通过资产编号（票号，权证编号）去查询权证信息
            QueryModel model = new QueryModel();
            model.addCondition("warrantNo",asplAssetsList.getAssetNo());//权证编号 票号 资产编号
            model.addCondition("guarNo",CmisBizConstants.STD_ZB_CERTI_STATE_04);// 入库状态
            model.setSort("input_date desc");//
            List<GuarWarrantInfo> records = guarWarrantInfoService.selectAll(model);
            if(CollectionUtils.isEmpty(records)){
                throw BizException.error(null,"9999","【权证信息获取失败】");
            }
            GuarWarrantInfo guarWarrantInfo = records.get(0);
            // 核心担保编号
            String coreGuarantyNo = guarWarrantInfo.getCoreGuarantyNo();
            // 核心记表外账出库(该接口目前支持单押品出池)
            Co3202ReqDto co3202ReqDto = getCo3202ReqDto(guarBaseInfo,asplAssetsList,guarWarrantInfo);
            // 发送押品核心出库接口
            ResultDto<Co3202RespDto> co3202ResultDto = dscms2CoreCoClientService.co3202(co3202ReqDto);
            // 判断核心 出库是否成功，更新担保权证出库申请
            if("0".equals(co3202ResultDto.getCode())){
                // 更新权证 担保合同 押品关系表 （资产池的关系表用完就逻辑删除）
                //根据核心担保编号获取权证信息更新权证信息出库
                logger.info(" 更新权证出库");
                guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_10);//权证状态
                guarWarrantInfoService.updateSelective(guarWarrantInfo);
            }else{
                throw BizException.error(null,"9999","【核心Co3202】:"+co3202ResultDto.getMessage());
            }
            // grtGuarContRelService.deleteByAssetNo(asplAssetsList.getAssetNo());
            // 营业时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 调押品出库 (发送certis接口)
            logger.info("调押品出库 (发送certis接口)");
            CertisReqDto certisReqDto = new CertisReqDto();
            List<CertisListInfo> certisListInfoList = new ArrayList<CertisListInfo>();
            CertisListInfo certisListInfo = new CertisListInfo();
            certisListInfo.setSernoy(coreGuarantyNo);//核心担保编号
            certisListInfo.setQlpzhm(coreGuarantyNo);//权利凭证号
            // 权证类型 (一期只有银票)
            if ("01".equals(asplAssetsList.getAssetType())) {
                // 银行承兑汇票
                certisListInfo.setQzlxyp("53");
            }else if ("02".equals(asplAssetsList.getAssetType())) {
                // 本行存单(电子)
                certisListInfo.setQzlxyp("40");
            } else if ("04".equals(asplAssetsList.getAssetType())) {
                // 国内信用证
                certisListInfo.setQzlxyp("06");
            }
            certisListInfo.setQzztyp("10008");//权证状态(出库)
            certisListInfo.setQzrkrq(StringUtil.EMPTY_STRING);//权证入库日期
            certisListInfo.setQzckrq(openDay);//权证正常出库日期
            certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称
            certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期
            certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期
            certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期
            certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因
            certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
            certisListInfoList.add(certisListInfo);
            certisReqDto.setList(certisListInfoList);
            ResultDto<CertisRespDto> certisResultDto = dscms2YpxtClientService.certis(certisReqDto);
            logger.info("【certis】接口："+ Objects.toString(certisResultDto));
            if (!"0".equals(certisResultDto.getCode())) {
                //默认成功
                throw BizException.error(null,"9999","【certis】:"+certisResultDto.getMessage());
            }
            // 资产池协议信息
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByContNo(asplAssetsList.getContNo());
            // 承兑行白名单恢复
            this.sendCmisLmt0025(asplAssetsList,ctrAsplDetails);
        }catch(BizException e){
            code = e.getErrorCode();
            msg = e.getMessage();
        }finally {
            map.put("code",code);
            map.put("msg",msg);
            return new ResultDto<Map<String, Object>>(map).code(code).message(msg);
        }
    }
    /**
     *  生成Co3202ReqDto对象(资产池 解质押-chuku )
     * */
    public Co3202ReqDto getCo3202ReqDto(GuarBaseInfo guarBaseInfo, AsplAssetsList asplAssetsList,GuarWarrantInfo guarWarrantInfo){
        // 是否票据池业务
        Co3202ReqDto co3202ReqDto = new Co3202ReqDto();
        co3202ReqDto.setDaikczbz("3");//业务操作标志（资产池） 1录入 2复核 3直通
        co3202ReqDto.setDzywbhao(guarWarrantInfo.getCoreGuarantyNo());// 核心担保编号
        co3202ReqDto.setDzywminc(guarBaseInfo.getPldimnMemo());//抵质押物名称
        co3202ReqDto.setDizyfshi("2");//抵质押方式
        co3202ReqDto.setChrkleix("2");//出入库类型
        co3202ReqDto.setSyqrkehh(guarBaseInfo.getGuarCusId());//所有权人客户号
        co3202ReqDto.setSyqrkehm(guarBaseInfo.getGuarCusName());//所有权人客户名
        co3202ReqDto.setRuzjigou(guarWarrantInfo.getFinaBrId());//入账机构
        co3202ReqDto.setHuobdhao(toConverCurrency(guarBaseInfo.getCurType()));//货币代号
        co3202ReqDto.setMinyjiaz(guarBaseInfo.getConfirmAmt());//名义价值
        co3202ReqDto.setShijjiaz(guarBaseInfo.getConfirmAmt());//实际价值
        co3202ReqDto.setDizybilv(guarBaseInfo.getMortagageRate()); //抵质押比率
        co3202ReqDto.setKeyongje(null);//可用金额
        co3202ReqDto.setShengxrq(guarBaseInfo.getInputDate().replace("-", "").substring(0,8));//生效日期
        co3202ReqDto.setDaoqriqi(asplAssetsList.getAssetEndDate().replace("-", "").substring(0,8));// 到期日期
        co3202ReqDto.setDzywztai("2");//抵质押物状态
        co3202ReqDto.setZhaiyoms(""); //摘要
        return co3202ReqDto;
    }
    /**
     * 承兑行白名单额度恢复
     * @param
     * @return
     */
    public void sendCmisLmt0025(AsplAssetsList asplAssetsList, CtrAsplDetails ctrAsplDetails){
        List<CmisLmt0025OccRelListReqDto> occRelList = new ArrayList<CmisLmt0025OccRelListReqDto>();
        CmisLmt0025OccRelListReqDto dealBiz = new CmisLmt0025OccRelListReqDto();
        dealBiz.setDealBizNo(asplAssetsList.getAssetNo());//交易业务编号
        occRelList.add(dealBiz);
        CmisLmt0025ReqDto cmisLmt0025ReqDto = new CmisLmt0025ReqDto();
        cmisLmt0025ReqDto.setOccRelList(occRelList);
        // 系统编号
        cmisLmt0025ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);
        // 金融机构代码
        cmisLmt0025ReqDto.setInstuCde("001");
        // 合同编号
        // 合同编号
        cmisLmt0025ReqDto.setInputId(ctrAsplDetails.getManagerId());
        cmisLmt0025ReqDto.setInputBrId(ctrAsplDetails.getManagerBrId());
        cmisLmt0025ReqDto.setInputDate(DateUtils.getCurrDateStr());

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025ReqDto));
        ResultDto<CmisLmt0025RespDto> cmisLmt0025RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0025(cmisLmt0025ReqDto)).orElse(new ResultDto<>());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025RespDtoResultDto));
        if (Objects.equals(cmisLmt0025RespDtoResultDto.getCode(), "0")) {
            if(!Objects.equals(cmisLmt0025RespDtoResultDto.getData().getErrorCode(), "0000")){
                throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getData().getErrorMsg());
            }
        }else{
            throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getMessage());
        }
    }
    /*
     * 新核心改造 币种映射 信贷--->核心
     * @param xdbz 信贷币种码值
     * @return hxbz 核心币种码值
     */
    public static String toConverCurrency(String xdbz){
        String hxbz = "";
        if("CNY".equals(xdbz)){
            hxbz = "01";
        }else if("MOP".equals(xdbz)){//澳门币
            hxbz = "81";
        }else if("CAD".equals(xdbz)){//加元
            hxbz = "28";
        }else if("CHF".equals(xdbz)){//瑞士法郎
            hxbz = "15";
        }else if("JPY".equals(xdbz)){//日元
            hxbz = "27";
        }else if("EUR".equals(xdbz)){//欧元
            hxbz = "38";
        }else if("GBP".equals(xdbz)){//英镑
            hxbz = "12";
        }else if("HKD".equals(xdbz)){//港币
            hxbz = "13";
        }else if("AUD".equals(xdbz)){//澳元
            hxbz = "29";
        }else if("USD".equals(xdbz)){//美元
            hxbz = "14";
        }else if("SGD".equals(xdbz)){//新加坡元
            hxbz = "18";
        }else if("SEK".equals(xdbz)){//瑞典克郎
            hxbz = "21";
        }else if("DKK".equals(xdbz)){//丹麦克朗
            hxbz = "22";
        }else if("NOK".equals(xdbz)){//挪威克朗
            hxbz = "23";
        }else{
            hxbz = xdbz;//未匹配到的币种发信贷的币种过去（DEM 德国马克;MSD 克鲁赛罗;NLG 荷兰盾;BEF 比利时法郎;ITL 意大利里拉;FRF 法国法郎;ATS 奥地利先令;FIM 芬兰马克）
        }
        return hxbz;
    }
}
