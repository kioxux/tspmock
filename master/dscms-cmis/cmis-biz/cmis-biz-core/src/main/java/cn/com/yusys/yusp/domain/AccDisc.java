/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccDisc
 * @类描述: acc_disc数据实体类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:42:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_disc")
public class AccDisc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 出账流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 票据种类 **/
	@Column(name = "DRFT_TYPE", unique = false, nullable = true, length = 5)
	private String drftType;

	/** 是否电子票据 **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;

	/** 持票人结算账号 **/
	@Column(name = "DRFTHLD_SETTL_ACCNO", unique = false, nullable = true, length = 40)
	private String drfthldSettlAccno;

	/** 持票人结算账户名称 **/
	@Column(name = "DRFTHLD_SETTL_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String drfthldSettlAcctName;

	/** 汇票号码 **/
	@Column(name = "PORDER_NO", unique = false, nullable = true, length = 40)
	private String porderNo;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 票面金额 **/
	@Column(name = "DRFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftAmt;

	/** 实付贴现金额 **/
	@Column(name = "RPAY_DSCNT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rpayDscntAmt;

	/** 票据签发日期 **/
	@Column(name = "DRAFT_START_DATE", unique = false, nullable = true, length = 20)
	private String draftStartDate;

	/** 票据到期日期 **/
	@Column(name = "DRAFT_END_DATE", unique = false, nullable = true, length = 20)
	private String draftEndDate;

	/** 贴现年利率 **/
	@Column(name = "DSCNT_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dscntRateYear;

	/** 贴现利息 **/
	@Column(name = "DSCNT_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dscntInt;

	/** 贴现类型 **/
	@Column(name = "DSCNT_TYPE", unique = false, nullable = true, length = 5)
	private String dscntType;

	/** 是否可转让 **/
	@Column(name = "IS_ALLOW_TAKEOVER", unique = false, nullable = true, length = 5)
	private String isAllowTakeover;

	/** 票据签发地 **/
	@Column(name = "DRAFT_SIGN_ADDR", unique = false, nullable = true, length = 500)
	private String draftSignAddr;

	/** 贴现日期 **/
	@Column(name = "DSCNT_DATE", unique = false, nullable = true, length = 20)
	private String dscntDate;

	/** 调整天数 **/
	@Column(name = "ADJ_DAY", unique = false, nullable = true, length = 10)
	private String adjDay;

	/** 贴现天数 **/
	@Column(name = "DSCNT_DAY", unique = false, nullable = true, length = 10)
	private String dscntDay;

	/** 回购日期 **/
	@Column(name = "REBUY_DATE", unique = false, nullable = true, length = 20)
	private String rebuyDate;

	/** 托收日期 **/
	@Column(name = "COLLECT_DATE", unique = false, nullable = true, length = 20)
	private String collectDate;

	/** 出票人名称 **/
	@Column(name = "DRWR_NAME", unique = false, nullable = true, length = 80)
	private String drwrName;

	/** 出票人账户 **/
	@Column(name = "DRWR_ACCNO", unique = false, nullable = true, length = 40)
	private String drwrAccno;

	/** 出票人开户行行号 **/
	@Column(name = "DRWR_ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String drwrAcctsvcrNo;

	/** 出票人开户行行名 **/
	@Column(name = "DRWR_ACCTSVCRNM", unique = false, nullable = true, length = 80)
	private String drwrAcctsvcrnm;

	/** 收款人名称 **/
	@Column(name = "PYEE_NAME", unique = false, nullable = true, length = 80)
	private String pyeeName;

	/** 收款人账号 **/
	@Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 40)
	private String pyeeAccno;

	/** 收款人开户行行号 **/
	@Column(name = "PYEE_ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String pyeeAcctsvcrNo;

	/** 收款人开户行名称 **/
	@Column(name = "PYEE_ACCTSVCR_NAME", unique = false, nullable = true, length = 80)
	private String pyeeAcctsvcrName;

	/** 承兑行类型 **/
	@Column(name = "AORG_TYPE", unique = false, nullable = true, length = 5)
	private String aorgType;

	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;

	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;

	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;

	/** 是否省心E帖 **/
	@Column(name = "IS_SXET", unique = false, nullable = true, length = 5)
	private String isSxet;

	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;

	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;

	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;

	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;

	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;

	/** 分类日期 **/
	@Column(name = "CLASS_DATE", unique = false, nullable = true, length = 20)
	private String classDate;

	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;

	/** 操作类型 STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	/**
	 * @return billNo
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param drftType
	 */
	public void setDrftType(String drftType) {
		this.drftType = drftType;
	}

	/**
	 * @return drftType
	 */
	public String getDrftType() {
		return this.drftType;
	}

	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}

	/**
	 * @return isEDrft
	 */
	public String getIsEDrft() {
		return this.isEDrft;
	}

	/**
	 * @param drfthldSettlAccno
	 */
	public void setDrfthldSettlAccno(String drfthldSettlAccno) {
		this.drfthldSettlAccno = drfthldSettlAccno;
	}

	/**
	 * @return drfthldSettlAccno
	 */
	public String getDrfthldSettlAccno() {
		return this.drfthldSettlAccno;
	}

	/**
	 * @param drfthldSettlAcctName
	 */
	public void setDrfthldSettlAcctName(String drfthldSettlAcctName) {
		this.drfthldSettlAcctName = drfthldSettlAcctName;
	}

	/**
	 * @return drfthldSettlAcctName
	 */
	public String getDrfthldSettlAcctName() {
		return this.drfthldSettlAcctName;
	}

	/**
	 * @param porderNo
	 */
	public void setPorderNo(String porderNo) {
		this.porderNo = porderNo;
	}

	/**
	 * @return porderNo
	 */
	public String getPorderNo() {
		return this.porderNo;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param drftAmt
	 */
	public void setDrftAmt(java.math.BigDecimal drftAmt) {
		this.drftAmt = drftAmt;
	}

	/**
	 * @return drftAmt
	 */
	public java.math.BigDecimal getDrftAmt() {
		return this.drftAmt;
	}

	/**
	 * @param rpayDscntAmt
	 */
	public void setRpayDscntAmt(java.math.BigDecimal rpayDscntAmt) {
		this.rpayDscntAmt = rpayDscntAmt;
	}

	/**
	 * @return rpayDscntAmt
	 */
	public java.math.BigDecimal getRpayDscntAmt() {
		return this.rpayDscntAmt;
	}

	/**
	 * @param draftStartDate
	 */
	public void setDraftStartDate(String draftStartDate) {
		this.draftStartDate = draftStartDate;
	}

	/**
	 * @return draftStartDate
	 */
	public String getDraftStartDate() {
		return this.draftStartDate;
	}

	/**
	 * @param draftEndDate
	 */
	public void setDraftEndDate(String draftEndDate) {
		this.draftEndDate = draftEndDate;
	}

	/**
	 * @return draftEndDate
	 */
	public String getDraftEndDate() {
		return this.draftEndDate;
	}

	/**
	 * @param dscntRateYear
	 */
	public void setDscntRateYear(java.math.BigDecimal dscntRateYear) {
		this.dscntRateYear = dscntRateYear;
	}

	/**
	 * @return dscntRateYear
	 */
	public java.math.BigDecimal getDscntRateYear() {
		return this.dscntRateYear;
	}

	/**
	 * @param dscntInt
	 */
	public void setDscntInt(java.math.BigDecimal dscntInt) {
		this.dscntInt = dscntInt;
	}

	/**
	 * @return dscntInt
	 */
	public java.math.BigDecimal getDscntInt() {
		return this.dscntInt;
	}

	/**
	 * @param dscntType
	 */
	public void setDscntType(String dscntType) {
		this.dscntType = dscntType;
	}

	/**
	 * @return dscntType
	 */
	public String getDscntType() {
		return this.dscntType;
	}

	/**
	 * @param isAllowTakeover
	 */
	public void setIsAllowTakeover(String isAllowTakeover) {
		this.isAllowTakeover = isAllowTakeover;
	}

	/**
	 * @return isAllowTakeover
	 */
	public String getIsAllowTakeover() {
		return this.isAllowTakeover;
	}

	/**
	 * @param draftSignAddr
	 */
	public void setDraftSignAddr(String draftSignAddr) {
		this.draftSignAddr = draftSignAddr;
	}

	/**
	 * @return draftSignAddr
	 */
	public String getDraftSignAddr() {
		return this.draftSignAddr;
	}

	/**
	 * @param dscntDate
	 */
	public void setDscntDate(String dscntDate) {
		this.dscntDate = dscntDate;
	}

	/**
	 * @return dscntDate
	 */
	public String getDscntDate() {
		return this.dscntDate;
	}

	/**
	 * @param adjDay
	 */
	public void setAdjDay(String adjDay) {
		this.adjDay = adjDay;
	}

	/**
	 * @return adjDay
	 */
	public String getAdjDay() {
		return this.adjDay;
	}

	/**
	 * @param dscntDay
	 */
	public void setDscntDay(String dscntDay) {
		this.dscntDay = dscntDay;
	}

	/**
	 * @return dscntDay
	 */
	public String getDscntDay() {
		return this.dscntDay;
	}

	/**
	 * @param rebuyDate
	 */
	public void setRebuyDate(String rebuyDate) {
		this.rebuyDate = rebuyDate;
	}

	/**
	 * @return rebuyDate
	 */
	public String getRebuyDate() {
		return this.rebuyDate;
	}

	/**
	 * @param collectDate
	 */
	public void setCollectDate(String collectDate) {
		this.collectDate = collectDate;
	}

	/**
	 * @return collectDate
	 */
	public String getCollectDate() {
		return this.collectDate;
	}

	/**
	 * @param drwrName
	 */
	public void setDrwrName(String drwrName) {
		this.drwrName = drwrName;
	}

	/**
	 * @return drwrName
	 */
	public String getDrwrName() {
		return this.drwrName;
	}

	/**
	 * @param drwrAccno
	 */
	public void setDrwrAccno(String drwrAccno) {
		this.drwrAccno = drwrAccno;
	}

	/**
	 * @return drwrAccno
	 */
	public String getDrwrAccno() {
		return this.drwrAccno;
	}

	/**
	 * @param drwrAcctsvcrNo
	 */
	public void setDrwrAcctsvcrNo(String drwrAcctsvcrNo) {
		this.drwrAcctsvcrNo = drwrAcctsvcrNo;
	}

	/**
	 * @return drwrAcctsvcrNo
	 */
	public String getDrwrAcctsvcrNo() {
		return this.drwrAcctsvcrNo;
	}

	/**
	 * @param drwrAcctsvcrnm
	 */
	public void setDrwrAcctsvcrnm(String drwrAcctsvcrnm) {
		this.drwrAcctsvcrnm = drwrAcctsvcrnm;
	}

	/**
	 * @return drwrAcctsvcrnm
	 */
	public String getDrwrAcctsvcrnm() {
		return this.drwrAcctsvcrnm;
	}

	/**
	 * @param pyeeName
	 */
	public void setPyeeName(String pyeeName) {
		this.pyeeName = pyeeName;
	}

	/**
	 * @return pyeeName
	 */
	public String getPyeeName() {
		return this.pyeeName;
	}

	/**
	 * @param pyeeAccno
	 */
	public void setPyeeAccno(String pyeeAccno) {
		this.pyeeAccno = pyeeAccno;
	}

	/**
	 * @return pyeeAccno
	 */
	public String getPyeeAccno() {
		return this.pyeeAccno;
	}

	/**
	 * @param pyeeAcctsvcrNo
	 */
	public void setPyeeAcctsvcrNo(String pyeeAcctsvcrNo) {
		this.pyeeAcctsvcrNo = pyeeAcctsvcrNo;
	}

	/**
	 * @return pyeeAcctsvcrNo
	 */
	public String getPyeeAcctsvcrNo() {
		return this.pyeeAcctsvcrNo;
	}

	/**
	 * @param pyeeAcctsvcrName
	 */
	public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
		this.pyeeAcctsvcrName = pyeeAcctsvcrName;
	}

	/**
	 * @return pyeeAcctsvcrName
	 */
	public String getPyeeAcctsvcrName() {
		return this.pyeeAcctsvcrName;
	}

	/**
	 * @param aorgType
	 */
	public void setAorgType(String aorgType) {
		this.aorgType = aorgType;
	}

	/**
	 * @return aorgType
	 */
	public String getAorgType() {
		return this.aorgType;
	}

	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}

	/**
	 * @return aorgNo
	 */
	public String getAorgNo() {
		return this.aorgNo;
	}

	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}

	/**
	 * @return aorgName
	 */
	public String getAorgName() {
		return this.aorgName;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	/**
	 * @return finaBrId
	 */
	public String getFinaBrId() {
		return this.finaBrId;
	}

	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}

	/**
	 * @return finaBrIdName
	 */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}

	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	/**
	 * @return fiveClass
	 */
	public String getFiveClass() {
		return this.fiveClass;
	}

	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}

	/**
	 * @return tenClass
	 */
	public String getTenClass() {
		return this.tenClass;
	}

	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}

	/**
	 * @return classDate
	 */
	public String getClassDate() {
		return this.classDate;
	}

	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

	/**
	 * @return accStatus
	 */
	public String getAccStatus() {
		return this.accStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getIsSxet() {
		return isSxet;
	}

	public void setIsSxet(String isSxet) {
		this.isSxet = isSxet;
	}
}