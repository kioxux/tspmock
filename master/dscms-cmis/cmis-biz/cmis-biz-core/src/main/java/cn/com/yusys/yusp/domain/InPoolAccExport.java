/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InPoolAccExport
 * @类描述:
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "池内业务台账", fileType = ExcelCsv.ExportFileType.XLS)
public class InPoolAccExport {

	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 40)
	private String cusId;

	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 40)
	private String cusName;

	/** 资产池协议编号 **/
	@ExcelField(title = "资产池协议编号", viewLength = 40)
	private String contNo;

	/** 资产池额度 **/
	@ExcelField(title = "资产池额度", viewLength = 20)
	private BigDecimal contAmt;

	/** 合同状态 **/
	@ExcelField(title = "合同状态", viewLength = 20)
	private String contStatus;

	/** 借据编号 **/
	@ExcelField(title = "借据编号", viewLength = 20)
	private String billNo;

	/** 借据金额 **/
	@ExcelField(title = "借据金额", viewLength = 20)
	private BigDecimal billAmt;

	/** 借据余额 **/
	@ExcelField(title = "借据余额", viewLength = 20)
	private BigDecimal billBalance;

	/** 借据起始日 **/
	@ExcelField(title = "借据起始日", viewLength = 20)
	private String billStartDate;

	/** 借据到期日 **/
	@ExcelField(title = "借据到期日", viewLength = 20)
	private String billEndDate;

	/** 借据状态 **/
	@ExcelField(title = "借据状态", viewLength = 20)
	private String billStatus;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(BigDecimal billAmt) {
		this.billAmt = billAmt;
	}

	public BigDecimal getBillBalance() {
		return billBalance;
	}

	public void setBillBalance(BigDecimal billBalance) {
		this.billBalance = billBalance;
	}

	public String getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}

	public String getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

}