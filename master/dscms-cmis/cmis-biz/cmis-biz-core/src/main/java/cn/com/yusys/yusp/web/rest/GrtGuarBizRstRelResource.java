/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.GrtGuarBizRstRelDto;
import cn.com.yusys.yusp.service.GrtGuarContService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.service.GrtGuarBizRstRelService;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称:  GrtGuarBizRstRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-20 17:02:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/grtguarbizrstrel")
public class GrtGuarBizRstRelResource {
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;
    @Autowired
    private GrtGuarContService grtGuarContService; // 担保合同

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GrtGuarBizRstRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.selectAll(queryModel);
        return new ResultDto<List<GrtGuarBizRstRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GrtGuarBizRstRel>> index(QueryModel queryModel) {
        List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.selectByModel(queryModel);
        return new ResultDto<List<GrtGuarBizRstRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<GrtGuarBizRstRel> show(@PathVariable("pkId") String pkId) {
        GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelService.selectByPrimaryKey(pkId);
        return new ResultDto<GrtGuarBizRstRel>(grtGuarBizRstRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GrtGuarBizRstRel> create(@RequestBody GrtGuarBizRstRel grtGuarBizRstRel) throws URISyntaxException {
        grtGuarBizRstRel.setPkId(UUID.randomUUID().toString());
        // 获取流水号
        String serno = grtGuarBizRstRel.getSerno();
        // 获取合同号
        String contNo = grtGuarBizRstRel.getContNo();
        // 获取担保合同号
        String guarContNo = grtGuarBizRstRel.getGuarContNo();
        // 根据流水号合同号查询
        QueryModel model = new QueryModel();
        model.addCondition("contNo",contNo);
        model.addCondition("oprType","01");
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectAll(model);
        // 是否原合同项下处理
        String isOld = "0";
        // 是否已经追加
        String isAddOld = "0";
        // 是否原合同项下处理
        for (int i = 0; i < grtGuarBizRstRelList.size(); i++) {
            GrtGuarBizRstRel rel = grtGuarBizRstRelList.get(i);
            if(null == rel.getGuarContNo()){
                throw BizException.error(null, "999999", "\"担保合同号为空,请确认！");
            }
            if((rel.getGuarContNo()).equals(guarContNo)){
                isOld = "1";
                break;
            }
        }
        // 是否已经追加处理
        for (int i = 0; i < grtGuarBizRstRelList.size(); i++) {
            GrtGuarBizRstRel rel = grtGuarBizRstRelList.get(i);
            if(null == rel.getGuarContNo()){
                throw BizException.error(null, "999999", "\"担保合同号为空,请确认！");
            }
            // 关联流水号为空跳过
            if(null == rel.getSerno()){
                continue;
            }
            if((rel.getSerno()).equals(serno) && rel.getGuarContNo().equals(guarContNo)){
                isAddOld = "1";
                break;
            }
        }
        if(grtGuarBizRstRelList.size() >0){ // 存在
            if("1".equals(isAddOld)){
                throw BizException.error(null, "999999", "\"已被合同["+guarContNo+"]引用，请确认！");
            } else if("1".equals(isOld)){
                throw BizException.error(null, "999999", "\"原合同项下，请勿引入！");
            } else {
                // 是否追加担保处理
                // 获取担保合同信息
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
                if(Objects.nonNull(grtGuarCont)){
//                    grtGuarCont.setIsSuperaddGuar("1");
//                    grtGuarContService.updateSelective(grtGuarCont);
                    grtGuarBizRstRel.setIsAddGuar("1");
                    grtGuarBizRstRel.setGuarPkId(grtGuarCont.getGuarPkId());
                    grtGuarBizRstRel.setGuarContNo(grtGuarCont.getGuarContNo());
                    grtGuarBizRstRelService.insert(grtGuarBizRstRel);
                } else {
                    throw BizException.error(null, "999999", "\"担保信息不存在！");
                }
            }
        } else {
            // 获取担保合同信息
            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
            if(Objects.nonNull(grtGuarCont)){
                grtGuarBizRstRel.setGuarPkId(grtGuarCont.getGuarPkId());
                grtGuarBizRstRel.setGuarContNo(grtGuarCont.getGuarContNo());
                grtGuarBizRstRelService.insert(grtGuarBizRstRel);
            } else {
                throw BizException.error(null, "999999", "\"担保信息不存在！");
            }
        }
        return new ResultDto<GrtGuarBizRstRel>(grtGuarBizRstRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GrtGuarBizRstRel grtGuarBizRstRel) throws URISyntaxException {
        int result = grtGuarBizRstRelService.update(grtGuarBizRstRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = grtGuarBizRstRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = grtGuarBizRstRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: queryGrtGuarContByParams
     * @函数描述: 根据申请流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据合同号获取担保合同信息")
    @PostMapping("/querygrtguarcontbyparams")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContByParams(@RequestBody Map map) {
        List<GrtGuarCont> list = grtGuarBizRstRelService.queryGrtGuarContByParams((String)map.get("contNo"));
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称: queryGrtGuarContByContNo
     * @函数描述: 根据申请流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据合同号获取担保合同信息")
    @PostMapping("/querygrtguarcontbycontno")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContByContNo(@RequestBody Map map) {
        List<GrtGuarCont> list = grtGuarBizRstRelService.queryGrtGuarContByContNo((String)map.get("serno"));
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * 通过担保合同编号获取担保合同已用金额信息
     * @param params
     * @return
     */
    @PostMapping("/getAmtInfoByGuarContNo")
    protected  ResultDto<Map> getAmtInfoByGuarContNo(@RequestBody Map params){
        Map rtnData = grtGuarBizRstRelService.getAmtInfoByGuarContNo(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * @函数名称:getGrtContAndBizRel
     * @函数描述:根据担保合同号编号获取完整合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getgrtcontandbizrel")
    protected ResultDto<List<GrtGuarBizRstRelDto>> getGrtContAndBizRel(@RequestBody QueryModel queryModel) {
        List<GrtGuarBizRstRelDto> list = grtGuarBizRstRelService.getGrtContAndBizRel(queryModel);
        return new ResultDto<List<GrtGuarBizRstRelDto>>(list);
    }

    /**
     * @函数名称:getGrtBaseByGarContNo
     * @函数描述:根据担保合同号编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getgrtbasebygarcontno")
    protected ResultDto<List<GuarBaseInfo>> getGrtBaseByGarContNo(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = grtGuarBizRstRelService.getGrtBaseByGarContNo(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:selectContNoByGuarContNo
     * @函数描述:根据担保合同号编号获取最早的合同编号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectcontnobyguarcontno")
    protected ResultDto<String> selectContNoByGuarContNo(@RequestBody QueryModel queryModel) {
        String contNo = grtGuarBizRstRelService.selectContNoByGuarContNo(queryModel);
        return new ResultDto<>(contNo);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<GrtGuarBizRstRel>> query(@RequestBody QueryModel queryModel) {
        List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @函数名称: queryGrtGuarContByParams
     * @函数描述: 根据申请流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-09-13 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号获取担保合同信息")
    @PostMapping("/querygrtguarcontbyserno")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContBySerno(@RequestBody Map map) {
        List<GrtGuarCont> list = grtGuarBizRstRelService.queryGrtGuarContBySerno((String)map.get("serno"));
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称:selectSernoByModel
     * @函数描述:根据担保合同号编号和合同编号查询最新的流水号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectsernobymodel")
    protected ResultDto<String> selectSernoByModel(@RequestBody QueryModel queryModel) {
        String serno = grtGuarBizRstRelService.selectSernoByModel(queryModel);
        return new ResultDto<>(serno);
    }

    /**
     * @函数名称:getGrtContAndBizRel
     * @函数描述:根据担保合同号编号获取完整合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getgrtcontbizrelinfo")
    protected ResultDto<List<GrtGuarBizRstRelDto>> getGrtContBizRelInfo(@RequestBody QueryModel queryModel) {
        ResultDto<List<GrtGuarBizRstRelDto>> resultDto = null;
        List<GrtGuarBizRstRelDto> list = grtGuarBizRstRelService.getGrtContBizRelInfo(queryModel);
        resultDto = new ResultDto<List<GrtGuarBizRstRelDto>>();
        resultDto.setData(list);
        resultDto.setTotal(list.size());
        return resultDto;
    }
}
