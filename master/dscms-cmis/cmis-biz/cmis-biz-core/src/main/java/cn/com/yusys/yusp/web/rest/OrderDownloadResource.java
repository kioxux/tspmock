/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 *//*

package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.OrderDownloadDto;
import cn.com.yusys.yusp.service.OrderDownloadService;

*/
/**
 * 预约下载 cmis-biz 入口
 *//*

@RestController
@RequestMapping("/api/orderdownlad")
public class OrderDownloadResource {
    @Autowired
    private  OrderDownloadService orderDownloadService;
 

    */
/**
     * 预约下载启动
     * @param cfgOrderDownload
     * @return
     * @throws URISyntaxException
     *//*

    @PostMapping("/start")
    protected ResultDto<String> start(@RequestBody OrderDownloadDto cfgOrderDownload) throws  URISyntaxException{
    	String result = orderDownloadService.start(cfgOrderDownload);
        return new ResultDto<String>(result);
    }

    */
/**
     * 预约下载终止
     * @param cfgOrderDownload
     * @return
     * @throws URISyntaxException
     *//*

    @PostMapping("/end")
    protected ResultDto<String> end(@RequestBody OrderDownloadDto cfgOrderDownload) throws  URISyntaxException{
    	String result = orderDownloadService.end(cfgOrderDownload);
        return new ResultDto<String>(result);
    }

    @GetMapping("/download")
    protected void download(@RequestParam("pkId") String filePath,HttpServletResponse response) throws  URISyntaxException{
    	orderDownloadService.download(filePath,response);
    }
    
}
*/
