package cn.com.yusys.yusp.web.server.xdxw0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0004.req.Xdxw0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0004.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0004.resp.Xdxw0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * 接口处理类:小微营业额校验查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0004:小微营业额校验查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0004Resource.class);

    /**
     * 交易码：xdxw0004
     * 交易描述：小微营业额校验查询
     *
     * @param xdxw0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微营业额校验查询")
    @PostMapping("/xdxw0004")
    protected @ResponseBody
    ResultDto<Xdxw0004DataRespDto> xdxw0004(@Validated @RequestBody Xdxw0004DataReqDto xdxw0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004DataReqDto));
        Xdxw0004DataRespDto xdxw0004DataRespDto = new Xdxw0004DataRespDto();// 响应Dto:小微营业额校验查询
        ResultDto<Xdxw0004DataRespDto> xdxw0004DataResultDto = new ResultDto<>();
		Xdxw0004DataRespDto xdxw0004RespDto = new Xdxw0004DataRespDto(); //响应Data:小微营业额校验查询
		try {
            // 从xdxw0004DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
			String surveyNo = xdxw0004DataReqDto.getSurveyNo();//调查流水号
            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0004DataRespDto对象开始
			List list = new List();
			xdxw0004DataRespDto.setList(list);// 主键

            // TODO 封装xdxw0004DataRespDto对象结束
            // 封装xdxw0004DataResultDto中正确的返回码和返回信息
            xdxw0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, e.getMessage());
            // 封装xdxw0004DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdxw0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdxw0004DataRespDto到xdxw0004DataResultDto中
        xdxw0004DataResultDto.setData(xdxw0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004DataResultDto));
        return xdxw0004DataResultDto;
    }
}
