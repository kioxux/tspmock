package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.req.Ln3053ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.req.Lstdkzqg;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp.Ln3053RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.Ln3054ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.resp.Ln3054RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:13
 * @desc 还款计划变更-村镇
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class SGCZ14BizService implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(SGCZ14BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Autowired
    private BGYW09BizService bGYW09BizService;
    @Autowired
    private BGYW10BizService bGYW10BizService;
    @Autowired
    private SGCZ15BizService sGCZ15BizService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 还款计划变更 --寿光
        if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH10.equals(bizType)){
            iqpBillAcctChgBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH12.equals(bizType)){
            // SGH12延期还款（寿光）
            bGYW09BizService.iqpBillAcctChgBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH14.equals(bizType)) { // 主动还款保证金代偿
            sGCZ15BizService.iqpBillAcctChgBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    private void iqpBillAcctChgBizApp(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpRepayWayChg iqpRepayWayChg = iqpRepayWayChgService.selectByPrimaryKey(iqpSerno);
            // 获取合同号
            String contNo = iqpRepayWayChg.getContNo();
            // 获取合同信息
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            // 获取批复号
            String replySerno = "";
            if(null != ctrLoanCont){
                replySerno = ctrLoanCont.getReplyNo();
            }
            if(null != replySerno && !"".equals(replySerno)){
                //  根据批复编号获取批复信息
                HashMap map = new HashMap();
                map.put("replySerno",replySerno);
                map.put("oprType", CommonConstance.OPR_TYPE_ADD);
                map.put("accStatus",CommonConstance.ACC_STATUS_01);
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                // 获取授信流水号
                String LmtSerno ="";
                if(null != lmtReplyAcc ){
                    LmtSerno = lmtReplyAcc.getSerno();
                }
                if(!"".equals(LmtSerno) && null != LmtSerno){
                    // 加载路由条件
                    put2VarParam(instanceInfo, LmtSerno);
                }
            }
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
            if (iqpRepayWayChg == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                iqpRepayWayChg.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                //TODO 修改借据表还款方式
                iqpRepayWayChg.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpRepayWayChg.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpRepayWayChg.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getRouterMapResult(serno);
        params.remove("approveStatus");
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    private void iqpBillAcctChgBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpRepayWayChg iqpRepayWayChg = new IqpRepayWayChg();
            iqpRepayWayChg.setIqpSerno(iqpSerno);
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
            if (iqpRepayWayChg == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                //档案
                String bizType = instanceInfo.getBizType();
                String nodeId = instanceInfo.getNodeId();
                if ((CmisFlowConstants.FLOW_TYPE_TYPE_BG035.equals(bizType) && "243_5".equals(nodeId)) ||
                        (CmisFlowConstants.FLOW_TYPE_TYPE_BG036.equals(bizType))){
                    this.createCentralFileTask(bizType,iqpSerno,instanceInfo);
                }
                iqpRepayWayChg.setContApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                /*
                 *************************************************************************
                 * 审批通过调用 还款计划调整 ln3053  还款方式调整 ln3054
                 * *************************************************************************/
                IqpRepayWayChg domain = iqpRepayWayChgService.selectByPrimaryKey(iqpSerno);
                Ln3054ReqDto ln3054req = new Ln3054ReqDto();
                //贷款借据号
                ln3054req.setDkjiejuh(domain.getBillNo());
                ln3054req.setKehuhaoo(domain.getCusId());
                ln3054req.setKehmingc(domain.getCusName());
                ln3054req.setHuobdhao("01");
                // TODO 还款周期待处理
                ln3054req.setHkzhouqi("1MA21");
                ln3054req.setHuankfsh(iqpRepayWayChgService.transRepayType(domain.getRepayMode()));
                ResultDto<Ln3054RespDto> ln3054ResultDto = dscms2CoreLnClientService.ln3054(ln3054req);
                String ln3054Code = Optional.ofNullable(ln3054ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String ln3054Meesage = Optional.ofNullable(ln3054ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                Ln3054RespDto ln3054RespDto = null;
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3054ResultDto.getCode())) {
                    //  获取相关的值并解析
                    ln3054RespDto = ln3054ResultDto.getData();
                    this.changeAccLoan(iqpSerno);
                } else {
                    //  抛出错误异常
                    throw BizException.error(null, ln3054Code, ln3054Meesage);
                }
                System.out.println(ln3054RespDto);
                //定制还款
                if ("7".equals(ln3054req.getHuankfsh())) {
                    // 还款计划调整	ln3053
                    Ln3053ReqDto ln3053reqDto = new Ln3053ReqDto();
                    ln3053reqDto.setDkjiejuh(domain.getBillNo());
                    ln3053reqDto.setKehuhaoo(domain.getCusId());
                    ln3053reqDto.setKehmingc(domain.getCusName());
                    ln3053reqDto.setQixiriqi(domain.getLoanStartDate().replaceAll("-", ""));
                    QueryModel model = new QueryModel();
                    model.addCondition("serno", iqpSerno);
                    List<RepayCapPlan> list = repayCapPlanService.selectAll(model);
                    List<Lstdkzqg> lstdkzqgs = new ArrayList<>();
                    String start = ln3053reqDto.getQixiriqi();
                    for (int i = 0; i < list.size(); i++) {
                        Lstdkzqg lstdkzqg = new Lstdkzqg();
                        lstdkzqg.setBenqqish(i + 1);
                        lstdkzqg.setQishriqi(start);
                        start = list.get(i).getRepayDate().replaceAll("-", "");
                        lstdkzqg.setZhzhriqi(start);
                        lstdkzqg.setBenjinnn(list.get(i).getRepayAmt());
                        lstdkzqgs.add(lstdkzqg);
                    }
                    ln3053reqDto.setLstdkzqg(lstdkzqgs);
                    ResultDto<Ln3053RespDto> ln3053ResultDto = dscms2CoreLnClientService.ln3053(ln3053reqDto);
                    String ln3053Code = Optional.ofNullable(ln3053ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3053Meesage = Optional.ofNullable(ln3053ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Ln3053RespDto ln3053RespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3053ResultDto.getCode())) {
                        //  获取相关的值并解析
                        ln3053RespDto = ln3053ResultDto.getData();
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null, ln3053Code, ln3053Meesage);
                    }
                }

                /*
                 *************************************************************************
                 * 审批通过调用 贷款信息维护    ln3053
                 * *************************************************************************/
                //TODO 修改借据表还款方式
                domain.setContApproveStatus(CmisCommonConstants.WF_STATUS_997);
                domain.setContStatus(CmisCommonConstants.CONT_STATUS_200);
                iqpRepayWayChgService.updateSelective(domain);



                // 生成归档任务
                log.info("开始系统生成档案归档信息");
                String cusId = domain.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                docArchiveClientDto.setDocType("26");// 26:业务变更-还款计划变更
                docArchiveClientDto.setBizSerno(iqpSerno);
                docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(domain.getManagerId());
                docArchiveClientDto.setManagerBrId(domain.getManagerBrId());
                docArchiveClientDto.setInputId(domain.getInputId());
                docArchiveClientDto.setInputBrId(domain.getInputBrId());
                docArchiveClientDto.setContNo(domain.getContNo());
                docArchiveClientDto.setBillNo(domain.getBillNo());
                docArchiveClientDto.setLoanAmt(domain.getLoanAmt());
                docArchiveClientDto.setStartDate(domain.getLoanStartDate());
                docArchiveClientDto.setEndDate(domain.getLoanEndDate());
                docArchiveClientDto.setPrdId(domain.getPrdId());
                docArchiveClientDto.setPrdName(domain.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成档案归档信息失败");
                }


                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpRepayWayChg.setContApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpRepayWayChg.setContApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    public Integer changeAccLoan(String iqpSerno){
        IqpRepayWayChg domain = iqpRepayWayChgService.selectByPrimaryKey(iqpSerno);
        String bill_no = domain.getBillNo();
        AccLoan accLoan = accLoanService.selectByBillNo(bill_no);
        accLoan.setRepayMode(domain.getRepayMode());
        return accLoanService.update(accLoan);
    }

    public void createCentralFileTask(String bizType , String iqpSerno,ResultInstanceDto instanceInfo){
        IqpRepayWayChg iqpRepayWayChg = iqpRepayWayChgService.selectByPrimaryKey(iqpSerno);
        try {
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpRepayWayChg.getInputBrId());
            String orgType = resultDto.getData().getOrgType();
            //        0-总行部室
            //        1-异地支行（有分行）
            //        2-异地支行（无分行）
            //        3-异地分行
            //        4-中心支行
            //        5-综合支行
            //        6-对公支行
            //        7-零售支行
            if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType)){
                //新增临时档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(iqpRepayWayChg.getIqpSerno());
                centralFileTaskdto.setCusId(iqpRepayWayChg.getCusId());
                centralFileTaskdto.setCusName(iqpRepayWayChg.getCusName());
                centralFileTaskdto.setBizType(bizType); //
                centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
                centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setInputId(iqpRepayWayChg.getInputId());
                centralFileTaskdto.setInputBrId(iqpRepayWayChg.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setTaskType("02"); // 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);
            }
        }catch (Exception e){
            log.info("利率变更审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_SGCZ14.equals(flowCode);
    }
}
