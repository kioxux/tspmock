package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdLevelDetails
 * @类描述: lmt_sig_invest_prod_level_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 17:25:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestProdLevelDetailsDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 投资级别 **/
	private String investLevel;
	
	/** 债项评级 **/
	private String debtGrade;
	
	/** 本级别募集规模（元） **/
	private java.math.BigDecimal curtLevelRaiseScale;
	
	/** 本级别本行拟投资规模（元） **/
	private java.math.BigDecimal curtLevelInvestScale;
	
	/** 本级别预计年化收益率 **/
	private java.math.BigDecimal curtLevelForeArr;
	
	/** 还本付息方式 **/
	private String gdpMode;
	
	/** 预期到期日 **/
	private String preEndDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param investLevel
	 */
	public void setInvestLevel(String investLevel) {
		this.investLevel = investLevel == null ? null : investLevel.trim();
	}
	
    /**
     * @return InvestLevel
     */	
	public String getInvestLevel() {
		return this.investLevel;
	}
	
	/**
	 * @param debtGrade
	 */
	public void setDebtGrade(String debtGrade) {
		this.debtGrade = debtGrade == null ? null : debtGrade.trim();
	}
	
    /**
     * @return DebtGrade
     */	
	public String getDebtGrade() {
		return this.debtGrade;
	}
	
	/**
	 * @param curtLevelRaiseScale
	 */
	public void setCurtLevelRaiseScale(java.math.BigDecimal curtLevelRaiseScale) {
		this.curtLevelRaiseScale = curtLevelRaiseScale;
	}
	
    /**
     * @return CurtLevelRaiseScale
     */	
	public java.math.BigDecimal getCurtLevelRaiseScale() {
		return this.curtLevelRaiseScale;
	}
	
	/**
	 * @param curtLevelInvestScale
	 */
	public void setCurtLevelInvestScale(java.math.BigDecimal curtLevelInvestScale) {
		this.curtLevelInvestScale = curtLevelInvestScale;
	}
	
    /**
     * @return CurtLevelInvestScale
     */	
	public java.math.BigDecimal getCurtLevelInvestScale() {
		return this.curtLevelInvestScale;
	}
	
	/**
	 * @param curtLevelForeArr
	 */
	public void setCurtLevelForeArr(java.math.BigDecimal curtLevelForeArr) {
		this.curtLevelForeArr = curtLevelForeArr;
	}
	
    /**
     * @return CurtLevelForeArr
     */	
	public java.math.BigDecimal getCurtLevelForeArr() {
		return this.curtLevelForeArr;
	}
	
	/**
	 * @param gdpMode
	 */
	public void setGdpMode(String gdpMode) {
		this.gdpMode = gdpMode == null ? null : gdpMode.trim();
	}
	
    /**
     * @return GdpMode
     */	
	public String getGdpMode() {
		return this.gdpMode;
	}
	
	/**
	 * @param preEndDate
	 */
	public void setPreEndDate(String preEndDate) {
		this.preEndDate = preEndDate == null ? null : preEndDate.trim();
	}
	
    /**
     * @return PreEndDate
     */	
	public String getPreEndDate() {
		return this.preEndDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}