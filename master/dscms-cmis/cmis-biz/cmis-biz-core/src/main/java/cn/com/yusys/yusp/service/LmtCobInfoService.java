package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CreditQryBizReal;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.LmtCobInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.IqpCusCreditInfoRetailDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCobInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCobInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-25 19:49:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCobInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private LmtCobInfoMapper lmtCobInfoMapper;
    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;

    @Autowired
    private IqpCusCreditInfoService iqpCusCreditInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtCobInfo selectByPrimaryKey(String pkId) {
        return lmtCobInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtCobInfo> selectAll(QueryModel model) {
        List<LmtCobInfo> records = (List<LmtCobInfo>) lmtCobInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtCobInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCobInfo> list = lmtCobInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param iqpSerno
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>
     * @author hubp
     * @date 2021/4/27 16:28
     * @version 1.0.0
     * @desc 通过申请流水号查询共有人信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<LmtCobInfo> selectByIqpSerno(String iqpSerno) {
        List<LmtCobInfo> list = lmtCobInfoMapper.selectByIqpSerno(iqpSerno);
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto insert(LmtCobInfo lmtCobInfo) {
        int results = 0;
        try {
            //新增共同借款人
            results = lmtCobInfoMapper.insert(lmtCobInfo);
            if (results == 1) {
                return new ResultDto(lmtCobInfo).message("操作成功");
            } else {
                throw new YuspException(EcbEnum.CTR_EXCEPTION_DEF.key,
                        EcbEnum.CTR_EXCEPTION_DEF.value);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new ResultDto(null);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtCobInfo record) {
        return lmtCobInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtCobInfo record) {
        return lmtCobInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtCobInfo record) {
        return lmtCobInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtCobInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtCobInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:iqpLoanApp
     * @算法描述: 无
     */
    public ResultDto<Integer> addLmtCobInfo(LmtCobInfo lmtCobInfo) {
        int results = 0;
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        try {
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByIqpSerno(lmtCobInfo.getSurveySerno());
            // 2021年10月23日16:32:28 hubp BUG15374 判断添加的共借人是否为借款人
            if(iqpLoanApp.getCertCode().equals(lmtCobInfo.getCommonDebitCertCode())){
                return new ResultDto(results).code("9999").message("借款人不可添加为共同借款人");
            }
            //新增共同借款人
            lmtCobInfo.setOprType("01");
            results = lmtCobInfoMapper.insert(lmtCobInfo);
            if (results != 1) {
                throw BizException.error(null, "9999", "生成共借人数据异常");
            }
            //共借人不为本人时，需查询征信
            if (!iqpLoanApp.getCusId().equals(lmtCobInfo.getCusId())) {
                //生成征信流水号
                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
                creditQryBizReal.setBizSerno(lmtCobInfo.getSurveySerno());
                creditQryBizReal.setCrqlSerno(crqlSerno);
                creditQryBizReal.setCusId(lmtCobInfo.getCusId());
                creditQryBizReal.setCusName(lmtCobInfo.getCommonDebitName());
                creditQryBizReal.setCertType(lmtCobInfo.getCommonDebitCertType());
                creditQryBizReal.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                creditQryBizReal.setBorrowRel("005");
                creditQryBizReal.setScene("01");
                results = creditQryBizRealMapper.insertSelective(creditQryBizReal);
                if (results != 1) {
                    throw BizException.error(null, "9999", "生成征信查询与业务关联表数据异常");
                }
                //征信详情表
                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                //生成征信详情数据
                creditReportQryLst.setCrqlSerno(crqlSerno);
                creditReportQryLst.setCusId(lmtCobInfo.getCusId());
                creditReportQryLst.setCusName(lmtCobInfo.getCommonDebitName());
                creditReportQryLst.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                creditReportQryLst.setCertType(lmtCobInfo.getCommonDebitCertType());
                creditReportQryLst.setBorrowerCusId(iqpLoanApp.getCusId());
                creditReportQryLst.setBorrowerCusName(iqpLoanApp.getCusName());
                creditReportQryLst.setBorrowerCertCode(iqpLoanApp.getCertCode());
                creditReportQryLst.setCertType("A");
                creditReportQryLst.setBorrowRel("005");
                creditReportQryLst.setQryCls("0");
                creditReportQryLst.setApproveStatus("000");
                creditReportQryLst.setQryResn(creditReportQryLstService.getQryResnByPerIodAndReal("005", "01", "0"));
                results = creditReportQryLstService.insertSelective(creditReportQryLst);
                if (results != 1) {
                    throw BizException.error(null, "9999", "生成征信报告查询信息表数据异常");
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new ResultDto(results);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteCobAndCrql(LmtCobInfo lmtCobInfo) {
        int result;
        result = lmtCobInfoMapper.deleteByPrimaryKey(lmtCobInfo.getPkId());
        if (result < 1) {
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "删除共同借款人异常！");
        }

        //共同借款人删除是联动信用信息
        iqpCusCreditInfoService.deleteIqpCusCreditInfo(lmtCobInfo);

        Map params = new HashMap();
        params.put("iqpSerno", lmtCobInfo.getSurveySerno());
        params.put("certCode", lmtCobInfo.getCommonDebitCertCode());
        result = creditQryBizRealMapper.deleteByCondition(params);
        return result;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/19 10:56
     * @注释 保存或修改
     * @修改历史 时间：2021年6月29日14:15:18  修改人：hubp  修改原因：软删的时候清除征信信息
     */
    public ResultDto saveandupdate(LmtCobInfo lmtCobInfo) {
        logger.info("小微合同申请增加共同借款人开始..........流水号：" + lmtCobInfo.getSurveySerno());
        String cusId = lmtCobInfo.getCusId();
        String surveySerno = lmtCobInfo.getSurveySerno();
        if (!StringUtil.isNotEmpty(cusId)) {
            return new ResultDto(null).code(9999).message("用户编号为空");
        }
        if (!StringUtil.isNotEmpty(surveySerno)) {
            return new ResultDto(null).code(9999).message("参数异常 请刷新当前页面重试");
        }
        int i = -1;
        try {
            //开始增加或更新
            if (StringUtil.isNotEmpty(lmtCobInfo.getPkId())) {
                lmtCobInfo.setUpdateTime(new Date());
                i = lmtCobInfoMapper.updateByPrimaryKeySelective(lmtCobInfo);
            } else {
                List<LmtCobInfo> list = lmtCobInfoMapper.selectByCusIdAndSerno(cusId, surveySerno);
                if (list.size() > 0) {
                    logger.info("小微合同申请增加共同借款人异常..........流水号：" + lmtCobInfo.getSurveySerno() + ",同一流水号不可重复添加共同借款人");
                    return new ResultDto().code(9999).message("同一流水号不可重复添加共同借款人");
                }
                lmtCobInfo.setOprType("01");
                i = lmtCobInfoMapper.insert(lmtCobInfo);
            }
        } catch (Exception e) {
            logger.info("小微合同申请增加共同借款人失败——————————流水号：" + lmtCobInfo.getSurveySerno());
            logger.info("小微合同申请增加共同借款人失败原因：" + e.getMessage());
            return new ResultDto().code(9999).message("保存失败");
        } finally {
            logger.info("小微合同申请增加共同借款人结束..........流水号：" + lmtCobInfo.getSurveySerno());
        }
        return new ResultDto(i);
    }

    /**
     * @param surveySerno
     * @return int
     * @author hubp
     * @date 2021/5/27 15:32
     * @version 1.0.0
     * @desc 根据流水号删除信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteBySerno(String surveySerno) {
        return lmtCobInfoMapper.deleteBySerno(surveySerno);
    }

    /**
     * @方法名称: updateSignStateByContNo
     * @方法描述: 根据合同号、共借人身份证号更新共同借款人信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSignStateByContNo(Map queryMap) {
        return lmtCobInfoMapper.updateSignStateByContNo(queryMap);
    }

    /**
     * @方法名称: selectSignFalgByContNo
     * @方法描述: 根据合同号查询名下共借人签约状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectSignFalgByContNo(String contNo) {
        return lmtCobInfoMapper.selectSignFalgByContNo(contNo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/26 15:35
     * @注释 惠享待新增的时候 直接把客户加入征信列表 主借款人
     */
    public int addHXD(LmtSurveyReportMainInfo mainInfo) {
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        int i = 0;
        //生成征信流水号
        String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
        creditQryBizReal.setBizSerno(mainInfo.getSurveySerno());
        creditQryBizReal.setCrqlSerno(crqlSerno);
        creditQryBizReal.setCusId(mainInfo.getCusId());
        creditQryBizReal.setCusName(mainInfo.getCusName());
        creditQryBizReal.setCertType(mainInfo.getCertType());
        creditQryBizReal.setCertCode(mainInfo.getCertCode());
        creditQryBizReal.setBorrowRel("001");
        creditQryBizReal.setScene("01");
        i = creditQryBizRealMapper.insert(creditQryBizReal);
        if (i != 1) {
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "新增征信异常！");
        }
        //征信详情表
        if (i == 1) {
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            //生成征信详情数据
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setCusId(mainInfo.getCusId());
            creditReportQryLst.setCusName(mainInfo.getCusName());
            creditReportQryLst.setCertCode(mainInfo.getCertCode());
            creditReportQryLst.setCertType(mainInfo.getCertType());
            creditReportQryLst.setBorrowerCusId(mainInfo.getCusId());
            creditReportQryLst.setBorrowerCusName(mainInfo.getCusName());
            creditReportQryLst.setBorrowerCertCode(mainInfo.getCertCode());
            creditReportQryLst.setBorrowRel("001");
            creditReportQryLst.setBorrowerCertCode(mainInfo.getCertCode());
            creditReportQryLst.setBorrowerCusId(mainInfo.getCusId());
            creditReportQryLst.setBorrowerCusName(mainInfo.getCusName());
            creditReportQryLst.setQryCls("0");
            creditReportQryLst.setApproveStatus("000");
            creditReportQryLst.setQryResn("02");
            i = creditReportQryLstService.insertSelective(creditReportQryLst);

        }
        return i;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/26 16:20
     * @注释 因为惠享贷新增共借人需要去增加征信信息，所以现在单拉出来一个方法 不用于区分其他的
     */
    public ResultDto saveandupdatehxd(LmtCobInfo lmtCobInfo) {
        logger.info("惠享贷增加共同借款人开始..........流水号：" + lmtCobInfo.getSurveySerno());
        String cusId = lmtCobInfo.getCusId();
        String surveySerno = lmtCobInfo.getSurveySerno();
        if (!StringUtil.isNotEmpty(cusId)) {
            return new ResultDto(null).code(9999).message("用户编号为空");
        }
        if (!StringUtil.isNotEmpty(surveySerno)) {
            return new ResultDto(null).code(9999).message("参数异常 请刷新当前页面重试");
        }
        int i = -1;
        try {
            //开始增加或更新
            if (StringUtil.isNotEmpty(lmtCobInfo.getPkId())) {
                lmtCobInfo.setUpdateTime(new Date());
                i = lmtCobInfoMapper.updateByPrimaryKeySelective(lmtCobInfo);
            } else {
                List<LmtCobInfo> list = lmtCobInfoMapper.selectByCusIdAndSerno(cusId, surveySerno);
                if (list.size() > 0) {
                    logger.info("小微惠享贷增加共同借款人异常..........流水号：" + lmtCobInfo.getSurveySerno() + ",同一流水号不可重复添加共同借款人");
                    return new ResultDto().code(9999).message("同一流水号不可重复添加共同借款人");
                }
                lmtCobInfo.setOprType("01");
                i = lmtCobInfoMapper.insert(lmtCobInfo);
            }
            // 新增的共同借款人，将其加入征信信息中
            this.saveZx(lmtCobInfo);
        } catch (Exception e) {
            logger.info("小微惠享贷增加共同借款人失败——————————流水号：" + lmtCobInfo.getSurveySerno());
            logger.info("小微惠享贷增加共同借款人失败原因：" + e.getMessage());
            return new ResultDto().code(9999).message("保存失败");
        } finally {
            logger.info("小微惠享贷增加共同借款人结束..........流水号：" + lmtCobInfo.getSurveySerno());
        }
        return new ResultDto(i);
    }


    /**
     * @创建人 WH
     * @创建时间 2021/6/26 16:32
     * @注释 hxd共借人剥离出来的方法 用于增改征信
     * @修改历史 时间：2021年6月29日20:16:52  修改人：hubp  修改原因：征信信息完善
     */

    @Transactional(rollbackFor = Exception.class)
    public ResultDto saveZx(LmtCobInfo lmtCobInfo) {
        logger.info("小微惠享贷共同借款人同步征信开始..........共同借款人证件号【{}】：", lmtCobInfo.getCommonDebitCertCode());
        int result = 0;
        try {
            // 1、获取当前登录人信息
            User userInfo = SessionUtils.getUserInformation();
            String managerId = userInfo.getLoginCode();
            // 2、查看此客户经理是否发起过改共同借款人的征信，且日期在30天之内
            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            creditReportQryLst.setCertCode(lmtCobInfo.getCommonDebitCertCode());
            creditReportQryLst.setQryCls("0");
            creditReportQryLst.setQryResn("02");
            if (creditReportQryLstService.selectByPeriod("01", creditReportQryLst)) {
                // 如果存在，直接关联
                // 查出该信息
                CreditReportQryLst creditReportQryLstNew = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                // 获取该流水号
                creditQryBizReal.setBizSerno(lmtCobInfo.getSurveySerno());
                creditQryBizReal.setCrqlSerno(creditReportQryLstNew.getCrqlSerno());
                creditQryBizReal.setCusId(lmtCobInfo.getCusId());
                creditQryBizReal.setCusName(lmtCobInfo.getCommonDebitName());
                creditQryBizReal.setCertType(lmtCobInfo.getCommonDebitCertType());
                creditQryBizReal.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                creditQryBizReal.setBorrowRel("005");
                creditQryBizReal.setScene("01");
                creditQryBizReal.setQryCls("0");
                // 直接更新征信关联表
                result = creditQryBizRealMapper.insert(creditQryBizReal);
                if (result != 1) {
                    throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "新增征信异常！");
                }
            } else {
                // 不存在，则插入信息
                //生成征信流水号
                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
                creditQryBizReal.setBizSerno(lmtCobInfo.getSurveySerno());
                creditQryBizReal.setCrqlSerno(crqlSerno);
                creditQryBizReal.setCusId(lmtCobInfo.getCusId());
                creditQryBizReal.setCusName(lmtCobInfo.getCommonDebitName());
                creditQryBizReal.setCertType(lmtCobInfo.getCommonDebitCertType());
                creditQryBizReal.setCertCode(lmtCobInfo.getCommonDebitCertCode());
                creditQryBizReal.setBorrowRel("005");
                creditQryBizReal.setScene("01");
                creditQryBizReal.setQryCls("0");
                result = creditQryBizRealMapper.insert(creditQryBizReal);
                if (result != 1) {
                    throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "新增征信异常！");
                }
                //征信详情表
                if (result == 1) {
                    //生成征信详情数据
                    creditReportQryLst.setCrqlSerno(crqlSerno);
                    creditReportQryLst.setCusId(lmtCobInfo.getCusId());
                    creditReportQryLst.setCusName(lmtCobInfo.getCommonDebitName());
                    creditReportQryLst.setCertType(lmtCobInfo.getCommonDebitCertType());
                    creditReportQryLst.setBorrowRel("005");
                    creditReportQryLst.setQryCls("0");
                    creditReportQryLst.setApproveStatus("000");
                    result = creditReportQryLstService.insertSelective(creditReportQryLst);

                }
            }


        } catch (Exception e) {
            logger.info("小微惠享贷共同借款人同步征信异常——————————共同借款人证件号【{}】：", lmtCobInfo.getCommonDebitCertCode());
            logger.info("小微惠享贷增加共同借款人失败原因：" + e.getMessage());
            return new ResultDto(-1).code(9999).message("保存失败");
        } finally {
            logger.info("小微惠享贷共同借款人同步征信结束..........共同借款人证件号【{}】：", lmtCobInfo.getCommonDebitCertCode());
        }
        return new ResultDto(result);
    }

    /**
     * @param lmtCobInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/29 14:20
     * @version 1.0.0
     * @desc 应wh要求单拉出一个删除方法
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto deleteHxd(LmtCobInfo lmtCobInfo) {
        logger.info("惠享贷删除共同借款人开始..........流水号：" + lmtCobInfo.getSurveySerno());
        String cusId = lmtCobInfo.getCusId();
        String surveySerno = lmtCobInfo.getSurveySerno();
        if (!StringUtil.isNotEmpty(cusId)) {
            return new ResultDto(null).code(9999).message("用户编号为空");
        }
        if (!StringUtil.isNotEmpty(surveySerno)) {
            return new ResultDto(null).code(9999).message("参数异常 请刷新当前页面重试");
        }
        int result = -1;
        try {
            //开始删除
            lmtCobInfo.setUpdateTime(new Date());
            lmtCobInfo.setOprType("02");
            result = lmtCobInfoMapper.updateByPrimaryKeySelective(lmtCobInfo);
            if (result != 1) {
                throw BizException.error(null, "9999", "共同借款人删除失败");
            }
            // 清除征信信息
            Map<String, String> map = new HashMap<>();
            map.put("iqpSerno", lmtCobInfo.getSurveySerno());
            map.put("certCode", lmtCobInfo.getCommonDebitCertCode());
            creditQryBizRealMapper.deleteByCondition(map);
        } catch (Exception e) {
            logger.info("小微惠享贷增加共同借款人失败——————————流水号：" + lmtCobInfo.getSurveySerno());
            logger.info("小微惠享贷增加共同借款人失败原因：" + e.getMessage());
            return new ResultDto(-1).code(9999).message(e.getMessage());
        } finally {
            logger.info("小微惠享贷增加共同借款人结束..........流水号：" + lmtCobInfo.getSurveySerno());
        }
        return new ResultDto(result);
    }
}
