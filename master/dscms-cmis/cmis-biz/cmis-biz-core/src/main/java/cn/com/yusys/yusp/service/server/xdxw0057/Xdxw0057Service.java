package cn.com.yusys.yusp.service.server.xdxw0057;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0057.req.Xdxw0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.resp.Xdxw0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 接口处理类:根据核心客户号查询经营性贷款批复额度
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0057Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0057Service.class);

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    /**
     * 根据核心客户号查询经营性贷款批复额度
     * @param xdxw0057DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0057DataRespDto getLmtByQueryType(Xdxw0057DataReqDto xdxw0057DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataReqDto));
        Xdxw0057DataRespDto result = new Xdxw0057DataRespDto();
        try {
            result.setContAmt(new BigDecimal(0L));// 合同金额
            if (CmisBizConstants.LMT_1.equals(xdxw0057DataReqDto.getQueryType())) {
                result = lmtCrdReplyInfoMapper.getLmtByQueryType(xdxw0057DataReqDto);
            }
            if (CmisBizConstants.LMT_2.equals(xdxw0057DataReqDto.getQueryType())) {
                result = lmtSurveyReportMainInfoMapper.getLmtByQueryType(xdxw0057DataReqDto);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(result));
        return result;
    }
}
