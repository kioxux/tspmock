/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAsplDetails
 * @类描述: ctr_aspl_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "池额度台账", fileType = ExcelCsv.ExportFileType.XLS)
public class CtrAsplDetailsExport {

	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 40)
	private String cusId;

	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 40)
	private String cusName;

	/** 资产池协议编号 **/
	@ExcelField(title = "资产池协议编号", viewLength = 40)
	private String contNo;

	/** 资产池额度 **/
	@ExcelField(title = "资产池额度", viewLength = 20)
	private java.math.BigDecimal contAmt;

	/** 合同状态 **/
	@ExcelField(title = "合同状态", viewLength = 20)
	private String contStatus;

	/** 起始日期 **/
	@ExcelField(title = "起始日期", viewLength = 20)
	private String startDate;

	/** 到期日期 **/
	@ExcelField(title = "到期日期", viewLength = 20)
	private String endDate;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}