package cn.com.yusys.yusp.service.server.xdzc0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0022.req.Xdzc0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.resp.Xdzc0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAccpTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:发票补录上传接口
 *
 * @Author xs
 * @Date 2022/22/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0022Service {

    @Autowired
    private AsplAccpTaskService asplAccpTaskService;


    private static final Logger logger = LoggerFactory.getLogger(Xdzc0022Service.class);

    /**
     * 交易码：xdzc0022
     * 交易描述:
     * 资产汇总查询
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0022DataRespDto xdzc0022Service(Xdzc0022DataReqDto xdzc0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value);
        Xdzc0022DataRespDto xdzc0022DataRespDto = new Xdzc0022DataRespDto();
        String pvpSerno = xdzc0022DataReqDto.getBillNo();//出账流水（批次号）
        String yxSerno = xdzc0022DataReqDto.getYxSerno();//发票影像ID
        xdzc0022DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 返回码
        xdzc0022DataRespDto.setOpMsg("发票补录,逻辑失败");// 返回信息

        try {
            // 根据出账流水（批次号）更新 补录状态
            asplAccpTaskService.updateIsAddBill(pvpSerno);
            xdzc0022DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 返回码
            xdzc0022DataRespDto.setOpMsg("发票补录,成功");// 返回信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value);
        return xdzc0022DataRespDto;
    }
}