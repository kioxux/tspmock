/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerBailDistApp
 * @类描述: coop_partner_bail_dist_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-10 18:42:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_partner_bail_dist_app")
public class CoopPartnerBailDistApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金账户最低金额(元) **/
	@Column(name = "BAIL_ACC_LOW_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAccLowAmt;
	
	/** 保证金账号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 60)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB_SEQ", unique = false, nullable = true, length = 60)
	private String bailAccNoSubSeq;
	
	/** 保证金账户余额(元) **/
	@Column(name = "BAIL_ACC_NO_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAccNoBal;
	
	/** 当前已担保余额(元) **/
	@Column(name = "CURT_GRT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtGrtBal;
	
	/** 可提取保证金金额(元) **/
	@Column(name = "ALLOW_DIST_BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal allowDistBailAmt;
	
	/** 本次提取金额(元) **/
	@Column(name = "CURT_DIST_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtDistAmt;
	
	/** 其他相关说明 **/
	@Column(name = "OTHER_CORRE_DESC", unique = false, nullable = true, length = 500)
	private String otherCorreDesc;
	
	/** 审批状态 **/
	@Column(name = "APPR_STATUS", unique = false, nullable = true, length = 10)
	private String apprStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAccLowAmt
	 */
	public void setBailAccLowAmt(java.math.BigDecimal bailAccLowAmt) {
		this.bailAccLowAmt = bailAccLowAmt;
	}
	
    /**
     * @return bailAccLowAmt
     */
	public java.math.BigDecimal getBailAccLowAmt() {
		return this.bailAccLowAmt;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSubSeq
	 */
	public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
		this.bailAccNoSubSeq = bailAccNoSubSeq;
	}
	
    /**
     * @return bailAccNoSubSeq
     */
	public String getBailAccNoSubSeq() {
		return this.bailAccNoSubSeq;
	}
	
	/**
	 * @param bailAccNoBal
	 */
	public void setBailAccNoBal(java.math.BigDecimal bailAccNoBal) {
		this.bailAccNoBal = bailAccNoBal;
	}
	
    /**
     * @return bailAccNoBal
     */
	public java.math.BigDecimal getBailAccNoBal() {
		return this.bailAccNoBal;
	}
	
	/**
	 * @param curtGrtBal
	 */
	public void setCurtGrtBal(java.math.BigDecimal curtGrtBal) {
		this.curtGrtBal = curtGrtBal;
	}
	
    /**
     * @return curtGrtBal
     */
	public java.math.BigDecimal getCurtGrtBal() {
		return this.curtGrtBal;
	}
	
	/**
	 * @param allowDistBailAmt
	 */
	public void setAllowDistBailAmt(java.math.BigDecimal allowDistBailAmt) {
		this.allowDistBailAmt = allowDistBailAmt;
	}
	
    /**
     * @return allowDistBailAmt
     */
	public java.math.BigDecimal getAllowDistBailAmt() {
		return this.allowDistBailAmt;
	}
	
	/**
	 * @param curtDistAmt
	 */
	public void setCurtDistAmt(java.math.BigDecimal curtDistAmt) {
		this.curtDistAmt = curtDistAmt;
	}
	
    /**
     * @return curtDistAmt
     */
	public java.math.BigDecimal getCurtDistAmt() {
		return this.curtDistAmt;
	}
	
	/**
	 * @param otherCorreDesc
	 */
	public void setOtherCorreDesc(String otherCorreDesc) {
		this.otherCorreDesc = otherCorreDesc;
	}
	
    /**
     * @return otherCorreDesc
     */
	public String getOtherCorreDesc() {
		return this.otherCorreDesc;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}