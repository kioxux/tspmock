/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarSaleBrhRel
 * @类描述: lmt_car_sale_brh_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 11:45:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_car_sale_brh_rel")
public class LmtCarSaleBrhRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 项目编号 **/
	@Column(name = "pro_no", unique = false, nullable = false, length = 40)
	private String proNo;
	
	/** 专卖店地址 **/
	@Column(name = "agency_addr", unique = false, nullable = true, length = 500)
	private String agencyAddr;
	
	/** 街道 **/
	@Column(name = "street", unique = false, nullable = true, length = 200)
	private String street;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param agencyAddr
	 */
	public void setAgencyAddr(String agencyAddr) {
		this.agencyAddr = agencyAddr;
	}
	
    /**
     * @return agencyAddr
     */
	public String getAgencyAddr() {
		return this.agencyAddr;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}


}