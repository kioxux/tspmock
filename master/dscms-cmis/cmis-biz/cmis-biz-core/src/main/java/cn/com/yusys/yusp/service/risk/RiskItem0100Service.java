package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.AccLoanService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 借款人贷款历史台账校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0100Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0100Service.class);


    @Autowired
    private AccLoanService accLoanService;

    public RiskResultDto riskItem0100(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        HashMap<String, String> map = (HashMap)queryModel.getCondition().get("param");
        String bizType = queryModel.getCondition().get("bizType").toString();
        String cusId = map.get("cusId"); //
        log.info("*************借款人贷款历史台账校验***********【{}】", cusId);
        if (StringUtils.isBlank(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10003);
            return riskResultDto;
        }
        if("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)){
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);
            model.addCondition("accStatus","5");
            List<AccLoan> accLoanList = accLoanService.selectByModel(model);
            log.info("*************借款人已核销呆账***********【{}】", JSON.toJSONString(accLoanList));
            if(accLoanList.size() > 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10001); //客户存在已核销呆账
                return riskResultDto;
            }
        } else if ("LS005".equals(bizType) || "LS006".equals(bizType)
                    || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            // 借款人名下消费贷款五级分类为损失后，拦截。
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);
            model.addCondition("fiveClass","50");
            List<AccLoan> accLoanList = accLoanService.selectByModel(model);
            log.info("*************借款人五级分类为损失***********【{}】", JSON.toJSONString(accLoanList));
            if(accLoanList.size() > 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10002);
                return riskResultDto;
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_00021);
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************借款人贷款历史台账校验***********【{}】", cusId);
        return riskResultDto;
    }
}
