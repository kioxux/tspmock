/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.dto.OtherAppDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.OtherDiscPerferRateApp;
import cn.com.yusys.yusp.repository.mapper.OtherDiscPerferRateAppMapper;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherDiscPerferRateAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-02 17:20:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherDiscPerferRateAppService {

    @Autowired
    private OtherDiscPerferRateAppMapper otherDiscPerferRateAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherDiscPerferRateApp selectByPrimaryKey(String serno) {
        return otherDiscPerferRateAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherDiscPerferRateApp> selectAll(QueryModel model) {
        List<OtherDiscPerferRateApp> records = (List<OtherDiscPerferRateApp>) otherDiscPerferRateAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherDiscPerferRateApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherDiscPerferRateApp> list = otherDiscPerferRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherDiscPerferRateApp record) {
    	record.setApproveStatus("000");
    	record.setCreateTime(new Date());
    	record.setOprType("01");
        return otherDiscPerferRateAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherDiscPerferRateApp record) {
        return otherDiscPerferRateAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherDiscPerferRateApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherDiscPerferRateAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherDiscPerferRateApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherDiscPerferRateAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherDiscPerferRateAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherDiscPerferRateAppMapper.deleteByIds(ids);
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherDiscPerferRateApp otherDiscPerferRateApp = new OtherDiscPerferRateApp();
        otherDiscPerferRateApp.setSerno(serno);
        otherDiscPerferRateApp.setApproveStatus(approveStatus);
        return updateSelective(otherDiscPerferRateApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author css
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherDiscPerferRateAppMapper.selectOtherAppDtoDataByParam(map);
    }

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherDiscPerferRateApp> selectByCusId(Map params) {
        String  cusId = (String) params.get("cusId");
        if(null!=cusId || !"".equals(cusId)) {
            return otherDiscPerferRateAppMapper.selectByCusId(params);
        }else {
            throw new RuntimeException("Oops: cusId can't be null!" );
        }
    }
}
