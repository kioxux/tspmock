/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpAcctChgCont;
import cn.com.yusys.yusp.domain.IqpAcctChgDtl;
import cn.com.yusys.yusp.repository.mapper.IqpAcctChgDtlMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctChgDtlService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-21 16:10:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAcctChgDtlService {

    @Autowired
    private IqpAcctChgDtlMapper iqpAcctChgDtlMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpAcctChgDtl selectByPrimaryKey(String pkId) {
        return iqpAcctChgDtlMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAcctChgDtl> selectAll(QueryModel model) {
        List<IqpAcctChgDtl> records = (List<IqpAcctChgDtl>) iqpAcctChgDtlMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpAcctChgDtl> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAcctChgDtl> list = iqpAcctChgDtlMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpAcctChgDtl record) {
        return iqpAcctChgDtlMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpAcctChgDtl record) {
        return iqpAcctChgDtlMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpAcctChgDtl record) {
        return iqpAcctChgDtlMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpAcctChgDtl record) {
        return iqpAcctChgDtlMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAcctChgDtlMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAcctChgDtlMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIqpSerno
     * @方法描述: 根据申请流水号 执行 删除操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIqpSerno(String iqpSerno) {
        return iqpAcctChgDtlMapper.deleteByIqpSerno(iqpSerno);
    }

    /**
     *
     * @description 存在待变更的合同下帐号信息返回true，不存在返回false
     * @version
     * @className
     * @methodName checkExistIqpAcctDtl
     * @return boolean
     * @author
     * @date 2021/1/21 20:02
     *
     */
    public boolean checkExistIqpAcctDtl(IqpAcctChgCont iqpAcctChgCont){
        return iqpAcctChgDtlMapper.checkExistIqpAcctDtl(iqpAcctChgCont.getContNo())<1?false:true;
    }

    /**
     * @方法名称: insertDefValueAfterContAdd
     * @方法描述: 根据合同号 执行 新增操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertDefValueAfterContAdd(String iqpSerno) {
        return iqpAcctChgDtlMapper.insertDefValueAfterContAdd(iqpSerno);
    }
}
