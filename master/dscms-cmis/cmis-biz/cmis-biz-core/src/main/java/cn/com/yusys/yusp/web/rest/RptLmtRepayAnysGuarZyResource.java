/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarZy;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarZyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarZyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 23:10:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarzy")
public class RptLmtRepayAnysGuarZyResource {
    @Autowired
    private RptLmtRepayAnysGuarZyService rptLmtRepayAnysGuarZyService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarZy>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarZy> list = rptLmtRepayAnysGuarZyService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarZy>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarZy>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarZy> list = rptLmtRepayAnysGuarZyService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarZy>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarZy> create(@RequestBody RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy) throws URISyntaxException {
        rptLmtRepayAnysGuarZyService.insert(rptLmtRepayAnysGuarZy);
        return new ResultDto<RptLmtRepayAnysGuarZy>(rptLmtRepayAnysGuarZy);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarZyService.update(rptLmtRepayAnysGuarZy);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = rptLmtRepayAnysGuarZyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptLmtRepayAnysGuarZy>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptLmtRepayAnysGuarZy>>(rptLmtRepayAnysGuarZyService.selectByModel(model));
    }

    @PostMapping("/deletePld")
    protected ResultDto<Integer> deletePld(@RequestBody RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy) {
        String pkId = rptLmtRepayAnysGuarZy.getPkId();
        return new ResultDto<Integer>(rptLmtRepayAnysGuarZyService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/insertPld")
    protected ResultDto<Integer> insetPld(@RequestBody RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy) {
        return new ResultDto<Integer>(rptLmtRepayAnysGuarZyService.insertSelective(rptLmtRepayAnysGuarZy));
    }

    @PostMapping("/updatePld")
    protected ResultDto<Integer> updatePld(@RequestBody RptLmtRepayAnysGuarZy rptLmtRepayAnysGuarZy) {
        return new ResultDto<Integer>(rptLmtRepayAnysGuarZyService.updateSelective(rptLmtRepayAnysGuarZy));
    }
}
