package cn.com.yusys.yusp.service.server.xdht0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.domain.GuarCommonOwner;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List1;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.req.Xddb06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.Xddb06RespDto;
import cn.com.yusys.yusp.dto.server.xdht0017.req.Xdht0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0017.resp.Xdht0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.Dscms2YphsxtClientService;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0017Service
 * @类描述: #服务类
 * @功能描述: 借款、担保合同PDF生成
 * @创建人: sunzhen
 * @创建时间: 2021-06-13 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0017Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0017Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Autowired
    private GrtGuarContRelMapper grtGuarContRelMapper;
    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;
    @Resource
    private ICusClientService icusClientService;
    @Autowired
    private GuarCommonOwnerMapper guarCommonOwnerMapper;


    /**
     * 借款、担保合同PDF生成
     * <p>
     * 根据借款合同号或者担保合同号，去合同表捞取合同详细信息，
     * 生成借款合同PDF文档或者担保合同PDF文档，并且返回PDF文档地址。
     * 业务校验：
     * 1.如果共有人ID不为null并且查到的共有人信息不完整，则返回错误信息，不生成合同PDF。
     * 2.优抵贷无一般担保合同，只有最高额担保合同，如果查到的合同类型和优抵贷产品不符合，则返回错误信息，不生成合同PDF。
     * <p>
     * 参考老接口代码：DzhtscAction.java
     *
     * @param xdht0017DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0017DataRespDto xdht0017(Xdht0017DataReqDto xdht0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataReqDto));
        Xdht0017DataRespDto xdht0017DataRespDto = new Xdht0017DataRespDto();
        try {
            //获取传参
            String loanContNo = xdht0017DataReqDto.getLoanContNo(); //借款合同号
            String grtContNo = xdht0017DataReqDto.getGrtContNo(); //担保合同号

            //借款合同号和担保合同有一个必输，但不能同时输入
            if (!StringUtils.isBlank(loanContNo) && !StringUtils.isBlank(grtContNo)) {
                throw new Exception("借款合同/担保合同不可全填，同时生成pdf！");
            } else if (StringUtils.isBlank(loanContNo) && StringUtils.isBlank(grtContNo)) {
                throw new Exception("借款合同号【loanContNo】和担保合同号【grtContNo】不能都为空！");
            }

            //根据不同种类的合同号生成相应的合同
            if (!StringUtils.isBlank(loanContNo)) {
                //创建借款合同PDF
                CtrLoanCont ctrLoanContInfo = ctrLoanContMapper.queryCtrLoanContInfoByContNo(loanContNo);
                String contType = ctrLoanContInfo.getContType(); //合同类型
                String managerBrId = ctrLoanContInfo.getManagerBrId(); //管理机构
                String cusId = ctrLoanContInfo.getCusId(); //客户ID

                CusIndivContactDto cusIndivContactDto = Optional.ofNullable(icusClientService.queryCusIndivByCusId(cusId)).orElse(new CusIndivContactDto());
                String mobile = cusIndivContactDto.getMobileNo(); //客户电话
                String postAddr = cusIndivContactDto.getSendAddr(); //邮寄地址

                if (StringUtil.isEmpty(mobile) || StringUtil.isEmpty(postAddr)) {
                    throw new Exception("主借款人联系信息不完整，请客户经理完善客户信息！");
                } else {
                    String TempleteName = "xwybjkht.cpt"; //合同模板名字 xdybjkht.cpt
                    String saveFileName = "xdybjkht_" + loanContNo + ""; // 合同另保存名字 （模板名字+合同申请流水号）

                    if ("1".equals(contType)) { //一般借款合同
                        TempleteName = "line_xwybjkht.cpt";//xdybjkht.cpt
                        saveFileName = "xdybjkht_" + loanContNo + ""; // 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("2".equals(contType)) { //最高额借款合同
                        TempleteName = "line_xwzgejkht.cpt"; // 合同模板名字 xdzgejkht.cpt
                        saveFileName = "xdzgejkht_" + loanContNo + ""; // 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("810100".equals(managerBrId)) {//东海
                        TempleteName = "dh" + TempleteName;
                        saveFileName = "dh" + saveFileName;
                    } else if ("800100".equals(managerBrId)) {//寿光
                        TempleteName = "sg" + TempleteName;
                        saveFileName = "sg" + saveFileName;
                    }

                    //查询服务器配置
                    //待调试
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("pkId", "00001");
                    ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
                    List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
                    CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
                    //查询存储地址信息
                    String ip = cfgGenerateTempFileDto.getLoginIp();
                    String port = cfgGenerateTempFileDto.getLoginPort();
                    String username = cfgGenerateTempFileDto.getLoginUsername();
                    String password = cfgGenerateTempFileDto.getLoginPwd();
                    String path = cfgGenerateTempFileDto.getFilePath();
                    String url = cfgGenerateTempFileDto.getMemo();

                    //调用帆软的生成pdf的方法
                    //1、传入帆软报表生成需要的参数
                    HashMap<String, Object> parameterMap = new HashMap<String, Object>();
                    parameterMap.put("contNo", loanContNo);
                    //传入公共参数
                    parameterMap.put("TempleteName", TempleteName);//模板名称（附带路径）
                    parameterMap.put("saveFileName", saveFileName);//待生成的PDF文件名称
                    parameterMap.put("path", path);
                    //2、调用公共方法生成pdf
                    RestTemplate restTemplate = new RestTemplate();
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    if (StringUtils.isEmpty(url)) {
                        // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
                    }
                    // 生成PDF文件
                    try {
                        FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                        frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                        frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                        frptPdfArgsDto.setSerno(loanContNo);//设置流水
                        frptPdfArgsDto.setPath(path);//路径
                        frptPdfArgsDto.setIp(ip);//IP地址
                        frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                        frptPdfArgsDto.setUserName(username);
                        frptPdfArgsDto.setPassWord(password);
                        frptPdfArgsDto.setMap(parameterMap);
                        //HttpEntity<HashMap> entity = new HttpEntity<HashMap>(parameterMap,headers);
                        HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                        String code = responseEntity.getBody();
                    } catch (Exception e) {
                        logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, e.getMessage());
                    }

                    //返回值pdfDepoAddr=PdfFilename#ip#port#username#password#path
                    String pdfDepoAddr = saveFileName + ".pdf#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
                    xdht0017DataRespDto.setPdfDepoAddr(pdfDepoAddr);
                    xdht0017DataRespDto.setOpMsg("借款合同生成成功！");
                }


            } else if (!StringUtils.isBlank(grtContNo)) {
                //创建担保合同PDF
                String str[] = grtContNo.split("#");
                if (str.length < 2) {
                    throw new Exception("担保合同号格式应为：【担保号#借款号】");
                }
                String guarContNo = str[0]; //担保号
                String contNo = str[1]; //合同号
                //查询担保合同项下的保证编号
                List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelMapper.getGuarNoByGuarContNo(guarContNo);
                for (int i = 0; i < grtGuarContRelList.size(); i++) {
                    String guarNo = grtGuarContRelList.get(i).getGuarNo();//押品编号
                    //调用押品系统接口xddb06查询共有人信息，获取相关参数
                    Xddb06ReqDto xddb06ReqDto = new Xddb06ReqDto();
                    xddb06ReqDto.setGuaranty_id(guarNo);
                    logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(xddb06ReqDto));
                    ResultDto<Xddb06RespDto> resultDto = dscms2YphsxtClientService.xddb06(xddb06ReqDto);
                    logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(resultDto));
                    String code = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String meesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Xddb06RespDto result = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, code)) {
                        //获取相关的值并解析
                        result = resultDto.getData();
                        List<cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List> list = result.getList();
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List xddb06 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List();
                        if (CollectionUtils.nonEmpty(list)) {
                            xddb06 = list.get(0);
                        }
                        String commonOwnerId = (null == xddb06.getCommon_owner_id() ? "" : xddb06.getCommon_owner_id());
                        String commonOwnerName = (null == xddb06.getCommon_owner_name() ? "" : xddb06.getCommon_owner_name());
                        String commonAddr = (null == xddb06.getCommon_addr() ? "" : xddb06.getCommon_addr());
                        String commonTel = (null == xddb06.getCommon_tel() ? "" : xddb06.getCommon_tel());
                        //共有人信息是否完整
                        if ("".equals(commonOwnerId) || "".equals(commonOwnerName) || "".equals(commonAddr) || "".equals(commonTel)) {
                            throw new Exception("共有人信息不完整，请客户经理完善客户信息！");
                        }
                    } else {
                        logger.info("*****调用押品系统接口xddb06查询共有人信息,返回为" + meesage);
                    }
                }
                //查询担保合同信息
                logger.info("****************XDHT0017*查询担保合同信息开始" + guarContNo);
                GrtGuarCont grtGuarContInfo = Optional.ofNullable(grtGuarContMapper.queryGrtGuarContByGuarContNo(guarContNo)).orElse(new GrtGuarCont());
                //获取担保合同信息要素
                String guarContState = grtGuarContInfo.getGuarContState(); //担保合同状态
                String signDate = grtGuarContInfo.getSignDate(); //签订日期
                String guarContType = grtGuarContInfo.getGuarContType(); //担保合同类型
                String guarWay = grtGuarContInfo.getGuarWay(); //担保合同担保方
                String managerBrId = grtGuarContInfo.getManagerBrId(); //主办机构
                String cusId = grtGuarContInfo.getCusId();//担保合同担保人客户号
                //如果GUAR_CONT_STATE为“101”，返回"您已在【" + SIGN_DATE + "】签约过本笔担保合同！"；
                if ("101".equals(guarContState)) {
                    logger.info("*****您已在" + signDate + "签约过本笔担保合同！");
                    //throw new Exception("您已在【" + signDate + "】签约过本笔担保合同！");
                }
                //定义合同模板名称和生成后的文件名
                String TempleteName = ""; //合同模板名字
                String saveFileName = ""; //合同另保存名字 （模板名字+合同申请流水号）
                //生成PDF
                if ("1".equals(guarContType)) {
                    throw new Exception("优抵贷无一般担保合同！");
                } else {
                    if ("10".equals(guarWay)) {
                        TempleteName = "zgedydbht-ydd.cpt"; //合同模板名字 zgedydbht-ydd.cpt
                        saveFileName = "xdzgedydbht_" + guarContNo + "";
                        if ("810100".equals(managerBrId)) {//东海
                            TempleteName = "dh" + TempleteName;
                            saveFileName = "dh" + saveFileName;
                        } else if ("800100".equals(managerBrId)) {//寿光
                            TempleteName = "sg" + TempleteName;
                            saveFileName = "sg" + saveFileName;
                        }
                    } else {
                        throw new Exception("优抵贷应是最高额抵押担保合同！");
                    }
                }
                /***********************************生成PDF开始**********************************************/
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("pkId", "00001");
                logger.info("*******************************************XDHT0017*生成PDF开始" + guarContNo);
                ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
                List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
                CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
                //查询存储地址信息
                String ip = cfgGenerateTempFileDto.getLoginIp();
                String port = cfgGenerateTempFileDto.getLoginPort();
                String username = cfgGenerateTempFileDto.getLoginUsername();
                String password = cfgGenerateTempFileDto.getLoginPwd();
                String path = cfgGenerateTempFileDto.getFilePath();
                String url = cfgGenerateTempFileDto.getMemo();

                //调用帆软的生成pdf的方法
                //1、传入帆软报表生成需要的参数
                HashMap<String, Object> parameterMap = new HashMap<String, Object>();
                parameterMap.put("guarContNo", guarContNo);
                parameterMap.put("contNo", contNo);

                /****************查询担保人和共有人,组装成一个参数传递给报表开始*****************/
                String comcusIds = "";
                List<GuarCommonOwner> commonlist = guarCommonOwnerMapper.selectCommonOwnerByGuarNo(guarContNo);
                if (CollectionUtils.nonEmpty(commonlist)) {
                    String cusids = cusId;
                    for (int j = 0; j < commonlist.size(); j++) {
                        GuarCommonOwner guarCommonOwner = commonlist.get(j);
                        cusids += "','" + guarCommonOwner.getCommonOwnerId();
                        if (j > 1) {
                            comcusIds += "','" + guarCommonOwner.getCommonOwnerId();
                        } else {
                            comcusIds = guarCommonOwner.getCommonOwnerId();
                        }
                    }
                    parameterMap.put("cusIds", cusids);
                } else {
                    parameterMap.put("cusIds", cusId);
                }
                parameterMap.put("cusId", cusId);//担保人
                parameterMap.put("comcusIds", comcusIds);//共有人
                /****************查询担保人和共有人,组装成一个参数传递给报表结束*****************/
                logger.info("***************************报表查询参数为【{}】", parameterMap);
                //传入公共参数
                parameterMap.put("TempleteName", TempleteName);//模板名称（附带路径）
                parameterMap.put("saveFileName", saveFileName);//待生成的PDF文件名称
                parameterMap.put("path", path);
                //2、调用公共方法生成pdf
                RestTemplate restTemplate = new RestTemplate();
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                // 生成PDF文件
                try {
                    FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                    frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                    frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                    frptPdfArgsDto.setSerno(loanContNo);//设置流水
                    frptPdfArgsDto.setPath(path);//路径
                    frptPdfArgsDto.setIp(ip);//IP地址
                    frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                    frptPdfArgsDto.setUserName(username);
                    frptPdfArgsDto.setPassWord(password);
                    frptPdfArgsDto.setMap(parameterMap);
                    //远程调用
                    logger.info("*******************************************XDHT0017*远程调用开始" + guarContNo);
                    HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                    ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                } catch (Exception e) {
                    logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, e.getMessage());
                }
                //3、返回参数
                String pdfDepoAddr = saveFileName + ".pdf#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
                xdht0017DataRespDto.setPdfDepoAddr(pdfDepoAddr);
                xdht0017DataRespDto.setOpMsg("担保合同生成成功！");
            }
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, e.getMessage());
            xdht0017DataRespDto.setOpMsg("合同生成失败！失败原因:" + e);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataRespDto));
        return xdht0017DataRespDto;
    }
}
