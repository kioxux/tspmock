package cn.com.yusys.yusp.service.server.xdcz0031;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0031.req.Xdcz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.resp.Xdcz0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpRenewLoanInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0003Service
 * @类描述: #服务类 大额分期合同列表查询接口
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0031Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0031Service.class);

    @Autowired
    private PvpRenewLoanInfoMapper pvpRenewLoanInfoMapper;

    /**
     *
     *
     * @param xdcz0031DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0031DataRespDto xdcz0031(Xdcz0031DataReqDto xdcz0031DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value);
        Xdcz0031DataRespDto xdcz0031DataRespDto = new Xdcz0031DataRespDto();
        try {
            String billNo = xdcz0031DataReqDto.getBillNo();//借据号

            //查询总记录数
            logger.info("*********XDCZ0031*根据借据号查询连续续贷次数是否超过2次查询接口开始,查询参数为:{}", billNo);
            String total = pvpRenewLoanInfoMapper.selectBySecondLoanNo(billNo);
            logger.info("*********XDCZ0031*根据借据号查询连续续贷次数是否超过2次查询接口结束,返回参数为:{}", billNo);

            //返回信息
            xdcz0031DataRespDto.setTotalNum(total);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value);
        return xdcz0031DataRespDto;
    }

}
