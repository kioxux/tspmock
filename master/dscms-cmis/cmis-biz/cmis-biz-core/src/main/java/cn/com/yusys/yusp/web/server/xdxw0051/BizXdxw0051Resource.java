package cn.com.yusys.yusp.web.server.xdxw0051;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0051.req.Xdxw0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0051.resp.Xdxw0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0051.Xdxw0051Service;
import org.apache.commons.lang.StringUtils;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:根据业务唯一编号查询无还本续贷贷销售收入
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0051:根据业务唯一编号查询无还本续贷贷销售收入")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0051Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0051Resource.class);

    @Resource
    private Xdxw0051Service xdxw0051Service;

    /**
     * 交易码：xdxw0051
     * 交易描述：根据业务唯一编号查询无还本续贷贷销售收入
     *
     * @param xdxw0051DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据业务唯一编号查询无还本续贷贷销售收入")
    @PostMapping("/xdxw0051")
    protected @ResponseBody
    ResultDto<Xdxw0051DataRespDto> xdxw0051(@Validated @RequestBody Xdxw0051DataReqDto xdxw0051DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051DataReqDto));
        Xdxw0051DataRespDto xdxw0051DataRespDto = new Xdxw0051DataRespDto();// 响应Dto:根据业务唯一编号查询无还本续贷贷销售收入
        ResultDto<Xdxw0051DataRespDto> xdxw0051DataResultDto = new ResultDto<>();
        String applySerno = xdxw0051DataReqDto.getApplySerno();//业务唯一编号
        try {
            // 从xdxw0051DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0051DataReqDto));
            xdxw0051DataRespDto = xdxw0051Service.xdxw0051(xdxw0051DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0051DataRespDto));
            // 封装xdxw0051DataResultDto中正确的返回码和返回信息
            xdxw0051DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0051DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, e.getMessage());
            // 封装xdxw0051DataResultDto中异常返回码和返回信息
            xdxw0051DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0051DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0051DataRespDto到xdxw0051DataResultDto中
        xdxw0051DataResultDto.setData(xdxw0051DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051DataResultDto));
        return xdxw0051DataResultDto;
    }
}
