/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRstService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 09:32:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRstService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestRstMapper lmtSigInvestRstMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtSigInvestBasicLmtRstService lmtSigInvestBasicLmtRstService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRst selectByPrimaryKey(String pkId) {
        return lmtSigInvestRstMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRst> selectAll(QueryModel model) {
        List<LmtSigInvestRst> records = (List<LmtSigInvestRst>) lmtSigInvestRstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRst> list = lmtSigInvestRstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestRst record) {
        return lmtSigInvestRstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRst record) {
        return lmtSigInvestRstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRst record) {
        return lmtSigInvestRstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRst record) {
        return lmtSigInvestRstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRstMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号获取批复记录
     * @param serno
     * @return
     */
    public LmtSigInvestRst selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.setSize(1);
        queryModel.setPage(1);
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestRst> lmtSigInvestRsts = selectByModel(queryModel);
        if (lmtSigInvestRsts!=null && lmtSigInvestRsts.size()>0){
            return lmtSigInvestRsts.get(0);
        }
        return null;
    }


    /**
     * 根据批复流水号查询同业授信批复信息
     * @param replySerno
     * @return
     */
    public LmtSigInvestRst selectByReplySerno(String replySerno){
        return lmtSigInvestRstMapper.selectByReplySerno(replySerno);
    }



    /**
     * @方法名称: 初始化单笔投资授信批复表数据信息 lmt_sig_invest_rst
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestRst initLmtSigInvestRstInfo(LmtSigInvestAppr entityBean){
        //如果是授信变更
        LmtSigInvestRst lmtSigInvestRst = new LmtSigInvestRst() ;
        //拷贝数据
        BeanUtils.copyProperties(entityBean, lmtSigInvestRst);
        //主键
        lmtSigInvestRst.setPkId(generatePkId());
        //批复编号
        lmtSigInvestRst.setReplySerno(generateSerno(SeqConstant.INVEST_LMT_REPLY_SEQ)) ;
        //终身机构
        lmtSigInvestRst.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //批复状态
        lmtSigInvestRst.setReplyStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //采用评审结论
        String apprResult = Optional.ofNullable(entityBean.getReviewResult()).orElse(entityBean.getReviewResultZh());
        lmtSigInvestRst.setApprResult(apprResult);
        //批复生效日期(使用openDay---by lixy)
        lmtSigInvestRst.setReplyInureDate(getCurrrentDateStr());
        //最新更新日期
        lmtSigInvestRst.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        //创建日期
        lmtSigInvestRst.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestRst.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestRst ;
    }

    /**
     * 初始化单笔投资授信批复表数据信息(批复变更)
     * @param lmtSigInvestChgReply
     * @return
     */
    public LmtSigInvestRst initLmtSigInvestRstInfoByChgReply(LmtSigInvestChgReply lmtSigInvestChgReply) {
        //如果是授信变更
        User user = SessionUtils.getUserInformation();
        LmtSigInvestRst lmtSigInvestRst = new LmtSigInvestRst() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtSigInvestChgReply,lmtSigInvestRst);
        //主键
        lmtSigInvestRst.setPkId(generatePkId());
        //批复编号
        lmtSigInvestRst.setReplySerno(generateSerno(CmisLmtConstants.LMT_SERNO)) ;
        //终身机构
        lmtSigInvestRst.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
 /*       //审批模式 TODO：不知道怎么取
        lmtSigInvestRst.setApprMode("");
        //审批结论 TODO：不知道怎么取
        lmtSigInvestRst.setApprResult("");*/
        //批复状态
        lmtSigInvestRst.setReplyStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //批复生效日期
        lmtSigInvestRst.setReplyInureDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        //最新更新日期
        lmtSigInvestRst.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        //创建日期
        lmtSigInvestRst.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestRst.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestRst ;
    }

    /**
     * @方法名称：queryLmtSigInvestRstDataByParams
     * @方法描述：通过参数查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangjw
     * @创建时间：2021-07-22
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtSigInvestRst> queryLmtSigInvestRstDataByParams(HashMap<String, String> paramMap) {
        return lmtSigInvestRstMapper.queryLmtSigInvestRstDataByParams(paramMap);
    }

    /**
     * @方法名称: selectSigInvestInvestType
     * @方法描述: 查询符合条件的债券投资授信批复，可以将其纳入进债券池额度内
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtSigInvestRst> selectSigInvestInvestType(QueryModel model) {
        List<LmtSigInvestRst> list = lmtSigInvestRstMapper.selectSigInvestInvestType(model);
        return list;
    }


    /**
     * @方法名称:
     * @方法描述: 终止批复
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: zhangjw
     * @创建时间: 2021-07-22
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void invalidatedOldAppr(String origiLmtReplySerno) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("replySerno", origiLmtReplySerno);
        queryMap.put("replyStatus", CmisBizConstants.STD_REPLY_STATUS_01);
        List<LmtSigInvestRst> lmtSigInvestRstList = this.queryLmtSigInvestRstDataByParams(queryMap);
        if (lmtSigInvestRstList != null && lmtSigInvestRstList.size() == 1) {
            LmtSigInvestRst oldLmtSigInvestRst = lmtSigInvestRstList.get(0);
            oldLmtSigInvestRst.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_02);
            this.update(oldLmtSigInvestRst);

            //终止原批复关联底层台账
            QueryModel model = new QueryModel();
            model.addCondition("replySerno", origiLmtReplySerno);
            BizInvestCommonService.checkParamsIsNull("replySerno",origiLmtReplySerno);
            model.addCondition("replyStatus", CmisBizConstants.STD_REPLY_STATUS_01);
            List<LmtSigInvestBasicLmtRst> list = lmtSigInvestBasicLmtRstService.selectByModel(model);
            if(list!=null && list.size()>0){
                for(LmtSigInvestBasicLmtRst oldLmtSigInvestBasicLmtRst : list){
                    oldLmtSigInvestBasicLmtRst.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_02);
                    lmtSigInvestBasicLmtRstService.update(oldLmtSigInvestBasicLmtRst);
                }
            }
        } else {
            throw new Exception("查询原批复数据异常！");
        }
    }

    /**
     * 更新资产编号
     * @param replySerno
     * @param assetNo
     */
    public void updateAssetNo(String replySerno, String assetNo) {
        LmtSigInvestRst lmtSigInvestRst = selectByReplySerno(replySerno);
        if (lmtSigInvestRst != null){
            lmtSigInvestRst.setAssetNo(assetNo);
            update(lmtSigInvestRst);
        }
    }
}
