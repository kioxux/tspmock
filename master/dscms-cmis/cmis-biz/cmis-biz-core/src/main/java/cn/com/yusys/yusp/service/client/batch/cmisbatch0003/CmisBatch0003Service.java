package cn.com.yusys.yusp.service.client.batch.cmisbatch0003;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBatch0003Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0003Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param cmisbatch0003ReqDto
     * @return
     */
    @Transactional
    public Cmisbatch0003RespDto cmisbatch0003(Cmisbatch0003ReqDto cmisbatch0003ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003ReqDto));
        ResultDto<Cmisbatch0003RespDto> cmisbatch0003ResultDto = cmisBatchClientService.cmisbatch0003(cmisbatch0003ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003ResultDto));

        String cmisbatch0003Code = Optional.ofNullable(cmisbatch0003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisbatch0003Meesage = Optional.ofNullable(cmisbatch0003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Cmisbatch0003RespDto cmisbatch0003RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0003ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbatch0003RespDto = cmisbatch0003ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, cmisbatch0003Code, cmisbatch0003Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value);
        return cmisbatch0003RespDto;
    }
}
