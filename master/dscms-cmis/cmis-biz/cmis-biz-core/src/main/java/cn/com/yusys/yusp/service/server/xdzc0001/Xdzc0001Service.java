package cn.com.yusys.yusp.service.server.xdzc0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.dto.server.xdzc0001.req.Xdzc0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.resp.List;
import cn.com.yusys.yusp.dto.server.xdzc0001.resp.Xdzc0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAsplDetailsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Objects;
import java.util.stream.Collectors;
/**
 * 接口处理类:资产池协议列表查询接口
 * @Author xs
 * @Date 2021/06/01 16:20
 * @Version 1.0
 */
@Service
public class Xdzc0001Service {
    @Autowired
    private CtrAsplDetailsMapper ctrAsplDetailsMapper;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0001.Xdzc0001Service.class);

    @Transactional
    public Xdzc0001DataRespDto xdzc0001Service(Xdzc0001DataReqDto xdzc0001DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value);
        Xdzc0001DataRespDto xdzc0001DataRespDto = new Xdzc0001DataRespDto();
        String contNo = xdzc0001DataReqDto.getContNo();//协议编号
        String cusId = xdzc0001DataReqDto.getCusId();//客户编号
        String contStatus = xdzc0001DataReqDto.getContStatus();//协议状态
        int page = xdzc0001DataReqDto.getPage();//页数
        int size = xdzc0001DataReqDto.getSize();//单页大小
        try {
            if (StringUtil.isEmpty(cusId)) {
                throw BizException.error(null, "9999", "客户编号不能为空");
            }
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("contNo", contNo);
            queryModel.addCondition("contStatus", contStatus);
            PageHelper.startPage(page, size);
            java.util.List<CtrAsplDetails> ctrAsplDetailist = ctrAsplDetailsMapper.selectByModel(queryModel);
            PageInfo<CtrAsplDetails> pageinfo = new PageInfo<>(ctrAsplDetailist);
            long count = pageinfo.getTotal();
            PageHelper.clearPage();// 如果查询数据为空
            if(CollectionUtils.isEmpty(ctrAsplDetailist)){
                xdzc0001DataRespDto.setTotal(count);// 总数
                xdzc0001DataRespDto.setList(new java.util.ArrayList<List>());//list
                return xdzc0001DataRespDto;
            }else {
                if (Objects.equals(CmisCommonConstants.CONT_STATUS_200, contStatus)) {
                    // 判断是否迁移数据，需要拦截重新签订协议
                    if (Objects.equals(ctrAsplDetailist.get(0).getContNo().substring(0, 3), "PJC")) {
                        throw BizException.error("9999", "协议合同需要重新签订,资产池：" + contNo);
                    } else {
                        java.util.List<List> lists = ctrAsplDetailist.parallelStream().map(e -> {
                            List list = new List();
                            list.setContNo(e.getContNo());//协议编号
                            list.setPrdId(e.getPrdId());//产品编号
                            list.setPrdName(e.getPrdName());//产品名称
                            list.setCusId(e.getCusId());//客户编号
                            list.setCusName(e.getCusName());//客户名称
                            list.setContAmt(e.getContAmt());//协议金额
                            list.setStartDate(e.getStartDate());//协议起始日期
                            list.setEndDate(e.getEndDate());//协议到期日期
                            list.setContStatus(e.getContStatus());//协议状态
                            list.setManagerId(e.getManagerId());//主管客户经理
                            list.setManagerBrId(e.getManagerBrId());//主管机构
                            return list;
                        }).collect(Collectors.toList());
                        xdzc0001DataRespDto.setTotal(count);// 总数
                        xdzc0001DataRespDto.setList(lists);//list
                    }
                }else{
                    java.util.List<List> lists = ctrAsplDetailist.parallelStream().map(e -> {
                        List list = new List();
                        list.setContNo(e.getContNo());//协议编号
                        list.setPrdId(e.getPrdId());//产品编号
                        list.setPrdName(e.getPrdName());//产品名称
                        list.setCusId(e.getCusId());//客户编号
                        list.setCusName(e.getCusName());//客户名称
                        list.setContAmt(e.getContAmt());//协议金额
                        list.setStartDate(e.getStartDate());//协议起始日期
                        list.setEndDate(e.getEndDate());//协议到期日期
                        list.setContStatus(e.getContStatus());//协议状态
                        list.setManagerId(e.getManagerId());//主管客户经理
                        list.setManagerBrId(e.getManagerBrId());//主管机构
                        return list;
                    }).collect(Collectors.toList());
                    xdzc0001DataRespDto.setTotal(count);// 总数
                    xdzc0001DataRespDto.setList(lists);//list
                }
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value);
        return xdzc0001DataRespDto;
    }


}
