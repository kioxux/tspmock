package cn.com.yusys.yusp.web.server.xdtz0049;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0049.req.Xdtz0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.resp.Xdtz0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0049.Xdtz0049Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户贷款信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0049:客户贷款信息查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0049Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0049Resource.class);

    @Autowired
    private Xdtz0049Service xdtz0049Service;
    /**
     * 交易码：xdtz0049
     * 交易描述：客户贷款信息查询
     *
     * @param xdtz0049DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户贷款信息查询")
    @PostMapping("/xdtz0049")
    protected @ResponseBody
    ResultDto<Xdtz0049DataRespDto> xdtz0049(@Validated @RequestBody Xdtz0049DataReqDto xdtz0049DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049DataReqDto));
        Xdtz0049DataRespDto xdtz0049DataRespDto = new Xdtz0049DataRespDto();// 响应Dto:客户贷款信息查询
        ResultDto<Xdtz0049DataRespDto> xdtz0049DataResultDto = new ResultDto<>();
        String certNo = xdtz0049DataReqDto.getCertNo();//证件号
        try {
            // 从xdtz0049DataReqDto获取业务值进行业务逻辑处理
            xdtz0049DataRespDto = xdtz0049Service.xdtz0049(xdtz0049DataReqDto);

            // 封装xdtz0049DataResultDto中正确的返回码和返回信息
            xdtz0049DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0049DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, e.getMessage());
            // 封装xdtz0049DataResultDto中异常返回码和返回信息
            xdtz0049DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0049DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0049DataRespDto到xdtz0049DataResultDto中
        xdtz0049DataResultDto.setData(xdtz0049DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049DataResultDto));
        return xdtz0049DataResultDto;
    }
}
