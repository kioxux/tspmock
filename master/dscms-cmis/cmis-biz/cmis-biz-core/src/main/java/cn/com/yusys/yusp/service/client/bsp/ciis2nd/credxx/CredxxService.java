package cn.com.yusys.yusp.service.client.bsp.ciis2nd.credxx;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2Ciis2ndClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CredxxService {
    private static final Logger logger = LoggerFactory.getLogger(CredxxService.class);

    // 1）注入：BSP封装调用二代征信系统的接口处理类（credxx）
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    /**
     * 业务逻辑处理方法：线下查询接口
     *
     * @param credxxReqDto
     * @return
     */
    @Transactional
    public CredxxRespDto credxx(CredxxReqDto credxxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxReqDto));
        ResultDto<CredxxRespDto> credxxResultDto = dscms2Ciis2ndClientService.credxx(credxxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxResultDto));
        String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CredxxRespDto credxxRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
            //  获取相关的值并解析
            credxxRespDto = credxxResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, credxxCode,credxxMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
        return credxxRespDto;
    }


}
