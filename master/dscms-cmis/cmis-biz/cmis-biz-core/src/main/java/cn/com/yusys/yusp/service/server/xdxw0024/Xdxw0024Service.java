package cn.com.yusys.yusp.service.server.xdxw0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0024.req.List;
import cn.com.yusys.yusp.dto.server.xdxw0024.req.Xdxw0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0024.resp.Xdxw0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0024Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0024Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0024Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CommonService commonService;

    /**
     * 查询是否有信贷记录
     *
     * @param xdxw0024DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0024DataRespDto getXdxw0024(Xdxw0024DataReqDto xdxw0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataReqDto));
        Xdxw0024DataRespDto xdtz0024DataRespDto = new Xdxw0024DataRespDto();
        try {
            String certNo = xdxw0024DataReqDto.getCertNo();
            String[] list = certNo.split(",");
            java.util.List<String> certNos = Arrays.asList(list);
            java.util.List<String> cusIds = commonService.getCusIdsByCertNos(certNos);
            int num = 0;
            if(CollectionUtils.nonEmpty(cusIds)){
                num = accLoanMapper.getHaveLoanBalByCusIdList(cusIds);
            }
            xdtz0024DataRespDto.setNum(num);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdtz0024DataRespDto));
        return xdtz0024DataRespDto;
    }

}
