package cn.com.yusys.yusp.service.server.xdsx0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAccInfo;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.req.Xdsx0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.resp.Xdsx0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAccInfoMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 接口处理类:担保公司合作协议查询
 *
 * @author xs
 * @version 1.0
 */
@Service
public class Xdsx0020Service {
    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;

    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdsx0020.Xdsx0020Service.class);

    /**
     * 专业贷款评级结果同步
     *
     * @param xdsx0020DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0020DataRespDto xdsx0020(Xdsx0020DataReqDto xdsx0020DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value);
        //返回对象
        Xdsx0020DataRespDto xdsx0020DataRespDto = new Xdsx0020DataRespDto();
        try {
            String contno = xdsx0020DataReqDto.getContno();//组织机构代码
            String cont_no1=contno.substring(0, 8);
            String cont_no2=contno.substring(8, 9);
            String cont_no3=cont_no1+"-"+cont_no2;
            // 获取cusId
            String cusId = "";
            // 跨服务 根据组织机构代码去对公客户信息表去查询cusId
            QueryModel queryModel = new QueryModel();
            ResultDto<List<CusCorpDto>> cusCorpDtoResult = null;
            //如果查询不到使用拼接后的组织机构代码
            queryModel.addCondition("insCode",contno);
            cusCorpDtoResult =  cmisCusClientService.selectCusCorpDtoList(queryModel);
            if(CollectionUtils.isEmpty(cusCorpDtoResult.getData())){
                queryModel.addCondition("insCode",cont_no3);
                cusCorpDtoResult = cmisCusClientService.selectCusCorpDtoList(queryModel);
            }
            CusCorpDto cusCorpDto = new CusCorpDto();
            cusCorpDto = JSON.parseObject(JSON.toJSONString(cusCorpDtoResult.getData().get(0)), new TypeReference<CusCorpDto>(){});
            cusId = cusCorpDto.getCusId();
            // 查询是否存在数据
            QueryModel newQueryModel = new QueryModel();
            newQueryModel.addCondition("partnerNo",cusId);
            List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfoList = coopPartnerAgrAccInfoMapper.selectByModel(newQueryModel);
            if(CollectionUtils.isEmpty(coopPartnerAgrAccInfoList)){
                return xdsx0020DataRespDto;
            }
            CoopPartnerAgrAccInfo coopPartnerAgrAccInfo= coopPartnerAgrAccInfoList.get(0);
            String serno = coopPartnerAgrAccInfo.getCoopPlanSerno();//合作方案申请流水号
            CoopPlanAccInfo coopPlanAccInfo = coopPlanAccInfoMapper.selectCoopPlanAccInfoBySerno(serno);
            if(coopPlanAccInfo == null){
                return xdsx0020DataRespDto;
            }
            xdsx0020DataRespDto.setCustid(coopPartnerAgrAccInfo.getPartnerNo());// 合作方客户号
            xdsx0020DataRespDto.setCustna(coopPartnerAgrAccInfo.getPartnerName());// 合作客户名称
            xdsx0020DataRespDto.setContno(coopPartnerAgrAccInfo.getCoopAgrNo());// 合作协议编号
            xdsx0020DataRespDto.setCnhtno("");// 中文合同编号
            xdsx0020DataRespDto.setDbhstp("");// 担保公司类型
            xdsx0020DataRespDto.setConamt(new BigDecimal(0L));// 协议金额
            xdsx0020DataRespDto.setStdate(coopPartnerAgrAccInfo.getCoopAgrStartDate());// 合作协议起始日期
            xdsx0020DataRespDto.setEddate(coopPartnerAgrAccInfo.getCoopAgrEndDate());// 合作协议到期日期
            xdsx0020DataRespDto.setSxxybh("");// 授信协议编号
            xdsx0020DataRespDto.setSxqsrq("");// 授信起始日期
            xdsx0020DataRespDto.setSxdqrq("");// 授信到期日期
            xdsx0020DataRespDto.setHzzeed(coopPlanAccInfo.getTotlCoopLmtAmt());// 合作总额度
            xdsx0020DataRespDto.setSigamt(coopPlanAccInfo.getSigBusiCoopQuota());// 单笔业务合作额度
            xdsx0020DataRespDto.setZxbhed(new BigDecimal(0L));// 在线保函额度
            xdsx0020DataRespDto.setZdseam(coopPlanAccInfo.getBailAccLowAmt());// 最低保证金金额
            xdsx0020DataRespDto.setSepert(coopPlanAccInfo.getBailPerc());// 保证金比例
            xdsx0020DataRespDto.setDhxeee(coopPlanAccInfo.getSingleCoopQuota());// 单户限额
            xdsx0020DataRespDto.setMagtim(coopPlanAccInfo.getOutguarMultiple());// 批复放大倍数
            xdsx0020DataRespDto.setXtcsbs(new BigDecimal(0L));// 系统测算倍数
            xdsx0020DataRespDto.setBzjdcd(new BigDecimal(coopPlanAccInfo.getSubpayGraper()));// 保证金代偿宽限期
            xdsx0020DataRespDto.setCostat(coopPartnerAgrAccInfo.getAgrStatus());// 协议状态
            xdsx0020DataRespDto.setBzjacc(coopPlanAccInfo.getBailAccNo());// 保证金账号
            xdsx0020DataRespDto.setInptid(coopPartnerAgrAccInfo.getInputId());// 登记人
            xdsx0020DataRespDto.setMangid(coopPartnerAgrAccInfo.getManagerId());// 责任人
            xdsx0020DataRespDto.setInbrid(coopPartnerAgrAccInfo.getInputBrId());// 登记人机构
            xdsx0020DataRespDto.setMabrid(coopPartnerAgrAccInfo.getManagerBrId());// 责任人机构
            xdsx0020DataRespDto.setIndate(coopPartnerAgrAccInfo.getUpdDate());// 登记日期
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value);
        return xdsx0020DataRespDto;
    }
}
