package cn.com.yusys.yusp.web.server.xdzc0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.resp.Xdzc0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0007.Xdzc0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池出池接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0007:资产池出池接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0007Resource.class);

    @Autowired
    private Xdzc0007Service xdzc0007Service;
    /**
     * 交易码：xdzc0007
     * 交易描述：资产池出池接口
     *
     * @param xdzc0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出池接口")
    @PostMapping("/xdzc0007")
    protected @ResponseBody
    ResultDto<Xdzc0007DataRespDto> xdzc0007(@Validated @RequestBody Xdzc0007DataReqDto xdzc0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007DataReqDto));
        Xdzc0007DataRespDto xdzc0007DataRespDto = new Xdzc0007DataRespDto();// 响应Dto:资产池出池接口
        ResultDto<Xdzc0007DataRespDto> xdzc0007DataResultDto = new ResultDto<>();
        try {
            xdzc0007DataRespDto = xdzc0007Service.xdzc0007Service(xdzc0007DataReqDto);
            xdzc0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, e.getMessage());
            // 封装xdzc0007DataResultDto中异常返回码和返回信息
            xdzc0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0007DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0007DataRespDto到xdzc0007DataResultDto中
        xdzc0007DataResultDto.setData(xdzc0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007DataResultDto));
        return xdzc0007DataResultDto;
    }
}
