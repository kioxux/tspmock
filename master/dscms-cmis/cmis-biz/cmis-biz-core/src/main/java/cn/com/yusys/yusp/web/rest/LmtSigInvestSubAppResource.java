/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.service.LmtSigInvestBasicLimitAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestSubApp;
import cn.com.yusys.yusp.service.LmtSigInvestSubAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestSubAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 14:59:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestsubapp")
public class LmtSigInvestSubAppResource {
    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService;

    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestSubApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestSubApp> list = lmtSigInvestSubAppService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestSubApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestSubApp>> index(QueryModel queryModel) {
        List<LmtSigInvestSubApp> list = lmtSigInvestSubAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestSubApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestSubApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestSubApp> list = lmtSigInvestSubAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestSubApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestSubApp> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestSubApp lmtSigInvestSubApp = lmtSigInvestSubAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestSubApp>(lmtSigInvestSubApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestSubApp> create(@RequestBody LmtSigInvestSubApp lmtSigInvestSubApp) throws URISyntaxException {
        lmtSigInvestSubAppService.insert(lmtSigInvestSubApp);
        return new ResultDto<LmtSigInvestSubApp>(lmtSigInvestSubApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestSubApp lmtSigInvestSubApp) throws URISyntaxException {
        int result = lmtSigInvestSubAppService.update(lmtSigInvestSubApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestSubAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestSubAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *
     * @param condition
     * @return
     */
    @PostMapping("/updateGuarDescExt")
    protected ResultDto<Integer> updateGuarDescExt(@RequestBody Map condition){
        String content = (String) condition.get("guarDescExt");//GUAR_DESC_EXT
        String serno = (String) condition.get("serno");
        int result = lmtSigInvestSubAppService.updateGuarDescExt(serno, content);
        //增加底层资产 增信更新
        //lmt_sig_invest_basic_limit_app
        if (result == 0){
            result = lmtSigInvestBasicLimitAppService.updateGuarDescExt(serno, content);
        }
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/getGuarDescExt")
    public ResultDto<String> getGuarDescExt(@RequestBody String serno){
        String guarDescExt = "";
        LmtSigInvestSubApp lmtSigInvestSubApp = lmtSigInvestSubAppService.selectBySerno(serno);
        if (lmtSigInvestSubApp != null) {
            guarDescExt = lmtSigInvestSubApp.getGuarDescExt();
        }else{
            LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp = lmtSigInvestBasicLimitAppService.selectByBasicSerno(serno);
            if (lmtSigInvestBasicLimitApp!=null){
                guarDescExt = lmtSigInvestBasicLimitApp.getGuarDescExt();
            }
        }
        return new ResultDto<String>(guarDescExt);
    }

    /**
     * 根据流水号查询企业基本信息从表
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtSigInvestSubApp> selectBySerno(@RequestBody Map map){
        String serno = (String) map.get("serno");
        LmtSigInvestSubApp subApp = lmtSigInvestSubAppService.selectBySerno(serno);
        return new ResultDto<>(subApp);
    }
}
