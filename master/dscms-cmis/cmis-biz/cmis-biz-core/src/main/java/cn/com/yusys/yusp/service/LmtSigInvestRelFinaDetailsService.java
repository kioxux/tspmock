/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaDetails;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRelFinaDetailsMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaDetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 15:09:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRelFinaDetailsService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestRelFinaDetailsMapper lmtSigInvestRelFinaDetailsMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRelFinaDetails selectByPrimaryKey(String pkId) {
        return lmtSigInvestRelFinaDetailsMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRelFinaDetails> selectAll(QueryModel model) {
        List<LmtSigInvestRelFinaDetails> records = (List<LmtSigInvestRelFinaDetails>) lmtSigInvestRelFinaDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRelFinaDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRelFinaDetails> list = lmtSigInvestRelFinaDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestRelFinaDetails record) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        record.setPkId(generatePkId());
        record.setInputBrId(getCurrentUser().getOrg().getCode());
        record.setInputDate(openDay);
        record.setInputId(getCurrentUser().getLoginCode());
        record.setUpdateTime(new Date());
        record.setCreateTime(new Date());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdDate(openDay);
        record.setUpdId(getCurrentUser().getLoginCode());
        return lmtSigInvestRelFinaDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRelFinaDetails record) {
        return lmtSigInvestRelFinaDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRelFinaDetails record) {
        return lmtSigInvestRelFinaDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRelFinaDetails record) {
        return lmtSigInvestRelFinaDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */

    public boolean addOrUpdateAllTable(List<LmtSigInvestRelFinaDetails> list) {
        for(LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetails : list){
            // 判断当前行是否已存在
            LmtSigInvestRelFinaDetails record = selectByPrimaryKey(lmtSigInvestRelFinaDetails.getPkId());
            if(record != null){
                // 更新操作
                User userInfo = SessionUtils.getUserInformation();
                lmtSigInvestRelFinaDetails.setUpdId(userInfo.getLoginCode());
                lmtSigInvestRelFinaDetails.setUpdBrId(userInfo.getOrg().getCode());
                lmtSigInvestRelFinaDetails.setUpdDate(getCurrrentDateStr());
                lmtSigInvestRelFinaDetails.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                updateSelective(lmtSigInvestRelFinaDetails);
            }else{
                // 新增操作
                lmtSigInvestRelFinaDetails.setPkId(generatePkId());
                insertSelective(lmtSigInvestRelFinaDetails);
            }
        }
        return true;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRelFinaDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRelFinaDetailsMapper.deleteByIds(ids);
    }

    /**
     * 逻辑删除
     * @param pkId
     * @return
     */
    public int deleteLogicByPkId(String pkId) {
        LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetails1 = selectByPrimaryKey(pkId);
        if (lmtSigInvestRelFinaDetails1!= null ){
            lmtSigInvestRelFinaDetails1.setOprType(CmisBizConstants.OPR_TYPE_02);//逻辑删除
            return update(lmtSigInvestRelFinaDetails1);
        }
        return 0;
    }
}
