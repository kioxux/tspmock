/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.Fb1150ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.resp.Fb1150RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.req.Wxp002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.LmtAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtChgDetailMapper;
import cn.com.yusys.yusp.repository.mapper.LmtGrpMemRelMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSxkdPlusFkspMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.util.MapUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:34:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Resource
    private LmtAppMapper lmtAppMapper;

    // 单一客户授信审批
    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtChgDetailService lmtChgDetailService;

    // 单一客户授信批复
    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtGrpApprService lmtGrpApprService;

    @Resource
    private LmtChgDetailMapper lmtChgDetailMapper;

    // 单一客户批复台账服务
    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtGrpMemRelMapper lmtGrpMemRelMapper;

    // 集团授信成员关联服务
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService; // 集团授信

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private ICusClientService iCusClientService;

    // 系统参数配置服务
    @Resource
    private AdminSmPropService adminSmPropService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private AccDiscService accDiscService;

    @Autowired
    private AccTfLocService accTfLocService;

    @Autowired
    private LmtLadEvalService lmtLadEvalService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private OtherCnyRateAppService otherCnyRateAppService;// 人民币定价

    @Autowired
    private OtherForRateAppService otherForRateAppService;// 外币

    @Autowired
    private OtherDiscPerferRateAppService otherDiscPerferRateAppService; // 贴现优惠利率申请

    @Autowired
    private OtherRecordAccpSignOfBocAppService otherRecordAccpSignOfBocAppService;// 中行代签电票申请

    @Autowired
    private OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;// 特殊贷款用信备案申请

    @Autowired
    private OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;// 授信抵质押物价值认定申请

    @Autowired
    private OtherItemAppService otherItemAppService;// 其他

    @Autowired
    private LmtOtherAppRelService lmtOtherAppRelService;// 其他事项申报授信关系

    @Autowired
    private OtherBailDepPreferRateAppService otherBailDepPreferRateAppService;// 保证金存款特惠利率申请

    @Autowired
    private OtherAccpPerferFeeAppService otherAccpPerferFeeAppService;// 银票手续费优惠申请表

    @Autowired
    private OtherRecordAccpSignPlanAppService otherRecordAccpSignPlanAppService;// 银票签发业务计划申请表

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    // 测算数据
    @Autowired
    private LmtHighCurfundEvalService lmtHighCurfundEvalService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private RptSpdAnysSxkdService rptSpdAnysSxkdService;

    @Autowired
    private RptBasicInfoAssoService rptBasicInfoAssoService;
    @Autowired
    private Dscms2QywxClientService dscms2QywxClientService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;// 额度系统

    @Autowired
    private LmtSxkdPlusFkspMapper lmtSxkdPlusFkspMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private LmtSubPrdLowGuarRelService lmtSubPrdLowGuarRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;//银承合同

    @Autowired
    private IqpDiscAppService iqpDiscAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private Dscms2WxClientService dscms2WxClientService;

    @Autowired
    private MessageCommonService sendMessage;

    @Autowired
    private CoopPlanAptiInfoService coopPlanAptiInfoService;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtApp selectByPrimaryKey(String pkId) {
        return lmtAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtApp> selectAll(QueryModel model) {
        List<LmtApp> records = (List<LmtApp>) lmtAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtApp record) {
        record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        return lmtAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtApp record) {
        return lmtAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(LmtApp record) {
        return lmtAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtApp record) {
        return lmtAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public Map deleteByPkId(LmtApp record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            String lmtType = record.getLmtType();
            String serno = record.getSerno();
            String approveStatus = record.getApproveStatus();
            if (CmisCommonConstants.WF_STATUS_992.equals(approveStatus)) {
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                int exit = lmtAppMapper.updateByPrimaryKeySelective(record);
                ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(serno);
                if (exit == 1) {
                    return rtnData;
                } else {
                    rtnCode = EpbEnum.EPB099999.key;
                    rtnMsg = EpbEnum.EPB099999.value;
                    return rtnData;
                }
            }
            int count = lmtAppMapper.updateByPkId(record);
            boolean resultSub = lmtAppSubService.updateBySerno(serno);
            if (!resultSub) {
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除授信分项信息失败！");
            }
            if ("02".equals(lmtType)) {
                boolean resultDetail = lmtChgDetailService.updateBySerno(serno);
                if (!resultDetail) {
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除变更从表信息失败！");
                }
            }
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除授信申请情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: getLmtTypeByCusId
     * @方法描述: 根据客户号查询客户的授信台账情况，
     * @参数与返回说明:
     * @算法描述: :帅
     * 1、授信到期后一年内（并且当前授信下用信余额为0）----- 续作
     * 2、授信到期后一年内（并且当前授信下用信余额不为0）------ 续作
     * 3、授信到期后一年之后并且当前授信下用信余额为0，------ 新增
     * 4、授信到期后一年之后并且当前授信下用信余额不为0，------ 续作
     * :雯
     * 1.授信到期后一年内，无需调用额度接口，发起的授信类型直接默认为授信续作
     * 2.授信到期后一年，调用额度接口，判断额度是否存在失效未结清的综合授信，若存则，则为授信续作；若不存在，则为授信新增
     * @创建人: mashun1
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map getLmtTypeByCusId(String cusId) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String lmtType = "";
        try {
            if (StringUtils.isBlank(cusId)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            // 根据客户号获取最新的一笔授信台账
            Map queryMap = new HashMap();
            queryMap.put("cusId",cusId);
            queryMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
            LmtReplyAcc lastLmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(queryMap);
            if (lastLmtReplyAcc != null && lastLmtReplyAcc.getPkId() != null) {
                Date inureDate = DateUtils.parseDateByDef(lastLmtReplyAcc.getInureDate());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                // 获取营业日期
                Date today = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
                // Date today = DateUtils.getCurrDate();
                //授信期限
                int lmtTerm = lastLmtReplyAcc.getLmtTerm();
                //台账生效日期到期日期
                Date dueDate = DateUtils.addMonth(DateUtils.addMonth(inureDate, lmtTerm), 12);
                // 如果台账生效日期到期后加一年大于当前日期 则认为是授信续作
                if (today.compareTo(dueDate) <= 0) {
                    log.info("台账生效日期距当前时间未超过一年,执行续作操作!");
                    lmtType = CmisCommonConstants.LMT_TYPE_03;
                } else {
                    // 续作判断处理舍弃,直接调额度系统查询
                    log.info(String.format("根据客户号%s,调用额度接口CmisLmt0019ReqDto判断新增续作逻辑!", cusId));
                    //调判断接口，判断客户是否存在综合授信额度
                    CmisLmt0019ReqDto cmisLmt0019ReqDto = new CmisLmt0019ReqDto();
                    User userInfo = SessionUtils.getUserInformation();
                    cmisLmt0019ReqDto.setInstuCde(CmisCommonUtils.getInstucde(userInfo.getOrg().getCode()));
                    cmisLmt0019ReqDto.setCusId(cusId);
                    log.info("判断客户："+cusId+" 判断新增续作逻辑开始，请求报文："+JSON.toJSONString(cmisLmt0019ReqDto));
                    ResultDto<CmisLmt0019RespDto> resultDto = cmisLmtClientService.cmisLmt0019(cmisLmt0019ReqDto);
                    log.info("判断客户："+cusId+" 判断新增续作逻辑结束，响应报文："+JSON.toJSONString(resultDto));
                    if (!"0".equals(resultDto.getCode())) {
                        // 接口调用判断
                        log.error("判断客户" + cusId + "是否存在综合授信额度接口异常！");
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    String code = resultDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        // 额度数据校验
                        log.error("判断客户" + cusId + "是否存在综合授信额度接口异常！");
                        throw BizException.error(null, resultDto.getData().getErrorCode(), resultDto.getData().getErrorMsg());
                    }
                    String result = resultDto.getData().getResult();
                    if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(result)) {
                        lmtType = CmisCommonConstants.LMT_TYPE_01;
                    } else {
                        lmtType = CmisCommonConstants.LMT_TYPE_03;
                    }
                }
            } else {
                lmtType = CmisCommonConstants.LMT_TYPE_01;
            }




            // 根据客户号获取最新的一笔授信台账
//            Map queryMap = new HashMap();
//            queryMap.put("cusId",cusId);
//            queryMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
//            LmtReplyAcc lastLmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(queryMap);
//            if (lastLmtReplyAcc != null && lastLmtReplyAcc.getPkId() != null) {
//                Date inureDate = DateUtils.parseDateByDef(lastLmtReplyAcc.getInureDate());
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                // 获取营业日期
//                Date today = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
//                // Date today = DateUtils.getCurrDate();
//                //授信期限
//                int lmtTerm = lastLmtReplyAcc.getLmtTerm();
//                //台账生效日期到期日期
//                Date dueDate = DateUtils.addMonth(DateUtils.addMonth(inureDate, lmtTerm), 12);
//                // 如果台账生效日期到期后加一年大于当前日期 则认为是授信续作
//                if (today.compareTo(dueDate) <= 0) {
//                    log.info("台账生效日期距当前时间未超过一年,执行续作操作!");
//                    lmtType = CmisCommonConstants.LMT_TYPE_03;
//                } else {
//                    log.info("台账生效日期+期限距当前时间超过一年,查询当前客户台账分项信息,台账编号:" + lastLmtReplyAcc.getAccNo());
//                    // 调用额度接口，判断额度是否存在失效未结清的综合授信，若存则，则为授信续作；若不存在，则为授信新增
//                    CmisLmt0047ReqDto cmisLmt0047ReqDto = new CmisLmt0047ReqDto();
//                    List<LmtReplyAccSub> list = lmtReplyAccSubService.selectByAccNo(lastLmtReplyAcc.getAccNo());
//                    cmisLmt0047ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_01);
//                    List<CmisLmt0047LmtSubDtoList> lmtSubDtoList = new ArrayList<>();
//                    if (!list.isEmpty() && list.size() > 0) {
//                        for (LmtReplyAccSub lmtReplyAccSub : list) {
//                            log.info("发送额度系统校验分项下是否存在未结清的业务,分项编号:" + lmtReplyAccSub.getAccSubNo());
//                            CmisLmt0047LmtSubDtoList cmisLmt0047LmtSubDtoList = new CmisLmt0047LmtSubDtoList();
//                            cmisLmt0047LmtSubDtoList.setAccSubNo(lmtReplyAccSub.getAccSubNo());
//                            lmtSubDtoList.add(cmisLmt0047LmtSubDtoList);
//                        }
//                        cmisLmt0047ReqDto.setLmtSubDtoList(lmtSubDtoList);
//                        log.info("发送额度系统校验----------start-----------------,请求报文{}", JSON.toJSONString(cmisLmt0047ReqDto));
//                        ResultDto<CmisLmt0047RespDto> cmisLmt0047RespDto = cmisLmtClientService.cmislmt0047(cmisLmt0047ReqDto);
//                        log.info("--------------响应报文{}", JSON.toJSONString(cmisLmt0047RespDto));
//                        if (cmisLmt0047RespDto != null && cmisLmt0047RespDto.getData() != null && "0000".equals(cmisLmt0047RespDto.getData().getErrorCode())) {
//                            log.info("发送额度系统校验成功!");
//                            List<CmisLmt0047ContRelDtoList> cmisLmt0047ContRelDtoLists = cmisLmt0047RespDto.getData().getContRelDtoList();
//                            if (!cmisLmt0047ContRelDtoLists.isEmpty() && cmisLmt0047ContRelDtoLists.size() > 0) {
//                                log.info("当前客户授信项下存在未结清的业务,执行续作操作!");
//                                lmtType = CmisCommonConstants.LMT_TYPE_03;
//                            } else {
//                                log.info("当前客户授信项下不存在未结清的业务,执行新增操作!");
//                                lmtType = CmisCommonConstants.LMT_TYPE_01;
//                            }
//                        } else {
//                            throw new Exception("同步额度系统异常");
//                        }
//                        log.info("发送额度系统校验----------end-----------------");
//                    } else {
//                        log.info("未查到当前台账项下分项信息,执行续作操作!");
//                        lmtType = CmisCommonConstants.LMT_TYPE_03;
//                    }
//                }
//            } else {
//                lmtType = CmisCommonConstants.LMT_TYPE_01;
//            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("根据客户号查询客户的授信台账情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtType", lmtType);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: judgeIsExistOnWayApp
     * @方法描述: 根据客户号查询客户是否存在在途的授信申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun1
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public boolean judgeIsExistOnWayApp(String cusId) {
        boolean flg = false;
        HashMap paramMap = new HashMap<String, Object>();
        paramMap.put("cusId", cusId);
        paramMap.put("oprType", CmisCommonConstants.ADD_OPR);
        paramMap.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
        List<LmtApp> lmtAppList = this.selectByParams(paramMap);
        if (lmtAppList.size() > 0) {
            // 如果是集团的 需要重新判断是否参与申报
            for (LmtApp lmtApp : lmtAppList) {
                if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                    LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtApp.getSerno());
                    if (lmtGrpMemRel != null) {
                        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(lmtGrpMemRel.getGrpSerno());
                        if (lmtGrpApp != null) {
                            if (CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpApp.getLmtType())) { // 变更
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                                    // 不填报时,不校验当前成员客户
                                    flg = false;
                                    continue;
                                } else {
                                    flg = true;
                                    break;
                                }
                            } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpApp.getLmtType())) { // 预授信细化
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                } else {
                                    flg = true;
                                    break;
                                }
                            } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpApp.getLmtType())) { // 额度调剂
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                } else {
                                    flg = true;
                                    break;
                                }
                            } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                } else {
                                    flg = true;
                                    break;
                                }
                            }
                        } else {
                            flg = false;
                            continue;
                        }
                    } else {
                        flg = false;
                        continue;
                    }
                } else {
                    flg = true;
                    break;
                }
            }
        } else {
            return flg;
        }
        return flg;
    }

    /**
     * @方法名称: selectByCusIdInApping
     * @方法描述: 根据客户号查询单一授信
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-19 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtApp selectByCusIdInApping(String cusId, String isGrp) {
        LmtApp lmtApp = new LmtApp();
        HashMap paramMap = new HashMap<String, Object>();
        paramMap.put("cusId", cusId);
        paramMap.put("oprType", CmisCommonConstants.ADD_OPR);
        paramMap.put("isGrp", isGrp);
        paramMap.put("sort", "UPD_DATE desc");
        List<LmtApp> lmtAppList = this.selectByParams(paramMap);
        if (!lmtAppList.isEmpty() && lmtAppList.size() > 0) {
            lmtApp = lmtAppList.get(0);
        }
        return lmtApp;
    }

    /**
     * @方法名称: saveLmtApp
     * @方法描述: 根据前台传入表单数据保存授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun1
     * @创建时间: 2021-04-09 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map saveLmtApp(LmtApp lmtApp, String isGrp) {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String cusId = "";
        String lmtType = "";
        try {
            cusId = lmtApp.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            // TODO 保存前校验 待补充 马顺
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }

            log.info(String.format("保存授信申请数据,生成流水号%s", serno));
            lmtType = lmtApp.getLmtType();
            if (CmisCommonConstants.LMT_TYPE_03.equals(lmtType)) {
                Map queryMap = new HashMap();
                queryMap.put("cusId",cusId);
                queryMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
                LmtReplyAcc lastLmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(queryMap);
                // LmtReplyAcc lastLmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(cusId);
                lmtApp.setOrigiLmtReplySerno(lastLmtReplyAcc.getReplySerno());
                LmtReply lmtReply = new LmtReply();
                HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
                lmtReplyQueryMap.put("replySerno", lastLmtReplyAcc.getReplySerno());
                lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                lmtReplyQueryMap.put("sort", "UPD_DATE desc");
                List<LmtReply> list = lmtReplyService.queryLmtReplyDataByParams(lmtReplyQueryMap);
                if (list != null && list.size() == 1) {
                    lmtReply = list.get(0);
                } else {
                    rtnCode = EcbEnum.ECB020009.key;
                    rtnMsg = EcbEnum.ECB020009.value;
                    return rtnData;
                }
                String pkId = "";
                if (CmisCommonConstants.YES_NO_1.equals(isGrp)) {
                    pkId = lmtApp.getPkId();
                }
                BeanUtils.copyProperties(lmtReply, lmtApp);
                lmtApp.setLmtType(CmisCommonConstants.LMT_TYPE_03);
                lmtApp.setOgrigiLmtSerno(serno);
                lmtApp.setOrigiLmtReplySerno(lmtReply.getReplySerno());
                lmtApp.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
                lmtApp.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
                lmtApp.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
                lmtApp.setOrigiLmtTerm(lmtReply.getLmtTerm());
                log.info(String.format("保存授信续作申请数据,生成流水号%s", serno));
                // 申请流水号
                lmtApp.setSerno(serno);
                // 数据操作标志为新增
                lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                // 流程状态
                lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                User userInfo = SessionUtils.getUserInformation();
                if ( userInfo == null) {
                    rtnCode = EcbEnum.ECB010004.key;
                    rtnMsg = EcbEnum.ECB010004.value;
                    return rtnData;
                } else {
                    if (CmisCommonConstants.YES_NO_1.equals(isGrp)) {
                        lmtApp.setPkId(pkId);
                    } else {
                        lmtApp.setPkId(UUID.randomUUID().toString());
                    }
                    lmtApp.setIsGrp(isGrp);
                    lmtApp.setInputId(userInfo.getLoginCode());
                    lmtApp.setInputBrId(userInfo.getOrg().getCode());
                    lmtApp.setInputDate(openday);
                    lmtApp.setUpdId(userInfo.getLoginCode());
                    lmtApp.setUpdBrId(userInfo.getOrg().getCode());
                    lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtApp.setManagerId(userInfo.getLoginCode());
                    lmtApp.setManagerBrId(userInfo.getOrg().getCode());
                    lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                }
                log.info(String.format("保存单一客户授信续作数据,流水号%s", serno));
                int count = lmtAppMapper.insert(lmtApp);
                boolean result = lmtReplySubService.copyToLmtAppSub(lastLmtReplyAcc.getReplySerno(), serno);
                if (count != 1 || !result) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成续作申请失败！");
                }
            } else {
                // 申请流水号
                lmtApp.setSerno(serno);
                //原授信流水号
                lmtApp.setOgrigiLmtSerno(serno);
                // 数据操作标志为新增
                lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                // 流程状态
                lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                // 是否集团授信
                lmtApp.setIsGrp(isGrp);
                // 币种
                lmtApp.setCurType(CmisCommonConstants.CUR_TYPE_CNY);
                // 敞口额度默认为0
                lmtApp.setOpenTotalLmtAmt(new BigDecimal(0));
                // 低风险额度默认为0
                lmtApp.setLowRiskTotalLmtAmt(new BigDecimal(0));
                log.info(String.format("保存授信申请数据%s-获取当前登录用户数据", serno));
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    rtnCode = EcbEnum.ECB010004.key;
                    rtnMsg = EcbEnum.ECB010004.value;
                    return rtnData;
                } else {
                    lmtApp.setInputId(userInfo.getLoginCode());
                    lmtApp.setInputBrId(userInfo.getOrg().getCode());
                    lmtApp.setInputDate(openday);
                    lmtApp.setUpdId(userInfo.getLoginCode());
                    lmtApp.setUpdBrId(userInfo.getOrg().getCode());
                    lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtApp.setManagerId(userInfo.getLoginCode());
                    lmtApp.setManagerBrId(userInfo.getOrg().getCode());
                    lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                }
                log.info(String.format("保存授信申请数据,流水号%s", serno));
                int count = lmtAppMapper.insert(lmtApp);
                if (count != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",保存失败！");
                }
            }
            //必输页签添加
            BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
            bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
            bizMustCheckDetailsDto.setBizType(lmtApp.getLmtType());
            cusId = lmtApp.getCusId();
            //客户类型判断
            String idList = "";
            String pageList = "";
            if (cusId.startsWith("8")) {
                idList = "sxsbjbxx,khxejzxpj,sxdcxx,ldzjcsb";
                pageList = "授信申报基本信息,客户限额及债项评级,授信调查信息,流动资金测算表";
            } else {
                idList = "sxsbjbxx,sxdcxx,lsnp";
                pageList = "授信申报基本信息,授信调查信息,零售内评";
//                idList = "sxsbjbxx,sxdcxx";
//                pageList = "授信申报基本信息,授信调查信息";
            }
            bizMustCheckDetailsDto.setPageList(pageList);
            bizMustCheckDetailsDto.setIdList(idList);
            bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存授信申请数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtApp);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: onRedicuss
     * @方法描述: 新增单一客户再议申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onRedicuss(LmtApp lmtApp) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        LmtApp lmtAppNew = new LmtApp();
        String cusId = "";
        try {
            cusId = lmtApp.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            boolean isEffectAcc = judgeIsEffectAcc(cusId);
//            if (isEffectAcc) {
//                rtnCode = EcbEnum.ECB010093.key;
//                rtnMsg = EcbEnum.ECB010093.value;
//                return rtnData;
//            }
            // TODO 保存前校验 待补充
            serno = lmtApp.getSerno();
            lmtAppNew = lmtAppMapper.selectBySerno(serno);
            if (!CmisCommonConstants.WF_STATUS_993.equals(lmtAppNew.getApproveStatus())) {
                rtnCode = EcbEnum.ECB019999.key;
                rtnMsg = EcbEnum.ECB019999.value;
                return rtnData;
            }
            lmtAppNew.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            //  记录申请类型为再议
            lmtAppNew.setLmtType(CmisCommonConstants.LMT_TYPE_06);
            lmtAppNew.setOrigiLowRiskTotalLmtAmt(lmtAppNew.getLowRiskTotalLmtAmt());
            lmtAppNew.setOrigiOpenTotalLmtAmt(lmtAppNew.getOpenTotalLmtAmt());
            lmtAppNew.setOrigiLmtGraperTerm(lmtAppNew.getLmtGraperTerm());
            lmtAppNew.setOrigiLmtTerm(lmtAppNew.getLmtTerm());
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtAppNew.getCusId());
            log.info("根据客户号查询客户信息，更新授信中最新的客户名称！【{}】",JSON.toJSONString(cusBaseClientDto));
            lmtAppNew.setCusName(cusBaseClientDto.getCusName());
            log.info(String.format("生成单一客户再议申请"));
            int count = lmtAppMapper.updateByPrimaryKey(lmtAppNew);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成再议申请失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成再议申请出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtAppNew);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: judgeIsEffectAcc
     * @方法描述: 判断当前客户是否存在已生效的的台账信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     */

    public boolean judgeIsEffectAcc(String cusId) {
        boolean isEffectAcc = false;
        HashMap map = new HashMap();
        map.put("cusId", cusId);
        map.put("accStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectByParams(map);
        if (!lmtReplyAccList.isEmpty() && lmtReplyAccList.size() > 0) {
            isEffectAcc = true;
        }
        return isEffectAcc;
    }

    /**
     * @方法名称: onReconside
     * @方法描述: 新增单一客户复议申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onReconside(LmtApp lmtApp) throws URISyntaxException {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String origSerno = "";
        String origReplySerno = "";
        LmtApp lmtAppNew = new LmtApp();
        LmtReply lmtReply = new LmtReply();
        String cusId = "";
        try {
            cusId = lmtApp.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            boolean isThreeFYFlg = isThreeFY(lmtApp);
            if (!isThreeFYFlg) {
                rtnCode = EcbEnum.ECB020000.key;
                rtnMsg = EcbEnum.ECB020000.value;
                return rtnData;
            }
            // TODO 保存前校验 待补充
            origSerno = lmtApp.getSerno();
            LmtApp lastLmtApp = lmtAppMapper.getLastLmtApp(cusId);
            if (lastLmtApp.getSerno().equals(origSerno)) {
                HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
                lmtReplyQueryMap.put("serno", origSerno);
                lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                List<LmtReply> list = lmtReplyService.queryLmtReplyDataByParams(lmtReplyQueryMap);
                lmtReply = list.get(0);
            } else {
                rtnCode = EcbEnum.ECB020009.key;
                rtnMsg = EcbEnum.ECB020009.value;
                return rtnData;
            }
            origReplySerno = lmtReply.getReplySerno();
            BeanUtils.copyProperties(lmtReply, lmtAppNew);
            lmtAppNew.setOgrigiLmtSerno(lastLmtApp.getOgrigiLmtSerno());
            lmtAppNew.setOrigiLmtReplySerno(lmtReply.getReplySerno());
            lmtAppNew.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
            lmtAppNew.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
            lmtAppNew.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
            lmtAppNew.setOrigiLmtTerm(lmtReply.getLmtTerm());
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info(String.format("保存授信复议申请数据,生成流水号%s", serno));
            // 申请流水号
            lmtAppNew.setSerno(serno);
            // 数据操作标志为新增
            lmtAppNew.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //  记录申请类型为复议
            lmtAppNew.setLmtType(CmisCommonConstants.LMT_TYPE_05);
            // 流程状态
            lmtAppNew.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtAppNew.setIsGrp("0");
                lmtAppNew.setPkId(UUID.randomUUID().toString());
                lmtAppNew.setInputId(userInfo.getLoginCode());
                lmtAppNew.setInputBrId(userInfo.getOrg().getCode());
                lmtAppNew.setInputDate(openday);
                lmtAppNew.setUpdId(userInfo.getLoginCode());
                lmtAppNew.setUpdBrId(userInfo.getOrg().getCode());
                lmtAppNew.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppNew.setManagerId(userInfo.getLoginCode());
                lmtAppNew.setManagerBrId(userInfo.getOrg().getCode());
                lmtAppNew.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtAppNew.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtAppNew.getCusId());
            log.info("根据客户号查询客户信息，更新授信中最新的客户名称！【{}】",JSON.toJSONString(cusBaseClientDto));
            lmtAppNew.setCusName(cusBaseClientDto.getCusName());
            log.info(String.format("保存单一客户授信复议数据,流水号%s", serno));
            int count = lmtAppMapper.insert(lmtAppNew);
            boolean result = lmtReplySubService.copyToLmtAppSub(origReplySerno, serno);
            if (count != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成复议申请失败！");
            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            creditReportQryLstAndRealDto.setCusId(lmtAppNew.getCusId());
            creditReportQryLstAndRealDto.setCusName(lmtAppNew.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("01");
            creditReportQryLstAndRealDto.setBizType("012");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成复议申请出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtAppNew);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: onReview
     * @方法描述: 新增单一客户复审申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onReview(LmtApp lmtApp) throws URISyntaxException {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String origSerno = "";
        String origReplySerno = "";
        LmtApp lmtAppNew = new LmtApp();
        LmtReply lmtReply = new LmtReply();
        String cusId = "";
        try {
            cusId = lmtApp.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            boolean isRulesFlg = judgeOnReviewRules(lmtApp);
            if (!isRulesFlg) {
                rtnCode = EcbEnum.ECB010098.key;
                rtnMsg = EcbEnum.ECB010098.value;
                return rtnData;
            }
            // TODO 保存前校验 待补充
            LmtApp lastLmtApp = lmtAppMapper.getLastLmtApp(cusId);
            origSerno = lmtApp.getSerno();
            HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
            lmtReplyQueryMap.put("serno", origSerno);
            lmtReplyQueryMap.put("replyStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtReply> list = lmtReplyService.queryLmtReplyDataByParams(lmtReplyQueryMap);
            if (list != null && list.size() == 1) {
                lmtReply = list.get(0);
            } else {
                rtnCode = EcbEnum.ECB020009.key;
                rtnMsg = EcbEnum.ECB020009.value;
                return rtnData;
            }
            origReplySerno = lmtReply.getReplySerno();
            BeanUtils.copyProperties(lmtReply, lmtAppNew);
            lmtAppNew.setOgrigiLmtSerno(lastLmtApp.getOgrigiLmtSerno());
            lmtAppNew.setOrigiLmtReplySerno(lmtReply.getReplySerno());
            lmtAppNew.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
            lmtAppNew.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
            lmtAppNew.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
            lmtAppNew.setOrigiLmtTerm(lmtReply.getLmtTerm());
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info(String.format("保存授信复议申请数据,生成流水号%s", serno));
            // 申请流水号
            lmtAppNew.setSerno(serno);
            // 数据操作标志为新增
            lmtAppNew.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            //  记录申请类型为复审
            lmtAppNew.setLmtType(CmisCommonConstants.LMT_TYPE_04);
            // 流程状态
            lmtAppNew.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                lmtAppNew.setIsGrp("0");
                lmtAppNew.setPkId(UUID.randomUUID().toString());
                lmtAppNew.setInputId(lmtApp.getInputId());
                lmtAppNew.setInputBrId(lmtApp.getInputBrId());
                lmtAppNew.setInputDate(openday);
                lmtAppNew.setUpdId(lmtApp.getInputId());
                lmtAppNew.setUpdBrId(lmtApp.getInputBrId());
                lmtAppNew.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppNew.setManagerId(lmtApp.getInputId());
                lmtAppNew.setManagerBrId(lmtApp.getInputBrId());
                lmtAppNew.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtAppNew.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            } else {
                lmtAppNew.setIsGrp("0");
                lmtAppNew.setPkId(UUID.randomUUID().toString());
                lmtAppNew.setInputId(userInfo.getLoginCode());
                lmtAppNew.setInputBrId(userInfo.getOrg().getCode());
                lmtAppNew.setInputDate(openday);
                lmtAppNew.setUpdId(userInfo.getLoginCode());
                lmtAppNew.setUpdBrId(userInfo.getOrg().getCode());
                lmtAppNew.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppNew.setManagerId(userInfo.getLoginCode());
                lmtAppNew.setManagerBrId(userInfo.getOrg().getCode());
                lmtAppNew.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtAppNew.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtAppNew.getCusId());
            log.info("根据客户号查询客户信息，更新授信中最新的客户名称！【{}】",JSON.toJSONString(cusBaseClientDto));
            lmtAppNew.setCusName(cusBaseClientDto.getCusName());
            log.info(String.format("保存单一客户授信复议数据,流水号%s", serno));
            int count = lmtAppMapper.insert(lmtAppNew);
            boolean result = lmtReplySubService.copyToLmtAppSub(origReplySerno, serno);
            if (count != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成复审申请失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成复议申请出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtAppNew);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: judgeOnReviewRules
     * @方法描述: 先判断当前授信期限是否大于一年;判断当前授信分项中是否存在循环授信的分项;
     * @参数与返回说明:
     * @算法描述: 无
     */

    private boolean judgeOnReviewRules(LmtApp lmtApp) throws ParseException {
        // 1、先判断当前授信期限是否大于一年  手动发起去除
        // 2、判断当前授信分项中是否存在循环授信的分项
        // 3、不存在审批中的复审任务
        // 4、在有效期内
        boolean rulesFlg = false;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // 审批中的复审任务
        HashMap map = new HashMap();
        map.put("cusId", lmtApp.getCusId());
        map.put("oprType", CmisCommonConstants.OP_TYPE_01);
        map.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        List<LmtApp> lmtApps = this.selectByParams(map);
        for (LmtApp lmtApp1 : lmtApps) {
            if (CmisCommonConstants.LMT_TYPE_04.equals(lmtApp1.getLmtType())) {
                log.info("当前客户存在一笔在途的复审任务:" + JSON.toJSONString(lmtApp1));
                return rulesFlg;
            }
        }
        // 循环授信额度判断
        List<LmtAppSub> list = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        if (!list.isEmpty() && list.size() > 0) {
            for (LmtAppSub lmtAppSub : list) {
                // 包含循环授信额度，并且不是低风险额度情况下，则需要复审
                if (CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit()) && !CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) {
                    log.info("授信项下当前分项符合循环授信额度并且非低风险情况,可发起复审:" + JSON.toJSONString(lmtAppSub));
                    rulesFlg = true;
                    break;
                } else {
                    continue;
                }
            }
        }
        // 是否还在有效期内
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(lmtApp.getCusId());
        if (lmtReplyAcc == null) {
            // 最新的生效的批复台账信息
            log.info("获取当前客户名下最新的生效的批复台账信息失败!");
            rulesFlg = false;
        } else {
            // 获取营业日期
            Date openday = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
            Date inureDate = simpleDateFormat.parse(lmtReplyAcc.getInureDate());
            Date endDate = DateUtils.addMonth(inureDate, lmtReplyAcc.getLmtTerm());
            if (endDate.before(openday)) {
                log.info("当前日期已超过单一客户生效的台账生效日期!");
                rulesFlg = false;
            }
        }
        return rulesFlg;
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据授信申请流水号查询授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpApp getLmtGrpAppBySerno(String serno) {
        LmtGrpApp lmtGrpApp = new LmtGrpApp();
        try{
            LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
            log.info("查询成员授信信息:"+ JSON.toJSONString(lmtApp));
            if(lmtApp == null ){
                return null;
            }else{
                LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtApp.getSerno());
                log.info("查询成员授信关系表信息:"+ JSON.toJSONString(lmtGrpMemRel));
                if(lmtGrpMemRel != null){
                    lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(lmtGrpMemRel.getGrpSerno());
                    log.info("查询集团授信信息:"+ JSON.toJSONString(lmtGrpApp));
                }else{
                    return null;
                }
            }
        }catch (Exception e){
            log.info("查询集团授信信息异常!"+ JSON.toJSONString(e));
        }
        return lmtGrpApp;
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据授信申请流水号查询授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtApp selectBySerno(String serno) {
        return lmtAppMapper.selectBySerno(serno);
    }

    /**
     * @方法名称: selectOnWayByCusid
     * @方法描述: 根据客户编号查询授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtApp selectOnWayByCusid(String cusId) {
        return lmtAppMapper.selectOnWayByCusid(cusId);
    }

    /**
     * @方法名称: updateLmtapp
     * @方法描述: 根据主键更新授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map updateLmtapp(LmtApp lmtApp) {
        Map rtnData = new HashMap();
        int count = 0;
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            // TODO 保存前校验 待补充
            User userInfo = SessionUtils.getUserInformation();
            lmtApp.setUpdId(userInfo.getLoginCode());
            lmtApp.setUpdBrId(userInfo.getOrg().getCode());
            lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info(String.format("更新申请信息"));
            if(CmisCommonConstants.LMT_TYPE_02.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_04.equals(lmtApp.getLmtType())|| CmisCommonConstants.LMT_TYPE_07.equals(lmtApp.getLmtType())){
                 if(StringUtils.nonEmpty(lmtApp.getOrigiLmtReplySerno())){
                     LmtReply lmtReplyOrigi = lmtReplyService.queryLmtReplyByReplySerno(lmtApp.getOrigiLmtReplySerno());
                     if(lmtReplyOrigi != null){
                         if(lmtApp.getLmtTerm().compareTo(lmtReplyOrigi.getLmtTerm()) > 0){
                             rtnCode = EcbEnum.ECB020065.key;
                             rtnMsg = EcbEnum.ECB020065.value;
                             return rtnData;
                         }
                     }
                 }
            }
            // 集团客户校验 当前期限不能大于 集团客户期限
            if ("1".equals(lmtApp.getIsGrp())) {
                log.info("成员客户授信申请流水号" + lmtApp.getSerno());
                // 根据成员客户申请流水号 查询 对应的集团客户授信申请信息
                List<LmtGrpApp> lmtGrpAppList = selectLmtGrpAppDataByLmtAppSerno(lmtApp.getSerno());
                // 理论上来说 只会存在一笔数据  但是 不排除测试数据紊乱的问题
                BigDecimal grpTerm = new BigDecimal(lmtGrpAppList.get(0).getLmtTerm());
                BigDecimal singleTerm = new BigDecimal(lmtApp.getLmtTerm());

                if (singleTerm.compareTo(grpTerm) > 0) {
                    //throw new YuspException(EcbEnum.ECB010062.key, EcbEnum.ECB010062.value + ",更新申请失败！");
                    throw BizException.error(null, EcbEnum.ECB010068.key, EcbEnum.ECB010068.value);
                }

                if (lmtApp.getOpenTotalLmtAmt().compareTo(lmtGrpAppList.get(0).getOpenTotalLmtAmt() == null ? new BigDecimal(0) : lmtGrpAppList.get(0).getOpenTotalLmtAmt()) == 1) {
                    //throw new YuspException(EcbEnum.ECB010062.key, EcbEnum.ECB010062.value + ",更新申请失败！");
                    throw BizException.error(null, EcbEnum.ECB020006.key, EcbEnum.ECB020006.value);
                }
            }
            List<LmtAppSubPrd> prdList = lmtAppSubPrdService.queryAllLmtAppSubPrdBySerno(lmtApp.getSerno());
            List<LmtAppSub> subList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
            //获取对应授信申请信息的原敞口金额以及低风险额度
            BigDecimal openAmt = new BigDecimal(0);
            BigDecimal lowAmt = new BigDecimal(0);
            for(LmtAppSub lmtAppSub:subList){
                if(CmisCommonConstants.LMT_TYPE_01.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtApp.getLmtType())|| CmisCommonConstants.LMT_TYPE_03.equals(lmtApp.getLmtType())){
                    if (CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit())) {
                        lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                        int subCount = lmtAppSubService.update(lmtAppSub);
                        if (subCount != 1) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                        }
                    }
                    List<LmtAppSubPrd> subPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                    for (LmtAppSubPrd lmtAppSubPrd : subPrdList) {
                        if (CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())) {
                            lmtAppSubPrd.setLmtTerm(lmtApp.getLmtTerm());
                            int subPrdCount = lmtAppSubPrdService.update(lmtAppSubPrd);
                            if (subPrdCount != 1) {
                                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                            }
                        }
                    }
                }else{
                    if (CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit())) {
                        lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                        int subCount = lmtAppSubService.update(lmtAppSub);
                        if (subCount != 1) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                        }
                    }
                    List<LmtAppSubPrd> subPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                    for (LmtAppSubPrd lmtAppSubPrd : subPrdList) {
                        if(StringUtils.nonEmpty(lmtAppSubPrd.getOrigiLmtAccSubPrdNo())){
                            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.selectLmtReplyAccSubPrdByAccSubPrdNo(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                            if(lmtReplyAccSubPrd == null){
                                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                            }
                            if (lmtApp.getLmtTerm().compareTo(lmtReplyAccSubPrd.getLmtTerm()) <= 0 && CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())) {
                                lmtAppSubPrd.setLmtTerm(lmtApp.getLmtTerm());
                                Map subPrdMap = lmtAppSubPrdService.updateLmtAppSubPrdByLmtApp(lmtAppSubPrd);
                                if (!EcbEnum.ECB010000.key.equals(subPrdMap.get("rtnCode"))) {
                                    throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                                }
                            }
                        }else{
                            if (CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())) {
                                lmtAppSubPrd.setLmtTerm(lmtApp.getLmtTerm());
                                int subPrdCount = lmtAppSubPrdService.update(lmtAppSubPrd);
                                if (subPrdCount != 1) {
                                    throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                                }
                            }
                        }
                    }
                }

                if(lmtAppSub.getLmtAmt() != null){
                    if ("60".equals(lmtAppSub.getGuarMode())) {
                        lowAmt = lowAmt.add(lmtAppSub.getLmtAmt());
                    } else {
                        openAmt = openAmt.add(lmtAppSub.getLmtAmt());
                    }
                }
                if(lmtAppSub.getLmtTerm() == null){
                    lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                    lmtAppSubService.update(lmtAppSub);
                }
            }
            lmtApp.setOpenTotalLmtAmt(openAmt);
            lmtApp.setLowRiskTotalLmtAmt(lowAmt);
            count = lmtAppMapper.updateByPrimaryKeySelective(lmtApp);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                // throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新申请失败！");
            }
        } catch (YuspException e) {
//            rtnCode = e.getCode();
//            rtnMsg = e.getMsg();
            throw BizException.error(null, e.getCode(), e.getMsg());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: selectLmtGrpAppDataByLmtAppSerno
     * @方法描述: 根据成员客户申请流水号 查询 对应的集团客户授信申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpApp> selectLmtGrpAppDataByLmtAppSerno(String serno) {
        return lmtAppMapper.selectLmtGrpAppDataByLmtAppSerno(serno);
    }

    /**
     * @方法名称: saveLmtAppForGrpLmt
     * @方法描述: 根据前台传入表单数据保存授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun1
     * @创建时间: 2021-04-09 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map saveLmtAppForGrpLmt(LmtApp lmtApp) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        try {
            // 保存单一客户授信申请数据
            //判断该成员客户当前是否有生效授信台账
            String cusId = lmtApp.getCusId();
            Map<String, String> map = this.getLmtTypeByCusId(cusId);
            lmtApp.setLmtType(map.get("lmtType"));
            rtnData = this.saveLmtApp(lmtApp, CmisCommonConstants.YES_NO_1);
            // 如果保存成功，则获取到授信申请流水号则将更新至集团授信成员关联表中
            if (EcbEnum.ECB010000.key.equals(rtnData.get("rtnCode"))) {
                LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.selectByPrimaryKey(lmtApp.getPkId());
                User userInfo = SessionUtils.getUserInformation();
                serno = StringUtils.toString(rtnData.get("serno")).trim();
                lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                log.info("由于shardkey的原因需要先删再增");
                lmtGrpMemRelService.deleteByPrimaryKey(lmtApp.getPkId());
                LmtGrpMemRel lmtGrpMemRel1 = lmtGrpMemRel;
                lmtGrpMemRel1.setSingleSerno(serno);
                lmtGrpMemRelService.insert(lmtGrpMemRel1);
            } else {
                rtnCode = (String) rtnData.get("rtnCode");
                rtnMsg = (String) rtnData.get("rtnMsg");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存授信申请数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 授信申请流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) throws Exception {
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        String approveStatus = lmtApp.getApproveStatus();
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        lmtAppMapper.updateByPrimaryKey(lmtApp);
        // 如果不是集团子流程审批
        if (!CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
            lmtApprService.generateLmtApprInfo(serno, approveStatus);
        } else {
            // 如果集团子成员授信流程， 则更新关联表中的客户经理提交状态
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_111);
            lmtGrpMemRelService.update(lmtGrpMemRel);
        }

    }


    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信申请流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno, String currentUserId, String currentOrgId, String flowCode) throws Exception {
        // 1.将审批状态更新为998
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        lmtAppMapper.updateByPrimaryKey(lmtApp);
        if (!CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {//如果不是集团子流程审批
            // 2.从根据流水号获取最新的一笔授信审批记录中的数据
            LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(serno);
            if (lmtAppr == null) {
                throw new Exception("查询审批中的授信结论异常");
            }
            // 3.根据最新的授信审批表中的数据生成批复数据
            lmtReplyService.generateLmtReplyHandleByLmtAppr(lmtAppr, currentUserId, currentOrgId);
            // 7.需要调用接口生成归档任务
            // 村镇银行不生成授信归档任务
            if (!Objects.equals(flowCode, CmisFlowConstants.SGCZ04) && !Objects.equals(flowCode, CmisFlowConstants.DHCZ04)) {
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                BeanUtils.copyProperties(lmtApp, docArchiveClientDto);
                docArchiveClientDto.setArchiveMode("02");
                docArchiveClientDto.setBizSerno(serno);
                docArchiveClientDto.setDocClass("02");
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                if ("1".equals(cusBaseClientDto.getCusCatalog())) {
                    docArchiveClientDto.setDocType("03");// 03个人授信
                } else {
                    docArchiveClientDto.setDocType("04");// 04企业授信
                }
                if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                    docArchiveClientDto.setDocType("04");// 04企业授信
                    docArchiveClientDto.setDocBizType("02");// 02集团授信
                } else {
                    docArchiveClientDto.setDocBizType("01");// 01单户授信
                }
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成档案归档信息失败");
                }
            }
            sendWbMsgNotice(lmtApp, lmtApp.getCusName(), "单一客户授信申报", lmtApp.getManagerId());
        } else {
            // 如果集团子成员授信流程， 则更新关联表中的客户经理提交状态
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_998);
            lmtGrpMemRelService.update(lmtGrpMemRel);
            sendWbMsgNotice(lmtApp, lmtApp.getCusName(), "集团授信申报子流程", lmtApp.getManagerId());
        }

        // 更新压降计划登记为失效
        ResultDto<Integer> result = icusClientService.updateStatusRefuse(serno);
        log.info("更新压降计划登记" + serno + "的状态为失效" + "，更新结果为：" + result);
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信申请流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) throws Exception {
        // 1.将审批状态更新为992
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        lmtAppMapper.updateByPrimaryKey(lmtApp);
        if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
            // 如果集团子成员授信流程， 则更新关联表中的客户经理提交状态
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_992);
            lmtGrpMemRelService.update(lmtGrpMemRel);
        }
        //微信通知
        String managerId = lmtApp.getManagerId();
        String mgrTel = "";
        if (StringUtil.isNotEmpty(managerId)) {
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            try {
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", lmtApp.getCusName());
                paramMap.put("result", "退回");
                if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                    paramMap.put("prdName", "集团授信申报子流程");
                } else {
                    if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_01)){
                        paramMap.put("prdName", "单一授信新增");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02)){
                        paramMap.put("prdName", "单一授信变更");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_03)){
                        paramMap.put("prdName", "单一授信续作");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)){
                        paramMap.put("prdName", "单一授信复审");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_05)){
                        paramMap.put("prdName", "单一授信复议");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_06)){
                        paramMap.put("prdName", "单一授信再议");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)){
                        paramMap.put("prdName", "单一预授信细化");
                    }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_08)){
                        paramMap.put("prdName", "单一授信额度调剂");
                    }else{
                        log.info("当前授信申请[{}]类型未识别",JSON.toJSONString(lmtApp));
                        paramMap.put("prdName", "单一授信申报");
                    }
                }
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            } catch (Exception e) {
                throw new Exception("发送短信失败！");
            }
        }
    }

    /**
     * @方法名称: handleBusinessAfterReStart
     * @方法描述: 授信申请流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterReStart(String serno) {
        // 1.将审批状态更新为993
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_993);
        lmtAppMapper.updateByPrimaryKey(lmtApp);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信申请流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.从根据流水号获取最新的一笔授信审批记录中的数据
     * 3.根据最新的授信审批表中的数据生成批复数据
     * 4.根据不同的授信类型生成或者更新批复台账
     * 4.1 ►	授信新增时，生成新的业务流水号，新的批复号，新的批复台账号
     * 4.2 ►	授信续作时，生成新业务流水号，新批复号，新批复生效时旧批复自动失效，TODO 原批复台账项下业务自动挂到新批复台账项下，要找李成金确认（是否需要判断台账的状态）
     * 4.3 ►	授信复审时，生成新的业务流水号，新的批复号，更新原批复台账
     * 4.4 ►	预授信细化时，生成新的业务流水号，新的批复号，更新原批复台账
     * 4.5 ►	授信变更时，复制原授信方案。生成新业务流水号，更新原批复台账
     * 4.6 ►	授信再议时，不改变业务流水号，生成新的批复号，TODO 根据再议前的授信类型去判断批复台账处理逻辑
     * 4.7 ►	授信复议时，系统将原批复对应的申报（授信基本信息、授信分项明细）内容进行复制，生成新的业务流水号，新的批复号，TODO 根据复议前的授信类型去判断批复台账处理逻辑
     * 5.推送批复台账至额度系统
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno, String currentUserId, String currentOrgId, String flowCode) throws Exception {
        // 1.将审批状态更新为997 通过
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.update(lmtApp);
        log.info("单一授信申请审批通过结束逻辑处理 -> 更新申请表状态为审批通过！");

        if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
            log.info("集团子成员授信申请审批通过结束逻辑处理 -> 更新审批表状态为审批通过！");
            // 如果集团子成员授信流程， 则更新关联表中的客户经理提交状态
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_997);
            lmtGrpMemRel.setOpenLmtAmt(lmtApp.getOpenTotalLmtAmt());
            lmtGrpMemRel.setLowRiskLmtAmt(lmtApp.getLowRiskTotalLmtAmt());
            lmtGrpMemRelService.update(lmtGrpMemRel);
            return;
        }

        // 更新压降计划登记为生效
        ResultDto<Integer> result = icusClientService.updateStatusPass(lmtApp.getCusId());
        log.info("单一授信申请审批通过结束逻辑处理 -> 更新更新压降计划登记状态,接口调用返回【{}】", result.toString());

        // 2.从根据流水号获取最新的一笔授信审批记录中的数据
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(serno);
        if (lmtAppr == null) {
            throw new Exception("查询审批中的授信结论异常");
        }

        // 3.根据最新的授信审批表中的数据生成批复数据
        lmtReplyService.generateLmtReplyHandleByLmtAppr(lmtAppr, currentUserId, currentOrgId);

        // 4.根据授信批复生成授信批复台账
        lmtReplyAccService.generateLmtReplyAccBySerno(serno);

        // 5.推送授信台账至额度系统
        lmtReplyAccService.synLmtReplyAccToLmtSys(lmtApp.getCusId());

        // 6.同步信贷客户建立时间
        CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto = new CusUpdateInitLoanDateDto();
        cusUpdateInitLoanDateDto.setCusId(lmtApp.getCusId());
        cusUpdateInitLoanDateDto.setInitLoanDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        iCusClientService.updateInitLoanDate(cusUpdateInitLoanDateDto);

        // 7.需要调用接口生成归档任务
        // 村镇银行不生成授信归档任务
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
        if (!Objects.equals(flowCode, CmisFlowConstants.SGCZ04) && !Objects.equals(flowCode, CmisFlowConstants.DHCZ04)) {
            if ("1".equals(cusBaseClientDto.getCusCatalog()) || "2".equals(cusBaseClientDto.getCusCatalog())) {
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                BeanUtils.copyProperties(lmtApp, docArchiveClientDto);
                docArchiveClientDto.setArchiveMode("02");
                docArchiveClientDto.setBizSerno(serno);
                docArchiveClientDto.setDocClass("02");
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                if ("1".equals(cusBaseClientDto.getCusCatalog())) {
                    docArchiveClientDto.setDocType("03");// 03个人授信
                } else {
                    docArchiveClientDto.setDocType("04");// 04企业授信
                }
                if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                    docArchiveClientDto.setDocType("04");// 04企业授信
                    docArchiveClientDto.setDocBizType("02");// 02集团授信
                } else {
                    docArchiveClientDto.setDocBizType("01");// 01单户授信
                }
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成档案归档信息失败");
                }
            }
        }
        // 8.同步押品系统的授信协议数据
        guarBusinessRelService.sendLmtinf("03", serno);

        // 科技贷：征信贷授信审批通过后，将审批信息（批复额度、推送给征信APP）
        CusCorpDto cusCorpDto = icusClientService.queryCusCropDtoByCusId(lmtApp.getCusId()).getData();
        String mobile = "";
        if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
            mobile = cusCorpDto.getFreqLinkmanTel();
        }

        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", lmtApp.getOprType());
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectLmtAppSubPrdByParams(paramsSub);
        for (int i = 0; i < lmtAppSubPrdList.size(); i++) {
            LmtAppSubPrd lmtAppSubPrd = lmtAppSubPrdList.get(i);
            String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
            if ("P009".equals(lmtBizTypeProp)) {
                HashMap<String, String> queryMap = new HashMap<String, String>();
                queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                queryMap.put("cusId", lmtAppSubPrd.getCusId());
                // 查询当前用户下所有的台账
                List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectByParams(queryMap);
                if (CollectionUtils.nonEmpty(lmtReplyAccList)) {
                    Wxp002ReqDto wxp002ReqDto = new Wxp002ReqDto();
                    // 筛选生效的台账
                    List<LmtReplyAcc> useList = lmtReplyAccList.parallelStream().filter(e
                            -> Objects.equals(CmisLmtConstants.STD_ZB_LMT_STATE_01, e.getAccStatus())).collect(Collectors.toList());
                    LmtReplyAcc lmtReplyAcc = useList.get(0);
                    if (lmtReplyAccList.size() - useList.size() > 0) {
                        wxp002ReqDto.setIffist("1");//是否首次贷款
                    } else {
                        wxp002ReqDto.setIffist("0");//是否首次贷款
                    }
                    wxp002ReqDto.setApplid(lmtReplyAcc.getPkId());//对接记录ID
                    wxp002ReqDto.setEntnam(lmtReplyAcc.getCusName());//企业名称
                    wxp002ReqDto.setEntpid(lmtReplyAcc.getCusId());//企业代码
                    wxp002ReqDto.setConnum(lmtReplyAcc.getAccNo());//授信合同编号
                    wxp002ReqDto.setCrerst("997");//授信结果
                    wxp002ReqDto.setCrerea("正常授信");//授信原因
                    wxp002ReqDto.setAdlmt(lmtReplyAcc.getOpenTotalLmtAmt().toString());//授信金额
                    wxp002ReqDto.setCrerat(lmtAppSubPrd.getRateYear().toString());//授信利率
                    wxp002ReqDto.setCrebeg(lmtAppSubPrd.getStartDate());//授信开始时间
                    wxp002ReqDto.setCreend(lmtAppSubPrd.getEndDate());//授信结束时间
                    dscms2WxClientService.wxp002(wxp002ReqDto);
                }

            }
        }
        // 对公授信变更时选择无还本续贷，审批结束后在无还本续贷名单中增加该客户记录
        if (Objects.equals("02", lmtApp.getLmtType())) {
            if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                for (LmtAppSubPrd sub : lmtAppSubPrdList) {
                    // 判断是不是无还本续贷
                    if (Objects.equals("1", sub.getIsRwrop())) {
                        // 生成无还本续贷名单
                        CusLstWhbxdDto cusLstWhbxdDto = new CusLstWhbxdDto();
                        // 客户id
                        cusLstWhbxdDto.setCusId(lmtApp.getCusId());
                        // 客户名称
                        cusLstWhbxdDto.setCusName(lmtApp.getCusName());
                        // 客户规模
                        cusLstWhbxdDto.setCusScale(Objects.nonNull(cusCorpDto) ? cusCorpDto.getCorpScale() : "");
                        // 是否优良信贷客户
                        cusLstWhbxdDto.setIsFineCretCus("");
                        // 授信金额
                        cusLstWhbxdDto.setLmtAmt(lmtApp.getOrigiOpenTotalLmtAmt());
                        // 内部评级
                        cusLstWhbxdDto.setInnerEval(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getCusCrdGrade() : "");
                        // 名单归属
                        cusLstWhbxdDto.setListBelg("");
                        // 管户客户经理
                        cusLstWhbxdDto.setManagerId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getManagerId() : "");
                        // 所属机构
                        cusLstWhbxdDto.setBelgOrg(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getManagerBrId() : "");
                        // 状态
                        cusLstWhbxdDto.setStatus(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getCusState() : "");
                        // 登记人
                        cusLstWhbxdDto.setInputId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputId() : "");
                        // 登记机构
                        cusLstWhbxdDto.setInputBrId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputBrId() : "");
                        // 登记时间
                        cusLstWhbxdDto.setInputDate(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getInputDate() : "");
                        // 更新人
                        cusLstWhbxdDto.setUpdId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdId() : "");
                        // 更新机构
                        cusLstWhbxdDto.setUpdBrId(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdBrId() : "");
                        // 更新时间
                        cusLstWhbxdDto.setUpdDate(Objects.nonNull(cusBaseClientDto) ? cusBaseClientDto.getUpdDate() : "");
                        // 创建时间
                        cusLstWhbxdDto.setCreateTime(new Date());
                        // 修改时间
                        cusLstWhbxdDto.setUpdateTime(new Date());
                        cmisCusClientService.addCusLstWhbxd(cusLstWhbxdDto);
                        break;
                    }
                }
            }
            //微信通知
            String managerId = lmtApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                    Map paramMap = new HashMap();//短信填充参数
                    paramMap.put("cusName", lmtApp.getCusName());
                    paramMap.put("result", "通过");
                    if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                        paramMap.put("prdName", "集团授信申报子流程");
                    } else {
                        if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_01)){
                            paramMap.put("prdName", "单一授信新增");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02)){
                            paramMap.put("prdName", "单一授信变更");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_03)){
                            paramMap.put("prdName", "单一授信续作");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)){
                            paramMap.put("prdName", "单一授信复审");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_05)){
                            paramMap.put("prdName", "单一授信复议");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_06)){
                            paramMap.put("prdName", "单一授信再议");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)){
                            paramMap.put("prdName", "单一预授信细化");
                        }else if(lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_08)){
                            paramMap.put("prdName", "单一授信额度调剂");
                        }else{
                            log.info("当前授信申请[{}]类型未识别",JSON.toJSONString(lmtApp));
                            paramMap.put("prdName", "单一授信申报");
                        }
                    }
                    //执行发送客户经理操作
                    messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
        log.info("授信审批业务逻辑处理完成" + serno);
    }


    /**
     * @方法名称: onModify
     * @方法描述: 新增单一客户授信变更申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onModify(String accNo) throws URISyntaxException {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String pkId = "";
        LmtApp lmtApp = new LmtApp();
        String cusId = "";
        String replySerno = "";
        // TODO 保存前校验 待补充
        try {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(accNo);
            replySerno = lmtReplyAcc.getReplySerno();
            LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
            if (lmtReply == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            cusId = lmtReply.getCusId();
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            LmtApp lastLmtApp = lmtAppMapper.getLastLmtApp(cusId);
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info(String.format("保存授信变更申请数据,生成流水号%s", serno));
            BeanUtils.copyProperties(lmtReply, lmtApp);
            pkId = UUID.randomUUID().toString();
            lmtApp.setPkId(pkId);
            // 申请流水号
            lmtApp.setSerno(serno);
            // 数据操作标志为新增
            lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtApp.setLmtType(CmisCommonConstants.LMT_TYPE_02);//授信类型为变更
                lmtApp.setIsGrp("0");
                lmtApp.setOgrigiLmtSerno(lastLmtApp.getOgrigiLmtSerno());
                lmtApp.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
                lmtApp.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
                lmtApp.setOrigiLmtTerm(lmtReply.getLmtTerm());
                lmtApp.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
                lmtApp.setOrigiLmtReplySerno(replySerno);
                lmtApp.setInputId(userInfo.getLoginCode());
                lmtApp.setInputBrId(userInfo.getOrg().getCode());
                lmtApp.setInputDate(openday);
                lmtApp.setUpdId(userInfo.getLoginCode());
                lmtApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtApp.setManagerId(userInfo.getLoginCode());
                lmtApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            //生成授信变更申请从表信息
            LmtChgDetail lmtChgDetail = new LmtChgDetail();
            lmtChgDetail.setLmtSerno(serno);
            lmtChgDetail.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtChgDetail.setInputId(userInfo.getLoginCode());
            lmtChgDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgDetail.setUpdId(userInfo.getLoginCode());
            lmtChgDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgDetail.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtChgDetail.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
            log.info("根据客户号查询客户信息，更新授信中最新的客户名称！【{}】",JSON.toJSONString(cusBaseClientDto));
            lmtApp.setCusName(cusBaseClientDto.getCusName());
            log.info(String.format("新增单一客户授信变更数据,流水号%s", serno));
            int count = lmtAppMapper.insert(lmtApp);
            log.info(String.format("新增单一客户授信变更从表数据,流水号%s", serno));
            int countDetail = lmtChgDetailMapper.insert(lmtChgDetail);
            boolean result = lmtReplySubService.copyToLmtAppSub(replySerno, serno);
            if (count != 1 || countDetail != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成变更申请失败！");
            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(cusBaseClientDto.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("01");
            creditReportQryLstAndRealDto.setBizType("012");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成授信变更申请出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtApp);
            rtnData.put("pkId", pkId);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: getLmtModify
     * @方法描述: 获取当前客户经理名下所以授信变更申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApp> getLmtModify(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", CmisCommonConstants.STD_ZB_YES_NO_0);
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_02);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtAllAppHis
     * @方法描述: 根据入参获取所有的授信历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtAllAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        //model.getCondition().put("isGrp", CmisCommonConstants.STD_ZB_YES_NO_0);
        List<String> lmtTypes = new ArrayList<>();
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_993);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("createTime desc");
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtModifyHis
     * @方法描述: 获取当前客户经理名下所以授信变更申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtModifyHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", CmisCommonConstants.STD_ZB_YES_NO_0);
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_02);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据入参获取当前客户经理名下授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> selectByParams(Map params) {
        if (!MapUtils.checkExistsEmptyEnum(params)) {
            return lmtAppMapper.selectByParams(params);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
    }

    /**
     * @方法名称: getLmtAppByModel
     * @方法描述: 根据querymodel获取当前客户经理名下所以授信申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", "0");
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_01);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_03);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_04);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_05);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_06);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_08);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("createTime desc");
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtAppByModel1
     * @方法描述: 根据querymodel获取当前客户经理名下所有的授信申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtAppByModel1(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("createTime desc");
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtAppHis
     * @方法描述: 获取当前客户经理名下所以授信申请历史数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", "0");
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_01);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_03);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_04);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_05);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_06);
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_08);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_993);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("createTime desc");
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtRefine
     * @方法描述: 获取当前客户经理名下所以预授信细化申请列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApp> getLmtRefine(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        // TODO 初始查询条件待补充
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", "0");
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_07);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getLmtRefineHis
     * @方法描述: 获取当前客户经理名下所以预授信细化申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApp> getLmtRefineHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("isGrp", "0");
        List<String> lmtTypes = new ArrayList<>();
        lmtTypes.add(CmisCommonConstants.LMT_TYPE_07);
        model.getCondition().put("lmtTypes", lmtTypes);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        List<LmtApp> list = lmtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: onRefine
     * @方法描述: 新增单一客户预授信细化申请
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map onRefine(String accNo, String lmtType, String isGrp) {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String origiSerno = "";
        String pkId = "";
        LmtApp lmtApp = new LmtApp();
        String cusId = "";
        String replySerno = "";
        // TODO 保存前校验 待补充
        try {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(accNo);
            if (CmisCommonConstants.YES_NO_1.equals(isGrp)) {
                if (lmtReplyAcc == null) {
                    log.info(String.format("当前集团成员台账不存在,直接返回,台账编号:", accNo));
                    rtnCode = EcbEnum.ECB010000.key;
                    rtnMsg = EcbEnum.ECB010000.value;
                    return rtnData;
                }
            }
            replySerno = lmtReplyAcc.getReplySerno();
            LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
            if (lmtReply == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            cusId = lmtReply.getCusId();
            LmtApp lastLmtApp = lmtAppMapper.getLastLmtApp(cusId);
            boolean exit = judgeIsExistOnWayApp(cusId);
            if (exit) {
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
                return rtnData;
            }
            origiSerno = lmtReply.getSerno();
            LmtApp origiLmtApp = lmtAppMapper.selectBySerno(origiSerno);
            if (origiLmtApp == null) {
                rtnCode = EpbEnum.EPB099999.key;
                rtnMsg = EpbEnum.EPB099999.value;
                return rtnData;
            }
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info(String.format("保存预授信细化申请数据,生成流水号%s", serno));
            pkId = UUID.randomUUID().toString();
            BeanUtils.copyProperties(lmtReply, lmtApp);
            // 申请流水号
            lmtApp.setSerno(serno);
            // 数据操作标志为新增
            lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                lmtApp.setPkId(pkId);
                lmtApp.setLmtType(lmtType);//授信类型为变更
                lmtApp.setIsGrp(isGrp);
                lmtApp.setEvalHighCurfundLmtAmt(origiLmtApp.getEvalHighCurfundLmtAmt());
                lmtApp.setOgrigiLmtSerno(lastLmtApp.getOgrigiLmtSerno());
                lmtApp.setOrigiLmtTerm(lmtReply.getLmtTerm());
                lmtApp.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
                lmtApp.setOrigiLmtReplySerno(replySerno);
                lmtApp.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
                lmtApp.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
                lmtApp.setInputId(lmtReply.getManagerId());
                lmtApp.setInputBrId(lmtReply.getManagerBrId());
                lmtApp.setInputDate(openday);
                lmtApp.setUpdId(lmtReply.getManagerId());
                lmtApp.setUpdBrId(lmtReply.getManagerBrId());
                lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtApp.setManagerId(lmtReply.getManagerId());
                lmtApp.setManagerBrId(lmtReply.getManagerBrId());
                lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            } else {
                lmtApp.setPkId(pkId);
                lmtApp.setLmtType(lmtType);//授信类型为变更
                lmtApp.setIsGrp(isGrp);
                lmtApp.setEvalHighCurfundLmtAmt(origiLmtApp.getEvalHighCurfundLmtAmt());
                lmtApp.setOgrigiLmtSerno(lastLmtApp.getOgrigiLmtSerno());
                lmtApp.setOrigiLmtTerm(lmtReply.getLmtTerm());
                lmtApp.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
                lmtApp.setOrigiLmtReplySerno(replySerno);
                lmtApp.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
                lmtApp.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
                lmtApp.setInputId(userInfo.getLoginCode());
                lmtApp.setInputBrId(userInfo.getOrg().getCode());
                lmtApp.setInputDate(openday);
                lmtApp.setUpdId(userInfo.getLoginCode());
                lmtApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtApp.setManagerId(userInfo.getLoginCode());
                lmtApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            //成员必输页签数据
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGrp)) {
                //复议必输页签数据
                if (CmisCommonConstants.LMT_TYPE_05.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxsbjbxx,khxejzxpj,ldzjcsb,fysqxx";
                        pageList = "授信申报基本信息,客户限额及债项评级,流动资金测算表,复议申请信息";
                    } else {
                        idList = "sxsbjbxx,fysqxx";
                        pageList = "授信申报基本信息,复议申请信息";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
                //复审必输页签数据
                else if (CmisCommonConstants.LMT_TYPE_04.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxsbjbxx,khxejzxpj,ldzjcsb,xhedfs";
                        pageList = "授信申报基本信息,客户限额及债项评级,流动资金测算表,循环授信额度复审";
                    } else {
                        idList = "sxsbjbxx,xhedfs";
                        pageList = "授信申报基本信息,循环授信额度复审";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
                //变更必输页签数据
                else if (CmisCommonConstants.LMT_TYPE_02.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxbgjbxx,khxejzxpj,sxbgsqs";
                        pageList = "授信变更基本信息,客户限额及债项评级,授信变更申请书";
                    } else {
                        idList = "sxbgjbxx,sxbgsqs";
                        pageList = "授信变更基本信息,授信变更申请书";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
                //续作必输页签数据
                else if (CmisCommonConstants.LMT_TYPE_03.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxsbjbxx,khxejzxpj,sxdcxx,ldzjcsb";
                        pageList = "授信申报基本信息,客户限额及债项评级,授信调查信息,流动资金测算表";
                    } else {
                        idList = "sxsbjbxx,sxdcxx";
                        pageList = "授信申报基本信息,授信调查信息";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
                //预授信细化必输页签数据
                else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "dykhysxxhxqym,khxejzxpj,ysxxhsqs";
                        pageList = "单一客户预授信细化详情页面,客户限额及债项评级,授信细化申请书";
                    } else {
                        idList = "dykhysxxhxqym,ysxxhsqs";
                        pageList = "单一客户预授信细化详情页面,授信细化申请书";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
                //成员额度调剂必输页签数据
                else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtType)) {
                    BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                    bizMustCheckDetailsDto.setSerno(lmtApp.getSerno());
                    bizMustCheckDetailsDto.setBizType(lmtType);
                    cusId = lmtApp.getCusId();
                    //客户类型判断
                    String idList = "";
                    String pageList = "";
                    if (cusId.startsWith("8")) {
                        idList = "sxsbjbxx,khxejzxpj,cytjsqb";
                        pageList = "授信申报基本信息,客户限额及债项评级,成员调剂申请表";
                    } else {
                        idList = "sxsbjbxx,cytjsqb";
                        pageList = "授信申报基本信息,成员调剂申请表";
                    }
                    bizMustCheckDetailsDto.setPageList(pageList);
                    bizMustCheckDetailsDto.setIdList(idList);
                    bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
                }
            }
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
            log.info("根据客户号查询客户信息，更新授信中最新的客户名称！【{}】",JSON.toJSONString(cusBaseClientDto));
            lmtApp.setCusName(cusBaseClientDto.getCusName());
            log.info(String.format("新增单一客户预授信细化数据,流水号%s", serno));
            int count = lmtAppMapper.insert(lmtApp);
            // 流动资金测算 问题复制原有授信申请信息 绑定新授信申请信息
            // 根据当前申请流水号  复制一条新的信息测算信息数据
            log.info(String.format("根据新流水号 复制一条新的测算数据%s", origiSerno));
            LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectBySerno(origiSerno);
            if (lmtHighCurfundEval != null) {
                lmtHighCurfundEval.setPkId(UUID.randomUUID().toString());
                lmtHighCurfundEval.setSerno(serno);
                // 登录信息
                lmtHighCurfundEval.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtHighCurfundEval.setInputId(lmtReply.getManagerId());
                lmtHighCurfundEval.setInputBrId(lmtReply.getManagerBrId());
                lmtHighCurfundEval.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtHighCurfundEval.setUpdId(lmtReply.getManagerId());
                lmtHighCurfundEval.setUpdBrId(lmtReply.getManagerBrId());
                lmtHighCurfundEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtHighCurfundEval.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtHighCurfundEval.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtHighCurfundEvalService.insertSelective(lmtHighCurfundEval);
            }
            // 单一法人分项信息
            boolean result = lmtReplySubService.copyToLmtAppSub(replySerno, serno);
            if (count != 1 || !result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成预授信细化申请失败！");
            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(cusBaseClientDto.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("01");
            creditReportQryLstAndRealDto.setBizType("012");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成预授信细化申请出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApp", lmtApp);
            rtnData.put("pkId", pkId);
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @方法名称: generateLmtAppByLmtGrpAcc
     * @方法描述: 通过批复台账数据生成申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Map generateLmtAppByLmtGrpAcc(String lmtReplyAccNo, String lmtType) throws Exception {
        return onRefine(lmtReplyAccNo, lmtType, CmisCommonConstants.YES_NO_1);
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：获取当前登录人下的审批信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-22 上午 9:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtApp> queryByManagerId() {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("managerId", managerId);
        return lmtAppMapper.queryLmtReplyDataByParams(paramMap);
    }

    /**
     * @方法名称：updateLmtAppBySingleSerno
     * @方法描述：通过授信申报流水号 更新项下成员的审批状态信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-06-10 xia午 15:52
     * @修改记录：修改时间 修改人员  修改原因
     */

    public int updateLmtAppBySingleSerno(HashMap map) {
        //再议情况下集团成员新增必输页签数据
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(map.get("serno").toString());
        if (CollectionUtils.nonEmpty(lmtGrpMemRelList)) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                BizMustCheckDetailsDto bizMustCheckDetailsDto = new BizMustCheckDetailsDto();
                bizMustCheckDetailsDto.setBizType(CmisCommonConstants.LMT_TYPE_06);
                bizMustCheckDetailsDto.setSerno(lmtGrpMemRel.getSingleSerno());
                bizMustCheckDetailsDto.setIdList("zysqxx");
                bizMustCheckDetailsDto.setPageList("再议申请信息");
                bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto);
            }
        }
        return lmtAppMapper.updateLmtAppBySingleSerno(map);
    }

    /**
     * @方法名称: backApproveStatus
     * @方法描述: 将审批状态重置为打回
     * @参数与返回说明:
     * @算法描述: 将审批状态重置为打回
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void backApproveStatus(String serno) {
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        lmtAppMapper.updateByPrimaryKey(lmtApp);
    }

    /**
     * @方法名称: riskItem0006
     * @方法描述: 委托人校验
     * @参数与返回说明:
     * @算法描述: 对公授信时，包含委托贷款，委托人在我行已经存在以下场景，强拦截
     * 1）退回、审批中、生效状态的授信
     * 2）退回、审批中、待签定、生效的合同
     * 3）未结清的借据
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0006(String serno) {
        log.info("委托人校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.queryAllLmtAppSubPrdBySerno(serno);
        if (CollectionUtils.isEmpty(lmtAppSubPrdList)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
            if (CmisCommonConstants.BIZ_TPYE_14020301.equals(lmtAppSubPrd.getLmtBizType())
                    || CmisCommonConstants.BIZ_TPYE_20040101.equals(lmtAppSubPrd.getLmtBizType()) ) {
                // 获取委托人编号
                String consignorCusId = lmtAppSubPrd.getConsignorCusId();
                // 审批状态
                String approveStatus = "";
                if (StringUtils.nonEmpty(consignorCusId)) {
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("cusId", consignorCusId);
                    queryModel.addCondition("oprType", "01");//01 新增
                    List<LmtApp> lmtAppList = lmtAppMapper.selectByModel(queryModel);//查询授信表信息
                    if (CollectionUtils.nonEmpty(lmtAppList)) {
                        for (LmtApp lmtApps : lmtAppList) {
                            approveStatus = lmtApps.getApproveStatus();//授信审批状态
                            // 退回、审批中、生效状态的授信
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的授信
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0068);
                                return riskResultDto;
                            }
                        }
                    }
                    // 退回、审批中、待签定、生效的合同（最高额授信协议申请）
                    List<IqpHighAmtAgrApp> iqpHighAmtAgrAppList = iqpHighAmtAgrAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpHighAmtAgrAppList)) {
                        for (IqpHighAmtAgrApp iqpHighAmtAgrApp : iqpHighAmtAgrAppList) {
                            approveStatus = iqpHighAmtAgrApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    // 退回、审批中、待签定、生效的合同(普通贷款合同申请、贸易融资合同申请、福费廷合同申请)
                    List<IqpLoanApp> iqpLoanAppList = iqpLoanAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpLoanAppList)) {
                        for (IqpLoanApp iqpLoanApp : iqpLoanAppList) {
                            approveStatus = iqpLoanApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    //退回、审批中、待签定、生效的合同(贴现协议申请)
                    List<IqpDiscApp> iqpDiscAppList = iqpDiscAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpDiscAppList)) {
                        for (IqpDiscApp iqpDiscApp : iqpDiscAppList) {
                            approveStatus = iqpDiscApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }

                    //退回、审批中、待签定、生效的合同(开证合同申请)
                    List<IqpTfLocApp> iqpTfLocAppList = iqpTfLocAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpTfLocAppList)) {
                        for (IqpTfLocApp iqpTfLocApp : iqpTfLocAppList) {
                            approveStatus = iqpTfLocApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    //退回、审批中、待签定、生效的合同(银承合同申请)
                    List<IqpAccpApp> iqpAccpAppList = iqpAccpAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpAccpAppList)) {
                        for (IqpAccpApp iqpAccpApp : iqpAccpAppList) {
                            approveStatus = iqpAccpApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    //退回、审批中、待签定、生效的合同(保函合同申请)
                    List<IqpCvrgApp> iqpCvrgAppList = iqpCvrgAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpCvrgAppList)) {
                        for (IqpCvrgApp iqpCvrgApp : iqpCvrgAppList) {
                            approveStatus = iqpCvrgApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    //退回、审批中、待签定、生效的合同(委托贷款合同申请)
                    List<IqpEntrustLoanApp> iqpEntrustLoanAppList = iqpEntrustLoanAppService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(iqpEntrustLoanAppList)) {
                        for (IqpEntrustLoanApp iqpEntrustLoanApp : iqpEntrustLoanAppList) {
                            approveStatus = iqpEntrustLoanApp.getApproveStatus();//合同表中对应审批状态
                            if (CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)
                                    || CmisCommonConstants.WF_STATUS_111.equals(approveStatus) || CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) { //在途的合同申请
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0069);
                                return riskResultDto;
                            }
                        }
                    }
                    // 生效状态
                    String accStatus = "";
                    // 未结清的借据(普通贷款、贸易融资、福费廷、最高额)
                    List<AccLoan> accLoanList = accLoanService.selectByModel(queryModel);//查询台账信息表
                    if (CollectionUtils.nonEmpty(accLoanList)) {
                        for (AccLoan accLoans : accLoanList) {
                            accStatus = accLoans.getAccStatus();//贷款台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                    // 未结清的借据（委托贷款）
                    List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByModel(queryModel);//查询委托贷款台账信息表
                    if (CollectionUtils.nonEmpty(accEntrustLoanList)) {
                        for (AccEntrustLoan accEntrustLoans : accEntrustLoanList) {
                            accStatus = accEntrustLoans.getAccStatus();//贷款台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                    // 未结清的借据（银承）
                    List<AccAccp> accAccpList = accAccpService.selectByModel(queryModel);//查询银承台账信息表
                    if (CollectionUtils.nonEmpty(accAccpList)) {
                        for (AccAccp accAccps : accAccpList) {
                            accStatus = accAccps.getAccStatus();//贷款台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                    // 未结清的借据（保函）
                    List<AccCvrs> accCvrsList = accCvrsService.selectByModel(queryModel);//查询保函台账账信息表
                    if (CollectionUtils.nonEmpty(accCvrsList)) {
                        for (AccCvrs accCvrss : accCvrsList) {
                            accStatus = accCvrss.getAccStatus();//保函台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                    // 未结清的借据（贴现）
                    List<AccDisc> accDiscList = accDiscService.selectByModel(queryModel);//查询贴现台账账信息表
                    if (CollectionUtils.nonEmpty(accDiscList)) {
                        for (AccDisc accDiscs : accDiscList) {
                            accStatus = accDiscs.getAccStatus();//贴现台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                    // 未结清的借据（开证）
                    List<AccTfLoc> accTfLocList = accTfLocService.selectByModel(queryModel);//查询开证台账信息表
                    if (CollectionUtils.nonEmpty(accTfLocList)) {
                        for (AccTfLoc accTfLocs : accTfLocList) {
                            accStatus = accTfLocs.getAccStatus();//开证台账表中台账状态
                            if (!CmisCommonConstants.ACC_STATUS_0.equals(accStatus) && !CmisCommonConstants.ACC_STATUS_7.equals(accStatus)) { //未结清
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0070);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
        }
        log.info("委托人校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @函数名称:getAllLmtByInputId
     * @函数描述:根据客户经理工号查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> getAllLmtByInputId(QueryModel queryModel) {
        return lmtAppMapper.getAllLmtByInputId(queryModel);
    }

    /**
     * @param
     * @函数名称:selectLmtAppDataBySubSerno
     * @函数描述:根据分项流水号查询 授信类型
     * @参数与返回说明:
     * @算法描述:
     */

    public LmtApp selectLmtAppDataBySubSerno(String subSerno) {
        return lmtAppMapper.selectLmtAppDataBySubSerno(subSerno);
    }

    /**
     * @param
     * @函数名称:selectLmtAppDataBySubSerno
     * @函数描述:根据集团授信流水号查询名下的成员客户申请信息
     * @参数与返回说明:
     * @算法描述:
     */

    public List<LmtApp> selectLmtAppByGrpSerno(HashMap hashMap) {
        List<LmtApp> lmtAppList = new ArrayList<>();
        String grpSerno = hashMap.containsKey("grpSerno") ? hashMap.get("grpSerno").toString() : StringUtils.EMPTY;
        if(StringUtils.nonEmpty(grpSerno)){
            List<String> sernos = lmtGrpMemRelMapper.getSingleSernosByGrpSerno(grpSerno);
            if(CollectionUtils.nonEmpty(sernos)){
                hashMap.put("sernos",sernos);
                lmtAppList = lmtAppMapper.getLmtAppBySernos(hashMap);
            }
        }
        return lmtAppList;
        //return lmtAppMapper.selectLmtAppByGrpSerno(hashMap);
    }

    /**
     * @函数名称:insertOtherApproveByLmtSerno
     * @函数描述:根据授信流水号新增其他事项申报信息
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */

    public OtherAppDto insertOtherApproveByLmtSerno(Map map) {
        OtherAppDto otherAppDto = new OtherAppDto();
        try {
            String otherAppType = map.get("otherAppType").toString();
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            if (StringUtils.isEmpty(otherAppType)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            } else {
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                }
                if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_01.equals(otherAppType)) {
                    log.info(String.format("人民币利率定价申请", serno));
                    OtherCnyRateApp otherCnyRateApp = new OtherCnyRateApp();
                    otherCnyRateApp.setSerno(serno);
                    otherCnyRateApp.setCusId(map.get("cusId").toString());
                    otherCnyRateApp.setCusName(map.get("cusName").toString());
                    otherCnyRateApp.setCusType(map.get("cusType").toString());
                    if (!StringUtils.isBlank((String) map.get("tradeClass"))) {
                        otherCnyRateApp.setTradeClass(map.get("tradeClass").toString());
                    }
                    if (!StringUtils.isBlank((String) map.get("corpScale"))) {
                        otherCnyRateApp.setCorpScale(map.get("corpScale").toString());
                    }
                    otherCnyRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherCnyRateApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherCnyRateApp.setInputId(userInfo.getLoginCode());
                    otherCnyRateApp.setInputBrId(userInfo.getOrg().getCode());
                    otherCnyRateApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherCnyRateApp.setUpdId(userInfo.getLoginCode());
                    otherCnyRateApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherCnyRateApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherCnyRateApp.setManagerId(userInfo.getLoginCode());
                    otherCnyRateApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherCnyRateApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherCnyRateApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherCnyRateAppService.insertSelective(otherCnyRateApp);
                    BeanUtils.copyProperties(otherCnyRateApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_01);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_02.equals(otherAppType)) {
                    log.info(String.format("外币利率定价申请", serno));
                    OtherForRateApp otherForRateApp = new OtherForRateApp();
                    otherForRateApp.setSerno(serno);
                    otherForRateApp.setCusId(map.get("cusId").toString());
                    otherForRateApp.setCusName(map.get("cusName").toString());
                    otherForRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherForRateApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // otherForRateApp.setStatus(CmisBizConstants.STD_REPLY_STATUS_01);
                    // 登录信息
                    otherForRateApp.setInputId(userInfo.getLoginCode());
                    otherForRateApp.setInputBrId(userInfo.getOrg().getCode());
                    otherForRateApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherForRateApp.setUpdId(userInfo.getLoginCode());
                    otherForRateApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherForRateApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherForRateApp.setManagerId(userInfo.getLoginCode());
                    otherForRateApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherForRateApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherForRateApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherForRateAppService.insertSelective(otherForRateApp);
                    BeanUtils.copyProperties(otherForRateApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_02);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_03.equals(otherAppType)) {
                    log.info(String.format("保证金存款特惠利率申请", ""));
                    OtherBailDepPreferRateApp otherBailDepPreferRateApp = new OtherBailDepPreferRateApp();
                    otherBailDepPreferRateApp.setSerno(serno);
                    otherBailDepPreferRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherBailDepPreferRateApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherBailDepPreferRateApp.setInputId(userInfo.getLoginCode());
                    otherBailDepPreferRateApp.setInputBrId(userInfo.getOrg().getCode());
                    otherBailDepPreferRateApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherBailDepPreferRateApp.setUpdId(userInfo.getLoginCode());
                    otherBailDepPreferRateApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherBailDepPreferRateApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherBailDepPreferRateApp.setManagerId(userInfo.getLoginCode());
                    otherBailDepPreferRateApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherBailDepPreferRateApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherBailDepPreferRateApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherBailDepPreferRateAppService.insertSelective(otherBailDepPreferRateApp);
                    BeanUtils.copyProperties(otherBailDepPreferRateApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_03);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_04.equals(otherAppType)) {
                    log.info(String.format("银票手续费优惠申请表", ""));
                    OtherAccpPerferFeeApp otherAccpPerferFeeApp = new OtherAccpPerferFeeApp();
                    otherAccpPerferFeeApp.setSerno(serno);
                    otherAccpPerferFeeApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherAccpPerferFeeApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherAccpPerferFeeApp.setInputId(userInfo.getLoginCode());
                    otherAccpPerferFeeApp.setInputBrId(userInfo.getOrg().getCode());
                    otherAccpPerferFeeApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherAccpPerferFeeApp.setUpdId(userInfo.getLoginCode());
                    otherAccpPerferFeeApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherAccpPerferFeeApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherAccpPerferFeeApp.setManagerId(userInfo.getLoginCode());
                    otherAccpPerferFeeApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherAccpPerferFeeApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherAccpPerferFeeApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherAccpPerferFeeAppService.insertSelective(otherAccpPerferFeeApp);
                    BeanUtils.copyProperties(otherAccpPerferFeeApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_04);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_05.equals(otherAppType)) {
                    log.info(String.format("贴现优惠利率申请", serno));
                    OtherDiscPerferRateApp otherDiscPerferRateApp = new OtherDiscPerferRateApp();
                    otherDiscPerferRateApp.setSerno(serno);
                    otherDiscPerferRateApp.setCusId(map.get("cusId").toString());
                    otherDiscPerferRateApp.setCusName(map.get("cusName").toString());
                    otherDiscPerferRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherDiscPerferRateApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherDiscPerferRateApp.setInputId(userInfo.getLoginCode());
                    otherDiscPerferRateApp.setInputBrId(userInfo.getOrg().getCode());
                    otherDiscPerferRateApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherDiscPerferRateApp.setUpdId(userInfo.getLoginCode());
                    otherDiscPerferRateApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherDiscPerferRateApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherDiscPerferRateApp.setManagerId(userInfo.getLoginCode());
                    otherDiscPerferRateApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherDiscPerferRateApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherDiscPerferRateApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherDiscPerferRateAppService.insertSelective(otherDiscPerferRateApp);
                    BeanUtils.copyProperties(otherDiscPerferRateApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_05);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_06.equals(otherAppType)) {
                    log.info(String.format("银票签发业务计划申请", serno));
                    OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp = new OtherRecordAccpSignPlanApp();
                    otherRecordAccpSignPlanApp.setSerno(serno);
                    otherRecordAccpSignPlanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherRecordAccpSignPlanApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherRecordAccpSignPlanApp.setInputId(userInfo.getLoginCode());
                    otherRecordAccpSignPlanApp.setInputBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignPlanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordAccpSignPlanApp.setUpdId(userInfo.getLoginCode());
                    otherRecordAccpSignPlanApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignPlanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordAccpSignPlanApp.setManagerId(userInfo.getLoginCode());
                    otherRecordAccpSignPlanApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignPlanApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordAccpSignPlanApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordAccpSignPlanAppService.insertSelective(otherRecordAccpSignPlanApp);
                    BeanUtils.copyProperties(otherRecordAccpSignPlanApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_06);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_07.equals(otherAppType)) {
                    log.info(String.format("中行代签电票申请", serno));
                    OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp = new OtherRecordAccpSignOfBocApp();
                    otherRecordAccpSignOfBocApp.setSerno(serno);
                    otherRecordAccpSignOfBocApp.setCusId(map.get("cusId").toString());
                    otherRecordAccpSignOfBocApp.setCusName(map.get("cusName").toString());
                    otherRecordAccpSignOfBocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherRecordAccpSignOfBocApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherRecordAccpSignOfBocApp.setInputId(userInfo.getLoginCode());
                    otherRecordAccpSignOfBocApp.setInputBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignOfBocApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordAccpSignOfBocApp.setUpdId(userInfo.getLoginCode());
                    otherRecordAccpSignOfBocApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignOfBocApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordAccpSignOfBocApp.setManagerId(userInfo.getLoginCode());
                    otherRecordAccpSignOfBocApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherRecordAccpSignOfBocApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordAccpSignOfBocApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordAccpSignOfBocAppService.insertSelective(otherRecordAccpSignOfBocApp);
                    BeanUtils.copyProperties(otherRecordAccpSignOfBocApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_07);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_08.equals(otherAppType)) {
                    log.info(String.format("特殊贷款用信备案申请", serno));
                    //特殊贷款用信备案申请
                    OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = new OtherRecordSpecialLoanApp();
                    otherRecordSpecialLoanApp.setSerno(serno);
                    otherRecordSpecialLoanApp.setCusId(map.get("cusId").toString());
                    otherRecordSpecialLoanApp.setCusName(map.get("cusName").toString());
                    otherRecordSpecialLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherRecordSpecialLoanApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 登录信息
                    otherRecordSpecialLoanApp.setInputId(userInfo.getLoginCode());
                    otherRecordSpecialLoanApp.setInputBrId(userInfo.getOrg().getCode());
                    otherRecordSpecialLoanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordSpecialLoanApp.setUpdId(userInfo.getLoginCode());
                    otherRecordSpecialLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherRecordSpecialLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherRecordSpecialLoanApp.setManagerId(userInfo.getLoginCode());
                    otherRecordSpecialLoanApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherRecordSpecialLoanApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordSpecialLoanApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherRecordSpecialLoanAppService.insertSelective(otherRecordSpecialLoanApp);
                    BeanUtils.copyProperties(otherRecordSpecialLoanApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_08);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_09.equals(otherAppType)) {
                    log.info(String.format("免追加担保申请", serno));
                    //免追加担保申请  没有  不做处理
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_10.equals(otherAppType)) {
                    log.info(String.format("授信抵质押物价值认定申请", serno));
                    OtherGrtValueIdentyApp otherGrtValueIdentyApp = new OtherGrtValueIdentyApp();

                    String dzy_cusId = map.get("cusId").toString();
                    //TODO    hxl通过客户号把其他事项中申请通过的加入lmt_other_app_rel表中
                    QueryModel model = new QueryModel();
                    model.addCondition("cusId", dzy_cusId);
                    model.addCondition("approveStatus", "997");
                    model.setSort("updateTime desc");
                    List<OtherGrtValueIdentyApp> otherGrtValueIdentyApps = otherGrtValueIdentyAppService.selectAll(model);
                    if (otherGrtValueIdentyApps.size() > 0) {
                        serno = otherGrtValueIdentyApps.get(0).getSerno();
                        log.info(String.format("授信抵质押物价值认定申请流水号修改成+", serno));
                        otherGrtValueIdentyApp.setSerno(serno);
                    } else {
                        otherGrtValueIdentyApp.setSerno(serno);
                        otherGrtValueIdentyApp.setCusId(map.get("cusId").toString());
                        otherGrtValueIdentyApp.setCusName(map.get("cusName").toString());
                        otherGrtValueIdentyApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        otherGrtValueIdentyApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                        // 根基客户号获取授信批复流水号 TODO
                        otherGrtValueIdentyApp.setLmtSerno(map.get("lmtSerno").toString());
                        // 登录信息
                        otherGrtValueIdentyApp.setInputId(userInfo.getLoginCode());
                        otherGrtValueIdentyApp.setInputBrId(userInfo.getOrg().getCode());
                        otherGrtValueIdentyApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        otherGrtValueIdentyApp.setUpdId(userInfo.getLoginCode());
                        otherGrtValueIdentyApp.setUpdBrId(userInfo.getOrg().getCode());
                        otherGrtValueIdentyApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        otherGrtValueIdentyApp.setManagerId(userInfo.getLoginCode());
                        otherGrtValueIdentyApp.setManagerBrId(userInfo.getOrg().getCode());
                        otherGrtValueIdentyApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        otherGrtValueIdentyApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        otherGrtValueIdentyAppService.insertSelective(otherGrtValueIdentyApp);
                    }
                    BeanUtils.copyProperties(otherGrtValueIdentyApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_10);
                } else if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_11.equals(otherAppType)) {
                    log.info(String.format("其他事项申请", serno));
                    // 其他事项申请
                    OtherItemApp otherItemApp = new OtherItemApp();
                    otherItemApp.setSerno(serno);
                    otherItemApp.setCusId(map.get("cusId").toString());
                    otherItemApp.setCusName(map.get("cusName").toString());
                    if (!StringUtils.isBlank((String) map.get("trade"))) {
                        otherItemApp.setTrade(map.get("trade").toString());
                    }
                    if (!StringUtils.isBlank((String) map.get("corpQlty"))) {
                        otherItemApp.setCorpQlty(map.get("corpQlty").toString());
                    }
                    otherItemApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    otherItemApp.setOprType(CmisCommonConstants.OP_TYPE_01);
                    // 根基客户号获取授信批复流水号 TODO
                    otherItemApp.setLmtSerno(map.get("lmtSerno").toString());

                    LmtApp lmtApp = selectBySerno(otherItemApp.getLmtSerno());
                    if (lmtApp != null) {
                        otherItemApp.setCusType(lmtApp.getCusType());
                    }
                    // 登录信息
                    otherItemApp.setInputId(userInfo.getLoginCode());
                    otherItemApp.setInputBrId(userInfo.getOrg().getCode());
                    otherItemApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherItemApp.setUpdId(userInfo.getLoginCode());
                    otherItemApp.setUpdBrId(userInfo.getOrg().getCode());
                    otherItemApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    otherItemApp.setManagerId(userInfo.getLoginCode());
                    otherItemApp.setManagerBrId(userInfo.getOrg().getCode());
                    otherItemApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherItemApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    otherItemAppService.insertSelective(otherItemApp);
                    BeanUtils.copyProperties(otherItemApp, otherAppDto);
                    otherAppDto.setOtherAppType(CmisBizConstants.STD_LMT_OTHER_APP_TYPE_11);
                } else {
                    throw BizException.error(null, EcbEnum.ECB010075.key, EcbEnum.ECB010075.value);
                }
                log.info(String.format("新增其他事项申报授信关系表", map.get("lmtSerno").toString()));
                LmtOtherAppRel lmtOtherAppRel = new LmtOtherAppRel();
                lmtOtherAppRel.setPkId(UUID.randomUUID().toString());
                lmtOtherAppRel.setLmtSerno(map.get("lmtSerno").toString());
                lmtOtherAppRel.setLmtOtherAppType(otherAppType);
                lmtOtherAppRel.setLmtOtherAppSerno(serno);
                lmtOtherAppRel.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                lmtOtherAppRel.setOprType(CmisCommonConstants.OP_TYPE_01);
                lmtOtherAppRel.setInputId(userInfo.getLoginCode());
                lmtOtherAppRel.setInputBrId(userInfo.getOrg().getCode());
                lmtOtherAppRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtOtherAppRel.setUpdId(userInfo.getLoginCode());
                lmtOtherAppRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtOtherAppRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtOtherAppRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtOtherAppRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtOtherAppRelService.insertSelective(lmtOtherAppRel);
            }
        } catch (Exception e) {
            throw BizException.error(null, String.valueOf(e.hashCode()), e.getMessage());
        }
        return otherAppDto;
    }

    /**
     * 查询申请客户信息
     *
     * @return
     */
    public Map<String, Object> getLmtAppCusData(String serno) {
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        Map<String, Object> map = new HashMap<>();
        if (lmtApp != null) {
            String cusId = lmtApp.getCusId();
            //调查报告类型
            String rptType = lmtApp.getRptType();
            String isGrp = lmtApp.getIsGrp();
            map.put("cusId", cusId);
            map.put("rptType", rptType);
            map.put("isGrp", isGrp);
            //查询客户基本信息
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            if (cusBaseClientDto != null) {
                //客户大类
                String cusCatalog = cusBaseClientDto.getCusCatalog();
                map.put("cusCatalog", cusCatalog);
            }
            //查询对公客户基本信息
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
            if (cusCorpDtoResultDto != null && cusCorpDtoResultDto.getData() != null) {
                //是否为小企业客户标识
                String isSmconCus = cusCorpDtoResultDto.getData().getIsSmconCus();
                map.put("isSmconCus", isSmconCus);
                //是否国控
                String isNatctl = cusCorpDtoResultDto.getData().getIsNatctl();
                map.put("isNatctl", isNatctl);
            }
        }
        return map;
    }

    /**
     * 根据流水号修改申请信息
     *
     * @param lmtApp
     * @return
     */
    public int updateBySerno(LmtApp lmtApp) {
        String serno = lmtApp.getSerno();
        LmtApp temp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setPkId(temp.getPkId());
        int count = 0;
        count = lmtAppMapper.updateByPrimaryKeySelective(lmtApp);
        return count;
    }

    /**
     * 根据集团流水号查询成员授信申请信息
     *
     * @param grpSerno
     * @return
     */
    public List<LmtApp> getLmtAppByGrpSerno(String grpSerno) {
        return lmtAppMapper.getLmtAppByGrpSerno(grpSerno);
    }

    /**
     * 根据集团流水号查询本次申报成员授信申请信息
     *
     * @param grpSerno
     * @return
     */
    public List<LmtApp> getLmtAppByGrpSernoIsDeclare(String grpSerno) {
        return lmtAppMapper.getLmtAppByGrpSernoIsDeclare(grpSerno);
    }

    /**
     * @方法名称: getApprMode
     * @方法描述: 获取
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-07-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String getApprMode(String serno) {
        // 初始默认节点为双签
        String apprMode = CmisFlowConstants.STD_APPR_MODE_02;
        // 查询申请数据
        LmtApp lmtApp = this.selectBySerno(serno);
        // 如果俩个金额都为空，或者为集团，则无需获取审批模式
        if (lmtApp.getOpenTotalLmtAmt() == null || lmtApp.getLowRiskTotalLmtAmt() == null || CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtApp.getIsGrp())) {
            return null;
        }
        // 客户评级
        CmisCus0014ReqDto cmisCus0014ReqDto = new CmisCus0014ReqDto();
        cmisCus0014ReqDto.setCusId(lmtApp.getCusId());
        log.info("获取客户评级传参{}", cmisCus0014ReqDto.toString());
        CmisCus0014RespDto cmisCus0014RespDto = cmisCusClientService.cmiscus0014(cmisCus0014ReqDto).getData();
        log.info("获取客户评级回参{}", cmisCus0014ReqDto.toString());
        String cusLevel = "";
        if (StringUtils.isBlank(cmisCus0014RespDto.getFinalRank())) {
        } else {
            cusLevel = cmisCus0014RespDto.getFinalRank();
        }
        // 债项评级
        LmtLadEval lmtLadEval = lmtLadEvalService.selectSingleBySerno(serno);
        String debtGrade = "";
        if (lmtLadEval != null && !StringUtils.isBlank(lmtLadEval.getPkId())) {
            debtGrade = lmtLadEval.getDebtLevel();
        }

        // 敞口金额
        BigDecimal openTotalLmtAmt = lmtApp.getOpenTotalLmtAmt();
        // 低风险金额
        BigDecimal lowRiskTotalLmtAmt = lmtApp.getLowRiskTotalLmtAmt();
        // 是否特定产品授信
        String isSpecialProduct = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否个人经营贷款
        String isPersonalManager = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否低风险授信
        String isOnlyLowRiskLmt = CmisCommonConstants.STD_ZB_YES_NO_0;
        String isChgOpen = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否只有委托贷款
        String isOnlyWT = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否只有委托贷款
        Boolean isOhterWithOutWT = false;
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        for (LmtAppSub lmtAppSub : lmtAppSubList) {
            List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                // 判断产品是否有特殊产品（项目贷款、房地产开发贷款）
                if ((lmtAppSubPrd.getLmtBizType().startsWith("140101")
                        || lmtAppSubPrd.getLmtBizType().startsWith("140102"))
                        && BigDecimal.ZERO.compareTo(lmtAppSubPrd.getLmtAmt()) < 0) {
                    isSpecialProduct = CmisCommonConstants.STD_ZB_YES_NO_1;
                }
                // 判断产品是否有个人经营性贷款
                if (lmtAppSubPrd.getLmtBizType().startsWith("20")
                        && BigDecimal.ZERO.compareTo(lmtAppSubPrd.getLmtAmt()) < 0) {
                    isPersonalManager = CmisCommonConstants.STD_ZB_YES_NO_1;
                }
                // 只包含委托贷款
                if (lmtAppSubPrd.getLmtBizType().startsWith("1402")
                        && !isOhterWithOutWT
                        && BigDecimal.ZERO.compareTo(lmtAppSubPrd.getLmtAmt()) < 0) {
                    isOnlyWT = CmisCommonConstants.STD_ZB_YES_NO_1;
                } else {
                    isOnlyWT = CmisCommonConstants.STD_ZB_YES_NO_0;
                    isOhterWithOutWT = true;
                }
                // 如果非低风险的产品存在变更或新增
               /* if(!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSubPrd.getGuarMode())
                        && ("1".equals(lmtAppSubPrd.getChgFlag()) || "2".equals(lmtAppSubPrd.getChgFlag()) )){
                    isChgOpen = CmisCommonConstants.STD_ZB_YES_NO_1;
                }*/
            }
        }

        // 如果存在敞口 则是否仅仅低风险为否
        if (openTotalLmtAmt.compareTo(BigDecimal.ZERO) <= 0) {
            isOnlyLowRiskLmt = CmisCommonConstants.STD_ZB_YES_NO_1;
        }
        //  或者授信变更仅仅增加低风险
       /* if (CmisCommonConstants.LMT_TYPE_02.equals(lmtApp.getLmtType()) && !CmisCommonConstants.STD_ZB_YES_NO_1.equals(isChgOpen)){
            log.info("本次变更是否修改敞口【{}】", isChgOpen);
            isOnlyLowRiskLmt = CmisCommonConstants.STD_ZB_YES_NO_1;
        }*/
        // 资本占有金额对应的岗位
        String zbDuty = "";
        // 资本占有率
        String zbRate = "";
        // 资本占有率对应岗位
        String rateDuty = "";
        CfgApproveAuthorityDto cfgApproveAuthorityDto = new CfgApproveAuthorityDto();
        cfgApproveAuthorityDto.setAuthAmt(openTotalLmtAmt);
        cfgApproveAuthorityDto.setDebtLevel(debtGrade);
        cfgApproveAuthorityDto.setCusLevel(cusLevel);
        log.info("计算资本占用对应的岗位传参{}", cfgApproveAuthorityDto.toString());
        cfgApproveAuthorityDto = iCmisCfgClientService.queryDutyByCusDebtLevel(cfgApproveAuthorityDto).getData();
        log.info("计算资本占用对应的岗位回参{}", cfgApproveAuthorityDto);
        if (cfgApproveAuthorityDto != null) {
            log.info("计算资本占用对应的岗位{}", cfgApproveAuthorityDto.getDutyCode());
            zbDuty = cfgApproveAuthorityDto.getDutyCode();
        }

        CfgApproveAuthorityDto cfgApproveAuthorityDto2 = new CfgApproveAuthorityDto();
        cfgApproveAuthorityDto2.setAuthAmt(openTotalLmtAmt);
        cfgApproveAuthorityDto2.setDebtLevel(debtGrade);
        cfgApproveAuthorityDto2.setCusLevel(cusLevel);
        log.info("获取审批人的资本占用率传参{}", cfgApproveAuthorityDto2.toString());
        cfgApproveAuthorityDto2 = iCmisCfgClientService.queryRateByCusDebtLevel(cfgApproveAuthorityDto2).getData();
        log.info("获取审批人的资本占用率回参{}", cfgApproveAuthorityDto2);
        if (cfgApproveAuthorityDto2 != null) {
            log.info("获取审批人的资本占用率{}", cfgApproveAuthorityDto2.getAuthAmt());
            zbRate = cfgApproveAuthorityDto2.getAuthAmt().toString();
        }
        CfgApproveCapitalrateDto cfgApproveCapitalrateDto = new CfgApproveCapitalrateDto();
        cfgApproveCapitalrateDto.setRate(zbRate);
        log.info("根据占用率获取岗位传参{}", cfgApproveCapitalrateDto.toString());
        cfgApproveCapitalrateDto = iCmisCfgClientService.queryDutyByRate(cfgApproveCapitalrateDto).getData();
        log.info("根据占用率获取岗位回参{}", cfgApproveCapitalrateDto);
        if (cfgApproveCapitalrateDto != null) {
            log.info("根据占用率获取岗位{}", cfgApproveCapitalrateDto.getDutyCode());
            rateDuty = cfgApproveCapitalrateDto.getDutyCode();
        }

        log.info("测算审批模式为判断条件-：客户评级【{}】,债项评级【{}】,敞口金额【{}】,低风险金额【{}】,是否特定产品授信【{}】,是否个人经营贷款【{}】,是否低风险授信【{}】,资本占有金额对应的岗位【{}】,资本占有率【{}】,资本占有率对应岗位【{}】",
                cusLevel, debtGrade, openTotalLmtAmt, lowRiskTotalLmtAmt, isSpecialProduct, isPersonalManager, isOnlyLowRiskLmt, zbDuty, zbRate, rateDuty);
        if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isSpecialProduct) // 如果仅为一般授信审批权限 并且 敞口金额500万以上
                && CmisCommonConstants.STD_ZB_YES_NO_0.equals(isPersonalManager)
                && CmisCommonConstants.STD_ZB_YES_NO_0.equals(isOnlyLowRiskLmt)
                && CmisCommonConstants.STD_ZB_YES_NO_0.equals(isOnlyWT)
                && openTotalLmtAmt.compareTo(new BigDecimal("5000000")) > 0) {
            // 3006 双签        3007 三签        7001 三人会商 无 贷审会
            if ("".equals(zbDuty) || "".equals(rateDuty)) {
                apprMode = CmisFlowConstants.STD_APPR_MODE_05;
            } else if ("7001".equals(zbDuty) || "7001".equals(rateDuty)) {
                apprMode = CmisFlowConstants.STD_APPR_MODE_04;
            } else if ("3007".equals(zbDuty) || "3007".equals(rateDuty)) {
                apprMode = CmisFlowConstants.STD_APPR_MODE_03;
            } else {
                apprMode = CmisFlowConstants.STD_APPR_MODE_02;
            }
        } else if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isSpecialProduct)) { // 特定产品授信
            apprMode = CmisFlowConstants.STD_APPR_MODE_05;
        } else if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isSpecialProduct)
                && CmisCommonConstants.STD_ZB_YES_NO_1.equals(isPersonalManager)) { // 个人经营性贷款审批
            if (openTotalLmtAmt.compareTo(new BigDecimal("5000000")) <= 0) { // 授信金额在500万元（含）以内
                apprMode = CmisFlowConstants.STD_APPR_MODE_02;
            } else {
                apprMode = CmisFlowConstants.STD_APPR_MODE_05;
            }
        } else if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isSpecialProduct)
                && CmisCommonConstants.STD_ZB_YES_NO_0.equals(isPersonalManager)
                && CmisCommonConstants.STD_ZB_YES_NO_1.equals(isOnlyLowRiskLmt)) {
            if (lowRiskTotalLmtAmt.compareTo(new BigDecimal("50000000")) <= 0) {
                apprMode = CmisFlowConstants.STD_APPR_MODE_01;
            } else {
                apprMode = CmisFlowConstants.STD_APPR_MODE_02;
            }
        } else if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isOnlyWT)) { // 委托贷款 三签
            apprMode = CmisFlowConstants.STD_APPR_MODE_03;
        }
        // 新增单一授信审批表数据判断:如果是集团项下的成员客户授信,则成员与集团保持一致
        if (lmtApp.getIsGrp().equals(CmisCommonConstants.STD_ZB_YES_NO_1)) {
            log.info("成员客户审批模式与集团客户保持一致!");
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppBySingleSerno(lmtApp.getSerno());
            if (null != lmtGrpApp) {
                LmtGrpAppr lmtGrpAppr = lmtGrpApprService.queryFinalLmtGrpApprBySerno(lmtGrpApp.getGrpSerno());
                if (null != lmtGrpAppr && null != lmtGrpAppr.getApprMode()) {
                    log.info("集团客户审批模式{}", lmtGrpAppr.getApprMode());
                    apprMode = lmtGrpAppr.getApprMode();
                }
            }
        }
        log.info("测算审批模式为【{}】", apprMode);
        return apprMode;
    }

    /**
     * @方法名称: getMortMapResult
     * @方法描述: 获取抵押物路由结果集
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: hxl
     * @创建时间: 2021-08-16 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getMortMapResult(String serno) {
        LmtApp lmtApp = this.selectBySerno(serno);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (Objects.nonNull(lmtApp)) {
            // 提交状态
            resultMap.put("approveStatus", lmtApp.getApproveStatus());
            // 授信金额和500万进行比较
            BigDecimal TotalAmt = new BigDecimal("0.0").add(lmtApp.getLowRiskTotalLmtAmt()).add(lmtApp.getOpenTotalLmtAmt());
            if (TotalAmt.compareTo(new BigDecimal("5000000")) == 1) {//大于500万
                resultMap.put("lmtAmt", "Y");
            } else {
                resultMap.put("lmtAmt", "N");//小于等于500万
            }
            // 敞口金额和1000万比较
            BigDecimal openAmt = new BigDecimal("0.0").add(lmtApp.getOpenTotalLmtAmt());
            if (openAmt.compareTo(new BigDecimal("10000000")) == 1) {
                // 敞口金额
                resultMap.put("openTotalLmtAmt", "Y");
            } else {
                resultMap.put("openTotalLmtAmt", "N");
            }
            // 是否低风险
            String lowRisk = "N";

            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                //低风险的品种
                if (CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) {
                    lowRisk = "Y";

                    break;
                }
            }
            resultMap.put("lowRisk", lowRisk);
            // 是否仅有低风险
            String isOnlyLowRisk = "Y";
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                //低风险的品种
                if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) {
                    isOnlyLowRisk = "N";
                    break;
                }
            }
            resultMap.put("isOnlyLowRisk", isOnlyLowRisk);
        } else { // 未找到申请信息全部默认否
            resultMap.put("lmtAmt", "N");
            resultMap.put("openTotalLmtAmt", "N");
            resultMap.put("lowRisk",  "N");
            resultMap.put("isOnlyLowRisk", "N");
        }

        return resultMap;
    }

    /**
     * @方法名称: getRouterMapResult
     * @方法描述: 获取路由结果集
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-07-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getRouterMapResult(String serno) {
        LmtApp lmtApp = this.selectBySerno(serno);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //判断当前登录人是否资产保全客户经理
        log.info("***********调用AdminSmUserService用户岗位信息查询服务开始*START**************");
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        userAndDutyReqDto.setManagerId(lmtApp.getManagerId());
        ResultDto<List<UserAndDutyRespDto>> resultDto = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
        log.info("***********调用AdminSmUserService用户岗位查询服务结束*END*****************");
        String code = resultDto.getCode();//返回结果
        String isZCBQB = CmisCommonConstants.YES_NO_0;
        List<UserAndDutyRespDto> userAndDutyRespDtos = resultDto.getData();
        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
            for (UserAndDutyRespDto userAndDutyRespDto : userAndDutyRespDtos) {
                if ("SGH21".equals(userAndDutyRespDto.getDutyNo()) || "DHH21".equals(userAndDutyRespDto.getDutyNo())) {
                    isZCBQB = CmisCommonConstants.YES_NO_1;
                    break;
                }
            }
        }
        log.info("当前客户经理是否资产保全部-----" + isZCBQB);
        // 是否资产保全部提交
        resultMap.put("isZCBQB", isZCBQB);
        // 提交状态
        resultMap.put("approveStatus", lmtApp.getApproveStatus());
        // 敞口金额
        resultMap.put("openTotalLmtAmt", lmtApp.getOpenTotalLmtAmt());
        // 低风险金额
        resultMap.put("lowRiskTotalLmtAmt", lmtApp.getLowRiskTotalLmtAmt());
        // 授信金额
        resultMap.put("lmtAmt", BigDecimal.ZERO.add(lmtApp.getLowRiskTotalLmtAmt()).add(lmtApp.getOpenTotalLmtAmt()));
        // 上一笔授信申请流水号
        String origiLmtReplySerno = lmtApp.getOrigiLmtReplySerno();
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(origiLmtReplySerno);
        if (lmtReply != null) {
            resultMap.put("lastSerno", lmtReply.getSerno());
        }

        // 机构类型
        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(lmtApp.getManagerBrId());
        String orgLevel = adminSmOrgDtoResultDto.getData().getOrgType().toString();
        resultMap.put("orgLevel", orgLevel);
        // 是否特资部审核
        String isTZBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        CusLstWtsxDto cusLstWtsxDto = iCusClientService.queryCuslstwtsxByCusId(lmtApp.getCusId()).getData();
        if (cusLstWtsxDto != null && "01".equals(cusLstWtsxDto.getImportMode())) {
            isTZBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
        }
        resultMap.put("isTZBAppr", isTZBAppr);

        // 是否贸金部审核
        String isMJBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否小企业审核
        String isXQYAppr = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否为为小企业
        String isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 是否对公客户，判断协办客户经理节点
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
        log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            resultMap.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_1);
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId()).getData();
            if (cusCorpDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusCorpDto.getIsSmconCus())) {
                isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
        } else {
            resultMap.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
            CusIndivAttrDto cusIndivAttrDto = iCusClientService.queryCusIndivAttrByCusId(lmtApp.getCusId()).getData();
            if (cusIndivAttrDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusIndivAttrDto.getIsSmconCus())) {
                isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
        }
        // 是否公司部审核
        String isGSBAppr = CmisCommonConstants.STD_ZB_YES_NO_0;

        // 存量授信金额（不包含委托贷款）
        BigDecimal originLmtAmtNoWT = BigDecimal.ZERO;
        // 新增贷款金额（不包括委托贷款）
        BigDecimal lmtAmtNoWT = BigDecimal.ZERO;
        // 存量授信金额（不包含委托贷款）
        BigDecimal originLmtAmtWT = BigDecimal.ZERO;
        // 新增贷款金额（不包括委托贷款）
        BigDecimal lmtAmtWT = BigDecimal.ZERO;
        // 低风险非关联金额
        BigDecimal lowRiskNoRelAmt = BigDecimal.ZERO;

        // 非小企业含无还本续贷
        String isNoSmconCusRwrop = CmisCommonConstants.STD_ZB_YES_NO_0;
        // 标准化产品属性
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.BANK_STD_PRD);
        AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        String stdPrdProp = String.valueOf(adminSmPropDto.getPropValue());
        String isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_1;

        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        for (LmtAppSub lmtAppSub : lmtAppSubList) {
            // 除去低风险之外的品种
            if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) { //非低风险分项
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                    // 判断是否为贸易融资类产品 或者币种是否可以调节
                    if (lmtAppSubPrd.getLmtBizType().startsWith("11") || CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getCurAdjustFlag())) {
                        isMJBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                    }
                    // 授信品种是否包含信保贷 或者 小企业客户授信分项无还本续贷
                    if ("P010".equals(lmtAppSubPrd.getLmtBizTypeProp())
                            || (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isSmconCus) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getIsRwrop()))) {
                        isXQYAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
                    }
                    // 是否非小企业含无还本续贷
                    if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isSmconCus) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppSubPrd.getIsRwrop())) {
                        isNoSmconCusRwrop = CmisCommonConstants.STD_ZB_YES_NO_1;
                    }
                    // 计算本次不包含委托贷款金额 ，本次委托贷款金额
                    if ("140203".equals(lmtAppSubPrd.getLmtBizType()) || "14020301".equals(lmtAppSubPrd.getLmtBizType())
                            || "200401".equals(lmtAppSubPrd.getLmtBizType()) || "20040101".equals(lmtAppSubPrd.getLmtBizType())) { //委托贷款
                        lmtAmtWT = lmtAmtWT.add(lmtAppSubPrd.getLmtAmt());
                    }
                    if (StringUtils.isBlank(lmtAppSubPrd.getLmtBizTypeProp())
                            || !stdPrdProp.contains(lmtAppSubPrd.getLmtBizTypeProp())) {
                        isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_0;
                    }
                }
            } else { // 低风险分项
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                    // 如果为低风险担保类型细分为：全额保证金、国债质押、存单质押则 将其算入
                    List<LmtSubPrdLowGuarRel> lmtSubPrdLowGuarRels = lmtSubPrdLowGuarRelService.queryLmtSubPrdLowGuarRelListByPrdSerno(lmtAppSubPrd.getSubPrdSerno());
                    for (LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel : lmtSubPrdLowGuarRels) {
                        if ("10".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                || "11".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                || "13".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())) {
                            lowRiskNoRelAmt = lowRiskNoRelAmt.add(lmtSubPrdLowGuarRel.getLmtAmt());
                        }
                    }
                }
            }
        }
        lmtAmtNoWT = lmtApp.getOpenTotalLmtAmt().subtract(lmtAmtWT);
        // 是否是标准化产品
        if(lmtApp.getOpenTotalLmtAmt().compareTo(BigDecimal.ZERO) <= 0){
            isAllStdPrd = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        resultMap.put("isAllStdPrd", isAllStdPrd);

        // 查询存量授信
        // 分支行审核规则
        String loanApprMode = "";
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(lmtApp.getCusId());
        if (lmtReplyAcc != null && StringUtils.nonBlank(lmtReplyAcc.getAccNo())) {
            loanApprMode = lmtReplyAcc.getLoanApprMode();
            List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.selectByAccNo(lmtReplyAcc.getAccNo());
            for (LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubList) {
                if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSub.getGuarMode())) { // 低风险分项
                    List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(lmtReplyAccSub.getAccSubNo());
                    for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                        // 原不包含委托贷款金额，原委托贷款金额
                        if ("140203".equals(lmtReplyAccSubPrd.getLmtBizType()) || "14020301".equals(lmtReplyAccSubPrd.getLmtBizType())
                         || "200401".equals(lmtReplyAccSubPrd.getLmtBizType()) || "20040101".equals(lmtReplyAccSubPrd.getLmtBizType())) { //委托贷款
                            originLmtAmtWT = originLmtAmtWT.add(lmtReplyAccSubPrd.getLmtAmt());
                        }
                    }
                }
            }
            originLmtAmtNoWT = lmtReplyAcc.getOpenTotalLmtAmt().subtract(originLmtAmtWT);
        }

        if (CmisCommonConstants.LMT_TYPE_05.equals(lmtApp.getLmtType())
                && ((lmtApp.getOpenTotalLmtAmt() == null ? BigDecimal.ZERO : lmtApp.getOpenTotalLmtAmt()).compareTo(lmtApp.getOrigiOpenTotalLmtAmt() == null ? BigDecimal.ZERO : lmtApp.getOrigiOpenTotalLmtAmt()) != 0
                || (lmtApp.getLowRiskTotalLmtAmt() == null ? BigDecimal.ZERO : lmtApp.getLowRiskTotalLmtAmt()).compareTo(lmtApp.getOrigiLowRiskTotalLmtAmt() == null ? BigDecimal.ZERO : lmtApp.getOrigiLowRiskTotalLmtAmt()) != 0)) {
            // 如果复审类型下，金额发生变化，则直接到总行审批
            resultMap.put("loanApprMode", "03");
            resultMap.put("lmtType", lmtApp.getLmtType());
        } else {
            // 其他情况送参数
            resultMap.put("loanApprMode", loanApprMode);
            resultMap.put("lmtType", lmtApp.getLmtType());
        }

        // 是否本次申报仅仅申请低风险
        if (lmtApp.getOpenTotalLmtAmt().compareTo(BigDecimal.ZERO) > 0) {
            resultMap.put("isAssignByTodoNum", CmisCommonConstants.STD_ZB_YES_NO_1);
        } else {
            resultMap.put("isAssignByTodoNum", CmisCommonConstants.STD_ZB_YES_NO_0);
        }

        log.info("公司部判断逻辑,入参为：原不包含委托贷款金额【{}】，原委托贷款金额【{}】，本次不包含委托贷款金额【{}】，本次委托贷款金额【{}】，是否非小企业含无还本续贷【{}】",
                originLmtAmtNoWT, originLmtAmtWT, lmtAmtNoWT, lmtAmtWT, isNoSmconCusRwrop);
        // 公司部判断逻辑
        if (originLmtAmtNoWT.compareTo(new BigDecimal("100000000")) >= 0
                || (lmtAmtNoWT.compareTo(new BigDecimal("15000000")) >= 0 && lmtAmtNoWT.compareTo(originLmtAmtNoWT) > 0)
                || originLmtAmtWT.compareTo(new BigDecimal("100000000")) >= 0
                || (lmtAmtWT.compareTo(new BigDecimal("15000000")) >= 0 && lmtAmtWT.compareTo(originLmtAmtWT) > 0)
                || CmisCommonConstants.STD_ZB_YES_NO_1.equals(isNoSmconCusRwrop)) {
            isGSBAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
        }

        // 是否贸金部审核
        resultMap.put("isMJBAppr", isMJBAppr);
        // 是否小企业审核
        resultMap.put("isXQYAppr", isXQYAppr);
        // 是否公司部审核
        resultMap.put("isGSBAppr", isGSBAppr);
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(serno);
        if (lmtAppr != null) {
            // 公司部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptGSB", StringUtils.isBlank(lmtAppr.getIsSubToOtherDeptCom()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getIsSubToOtherDeptCom());
            // 公司部审批部门获取
            resultMap.put("commitDeptTypeGSB", StringUtils.isBlank(lmtAppr.getSubOtherDeptCom()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getSubOtherDeptCom());
            // 信贷管理部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptXDGLB", StringUtils.isBlank(lmtAppr.getIsSubOtherDeptXd()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getIsSubOtherDeptXd());
            // 信贷管理部审批部门获取
            resultMap.put("commitDeptTypeXDGLB", StringUtils.isBlank(lmtAppr.getSubOtherDeptXd()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getSubOtherDeptXd());
            // 审批模式
            resultMap.put("apprMode", lmtAppr.getApprMode());
            // 是否大额业务授信备案
            resultMap.put("isDAELmtAppr", StringUtils.isBlank(lmtAppr.getIsBigLmt()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getIsBigLmt());
            // 是否上调权限
            resultMap.put("isUpAppr", StringUtils.isBlank(lmtAppr.getIsUpperApprAuth()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getIsUpperApprAuth());
            // 上调权限类型
            resultMap.put("upApprType", StringUtils.isBlank(lmtAppr.getUpperApprAuthType()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getUpperApprAuthType());
            // 如果上调权限后，则将审批模式中的权限更新为上调后的权限
            if (!StringUtils.isBlank(lmtAppr.getUpperApprAuthType())) {
                resultMap.put("apprMode", lmtAppr.getUpperApprAuthType());
            }
            // 是否下调权限
            resultMap.put("isDownAppr", StringUtils.isBlank(lmtAppr.getIsLowerApprAuth()) ? CmisCommonConstants.STD_ZB_YES_NO_0 : lmtAppr.getIsLowerApprAuth());
            // 如果下调权限后，则将审批模式中的权限更新为三人会签
            if (!StringUtils.isBlank(lmtAppr.getIsLowerApprAuth()) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtAppr.getIsLowerApprAuth())) {
                resultMap.put("apprMode", CmisFlowConstants.STD_APPR_MODE_03);
            }
        } else {
            // 公司部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptGSB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 公司部审批部门获取
            resultMap.put("commitDeptTypeGSB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 信贷管理部是否提交其他部门审批
            resultMap.put("isCommitOtherDeptXDGLB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 信贷管理部审批部门获取
            resultMap.put("commitDeptTypeXDGLB", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 审批模式
            resultMap.put("apprMode", CmisFlowConstants.STD_APPR_MODE_01);
            // 是否大额业务授信备案
            resultMap.put("isDAELmtAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 是否上调权限
            resultMap.put("isUpAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 上调权限类型
            resultMap.put("upApprType", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 是否下调权限
            resultMap.put("isDownAppr", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        // 是否关联审批
        // 是否关联交易
        String isGLAppr = bizCommonService.checkCusIsGlf(lmtApp.getCusId());
        resultMap.put("isGLAppr", isGLAppr);
        // 低风险非关联方控制额度
        resultMap.put("lowRiskNoRelAmt", lowRiskNoRelAmt);
        // 如果是关联交易
        if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGLAppr)) {
            Map<String, Object> gljyMap = bizCommonService.calGljyType(lmtApp.getCusId(), lmtApp.getOpenTotalLmtAmt().subtract(lmtAmtWT), lmtApp.getLowRiskTotalLmtAmt(), lowRiskNoRelAmt, CmisBizConstants.STD_ZB_INSTU_CDE_ZJG);

            BigDecimal newLmtRelRate = (BigDecimal) gljyMap.get("newLmtRelRate");
            BigDecimal allLmtRelRate = (BigDecimal) gljyMap.get("allLmtRelRate");
            String isSupGLFLmtAmt = (String) gljyMap.get("isSupGLFLmtAmt");

            resultMap.put("newLmtRelRate", newLmtRelRate);
            resultMap.put("allLmtRelRate", allLmtRelRate);
            resultMap.put("isSupGLFLmtAmt", isSupGLFLmtAmt);
        }
        // 获取分成比例协办客户经理编号
        String managerId = null;
        CusMgrDividePercDto cusMgrDividePercDto = icusClientService.selectXBManagerId(lmtApp.getCusId()).getData();
        log.info("根据客户号【{}】查询客户协办客户经理，返回为{}", lmtApp.getCusId(), cusMgrDividePercDto);
        if (cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())) {
            managerId = cusMgrDividePercDto.getManagerId();
        }
        resultMap.put("managerId", managerId);

        //省心快贷线上审批
        String sxkdRiskResult = ""; //省心快贷风险拦截结果 成功1 失败0
        String isSubAutoAppr = ""; //是否提交自动化审批 1是0否
        String approveResult = ""; //风控自动审批结果 1成功 2否决
        String isGrp = "";//是否集团

        //省心快贷风险拦截结果 成功1 失败0
        if (StringUtils.isBlank(lmtApp.getSxkdRiskResult())) {
            sxkdRiskResult = "0";
        } else {
            sxkdRiskResult = lmtApp.getSxkdRiskResult();
        }
        //是否集团 1是0否
        if (StringUtils.isBlank(lmtApp.getIsGrp())) {
            isGrp = "0";
        } else {
            isGrp = lmtApp.getIsGrp();
        }
        //是否提交自动化审批1是0否
        if (StringUtils.isBlank(lmtApp.getIsSubAutoAppr())) {
            isSubAutoAppr = "0";
        } else {
            isSubAutoAppr = lmtApp.getIsSubAutoAppr();
        }
        //获取风控返回结果 pass:通过 1 reject:否决 0
        LmtSxkdPlusFksp lmtSxkdPlusFksp = lmtSxkdPlusFkspMapper.selectBySerno(lmtApp.getSerno());
        // 如果风控没有返回省心快贷自动化审批结果，则风控返回结果默认：否决 ：0
        if (lmtSxkdPlusFksp == null || StringUtils.isBlank(lmtSxkdPlusFksp.getApproveResult())) {
            approveResult = "0";
        } else {
            //只有省心快贷风险拦截结果和
            if ("1".equals(sxkdRiskResult) && "1".equals(isSubAutoAppr)) {
                // 获取风控自动审批结果和是否提交自动化审批都通过的情况下,才走省心快贷线上审批
                approveResult = "pass".equals(lmtSxkdPlusFksp.getApproveResult()) ? "1" : "0";
            } else {
                approveResult = "0";
            }
        }
        resultMap.put("sxkdRiskResult", sxkdRiskResult);
        resultMap.put("isSubAutoAppr", isSubAutoAppr);
        resultMap.put("approveResult", approveResult);
        resultMap.put("isGrp", isGrp);

        // 查询抵押方式
        QueryModel queryModel = new QueryModel();
        List<LmtAppSub> listSubs = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        String isFCDY = "N";
        String guarCusId = "";
        for (LmtAppSub lmtAppSub : listSubs) {
            queryModel.getCondition().put("serno", lmtAppSub.getSubSerno());
            List<GuarBizRelGuarBaseDto> guarBaseInfoList = guarBaseInfoService.queryGuarInfoSell(queryModel);
            for (GuarBizRelGuarBaseDto guarBizRelGuarBaseDto : guarBaseInfoList) {
                if (guarBizRelGuarBaseDto.getGuarTypeCd().startsWith("DY01")) {
                    isFCDY = "Y";
                    break;
                }
            }
            if ("Y".equals(isFCDY)) {
                break;
            }
            List<GuarBizRelGuaranteeDto> GuarGuaranteeList = guarGuaranteeService.queryGuarInfoSell(queryModel);
            for (GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : GuarGuaranteeList) {
                guarCusId = guarBizRelGuaranteeDto.getCusId();
                QueryModel queryModel1 = new QueryModel();
                queryModel1.getCondition().put("partnerType","2");
                queryModel1.getCondition().put("partnerNo",guarCusId);
                queryModel1.getCondition().put("coopPlanStatus","1");
                queryModel1.getCondition().put("oprType",CmisCommonConstants.LMT_OPER_OPT_TYPE_01);
                List<CoopPlanAccInfo> coopPlanAccInfoList = coopPlanAccInfoService.selectByModel(queryModel1);
                for(CoopPlanAccInfo coopPlanAccInfo:coopPlanAccInfoList){
                    QueryModel queryModel2 = new QueryModel();
                    queryModel2.getCondition().put("partnerNo",coopPlanAccInfo.getPartnerNo());
                    queryModel2.getCondition().put("coopPlanNo",coopPlanAccInfo.getCoopPlanNo());
                    queryModel2.getCondition().put("aptiLvl","10");
                    queryModel2.getCondition().put("partnerType","2");
                    queryModel2.getCondition().put("oprType",CmisCommonConstants.LMT_OPER_OPT_TYPE_01);
                    List<CoopPlanAptiInfoDto> coopPlanAptiInfoList =  coopPlanAptiInfoService.selectByModel(queryModel2);
                    if (coopPlanAptiInfoList != null && coopPlanAptiInfoList.size() > 0){
                            isFCDY = "Y";
                            break;
                    }
                }
                if ("Y".equals(isFCDY)) {
                    break;
                }
            }
            if ("Y".equals(isFCDY)) {
                break;
            }
        }
        resultMap.put("isFCDY", isFCDY);
        return resultMap;
    }

    /**
     * 根据本次流水号查询上次申请信息
     *
     * @param serno
     * @return
     */
    public LmtApp getLastSerno(String serno) {
        if (!StringUtils.isBlank(serno)) {
            LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
            String ogrigiLmtSerno = lmtApp.getOgrigiLmtSerno();
            return lmtAppMapper.selectBySerno(ogrigiLmtSerno);
        } else {
            return new LmtApp();
        }
    }

    /**
     * 复审任务发起次数控制分项拦截
     *
     * @param serno
     * @return
     */
    public Object riskItem0043(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map params = new HashMap();
        params.put("serno", serno);
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApp> list = this.selectByParams(params);
        if (list.size() != 1) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04103);
            return riskResultDto;
        } else {
            String origiLmtReplySerno = list.get(0).getOrigiLmtReplySerno();
            if (StringUtils.isBlank(origiLmtReplySerno)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            LmtReply lmtReplyAcc = lmtReplyService.queryLmtReplyByReplySerno(origiLmtReplySerno);
            if (lmtReplyAcc == null) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            } else {
                String lmtType = lmtReplyAcc.getLmtType();
                if (lmtType != "05") {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                } else {
                    LmtReply lmtReplyAcc1 = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyAcc.getOrigiLmtReplySerno());
                    if (lmtReplyAcc1 == null) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                        return riskResultDto;
                    } else {
                        lmtType = lmtReplyAcc1.getLmtType();
                        if (lmtType != "05") {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                        } else {
                            LmtReply lmtReplyAcc2 = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyAcc.getOrigiLmtReplySerno());
                            if (lmtReplyAcc2 == null) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                                return riskResultDto;
                            } else {
                                lmtType = lmtReplyAcc2.getLmtType();
                                if (lmtType != "05") {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                                } else {
                                    LmtReply lmtReplyAcc3 = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyAcc.getOrigiLmtReplySerno());
                                    if (lmtReplyAcc3 == null) {
                                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                                        return riskResultDto;
                                    } else {
                                        lmtType = lmtReplyAcc3.getLmtType();
                                        if (lmtType != "05") {
                                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                                        } else {
                                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04104);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;

    }

    /**
     * 根据分项流水号查询授信申请信息
     */
    public LmtApp getLmtAppBySubSerno(String subSerno) {
        LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(subSerno);
        if (lmtAppSub == null) {
            // 纯查询操作 不做处理
            return null;
        }
        LmtApp lmtApp = lmtAppMapper.selectBySerno(lmtAppSub.getSerno());
        if (lmtApp == null) {
            // 纯查询操作 不做处理
            return null;
        }
        return lmtApp;
    }

    /**
     * @方法名称: riskItem0072
     * @方法描述: 企业信用评分检验
     * @参数与返回说明:
     * @算法描述: 评分是否在650（含）分以上
     * @创建人: wangqing
     * @创建时间: 2021-8-3 10:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0072(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        Map paramsSub = new HashMap();
        LmtAppSubPrd lmtAppSub = new LmtAppSubPrd();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectLmtAppSubPrdByParams(paramsSub);

        if (CollectionUtils.nonEmpty(subListPrd)) {
            for (int i = 0; i < subListPrd.size(); i++) {
                lmtAppSub = subListPrd.get(i);
                BigDecimal qyxyScore = lmtAppSub.getQyxyScore();
                String lmtBizTypeProp = lmtAppSub.getLmtBizTypeProp();
                if ("P010".equals(lmtBizTypeProp) && qyxyScore.compareTo(new BigDecimal("650")) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04512);
                    return riskResultDto;
                }
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0023);
            return riskResultDto;
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: RiskItem0058
     * @方法描述: 两高一剩行业校验
     * @参数与返回说明:
     * @算法描述: 两高一剩行业校验：
     * （1）在单一客户授信时，校验改客户是否两高一剩行业，若是，则拦截
     * @创建人: yuanfs
     * @创建时间: 2021-08-17
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0058(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("两高一剩行业校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 查询授信申请信息
        LmtApp lmtApp = selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }
        // 两高一剩行业类别集合
        String twoHightOneLeftInd = "";
        //获取系统参数获取
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.TWO_HIGH_ONE_LEFT_IND);
        AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        if (Objects.isNull(adminSmPropDto) || StringUtils.isBlank(adminSmPropDto.getPropValue())) {
            // 棉印染精加工,毛染整精加工,麻染整精加工,丝印染精加工,化纤织物染整精加工,皮革鞣制加工,毛皮鞣制加工,
            // 木竹浆制造,非木竹浆制造,炼焦,无机酸制造,无机碱制造,电石*,甲醇*,有机硅单体*,黄磷*,氮肥制造,磷肥制造,电石法聚氯乙烯*,
            // 斜交轮胎*,力车胎*,水泥制造,平板玻璃制造,多晶硅*,炼铁,炼钢,铁合金冶炼,铝冶炼,金属船舶制造
            twoHightOneLeftInd = "C1713,C1723,C1733,C1743,C1752,C1910,C1931,C2211,C2212,C2520,C2611,C2612,C2613,C2614,C2614,C2619,C2621,C2622,C2651,C2911,C2911,C3011,C3041,C3099,C3110,C3120,C3150,C3216,C3731";
        } else {
            twoHightOneLeftInd = adminSmPropDto.getPropValue();
        }

        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            ResultDto<CusCorpDto> resultDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
            if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0100);
                return riskResultDto;
            }
            CusCorpDto cusCorpDto = resultDto.getData();
            if (StringUtils.isBlank(cusCorpDto.getTradeClass())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05802);
                return riskResultDto;
            }
            if (twoHightOneLeftInd.contains(cusCorpDto.getTradeClass())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05801);
                return riskResultDto;
            }
        }
        log.info("两高一剩行业校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 判断当前复议数据是否已复议3次
     */

    public Boolean isThreeFY(LmtApp lmtApp) throws Exception {
        boolean result = true;
        int curTimes = 1;
        // 获取复议次数
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.FY_TIMES);
        int fYTimes = Integer.parseInt(adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue());
        log.info("获取系统参数<复议次数>:" + fYTimes);
        // 获取复议天数 FY_DATE
        adminSmPropQueryDto.setPropName(CmisCommonConstants.FY_DATE);
        int fYDate = Integer.parseInt(adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue());
        log.info("获取系统参数<复议天数>:" + fYDate);
        // 查询当前客户的台账数据:批复生效日期+复议天数
        // 否决发起复议的情况时,取批复表数据中的批复生效日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date inureDate = new Date();
        log.info("区分否决与非否决状态的批复生效日期,当前数据审批状态为:" + lmtApp.getApproveStatus());
        if (CmisCommonConstants.WF_STATUS_998.equals(lmtApp.getApproveStatus())) {
            LmtReply lmtReply = lmtReplyService.queryLmtReplyBySerno(lmtApp.getSerno());
            log.info("否决发起复议时,取批复中的生效日期:" + JSON.toJSONString(lmtReply));
            inureDate = DateUtils.addDay(simpleDateFormat.parse(lmtReply.getReplyInureDate()), fYDate);
        } else {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(lmtApp.getCusId());
            log.info("非否决发起复议时,取批复台账中的生效日期:" + JSON.toJSONString(lmtReplyAcc));
            inureDate = DateUtils.addDay(simpleDateFormat.parse(lmtReplyAcc.getInureDate()), fYDate);
        }
        // 当前时间 取营业日期
        Date nowDate = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
        if (nowDate.after(inureDate)) {
            log.info("当前日期已超过当前数据复议的最大天数限制:" + fYDate);
            result = false;
        } else {
            if (!StringUtils.isBlank(lmtApp.getOrigiLmtReplySerno())) {
                String origiLmtReplySerno = lmtApp.getOrigiLmtReplySerno();
                while (curTimes < fYTimes) {
                    log.info("第{[]}次复议次数校验,批复编号为:", curTimes + 1);
                    LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtApp.getOrigiLmtReplySerno());
                    if (lmtReply != null) {
                        LmtApp lmtApp1 = lmtAppMapper.selectBySerno(lmtReply.getSerno());
                        if (lmtApp1 != null && CmisCommonConstants.LMT_TYPE_05.equals(lmtApp1.getLmtType()) && !StringUtils.isBlank(lmtApp1.getOrigiLmtReplySerno())) {
                            origiLmtReplySerno = lmtApp1.getOrigiLmtReplySerno();
                            curTimes++;
                            result = false;
                        } else {
                            result = true;
                            break;
                        }
                    } else {
                        result = true;
                        break;
                    }
                }

            }
        }
        return result;
    }

    /**
     * 通过流水号获取当前客户相关信息
     */

    public Map getGuarRingDataBySerno(String serno) {
        Map map = lmtAppMapper.getGuarRingDataBySerno(serno);
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus((String) map.get("cusId"));
        if (cusBaseClientDto != null) {
            map.put("certCode", cusBaseClientDto.getCertCode());
        }
        return map;
    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 20:18
     **/
    public void createImageSpplInfo(String serno, String bizType, LmtApp lmtApp, String instanceId, String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(serno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(serno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_03);// 影像补扫类型 03:授信类型资料补录
            docImageSpplClientDto.setSpplBizType(CmisCommonConstants.STD_SPPL_BIZ_TYPE_05);
            docImageSpplClientDto.setCusId(lmtApp.getCusId());// 客户号
            docImageSpplClientDto.setCusName(lmtApp.getCusName());// 客户名称
            docImageSpplClientDto.setInputId(lmtApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(lmtApp.getManagerBrId());// 登记机构
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), lmtApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", lmtApp.getCusName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * 获取内评低准入例外审批授信申请信息
     *
     * @param model
     * @return
     */
    public List<LmtApp> getNpGreenApp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.setSort("createTime desc");
        PageHelper.clearPage();
        List<LmtApp> list = selectByModel(model);
        return list;
    }

    /**
     * @函数名称: updateLmtApprChoose
     * @函数描述: 省心快贷对公智能风控提交审批
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> judgeSxkdOnlineAppr(Map<String, Object> params) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ResultDto<String> resultDto = new ResultDto<>();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String approveResult = ""; //风控自动审批结果 1成功 0否决
        // 获取授信申请信息
        LmtApp lmtApp = this.selectBySerno((String) params.get("serno"));
        int count = 0;
        String sxkdRiskResult = ""; //省心快贷风险拦截结果
        String isSubAutoAppr = ""; //是否提交自动化审批

        if (lmtApp != null) {
            //省心快贷风险拦截结果 1是0否
            sxkdRiskResult = lmtApp.getSxkdRiskResult();
            //是否提交自动化审批1是0否
            isSubAutoAppr = lmtApp.getIsSubAutoAppr();
            //获取风控返回结果
            LmtSxkdPlusFksp lmtSxkdPlusFksp = lmtSxkdPlusFkspMapper.selectBySerno((String) params.get("serno"));
            // 当省心快贷风险拦截结果为通过 是否提交自动化审批为是 且风控接口没有返回调用fb1150
            if ("1".equals(sxkdRiskResult) && "1".equals(isSubAutoAppr)) {
                if (Objects.isNull(lmtSxkdPlusFksp) || StringUtils.isBlank(lmtSxkdPlusFksp.getPkId())) {
                    String entCustName = "";//企业法人客户名称
                    String entCustNo = "";//企业法人身份证号码
                    String entCustNameId = "";//企业法人客户编号
                    String phone = "";//企业法人手机号
                    String spouseCusId = "";//企业法人配偶客户号
                    String spouseCode = "";//企业法人配偶身份证号码
                    String spouseName = "";//企业法人配偶客户名称
                    String spousePhone = "";///企业法人配偶手机号
                    String netAssets = "";//净资产
                    String salesIndicators = "";//销售指标
                    String orgCode = "";//企业统一社会信用代码
                    String managerId = lmtApp.getManagerId();
                    String managerBrId = lmtApp.getManagerBrId();
                    String managerName = "";
                    String orgName = "";
                    // 获取营业日期
                    Date openday = simpleDateFormat.parse(stringRedisTemplate.opsForValue().get("openDay"));
                    Date endDate = DateUtils.addMonth(openday, lmtApp.getLmtTerm());

                    BigDecimal appAmt = BigDecimal.ZERO;

                    // 获取企业统一社会信用代码
                    log.info("省心快贷对公智能风控提交审批: " + params.get("serno") + " 调用客户服务接口开始，请求报文：" + lmtApp.getCusId());
                    ResultDto<CusCorpDto> cusCorpDtoResultDto = icusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
                    log.info("省心快贷对公智能风控提交审批: " + params.get("serno") + " 调用客户服务接口结束，响应报文：" + cusCorpDtoResultDto.toString());

                    Map mapData = new HashMap();
                    mapData.put("cusId", lmtApp.getCusId());
                    mapData.put("certType", CmisCusConstants.STD_ECIF_CERT_TYP_R);
                    List<CusCorpCertDto> cusCorpCertDtoList = icusClientService.queryCusCorpCertDataByParams(mapData);
                    if (CollectionUtils.isEmpty(cusCorpCertDtoList)) {
                        orgCode = cusCorpDtoResultDto.getData().getLoanCardId();//中证码
                    } else {
                        orgCode = cusCorpCertDtoList.get(0).getCertCode();//企业统一社会信用代码
                    }

                    // 获取企业法人客户名称
                    log.info("根据客户号查询法人信息请求：" + lmtApp.getCusId());
                    // 通过客户号和高管类别得到公司法人代表信息
                    Map cusQueryMap = new HashMap();
                    cusQueryMap.put("cusIdRel", lmtApp.getCusId());
                    cusQueryMap.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
                    ResultDto<CusCorpMgrDto> cusCorpMgrDtoResultDto = cmisCusClientService.getCusCorpMgrByParams(cusQueryMap);
                    if (Objects.nonNull(cusCorpMgrDtoResultDto)) {
                        entCustName = cusCorpMgrDtoResultDto.getData().getMrgName();//企业法人
                        //entCustNameId = cusCorpMgrDtoResultDto.getData().getCusId();//企业法人客户号
                        entCustNo = cusCorpMgrDtoResultDto.getData().getMrgCertCode();//企业法人身份证
                    }
                    // 根据证件号获取客户基本信息 (对公高管人员信息表企业法人客户号与客户基本信息表客户号不一致，取客户基本信息表客户号)
                    CusBaseClientDto cusInfoDto = cmisCusClientService.queryCusByCertCode(cusCorpMgrDtoResultDto.getData().getMrgCertCode());
                    if (Objects.nonNull(cusInfoDto)) {
                        entCustNameId = cusInfoDto.getCusId();//企业法人客户号
                        log.info("根据证件号获取客户基本信息：" + entCustNameId);
                    }
                    // 获取企业法人的联系信息
                    CusIndivContactDto entCusIndiv = iCusClientService.queryCusIndivByCusId(entCustNameId);
                    if (Objects.nonNull(entCusIndiv)) {
                        phone = entCusIndiv.getMobileNo();//企业法人联系方式
                    }
                    CusIndivDto cusIndivDto = cmisCusClientService.queryCusindivByCusid(entCustNameId).getData();
                    log.info("根据客户号查询法人信息请求：" + cusCorpMgrDtoResultDto.toString());

                    // 判断法人代表婚姻情况
                    // 婚姻状况 已婚 :20
                    if (Objects.equals("20", cusIndivDto.getMarStatus())) {
                        log.info("根据证件编号查询是否有配偶请求：" + entCustNo);
                        ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(entCustNo);
                        log.info("根据证件编号查询是否有配偶返回：" + cmisCus0013RespDtoResultDto);
                        if (ResultDto.success().getCode().equals(cmisCus0013RespDtoResultDto.getCode())) {
                            CmisCus0013RespDto cmisCus0013RespDto = cmisCus0013RespDtoResultDto.getData();
                            if (Objects.nonNull(cmisCus0013RespDto)) {
                                spouseCusId = cmisCus0013RespDto.getCusId();
                                spouseCode = cmisCus0013RespDto.getCertCode();
                                spouseName = cmisCus0013RespDto.getName();

                            }
                        }
                        // 获取客户的配偶联系信息
                        CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(cmisCus0013RespDtoResultDto.getData().getCusId());
                        if (Objects.nonNull(cusIndivContactDto)) {
                            spousePhone = cusIndivContactDto.getMobileNo();
                        }
                    }
                    // 获取授信金额
                    appAmt = new BigDecimal("0.0").add(lmtApp.getLowRiskTotalLmtAmt()).add(lmtApp.getOpenTotalLmtAmt());

                    // 获取净资产
                    CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
                    Date buildDate = DateUtils.parseDate(cusCorpDto.getBuildDate(), "yyyy-MM-dd");
                    log.info("当前客户{【" + lmtApp.getCusId() + "】}成立时间" + buildDate);
                    ResultDto<Map<String, FinanIndicAnalyDto>> finanIndicAnalyDto = iCusClientService.getFinRepRetProAndRatOfLia(lmtApp.getCusId());
                    Map<String, FinanIndicAnalyDto> resultMap = finanIndicAnalyDto.getData();
                    log.info("当前客户{【" + lmtApp.getCusId() + "】}获取的财报信息{【" + JSON.toJSONString(finanIndicAnalyDto) + "】}");
                    // 获取销售指标
                    // 若未满1年的，取近一期的报表数据的值判断；
                    if (DateUtils.compare(DateUtils.addMonth(buildDate, 12), DateUtils.getCurrDate()) >= 0) {
                        // 获取当期销售指标
                        salesIndicators = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("salesIncome")).get().getCurYmValue()).toString();
                    } else {
                        // 获取去年销售指标
                        salesIndicators = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearSalesIncome")).get().getCurYmValue()).toString();
                    }
                    log.info("获取销售指标" + salesIndicators);
                    // 获取净资产 公式为净资产 = （期初所有者权益合计 + 期末所有者权益合计 ） / 2
                    // 期初所有者权益合计
                    BigDecimal statInitAmt = new BigDecimal(0);
                    // 期末所有者权益合计
                    BigDecimal statEndAmt = new BigDecimal(0);
                    statInitAmt = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("statInitAmt")).get().getCurYmValue());
                    statEndAmt = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("statEndAmt")).get().getCurYmValue());
                    netAssets = (statInitAmt.add(statEndAmt)).divide(new BigDecimal(2)).toString();
                    log.info("获取净资产" + netAssets);
                    //翻译用户名称和机构名称
                    if (org.apache.commons.lang.StringUtils.isNotEmpty(managerId) && org.apache.commons.lang.StringUtils.isNotEmpty(managerBrId)) {
                        AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(managerId);
                        log.info("fb1150：根据客户经理号【{}】查询用户信息结束,返回用户信息【{}】", managerId, JSON.toJSONString(adminSmUserDto));
                        if (Objects.nonNull(adminSmUserDto)) {
                            managerName = adminSmUserDto.getUserName();
                        }
                        AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(managerBrId);

                        log.info("fb1150：根据机构号【{}】查询机构信息结束,返回机构信息【{}】", managerBrId, JSON.toJSONString(adminSmOrgDto));
                        if (Objects.nonNull(adminSmOrgDto)) {
                            orgName = adminSmOrgDto.getOrgName();
                        }
                    }
                    // 获取出资人构成
                    log.info("fb1150：根据客户号获取出资人构成信息,根据客户号【{}】", lmtApp.getCusId());
                    ResultDto<List<CusCorpApitalDto>> cusCorpApitalDtoList = iCusClientService.selectByCusIdRel(lmtApp.getCusId());
                    List<cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.List> lists = new ArrayList<>();
                    if (cusCorpApitalDtoList != null) {
                        List<CusCorpApitalDto> cusCorpApitallist = cusCorpApitalDtoList.getData();
                        if (cusCorpApitallist != null && cusCorpApitallist.size() > 0) {
                            for (CusCorpApitalDto cusCorpApitalDto : cusCorpApitallist) {
                                log.info("fb1150：获取出资人姓名【{}】", cusCorpApitalDto.getInvtName());
                                cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.List shareHolderList = new cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.List();
                                shareHolderList.setShareholder(cusCorpApitalDto.getInvtName());
                                lists.add(shareHolderList);
                            }
                        }
                    }
                    log.info("fb1150：根据客户号获取出资人构成信息结束", lmtApp.getCusId());

                    /*
                     *************************************************************************
                     * 调用风控fb1150省心快贷plus授信申请
                     * *************************************************************************/
                    log.info("调用省心快贷plus授信申请开始,流水号【{}】", (String) params.get("serno"));
                    Fb1150ReqDto fb1150ReqDto = new Fb1150ReqDto();
                    fb1150ReqDto.setLmt_serno((String) params.get("serno"));//信贷授信申请流水号
                    fb1150ReqDto.setEnt_name(lmtApp.getCusName());//企业名称
                    fb1150ReqDto.setEnt_cust_id(lmtApp.getCusId());//企业核心客户号
                    fb1150ReqDto.setOrg_code(orgCode);//企业统一社会信用代码
                    fb1150ReqDto.setCust_name(entCustName);//企业法人客户名称
                    fb1150ReqDto.setCert_code(entCustNo);//企业法人身份证号码
                    fb1150ReqDto.setCust_id(entCustNameId);//企业法人核心客户号
                    fb1150ReqDto.setPhone(phone);//企业法人手机号
                    fb1150ReqDto.setSpouse_name(spouseName);//企业法人配偶客户名称
                    fb1150ReqDto.setSpouse_code(spouseCode);//企业法人配偶身份证号码
                    fb1150ReqDto.setSpouse_cust_id(spouseCusId);//企业法人配偶核心客户号
                    fb1150ReqDto.setSpouse_phone(spousePhone);//企业法人配偶手机号
                    fb1150ReqDto.setNet_assets(netAssets);//净资产
                    fb1150ReqDto.setSales_indicators(salesIndicators);//销售指标
                    fb1150ReqDto.setAppamt(appAmt.toString());//申请金额
                    fb1150ReqDto.setSxstartdate(simpleDateFormat.format(openday));//授信起始日期
                    fb1150ReqDto.setSxenddate(simpleDateFormat.format(endDate));//授信到期日期
                    fb1150ReqDto.setManager_id(lmtApp.getManagerId());//管户经理ID
                    fb1150ReqDto.setManager_name(managerName);//管户经理名称
                    fb1150ReqDto.setManager_org_id(lmtApp.getManagerBrId());//管户经理所属机构ID
                    fb1150ReqDto.setManager_org_name(orgName);//管户经理所属机构名称
                    fb1150ReqDto.setList(lists);
                    log.info("调用省心快贷plus授信申请请求报文：" + fb1150ReqDto);
                    ResultDto<Fb1150RespDto> fb1150RespResultDto = dscms2CircpClientService.fb1150(fb1150ReqDto);
                    log.info("调用省心快贷plus授信申请返回：" + fb1150RespResultDto);
                    String fb1174Code = Optional.ofNullable(fb1150RespResultDto.getCode()).orElse(StringUtils.EMPTY);
                    String fb1174Meesage = Optional.ofNullable(fb1150RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
                    log.info("调用省心快贷plus授信申请返回信息：" + fb1174Meesage);
                    if (Objects.equals(fb1174Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //1.删除省心快贷风控返回信息主表
                        lmtSxkdPlusFkspMapper.deleteByLmtSerno((String) params.get("serno"));
                        //2.插入省心快贷风控返回信息主表
                        Map dataMap = new HashMap();
                        String pkValue = UUID.randomUUID().toString();
                        dataMap.put("pkId", pkValue);
                        dataMap.put("serno", (String) params.get("serno"));
                        dataMap.put("approveResult", "");
                        dataMap.put("approveResultName", "");
                        lmtSxkdPlusFkspMapper.insertByMap(dataMap);

                        resultDto.setCode(fb1174Code);
                        resultDto.setMessage(fb1174Meesage);
                    } else {
                        resultDto.setCode(fb1174Code);
                        resultDto.setMessage(fb1174Meesage);
                    }
                }
                // 发送接口后再次获取风控自动审批结果
                lmtSxkdPlusFksp = lmtSxkdPlusFkspMapper.selectBySerno((String) params.get("serno"));
                // 若查询到返回结果为空，则风控自动审批结果的路由结果不更新
                if (Objects.isNull(lmtSxkdPlusFksp) || StringUtils.isBlank(lmtSxkdPlusFksp.getApproveResult())) {
                    approveResult = "0";
                } else {
                    ResultInstanceDto resultInstanceDto = JSONObject.parseObject((String) params.get("instanceIdInfo"), ResultInstanceDto.class);
                    WFBizParamDto wfParams = new WFBizParamDto();
                    wfParams.setBizId(resultInstanceDto.getBizId());
                    wfParams.setInstanceId(resultInstanceDto.getInstanceId());
                    Map<String, Object> routerParams = new HashMap<>();
                    routerParams = this.getRouterMapResult((String) params.get("serno"));
                    wfParams.setParam(routerParams);
                    workflowCoreClient.updateFlowParam(wfParams);
                    log.info("更新流程路由节点成功");
                }
            }
        }
        return resultDto;
    }


    /**
     * @方法名称: getApproveResult
     * @方法描述: 根据授信申请流水, 查询省心快贷plus自动审批风控返回结果
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-10-07 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map getApproveResult(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String approveResult = "0"; //风控审批结果
        String sxkdRiskResult = ""; //省心快贷风险拦截结果
        String isSubAutoAppr = ""; //是否提交自动化审批
        try {
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            log.info(String.format("根据授信申请流水,查询省心快贷plus自动审批风控返回结果开始", serno));
            // 获取授信申请信息
            LmtApp lmtApp = this.selectBySerno(serno);
            log.info(String.format("根据授信申请流水, 获取授信申请信息", serno));
            if (Objects.nonNull(lmtApp)) {
                //省心快贷风险拦截结果 1是0否
                sxkdRiskResult = lmtApp.getSxkdRiskResult();
                //是否提交自动化审批1是0否
                isSubAutoAppr = lmtApp.getIsSubAutoAppr();
                // 当省心快贷风险拦截结果为通过 是否提交自动化审批为是
                if ("1".equals(sxkdRiskResult) && "1".equals(isSubAutoAppr)) {
                    //获取风控自动审批结果
                    LmtSxkdPlusFksp lmtSxkdPlusFksp = lmtSxkdPlusFkspMapper.selectBySerno(serno);
                    // 若查询到返回结果为空，则风控自动审批结果的路由结果不更新
                    if (Objects.isNull(lmtSxkdPlusFksp) || StringUtils.isBlank(lmtSxkdPlusFksp.getApproveResult())) {
                        approveResult = "0";
                    } else {
                        // 获取风控自动审批结果和是否提交自动化审批都通过的情况下,才走省心快贷线上审批
                        approveResult = "pass".equals(lmtSxkdPlusFksp.getApproveResult()) ? "1" : "0";
                    }
                }
            } else {
                log.info(String.format("根据授信申请流水, 获取授信申请信息失败", serno));
                rtnCode = EcbEnum.ECB010079.key;
                rtnMsg = EcbEnum.ECB010079.value;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("根据授信申请流水获取省心快贷plus自动审批返回结果！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("approveResult", approveResult);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @作者:lizx
     * @方法名称: sendWbMsgNotice
     * @方法描述: 推送首页提醒事项
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 15:36
     * @return:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendWbMsgNotice(LmtApp lmtapp, String cusName, String bizTypeName, String managerId) throws Exception {
        try {
            log.info("业务类型【{}】流水号【{}】处理通知首页提醒事项处理", lmtapp.getSerno());
            Map<String, String> map = new HashMap<>();
            map.put("cusName", cusName);
            map.put("prdName", bizTypeName);
            AdminSmUserDto adminSmUserDto = adminSmUserService.getByLoginCode(managerId).getData();
            sendMessage.sendMessage("MSG_ZJ_M_0001", map, "1", managerId, adminSmUserDto.getUserMobilephone());
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    /**
     * @作者:zhuzr
     * @方法名称: passForSGAndDH
     * @方法描述: 东海寿光村镇直接通过、更新表数据
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/29 00:36
     * @return:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map passForSGAndDH(String serno) throws Exception {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        // 1.将审批状态更新为997 通过
        LmtApp lmtApp = lmtAppMapper.selectBySerno(serno);
        lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.update(lmtApp);
        log.info("更新授信申请" + serno + "的流程状态为997");

        if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
            // 如果集团子成员授信流程， 则更新关联表中的客户经理提交状态
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(serno);
            lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_997);
            lmtGrpMemRelService.update(lmtGrpMemRel);
        }
        rtnData.put("rtnCode", rtnCode);
        rtnData.put("rtnMsg", rtnMsg);
        return rtnData;
    }


    public void test() {
        String a = "P011,P019,P032,P009,P013,P014,P016";
        String[] as = a.split(",");
        if (Arrays.stream(as).anyMatch(s -> "P004".equals(s))) {
            System.out.println(" yes ");
        } else {
            System.out.println(" no ");
        }
    }
}
