/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditAuthbookInfo
 * @类描述: credit_authbook_info数据实体类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-26 15:05:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_authbook_info")
public class CreditAuthbookInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 授权业务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CAI_SERNO")
	private String caiSerno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;

	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = false, length = 30)
	private String certCode;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 300)
	private String cusName;
	
	/** 征信查询类别 **/
	@Column(name = "QRY_CLS", unique = false, nullable = false, length = 5)
	private String qryCls;
	
	/** 授权书编号 **/
	@Column(name = "AUTHBOOK_NO", unique = false, nullable = false, length = 40)
	private String authbookNo;
	
	/** 授权书日期 **/
	@Column(name = "AUTHBOOK_DATE", unique = false, nullable = false, length = 10)
	private String authbookDate;
	
	/** 授权书内容 **/
	@Column(name = "AUTHBOOK_CONTENT", unique = false, nullable = false, length = 40)
	private String authbookContent;
	
	/** 其他授权书内容 **/
	@Column(name = "OTHER_AUTHBOOK_CONTENT", unique = false, nullable = false, length = 256)
	private String otherAuthbookContent;
	
	/** 授权方式 **/
	@Column(name = "AUTH_MODE", unique = false, nullable = false, length = 5)
	private String authMode;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 40)
	private String imageNo;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @param caiSerno
	 */
	public void setCaiSerno(String caiSerno) {
		this.caiSerno = caiSerno;
	}
	
    /**
     * @return caiSerno
     */
	public String getCaiSerno() {
		return this.caiSerno;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param qryCls
	 */
	public void setQryCls(String qryCls) {
		this.qryCls = qryCls;
	}
	
    /**
     * @return qryCls
     */
	public String getQryCls() {
		return this.qryCls;
	}
	
	/**
	 * @param authbookNo
	 */
	public void setAuthbookNo(String authbookNo) {
		this.authbookNo = authbookNo;
	}
	
    /**
     * @return authbookNo
     */
	public String getAuthbookNo() {
		return this.authbookNo;
	}
	
	/**
	 * @param authbookDate
	 */
	public void setAuthbookDate(String authbookDate) {
		this.authbookDate = authbookDate;
	}
	
    /**
     * @return authbookDate
     */
	public String getAuthbookDate() {
		return this.authbookDate;
	}
	
	/**
	 * @param authbookContent
	 */
	public void setAuthbookContent(String authbookContent) {
		this.authbookContent = authbookContent;
	}
	
    /**
     * @return authbookContent
     */
	public String getAuthbookContent() {
		return this.authbookContent;
	}
	
	/**
	 * @param otherAuthbookContent
	 */
	public void setOtherAuthbookContent(String otherAuthbookContent) {
		this.otherAuthbookContent = otherAuthbookContent;
	}
	
    /**
     * @return otherAuthbookContent
     */
	public String getOtherAuthbookContent() {
		return this.otherAuthbookContent;
	}
	
	/**
	 * @param authMode
	 */
	public void setAuthMode(String authMode) {
		this.authMode = authMode;
	}
	
    /**
     * @return authMode
     */
	public String getAuthMode() {
		return this.authMode;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
}