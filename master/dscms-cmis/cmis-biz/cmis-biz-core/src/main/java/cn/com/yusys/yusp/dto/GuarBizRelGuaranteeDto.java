package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRel
 * @类描述: GUAR_BIZ_REL数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarBizRelGuaranteeDto implements Serializable{

	private static final long serialVersionUID = 1L;

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}


	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCerType() {
		return cerType;
	}

	public void setCerType(String cerType) {
		this.cerType = cerType;
	}

	public String getMarry() {
		return marry;
	}

	public void setMarry(String marry) {
		this.marry = marry;
	}

	public String getGuarAmt() {
		return guarAmt;
	}

	public void setGuarAmt(String guarAmt) {
		this.guarAmt = guarAmt;
	}

	public String getBusiness() {
		return Business;
	}

	public void setBusiness(String business) {
		Business = business;
	}

	public String getCusTyp() {
		return cusTyp;
	}

	public void setCusTyp(String cusTyp) {
		this.cusTyp = cusTyp;
	}

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getIsAddGuar() {
		return isAddGuar;
	}

	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar;
	}

	/** 主键 **/
	private String pkId;

	/** 业务流水号 **/
	private String serno;

	public String getGuarantyId() {
		return guarantyId;
	}

	public void setGuarantyId(String guarantyId) {
		this.guarantyId = guarantyId;
	}

	/** 押品编号 **/
	private String guarantyId;

	/** 客户号 **/
	private String cusId;

	/** 客户名称 **/
	private String assureName;

	/** 性别 **/
	private String sex;

	/** 证件类型 **/
	private String cerType;

	public String getAssureName() {
		return assureName;
	}

	public void setAssureName(String assureName) {
		this.assureName = assureName;
	}

	public String getAssureCertCode() {
		return assureCertCode;
	}

	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode;
	}

	public String getGuarantyType() {
		return guarantyType;
	}

	public void setGuarantyType(String guarantyType) {
		this.guarantyType = guarantyType;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	/** 证件号码 **/
	private String assureCertCode;

	/** 婚姻状况 **/
	private String marry;

	/** 担保金额 **/
	private String guarAmt;

	/** 工作单位 **/
	private String Business;

	/** 担保人类型 **/
	private String cusTyp;


	/** 是否追加担保 **/
	private String isAddGuar;

	/**
	 * 保证担保形式
	 **/
	private String guarantyType;

	/*
	* 统一业务流水号
	* */
	private String bizSerno;
}