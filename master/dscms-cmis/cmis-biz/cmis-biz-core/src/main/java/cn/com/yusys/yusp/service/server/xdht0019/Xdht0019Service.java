package cn.com.yusys.yusp.service.server.xdht0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdht0019.req.Xdht0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0019.resp.Xdht0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.DiscCouponBusinRelMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.RepayDayRecordInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0007.CmisCus0007Service;
import cn.com.yusys.yusp.service.server.xdcz0016.Xdcz0016Service;
import cn.com.yusys.yusp.util.IDCardUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 合同生成
 */
@Service
public class Xdht0019Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0019Service.class);

    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private CmisCus0007Service cmisCus0007Service;//业务逻辑处理类：查询个人客户基本信息
    @Autowired
    private SenddxService senddxService;
    @Resource
    private ICusClientService icusClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Resource
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private Dscms2XwhClientService dscms2XwhClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private DiscCouponBusinRelMapper discCouponBusinRelMapper;
    @Autowired
    private Xdcz0016Service xdcz0016Service;
    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    /**
     * 合同生成
     *
     * @param xdht0019DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0019DataRespDto xdht0019(Xdht0019DataReqDto xdht0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value);
        Xdht0019DataRespDto xdht0019DataRespDto = new Xdht0019DataRespDto();
        String certNo = xdht0019DataReqDto.getCertNo();//客户证件号
        String loanType = xdht0019DataReqDto.getLoanType();//贷款类型
        BigDecimal applyAmt = Optional.ofNullable(xdht0019DataReqDto.getApplyAmt()).orElse(BigDecimal.ZERO);//申请金额
        String startDate = xdht0019DataReqDto.getStartDate();//起始日
        String endDate = xdht0019DataReqDto.getEndDate();//到期日
        String rate = Optional.ofNullable(xdht0019DataReqDto.getRate()).orElse("0");//利率
        String cusId = xdht0019DataReqDto.getCusId();//所在公司客户号
        String preferCode = xdht0019DataReqDto.getPreferCode();//优惠券号
        BigDecimal rateb = BigDecimal.ZERO;
        if (StringUtils.isNotBlank(rate) && rate.contains("%")) {
            rate = rate.replaceAll("%", StringUtils.EMPTY);
            rateb = new BigDecimal(rate).divide(new BigDecimal("100"), 9, BigDecimal.ROUND_HALF_UP);//利率处理
        } else {
            rateb = new BigDecimal(rate).divide(new BigDecimal("1"), 9, BigDecimal.ROUND_HALF_UP);//利率处理
        }
        // 响应Data：合同生成
        String contNo = StringUtils.EMPTY;// 合同号
        //映射取产品编号
        String prd_id = "";
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_1.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_1.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_2.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_2.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_3.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_3.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_4.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_4.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_5.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_5.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_6.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_6.value;
        if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_7.key, loanType))
            prd_id = DscmsBizHtEnum.XDHT0019_LOANTYPE_7.value;
        //获取产品名称
        Xdqt0001DataReqDto xdqt0001DataReqDto = new Xdqt0001DataReqDto();
        xdqt0001DataReqDto.setPrdId(prd_id);
        ResultDto<Xdqt0001DataRespDto> resultDtoPrd = dscmsCfgQtClientService.xdqt0001(xdqt0001DataReqDto);
        Xdqt0001DataRespDto xdqt0001DataRespDto = resultDtoPrd.getData();
        String prd_name = xdqt0001DataRespDto.getPrdName();

        try {
            /** 贷款形式 1--新增 6--无还本续贷 默认1**/
            String loanModal = "1";
            // 起始日和到期日之间的月份，格式为yyyyMMdd
            int monthsByTwoDates = DateUtils.getMonthsByTwoDates(startDate, "yyyyMMdd", endDate, "yyyyMMdd");

            if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_2.key, loanType) || Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_3.key, loanType)) {
                // 根据身份证第17位来判断性别
                boolean idCardValidFlag = IDCardUtils.validator(certNo);
                String gender = StringUtils.EMPTY;//性别 男：M ，女：F
                if (idCardValidFlag) {
                    gender = IDCardUtils.getGenderByIdCard(certNo);
                } else {
                    String ecb10033Value = EcbEnum.ECB010033.value + certNo;//校验身份证号码[{}]未通过
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, ecb10033Value);
                    throw BizException.error(null, EcbEnum.ECB010033.key, ecb10033Value);
                }
                // 根据性别来判断60岁还是55岁退休
                if (Objects.equals(gender, "M")) {//性别 男：M
                    int ageM = IDCardUtils.getAgeByIdCard(certNo);
                    int age60 = 720;
                    if (ageM * 12 + monthsByTwoDates > age60) {
                        logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, EcbEnum.ECB010034.value);
                        throw BizException.error(null, EcbEnum.ECB010034.key, EcbEnum.ECB010034.value);//合同到期日不能大于用户60岁生日
                    }
                } else if (Objects.equals(gender, "F")) {//性别 女：F
                    int ageF = IDCardUtils.getAgeByIdCard(certNo);
                    int age55 = 12 * 55;
                    if (ageF * 12 + monthsByTwoDates > age55) {
                        logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, EcbEnum.ECB010034.value);
                        throw BizException.error(null, EcbEnum.ECB010035.key, EcbEnum.ECB010035.value);//合同到期日不能大于用户55岁生日
                    }
                }
            }
            String approvalResultSerno = "";
            String surveySerno = "";

            //根据借款人证件号certNo 获取借款人客户号 调用cus服务
            CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
            cusId = cusBaseClientDto.getCusId();
            String borrower_cus_id = cusBaseClientDto.getCusId();
            String manager_id = cusBaseClientDto.getManagerId();
            String manager_br_id = cusBaseClientDto.getManagerBrId();
            String cusName = cusBaseClientDto.getCusName();

            /***************优农贷、优企贷、惠享贷产品互斥校验开始******************/
            if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_4.key, loanType) || Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_5.key, loanType)) {//优企、优农
                Map contNoParamMap = new HashMap();
                contNoParamMap.put("cusId", cusId);
                contNoParamMap.put("prdId", prd_id);
                int count = ctrLoanContService.quertOtherPrdIdContSum(contNoParamMap);
                if (count > 0) {//存在
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, EcbEnum.ECB020028.value);
                    throw BizException.error(null, EpbEnum.EPB099999.key, EcbEnum.ECB020028.value);//该客户所在企业贷款历史总额已超过5千万，无法申请工资贷
                }
            }
            /***************优农贷、优企贷、惠享贷产品互斥校验结束******************/
            // 贷款期限
            int loanTerms = monthsByTwoDates;
            BigDecimal contAmt;// 合同金额 = 申请金额 *期限
            if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_2.key, loanType)) {
                //loanType=2;022032工资贷
                contAmt = NumberUtils.multiply(applyAmt, loanTerms);
                /* 规则：合同金额 大于10万元 则合同金额为10万元 */
                if (contAmt.compareTo(new BigDecimal(100000)) > 0) contAmt = new BigDecimal(100000);
                /*针对工资贷判断该检测公司资质*/
                /* 规则1：该客户所在企业是否有逾期、欠息贷款 */
                Map<String, String> accLoanQueryMap = new HashMap<>();
                accLoanQueryMap.put("cusId", cusId);
                accLoanQueryMap.put("loanEndDate", endDate);
                int result = accLoanService.queryAccLoanDebitInit(accLoanQueryMap);
                if (result > 0) {
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, EcbEnum.ECB010036.value);
                    throw BizException.error(null, EcbEnum.ECB010036.key, EcbEnum.ECB010036.value);
                }
                /*规则2：该客户所在企业贷款历史总额是否已超过5千万*/
                BigDecimal entHisAmt = accLoanService.getHisLoanAmt(cusId);
                if (entHisAmt.compareTo(new BigDecimal(50000000)) > 0) {
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, EcbEnum.ECB010038.value);
                    throw BizException.error(null, EcbEnum.ECB010038.key, EcbEnum.ECB010038.value);//该客户所在企业贷款历史总额已超过5千万，无法申请工资贷
                }

            } else if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_3.key, loanType)) {
                //loanType=022033公积金贷
                contAmt = NumberUtils.multiply(applyAmt, 30);
                if (contAmt.compareTo(new BigDecimal(300000)) > 0) contAmt = new BigDecimal(300000);
            } else if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_4.key, loanType)
                    || Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_5.key, loanType)) {//优企贷 优农贷
                //校验小贷产品合同与决议是否一致
                contAmt = applyAmt;
                LmtCrdReplyInfo lmtCrdReplyInfo = checkXdPrdResult(startDate, cusId, applyAmt, rateb, prd_id);
                surveySerno = lmtCrdReplyInfo.getSurveySerno();
                approvalResultSerno = lmtCrdReplyInfo.getReplySerno();
                // 判断合同是否为优企贷续贷 确定贷款形式 1--新增 6--无还本续贷
                loanModal = Objects.equals(lmtCrdReplyInfo.getIsWxbxd(), "1") ? "6" : loanModal;
            } else {
                contAmt = applyAmt;
            }
            //计算日息
            BigDecimal contAmt1 = null;
            BigDecimal avgRate = NumberUtils.divide(NumberUtils.multiply(contAmt1, rateb), new BigDecimal(360 * 100));//100*0.01/360  日息

            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            //判断合同是否重复等
            if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_4.key, loanType) || Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_7.key, loanType)) {
                Map contParamMap = new HashMap();
                contParamMap.put("openday", openDay);
                contParamMap.put("borrower_cus_id", borrower_cus_id);
                contParamMap.put("loanType", loanType);
                int ts = ctrLoanContService.quertUnSignContSum(contParamMap);
                if (ts > 0) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "该客户已经存在未签订合同，请撤消后再申请！");
                }

                HashMap contResult = ctrLoanContService.quertContByDate(contParamMap);
                if (contResult != null) {
                    String cont_no = (String) contResult.get("cont_no");
                    String sign_date = (String) contResult.get("sign_date");
                    String existContAmt = (String) contResult.get("cont_amt");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date sign_date_date = sdf.parse(sign_date);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sign_date_date);
                    cal.add(Calendar.DAY_OF_MONTH, -30);//30天前
                    String signDate30After = sdf.format(cal.getTime());

                    if (openDay.compareTo(signDate30After) <= 0) {
                        //如果当前日期<=合同签订日sign_date+30
                        BigDecimal existAcc = accLoanService.checkCusNotClosedAccByCusId(cusId);
                        if (existAcc.compareTo(BigDecimal.ZERO) > 0) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "该客户已经存在未结清的借据，请结清后再申请！");
                        }
                    }

                    // 获取合同可用额度
                    BigDecimal lmt = new BigDecimal(existContAmt).subtract(accLoanService.selectSumLoanBalAmtByParams(cont_no, "'1', '2', '5'".split(",")));
                    //校验合同可用金额是否超出，如果超出返回并提示："本次出账金额【a】元，已超合同可用额度(包括在途出账记录)【b】元！"
                    if (lmt.compareTo(BigDecimal.ZERO) > 0) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "该客户现有合同还有额度可用，不用申请新合同！");
                    }
                }

                if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_7.key, loanType)) {//增享贷
                    // 查询客户基本信息
                    Xdkh0001DataRespDto xdkh0001DataRespDto = Optional.ofNullable(xdcz0016Service.queryXdkh0001DataRespDto(borrower_cus_id)).orElse(new Xdkh0001DataRespDto());
                    //客户详细信息
                    CusIndivDto cusIndiv = queryCusIndiv(certNo);
                    //要素查询
                    String indivComFld = xdkh0001DataRespDto.getIndivComTrade();//单位所属行业
                    String indivComFldName = xdkh0001DataRespDto.getIndivComFldName();//单位所属名称
                    String cusType = cusIndiv.getCusType();
                    if (StringUtil.isEmpty(indivComFld)) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "客户单位所属行业为空，请先通知客户经理维护您的客户信息！");
                    } else if (StringUtil.isEmpty(cusType)) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "请联系客户经理前往小微信贷系统维护客户类型！");
                    } else if ("110".equals(cusType) && applyAmt.compareTo(new BigDecimal(300000)) > 0) {//借款人为一般自然人且合同金额>=300000元
                        throw BizException.error(null, EpbEnum.EPB099999.key, "借款人为一般自然人且合同金额大于300000元!");
                    }

                    //验小贷产品合同与决议是否一致
                    LmtCrdReplyInfo lmtCrdReplyInfo = checkXdPrdResult(startDate, cusId, applyAmt, rateb, prd_id);
                    surveySerno = lmtCrdReplyInfo.getSurveySerno();
                    approvalResultSerno = lmtCrdReplyInfo.getReplySerno();
                    // 判断合同是否为优企贷续贷 确定贷款形式 1--新增 6--无还本续贷
                    loanModal = Objects.equals(lmtCrdReplyInfo.getIsWxbxd(), "1") ? "6" : loanModal;

                }
            } else if (Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_5.key, loanType) || Objects.equals(DscmsBizHtEnum.XDHT0019_LOANTYPE_6.key, loanType)) {
                Map contParamMap = new HashMap();
                contParamMap.put("openday", openDay);
                contParamMap.put("borrower_cus_id", borrower_cus_id);
                contParamMap.put("loanType", loanType);
                int ts = ctrLoanContService.quertUnSignContSumByPf(contParamMap);
                if (ts > 0) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "该客户已经存在未签订合同，请撤消后再申请！");
                }
                HashMap contResult = ctrLoanContService.quertContByDateByPf(contParamMap);
                if (Objects.nonNull(contResult)) {
                    String cont_no = (String) contResult.get("cont_no");
                    String sign_date = (String) contResult.get("sign_date");
                    String existContAmt = (String) contResult.get("cont_amt");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date sign_date_date = sdf.parse(sign_date);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sign_date_date);
                    cal.add(Calendar.DAY_OF_MONTH, -30);//30天前
                    String signDate30After = sdf.format(cal.getTime());
                    if (openDay.compareTo(signDate30After) <= 0) {
                        //如果当前日期<=合同签订日sign_date+30
                        BigDecimal existAcc = accLoanService.checkCusNotClosedAccByCusId(cusId);
                        if (existAcc.compareTo(BigDecimal.ZERO) > 0) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "该客户已经存在未结清的借据，请结清后再申请！");
                        }
                    }
                    // 获取合同可用额度
                    BigDecimal lmt = new BigDecimal(existContAmt).subtract(accLoanService.selectSumLoanBalAmtByParams(cont_no, "'1', '2', '5'".split(",")));
                    //校验合同可用金额是否超出，如果超出返回并提示："本次出账金额【a】元，已超合同可用额度(包括在途出账记录)【b】元！"
                    if (lmt.compareTo(BigDecimal.ZERO) > 0) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "该客户现有合同还有额度可用，不用申请新合同！");
                    }
                }
                //验小贷产品合同与决议是否一致
                LmtCrdReplyInfo lmtCrdReplyInfo = checkXdPrdResult(startDate, cusId, applyAmt, rateb, prd_id);
                surveySerno = lmtCrdReplyInfo.getSurveySerno();
                approvalResultSerno = lmtCrdReplyInfo.getReplySerno();
            } else {
                //先用这个语句查询
                Map contParamMap = new HashMap();
                contParamMap.put("openday", openDay);
                contParamMap.put("borrower_cus_id", borrower_cus_id);
                contParamMap.put("loanType", loanType);
                int ts = ctrLoanContService.quertContSum(contParamMap);
                if (ts > 0) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "该客户已经存在合同，请撤消后再申请！");
                }
            }

            // 查询客户基本信息
            CusIndivDto cusIndiv = queryCusIndiv(certNo);
            //客户基本信息
            Xdkh0001DataRespDto xdkh0001DataRespDto = Optional.ofNullable(xdcz0016Service.queryXdkh0001DataRespDto(borrower_cus_id)).orElse(new Xdkh0001DataRespDto());
            String cusType = cusIndiv.getCusType();//客户类型
            String agriFlg = cusIndiv.getAgriFlg();//是否为农户
            String loanDirection = xdkh0001DataRespDto.getIndivComTrade();
            String firstIndustryCode = "";
            if (!"".equals(loanDirection)) {
                firstIndustryCode = loanDirection.substring(0, 1);
            }
            /*****************是否为农户检查*****************/
            if (StringUtil.isNotEmpty(cusId)) {
                CusIndivContactDto cusIndivContactDto = Optional.ofNullable(icusClientService.queryCusIndivByCusId(cusId)).orElse(new CusIndivContactDto());
                agriFlg = cusIndivContactDto.getIsAgri();//是否为农户
            }

            String areaCode = xdkh0001DataRespDto.getRegionalism();//居住区域编号
            String areaName = xdkh0001DataRespDto.getRegionName();//居住区域名称
            String indivComName = xdkh0001DataRespDto.getUnitName();//经营企业名称/工作单位
            String indivComFld = xdkh0001DataRespDto.getIndivComTrade();//单位所属行业
            String indivComFldName = xdkh0001DataRespDto.getIndivComFldName();//单位所属名称
            String phone = xdkh0001DataRespDto.getMobileNo();
            if ("5".equals(loanType)) {

                //优农贷准入要求判断
                logger.info("**********XDHT0019:优农贷准入要求判断;校验客户农户属性:" + agriFlg + ";客户类型:" + cusType + ";行业为农林牧渔:" + firstIndustryCode);
                //校验优农贷客户属性：农户，客户类型小微企业主、一般自然人，行业为农林牧渔  老信贷通过xd_prd_class_rel判断，此处写死判断
                int num = 0;
                //客户属性：农户
                if (!"1".equals(agriFlg)) {
                    num++;
                }
                //客户类型小微企业主、一般自然人、个体工商户无字号
                if (!"110".equals(cusType) && !"150".equals(cusType) && !"120".equals(cusType)) {
                    num++;
                }
                //行业为农林牧渔
                if (!"A".equals(firstIndustryCode)) {
                    num++;
                }
                if (num > 0) {
                    // 您不满足优农贷准入要求，请联系客户经理维护您的个人信息
                    throw BizException.error(null, EcbEnum.ECB010041.key, EcbEnum.ECB010041.value);
                }
            } else if ("4".equals(loanType)) {
                // 对客户基本信息进行校验
                boolean pass = true;
                String msg = "";
                if (StringUtils.isBlank(areaCode)) {
                    msg = "居住区域编码为空！";
                    pass = false;
                }
                //新信贷客户信息不存区域名称
//                if (StringUtils.isBlank(areaName)) {
//                    msg = "居住区域名称为空！";
//                    pass = false;
//                }
                if (StringUtils.isBlank(agriFlg)) {
                    msg = "是否农户为空！";
                    pass = false;
                }
                if (StringUtils.isBlank(indivComName)) {
                    msg = "经营企业名称/工作单位为空！";
                    pass = false;
                }
                if (StringUtils.isBlank(indivComFld)) {
                    msg = "单位所属行业为空！";
                    pass = false;
                }
                if (StringUtils.isBlank(indivComFldName)) {
                    msg = "单位所属行业名称为空！";
                    pass = false;
                }
                if (!"150".equals(cusType)) {
                    msg = "客户类型非小微企业主！";
                    pass = false;
                }

                ResultDto<Integer> integerResultDto = dscmsCfgClientService.queryAreaNum(areaCode, areaCode);
                int areaNum = 0;
                if (integerResultDto.getCode().equals("0")) {
                    areaNum = integerResultDto.getData();
                }
                if (areaNum > 0) {
                    // 居住区域编码和是否农户不一致
                    throw BizException.error(null, null, null);
                }

                //如果校验失败的话，调用发送短信方法 
                if (!pass) {
                    String telnum = "";
                    ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(manager_id);
                    if (manager.getData() == null) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "未查询到相关信息！");
                    }
                    telnum = manager.getData().getUserMobilephone();

                    String sendMessage = "您的客户" + cusName + "合同签订时，" + msg + "请及时联系处理";
                    SenddxReqDto senddxReqDto = new SenddxReqDto();
                    senddxReqDto.setInfopt("dx");
                    SenddxReqList senddxReqList = new SenddxReqList();
                    senddxReqList.setMobile(telnum);
                    senddxReqList.setSmstxt(sendMessage);
                    ArrayList<SenddxReqList> list = new ArrayList<>();
                    list.add(senddxReqList);
                    senddxReqDto.setSenddxReqList(list);
                    if (StringUtils.isNotBlank(telnum)) {//电话不为空
                        senddxService.senddx(senddxReqDto);
                    }
                    // 抛出异常
                    throw BizException.error(null, EpbEnum.EPB099999.key, "合同签订时，" + msg);
                }
            }
            /* 生成合同业务流水号 */
            /* 生成生成合同编号 */
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            contNo = sequenceTemplateClient.getSequenceTemplate(cmisBizXwCommonService.getDKSeq(manager_br_id), new HashMap<>());

            //生成IqpLoanApp表，有多少值插入多少值
            IqpLoanApp iqpLoanApp = new IqpLoanApp();
            BeanUtils.copyProperties(xdkh0001DataRespDto, iqpLoanApp);
            //iqpLoanApp.setSerno(serno);
            iqpLoanApp.setIqpSerno(serno);
            iqpLoanApp.setReplyNo(approvalResultSerno);
            iqpLoanApp.setLoanModal(loanModal);
            iqpLoanApp.setCusId(borrower_cus_id);
            iqpLoanApp.setCusName(cusName);
            iqpLoanApp.setCertCode(certNo);
            iqpLoanApp.setPhone(phone);
            iqpLoanApp.setBizType("02");//STD_BUSI_TYPE 默认 02 普通贷款合同
            iqpLoanApp.setAppDate(openDay);
            iqpLoanApp.setPrdId(prd_id);
            iqpLoanApp.setPrdName(prd_name);
            iqpLoanApp.setLoanPurp("");//贷款用途
            iqpLoanApp.setLoanCha("");//贷款性质
            iqpLoanApp.setSurveySerno(surveySerno);//调查编号
            iqpLoanApp.setContType("2");//合同类型2	最高额合同
            iqpLoanApp.setCurType("CNY");//合同币种
            iqpLoanApp.setContAmt(contAmt);//合同金额
            iqpLoanApp.setContHighAvlAmt(contAmt);// 合同最高可用金额
            iqpLoanApp.setCvtCnyAmt(contAmt);    //折算人民币金额
            iqpLoanApp.setContTerm(loanTerms);//合同期限
            iqpLoanApp.setStartDate(startDate);//合同起始日
            iqpLoanApp.setEndDate(endDate);//合同到期日
            iqpLoanApp.setLoanTer(indivComFld);//贷款投向
            iqpLoanApp.setRepayMode("");//还款方式
            iqpLoanApp.setLoanType("");//借款种类
            iqpLoanApp.setAppCurType("CNY");//申请币种
            iqpLoanApp.setAppTerm(Optional.ofNullable(new BigDecimal(loanTerms)).orElse(BigDecimal.ZERO));//申请期限
            iqpLoanApp.setAppRate(BigDecimal.ONE);    //申请汇率
            BigDecimal curtLprRate = queryLprRate(new BigDecimal(loanTerms));
            iqpLoanApp.setCurtLprRate(curtLprRate);
            iqpLoanApp.setGuarWay("00");//主担保方式,这里都是信用担保
            iqpLoanApp.setApproveStatus("000");//审批状态默认未提交
            iqpLoanApp.setOprType("01");//操作类型 默认新增 01
            iqpLoanApp.setManagerBrId(manager_br_id);//主管机构
            iqpLoanApp.setManagerId(manager_id);//主管客户经理
            iqpLoanApp.setInputBrId(manager_br_id);//登记机构
            iqpLoanApp.setInputId(manager_id);//登记人
            iqpLoanApp.setInputDate(openDay);//登记日期
            iqpLoanApp.setCreateTime(new Date());
            iqpLoanApp.setUpdateTime(new Date());
            iqpLoanApp.setExecRateYear(rateb);//执行年利率
            iqpLoanApp.setRealityIrM(rateb.divide(new BigDecimal("12"), 9, BigDecimal.ROUND_HALF_UP));//执行月利率
            iqpLoanApp.setOverdueRatePefloat(new BigDecimal("0.5"));//逾期利率浮动百分比
            iqpLoanApp.setOverdueExecRate(rateb.multiply(iqpLoanApp.getOverdueRatePefloat().add(new BigDecimal("0.5"))));//逾期利率（年）
            iqpLoanApp.setDefaultRate(new BigDecimal("0.5"));//违约利率浮动百分比
            iqpLoanApp.setDefaultRateY(rateb.multiply(iqpLoanApp.getDefaultRate().add(new BigDecimal("0.5"))));//违约利率（年）
            iqpLoanApp.setBelgLine("01");//业务条线 默认小微
            iqpLoanApp.setChnlSour("01");// 默认手机银行
            /******************扣款日期获取开始******************/
            String repayDate = "21";
            RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
            if (repayDayRecordInfo != null) {//查询结果不为null
                repayDate = repayDayRecordInfo.getRepayDate().toString();
            }
            iqpLoanApp.setRepayDate(repayDate);
            /******************扣款日期获取结束******************/

            iqpLoanAppMapper.insert(iqpLoanApp);

            CtrLoanCont ctrloancont = new CtrLoanCont();
            BeanUtils.copyProperties(iqpLoanApp, ctrloancont);
            ctrloancont.setSurveySerno(surveySerno);
            ctrloancont.setContNo(contNo);
            ctrloancont.setHighAvlAmt(iqpLoanApp.getContAmt());
            ctrloancont.setCvtCnyAmt(contAmt); //折算人民币金额
            ctrloancont.setContStartDate(startDate);
            ctrloancont.setContEndDate(endDate);
            ctrloancont.setContCnNo("");
            ctrloancont.setContStatus("100");//合同状态默认100未生效
            ctrloancont.setBelgLine("01");//业务条线 默认小微
            ctrloancont.setChnlSour("01");// 默认手机银行
            ctrloancont.setRepayDate(repayDate);

            ctrLoanContMapper.insert(ctrloancont);

            //保存优惠券信息
            if (StringUtils.isNotBlank(preferCode)) {
                //1、老信贷新增插入DiscCouponBusinRel优惠券业务关联表
                //pk 主键
                //business_no 业务编号（合同号、借据号）
                //discount_code 优惠券编号
                //business_type 业务类型 01:合同 02:借据
                DiscCouponBusinRel discCouponBusinRel = new DiscCouponBusinRel();
                String pkid = UUID.randomUUID().toString();//sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ_BIZ_13, new HashMap<>());
                discCouponBusinRel.setPkId(pkid);
                discCouponBusinRel.setBusiType("01");
                discCouponBusinRel.setCreateTime(new Date());
                discCouponBusinRel.setDiscountCode(preferCode);
                discCouponBusinRel.setSerno(serno);
                discCouponBusinRel.setUpdateTime(new Date());
                discCouponBusinRelMapper.insert(discCouponBusinRel);

                //2、调用小微公众号接口核销优惠券
                Xwh003ReqDto xwh003ReqDto = new Xwh003ReqDto();
                xwh003ReqDto.setCardSecret(preferCode);
                xwh003ReqDto.setCusId(borrower_cus_id);
                xwh003ReqDto.setIdCard(certNo);
                //状态1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
                xwh003ReqDto.setStatus("1");
                xwh003ReqDto.setBillNo("");
                xwh003ReqDto.setLoanContNo(contNo);
                ResultDto<Xwh003RespDto> resultDto = dscms2XwhClientService.xwh003(xwh003ReqDto);
            }

            xdht0019DataRespDto.setContNo(contNo);// 合同号
            xdht0019DataRespDto.setCnContNo(contNo);// 中文合同号（新信贷取消中文合同）
            xdht0019DataRespDto.setApplyAmt(applyAmt);// 申请金额
            xdht0019DataRespDto.setDate(openDay);// 日期
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataRespDto));
        return xdht0019DataRespDto;
    }

    /**
     * @param startDate, cusId, applyAmt, rateb
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/9/17 17:00
     * @version 1.0.0
     * @desc 校验批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private LmtCrdReplyInfo checkXdPrdResult(String startDate, String cusId, BigDecimal applyAmt, BigDecimal rateb, String prd_id) {
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date loanStartDate = sdf.parse(startDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(loanStartDate);
            cal.add(Calendar.DAY_OF_MONTH, -30);//30天前
            String dayBeforeLoanStartDate30 = sdf.format(cal.getTime());
            Map checkMap = new HashMap();
            checkMap.put("cus_id", cusId);
            checkMap.put("prdId", prd_id);
            checkMap.put("day1", dayBeforeLoanStartDate30);
            checkMap.put("day2", startDate);
            lmtCrdReplyInfo = lmtCrdReplyInfoService.selectLastApprovalResult(checkMap);
            if (lmtCrdReplyInfo == null) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "信贷审批表中未查询到30天内的审批结果！");
            } else {
                BigDecimal approvalAmount = lmtCrdReplyInfo.getReplyAmt();
                String approvalResultSerno = lmtCrdReplyInfo.getReplySerno();
                BigDecimal realityIrY = lmtCrdReplyInfo.getExecRateYear();
                if (Objects.nonNull(approvalResultSerno)) {
                    if (applyAmt.compareTo(approvalAmount) != 0) {
                        //  "合同金额:"  + apply_amount + "与审批结果:" + applyAmount + "不一致，请核实！"
                        throw BizException.error(null, EcbEnum.ECB010039.key, EcbEnum.ECB010039.value);
                    }
                    if (rateb.compareTo(realityIrY) != 0) {
                        //"合同利率:" + rate + "%与审批结果:" + realityIrY + "%不一致，请核实！";
                        throw BizException.error(null, EcbEnum.ECB010040.key, EcbEnum.ECB010040.value);
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return lmtCrdReplyInfo;
    }

    /********
     *根据证件号查询客户信息
     * *********/
    public CusIndivDto queryCusIndiv(String certNo) {
        CusIndivDto cusIndiv = new CusIndivDto();
        if (StringUtil.isNotEmpty(certNo)) {//证件号不能为空
            CmisCus0007ReqDto cmisCus0007ReqDto = new CmisCus0007ReqDto();
            cmisCus0007ReqDto.setCertCode(certNo);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
            CmisCus0007RespDto cmisCus0007RespDto = cmisCus0007Service.cmisCus0007(cmisCus0007ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
            List<CusIndivDto> cusIndivList = cmisCus0007RespDto.getCusIndivList();
            cusIndiv = new CusIndivDto();
            List<CusIndivDto> collect = cusIndivList.stream().filter(cusIndivDto -> Objects.equals(certNo, cusIndivDto.getCertCode())).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(collect)) {
                cusIndiv = collect.get(0);
            }
        }
        return cusIndiv;
    }

    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(BigDecimal term) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0.0485");
        try {
            String newVal = "A1";//
            if (term.compareTo(new BigDecimal(60)) > 0) {
                newVal = "A2";
            }
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
                logger.info("根据贷款期限查询lpr利率{}", curtLprRate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }
}
