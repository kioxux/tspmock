package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.DocDestroyAppList;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.DocDestroyAppListService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @className DHCZ20BizService
 * @Description 档案销毁（东海）后处理
 * @Date 2021/08/31 : 10:43
 */
@Service
public class DHCZ20BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ20BizService.class);

    @Autowired
    private DocDestroyAppListService docDestroyAppListService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String ddalSerno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            DocDestroyAppList docDestroyAppList = docDestroyAppListService.selectByPrimaryKey(ddalSerno);

            if (OpType.STRAT.equals(currentOpType)) {
                if (Objects.nonNull(resultInstanceDto.getParam())) {
                    docDestroyAppList.setDestroyType((String) resultInstanceDto.getParam().get("destroyType"));
                    docDestroyAppListService.updateSelective(docDestroyAppList);
                }
                log.info("档案销毁（东海）申请" + ddalSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                docDestroyAppListService.handleBusinessDataAfterStart(ddalSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //由审批中111 -> 审批通过 997
                docDestroyAppListService.handleBusinessDataAfterEnd(ddalSerno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docDestroyAppListService.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docDestroyAppListService.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    docDestroyAppListService.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                docDestroyAppListService.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("档案销毁（东海）申请" + ddalSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                docDestroyAppList.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                docDestroyAppListService.updateAppStatus(ddalSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("档案销毁（东海）申请" + ddalSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHCZ20.equals(flowCode);
    }
}
