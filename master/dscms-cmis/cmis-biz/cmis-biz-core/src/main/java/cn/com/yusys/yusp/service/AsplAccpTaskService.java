/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.dto.AsplAccpDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.Xdzc0024DataRespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.AsplAccpTaskMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccpTaskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:19:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplAccpTaskService {

    @Autowired
    private AsplAccpTaskMapper asplAccpTaskMapper;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;



	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplAccpTask selectByPrimaryKey(String pkId) {
        return asplAccpTaskMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplAccpTask> selectAll(QueryModel model) {
        List<AsplAccpTask> records = (List<AsplAccpTask>) asplAccpTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplAccpTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplAccpTask> list = asplAccpTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplAccpTask record) {
        return asplAccpTaskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(AsplAccpTask record) {
        return asplAccpTaskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplAccpTask record) {
        return asplAccpTaskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplAccpTask record) {
        return asplAccpTaskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplAccpTaskMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplAccpTaskMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByParam
     * @方法描述: 根据参数查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AsplAccpTask selectByParam(HashMap map) {
        return asplAccpTaskMapper.selectByParam(map);
    }

    /**
     * @方法名称: updateApproveStatueAfterFlow
     * @方法描述: 流程审批后处理
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void updateApproveStatueAfterFlow(AsplAccpTask asplAccpTask,String wfStatus) {
        if (Objects.equals(CmisCommonConstants.WF_STATUS_997,wfStatus)){
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            asplAccpTask.setApproveStatus(wfStatus);
            asplAccpTask.setCollectDate(openDay);
            asplAccpTaskMapper.updateByPrimaryKeySelective(asplAccpTask);
        }else{
            updateByTaskId(wfStatus,asplAccpTask.getTaskId());
        }
    }

    private int updateByTaskId(String wfStatus,String taskId) {
        return asplAccpTaskMapper.updateByTaskId(wfStatus,taskId);
    }

    public List<AsplAccpTask> selectInfoByCondition(QueryModel model){
        return asplAccpTaskMapper.selectInfoByCondition(model);
    }

    /**
     * @方法名称: updateEmptyByTContNo
     * @方法描述:  根据贸易合同编号置空
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateEmptyByTContNo(String tContNO){
         return asplAccpTaskMapper.updateEmptyByTContNo(tContNO);
     }

    /**
     * @方法名称: updateBillImgIdStatus
     * @方法描述: 根据银承流水更新补录状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateIsAddBill(String pvpSerno) {
        return asplAccpTaskMapper.updateIsAddBill(pvpSerno);
    }

    /**
     * @方法名称: xdzc0024
     * @方法描述: 根据银承编号并联查询 资产池贸易背景收集任务表 和 银承台账票据明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public java.util.List<cn.com.yusys.yusp.dto.server.xdzc0024.resp.List> xdzc0024(QueryModel model) {
        java.util.List<cn.com.yusys.yusp.dto.server.xdzc0024.resp.List> list = asplAccpTaskMapper.xdzc0024(model);
        return list;
    }

    /**
     * @函数名称: asplaccpchecklist
     * @函数描述: 贸易背景资料审核
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<AsplAccpTask> asplAccpTasklist(QueryModel queryModel) {
        return asplAccpTaskMapper.selectByModel(queryModel);
    }

    public ResultDto<AsplAccpTask> addAsplAccpTask(AsplAccpTask asplAccpTask) {
        String pvpSerno = asplAccpTask.getPvpSerno();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 校验发票补录下是否已存在在途业务
        List<AsplAccpTask> list= asplAccpTaskMapper.selectByPvpserno(pvpSerno);
        if(CollectionUtils.nonEmpty(list)){
            return new ResultDto<AsplAccpTask>(asplAccpTask).code("9999").message(" 该笔银承出账已存在在途发票补录记录");
        }
        AccAccp accAccp = accAccpService.selectByPvpSerno(pvpSerno);
        asplAccpTask.setPkId(UUID.randomUUID().toString().replace("-",""));// 主键
        asplAccpTask.setTaskId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.GXBJ_SERNO_SEQ, new HashMap()));// 任务编号
        asplAccpTask.setBillImgId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YX_SERNO_SEQ, new HashMap()));// 发票影像流水号
        asplAccpTask.setContNo(accAccp.getContNo());// 合同编号
//        asplAccpTask.setContCnNo(accAccp.get);// 中文合同号
        asplAccpTask.setPvpSerno(accAccp.getPvpSerno());// 出账流水号
        asplAccpTask.setCoreContNo(accAccp.getCoreBillNo());// 核心合同号
        asplAccpTask.setCusId(accAccp.getCusId());// 客户编号
        asplAccpTask.setCusName(accAccp.getCusName());// 客户名称
        asplAccpTask.setTaskCreateDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 任务生成日期
        // 当前时间加60天
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.getCurrDate());
        calendar.add(Calendar.DATE,60);
        asplAccpTask.setNeedFinishDate(DateUtils.formatDate(calendar.getTime(),DateFormatEnum.DEFAULT.getValue()));// 要求完成日期
        asplAccpTask.setBatchDrftAmt(accAccp.getDrftTotalAmt());// 批次票面金额
        asplAccpTask.setBatchQnt(Integer.parseInt(accAccp.getIsseCnt()));// 批次条数
        asplAccpTask.setCollectDate(StringUtils.EMPTY);// 收集日期
        asplAccpTask.setApproveStatus(BizFlowConstant.WF_STATUS_000);// 审批状态
        asplAccpTask.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 操作类型
        asplAccpTask.setRemark(StringUtils.EMPTY);// 备注描述
        asplAccpTask.setInputId(accAccp.getInputId());// 登记人
        asplAccpTask.setInputBrId(accAccp.getInputBrId());// 登记机构
        asplAccpTask.setInputDate(openDay);// 登记日期
        asplAccpTask.setUpdId(accAccp.getUpdId());// 最近修改人
        asplAccpTask.setUpdBrId(accAccp.getUpdBrId());// 最近修改机构
        asplAccpTask.setUpdDate(openDay);// 最近修改日期
        asplAccpTask.setManagerId(accAccp.getManagerId());// 主管客户经理
        asplAccpTask.setManagerBrId(accAccp.getManagerBrId());// 主管机构
        asplAccpTask.setCreateTime(DateUtils.getCurrDate());// 创建时间
        asplAccpTask.setUpdateTime(DateUtils.getCurrDate());// 修改时间
        asplAccpTask.setIsAddBill(CommonConstance.STD_ZB_YES_NO_0);// 是否完成补扫 0否
        if(asplAccpTaskMapper.insertSelective(asplAccpTask)>0){
            return new ResultDto<AsplAccpTask>(asplAccpTask);
        }else{
            return new ResultDto<AsplAccpTask>(asplAccpTask).code("9999");
        }
    }

    /**
     * 根据任务编号删除
     * @param taskId
     * @return
     */
    public int deletebytaskid(String taskId) {
        AsplAccpTask asplAccpTask = selectByTaskId(taskId);
        int rest = 0;
        if(Objects.equals(CmisCommonConstants.WF_STATUS_992,asplAccpTask.getApproveStatus())){
            rest = updateByTaskId(CmisCommonConstants.WF_STATUS_996,taskId);
            workflowCoreClient.deleteByBizId(taskId);
        }else{
            rest = asplAccpTaskMapper.deleteByPrimaryKey(taskId);
        }
        return rest;
    }

    /**
     * 根据任务编号查询
     * @param taskId
     * @return
     */
    public AsplAccpTask selectByTaskId(String taskId) {
        return asplAccpTaskMapper.selectByTaskId(taskId);
    }
}
