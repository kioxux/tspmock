/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.InvoiceInfo;
import cn.com.yusys.yusp.repository.mapper.InvoiceInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InvoiceInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 22:43:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class InvoiceInfoService {

    @Autowired
    private InvoiceInfoMapper invoiceInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public InvoiceInfo selectByPrimaryKey(String serno) {
        return invoiceInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<InvoiceInfo> selectAll(QueryModel model) {
        List<InvoiceInfo> records = (List<InvoiceInfo>) invoiceInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<InvoiceInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<InvoiceInfo> list = invoiceInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(InvoiceInfo record) {
        return invoiceInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(InvoiceInfo record) {
        return invoiceInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(InvoiceInfo record) {
        return invoiceInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(InvoiceInfo record) {
        return invoiceInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return invoiceInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return invoiceInfoMapper.deleteByIds(ids);
    }


    /**
     * @函数名称:insert
     * @函数描述: 新增发票信息
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional
    public ResultDto<InvoiceInfo> insertReturn(InvoiceInfo invoiceInfo) {
        // 新增默认未补录 0
        invoiceInfo.setIsAddpReco(CommonConstance.STD_ZB_YES_NO_0);
        this.insertSelective(invoiceInfo);
        return new ResultDto<InvoiceInfo>(invoiceInfo).code(SuccessEnum.CMIS_SUCCSESS.key);
    }

}
