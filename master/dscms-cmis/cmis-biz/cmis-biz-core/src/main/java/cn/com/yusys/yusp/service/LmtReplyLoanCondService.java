/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.web.rest.LmtReplyChgCondResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtReplyLoanCondMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyLoanCondService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyLoanCondService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyLoanCondService.class);

    @Autowired
    private LmtReplyLoanCondMapper lmtReplyLoanCondMapper;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private LmtReplyChgCondService lmtReplyChgCondService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplyLoanCond selectByPrimaryKey(String pkId) {
        return lmtReplyLoanCondMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplyLoanCond> selectAll(QueryModel model) {
        List<LmtReplyLoanCond> records = (List<LmtReplyLoanCond>) lmtReplyLoanCondMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyLoanCond> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyLoanCond> list = lmtReplyLoanCondMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtReplyLoanCond record) {
        return lmtReplyLoanCondMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyLoanCond record) {
        record.setCreateTime(new Date());
        record.setUpdateTime(new Date());
        return lmtReplyLoanCondMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtReplyLoanCond record) {
        return lmtReplyLoanCondMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyLoanCond record) {
        return lmtReplyLoanCondMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyLoanCondMapper.deleteByPrimaryKey(pkId);
    }


    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyLoanCondMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：queryLmtReplyLoanCondDataByParams
     * @方法描述：通过条件查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-25 上午 9:31
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyLoanCond> queryLmtReplyLoanCondDataByParams(HashMap<String, String> lmtReplyLoanCondMap) {
        return lmtReplyLoanCondMapper.queryLmtReplyLoanCondDataByParams(lmtReplyLoanCondMap);
    }


    /**
     * @方法名称: generateNewLmtReplyLoanCond
     * @方法描述: 通过授信审批的用信条件生成批复中的用信条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void generateNewLmtReplyLoanCondByLmtAppr(LmtAppr lmtAppr, String replySerno) throws Exception {
        HashMap<String, String> apprLoanCondQueryMap = new HashMap<String, String>();
        apprLoanCondQueryMap.put("apprSerno", lmtAppr.getApproveSerno());
        // 3 获取授信审批中其他条件信息
        List<LmtApprLoanCond> lmtApprLoanCondList = lmtApprLoanCondService.selectLmtApprLoanCondByParams(apprLoanCondQueryMap);
        if(lmtApprLoanCondList != null && lmtApprLoanCondList.size() > 0){
            for (LmtApprLoanCond lmtApprLoanCond : lmtApprLoanCondList) {
                LmtReplyLoanCond lmtReplyLoanCond = new LmtReplyLoanCond();
                BeanUtils.copyProperties(lmtApprLoanCond, lmtReplyLoanCond);
                lmtReplyLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyLoanCond.setReplySerno(replySerno);
                this.insert(lmtReplyLoanCond);
                log.info("生成其他条件信息:" + lmtReplyLoanCond.toString());
            }
        }else {
            // TODO throw new Exception("查询审批限制条件数据异常！");
        }
    }


    /**
     * @方法名称: updateLmtReplyLoanCondByLmtReplyChg
     * @方法描述: 通过授信审批变更的用信条件生成批复中的用信条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyLoanCondByLmtReplyChg(LmtReplyChg lmtReplyChg) throws Exception {
        HashMap<String, String> lmtReplyLoanCondQueryMap = new HashMap<String, String>();
        // 查询批复变更流水号对应的用信条件
        lmtReplyLoanCondQueryMap.put("serno", lmtReplyChg.getSerno());
        List<LmtReplyChgCond> lmtReplyChgCondList = lmtReplyChgCondService.queryLmtReplyChgCondByParams(lmtReplyLoanCondQueryMap);
        // 根据批复号删除批复对应的用信条件数据
        logicDeleteLmtReplyLoanCondByReplySerno(lmtReplyChg.getReplySerno());
        if(lmtReplyChgCondList != null && lmtReplyChgCondList.size() > 0){
            for (LmtReplyChgCond lmtReplyChgCond : lmtReplyChgCondList) {
                LmtReplyLoanCond lmtReplyLoanCond = new LmtReplyLoanCond();
                BeanUtils.copyProperties(lmtReplyChgCond, lmtReplyLoanCond);
                lmtReplyLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyLoanCond.setReplySerno(lmtReplyChg.getReplySerno());
                this.insert(lmtReplyLoanCond);
                log.info("生成其他条件信息:" + lmtReplyLoanCond.toString());
            }
        }
    }

    /**
     * @方法名称: logicDeleteLmtReplyLoanCondByReplySerno
     * @方法描述: 删除数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void logicDeleteLmtReplyLoanCondByReplySerno(String serno) throws Exception {
        int result = lmtReplyLoanCondMapper.logicDeleteLmtReplyLoanCondByReplySerno(serno);
//        if(result <= 0){
//            throw new Exception("删除批复用信条件数据异常");
//        }
    }

    /**
     * @方法名称：queryLmtReplyLoanCondDataByParams
     * @方法描述：通过条件查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-25 上午 9:31
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyLoanCond> queryLmtReplyLoanCondDataByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondMapper.selectByModel(queryModel);
        return lmtReplyLoanCondList;
    }
}
