package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

public class LmtReplySubPrdDto {

    private static final long serialVersionUID = 1L;

    private String pkId;

    private String replySubPrdSerno;

    private String replySubSerno;

    private String subPrdSerno;

    private String subSerno;

    private String lmtBizType;

    private String LmtBizTypeName;

    private String cusId;

    private String cusName;

    private String isRevolvLimit;

    private String guarMode;

    private String curType;

    private java.math.BigDecimal lmtAmt;

    private String startDate;

    private String endDate;

    private Integer lmtTerm;

    private Integer graper;

    private String origiLmtAccSubPrdNo;

    private java.math.BigDecimal origiLmtAccSubPrdAmt;

    private Integer origiLmtAccSubPrdTerm;

    private String chgFlag;

    private String isPreLmt;

    private String adjustFlag;

    private String isCurtRefine;

    private String isSfcaLmt;

    private java.math.BigDecimal bailPreRate;

    private java.math.BigDecimal rateYear;

    private String repayMode;

    private String eiMode;

    private String isRefinance;

    private String isRwrop;

    private java.math.BigDecimal chrgRate;

    private String chrgCollectMode;

    private String consignorType;

    private String consignorCusId;

    private String consignorCusName;

    private String consignorCertCode;

    private String consignorCertType;

    private String repayPlanDesc;

    private String oprType;

    private String inputId;

    private String inputBrId;

    private String inputDate;

    private String updId;

    private String updBrId;

    private String updDate;

    private java.util.Date createTime;

    private java.util.Date updateTime;

    /**
     * 页面用的授信分项额度编号
     **/
    private String lmtDrawNo;

    /**
     * 页面用的授信品种
     **/
    private String lmtDrawType;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getReplySubPrdSerno() {
        return replySubPrdSerno;
    }

    public void setReplySubPrdSerno(String replySubPrdSerno) {
        this.replySubPrdSerno = replySubPrdSerno;
    }

    public String getReplySubSerno() {
        return replySubSerno;
    }

    public void setReplySubSerno(String replySubSerno) {
        this.replySubSerno = replySubSerno;
    }

    public String getSubPrdSerno() {
        return subPrdSerno;
    }

    public void setSubPrdSerno(String subPrdSerno) {
        this.subPrdSerno = subPrdSerno;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getIsRevolvLimit() {
        return isRevolvLimit;
    }

    public void setIsRevolvLimit(String isRevolvLimit) {
        this.isRevolvLimit = isRevolvLimit;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public Integer getGraper() {
        return graper;
    }

    public void setGraper(Integer graper) {
        this.graper = graper;
    }

    public String getOrigiLmtAccSubPrdNo() {
        return origiLmtAccSubPrdNo;
    }

    public void setOrigiLmtAccSubPrdNo(String origiLmtAccSubPrdNo) {
        this.origiLmtAccSubPrdNo = origiLmtAccSubPrdNo;
    }

    public BigDecimal getOrigiLmtAccSubPrdAmt() {
        return origiLmtAccSubPrdAmt;
    }

    public void setOrigiLmtAccSubPrdAmt(BigDecimal origiLmtAccSubPrdAmt) {
        this.origiLmtAccSubPrdAmt = origiLmtAccSubPrdAmt;
    }

    public Integer getOrigiLmtAccSubPrdTerm() {
        return origiLmtAccSubPrdTerm;
    }

    public void setOrigiLmtAccSubPrdTerm(Integer origiLmtAccSubPrdTerm) {
        this.origiLmtAccSubPrdTerm = origiLmtAccSubPrdTerm;
    }

    public String getChgFlag() {
        return chgFlag;
    }

    public void setChgFlag(String chgFlag) {
        this.chgFlag = chgFlag;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getAdjustFlag() {
        return adjustFlag;
    }

    public void setAdjustFlag(String adjustFlag) {
        this.adjustFlag = adjustFlag;
    }

    public String getIsCurtRefine() {
        return isCurtRefine;
    }

    public void setIsCurtRefine(String isCurtRefine) {
        this.isCurtRefine = isCurtRefine;
    }

    public String getIsSfcaLmt() {
        return isSfcaLmt;
    }

    public void setIsSfcaLmt(String isSfcaLmt) {
        this.isSfcaLmt = isSfcaLmt;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getIsRefinance() {
        return isRefinance;
    }

    public void setIsRefinance(String isRefinance) {
        this.isRefinance = isRefinance;
    }

    public String getIsRwrop() {
        return isRwrop;
    }

    public void setIsRwrop(String isRwrop) {
        this.isRwrop = isRwrop;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public String getChrgCollectMode() {
        return chrgCollectMode;
    }

    public void setChrgCollectMode(String chrgCollectMode) {
        this.chrgCollectMode = chrgCollectMode;
    }

    public String getConsignorType() {
        return consignorType;
    }

    public void setConsignorType(String consignorType) {
        this.consignorType = consignorType;
    }

    public String getConsignorCusId() {
        return consignorCusId;
    }

    public void setConsignorCusId(String consignorCusId) {
        this.consignorCusId = consignorCusId;
    }

    public String getRepayPlanDesc() {
        return repayPlanDesc;
    }

    public void setRepayPlanDesc(String repayPlanDesc) {
        this.repayPlanDesc = repayPlanDesc;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLmtDrawNo() {
        return lmtDrawNo;
    }

    public void setLmtDrawNo(String lmtDrawNo) {
        this.lmtDrawNo = lmtDrawNo;
    }

    public String getLmtDrawType() {
        return lmtDrawType;
    }

    public void setLmtDrawType(String lmtDrawType) {
        this.lmtDrawType = lmtDrawType;
    }

    public String getLmtBizType() {
        return lmtBizType;
    }

    public void setLmtBizType(String lmtBizType) {
        this.lmtBizType = lmtBizType;
    }

    public String getLmtBizTypeName() {
        return LmtBizTypeName;
    }

    public void setLmtBizTypeName(String lmtBizTypeName) {
        LmtBizTypeName = lmtBizTypeName;
    }

    public String getConsignorCusName() {
        return consignorCusName;
    }

    public void setConsignorCusName(String consignorCusName) {
        this.consignorCusName = consignorCusName;
    }

    public String getConsignorCertCode() {
        return consignorCertCode;
    }

    public void setConsignorCertCode(String consignorCertCode) {
        this.consignorCertCode = consignorCertCode;
    }

    public String getConsignorCertType() {
        return consignorCertType;
    }

    public void setConsignorCertType(String consignorCertType) {
        this.consignorCertType = consignorCertType;
    }
}
