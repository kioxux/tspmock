package cn.com.yusys.yusp.web.server.xdxw0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0008.req.Xdxw0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0008.resp.Xdxw0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0008.Xdxw0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:优农贷黑白名单校验
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0008:优农贷黑白名单校验")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0008Resource.class);

    @Resource
    private Xdxw0008Service xdxw0008Service;

    /**
     * 交易码：xdxw0008
     * 交易描述：优农贷黑白名单校验
     *
     * @param xdxw0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷黑白名单校验")
    @PostMapping("/xdxw0008")
    protected @ResponseBody
    ResultDto<Xdxw0008DataRespDto> xdxw0008(@Validated @RequestBody Xdxw0008DataReqDto xdxw0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataReqDto));
        Xdxw0008DataRespDto xdxw0008DataRespDto = new Xdxw0008DataRespDto();// 响应Dto:优农贷黑白名单校验
        ResultDto<Xdxw0008DataRespDto> xdxw0008DataResultDto = new ResultDto<>();
        String rqstrName = xdxw0008DataReqDto.getRqstrName();//申请人姓名
        String rqstrCertNo = xdxw0008DataReqDto.getRqstrCertNo();//申请人证件号
        String rqstrMobile = xdxw0008DataReqDto.getRqstrMobile();//申请人手机号
        String actOperAddr = xdxw0008DataReqDto.getActOperAddr();//经营地址
        BigDecimal applyAmt = xdxw0008DataReqDto.getApplyAmt();//申请金额
        BigDecimal applyTerm = xdxw0008DataReqDto.getApplyTerm();//申请期限
        String saleManagerId = xdxw0008DataReqDto.getSaleManagerId();//营销客户经理编号
        try {
            // 从xdxw0008DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataReqDto));
            xdxw0008DataRespDto = xdxw0008Service.xdxw0008(xdxw0008DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataRespDto));
            // 封装xdxw0008DataResultDto中正确的返回码和返回信息
            xdxw0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, e.getMessage());
            // 封装xdxw0008DataResultDto中异常返回码和返回信息
            xdxw0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0008DataRespDto到xdxw0008DataResultDto中
        xdxw0008DataResultDto.setData(xdxw0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataResultDto));
        return xdxw0008DataResultDto;
    }
}
