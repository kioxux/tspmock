/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDelayInfo
 * @类描述: doc_read_delay_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 09:36:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_read_delay_info")
public class DocReadDelayInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 延期申请流水号 **/
	@Id
	@Column(name = "DRDI_SERNO")
	private String drdiSerno;
	
	/** 调阅流水号 **/
	@Column(name = "DRAI_SERNO", unique = false, nullable = true, length = 40)
	private String draiSerno;
	
	/** 延期归还日期 **/
	@Column(name = "DELAY_BACK_DATE", unique = false, nullable = true, length = 10)
	private String delayBackDate;
	
	/** 延期申请人 **/
	@Column(name = "DELAY_RQSTR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="delayRqstrName")
	private String delayRqstrId;
	
	/** 延期申请机构 **/
	@Column(name = "DELAY_RQSTR_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="delayRqstrOrgName")
	private String delayRqstrOrg;
	
	/** 延期申请日期 **/
	@Column(name = "DELAY_RQSTR_DATE", unique = false, nullable = true, length = 10)
	private String delayRqstrDate;
	
	/** 延期情况说明 **/
	@Column(name = "DELAY_CASE_DESC", unique = false, nullable = true, length = 2000)
	private String delayCaseDesc;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param drdiSerno
	 */
	public void setDrdiSerno(String drdiSerno) {
		this.drdiSerno = drdiSerno;
	}
	
    /**
     * @return drdiSerno
     */
	public String getDrdiSerno() {
		return this.drdiSerno;
	}
	
	/**
	 * @param draiSerno
	 */
	public void setDraiSerno(String draiSerno) {
		this.draiSerno = draiSerno;
	}
	
    /**
     * @return draiSerno
     */
	public String getDraiSerno() {
		return this.draiSerno;
	}
	
	/**
	 * @param delayBackDate
	 */
	public void setDelayBackDate(String delayBackDate) {
		this.delayBackDate = delayBackDate;
	}
	
    /**
     * @return delayBackDate
     */
	public String getDelayBackDate() {
		return this.delayBackDate;
	}
	
	/**
	 * @param delayRqstrId
	 */
	public void setDelayRqstrId(String delayRqstrId) {
		this.delayRqstrId = delayRqstrId;
	}
	
    /**
     * @return delayRqstrId
     */
	public String getDelayRqstrId() {
		return this.delayRqstrId;
	}
	
	/**
	 * @param delayRqstrOrg
	 */
	public void setDelayRqstrOrg(String delayRqstrOrg) {
		this.delayRqstrOrg = delayRqstrOrg;
	}
	
    /**
     * @return delayRqstrOrg
     */
	public String getDelayRqstrOrg() {
		return this.delayRqstrOrg;
	}
	
	/**
	 * @param delayRqstrDate
	 */
	public void setDelayRqstrDate(String delayRqstrDate) {
		this.delayRqstrDate = delayRqstrDate;
	}
	
    /**
     * @return delayRqstrDate
     */
	public String getDelayRqstrDate() {
		return this.delayRqstrDate;
	}
	
	/**
	 * @param delayCaseDesc
	 */
	public void setDelayCaseDesc(String delayCaseDesc) {
		this.delayCaseDesc = delayCaseDesc;
	}
	
    /**
     * @return delayCaseDesc
     */
	public String getDelayCaseDesc() {
		return this.delayCaseDesc;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}