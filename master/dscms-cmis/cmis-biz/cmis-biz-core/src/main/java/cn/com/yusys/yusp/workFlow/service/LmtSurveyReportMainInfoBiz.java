package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtSurveyReportMainInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @创建人 WH
 * @创建时间 2021-04-14
 * @return SurveyReportMainInfoBiz
 **/
@Service
public class LmtSurveyReportMainInfoBiz implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(LmtSurveyReportMainInfoBiz.class);
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private LmtSurveyReportMainInfoService lmtsurveyReportMainInfoService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();

        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.BIZ_TYPE_WF_LMT_SURVEY_REPORT_MAIN.equals(flowCode);
//        return "XD_PERFER_RATE_APPLY_TEST".equals(flowCode);

    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批状态更换
     */
    public void updateStatus(String serno,String state){
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtsurveyReportMainInfoService.selectByPrimaryKey(serno);
        lmtSurveyReportMainInfo.setApproveStatus(state);
        lmtsurveyReportMainInfoService.updateSelective(lmtSurveyReportMainInfo);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批通过
     */
    public void end(String extSerno) {
        //审批通过的操作
        lmtsurveyReportMainInfoService.endPass(extSerno);
    }

}
