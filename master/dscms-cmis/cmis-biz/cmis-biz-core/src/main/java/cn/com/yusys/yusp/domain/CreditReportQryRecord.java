/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportQryRecord
 * @类描述: credit_report_qry_record数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 09:30:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_report_qry_record")
public class CreditReportQryRecord extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 查看记录流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CRQR_SERNO")
	private String crqrSerno;
	
	/** 征信查询编号 **/
	@Column(name = "CRQL_SERNO", unique = false, nullable = true, length = 40)
	private String crqlSerno;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 岗位编号 **/
	@Column(name = "DUTY_CDE", unique = false, nullable = true, length = 20)
	private String dutyCde;
	
	/** 征信查询原因 **/
	@Column(name = "QRY_RESN", unique = false, nullable = true, length = 5)
	private String qryResn;
	
	/** 查询IP地址 **/
	@Column(name = "IP", unique = false, nullable = true, length = 20)
	private String ip;
	
	/** 成功查询时间 **/
	@Column(name = "QRY_TIME", unique = false, nullable = true, length = 20)
	private String qryTime;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param crqrSerno
	 */
	public void setCrqrSerno(String crqrSerno) {
		this.crqrSerno = crqrSerno;
	}
	
    /**
     * @return crqrSerno
     */
	public String getCrqrSerno() {
		return this.crqrSerno;
	}
	
	/**
	 * @param crqlSerno
	 */
	public void setCrqlSerno(String crqlSerno) {
		this.crqlSerno = crqlSerno;
	}
	
    /**
     * @return crqlSerno
     */
	public String getCrqlSerno() {
		return this.crqlSerno;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param dutyCde
	 */
	public void setDutyCde(String dutyCde) {
		this.dutyCde = dutyCde;
	}
	
    /**
     * @return dutyCde
     */
	public String getDutyCde() {
		return this.dutyCde;
	}
	
	/**
	 * @param qryResn
	 */
	public void setQryResn(String qryResn) {
		this.qryResn = qryResn;
	}
	
    /**
     * @return qryResn
     */
	public String getQryResn() {
		return this.qryResn;
	}
	
	/**
	 * @param ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	
    /**
     * @return ip
     */
	public String getIp() {
		return this.ip;
	}
	
	/**
	 * @param qryTime
	 */
	public void setQryTime(String qryTime) {
		this.qryTime = qryTime;
	}
	
    /**
     * @return qryTime
     */
	public String getQryTime() {
		return this.qryTime;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}