/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CentralFileOptRecord;
import cn.com.yusys.yusp.repository.mapper.CentralFileOptRecordMapper;
import cn.com.yusys.yusp.vo.CentralFileInfoOptVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CentralFileInfo;
import cn.com.yusys.yusp.repository.mapper.CentralFileInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 23:33:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CentralFileInfoService {

    @Autowired
    private CentralFileInfoMapper centralFileInfoMapper;
    @Autowired
    private CentralFileOptRecordMapper centralFileOptRecordMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CentralFileInfo selectByPrimaryKey(String fileNo) {
        return centralFileInfoMapper.selectByPrimaryKey(fileNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CentralFileInfo> selectAll(QueryModel model) {
        List<CentralFileInfo> records = (List<CentralFileInfo>) centralFileInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CentralFileInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CentralFileInfo> list = centralFileInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CentralFileInfo record) {
        return centralFileInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CentralFileInfo record) {
        return centralFileInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CentralFileInfo record) {
        return centralFileInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CentralFileInfo record) {
        return centralFileInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String fileNo) {
        return centralFileInfoMapper.deleteByPrimaryKey(fileNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return centralFileInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据库里的数据查询下一个临时库位
     * @return
     */
    public Integer queryTempLocationNo(){
        List<Integer> allUsedTempLocationNo = centralFileInfoMapper.queryAllUsedTempLocationNo();
        // 临时库是空的，无占用
        if(CollectionUtils.isEmpty(allUsedTempLocationNo)){
            return 1;
        }
        if(allUsedTempLocationNo.size() < 1000){
            Integer res = null;
            for (int i = 0; i < allUsedTempLocationNo.size(); i++) {
                if(allUsedTempLocationNo.get(i) == (i+1)){
                    continue;
                }else{
                    res = i + 1;
                    break;
                }
            };
            // 所有已用库位无中间断开情况
            if(res == null){
                res =   allUsedTempLocationNo.size() + 1;
            }
            return res;
        }else{
            throw BizException.error(null, "99999", "临时库已满，无可用的临时库位");
        }
    };

    @Transactional(rollbackFor = Exception.class)
    public int batchUpdateSelective(CentralFileInfoOptVo centralFileInfoOptVo){
        int result = 0;
        List<CentralFileInfo> centralfileinfoList = centralFileInfoOptVo.getCentralfileinfoList();
        CentralFileOptRecord centralfileoptrecord = centralFileInfoOptVo.getCentralfileoptrecord();
        if(CollectionUtils.nonEmpty(centralfileinfoList)){
            for (CentralFileInfo centralFileInfo: centralfileinfoList) {
                // 1、 更新台账状态及其接收人或出库人等信息
                if("03".equals(centralFileInfo.getAccStatus())){ // 出库操作
                    centralFileInfo.setOutId(SessionUtils.getLoginCode());
                    centralFileInfo.setOutOrg(SessionUtils.getUserInformation().getOrg().getCode());
                    centralFileInfo.setFileOutDate(DateUtils.getCurrDateTimeStr());
                }
                int temi = centralFileInfoMapper.updateByPrimaryKeySelective(centralFileInfo);
                result = result + temi;
                // 2、添加台账操作记录
                CentralFileOptRecord tempCentralFileOptRecord = new CentralFileOptRecord();
                tempCentralFileOptRecord.setFileNo(centralFileInfo.getFileNo());
                tempCentralFileOptRecord.setOptUsr(centralFileInfo.getReceiverId());
                tempCentralFileOptRecord.setOptOrg(centralFileInfo.getReceiverOrg());
                tempCentralFileOptRecord.setOptTime(new Date());
                tempCentralFileOptRecord.setFileOptType(centralfileoptrecord.getFileOptType());// 合并暂存
                tempCentralFileOptRecord.setOptReason(centralfileoptrecord.getOptReason());
                centralFileOptRecordMapper.insert(tempCentralFileOptRecord);
            }
        }
        return result;
    }
}
