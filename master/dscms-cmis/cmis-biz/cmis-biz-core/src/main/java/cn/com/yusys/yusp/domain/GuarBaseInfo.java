/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfo
 * @类描述: guar_base_info数据实体类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-04-14 15:25:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_base_info")
public class GuarBaseInfo extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;

	/** 押品类型 STD_GRT_FLAG **/
	@Column(name = "GRT_FLAG", unique = false, nullable = true, length = 5)
	private String grtFlag;

	/** 抵质押物名称（抵质押物类型名称） **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;

	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 40)
	private String guarTypeCd;

	/** 抵质押分类（大类）STD_GAGE_TYPE **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;

	/** 押品初次登记时间 **/
	@Column(name = "GUAR_FIRST_CREATE_TIME", unique = false, nullable = true, length = 10)
	private String guarFirstCreateTime;

	/** 借款人 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 押品所有人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;

	/** 押品所有人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;

	/** 押品所有人类型 STD_CUS_TYPE **/
	@Column(name = "GUAR_CUS_TYPE", unique = false, nullable = true, length = 10)
	private String guarCusType;

	/** 押品所有人证件类型 STD_CERT_TYPE **/
	@Column(name = "GUAR_CERT_TYPE", unique = false, nullable = true, length = 10)
	private String guarCertType;

	/** 押品所有人证件号码 **/
	@Column(name = "GUAR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String guarCertCode;

	/** 押品所有人贷款卡号(抵押)  或 出质人贷款卡号(质押) **/
	@Column(name = "ASSURE_CARD_NO", unique = false, nullable = true, length = 40)
	private String assureCardNo;

	/** 是否实质正相关  STD_YES_NO **/
	@Column(name = "RELATION_INT", unique = false, nullable = true, length = 10)
	private String relationInt;

	/** 是否共有财产  STD_YES_NO **/
	@Column(name = "COMMON_ASSETS_IND", unique = false, nullable = true, length = 10)
	private String commonAssetsInd;

	/** 押品所有人所占份额 **/
	@Column(name = "OCCUPYOFOWNER", unique = false, nullable = true, length = 5)
	private String occupyofowner;

	/** 是否需要办理保险 STD_YES_NO **/
	@Column(name = "INSURANCE_IND", unique = false, nullable = true, length = 10)
	private String insuranceInd;

	/** 是否需要办理公证 STD_YES_NO **/
	@Column(name = "NOTARIZATION_IND", unique = false, nullable = true, length = 5)
	private String notarizationInd;

	/** 人工认定结果 STD_FORCECREATE_IND **/
	@Column(name = "FORCECREATE_IND", unique = false, nullable = true, length = 5)
	private String forcecreateInd;

	/** 强制创建理由 **/
	@Column(name = "FORCECREATE_REASON", unique = false, nullable = true, length = 750)
	private String forcecreateReason;

	/** 押品所在业务阶段 STD_GUAR_BUSISTATE **/
	@Column(name = "GUAR_BUSISTATE", unique = false, nullable = true, length = 20)
	private String guarBusistate;

	/** 押品状态 STD_GUAR_STATE **/
	@Column(name = "GUAR_STATE", unique = false, nullable = true, length = 10)
	private String guarState;

	/** 是否权属清晰  STD_YES_NO **/
	@Column(name = "IS_OWNERSHIP_CLEAR", unique = false, nullable = true, length = 10)
	private String isOwnershipClear;

	/** 是否抵债资产  STD_YES_NO **/
	@Column(name = "IS_DEBT_ASSET", unique = false, nullable = true, length = 1)
	private String isDebtAsset;

	/** 法律规定禁止流通财产  STD_YES_NO **/
	@Column(name = "FORBID_CIR_BELOG_FLAG", unique = false, nullable = true, length = 1)
	private String forbidCirBelogFlag;

	/** 是否被查封、扣押或监管  STD_YES_NO **/
	@Column(name = "SUPERVISION_FLAG", unique = false, nullable = true, length = 1)
	private String supervisionFlag;

	/** 查封时间 **/
	@Column(name = "SUPERVISION_DATE", unique = false, nullable = true, length = 20)
	private String supervisionDate;

	/** 是否有强行执行条款  STD_YES_NO **/
	@Column(name = "ENFORE_FLAG", unique = false, nullable = true, length = 1)
	private String enforeFlag;

	/** 抵权证编号及其他编号 **/
	@Column(name = "RIGHT_OTHER_NO", unique = false, nullable = true, length = 200)
	private String rightOtherNo;

	/** 押品登记办理状态 STD_REG_STATE **/
	@Column(name = "REG_STATE", unique = false, nullable = true, length = 10)
	private String regState;

	/** 担保权生效方式 STD_DEF_EFFECT_TYPE **/
	@Column(name = "DEF_EFFECT_TYPE", unique = false, nullable = true, length = 10)
	private String defEffectType;

	/** 他行是否已设定担保权  STD_YES_NO **/
	@Column(name = "OTHER_BACK_GUAR_IND", unique = false, nullable = true, length = 10)
	private String otherBackGuarInd;

	/** 抵质押物与借款人相关性 **/
	@Column(name = "PLDIMN_DEBIT_RELATIVE", unique = false, nullable = true, length = 3)
	private String pldimnDebitRelative;

	/** 查封便利性 **/
	@Column(name = "SUPERVISION_CONVENIENCE", unique = false, nullable = true, length = 3)
	private String supervisionConvenience;

	/** 法律有效性 **/
	@Column(name = "LAW_VALIDITY", unique = false, nullable = true, length = 3)
	private String lawValidity;

	/** 抵质押品通用性 **/
	@Column(name = "PLDIMN_COMMON", unique = false, nullable = true, length = 3)
	private String pldimnCommon;

	/** 抵质押品变现能力 **/
	@Column(name = "PLDIMN_CASHABILITY", unique = false, nullable = true, length = 5)
	private String pldimnCashability;

	/** 价格波动性 **/
	@Column(name = "PRICE_WAVE", unique = false, nullable = true, length = 5)
	private String priceWave;

	/** 是否扫描资料 **/
	@Column(name = "IS_SCAN_MATER", unique = false, nullable = true, length = 5)
	private String isScanMater;

	/** 权证编号及其他编号 **/
	@Column(name = "RINGHT_NO_AND_OTHER_NO", unique = false, nullable = true, length = 40)
	private String ringhtNoAndOtherNo;

	/** 我行担保权受偿顺序 **/
	@Column(name = "MYBACK_GUAR_FIRST_SEQ", unique = false, nullable = true, length = 5)
	private String mybackGuarFirstSeq;

	/** 法定优先受偿款(元) **/
	@Column(name = "LEGAL_PRI_PAYMENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal legalPriPayment;

	/** 创建系统/来源系统 STD_DATA_SOURCE **/
	@Column(name = "CREATE_SYS", unique = false, nullable = true, length = 10)
	private String createSys;

	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 1000)
	private String remark;

	/** 管户人 **/
	@Column(name = "ACCOUNT_MANAGER", unique = false, nullable = true, length = 10)
	private String accountManager;

	/** 押品评估价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;

	/** 评估日期 **/
	@Column(name = "EVAL_DATE", unique = false, nullable = true, length = 20)
	private String evalDate;

	/** 押品认定价值 **/
	@Column(name = "CONFIRM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal confirmAmt;

	/** 币种 STD_CUR_TYPE  **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 权证凭证号 **/
	@Column(name = "CERTI_RECORD_ID", unique = false, nullable = true, length = 40)
	private String certiRecordId;

	/** 权证到期日 **/
	@Column(name = "CERTI_END_DATE", unique = false, nullable = true, length = 20)
	private String certiEndDate;

	/** 最高抵押率（%） **/
	@Column(name = "MORTAGAGE_MAX_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mortagageMaxRate;

	/** 设定抵押率（%） **/
	@Column(name = "MORTAGAGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mortagageRate;

	/** 我行可用价值 **/
	@Column(name = "MAX_MORTAGAGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maxMortagageAmt;

	/** 抵押物存放地点 **/
	@Column(name = "PLD_LOCATION", unique = false, nullable = true, length = 500)
	private String pldLocation;

	/** 共有权人名称 **/
	@Column(name = "REF_NAME", unique = false, nullable = true, length = 250)
	private String refName;

	/** 是否权益争议STD_ZB_YES_NO **/
	@Column(name = "IS_DISPUTED", unique = false, nullable = true, length = 5)
	private String isDisputed;

	/** 核心担保品编号 **/
	@Column(name = "CORE_GUARANTY_NO", unique = false, nullable = true, length = 40)
	private String coreGuarantyNo;

	/** 核心担保品序号 **/
	@Column(name = "CORE_GUARANTY_SEQ", unique = false, nullable = true, length = 40)
	private String coreGuarantySeq;

	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;

	/** 认定日期 **/
	@Column(name = "CONFIRM_DATE", unique = false, nullable = true, length = 20)
	private String confirmDate;

	/** 评估方式 STD_EVAL_TYPE **/
	@Column(name = "EVAL_TYPE", unique = false, nullable = true, length = 5)
	private String evalType;

	/** 评估机构 **/
	@Column(name = "EVAL_ORG", unique = false, nullable = true, length = 120)
	private String evalOrg;

	/** 评估机构组织机构代码 **/
	@Column(name = "EVAL_ORG_INS_CODE", unique = false, nullable = true, length = 10)
	private String evalOrgInsCode;

	/** 购入金额 **/
	@Column(name = "BUY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal buyAmt;

	/** 内部评估确认金额（元） **/
	@Column(name = "INNER_EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal innerEvalAmt;

	/** 下次估值到期日期 **/
	@Column(name = "NEXT_EVAL_END_DATE", unique = false, nullable = true, length = 20)
	private String nextEvalEndDate;

	/** 下次估值日期 **/
	@Column(name = "NEXT_EVAL_DATE", unique = false, nullable = true, length = 20)
	private String nextEvalDate;

	/** 是否资产保全 STD_ZB_YES_NO **/
	@Column(name = "IS_SPECIAL", unique = false, nullable = true, length = 5)
	private String isSpecial;

	/** 是否保全资产STD_ZB_YES_NO **/
	@Column(name = "IS_SPECIAL_ASSET", unique = false, nullable = true, length = 5)
	private String isSpecialAsset;

	/** 是否成品房 STD_ZB_YES_NO **/
	@Column(name = "IS_EXISTINGHOME", unique = false, nullable = true, length = 5)
	private String isExistinghome;

	/** 是否需要修改 STD_ZB_YES_NO **/
	@Column(name = "IS_UPDATE", unique = false, nullable = true, length = 5)
	private String isUpdate;

	/** 授信评估金额 **/
	@Column(name = "LMT_EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtEvalAmt;

	/** 所在区域编号 **/
	@Column(name = "AREA_CODE", unique = false, nullable = true, length = 200)
	private String areaCode;

	/** 所在区域名称 **/
	@Column(name = "AREA_NAME", unique = false, nullable = true, length = 500)
	private String areaName;

	/** 复制来源 **/
	@Column(name = "COPY_FROM", unique = false, nullable = true, length = 32)
	private String copyFrom;

	/** 是否变化 STD_ZB_YES_NO **/
	@Column(name = "IS_CHANGED", unique = false, nullable = true, length = 5)
	private String isChanged;

	/** 出入库流水号 **/
	@Column(name = "INOUT_SERNO", unique = false, nullable = true, length = 40)
	private String inoutSerno;

	/** 入库人编号 **/
	@Column(name = "IN_USER", unique = false, nullable = true, length = 40)
	private String inUser;

	/** 从属类型  STD_RIGHT_TYPE **/
	@Column(name = "RIGHT_TYPE", unique = false, nullable = true, length = 5)
	private String rightType;

	/** 权属证件类型 STD_RIGHT_CERT_TYPE_CODE **/
	@Column(name = "RIGHT_CERT_TYPE_CODE", unique = false, nullable = true, length = 5)
	private String rightCertTypeCode;

	/** 权属证件号 **/
	@Column(name = "RIGHT_CERT_NO", unique = false, nullable = true, length = 40)
	private String rightCertNo;

	/** 权属登记机关 **/
	@Column(name = "RIGHT_ORG", unique = false, nullable = true, length = 80)
	private String rightOrg;

	/** 他项权证号到期日 **/
	@Column(name = "REGISTER_END_DATE", unique = false, nullable = true, length = 20)
	private String registerEndDate;

	/** 来源渠道 **/
	@Column(name = "SOURCE_PATH", unique = false, nullable = true, length = 20)
	private String sourcePath;

	/** 创建人用户编号 **/
	@Column(name = "CREATE_USER_ID", unique = false, nullable = true, length = 40)
	private String createUserId;

	/** 登记证明编号/止付通知书编号 **/
	@Column(name = "REGISTER_NO", unique = false, nullable = true, length = 100)
	private String registerNo;

	/** 抵押登记机关 **/
	@Column(name = "REGISTER_ORG", unique = false, nullable = true, length = 60)
	private String registerOrg;

	/** 登记/止付日期 **/
	@Column(name = "REGISTER_DATE", unique = false, nullable = true, length = 20)
	private String registerDate;

	/** 投保险种  **/
	@Column(name = "ASSURANCE_TYPE", unique = false, nullable = true, length = 60)
	private String assuranceType;

	/** 保单编号 **/
	@Column(name = "ASSURANCE_NO", unique = false, nullable = true, length = 40)
	private String assuranceNo;

	/** 保险金额（元） **/
	@Column(name = "ASSURANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assuranceAmt;

	/** 投保日期 **/
	@Column(name = "ASSURANCE_DATE", unique = false, nullable = true, length = 20)
	private String assuranceDate;

	/** 保险到期日 **/
	@Column(name = "ASSURANCE_END_DATE", unique = false, nullable = true, length = 20)
	private String assuranceEndDate;

	/** 保险公司名称 **/
	@Column(name = "ASSURANCE_COM_NAME", unique = false, nullable = true, length = 200)
	private String assuranceComName;

	/** 租赁情况  STD_TENANCY_CIRCE **/
	@Column(name = "TENANCY_CIRCE", unique = false, nullable = true, length = 5)
	private String tenancyCirce;

	/** 租赁到期日期 **/
	@Column(name = "TENANCY_END_DATE", unique = false, nullable = true, length = 20)
	private String tenancyEndDate;

	/** 年租金（元） **/
	@Column(name = "TENANCY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tenancyAmt;

	/** 保管人 **/
	@Column(name = "KEEP_USER", unique = false, nullable = true, length = 40)
	private String keepUser;

	/** 申请入库时间 **/
	@Column(name = "APP_IN_DATE", unique = false, nullable = true, length = 20)
	private String appInDate;

	/** 入库时间 **/
	@Column(name = "IN_DATE", unique = false, nullable = true, length = 20)
	private String inDate;

	/** 申请出库时间 **/
	@Column(name = "APP_OUT_DATE", unique = false, nullable = true, length = 20)
	private String appOutDate;

	/** 出库时间 **/
	@Column(name = "OUT_DATE", unique = false, nullable = true, length = 20)
	private String outDate;

	/** 出库事由 **/
	@Column(name = "OUT_REASON", unique = false, nullable = true, length = 500)
	private String outReason;

	/** 新押品编码 **/
	@Column(name = "NEWCODE", unique = false, nullable = true, length = 32)
	private String newcode;

	/** 新押品编码名称 **/
	@Column(name = "NEWLABEL", unique = false, nullable = true, length = 200)
	private String newlabel;

	/** 质押特定类型 **/
	@Column(name = "STYLE", unique = false, nullable = true, length = 10)
	private String style;

	/** 外部评级机构 **/
	@Column(name = "OUTER_LEVEL_ORG", unique = false, nullable = true, length = 10)
	private String outerLevelOrg;

	/** 外部评级等级 **/
	@Column(name = "OUTER_LEVEL", unique = false, nullable = true, length = 6)
	private String outerLevel;

	/** 是否到期 STD_ZB_YES_NO **/
	@Column(name = "IS_END_DATE", unique = false, nullable = true, length = 5)
	private String isEndDate;

	/** 是否票据池 STD_ZB_YES_NO **/
	@Column(name = "IS_PJC", unique = false, nullable = true, length = 5)
	private String isPjc;

	/** 票据池状态 STD_PJC_STATUS **/
	@Column(name = "RC_STATE", unique = false, nullable = true, length = 5)
	private String rcState;

	/** 票据入池时质押率 **/
	@Column(name = "PJC_ZYL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pjcZyl;

	/** 入池日期 **/
	@Column(name = "RC_DATE", unique = false, nullable = true, length = 20)
	private String rcDate;

	/** 出池日期 **/
	@Column(name = "CC_DATE", unique = false, nullable = true, length = 20)
	private String ccDate;

	/** 票据池合同编号 **/
	@Column(name = "PJC_CONT_NO", unique = false, nullable = true, length = 40)
	private String pjcContNo;

	/** 车架号 **/
	@Column(name = "VEHICLE_FLAG_CD", unique = false, nullable = true, length = 40)
	private String vehicleFlagCd;

	/** 审批状态STD_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 5)
	private String managerId;

	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 5)
	private String managerBrId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期  **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private String updDate;

	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 面积 **/
	@Column(name = "SQU", unique = false, nullable = true, length = 40)
	private String squ;

	/** 票据号 **/
	@Column(name = "DRFT_NO", unique = false, nullable = true, length = 40)
	private String drftNo;

	/** 票面金额 **/
	@Column(name = "DRFT_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal drftAmt;

	/** 出票日期 **/
	@Column(name = "ISSE_DATE", unique = false, nullable = true, length = 20)
	private String isseDate;

	/** 到期日期 **/
	@Column(name = "DRAFT_END_DATE", unique = false, nullable = true, length = 20)
	private String draftEndDate;

	/** 出票人名称 **/
	@Column(name = "DRWR_NAME", unique = false, nullable = true, length = 80)
	private String drwrName;

	/** 出票人账户 **/
	@Column(name = "DRWR_ACCNO", unique = false, nullable = true, length = 40)
	private String drwrAccno;

	/** 收款人名称 **/
	@Column(name = "PYEE_NAME", unique = false, nullable = true, length = 80)
	private String pyeeName;

	/** 出票人开户行行名 **/
	@Column(name = "DRWR_ACCTSVCRNM", unique = false, nullable = true, length = 80)
	private String drwrAcctsvcrnm;

	/** 出票人开户行行号 **/
	@Column(name = "DRWR_ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String drwrAcctsvcrNo;

	/** 收款人账号 **/
	@Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 40)
	private String pyeeAccno;

	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;

	/** 承兑行行名 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	/**
	 * @return guarNo
	 */
	public String getGuarNo() {
		return this.guarNo;
	}

	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}

	/**
	 * @return grtFlag
	 */
	public String getGrtFlag() {
		return this.grtFlag;
	}

	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}

	/**
	 * @return pldimnMemo
	 */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}

	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	/**
	 * @return guarTypeCd
	 */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}

	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}

	/**
	 * @return guarType
	 */
	public String getGuarType() {
		return this.guarType;
	}

	/**
	 * @param guarFirstCreateTime
	 */
	public void setGuarFirstCreateTime(String guarFirstCreateTime) {
		this.guarFirstCreateTime = guarFirstCreateTime;
	}

	/**
	 * @return guarFirstCreateTime
	 */
	public String getGuarFirstCreateTime() {
		return this.guarFirstCreateTime;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}

	/**
	 * @return guarCusId
	 */
	public String getGuarCusId() {
		return this.guarCusId;
	}

	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	/**
	 * @return guarCusName
	 */
	public String getGuarCusName() {
		return this.guarCusName;
	}

	/**
	 * @param guarCusType
	 */
	public void setGuarCusType(String guarCusType) {
		this.guarCusType = guarCusType;
	}

	/**
	 * @return guarCusType
	 */
	public String getGuarCusType() {
		return this.guarCusType;
	}

	/**
	 * @param guarCertType
	 */
	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType;
	}

	/**
	 * @return guarCertType
	 */
	public String getGuarCertType() {
		return this.guarCertType;
	}

	/**
	 * @param guarCertCode
	 */
	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode;
	}

	/**
	 * @return guarCertCode
	 */
	public String getGuarCertCode() {
		return this.guarCertCode;
	}

	/**
	 * @param assureCardNo
	 */
	public void setAssureCardNo(String assureCardNo) {
		this.assureCardNo = assureCardNo;
	}

	/**
	 * @return assureCardNo
	 */
	public String getAssureCardNo() {
		return this.assureCardNo;
	}

	/**
	 * @param relationInt
	 */
	public void setRelationInt(String relationInt) {
		this.relationInt = relationInt;
	}

	/**
	 * @return relationInt
	 */
	public String getRelationInt() {
		return this.relationInt;
	}

	/**
	 * @param commonAssetsInd
	 */
	public void setCommonAssetsInd(String commonAssetsInd) {
		this.commonAssetsInd = commonAssetsInd;
	}

	/**
	 * @return commonAssetsInd
	 */
	public String getCommonAssetsInd() {
		return this.commonAssetsInd;
	}

	/**
	 * @param occupyofowner
	 */
	public void setOccupyofowner(String occupyofowner) {
		this.occupyofowner = occupyofowner;
	}

	/**
	 * @return occupyofowner
	 */
	public String getOccupyofowner() {
		return this.occupyofowner;
	}

	/**
	 * @param insuranceInd
	 */
	public void setInsuranceInd(String insuranceInd) {
		this.insuranceInd = insuranceInd;
	}

	/**
	 * @return insuranceInd
	 */
	public String getInsuranceInd() {
		return this.insuranceInd;
	}

	/**
	 * @param notarizationInd
	 */
	public void setNotarizationInd(String notarizationInd) {
		this.notarizationInd = notarizationInd;
	}

	/**
	 * @return notarizationInd
	 */
	public String getNotarizationInd() {
		return this.notarizationInd;
	}

	/**
	 * @param forcecreateInd
	 */
	public void setForcecreateInd(String forcecreateInd) {
		this.forcecreateInd = forcecreateInd;
	}

	/**
	 * @return forcecreateInd
	 */
	public String getForcecreateInd() {
		return this.forcecreateInd;
	}

	/**
	 * @param forcecreateReason
	 */
	public void setForcecreateReason(String forcecreateReason) {
		this.forcecreateReason = forcecreateReason;
	}

	/**
	 * @return forcecreateReason
	 */
	public String getForcecreateReason() {
		return this.forcecreateReason;
	}

	/**
	 * @param guarBusistate
	 */
	public void setGuarBusistate(String guarBusistate) {
		this.guarBusistate = guarBusistate;
	}

	/**
	 * @return guarBusistate
	 */
	public String getGuarBusistate() {
		return this.guarBusistate;
	}

	/**
	 * @param guarState
	 */
	public void setGuarState(String guarState) {
		this.guarState = guarState;
	}

	/**
	 * @return guarState
	 */
	public String getGuarState() {
		return this.guarState;
	}

	/**
	 * @param isOwnershipClear
	 */
	public void setIsOwnershipClear(String isOwnershipClear) {
		this.isOwnershipClear = isOwnershipClear;
	}

	/**
	 * @return isOwnershipClear
	 */
	public String getIsOwnershipClear() {
		return this.isOwnershipClear;
	}

	/**
	 * @param isDebtAsset
	 */
	public void setIsDebtAsset(String isDebtAsset) {
		this.isDebtAsset = isDebtAsset;
	}

	/**
	 * @return isDebtAsset
	 */
	public String getIsDebtAsset() {
		return this.isDebtAsset;
	}

	/**
	 * @param forbidCirBelogFlag
	 */
	public void setForbidCirBelogFlag(String forbidCirBelogFlag) {
		this.forbidCirBelogFlag = forbidCirBelogFlag;
	}

	/**
	 * @return forbidCirBelogFlag
	 */
	public String getForbidCirBelogFlag() {
		return this.forbidCirBelogFlag;
	}

	/**
	 * @param supervisionFlag
	 */
	public void setSupervisionFlag(String supervisionFlag) {
		this.supervisionFlag = supervisionFlag;
	}

	/**
	 * @return supervisionFlag
	 */
	public String getSupervisionFlag() {
		return this.supervisionFlag;
	}

	/**
	 * @param supervisionDate
	 */
	public void setSupervisionDate(String supervisionDate) {
		this.supervisionDate = supervisionDate;
	}

	/**
	 * @return supervisionDate
	 */
	public String getSupervisionDate() {
		return this.supervisionDate;
	}

	/**
	 * @param enforeFlag
	 */
	public void setEnforeFlag(String enforeFlag) {
		this.enforeFlag = enforeFlag;
	}

	/**
	 * @return enforeFlag
	 */
	public String getEnforeFlag() {
		return this.enforeFlag;
	}

	/**
	 * @param rightOtherNo
	 */
	public void setRightOtherNo(String rightOtherNo) {
		this.rightOtherNo = rightOtherNo;
	}

	/**
	 * @return rightOtherNo
	 */
	public String getRightOtherNo() {
		return this.rightOtherNo;
	}

	/**
	 * @param regState
	 */
	public void setRegState(String regState) {
		this.regState = regState;
	}

	/**
	 * @return regState
	 */
	public String getRegState() {
		return this.regState;
	}

	/**
	 * @param defEffectType
	 */
	public void setDefEffectType(String defEffectType) {
		this.defEffectType = defEffectType;
	}

	/**
	 * @return defEffectType
	 */
	public String getDefEffectType() {
		return this.defEffectType;
	}

	/**
	 * @param otherBackGuarInd
	 */
	public void setOtherBackGuarInd(String otherBackGuarInd) {
		this.otherBackGuarInd = otherBackGuarInd;
	}

	/**
	 * @return otherBackGuarInd
	 */
	public String getOtherBackGuarInd() {
		return this.otherBackGuarInd;
	}

	/**
	 * @param pldimnDebitRelative
	 */
	public void setPldimnDebitRelative(String pldimnDebitRelative) {
		this.pldimnDebitRelative = pldimnDebitRelative;
	}

	/**
	 * @return pldimnDebitRelative
	 */
	public String getPldimnDebitRelative() {
		return this.pldimnDebitRelative;
	}

	/**
	 * @param supervisionConvenience
	 */
	public void setSupervisionConvenience(String supervisionConvenience) {
		this.supervisionConvenience = supervisionConvenience;
	}

	/**
	 * @return supervisionConvenience
	 */
	public String getSupervisionConvenience() {
		return this.supervisionConvenience;
	}

	/**
	 * @param lawValidity
	 */
	public void setLawValidity(String lawValidity) {
		this.lawValidity = lawValidity;
	}

	/**
	 * @return lawValidity
	 */
	public String getLawValidity() {
		return this.lawValidity;
	}

	/**
	 * @param pldimnCommon
	 */
	public void setPldimnCommon(String pldimnCommon) {
		this.pldimnCommon = pldimnCommon;
	}

	/**
	 * @return pldimnCommon
	 */
	public String getPldimnCommon() {
		return this.pldimnCommon;
	}

	/**
	 * @param pldimnCashability
	 */
	public void setPldimnCashability(String pldimnCashability) {
		this.pldimnCashability = pldimnCashability;
	}

	/**
	 * @return pldimnCashability
	 */
	public String getPldimnCashability() {
		return this.pldimnCashability;
	}

	/**
	 * @param priceWave
	 */
	public void setPriceWave(String priceWave) {
		this.priceWave = priceWave;
	}

	/**
	 * @return priceWave
	 */
	public String getPriceWave() {
		return this.priceWave;
	}

	/**
	 * @param isScanMater
	 */
	public void setIsScanMater(String isScanMater) {
		this.isScanMater = isScanMater;
	}

	/**
	 * @return isScanMater
	 */
	public String getIsScanMater() {
		return this.isScanMater;
	}

	/**
	 * @param ringhtNoAndOtherNo
	 */
	public void setRinghtNoAndOtherNo(String ringhtNoAndOtherNo) {
		this.ringhtNoAndOtherNo = ringhtNoAndOtherNo;
	}

	/**
	 * @return ringhtNoAndOtherNo
	 */
	public String getRinghtNoAndOtherNo() {
		return this.ringhtNoAndOtherNo;
	}

	/**
	 * @param mybackGuarFirstSeq
	 */
	public void setMybackGuarFirstSeq(String mybackGuarFirstSeq) {
		this.mybackGuarFirstSeq = mybackGuarFirstSeq;
	}

	/**
	 * @return mybackGuarFirstSeq
	 */
	public String getMybackGuarFirstSeq() {
		return this.mybackGuarFirstSeq;
	}

	/**
	 * @param legalPriPayment
	 */
	public void setLegalPriPayment(java.math.BigDecimal legalPriPayment) {
		this.legalPriPayment = legalPriPayment;
	}

	/**
	 * @return legalPriPayment
	 */
	public java.math.BigDecimal getLegalPriPayment() {
		return this.legalPriPayment;
	}

	/**
	 * @param createSys
	 */
	public void setCreateSys(String createSys) {
		this.createSys = createSys;
	}

	/**
	 * @return createSys
	 */
	public String getCreateSys() {
		return this.createSys;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return remark
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param accountManager
	 */
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	/**
	 * @return accountManager
	 */
	public String getAccountManager() {
		return this.accountManager;
	}

	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}

	/**
	 * @return evalAmt
	 */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}

	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}

	/**
	 * @return evalDate
	 */
	public String getEvalDate() {
		return this.evalDate;
	}

	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}

	/**
	 * @return confirmAmt
	 */
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId;
	}

	/**
	 * @return certiRecordId
	 */
	public String getCertiRecordId() {
		return this.certiRecordId;
	}

	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate;
	}

	/**
	 * @return certiEndDate
	 */
	public String getCertiEndDate() {
		return this.certiEndDate;
	}

	/**
	 * @param mortagageMaxRate
	 */
	public void setMortagageMaxRate(java.math.BigDecimal mortagageMaxRate) {
		this.mortagageMaxRate = mortagageMaxRate;
	}

	/**
	 * @return mortagageMaxRate
	 */
	public java.math.BigDecimal getMortagageMaxRate() {
		return this.mortagageMaxRate;
	}

	/**
	 * @param mortagageRate
	 */
	public void setMortagageRate(java.math.BigDecimal mortagageRate) {
		this.mortagageRate = mortagageRate;
	}

	/**
	 * @return mortagageRate
	 */
	public java.math.BigDecimal getMortagageRate() {
		return this.mortagageRate;
	}

	/**
	 * @param maxMortagageAmt
	 */
	public void setMaxMortagageAmt(java.math.BigDecimal maxMortagageAmt) {
		this.maxMortagageAmt = maxMortagageAmt;
	}

	/**
	 * @return maxMortagageAmt
	 */
	public java.math.BigDecimal getMaxMortagageAmt() {
		return this.maxMortagageAmt;
	}

	/**
	 * @param pldLocation
	 */
	public void setPldLocation(String pldLocation) {
		this.pldLocation = pldLocation;
	}

	/**
	 * @return pldLocation
	 */
	public String getPldLocation() {
		return this.pldLocation;
	}

	/**
	 * @param refName
	 */
	public void setRefName(String refName) {
		this.refName = refName;
	}

	/**
	 * @return refName
	 */
	public String getRefName() {
		return this.refName;
	}

	/**
	 * @param isDisputed
	 */
	public void setIsDisputed(String isDisputed) {
		this.isDisputed = isDisputed;
	}

	/**
	 * @return isDisputed
	 */
	public String getIsDisputed() {
		return this.isDisputed;
	}

	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}

	/**
	 * @return coreGuarantyNo
	 */
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}

	/**
	 * @param coreGuarantySeq
	 */
	public void setCoreGuarantySeq(String coreGuarantySeq) {
		this.coreGuarantySeq = coreGuarantySeq;
	}

	/**
	 * @return coreGuarantySeq
	 */
	public String getCoreGuarantySeq() {
		return this.coreGuarantySeq;
	}

	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	/**
	 * @return finaBrId
	 */
	public String getFinaBrId() {
		return this.finaBrId;
	}

	/**
	 * @param confirmDate
	 */
	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return confirmDate
	 */
	public String getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType;
	}

	/**
	 * @return evalType
	 */
	public String getEvalType() {
		return this.evalType;
	}

	/**
	 * @param evalOrg
	 */
	public void setEvalOrg(String evalOrg) {
		this.evalOrg = evalOrg;
	}

	/**
	 * @return evalOrg
	 */
	public String getEvalOrg() {
		return this.evalOrg;
	}

	/**
	 * @param evalOrgInsCode
	 */
	public void setEvalOrgInsCode(String evalOrgInsCode) {
		this.evalOrgInsCode = evalOrgInsCode;
	}

	/**
	 * @return evalOrgInsCode
	 */
	public String getEvalOrgInsCode() {
		return this.evalOrgInsCode;
	}

	/**
	 * @param buyAmt
	 */
	public void setBuyAmt(java.math.BigDecimal buyAmt) {
		this.buyAmt = buyAmt;
	}

	/**
	 * @return buyAmt
	 */
	public java.math.BigDecimal getBuyAmt() {
		return this.buyAmt;
	}

	/**
	 * @param innerEvalAmt
	 */
	public void setInnerEvalAmt(java.math.BigDecimal innerEvalAmt) {
		this.innerEvalAmt = innerEvalAmt;
	}

	/**
	 * @return innerEvalAmt
	 */
	public java.math.BigDecimal getInnerEvalAmt() {
		return this.innerEvalAmt;
	}

	/**
	 * @param nextEvalEndDate
	 */
	public void setNextEvalEndDate(String nextEvalEndDate) {
		this.nextEvalEndDate = nextEvalEndDate;
	}

	/**
	 * @return nextEvalEndDate
	 */
	public String getNextEvalEndDate() {
		return this.nextEvalEndDate;
	}

	/**
	 * @param nextEvalDate
	 */
	public void setNextEvalDate(String nextEvalDate) {
		this.nextEvalDate = nextEvalDate;
	}

	/**
	 * @return nextEvalDate
	 */
	public String getNextEvalDate() {
		return this.nextEvalDate;
	}

	/**
	 * @param isSpecial
	 */
	public void setIsSpecial(String isSpecial) {
		this.isSpecial = isSpecial;
	}

	/**
	 * @return isSpecial
	 */
	public String getIsSpecial() {
		return this.isSpecial;
	}

	/**
	 * @param isSpecialAsset
	 */
	public void setIsSpecialAsset(String isSpecialAsset) {
		this.isSpecialAsset = isSpecialAsset;
	}

	/**
	 * @return isSpecialAsset
	 */
	public String getIsSpecialAsset() {
		return this.isSpecialAsset;
	}

	/**
	 * @param isExistinghome
	 */
	public void setIsExistinghome(String isExistinghome) {
		this.isExistinghome = isExistinghome;
	}

	/**
	 * @return isExistinghome
	 */
	public String getIsExistinghome() {
		return this.isExistinghome;
	}

	/**
	 * @param isUpdate
	 */
	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * @return isUpdate
	 */
	public String getIsUpdate() {
		return this.isUpdate;
	}

	/**
	 * @param lmtEvalAmt
	 */
	public void setLmtEvalAmt(java.math.BigDecimal lmtEvalAmt) {
		this.lmtEvalAmt = lmtEvalAmt;
	}

	/**
	 * @return lmtEvalAmt
	 */
	public java.math.BigDecimal getLmtEvalAmt() {
		return this.lmtEvalAmt;
	}

	/**
	 * @param areaCode
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	/**
	 * @return areaCode
	 */
	public String getAreaCode() {
		return this.areaCode;
	}

	/**
	 * @param areaName
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * @return areaName
	 */
	public String getAreaName() {
		return this.areaName;
	}

	/**
	 * @param copyFrom
	 */
	public void setCopyFrom(String copyFrom) {
		this.copyFrom = copyFrom;
	}

	/**
	 * @return copyFrom
	 */
	public String getCopyFrom() {
		return this.copyFrom;
	}

	/**
	 * @param isChanged
	 */
	public void setIsChanged(String isChanged) {
		this.isChanged = isChanged;
	}

	/**
	 * @return isChanged
	 */
	public String getIsChanged() {
		return this.isChanged;
	}

	/**
	 * @param inoutSerno
	 */
	public void setInoutSerno(String inoutSerno) {
		this.inoutSerno = inoutSerno;
	}

	/**
	 * @return inoutSerno
	 */
	public String getInoutSerno() {
		return this.inoutSerno;
	}

	/**
	 * @param inUser
	 */
	public void setInUser(String inUser) {
		this.inUser = inUser;
	}

	/**
	 * @return inUser
	 */
	public String getInUser() {
		return this.inUser;
	}

	/**
	 * @param rightType
	 */
	public void setRightType(String rightType) {
		this.rightType = rightType;
	}

	/**
	 * @return rightType
	 */
	public String getRightType() {
		return this.rightType;
	}

	/**
	 * @param rightCertTypeCode
	 */
	public void setRightCertTypeCode(String rightCertTypeCode) {
		this.rightCertTypeCode = rightCertTypeCode;
	}

	/**
	 * @return rightCertTypeCode
	 */
	public String getRightCertTypeCode() {
		return this.rightCertTypeCode;
	}

	/**
	 * @param rightCertNo
	 */
	public void setRightCertNo(String rightCertNo) {
		this.rightCertNo = rightCertNo;
	}

	/**
	 * @return rightCertNo
	 */
	public String getRightCertNo() {
		return this.rightCertNo;
	}

	/**
	 * @param rightOrg
	 */
	public void setRightOrg(String rightOrg) {
		this.rightOrg = rightOrg;
	}

	/**
	 * @return rightOrg
	 */
	public String getRightOrg() {
		return this.rightOrg;
	}

	/**
	 * @param registerEndDate
	 */
	public void setRegisterEndDate(String registerEndDate) {
		this.registerEndDate = registerEndDate;
	}

	/**
	 * @return registerEndDate
	 */
	public String getRegisterEndDate() {
		return this.registerEndDate;
	}

	/**
	 * @param sourcePath
	 */
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	/**
	 * @return sourcePath
	 */
	public String getSourcePath() {
		return this.sourcePath;
	}

	/**
	 * @param createUserId
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return createUserId
	 */
	public String getCreateUserId() {
		return this.createUserId;
	}

	/**
	 * @param registerNo
	 */
	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}

	/**
	 * @return registerNo
	 */
	public String getRegisterNo() {
		return this.registerNo;
	}

	/**
	 * @param registerOrg
	 */
	public void setRegisterOrg(String registerOrg) {
		this.registerOrg = registerOrg;
	}

	/**
	 * @return registerOrg
	 */
	public String getRegisterOrg() {
		return this.registerOrg;
	}

	/**
	 * @param registerDate
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return registerDate
	 */
	public String getRegisterDate() {
		return this.registerDate;
	}

	/**
	 * @param assuranceType
	 */
	public void setAssuranceType(String assuranceType) {
		this.assuranceType = assuranceType;
	}

	/**
	 * @return assuranceType
	 */
	public String getAssuranceType() {
		return this.assuranceType;
	}

	/**
	 * @param assuranceNo
	 */
	public void setAssuranceNo(String assuranceNo) {
		this.assuranceNo = assuranceNo;
	}

	/**
	 * @return assuranceNo
	 */
	public String getAssuranceNo() {
		return this.assuranceNo;
	}

	/**
	 * @param assuranceAmt
	 */
	public void setAssuranceAmt(java.math.BigDecimal assuranceAmt) {
		this.assuranceAmt = assuranceAmt;
	}

	/**
	 * @return assuranceAmt
	 */
	public java.math.BigDecimal getAssuranceAmt() {
		return this.assuranceAmt;
	}

	/**
	 * @param assuranceDate
	 */
	public void setAssuranceDate(String assuranceDate) {
		this.assuranceDate = assuranceDate;
	}

	/**
	 * @return assuranceDate
	 */
	public String getAssuranceDate() {
		return this.assuranceDate;
	}

	/**
	 * @param assuranceEndDate
	 */
	public void setAssuranceEndDate(String assuranceEndDate) {
		this.assuranceEndDate = assuranceEndDate;
	}

	/**
	 * @return assuranceEndDate
	 */
	public String getAssuranceEndDate() {
		return this.assuranceEndDate;
	}

	/**
	 * @param assuranceComName
	 */
	public void setAssuranceComName(String assuranceComName) {
		this.assuranceComName = assuranceComName;
	}

	/**
	 * @return assuranceComName
	 */
	public String getAssuranceComName() {
		return this.assuranceComName;
	}

	/**
	 * @param tenancyCirce
	 */
	public void setTenancyCirce(String tenancyCirce) {
		this.tenancyCirce = tenancyCirce;
	}

	/**
	 * @return tenancyCirce
	 */
	public String getTenancyCirce() {
		return this.tenancyCirce;
	}

	/**
	 * @param tenancyEndDate
	 */
	public void setTenancyEndDate(String tenancyEndDate) {
		this.tenancyEndDate = tenancyEndDate;
	}

	/**
	 * @return tenancyEndDate
	 */
	public String getTenancyEndDate() {
		return this.tenancyEndDate;
	}

	/**
	 * @param tenancyAmt
	 */
	public void setTenancyAmt(java.math.BigDecimal tenancyAmt) {
		this.tenancyAmt = tenancyAmt;
	}

	/**
	 * @return tenancyAmt
	 */
	public java.math.BigDecimal getTenancyAmt() {
		return this.tenancyAmt;
	}

	/**
	 * @param keepUser
	 */
	public void setKeepUser(String keepUser) {
		this.keepUser = keepUser;
	}

	/**
	 * @return keepUser
	 */
	public String getKeepUser() {
		return this.keepUser;
	}

	/**
	 * @param appInDate
	 */
	public void setAppInDate(String appInDate) {
		this.appInDate = appInDate;
	}

	/**
	 * @return appInDate
	 */
	public String getAppInDate() {
		return this.appInDate;
	}

	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}

	/**
	 * @return inDate
	 */
	public String getInDate() {
		return this.inDate;
	}

	/**
	 * @param appOutDate
	 */
	public void setAppOutDate(String appOutDate) {
		this.appOutDate = appOutDate;
	}

	/**
	 * @return appOutDate
	 */
	public String getAppOutDate() {
		return this.appOutDate;
	}

	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}

	/**
	 * @return outDate
	 */
	public String getOutDate() {
		return this.outDate;
	}

	/**
	 * @param outReason
	 */
	public void setOutReason(String outReason) {
		this.outReason = outReason;
	}

	/**
	 * @return outReason
	 */
	public String getOutReason() {
		return this.outReason;
	}

	/**
	 * @param newcode
	 */
	public void setNewcode(String newcode) {
		this.newcode = newcode;
	}

	/**
	 * @return newcode
	 */
	public String getNewcode() {
		return this.newcode;
	}

	/**
	 * @param newlabel
	 */
	public void setNewlabel(String newlabel) {
		this.newlabel = newlabel;
	}

	/**
	 * @return newlabel
	 */
	public String getNewlabel() {
		return this.newlabel;
	}

	/**
	 * @param style
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * @return style
	 */
	public String getStyle() {
		return this.style;
	}

	/**
	 * @param outerLevelOrg
	 */
	public void setOuterLevelOrg(String outerLevelOrg) {
		this.outerLevelOrg = outerLevelOrg;
	}

	/**
	 * @return outerLevelOrg
	 */
	public String getOuterLevelOrg() {
		return this.outerLevelOrg;
	}

	/**
	 * @param outerLevel
	 */
	public void setOuterLevel(String outerLevel) {
		this.outerLevel = outerLevel;
	}

	/**
	 * @return outerLevel
	 */
	public String getOuterLevel() {
		return this.outerLevel;
	}

	/**
	 * @param isEndDate
	 */
	public void setIsEndDate(String isEndDate) {
		this.isEndDate = isEndDate;
	}

	/**
	 * @return isEndDate
	 */
	public String getIsEndDate() {
		return this.isEndDate;
	}

	/**
	 * @param isPjc
	 */
	public void setIsPjc(String isPjc) {
		this.isPjc = isPjc;
	}

	/**
	 * @return isPjc
	 */
	public String getIsPjc() {
		return this.isPjc;
	}

	/**
	 * @param rcState
	 */
	public void setRcState(String rcState) {
		this.rcState = rcState;
	}

	/**
	 * @return rcState
	 */
	public String getRcState() {
		return this.rcState;
	}

	/**
	 * @param pjcZyl
	 */
	public void setPjcZyl(java.math.BigDecimal pjcZyl) {
		this.pjcZyl = pjcZyl;
	}

	/**
	 * @return pjcZyl
	 */
	public java.math.BigDecimal getPjcZyl() {
		return this.pjcZyl;
	}

	/**
	 * @param rcDate
	 */
	public void setRcDate(String rcDate) {
		this.rcDate = rcDate;
	}

	/**
	 * @return rcDate
	 */
	public String getRcDate() {
		return this.rcDate;
	}

	/**
	 * @param ccDate
	 */
	public void setCcDate(String ccDate) {
		this.ccDate = ccDate;
	}

	/**
	 * @return ccDate
	 */
	public String getCcDate() {
		return this.ccDate;
	}

	/**
	 * @param pjcContNo
	 */
	public void setPjcContNo(String pjcContNo) {
		this.pjcContNo = pjcContNo;
	}

	/**
	 * @return pjcContNo
	 */
	public String getPjcContNo() {
		return this.pjcContNo;
	}

	/**
	 * @param vehicleFlagCd
	 */
	public void setVehicleFlagCd(String vehicleFlagCd) {
		this.vehicleFlagCd = vehicleFlagCd;
	}

	/**
	 * @return vehicleFlagCd
	 */
	public String getVehicleFlagCd() {
		return this.vehicleFlagCd;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return managerId;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return managerBrId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getSqu() {
		return squ;
	}

	public void setSqu(String squ) {
		this.squ = squ;
	}

	public String getDrftNo() {
		return drftNo;
	}

	public void setDrftNo(String drftNo) {
		this.drftNo = drftNo;
	}

	public BigDecimal getDrftAmt() {
		return drftAmt;
	}

	public void setDrftAmt(BigDecimal drftAmt) {
		this.drftAmt = drftAmt;
	}

	public String getIsseDate() {
		return isseDate;
	}

	public void setIsseDate(String isseDate) {
		this.isseDate = isseDate;
	}

	public String getDraftEndDate() {
		return draftEndDate;
	}

	public void setDraftEndDate(String draftEndDate) {
		this.draftEndDate = draftEndDate;
	}

	public String getDrwrName() {
		return drwrName;
	}

	public void setDrwrName(String drwrName) {
		this.drwrName = drwrName;
	}

	public String getDrwrAccno() {
		return drwrAccno;
	}

	public void setDrwrAccno(String drwrAccno) {
		this.drwrAccno = drwrAccno;
	}

	public String getPyeeName() {
		return pyeeName;
	}

	public void setPyeeName(String pyeeName) {
		this.pyeeName = pyeeName;
	}

	public String getDrwrAcctsvcrnm() {
		return drwrAcctsvcrnm;
	}

	public void setDrwrAcctsvcrnm(String drwrAcctsvcrnm) {
		this.drwrAcctsvcrnm = drwrAcctsvcrnm;
	}

	public String getDrwrAcctsvcrNo() {
		return drwrAcctsvcrNo;
	}

	public void setDrwrAcctsvcrNo(String drwrAcctsvcrNo) {
		this.drwrAcctsvcrNo = drwrAcctsvcrNo;
	}

	public String getPyeeAccno() {
		return pyeeAccno;
	}

	public void setPyeeAccno(String pyeeAccno) {
		this.pyeeAccno = pyeeAccno;
	}

	public String getAorgNo() {
		return aorgNo;
	}

	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}

	public String getAorgName() {
		return aorgName;
	}

	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}