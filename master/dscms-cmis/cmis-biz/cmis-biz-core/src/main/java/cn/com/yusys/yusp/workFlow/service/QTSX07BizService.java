package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 小微人民币利率定价申请流程业务处理类
 *
 * @author macm
 * @version 1.0
 */
@Service
public class QTSX07BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(QTSX07BizService.class);

    @Autowired
    private QTSX02BizService qTSX02BizService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        qTSX02BizService.QT002biz(resultInstanceDto);
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.QTSX07.equals(flowCode);
    }
}
