package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApprReport
 * @类描述: iqp_appr_report数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:17:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApprReportDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 发放方式 STD_ZB_GRANT_TYPE **/
	private String grantType;
	
	/** 放款模式 STD_ZB_LOAN_MODE **/
	private String loanMode;
	
	/** 支付方式 STD_ZB_PAT_TYPE **/
	private String patType;
	
	/** 贷款审核方式 STD_ZB_LOAN_CHECK_TYPE **/
	private String loanChkTyp;
	
	/** 核准意见 STD_ZB_APPRV_OPT **/
	private String apprvOpt;
	
	/** 具体核准意见  **/
	private String apprvOptDesc;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param grantType
	 */
	public void setGrantType(String grantType) {
		this.grantType = grantType == null ? null : grantType.trim();
	}
	
    /**
     * @return GrantType
     */	
	public String getGrantType() {
		return this.grantType;
	}
	
	/**
	 * @param loanMode
	 */
	public void setLoanMode(String loanMode) {
		this.loanMode = loanMode == null ? null : loanMode.trim();
	}
	
    /**
     * @return LoanMode
     */	
	public String getLoanMode() {
		return this.loanMode;
	}
	
	/**
	 * @param patType
	 */
	public void setPatType(String patType) {
		this.patType = patType == null ? null : patType.trim();
	}
	
    /**
     * @return PatType
     */	
	public String getPatType() {
		return this.patType;
	}
	
	/**
	 * @param loanChkTyp
	 */
	public void setLoanChkTyp(String loanChkTyp) {
		this.loanChkTyp = loanChkTyp == null ? null : loanChkTyp.trim();
	}
	
    /**
     * @return LoanChkTyp
     */	
	public String getLoanChkTyp() {
		return this.loanChkTyp;
	}
	
	/**
	 * @param apprvOpt
	 */
	public void setApprvOpt(String apprvOpt) {
		this.apprvOpt = apprvOpt == null ? null : apprvOpt.trim();
	}
	
    /**
     * @return ApprvOpt
     */	
	public String getApprvOpt() {
		return this.apprvOpt;
	}
	
	/**
	 * @param apprvOptDesc
	 */
	public void setApprvOptDesc(String apprvOptDesc) {
		this.apprvOptDesc = apprvOptDesc == null ? null : apprvOptDesc.trim();
	}
	
    /**
     * @return ApprvOptDesc
     */	
	public String getApprvOptDesc() {
		return this.apprvOptDesc;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}