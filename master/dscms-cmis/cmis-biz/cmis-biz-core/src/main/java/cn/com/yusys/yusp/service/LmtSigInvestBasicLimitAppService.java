/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.domain.LmtSigInvestSubApp;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLimitAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestSubAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLimitAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 15:46:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicLimitAppService {

    @Autowired
    private LmtSigInvestBasicLimitAppMapper lmtSigInvestBasicLimitAppMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtSigInvestAppMapper lmtSigInvestAppMapper;

    @Autowired
    private LmtSigInvestSubAppMapper lmtSigInvestSubAppMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService;

    /**
     * 生成主键id
     * @return
     */
    public String generatePkId(){
        return sequenceTemplateClient.getSequenceTemplate(CmisBizConstants.PK_VALUE, new HashMap<>());
    }
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicLimitApp selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLimitAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicLimitApp> selectAll(QueryModel model) {
        List<LmtSigInvestBasicLimitApp> records = (List<LmtSigInvestBasicLimitApp>) lmtSigInvestBasicLimitAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicLimitApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicLimitApp record) {
        return lmtSigInvestBasicLimitAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicLimitApp record) {
        return lmtSigInvestBasicLimitAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicLimitApp record) {
        return lmtSigInvestBasicLimitAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicLimitApp record) {
        return lmtSigInvestBasicLimitAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLimitAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicLimitAppMapper.deleteByIds(ids);
    }


    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public List<LmtSigInvestBasicLimitApp> selectBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestBasicLimitApp> lmtSigInvestBasicLimitAppList = selectByModel(queryModel);
        if (lmtSigInvestBasicLimitAppList!=null && lmtSigInvestBasicLimitAppList.size()>0){
            return lmtSigInvestBasicLimitAppList;
        }
        return null;
    }

    /**
     * 根据底层明细表，在底层申请主表添加相应信息
     * @param record
     */
    public void insert(LmtSigInvestBasicInfoSub record){
        LmtSigInvestBasicLimitApp basicLimitApp = new LmtSigInvestBasicLimitApp();
        basicLimitApp.setPkId(generatePkId());
        basicLimitApp.setSerno(record.getSerno());
        basicLimitApp.setBasicSerno(record.getBasicSerno());
        basicLimitApp.setBasicCusId(record.getBasicCusId());
        basicLimitApp.setBasicCusName(record.getBasicCusName());
        basicLimitApp.setBasicCusCatalog(record.getBasicCusCatalog());
        basicLimitApp.setBasicCusType(record.getBasicCusType());
        basicLimitApp.setBasicLmtBizType(record.getUseBasicLmtItemNo());
        basicLimitApp.setBasicLmtBizTypeName(record.getUseBasicLmtItemName());

        //计算本行在该底层投资金额：底层资产余额*项目授信金额/项目总金额
        QueryModel model = new QueryModel();
        model.addCondition("serno",record.getSerno());
        BizInvestCommonService.checkParamsIsNull("serno",record.getSerno());
        model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppMapper.selectByModel(model);
        List<LmtSigInvestSubApp> lmtSigInvestSubAppList = lmtSigInvestSubAppService.selectByModel(model);
        if (lmtSigInvestSubAppList != null &&
                (lmtSigInvestSubAppList.get(0).getPrdTotalAmt() == null || BigDecimal.ZERO.compareTo(lmtSigInvestSubAppList.get(0).getPrdTotalAmt()) == 0 )){
            throw BizException.error(null,"9999","项目总金额获取失败");
        }
        if (lmtSigInvestApps!=null && lmtSigInvestApps.size()>0){
            BigDecimal BasicAssetBalanceAmt = record.getBasicAssetBalanceAmt();
            if (Objects.isNull(record.getBasicAssetBalanceAmt())) {
                BasicAssetBalanceAmt = BigDecimal.ZERO;
            }
            BigDecimal a = BasicAssetBalanceAmt.multiply(lmtSigInvestApps.get(0).getLmtAmt());
            if (!CollectionUtils.isEmpty(lmtSigInvestSubAppList)){
                basicLimitApp.setLmtAmt(NumberUtils.divide(a, lmtSigInvestSubAppList.get(0).getPrdTotalAmt()));
            }
            basicLimitApp.setLmtTerm(lmtSigInvestApps.get(0).getLmtTerm());
        }

        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

        basicLimitApp.setOprType(record.getOprType());
        basicLimitApp.setInputId(record.getInputId());
        basicLimitApp.setInputBrId(record.getInputBrId());
        basicLimitApp.setInputDate(openDay);
        basicLimitApp.setUpdId(record.getUpdId());
        basicLimitApp.setUpdBrId(record.getUpdBrId());
        basicLimitApp.setUpdDate(openDay);
        basicLimitApp.setUpdateTime(record.getUpdateTime());
        basicLimitApp.setCreateTime(record.getCreateTime());
        lmtSigInvestBasicLimitAppMapper.insert(basicLimitApp);
    }

    /**
     *  更新底层资产 其他担保/增信情况说明
     * @param basicSerno
     * @param content
     * @return
     */
    public int updateGuarDescExt(String basicSerno, String content) {
        LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp = selectByBasicSerno(basicSerno);
        if (lmtSigInvestBasicLimitApp != null) {
            lmtSigInvestBasicLimitApp.setGuarDescExt(content);
            return update(lmtSigInvestBasicLimitApp);
        }
        return 0;
    }





    public LmtSigInvestBasicLimitApp selectByBasicSerno(String basicSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("basicSerno", basicSerno);
        BizInvestCommonService.checkParamsIsNull("basicSerno",basicSerno);
        List<LmtSigInvestBasicLimitApp> lmtSigInvestBasicLimitApps = selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(lmtSigInvestBasicLimitApps)) {
            return lmtSigInvestBasicLimitApps.get(0);
        }
        return null;
    }

}
