package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpPrePayment
 * @类描述: iqp_pre_payment数据实体类
 * @功能描述: 
 * @创建人: AbsonZ
 * @创建时间: 2021-04-22 19:01:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpPrePaymentDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String iqpSerno;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 币种 **/
	private String curType;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** PvpLoanApp **/
	private String accStatus;
	
	/** 还款模式 **/
	private String repayMode;
	
	/** 还款模式明细 **/
	private String repayModeDesc;
	
	/** 资金来源 **/
	private String amtSource;
	
	/** 贷款收回方式 **/
	private String repayType;
	
	/** 还本金额 **/
	private java.math.BigDecimal repayAmt;
	
	/** 是否缩期 **/
	private String isPeriod;
	
	/** 缩期期数 **/
	private java.math.BigDecimal periodTimes;
	
	/** 贷款业务名称 **/
	private String loanBizName;
	
	/** 申请时间 **/
	private String appTime;
	
	/** 申请理由 **/
	private String appResn;
	
	/** 是否第三方还款 **/
	private String isOtherAcct;
	
	/** 第三方还款账号 **/
	private String repayAcctNo;
	
	/** 账户名称 **/
	private String repayAcctName;
	
	/** 还款账户子序号 **/
	private String repayAcctNoSrl;
	
	/** 1-归还正常本金 **/
	private java.math.BigDecimal zcbjAmt;
	
	/** 2-归还呆滞本金 **/
	private java.math.BigDecimal dzbjAmt;
	
	/** 3-归还应收应计利息 **/
	private java.math.BigDecimal ysyjlxXmt;
	
	/** 4-归还应收欠息 **/
	private java.math.BigDecimal ysqxAmt;
	
	/** 5-归还应收应计罚息 **/
	private java.math.BigDecimal ysyjfxAmt;
	
	/** 6-归还应收罚息 **/
	private java.math.BigDecimal ysfxAmt;
	
	/** 7-归还应计复息 **/
	private java.math.BigDecimal yjfxAmt;
	
	/** 8-归还应收罚金 **/
	private java.math.BigDecimal ysfjAmt;
	
	/** 9-归还逾期本金 **/
	private java.math.BigDecimal yqbjAmt;
	
	/** 10-归还呆账本金 **/
	private java.math.BigDecimal daizbjAmt;
	
	/** 11-归还催收应计立项 **/
	private java.math.BigDecimal csyjlxAmt;
	
	/** 12-归还催收欠息 **/
	private java.math.BigDecimal csqxAmt;
	
	/** 13-归还催收应计罚息 **/
	private java.math.BigDecimal csyjfxAmt;
	
	/** 14-归还催收罚息 **/
	private java.math.BigDecimal csfxAmt;
	
	/** 15-归还复息 **/
	private java.math.BigDecimal fxAmt;
	
	/** 本金 **/
	private java.math.BigDecimal totalBjAmt;
	
	/** 利息 **/
	private java.math.BigDecimal totalLxAmt;
	
	/** 罚息 **/
	private java.math.BigDecimal totalFxAmt;
	
	/** 复息 **/
	private java.math.BigDecimal totalFuxAmt;
	
	/** 核销本金 **/
	private java.math.BigDecimal totalHxbjAmt;
	
	/** 核销利息 **/
	private java.math.BigDecimal totalHxlxAmt;
	
	/** 拖欠利息总额 **/
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 申请状态 **/
	private String approveStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param repayModeDesc
	 */
	public void setRepayModeDesc(String repayModeDesc) {
		this.repayModeDesc = repayModeDesc == null ? null : repayModeDesc.trim();
	}
	
    /**
     * @return RepayModeDesc
     */	
	public String getRepayModeDesc() {
		return this.repayModeDesc;
	}
	
	/**
	 * @param amtSource
	 */
	public void setAmtSource(String amtSource) {
		this.amtSource = amtSource == null ? null : amtSource.trim();
	}
	
    /**
     * @return AmtSource
     */	
	public String getAmtSource() {
		return this.amtSource;
	}
	
	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType == null ? null : repayType.trim();
	}
	
    /**
     * @return RepayType
     */	
	public String getRepayType() {
		return this.repayType;
	}
	
	/**
	 * @param repayAmt
	 */
	public void setRepayAmt(java.math.BigDecimal repayAmt) {
		this.repayAmt = repayAmt;
	}
	
    /**
     * @return RepayAmt
     */	
	public java.math.BigDecimal getRepayAmt() {
		return this.repayAmt;
	}
	
	/**
	 * @param isPeriod
	 */
	public void setIsPeriod(String isPeriod) {
		this.isPeriod = isPeriod == null ? null : isPeriod.trim();
	}
	
    /**
     * @return IsPeriod
     */	
	public String getIsPeriod() {
		return this.isPeriod;
	}
	
	/**
	 * @param periodTimes
	 */
	public void setPeriodTimes(java.math.BigDecimal periodTimes) {
		this.periodTimes = periodTimes;
	}
	
    /**
     * @return PeriodTimes
     */	
	public java.math.BigDecimal getPeriodTimes() {
		return this.periodTimes;
	}
	
	/**
	 * @param loanBizName
	 */
	public void setLoanBizName(String loanBizName) {
		this.loanBizName = loanBizName == null ? null : loanBizName.trim();
	}
	
    /**
     * @return LoanBizName
     */	
	public String getLoanBizName() {
		return this.loanBizName;
	}
	
	/**
	 * @param appTime
	 */
	public void setAppTime(String appTime) {
		this.appTime = appTime == null ? null : appTime.trim();
	}
	
    /**
     * @return AppTime
     */	
	public String getAppTime() {
		return this.appTime;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn == null ? null : appResn.trim();
	}
	
    /**
     * @return AppResn
     */	
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param isOtherAcct
	 */
	public void setIsOtherAcct(String isOtherAcct) {
		this.isOtherAcct = isOtherAcct == null ? null : isOtherAcct.trim();
	}
	
    /**
     * @return IsOtherAcct
     */	
	public String getIsOtherAcct() {
		return this.isOtherAcct;
	}
	
	/**
	 * @param repayAcctNo
	 */
	public void setRepayAcctNo(String repayAcctNo) {
		this.repayAcctNo = repayAcctNo == null ? null : repayAcctNo.trim();
	}
	
    /**
     * @return RepayAcctNo
     */	
	public String getRepayAcctNo() {
		return this.repayAcctNo;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName == null ? null : repayAcctName.trim();
	}
	
    /**
     * @return RepayAcctName
     */	
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param repayAcctNoSrl
	 */
	public void setRepayAcctNoSrl(String repayAcctNoSrl) {
		this.repayAcctNoSrl = repayAcctNoSrl == null ? null : repayAcctNoSrl.trim();
	}
	
    /**
     * @return RepayAcctNoSrl
     */	
	public String getRepayAcctNoSrl() {
		return this.repayAcctNoSrl;
	}
	
	/**
	 * @param zcbjAmt
	 */
	public void setZcbjAmt(java.math.BigDecimal zcbjAmt) {
		this.zcbjAmt = zcbjAmt;
	}
	
    /**
     * @return ZcbjAmt
     */	
	public java.math.BigDecimal getZcbjAmt() {
		return this.zcbjAmt;
	}
	
	/**
	 * @param dzbjAmt
	 */
	public void setDzbjAmt(java.math.BigDecimal dzbjAmt) {
		this.dzbjAmt = dzbjAmt;
	}
	
    /**
     * @return DzbjAmt
     */	
	public java.math.BigDecimal getDzbjAmt() {
		return this.dzbjAmt;
	}
	
	/**
	 * @param ysyjlxXmt
	 */
	public void setYsyjlxXmt(java.math.BigDecimal ysyjlxXmt) {
		this.ysyjlxXmt = ysyjlxXmt;
	}
	
    /**
     * @return YsyjlxXmt
     */	
	public java.math.BigDecimal getYsyjlxXmt() {
		return this.ysyjlxXmt;
	}
	
	/**
	 * @param ysqxAmt
	 */
	public void setYsqxAmt(java.math.BigDecimal ysqxAmt) {
		this.ysqxAmt = ysqxAmt;
	}
	
    /**
     * @return YsqxAmt
     */	
	public java.math.BigDecimal getYsqxAmt() {
		return this.ysqxAmt;
	}
	
	/**
	 * @param ysyjfxAmt
	 */
	public void setYsyjfxAmt(java.math.BigDecimal ysyjfxAmt) {
		this.ysyjfxAmt = ysyjfxAmt;
	}
	
    /**
     * @return YsyjfxAmt
     */	
	public java.math.BigDecimal getYsyjfxAmt() {
		return this.ysyjfxAmt;
	}
	
	/**
	 * @param ysfxAmt
	 */
	public void setYsfxAmt(java.math.BigDecimal ysfxAmt) {
		this.ysfxAmt = ysfxAmt;
	}
	
    /**
     * @return YsfxAmt
     */	
	public java.math.BigDecimal getYsfxAmt() {
		return this.ysfxAmt;
	}
	
	/**
	 * @param yjfxAmt
	 */
	public void setYjfxAmt(java.math.BigDecimal yjfxAmt) {
		this.yjfxAmt = yjfxAmt;
	}
	
    /**
     * @return YjfxAmt
     */	
	public java.math.BigDecimal getYjfxAmt() {
		return this.yjfxAmt;
	}
	
	/**
	 * @param ysfjAmt
	 */
	public void setYsfjAmt(java.math.BigDecimal ysfjAmt) {
		this.ysfjAmt = ysfjAmt;
	}
	
    /**
     * @return YsfjAmt
     */	
	public java.math.BigDecimal getYsfjAmt() {
		return this.ysfjAmt;
	}
	
	/**
	 * @param yqbjAmt
	 */
	public void setYqbjAmt(java.math.BigDecimal yqbjAmt) {
		this.yqbjAmt = yqbjAmt;
	}
	
    /**
     * @return YqbjAmt
     */	
	public java.math.BigDecimal getYqbjAmt() {
		return this.yqbjAmt;
	}
	
	/**
	 * @param daizbjAmt
	 */
	public void setDaizbjAmt(java.math.BigDecimal daizbjAmt) {
		this.daizbjAmt = daizbjAmt;
	}
	
    /**
     * @return DaizbjAmt
     */	
	public java.math.BigDecimal getDaizbjAmt() {
		return this.daizbjAmt;
	}
	
	/**
	 * @param csyjlxAmt
	 */
	public void setCsyjlxAmt(java.math.BigDecimal csyjlxAmt) {
		this.csyjlxAmt = csyjlxAmt;
	}
	
    /**
     * @return CsyjlxAmt
     */	
	public java.math.BigDecimal getCsyjlxAmt() {
		return this.csyjlxAmt;
	}
	
	/**
	 * @param csqxAmt
	 */
	public void setCsqxAmt(java.math.BigDecimal csqxAmt) {
		this.csqxAmt = csqxAmt;
	}
	
    /**
     * @return CsqxAmt
     */	
	public java.math.BigDecimal getCsqxAmt() {
		return this.csqxAmt;
	}
	
	/**
	 * @param csyjfxAmt
	 */
	public void setCsyjfxAmt(java.math.BigDecimal csyjfxAmt) {
		this.csyjfxAmt = csyjfxAmt;
	}
	
    /**
     * @return CsyjfxAmt
     */	
	public java.math.BigDecimal getCsyjfxAmt() {
		return this.csyjfxAmt;
	}
	
	/**
	 * @param csfxAmt
	 */
	public void setCsfxAmt(java.math.BigDecimal csfxAmt) {
		this.csfxAmt = csfxAmt;
	}
	
    /**
     * @return CsfxAmt
     */	
	public java.math.BigDecimal getCsfxAmt() {
		return this.csfxAmt;
	}
	
	/**
	 * @param fxAmt
	 */
	public void setFxAmt(java.math.BigDecimal fxAmt) {
		this.fxAmt = fxAmt;
	}
	
    /**
     * @return FxAmt
     */	
	public java.math.BigDecimal getFxAmt() {
		return this.fxAmt;
	}
	
	/**
	 * @param totalBjAmt
	 */
	public void setTotalBjAmt(java.math.BigDecimal totalBjAmt) {
		this.totalBjAmt = totalBjAmt;
	}
	
    /**
     * @return TotalBjAmt
     */	
	public java.math.BigDecimal getTotalBjAmt() {
		return this.totalBjAmt;
	}
	
	/**
	 * @param totalLxAmt
	 */
	public void setTotalLxAmt(java.math.BigDecimal totalLxAmt) {
		this.totalLxAmt = totalLxAmt;
	}
	
    /**
     * @return TotalLxAmt
     */	
	public java.math.BigDecimal getTotalLxAmt() {
		return this.totalLxAmt;
	}
	
	/**
	 * @param totalFxAmt
	 */
	public void setTotalFxAmt(java.math.BigDecimal totalFxAmt) {
		this.totalFxAmt = totalFxAmt;
	}
	
    /**
     * @return TotalFxAmt
     */	
	public java.math.BigDecimal getTotalFxAmt() {
		return this.totalFxAmt;
	}
	
	/**
	 * @param totalFuxAmt
	 */
	public void setTotalFuxAmt(java.math.BigDecimal totalFuxAmt) {
		this.totalFuxAmt = totalFuxAmt;
	}
	
    /**
     * @return TotalFuxAmt
     */	
	public java.math.BigDecimal getTotalFuxAmt() {
		return this.totalFuxAmt;
	}
	
	/**
	 * @param totalHxbjAmt
	 */
	public void setTotalHxbjAmt(java.math.BigDecimal totalHxbjAmt) {
		this.totalHxbjAmt = totalHxbjAmt;
	}
	
    /**
     * @return TotalHxbjAmt
     */	
	public java.math.BigDecimal getTotalHxbjAmt() {
		return this.totalHxbjAmt;
	}
	
	/**
	 * @param totalHxlxAmt
	 */
	public void setTotalHxlxAmt(java.math.BigDecimal totalHxlxAmt) {
		this.totalHxlxAmt = totalHxlxAmt;
	}
	
    /**
     * @return TotalHxlxAmt
     */	
	public java.math.BigDecimal getTotalHxlxAmt() {
		return this.totalHxlxAmt;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return TotalTqlxAmt
     */	
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}