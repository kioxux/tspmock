/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest.bat;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.bat.BatBizRiskNotice;
import cn.com.yusys.yusp.service.bat.BatBizRiskNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizRiskNoticeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batbizrisknotice")
public class BatBizRiskNoticeResource {
    @Autowired
    private BatBizRiskNoticeService batBizRiskNoticeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatBizRiskNotice>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatBizRiskNotice> list = batBizRiskNoticeService.selectAll(queryModel);
        return new ResultDto<List<BatBizRiskNotice>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatBizRiskNotice>> index(QueryModel queryModel) {
        List<BatBizRiskNotice> list = batBizRiskNoticeService.selectByModel(queryModel);
        return new ResultDto<List<BatBizRiskNotice>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<BatBizRiskNotice> show(@PathVariable("serno") String serno) {
        BatBizRiskNotice batBizRiskNotice = batBizRiskNoticeService.selectByPrimaryKey(serno);
        return new ResultDto<BatBizRiskNotice>(batBizRiskNotice);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatBizRiskNotice> create(@RequestBody BatBizRiskNotice batBizRiskNotice) throws URISyntaxException {
        batBizRiskNoticeService.insert(batBizRiskNotice);
        return new ResultDto<BatBizRiskNotice>(batBizRiskNotice);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatBizRiskNotice batBizRiskNotice) throws URISyntaxException {
        int result = batBizRiskNoticeService.update(batBizRiskNotice);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = batBizRiskNoticeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batBizRiskNoticeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
