/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydHkDetails
 * @类描述: tmp_wyd_hk_details数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_hk_details")
public class TmpWydHkDetails {
	
	/** 交易流水号 **/
	@Id
	@Column(name = "REF_NO")
	private String refNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 合作机构号 **/
	@Column(name = "OPERORG", unique = false, nullable = true, length = 10)
	private String operorg;
	
	/** 借款合同号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 64)
	private String contractNo;
	
	/** 借据号 **/
	@Column(name = "LENDING_REF", unique = false, nullable = true, length = 64)
	private String lendingRef;
	
	/** 还本金额 **/
	@Column(name = "P_PAY", unique = false, nullable = true, length = 16)
	private String pPay;
	
	/** 还利息金额 **/
	@Column(name = "I_PAY", unique = false, nullable = true, length = 16)
	private String iPay;
	
	/** 还罚息 **/
	@Column(name = "PP_PAY", unique = false, nullable = true, length = 16)
	private String ppPay;
	
	/** 还费用 **/
	@Column(name = "FEE_REPAY", unique = false, nullable = true, length = 16)
	private String feeRepay;
	
	/** 还款类型 **/
	@Column(name = "TYPE", unique = false, nullable = true, length = 2)
	private String type;
	
	/** 保险代偿标志(y-是空为否) **/
	@Column(name = "INSURANCE_PAYMENT_FLAG", unique = false, nullable = true, length = 1)
	private String insurancePaymentFlag;
	
	/** 保险代偿日期 **/
	@Column(name = "INSURANCE_PAYMENT_DATE", unique = false, nullable = true, length = 10)
	private String insurancePaymentDate;
	
	/** 交易日期 **/
	@Column(name = "TRANS_DATE", unique = false, nullable = true, length = 8)
	private String transDate;
	
	/** 归属于资金端还本金额 **/
	@Column(name = "FUND_PRINCIPAL", unique = false, nullable = true, length = 16)
	private String fundPrincipal;
	
	/** 归属于资金端还利息金额 **/
	@Column(name = "FUND_INTEREST", unique = false, nullable = true, length = 16)
	private String fundInterest;
	
	/** 归属于资金端还罚息 **/
	@Column(name = "FUND_PENALTY", unique = false, nullable = true, length = 16)
	private String fundPenalty;
	
	/** 归属于资金端还费用 **/
	@Column(name = "FUND_FEE", unique = false, nullable = true, length = 16)
	private String fundFee;
	
	/** 归属于票据还本金额 **/
	@Column(name = "BILL_PRINCIPAL", unique = false, nullable = true, length = 16)
	private String billPrincipal;
	
	/** 归属于票据还利息金额 **/
	@Column(name = "BILL_INTEREST", unique = false, nullable = true, length = 16)
	private String billInterest;
	
	/** 归属于票据还罚息 **/
	@Column(name = "BILL_PENALTY", unique = false, nullable = true, length = 16)
	private String billPenalty;
	
	/** 归属于票据还费用 **/
	@Column(name = "BILL_FEE", unique = false, nullable = true, length = 16)
	private String billFee;
	
	/** 还表内利息 **/
	@Column(name = "I_PAY_BN", unique = false, nullable = true, length = 16)
	private String iPayBn;

	/** 还表内罚息 **/
	@Column(name = "F_PAY_BN", unique = false, nullable = true, length = 16)
	private String fPayBn;
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param operorg
	 */
	public void setOperorg(String operorg) {
		this.operorg = operorg;
	}
	
    /**
     * @return operorg
     */
	public String getOperorg() {
		return this.operorg;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param refNo
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
    /**
     * @return refNo
     */
	public String getRefNo() {
		return this.refNo;
	}
	
	/**
	 * @param pPay
	 */
	public void setPPay(String pPay) {
		this.pPay = pPay;
	}
	
    /**
     * @return pPay
     */
	public String getPPay() {
		return this.pPay;
	}
	
	/**
	 * @param iPay
	 */
	public void setIPay(String iPay) {
		this.iPay = iPay;
	}
	
    /**
     * @return iPay
     */
	public String getIPay() {
		return this.iPay;
	}
	
	/**
	 * @param ppPay
	 */
	public void setPpPay(String ppPay) {
		this.ppPay = ppPay;
	}
	
    /**
     * @return ppPay
     */
	public String getPpPay() {
		return this.ppPay;
	}
	
	/**
	 * @param feeRepay
	 */
	public void setFeeRepay(String feeRepay) {
		this.feeRepay = feeRepay;
	}
	
    /**
     * @return feeRepay
     */
	public String getFeeRepay() {
		return this.feeRepay;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param insurancePaymentFlag
	 */
	public void setInsurancePaymentFlag(String insurancePaymentFlag) {
		this.insurancePaymentFlag = insurancePaymentFlag;
	}
	
    /**
     * @return insurancePaymentFlag
     */
	public String getInsurancePaymentFlag() {
		return this.insurancePaymentFlag;
	}
	
	/**
	 * @param insurancePaymentDate
	 */
	public void setInsurancePaymentDate(String insurancePaymentDate) {
		this.insurancePaymentDate = insurancePaymentDate;
	}
	
    /**
     * @return insurancePaymentDate
     */
	public String getInsurancePaymentDate() {
		return this.insurancePaymentDate;
	}
	
	/**
	 * @param transDate
	 */
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	
    /**
     * @return transDate
     */
	public String getTransDate() {
		return this.transDate;
	}
	
	/**
	 * @param fundPrincipal
	 */
	public void setFundPrincipal(String fundPrincipal) {
		this.fundPrincipal = fundPrincipal;
	}
	
    /**
     * @return fundPrincipal
     */
	public String getFundPrincipal() {
		return this.fundPrincipal;
	}
	
	/**
	 * @param fundInterest
	 */
	public void setFundInterest(String fundInterest) {
		this.fundInterest = fundInterest;
	}
	
    /**
     * @return fundInterest
     */
	public String getFundInterest() {
		return this.fundInterest;
	}
	
	/**
	 * @param fundPenalty
	 */
	public void setFundPenalty(String fundPenalty) {
		this.fundPenalty = fundPenalty;
	}
	
    /**
     * @return fundPenalty
     */
	public String getFundPenalty() {
		return this.fundPenalty;
	}
	
	/**
	 * @param fundFee
	 */
	public void setFundFee(String fundFee) {
		this.fundFee = fundFee;
	}
	
    /**
     * @return fundFee
     */
	public String getFundFee() {
		return this.fundFee;
	}
	
	/**
	 * @param billPrincipal
	 */
	public void setBillPrincipal(String billPrincipal) {
		this.billPrincipal = billPrincipal;
	}
	
    /**
     * @return billPrincipal
     */
	public String getBillPrincipal() {
		return this.billPrincipal;
	}
	
	/**
	 * @param billInterest
	 */
	public void setBillInterest(String billInterest) {
		this.billInterest = billInterest;
	}
	
    /**
     * @return billInterest
     */
	public String getBillInterest() {
		return this.billInterest;
	}
	
	/**
	 * @param billPenalty
	 */
	public void setBillPenalty(String billPenalty) {
		this.billPenalty = billPenalty;
	}
	
    /**
     * @return billPenalty
     */
	public String getBillPenalty() {
		return this.billPenalty;
	}
	
	/**
	 * @param billFee
	 */
	public void setBillFee(String billFee) {
		this.billFee = billFee;
	}
	
    /**
     * @return billFee
     */
	public String getBillFee() {
		return this.billFee;
	}
	
	/**
	 * @param iPayBn
	 */
	public void setIPayBn(String iPayBn) {
		this.iPayBn = iPayBn;
	}
	
    /**
     * @return iPayBn
     */
	public String getIPayBn() {
		return this.iPayBn;
	}

	public String getfPayBn() {
		return fPayBn;
	}

	public void setfPayBn(String fPayBn) {
		this.fPayBn = fPayBn;
	}
}