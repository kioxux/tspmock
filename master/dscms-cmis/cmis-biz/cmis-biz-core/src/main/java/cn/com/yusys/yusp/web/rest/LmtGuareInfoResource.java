/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGuareCloestInfo;
import cn.com.yusys.yusp.domain.LmtGuareInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.service.LmtGuareInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-25 17:16:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "抵质押品信息")
@RequestMapping("/api/lmtguareinfo")
public class LmtGuareInfoResource {
    @Autowired
    private LmtGuareInfoService lmtGuareInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGuareInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGuareInfo> list = lmtGuareInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtGuareInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGuareInfo>> index(QueryModel queryModel) {
        List<LmtGuareInfo> list = lmtGuareInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtGuareInfo> show(@PathVariable("serno") String serno) {
        LmtGuareInfo lmtGuareInfo = lmtGuareInfoService.selectByPrimaryKey(serno);
        return new ResultDto<LmtGuareInfo>(lmtGuareInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGuareInfo> create(@RequestBody LmtGuareInfo lmtGuareInfo) throws URISyntaxException {
        lmtGuareInfoService.insert(lmtGuareInfo);
        return new ResultDto<LmtGuareInfo>(lmtGuareInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGuareInfo lmtGuareInfo) throws URISyntaxException {
        int result = lmtGuareInfoService.update(lmtGuareInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtGuareInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGuareInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
      * @函数名称:saveguareinfo
     * @参数与返回说明:
      * @函数描述: 保存
      * @算法描述:
      */
    @ApiOperation("保存信息")
    @PostMapping("/saveguareinfo")
    protected ResultDto<Integer> saveGuareInfo(@RequestBody LmtGuareInfo lmtGuareInfo) {
        int result = lmtGuareInfoService.saveGuareInfo(lmtGuareInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveguareinfo
     * @参数与返回说明:
     * @函数描述: 保存
     * @算法描述:
     */
    @ApiOperation("保存信息")
    @PostMapping("/saveguareinfogu")
    protected ResultDto<Integer> saveGuareInfogu(@RequestBody LmtGuareInfo lmtGuareInfo) {
        int result = lmtGuareInfoService.saveGuareInfogu(lmtGuareInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.LmtGuareInfo>
     * @author hubp
     * @date 2021/6/8 10:14
     * @version 1.0.0
     * @desc    POST 请求，通过主键PKID获取对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("POST请求，通过主键PKID获取对象")
    @PostMapping("/selectbypkid")
    protected ResultDto<LmtGuareInfo> selectByPkId(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtGuareInfo lmtGuareInfo = lmtGuareInfoService.selectByPrimaryKey(lmtSurveyReportDto.getPkId());
        return new ResultDto<LmtGuareInfo>(lmtGuareInfo);
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.LmtGuareInfo>
     * @author hubp
     * @date 2021/6/15 13:42
     * @version 1.0.0
     * @desc    POST 请求，通过主键PKID删除对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("POST请求，通过主键PKID删除对象")
    @PostMapping("/deletebypkid")
    protected ResultDto<Integer> deletebypkid(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        int result = lmtGuareInfoService.deleteByPrimaryKey(lmtSurveyReportDto.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.LmtGuareInfo>>
     * @author hubp
     * @date 2021/6/15 15:14
     * @version 1.0.0
     * @desc    post请求，条件查询列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("post请求，条件查询列表")
    @PostMapping("/selectbyserno")
    protected ResultDto<List<LmtGuareInfo>> selectBySerno(@RequestBody QueryModel queryModel) {
        List<LmtGuareInfo> list = lmtGuareInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareInfo>>(list);
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.LmtGuareCloestInfo>
     * @author hubp
     * @date 2021/6/15 19:45
     * @version 1.0.0
     * @desc 查找抵押物评估信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查找抵押物评估信息")
    @PostMapping("/selectguar")
    protected ResultDto<LmtGuareCloestInfo> selectGuar(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        return lmtGuareInfoService.selectGuar(lmtSurveyReportDto.getPkId());
    }
}
