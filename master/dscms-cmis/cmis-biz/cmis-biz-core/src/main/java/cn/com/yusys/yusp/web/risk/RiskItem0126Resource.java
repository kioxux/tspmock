package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0126Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@Api(tags = "riskItem0126大额风险暴露超限校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0126")
public class RiskItem0126Resource {

    @Autowired
    private RiskItem0126Service riskItem0126Service;

    @ApiOperation(value = "大额风险暴露超限校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0125(@RequestBody QueryModel queryModel) {
        if (queryModel.getCondition().size() == 0) {
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0126Service.riskItem0126(queryModel));
    }
}
