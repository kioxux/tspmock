/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtModelApprResultInfo;
import cn.com.yusys.yusp.service.LmtModelApprResultInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtModelApprResultInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-27 15:30:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "模型审批结果")
@RequestMapping("/api/lmtmodelapprresultinfo")
public class LmtModelApprResultInfoResource {
    @Autowired
    private LmtModelApprResultInfoService lmtModelApprResultInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtModelApprResultInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtModelApprResultInfo> list = lmtModelApprResultInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtModelApprResultInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtModelApprResultInfo>> index(QueryModel queryModel) {
        List<LmtModelApprResultInfo> list = lmtModelApprResultInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtModelApprResultInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/{surveySerno}")
    protected ResultDto<LmtModelApprResultInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtModelApprResultInfo>(lmtModelApprResultInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtModelApprResultInfo> create(@RequestBody LmtModelApprResultInfo lmtModelApprResultInfo) throws URISyntaxException {
        lmtModelApprResultInfoService.insert(lmtModelApprResultInfo);
        return new ResultDto<LmtModelApprResultInfo>(lmtModelApprResultInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtModelApprResultInfo lmtModelApprResultInfo) throws URISyntaxException {
        int result = lmtModelApprResultInfoService.update(lmtModelApprResultInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtModelApprResultInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("全表查询，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtModelApprResultInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtSurveyReportDto
     * @return ResultDto
     * @author wh
     * @date 2021/6/16 10:18
     * @version 1.0.0
     * @desc    POST查询单条数据
     * @修改历史: 修改时间: 2021年6月16日10:20:06    修改人员：hubp    修改原因
     */
    @ApiOperation("POST查询单条数据")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<LmtModelApprResultInfo> selectBySurveySerno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtModelApprResultInfo>(lmtModelApprResultInfo);
    }
}
