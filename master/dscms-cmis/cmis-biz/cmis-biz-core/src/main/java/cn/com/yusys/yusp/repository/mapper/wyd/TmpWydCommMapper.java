/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper.wyd;


import cn.com.yusys.yusp.domain.bat.BatApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.bat.BatApprStrMtableInfo;
import org.apache.ibatis.annotations.Param;



import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydAccountFlowMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 *
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface TmpWydCommMapper {
    /**
     * 往指定表指定字段插入结果集
     *
     */
    int batchInsert(@Param("tableName") String tableName,@Param("columns") String columns, @Param("valueList")List<String> valueList);
    /**
     * 往指定表写入公共8个字段
     */
    int batchUpdate(@Param("tableName") String tableName,@Param("inputId") String inputId,@Param("inputBrId") String inputBrId,
                    @Param("inputDate")String inputDate,@Param("updId") String updId,@Param("updBrId") String updBrId,
                    @Param("updDate")String updDate,@Param("createTime") String createTime,@Param("updateTime") String updateTime);
    /**
     * 清空指定表中的数据
     */
    void deleteAll(@Param("tableName") String tableName);
    //List<String> getColumnsByTableName(@Param("tableName") String tableName);
    /**
     * 根据主键匹配，将同时存在A,B表中的数据从A表中删除
     */
    void deleleExistsData(@Param("tableA")String tableA,@Param("tableB")String tableB,@Param("indexCondition")String indexCondition );
    /**
     * 将B表中所有数据插入到A表中
     */
    void insertIntoDataFromB2A(@Param("tableA")String tableA,@Param("tableB")String tableB,@Param("columns")String columns);

    /**
     * 获取微业贷风险分类结果
     * @return 风险分类结果集
     */
    List<Map> getWydRiskClassResult();

    /**
     * 获取未逾期天数
     * @return
     */
    List<Map> getWydNotOverdueDays();

    /**
     * 新增批复主信息
     * @param batApprStrMtableInfo
     * @return
     */
    int insertBatApprStrMtableInfo(BatApprStrMtableInfo batApprStrMtableInfo);

    /**
     * 新增批复额度分项基础信息
     * @param batApprLmtSubBasicInfo
     * @return
     */
    int insertBatApprLmtSubBasicInfo(BatApprLmtSubBasicInfo batApprLmtSubBasicInfo);

    /**
     * 根据主键查询批复额度
     * @param apprSerno
     * @return
     */
    BatApprStrMtableInfo getBatApprStrMtableByAppSerno(@Param("apprSerno") String apprSerno);

    /**
     * 根据分项品种编号查询批复额度分项
     * @param apprSerno
     * @return
     */
    BatApprLmtSubBasicInfo getBatApprLmtSubBasicInfoByApprSubSerno(@Param("apprSubSerno") String apprSerno);

    /**
     *根据分项编号查询批复额度分项
     * @param apprSerno
     * @return
     */
    BatApprLmtSubBasicInfo getBatApprLmtSubBasicInfoBySerno(@Param("serno") String apprSerno);

    /**
     * 根据客户查询微业贷额度产品分项
     * @param param
     * @return
     */
    BatApprLmtSubBasicInfo getWydApprLmtSubBasicInfoByCus(Map param);

    /**
     * 根据主键更新批复主信息-只更新非空字段
     * @param batApprStrMtableInfo
     * @return
     */
    int updateBatApprStrMtableInfoByPrimaryKeySelective(BatApprStrMtableInfo batApprStrMtableInfo);

    /**
     * 根据主键更新批复额度分项-只更新非空字段
     * @param batApprLmtSubBasicInfo
     * @return
     */
    int updateBatApprLmtSubBasicInfoByPrimaryKeySelective(BatApprLmtSubBasicInfo batApprLmtSubBasicInfo);

    /**
     * 根据交易日期查询不合法微业贷借据数量
     * @param bizDate 交易日期
     * @return
     */
    int getWydAccLoanByBizDate(@Param("bizDate") String bizDate);

}