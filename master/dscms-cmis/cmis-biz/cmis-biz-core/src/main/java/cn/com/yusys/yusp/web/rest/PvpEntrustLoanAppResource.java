/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.*;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.PvpEntrustLoanAppService;
import com.github.pagehelper.Page;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpEntrustLoanAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-19 16:56:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "委托贷款出账申请")
@RequestMapping("/api/pvpentrustloanapp")
public class PvpEntrustLoanAppResource {
    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpEntrustLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpEntrustLoanApp> list = pvpEntrustLoanAppService.selectAll(queryModel);
        return new ResultDto<List<PvpEntrustLoanApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpEntrustLoanApp>> index(QueryModel queryModel) {
        List<PvpEntrustLoanApp> list = pvpEntrustLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<PvpEntrustLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<PvpEntrustLoanApp> show(@PathVariable("serno") String serno) {
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByPrimaryKey(serno);
        return new ResultDto<PvpEntrustLoanApp>(pvpEntrustLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpEntrustLoanApp> create(@RequestBody PvpEntrustLoanApp pvpEntrustLoanApp) throws URISyntaxException {
        pvpEntrustLoanAppService.insert(pvpEntrustLoanApp);
        return new ResultDto<PvpEntrustLoanApp>(pvpEntrustLoanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpEntrustLoanApp pvpEntrustLoanApp) throws URISyntaxException {
        int result = pvpEntrustLoanAppService.update(pvpEntrustLoanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = pvpEntrustLoanAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpEntrustLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请保存新增操作")
    @PostMapping("/saveentrustloanapp")
    protected ResultDto<Map> saveEntrustLoanApp(@RequestBody PvpEntrustLoanApp pvpEntrustLoanApp) throws URISyntaxException {
        ResultDto<PvpLoanAppSegInterstSub> resultDto = new ResultDto<PvpLoanAppSegInterstSub>();
        Map result = pvpEntrustLoanAppService.savepvpEntrustLoanApp(pvpEntrustLoanApp);
        return  new ResultDto<>(result);
    }

    /**
     * 通用的保存方法
     * @param pvpEntrustLoanApp
     * @return
     */
    @ApiOperation("委托贷款出账通用的保存方法")
    @PostMapping("/commonupdateentrustloanapp")
    public ResultDto<Map> commonUpdateEntrustLoanApp(@RequestBody PvpEntrustLoanApp pvpEntrustLoanApp){
        Map rtnData = pvpEntrustLoanAppService.commonUpdatePvpEntrustLoanApp(pvpEntrustLoanApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpEntrustLoanApp temp = new PvpEntrustLoanApp();
        PvpEntrustLoanApp studyDemo = pvpEntrustLoanAppService.selectByEntrustSernoKey((String)map.get("pvpSerno"));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tosignlist")
    protected ResultDto<List<PvpEntrustLoanApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpEntrustLoanApp> list = pvpEntrustLoanAppService.toSignlist(queryModel);
        ResultDto<List<PvpEntrustLoanApp>> resultDto = new ResultDto<List<PvpEntrustLoanApp>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
//        if (list instanceof Page) {
//            Page<?> studyDemoPage = (Page<?>) list;
//            total = studyDemoPage.getTotal();
//        }
        return new ResultDto<List<PvpEntrustLoanApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/donesignlist")
    protected ResultDto<List<PvpEntrustLoanApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpEntrustLoanApp> list = pvpEntrustLoanAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpEntrustLoanApp>>(list);
    }

    /**
     * @创建人 ZXZ
     * @创建时间 2021-04-24 15:04
     * @注释 删除/作废放款申请单 申请状态 ->998
     */
    @PostMapping("/invalid")
    protected ResultDto<Map> invalidEntrustloanapp(@RequestBody Map map) {
        Map rtnData =  pvpEntrustLoanAppService.invalidentrustloanapp((String)map.get("pvpSerno"));
        return new ResultDto<>(rtnData);
    }
    /**
     * @创建人 ZXZ
     * @创建时间 2021-04-24 15:29
     * @注释 借据 打印接口
     */
    @PostMapping("/entrustprint")
    protected ResultDto<Map> pvploanprint(@RequestBody String pvpSerno) {
        Map rtnData =  pvpEntrustLoanAppService.entrustprint(pvpSerno);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称：sendpvpauth
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendpvpauth")
    protected  ResultDto<Object> sendPvpAuth(@RequestBody Map map) {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        pvpEntrustLoanAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }
}
