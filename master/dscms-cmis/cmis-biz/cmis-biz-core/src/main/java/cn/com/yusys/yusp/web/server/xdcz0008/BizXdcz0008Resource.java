package cn.com.yusys.yusp.web.server.xdcz0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0008.req.Xdcz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0008.resp.Xdcz0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:出账记录详情查看
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0008:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0008.BizXdcz0008Resource.class);
    /**
     * 交易码：xdcz0008
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0008DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0008")
    protected @ResponseBody
    ResultDto<Xdcz0008DataRespDto>  xdcz0008(@Validated @RequestBody Xdcz0008DataReqDto xdcz0008DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008DataReqDto));
        Xdcz0008DataRespDto  xdcz0008DataRespDto  = new Xdcz0008DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0008DataRespDto>xdcz0008DataResultDto = new ResultDto<>();

        BigDecimal guarantAmt = xdcz0008DataReqDto.getGuarantAmt();//保函金额
        String startDate = xdcz0008DataReqDto.getStartDate();//生效日期
        String endDate = xdcz0008DataReqDto.getEndDate();//到期日
        String huser = xdcz0008DataReqDto.getHuser();//经办人
        String openGuarantBankNo = xdcz0008DataReqDto.getOpenGuarantBankNo();//开立保函行号
        String openGuarantBankName = xdcz0008DataReqDto.getOpenGuarantBankName();//开立保函行名称
        String cusId = xdcz0008DataReqDto.getCusId();//客户号
        String cusName = xdcz0008DataReqDto.getCusName();//客户中文名称
        String serno = xdcz0008DataReqDto.getSerno();//交易流水号
        String guarantNo = xdcz0008DataReqDto.getGuarantNo();//保函号码
        String oprType = xdcz0008DataReqDto.getOprType();//操作类型
        String limitType = xdcz0008DataReqDto.getLimitType();//同业间额度类型
        try {
            // 从xdcz0008DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0008DataRespDto对象开始
            Xdcz0008DataRespDto xdcz0008RespDto = new Xdcz0008DataRespDto(); //响应Data:代开保函
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdcz0008DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xdcz0008DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdcz0008DataRespDto对象结束
            // 封装xdcz0008DataResultDto中正确的返回码和返回信息
            xdcz0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value,e.getMessage());
            // 封装xdcz0008DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0008DataRespDto到xdcz0008DataResultDto中
        xdcz0008DataResultDto.setData(xdcz0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008DataRespDto));
        return xdcz0008DataResultDto;
    }
}
