/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;



/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydCusManager
 * @类描述: tmp_wyd_cus_manager数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_cus_manager_tmp")
public class TmpWydCusManagerTmp {
	
	/** 对公客户编号 **/
	@Column(name = "CUSTOMERID")
	private String customerid;
	
	/** 高管类型 **/
	@Column(name = "POSITIONS")
	private String positions;
	
	/** 证件号码 **/
	@Column(name = "CERTID")
	private String certid;
	
	/** 高管名称 **/
	@Column(name = "MANAGERNAME", unique = false, nullable = true, length = 200)
	private String managername;
	
	/** 证件类型 **/
	@Column(name = "CERTTYPE", unique = false, nullable = true, length = 3)
	private String certtype;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 2)
	private String sex;
	
	/** 出生日期 **/
	@Column(name = "BIRTHDAY", unique = false, nullable = true, length = 50)
	private String birthday;
	
	/** 证件签发日期 **/
	@Column(name = "CERTBEGINDATE", unique = false, nullable = true, length = 10)
	private String certbegindate;
	
	/** 证件到期日期 **/
	@Column(name = "CERTENDDATE", unique = false, nullable = true, length = 10)
	private String certenddate;
	
	/** 学历 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 2)
	private String duty;
	
	/** 联系电话 **/
	@Column(name = "TELEPHONE", unique = false, nullable = true, length = 20)
	private String telephone;
	
	/** 个人征信记录 **/
	@Column(name = "PCREDIT", unique = false, nullable = true, length = 20)
	private String pcredit;
	
	/** 更新日期 **/
	@Column(name = "UPDATEDATE", unique = false, nullable = true, length = 10)
	private String updatedate;
	
	
	/**
	 * @param customerid
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
    /**
     * @return customerid
     */
	public String getCustomerid() {
		return this.customerid;
	}
	
	/**
	 * @param positions
	 */
	public void setPositions(String positions) {
		this.positions = positions;
	}
	
    /**
     * @return positions
     */
	public String getPositions() {
		return this.positions;
	}
	
	/**
	 * @param managername
	 */
	public void setManagername(String managername) {
		this.managername = managername;
	}
	
    /**
     * @return managername
     */
	public String getManagername() {
		return this.managername;
	}
	
	/**
	 * @param certtype
	 */
	public void setCerttype(String certtype) {
		this.certtype = certtype;
	}
	
    /**
     * @return certtype
     */
	public String getCerttype() {
		return this.certtype;
	}
	
	/**
	 * @param certid
	 */
	public void setCertid(String certid) {
		this.certid = certid;
	}
	
    /**
     * @return certid
     */
	public String getCertid() {
		return this.certid;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param certbegindate
	 */
	public void setCertbegindate(String certbegindate) {
		this.certbegindate = certbegindate;
	}
	
    /**
     * @return certbegindate
     */
	public String getCertbegindate() {
		return this.certbegindate;
	}
	
	/**
	 * @param certenddate
	 */
	public void setCertenddate(String certenddate) {
		this.certenddate = certenddate;
	}
	
    /**
     * @return certenddate
     */
	public String getCertenddate() {
		return this.certenddate;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
    /**
     * @return telephone
     */
	public String getTelephone() {
		return this.telephone;
	}
	
	/**
	 * @param pcredit
	 */
	public void setPcredit(String pcredit) {
		this.pcredit = pcredit;
	}
	
    /**
     * @return pcredit
     */
	public String getPcredit() {
		return this.pcredit;
	}
	
	/**
	 * @param updatedate
	 */
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
    /**
     * @return updatedate
     */
	public String getUpdatedate() {
		return this.updatedate;
	}


}