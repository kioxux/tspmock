package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRefineDetail
 * @类描述: lmt_refine_detail数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-11 11:24:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtRefineDetailQueryVo extends PageQuery{
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String lmtSerno;
	
	/** 集团细化申请概述 **/
	private String grpRefineAppMemo;
	
	/** 原授信情况 **/
	private String origiLmtSurvey;
	
	/** 本次授信申请细化内容 **/
	private String lmtRefineContent;
	
	/** 授信细化理由 **/
	private String lmtRefineResn;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param grpRefineAppMemo
	 */
	public void setGrpRefineAppMemo(String grpRefineAppMemo) {
		this.grpRefineAppMemo = grpRefineAppMemo == null ? null : grpRefineAppMemo.trim();
	}
	
    /**
     * @return GrpRefineAppMemo
     */	
	public String getGrpRefineAppMemo() {
		return this.grpRefineAppMemo;
	}
	
	/**
	 * @param origiLmtSurvey
	 */
	public void setOrigiLmtSurvey(String origiLmtSurvey) {
		this.origiLmtSurvey = origiLmtSurvey == null ? null : origiLmtSurvey.trim();
	}
	
    /**
     * @return OrigiLmtSurvey
     */	
	public String getOrigiLmtSurvey() {
		return this.origiLmtSurvey;
	}
	
	/**
	 * @param lmtRefineContent
	 */
	public void setLmtRefineContent(String lmtRefineContent) {
		this.lmtRefineContent = lmtRefineContent == null ? null : lmtRefineContent.trim();
	}
	
    /**
     * @return LmtRefineContent
     */	
	public String getLmtRefineContent() {
		return this.lmtRefineContent;
	}
	
	/**
	 * @param lmtRefineResn
	 */
	public void setLmtRefineResn(String lmtRefineResn) {
		this.lmtRefineResn = lmtRefineResn == null ? null : lmtRefineResn.trim();
	}
	
    /**
     * @return LmtRefineResn
     */	
	public String getLmtRefineResn() {
		return this.lmtRefineResn;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}