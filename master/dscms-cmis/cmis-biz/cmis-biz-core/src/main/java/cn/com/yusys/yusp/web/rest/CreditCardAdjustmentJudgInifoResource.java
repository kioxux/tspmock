/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentJudgInifo;
import cn.com.yusys.yusp.service.CreditCardAdjustmentJudgInifoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAdjustmentJudgInifoResource
 * @类描述: #额度申请审批模块
 * @功能描述: 
 * @创建人: ZSM
 * @创建时间: 2021-05-25 10:00:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditcardadjustmentjudginifo")
public class CreditCardAdjustmentJudgInifoResource {
    @Autowired
    private CreditCardAdjustmentJudgInifoService creditCardAdjustmentJudgInifoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardAdjustmentJudgInifo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardAdjustmentJudgInifo> list = creditCardAdjustmentJudgInifoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardAdjustmentJudgInifo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditCardAdjustmentJudgInifo>> index(QueryModel queryModel) {
        List<CreditCardAdjustmentJudgInifo> list = creditCardAdjustmentJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAdjustmentJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardAdjustmentJudgInifo> show(@PathVariable("pkId") String pkId) {
        CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardAdjustmentJudgInifo>(creditCardAdjustmentJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增额度申请审批")
    @PostMapping("/")
    protected ResultDto<CreditCardAdjustmentJudgInifo> create(@RequestBody CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo) throws URISyntaxException {
        creditCardAdjustmentJudgInifoService.insert(creditCardAdjustmentJudgInifo);
        return new ResultDto<CreditCardAdjustmentJudgInifo>(creditCardAdjustmentJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的新增额度申请审批")
    @PostMapping("/save")
    protected ResultDto<CreditCardAdjustmentJudgInifo> save(@RequestBody CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo) throws URISyntaxException {
        creditCardAdjustmentJudgInifoService.save(creditCardAdjustmentJudgInifo);
        return new ResultDto<CreditCardAdjustmentJudgInifo>(creditCardAdjustmentJudgInifo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("更新额度申请审批")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo) throws URISyntaxException {
        int result = creditCardAdjustmentJudgInifoService.update(creditCardAdjustmentJudgInifo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardAdjustmentJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cut
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/cut")
    protected ResultDto<Integer> cut(@RequestBody String pkId) {
        int result = creditCardAdjustmentJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardAdjustmentJudgInifoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询额度申请审批")
    @PostMapping("/querymodel")
    protected ResultDto<List<CreditCardAdjustmentJudgInifo>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditCardAdjustmentJudgInifo> list = creditCardAdjustmentJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAdjustmentJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键查询额度申请审批")
    @PostMapping("/{pkId}")
    protected ResultDto<CreditCardAdjustmentJudgInifo> showPost(@RequestBody CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifos) {
        CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectByPrimaryKey(creditCardAdjustmentJudgInifos.getPkId());
        return new ResultDto<CreditCardAdjustmentJudgInifo>(creditCardAdjustmentJudgInifo);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水查询额度申请审批")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardAdjustmentJudgInifo> selectbyserno(@RequestBody CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifos) {
        CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectBySerno(creditCardAdjustmentJudgInifos.getSerno());
        return new ResultDto<CreditCardAdjustmentJudgInifo>(creditCardAdjustmentJudgInifo);
    }
}
