/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrContExt;
import cn.com.yusys.yusp.service.CtrContExtService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContExtResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 20:03:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrcontext")
public class CtrContExtResource {
    @Autowired
    private CtrContExtService ctrContExtService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrContExt>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrContExt> list = ctrContExtService.selectAll(queryModel);
        return new ResultDto<List<CtrContExt>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrContExt>> index(QueryModel queryModel) {
        List<CtrContExt> list = ctrContExtService.selectByModel(queryModel);
        return new ResultDto<List<CtrContExt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{extCtrNo}")
    protected ResultDto<CtrContExt> show(@PathVariable("extCtrNo") String extCtrNo) {
        CtrContExt ctrContExt = ctrContExtService.selectByPrimaryKey(extCtrNo);
        return new ResultDto<CtrContExt>(ctrContExt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrContExt> create(@RequestBody CtrContExt ctrContExt) throws URISyntaxException {
        ctrContExtService.insert(ctrContExt);
        return new ResultDto<CtrContExt>(ctrContExt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期协议修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrContExt ctrContExt) throws URISyntaxException {
        int result = ctrContExtService.update(ctrContExt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{extCtrNo}")
    protected ResultDto<Integer> delete(@PathVariable("extCtrNo") String extCtrNo) {
        int result = ctrContExtService.deleteByPrimaryKey(extCtrNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrContExtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期协议列表查询")
    @PostMapping("/queryCtrContExt")
    protected ResultDto<List<CtrContExt>> queryCtrContExt(@RequestBody QueryModel queryModel) {
        List<CtrContExt> list = ctrContExtService.selectByModel(queryModel);
        return new ResultDto<List<CtrContExt>>(list);
    }

    /**
     * @函数名称:queryCtrContExtInfo
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "展期协议信息查询")
    @PostMapping("/queryCtrContExtInfo")
    protected ResultDto<CtrContExt> queryCtrContExtInfo(@RequestBody CtrContExt ctrContExt) {
        CtrContExt ctrContExts = ctrContExtService.selectByPrimaryKey(ctrContExt.getExtCtrNo());
        return new ResultDto<CtrContExt>(ctrContExts);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期协议保存")
    @PostMapping("/save")
    protected ResultDto<CtrContExt> save(@RequestBody CtrContExt ctrContExt) throws URISyntaxException {
        ctrContExtService.insert(ctrContExt);
        return new ResultDto<CtrContExt>(ctrContExt);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期协议删除")
    @PostMapping("/deleteByExtCtrNo")
    protected ResultDto<Integer> deleteByExtCtrNo(@RequestBody CtrContExt ctrContExt) {
        int result = ctrContExtService.deleteByPrimaryKey(ctrContExt.getExtCtrNo());
        return new ResultDto<Integer>(result);
    }
}
