/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.CfgDocParamsDetail;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsDetailMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 19:25:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgDocParamsDetailMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CfgDocParamsDetail selectByPrimaryKey(@Param("cdpdSerno") String cdpdSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CfgDocParamsDetail> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CfgDocParamsDetail record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CfgDocParamsDetail record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CfgDocParamsDetail record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CfgDocParamsDetail record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("cdpdSerno") String cdpdSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: selectByCdplSerno
     * @方法描述: 根据参数配置流水号查询清单信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    CfgDocParamsDetail selectByCdplSerno(String cdplSerno);

    /**
     * @函数名称:deleteByCdplSerno
     * @函数描述:根据配置流水号删除档案配置信息
     * @参数与返回说明:
     * @算法描述:
     */
    int deleteByCdplSerno(String cdplSerno);

    /**
     * @函数名称:selectCountsByDocTypeData
     * @函数描述:查询同一材料类型条数
     * @参数与返回说明:
     * @算法描述:
     */
    int selectCountsByDocTypeData(QueryModel queryModel);
}