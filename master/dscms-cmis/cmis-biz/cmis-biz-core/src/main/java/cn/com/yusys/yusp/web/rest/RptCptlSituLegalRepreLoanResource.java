/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCptlSituLegalRepreLoan;
import cn.com.yusys.yusp.service.RptCptlSituLegalRepreLoanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituLegalRepreLoanResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:49:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcptlsitulegalrepreloan")
public class RptCptlSituLegalRepreLoanResource {
    @Autowired
    private RptCptlSituLegalRepreLoanService rptCptlSituLegalRepreLoanService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCptlSituLegalRepreLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCptlSituLegalRepreLoan> list = rptCptlSituLegalRepreLoanService.selectAll(queryModel);
        return new ResultDto<List<RptCptlSituLegalRepreLoan>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCptlSituLegalRepreLoan>> index(QueryModel queryModel) {
        List<RptCptlSituLegalRepreLoan> list = rptCptlSituLegalRepreLoanService.selectByModel(queryModel);
        return new ResultDto<List<RptCptlSituLegalRepreLoan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptCptlSituLegalRepreLoan> show(@PathVariable("pkId") String pkId) {
        RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan = rptCptlSituLegalRepreLoanService.selectByPrimaryKey(pkId);
        return new ResultDto<RptCptlSituLegalRepreLoan>(rptCptlSituLegalRepreLoan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCptlSituLegalRepreLoan> create(@RequestBody RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan) throws URISyntaxException {
        rptCptlSituLegalRepreLoanService.insert(rptCptlSituLegalRepreLoan);
        return new ResultDto<RptCptlSituLegalRepreLoan>(rptCptlSituLegalRepreLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan) throws URISyntaxException {
        int result = rptCptlSituLegalRepreLoanService.update(rptCptlSituLegalRepreLoan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptCptlSituLegalRepreLoanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptCptlSituLegalRepreLoanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptCptlSituLegalRepreLoan>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptCptlSituLegalRepreLoan>>(rptCptlSituLegalRepreLoanService.selectByModel(model));
    }

    @PostMapping("/updateRepreLoan")
    protected ResultDto<Integer> updateRepreLoan(@RequestBody RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan){
        return new ResultDto<Integer>(rptCptlSituLegalRepreLoanService.updateSelective(rptCptlSituLegalRepreLoan));
    }

    @PostMapping("/insertRepreLoan")
    protected ResultDto<Integer> insertRepreLoan(@RequestBody RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan){
        rptCptlSituLegalRepreLoan.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptCptlSituLegalRepreLoanService.insertSelective(rptCptlSituLegalRepreLoan));
    }

    @PostMapping("/deleteRepreLoan")
    protected ResultDto<Integer> deleteRepreLoan(@RequestBody RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan){
        String pkId = rptCptlSituLegalRepreLoan.getPkId();
        return new ResultDto<Integer>(rptCptlSituLegalRepreLoanService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/selectGrpRepreLoan")
    protected ResultDto<List<Map<String,Object>>> selectGrpRepreLoan(@RequestBody QueryModel model){
        return new ResultDto<List<Map<String, Object>>>(rptCptlSituLegalRepreLoanService.selectGrpRepreLoan(model));
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptCptlSituLegalRepreLoan>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptCptlSituLegalRepreLoan>>(rptCptlSituLegalRepreLoanService.selectBySerno(serno));
    }
}
