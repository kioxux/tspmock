package cn.com.yusys.yusp.service.server.xddb0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0002.req.Xddb0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0002.resp.Xddb0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddb0002Service
 * @类描述: #服务类
 * @功能描述:查询押品是否已入库 1、根据押品编号查询是否存在此押品
 * 2、查询押品是否为已入库状态，grt_gp_inout_info，10006，10011，10010
 * @创建人: xull2
 * @创建时间: 2021-04-25 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0002Service.class);
    @Resource
    private GuarWarrantInfoService guarWarrantInfoService;

    /**
     * 查询押品出入库状态
     *
     * @param xddb0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0002DataRespDto selectStateByguarNo(Xddb0002DataReqDto xddb0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value);
        Xddb0002DataRespDto xddb0002DataRespDto = new Xddb0002DataRespDto();// 响应Dto:押品状态查询
        try {
            String coreGrtNo = xddb0002DataReqDto.getGuarNo();//押品编号
            //查询是否存在指定的编号的押品信息
            int count = guarWarrantInfoService.selectIsExistByCoreGuarantyNo(coreGrtNo);
            if (count > 0) {
                int result = guarWarrantInfoService.countWarrantInStateByCoreGuarantyNo(coreGrtNo);
                if (result > 0) {// 存在押品状态未入账的押品
                    xddb0002DataRespDto.setStatus(DscmsBizDbEnum.YESNO_YES.key);
                } else {// 不存在押品状态未入账的押品
                    xddb0002DataRespDto.setStatus(DscmsBizDbEnum.YESNO_NO.key);
                }
            } else if (coreGrtNo.startsWith("YP")) {// 不存在押品状态未入账的押品
                xddb0002DataRespDto.setStatus(DscmsBizDbEnum.YESNO_NO.key);
                throw new Exception("未查到数据！");
            } else {// 存在押品状态未入账的押品
                xddb0002DataRespDto.setStatus(DscmsBizDbEnum.YESNO_NO.key);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        //返回查询结果
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value);
        return xddb0002DataRespDto;
    }
}
