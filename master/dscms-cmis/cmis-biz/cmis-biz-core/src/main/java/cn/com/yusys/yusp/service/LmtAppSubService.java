package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.repository.mapper.LmtAppSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.util.MapUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-07 19:49:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppSubService {

    private static final Logger log = LoggerFactory.getLogger(LmtAppSubService.class);

    @Resource
    private LmtAppSubMapper lmtAppSubMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;// 额度系统

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtAppSub selectByPrimaryKey(String pkId) {
        return lmtAppSubMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtAppSub> selectAll(QueryModel model) {
        List<LmtAppSub> records = (List<LmtAppSub>) lmtAppSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtAppSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppSub> list = lmtAppSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtAppSub record) {
        return lmtAppSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtAppSub record) {
        return lmtAppSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtAppSub record) {
        return lmtAppSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtAppSub record) {
        return lmtAppSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteAppSub
     * @方法描述: 根据实体类逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map deleteAppSub(LmtAppSub record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            // TODO 待补充   删除分项  更新申请数据

            String parentSerno = record.getSerno();
            if (StringUtils.isEmpty(parentSerno)) {
                rtnCode = EpbEnum.EPB099999.key;
                rtnMsg = EpbEnum.EPB099999.value;
                return rtnData;
            }
            BigDecimal lmtAmt = record.getLmtAmt();
            String guarMode = record.getGuarMode();
            LmtApp lmtApp = lmtAppService.selectBySerno(parentSerno);
            //获取对应授信申请信息的原敞口金额以及低风险额度
            BigDecimal originOpenAmt = new BigDecimal(0);
            BigDecimal originLowAmt = new BigDecimal(0);
            BigDecimal openAmt = new BigDecimal(0);
            BigDecimal lowAmt = new BigDecimal(0);
            List<LmtAppSub> appSubList = this.queryLmtAppSubBySerno(lmtApp.getSerno());
            for(LmtAppSub lmtAppSubItem: appSubList){
                if(lmtAppSubItem.getLmtAmt() != null){
                    if ("60".equals(lmtAppSubItem.getGuarMode())) {
                        lowAmt = lowAmt.add(lmtAppSubItem.getLmtAmt());
                    } else {
                        openAmt = openAmt.add(lmtAppSubItem.getLmtAmt());
                    }
                }
            }
            originOpenAmt = openAmt;
            originLowAmt = lowAmt;
            if ("60".equals(guarMode)) {
                lmtApp.setOpenTotalLmtAmt(originOpenAmt);
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt.subtract(lmtAmt));
            } else {
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt);
                lmtApp.setOpenTotalLmtAmt(originOpenAmt.subtract(lmtAmt));
            }

            int count = lmtAppSubMapper.updateByPkId(record);
            int countApp = lmtAppService.update(lmtApp);
            if (count != 1 | countApp != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除授信明细情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    ;

    /**
     * @方法名称: saveLmtApp
     * @方法描述: 根据前台传入表单数据保存授信分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map saveLmtAppSub(LmtAppSub lmtAppSub) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String lmtType = "";
        try {

            // TODO 保存前校验 待补充
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("保存授信申请数据,生成流水号%s", serno));

            // 申请流水号
            lmtAppSub.setSubSerno(serno);
            // 数据操作标志为新增
            lmtAppSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 币种
            lmtAppSub.setCurType(CmisCommonConstants.CUR_TYPE_CNY);

            log.info(String.format("保存授信分项数据%s-获取当前登录用户数据", serno));
            User userInfo = SessionUtils.getUserInformation();

            String parentSerno = lmtAppSub.getSerno();

            if (StringUtils.isEmpty(parentSerno)) {
                rtnCode = EpbEnum.EPB099999.key;
                rtnMsg = EpbEnum.EPB099999.value;
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            LmtApp lmtApp = lmtAppService.selectBySerno(parentSerno);

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                lmtAppSub.setPkId(UUID.randomUUID().toString());
                lmtAppSub.setLmtTerm(lmtApp.getLmtTerm());
                lmtAppSub.setInputId(userInfo.getLoginCode());
                lmtAppSub.setInputBrId(userInfo.getOrg().getCode());
                lmtAppSub.setInputDate(openday);
                lmtAppSub.setUpdId(userInfo.getLoginCode());
                lmtAppSub.setUpdBrId(userInfo.getOrg().getCode());
                lmtAppSub.setUpdDate(openday);
                lmtAppSub.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                lmtAppSub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
            }
            String guarMode = lmtAppSub.getGuarMode();
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            lmtType = lmtApp.getLmtType();
            //获取对应授信申请信息的原敞口金额以及低风险额度
            BigDecimal openAmt = new BigDecimal(0);
            BigDecimal lowAmt = new BigDecimal(0);
            List<LmtAppSub> appSubList = this.queryLmtAppSubBySerno(lmtApp.getSerno());
            for(LmtAppSub lmtAppSubItem: appSubList){
                if(lmtAppSubItem.getLmtAmt() != null){
                    if ("60".equals(lmtAppSubItem.getGuarMode())) {
                        lowAmt = lowAmt.add(lmtAppSubItem.getLmtAmt());
                    } else {
                        openAmt = openAmt.add(lmtAppSubItem.getLmtAmt());
                    }
                }
            }
            if ("60".equals(guarMode)) {
                Map map = new HashMap();
                map.put("serno", parentSerno);
                map.put("oprType", CmisCommonConstants.ADD_OPR);
                map.put("guarMode", "60");
                List<LmtAppSub> list = this.selectByParams(map);
                if (list != null && list.size() > 0) {
                    rtnCode = EcbEnum.ECB010025.key;
                    rtnMsg = EcbEnum.ECB010025.value;
                    return rtnData;
                }
                lowAmt = lowAmt.add(lmtAmt);
            } else {
                openAmt = openAmt.add(lmtAmt);
                if (CmisCommonConstants.LMT_TYPE_02.equals(lmtType) || CmisCommonConstants.LMT_TYPE_04.equals(lmtType) || CmisCommonConstants.LMT_TYPE_06.equals(lmtType)) {
                    if(lmtApp.getOrigiOpenTotalLmtAmt() != null){
                        if (lmtApp.getOrigiOpenTotalLmtAmt().compareTo(openAmt) < 0) {
                            rtnCode = EcbEnum.ECB010042.key;
                            rtnMsg = EcbEnum.ECB010042.value;
                            return rtnData;
                        }
                    }
                }
            }
            lmtApp.setLowRiskTotalLmtAmt(lowAmt);
            lmtApp.setOpenTotalLmtAmt(openAmt);
            int count = lmtAppSubMapper.insert(lmtAppSub);
            int countApp = lmtAppService.update(lmtApp);
            log.info(String.format("更新授信分项数据,流水号%s", serno));
            if (count != 1 || countApp != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                // throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",保存失败！");
            }
            // 如果是集团的授信分项额度
            if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                lmtGrpMemRelService.updateAmtByLmtApp(lmtApp);
                log.info(String.format("更新授信分项数据,流水号%s", serno));
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("保存授信分项数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "," + e.getMessage());
        } finally {
            rtnData.put("subSerno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    ;

    /**
     * @方法名称: updateLmtAppSub
     * @方法描述: 根据前台传入表单数据更新授信分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map updateLmtAppSub(LmtAppSub lmtAppSub) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String lmtType = "";
        try {

            // TODO 保存前校验 待补充

            //获取原授信分项
            LmtAppSub originLmtAppSub = lmtAppSubMapper.selectByPrimaryKey(lmtAppSub.getPkId());
            BigDecimal originLmtAmt = originLmtAppSub.getLmtAmt();
            //获取对应授信申请信息
            LmtApp lmtApp = lmtAppService.selectBySerno(lmtAppSub.getSerno());
            lmtType = lmtApp.getLmtType();
            //获取对应授信申请信息的原敞口金额以及低风险额度
            BigDecimal originOpenAmt = new BigDecimal(0);
            BigDecimal originLowAmt = new BigDecimal(0);
            BigDecimal openAmt = new BigDecimal(0);
            BigDecimal lowAmt = new BigDecimal(0);
            List<LmtAppSub> appSubList = this.queryLmtAppSubBySerno(lmtApp.getSerno());
            for(LmtAppSub lmtAppSubItem: appSubList){
                if(lmtAppSubItem.getLmtAmt() != null){
                    if ("60".equals(lmtAppSubItem.getGuarMode())) {
                        lowAmt = lowAmt.add(lmtAppSubItem.getLmtAmt());
                    } else {
                        openAmt = openAmt.add(lmtAppSubItem.getLmtAmt());
                    }
                }
            }
            originOpenAmt = openAmt;
            originLowAmt = lowAmt;
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            String guarMode = lmtAppSub.getGuarMode();
            if ("60".equals(guarMode)) {
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt.subtract(originLmtAmt).add(lmtAmt));
                lmtApp.setOpenTotalLmtAmt(originOpenAmt);
            } else {
                BigDecimal newOpenTotalLmtAmt = originOpenAmt.subtract(originLmtAmt).add(lmtAmt);
                if (CmisCommonConstants.LMT_TYPE_02.equals(lmtType) || CmisCommonConstants.LMT_TYPE_04.equals(lmtType) || CmisCommonConstants.LMT_TYPE_06.equals(lmtType)) {
                    if(lmtApp.getOrigiOpenTotalLmtAmt() != null){
                        if (lmtApp.getOrigiOpenTotalLmtAmt().compareTo(newOpenTotalLmtAmt) < 0) {
                            rtnCode = EcbEnum.ECB010042.key;
                            rtnMsg = EcbEnum.ECB010042.value;
                            return rtnData;
                        }
                    }
                }
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt);
                lmtApp.setOpenTotalLmtAmt(newOpenTotalLmtAmt);
            }
            User userInfo = SessionUtils.getUserInformation();
            lmtAppSub.setUpdId(userInfo.getLoginCode());
            lmtAppSub.setUpdBrId(userInfo.getOrg().getCode());
            lmtAppSub.setUpdDate(openday);
            lmtAppSub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
            List<LmtAppSubPrd> list = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
            StringBuilder subNameAppend = new StringBuilder();
            for(LmtAppSubPrd lmtAppSubPrdItem :list){
                subNameAppend.append(lmtAppSubPrdItem.getLmtBizTypeName()).append(",");
            }
            if(subNameAppend.length() >0 ){
                subNameAppend.deleteCharAt(subNameAppend.length()-1);
            }
            lmtAppSub.setSubName(subNameAppend.toString());
            BigDecimal highestLmtAmt = lmtAppSubPrdService.selectHighestLmtAmt(lmtAppSub.getSubSerno());
            if (highestLmtAmt != null && lmtAmt.compareTo(highestLmtAmt) < 0) {
                rtnCode = EcbEnum.ECB010027.key;
                rtnMsg = EcbEnum.ECB010027.value;
                return rtnData;
            }
            int count = lmtAppSubMapper.updateByPrimaryKeySelective(lmtAppSub);
            log.info("更新授信申请数据,流水号{}", lmtApp.getSerno());
            log.info("更新授信分项信息");

            //更新对应授信申请数据
            int countApp = lmtAppService.update(lmtApp);
            log.info("更新授信申请数据,流水号【{}】,敞口金额为【{}】，低风险金额为【{}】",
                    lmtApp.getSerno(), lmtApp.getOpenTotalLmtAmt(), lmtApp.getLowRiskTotalLmtAmt());
            if (count != 1 || countApp != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新授信分项信息失败！");
            }
            // 如果是集团的授信分项额度
            if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                lmtGrpMemRelService.updateAmtByLmtApp(lmtApp);
                log.info(String.format("更新授信分项数据,流水号%s", lmtAppSub.getSubSerno()));
            }

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新授信分项信息出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据授信分项流水号查询授信分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtAppSub selectBySubSerno(String subSerno) {
        return lmtAppSubMapper.selectBySubSerno(subSerno);
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据传参查询授信分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtAppSub> selectByParams(Map params) {
        log.info("查询参数："+JSON.toJSONString(params));
        // 不知道从何处出来的Map中会多一个dataAuth
        if(params.containsKey("dataAuth")){
            params.remove("dataAuth");
        }
        log.info("查询参数："+JSON.toJSONString(params));
        if (!MapUtils.checkExistsEmptyEnum(params)) {
            return lmtAppSubMapper.selectByParams(params);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
    }


    /**
     * @函数名称: queryLmtAppSubBySerno
     * @函数描述: 根据流水号获取分项明细
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSub> queryLmtAppSubBySerno(String Serno) {
        HashMap<String, String> queryHashMap = new HashMap<String, String>();
        queryHashMap.put("serno", Serno);
        queryHashMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return this.selectByParams(queryHashMap);
    }


    /**
     * @方法名称: updateBySerno
     * @方法描述: 授信申请删除后，将对应授信分项进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public boolean updateBySerno(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String subSerno = "";
        boolean result = true;
        Map map = new HashMap();
        map.put("serno", serno);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSub> list = this.selectByParams(map);
        if (list.size() == 0) {
            return result;
        }
        for (int i = 0; i < list.size(); i++) {
            subSerno = list.get(i).getSubSerno();
            boolean resultSubPrd = lmtAppSubPrdService.deleteBySubSerno(subSerno);
            if (!resultSubPrd) {
                return resultSubPrd;
            }
        }
        int count = lmtAppSubMapper.updateBySerno(serno);
        if (count == 0) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
        }
        return result;
    }

    /**
     * @方法名称: copyLmtAppSub
     * @方法描述: 将原先的授信分项及对应的适用产品明细要挂载在新的授信流水号下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyLmtAppSub(String originSerno, String curSerno) {
        Map params = new HashMap();
        String subSerno = "";
        String originSubSerno = "";
        LmtAppSub lmtAppSub = new LmtAppSub();
        params.put("serno", originSerno);
        List<LmtAppSub> subList = this.selectByParams(params);
        if (subList.size() > 0) {
            for (int i = 0; i < subList.size(); i++) {
                lmtAppSub = subList.get(i);
                originSubSerno = lmtAppSub.getSubSerno();
                subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                lmtAppSub.setPkId(UUID.randomUUID().toString());
                lmtAppSub.setSerno(curSerno);
                lmtAppSub.setSubSerno(subSerno);
                int count = lmtAppSubMapper.insert(lmtAppSub);
                boolean copySubPrd = lmtAppSubPrdService.copyBySubSerno(originSubSerno, subSerno);
                boolean copyGuarRel = guarBizRelService.copyLmtAppGuarRel(originSubSerno, subSerno);
                if (count != 1 || !copySubPrd || !copyGuarRel) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",分项挂载失败！");
                }
            }
        }
        return true;
    }

    /**
     * @函数名称:getSubAndPrdForGrpSerno
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种,复用
     * @参数与返回说明:
     * @算法描述:
     */

    public Map getSubAndPrdForGrpSerno(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        String subSerno = "";
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        LmtAppSub lmtAppSub = new LmtAppSub();
        try {
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSub> subList = this.selectByParams(paramsSub);

            if (subList.size() > 0) {
                for (int i = 0; i < subList.size(); i++) {
                    lmtAppSub = subList.get(i);
                    subSerno = lmtAppSub.getSubSerno();
                    paramsSubPrd.put("subSerno", subSerno);
                    paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                    tempMap = new HashMap();
                    tempMap.put("pkId", lmtAppSub.getPkId());
                    tempMap.put("serno", serno);
                    tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                    tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                    tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                    tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                    tempMap.put("guarMode", lmtAppSub.getGuarMode());
                    tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                    tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                    tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                    tempMap.put("children", subListPrd);
                    list.add(tempMap);
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subList", list);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */

    public Map getSubAndPrd(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        String subSerno = "";
        Map<String,String> paramsSub = new HashMap();
        Map<String,String> paramsSubPrd = null;
        try {
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("分项列表查询参数："+JSON.toJSONString(paramsSub));
            List<LmtAppSub> subList = this.selectByParams(paramsSub);
            for(LmtAppSub lmtAppSub :subList){
                paramsSubPrd = new HashMap();
                subSerno = lmtAppSub.getSubSerno();
                if(subSerno == null || "".equals(subSerno)){
                    continue;
                }
                paramsSubPrd.put("subSerno", subSerno);
                paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                tempMap = new HashMap();
                tempMap.put("pkId", lmtAppSub.getPkId());
                tempMap.put("serno", serno);
                tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                tempMap.put("isRevolvLimit", lmtAppSub.getIsRevolvLimit());
                tempMap.put("guarMode", lmtAppSub.getGuarMode());
                tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                //tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                tempMap.put("children", subListPrd);
                list.add(tempMap);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subList", list);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */

    public Map getSubAndPrdForReCheck(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        String subSerno = "";
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        LmtAppSub lmtAppSub = new LmtAppSub();
        BigDecimal subSumAmt = new BigDecimal(0);
        BigDecimal subPrdSumAmt = new BigDecimal(0);
        try {
            User userInfo = SessionUtils.getUserInformation();
            String instuCode = CmisCommonUtils.getInstucde(userInfo.getOrg().getCode());
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSub> subList = this.selectByParams(paramsSub);
            if (subList.size() > 0) {
                for (int i = 0; i < subList.size(); i++) {
                    lmtAppSub = subList.get(i);
                    subSerno = lmtAppSub.getSubSerno();
                    paramsSubPrd.put("subSerno", subSerno);
                    paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
                    tempMap = new HashMap();
                    tempMap.put("serno", serno);
                    tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                    tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                    tempMap.put("guarMode", lmtAppSub.getGuarMode());
                    tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                    //  计算授信合计
                    subSumAmt = subSumAmt.add(lmtAppSub.getLmtAmt()==null?new BigDecimal(0):lmtAppSub.getLmtAmt());
                    List<Map> mapPrdList = new ArrayList<>();
                    for(LmtAppSubPrd lmtAppSubPrd : subListPrd){
                        Map map = new HashMap();
                        map.put("subPrdSerno",lmtAppSubPrd.getSubPrdSerno());
                        map.put("lmtBizTypeName",lmtAppSubPrd.getLmtBizTypeName());
                        map.put("guarMode",lmtAppSubPrd.getGuarMode());
                        map.put("lmtAmt",lmtAppSubPrd.getLmtAmt());
                        // 目前用信净额 contAmt
                        CmisLmt0059ReqDto cmisLmt0059ReqDto = new CmisLmt0059ReqDto();
                        cmisLmt0059ReqDto.setInstuCde(instuCode);
                        cmisLmt0059ReqDto.setApprSubSerno(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                        log.info("获取分项品种编号对应的用信净额,请求报文:"+JSON.toJSONString(cmisLmt0059ReqDto));
                        CmisLmt0059RespDto cmisLmt0059RespDto = cmisLmtClientService.cmislmt0059(cmisLmt0059ReqDto).getData();
                        log.info("获取分项品种编号对应的用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                        if(cmisLmt0059RespDto.getErrorCode().equals(EcbEnum.ECB010000.key)){
                            if(lmtAppSubPrd.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)){
                                log.info("低风险二级分项用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                                map.put("contAmt",cmisLmt0059RespDto.getSubLoanBal());
                                subPrdSumAmt = subPrdSumAmt.add(cmisLmt0059RespDto.getSubLoanBal());
                            }else{
                                log.info("非低风险二级分项用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                                map.put("contAmt",cmisLmt0059RespDto.getSubSpacLoanBal());
                                subPrdSumAmt = subPrdSumAmt.add(cmisLmt0059RespDto.getSubSpacLoanBal());
                            }
                        }else{
                            map.put("contAmt","N/A");
                        }
                        mapPrdList.add(map);
                    }
                    tempMap.put("children", mapPrdList);
                    list.add(tempMap);
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subSumAmt", subSumAmt);
            rtnData.put("subPrdSumAmt", subPrdSumAmt);
            rtnData.put("subList", list);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map updateByPkId(String pkid) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {

            // TODO 待补充   删除分项  更新申请数据
            LmtAppSub lmtAppSub = lmtAppSubMapper.selectByPrimaryKey(pkid);
            String parentSerno = lmtAppSub.getSerno();
            LmtApp lmtApp = lmtAppService.selectBySerno(parentSerno);
            String subSerno = lmtAppSub.getSubSerno();
            if(!CmisCommonConstants.LMT_TYPE_01.equals(lmtApp.getLmtType()) && lmtAppSub.getOrigiLmtAccSubNo() != null && !"".equals(lmtAppSub.getOrigiLmtAccSubNo())) {
                CmisLmt0047ReqDto cmisLmt0047ReqDto = new CmisLmt0047ReqDto();
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("accSubNo", lmtAppSub.getOrigiLmtAccSubNo());
                List<LmtReplyAccSub> list = lmtReplyAccSubService.queryLmtReplyAccSubByParams(queryMap);
                cmisLmt0047ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_01);
                List<CmisLmt0047LmtSubDtoList> lmtSubDtoList = new ArrayList<>();
                if (list != null && list.size() == 1) {
                    log.info("发送额度系统校验分项下是否存在未结清的业务,分项编号:" + lmtAppSub.getOrigiLmtAccSubNo());
                    CmisLmt0047LmtSubDtoList cmisLmt0047LmtSubDtoList = new CmisLmt0047LmtSubDtoList();
                    cmisLmt0047LmtSubDtoList.setAccSubNo(lmtAppSub.getOrigiLmtAccSubNo());
                    lmtSubDtoList.add(cmisLmt0047LmtSubDtoList);
                }
                cmisLmt0047ReqDto.setLmtSubDtoList(lmtSubDtoList);
                log.info("发送额度系统校验----------start-----------------,请求报文{}", JSON.toJSONString(cmisLmt0047ReqDto));
                ResultDto<CmisLmt0047RespDto> cmisLmt0047RespDto = cmisLmtClientService.cmislmt0047(cmisLmt0047ReqDto);
                if (cmisLmt0047RespDto != null && cmisLmt0047RespDto.getData() != null && "0000".equals(cmisLmt0047RespDto.getData().getErrorCode())) {
                    log.info("发送额度系统校验成功!");
                    List<CmisLmt0047ContRelDtoList> cmisLmt0047ContRelDtoLists = cmisLmt0047RespDto.getData().getContRelDtoList();
                    if (!cmisLmt0047ContRelDtoLists.isEmpty() && cmisLmt0047ContRelDtoLists.size() > 0) {
                        log.info("当前授信分项下存在未结清的业务,不可删除!");
                        rtnCode = EcbEnum.ECB020041.key;
                        rtnMsg = EcbEnum.ECB020041.value;
                        return rtnData;
                    }
                }
            }
            if (StringUtils.isEmpty(parentSerno)) {
                rtnCode = EpbEnum.EPB099999.key;
                rtnMsg = EpbEnum.EPB099999.value;
                return rtnData;
            }
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            String guarMode = lmtAppSub.getGuarMode();
            //获取对应授信申请信息的原敞口金额以及低风险额度
            BigDecimal originOpenAmt = new BigDecimal(0);
            BigDecimal originLowAmt = new BigDecimal(0);
            BigDecimal openAmt = new BigDecimal(0);
            BigDecimal lowAmt = new BigDecimal(0);
            List<LmtAppSub> appSubList = this.queryLmtAppSubBySerno(lmtApp.getSerno());
            for(LmtAppSub lmtAppSubItem: appSubList){
                if(lmtAppSubItem.getLmtAmt() != null){
                    if ("60".equals(lmtAppSubItem.getGuarMode())) {
                        lowAmt = lowAmt.add(lmtAppSubItem.getLmtAmt());
                    } else {
                        openAmt = openAmt.add(lmtAppSubItem.getLmtAmt());
                    }
                }
            }
            originOpenAmt = openAmt;
            originLowAmt = lowAmt;
            if ("60".equals(guarMode)) {
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt.subtract(lmtAmt));
                lmtApp.setOpenTotalLmtAmt(originOpenAmt);
            } else {
                lmtApp.setLowRiskTotalLmtAmt(originLowAmt);
                lmtApp.setOpenTotalLmtAmt(originOpenAmt.subtract(lmtAmt));
            }
            int countSub = lmtAppSubMapper.updateByPkId(lmtAppSub);
            int countApp = lmtAppService.update(lmtApp);
            boolean resultPrd = lmtAppSubPrdService.deleteBySubSerno(subSerno);
            if (countSub != 1 || countApp != 1 || !resultPrd) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
            // 如果是集团的授信分项额度
            if (CmisCommonConstants.YES_NO_1.equals(lmtApp.getIsGrp())) {
                lmtGrpMemRelService.updateAmtByLmtApp(lmtApp);
                log.info(String.format("更新授信分项数据,流水号%s", lmtAppSub.getSubSerno()));
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除授信明细情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    ;

    /**
     * @方法名称: riskItem0002
     * @方法描述: 授信分项金额与项下额度品种金额校验
     * @参数与返回说明:
     * @算法描述: 授信分项下的单个授信品种的授信额度必须小于等于授信分项的授信额度
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0002(QueryModel queryModel) {
        log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 开始");
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = (String) queryModel.getCondition().get("bizId");
        String bizType = (String) queryModel.getCondition().get("bizType");
        log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 获取业务参数正常");

        // 判断是否为单一授信
        if(CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType)){
            log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请逻辑校验");
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            // 判断是审批流程中的拦截还是待发起业务拦截
            if(CmisCommonConstants.WF_STATUS_111.equals(lmtApp.getApproveStatus())){
                log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请审批中逻辑校验 -> 查询审批表数据进行校验");
                LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(serno);
                if(lmtAppr == null || lmtAppr.getApproveSerno() == null){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_000301);
                    return riskResultDto;
                }
                log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请审批中逻辑校验 -> 查询审批表数据进行校验");
                List<LmtApprSub> lmtApprSubList = lmtApprSubService.selectAllByApprSerno(lmtAppr.getApproveSerno());
                if (CollectionUtils.isEmpty(lmtApprSubList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_000401);
                    return riskResultDto;
                }
                log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请审批中逻辑校验 -> 遍历授信审批分项");
                // 初始化授信分项的授信额度
                BigDecimal lmtSubAmt = BigDecimal.ZERO;
                for (LmtApprSub lmtApprSub : lmtApprSubList) {
                    // 初始化授信分项品种的授信额度
                    BigDecimal lmtSubPrdSumAmt = BigDecimal.ZERO;
                    List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdService.selectBySubSerno(lmtApprSub.getApproveSubSerno());
                    log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请审批中逻辑校验 -> 遍历授信申请分项品种");
                    if (CollectionUtils.nonEmpty(lmtApprSubPrdList)) {
                        for (LmtApprSubPrd lmtApprSubPrd : lmtApprSubPrdList) {
                            lmtSubPrdSumAmt = lmtSubPrdSumAmt.add(Objects.isNull(lmtApprSubPrd.getLmtAmt()) ? BigDecimal.ZERO : lmtApprSubPrd.getLmtAmt());
                            lmtSubAmt = Objects.isNull(lmtApprSub.getLmtAmt()) ? BigDecimal.ZERO : lmtApprSub.getLmtAmt();
                            // 如果单个授信品种的授信额度必须小于等于授信分项的授信额度
                            if (Objects.isNull(lmtApprSubPrd)
                                    || lmtSubAmt.compareTo(Objects.isNull(lmtApprSubPrd.getLmtAmt()) ? BigDecimal.ZERO : lmtApprSubPrd.getLmtAmt()) < 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_000401 + ",单一授信品种编号为" + lmtApprSubPrd.getSubPrdSerno());
                                return riskResultDto;
                            }
                        }
                        // 如果分项金额大于品种金额之和
                        if(lmtSubAmt.compareTo(lmtSubPrdSumAmt) > 0){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_000402);
                            return riskResultDto;
                        }
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_000501);
                        return riskResultDto;
                    }
                }
            }else{
                log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请待发起逻辑校验 -> 查询申请表数据进行校验");
                List<LmtAppSub> lmtAppSubList = this.queryLmtAppSubBySerno(serno);
                if (CollectionUtils.isEmpty(lmtAppSubList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                    return riskResultDto;
                }
                log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请待发起逻辑校验 -> 遍历授信申请分项");
                // 初始化授信分项的授信额度
                BigDecimal lmtSubAmt = BigDecimal.ZERO;
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    // 初始化授信分项品种的授信额度
                    BigDecimal lmtSubPrdSumAmt = BigDecimal.ZERO;
                    List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                    log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 单一授信申请待发起逻辑校验 -> 遍历授信申请分项品种");
                    if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            lmtSubPrdSumAmt = lmtSubPrdSumAmt.add(Objects.isNull(lmtAppSubPrd.getLmtAmt()) ? BigDecimal.ZERO : lmtAppSubPrd.getLmtAmt());
                            lmtSubAmt = Objects.isNull(lmtAppSub.getLmtAmt()) ? BigDecimal.ZERO : lmtAppSub.getLmtAmt();
                            // 如果单个授信品种的授信额度必须小于等于授信分项的授信额度
                            if (Objects.isNull(lmtAppSubPrd)
                                    || lmtSubAmt.compareTo(Objects.isNull(lmtAppSubPrd.getLmtAmt()) ? BigDecimal.ZERO : lmtAppSubPrd.getLmtAmt()) < 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_000401 + ",单一授信品种编号为" + lmtAppSubPrd.getSubPrdSerno());
                                return riskResultDto;
                            }
                        }
                        // 如果分项金额大于品种金额之和
                        if(lmtSubAmt.compareTo(lmtSubPrdSumAmt) > 0){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_000402);
                            return riskResultDto;
                        }
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                        return riskResultDto;
                    }
                }
            }
        }else{
            log.info("风险拦截riskItem0002执行逻辑 -> 授信分项金额与项下额度品种金额校验 -> 集团授信申请逻辑校验");
            // TODO 集团客户的调整逻辑需要确定后完善
        }


        log.info("授信分项金额与项下额度品种金额校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0010
     * @方法描述: 对公授信业务品种互斥校验
     * @参数与返回说明:
     * @算法描述: 对公授信分项额度不为0时，触发以下规则
     * 1、同一个授信项下，不能同时存在保函和在线保函
     * 2、同一授信分项项下，授信品种+产品类型属性不能重复
     * 3、授信分项项下必须存在业务品种
     * @创建人: cainingbo
     * @创建时间: 2021-06-23 10:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0010(String serno) {
        log.info("对公授信业务品种互斥校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        List<LmtAppSub> lmtAppSubList = this.queryLmtAppSubBySerno(serno);
        if (CollectionUtils.isEmpty(lmtAppSubList)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        for (LmtAppSub lmtAppSubs : lmtAppSubList) {
            Map map = new HashMap();
            BigDecimal lmtAmt = Objects.isNull(lmtAppSubs.getLmtAmt()) ? new BigDecimal("0") : lmtAppSubs.getLmtAmt();
            //授信额度不为0时触发规则
            if (BigDecimal.ZERO.compareTo(lmtAmt) != 0) {
                map.put("subSerno", lmtAppSubs.getSubSerno());
                map.put("lmtBizType", "13010102");
                int count = lmtAppSubPrdService.selectCountsByParams(map);//查询线上保函数量
                if (count > 0) {
                    int newCount = lmtAppSubPrdService.selectNewCountsByParams(lmtAppSubs.getSubSerno());//查询保函数量
                    if (newCount > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01001);
                        return riskResultDto;
                    }
                }
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSubs.getSubSerno());
                if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                    for (LmtAppSubPrd lmtAppSubPrds : lmtAppSubPrdList) {
                        map.put("lmtBizType", lmtAppSubPrds.getLmtBizType());
                        map.put("lmtBizTypeProp", lmtAppSubPrds.getLmtBizTypeProp());
                        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                        int Secondcount = lmtAppSubPrdService.selectCountsByParams(map);
                        if (Secondcount >= 2) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01002);
                            return riskResultDto;
                        }
                    }
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0013);
                    return riskResultDto;
                }
            }
        }
        log.info("对公授信业务品种互斥校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param
     * @函数名称:selectLmtAppDataBySubSerno
     * @函数描述:根据分项流水号查询 授信类型
     * @参数与返回说明:
     * @算法描述:
     */

    public LmtApp selectLmtAppDataBySubSerno(String subSerno) {
        return lmtAppService.selectLmtAppDataBySubSerno(subSerno);
    }

    /**
     * @param
     * @函数名称:selectLmtAppSubDataBySerno
     * @函数描述:根据单一客户流水号查询项下的分项信息
     * @参数与返回说明:
     * @算法描述:
     */

    public List<LmtAppSub> selectLmtAppSubDataBySerno(String singleSerno) {
        HashMap paramsSub = new HashMap();
        paramsSub.put("serno", singleSerno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSub> subList = this.selectByParams(paramsSub);
        return subList;
    }

    /**
     * @方法名称: riskItem0042
     * @方法描述: 授信担保信息校验
     * @参数与返回说明:
     * @算法描述:单一客户授信申报时，如果分项填写时未落实具体担保及用信相关信息，则拦截
     * @创建人: yfs
     * @创建时间: 2021-07-13 10:47:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0042(String serno) {
        log.info("授信担保信息校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSub> subList = this.selectByParams(paramsSub);
        if(CollectionUtils.isEmpty(subList)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        for (LmtAppSub lmtAppSub : subList) {
            // 担保方式：信用:00,抵押:10,质押:20,低风险质押:21,保证：30,全额保证金:40,低风险:60
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            if(CollectionUtils.isEmpty(subListPrd)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                return riskResultDto;
            }
            if (Objects.equals("10", lmtAppSub.getGuarMode()) || Objects.equals("20", lmtAppSub.getGuarMode())) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno", serno);
                List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseDtoList = guarBaseInfoMapper.selectByIqpSernoModel(queryModel);
                if (CollectionUtils.isEmpty(guarBizRelGuarBaseDtoList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0008);
                    return riskResultDto;
                }
            }
            if (Objects.equals("30", lmtAppSub.getGuarMode())) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno", serno);
                // 查询保证担保信息
                List<GuarBizRelGuaranteeDto> guarBizRelGuaranteeDtoList = guarGuaranteeMapper.selectByIqpSernoModel(queryModel);
                // 判定保证担保人信息是否为空
                if (CollectionUtils.isEmpty(guarBizRelGuaranteeDtoList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0009);
                    return riskResultDto;
                }
            }
        }
        log.info("授信担保信息校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }


    /**
     * @方法名称: riskItem0015
     * @方法描述: 外贸贷产品授信校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuqi
     * @创建时间: 2021-06-24
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0015(String serno) {
        log.info("外贸贷产品授信校验*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 获取申请信息
        Map<String, Object> map = cusCheck(serno);
        // 单一客户授信
        LmtApp lmtApp = (LmtApp) map.get("lmtApp");
        // 集团客户授信
        LmtGrpApp lmtGrpApp = (LmtGrpApp) map.get("lmtGrpApp");
        if (Objects.isNull(lmtApp) && Objects.isNull(lmtGrpApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }
        // 如果是集团客户，则去获取对应的集团成员客户信息
        if (Objects.nonNull(lmtGrpApp)) {
            List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
            if (Objects.nonNull(list) && list.size() > 0) {
                for (LmtGrpMemRel lmtGrpMemRel : list) {
                    riskResultDto = riskItem0015Check(lmtGrpMemRel.getSingleSerno(), lmtGrpMemRel.getCusId());
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0006);
                return riskResultDto;
            }
        }
        if (Objects.nonNull(lmtApp)) {
            riskResultDto = riskItem0015Check(lmtApp.getSerno(), lmtApp.getCusId());
            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param serno
     * @param cusId
     * @return
     */
    public RiskResultDto riskItem0015Check(String serno, String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("外贸贷产品授信校验，查询分项参数："+ JSON.toJSONString(paramsSub));
        List<LmtAppSub> subList = selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        // 初始化（授信分项（循环+非循环）总额）

        Boolean checkFlag = true;
        for (LmtAppSub lmtAppSub : subList) {
            // 外贸贷：P032
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("外贸贷产品授信校验，查询品种参数："+ JSON.toJSONString(paramsSubPrd));
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            if (Objects.nonNull(subListPrd) && !subListPrd.isEmpty()) {
                for (LmtAppSubPrd lmtAppSubPrd : subListPrd) {
                    // 外贸贷：外贸贷授信分项总额（循环+非循环）不能超过1000万。（设定参数：外贸贷授信限额）
                    if (Objects.equals("P032", lmtAppSubPrd.getLmtBizTypeProp())) {
                        if (checkFlag) {
                            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                            if (!CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);//不通过
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1501); //借款人不是对公客户
                                return riskResultDto;
                            }
                            checkFlag = false;
                        }
                        BigDecimal P032total = new BigDecimal("0");
                        P032total = P032total.add(Objects.nonNull(lmtAppSubPrd.getLmtAmt()) ? lmtAppSubPrd.getLmtAmt() : new BigDecimal("0"));
                        // 外贸贷授信分项总额（循环+非循环）不能超过1000万
                        if (P032total.compareTo(new BigDecimal("10000000")) > 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001501);
                            return riskResultDto;
                        }
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                return riskResultDto;
            }
        }
        return riskResultDto;
    }


    /**
     * @方法名称: riskItem0017
     * @方法描述: 特色产品限额检验
     * @参数与返回说明:
     * @算法描述: 授信申请提交时点触发：
     * （1）诚易融：授信分项（循环+非循环）总额大于200万，拦截。
     * （2）简易融：授信分项（循环+非循环）总额大于500万，拦截。
     * （3）宿迁园区保：授信分项（循环+非循环）总额大于1000万，拦截。
     * （设定参数：诚易融授信限额、简易融授信限额、宿迁园区保授信限额）
     * （4）省心快贷：授信分项（循环+非循环）总额大于1000万，拦截；
     * （5）外贸贷：授信分项（循环+非循环）总额大于1000万，拦截；
     * （6）结息贷：授信分项（循环+非循环）总额大于200万，拦截；
     * （7）优税贷：授信分项（循环+非循环）总额大于300万，拦截；
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0017(String serno) {
        log.info("特色产品限额检验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 获取申请信息
        Map<String, Object> map = cusCheck(serno);
        // 单一客户授信
        LmtApp lmtApp = (LmtApp) map.get("lmtApp");
        // 集团客户授信
        LmtGrpApp lmtGrpApp = (LmtGrpApp) map.get("lmtGrpApp");
        if (Objects.isNull(lmtApp) && Objects.isNull(lmtGrpApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }
        // 如果是集团客户，则去获取对应的集团成员客户信息
        if (Objects.nonNull(lmtGrpApp)) {
            List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
            if (Objects.nonNull(list) && list.size() > 0) {
                for (LmtGrpMemRel lmtGrpMemRel : list) {
                    riskResultDto = riskItem0017Check(lmtGrpMemRel.getSingleSerno());
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0006);
                return riskResultDto;
            }
        }
        if (Objects.nonNull(lmtApp)) {
            riskResultDto = riskItem0017Check(lmtApp.getSerno());
            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param serno
     * @return
     */
    public RiskResultDto riskItem0017Check(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        Map paramsSubPrd = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("特色产品限额检验开始，查询分项参数："+ JSON.toJSONString(paramsSub));
        List<LmtAppSub> subList = selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        // 初始化（授信分项（循环+非循环）总额）
        BigDecimal P016total = new BigDecimal("0");
        BigDecimal P025total = new BigDecimal("0");
        BigDecimal P023total = new BigDecimal("0");
        BigDecimal P011total = new BigDecimal("0");
        BigDecimal P032total = new BigDecimal("0");
        BigDecimal P013total = new BigDecimal("0");
        BigDecimal P014total = new BigDecimal("0");
        for (LmtAppSub lmtAppSub : subList) {
            // 诚易融：P016，简易融：P025，宿迁园区保：P023 省心快贷:P011 外贸贷:P032 结息贷：P013 优税贷：P014
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("特色产品限额检验开始，查询品种参数："+ JSON.toJSONString(paramsSubPrd));
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            BigDecimal P016item = new BigDecimal("0");
            BigDecimal P025item = new BigDecimal("0");
            BigDecimal P023item = new BigDecimal("0");
            BigDecimal P011item = new BigDecimal("0");
            BigDecimal P032item = new BigDecimal("0");
            BigDecimal P013item = new BigDecimal("0");
            BigDecimal P014item = new BigDecimal("0");
            if (Objects.nonNull(subListPrd)) {
                for (LmtAppSubPrd LmtAppSubPrd : subListPrd) {
                    // 诚易融：授信分项（循环+非循环）总额大于200万
                    if (Objects.equals("P016", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P016item) > 0){
                                P016item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 简易融：授信分项（循环+非循环）总额大于500万，拦截。
                    if (Objects.equals("P025", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P025item) > 0){
                                P025item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 宿迁园区保：授信分项（循环+非循环）总额大于1000万，拦截。
                    if (Objects.equals("P023", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P023item) > 0){
                                P023item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 省心快贷：授信分项（循环+非循环）总额大于1000万，拦截；
                    if (Objects.equals("P011", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P011item) > 0){
                                P011item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 外贸贷：授信分项（循环+非循环）总额大于1000万，拦截；
                    if (Objects.equals("P032", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P032item) > 0){
                                P032item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 结息贷：授信分项（循环+非循环）总额大于200万，拦截；
                    if (Objects.equals("P013", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P013item) > 0){
                                P013item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }

                    // 优税贷：授信分项（循环+非循环）总额大于300万，拦截；
                    if (Objects.equals("P014", LmtAppSubPrd.getLmtBizTypeProp())) {
                        if(Objects.nonNull(LmtAppSubPrd.getLmtAmt())){
                            if(LmtAppSubPrd.getLmtAmt().compareTo(P014item) > 0){
                                P014item = LmtAppSubPrd.getLmtAmt();
                            }
                        }
                    }
                }

                P016total = P016total.add(P016item);
                // 诚易融：授信分项（循环+非循环）总额大于200万
                if (P016total.compareTo(new BigDecimal("2000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001701);
                    return riskResultDto;
                }

                P025total = P025total.add(P025item);
                // 简易融：授信分项（循环+非循环）总额大于500万，拦截。
                if (P025total.compareTo(new BigDecimal("5000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001702);
                    return riskResultDto;
                }

                P023total =  P023total.add(P023item);
                // 宿迁园区保：授信分项（循环+非循环）总额大于1000万，拦截。
                if (P023total.compareTo(new BigDecimal("10000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001703);
                    return riskResultDto;
                }

                P011total = P011total.add(P011item);
                // 省心快贷：授信分项（循环+非循环）总额大于1000万，拦截；
                if (P011total.compareTo(new BigDecimal("10000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001704);
                    return riskResultDto;
                }

                P032total = P032total.add(P032item);
                // 外贸贷：授信分项（循环+非循环）总额大于1000万，拦截；
                if (P032total.compareTo(new BigDecimal("10000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001705);
                    return riskResultDto;
                }

                P013total = P013total.add(P013item);
                // 结息贷：授信分项（循环+非循环）总额大于200万，拦截；
                if (P013total.compareTo(new BigDecimal("2000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001706);
                    return riskResultDto;
                }

                P014total = P014total.add(P014item);
                // 优税贷：授信分项（循环+非循环）总额大于300万，拦截；
                if (P014total.compareTo(new BigDecimal("3000000")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001707);
                    return riskResultDto;
                }

            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                return riskResultDto;
            }
        }
        return riskResultDto;
    }

    /**
     * 根据申请流水号去判定是单一客户授信或者是集团客户授信
     *
     * @param serno
     * @return
     */
    public Map<String, Object> cusCheck(String serno) {
        Map<String, Object> map = new HashMap<>();
        // 查询是否是单一客户授信
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            // 查询是否是集团客户授信
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
            if (Objects.nonNull(lmtGrpApp)) {
                map.put("lmtGrpApp", lmtGrpApp);
            }
        } else {
            map.put("lmtApp", lmtApp);
        }
        return map;
    }

    /**
     * 通用获取分项以及分项明细校验
     *
     * @param serno
     * @return
     */
    public Map<String, Object> riskItemCommonAppSubCheck(String serno) {
        log.info("通用获取分项以及分项明细校验开始*******************业务流水号：【{}】", serno);
        Map<String, Object> map = new HashMap<>();
        RiskResultDto riskResultDto = new RiskResultDto();
        map.put("riskResultDto", riskResultDto);
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("通用获取分项校验，查询参数："+JSON.toJSONString(paramsSub));
        List<LmtAppSub> subList = selectByParams(paramsSub);
        List<LmtAppSubPrd> subPrdList = new ArrayList<>();
        if (CollectionUtils.isEmpty(subList)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            map.put("riskResultDto", riskResultDto);
        } else {
            map.put("subList", subList);
        }
        for (LmtAppSub lmtAppSub : subList) {
            Map paramsSubPrd = new HashMap();
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("通用获取分项明细校验，查询参数："+JSON.toJSONString(paramsSubPrd));
            List<LmtAppSubPrd> subPrds = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            if (CollectionUtils.nonEmpty(subPrds)) {
                subPrdList.addAll(subPrds);
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0013);
                map.put("riskResultDto", riskResultDto);
            }
        }
        log.info("通用获取分项以及分项明细校验结束*******************业务流水号：【{}】", serno);
        map.put("subPrdList", subPrdList);
        return map;
    }

    /**
     * 获取一般额度分项信息
     */
    public List<LmtAppSub> getNormal(String serno) {
        return lmtAppSubMapper.getNormal(serno);
    }

    /**
     * 获取低风险额度分项信息
     */
    public List<LmtAppSub> getLow(String serno) {
        return lmtAppSubMapper.getLow(serno);
    }

    /**
     * @函数名称:judgesubandprd
     * @函数描述:判断当前授信申请对应的授信分项及授信分项下的适用授信品种是否包含信保贷
     * @参数与返回说明:
     * @算法描述:
     */
    public Map judgesubandprd(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<Map> list = new ArrayList<>();
        Map tempMap = new HashMap();
        Map paramsSub = new HashMap();
        LmtAppSubPrd lmtAppSub = new LmtAppSubPrd();
        String lmtBizTypeProp = "";
        String isXbd = "2";
        try {
            paramsSub.put("serno", serno);
            paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.judgePrdByParams(paramsSub);
            for (int i = 0; i < subListPrd.size(); i++) {
                lmtAppSub = subListPrd.get(i);
                lmtBizTypeProp = lmtAppSub.getLmtBizTypeProp();
                if ("P010".equals(lmtBizTypeProp)) {
                    isXbd = "1";
                    return tempMap;
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("分项信息查询失败！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            tempMap.put("isXbd", isXbd);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return tempMap;
    }

    /**
     * @函数名称:queryLmtAppSubByGrpSerno
     * @函数描述:根据集团授信申请流水号查询项下的分项信息
     * @参数与返回说明:
     * @算法描述:
     */

    public List<LmtAppSub> queryLmtAppSubByGrpSerno(String grpSerno) {
        return lmtAppSubMapper.queryLmtAppSubByGrpSerno(grpSerno);
    }
}
