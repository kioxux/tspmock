/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarSpeCorp
 * @类描述: rpt_lmt_repay_anys_guar_spe_corp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:40:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_spe_corp")
public class RptLmtRepayAnysGuarSpeCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 总体概述 **/
	@Column(name = "GENERAL_OVERVIEW", unique = false, nullable = true, length = 65535)
	private String generalOverview;
	
	/** 担保公司名称 **/
	@Column(name = "GUAR_COM_NAME", unique = false, nullable = true, length = 40)
	private String guarComName;
	
	/** 担保公司类型 **/
	@Column(name = "GUAR_COM_TYPE", unique = false, nullable = true, length = 5)
	private String guarComType;
	
	/** 反担保情况 **/
	@Column(name = "GUAR_COUNTER", unique = false, nullable = true, length = 65535)
	private String guarCounter;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 总合作额度 **/
	@Column(name = "COOP_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal coopTotalAmt;
	
	/** 额度是否可循环 **/
	@Column(name = "IS_QUOTA_RECY", unique = false, nullable = true, length = 5)
	private String isQuotaRecy;
	
	/** 单笔业务合作限额 **/
	@Column(name = "SING_BUSI_COOP_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singBusiCoopLmt;
	
	/** 承担违约风险比例 **/
	@Column(name = "PROP_DEFAULT_RISK", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal propDefaultRisk;
	
	/** 单户限额 **/
	@Column(name = "SING_ACCO_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singAccoLmt;
	
	/** 代偿宽限期 **/
	@Column(name = "COM_GRA_PED", unique = false, nullable = true, length = 10)
	private Integer comGraPed;
	
	/** 已用合作额度 **/
	@Column(name = "USED_COOP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal usedCoopAmt;
	
	/** 可用合作额度 **/
	@Column(name = "AVAIL_COOP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal availCoopAmt;
	
	/** 合作起始日 **/
	@Column(name = "COOP_START_DATE", unique = false, nullable = true, length = 20)
	private String coopStartDate;
	
	/** 合作到期日 **/
	@Column(name = "COOP_END_DATE", unique = false, nullable = true, length = 20)
	private String coopEndDate;
	
	/** 合作方状态 **/
	@Column(name = "PARTNER_STATUS", unique = false, nullable = true, length = 5)
	private String partnerStatus;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param generalOverview
	 */
	public void setGeneralOverview(String generalOverview) {
		this.generalOverview = generalOverview;
	}
	
    /**
     * @return generalOverview
     */
	public String getGeneralOverview() {
		return this.generalOverview;
	}
	
	/**
	 * @param guarComName
	 */
	public void setGuarComName(String guarComName) {
		this.guarComName = guarComName;
	}
	
    /**
     * @return guarComName
     */
	public String getGuarComName() {
		return this.guarComName;
	}
	
	/**
	 * @param guarComType
	 */
	public void setGuarComType(String guarComType) {
		this.guarComType = guarComType;
	}
	
    /**
     * @return guarComType
     */
	public String getGuarComType() {
		return this.guarComType;
	}
	
	/**
	 * @param guarCounter
	 */
	public void setGuarCounter(String guarCounter) {
		this.guarCounter = guarCounter;
	}
	
    /**
     * @return guarCounter
     */
	public String getGuarCounter() {
		return this.guarCounter;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param coopTotalAmt
	 */
	public void setCoopTotalAmt(java.math.BigDecimal coopTotalAmt) {
		this.coopTotalAmt = coopTotalAmt;
	}
	
    /**
     * @return coopTotalAmt
     */
	public java.math.BigDecimal getCoopTotalAmt() {
		return this.coopTotalAmt;
	}
	
	/**
	 * @param isQuotaRecy
	 */
	public void setIsQuotaRecy(String isQuotaRecy) {
		this.isQuotaRecy = isQuotaRecy;
	}
	
    /**
     * @return isQuotaRecy
     */
	public String getIsQuotaRecy() {
		return this.isQuotaRecy;
	}
	
	/**
	 * @param singBusiCoopLmt
	 */
	public void setSingBusiCoopLmt(java.math.BigDecimal singBusiCoopLmt) {
		this.singBusiCoopLmt = singBusiCoopLmt;
	}
	
    /**
     * @return singBusiCoopLmt
     */
	public java.math.BigDecimal getSingBusiCoopLmt() {
		return this.singBusiCoopLmt;
	}
	
	/**
	 * @param propDefaultRisk
	 */
	public void setPropDefaultRisk(java.math.BigDecimal propDefaultRisk) {
		this.propDefaultRisk = propDefaultRisk;
	}
	
    /**
     * @return propDefaultRisk
     */
	public java.math.BigDecimal getPropDefaultRisk() {
		return this.propDefaultRisk;
	}
	
	/**
	 * @param singAccoLmt
	 */
	public void setSingAccoLmt(java.math.BigDecimal singAccoLmt) {
		this.singAccoLmt = singAccoLmt;
	}
	
    /**
     * @return singAccoLmt
     */
	public java.math.BigDecimal getSingAccoLmt() {
		return this.singAccoLmt;
	}
	
	/**
	 * @param comGraPed
	 */
	public void setComGraPed(Integer comGraPed) {
		this.comGraPed = comGraPed;
	}
	
    /**
     * @return comGraPed
     */
	public Integer getComGraPed() {
		return this.comGraPed;
	}
	
	/**
	 * @param usedCoopAmt
	 */
	public void setUsedCoopAmt(java.math.BigDecimal usedCoopAmt) {
		this.usedCoopAmt = usedCoopAmt;
	}
	
    /**
     * @return usedCoopAmt
     */
	public java.math.BigDecimal getUsedCoopAmt() {
		return this.usedCoopAmt;
	}
	
	/**
	 * @param availCoopAmt
	 */
	public void setAvailCoopAmt(java.math.BigDecimal availCoopAmt) {
		this.availCoopAmt = availCoopAmt;
	}
	
    /**
     * @return availCoopAmt
     */
	public java.math.BigDecimal getAvailCoopAmt() {
		return this.availCoopAmt;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate;
	}
	
    /**
     * @return coopStartDate
     */
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate;
	}
	
    /**
     * @return coopEndDate
     */
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param partnerStatus
	 */
	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}
	
    /**
     * @return partnerStatus
     */
	public String getPartnerStatus() {
		return this.partnerStatus;
	}


}