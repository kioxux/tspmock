package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @创建人 WH
 * @创建时间 2021-05-06
 * @return LmtSurveyReportDto
 **/
public class LmtSurveyReportDto extends BaseDomain implements Serializable {
    //基本信息
    private LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo;
    //调查结论
    private LmtSurveyConInfo lmtSurveyConInfo;
    //企业信息
    private LmtSurveyReportComInfo lmtSurveyReportComInfo;
    //流水号
    private String surveySerno;
    //模型审批结果
    private String model;
    //优惠利率审批结果
    private String perfer;
    //营销人工号
    private String marId;
    //客户经理工号
    private String managerId;

    private String iqpSerno;
    private int page = 1;
    private int size = 10;
    private Map<String, Object> condition = new HashMap();
    private String pkId;
    private String contNo;
    //双人调查模式
    private String surveyModel;
    //是否新员工
    private String isNewEmployee;


    public String getMarId() {
        return marId;
    }

    public void setMarId(String marId) {
        this.marId = marId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getIsNewEmployee() {
        return isNewEmployee;
    }

    public void setIsNewEmployee(String isNewEmployee) {
        this.isNewEmployee = isNewEmployee;
    }

    public String getSurveyModel() {
        return surveyModel;
    }

    public void setSurveyModel(String surveyModel) {
        this.surveyModel = surveyModel;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public LmtSurveyReportBasicInfo getLmtSurveyReportBasicInfo() {
        return lmtSurveyReportBasicInfo;
    }

    public void setLmtSurveyReportBasicInfo(LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        this.lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfo;
    }

    public LmtSurveyConInfo getLmtSurveyConInfo() {
        return lmtSurveyConInfo;
    }

    public void setLmtSurveyConInfo(LmtSurveyConInfo lmtSurveyConInfo) {
        this.lmtSurveyConInfo = lmtSurveyConInfo;
    }

    public LmtSurveyReportComInfo getLmtSurveyReportComInfo() {
        return lmtSurveyReportComInfo;
    }

    public void setLmtSurveyReportComInfo(LmtSurveyReportComInfo lmtSurveyReportComInfo) {
        this.lmtSurveyReportComInfo = lmtSurveyReportComInfo;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPerfer() {
        return perfer;
    }

    public void setPerfer(String perfer) {
        this.perfer = perfer;
    }

    public Map<String, Object> getCondition() {
        return condition;
    }

    public void setCondition(Map<String, Object> condition) {
        this.condition = condition;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }
}
