/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncome
 * @类描述: iqp_dis_asset_income数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-12 09:54:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_dis_asset_income")
public class IqpDisAssetIncome extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 月收入表主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "INCOME_PK")
	private String incomePk;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	@Column(name = "APPT_CODE", unique = false, nullable = true, length = 32)
	private String apptCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 资产折算收入类型 **/
	@Column(name = "DISCOUNT_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String discountAssetType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 客户经理认定收入金额 **/
	@Column(name = "MANAGER_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	@Column(name = "BRANCH_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	@Column(name = "HEAD_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal headExIncome;
	
	/** 借款人月收入 **/
	@Column(name = "MEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mearn;
	
	/** 配偶月收入 **/
	@Column(name = "SPOUSE_MEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spouseMearn;
	
	/** 所有共同借款人月收入小计 **/
	@Column(name = "COMMON_MEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commonMearn;
	
	/** 数据来源 **/
	@Column(name = "DATA_SOURCE", unique = false, nullable = true, length = 20)
	private String dataSource;
	
	/** 收入来源 **/
	@Column(name = "INCOME_SOUR", unique = false, nullable = true, length = 20)
	private String incomeSour;
	
	/** 分析时间段 **/
	@Column(name = "ANALY_TIME", unique = false, nullable = true, length = 40)
	private String analyTime;
	
	/** 销售额(元) **/
	@Column(name = "SALE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal saleAmt;
	
	/** 推算年销售额(元) **/
	@Column(name = "YEAR_SALE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearSaleAmt;
	
	/** 利润率(%) **/
	@Column(name = "PROFIT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal profitRate;
	
	/** 年净利润(元) **/
	@Column(name = "YEAR_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearProfit;
	
	/** 小计(元) **/
	@Column(name = "SUBTOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subtotal;
	
	/** 调查核实情况说明 **/
	@Column(name = "INDGT_EXPL", unique = false, nullable = true, length = 500)
	private String indgtExpl;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param incomePk
	 */
	public void setIncomePk(String incomePk) {
		this.incomePk = incomePk;
	}
	
    /**
     * @return incomePk
     */
	public String getIncomePk() {
		return this.incomePk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param discountAssetType
	 */
	public void setDiscountAssetType(String discountAssetType) {
		this.discountAssetType = discountAssetType;
	}
	
    /**
     * @return discountAssetType
     */
	public String getDiscountAssetType() {
		return this.discountAssetType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return managerIncome
     */
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return branchExIncome
     */
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return headExIncome
     */
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param mearn
	 */
	public void setMearn(java.math.BigDecimal mearn) {
		this.mearn = mearn;
	}
	
    /**
     * @return mearn
     */
	public java.math.BigDecimal getMearn() {
		return this.mearn;
	}
	
	/**
	 * @param spouseMearn
	 */
	public void setSpouseMearn(java.math.BigDecimal spouseMearn) {
		this.spouseMearn = spouseMearn;
	}
	
    /**
     * @return spouseMearn
     */
	public java.math.BigDecimal getSpouseMearn() {
		return this.spouseMearn;
	}
	
	/**
	 * @param commonMearn
	 */
	public void setCommonMearn(java.math.BigDecimal commonMearn) {
		this.commonMearn = commonMearn;
	}
	
    /**
     * @return commonMearn
     */
	public java.math.BigDecimal getCommonMearn() {
		return this.commonMearn;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	
    /**
     * @return dataSource
     */
	public String getDataSource() {
		return this.dataSource;
	}
	
	/**
	 * @param incomeSour
	 */
	public void setIncomeSour(String incomeSour) {
		this.incomeSour = incomeSour;
	}
	
    /**
     * @return incomeSour
     */
	public String getIncomeSour() {
		return this.incomeSour;
	}
	
	/**
	 * @param analyTime
	 */
	public void setAnalyTime(String analyTime) {
		this.analyTime = analyTime;
	}
	
    /**
     * @return analyTime
     */
	public String getAnalyTime() {
		return this.analyTime;
	}
	
	/**
	 * @param saleAmt
	 */
	public void setSaleAmt(java.math.BigDecimal saleAmt) {
		this.saleAmt = saleAmt;
	}
	
    /**
     * @return saleAmt
     */
	public java.math.BigDecimal getSaleAmt() {
		return this.saleAmt;
	}
	
	/**
	 * @param yearSaleAmt
	 */
	public void setYearSaleAmt(java.math.BigDecimal yearSaleAmt) {
		this.yearSaleAmt = yearSaleAmt;
	}
	
    /**
     * @return yearSaleAmt
     */
	public java.math.BigDecimal getYearSaleAmt() {
		return this.yearSaleAmt;
	}
	
	/**
	 * @param profitRate
	 */
	public void setProfitRate(java.math.BigDecimal profitRate) {
		this.profitRate = profitRate;
	}
	
    /**
     * @return profitRate
     */
	public java.math.BigDecimal getProfitRate() {
		return this.profitRate;
	}
	
	/**
	 * @param yearProfit
	 */
	public void setYearProfit(java.math.BigDecimal yearProfit) {
		this.yearProfit = yearProfit;
	}
	
    /**
     * @return yearProfit
     */
	public java.math.BigDecimal getYearProfit() {
		return this.yearProfit;
	}
	
	/**
	 * @param subtotal
	 */
	public void setSubtotal(java.math.BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
    /**
     * @return subtotal
     */
	public java.math.BigDecimal getSubtotal() {
		return this.subtotal;
	}
	
	/**
	 * @param indgtExpl
	 */
	public void setIndgtExpl(String indgtExpl) {
		this.indgtExpl = indgtExpl;
	}
	
    /**
     * @return indgtExpl
     */
	public String getIndgtExpl() {
		return this.indgtExpl;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}