package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GuarMortgageManageApp;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @className GuarMortgageManageAppBiz
 * @Description 抵押登记审批流程（本地机构放款后抵押模式）
 * @author zhengfq
 * @Date 2020/05/19
 */
@Service
public class DBGL03BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DBGL03BizService.class);//定义log

    @Autowired
    private GuarMortgageManageAppService guarMortgageManageAppService;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String logPrefix = resultInstanceDto.getFlowName()+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto.toString());
                //guarMortgageManageAppService.handleBusinessAfterStart(serno);
                GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppService.selectByPrimaryKey(serno);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                guarMortgageManageAppService.handleBusinessAfterStart(serno);
                if ("114_17".equals(currNodeId)){
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(serno);
                    //根据担保合同编号查询担保合同信息
                    GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarMortgageManageApp.getGuarContNo());

                    if (grtGuarCont!=null){
                        //客户编号取担保合同客户编号
                        centralFileTaskdto.setCusId(grtGuarCont.getCusId());
                        //客户名称取担保合同客户名称
                        centralFileTaskdto.setCusName(grtGuarCont.getCusName());
                    }
                    centralFileTaskdto.setTraceId(guarMortgageManageApp.getMainContNo());
                    centralFileTaskdto.setBizType(resultInstanceDto.getBizType());
                    centralFileTaskdto.setInputId(guarMortgageManageApp.getInputId());
                    centralFileTaskdto.setInputBrId(guarMortgageManageApp.getInputBrId());
                    //档案任务操作类型 02--非纯指令
                    centralFileTaskdto.setOptType("02");
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    //档案任务类型 04--暂存及派发
                    centralFileTaskdto.setTaskType("04");
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    centralFileTaskService.insertSelective(centralFileTaskdto);
                }

                if ("114_22".equals(currNodeId) || "114_23".equals(currNodeId)){
                    //114_22 集中作业合同初审岗、114_23 集中作业零售合同审批岗,推送用印系统
                    try {
                        String cusName = "";
                        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarMortgageManageApp.getGuarContNo());

                        if (grtGuarCont!=null){
                            cusName = grtGuarCont.getCusName();
                        }
                        cmisBizXwCommonService.sendYk(resultInstanceDto.getCurrentUserId(),serno,cusName);
                        log.info("推送用印系统成功:【{}】", serno);
                    } catch (Exception e) {
                        log.info("推送用印系统异常:【{}】", e.getMessage());
                    }
                }
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto);
                guarMortgageManageAppService.handleBusinessAfterEnd(serno);
                //调用影像平台交易，为该笔业务影像批量打上已审核标识。
                sendImage(resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarMortgageManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarMortgageManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                guarMortgageManageAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DBGL03.equals(flowCode);
    }

    /**
     * 推送影像审批信息
     * 审核人取【集中作业合同初审岗】【集中作业合同复审岗】或者【集中作业零售合同初审岗】【集中作业零售放款审批岗】的员工号
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());

        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";

        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();

            if ("114_7".equals(nodeId) || "114_8".equals(nodeId) || "114_22".equals(nodeId) || "114_23".equals(nodeId)
                    || "114_29".equals(nodeId) || "114_30".equals(nodeId)){
                //如果是【集中作业合同初审岗】【集中作业合同复审岗】或者【集中作业零售合同初审岗】【集中作业零售放款审批岗】
                String userId = resultCommentDto.getUserId();

                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }
        approveUserId = approveUserId.substring(1);
        String topOutSystemCodes = "GRXFDKSX;GRXFDKCZDY";
        bizCommonService.sendImage(resultInstanceDto.getBizId(),topOutSystemCodes,approveUserId);
    }
}