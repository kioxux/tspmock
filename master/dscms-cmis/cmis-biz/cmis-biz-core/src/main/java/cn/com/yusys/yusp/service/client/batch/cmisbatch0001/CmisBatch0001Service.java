package cn.com.yusys.yusp.service.client.batch.cmisbatch0001;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查詢[调度运行管理]信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBatch0001Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0001Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查詢[调度运行管理]信息
     *
     * @param cmisbatch0001ReqDto
     * @return
     */
    @Transactional
    public Cmisbatch0001RespDto cmisbatch0001(Cmisbatch0001ReqDto cmisbatch0001ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001ReqDto));
        ResultDto<Cmisbatch0001RespDto> cmisbatch0001ResultDto = cmisBatchClientService.cmisbatch0001(cmisbatch0001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001ResultDto));

        String cmisbatch0001Code = Optional.ofNullable(cmisbatch0001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisbatch0001Meesage = Optional.ofNullable(cmisbatch0001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Cmisbatch0001RespDto cmisbatch0001RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0001ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbatch0001RespDto = cmisbatch0001ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, cmisbatch0001Code, cmisbatch0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value);
        return cmisbatch0001RespDto;
    }
}
