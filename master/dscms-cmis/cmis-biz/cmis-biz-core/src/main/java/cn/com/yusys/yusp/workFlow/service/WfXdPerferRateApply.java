package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.domain.PerferRateApplyInfo;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PerferRateApplyInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @功能描述:小贷优惠利率申请逻辑
 * @param
 * @author: 王玉坤
 * @date: 19:29 2021/4/13
 * @return:
 */
@Service
public class WfXdPerferRateApply implements ClientBizInterface {
	private final Logger log = LoggerFactory.getLogger(WfXdPerferRateApply.class);
	@Autowired
	private AmqpTemplate amqpTemplate;

	@Autowired
	private PerferRateApplyInfoService perferRateApplyInfoService;

	private PerferRateApplyInfo perferRateApplyInfo = null;

	@Override
	public void bizOp(ResultInstanceDto resultInstanceDto) {
		String currentOpType = resultInstanceDto.getCurrentOpType();
		String grtSerno = resultInstanceDto.getBizId();
		log.info("后业务处理类型" + currentOpType);
		try {
			perferRateApplyInfo = perferRateApplyInfoService.selectByPrimaryKey(grtSerno);
			if (OpType.STRAT.equals(currentOpType)) {
				log.info("担保变更申请" + grtSerno + "流程发起操作，流程参数" + resultInstanceDto);
			} else if (OpType.RUN.equals(currentOpType)) {
				log.info("担保变更申请" + grtSerno + "流程提交操作，流程参数" + resultInstanceDto);
				perferRateApplyInfo.setApproveoveStatus("111");
				perferRateApplyInfoService.update(perferRateApplyInfo);
			} else if (OpType.JUMP.equals(currentOpType)) {
				log.info("担保变更申请" + grtSerno + "流程跳转操作，流程参数" + resultInstanceDto);
			} else if (OpType.END.equals(currentOpType)) {
				log.info("担保变更申请" + grtSerno + "流程结束操作，流程参数" + resultInstanceDto);
				//针对流程到办结节点，进行以下处理
				perferRateApplyInfo.setApproveoveStatus("997");
				perferRateApplyInfoService.update(perferRateApplyInfo);
			} else {
				log.warn("担保变更申请" + grtSerno + "未知操作:" + resultInstanceDto);
			}
		} catch (Exception e) {
			log.error("后业务处理失败", e);
			try {
				BizCommonUtils bizCommonUtils = new BizCommonUtils();
				bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
			} catch (Exception e1) {
				log.error("发送异常消息失败", e1);
			}
		}
	}
	// 判定流程能否进行业务处理
	@Override
	public boolean should(ResultInstanceDto resultInstanceDto) {
		String flowCode = resultInstanceDto.getFlowCode();
		return "XD_PERFER_RATE_APPLY111".equals(flowCode);
	}
}
