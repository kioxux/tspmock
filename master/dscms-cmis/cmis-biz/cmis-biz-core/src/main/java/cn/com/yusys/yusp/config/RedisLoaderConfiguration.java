package cn.com.yusys.yusp.config;

import cn.com.yusys.yusp.command.RedisCacheStartLoader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 启动时加载数据到Redis缓存配置
 *
 * @author yangzai
 * @since 2021/4/23
 **/
@Configuration
@ConditionalOnClass({StringRedisTemplate.class})
@ConditionalOnProperty(name = "yusp.cache.start-loader.enabled", matchIfMissing = true)
public class RedisLoaderConfiguration {

    @Bean
    public RedisCacheStartLoader redisCacheStartRunner() {
        return new RedisCacheStartLoader();
    }

}