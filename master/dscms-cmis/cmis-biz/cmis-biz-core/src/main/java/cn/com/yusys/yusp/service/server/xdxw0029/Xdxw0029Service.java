package cn.com.yusys.yusp.service.server.xdxw0029;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0029.req.Xdxw0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0029.resp.Xdxw0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0029Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-17 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0029Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0029Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;

    /**
     * 根据客户号查询现有融资总额、总余额、担保方式
     *
     * @param xdxw0029DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0029DataRespDto getXdxw0029(Xdxw0029DataReqDto xdxw0029DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataReqDto));
        Xdxw0029DataRespDto xdxw0029DataRespDto = new Xdxw0029DataRespDto();
        String cusId = xdxw0029DataReqDto.getCusNo();
        try {
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0029.resp.List> list = new ArrayList<>();
            if (StringUtils.isEmpty(cusId)) {
                throw BizException.error(null, EcbEnum.ECB010014.key, EcbEnum.ECB010014.value);
            } else {
                list = accLoanMapper.getXdxw0029(cusId);
                for(cn.com.yusys.yusp.dto.server.xdxw0029.resp.List list1:list){
                    //担保方式
                    String assureMeans = list1.getAssureMeans();
                    //担保方式名称
                    String assusreMeansName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY",assureMeans);
                    list1.setAssureMeans(assusreMeansName);
                }
            }
            xdxw0029DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataReqDto));
        return xdxw0029DataRespDto;
    }

}
