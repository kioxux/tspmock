/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.dto.DocReceiveInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocReadAppInfo;
import cn.com.yusys.yusp.service.DocReadAppInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadAppInfoResource
 * @类描述: #资源类
 * @功能描述: 文档调阅入口类
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 17:00:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docreadappinfo")
public class DocReadAppInfoResource {
    @Autowired
    private DocReadAppInfoService docReadAppInfoService;
	
    /**
     * @函数名称:index
     * @函数描述:分页查询
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<DocReadAppInfo>> index(@RequestBody QueryModel queryModel) {
        List<DocReadAppInfo> list = docReadAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocReadAppInfo>>(list);
    }
    /**
     * @函数名称:index
     * @函数描述:分页查询 - 查询接收中和归还中的档案数据
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @PostMapping("/querydocreceiveinfo")
    protected ResultDto<List<DocReceiveInfoDto>> queryDocReceiveInfo(@RequestBody QueryModel queryModel) {
        List<DocReceiveInfoDto> list = docReadAppInfoService.queryDocReceiveInfo(queryModel);
        return new ResultDto<List<DocReceiveInfoDto>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询详情
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/detail")
    protected ResultDto<DocReadAppInfo> show(@RequestBody DocReadAppInfo param) {
        DocReadAppInfo docReadAppInfo = docReadAppInfoService.selectByPrimaryKey(param.getDraiSerno());
        return new ResultDto<DocReadAppInfo>(docReadAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:新增保存
     * @参数与返回说明: 保存对象
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<DocReadAppInfo> create(@RequestBody DocReadAppInfo docReadAppInfo) throws URISyntaxException {
        docReadAppInfoService.insert(docReadAppInfo);
        return new ResultDto<DocReadAppInfo>(docReadAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:修改
     * @参数与返回说明: 保存对象
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocReadAppInfo docReadAppInfo) throws URISyntaxException {
        int result = docReadAppInfoService.update(docReadAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody DocReadAppInfo docReadAppInfo) {
        int result = docReadAppInfoService.deleteByPrimaryKey(docReadAppInfo.getDraiSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docReadAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/applyDelivery/{id}")
    protected ResultDto<Boolean> applyDelivery(@PathVariable String id) {
        docReadAppInfoService.applyDelivery(id);
        return new ResultDto<Boolean>(true);
    }
}
