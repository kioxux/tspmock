/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrAccpCont;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.domain.CtrDiscCont;
import cn.com.yusys.yusp.domain.IqpCvrgApp;
import cn.com.yusys.yusp.service.CtrCvrgContService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrCvrgContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 16:08:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrcvrgcont")
public class CtrCvrgContResource {
    @Autowired
    private CtrCvrgContService ctrCvrgContService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrCvrgCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrCvrgCont> list = ctrCvrgContService.selectAll(queryModel);
        return new ResultDto<List<CtrCvrgCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrCvrgCont>> index(QueryModel queryModel) {
        List<CtrCvrgCont> list = ctrCvrgContService.selectByModel(queryModel);
        return new ResultDto<List<CtrCvrgCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrCvrgCont> show(@PathVariable("pkId") String pkId) {
        CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrCvrgCont>(ctrCvrgCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrCvrgCont> create(@RequestBody CtrCvrgCont ctrCvrgCont) throws URISyntaxException {
        ctrCvrgContService.insert(ctrCvrgCont);
        return new ResultDto<CtrCvrgCont>(ctrCvrgCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口.签订
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrCvrgCont ctrCvrgCont) throws URISyntaxException {
        int result = ctrCvrgContService.update(ctrCvrgCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logout
     * @函数描述:注销
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("保函合同注销")
    @PostMapping("/logout")
    protected ResultDto<Map> logout(@RequestBody Map params) throws URISyntaxException, ParseException {
        Map rtnData = ctrCvrgContService.logout(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrCvrgContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrCvrgContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:deleteCtrCvrgContinfo
     * @函数描述:通过主键对保函合同进行逻辑删除
     * @参数与返回说明: iqpSerno
     * @算法描述: 对数据类型进行更新，并修改明细列表中的数据类型
     */
    @GetMapping("/deleteCtrCvrgContinfo/{serno}")
    protected ResultDto<Map> deleteCtrCvrgContinfo(@PathVariable("serno") String iqpSerno) {
        System.out.println("=======" + iqpSerno);
        Map map = ctrCvrgContService.deleteCtrCvrgContinfo(iqpSerno);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:待签定列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrCvrgCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrCvrgCont> list = ctrCvrgContService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrCvrgCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrCvrgCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrCvrgCont> list = ctrCvrgContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrCvrgCont>>(list);
    }

    /**
     * @函数名称:showdetial
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:map
     * @算法描述:
     */
    @ApiOperation("保函合同查看")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        CtrCvrgCont temp = new CtrCvrgCont();
        CtrCvrgCont studyDemo = ctrCvrgContService.selectByIqpSerno((String)map.get("bizSerno"));
        if (studyDemo != null) {
            resultDto.setCode(0);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(00);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrCvrgCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrCvrgCont> list = ctrCvrgContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrCvrgCont>>(list);
    }

    /**
     * @函数名称:queryCtrCvrgContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrcvrgcontdatabyserno")
    protected ResultDto<CtrCvrgCont> queryCtrCvrgContDataBySerno(@RequestBody String serno) {
        CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByIqpSerno(serno);
        return new ResultDto<CtrCvrgCont>(ctrCvrgCont);
    }

    /**
     * @函数名称:queryCtrCvrgContDataByContNo
     * @函数描述:根据合同号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrcvrgcontdatabycontno")
    protected ResultDto<CtrCvrgCont> queryCtrCvrgContDataByContNo(@RequestBody String contNo) {
        CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByContNo(contNo);
        return new ResultDto<CtrCvrgCont>(ctrCvrgCont);
    }
}
