package cn.com.yusys.yusp.web.server.xddh0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0008.Xddh0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:还款计划列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDH0008:还款计划列表查询")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0008Resource.class);

    @Autowired
    private Xddh0008Service xddh0008Service;

    /**
     * 交易码：xddh0008
     * 交易描述：还款计划列表查询
     *
     * @param xddh0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款计划列表查询")
    @PostMapping("/xddh0008")
    protected @ResponseBody
    ResultDto<Xddh0008DataRespDto> xddh0008(@Validated @RequestBody Xddh0008DataReqDto xddh0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataReqDto));
        Xddh0008DataRespDto xddh0008DataRespDto = new Xddh0008DataRespDto();// 响应Dto:还款计划列表查询
        ResultDto<Xddh0008DataRespDto> xddh0008DataResultDto = new ResultDto<>();
        try {
            // 从xddh0008DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataReqDto));
            xddh0008DataRespDto = xddh0008Service.getRepayList(xddh0008DataReqDto).getData();
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataRespDto));
            // 封装xddh0008DataResultDto中正确的返回码和返回信息
            xddh0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, e.getMessage());
            // 封装xddh0008DataResultDto中异常返回码和返回信息
            xddh0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0008DataRespDto到xddh0008DataResultDto中
        xddh0008DataResultDto.setData(xddh0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataResultDto));
        return xddh0008DataResultDto;
    }
}
