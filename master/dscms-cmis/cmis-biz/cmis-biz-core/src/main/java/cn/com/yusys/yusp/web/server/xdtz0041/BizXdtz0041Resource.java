package cn.com.yusys.yusp.web.server.xdtz0041;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0041.req.Xdtz0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0041.Xdtz0041Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0041:根据客户号前往信贷查找房贷借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0041Resource.class);

    @Autowired
    private Xdtz0041Service xdtz0041Service;

    /**
     * 交易码：xdtz0041
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param xdtz0041DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0041")
    protected @ResponseBody
    ResultDto<Xdtz0041DataRespDto> xdtz0041(@Validated @RequestBody Xdtz0041DataReqDto xdtz0041DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataReqDto));
        Xdtz0041DataRespDto xdtz0041DataRespDto = null;// 响应Dto:根据客户号前往信贷查找房贷借据信息
        ResultDto<Xdtz0041DataRespDto> xdtz0041DataResultDto = new ResultDto<>();
        // 从xdtz0041DataReqDto获取业务值进行业务逻辑处理
        String cusId = xdtz0041DataReqDto.getCusId();//客户号
        try {
            // 调用xdtz0041Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, cusId);
            xdtz0041DataRespDto = Optional.ofNullable(xdtz0041Service.getHouseLoanByCusId(cusId))
                    .orElse(new Xdtz0041DataRespDto(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, new BigDecimal(0L)));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataRespDto));
            // 封装xdtz0041DataResultDto中正确的返回码和返回信息
            xdtz0041DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0041DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, e.getMessage());
            // 封装xdtz0041DataResultDto中异常返回码和返回信息
            xdtz0041DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0041DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0041DataRespDto到xdtz0041DataResultDto中
        xdtz0041DataResultDto.setData(xdtz0041DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataResultDto));
        return xdtz0041DataResultDto;
    }
}
