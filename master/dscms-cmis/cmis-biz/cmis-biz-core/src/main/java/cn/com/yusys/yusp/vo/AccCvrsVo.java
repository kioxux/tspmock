package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "保函台账列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccCvrsVo {

    /*
     借据编号
      */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    保函金额
     */
    @ExcelField(title = "保函金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal guarantAmt;

    /*
    币种
     */
    @ExcelField(title = "币种", viewLength = 20, dictCode = "STD_ZB_CUR_TYP")
    private String curType;

    /*
    保证金比例
     */
    @ExcelField(title = "保证金比例", viewLength = 20)
    private java.math.BigDecimal bailPerc;

    /*
    担保方式
     */
    @ExcelField(title = "担保方式", viewLength = 20, dictCode = "STD_ZB_GUAR_WAY")
    private String guarMode;

    /*
    生效日期
     */
    @ExcelField(title = "生效日期", viewLength = 20)
    private String inureDate;

    /*
    失效日期
     */
    @ExcelField(title = "失效日期", viewLength = 20)
    private String invlDate;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
     */
    @ExcelField(title = "台账状态", viewLength = 20, dictCode = "STD_ACC_STATUS")
    private String accStatus;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getGuarantAmt() {
        return guarantAmt;
    }

    public void setGuarantAmt(BigDecimal guarantAmt) {
        this.guarantAmt = guarantAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(BigDecimal bailPerc) {
        this.bailPerc = bailPerc;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getInureDate() {
        return inureDate;
    }

    public void setInureDate(String inureDate) {
        this.inureDate = inureDate;
    }

    public String getInvlDate() {
        return invlDate;
    }

    public void setInvlDate(String invlDate) {
        this.invlDate = invlDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
