package cn.com.yusys.yusp.service.server.xdzc0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0020.req.Xdzc0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.resp.List;
import cn.com.yusys.yusp.dto.server.xdzc0020.resp.Xdzc0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import cn.com.yusys.yusp.service.AsplIoPoolDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:出入池详情查询
 *
 * @Author xs
 * @Date 2021/6/11 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0020Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0020Service.class);

    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    /**
     * 交易码：xdzc0020
     * 交易描述：
     * 出入池详情查询
     *
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0020DataRespDto xdzc0020Service(Xdzc0020DataReqDto xdzc0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value);
        Xdzc0020DataRespDto xdzc0020DataRespDto = new Xdzc0020DataRespDto();
        String serno = xdzc0020DataReqDto.getSerno();//出入池流水编号
        try {
            java.util.List<List> list = asplIoPoolDetailsService.selectAssetIoInfoBySerno(serno);
            xdzc0020DataRespDto.setTotalSize(list.size());
            xdzc0020DataRespDto.setList(list);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value);
        return xdzc0020DataRespDto;
    }
}
