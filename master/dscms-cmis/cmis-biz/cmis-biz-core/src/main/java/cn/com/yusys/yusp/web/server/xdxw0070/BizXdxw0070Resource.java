package cn.com.yusys.yusp.web.server.xdxw0070;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0070.req.Xdxw0070DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0070.resp.Xdxw0070DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0070.Xdxw0070Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:授信侧面调查信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0070:授信侧面调查信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0070Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0070Resource.class);

    @Autowired
    private Xdxw0070Service xdxw0070Service;

    /**
     * 交易码：xdxw0070
     * 交易描述：授信侧面调查信息查询
     *
     * @param xdxw0070DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授信侧面调查信息查询")
    @PostMapping("/xdxw0070")
    protected @ResponseBody
    ResultDto<Xdxw0070DataRespDto> xdxw0070(@Validated @RequestBody Xdxw0070DataReqDto xdxw0070DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070DataReqDto));
        Xdxw0070DataRespDto xdxw0070DataRespDto = new Xdxw0070DataRespDto();// 响应Dto:授信侧面调查信息查询
        ResultDto<Xdxw0070DataRespDto> xdxw0070DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0070DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0070DataReqDto));
            xdxw0070DataRespDto = xdxw0070Service.xdxw0070(xdxw0070DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0070DataRespDto));
            // 封装xdxw0070DataResultDto中正确的返回码和返回信息
            xdxw0070DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0070DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            // 封装xdxw0070DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdxw0070DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0070DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdxw0070DataRespDto到xdxw0070DataResultDto中
        xdxw0070DataResultDto.setData(xdxw0070DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070DataRespDto));
        return xdxw0070DataResultDto;
    }
}
