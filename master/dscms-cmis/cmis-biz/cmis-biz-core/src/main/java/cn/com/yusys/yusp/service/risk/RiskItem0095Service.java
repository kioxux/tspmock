package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class RiskItem0095Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0095Service.class);
    @Autowired
    private OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private  PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private  PvpAccpAppService pvpAccpAppService;


    public RiskResultDto riskItem0095 (QueryModel queryModel) {

        RiskResultDto riskResultDto = new RiskResultDto();
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        String serno = queryModel.getCondition().get("bizId").toString();//业务流水号
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
        PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
        String replyNo ="";//初始值
        if(pvpLoanApp !=null){
             replyNo = pvpLoanApp.getReplyNo();//批复编号
        }else if(pvpEntrustLoanApp !=null){
            replyNo = pvpEntrustLoanApp.getReplyNo();//批复编号
        }else if(pvpAccpApp != null){
            replyNo = pvpAccpApp.getReplyNo();//批复编号
        }
        if(!("".equals(replyNo)||replyNo == null)){
            BigDecimal toTalAmt = new BigDecimal("0");
            queryModel.addCondition("replySerno",replyNo);
            queryModel.addCondition("approveStatus","997");
            queryModel.setSort("updateTime desc");
            //用信备案表
            List<OtherRecordSpecialLoanApp> otherRecordSpecialLoanApps = otherRecordSpecialLoanAppService.selectByModel(queryModel);
            if(otherRecordSpecialLoanApps.size()==0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("不存在通过的用信审核备案");
                return riskResultDto;
            }
        }

        return riskResultDto;
    }
}
