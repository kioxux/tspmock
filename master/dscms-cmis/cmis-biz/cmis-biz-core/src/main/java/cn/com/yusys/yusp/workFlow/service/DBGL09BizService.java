package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GuarMortgageManageApp;
import cn.com.yusys.yusp.domain.GuarMortgageManageRel;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.GuarMortgageManageRelDto;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @className GuarMortgageManageAppBiz
 * @Description 抵押注销审批流程（本地机构）
 * @author zhengfq
 * @Date 2020/05/19
 */
@Service
public class DBGL09BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DBGL09BizService.class);//定义log

    @Autowired
    private GuarMortgageManageAppService guarMortgageManageAppService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private GuarMortgageManageRelService guarMortgageManageRelService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String logPrefix = resultInstanceDto.getFlowName()+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto);
                guarMortgageManageAppService.handleBusinessAfterStart(serno);

                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if ("115_7".equals(currNodeId) && "115_4".equals(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId())){
                    if (!resultInstanceDto.getParam().containsKey("isAutoCommit") || !CmisBizConstants.STD_ZB_YES_NO_Y.equals(resultInstanceDto.getParam().get("isAutoCommit"))){
                        log.info("流水号【"+serno+"】,当前提交节点是115_7，下一提交节点是115_4，生成档案任务开始");
                        createCentralFileTask(serno,serno,resultInstanceDto,resultInstanceDto.getBizType(),CmisBizConstants.STD_FILE_TASK_TYPE_03);
                        log.info("流水号【"+serno+"】,当前提交节点是115_7，下一提交节点是115_4，生成档案任务结束");
                    }
                }

                if ("115_5".equals(currNodeId) || "115_8".equals(currNodeId)){
                    //集中作业权证注销初审岗审批后推送用印系统
                    try {
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("serno",serno);
                        List<GuarMortgageManageRel> guarMortgageManageRels = guarMortgageManageRelService.selectAll(queryModel);
                        //押品所有权人名称
                        String guarCusName = "";

                        if (CollectionUtils.isNotEmpty(guarMortgageManageRels)){
                            guarCusName = guarMortgageManageRels.get(0).getGuarCusName();
                        }

                        cmisBizXwCommonService.sendYk(resultInstanceDto.getCurrentUserId(),serno,guarCusName);
                        log.info("推送用印系统成功:【{}】", serno);
                    } catch (Exception e) {
                        log.info("推送用印系统异常:【{}】", e.getMessage());
                    }
                }
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto);
                guarMortgageManageAppService.handleBusinessAfterEnd(serno);

                GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppService.selectByPrimaryKey(serno);
                String isRepayRemoveGuar = guarMortgageManageApp.getIsRepayRemoveGuar();

                if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(isRepayRemoveGuar)){
                    log.info("流水号【"+serno+"】,集中作业权证注销复审岗注销任务完成后，集中作业中心档案池生成一条纯指令对应的档案接受任务开始");
                    createCentralFileTask(serno,serno,resultInstanceDto,CmisBizConstants.STD_BIZ_SUB_TYPE_HKJJY,CmisBizConstants.STD_FILE_TASK_TYPE_01);
                    log.info("流水号【"+serno+"】,集中作业权证注销复审岗注销任务完成后，集中作业中心档案池生成一条纯指令对应的档案接受任务结束");
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarMortgageManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    guarMortgageManageAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto);
                guarMortgageManageAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DBGL09.equals(flowCode);
    }

    /**
     * 生成集中作业档案池任务
     * @param serno
     * @param taskSerno
     * @param resultInstanceDto
     * @param bizType
     */
    public void createCentralFileTask(String serno,String taskSerno,ResultInstanceDto resultInstanceDto,String bizType,String taskType){
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(taskSerno);
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppService.selectByPrimaryKey(serno);

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<GuarMortgageManageRel> guarMortgageManageRels = guarMortgageManageRelService.selectAll(queryModel);

        if (CollectionUtils.isNotEmpty(guarMortgageManageRels)){
            GuarMortgageManageRel guarMortgageManageRel = guarMortgageManageRels.get(0);
            //客户编号取押品所有人客户编号
            centralFileTaskdto.setCusId(guarMortgageManageRel.getGuarCusId());
            //客户名称取押品所有人客户名称
            centralFileTaskdto.setCusName(guarMortgageManageRel.getGuarCusName());
            //全局流水号取核心担保编号
            centralFileTaskdto.setTraceId(guarMortgageManageRel.getCoreGuarantyNo());
        }

        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(guarMortgageManageApp.getInputId());
        centralFileTaskdto.setInputBrId(guarMortgageManageApp.getInputBrId());

        //档案任务操作类型 01--纯指令
        centralFileTaskdto.setOptType(CmisBizConstants.STD_OPT_TYPE_01);
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        //档案任务类型
        centralFileTaskdto.setTaskType(taskType);
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急

        centralFileTaskService.insertSelective(centralFileTaskdto);
    }
}

