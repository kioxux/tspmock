/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.LmtSubPrdCtrLoanContRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtSubPrdCtrLoanContRelMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubPrdCtrLoanContRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-25 21:20:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSubPrdCtrLoanContRelService {

    @Autowired
    private LmtSubPrdCtrLoanContRelMapper lmtSubPrdCtrLoanContRelMapper;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CtrDiscContService ctrDiscContService;
    @Autowired
    private CtrTfLocContService ctrTfLocContService;
    @Autowired
    private CtrAccpContService ctrAccpContService;
    @Autowired
    private CtrCvrgContService ctrCvrgContService;
    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSubPrdCtrLoanContRel selectByPrimaryKey(String pkId) {
        return lmtSubPrdCtrLoanContRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtSubPrdCtrLoanContRel> selectAll(QueryModel model) {
        List<LmtSubPrdCtrLoanContRel> records = (List<LmtSubPrdCtrLoanContRel>) lmtSubPrdCtrLoanContRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtSubPrdCtrLoanContRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSubPrdCtrLoanContRel> list = lmtSubPrdCtrLoanContRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtSubPrdCtrLoanContRel record) {
        return lmtSubPrdCtrLoanContRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtSubPrdCtrLoanContRel record) {
        return lmtSubPrdCtrLoanContRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtSubPrdCtrLoanContRel record) {
        return lmtSubPrdCtrLoanContRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtSubPrdCtrLoanContRel record) {
        return lmtSubPrdCtrLoanContRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String subPrdSerno) {
        return lmtSubPrdCtrLoanContRelMapper.deleteByPrimaryKey(pkId, subPrdSerno);
    }

    /**
     * 查询诚易融关联合同
     *
     * @param subPrdSerno
     * @return
     */
    public List<LmtSubPrdCtrLoanContRelDto> selectBySubPrdSerno(String subPrdSerno) {
        List<LmtSubPrdCtrLoanContRelDto> result = new ArrayList<>();
        List<LmtSubPrdCtrLoanContRel> lmtSubPrdCtrLoanContRelList = lmtSubPrdCtrLoanContRelMapper.selectBySubPrdSerno(subPrdSerno);
        if (CollectionUtils.nonEmpty(lmtSubPrdCtrLoanContRelList)) {
            for (LmtSubPrdCtrLoanContRel lmtSubPrdCtrLoanContRel : lmtSubPrdCtrLoanContRelList) {
                LmtSubPrdCtrLoanContRelDto lmtSubPrdCtrLoanContRelDto = new LmtSubPrdCtrLoanContRelDto();
                //业务类型
                String bizType = lmtSubPrdCtrLoanContRel.getBizType();
                //合同编号
                String contNo = lmtSubPrdCtrLoanContRel.getContNo();
                lmtSubPrdCtrLoanContRelDto.setPkId(lmtSubPrdCtrLoanContRel.getPkId());
                lmtSubPrdCtrLoanContRelDto.setContNo(contNo);
                lmtSubPrdCtrLoanContRelDto.setBizType(bizType);
                lmtSubPrdCtrLoanContRelDto.setSubPrdSerno(subPrdSerno);
                if (CmisCommonConstants.STD_BUSI_TYPE_01.equals(bizType)) {
                    //最高额协议
                    CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
                    if(Objects.nonNull(ctrHighAmtAgrCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrHighAmtAgrCont.getAgrAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrHighAmtAgrCont.getAgrType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrHighAmtAgrCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrHighAmtAgrCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrHighAmtAgrCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrHighAmtAgrCont.getManagerBrId());
                    }
                } else if (CmisCommonConstants.STD_BUSI_TYPE_02.equals(bizType) || CmisCommonConstants.STD_BUSI_TYPE_04.equals(bizType) || CmisCommonConstants.STD_BUSI_TYPE_05.equals(bizType)) {
                    //普通贷款,贸易融资,福费廷合同
                    CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                    if(Objects.nonNull(ctrLoanCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrLoanCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrLoanCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrLoanCont.getContStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrLoanCont.getContEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrLoanCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrLoanCont.getManagerBrId());
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_03.equals(bizType)){
                    //贴现协议
                    CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(contNo);
                    if(Objects.nonNull(ctrDiscCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrDiscCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrDiscCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrDiscCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrDiscCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrDiscCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrDiscCont.getManagerBrId());
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_06.equals(bizType)){
                    //开证合同
                    CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByContNo(contNo);
                    if(Objects.nonNull(ctrTfLocCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrTfLocCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrTfLocCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrTfLocCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrTfLocCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrTfLocCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrTfLocCont.getManagerBrId());
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_07.equals(bizType)){
                    //银承合同
                    CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(contNo);
                    if(Objects.nonNull(ctrAccpCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrAccpCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrAccpCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrAccpCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrAccpCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrAccpCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrAccpCont.getManagerBrId());
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_08.equals(bizType)){
                    //保函合同
                    CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByContNo(contNo);
                    if(Objects.nonNull(ctrCvrgCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrCvrgCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrCvrgCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrCvrgCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrCvrgCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrCvrgCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrCvrgCont.getManagerBrId());
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_09.equals(bizType)){
                    //委托贷款合同
                    CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(contNo);
                    if(Objects.nonNull(ctrEntrustLoanCont)){
                        lmtSubPrdCtrLoanContRelDto.setContAmt(ctrEntrustLoanCont.getContAmt());
                        lmtSubPrdCtrLoanContRelDto.setCurType(ctrEntrustLoanCont.getCurType());
                        lmtSubPrdCtrLoanContRelDto.setContStartDate(ctrEntrustLoanCont.getStartDate());
                        lmtSubPrdCtrLoanContRelDto.setContEndDate(ctrEntrustLoanCont.getEndDate());
                        lmtSubPrdCtrLoanContRelDto.setManagerId(ctrEntrustLoanCont.getManagerId());
                        lmtSubPrdCtrLoanContRelDto.setManagerBrId(ctrEntrustLoanCont.getManagerBrId());
                    }
                }
                result.add(lmtSubPrdCtrLoanContRelDto);
            }
        }
        return result;
    }

    /**
     * @param param
     * @return Boolean
     * @author qw
     * @date 2021/8/9 17:12
     * @version 1.0.0
     * @desc 点击下一步新增合同关联
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Integer saveCtrLoanContDtoData(Map param) {
        LmtSubPrdCtrLoanContRel lmtSubPrdCtrLoanContRel = new LmtSubPrdCtrLoanContRel();
        lmtSubPrdCtrLoanContRel.setPkId(StringUtils.getUUID());
        lmtSubPrdCtrLoanContRel.setSubPrdSerno(param.get("subPrdSerno").toString());
        lmtSubPrdCtrLoanContRel.setContNo(param.get("contNo").toString());
        lmtSubPrdCtrLoanContRel.setBizType(param.get("bizType").toString());
        lmtSubPrdCtrLoanContRel.setCreateTime(DateUtils.getCurrDate());
        lmtSubPrdCtrLoanContRel.setOprType(CmisBizConstants.OPR_TYPE_01);
        User userInfo = SessionUtils.getUserInformation();
        lmtSubPrdCtrLoanContRel.setInputId(userInfo.getLoginCode());
        lmtSubPrdCtrLoanContRel.setInputBrId(userInfo.getOrg().getCode());
        return insert(lmtSubPrdCtrLoanContRel);
    }

    /**
     * 根据主键逻辑删除
     * @param pkId
     * @return
     */
    public int logicDelete(String pkId){
        int count = 0;
        LmtSubPrdCtrLoanContRel lmtSubPrdCtrLoanContRel = lmtSubPrdCtrLoanContRelMapper.selectByPrimaryKey(pkId);
        if(Objects.nonNull(lmtSubPrdCtrLoanContRel)){
            lmtSubPrdCtrLoanContRel.setOprType(CmisBizConstants.OPR_TYPE_02);
            User userInfo = SessionUtils.getUserInformation();
            lmtSubPrdCtrLoanContRel.setUpdId(userInfo.getLoginCode());
            lmtSubPrdCtrLoanContRel.setUpdBrId(userInfo.getOrg().getCode());
            lmtSubPrdCtrLoanContRel.setUpdateTime(DateUtils.getCurrDate());
            count = update(lmtSubPrdCtrLoanContRel);
        }
        return count;
    }
}
