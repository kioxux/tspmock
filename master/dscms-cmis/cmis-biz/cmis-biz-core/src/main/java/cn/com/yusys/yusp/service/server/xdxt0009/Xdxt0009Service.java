package cn.com.yusys.yusp.service.server.xdxt0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetIsXwUserDto;
import cn.com.yusys.yusp.dto.QueryAreaInfoDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.req.Xdxt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.resp.Xdxt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AreaAdminUserMapper;
import cn.com.yusys.yusp.repository.mapper.AreaOrgMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 业务逻辑类:客户经理是否为小微客户经理
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxt0009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0009Service.class);

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Resource
    private AreaAdminUserMapper areaAdminUserMapper;

    @Resource
    private AreaOrgMapper areaOrgMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * 客户经理是否为小微客户经理
     *
     * @param xdxt0009DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0009DataRespDto getXdxt0009(Xdxt0009DataReqDto xdxt0009DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataReqDto));
        Xdxt0009DataRespDto xdxt0009DataRespDto = new Xdxt0009DataRespDto();
        try {
            //客户经理编号
            String managerId = xdxt0009DataReqDto.getManagerId();
            //获取是否小微客户经理
            String isXWUser = Strings.EMPTY;
            //机构号
            String orgId = Strings.EMPTY;
            //机构号
            String managerBrName = Strings.EMPTY;
            logger.info("************XDXT0009*第一步根据客户经理编号【{}】查询客户经理信息开始", managerId);
            ResultDto<GetIsXwUserDto> getIsXwUserDtoResultDto = adminSmUserService.getIsXWUserByLoginCode(managerId);
            logger.info("************XDXT0009*第一步根据客户经理编号【{}】查询客户经理信息结束,客户信息【{}】", managerId, JSON.toJSONString(getIsXwUserDtoResultDto));
            if (ResultDto.success().getCode().equals(getIsXwUserDtoResultDto.getCode())) {
                GetIsXwUserDto getIsXwUserDto = getIsXwUserDtoResultDto.getData();
                if (Objects.nonNull(getIsXwUserDto)) {
                    isXWUser = getIsXwUserDto.getIsXWUser();
                }
            }
            xdxt0009DataRespDto.setIsMicroManager(isXWUser);
            logger.info("**************2*XDXT0009*第二步根据客户经理编号【{}】查询客户所在机构开始", managerId);
            ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
            if (ResultDto.success().getCode().equals(adminSmUserDtoResultDto.getCode())) {
                AdminSmUserDto adminSmUserDto = adminSmUserDtoResultDto.getData();
                if (Objects.nonNull(adminSmUserDto)) {
                    orgId = adminSmUserDto.getOrgId();
                }
            }
            logger.info("***************XDXT0009*第二步根据客户经理编号【{}】查询客户所在机构结束,机构信息【{}】", managerId, orgId);
            xdxt0009DataRespDto.setManagerBrId(orgId);
            //获取机构名称
            managerBrName = findManagerBrName(orgId);
            xdxt0009DataRespDto.setManagerBrName(managerBrName);

            //是否新客户经理标志
            String isNewManager = Strings.EMPTY;
            //直营团队类型
            String teamType = Strings.EMPTY;
            //调查类型
            String surveyType = Strings.EMPTY;
            //是否派遣员工
            String isDispatch = Strings.EMPTY;
            // 大额信用权限
            String isLargeCredit = Strings.EMPTY;
            //微贷用户信息
            logger.info("***************XDXT0009*第三步根据客户经理编号【{}】查询客户微贷信息开始", managerId);
            AreaAdminUser areaAdminUser = areaAdminUserMapper.selectByPrimaryKey(managerId);
            if (Objects.nonNull(areaAdminUser)) {
                isNewManager = PUBUtilTools.changeYesNo(areaAdminUser.getIsNewEmployee());
                teamType = areaAdminUser.getTeamType();
                surveyType = areaAdminUser.getSurveyMode();
                isDispatch = areaAdminUser.getIsDispatchEmployee();
                isLargeCredit = areaAdminUser.getIsLargeCredit();
            }
            logger.info("***************XDXT0009*第三步根据客户经理编号【{}】查询客户微贷信息结束,微贷信息【{}】", managerId, JSON.toJSONString(areaAdminUser));
            xdxt0009DataRespDto.setIsNewManager(isNewManager);
            xdxt0009DataRespDto.setTeamType(teamType);
            xdxt0009DataRespDto.setSurveyType(surveyType);
            xdxt0009DataRespDto.setIsDispatch(isDispatch);
            xdxt0009DataRespDto.setIsLargeCredit(isLargeCredit);
            //客户经理所属分中心
            String managerDept = Strings.EMPTY;
            //分中心负责人工号
            String deptChiefId = Strings.EMPTY;
            logger.info("***************XDXT0009*第四步根据客户机构号【{}】查询客户微贷区域机构关联表信息开始", orgId);
            QueryAreaInfoDto queryAreaInfoDto = areaOrgMapper.getAreaInfoByOrgId(orgId);
            if (Objects.nonNull(queryAreaInfoDto)) {
                managerDept = queryAreaInfoDto.getAreaName();
                deptChiefId = queryAreaInfoDto.getUserNo();
            }
            logger.info("***************XDXT0009*第四步根据客户机构号【{}】查询客户微贷区域机构关联表信息结束,微贷区域机构信息【{}】", orgId, JSON.toJSONString(queryAreaInfoDto));
            xdxt0009DataRespDto.setManagerDept(managerDept);
            xdxt0009DataRespDto.setDeptChiefId(deptChiefId);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataRespDto));
        return xdxt0009DataRespDto;
    }

    /***
     * 获取责任机构名称
     * **/
    public String  findManagerBrName(String managerBrId){
        String managerName = "";
        if(StringUtil.isNotEmpty(managerBrId)){
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(managerBrId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                managerName = adminSmOrgDto.getOrgName();
            }
        }
        return managerName;
    }
}
