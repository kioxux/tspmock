/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 72908
 * @创建时间: 2021-05-06 21:42:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PvpLoanAppRepayBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PvpLoanAppRepayBillRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PvpLoanAppRepayBillRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(PvpLoanAppRepayBillRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PvpLoanAppRepayBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PvpLoanAppRepayBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PvpLoanAppRepayBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);


    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据合同编号删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByBillNo(@Param("billNo") String billNo);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCtrContEntrustByParams
     * @方法描述: 通过查询报表编号查询子表的参数配置
     * @参数与返回说明:
     * @算法描述: 无
     * @return
     */
    List<RepayBillRelDto> selectRepayBillRelByParams2(HashMap<String, String> queryMap);
    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<RepayBillRelDto> selectRepayBillRelByParams(QueryModel model);

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账申请流水号查询关联的原借据
     * @参数与返回说明:
     * @算法描述: 无
     */
    PvpLoanAppRepayBillRel selectByPvpSerno(@Param("pvpSerno") String pvpSerno);
}