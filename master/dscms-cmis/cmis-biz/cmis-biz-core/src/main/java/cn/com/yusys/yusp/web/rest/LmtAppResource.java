/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.OtherAppDto;
import cn.com.yusys.yusp.service.LmtOtherAppRelService;
import cn.com.yusys.yusp.service.OtherGrtValueIdentyAppService;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.LmtAppService;

import javax.xml.namespace.QName;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 朱滋润
 * @创建时间: 2021-04-03 15:34:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapp")
public class LmtAppResource {
    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;

    @Autowired
    private LmtOtherAppRelService lmtOtherAppRelService;


	/**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtApp> list = lmtAppService.selectAll(queryModel);
        return new ResultDto<List<LmtApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<LmtApp>> index(@RequestBody QueryModel queryModel) {
        List<LmtApp> list = lmtAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtApp> show(@PathVariable("pkId") String pkId) {
        LmtApp lmtApp = lmtAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtApp>(lmtApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<LmtApp> create(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        lmtAppService.insert(lmtApp);
        return new ResultDto<LmtApp>(lmtApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Map> update(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map result = lmtAppService.updateLmtapp(lmtApp);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:deleteLmtApp
     * @函数描述:逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */

    @PostMapping("/deletelmtapp")
    protected ResultDto<Map> deleteLmtApp(@RequestBody LmtApp lmtApp) {
        Map rtnData = new HashMap();
        rtnData = lmtAppService.deleteByPkId(lmtApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:getLmtTypeByCusId
     * @函数描述:通过客户编号获取客户的授信申请类型
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getlmttypebycusid")
    protected ResultDto<Map> getLmtTypeByCusId(@RequestBody String cusId){
        Map map = lmtAppService.getLmtTypeByCusId(cusId);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:saveLmtApp
     * @函数描述:保存授信申请新增数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savelmtapp")
    protected ResultDto<Map> saveLmtApp(@RequestBody LmtApp lmtApp) {
        Map map = lmtAppService.saveLmtApp(lmtApp, "0");
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:onReconside
     * @函数描述:新增单一客户复议申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onreconside")
    protected ResultDto<Map> onReconside(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map rtnData = new HashMap();
        rtnData= lmtAppService.onReconside(lmtApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:onRedicuss
     * @函数描述:新增单一客户再议申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onredicuss")
    protected ResultDto<Map> onRedicuss(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map rtnData = lmtAppService.onRedicuss(lmtApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:updateLmtapp
     * @函数描述:更新单一客户授信申请
     * @参数与返回说明:
     * @算法描述:update
     */
    @PostMapping("/updatelmtapp")
    protected ResultDto<Map> updateLmtApp(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map rtnData = lmtAppService.updateLmtapp(lmtApp);
        return ResultDto.success(rtnData);
    }

    /**
     * @函数名称:savelmtappforgrplmt
     * @函数描述: 新增集团成员的授信申请
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增集团成员的授信申请")
    @PostMapping("/savelmtappforgrplmt")
    protected ResultDto<Map> saveLmtAppForGrpLmt(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map rtnData = lmtAppService.saveLmtAppForGrpLmt(lmtApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称: getLmtAllAppHis
     * @方法描述: 根据入参获取所有的授信历史
     * @参数与返回说明:
     * @算法描述: 无
     */

    @PostMapping("/getlmtallapphis")
    protected ResultDto<List<LmtApp>> getLmtAllAppHis(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtAllAppHis(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @函数名称:onModify
     * @函数描述:新增单一客户授信变更申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onmodify")
    protected ResultDto<Map> onModify(@RequestBody String accNo) throws URISyntaxException {
        Map rtnData = lmtAppService.onModify(accNo);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称: getLmtModify
     * @方法描述: 获取当前客户经理名下所以授信变更申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtmodify")
    protected ResultDto<List<LmtApp>> getLmtModify(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtModify(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @方法名称: getLmtModifyHis
     * @方法描述: 获取当前客户经理名下所以授信变更申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtmodifyhis")
    protected ResultDto<List<LmtApp>> getLmtModifyHis(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtModifyHis(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @方法名称: getLmtApp
     * @方法描述: 根据入参获取当前客户经理名下授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtapp")
    protected ResultDto<List<LmtApp>> getLmtApp(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtAppByModel(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @方法名称: getLmtAppHis
     * @方法描述: 根据入参获取当前客户经理名下授信申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    @PostMapping("/getlmtapphis")
    protected ResultDto<List<LmtApp>> getLmtAppHis(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtAppHis(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @方法名称: getLmtModify
     * @方法描述: 获取当前客户经理名下所以授信变更申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtrefine")
    protected ResultDto<List<LmtApp>> getLmtRefine(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtRefine(model);
        return new ResultDto<List<LmtApp>>(list);
    }

    /**
     * @方法名称: getLmtModifyHis
     * @方法描述: 获取当前客户经理名下所以授信变更申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtrefinehis")
    protected ResultDto<List<LmtApp>> getLmtRefineHis(@RequestBody QueryModel model) {
        List<LmtApp> list = lmtAppService.getLmtRefineHis(model);
        return new ResultDto<List<LmtApp>>(list);
    }

//    /**
//     * @方法名称: getLmtAppBySerno
//     * @方法描述: 根据业务流水号查出该笔业务
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//    @PostMapping("/getlmtappbyserno")
//    protected ResultDto<LmtApp> getLmtAppBySerno(@RequestBody String serno) {
//        LmtApp lmtApp = lmtAppService.selectBySerno(serno.replaceAll("\"", ""));
//        return new ResultDto<LmtApp>(lmtApp);
//    }

    /**
     * @方法名称: getLmtAppBySerno
     * @方法描述: 根据业务流水号查出该笔业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtappbyserno")
    protected ResultDto<LmtApp> getLmtAppBySerno(@RequestBody Map<String,Object> params) {
        LmtApp lmtApp = lmtAppService.selectBySerno((String) params.get("serno"));
        return new ResultDto<LmtApp>(lmtApp);
    }

    /**
     * @方法名称: getLmtGrpAppBySerno
     * @方法描述: 根据成员流水号查寻集团业务信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtgrpappbyserno")
    protected ResultDto<LmtGrpApp> getLmtGrpAppBySerno(@RequestBody Map<String,String> params) {
        LmtGrpApp lmtGrpApp = lmtAppService.getLmtGrpAppBySerno(params.get("serno"));
        return new ResultDto<LmtGrpApp>(lmtGrpApp);
    }

    /**
     * @函数名称:getByPkId
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getbypkid")
    protected ResultDto<LmtApp> getByPkId(@RequestBody String pkId) {
        LmtApp lmtApp = lmtAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtApp>(lmtApp);
    }

    /**
     * @函数名称:onRefine
     * @函数描述:新增单一客户预授信细化申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onrefine")
    protected ResultDto<Map> onRefine(@RequestBody String accNo) {
        Map rtnData = lmtAppService.onRefine(accNo, CmisCommonConstants.LMT_TYPE_07, "0");
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:onReview
     * @函数描述:新增单一客户复审任务
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onreview")
    protected ResultDto<Map> onReview(@RequestBody LmtApp lmtApp) throws URISyntaxException {
        Map rtnData = new HashMap();
        rtnData= lmtAppService.onReview(lmtApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：获取当前登录人下的申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 9:07
     * @修改记录：修改时间   修改人员  修改原因
     */
    @PostMapping("/querybymanagerid")
    protected ResultDto<List<LmtApp>> queryByManagerId() {
        List<LmtApp> lmtAppList = lmtAppService.queryByManagerId();
        return new ResultDto<>(lmtAppList);
    }

    /**
     * @函数名称:getAllLmtByInputId
     * @函数描述:根据客户经理工号查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询授信申请数据")
    @PostMapping("/getAllLmtByInputId")
    protected ResultDto<List<Map<String, Object>>> getAllLmtByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = lmtAppService.getAllLmtByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:insertOtherApproveByLmtSerno
     * @函数描述:根据授信流水号新增其他事项申报信息
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据授信流水号新增其他事项申报信息")
    @PostMapping("/insertOtherApproveByLmtSerno")
    protected ResultDto<OtherAppDto> insertOtherApproveByLmtSerno(@RequestBody Map map){
        OtherAppDto otherAppDto = lmtAppService.insertOtherApproveByLmtSerno(map);
        return ResultDto.success(otherAppDto);
    }

    /**
     * 查询申请客户信息
     * @param map
     * @return
     */
    @PostMapping("/getLmtAppCusData")
    protected ResultDto<Map<String, Object>> getLmtAppCusData(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        return new ResultDto<Map<String, Object>>(lmtAppService.getLmtAppCusData(serno));
    }

    /**
     * 根据流水号修改申请信息
     */
    @PostMapping("/updateBySerno")
    protected ResultDto<Integer> updateBySerno(@RequestBody LmtApp lmtApp){
        return new ResultDto<Integer>(lmtAppService.updateBySerno(lmtApp));
    }

    /**
     *根据本次流水号查询上次申请信息
     */
    @PostMapping("/getLastSerno")
    protected ResultDto<LmtApp> getLastSerno(@RequestBody String serno){
        return new ResultDto<LmtApp>(lmtAppService.getLastSerno(serno));
    }

    /**
     *根据分项流水号查询授信申请信息
     */
    @PostMapping("/getlmtappbysubserno")
    protected ResultDto<LmtApp> getLmtAppBySubSerno(@RequestBody Map map){
        return new ResultDto<LmtApp>(lmtAppService.getLmtAppBySubSerno((String) map.get("subSerno")));
    }
    /**
     *根据客户号更新该客户下的其他事项申报记录到授信关联表中
     */
    @PostMapping("/changeselectListByPageData")
    public ResultDto<Integer> changeselectListByPageData(@RequestBody Map<String,String> params){
        int count = 0;
        String Serno = params.get("serno");//授信申请流水号
        LmtApp lmtApp = lmtAppService.selectBySerno(Serno);
        String  cusId = lmtApp.getCusId();//获取客户号
        //通过客户号查询该客户所有的其他事项申报的记录(抵质押)
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        List<OtherGrtValueIdentyApp> otherGrtValueIdentyApps = otherGrtValueIdentyAppService.selectAll(queryModel);
        if(otherGrtValueIdentyApps.size()>0){
            LmtOtherAppRel lmtOtherAppRel = new LmtOtherAppRel();
            for (int i = 0; i < otherGrtValueIdentyApps.size(); i++) {
                lmtOtherAppRel.setPkId(UUID.randomUUID().toString());
                lmtOtherAppRel.setLmtSerno(Serno);
                lmtOtherAppRel.setLmtOtherAppType("10");
                lmtOtherAppRel.setOprType("01");
                lmtOtherAppRel.setLmtOtherAppSerno(otherGrtValueIdentyApps.get(i).getSerno());
                lmtOtherAppRel.setInputId(otherGrtValueIdentyApps.get(i).getInputId());
                lmtOtherAppRel.setInputBrId(otherGrtValueIdentyApps.get(i).getInputBrId());
                lmtOtherAppRel.setUpdateTime(otherGrtValueIdentyApps.get(i).getUpdateTime());
                lmtOtherAppRel.setApproveStatus(otherGrtValueIdentyApps.get(i).getApproveStatus());
                count=lmtOtherAppRelService.insertSelective(lmtOtherAppRel);
            }
        }
        return new ResultDto<Integer>(count);
    }


    /**
     *删除授信关联表中
     */
    @PostMapping("/changedeleteListByPageData")
    public ResultDto<Integer> changedeleteListByPageData(@RequestBody Map<String,String> params){
        int count = 0;
        String Serno = params.get("serno");//申请流水号
        String lmtSerno= params.get("lmtSerno");//申请流水号
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("lmtSerno",lmtSerno);
        queryModel.addCondition("lmtOtherAppSerno",Serno);
        List<LmtOtherAppRel> lmtOtherAppRels = lmtOtherAppRelService.selectAll(queryModel);
        LmtOtherAppRel lmtOtherAppRel =lmtOtherAppRels.get(0);
        String pkID=lmtOtherAppRel.getPkId();
        lmtOtherAppRelService.deleteByPrimaryKey(pkID);
        return new ResultDto<Integer>(count);
    }

    /**
     *通过流水号获取当前客户相关信息
     */
    @PostMapping("/getguarringdatabyserno")
    protected ResultDto<Map> getGuarRingDataBySerno(@RequestBody Map map){
        return new ResultDto<Map>(lmtAppService.getGuarRingDataBySerno((String) map.get("serno")));
    }

    /**
     * @函数名称: judgeSxkdOnlineAppr
     * @函数描述: 省心快贷对公智能风控提交审批
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/judgeSxkdOnlineAppr")
    protected ResultDto<String> judgeSxkdOnlineAppr(@RequestBody Map<String, Object> params) throws Exception {
        return lmtAppService.judgeSxkdOnlineAppr(params);
    }

    /**
     * @函数名称:getApproveResult
     * @函数描述:通过申请流水号获取省心快贷风控返回审批结果
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getApproveResult")
    protected ResultDto<Map> getApproveResult(@RequestBody String serno){
        Map map = lmtAppService.getApproveResult(serno);
        return new ResultDto<>(map);
    }

    /**
     * 获取内评低准入例外审批授信申请信息
     * @param model
     * @return
     */
    @PostMapping("/getNpGreenApp")
    protected ResultDto<List<LmtApp>> getNpGreenApp(@RequestBody QueryModel model){
        return new ResultDto<List<LmtApp>>(lmtAppService.getNpGreenApp(model));
    }

    /**
     * 针对东海，村镇集团下子成员流程直接通过
     * @param serno
     * @return
     */
    @PostMapping("/passforsganddh")
    protected ResultDto<Map> passForSGAndDH(@RequestBody String serno) throws Exception {
        return new ResultDto<Map>(lmtAppService.passForSGAndDH(serno));
    }
}