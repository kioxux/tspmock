//package cn.com.yusys.yusp.web.rest;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.AccWriteoff;
//import cn.com.yusys.yusp.service.AccWriteoffService;
//import cn.com.yusys.yusp.service.ReyPlanService;
//import cn.com.yusys.yusp.service.YqdRiskInfoService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import java.net.URISyntaxException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @项目名称: cmis-biz模块
// * @类名称: AccWriteoffResource
// * @类描述: #资源类
// * @功能描述:
// * @创建人: mashun
// * @创建时间: 2021-01-19 19:41:22
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/yqdriskinfo")
//public class yqdRiskInfoResource {
//
//    @Autowired
//    private YqdRiskInfoService yqdriskinfoservice;
//
//    /**
//     * @param
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.String>>>
//     * @author shenli
//     * @date 2021/4/25 0025 20:12
//     * @version 1.0.0
//     * @desc 优企贷-征信风险提示
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @PostMapping("/query/creditrisk")
//    protected ResultDto<List<Map<String, String>>> queryCreditRisk() {
//        return new ResultDto<List<Map<String, String>>>(yqdriskinfoservice.queryCreditRisk());
//    }
//
//    /**
//     * @param
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.String>>>
//     * @author shenli
//     * @date 2021/4/25 0025 20:12
//     * @version 1.0.0
//     * @desc 优企贷-历史信贷表现（灰名单）
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @PostMapping("/query/blacklist")
//    protected ResultDto<List<Map<String, String>>> queryBlackList() {
//        return new ResultDto<List<Map<String, String>>>(yqdriskinfoservice.queryBlackList());
//    }
//
//    @PostMapping("/queryRiskInfo")
//    protected ResultDto<Map<String,Object>> queryRiskInfo() {
//        Map result = yqdriskinfoservice.queryRiskInfo();
//        return new ResultDto<Map<String,Object>>(result);
//    }
//
//
//
//    @PostMapping("/query/queryLawsuit")
//    protected ResultDto<List<Map<String, String>>> queryLawsuit() {
//        return new ResultDto<List<Map<String, String>>>(yqdriskinfoservice.queryLawsuit());
//    }
//}
