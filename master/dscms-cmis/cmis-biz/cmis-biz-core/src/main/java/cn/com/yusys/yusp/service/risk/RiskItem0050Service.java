package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarBizRel;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import cn.com.yusys.yusp.service.GuarBizRelService;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0050Service
 * @类描述: 白领易贷通贷款金额校验
 * @功能描述: 白领易贷通贷款金额校验
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0050Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0049Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private GuarBizRelService guarBizRelService; // 业务与担保品关系表

    @Autowired
    private GuarBaseInfoService guarBaseInfoService; // 担保品详细表

    /**
     * @方法名称: riskItem0050
     * @方法描述: 白领易贷通贷款金额校验
     * @参数与返回说明:
     * @算法描述: 担保方式=抵押时，贷款金额必须小于等于抵押物评估金额的70%（产品编号：022028）
     * @创建人: macm
     * @创建时间: 2021年9月8日17:20:58
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0050(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        log.info("*************白领易贷通贷款金额校验开始***********流水号：【{}】", iqpSerno);
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if(Objects.isNull(iqpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1503); //业务流水号为空
            return riskResultDto;
        }
        if("022028".equals(iqpLoanApp.getPrdId()) && CmisCommonConstants.GUAR_MODE_10.equals(iqpLoanApp.getGuarWay())){
            // 开始查找业务所关联的担保品
            List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(iqpSerno);
            log.info("白领易贷通贷款金额校验--》共查找到抵押品数量为：【{}】", guarBizRelList.size());
            if(guarBizRelList.size() > 0){
                // 开始循环相加担保品评估价值
                BigDecimal tatolEvalAmt = BigDecimal.ZERO;
                for(GuarBizRel guarBizRel : guarBizRelList) {
                    GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryBaseInfoByGuarId(guarBizRel.getGuarNo());
                    tatolEvalAmt = tatolEvalAmt.add(guarBaseInfo.getEvalAmt());
                }
                log.info("白领易贷通贷款金额校验--》申请金额为：【{}】，评估价值和为：【{}】", iqpLoanApp.getAppAmt().toPlainString(),tatolEvalAmt.toPlainString());
                if(iqpLoanApp.getAppAmt().compareTo(tatolEvalAmt.multiply(BigDecimal.valueOf(0.7))) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05001);
                    return riskResultDto;
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05002);
                return riskResultDto;
            }
        }
        log.info("*************白领易贷通贷款金额校验开始***********流水号：【{}】", iqpSerno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
