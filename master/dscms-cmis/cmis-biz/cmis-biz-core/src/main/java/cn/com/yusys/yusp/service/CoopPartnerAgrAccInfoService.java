package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constant.PartnerTypeEnums;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CoopPlanEspecQuotaCtrlDto;
import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import cn.com.yusys.yusp.dto.DocImageSpplClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004StrOrgListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.resp.CmisLmt0004RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.math3.util.MathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAccInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 10:44:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerAgrAccInfoService {

    private static final Logger log = LoggerFactory.getLogger(CoopPartnerAgrAccInfoService.class);

    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

    @Autowired
    private CoopPlanSuitOrgInfoService coopPlanSuitOrgInfoService;

    @Autowired
    private CoopPlanEspecQuotaCtrlService coopPlanEspecQuotaCtrlService;

    @Autowired
    private CoopPlanProInfoService coopPlanProInfoService;

    @Autowired
    private CoopPartnerAgrAddRelMapper coopPartnerAgrAddRelMapper;

    @Autowired
    private CoopReplyAgrProMapper coopReplyAgrProMapper;

    @Autowired
    private CoopReplyAgrSubMapper coopReplyAgrSubMapper;

    @Autowired
    private CoopReplyAccSubMapper coopReplyAccSubMapper;

    @Autowired
    private CoopReplyAccProMapper coopReplyAccProMapper;

    @Autowired
    private CoopPartnerAgrAppMapper coopPartnerAgrAppMapper;

    @Autowired
    private CoopReplyAccMapper coopReplyAccMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CoopPartnerAgrAccInfo selectByPrimaryKey(String coopAgrNo) {
        return coopPartnerAgrAccInfoMapper.selectByPrimaryKey(coopAgrNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CoopPartnerAgrAccInfo> selectAll(QueryModel model) {
        List<CoopPartnerAgrAccInfo> records = coopPartnerAgrAccInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CoopPartnerAgrAccInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerAgrAccInfo> list = coopPartnerAgrAccInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CoopPartnerAgrAccInfo record) {
        return coopPartnerAgrAccInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CoopPartnerAgrAccInfo record) {
        return coopPartnerAgrAccInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CoopPartnerAgrAccInfo record) {
        return coopPartnerAgrAccInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CoopPartnerAgrAccInfo record) {
        return coopPartnerAgrAccInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String coopAgrNo) {
        return coopPartnerAgrAccInfoMapper.deleteByPrimaryKey(coopAgrNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return coopPartnerAgrAccInfoMapper.deleteByIds(ids);
    }

    /**
     * 流程后处理-更新协议台账、调用额度接口同步数据
     *
     * @param record
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int wfupdate(CoopPartnerAgrAccInfo record) {
        QueryModel qmSerno = new QueryModel();
        qmSerno.addCondition("serno",record.getCoopAgrSerno());
        //判断是否签订一般担保额度，更新分项批复台账
        CoopPartnerAgrApp coopPartnerAgrApp1 = coopPartnerAgrAppMapper.selectByPrimaryKey(record.getCoopAgrSerno());
        if (coopPartnerAgrApp1 != null && "1".equals(coopPartnerAgrApp1.getIsCommonGrtLmtAmt())) {
            QueryModel qmCoopPlanNo = new QueryModel();
            qmCoopPlanNo.addCondition("coopPlanNo", record.getCoopPlanNo());
            //更新批复台账表中一般担保额度
            List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(qmCoopPlanNo);
            CoopReplyAcc coopReplyAcc = null;
            if (!CollectionUtils.isEmpty(coopReplyAccList)) {
                //计算一般担保额度签订的总的金额,获取本次协议申请的一般担保额度
                coopReplyAcc = coopReplyAccList.get(0);
                //获取变更的原协议签订金额
                CoopPartnerAgrAccInfo coopPartnerAgrAccInfo =coopPartnerAgrAccInfoMapper.selectByPrimaryKey(record.getCoopAgrNo());
                BigDecimal signAmt = coopPartnerAgrApp1.getCommonGrtLmtAmt();//本次申请签订的一般担保额度
                BigDecimal signAmtHis = coopReplyAcc.getSignAmt();//批复台账中的已签订金额
                if(coopReplyAcc.getSignAmt() !=null){
                    signAmtHis= coopReplyAcc.getSignAmt();
                }
                BigDecimal signAmtOld =coopPartnerAgrAccInfo.getCommonGrtLmtAmt();//原协议签订金额
                signAmt = signAmt.add(signAmtHis).subtract(signAmtOld);
                coopReplyAcc.setSignAmt(signAmt);
                coopReplyAccMapper.updateByPrimaryKeySelective(coopReplyAcc);
            }
        }
        //更新批复分项信息
        List<CoopReplyAgrSub> coopReplyAgrSubList = coopReplyAgrSubMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrSubList)) {
            for (CoopReplyAgrSub coopReplyAgrSub : coopReplyAgrSubList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("subNo",coopReplyAgrSub.getSubNo());
                CoopReplyAccSub coopReplyAccSub = new CoopReplyAccSub();
                //计算已签订总金额
                List<CoopReplyAccSub>  coopReplyAccSubList= coopReplyAccSubMapper.selectByModel(qmSignAmt);
                //之前签订总金额
                BigDecimal signAmt1 = BigDecimal.ZERO;
                if(coopReplyAccSubList!=null && coopReplyAccSubList.size()>0){
                     coopReplyAccSub = coopReplyAccSubList.get(0);
                    if(coopReplyAccSub!=null&&coopReplyAccSub.getSignAmt()!=null){
                        signAmt1 = coopReplyAccSub.getSignAmt();
                    }
                }
                QueryModel qmCoopAgrSerno = new QueryModel();
                qmCoopAgrSerno.addCondition("coopAgrNo",record.getCoopAgrNo());
                List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfos = coopPartnerAgrAccInfoMapper.selectByModel(qmCoopAgrSerno);
                QueryModel qmCoopAgrNo = new QueryModel();
                qmCoopAgrNo.addCondition("serno",coopPartnerAgrAccInfos.get(0).getCoopAgrSerno());
                qmCoopAgrNo.addCondition("subNo",coopReplyAgrSub.getSubNo());
                List<CoopReplyAgrSub> coopReplyAgrSubs =coopReplyAgrSubMapper.selectByModel(qmCoopAgrNo);
                //获取变更之前签订金额
                BigDecimal signAmtOld = BigDecimal.ZERO;
                  if(coopReplyAgrSubs!=null && coopReplyAgrSubs.size()>0){
                          CoopReplyAgrSub coopReplyAgrSubOld =coopReplyAgrSubs.get(0);
                          if(coopReplyAgrSubOld!=null&&coopReplyAgrSubOld.getSignAmt()!=null){
                              signAmtOld = signAmtOld.add(coopReplyAgrSubOld.getSignAmt());
                      }
                  }
                //本次签订金额
                BigDecimal signAmtNew = coopReplyAgrSub.getSignAmt();
                //计算签订总金额
                signAmt = signAmt1.add(signAmtNew.subtract(signAmtOld));
                //更新台账表中已签订金额
                coopReplyAccSub.setSignAmt(signAmt);
                coopReplyAccSubMapper.updateByPrimaryKeySelective(coopReplyAccSub);
            }
        }
        //更新批复分项项目信息
        List<CoopReplyAgrPro> coopReplyAgrProList = coopReplyAgrProMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrProList)) {
            for (CoopReplyAgrPro coopReplyAgrPro : coopReplyAgrProList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("proNo", coopReplyAgrPro.getProNo());
                //计算变更时分项的已签订总金额
                List<CoopReplyAccPro> coopReplyAccProList = coopReplyAccProMapper.selectByModel(qmSignAmt);
                BigDecimal signAmt1 = new BigDecimal(0);
                CoopReplyAccPro coopReplyAccPro = coopReplyAccProList.get(0);
                if(coopReplyAccPro!=null && coopReplyAccPro.getSignAmt()!=null){
                    signAmt1 = coopReplyAccPro.getSignAmt();
                }
                //本次协议变更之前旧协议的签订金额
                QueryModel qmCoopAgrSerno = new QueryModel();
                qmCoopAgrSerno.addCondition("coopAgrNo",record.getCoopAgrNo());
                List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfos = coopPartnerAgrAccInfoMapper.selectByModel(qmCoopAgrSerno);
                List<CoopReplyAgrPro> coopReplyAgrPros =null;
                if(coopPartnerAgrAccInfos != null &&coopPartnerAgrAccInfos.size()>0){
                    QueryModel qmCoopAgrNo = new QueryModel();
                    qmCoopAgrNo.addCondition("serno",coopPartnerAgrAccInfos.get(0).getCoopAgrSerno());
                    qmCoopAgrNo.addCondition("proNo",coopReplyAgrPro.getProNo());
                    coopReplyAgrPros =coopReplyAgrProMapper.selectByModel(qmCoopAgrNo);
                }
                BigDecimal signAmtOld = new BigDecimal(0);
              if(coopReplyAgrPros!=null && coopReplyAgrPros.size()>0){
                  //获取变更之前签订金额
                   signAmtOld = coopReplyAgrPros.get(0).getSignAmt();
              }

                //本次签订金额
                BigDecimal signAmtNew = coopReplyAgrPro.getSignAmt();
                //计算签订总金额
                signAmt = signAmt1.add(signAmtNew.subtract(signAmtOld));
                //更新台账表中已签订金额
                coopReplyAccPro.setSignAmt(signAmt);
                coopReplyAccProMapper.updateByPrimaryKeySelective(coopReplyAccPro);
            }
        }
        // 1、插入数据
        int result=0;
        CoopPartnerAgrAccInfo isExists = coopPartnerAgrAccInfoMapper.selectByPrimaryKey(record.getCoopAgrNo());
        if(isExists!=null){
            result = coopPartnerAgrAccInfoMapper.updateByPrimaryKeySelective(record);
        }else{
            result = coopPartnerAgrAccInfoMapper.insert(record);
        }
        if("1".equals(record.getPartnerType()) || "2".equals(record.getPartnerType()) || "3".equals(record.getPartnerType()) || "4".equals(record.getPartnerType()) || "5".equals(record.getPartnerType()) || "8".equals(record.getPartnerType()) || "15".equals(record.getPartnerType())) {
            feignLmt0004(record);
        }
        return result;
    }


    /**
     * 续签流程后处理-更新协议台账、调用额度接口同步数据
     *
     * @param record
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int wfupdate4resign(CoopPartnerAgrAccInfo record) {
        //1、更新续签的原协议状态为失效
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",record.getCoopAgrSerno());
        List<CoopPartnerAgrAddRel> list = coopPartnerAgrAddRelMapper.selectByModel(queryModel);
        List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfos = new ArrayList<CoopPartnerAgrAccInfo>();
        for(CoopPartnerAgrAddRel coopPartnerAgrAddRel:list) {
            QueryModel queryModelCoopPartnerAgrAccInfo = new QueryModel();
            queryModelCoopPartnerAgrAccInfo.addCondition("coopAgrNo",coopPartnerAgrAddRel.getCoopAgrNo());
            queryModelCoopPartnerAgrAccInfo.addCondition("agrStatus","01");
            List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfoList= coopPartnerAgrAccInfoMapper.selectByModel(queryModelCoopPartnerAgrAccInfo);
            if(!CollectionUtils.isEmpty(coopPartnerAgrAccInfoList)){
                CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = coopPartnerAgrAccInfoList.get(0);
                coopPartnerAgrAccInfo.setAgrStatus("02");
                coopPartnerAgrAccInfoMapper.updateByPrimaryKey(coopPartnerAgrAccInfo);
                coopPartnerAgrAccInfos.add(coopPartnerAgrAccInfo);
            }
        }

        QueryModel qmSerno = new QueryModel();
        qmSerno.addCondition("serno",record.getCoopAgrSerno());
        //判断是否签订一般担保额度，更新分项批复台账
        CoopPartnerAgrApp coopPartnerAgrApp1 = coopPartnerAgrAppMapper.selectByPrimaryKey(record.getCoopAgrSerno());
        if (coopPartnerAgrApp1 != null && "1".equals(coopPartnerAgrApp1.getIsCommonGrtLmtAmt())) {
            QueryModel qmCoopPlanNo = new QueryModel();
            qmCoopPlanNo.addCondition("coopPlanNo", record.getCoopPlanNo());
            //更新批复台账表中一般担保额度
            List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(qmCoopPlanNo);
            CoopReplyAcc coopReplyAcc = null;
            if (!CollectionUtils.isEmpty(coopReplyAccList)) {
                //计算一般担保额度签订的总的金额,获取本次协议申请的一般担保额度
                coopReplyAcc = coopReplyAccList.get(0);
                //获取续签的的原协议签订金额
                BigDecimal signAmtOld= new BigDecimal(0);
                for(CoopPartnerAgrAccInfo coopPartnerAgrAccInfo:coopPartnerAgrAccInfos){
                    if(coopPartnerAgrAccInfo.getCommonGrtLmtAmt()!=null)
                     signAmtOld =signAmtOld.add(coopPartnerAgrAccInfo.getCommonGrtLmtAmt());//原协议签订金额
                }
                BigDecimal signAmt = coopPartnerAgrApp1.getCommonGrtLmtAmt();//本次申请签订的一般担保额度
                BigDecimal signAmtHis = new BigDecimal(0);//批复台账中的已签订金额
                if(coopReplyAcc.getSignAmt()!=null){
                    signAmtHis = coopReplyAcc.getSignAmt();//批复台账中的已签订金额
                }

                signAmt = signAmt.add(signAmtHis).subtract(signAmtOld);
                coopReplyAcc.setSignAmt(signAmt);
                coopReplyAccMapper.updateByPrimaryKeySelective(coopReplyAcc);
            }
        }
        //更新批复分项信息
        List<CoopReplyAgrSub> coopReplyAgrSubList = coopReplyAgrSubMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrSubList)) {
            for (CoopReplyAgrSub coopReplyAgrSub : coopReplyAgrSubList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("subNo", coopReplyAgrSub.getSubNo());
                //计算续签时分项的已签订总金额
                List<CoopReplyAccSub> coopReplyAccSubList = coopReplyAccSubMapper.selectByModel(qmSignAmt);
                BigDecimal signAmt1 = new BigDecimal(0);
                CoopReplyAccSub coopReplyAccSub = coopReplyAccSubList.get(0);
                if(coopReplyAccSub!=null && coopReplyAccSub.getSignAmt()!=null){
                    signAmt1 = coopReplyAccSub.getSignAmt();
                }
                //本次协议变更之前旧协议的签订金额
                //获取续签的的原协议签订的金额
                BigDecimal signAmtOld= new BigDecimal(0);//原协议下该项目的历史签订金额
                for(CoopPartnerAgrAccInfo coopPartnerAgrAccInfo:coopPartnerAgrAccInfos){
                    QueryModel qmcoopReplyAgrSub = new QueryModel();
                    qmcoopReplyAgrSub.addCondition("serno",coopPartnerAgrAccInfo.getCoopAgrSerno());
                    qmcoopReplyAgrSub.addCondition("subNo",coopReplyAccSub.getSubNo());
                    List<CoopReplyAgrSub> coopReplyAgrSubs =coopReplyAgrSubMapper.selectByModel(qmcoopReplyAgrSub);
                    if(coopReplyAgrSubs!=null && coopReplyAgrSubs.size()>0){
                        CoopReplyAgrSub coopReplyAgrSubOld =coopReplyAgrSubs.get(0);
                        if(coopReplyAgrSubOld!=null&&coopReplyAgrSubOld.getSignAmt()!=null){
                            signAmtOld = signAmtOld.add(coopReplyAgrSubOld.getSignAmt());
                        }
                    }
                }
                //本次签订金额
                BigDecimal signAmtNew = coopReplyAgrSub.getSignAmt();
                //计算签订总金额
                signAmt = signAmt1.add(signAmtNew.subtract(signAmtOld));
                //更新台账表中已签订金额
                coopReplyAccSub.setSignAmt(signAmt);
                coopReplyAccSubMapper.updateByPrimaryKeySelective(coopReplyAccSub);
            }
        }
        //更新批复分项项目信息
        List<CoopReplyAgrPro> coopReplyAgrProList = coopReplyAgrProMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrProList)) {
            for (CoopReplyAgrPro coopReplyAgrPro : coopReplyAgrProList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("proNo", coopReplyAgrPro.getProNo());
                //计算续签时分项的已签订总金额
                List<CoopReplyAccPro> coopReplyAccProList = coopReplyAccProMapper.selectByModel(qmSignAmt);
                BigDecimal signAmt1 = new BigDecimal(0);
                CoopReplyAccPro coopReplyAccPro = coopReplyAccProList.get(0);
                if(coopReplyAccPro!=null && coopReplyAccPro.getSignAmt()!=null){
                    signAmt1 = coopReplyAccPro.getSignAmt();
                }
                //本次协议变更之前旧协议的签订金额
                //获取续签的的原协议签订的金额
                BigDecimal signAmtOld= new BigDecimal(0);//原协议下该项目的历史签订金额
                for(CoopPartnerAgrAccInfo coopPartnerAgrAccInfo:coopPartnerAgrAccInfos){
                    QueryModel qmcoopReplyAgrPro = new QueryModel();
                    qmcoopReplyAgrPro.addCondition("serno",coopPartnerAgrAccInfo.getCoopAgrSerno());
                    qmcoopReplyAgrPro.addCondition("proNo",coopReplyAgrPro.getProNo());
                    List<CoopReplyAgrPro> coopReplyAgrPros =coopReplyAgrProMapper.selectByModel(qmcoopReplyAgrPro);
                    if(coopReplyAgrPros!=null && coopReplyAgrPros.size()>0){
                        CoopReplyAgrPro coopReplyAgrProOld =coopReplyAgrPros.get(0);
                        if(coopReplyAgrProOld!=null&&coopReplyAgrProOld.getSignAmt()!=null){
                            signAmtOld = signAmtOld.add(coopReplyAgrProOld.getSignAmt());
                        }
                    }
                }
                //本次签订金额
                BigDecimal signAmtNew = coopReplyAgrPro.getSignAmt();
                //计算签订总金额
                signAmt = signAmt1.add(signAmtNew.subtract(signAmtOld));
                //更新台账表中已签订金额
                coopReplyAccPro.setSignAmt(signAmt);
                coopReplyAccProMapper.updateByPrimaryKeySelective(coopReplyAccPro);
            }
        }
        // 1、插入数据
        int result=0;
        CoopPartnerAgrAccInfo isExists = coopPartnerAgrAccInfoMapper.selectByPrimaryKey(record.getCoopAgrNo());
        if(isExists!=null){
            result = coopPartnerAgrAccInfoMapper.updateByPrimaryKeySelective(record);
        }else{
            result = coopPartnerAgrAccInfoMapper.insert(record);
        }
        //其它合作方/律师事务所/网金业务合作方/仓储公司/旅游公司/物联网动产贷交易平台归属方/监管公司/押品评估机构
        //[{"key":"1","value":"房地产开发商"},{"key":"2","value":"专业担保公司"},{"key":"3","value":"保险公司"},{"key":"4","value":"集群贷市场方"},{"key":"5","value":"光伏设备生产企业"},{"key":"6","value":"旅游公司"},{"key":"7","value":"物联网动产贷交易平台归属方"},{"key":"8","value":"核心企业"},{"key":"9","value":"监管公司"},{"key":"10","value":"仓储公司"},{"key":"11","value":"押品评估机构"},{"key":"12","value":"网金业务合作方"},{"key":"13","value":"律师事务所"},{"key":"14","value":"其他合作方"},{"key":"15","value":"教育机构"},{"key":"16","value":"项目经理人合作方"}]
        if("1".equals(record.getPartnerType()) || "2".equals(record.getPartnerType()) || "3".equals(record.getPartnerType()) || "4".equals(record.getPartnerType()) || "5".equals(record.getPartnerType()) || "8".equals(record.getPartnerType()) || "15".equals(record.getPartnerType())) {
            feignLmt0004(record);
        }
        return result;
    }
    /**
     * 流程后处理-新增协议台账、调用额度接口同步数据
     *
     * @param record
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int wfinsert(CoopPartnerAgrAccInfo record) {
        QueryModel qmSerno = new QueryModel();
        qmSerno.addCondition("serno",record.getCoopAgrSerno());
        //判断是否签订一般担保额度，更新分项批复台账
        CoopPartnerAgrApp coopPartnerAgrApp1 = coopPartnerAgrAppMapper.selectByPrimaryKey(record.getCoopAgrSerno());
        if (coopPartnerAgrApp1 != null && "1".equals(coopPartnerAgrApp1.getIsCommonGrtLmtAmt())) {
            QueryModel qmCoopPlanNo = new QueryModel();
            qmCoopPlanNo.addCondition("coopPlanNo", record.getCoopPlanNo());
            //更新批复台账表中一般担保额度
            List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(qmCoopPlanNo);
            CoopReplyAcc coopReplyAcc = null;
            if (!CollectionUtils.isEmpty(coopReplyAccList)) {
                //计算一般担保额度签订的总的金额,获取本次协议申请的一般担保的
                coopReplyAcc = coopReplyAccList.get(0);
                BigDecimal signAmt = coopPartnerAgrApp1.getCommonGrtLmtAmt();
                BigDecimal signAmtHis = new BigDecimal(0);
                if(coopReplyAcc.getSignAmt() !=null){
                    signAmtHis= coopReplyAcc.getSignAmt();
                }
                signAmt = signAmt.add(signAmtHis);
                coopReplyAcc.setSignAmt(signAmt);
                coopReplyAccMapper.updateByPrimaryKeySelective(coopReplyAcc);
            }
        }
        //更新批复分项信息
        List<CoopReplyAgrSub> coopReplyAgrSubList = coopReplyAgrSubMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrSubList)) {
            for (CoopReplyAgrSub coopReplyAgrSub : coopReplyAgrSubList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("subNo",coopReplyAgrSub.getSubNo());
                //计算已签订总金额
                List<CoopReplyAccSub>  coopReplyAccSubList= coopReplyAccSubMapper.selectByModel(qmSignAmt);
                CoopReplyAccSub coopReplyAccSub = coopReplyAccSubList.get(0);
                //之前签订总金额
                BigDecimal signAmt1 = new BigDecimal(0);
                //本次签订金额
                BigDecimal signAmt2 = new BigDecimal(0);
                if (coopReplyAccSub.getSignAmt() != null){
                     signAmt1 = coopReplyAccSub.getSignAmt();
                }
                if (coopReplyAgrSub.getSignAmt() != null){
                    signAmt2 = coopReplyAgrSub.getSignAmt();
                }
                //总的已签订金额
                signAmt = signAmt1.add(signAmt2);
                coopReplyAccSub.setSignAmt(signAmt);
                coopReplyAccSubMapper.updateByPrimaryKeySelective(coopReplyAccSub);
            }
        }
        //更新批复分项项目信息
        List<CoopReplyAgrPro> coopReplyAgrProList = coopReplyAgrProMapper.selectByModel(qmSerno);
        if (!CollectionUtils.isEmpty(coopReplyAgrProList)) {
            for (CoopReplyAgrPro coopReplyAgrPro : coopReplyAgrProList) {
                BigDecimal signAmt = new BigDecimal(0);
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("proNo", coopReplyAgrPro.getProNo());
                //计算已签订总金额
                List<CoopReplyAccPro> coopReplyAccProList = coopReplyAccProMapper.selectByModel(qmSignAmt);
                CoopReplyAccPro coopReplyAccPro = coopReplyAccProList.get(0);
                //之前签订总金额
                BigDecimal signAmt1 = new BigDecimal(0);
                if (coopReplyAccPro.getSignAmt() != null){
                    signAmt1 = coopReplyAccPro.getSignAmt();
                }
                //本次签订金额
                BigDecimal signAmt2 = coopReplyAgrPro.getSignAmt();
                //总的已签订金额
                signAmt = signAmt1.add(signAmt2);
                coopReplyAccPro.setSignAmt(signAmt);
                coopReplyAccProMapper.updateByPrimaryKeySelective(coopReplyAccPro);
            }
        }
        // 1、插入数据
        int result=0;
        CoopPartnerAgrAccInfo isExists = coopPartnerAgrAccInfoMapper.selectByPrimaryKey(record.getCoopAgrNo());
        if(isExists!=null){
            result = coopPartnerAgrAccInfoMapper.updateByPrimaryKeySelective(record);
        }else{
            result = coopPartnerAgrAccInfoMapper.insert(record);
        }

        // 2、调用额度服务更新额度
        if("1".equals(record.getPartnerType()) || "2".equals(record.getPartnerType()) || "3".equals(record.getPartnerType()) || "4".equals(record.getPartnerType()) || "5".equals(record.getPartnerType()) || "8".equals(record.getPartnerType()) || "15".equals(record.getPartnerType())) {
            feignLmt0004(record);
        }
        return result;
    }

    public void feignLmt0004(CoopPartnerAgrAccInfo record) {
        // 2、调用额度服务更新额度
        // 2.1 根据合作方案编号查询方案信息
        CoopPlanAccInfo coopPlanAccInfo = new CoopPlanAccInfo();
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("coopPlanNo", record.getCoopPlanNo());
        List<CoopPlanAccInfo> planList = coopPlanAccInfoService.selectByModel(queryModel1);
        if (!CollectionUtils.isEmpty(planList)) {
            coopPlanAccInfo = planList.get(0);
        }
        //获取合作方协议操作类型
        CoopPartnerAgrApp coopPartnerAgrApp = coopPartnerAgrAppMapper.selectByPrimaryKey(record.getCoopAgrSerno());
        //1,新增  2，变更  3续签
        String coopAgrOprType = coopPartnerAgrApp.getCoopAgrOprType();

        // 2.2 组装请求数据
        // 2.2.1 基本信息
        CmisLmt0004ReqDto reqDto = new CmisLmt0004ReqDto();
        reqDto.setSysId("cmis"); //系统编号
        //获取机构表中的INSTU_ID
        ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(coopPlanAccInfo.getManagerBrId());
        AdminSmOrgDto adminSmOrgDto = resultDto.getData();
        if(adminSmOrgDto != null && adminSmOrgDto.getInstuId()!=null){
            reqDto.setInstuCde(adminSmOrgDto.getInstuId()); //金融机构代码  C1115632000023-总行 C1102137000013-寿光 C1100832000011-东海
        }else{
            reqDto.setInstuCde("C1115632000023"); //金融机构代码  C1115632000023-总行 C1102137000013-寿光 C1100832000011-东海
        }
        reqDto.setAccNo(record.getCoopPlanNo()); //批复台账编号
//        reqDto.setOrigiAccNo(); //原批复台账编号
        reqDto.setCusId(record.getPartnerNo()); //客户编号 --- 合作方编号
        reqDto.setCusName(record.getPartnerName());//客户名称 --- 合作方名称
        reqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG2);//客户类型 -- 对公
        reqDto.setCopType(record.getPartnerType()); // 合作方类型
//        reqDto.setIsCreateAcc();// 是否生成新批复台账
        reqDto.setLmtAmt(coopPlanAccInfo.getTotlCoopLmtAmt()); //授信金额 以方案中为准
        reqDto.setTerm(coopPlanAccInfo.getCoopTerm());//授信期限
        reqDto.setStartDate(coopPlanAccInfo.getCoopStartDate()); //起始日期 以方案中为准
        reqDto.setEndDate(coopPlanAccInfo.getCoopEndDate()); //到期日期 以方案中为准
        reqDto.setCurType("CNY"); //币种 默认人民币
        reqDto.setAccStatus("01");// 批复台账状态  默认 01-生效
        reqDto.setIsWholeBankSuit(coopPlanAccInfo.getIsWholeBankSuit()); //是否全行适用
        reqDto.setManagerId(coopPlanAccInfo.getManagerId()); //责任人
        reqDto.setManagerBrId(coopPlanAccInfo.getManagerBrId()); // 责任机构
        reqDto.setInputId(coopPlanAccInfo.getInputId()); // 登记人
        reqDto.setInputBrId(coopPlanAccInfo.getInputBrId()); // 登记机构
        reqDto.setInputDate(coopPlanAccInfo.getInputDate()); // 登记日期

        // 2.2.2 授信分项信息
        QueryModel queryModel2 = new QueryModel();
        queryModel2.addCondition("serno", record.getCoopAgrSerno());
        queryModel2.addCondition("coopPlanNo", coopPlanAccInfo.getCoopPlanNo());

        List<CmisLmt0004LmtSubListReqDto> lmtSubList = new ArrayList<>();
        // 担保类--限额分项
        List<CoopReplyAgrSub> coopReplyAgrSubList = coopReplyAgrSubMapper.selectByModel(queryModel2);
        if (!CollectionUtils.isEmpty(coopReplyAgrSubList)) {
            for (CoopReplyAgrSub coopReplyAgrSub : coopReplyAgrSubList) {
                CmisLmt0004LmtSubListReqDto lmtSubListReqDto = new CmisLmt0004LmtSubListReqDto();
                lmtSubListReqDto.setAccSubNo(coopReplyAgrSub.getSubNo());// 授信分项编号-- 合作产品编号
//                lmtSubListReqDto.setOrigiAccSubNo();//原授信分项编号
//                lmtSubListReqDto.setParentId(); //父节点
                if (!(Integer.valueOf(coopPlanAccInfo.getPartnerType()) > 8)) { // 仅传前8种
                    lmtSubListReqDto.setLmtBizTypeProp(coopReplyAgrSub.getPrdTypeProp()); //授信品种编号
                    lmtSubListReqDto.setLimitSubNo("2");//授信品种编号 2-担保公司
                    //lmtSubListReqDto.setLimitSubName(CmisCusConstants.prdTypePropName.get(coopReplyAgrSub.getPrdTypeProp())); // 授信品种名称
                }
                QueryModel qm = new QueryModel();
                qm.addCondition("subNo",coopReplyAgrSub.getSubNo());
                BigDecimal signAmt = coopReplyAccSubMapper.selectByModel(qm).get(0).getSignAmt();
                lmtSubListReqDto.setAvlAmt(signAmt);//授信金额--单个产品合作额度
                lmtSubListReqDto.setSigAmt(coopPlanAccInfo.getSingleCoopQuota());//单户限额
                lmtSubListReqDto.setSigBussAmt(coopPlanAccInfo.getSigBusiCoopQuota());//单笔业务限额
                lmtSubListReqDto.setCurType("CNY"); //币种 默认人民币
                lmtSubListReqDto.setTerm(coopPlanAccInfo.getCoopTerm()); //授信期限
                lmtSubListReqDto.setStartDate(coopPlanAccInfo.getCoopStartDate()); //额度起始日
                lmtSubListReqDto.setEndDate(coopPlanAccInfo.getCoopEndDate()); //额度到期日
                lmtSubListReqDto.setIsRevolv("1");//是否可循环 默认Y
                lmtSubListReqDto.setOprType("01");
                lmtSubList.add(lmtSubListReqDto);
            }//一般担保额度limitSubNo——>3,LimitSubName——>一般担保额度
        }
        //判断是否签订一般担保额度，把一般担保额度跟分项一块推送过去
        CoopPartnerAgrApp coopPartnerAgrApp1 = coopPartnerAgrAppMapper.selectByPrimaryKey(record.getCoopAgrSerno());
        if (coopPartnerAgrApp1 != null && "1".equals(coopPartnerAgrApp1.getIsCommonGrtLmtAmt())){
            QueryModel qmCoopPlanNo = new QueryModel();
            qmCoopPlanNo.addCondition("coopPlanNo",record.getCoopPlanNo());
            //更新批复台账表中一般担保额度
            List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(qmCoopPlanNo);
            CoopReplyAcc coopReplyAcc = null;
            if(!CollectionUtils.isEmpty(coopReplyAccList)){
                //一般担保额度信息推送放到额度系统
                coopReplyAcc = coopReplyAccList.get(0);
                CmisLmt0004LmtSubListReqDto lmtSubListReqDto = new CmisLmt0004LmtSubListReqDto();
                lmtSubListReqDto.setAccSubNo(coopReplyAcc.getReplyNo());
                lmtSubListReqDto.setLimitSubNo("2");
                lmtSubListReqDto.setLimitSubName("一般担保额度");
                lmtSubListReqDto.setAvlAmt(coopReplyAcc.getSignAmt());//授信金额--分项签订额度
                lmtSubListReqDto.setSigAmt(coopReplyAcc.getSingleCoopQuota());//单户限额
                lmtSubListReqDto.setSigBussAmt(coopReplyAcc.getSigBusiCoopQuota());//单笔业务限额
                lmtSubListReqDto.setCurType("CNY"); //币种 默认人民币
                lmtSubListReqDto.setTerm(coopReplyAcc.getCoopTerm()); //授信期限
                lmtSubListReqDto.setStartDate(coopReplyAcc.getCoopStartDate()); //额度起始日
                lmtSubListReqDto.setEndDate(coopReplyAcc.getCoopEndDate()); //额度到期日
                lmtSubListReqDto.setIsRevolv("1");//是否可循环 默认Y
                lmtSubListReqDto.setOprType("01");
                lmtSubList.add(lmtSubListReqDto);
            }


        }

        QueryModel queryModel3 = new QueryModel();
        queryModel3.addCondition("serno", record.getCoopAgrSerno());
        queryModel3.addCondition("coopPlanNo", coopPlanAccInfo.getCoopPlanNo());
        // 房地产类--项目信息
        List<CoopReplyAgrPro> coopReplyAgrProList = coopReplyAgrProMapper.selectByModel(queryModel3);
        if (!CollectionUtils.isEmpty(coopReplyAgrProList)) {
            for (CoopReplyAgrPro coopReplyAgrPro : coopReplyAgrProList) {
                CmisLmt0004LmtSubListReqDto lmtSubListReqDto = new CmisLmt0004LmtSubListReqDto();
                lmtSubListReqDto.setAccSubNo(coopReplyAgrPro.getProNo());// 授信分项编号-- 合作产品编号
//                lmtSubListReqDto.setOrigiAccSubNo();//原授信分项编号
//                lmtSubListReqDto.setParentId(); //父节点
                if (!(Integer.valueOf(coopPlanAccInfo.getPartnerType()) > 8)) { // 仅传前8种
                    lmtSubListReqDto.setLimitSubNo("1"); //授信品种编号
                    lmtSubListReqDto.setLimitSubName(coopReplyAgrPro.getProName()); // 授信品种名称
                }
                QueryModel qmSignAmt = new QueryModel();
                qmSignAmt.addCondition("proNo",coopReplyAgrPro.getProNo());
                BigDecimal signAmt = coopReplyAccProMapper.selectByModel(qmSignAmt).get(0).getSignAmt();

                lmtSubListReqDto.setAvlAmt(signAmt);//授信金额--项目额度
                lmtSubListReqDto.setSigAmt(coopPlanAccInfo.getSingleCoopQuota());//单户限额
                lmtSubListReqDto.setSigBussAmt(coopPlanAccInfo.getSigBusiCoopQuota());//单笔业务限额
                lmtSubListReqDto.setCurType("CNY"); //币种 默认人民币
                lmtSubListReqDto.setTerm(coopPlanAccInfo.getCoopTerm()); //授信期限
                lmtSubListReqDto.setStartDate(coopPlanAccInfo.getCoopStartDate()); //额度起始日
                lmtSubListReqDto.setEndDate(coopPlanAccInfo.getCoopEndDate()); //额度到期日
                lmtSubListReqDto.setIsRevolv("0");//是否可循环
                lmtSubListReqDto.setOprType("01");
                lmtSubList.add(lmtSubListReqDto);
            }
        }

        // 其他类--lmtSubList为空则复制方案基本信息里的数据
        if (CollectionUtils.isEmpty(lmtSubList)) {
            CmisLmt0004LmtSubListReqDto lmtSubListReqDto = new CmisLmt0004LmtSubListReqDto();
            lmtSubListReqDto.setAccSubNo(coopPlanAccInfo.getCoopPlanNo());// 授信分项编号-- 合作产品编号
//                lmtSubListReqDto.setOrigiAccSubNo();//原授信分项编号
//                lmtSubListReqDto.setParentId(); //父节点
            if (!(Integer.valueOf(coopPlanAccInfo.getPartnerType()) > 8)) { // 仅传前8种
                lmtSubListReqDto.setLimitSubNo(coopPlanAccInfo.getPartnerType()); //授信品种编号
                lmtSubListReqDto.setLimitSubName(PartnerTypeEnums.getDescByValue(coopPlanAccInfo.getPartnerType())); // 授信品种名称
            }
            //计算分项的签订金额，分项的签订金额+本次签订的金额
            BigDecimal signAmt = new BigDecimal(0);
            if(record.getCoopAgrAmt()!=null){
                signAmt = record.getCoopAgrAmt();
            }

            lmtSubListReqDto.setAvlAmt(signAmt);//授信金额--单个产品合作额度
            lmtSubListReqDto.setSigAmt(coopPlanAccInfo.getSingleCoopQuota());//单户限额
            lmtSubListReqDto.setSigBussAmt(coopPlanAccInfo.getSigBusiCoopQuota());//单笔业务限额
            lmtSubListReqDto.setCurType("CNY"); //币种 默认人民币
            lmtSubListReqDto.setTerm(coopPlanAccInfo.getCoopTerm()); //授信期限
            lmtSubListReqDto.setStartDate(coopPlanAccInfo.getCoopStartDate()); //额度起始日
            lmtSubListReqDto.setEndDate(coopPlanAccInfo.getCoopEndDate()); //额度到期日
            //光伏企业可循环，其他不可循环
            if("5".equals(coopPlanAccInfo.getPartnerType())){
                lmtSubListReqDto.setIsRevolv("0");//是否可循环
            }else{
                lmtSubListReqDto.setIsRevolv("1");//是否可循环
            }

            lmtSubListReqDto.setOprType("01");
            lmtSubList.add(lmtSubListReqDto);
        }
        reqDto.setLmtSubList(lmtSubList);

        // 2.2.3 适用机构
        QueryModel queryModel4 = new QueryModel();
        queryModel4.addCondition("serno", coopPlanAccInfo.getSerno());
        List<CoopPlanSuitOrgInfo> suirOrgList = coopPlanSuitOrgInfoService.selectByModel(queryModel4);
        List<CmisLmt0004StrOrgListReqDto> strOrgList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(suirOrgList)) {
            for (CoopPlanSuitOrgInfo coopPlanSuitOrgInfo : suirOrgList) {
                CmisLmt0004StrOrgListReqDto cmisLmt0004StrOrgListReqDto = new CmisLmt0004StrOrgListReqDto();
                cmisLmt0004StrOrgListReqDto.setAccNo(coopPlanSuitOrgInfo.getCoopPlanNo());
                cmisLmt0004StrOrgListReqDto.setBchCde(coopPlanSuitOrgInfo.getSuitOrgNo());
                cmisLmt0004StrOrgListReqDto.setBchDesc(coopPlanSuitOrgInfo.getSuitOrgName());
                cmisLmt0004StrOrgListReqDto.setOprType("01");
//                cmisLmt0004StrOrgListReqDto.setBchSupCde();
                strOrgList.add(cmisLmt0004StrOrgListReqDto);
            }
        }
        reqDto.setStrOrgList(strOrgList);

        // 2.3 发送请求
        log.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0004RespDto> result = cmisLmtClientService.cmisLmt0004(reqDto);
        log.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, JSON.toJSONString(result));
        // 3、判断调用结果，若失败直接抛异常回滚事务
        String resCode = result.getData().getErrorCode();
        String resMsg = result.getData().getErrorMsg();
        if ("0000".equals(resCode)) {
            log.info("合作方协议申请，协议编号【{}】；后处理调用额度接口【cmisLmt0004】同步信息成功！", record.getCoopAgrNo());
        } else {
            log.error("合作方协议申请，协议编号【{}】；后处理调用额度接口【cmisLmt0004】同步信息失败！失败原因【{}】", record.getCoopAgrNo(), resMsg);
            throw BizException.error(null, resCode, resMsg);
        }
    }
}
