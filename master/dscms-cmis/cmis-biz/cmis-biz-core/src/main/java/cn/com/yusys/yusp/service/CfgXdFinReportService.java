/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgXdFinReport;
import cn.com.yusys.yusp.repository.mapper.CfgXdFinReportMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgXdFinReportService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgXdFinReportService {

    @Autowired
    private CfgXdFinReportMapper cfgXdFinReportMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgXdFinReport selectByPrimaryKey(String codeNo) {
        return cfgXdFinReportMapper.selectByPrimaryKey(codeNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgXdFinReport> selectAll(QueryModel model) {
        List<CfgXdFinReport> records = (List<CfgXdFinReport>) cfgXdFinReportMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgXdFinReport> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgXdFinReport> list = cfgXdFinReportMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgXdFinReport record) {
        return cfgXdFinReportMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgXdFinReport record) {
        return cfgXdFinReportMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgXdFinReport record) {
        return cfgXdFinReportMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgXdFinReport record) {
        return cfgXdFinReportMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String codeNo) {
        return cfgXdFinReportMapper.deleteByPrimaryKey(codeNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgXdFinReportMapper.deleteByIds(ids);
    }

    /**
     * @创建人 shangzy
     * @创建时间 2021/8/18 22:30
     * @注释 根据prdId和type查询资产负债或损益明细信息 1 资产负债 2损益明细
     */
    public List<CfgXdFinReport> selectByPrd(String prdId){ return cfgXdFinReportMapper.selectByPrd(prdId);}
}
