package cn.com.yusys.yusp.dto;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class LmtReplySubDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 批复流水号
     **/
    private String replySerno;

    /**
     * 批复分项流水号
     **/
    private String replySubSerno;

    /**
     * 分项流水号
     **/
    private String subSerno;

    /**
     * 分项名称
     **/
    private String subName;

    /**
     * 客户号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 担保方式
     **/
    private String guarMode;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 预授信品种编号
     **/
    private String preLmtBizType;

    /**
     * 授信金额
     **/
    private java.math.BigDecimal lmtAmt;

    /**
     * 授信期限
     **/
    private Integer lmtTerm;

    /**
     * 是否预授信额度
     **/
    private String isPreLmt;

    /**
     * 原额度分项编号
     **/
    private String origiLmtAccSubNo;

    /**
     * 原额度分项金额
     **/
    private java.math.BigDecimal origiLmtAccSubAmt;

    /**
     * 原额度分项期限
     **/
    private Integer origiLmtAccSubTerm;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最近修改人
     **/
    private String updId;

    /**
     * 最近修改机构
     **/
    private String updBrId;

    /**
     * 最近修改日期
     **/
    private String updDate;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    /**
     * 分项下的分项产品
     **/
    private List<LmtReplySubPrdDto> children;

    /**
     * 页面用的授信分项额度编号
     **/
    private String lmtDrawNo;

    public List<LmtReplySubPrdDto> getChildren() {
        return children;
    }

    public void setChildren(List<LmtReplySubPrdDto> children) {
        this.children = children;
    }

    /**
     * 页面用的授信品种
     **/


    private String lmtDrawType;

    public String getLmtDrawNo() {
        return lmtDrawNo;
    }

    public void setLmtDrawNo(String lmtDrawNo) {
        this.lmtDrawNo = lmtDrawNo;
    }

    public String getLmtDrawType() {
        return lmtDrawType;
    }

    public void setLmtDrawType(String lmtDrawType) {
        this.lmtDrawType = lmtDrawType;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getReplySerno() {
        return replySerno;
    }

    public void setReplySerno(String replySerno) {
        this.replySerno = replySerno;
    }

    public String getReplySubSerno() {
        return replySubSerno;
    }

    public void setReplySubSerno(String replySubSerno) {
        this.replySubSerno = replySubSerno;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getPreLmtBizType() {
        return preLmtBizType;
    }

    public void setPreLmtBizType(String preLmtBizType) {
        this.preLmtBizType = preLmtBizType;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getOrigiLmtAccSubNo() {
        return origiLmtAccSubNo;
    }

    public void setOrigiLmtAccSubNo(String origiLmtAccSubNo) {
        this.origiLmtAccSubNo = origiLmtAccSubNo;
    }

    public BigDecimal getOrigiLmtAccSubAmt() {
        return origiLmtAccSubAmt;
    }

    public void setOrigiLmtAccSubAmt(BigDecimal origiLmtAccSubAmt) {
        this.origiLmtAccSubAmt = origiLmtAccSubAmt;
    }

    public Integer getOrigiLmtAccSubTerm() {
        return origiLmtAccSubTerm;
    }

    public void setOrigiLmtAccSubTerm(Integer origiLmtAccSubTerm) {
        this.origiLmtAccSubTerm = origiLmtAccSubTerm;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
