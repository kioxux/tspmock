/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.dto.GrtGuarContRelDto;
import cn.com.yusys.yusp.service.GrtGuarContRelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-13 15:27:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/grtguarcontrel")
public class GrtGuarContRelResource {
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GrtGuarContRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<GrtGuarContRel> list = grtGuarContRelService.selectAll(queryModel);
        return new ResultDto<List<GrtGuarContRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GrtGuarContRel>> index(QueryModel queryModel) {
        List<GrtGuarContRel> list = grtGuarContRelService.selectByModel(queryModel);
        return new ResultDto<List<GrtGuarContRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<GrtGuarContRel> show(@PathVariable("pkId") String pkId) {
        GrtGuarContRel grtGuarContRel = grtGuarContRelService.selectByPrimaryKey(pkId);
        return new ResultDto<GrtGuarContRel>(grtGuarContRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GrtGuarContRel> create(@RequestBody GrtGuarContRel grtGuarContRel) throws URISyntaxException {
        grtGuarContRelService.insert(grtGuarContRel);
        return new ResultDto<GrtGuarContRel>(grtGuarContRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GrtGuarContRel grtGuarContRel) throws URISyntaxException {
        int result = grtGuarContRelService.update(grtGuarContRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = grtGuarContRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = grtGuarContRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectGrtGuarContRelLinkGuarBaseInfo
     * @函数描述:查询担保合同和押品基本信息列表通过关系表关联
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectGrtGuarContRelLinkGuarBaseInfo")
    protected ResultDto<List<GrtGuarContRelDto>> selectGrtGuarContRelLinkGuarBaseInfo(@RequestBody QueryModel queryModel) {
        List<GrtGuarContRelDto> list = grtGuarContRelService.selectGrtGuarContRelLinkGuarBaseInfo(queryModel);
        return new ResultDto<List<GrtGuarContRelDto>>(list);
    }

    /**
     * @函数名称:creteGrtGuarContRel
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增")
    @PostMapping("/creteGrtGuarContRel")
    protected ResultDto<Integer> creteGrtGuarContRel(@RequestBody GrtGuarContRel grtGuarContRel)  {
        grtGuarContRel.setPkId(UUID.randomUUID().toString());
        int result  =  grtGuarContRelService.insert(grtGuarContRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<GrtGuarContRel>> selectByModel(@RequestBody Map map) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("guarContNo",map.get("guarContNo"));
        queryModel.addCondition("guarNo",map.get("guarNo"));
        queryModel.addCondition("oprType",map.get("oprType"));
        List<GrtGuarContRel> list = grtGuarContRelService.selectByModel(queryModel);
        return new ResultDto<List<GrtGuarContRel>>(list);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatecertiamt")
    protected ResultDto<Integer> updateCertiAmt(@RequestBody QueryModel queryModel) {
        int result = grtGuarContRelService.updateCertiAmt(queryModel);
        return new ResultDto<>(result);
    }
}
