package cn.com.yusys.yusp.web.server.xdxw0077;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0077.req.Xdxw0077DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0077.resp.Xdxw0077DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0077.Xdxw0077Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.util.Arrays;

/**
 * 接口处理类:渠道端查询我的贷款（流程监控）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0077:渠道端查询我的贷款（流程监控）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0077Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0077Resource.class);

    @Autowired
    private Xdxw0077Service xdxw0077Service;
    /**
     * 交易码：xdxw0077
     * 交易描述：渠道端查询我的贷款（流程监控）
     *
     * @param xdxw0077DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("渠道端查询我的贷款（流程监控）")
    @PostMapping("/xdxw0077")
    protected @ResponseBody
    ResultDto<Xdxw0077DataRespDto> xdxw0077(@Validated @RequestBody Xdxw0077DataReqDto xdxw0077DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataReqDto));
        Xdxw0077DataRespDto xdxw0077DataRespDto = new Xdxw0077DataRespDto();// 响应Dto:渠道端查询我的贷款（流程监控）
        ResultDto<Xdxw0077DataRespDto> xdxw0077DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0077DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataReqDto));
            xdxw0077DataRespDto = xdxw0077Service.getXdxw0077(xdxw0077DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataRespDto));

            // 封装xdxw0077DataResultDto中正确的返回码和返回信息
            xdxw0077DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0077DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }  catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdxw0077DataResultDto.setCode(e.getErrorCode());
            xdxw0077DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, e.getMessage());
            // 封装xdxw0077DataResultDto中异常返回码和返回信息
            xdxw0077DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0077DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0077DataRespDto到xdxw0077DataResultDto中
        xdxw0077DataResultDto.setData(xdxw0077DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataResultDto));
        return xdxw0077DataResultDto;
    }
}
