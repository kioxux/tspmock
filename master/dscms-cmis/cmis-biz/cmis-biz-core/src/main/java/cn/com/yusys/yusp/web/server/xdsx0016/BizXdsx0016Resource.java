package cn.com.yusys.yusp.web.server.xdsx0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0016.req.Xdsx0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.resp.Xdsx0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0016.Xdsx0016Service;
import cn.com.yusys.yusp.service.server.xdsx0018.Xdsx0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:抵押查封信息同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDSX0016:抵押查封信息同步")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0016Resource.class);
    @Autowired
    private Xdsx0016Service xdsx0016Service;
    /**
     * 交易码：xdsx0016
     * 交易描述：抵押查封信息同步
     *
     * @param xdsx0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押查封信息同步")
    @PostMapping("/xdsx0016")
    protected @ResponseBody
    ResultDto<Xdsx0016DataRespDto> xdsx0016(@Validated @RequestBody Xdsx0016DataReqDto xdsx0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016DataReqDto));
        Xdsx0016DataRespDto xdsx0016DataRespDto = new Xdsx0016DataRespDto();// 响应Dto:抵押查封信息同步
        ResultDto<Xdsx0016DataRespDto> xdsx0016DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0016DataReqDto获取业务值进行业务逻辑处理
            // 调用xdsx0016Service层开始
            xdsx0016DataRespDto = xdsx0016Service.xdsx0016(xdsx0016DataReqDto);
            // 调用xdsx0016Service层结束
            // 封装xdsx0016DataResultDto中正确的返回码和返回信息
            xdsx0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, e.getMessage());
            // 封装xdsx0016DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdsx0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdsx0016DataRespDto到xdsx0016DataResultDto中
        xdsx0016DataResultDto.setData(xdsx0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016DataRespDto));
        return xdsx0016DataResultDto;
    }
}
