package cn.com.yusys.yusp.web.server.xdtz0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0014.Xdtz0014Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0014.req.Xdtz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.resp.Xdtz0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:保证金补交/冲补交结果通知服务
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0014:保证金补交/冲补交结果通知服务")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0014Resource<Xdc> {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0014Resource.class);

    @Autowired
    private Xdtz0014Service xdtz0014Service;
    /**
     * 交易码：xdtz0014
     * 交易描述：保证金补交/冲补交结果通知服务
     *
     * @param xdtz0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金补交/冲补交结果通知服务")
    @PostMapping("/xdtz0014")
    protected @ResponseBody
    ResultDto<Xdtz0014DataRespDto> xdtz0014(@Validated @RequestBody Xdtz0014DataReqDto xdtz0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014DataReqDto));
        Xdtz0014DataRespDto xdtz0014DataRespDto = new Xdtz0014DataRespDto();// 响应Dto:保证金补交/冲补交结果通知服务
        ResultDto<Xdtz0014DataRespDto> xdtz0014DataResultDto = new ResultDto<>();
        String seqNo = xdtz0014DataReqDto.getSeqNo();//批次号
        String oprId = xdtz0014DataReqDto.getOprId();//操作码
        String coreBailPaySerNo = xdtz0014DataReqDto.getCoreBailPaySerNo();//核心保证金缴纳流水号
        String contNo = xdtz0014DataReqDto.getContNo();//银承合同编号
        String bailType = xdtz0014DataReqDto.getBailType();//保证金类型
        String bailAcctNo = xdtz0014DataReqDto.getBailAcctNo();//保证金账号
        String bailCurType = xdtz0014DataReqDto.getBailCurType();//保证金币种
        String bailAmt = xdtz0014DataReqDto.getBailAmt();//保证金金额
        String intType = xdtz0014DataReqDto.getIntType();//计息方式
        String bailIntAcctNo = xdtz0014DataReqDto.getBailIntAcctNo();//保证金利息存入账号
        String rqstrAcctName = xdtz0014DataReqDto.getRqstrAcctName();//申请人户名
        String rqstrAcctNo = xdtz0014DataReqDto.getRqstrAcctNo();//申请人账号
        String endDate = xdtz0014DataReqDto.getEndDate();//到期日期
        String drfpoIsseMk = xdtz0014DataReqDto.getDrfpoIsseMk();//票据池出票标记
        String drftNo = xdtz0014DataReqDto.getDrftNo();//票号
        BigDecimal drftAmt = xdtz0014DataReqDto.getDrftAmt();//票面金额
        try {
            // 从xdtz0014DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdtz0014DataReqDto));
            xdtz0014DataRespDto = xdtz0014Service.bzjbjInfo(xdtz0014DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdtz0014DataReqDto));
            // TODO 调用XXXXXService层结束
            // TODO 封装xdtz0014DataRespDto对象开始
//            xdtz0014DataRespDto.setOpFlag(StringUtils.EMPTY);// 成功失败标志
//            xdtz0014DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdtz0014DataRespDto对象结束
            // 封装xdtz0014DataResultDto中正确的返回码和返回信息
            xdtz0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, e.getMessage());
            // 封装xdtz0014DataResultDto中异常返回码和返回信息
            xdtz0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0014DataRespDto到xdtz0014DataResultDto中
        xdtz0014DataResultDto.setData(xdtz0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014DataResultDto));
        return xdtz0014DataResultDto;
    }
}
