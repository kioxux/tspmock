/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtCarSaleBrhRel;
import cn.com.yusys.yusp.repository.mapper.LmtCarSaleBrhRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarSaleBrhRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 11:45:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCarSaleBrhRelService {

    @Autowired
    private LmtCarSaleBrhRelMapper lmtCarSaleBrhRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtCarSaleBrhRel selectByPrimaryKey(String pkId) {
        return lmtCarSaleBrhRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtCarSaleBrhRel> selectAll(QueryModel model) {
        List<LmtCarSaleBrhRel> records = (List<LmtCarSaleBrhRel>) lmtCarSaleBrhRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtCarSaleBrhRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCarSaleBrhRel> list = lmtCarSaleBrhRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtCarSaleBrhRel record) {
        return lmtCarSaleBrhRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtCarSaleBrhRel record) {
        return lmtCarSaleBrhRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtCarSaleBrhRel record) {
        return lmtCarSaleBrhRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtCarSaleBrhRel record) {
        return lmtCarSaleBrhRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtCarSaleBrhRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtCarSaleBrhRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtCarSaleBrhRel> selectBySerno(String serno) {
        return lmtCarSaleBrhRelMapper.selectBySerno(serno);
    }

}
