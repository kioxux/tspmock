/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import cn.com.yusys.yusp.dto.RepayBillRellDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRelMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRellMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpJxhjRepayLoan;
import cn.com.yusys.yusp.repository.mapper.PvpJxhjRepayLoanMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpJxhjRepayLoanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-22 19:18:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpJxhjRepayLoanService {

    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppService.class);

    @Autowired
    private PvpJxhjRepayLoanMapper pvpJxhjRepayLoanMapper;

    @Autowired
    private PvpLoanAppRepayBillRellMapper pvpLoanAppRepayBillRellMapper;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpJxhjRepayLoan selectByPrimaryKey(String pkId) {
        return pvpJxhjRepayLoanMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PvpJxhjRepayLoan> selectAll(QueryModel model) {
        List<PvpJxhjRepayLoan> records = (List<PvpJxhjRepayLoan>) pvpJxhjRepayLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpJxhjRepayLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpJxhjRepayLoan> list = pvpJxhjRepayLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpJxhjRepayLoan record) {
        return pvpJxhjRepayLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insertSelective(PvpJxhjRepayLoan record) {
        return pvpJxhjRepayLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpJxhjRepayLoan record) {
        return pvpJxhjRepayLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(PvpJxhjRepayLoan record) {
        return pvpJxhjRepayLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pvpJxhjRepayLoanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpJxhjRepayLoanMapper.deleteByIds(ids);
    }

    public PvpJxhjRepayLoan queryPvpJxhjRepayLoanDataByParams(HashMap<String, String> queryData) {
        return pvpJxhjRepayLoanMapper.queryPvpJxhjRepayLoanDataByParams(queryData);
    }

    public Map ClaJxhjLoanRepay(QueryModel queryModel) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey((String) queryModel.getCondition().get("serno"));
            if(Objects.isNull(pvpLoanApp)){
                log.error("根据出账流水号"+(String) queryModel.getCondition().get("serno")+"未找到出账申请信息！");
                throw BizException.error(null,EcbEnum.ECB010059.key,EcbEnum.ECB010059.value);
            }
            List<RepayBillRellDto> ctrList = pvpLoanAppRepayBillRellMapper.selectRepayBillRellByParams(queryModel);
            for (int i = 0; i < ctrList.size(); i++) {
                RepayBillRellDto repayBillRellDto = ctrList.get(i);
                Ln3100ReqDto Ln3100ReqDto = new Ln3100ReqDto();
                Ln3100ReqDto.setDkjiejuh(repayBillRellDto.getBillNo());
                log.info("调核心3100接口查询借据【{}】信息开始，请求报文为:【{}】", repayBillRellDto.getBillNo(), Ln3100ReqDto.toString());
                ResultDto<Ln3100RespDto> resultDto = dscms2CoreLnClientService.ln3100(Ln3100ReqDto);
                log.info("调核心3100接口查询借据【{}】信息结束，请求报文为:【{}】", repayBillRellDto.getBillNo(), resultDto.toString());
                String code = resultDto.getCode();
                if (code.equals("0")) {
                    String v_loan_no = resultDto.getData().getDkjiejuh();//贷款账号
                    BigDecimal v_prcp = resultDto.getData().getYuqibjin();//逾期本金
                    BigDecimal v_int = resultDto.getData().getYsqianxi();//应收欠息
                    BigDecimal zuigdkye = resultDto.getData().getZuigdkye();//最高贷款余额
                    BigDecimal v_od_int = resultDto.getData().getYshofaxi();//应收罚息
                    BigDecimal v_comm_od_int = resultDto.getData().getYingjifx();//应计复息
                    BigDecimal v_next_int = resultDto.getData().getYsyjlixi();//应收应计利息
                    BigDecimal next_prcp = resultDto.getData().getZhchbjin();//正常本金
                    BigDecimal loanAmt = Optional.ofNullable(pvpLoanApp.getCvtCnyAmt()).orElse(BigDecimal.ZERO);
                    String selfPrcp = (next_prcp.add(v_prcp)).subtract(loanAmt).toString();//自还本金
                    String selfInt = v_int.add(v_od_int).add(v_comm_od_int).add(v_next_int).toString();//自还利息
                    String newLoanAmt = loanAmt.toString();
                    HashMap<String, String> queryData = new HashMap<String, String>();
                    queryData.put("serno", (String) queryModel.getCondition().get("serno"));
                    //queryData.put("loanNo",v_loan_no);
                    PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanMapper.queryPvpJxhjRepayLoanDataByParams(queryData);
                    if (pvpJxhjRepayLoan != null) {
                        AccLoan accloan = accLoanService.queryAccLoanDataByBillNo(v_loan_no);
                        String pkId = StringUtils.uuid(false);
                        pvpJxhjRepayLoan.setPkId(pkId);//主键
                        pvpJxhjRepayLoan.setSerno((String) queryModel.getCondition().get("serno"));//业务申请流水号
                        pvpJxhjRepayLoan.setBillNo(v_loan_no);//借据编号
                        pvpJxhjRepayLoan.setPrcp(v_prcp.toString());//拖欠本金
                        pvpJxhjRepayLoan.setNormInt(v_int.toString());//拖欠利息
                        pvpJxhjRepayLoan.setOdInt(v_od_int.toString());//拖欠罚息
                        pvpJxhjRepayLoan.setCommOdInt(v_comm_od_int.toString());//拖欠复利
                        pvpJxhjRepayLoan.setNextPrcp(next_prcp.toString());//未到期本金
                        pvpJxhjRepayLoan.setNextInt(v_next_int.toString());//应计利息
                        pvpJxhjRepayLoan.setSelfPrcp(selfPrcp);//自还本金
                        pvpJxhjRepayLoan.setSelfInt(selfInt);//自还利息
                        pvpJxhjRepayLoan.setNewLoanAmt(newLoanAmt);//借新本金
                        pvpJxhjRepayLoan.setCheckStatus("000");//试算检查状态000未验证,111已经验证
                        if (accloan != null) {
                            pvpJxhjRepayLoan.setLoanStartDate(accloan.getLoanStartDate());//贷款起始日期
                            pvpJxhjRepayLoan.setLoanEndDate(accloan.getLoanEndDate());//贷款到期日期
                            pvpJxhjRepayLoan.setAccStatus(accloan.getAccStatus());//贷款台帐状态
                            pvpJxhjRepayLoan.setLoanAmt(accloan.getLoanAmt().toString());//贷款金额
                            pvpJxhjRepayLoan.setLoanBalance(accloan.getLoanBalance().toString());//贷款余额
                        }

                        User userInfo = SessionUtils.getUserInformation();
                        pvpJxhjRepayLoan.setInputId(userInfo.getLoginCode());
                        pvpJxhjRepayLoan.setInputBrId(userInfo.getOrg().getCode());
                        pvpJxhjRepayLoan.setManagerId(userInfo.getLoginCode());
                        pvpJxhjRepayLoan.setManagerBrId(userInfo.getOrg().getCode());
                        pvpJxhjRepayLoan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        this.updateSelective(pvpJxhjRepayLoan);
                    } else {
                        pvpJxhjRepayLoan = new PvpJxhjRepayLoan();
                        AccLoan accloan = accLoanService.queryAccLoanDataByBillNo(v_loan_no);
                        String pkId = StringUtils.uuid(false);
                        pvpJxhjRepayLoan.setPkId(pkId);//主键
                        pvpJxhjRepayLoan.setSerno((String) queryModel.getCondition().get("serno"));//业务申请流水号
                        pvpJxhjRepayLoan.setBillNo(v_loan_no);//借据编号
                        pvpJxhjRepayLoan.setPrcp(v_prcp.toString());//拖欠本金
                        pvpJxhjRepayLoan.setNormInt(v_int.toString());//拖欠利息
                        pvpJxhjRepayLoan.setOdInt(v_od_int.toString());//拖欠罚息
                        pvpJxhjRepayLoan.setCommOdInt(v_comm_od_int.toString());//拖欠复利
                        pvpJxhjRepayLoan.setNextPrcp(next_prcp.toString());//未到期本金
                        pvpJxhjRepayLoan.setNextInt(v_next_int.toString());//应计利息
                        pvpJxhjRepayLoan.setSelfPrcp(selfPrcp);//自还本金
                        pvpJxhjRepayLoan.setSelfInt(selfInt);//自还利息
                        pvpJxhjRepayLoan.setNewLoanAmt(newLoanAmt);//借新本金
                        pvpJxhjRepayLoan.setCheckStatus("000");//试算检查状态000未验证,111已经验证
                        if (accloan != null) {
                            pvpJxhjRepayLoan.setLoanStartDate(accloan.getLoanStartDate());//贷款起始日期
                            pvpJxhjRepayLoan.setLoanEndDate(accloan.getLoanEndDate());//贷款到期日期
                            pvpJxhjRepayLoan.setAccStatus(accloan.getAccStatus());//贷款台帐状态
                            pvpJxhjRepayLoan.setLoanAmt(accloan.getLoanAmt().toString());//贷款金额
                            pvpJxhjRepayLoan.setLoanBalance(accloan.getLoanBalance().toString());//贷款余额
                        }
                        User userInfo = SessionUtils.getUserInformation();

                        pvpJxhjRepayLoan.setInputId(Objects.nonNull(userInfo) ? userInfo.getLoginCode() : pvpLoanApp.getInputId());
                        pvpJxhjRepayLoan.setInputBrId(Objects.nonNull(userInfo) ? userInfo.getOrg().getCode() : pvpLoanApp.getInputBrId());
                        pvpJxhjRepayLoan.setManagerId(Objects.nonNull(userInfo) ? userInfo.getLoginCode() : pvpLoanApp.getManagerId());
                        pvpJxhjRepayLoan.setManagerBrId(Objects.nonNull(userInfo) ? userInfo.getOrg().getCode() : pvpLoanApp.getManagerBrId());
                        pvpJxhjRepayLoan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        this.insertSelective(pvpJxhjRepayLoan);
                    }
                } else {
                    rtnCode = resultDto.getCode();
                    rtnMsg = resultDto.getMessage();
                    break;
                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }
}

