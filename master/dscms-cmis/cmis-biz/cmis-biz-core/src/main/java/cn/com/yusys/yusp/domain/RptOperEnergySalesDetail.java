/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySalesDetail
 * @类描述: rpt_oper_energy_sales_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:49:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_energy_sales_detail")
public class RptOperEnergySalesDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 产品品种 **/
	@Column(name = "PRODUCT_TYPE", unique = false, nullable = true, length = 5)
	private String productType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;
	
	/** 上年销售金额/今年销售金额 **/
	@Column(name = "LAST_CURR_SALE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastCurrSaleAmt;
	
	/** 占比 **/
	@Column(name = "PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal perc;
	
	/** 结算方式及账期 **/
	@Column(name = "SETTLEMENT_METHOD", unique = false, nullable = true, length = 5)
	private String settlementMethod;
	
	/** 结账周期 **/
	@Column(name = "CLOS_CYCLE", unique = false, nullable = true, length = 5)
	private String closCycle;
	
	/** 合作年限 **/
	@Column(name = "COOP_YEAR", unique = false, nullable = true, length = 10)
	private Integer coopYear;
	
	/** 价格波动情况 **/
	@Column(name = "PRICE_WAVE_CASE", unique = false, nullable = true, length = 65535)
	private String priceWaveCase;
	
	/** 年末应收账款余额/目前应收账款 **/
	@Column(name = "LAST_CURR_ACCOUANT_REVICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastCurrAccouantRevice;
	
	/** 市场渠道 **/
	@Column(name = "MARKET_CHANNEL", unique = false, nullable = true, length = 5)
	private String marketChannel;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 项目 **/
	@Column(name = "PROJECT", unique = false, nullable = true, length = 65535)
	private String project;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param productType
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
    /**
     * @return productType
     */
	public String getProductType() {
		return this.productType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param lastCurrSaleAmt
	 */
	public void setLastCurrSaleAmt(java.math.BigDecimal lastCurrSaleAmt) {
		this.lastCurrSaleAmt = lastCurrSaleAmt;
	}
	
    /**
     * @return lastCurrSaleAmt
     */
	public java.math.BigDecimal getLastCurrSaleAmt() {
		return this.lastCurrSaleAmt;
	}
	
	/**
	 * @param perc
	 */
	public void setPerc(java.math.BigDecimal perc) {
		this.perc = perc;
	}
	
    /**
     * @return perc
     */
	public java.math.BigDecimal getPerc() {
		return this.perc;
	}
	
	/**
	 * @param settlementMethod
	 */
	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}
	
    /**
     * @return settlementMethod
     */
	public String getSettlementMethod() {
		return this.settlementMethod;
	}
	
	/**
	 * @param closCycle
	 */
	public void setClosCycle(String closCycle) {
		this.closCycle = closCycle;
	}
	
    /**
     * @return closCycle
     */
	public String getClosCycle() {
		return this.closCycle;
	}
	
	/**
	 * @param coopYear
	 */
	public void setCoopYear(Integer coopYear) {
		this.coopYear = coopYear;
	}
	
    /**
     * @return coopYear
     */
	public Integer getCoopYear() {
		return this.coopYear;
	}
	
	/**
	 * @param priceWaveCase
	 */
	public void setPriceWaveCase(String priceWaveCase) {
		this.priceWaveCase = priceWaveCase;
	}
	
    /**
     * @return priceWaveCase
     */
	public String getPriceWaveCase() {
		return this.priceWaveCase;
	}
	
	/**
	 * @param lastCurrAccouantRevice
	 */
	public void setLastCurrAccouantRevice(java.math.BigDecimal lastCurrAccouantRevice) {
		this.lastCurrAccouantRevice = lastCurrAccouantRevice;
	}
	
    /**
     * @return lastCurrAccouantRevice
     */
	public java.math.BigDecimal getLastCurrAccouantRevice() {
		return this.lastCurrAccouantRevice;
	}
	
	/**
	 * @param marketChannel
	 */
	public void setMarketChannel(String marketChannel) {
		this.marketChannel = marketChannel;
	}
	
    /**
     * @return marketChannel
     */
	public String getMarketChannel() {
		return this.marketChannel;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param project
	 */
	public void setProject(String project) {
		this.project = project;
	}
	
    /**
     * @return project
     */
	public String getProject() {
		return this.project;
	}


}