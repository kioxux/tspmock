/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.req.YpztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.YpztcxRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.GuarGcfAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGcfAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-09 14:06:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarGcfAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private GuarGcfAppMapper guarGcfAppMapper;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;
    @Autowired
    private GuarGcfBookService guarGcfBookService;
    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;
    @Autowired
    private GrtGuarContService grtGuarContService;
    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private SxkdDrawCheckService sxkdDrawCheckService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarGcfApp selectByPrimaryKey(String pkId) {
        return guarGcfAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarGcfApp> selectAll(QueryModel model) {
        List<GuarGcfApp> records = (List<GuarGcfApp>) guarGcfAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarGcfApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarGcfApp> list = guarGcfAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarGcfApp record) {
        // 获取创建，修改日期
        record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        int count = guarGcfAppMapper.insert(record);
        if (count != 1) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷企业提款押品查封审核新增失败！");
        }
        return count;
    }

    /**
     * @方法名称: insertGuarGcfApp
     * @方法描述: 省心快贷企业提款押品查封审核新增下一步落表校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map insertGuarGcfApp(GuarGcfApp record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        // 合同余额
        BigDecimal contBalance = BigDecimal.ZERO;
        String pkId = "";
        String contNo = "";
        try {
            // 获取借款合同
            contNo = record.getContNo();
            // 根据合同号获取合同余额
            contBalance = ctrLoanContService.getCanOutAccountAmt(contNo);
            // 将获取到最新的合同余额更新到表中
            record.setContBalance(contBalance);
            // 通过借款合同号查询合同项下关联抵押物是否有未入库的
            // 通过合同编号开始查找所有担保合同
            QueryModel queryModel1 = new QueryModel();
            QueryModel queryModel2 = new QueryModel();
            queryModel1.addCondition("contNo", contNo);
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectGuarContByContNo(queryModel1);

            if (CollectionUtils.isEmpty(grtGuarContList)) {
                rtnCode = EcbEnum.ECB020053.key;
                rtnMsg = EcbEnum.ECB020053.value;
                return rtnData;
            }
            for (GrtGuarCont grtGuarCont : grtGuarContList) {
                if (Objects.equals(CmisCommonConstants.GUAR_MODE_10, grtGuarCont.getGuarWay()) || Objects.equals(CmisCommonConstants.GUAR_MODE_20, grtGuarCont.getGuarWay())) {
                    //根据担保合同编号查询核心担保编号
                    queryModel2.addCondition("guarContNo", grtGuarCont.getGuarContNo());
                    queryModel2.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                    List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectAll(queryModel2);

                    if (!CollectionUtils.isEmpty(guarContRelWarrants)) {
                        for (GuarContRelWarrant guarContRelWarrant : guarContRelWarrants) {
                            //核心担保编号
                            String coreGuarantyNo = guarContRelWarrant.getCoreGuarantyNo();
                            GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

                            if (guarWarrantInfo!=null) {
                                String certiState = guarWarrantInfo.getCertiState();
                                //权证状态 04--入库已记账
                                if (!"04".equals(certiState) && !"13".equals(certiState) && !"14".equals(certiState)) {
                                    rtnCode = EcbEnum.ECB020032.key;
                                    rtnMsg = EcbEnum.ECB020032.value;
                                    return rtnData;
                                }
                            }
                        }
                    }
                }
            }

            /**
             *  若已全部入库，调用接口(bare01)苏州在线抵押校验房产的查封查验状态 接口返回：0000查到未查封  0001未查到信息 9999存在查封
             *  1、在线查封查验通过，提示，抵押物查封查验已通过，无需在进行人工查询，跳过人工审核页面，直接更新为审批通过
             *  2、存在查封冻结记录，弹出提示框，存在抵押物被查封无法放款，流程结束
             *  3、存在未查询到的，下一步正常跳转人工查封查验流程，由客户经理上传
             */
            log.info("信贷押品状态查询开始,合同号【{}】", contNo);
            YpztcxReqDto ypztcxReqDto = new YpztcxReqDto();
            ypztcxReqDto.setDistco("GJ"); // 国结调用此接口
            ypztcxReqDto.setCertnu("1"); //不动产权证书号 调用接口国结时，老代码逻辑此字段未使用因校验必输，传值1
            ypztcxReqDto.setMocenu(contNo); //不动产权登记证明号
            log.info("信贷押品状态查询请求：" + ypztcxReqDto);
            ResultDto<YpztcxRespDto> ypztcxReqResultDto = dscms2YphsxtClientService.ypztcx(ypztcxReqDto);
            log.info("信贷押品状态查询返回：" + ypztcxReqResultDto);
            String ypztcxCode = Optional.ofNullable(ypztcxReqResultDto.getCode()).orElse(StringUtils.EMPTY);
            String ypztcxMeesage = Optional.ofNullable(ypztcxReqResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("信贷押品状态查询返回信息：" + ypztcxMeesage);
            // 生成新流水
            pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            record.setPkId(pkId);
            // 接口返回信息
            //0 - 交易成功(查到未查封)
            //0001 - 抵押物编号：""+guar+"" 未查询到信息！
            //5555 - 抵押物编号：""+guarno+"" 存在查封情况！
            //0002 - 合同编号："+mocenu+" 未查询到信息！
            //0003 - 抵押物类型非不动产类型！
            //9999 - 通讯异常等信息
            // 如果查到未查封，则自动审批通过,查封登记簿落表
            if (Objects.equals(ypztcxCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                this.insert(record);
                // 省心快贷查封登记簿落表
                // 获取营业日期
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                GuarGcfBook guarGcfBook = new GuarGcfBook();
                guarGcfBook.setSerno(record.getPkId());//主键
                guarGcfBook.setCusId(record.getCusId());//客户号
                guarGcfBook.setContNo(record.getContNo());//合同号
                guarGcfBook.setAuditDate(openday);//审核日期
                guarGcfBook.setManagerId(record.getManagerId());//操作人
                guarGcfBook.setApprId("admin");//审批人
                guarGcfBook.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                guarGcfBook.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                int count =  guarGcfBookService.insert(guarGcfBook);
                if (count != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷查封登记簿新增失败！");
                }
                rtnCode = EcbEnum.ECB020030.key;
                rtnMsg = EcbEnum.ECB020030.value;

            } else if ((Objects.equals(ypztcxCode, "0001")) || (Objects.equals(ypztcxCode, "0002"))){
                // 未查询到信息，保存数据，进行人工审批
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                this.insert(record);
                rtnCode = EcbEnum.ECB020031.key;
                rtnMsg = EcbEnum.ECB020031.value;
            } else if (Objects.equals(ypztcxCode, "5555")) {
                // 查询到有冻结信息
                rtnCode = EcbEnum.ECB020033.key;
                rtnMsg = EcbEnum.ECB020033.value;
            } else {
                rtnCode = ypztcxCode;
                rtnMsg = ypztcxMeesage;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存省心快贷企业提款押品查封审核数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("pkId", pkId);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 获取基本信息
     *
     * @param pkId
     * @return
     */
    public GuarGcfApp selectBySerno(String pkId) {
        // 合同余额
        BigDecimal contBalance = BigDecimal.ZERO;
        String contNo = "";
        GuarGcfApp guarGcfApp = new GuarGcfApp();
        try{
            guarGcfApp = guarGcfAppMapper.selectByPrimaryKey(pkId);
            // 获取借款合同
            contNo = guarGcfApp.getContNo();
            // 根据合同号获取合同余额
            contBalance = sxkdDrawCheckService.getContBalance(contNo);
            // 将获取到最新的合同余额更新到表中
            guarGcfApp.setContBalance(contBalance);
        } catch (Exception e) {
            log.error("查询省心快贷企业提款押品查封审核详情数据出现异常！", e);
        }
        return guarGcfApp;
    }

    /**
     * @方法名称: guarGcfApplist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（省心快贷企业提款押品查封审核申请列表）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GuarGcfApp> guarGcfApplist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return guarGcfAppMapper.selectByModel(model);
    }
    /**

     * @方法名称: guarGcfAppHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（省心快贷企业提款押品查封审核申请列表）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GuarGcfApp> guarGcfAppHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return guarGcfAppMapper.selectByModel(model);
    }



    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarGcfApp record) {
        return guarGcfAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarGcfApp record) {
        return guarGcfAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarGcfApp record) {
        return guarGcfAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return guarGcfAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarGcfAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: guarGcfAppdelete
     * @方法描述: 根据主键做逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map guarGcfAppdelete(GuarGcfApp guarGcfApp) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String pkId = guarGcfApp.getPkId();
            // 如果是打回则自行退出，不是打回自行删除
            if(Objects.equals(CmisCommonConstants.WF_STATUS_992,guarGcfApp.getApproveStatus())){
                // 修改状态为自行退出
                guarGcfApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                guarGcfAppMapper.updateByPrimaryKeySelective(guarGcfApp);
                workflowCoreClient.deleteByBizId(guarGcfApp.getPkId());
            }else {
                log.info(String.format("根据主键%s对省心快贷企业提款押品查封审核申请列表进行删除", pkId));
                guarGcfAppMapper.deleteByPrimaryKey(pkId);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除省心快贷企业提款押品查封审核申请列表信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }
}
