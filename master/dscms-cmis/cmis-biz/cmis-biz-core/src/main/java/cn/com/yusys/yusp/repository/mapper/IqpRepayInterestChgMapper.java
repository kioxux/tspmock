package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.domain.IqpRepayInterestChg;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

@Mapper
public interface IqpRepayInterestChgMapper {
    /**
     * 新增利息减免申请数据
     * @param iqpRepayInterestChg
     * @return
     */
    int insert(IqpRepayInterestChg iqpRepayInterestChg);

    IqpRepayInterestChg selectByPrimaryKey(IqpRepayInterestChg iqpRepayInterestChg);

    int checkIsExistChgBizByBillNo(HashMap<String, String> param);


    void handleBusinessDataAfterStart(IqpRepayInterestChg iqpRepayInterestChg);

    int updateByPrimaryKey(IqpRepayInterestChg iqpRepayInterestChg);

    int updateYtgByPrimaryKey(IqpRepayInterestChg iqpRepayInterestChg);

    int handleBusinessDataAfteCallBack(IqpRepayInterestChg iqpRepayInterestChg);

    int handleBusinessDataAfteTackBack(IqpRepayInterestChg iqpRepayInterestChg);

    int handleBusinessDataAfterRefuse(IqpRepayInterestChg iqpRepayInterestChg);

    /**
     * 根据对象当中的非空字段更新申请修改信息
     * @param iqpRepayInterestChg
     * @return
     */
    int updateCommitByPrimaryKey(IqpRepayInterestChg iqpRepayInterestChg);
}
