/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.domain.LmtReplyLoanCond;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyChgCond;
import cn.com.yusys.yusp.repository.mapper.LmtReplyChgCondMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgCondService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-26 14:34:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyChgCondService {

    @Autowired
    private LmtReplyChgCondMapper lmtReplyChgCondMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplyChgCond selectByPrimaryKey(String pkId) {
        return lmtReplyChgCondMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplyChgCond> selectAll(QueryModel model) {
        List<LmtReplyChgCond> records = (List<LmtReplyChgCond>) lmtReplyChgCondMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyChgCond> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyChgCond> list = lmtReplyChgCondMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyChgCond lmtReplyChgCond) {
        lmtReplyChgCond.setPkId(UUID.randomUUID().toString());
        lmtReplyChgCond.setCreateTime(new Date());
        lmtReplyChgCond.setUpdateTime(new Date());
        return lmtReplyChgCondMapper.insert(lmtReplyChgCond);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyChgCond record) {
        return lmtReplyChgCondMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtReplyChgCond record) {
        return lmtReplyChgCondMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyChgCond record) {
        return lmtReplyChgCondMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyChgCondMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyChgCondMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：insertList
     * @方法描述：插入集合
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-27 下午 3:59
     * @修改记录：修改时间 修改人员  修改原因
     */
    public int insertList(List<LmtReplyChgCond> lmtReplyChgCondList) {
        return lmtReplyChgCondMapper.insertList(lmtReplyChgCondList);
    }

    /**
     * @方法名称: queryLmtReplyLoanCondByParams
     * @方法描述: 通过条件查询授信批复变更用信条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyChgCond> queryLmtReplyChgCondByParams(HashMap<String, String> lmtReplyLoanCondQueryMap) {
        return lmtReplyChgCondMapper.queryLmtReplyChgCondByParams(lmtReplyLoanCondQueryMap);
    }

}
