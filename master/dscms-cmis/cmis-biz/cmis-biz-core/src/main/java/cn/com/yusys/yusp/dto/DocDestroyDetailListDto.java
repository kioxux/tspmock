package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyDetailList
 * @类描述: doc_destroy_detail_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 20:16:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocDestroyDetailListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 销毁明细流水号 **/
	private String dddlSerno;
	
	/** 销毁申请流水号 **/
	private String ddalSerno;
	
	/** 档案流水号 **/
	private String docSerno;
	
	/** 档案编号 **/
	private String docNo;
	
	/** 归档模式 **/
	private String archiveMode;
	
	/** 档案类型 **/
	private String docType;
	
	/** 档案分类 **/
	private String docClass;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 关联业务流水号 **/
	private String bizSerno;
	
	/** 资料件数 **/
	private String docNum;
	
	/** 生成日期 **/
	private String createDate;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 已保年限 **/
	private String inKeepYears;
	
	/** 销毁状态 **/
	private String destroyStatus;
	
	/** 销毁日期 **/
	private String destroyDate;
	
	/** 档案状态 **/
	private String docStauts;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param dddlSerno
	 */
	public void setDddlSerno(String dddlSerno) {
		this.dddlSerno = dddlSerno == null ? null : dddlSerno.trim();
	}
	
    /**
     * @return DddlSerno
     */	
	public String getDddlSerno() {
		return this.dddlSerno;
	}
	
	/**
	 * @param ddalSerno
	 */
	public void setDdalSerno(String ddalSerno) {
		this.ddalSerno = ddalSerno == null ? null : ddalSerno.trim();
	}
	
    /**
     * @return DdalSerno
     */	
	public String getDdalSerno() {
		return this.ddalSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno == null ? null : docSerno.trim();
	}
	
    /**
     * @return DocSerno
     */	
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo == null ? null : docNo.trim();
	}
	
    /**
     * @return DocNo
     */	
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param archiveMode
	 */
	public void setArchiveMode(String archiveMode) {
		this.archiveMode = archiveMode == null ? null : archiveMode.trim();
	}
	
    /**
     * @return ArchiveMode
     */	
	public String getArchiveMode() {
		return this.archiveMode;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType == null ? null : docType.trim();
	}
	
    /**
     * @return DocType
     */	
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass == null ? null : docClass.trim();
	}
	
    /**
     * @return DocClass
     */	
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param docNum
	 */
	public void setDocNum(String docNum) {
		this.docNum = docNum == null ? null : docNum.trim();
	}
	
    /**
     * @return DocNum
     */	
	public String getDocNum() {
		return this.docNum;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate == null ? null : createDate.trim();
	}
	
    /**
     * @return CreateDate
     */	
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param inKeepYears
	 */
	public void setInKeepYears(String inKeepYears) {
		this.inKeepYears = inKeepYears == null ? null : inKeepYears.trim();
	}
	
    /**
     * @return InKeepYears
     */	
	public String getInKeepYears() {
		return this.inKeepYears;
	}
	
	/**
	 * @param destroyStatus
	 */
	public void setDestroyStatus(String destroyStatus) {
		this.destroyStatus = destroyStatus == null ? null : destroyStatus.trim();
	}
	
    /**
     * @return DestroyStatus
     */	
	public String getDestroyStatus() {
		return this.destroyStatus;
	}
	
	/**
	 * @param destroyDate
	 */
	public void setDestroyDate(String destroyDate) {
		this.destroyDate = destroyDate == null ? null : destroyDate.trim();
	}
	
    /**
     * @return DestroyDate
     */	
	public String getDestroyDate() {
		return this.destroyDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts == null ? null : docStauts.trim();
	}
	
    /**
     * @return DocStauts
     */	
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}