package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.LoanList;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.Lsnp01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.MortList;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.Lsnp01RespDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.RiskData;
import cn.com.yusys.yusp.dto.client.http.ypxt.callguar.CallGuarReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.IqpCusLsnpInfoMapper;
import cn.com.yusys.yusp.service.client.bsp.lsnp.Lsnp01.Lsnp01Service;
import cn.com.yusys.yusp.util.CmisBizGuarUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusLsnpInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 李志敏
 * @创建时间: 2021-05-04 19:28:02
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpCusLsnpInfoService {

    private static final Logger log = LoggerFactory.getLogger(IqpCusLsnpInfoService.class);

    @Autowired(required = false)
    private IqpCusLsnpInfoMapper iqpCusLsnpInfoMapper;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpHouseService iqpHouseService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private Lsnp01Service lsnp01Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Value("${application.lsnpRirs.url}")
    private String url;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpCusLsnpInfo selectByPrimaryKey(String iqpSerno) {
        return iqpCusLsnpInfoMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<IqpCusLsnpInfo> selectAll(QueryModel model) {
        List<IqpCusLsnpInfo> records = (List<IqpCusLsnpInfo>) iqpCusLsnpInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpCusLsnpInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpCusLsnpInfo> list = iqpCusLsnpInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpCusLsnpInfo record) {
        return iqpCusLsnpInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpCusLsnpInfo record) {
        return iqpCusLsnpInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpCusLsnpInfo record) {
        return iqpCusLsnpInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpCusLsnpInfo record) {
        return iqpCusLsnpInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpCusLsnpInfoMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpCusLsnpInfoMapper.deleteByIds(ids);
    }

    /**
     * @author zlf
     * @date 2021/5/4 16:41
     * @version 1.0.0
     * @desc 发送零售内评评级测算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<IqpCusLsnpInfo> updateLevel(IqpCusLsnpInfo iqpCusLsnpInfo) {
        log.info("根据流水号【{}】,前往零售内评评级测算开始", iqpCusLsnpInfo.getIqpSerno());

        // 业务申请基本信息对象
        IqpLoanApp iqpLoanApp = null;
        // 业务申请基本信息-房屋信息对象
        IqpHouse iqpHouse = null;
        // 客户基本信息对象
        CusBaseClientDto cusBaseClientDto = null;
        // 零售评级临时对象
        IqpCusLsnpInfo iqpCusLsnpInfoTemp = null;

        // 请求对象信息
        Lsnp01ReqDto lsnp01ReqDto = null;

        // 返回
        ResultDto<IqpCusLsnpInfo> iqpCusLsnpInfoResultDto = null;

        try {
            iqpCusLsnpInfoResultDto = new ResultDto<>();
            log.info("根据流水号【{}】,查询零售评级信息开始", iqpCusLsnpInfo.getIqpSerno());
            iqpCusLsnpInfoTemp = this.selectByPrimaryKey(iqpCusLsnpInfo.getPkId());
            log.info("根据流水号【{}】,查询零售评级信息结束", iqpCusLsnpInfo.getIqpSerno());

            log.info("根据流水号【{}】,查询业务申请基本信息开始", iqpCusLsnpInfo.getIqpSerno());
            iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpCusLsnpInfo.getIqpSerno());
            log.info("根据流水号【{}】,查询业务申请基本信息结束", iqpCusLsnpInfo.getIqpSerno());

            log.info("根据流水号【{}】,查询业务申请基本信息-房屋信息开始", iqpCusLsnpInfo.getIqpSerno());
            iqpHouse = iqpHouseService.selectByPrimaryKey(iqpCusLsnpInfo.getIqpSerno());
            log.info("根据流水号【{}】,查询业务申请基本信息-房屋信息结束", iqpCusLsnpInfo.getIqpSerno());

            log.info("根据客户号【{}】,查询客户信息开始", iqpLoanApp.getCusId());
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(iqpLoanApp.getCusId());
            log.info("根据客户号【{}】,查询客户信息结束", iqpLoanApp.getCusId());

            // 拼装请求报文信息
            lsnp01ReqDto = this.buildLsnp01ReqDto(iqpLoanApp, iqpHouse, cusIndivDto.getData());
            System.out.println(lsnp01ReqDto.toString());
            Lsnp01RespDto lsnp01RespDto = null;
            try {
                lsnp01RespDto = lsnp01Service.lsnp01(lsnp01ReqDto);
            } catch (BizException be) {
                throw BizException.error(null, be.getErrorCode(), be.getMessage());
            }
            

            log.info("根据流水号【{}】,查询零售评级信息开始", iqpCusLsnpInfo.getIqpSerno());
            List<IqpCusLsnpInfo> iqpCusLsnpInfoLsit  = this.selectByIqpSerno(iqpCusLsnpInfo.getIqpSerno());
            log.info("根据流水号【{}】,查询零售评级信息结束", iqpCusLsnpInfo.getIqpSerno());
            if(iqpCusLsnpInfoLsit.size() > 0){
                for(IqpCusLsnpInfo iqpCusLsnpInfo1:iqpCusLsnpInfoLsit) {
                    this.deleteByPrimaryKey(iqpCusLsnpInfo1.getPkId());//删除
                }
            }

            // 获取解读信息
            RiskData riskData = lsnp01RespDto.getList().get(0).getRiskData().get(0);
            // 数字解读值
            iqpCusLsnpInfo.setDigIntVal(riskData.getInpret_val() != null ? riskData.getInpret_val().toString() : "");
            // 数字解读值风险等级
            iqpCusLsnpInfo.setDigIntValRiskLvl(riskData.getInpret_val_risk_lvl() != null ? riskData.getInpret_val_risk_lvl() : "");
            // 申请评分
            iqpCusLsnpInfo.setAppScore(riskData.getApply_score().toString() != null ? riskData.getApply_score().toString() : "");
            // 申请评分风险等级
            iqpCusLsnpInfo.setAppScoreRiskLvl(riskData.getApply_score_risk_lvl() != null ? riskData.getApply_score_risk_lvl() : "");
            // 规则风险等级
            iqpCusLsnpInfo.setRuleRiskLvl(riskData.getRule_risk_lvl() != null ? riskData.getRule_risk_lvl() : "");
            // 综合风险等级
            iqpCusLsnpInfo.setInteRiskLvl(riskData.getComplex_risk_lvl() != null ? riskData.getComplex_risk_lvl() : "");
            // 额度建议
            iqpCusLsnpInfo.setLmtAdvice(riskData.getLmt_advice() != null ? riskData.getLmt_advice() : BigDecimal.ZERO);
            // 定价建议
            iqpCusLsnpInfo.setPriceAdvice(riskData.getPrice_advice() != null ? riskData.getPrice_advice() : BigDecimal.ZERO);
            // 自动化审批标志
            iqpCusLsnpInfo.setLimitFlag(riskData.getLimit_flag() != null ? riskData.getLimit_flag() : "");
            iqpCusLsnpInfo.setCreateTime(DateUtils.getCurrDate());
            iqpCusLsnpInfo.setUpdateTime(DateUtils.getCurrDate());
            this.insert(iqpCusLsnpInfo);
            iqpCusLsnpInfoResultDto.setData(iqpCusLsnpInfo);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            if (null != iqpCusLsnpInfoResultDto) {
                iqpCusLsnpInfoResultDto.setCode(EpbEnum.EPB099999.key);
                iqpCusLsnpInfoResultDto.setMessage(e.getMessage());
            }
        }
        return iqpCusLsnpInfoResultDto;
    }

    /**
     * @author zlf
     * @date 2021/5/14 16:41
     * @version 1.0.0
     * @desc 拼装零售内屏请求报文开始，针对于对私客户
     * 1、当机构为寿光村镇、主担保仅有存单质押、低风险质押无需进行征信校验
     * 2、查询客户有无近30天内征信报告，无则提示
     * 3、
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private Lsnp01ReqDto buildLsnp01ReqDto(IqpLoanApp iqpLoanApp, IqpHouse iqpHouse, CusIndivDto cusIndivDto) throws Exception {
        log.info("根据流水号【{}】,拼装零售内平请求信息开始", iqpLoanApp.getIqpSerno());

        // 1、获取当前营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");

        // 2、获取客户基本信息
        // 2.1 获取客户年龄
        String age = CmisCommonUtils.getAge(new SimpleDateFormat("yyyyMMdd")
                .parse(CmisCommonUtils.getBirthdayByID(iqpLoanApp.getCertCode())), openDay);
        // 2.2 获取客户年收入
        // 根据客户号查询客户年收入信息,查询客户单位信息
        //CusIndivUnitDto cusIndivUnitDto = new CusIndivUnitDto();
        CusIndivUnitDto cusIndivUnitDto = cmisCusClientService.queryCusindivUnitByCusid(iqpLoanApp.getCusId()).getData();
        if (cusIndivUnitDto == null) {
            throw new BizException(null, "", null, "客户单位信息为空！");
        }

        CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(iqpLoanApp.getCusId());
        if (cusIndivContactDto == null) {
            throw new BizException(null, "", null, "客户联系信息为空！");
        }
        if (cusIndivContactDto.getMobileNo() == null || "".equals(cusIndivContactDto.getMobileNo())) {
            throw new BizException(null, "", null, "客户手机为空！");
        }

        // 请求报文体
        Lsnp01ReqDto lsnp01ReqDto = new Lsnp01ReqDto();
        //
        lsnp01ReqDto.setApply_seq(iqpLoanApp.getIqpSerno());
        lsnp01ReqDto.setCust_no(iqpLoanApp.getCusId());
        //01 对公 02 对私
        lsnp01ReqDto.setCust_type("02");
        // 证件类型 10--身份证
        lsnp01ReqDto.setCert_type("10");
        // 客户id
        lsnp01ReqDto.setCust_no(iqpLoanApp.getCusId());
        // 证件号码
        lsnp01ReqDto.setCert_no(iqpLoanApp.getCertCode());
        // 客户名称
        lsnp01ReqDto.setCust_name(iqpLoanApp.getCusName());
        // 电话号码
        lsnp01ReqDto.setTel(cusIndivContactDto.getMobileNo());
        // 年龄
        lsnp01ReqDto.setAge(age);
        // 行业类别
        lsnp01ReqDto.setIndus_type(StringUtils.isBlank(cusIndivUnitDto.getIndivComTrade()) ? ""
                : cusIndivUnitDto.getIndivComTrade().substring(0, 1));
        // 性别 F：女 M:男
        lsnp01ReqDto.setSex(CmisCommonUtils.getGenderByIDToFM(iqpLoanApp.getCertCode()));
        // 婚姻状况
        lsnp01ReqDto.setMarry_stat(DicTranEnum.lookup("MAR_STATUS_XDTOLSNP_" + cusIndivDto.getMarStatus()));
        // 职业
        lsnp01ReqDto.setProfession(cusIndivUnitDto.getOcc());//2021-10-12 10:49:48 与于淼确认：这个转不转我们都能适配
        //最高学位
        lsnp01ReqDto.setMax_degree(changeIndivDgr(cusIndivDto.getIndivDgr()));
        // 公司性质
        lsnp01ReqDto.setCorp_prop(StringUtils.isBlank(cusIndivUnitDto.getIndivComTyp()) ? ""
                : DicTranEnum.lookup("COM_TYPE_XDTOLSNP_" + cusIndivUnitDto.getIndivComTyp()));
        // 手机号
        //lsnp01ReqDto.setTel(iqpLoanApp.getPhone());
        // 个人月收入总额
        lsnp01ReqDto.setTotal_income(StringUtils.isBlank(cusIndivUnitDto.getIndivYearn()) ? BigDecimal.ZERO
                : new BigDecimal(cusIndivUnitDto.getIndivYearn()));
        // 家庭资负债比
        lsnp01ReqDto.setFam_debt_div_asset(null);
        // 成为我行客户时长（月） TODO
        Integer term = null;
        if( cusIndivDto.getInitLoanDate() != null && !"".equals(cusIndivDto.getInitLoanDate())){
            term = DateUtils.getMonthsByTwoDates(cusIndivDto.getInitLoanDate(), "yyyy-MM-dd", openDay, "yyyy-MM-dd");
            String targetDate = DateUtils.addMonth(cusIndivDto.getInitLoanDate(), "yyyy-MM-dd", term);
            if (!cusIndivDto.getInitLoanDate().equals(targetDate)) {//不满一个月按一个月计算
                term++;
            }
        }
        // 本行开设基本账户标志 todo 对公字段
        lsnp01ReqDto.setBase_acct_flag("");
        // 本地经营期限
        lsnp01ReqDto.setOper_dura(null);
        // 自筹资金比例
        lsnp01ReqDto.setOwned_money_rate(null);
        // 有无房产 todo
        lsnp01ReqDto.setEstate_flag("");
        // 房屋销售价格
        if(iqpHouse != null && iqpHouse.getHouseTotal() != null ){
            lsnp01ReqDto.setSale_price(iqpHouse.getHouseTotal());
        }else{
            lsnp01ReqDto.setSale_price(null);
        }
        // 年可核实销售收入 TODO
        lsnp01ReqDto.setSale_proceeds(null);
        // 借款企业和借款人家庭净资产
        lsnp01ReqDto.setNet_assets(null);
        // 有无子女
        lsnp01ReqDto.setBaby_flag(StringUtils.isBlank(cusIndivDto.getIsHaveChildren()) ? "" : cusIndivDto.getIsHaveChildren());
        // 企业成立年限(距所在月份时点)
        lsnp01ReqDto.setCorp_estb_dura(null);
        // 经营企业的单位名称
        lsnp01ReqDto.setCorp_name("");
        // 经营企业的企业组织代码证
        lsnp01ReqDto.setOrg_cred_cd("");
        // 经营企业的中征码
        lsnp01ReqDto.setLoan_card_code("");
        // 法人证件号
        lsnp01ReqDto.setLegal_cert_no("");
        // 法人名称
        lsnp01ReqDto.setLegal_cust_name("");

        // 第一层循环体
        //循环体
        List list = new ArrayList();
        cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.List listArray = new cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.List();
        // 业务流水号
        listArray.setBusi_seq(iqpLoanApp.getIqpSerno());
        // 授信分项编号 默认空
        listArray.setCred_sub_no("");

        // 第二层循环体 包含贷款循环体、押品循环体
        // 贷款循环体
        List<LoanList> loanLists = new ArrayList<>();
        // 业务信息
        LoanList loan = new LoanList();
        // 业务品种
        loan.setBusi_type(iqpLoanApp.getPrdId());
        // 还款方式
        loan.setRepay_type(DicTranEnum.lookup("REPAY_MODE_XDTOLSNP_" + iqpLoanApp.getRepayMode()));
        // 担保方式
        loan.setGuar_type(DicTranEnum.lookup("GUAR_MODE_XDTOLSNP_" + iqpLoanApp.getGuarWay()));
        //老代码搬运
        String guarWay = iqpLoanApp.getGuarWay();//担保方式
        BigDecimal appAmt = iqpLoanApp.getAppAmt();
        String prdId = iqpLoanApp.getPrdId();
        BigDecimal credit_amt_div_eval_val= null;//原始贷款金额占合同层抵质押物市场评估价值总和的百分比
        BigDecimal credit_amt_div_firm_value= null;//原始贷款金额占合同层抵质押物实际认定价值总和的百分比
        //房贷产品
        String ls_anjie_biz_types = "022001,022002,022020,022021,022024,022031,022040,022051,022052,022053,022054,022055,022056";
        //根据业务编号查询项下的所有抵质押评估金额之和
        BigDecimal evalAmtSum =  guarBaseInfoMapper.queryEvalAmtSumBySerno(iqpLoanApp.getIqpSerno());
        //根据担保业务编号查询项下的所有抵质押我行可用价值之和
        BigDecimal maxMortagageAmtSum =  guarBaseInfoMapper.queryMaxMortagageAmtSumBySerno(iqpLoanApp.getIqpSerno());
        //10-抵押，20-质押，21-低风险质押
        if("10".equals(guarWay) || "20".equals(guarWay) || "21".equals(guarWay)){

                if(ls_anjie_biz_types.indexOf(iqpLoanApp.getPrdId()) > -1){//判断是否为房贷
                    //计算原始贷款金额占合同层抵质押物评估价值总和的百分比(房贷)
                    if(evalAmtSum.doubleValue() != 0){
                        credit_amt_div_eval_val = appAmt.divide(evalAmtSum, 4, BigDecimal.ROUND_HALF_UP);
                    }
                }else{
                    if(maxMortagageAmtSum.doubleValue() != 0){
                        //计算原始贷款金额占合同层抵质押物实际认定价值总和的百分比(消费贷)
                        credit_amt_div_firm_value = appAmt.divide(maxMortagageAmtSum, 4, BigDecimal.ROUND_HALF_UP);
                    }else {
                        credit_amt_div_firm_value= new BigDecimal("-1");
                    }
                }

        }
        // 原始贷款金额占合同层抵质押物市场评估价值总和的百分比
        loan.setCredit_amt_div_eval_val(credit_amt_div_eval_val);
        //原始贷款金额占合同层抵质押物实际认定价值总和的百分比
        loan.setCredit_amt_div_firm_value(credit_amt_div_firm_value);
        //房屋性质转换
        loan.setHouse_prop(changeHouseProp(prdId));
        // 房产类型
        loan.setEstate_type(changeHouseProp(iqpHouse));
        //贷款金额
        loan.setLoan_amt(iqpLoanApp.getAppAmt());
        //贷款期限
        loan.setLoan_term(iqpLoanApp.getAppTerm().intValue());
        // 上线控制额度
        BigDecimal upper_limit = new BigDecimal(changeSxLmt(iqpLoanApp.getPrdId()));
        loan.setUpper_limit(upper_limit);
        // 银行规定利率
        loan.setRegu_rate(iqpLoanApp.getExecRateYear());
        // 车辆销售价格
        loan.setCar_sale_price(null);

        loanLists.add(loan);

        // 抵质押循环体
        List<MortList> mortLists = new ArrayList<>();
        //押品信息
        MortList mort = new MortList();

        // 根据担保方式查询抵押物信息
        // 根据业务编号查询抵押物信息
        List<GuarBaseInfo> guarBaseInfoList = null;
        // 10--抵押、20、21--质押
        if ("10".equals(iqpLoanApp.getGuarWay()) || "20".equals(iqpLoanApp.getGuarWay()) || "21".equals(iqpLoanApp.getGuarWay())) {
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("serno", iqpLoanApp.getIqpSerno());
            List<GuarBizRelGuarBaseDto> guarBizRelList = guarBaseInfoService.queryGuarInfoSell(queryModel);

            // 根据押品编号查询抵押物信息
            if (CollectionUtils.nonEmpty(guarBizRelList)) {
                guarBaseInfoList = guarBizRelList.parallelStream().map(s -> {
                    log.info("根据押品编号【{}】查询押品信息", s.getGuarNo());

                    return guarBaseInfoService.getGuarBaseInfoByGuarNo(s.getGuarNo());
                }).collect(Collectors.toList());

                // 遍历抵质押物信息
                if (CollectionUtils.nonEmpty(guarBaseInfoList)) {
                    guarBaseInfoList.parallelStream().forEach(s -> {
                        MortList mortTemp = new MortList();
                        // 抵押物类型
                        mortTemp.setPawn_type(changeGuarTypeCd(s.getGuarTypeCd()));
                        // 抵押率 授信分项的授信额度/押品的权利金额
                        mortTemp.setMort_rate(BigDecimal.ZERO);
                        // 抵质押物评估价
                        mortTemp.setValuation(s.getEvalAmt());
                        mortLists.add(mortTemp);
                    });
                } else {
                    throw new BizException(null, "", null, "抵质押信息为空！");
                }
            } else {
                throw new BizException(null, "", null, "抵质押信息为空！");
            }

        } else { // 其他方式均为空
            // 抵押物类型
            mort.setPawn_type("");
            // 抵押率
            mort.setMort_rate(BigDecimal.ZERO);
            // 抵质押物评估价
            mort.setValuation(BigDecimal.ZERO);
            mortLists.add(mort);
        }


        //放入业务信息
        listArray.setLoanList(loanLists);
        //放入押品信息
        listArray.setMortList(mortLists);
        list.add(listArray);
        lsnp01ReqDto.setList(list);
        return lsnp01ReqDto;
    }

    /**
     * @author zzr
     * @date 2021/5/4 16:41
     * @version 1.0.0
     * @desc 发送零售内评评级测算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map updateLevelForLmt(String iqpSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoMapper.selectByLmtSerno(iqpSerno);
        if(iqpCusLsnpInfo == null){
            iqpCusLsnpInfo = new IqpCusLsnpInfo();
        }
        log.info("根据流水号【{}】,前往零售内评评级测算开始", iqpCusLsnpInfo.getIqpSerno());

        // 业务申请基本信息对象
        LmtApp lmtApp = null;
        // 业务申请基本信息-房屋信息对象
        IqpHouse iqpHouse = null;
        // 客户基本信息对象
        CusBaseClientDto cusBaseClientDto = null;
        // 零售评级临时对象
        IqpCusLsnpInfo iqpCusLsnpInfoTemp = null;

        // 请求对象信息
        Lsnp01ReqDto lsnp01ReqDto = null;

        // 返回
        ResultDto<IqpCusLsnpInfo> iqpCusLsnpInfoResultDto = null;
        ResultDto<Lsnp01RespDto> lsnp01RespResultDto = null;

        try {
            iqpCusLsnpInfoResultDto = new ResultDto<>();
            log.info("根据流水号【{}】,查询零售评级信息开始", iqpSerno);
            iqpCusLsnpInfoTemp = iqpCusLsnpInfoMapper.selectByLmtSerno(iqpSerno);
            //iqpCusLsnpInfoTemp = this.selectByPrimaryKey(iqpCusLsnpInfo.getPkId());
            log.info("根据流水号【{}】,查询零售评级信息结束", iqpSerno);

            log.info("根据流水号【{}】,查询业务申请基本信息开始", iqpSerno);
            lmtApp = lmtAppService.selectBySerno(iqpSerno);
            log.info("根据流水号【{}】,查询业务申请基本信息结束", iqpSerno);

//                log.info("根据流水号【{}】,查询业务申请基本信息-房屋信息开始", iqpCusLsnpInfo.getIqpSerno());
//                iqpHouse = iqpHouseService.selectByPrimaryKey(iqpCusLsnpInfo.getIqpSerno());
            log.info("根据流水号【{}】,查询业务申请基本信息-房屋信息结束", iqpSerno);

            log.info("根据客户号【{}】,查询客户信息开始", lmtApp.getCusId());
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(lmtApp.getCusId());
            log.info("根据客户号【{}】,查询客户信息结束", lmtApp.getCusId());
            if(cusIndivDto == null){
                throw BizException.error(null, EcbEnum.ECB020058.key, EcbEnum.ECB020058.value);
            }

            // 拼装请求报文信息
            lsnp01ReqDto = this.buildLsnp01ReqDtoForLmt(lmtApp, cusIndivDto.getData());
            if(lsnp01ReqDto == null){
                rtnCode = EcbEnum.ECB020057.key;
                rtnMsg = EcbEnum.ECB020057.value;
                return rtnData;
            }
            System.out.println(lsnp01ReqDto.toString());
            Lsnp01RespDto lsnp01RespDto = null;
            try {
                lsnp01RespDto = lsnp01Service.lsnp01(lsnp01ReqDto);
            } catch (BizException be) {
                throw BizException.error(null, be.getErrorCode(), be.getMessage());
            }
            // 获取解读信息
            RiskData riskData = lsnp01RespDto.getList().get(0).getRiskData().get(0);
            // 数字解读值
            iqpCusLsnpInfo.setDigIntVal(riskData.getInpret_val() != null ? riskData.getInpret_val().toString() : "");
            // 数字解读值风险等级
            iqpCusLsnpInfo.setDigIntValRiskLvl(riskData.getInpret_val_risk_lvl() != null ? riskData.getInpret_val_risk_lvl() : "");
            // 申请评分
            iqpCusLsnpInfo.setAppScore(riskData.getApply_score().toString() != null ? riskData.getApply_score().toString() : "");
            // 申请评分风险等级
            iqpCusLsnpInfo.setAppScoreRiskLvl(riskData.getApply_score_risk_lvl() != null ? riskData.getApply_score_risk_lvl() : "");
            // 规则风险等级
            iqpCusLsnpInfo.setRuleRiskLvl(riskData.getRule_risk_lvl() != null ? riskData.getRule_risk_lvl() : "");
            // 综合风险等级
            iqpCusLsnpInfo.setInteRiskLvl(riskData.getComplex_risk_lvl() != null ? riskData.getComplex_risk_lvl() : "");
            // 额度建议
            iqpCusLsnpInfo.setLmtAdvice(riskData.getLmt_advice() != null ? riskData.getLmt_advice() : BigDecimal.ZERO);
            // 定价建议
            iqpCusLsnpInfo.setPriceAdvice(riskData.getPrice_advice() != null ? riskData.getPrice_advice() : BigDecimal.ZERO);
            // 自动化审批标志
            iqpCusLsnpInfo.setLimitFlag(riskData.getLimit_flag() != null ? riskData.getLimit_flag() : "");
            iqpCusLsnpInfo.setUpdateTime(DateUtils.getCurrDate());
            if (iqpCusLsnpInfoTemp == null) {
                iqpCusLsnpInfo.setIqpSerno(iqpSerno);
                iqpCusLsnpInfo.setCreateTime(DateUtils.getCurrDate());
                iqpCusLsnpInfo.setPkId(UUID.randomUUID().toString());
                this.insert(iqpCusLsnpInfo);
            }else{
                this.updateSelective(iqpCusLsnpInfo);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        }finally {
            rtnData.put("lsnpResult",iqpCusLsnpInfo);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @author zzr
     * @date 2021/5/14 16:41
     * @version 1.0.0
     * @desc 拼装零售内屏请求报文开始，针对于单一对公授信客户
     * 1、当机构为寿光村镇、主担保仅有存单质押、低风险质押无需进行征信校验
     * 2、查询客户有无近30天内征信报告，无则提示
     * 3、
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private Lsnp01ReqDto buildLsnp01ReqDtoForLmt(LmtApp lmtApp, CusIndivDto cusIndivDto) throws Exception {
        log.info("根据流水号【{}】,拼装零售内平请求信息开始", lmtApp.getSerno());

        // 1、获取当前营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());;

        // 2、获取客户基本信息
        // 2.1 获取客户年龄
        String age = CmisCommonUtils.getAge(new SimpleDateFormat("yyyyMMdd")
                .parse(CmisCommonUtils.getBirthdayByID(cusBaseClientDto.getCertCode())), openDay);
        // 2.2 获取客户年收入
        // 根据客户号查询客户年收入信息,查询客户单位信息
        //CusIndivUnitDto cusIndivUnitDto = new CusIndivUnitDto();
        CusIndivUnitDto cusIndivUnitDto = cmisCusClientService.queryCusindivUnitByCusid(lmtApp.getCusId()).getData();


        if (cusIndivUnitDto == null) {
            throw new BizException(null, "", null, "客户单位信息为空！");
        }

        // 请求报文体
        Lsnp01ReqDto lsnp01ReqDto = new Lsnp01ReqDto();
        //
        lsnp01ReqDto.setApply_seq(lmtApp.getSerno());
        //01 对公 02 对私
        lsnp01ReqDto.setCust_type("02");
        // 证件类型 10--身份证
        lsnp01ReqDto.setCert_type("10");
        // 客户id
        lsnp01ReqDto.setCust_no(lmtApp.getCusId());
        // 证件号码
        lsnp01ReqDto.setCert_no(cusBaseClientDto.getCertCode());
        // 客户名称
        lsnp01ReqDto.setCust_name(lmtApp.getCusName());
        // 年龄
        lsnp01ReqDto.setAge(age);
        // 行业类别
        lsnp01ReqDto.setIndus_type(StringUtils.isBlank(cusIndivUnitDto.getIndivComTrade()) ? ""
                : cusIndivUnitDto.getIndivComTrade().substring(0, 1));
        // 性别 F：女 M:男
        lsnp01ReqDto.setSex(CmisCommonUtils.getGenderByIDToFM(cusBaseClientDto.getCertCode()));
        // 婚姻状况
        lsnp01ReqDto.setMarry_stat(DicTranEnum.lookup("MAR_STATUS_XDTOLSNP_" + cusIndivDto.getMarStatus()));
        // 职业
        lsnp01ReqDto.setProfession(DicTranEnum.lookup("PROFESSION_XDTOLSNP_" + cusIndivUnitDto.getOcc()));
        // TODO 最高学位
        lsnp01ReqDto.setMax_degree("1");
        // 公司性质
        lsnp01ReqDto.setCorp_prop(StringUtils.isBlank(cusIndivUnitDto.getIndivComTyp()) ? ""
                : DicTranEnum.lookup("COM_TYPE_XDTOLSNP_" + cusIndivUnitDto.getIndivComTyp()));
        // 手机号
        // TODO 手机号
        //lsnp01ReqDto.setTel(iqpLoanApp.getPhone());
        // 个人月收入总额
        lsnp01ReqDto.setTotal_income(StringUtils.isBlank(cusIndivUnitDto.getIndivYearn()) ? BigDecimal.ZERO
                : new BigDecimal(cusIndivUnitDto.getIndivYearn()));
        // 家庭资负债比
        lsnp01ReqDto.setFam_debt_div_asset(BigDecimal.ZERO);
        // 成为我行客户时长（月） TODO
        lsnp01ReqDto.setCust_dura(0);
        // 本行开设基本账户标志 todo 对公字段
        lsnp01ReqDto.setBase_acct_flag("0");
        // 本地经营期限
        lsnp01ReqDto.setOper_dura(0);
        // 自筹资金比例
        lsnp01ReqDto.setOwned_money_rate(new BigDecimal("0"));
        // 有无房产 todo
        lsnp01ReqDto.setEstate_flag("0");
        // 房屋销售价格
        lsnp01ReqDto.setSale_price(new BigDecimal("0"));
        // 年可核实销售收入 TODO
        lsnp01ReqDto.setSale_proceeds(BigDecimal.ZERO);
        // 借款企业和借款人家庭净资产
        lsnp01ReqDto.setNet_assets(BigDecimal.ZERO);
        // 有无子女
        lsnp01ReqDto.setBaby_flag(StringUtils.isBlank(cusIndivDto.getIsHaveChildren()) ? "" : cusIndivDto.getIsHaveChildren());
        // 企业成立年限(距所在月份时点)
        lsnp01ReqDto.setCorp_estb_dura(0);
        // 经营企业的单位名称
        lsnp01ReqDto.setCorp_name("");
        // 经营企业的企业组织代码证
        lsnp01ReqDto.setOrg_cred_cd("");
        // 经营企业的中征码
        lsnp01ReqDto.setLoan_card_code("");
        // 法人证件号
        lsnp01ReqDto.setLegal_cert_no("");
        // 法人名称
        lsnp01ReqDto.setLegal_cust_name("");

        // 第一层循环体
        //循环体
        List list = new ArrayList();
        List<LmtAppSub> subList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
        for(LmtAppSub lmtAppSub :subList){
            if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsPreLmt()) || "60".equals(lmtAppSub.getGuarMode())){
                continue;
            }
            cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.List listArray = new cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.List();
            // 业务流水号
            listArray.setBusi_seq(lmtApp.getSerno());
            // 授信分项编号 默认空
            listArray.setCred_sub_no(lmtAppSub.getSubSerno());
            List<LmtAppSubPrd> subPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
            // 第二层循环体 包含贷款循环体、押品循环体
            // 贷款循环体
            List<LoanList> loanLists = new ArrayList<>();
            for(LmtAppSubPrd lmtAppSubPrd:subPrdList){
                LoanList loan = new LoanList();
                // 业务品种
                String bizTYpe = lmtAppSubPrd.getLmtBizType();
                if("20010102".equals(bizTYpe)){
                    loan.setBusi_type("022011");
                }else if("20020101".equals(bizTYpe)){
                    loan.setBusi_type("022004");
                }else{
                    continue;
                }
                // 还款方式
                loan.setRepay_type(DicTranEnum.lookup("REPAY_MODE_XDTOLSNP_" + lmtAppSubPrd.getRepayMode()));
                // 担保方式
                loan.setGuar_type(DicTranEnum.lookup("GUAR_MODE_XDTOLSNP_" + lmtAppSubPrd.getGuarMode()));
//                // TODO 原始贷款金额占合同层抵质押物市场评估价值总和的百分比
                loan.setCredit_amt_div_eval_val(BigDecimal.valueOf(0));
//                // TODO 原始贷款金额占合同层抵质押物实际认定价值总和的百分比
                loan.setCredit_amt_div_firm_value(BigDecimal.valueOf(0));
//                // TODO 房屋性质,老信贷根据产品映射，待产品代码完善后，更新
//                loan.setHouse_prop("1");
//                // 房产类型
//                loan.setEstate_type("2");
                //贷款金额
                loan.setLoan_amt(lmtAppSubPrd.getLmtAmt());
                //贷款期限
                loan.setLoan_term(lmtAppSubPrd.getLmtTerm());
                // 上线控制额度
                loan.setUpper_limit(BigDecimal.ZERO);
                // 银行规定利率
                loan.setRegu_rate(lmtAppSubPrd.getRateYear());
                // 车辆销售价格
//                loan.setCar_sale_price(BigDecimal.ZERO);
                loanLists.add(loan);
            }
            // 抵质押循环体
            List<MortList> mortLists = new ArrayList<>();
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("serno",lmtAppSub.getSubSerno());
            List<GuarBizRelGuarBaseDto> guarBaseInfo = (List<GuarBizRelGuarBaseDto>) guarBaseInfoService.queryGuarInfoSell(queryModel);
            for(GuarBizRelGuarBaseDto guarBizRelGuarBaseDto:guarBaseInfo){
                //押品信息
                MortList mort = new MortList();
                mort.setPawn_type(guarBizRelGuarBaseDto.getGuarTypeCd());
                if(guarBizRelGuarBaseDto.getMortagageRate() == null || "".equals(guarBizRelGuarBaseDto.getMortagageRate())){
                    mort.setMort_rate(new BigDecimal(0));
                }else{
                    mort.setMort_rate(new BigDecimal(guarBizRelGuarBaseDto.getMortagageRate()));
                }
                mort.setValuation(new BigDecimal(guarBizRelGuarBaseDto.getEvalAmt()));
                mortLists.add(mort);
            }
            if(loanLists.size() == 0){
                return null;
            }
            //放入业务信息
            listArray.setLoanList(loanLists);
            //放入押品信息
            listArray.setMortList(mortLists);
            list.add(listArray);
        }
        if(list.size()==0){
            return null;
        }
        lsnp01ReqDto.setList(list);
        return lsnp01ReqDto;
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/31 13:57
     * @version 1.0.0
     * @desc 根据业务流水号查询内评信息
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public List<IqpCusLsnpInfo> selectByIqpSerno(String iqpSerno) {
        return iqpCusLsnpInfoMapper.selectByIqpSerno(iqpSerno);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/31 13:57
     * @version 1.0.0
     * @desc 根据业务流水号查询内评信息
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public IqpCusLsnpInfo selectByIqpSernoAndPrd(IqpCusLsnpInfo iqpCusLsnpInfo) {
        Map map  = new HashMap();
        map.put("iqpSerno",iqpCusLsnpInfo.getIqpSerno());
        map.put("applyCardPrd",iqpCusLsnpInfo.getApplyCardPrd());
        return iqpCusLsnpInfoMapper.selectByIqpSernoAndPrd(map);
    }

    /**
     * @param
     * @return
     * @author zzr
     * @date 2021/09/03 13:57
     * @version 1.0.0
     * @desc 根据对公授信业务流水号查询内评信息
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public IqpCusLsnpInfo selectByLmtSerno(String iqpSerno) {
        return iqpCusLsnpInfoMapper.selectByLmtSerno(iqpSerno);
    }


    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-9-29
     * @version 1.0.0
     * @desc 拼装零售内评url
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getLsnpRirsUrl(IqpLoanApp iqpLoanApp) {

        String lsnpRirsUrl = url + "userSignOnXD.do?userId="+iqpLoanApp.getManagerId()+"&busi_seq="+iqpLoanApp.getIqpSerno();
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                // 综合客户经理 R0020; 零售客户经理 R0030; 小企业客户经理 R0050
                // 小微客户经理 R0010；客户经理（寿光） RSG01；客户经理（东海） RDH01
                if ("R0020".equals(roles.get(i).getCode()) || "R0030".equals(roles.get(i).getCode()) || "R0050".equals(roles.get(i).getCode())
                        || "R0010".equals(roles.get(i).getCode()) || "RSG01".equals(roles.get(i).getCode()) || "RDH01".equals(roles.get(i).getCode())) {
                    lsnpRirsUrl = url + "userSignOnXDKH.do?userId="+iqpLoanApp.getManagerId()+"&busi_seq="+iqpLoanApp.getIqpSerno();
                    break;
                }
            }
        }
        log.info("零售内评系统URL为[{}]", lsnpRirsUrl);
        return lsnpRirsUrl;
    }

    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-9-29
     * @version 1.0.0
     * @desc 老代码搬运 根据产品查找对应额度上限
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String changeSxLmt(String prdId){
        String upper_limit="";
        if("022001".equals(prdId)||"022052".equals(prdId)||"022053".equals(prdId)||"022055".equals(prdId)||"022056".equals(prdId)||"022002".equals(prdId)||"022020".equals(prdId)||"022021".equals(prdId)||"022051".equals(prdId)||"022054".equals(prdId)||"022024".equals(prdId)||"022031".equals(prdId)||"022040".equals(prdId)){
            upper_limit="10000000";
        }else if("022005".equals(prdId)||"022006".equals(prdId)||"022038".equals(prdId)||"022090".equals(prdId)||"022017".equals(prdId)||"022019".equals(prdId)||"022023".equals(prdId)){
            upper_limit="3000000";
        }else if("022025".equals(prdId)){
            upper_limit="500000";
        }else if("022028".equals(prdId)){
            upper_limit="1000000";
        }else if("022030".equals(prdId)){
            upper_limit="300000";
        }else if("022035".equals(prdId)){
            upper_limit="200000";
        }else if("022036".equals(prdId)){
            upper_limit="1500000";
        }else if("022018".equals(prdId)){
            upper_limit="3000000";
        }else if("022027".equals(prdId)){
            upper_limit="500000";
        }else{
            upper_limit="0";
        }
        return upper_limit;
    }

    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-10-7 17:22:56
     * @version 1.0.0
     * @desc 老代码搬运 房屋性质转换
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String changeHouseProp(String prdId){
        String house_prop="";
        if("022001".equals(prdId)||"022052".equals(prdId)||"022055".equals(prdId)||"022056".equals(prdId)||"022020".equals(prdId)||"022024".equals(prdId)){
            house_prop="1";
        }else if("022002".equals(prdId)||"022021".equals(prdId)||"022051".equals(prdId)||"022054".equals(prdId)||"022040".equals(prdId)||"022053".equals(prdId)||"022031".equals(prdId)){
            house_prop="2";
        }
        return house_prop;
    }

    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-10-7 17:22:56
     * @version 1.0.0
     * @desc 老代码搬运 转换房屋类型
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String changeHouseProp(IqpHouse iqpHouse){
        String housing_type = "";

        try{//捕捉异常，非房贷无房产信息，传空
            if("05".equals(iqpHouse.getHouseType())){
                housing_type="1";
            }else{
                housing_type="2";
            }
        }catch (Exception e){}
        return housing_type;
    }

    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-10-7 17:22:56
     * @version 1.0.0
     * @desc 老代码搬运 转换转换最高学位
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String changeIndivDgr(String indivDgr){
        //新信贷 STD_ZB_DEGREE： [{"key":"0","value":"其他"},{"key":"1","value":"名誉博士"},{"key":"2","value":"博士"},{"key":"3","value":"硕士"},{"key":"4","value":"学士"}]
        //零售内评: MV-缺失, 1-学士, 2-硕士, 3-博士, 5-未知, 6-其他,7-无
        String max_degree = "";
        if("0".equals(indivDgr)){
            max_degree = "6";
        }else if("1".equals(indivDgr)){
            max_degree = "3";
        }else if("2".equals(indivDgr)){
            max_degree = "3";
        }else if("3".equals(indivDgr)){
            max_degree = "2";
        }else if("4".equals(indivDgr)){
            max_degree = "1";
        }
        return max_degree;
    }


    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-10-12 13:55:03
     * @version 1.0.0
     * @desc 老代码搬运 转换抵押物类型
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String changeGuarTypeCd (String guarTypeCd){

        //  1普通住房
        //  2高档住宅、高档公寓、别墅等
        //  3商铺、写字楼等商业用房
        //  4金融质押品-存单
        //  5其他
        if("DY0102005".equals(guarTypeCd)||"DY0102002".equals(guarTypeCd)||"DY0102003".equals(guarTypeCd)||"DY0102999".equals(guarTypeCd)||"DY0103001".equals(guarTypeCd)
                ||"DY0602001".equals(guarTypeCd)||"DY0103002".equals(guarTypeCd)||"DY0103003".equals(guarTypeCd)||"DY0103004".equals(guarTypeCd)
                ||"DY0102001".equals(guarTypeCd)||"DY0102001".equals(guarTypeCd)||"DY0102001".equals(guarTypeCd)
                ||"DY0603001".equals(guarTypeCd)||"DY0602001".equals(guarTypeCd)){
            guarTypeCd="3";
        }else if("DY0101002".equals(guarTypeCd)||"DY0101005".equals(guarTypeCd)||"DY0101001".equals(guarTypeCd)||"DY0101999".equals(guarTypeCd)||"DY0101999".equals(guarTypeCd)
                ||"DY0601001".equals(guarTypeCd)||"DY0101001".equals(guarTypeCd)||"DY0401001".equals(guarTypeCd)){
            guarTypeCd="1";
        }else if("DY0101004".equals(guarTypeCd)){
            guarTypeCd="2";
        }else if("ZY0102001".equals(guarTypeCd)||"ZY0102002".equals(guarTypeCd)||"ZY0102003".equals(guarTypeCd)||"ZY0102004".equals(guarTypeCd)){
            guarTypeCd="4";
        } else{
            guarTypeCd="5";
        }
        return guarTypeCd;
    }



}
