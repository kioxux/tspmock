/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccCvrsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:54:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class SendMessageForDgService {
    private static final Logger logger = LoggerFactory.getLogger(SendMessageForDgService.class);

    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;
    /**
     * @param imageApprDto
     * @return void
     * @author 王玉坤
     * @date 2021/9/29 11:39
     * @version 1.0.0
     * @desc 推送影像审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> sendImage(ImageApprDto imageApprDto) {
        logger.info("推送影像审批信息交易开始,影像流水号【{}】", imageApprDto.getDocId());
        ResultDto<String> responseEntityResultDto = null;
        try {
            responseEntityResultDto = dscms2ImageClientService.imageappr(imageApprDto);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, responseEntityResultDto.getCode()) && responseEntityResultDto.getData() != null) {
                if (Objects.equals("true", responseEntityResultDto.getData())) {
                    logger.info("推送影像审批信息交易结束,交易成功，影像流水号【{}】", imageApprDto.getDocId());
                } else {
                    logger.info("推送影像审批信息交易结束,交易失败，影像流水号【{}】", imageApprDto.getDocId());
                }
            } else {
                logger.info("推送影像审批信息交易结束,交易异常，影像流水号【{}】", imageApprDto.getDocId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        logger.info("推送影像审批信息交易结束,影像流水号【{}】", imageApprDto.getDocId());
        return responseEntityResultDto;
    }
}
