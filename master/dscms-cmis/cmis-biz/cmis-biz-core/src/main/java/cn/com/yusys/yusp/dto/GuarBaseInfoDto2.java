package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfo
 * @类描述: guar_base_info数据实体类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-04-14 15:25:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarBaseInfoDto2 implements Serializable{
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    private String serno;

    /** 押品统一编号 **/
    private String guarNo;

    /** 押品标识 **/
    private String grtFlag;

    /** 抵质押物名称（抵质押物类型名称） **/
    private String pldimnMemo;

    /** 担保分类代码 **/
    private String guarTypeCd;

    /** 抵质押分类（大类）STD_GAGE_TYPE **/
    private String guarType;

    /** 押品初次登记时间 **/
    private String guarFirstCreateTime;

    /** 借款人 **/
    private String cusId;

    /** 押品所有人编号 **/
    private String guarCusId;

    /** 押品所有人名称 **/
    private String guarCusName;

    /** 押品所有人类型 STD_CUS_TYPE **/
    private String guarCusType;

    /** 押品所有人证件类型 STD_CERT_TYPE **/
    private String guarCertType;

    /** 押品所有人证件号码 **/
    private String guarCertCode;

    /** 押品所有人贷款卡号(抵押)  或 出质人贷款卡号(质押) **/
    private String assureCardNo;

    /** 是否实质正相关  STD_YES_NO **/
    private String relationInt;

    /** 是否共有财产  STD_YES_NO **/
    private String commonAssetsInd;

    /** 押品所有人所占份额 **/
    private String occupyofowner;

    /** 是否需要办理保险 STD_YES_NO **/
    private String insuranceInd;

    /** 是否需要办理公证 STD_YES_NO **/
    private String notarizationInd;

    /** 人工认定结果 STD_FORCECREATE_IND **/
    private String forcecreateInd;

    /** 强制创建理由 **/
    private String forcecreateReason;

    /** 押品所在业务阶段 STD_GUAR_BUSISTATE **/
    private String guarBusistate;

    /** 押品状态 STD_GUAR_STATE **/
    private String guarState;

    /** 是否权属清晰  STD_YES_NO **/
    private String isOwnershipClear;

    /** 是否抵债资产  STD_YES_NO **/
    private String isDebtAsset;

    /** 法律规定禁止流通财产  STD_YES_NO **/
    private String forbidCirBelogFlag;

    /** 是否被查封、扣押或监管  STD_YES_NO **/
    private String supervisionFlag;

    /** 查封时间 **/
    private String supervisionDate;

    /** 是否有强行执行条款  STD_YES_NO **/
    private String enforeFlag;

    /** 抵权证编号及其他编号 **/
    private String rightOtherNo;

    /** 押品登记办理状态 STD_REG_STATE **/
    private String regState;

    /** 担保权生效方式 STD_DEF_EFFECT_TYPE **/
    private String defEffectType;

    /** 他行是否已设定担保权  STD_YES_NO **/
    private String otherBackGuarInd;

    /** 抵质押物与借款人相关性 **/
    private String pldimnDebitRelative;

    /** 查封便利性 **/
    private String supervisionConvenience;

    /** 法律有效性 **/
    private String lawValidity;

    /** 抵质押品通用性 **/
    private String pldimnCommon;

    /** 抵质押品变现能力 **/
    private String pldimnCashability;

    /** 价格波动性 **/
    private String priceWave;

    /** 是否扫描资料 **/
    private String isScanMater;

    /** 权证编号及其他编号 **/
    private String ringhtNoAndOtherNo;

    /** 我行担保权受偿顺序 **/
    private String mybackGuarFirstSeq;

    /** 法定优先受偿款(元) **/
    private java.math.BigDecimal legalPriPayment;

    /** 创建系统/来源系统 STD_DATA_SOURCE **/
    private String createSys;

    /** 备注 **/
    private String remark;

    /** 管户人 **/
    private String accountManager;

    /** 押品评估价值 **/
    private java.math.BigDecimal evalAmt;

    /** 评估日期 **/
    private String evalDate;

    /** 押品认定价值 **/
    private java.math.BigDecimal confirmAmt;

    /** 币种 STD_CUR_TYPE  **/
    private String curType;

    /** 权证凭证号 **/
    private String certiRecordId;

    /** 权证到期日 **/
    private String certiEndDate;

    /** 最高抵押率（%） **/
    private java.math.BigDecimal mortagageMaxRate;

    /** 设定抵押率（%） **/
    private java.math.BigDecimal mortagageRate;

    /** 最高可抵质押金额（元） **/
    private java.math.BigDecimal maxMortagageAmt;

    /** 抵押物存放地点 **/
    private String pldLocation;

    /** 共有权人名称 **/
    private String refName;

    /** 是否权益争议STD_ZB_YES_NO **/
    private String isDisputed;

    /** 核心担保品编号 **/
    private String coreGuarantyNo;

    /** 核心担保品序号 **/
    private String coreGuarantySeq;

    /** 账务机构 **/
    private String finaBrId;

    /** 认定日期 **/
    private String confirmDate;

    /** 评估方式 STD_EVAL_TYPE **/
    private String evalType;

    /** 评估机构 **/
    private String evalOrg;

    /** 评估机构组织机构代码 **/
    private String evalOrgInsCode;

    /** 购入金额 **/
    private java.math.BigDecimal buyAmt;

    /** 内部评估确认金额（元） **/
    private java.math.BigDecimal innerEvalAmt;

    /** 下次估值到期日期 **/
    private String nextEvalEndDate;

    /** 下次估值日期 **/
    private String nextEvalDate;

    /** 是否资产保全 STD_ZB_YES_NO **/
    private String isSpecial;

    /** 是否保全资产STD_ZB_YES_NO **/
    private String isSpecialAsset;

    /** 是否成品房 STD_ZB_YES_NO **/
    private String isExistinghome;

    /** 是否需要修改 STD_ZB_YES_NO **/
    private String isUpdate;

    /** 授信评估金额 **/
    private java.math.BigDecimal lmtEvalAmt;

    /** 所在区域编号 **/
    private String areaCode;

    /** 所在区域名称 **/
    private String areaName;

    /** 复制来源 **/
    private String copyFrom;

    /** 是否变化 STD_ZB_YES_NO **/
    private String isChanged;

    /** 出入库流水号 **/
    private String inoutSerno;

    /** 入库人编号 **/
    private String inUser;

    /** 从属类型  STD_RIGHT_TYPE **/
    private String rightType;

    /** 权属证件类型 STD_RIGHT_CERT_TYPE_CODE **/
    private String rightCertTypeCode;

    /** 权属证件号 **/
    private String rightCertNo;

    /** 权属登记机关 **/
    private String rightOrg;

    /** 他项权证号到期日 **/
    private String registerEndDate;

    /** 来源渠道 **/
    private String sourcePath;

    /** 创建人用户编号 **/
    private String createUserId;

    /** 登记证明编号/止付通知书编号 **/
    private String registerNo;

    /** 抵押登记机关 **/
    private String registerOrg;

    /** 登记/止付日期 **/
    private String registerDate;

    /** 投保险种  **/
    private String assuranceType;

    /** 保单编号 **/
    private String assuranceNo;

    /** 保险金额（元） **/
    private java.math.BigDecimal assuranceAmt;

    /** 投保日期 **/
    private String assuranceDate;

    /** 保险到期日 **/
    private String assuranceEndDate;

    /** 保险公司名称 **/
    private String assuranceComName;

    /** 租赁情况  STD_TENANCY_CIRCE **/
    private String tenancyCirce;

    /** 租赁到期日期 **/
    private String tenancyEndDate;

    /** 年租金（元） **/
    private java.math.BigDecimal tenancyAmt;

    /** 保管人 **/
    private String keepUser;

    /** 申请入库时间 **/
    private String appInDate;

    /** 入库时间 **/
    private String inDate;

    /** 申请出库时间 **/
    private String appOutDate;

    /** 出库时间 **/
    private String outDate;

    /** 出库事由 **/
    private String outReason;

    /** 新押品编码 **/
    private String newcode;

    /** 新押品编码名称 **/
    private String newlabel;

    /** 质押特定类型 **/
    private String style;

    /** 外部评级机构 **/
    private String outerLevelOrg;

    /** 外部评级等级 **/
    private String outerLevel;

    /** 是否到期 STD_ZB_YES_NO **/
    private String isEndDate;

    /** 是否票据池 STD_ZB_YES_NO **/
    private String isPjc;

    /** 票据池状态 STD_PJC_STATUS **/
    private String rcState;

    /** 票据入池时质押率 **/
    private java.math.BigDecimal pjcZyl;

    /** 入池日期 **/
    private String rcDate;

    /** 出池日期 **/
    private String ccDate;

    /** 票据池合同编号 **/
    private String pjcContNo;

    /** 车架号 **/
    private String vehicleFlagCd;

    /** 审批状态STD_APP_ST **/
    private String approveStatus;

    /** 责任人 **/
    private String managerId;

    /** 责任机构 **/
    private String managerBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期  **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 操作类型  STD_OPR_TYPE **/
    private String oprType;

    /** 面积 **/
    private String squ;

    /** 权重法下变现能力/代偿能力要求 STD_ZB_SALE_STATE **/
    private String pctAbility;
    /** 初级内部评级法下变现能力/代偿能力要求 STD_ZB_SALE_STATE **/
    private String innerAbility;

    /**押品系统跳转调用页面**/
    private String callMethod;

    /**担保合同借款人编号,保证人调整押品系统时用**/
    private String borrowNo;

    /**权证编号,抵押注销选押品时用**/
    private String warrantNo;

    /** 权利价值 */
    private String certiAmt;

    public String getBorrowNo() {
        return borrowNo;
    }

    public void setBorrowNo(String borrowNo) {
        this.borrowNo = borrowNo;
    }


    public String getCallMethod() {
        return callMethod;
    }

    public void setCallMethod(String callMethod) {
        this.callMethod = callMethod;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno == null ? null : serno.trim();
    }

    /**
     * @return Serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param guarNo
     */
    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo == null ? null : guarNo.trim();
    }

    /**
     * @return GuarNo
     */
    public String getGuarNo() {
        return this.guarNo;
    }

    /**
     * @param grtFlag
     */
    public void setGrtFlag(String grtFlag) {
        this.grtFlag = grtFlag == null ? null : grtFlag.trim();
    }

    /**
     * @return GrtFlag
     */
    public String getGrtFlag() {
        return this.grtFlag;
    }

    /**
     * @param pldimnMemo
     */
    public void setPldimnMemo(String pldimnMemo) {
        this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
    }

    /**
     * @return PldimnMemo
     */
    public String getPldimnMemo() {
        return this.pldimnMemo;
    }

    /**
     * @param guarTypeCd
     */
    public void setGuarTypeCd(String guarTypeCd) {
        this.guarTypeCd = guarTypeCd == null ? null : guarTypeCd.trim();
    }

    /**
     * @return GuarTypeCd
     */
    public String getGuarTypeCd() {
        return this.guarTypeCd;
    }

    /**
     * @param guarType
     */
    public void setGuarType(String guarType) {
        this.guarType = guarType == null ? null : guarType.trim();
    }

    /**
     * @return GuarType
     */
    public String getGuarType() {
        return this.guarType;
    }

    /**
     * @param guarFirstCreateTime
     */
    public void setGuarFirstCreateTime(String guarFirstCreateTime) {
        this.guarFirstCreateTime = guarFirstCreateTime == null ? null : guarFirstCreateTime.trim();
    }

    /**
     * @return GuarFirstCreateTime
     */
    public String getGuarFirstCreateTime() {
        return this.guarFirstCreateTime;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId == null ? null : cusId.trim();
    }

    /**
     * @return CusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param guarCusId
     */
    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId == null ? null : guarCusId.trim();
    }

    /**
     * @return GuarCusId
     */
    public String getGuarCusId() {
        return this.guarCusId;
    }

    /**
     * @param guarCusName
     */
    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName == null ? null : guarCusName.trim();
    }

    /**
     * @return GuarCusName
     */
    public String getGuarCusName() {
        return this.guarCusName;
    }

    /**
     * @param guarCusType
     */
    public void setGuarCusType(String guarCusType) {
        this.guarCusType = guarCusType == null ? null : guarCusType.trim();
    }

    /**
     * @return GuarCusType
     */
    public String getGuarCusType() {
        return this.guarCusType;
    }

    /**
     * @param guarCertType
     */
    public void setGuarCertType(String guarCertType) {
        this.guarCertType = guarCertType == null ? null : guarCertType.trim();
    }

    /**
     * @return GuarCertType
     */
    public String getGuarCertType() {
        return this.guarCertType;
    }

    /**
     * @param guarCertCode
     */
    public void setGuarCertCode(String guarCertCode) {
        this.guarCertCode = guarCertCode == null ? null : guarCertCode.trim();
    }

    /**
     * @return GuarCertCode
     */
    public String getGuarCertCode() {
        return this.guarCertCode;
    }

    /**
     * @param assureCardNo
     */
    public void setAssureCardNo(String assureCardNo) {
        this.assureCardNo = assureCardNo == null ? null : assureCardNo.trim();
    }

    /**
     * @return AssureCardNo
     */
    public String getAssureCardNo() {
        return this.assureCardNo;
    }

    /**
     * @param relationInt
     */
    public void setRelationInt(String relationInt) {
        this.relationInt = relationInt == null ? null : relationInt.trim();
    }

    /**
     * @return RelationInt
     */
    public String getRelationInt() {
        return this.relationInt;
    }

    /**
     * @param commonAssetsInd
     */
    public void setCommonAssetsInd(String commonAssetsInd) {
        this.commonAssetsInd = commonAssetsInd == null ? null : commonAssetsInd.trim();
    }

    /**
     * @return CommonAssetsInd
     */
    public String getCommonAssetsInd() {
        return this.commonAssetsInd;
    }

    /**
     * @param occupyofowner
     */
    public void setOccupyofowner(String occupyofowner) {
        this.occupyofowner = occupyofowner == null ? null : occupyofowner.trim();
    }

    /**
     * @return Occupyofowner
     */
    public String getOccupyofowner() {
        return this.occupyofowner;
    }

    /**
     * @param insuranceInd
     */
    public void setInsuranceInd(String insuranceInd) {
        this.insuranceInd = insuranceInd == null ? null : insuranceInd.trim();
    }

    /**
     * @return InsuranceInd
     */
    public String getInsuranceInd() {
        return this.insuranceInd;
    }

    /**
     * @param notarizationInd
     */
    public void setNotarizationInd(String notarizationInd) {
        this.notarizationInd = notarizationInd == null ? null : notarizationInd.trim();
    }

    /**
     * @return NotarizationInd
     */
    public String getNotarizationInd() {
        return this.notarizationInd;
    }

    /**
     * @param forcecreateInd
     */
    public void setForcecreateInd(String forcecreateInd) {
        this.forcecreateInd = forcecreateInd == null ? null : forcecreateInd.trim();
    }

    /**
     * @return ForcecreateInd
     */
    public String getForcecreateInd() {
        return this.forcecreateInd;
    }

    /**
     * @param forcecreateReason
     */
    public void setForcecreateReason(String forcecreateReason) {
        this.forcecreateReason = forcecreateReason == null ? null : forcecreateReason.trim();
    }

    /**
     * @return ForcecreateReason
     */
    public String getForcecreateReason() {
        return this.forcecreateReason;
    }

    /**
     * @param guarBusistate
     */
    public void setGuarBusistate(String guarBusistate) {
        this.guarBusistate = guarBusistate == null ? null : guarBusistate.trim();
    }

    /**
     * @return GuarBusistate
     */
    public String getGuarBusistate() {
        return this.guarBusistate;
    }

    /**
     * @param guarState
     */
    public void setGuarState(String guarState) {
        this.guarState = guarState == null ? null : guarState.trim();
    }

    /**
     * @return GuarState
     */
    public String getGuarState() {
        return this.guarState;
    }

    /**
     * @param isOwnershipClear
     */
    public void setIsOwnershipClear(String isOwnershipClear) {
        this.isOwnershipClear = isOwnershipClear == null ? null : isOwnershipClear.trim();
    }

    /**
     * @return IsOwnershipClear
     */
    public String getIsOwnershipClear() {
        return this.isOwnershipClear;
    }

    /**
     * @param isDebtAsset
     */
    public void setIsDebtAsset(String isDebtAsset) {
        this.isDebtAsset = isDebtAsset == null ? null : isDebtAsset.trim();
    }

    /**
     * @return IsDebtAsset
     */
    public String getIsDebtAsset() {
        return this.isDebtAsset;
    }

    /**
     * @param forbidCirBelogFlag
     */
    public void setForbidCirBelogFlag(String forbidCirBelogFlag) {
        this.forbidCirBelogFlag = forbidCirBelogFlag == null ? null : forbidCirBelogFlag.trim();
    }

    /**
     * @return ForbidCirBelogFlag
     */
    public String getForbidCirBelogFlag() {
        return this.forbidCirBelogFlag;
    }

    /**
     * @param supervisionFlag
     */
    public void setSupervisionFlag(String supervisionFlag) {
        this.supervisionFlag = supervisionFlag == null ? null : supervisionFlag.trim();
    }

    /**
     * @return SupervisionFlag
     */
    public String getSupervisionFlag() {
        return this.supervisionFlag;
    }

    /**
     * @param supervisionDate
     */
    public void setSupervisionDate(String supervisionDate) {
        this.supervisionDate = supervisionDate == null ? null : supervisionDate.trim();
    }

    /**
     * @return SupervisionDate
     */
    public String getSupervisionDate() {
        return this.supervisionDate;
    }

    /**
     * @param enforeFlag
     */
    public void setEnforeFlag(String enforeFlag) {
        this.enforeFlag = enforeFlag == null ? null : enforeFlag.trim();
    }

    /**
     * @return EnforeFlag
     */
    public String getEnforeFlag() {
        return this.enforeFlag;
    }

    /**
     * @param rightOtherNo
     */
    public void setRightOtherNo(String rightOtherNo) {
        this.rightOtherNo = rightOtherNo == null ? null : rightOtherNo.trim();
    }

    /**
     * @return RightOtherNo
     */
    public String getRightOtherNo() {
        return this.rightOtherNo;
    }

    /**
     * @param regState
     */
    public void setRegState(String regState) {
        this.regState = regState == null ? null : regState.trim();
    }

    /**
     * @return RegState
     */
    public String getRegState() {
        return this.regState;
    }

    /**
     * @param defEffectType
     */
    public void setDefEffectType(String defEffectType) {
        this.defEffectType = defEffectType == null ? null : defEffectType.trim();
    }

    /**
     * @return DefEffectType
     */
    public String getDefEffectType() {
        return this.defEffectType;
    }

    /**
     * @param otherBackGuarInd
     */
    public void setOtherBackGuarInd(String otherBackGuarInd) {
        this.otherBackGuarInd = otherBackGuarInd == null ? null : otherBackGuarInd.trim();
    }

    /**
     * @return OtherBackGuarInd
     */
    public String getOtherBackGuarInd() {
        return this.otherBackGuarInd;
    }

    /**
     * @param pldimnDebitRelative
     */
    public void setPldimnDebitRelative(String pldimnDebitRelative) {
        this.pldimnDebitRelative = pldimnDebitRelative == null ? null : pldimnDebitRelative.trim();
    }

    /**
     * @return PldimnDebitRelative
     */
    public String getPldimnDebitRelative() {
        return this.pldimnDebitRelative;
    }

    /**
     * @param supervisionConvenience
     */
    public void setSupervisionConvenience(String supervisionConvenience) {
        this.supervisionConvenience = supervisionConvenience == null ? null : supervisionConvenience.trim();
    }

    /**
     * @return SupervisionConvenience
     */
    public String getSupervisionConvenience() {
        return this.supervisionConvenience;
    }

    /**
     * @param lawValidity
     */
    public void setLawValidity(String lawValidity) {
        this.lawValidity = lawValidity == null ? null : lawValidity.trim();
    }

    /**
     * @return LawValidity
     */
    public String getLawValidity() {
        return this.lawValidity;
    }

    /**
     * @param pldimnCommon
     */
    public void setPldimnCommon(String pldimnCommon) {
        this.pldimnCommon = pldimnCommon == null ? null : pldimnCommon.trim();
    }

    /**
     * @return PldimnCommon
     */
    public String getPldimnCommon() {
        return this.pldimnCommon;
    }

    /**
     * @param pldimnCashability
     */
    public void setPldimnCashability(String pldimnCashability) {
        this.pldimnCashability = pldimnCashability == null ? null : pldimnCashability.trim();
    }

    /**
     * @return PldimnCashability
     */
    public String getPldimnCashability() {
        return this.pldimnCashability;
    }

    /**
     * @param priceWave
     */
    public void setPriceWave(String priceWave) {
        this.priceWave = priceWave == null ? null : priceWave.trim();
    }

    /**
     * @return PriceWave
     */
    public String getPriceWave() {
        return this.priceWave;
    }

    /**
     * @param isScanMater
     */
    public void setIsScanMater(String isScanMater) {
        this.isScanMater = isScanMater == null ? null : isScanMater.trim();
    }

    /**
     * @return IsScanMater
     */
    public String getIsScanMater() {
        return this.isScanMater;
    }

    /**
     * @param ringhtNoAndOtherNo
     */
    public void setRinghtNoAndOtherNo(String ringhtNoAndOtherNo) {
        this.ringhtNoAndOtherNo = ringhtNoAndOtherNo == null ? null : ringhtNoAndOtherNo.trim();
    }

    /**
     * @return RinghtNoAndOtherNo
     */
    public String getRinghtNoAndOtherNo() {
        return this.ringhtNoAndOtherNo;
    }

    /**
     * @param mybackGuarFirstSeq
     */
    public void setMybackGuarFirstSeq(String mybackGuarFirstSeq) {
        this.mybackGuarFirstSeq = mybackGuarFirstSeq == null ? null : mybackGuarFirstSeq.trim();
    }

    /**
     * @return MybackGuarFirstSeq
     */
    public String getMybackGuarFirstSeq() {
        return this.mybackGuarFirstSeq;
    }

    /**
     * @param legalPriPayment
     */
    public void setLegalPriPayment(java.math.BigDecimal legalPriPayment) {
        this.legalPriPayment = legalPriPayment;
    }

    /**
     * @return LegalPriPayment
     */
    public java.math.BigDecimal getLegalPriPayment() {
        return this.legalPriPayment;
    }

    /**
     * @param createSys
     */
    public void setCreateSys(String createSys) {
        this.createSys = createSys == null ? null : createSys.trim();
    }

    /**
     * @return CreateSys
     */
    public String getCreateSys() {
        return this.createSys;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return Remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param accountManager
     */
    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager == null ? null : accountManager.trim();
    }

    /**
     * @return AccountManager
     */
    public String getAccountManager() {
        return this.accountManager;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return EvalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    /**
     * @param evalDate
     */
    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate == null ? null : evalDate.trim();
    }

    /**
     * @return EvalDate
     */
    public String getEvalDate() {
        return this.evalDate;
    }

    /**
     * @param confirmAmt
     */
    public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
        this.confirmAmt = confirmAmt;
    }

    /**
     * @return ConfirmAmt
     */
    public java.math.BigDecimal getConfirmAmt() {
        return this.confirmAmt;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType == null ? null : curType.trim();
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param certiRecordId
     */
    public void setCertiRecordId(String certiRecordId) {
        this.certiRecordId = certiRecordId == null ? null : certiRecordId.trim();
    }

    /**
     * @return CertiRecordId
     */
    public String getCertiRecordId() {
        return this.certiRecordId;
    }

    /**
     * @param certiEndDate
     */
    public void setCertiEndDate(String certiEndDate) {
        this.certiEndDate = certiEndDate == null ? null : certiEndDate.trim();
    }

    /**
     * @return CertiEndDate
     */
    public String getCertiEndDate() {
        return this.certiEndDate;
    }

    /**
     * @param mortagageMaxRate
     */
    public void setMortagageMaxRate(java.math.BigDecimal mortagageMaxRate) {
        this.mortagageMaxRate = mortagageMaxRate;
    }

    /**
     * @return MortagageMaxRate
     */
    public java.math.BigDecimal getMortagageMaxRate() {
        return this.mortagageMaxRate;
    }

    /**
     * @param mortagageRate
     */
    public void setMortagageRate(java.math.BigDecimal mortagageRate) {
        this.mortagageRate = mortagageRate;
    }

    /**
     * @return MortagageRate
     */
    public java.math.BigDecimal getMortagageRate() {
        return this.mortagageRate;
    }

    /**
     * @param maxMortagageAmt
     */
    public void setMaxMortagageAmt(java.math.BigDecimal maxMortagageAmt) {
        this.maxMortagageAmt = maxMortagageAmt;
    }

    /**
     * @return MaxMortagageAmt
     */
    public java.math.BigDecimal getMaxMortagageAmt() {
        return this.maxMortagageAmt;
    }

    /**
     * @param pldLocation
     */
    public void setPldLocation(String pldLocation) {
        this.pldLocation = pldLocation == null ? null : pldLocation.trim();
    }

    /**
     * @return PldLocation
     */
    public String getPldLocation() {
        return this.pldLocation;
    }

    /**
     * @param refName
     */
    public void setRefName(String refName) {
        this.refName = refName == null ? null : refName.trim();
    }

    /**
     * @return RefName
     */
    public String getRefName() {
        return this.refName;
    }

    /**
     * @param isDisputed
     */
    public void setIsDisputed(String isDisputed) {
        this.isDisputed = isDisputed == null ? null : isDisputed.trim();
    }

    /**
     * @return IsDisputed
     */
    public String getIsDisputed() {
        return this.isDisputed;
    }

    /**
     * @param coreGuarantyNo
     */
    public void setCoreGuarantyNo(String coreGuarantyNo) {
        this.coreGuarantyNo = coreGuarantyNo == null ? null : coreGuarantyNo.trim();
    }

    /**
     * @return CoreGuarantyNo
     */
    public String getCoreGuarantyNo() {
        return this.coreGuarantyNo;
    }

    /**
     * @param coreGuarantySeq
     */
    public void setCoreGuarantySeq(String coreGuarantySeq) {
        this.coreGuarantySeq = coreGuarantySeq == null ? null : coreGuarantySeq.trim();
    }

    /**
     * @return CoreGuarantySeq
     */
    public String getCoreGuarantySeq() {
        return this.coreGuarantySeq;
    }

    /**
     * @param finaBrId
     */
    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId == null ? null : finaBrId.trim();
    }

    /**
     * @return FinaBrId
     */
    public String getFinaBrId() {
        return this.finaBrId;
    }

    /**
     * @param confirmDate
     */
    public void setConfirmDate(String confirmDate) {
        this.confirmDate = confirmDate == null ? null : confirmDate.trim();
    }

    /**
     * @return ConfirmDate
     */
    public String getConfirmDate() {
        return this.confirmDate;
    }

    /**
     * @param evalType
     */
    public void setEvalType(String evalType) {
        this.evalType = evalType == null ? null : evalType.trim();
    }

    /**
     * @return EvalType
     */
    public String getEvalType() {
        return this.evalType;
    }

    /**
     * @param evalOrg
     */
    public void setEvalOrg(String evalOrg) {
        this.evalOrg = evalOrg == null ? null : evalOrg.trim();
    }

    /**
     * @return EvalOrg
     */
    public String getEvalOrg() {
        return this.evalOrg;
    }

    /**
     * @param evalOrgInsCode
     */
    public void setEvalOrgInsCode(String evalOrgInsCode) {
        this.evalOrgInsCode = evalOrgInsCode == null ? null : evalOrgInsCode.trim();
    }

    /**
     * @return EvalOrgInsCode
     */
    public String getEvalOrgInsCode() {
        return this.evalOrgInsCode;
    }

    /**
     * @param buyAmt
     */
    public void setBuyAmt(java.math.BigDecimal buyAmt) {
        this.buyAmt = buyAmt;
    }

    /**
     * @return BuyAmt
     */
    public java.math.BigDecimal getBuyAmt() {
        return this.buyAmt;
    }

    /**
     * @param innerEvalAmt
     */
    public void setInnerEvalAmt(java.math.BigDecimal innerEvalAmt) {
        this.innerEvalAmt = innerEvalAmt;
    }

    /**
     * @return InnerEvalAmt
     */
    public java.math.BigDecimal getInnerEvalAmt() {
        return this.innerEvalAmt;
    }

    /**
     * @param nextEvalEndDate
     */
    public void setNextEvalEndDate(String nextEvalEndDate) {
        this.nextEvalEndDate = nextEvalEndDate == null ? null : nextEvalEndDate.trim();
    }

    /**
     * @return NextEvalEndDate
     */
    public String getNextEvalEndDate() {
        return this.nextEvalEndDate;
    }

    /**
     * @param nextEvalDate
     */
    public void setNextEvalDate(String nextEvalDate) {
        this.nextEvalDate = nextEvalDate == null ? null : nextEvalDate.trim();
    }

    /**
     * @return NextEvalDate
     */
    public String getNextEvalDate() {
        return this.nextEvalDate;
    }

    /**
     * @param isSpecial
     */
    public void setIsSpecial(String isSpecial) {
        this.isSpecial = isSpecial == null ? null : isSpecial.trim();
    }

    /**
     * @return IsSpecial
     */
    public String getIsSpecial() {
        return this.isSpecial;
    }

    /**
     * @param isSpecialAsset
     */
    public void setIsSpecialAsset(String isSpecialAsset) {
        this.isSpecialAsset = isSpecialAsset == null ? null : isSpecialAsset.trim();
    }

    /**
     * @return IsSpecialAsset
     */
    public String getIsSpecialAsset() {
        return this.isSpecialAsset;
    }

    /**
     * @param isExistinghome
     */
    public void setIsExistinghome(String isExistinghome) {
        this.isExistinghome = isExistinghome == null ? null : isExistinghome.trim();
    }

    /**
     * @return IsExistinghome
     */
    public String getIsExistinghome() {
        return this.isExistinghome;
    }

    /**
     * @param isUpdate
     */
    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate == null ? null : isUpdate.trim();
    }

    /**
     * @return IsUpdate
     */
    public String getIsUpdate() {
        return this.isUpdate;
    }

    /**
     * @param lmtEvalAmt
     */
    public void setLmtEvalAmt(java.math.BigDecimal lmtEvalAmt) {
        this.lmtEvalAmt = lmtEvalAmt;
    }

    /**
     * @return LmtEvalAmt
     */
    public java.math.BigDecimal getLmtEvalAmt() {
        return this.lmtEvalAmt;
    }

    /**
     * @param areaCode
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    /**
     * @return AreaCode
     */
    public String getAreaCode() {
        return this.areaCode;
    }

    /**
     * @param areaName
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    /**
     * @return AreaName
     */
    public String getAreaName() {
        return this.areaName;
    }

    /**
     * @param copyFrom
     */
    public void setCopyFrom(String copyFrom) {
        this.copyFrom = copyFrom == null ? null : copyFrom.trim();
    }

    /**
     * @return CopyFrom
     */
    public String getCopyFrom() {
        return this.copyFrom;
    }

    /**
     * @param isChanged
     */
    public void setIsChanged(String isChanged) {
        this.isChanged = isChanged == null ? null : isChanged.trim();
    }

    /**
     * @return IsChanged
     */
    public String getIsChanged() {
        return this.isChanged;
    }

    /**
     * @param inoutSerno
     */
    public void setInoutSerno(String inoutSerno) {
        this.inoutSerno = inoutSerno == null ? null : inoutSerno.trim();
    }

    /**
     * @return InoutSerno
     */
    public String getInoutSerno() {
        return this.inoutSerno;
    }

    /**
     * @param inUser
     */
    public void setInUser(String inUser) {
        this.inUser = inUser == null ? null : inUser.trim();
    }

    /**
     * @return InUser
     */
    public String getInUser() {
        return this.inUser;
    }

    /**
     * @param rightType
     */
    public void setRightType(String rightType) {
        this.rightType = rightType == null ? null : rightType.trim();
    }

    /**
     * @return RightType
     */
    public String getRightType() {
        return this.rightType;
    }

    /**
     * @param rightCertTypeCode
     */
    public void setRightCertTypeCode(String rightCertTypeCode) {
        this.rightCertTypeCode = rightCertTypeCode == null ? null : rightCertTypeCode.trim();
    }

    /**
     * @return RightCertTypeCode
     */
    public String getRightCertTypeCode() {
        return this.rightCertTypeCode;
    }

    /**
     * @param rightCertNo
     */
    public void setRightCertNo(String rightCertNo) {
        this.rightCertNo = rightCertNo == null ? null : rightCertNo.trim();
    }

    /**
     * @return RightCertNo
     */
    public String getRightCertNo() {
        return this.rightCertNo;
    }

    /**
     * @param rightOrg
     */
    public void setRightOrg(String rightOrg) {
        this.rightOrg = rightOrg == null ? null : rightOrg.trim();
    }

    /**
     * @return RightOrg
     */
    public String getRightOrg() {
        return this.rightOrg;
    }

    /**
     * @param registerEndDate
     */
    public void setRegisterEndDate(String registerEndDate) {
        this.registerEndDate = registerEndDate == null ? null : registerEndDate.trim();
    }

    /**
     * @return RegisterEndDate
     */
    public String getRegisterEndDate() {
        return this.registerEndDate;
    }

    /**
     * @param sourcePath
     */
    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath == null ? null : sourcePath.trim();
    }

    /**
     * @return SourcePath
     */
    public String getSourcePath() {
        return this.sourcePath;
    }

    /**
     * @param createUserId
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId == null ? null : createUserId.trim();
    }

    /**
     * @return CreateUserId
     */
    public String getCreateUserId() {
        return this.createUserId;
    }

    /**
     * @param registerNo
     */
    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo == null ? null : registerNo.trim();
    }

    /**
     * @return RegisterNo
     */
    public String getRegisterNo() {
        return this.registerNo;
    }

    /**
     * @param registerOrg
     */
    public void setRegisterOrg(String registerOrg) {
        this.registerOrg = registerOrg == null ? null : registerOrg.trim();
    }

    /**
     * @return RegisterOrg
     */
    public String getRegisterOrg() {
        return this.registerOrg;
    }

    /**
     * @param registerDate
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate == null ? null : registerDate.trim();
    }

    /**
     * @return RegisterDate
     */
    public String getRegisterDate() {
        return this.registerDate;
    }

    /**
     * @param assuranceType
     */
    public void setAssuranceType(String assuranceType) {
        this.assuranceType = assuranceType == null ? null : assuranceType.trim();
    }

    /**
     * @return AssuranceType
     */
    public String getAssuranceType() {
        return this.assuranceType;
    }

    /**
     * @param assuranceNo
     */
    public void setAssuranceNo(String assuranceNo) {
        this.assuranceNo = assuranceNo == null ? null : assuranceNo.trim();
    }

    /**
     * @return AssuranceNo
     */
    public String getAssuranceNo() {
        return this.assuranceNo;
    }

    /**
     * @param assuranceAmt
     */
    public void setAssuranceAmt(java.math.BigDecimal assuranceAmt) {
        this.assuranceAmt = assuranceAmt;
    }

    /**
     * @return AssuranceAmt
     */
    public java.math.BigDecimal getAssuranceAmt() {
        return this.assuranceAmt;
    }

    /**
     * @param assuranceDate
     */
    public void setAssuranceDate(String assuranceDate) {
        this.assuranceDate = assuranceDate == null ? null : assuranceDate.trim();
    }

    /**
     * @return AssuranceDate
     */
    public String getAssuranceDate() {
        return this.assuranceDate;
    }

    /**
     * @param assuranceEndDate
     */
    public void setAssuranceEndDate(String assuranceEndDate) {
        this.assuranceEndDate = assuranceEndDate == null ? null : assuranceEndDate.trim();
    }

    /**
     * @return AssuranceEndDate
     */
    public String getAssuranceEndDate() {
        return this.assuranceEndDate;
    }

    /**
     * @param assuranceComName
     */
    public void setAssuranceComName(String assuranceComName) {
        this.assuranceComName = assuranceComName == null ? null : assuranceComName.trim();
    }

    /**
     * @return AssuranceComName
     */
    public String getAssuranceComName() {
        return this.assuranceComName;
    }

    /**
     * @param tenancyCirce
     */
    public void setTenancyCirce(String tenancyCirce) {
        this.tenancyCirce = tenancyCirce == null ? null : tenancyCirce.trim();
    }

    /**
     * @return TenancyCirce
     */
    public String getTenancyCirce() {
        return this.tenancyCirce;
    }

    /**
     * @param tenancyEndDate
     */
    public void setTenancyEndDate(String tenancyEndDate) {
        this.tenancyEndDate = tenancyEndDate == null ? null : tenancyEndDate.trim();
    }

    /**
     * @return TenancyEndDate
     */
    public String getTenancyEndDate() {
        return this.tenancyEndDate;
    }

    /**
     * @param tenancyAmt
     */
    public void setTenancyAmt(java.math.BigDecimal tenancyAmt) {
        this.tenancyAmt = tenancyAmt;
    }

    /**
     * @return TenancyAmt
     */
    public java.math.BigDecimal getTenancyAmt() {
        return this.tenancyAmt;
    }

    /**
     * @param keepUser
     */
    public void setKeepUser(String keepUser) {
        this.keepUser = keepUser == null ? null : keepUser.trim();
    }

    /**
     * @return KeepUser
     */
    public String getKeepUser() {
        return this.keepUser;
    }

    /**
     * @param appInDate
     */
    public void setAppInDate(String appInDate) {
        this.appInDate = appInDate == null ? null : appInDate.trim();
    }

    /**
     * @return AppInDate
     */
    public String getAppInDate() {
        return this.appInDate;
    }

    /**
     * @param inDate
     */
    public void setInDate(String inDate) {
        this.inDate = inDate == null ? null : inDate.trim();
    }

    /**
     * @return InDate
     */
    public String getInDate() {
        return this.inDate;
    }

    /**
     * @param appOutDate
     */
    public void setAppOutDate(String appOutDate) {
        this.appOutDate = appOutDate == null ? null : appOutDate.trim();
    }

    /**
     * @return AppOutDate
     */
    public String getAppOutDate() {
        return this.appOutDate;
    }

    /**
     * @param outDate
     */
    public void setOutDate(String outDate) {
        this.outDate = outDate == null ? null : outDate.trim();
    }

    /**
     * @return OutDate
     */
    public String getOutDate() {
        return this.outDate;
    }

    /**
     * @param outReason
     */
    public void setOutReason(String outReason) {
        this.outReason = outReason == null ? null : outReason.trim();
    }

    /**
     * @return OutReason
     */
    public String getOutReason() {
        return this.outReason;
    }

    /**
     * @param newcode
     */
    public void setNewcode(String newcode) {
        this.newcode = newcode == null ? null : newcode.trim();
    }

    /**
     * @return Newcode
     */
    public String getNewcode() {
        return this.newcode;
    }

    /**
     * @param newlabel
     */
    public void setNewlabel(String newlabel) {
        this.newlabel = newlabel == null ? null : newlabel.trim();
    }

    /**
     * @return Newlabel
     */
    public String getNewlabel() {
        return this.newlabel;
    }

    /**
     * @param style
     */
    public void setStyle(String style) {
        this.style = style == null ? null : style.trim();
    }

    /**
     * @return Style
     */
    public String getStyle() {
        return this.style;
    }

    /**
     * @param outerLevelOrg
     */
    public void setOuterLevelOrg(String outerLevelOrg) {
        this.outerLevelOrg = outerLevelOrg == null ? null : outerLevelOrg.trim();
    }

    /**
     * @return OuterLevelOrg
     */
    public String getOuterLevelOrg() {
        return this.outerLevelOrg;
    }

    /**
     * @param outerLevel
     */
    public void setOuterLevel(String outerLevel) {
        this.outerLevel = outerLevel == null ? null : outerLevel.trim();
    }

    /**
     * @return OuterLevel
     */
    public String getOuterLevel() {
        return this.outerLevel;
    }

    /**
     * @param isEndDate
     */
    public void setIsEndDate(String isEndDate) {
        this.isEndDate = isEndDate == null ? null : isEndDate.trim();
    }

    /**
     * @return IsEndDate
     */
    public String getIsEndDate() {
        return this.isEndDate;
    }

    /**
     * @param isPjc
     */
    public void setIsPjc(String isPjc) {
        this.isPjc = isPjc == null ? null : isPjc.trim();
    }

    /**
     * @return IsPjc
     */
    public String getIsPjc() {
        return this.isPjc;
    }

    /**
     * @param rcState
     */
    public void setRcState(String rcState) {
        this.rcState = rcState == null ? null : rcState.trim();
    }

    /**
     * @return RcState
     */
    public String getRcState() {
        return this.rcState;
    }

    /**
     * @param pjcZyl
     */
    public void setPjcZyl(java.math.BigDecimal pjcZyl) {
        this.pjcZyl = pjcZyl;
    }

    /**
     * @return PjcZyl
     */
    public java.math.BigDecimal getPjcZyl() {
        return this.pjcZyl;
    }

    /**
     * @param rcDate
     */
    public void setRcDate(String rcDate) {
        this.rcDate = rcDate == null ? null : rcDate.trim();
    }

    /**
     * @return RcDate
     */
    public String getRcDate() {
        return this.rcDate;
    }

    /**
     * @param ccDate
     */
    public void setCcDate(String ccDate) {
        this.ccDate = ccDate == null ? null : ccDate.trim();
    }

    /**
     * @return CcDate
     */
    public String getCcDate() {
        return this.ccDate;
    }

    /**
     * @param pjcContNo
     */
    public void setPjcContNo(String pjcContNo) {
        this.pjcContNo = pjcContNo == null ? null : pjcContNo.trim();
    }

    /**
     * @return PjcContNo
     */
    public String getPjcContNo() {
        return this.pjcContNo;
    }

    /**
     * @param vehicleFlagCd
     */
    public void setVehicleFlagCd(String vehicleFlagCd) {
        this.vehicleFlagCd = vehicleFlagCd == null ? null : vehicleFlagCd.trim();
    }

    /**
     * @return VehicleFlagCd
     */
    public String getVehicleFlagCd() {
        return this.vehicleFlagCd;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus == null ? null : approveStatus.trim();
    }

    /**
     * @return ApproveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    public String getPctAbility() {
        return pctAbility;
    }

    public void setPctAbility(String pctAbility) {
        this.pctAbility = pctAbility;
    }

    public String getInnerAbility() {
        return innerAbility;
    }

    public void setInnerAbility(String innerAbility) {
        this.innerAbility = innerAbility;
    }

    public String getWarrantNo() {
        return warrantNo;
    }

    public void setWarrantNo(String warrantNo) {
        this.warrantNo = warrantNo;
    }

    public String getSqu() {
        return squ;
    }

    public void setSqu(String squ) {
        this.squ = squ;
    }

    public String getCertiAmt() {
        return certiAmt;
    }

    public void setCertiAmt(String certiAmt) {
        this.certiAmt = certiAmt;
    }
}