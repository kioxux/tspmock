/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022CusListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.IqpHighAmtAgrAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHighAmtAgrAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-12 13:58:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpHighAmtAgrAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpHighAmtAgrAppService.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private IqpHighAmtAgrAppMapper iqpHighAmtAgrAppMapper;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;//银承合同

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;//开证合同申请详情

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请

    @Autowired
    private IqpDiscAppService iqpDiscAppService;//贴现协议申请

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;//委托贷款

    @Autowired
    private LmtAppService lmtAppService; //授信申请

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;//担保合同和业务关系

    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同表

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private LmtReplyChgService lmtReplyChgService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpHighAmtAgrApp selectByPrimaryKey(String pkId) {
        return iqpHighAmtAgrAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpHighAmtAgrApp> selectAll(QueryModel model) {
        List<IqpHighAmtAgrApp> records = (List<IqpHighAmtAgrApp>) iqpHighAmtAgrAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpHighAmtAgrApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpHighAmtAgrApp> list = iqpHighAmtAgrAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpHighAmtAgrApp record) {
        return iqpHighAmtAgrAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpHighAmtAgrApp record) {
        return iqpHighAmtAgrAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpHighAmtAgrApp record) {
        return iqpHighAmtAgrAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpHighAmtAgrApp record) {
        return iqpHighAmtAgrAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpHighAmtAgrAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpHighAmtAgrAppMapper.deleteByIds(ids);
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("流程发起-获取最高额授信业务申请" + iqpSerno + "申请主表信息");
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(iqpSerno);
        if (iqpHighAmtAgrApp == null) {
            throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }
        int updateCount = 0;
        log.info("流程发起-更新最高额授信业务申请" + iqpSerno + "流程审批状态为【111】-审批中");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
        if (updateCount < 0) {
            throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //向额度系统发送接口,占用额度
        this.sendToLmt(iqpHighAmtAgrApp);
    }

    public void sendToLmt(IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        String guarMode = iqpHighAmtAgrApp.getGuarMode();
        String isAmtEnough = checkLmtAmtIsEnough(iqpHighAmtAgrApp);

        String isLriskBiz = "";
        BigDecimal bizTotalAmt = BigDecimal.ZERO;
        BigDecimal bizSpacAmt = BigDecimal.ZERO;
        BigDecimal bizTotalAmtCny = BigDecimal.ZERO;
        BigDecimal bizSpacAmtCny = BigDecimal.ZERO;

        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpHighAmtAgrApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpHighAmtAgrApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode) && !CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                && !CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
            isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_0;//是否低风险
            bizTotalAmt = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
            bizSpacAmt = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
            bizTotalAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
            bizSpacAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
        }else{
            isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_1;//是否低风险
            bizTotalAmt = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
            bizSpacAmt = BigDecimal.ZERO;
            bizTotalAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
            bizSpacAmtCny = BigDecimal.ZERO;
        }

        CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
        cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpHighAmtAgrApp.getManagerBrId()));//金融机构代码
        cmisLmt0011ReqDto.setDealBizNo(iqpHighAmtAgrApp.getContNo());//合同编号
        cmisLmt0011ReqDto.setCusId(iqpHighAmtAgrApp.getCusId());//客户编号
        cmisLmt0011ReqDto.setCusName(iqpHighAmtAgrApp.getCusName());//客户名称
        cmisLmt0011ReqDto.setDealBizType(iqpHighAmtAgrApp.getContType());//交易业务类型
        cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
        cmisLmt0011ReqDto.setPrdId("");//产品编号
        cmisLmt0011ReqDto.setPrdName("");//产品名称
        cmisLmt0011ReqDto.setIsLriskBiz(isLriskBiz);//是否低风险
        cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
        cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
        cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
        cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
        cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
        cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
        cmisLmt0011ReqDto.setDealBizAmt(iqpHighAmtAgrApp.getAgrContHighAvlAmt());//交易业务金额
        cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
        cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
        cmisLmt0011ReqDto.setStartDate(iqpHighAmtAgrApp.getStartDate());//合同起始日
        cmisLmt0011ReqDto.setEndDate(iqpHighAmtAgrApp.getEndDate());//合同到期日
        cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
        cmisLmt0011ReqDto.setInputId(iqpHighAmtAgrApp.getInputId());//登记人
        cmisLmt0011ReqDto.setInputBrId(iqpHighAmtAgrApp.getInputBrId());//登记机构
        cmisLmt0011ReqDto.setInputDate(iqpHighAmtAgrApp.getInputDate());//登记日期

        List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

        CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
        cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
        cmisLmt0011OccRelListDto.setLmtSubNo(iqpHighAmtAgrApp.getLmtAccNo());//额度分项编号
        cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
        cmisLmt0011OccRelListDto.setBizTotalAmt(bizTotalAmt);//占用总额(原币种)
        cmisLmt0011OccRelListDto.setBizSpacAmt(bizSpacAmt);//占用敞口(原币种)
        cmisLmt0011OccRelListDto.setBizTotalAmtCny(bizTotalAmtCny);//占用总额(折人民币)
        cmisLmt0011OccRelListDto.setBizSpacAmtCny(bizSpacAmtCny);//占用敞口(折人民币)
        cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
        cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

        log.info("最高额授信协议申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpHighAmtAgrApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
        ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
        log.info("最高额授信协议申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpHighAmtAgrApp.getSerno(), JSON.toJSONString(resultDtoDto));
        if(!"0".equals(resultDtoDto.getCode())){
            log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
            throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        String code = resultDtoDto.getData().getErrorCode();
        if(!"0000".equals(code)){
            log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
            throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
    }

    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpHighAmtAgrAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取最高额授信业务申请" + iqpSerno + "申请主表信息");
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(iqpSerno);
        if (iqpHighAmtAgrApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        String guarMode = iqpHighAmtAgrApp.getGuarMode();
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpHighAmtAgrApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpHighAmtAgrApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        int updateCount = 0;
        log.info("审批通过-更新业务申请" + iqpSerno + "流程审批状态为【997】-通过");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }

        log.info("审批通过生成最高额授信合同信息-合同填充业务申请开始" + iqpSerno + "信息");
        //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
        //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
        CtrHighAmtAgrCont ctrHighAmtAgrCont = new CtrHighAmtAgrCont();
        BeanUtils.copyProperties(iqpHighAmtAgrApp, ctrHighAmtAgrCont);
        //获取主键,生成合同数据
        ctrHighAmtAgrCont.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_01);
        ctrHighAmtAgrCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);
        ctrHighAmtAgrCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);//合同状态默认【未生效】
        //ctrHighAmtAgrCont.setContPrintNum(new BigDecimal("0"));//合同打印次数默认设置为【0】
        log.info("最高额授信合同信息【{}】填充完成" + ctrHighAmtAgrCont.toString() + "信息");
        int insertCount = ctrHighAmtAgrContService.insertSelective(ctrHighAmtAgrCont);
        if (insertCount < 0) {
            log.error("新增异常！");
            throw new YuspException(EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }
        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpHighAmtAgrApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpHighAmtAgrApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrHighAmtAgrCont origiCtrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(origiContNo);
                if(Objects.nonNull(origiCtrHighAmtAgrCont)) {
                    origiCtrHighAmtAgrCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    updateCount = ctrHighAmtAgrContService.updateSelective(origiCtrHighAmtAgrCont);
                }
                if (updateCount < 1) {
                    throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                }
            }
        }
        //判断是否是低风险的一步流程，是：进行低风险占额后再按照正常流程走；否：直接按照正常流程走
        if((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode))) {

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpHighAmtAgrApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpHighAmtAgrApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpHighAmtAgrApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpHighAmtAgrApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpHighAmtAgrApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId("");//产品编号
            cmisLmt0011ReqDto.setPrdName("");//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpHighAmtAgrApp.getAgrContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpHighAmtAgrApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpHighAmtAgrApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpHighAmtAgrApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpHighAmtAgrApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpHighAmtAgrApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpHighAmtAgrApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpHighAmtAgrApp.getAgrContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpHighAmtAgrApp.getAgrContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("最高额授信协议申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("最高额授信协议申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
        log.info("审批通过生成最高额授信合同信息-合同填充业务申请" + iqpSerno + "结束");
    }

    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpHighAmtAgrApp selectByHighAmtAgrSernoKey(String iqpSerno) {
        return iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(iqpSerno);
    }

    /**
     * 最高额授信协议申请新增页面点击下一步
     * @param iqpHighAmtAgrApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpHighAmtAgrAppInfo(IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpHighAmtAgrApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpHighAmtAgrApp.getCusId();
            // 将 地址与联系方式 信息更新值贷款申请表中
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",cusId);
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",cusId, cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpHighAmtAgrApp.setPhone(CusIndivAllDto.getMobile());
                    iqpHighAmtAgrApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpHighAmtAgrApp.setEmail(CusIndivAllDto.getEmail());
                    iqpHighAmtAgrApp.setQq(CusIndivAllDto.getQq());
                    iqpHighAmtAgrApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpHighAmtAgrApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpHighAmtAgrApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpHighAmtAgrApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpHighAmtAgrApp.setFax(cusCorpDto.getFax());
                    iqpHighAmtAgrApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpHighAmtAgrApp.setQq(cusCorpDto.getQq());
                    iqpHighAmtAgrApp.setWechat(cusCorpDto.getWechatNo());
                    iqpHighAmtAgrApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }

            log.info("客户" + cusId + "新增个人额度申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpHighAmtAgrApp.setInputId(userInfo.getLoginCode());
                iqpHighAmtAgrApp.setInputBrId(userInfo.getOrg().getCode());
                iqpHighAmtAgrApp.setManagerId(userInfo.getLoginCode());
                iqpHighAmtAgrApp.setManagerBrId(userInfo.getOrg().getCode());
                iqpHighAmtAgrApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                iqpHighAmtAgrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                iqpHighAmtAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                iqpHighAmtAgrApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_01);
            }
            //生成流水号
            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            //生成合同编号
            HashMap param = new HashMap();
            //获取序列
            String dkSeq = this.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_01);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);
            iqpHighAmtAgrApp.setSerno(serno);
            iqpHighAmtAgrApp.setContNo(contNo);
            iqpHighAmtAgrApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpHighAmtAgrApp.setPkId(StringUtils.uuid(true));

            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpHighAmtAgrApp.getLmtAccNo())){
                Map map = new HashMap();
                map.put("accSubNo",iqpHighAmtAgrApp.getLmtAccNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",iqpHighAmtAgrApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpHighAmtAgrApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            int insertCount = iqpHighAmtAgrAppMapper.insertSelective(iqpHighAmtAgrApp);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno",serno);
            result.put("contNo",contNo);
            log.info("最高额授信协议申请"+serno+"-保存成功！");
            //根据选择的额度分项自动生成担保合同
            IqpHighAmtAgrApp iqpHighAmtAgrAppData = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
            if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpHighAmtAgrAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpHighAmtAgrAppData.getGuarMode())
                    && !CmisCommonConstants.GUAR_MODE_40.equals(iqpHighAmtAgrAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpHighAmtAgrAppData.getGuarMode())){
                Map map = new HashMap();
                map.put("accSubNo",iqpHighAmtAgrAppData.getLmtAccNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                String subSerno = lmtReplyAccSub.getSubSerno();
                GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                grtGuarContDto.setSerno(iqpHighAmtAgrAppData.getSerno());//业务流水号
                grtGuarContDto.setCusId(iqpHighAmtAgrAppData.getCusId());//借款人编号
                grtGuarContDto.setBizLine(iqpHighAmtAgrAppData.getBelgLine());//业务条线
                grtGuarContDto.setCusName(iqpHighAmtAgrAppData.getCusName());//借款人名称
                grtGuarContDto.setGuarWay(iqpHighAmtAgrAppData.getGuarMode());//担保方式
                grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);//是否授信项下
                grtGuarContDto.setGuarContType(iqpHighAmtAgrAppData.getContType());//担保合同类型
                grtGuarContDto.setGuarAmt(iqpHighAmtAgrAppData.getAgrAmt());//担保金额
                grtGuarContDto.setGuarTerm(iqpHighAmtAgrAppData.getAgrTerm());//担保期限
                grtGuarContDto.setGuarStartDate(iqpHighAmtAgrAppData.getStartDate());//担保起始日
                grtGuarContDto.setGuarEndDate(iqpHighAmtAgrAppData.getEndDate());//担保终止日
                grtGuarContDto.setReplyNo(iqpHighAmtAgrAppData.getReplyNo());//批复编号
                grtGuarContDto.setLmtAccNo(iqpHighAmtAgrAppData.getLmtAccNo());//授信额度编号
                grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                grtGuarContDto.setInputId(iqpHighAmtAgrAppData.getInputId());//登记人
                grtGuarContDto.setInputBrId(iqpHighAmtAgrAppData.getInputBrId());//登记机构
                grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记时间
                grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
            }
            // 生成用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpHighAmtAgrAppData.getReplyNo());
            queryMap.put("contNo",iqpHighAmtAgrAppData.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }
            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(cusBaseClientDto.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        }catch(YuspException e){
            log.error("最高额授信协议申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("保存最高额授信协议申请异常！",e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 最高额授信协议申请提交保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpHighAmtAgrAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "最高额授信协议申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpHighAmtAgrApp iqpHighAmtAgrApp = JSONObject.parseObject(JSON.toJSONString(params), IqpHighAmtAgrApp.class);
            if (iqpHighAmtAgrApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存最高额授信协议申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpHighAmtAgrApp.setUpdId(userInfo.getLoginCode());
                iqpHighAmtAgrApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpHighAmtAgrApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpHighAmtAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                iqpHighAmtAgrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            }

            log.info(logPrefix + "保存最高额授信协议申请数据");
            int updCount = iqpHighAmtAgrAppMapper.updateByPrimaryKeySelective(iqpHighAmtAgrApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存最高额授信协议申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpHighAmtAgrApp> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpHighAmtAgrAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpHighAmtAgrApp> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpHighAmtAgrAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 根据主键做逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map logicDelete(IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        iqpHighAmtAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);

        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String serno = iqpHighAmtAgrApp.getSerno();
            log.info(String.format("根据主键%s对最高额授信申请明细信息进行逻辑删除", serno));
            iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);

            log.info(String.format("根据主键%s对最高额授信申请明细信息进行逻辑删除,获取用户登录信息", serno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpHighAmtAgrApp.setUpdId(userInfo.getLoginCode());
                iqpHighAmtAgrApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpHighAmtAgrApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对最高额授信申请信息进行逻辑删除,信息进行逻辑删除", serno));

            if(CmisCommonConstants.WF_STATUS_000.equals(iqpHighAmtAgrApp.getApproveStatus())){
                //删除关联的担保合同及担保合同与押品关系表
                bizCommonService.logicDeleteGrtGuarCont(iqpHighAmtAgrApp.getSerno());

                iqpHighAmtAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                iqpHighAmtAgrAppMapper.updateByPrimaryKeySelective(iqpHighAmtAgrApp);

            }else if(CmisCommonConstants.WF_STATUS_992.equals(iqpHighAmtAgrApp.getApproveStatus())) {
                iqpHighAmtAgrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                iqpHighAmtAgrAppMapper.updateByPrimaryKeySelective(iqpHighAmtAgrApp);
                ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(serno);
                //打回改为自行退出后，恢复占额
                if (iqpHighAmtAgrApp.getLmtAccNo() != null && !"".equals(iqpHighAmtAgrApp.getLmtAccNo())) {
                    String guarMode = iqpHighAmtAgrApp.getGuarMode();
                    //恢复敞口
                    BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                    //恢复总额
                    BigDecimal recoverAmtCny = BigDecimal.ZERO;
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                            || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                        recoverSpacAmtCny = BigDecimal.ZERO;
                        recoverAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                    } else {
                        recoverSpacAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                        recoverAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                    }
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpHighAmtAgrApp.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(iqpHighAmtAgrApp.getContNo());//合同编号
                    cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                    cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                    cmisLmt0012ReqDto.setInputId(iqpHighAmtAgrApp.getInputId());
                    cmisLmt0012ReqDto.setInputBrId(iqpHighAmtAgrApp.getInputBrId());
                    cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    String code = resultDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.error("业务申请恢复额度异常！");
                        throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除对最高额授信申请信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateStatus
     * @方法描述: 根据主键做修改状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateStatus(IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        iqpHighAmtAgrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
        return iqpHighAmtAgrAppMapper.updateByPrimaryKey(iqpHighAmtAgrApp);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public IqpHighAmtAgrApp queryIqpHighAmtAgrAppDataBySerno(String serno) {
        return iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
    }


    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpHighAmtAgrApp> selectForLmtAccNo(String lmtAccNo){
        return iqpHighAmtAgrAppMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: riskItem0030
     * @方法描述: 对公授信在途校验
     * @参数与返回说明:
     * @算法描述: 当合同申请对应的客户存在在途的授信申报/授信变更/预授信细化/授信批复
     * 变更/授信额度调剂时进行风险拦截
     * @创建人: cainingbo
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0030(String serno) {
        log.info("对公授信在途校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        //查询最高额授信协议申请
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
        if(iqpHighAmtAgrApp != null){
            riskResultDto =this.checkRiskResultDto(iqpHighAmtAgrApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //普通贷款合同申请
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serno);
        if(iqpLoanApp != null){
            riskResultDto =this.checkRiskResultDto(iqpLoanApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //开证合同
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);
        if(iqpTfLocApp != null){
            riskResultDto =this.checkRiskResultDto(iqpTfLocApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //银承合同
        IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByIqpSerno(serno);
        if(iqpAccpApp != null){
            riskResultDto =this.checkRiskResultDto(iqpAccpApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //保函合同申请
        IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
        if(iqpCvrgApp != null){
            riskResultDto =this.checkRiskResultDto(iqpCvrgApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //贴现协议申请
        IqpDiscApp iqpDiscApp = iqpDiscAppService.selectBySerno(serno);
        if(iqpDiscApp != null){
            riskResultDto =this.checkRiskResultDto(iqpDiscApp.getCusId());
            if(riskResultDto != null && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        //委托贷款
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(serno);
        if(iqpEntrustLoanApp != null){
            riskResultDto =this.checkRiskResultDto(iqpEntrustLoanApp.getCusId());
            if(null != riskResultDto && CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        }
        log.info("对公授信在途校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
    /**
     * @方法名称: riskItem0030
     * @方法描述: 对公授信在途校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: cainingbo
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    private RiskResultDto checkRiskResultDto(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        QueryModel queryModel = new QueryModel();
        String approveStatus ="";
        String lmtType ="";
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("oprType", CommonConstance.OPR_TYPE_ADD);
        //单一客户授信申报校验
        List<LmtApp> lmtAppList =lmtAppService.selectAll(queryModel);
        if(lmtAppList != null && lmtAppList.size() != 0){
            for(LmtApp lmtApps : lmtAppList){
                approveStatus = lmtApps.getApproveStatus();
                lmtType = lmtApps.getLmtType();
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtApps.getIsGrp())){
                    LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtApps.getSerno());
                    if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())){
                        if( CmisBizConstants.APPLY_STATE_APP.equals(approveStatus)
                                || CmisBizConstants.APPLY_STATE_CALL_BACK.equals(approveStatus)
                                || CmisBizConstants.APPLY_STATE_TODO.equals(approveStatus)){
                            if("01".equals(lmtType) || "03".equals(lmtType) || "04".equals(lmtType)  || "05".equals(lmtType) || "06".equals(lmtType)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03001);
                                return riskResultDto;
                            }
                            if("02".equals(lmtType)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03002);
                                return riskResultDto;
                            }
                            if("07".equals(lmtType)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03003);
                                return riskResultDto;
                            }
                            if("08".equals(lmtType)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03005);
                                return riskResultDto;
                            }
                        }
                    }
                }else{
                    if( CmisBizConstants.APPLY_STATE_APP.equals(approveStatus)
                            || CmisBizConstants.APPLY_STATE_CALL_BACK.equals(approveStatus)
                            || CmisBizConstants.APPLY_STATE_TODO.equals(approveStatus)){
                        if("01".equals(lmtType) || "03".equals(lmtType) || "04".equals(lmtType)  || "05".equals(lmtType) || "06".equals(lmtType)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03001);
                            return riskResultDto;
                        }
                        if("02".equals(lmtType)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03002);
                            return riskResultDto;
                        }
                        if("07".equals(lmtType)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03003);
                            return riskResultDto;
                        }
                        if("08".equals(lmtType)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03005);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        //单一客户授信批复变更
        List<LmtReplyChg> lmtList =lmtReplyChgService.selectAll(queryModel);
        if(CollectionUtils.nonEmpty(lmtList)) {
            for(LmtReplyChg lmtReplyChg : lmtList) {
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtReplyChg.getIsGrp())){
                    LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtReplyChg.getSerno());
                    if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())){
                        if( CmisBizConstants.APPLY_STATE_APP.equals(lmtReplyChg.getApproveStatus())
                                || CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtReplyChg.getApproveStatus())
                                || CmisBizConstants.APPLY_STATE_TODO.equals(lmtReplyChg.getApproveStatus())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03004);
                            return riskResultDto;
                        }
                    }
                }else{
                    if( CmisBizConstants.APPLY_STATE_APP.equals(lmtReplyChg.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtReplyChg.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_TODO.equals(lmtReplyChg.getApproveStatus())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03004);
                        return riskResultDto;
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return  riskResultDto;
    }

    /**
     * @方法名称: riskItem0028
     * @方法描述: 借款人和主担保人正式客户校验
     * @参数与返回说明:
     * @算法描述:借款人和主担保人（非追加）必须是正式客户，否则拦截
     * @创建人: cainingbo
     * @创建时间: 2021-06-24 10:47:44
     * @修改记录: 修改时间:2021年7月21日17:15:59    修改人员：hubp    修改原因 :
     */
    public Object riskItem0028(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("借款人和主担保人正式客户校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_018);
            return riskResultDto;
        }
        // 获取客户模块中的客户基本信息
        ResultDto<CusBaseDto>  resultDto= cmisCusClientService.cusBaseInfo(cusId);
        if(Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
            return riskResultDto;
        }
        CusBaseDto cusBaseDtoDo = resultDto.getData();
        String cusRankCls = cusBaseDtoDo.getCusRankCls();
        // 借款人必须是正式客户
        if(!Objects.equals("01",cusRankCls)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02801);
            return riskResultDto;
        }

        //通过流水号查询担保合同和业务关系表 查询担保合同编号
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectByLmtSerno(serno);
        if(CollectionUtils.nonEmpty(grtGuarBizRstRelList)) {
            for(GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                String guarContNo = grtGuarBizRstRel.getGuarContNo();
                if(StringUtils.isEmpty(guarContNo)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02802);
                    return riskResultDto;
                }
                log.info("借款人和主担保人正式客户校验执行中*******************担保合同编号：【{}】",grtGuarBizRstRel.getGuarContNo());
                GrtGuarCont grtGuarCont =  grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                log.info("借款人和主担保人正式客户校验执行中*******************担保合同信息：【{}】",grtGuarCont);
                if(Objects.isNull(grtGuarCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0014);
                    return riskResultDto;
                }
                // 非追加担保
                if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(grtGuarCont.getIsSuperaddGuar())) {
                    queryModel.addCondition("guarContNo",grtGuarCont.getGuarContNo());
                    List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
                    //数据库查询结果返回值可能存在null，此处将null移除后继续判断
                    guarGuaranteeList.removeAll(Collections.singleton(null));
                    if(CollectionUtils.nonEmpty(guarGuaranteeList)) {
                        for(GuarGuarantee guarGuarantee : guarGuaranteeList) {
                            // 获取客户模块中的客户基本信息
                            resultDto= cmisCusClientService.cusBaseInfo(guarGuarantee.getCusId());
                            if(Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                                return riskResultDto;
                            }
                            cusBaseDtoDo = resultDto.getData();
                            cusRankCls = cusBaseDtoDo.getCusRankCls();
                            // 主担保人（非追加）必须是正式客户
                            if(!Objects.equals("01",cusRankCls)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02803);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
        }
        log.info("借款人和主担保人正式客户校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0029
     * @方法描述: 额度状态及是否足额校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0029(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("额度状态及是否足额校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        Map map = new HashMap();
        Map<String,String > loanMap = new HashMap();
        loanMap.put("serno",serno);
        if("YX001".equals(bizType)){//最高额授信协议申请
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
            if(iqpHighAmtAgrApp == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险
            String guarMode = iqpHighAmtAgrApp.getGuarMode();
            String isLowRisk = "";
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                    CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
            }else{
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
            }
            map.put("contNo",iqpHighAmtAgrApp.getContNo());//合同号--交易业务编号
            map.put("cusId",iqpHighAmtAgrApp.getCusId());//客户编号
            map.put("cusName",iqpHighAmtAgrApp.getCusName());//客户名称
            map.put("contType",iqpHighAmtAgrApp.getContType());//交易类型
            map.put("prdId","");//产品编号
            map.put("prdTypeProp","");//产品类型属性
            map.put("prdName","");//产品名称
            map.put("lmtAccNo",iqpHighAmtAgrApp.getLmtAccNo());//额度分项编号
            map.put("contStartDate",iqpHighAmtAgrApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpHighAmtAgrApp.getEndDate());//合同到期日
            map.put("isRenew",iqpHighAmtAgrApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpHighAmtAgrApp.getOrigiContNo());//原合同编号
            map.put("isSeaJnt","0");//是否无缝衔接
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",BigDecimal.ZERO);//保证金金额
            map.put("bailPerc",BigDecimal.ZERO);//保证金比例
            map.put("drftType","");//票据种类
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("managerBrId",iqpHighAmtAgrApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = iqpHighAmtAgrApp.getAgrContHighAvlAmt();//最高可用信金额
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX002".equals(bizType) || "YX004".equals(bizType)
                || "YX003".equals(bizType) ||"XW003".equals(bizType)
                || "LS001".equals(bizType) || "YX004".equals(bizType)
                ||"YX004".equals(bizType)){ //普通贷款合同/福费廷合同申请/贸易融资合同申请/小微/零售)
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serno);
            if(iqpLoanApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险,福费廷默认是低风险
            String guarMode = iqpLoanApp.getGuarWay();
            //分项额度编号
            String lmtAccNo = "";
            String isLowRisk = "";
            if(CmisCommonConstants.STD_BUSI_TYPE_05.equals(iqpLoanApp.getBizType())){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                if(iqpLoanApp.getLmtAccNo()==null || "".equals(iqpLoanApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpLoanApp.getLmtAccNo();
                }
            }else{
                if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                        CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                    isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                    if(iqpLoanApp.getLmtAccNo()==null||"".equals(iqpLoanApp.getLmtAccNo())){
                        lmtAccNo = "";
                    }else {
                        lmtAccNo = iqpLoanApp.getLmtAccNo();
                    }
                }else{
                    isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
                    lmtAccNo = iqpLoanApp.getLmtAccNo();
                }
            }
            //是否无缝衔接
            String isSeajnt ="";
            if(iqpLoanApp.getIsSeajnt()!=null && !"".equals(iqpLoanApp.getIsSeajnt())){
                isSeajnt = iqpLoanApp.getIsSeajnt();
            }else{
                if(CmisCommonConstants.STD_LOAN_MODAL_3.equals(iqpLoanApp.getLoanModal())||CmisCommonConstants.STD_LOAN_MODAL_6.equals(iqpLoanApp.getLoanModal())){
                    isSeajnt = "1";
                }else{
                    isSeajnt = "0";
                }
            }
            String isOutstndTrdLmtAmt = iqpLoanApp.getIsOutstndTrdLmtAmt();//是否占用第三方额度
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isOutstndTrdLmtAmt)){
                map.put("tdpAgrNo",iqpLoanApp.getTdpAgrNo());//第三方额度编号
                map.put("tdpPrdTypeProp","");//第三方授信品种类型属性
            }
            map.put("contNo",iqpLoanApp.getContNo());//合同编号--交易业务编号
            map.put("cusId",iqpLoanApp.getCusId());//客户编号
            map.put("cusName",iqpLoanApp.getCusName());//客户名称
            map.put("contType",iqpLoanApp.getContType());//交易类型
            map.put("prdId",iqpLoanApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpLoanApp.getPrdName());//产品名称
            map.put("lmtAccNo",lmtAccNo);//额度分项编号
            map.put("contStartDate",iqpLoanApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpLoanApp.getEndDate());//合同到期日
            map.put("isRenew",iqpLoanApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpLoanApp.getOrigiContNo());//原合同编号
            map.put("isSeaJnt",isSeajnt);//是否无缝衔接
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("isOutstndTrdLmtAmt",isOutstndTrdLmtAmt);//是否占用第三方
            map.put("bailAmt",BigDecimal.ZERO);//保证金金额
            map.put("bailPerc",BigDecimal.ZERO);//保证金比例
            map.put("drftType","");//票据种类
            map.put("managerBrId",iqpLoanApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = BigDecimal.ZERO;
            if("YX002".equals(bizType) || "YX004".equals(bizType)
                    || "YX003".equals(bizType)) {
                appAmt = iqpLoanApp.getCvtCnyAmt();
            } else {
                //小微业务需确认是否会涉及外币
                appAmt = iqpLoanApp.getContHighAvlAmt();
            }
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX006".equals(bizType)){//银承合同
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByIqpSerno(serno);
            if(iqpAccpApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险
            String guarMode = iqpAccpApp.getGuarMode();
            //分项额度编号
            String lmtAccNo = "";
            String isLowRisk = "";
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                    CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                if(iqpAccpApp.getLmtAccNo()==null||"".equals(iqpAccpApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpAccpApp.getLmtAccNo();
                }
            }else{
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
                lmtAccNo = iqpAccpApp.getLmtAccNo();
            }
            map.put("contNo",iqpAccpApp.getContNo());//合同编号--交易业务编号
            map.put("cusId",iqpAccpApp.getCusId());//客户编号
            map.put("cusName",iqpAccpApp.getCusName());//客户名称
            map.put("contType",iqpAccpApp.getContType());//交易类型
            map.put("prdId",iqpAccpApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpAccpApp.getPrdName());//产品名称
            map.put("lmtAccNo",lmtAccNo);//额度分项编号
            map.put("contStartDate",iqpAccpApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpAccpApp.getEndDate());//合同到期日
            map.put("isRenew",iqpAccpApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpAccpApp.getOrigiContNo());//原合同编号
            map.put("isSeaJnt","0");//是否无缝衔接
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",iqpAccpApp.getBailAmt());//保证金金额
            map.put("bailPerc",iqpAccpApp.getBailPerc());//保证金比例
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("drftType","");//票据种类
            map.put("managerBrId",iqpAccpApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = iqpAccpApp.getContHighAvlAmt();//本合同项下最高可用信金额
            riskResultDto = this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX005".equals(bizType)){//开证合同
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);
            if(iqpTfLocApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险
            String guarMode = iqpTfLocApp.getGuarMode();

            // 折算金额和保证金比例
            BigDecimal contHighAmt = iqpTfLocApp.getContHighAvlAmt();
            BigDecimal bailPerc = iqpTfLocApp.getBailPerc();
            // 保证金金额
            BigDecimal bailAmt = BigDecimal.ZERO;
            //分项额度编号
            String lmtAccNo = "";
            String isLowRisk = "";
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                    CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                bailAmt = contHighAmt.multiply(bailPerc);
                if(iqpTfLocApp.getLmtAccNo()==null || "".equals(iqpTfLocApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpTfLocApp.getLmtAccNo();
                }
            }else{
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
                bailAmt = bailPerc.multiply(contHighAmt.divide(BigDecimal.ONE.subtract(bailPerc),2,BigDecimal.ROUND_HALF_UP));
                lmtAccNo = iqpTfLocApp.getLmtAccNo();
            }
            map.put("contNo",iqpTfLocApp.getContNo());//合同编号--交易业务编号
            map.put("cusId",iqpTfLocApp.getCusId());//客户编号
            map.put("cusName",iqpTfLocApp.getCusName());//客户名称
            map.put("contType",iqpTfLocApp.getContType());//交易类型
            map.put("prdId",iqpTfLocApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpTfLocApp.getPrdName());//产品名称
            map.put("lmtAccNo",lmtAccNo);//额度分项编号
            map.put("contStartDate",iqpTfLocApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpTfLocApp.getEndDate());//合同到期日
            map.put("isRenew",iqpTfLocApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpTfLocApp.getOrigiContNo());//原合同编号
            map.put("isSeaJnt","0");//是否无缝衔接
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",bailAmt);//保证金金额
            map.put("bailPerc",iqpTfLocApp.getBailPerc());//保证金比例
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("drftType","");//票据种类
            map.put("managerBrId",iqpTfLocApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = iqpTfLocApp.getCvtCnyAmt();
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX007".equals(bizType)){ //保函合同申请
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if(iqpCvrgApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险
            String guarMode = iqpCvrgApp.getGuarMode();
            //分项额度编号
            String lmtAccNo = "";
            String isLowRisk = "";
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                    CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                if(iqpCvrgApp.getLmtAccNo()==null || "".equals(iqpCvrgApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpCvrgApp.getLmtAccNo();
                }
            }else{
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
                lmtAccNo = iqpCvrgApp.getLmtAccNo();
            }
            map.put("contNo",iqpCvrgApp.getContNo());//合同编号--交易业务编号
            map.put("cusId",iqpCvrgApp.getCusId());//客户编号
            map.put("cusName",iqpCvrgApp.getCusName());//客户名称
            map.put("contType",iqpCvrgApp.getContType());//交易类型
            map.put("prdId",iqpCvrgApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpCvrgApp.getPrdName());//产品名称
            map.put("lmtAccNo",lmtAccNo);//额度分项编号
            map.put("contStartDate",iqpCvrgApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpCvrgApp.getEndDate());//合同到期日
            map.put("isRenew",iqpCvrgApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpCvrgApp.getOrigiContNo());//原合同编号
            map.put("isSeaJnt","0");//是否无缝衔接
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",iqpCvrgApp.getBailCvtCnyAmt());//保证金金额
            map.put("bailPerc",iqpCvrgApp.getBailPerc());//保证金比例
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("drftType","");//票据种类
            map.put("managerBrId",iqpCvrgApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = iqpCvrgApp.getCvtCnyAmt();
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX009".equals(bizType)){//贴现协议申请
            IqpDiscApp iqpDiscApp = iqpDiscAppService.selectBySerno(serno);
            if(iqpDiscApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            String isLowRisk = "";
            map.put("contNo",iqpDiscApp.getContNo());//合同编号--交易业务编号
            map.put("cusId",iqpDiscApp.getCusId());//客户编号
            map.put("cusName",iqpDiscApp.getCusName());//客户名称
            String contType = iqpDiscApp.getDiscContType();//交易类型
            if("01".equals(contType)){//一般贴现协议
                String openday = stringRedisTemplate.opsForValue().get("openDay");//营业日期
                map.put("contType","1");
                map.put("contStartDate",openday);//营业日期
                map.put("contEndDate","");//合同到期日
            }else if("02".equals(contType)){//贴现额度协议
                map.put("contType","2");
                map.put("contStartDate",iqpDiscApp.getStartDate());//合同起始日
                map.put("contEndDate",iqpDiscApp.getEndDate());//合同到期日
            }
            String drftType = iqpDiscApp.getDrftType();//票据种类
            String lmtAccNo = "";
            if("2".equals(drftType)){
                lmtAccNo = iqpDiscApp.getLmtAccNo();
                map.put("lmtAccNo",lmtAccNo);//申请人额度分项编号
                map.put("acptCrpLmtNo",iqpDiscApp.getAcptCrpLmtNo());//承兑企业授信额度编号
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
            }else{
                if(iqpDiscApp.getLmtAccNo()==null || "".equals(iqpDiscApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpDiscApp.getLmtAccNo();
                }
                map.put("lmtAccNo",lmtAccNo);//额度分项编号
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            //实际占额金额
            BigDecimal actualAmt = BigDecimal.ZERO;
            /*
             * 1.一般贴现协议 实际占额金额为票面总金额
             * 2.贴现额度协议 实际占额金额为合同金额
             */
            if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(iqpDiscApp.getDiscContType())){
                //一般贴现协议
                actualAmt = iqpDiscApp.getDrftTotalAmt();
            }else{
                //贴现额度协议
                actualAmt = iqpDiscApp.getContAmt();
            }
            map.put("drftType",iqpDiscApp.getDrftType());//票据种类
            map.put("prdId",iqpDiscApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpDiscApp.getPrdName());//产品名称
            map.put("isSeaJnt","0");
            map.put("isRenew",iqpDiscApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpDiscApp.getOrigiContNo());//原合同编号
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",BigDecimal.ZERO);//保证金金额
            map.put("bailPerc",BigDecimal.ZERO);//保证金比例
            map.put("managerBrId",iqpDiscApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = actualAmt;
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if("YX008".equals(bizType)){//委托贷款申请
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(serno);
            if(iqpEntrustLoanApp==null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }
            //根据担保方式判断是否低风险
            String guarMode = iqpEntrustLoanApp.getGuarMode();
            //分项额度编号
            String lmtAccNo = "";
            String isLowRisk = "";
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)||CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||
                    CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_1;
                if(iqpEntrustLoanApp.getLmtAccNo()==null || "".equals(iqpEntrustLoanApp.getLmtAccNo())){
                    lmtAccNo = "";
                }else {
                    lmtAccNo = iqpEntrustLoanApp.getLmtAccNo();
                }
            }else{
                isLowRisk = CmisCommonConstants.STD_ZB_YES_NO_0;
                lmtAccNo = iqpEntrustLoanApp.getLmtAccNo();
            }
            map.put("contNo",iqpEntrustLoanApp.getContNo());//合同编号
            map.put("cusId",iqpEntrustLoanApp.getCusId());//客户编号
            map.put("cusName",iqpEntrustLoanApp.getCusName());//客户名称
            map.put("contType",iqpEntrustLoanApp.getContType());//交易类型
            map.put("prdId",iqpEntrustLoanApp.getPrdId());//产品编号
            map.put("prdTypeProp",this.getPrdTypeProp(lmtAccNo));//产品类型属性
            map.put("prdName",iqpEntrustLoanApp.getPrdName());//产品名称
            map.put("lmtAccNo",lmtAccNo);//额度分项编号
            map.put("contStartDate",iqpEntrustLoanApp.getStartDate());//合同起始日
            map.put("contEndDate",iqpEntrustLoanApp.getEndDate());//合同到期日
            map.put("isSeaJnt","0");
            map.put("isRenew",iqpEntrustLoanApp.getIsRenew());//是否续签
            map.put("origiContNo",iqpEntrustLoanApp.getOrigiContNo());//原合同编号
            map.put("isOutstndTrdLmtAmt","");//是否占用第三方
            map.put("drftType","");//票据种类
            map.put("isLowRisk",isLowRisk);//是否低风险
            map.put("bailAmt",BigDecimal.ZERO);//保证金金额
            map.put("bailPerc",BigDecimal.ZERO);//保证金比例
            map.put("managerBrId",iqpEntrustLoanApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = iqpEntrustLoanApp.getContHighAvlAmt();
            riskResultDto =this.signCtrLoanContRetail(map,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) || CmisFlowConstants.FLOW_TYPE_DHD02.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)){ //对公贷款出账流程,对公贷款出账申请（东海）,对公贷款出账申请（寿光）
            log.info("进入贷款出账申请【{}】额度足额交验",serno);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                // 是否无缝衔接
                loanMap.put("isSeajnt","1");
//                QueryModel model = new QueryModel();
//                model.addCondition("serno",pvpLoanApp.getPvpSerno());
//                model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
//                List<RepayBillRellDto> billRellList = pvpLoanAppRepayBillRellService.queryAllRepayBillRell(model);
//                BigDecimal billTotal = BigDecimal.ZERO;
//                // 偿还借据是否为空
//                if(CollectionUtils.isEmpty(billRellList)) {
//                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02902);
//                    return riskResultDto;
//                }
//                for(RepayBillRellDto repayBillRellDto : billRellList) {
//                    billTotal = billTotal.add(Objects.nonNull(repayBillRellDto.getLoanAmt()) ? repayBillRellDto.getLoanAmt() : BigDecimal.ZERO);
//                }
//                if((Objects.nonNull(pvpLoanApp.getPvpAmt()) ? pvpLoanApp.getPvpAmt() : BigDecimal.ZERO).compareTo(billTotal) != 0) {
//                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02903);
//                    return riskResultDto;
//                }
            }

            loanMap.put("contNo",pvpLoanApp.getContNo());// 合同编号
            loanMap.put("billNo",pvpLoanApp.getBillNo());// 借据编号
            loanMap.put("cusId",pvpLoanApp.getCusId());//客户编号
            loanMap.put("cusName",pvpLoanApp.getCusName());//客户名称
            loanMap.put("prdId",pvpLoanApp.getPrdId());//产品编号
            loanMap.put("prdTypeProp",pvpLoanApp.getPrdTypeProp());//产品类型属性
            loanMap.put("prdName",pvpLoanApp.getPrdName());//产品名称
            loanMap.put("loanEndDate",pvpLoanApp.getLoanEndDate());// 合同到期日
            loanMap.put("managerBrId",pvpLoanApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = pvpLoanApp.getCvtCnyAmt();
            riskResultDto =this.riskItem002902Check(loanMap,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)) { //委托贷款出账申请
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            if(Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            loanMap.put("pvpSerno",pvpEntrustLoanApp.getPvpSerno());// 放款流水号
            loanMap.put("contNo",pvpEntrustLoanApp.getContNo());// 合同编号
            loanMap.put("billNo",pvpEntrustLoanApp.getBillNo());// 借据编号
            loanMap.put("cusId",pvpEntrustLoanApp.getCusId());//客户编号
            loanMap.put("cusName",pvpEntrustLoanApp.getCusName());//客户名称
            loanMap.put("prdId",pvpEntrustLoanApp.getPrdId());//产品编号
            loanMap.put("prdTypeProp",pvpEntrustLoanApp.getPrdTypeProp());//产品类型属性
            loanMap.put("prdName",pvpEntrustLoanApp.getPrdName());//产品名称
            loanMap.put("loanEndDate",pvpEntrustLoanApp.getLoanEndDate());// 合同到期日
            loanMap.put("managerBrId",pvpEntrustLoanApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = pvpEntrustLoanApp.getPvpAmt();
            riskResultDto =this.riskItem002902Check(loanMap,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX012.equals(bizType)) { //银承出账申请
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            if(Objects.isNull(pvpAccpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 保证金额判定
            List<BailAccInfo> bailAccInfoList = bailAccInfoService.selectBySerno(serno);
            if(CollectionUtils.nonEmpty(bailAccInfoList)) {
                for(BailAccInfo bailAccInfo : bailAccInfoList) {
                    if((Objects.nonNull(pvpAccpApp.getAppAmt()) ? pvpAccpApp.getAppAmt():BigDecimal.ZERO).compareTo(
                            Objects.nonNull(bailAccInfo.getBailAmt()) ? bailAccInfo.getBailAmt():BigDecimal.ZERO) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02904);
                        return riskResultDto;
                    }
                }
            }
            riskResultDto =this.checkPvpAccpApp(pvpAccpApp);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType)) { //小微放款、零售放款申请
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }

            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                // 是否无缝衔接
                loanMap.put("isSeajnt","1");
            }

            loanMap.put("contNo",pvpLoanApp.getContNo());// 合同编号
            loanMap.put("billNo",pvpLoanApp.getBillNo());// 借据编号
            loanMap.put("cusId",pvpLoanApp.getCusId());//客户编号
            loanMap.put("cusName",pvpLoanApp.getCusName());//客户名称
            loanMap.put("prdId",pvpLoanApp.getPrdId());//产品编号
            loanMap.put("prdTypeProp",pvpLoanApp.getPrdTypeProp());//产品类型属性
            loanMap.put("prdName",pvpLoanApp.getPrdName());//产品名称
            loanMap.put("loanEndDate",pvpLoanApp.getLoanEndDate());// 合同到期日
            loanMap.put("managerBrId",pvpLoanApp.getManagerBrId());//客户管理机构
            BigDecimal appAmt = pvpLoanApp.getPvpAmt();
            riskResultDto =this.riskItem002902Check(loanMap,appAmt);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return  riskResultDto;
            }
        }
        log.info("额度状态及是否足额校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     *
     * @param lmtAccNo
     * @return
     */
    public String getPrdTypeProp(String lmtAccNo) {
        String prdTypeProp = "";
        if(lmtAccNo == ""){
            return prdTypeProp;
        }
        LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
        try {
            lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if(Objects.nonNull(lmtReplyAccSubPrd)) {
                prdTypeProp = lmtReplyAccSubPrd.getLmtBizTypeProp();
                if(prdTypeProp==null||"".equals(prdTypeProp)){
                    prdTypeProp = "";
                    return prdTypeProp;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prdTypeProp;
    }
    /**
     * @方法名称: riskItem0029
     * @方法描述: 调用额度接口校验额度
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto signCtrLoanContRetail(Map map,BigDecimal appAmt) {
        Map<String, Object> returnMap = new HashMap<>();
        RiskResultDto riskResultDto = new RiskResultDto();
        if(map.get("lmtAccNo")!=null && !"".equals(map.get("lmtAccNo"))
                && map.get("managerBrId")!=null && !"".equals(map.get("managerBrId"))){
            CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
            cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde((String) map.get("managerBrId")));//金融机构代码
            cmisLmt0026ReqDto.setSubSerno((String) map.get("lmtAccNo"));//分项编号
            cmisLmt0026ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//分项类型
            log.info("额度状态校验【{}】，前往额度系统进行额度状态校验开始,请求报文为:【{}】", map.get("lmtAccNo"), cmisLmt0026ReqDto.toString());
            ResultDto<CmisLmt0026RespDto> cmisLmt0026RespDtoResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
            log.info("额度状态校验【{}】，前往额度系统进行额度状态校验结束,响应报文为:【{}】", map.get("lmtAccNo"), cmisLmt0026RespDtoResultDto.toString());
            if(!"0".equals(cmisLmt0026RespDtoResultDto.getCode())){
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(cmisLmt0026RespDtoResultDto.getData().getErrorCode())){
                throw BizException.error(null,cmisLmt0026RespDtoResultDto.getData().getErrorCode(),cmisLmt0026RespDtoResultDto.getData().getErrorMsg());
            }
            String status = cmisLmt0026RespDtoResultDto.getData().getStatus();
            if(!CmisLmtConstants.STD_ZB_LMT_STATE_01.equals(status)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11203);
                return riskResultDto;
            }
        }

        //占用总额
        BigDecimal bizTotalAmt = BigDecimal.ZERO;
        //占用敞口
        BigDecimal bizSpacAmt = BigDecimal.ZERO;

        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(map.get("isRenew"))){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = (String) map.get("origiContNo");
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(map.get("isLowRisk"))){
            bizTotalAmt = appAmt;
            bizSpacAmt = BigDecimal.ZERO;
        }else{
            bizTotalAmt = appAmt.add((BigDecimal) map.get("bailAmt"));
            bizSpacAmt = appAmt;
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        //1. 向额度系统发送接口：额度校验
        CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
        cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0009ReqDto.setInstuCde(CmisCommonUtils.getInstucde((String) map.get("managerBrId")));//金融机构代码
        cmisLmt0009ReqDto.setDealBizNo((String) map.get("contNo"));//交易业务编号
        cmisLmt0009ReqDto.setCusId((String) map.get("cusId"));//客户编号
        cmisLmt0009ReqDto.setCusName((String) map.get("cusName"));//客户名称
        cmisLmt0009ReqDto.setDealBizType((String) map.get("contType"));//交易业务类型
        cmisLmt0009ReqDto.setPrdId((String) map.get("prdId"));//产品编号
        cmisLmt0009ReqDto.setPrdName((String) map.get("prdName"));//产品名称
        cmisLmt0009ReqDto.setIsLriskBiz((String) map.get("isLowRisk"));//是否低风险
        cmisLmt0009ReqDto.setIsFollowBiz((String) map.get("isSeaJnt"));//是否无缝衔接
        cmisLmt0009ReqDto.setIsBizRev(isBizRev);//是否合同重签
        cmisLmt0009ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
        cmisLmt0009ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
        cmisLmt0009ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
        cmisLmt0009ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
        cmisLmt0009ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性
        cmisLmt0009ReqDto.setDealBizAmt(appAmt);//交易业务金额
        cmisLmt0009ReqDto.setDealBizBailPreRate((BigDecimal) map.get("bailPerc"));//保证金比例
        cmisLmt0009ReqDto.setDealBizBailPreAmt((BigDecimal) map.get("bailAmt"));//保证金
        cmisLmt0009ReqDto.setStartDate((String) map.get("contStartDate"));//合同起始日
        cmisLmt0009ReqDto.setEndDate((String) map.get("contEndDate"));//合同到期日
        cmisLmt0009ReqDto.setInputBrId((String) map.get("managerBrId"));//客户管理机构

        List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
        //第三方
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(map.get("isOutstndTrdLmtAmt"))){
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDtoData = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDtoData.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_03);//额度类型
            cmisLmt0009OccRelListReqDtoData.setLmtSubNo((String) map.get("tdpAgrNo"));//额度分项编号
            cmisLmt0009OccRelListReqDtoData.setPrdTypeProp((String) map.get("tdpPrdTypeProp"));//授信品种类型属性
            cmisLmt0009OccRelListReqDtoData.setBizTotalAmt(bizTotalAmt);//占用总额(折人民币)
            cmisLmt0009OccRelListReqDtoData.setBizSpacAmt(bizSpacAmt);//占用敞口(折人民币)
            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDtoData);
        }

        //商票双占
        if(CmisCommonConstants.STD_DRFT_TYPE_2.equals(map.get("drftType"))){
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDtoData = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDtoData.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0009OccRelListReqDtoData.setLmtSubNo((String) map.get("acptCrpLmtNo"));//额度分项编号
            cmisLmt0009OccRelListReqDtoData.setPrdTypeProp(getPrdTypeProp((String) map.get("acptCrpLmtNo")));//授信品种类型属性
            cmisLmt0009OccRelListReqDtoData.setBizTotalAmt(bizTotalAmt);//占用总额(折人民币)
            cmisLmt0009OccRelListReqDtoData.setBizSpacAmt(bizSpacAmt);//占用敞口(折人民币)
            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDtoData);
        }

        CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
        cmisLmt0009OccRelListReqDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
        cmisLmt0009OccRelListReqDto.setLmtSubNo((String) map.get("lmtAccNo"));//额度分项编号
        cmisLmt0009OccRelListReqDto.setPrdTypeProp((String) map.get("prdTypeProp"));//授信品种类型属性
        cmisLmt0009OccRelListReqDto.setBizTotalAmt(bizTotalAmt);//占用总额(折人民币)
        cmisLmt0009OccRelListReqDto.setBizSpacAmt(bizSpacAmt);//占用敞口(折人民币)
        cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
        cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
        log.info("根据合同编号【{}】，前往额度系统-额度校验开始，请求报文:【{}】", map.get("contNo"), JSON.toJSONString(cmisLmt0009ReqDto));
        ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
        log.info("根据合同编号【{}】，前往额度系统-额度校验结束，请求报文:【{}】", map.get("contNo"), JSON.toJSONString(cmisLmt0009RespDto));
        if(!"0".equals(cmisLmt0009RespDto.getCode())){
            log.info("调用额度09接口失败！");
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(EcbEnum.ECB019999.value);
            return riskResultDto;
        }
        String code = cmisLmt0009RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.info("根据合同编号【{}】,前往额度系统-额度校验失败！",map.get("contNo"));
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(cmisLmt0009RespDto.getData().getErrorMsg());
            return riskResultDto;
        }
        // 响应内容为空
        if(Objects.isNull(cmisLmt0009RespDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02901);
            return riskResultDto;
        }
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem002902Check
     * @方法描述: 调用额度接口台账校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem002902Check(Map<String,String> map,BigDecimal appAmt) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //1. 向额度系统发送接口：校验额度
        String serno = map.get("serno");
        CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
        cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0010ReqDto.setInstuCde(CmisCommonUtils.getInstucde((String) map.get("managerBrId")));//金融机构代码
        cmisLmt0010ReqDto.setBizNo(map.get("contNo"));//合同编号
        CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
        cmisLmt0010ReqDealBizListDto.setDealBizNo(map.get("billNo"));
        cmisLmt0010ReqDealBizListDto.setIsFollowBiz(Objects.isNull(map.get("isSeajnt")) ? "0" : map.get("isSeajnt"));
        // 无缝衔接时，录入以下的值
        //原交易业务编号
        //原交易业务恢复类型
        //原交易业务状态
        //原交易属性
        cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo("");
        cmisLmt0010ReqDealBizListDto.setOrigiRecoverType("");
        cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus("");
        cmisLmt0010ReqDealBizListDto.setOrigiBizAttr("");

        cmisLmt0010ReqDealBizListDto.setCusId(map.get("cusId"));
        cmisLmt0010ReqDealBizListDto.setCusName(map.get("cusName"));
        cmisLmt0010ReqDealBizListDto.setPrdId(map.get("prdId"));
        cmisLmt0010ReqDealBizListDto.setPrdTypeProp(map.get("prdTypeProp"));
        cmisLmt0010ReqDealBizListDto.setPrdName(map.get("prdName"));
        cmisLmt0010ReqDealBizListDto.setDealBizAmt(appAmt);
        cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(appAmt);

        cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
        cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
        cmisLmt0010ReqDealBizListDto.setEndDate(map.get("loanEndDate"));
        List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
        cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
        cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);
        log.info("根据业务申请编号【" + serno + "】前往额度系统-台账校验请求报文：" + cmisLmt0010ReqDto.toString());
        ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDto = cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto);
        log.info("根据业务申请编号【" + serno + "】前往额度系统-台账校验返回报文：" + cmisLmt0010RespDto.toString());
        // 响应内容为空
        if(Objects.isNull(cmisLmt0010RespDto) || Objects.isNull(cmisLmt0010RespDto.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02901);
            return riskResultDto;
        }
        log.info("根据业务申请编号【" + serno + "】前往额度系统-额度校验返回报文：" + cmisLmt0010RespDto.toString());
        String code = cmisLmt0010RespDto.getData().getErrorCode();
        if (!Objects.equals("0000",code)) {
            log.info("根据业务申请编号【{}】,前往额度系统-额度校验失败！",serno);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(cmisLmt0010RespDto.getData().getErrorMsg());
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: checkPvpAccpApp
     * @方法描述: 国结票据出账额度校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto checkPvpAccpApp(PvpAccpApp pvpAccpApp) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //1. 向额度系统发送接口：校验额度
        String pvpSerno = pvpAccpApp.getPvpSerno();
        CmisLmt0033ReqDto cmisLmt0033ReqDto = new CmisLmt0033ReqDto();
        cmisLmt0033ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0033ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpAccpApp.getManagerBrId()));//金融机构代码
        cmisLmt0033ReqDto.setSerno(pvpSerno);//交易流水号
        cmisLmt0033ReqDto.setBizNo(pvpAccpApp.getContNo());//合同编号

        String guarMode = pvpAccpApp.getGuarMode();
        BigDecimal BizAmtCny = BigDecimal.ZERO;
        BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            BizAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
            bizSpacAmtCny = BigDecimal.ZERO;
        } else {
            BizAmtCny = pvpAccpApp.getAppAmt();
            bizSpacAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
        }

        //调票据接口查询票据起始日到期日
        String endDate = "";
        Xdpj23ReqDto xdpj23ReqDto = new Xdpj23ReqDto();
        xdpj23ReqDto.setsBatchNo(pvpSerno);
        xdpj23ReqDto.setTurnPageShowNum("100");
        xdpj23ReqDto.setTurnPageBeginPos("1");
        LocalDateTime now = LocalDateTime.now();
        xdpj23ReqDto.setMac("");//mac地址
        xdpj23ReqDto.setIpaddr("");//ip地址
        xdpj23ReqDto.setServdt(tranDateFormtter.format(now));//交易日期
        xdpj23ReqDto.setServti(tranTimestampFormatter.format(now));//交易时间
        log.info("通过批次号【{}】，前往票据系统查询票据明细开始,请求报文为:【{}】", pvpSerno, JSON.toJSONString(xdpj23ReqDto));
        ResultDto<Xdpj23RespDto> xdpj23RespDtoResultDto = dscms2PjxtClientService.xdpj23(xdpj23ReqDto);
        log.info("通过批次号【{}】，前往票据系统查询票据明细结束,响应报文为:【{}】", pvpSerno, JSON.toJSONString(xdpj23RespDtoResultDto));
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj23RespDtoResultDto.getCode())
                && Objects.nonNull(xdpj23RespDtoResultDto.getData())) {
            Xdpj23RespDto data = xdpj23RespDtoResultDto.getData();
            List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> xdpj23List = data.getList();
            if(CollectionUtils.nonEmpty(xdpj23List)){
                List<String> collect = xdpj23List.stream().filter(Objects::nonNull).sorted((e1, e2) -> e2.getDueDt().compareTo(e1.getDueDt())).map(e -> e.getDueDt()).collect(Collectors.toList());
                endDate = collect.get(0);
            }
        }
        log.info("票据到期日为：【{}】",endDate);

        //台账明细
        List<CmisLmt0033DealBizListReqDto> cmisLmt0033DealBizListReqDtoList = new ArrayList<CmisLmt0033DealBizListReqDto>();
        CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto = new CmisLmt0033DealBizListReqDto();
        cmisLmt0033DealBizListReqDto.setDealBizNo(pvpSerno);//台账编号
        cmisLmt0033DealBizListReqDto.setIsFollowBiz("0");//是否无缝衔接
        cmisLmt0033DealBizListReqDto.setOrigiDealBizNo("");//原交易业务编号
        cmisLmt0033DealBizListReqDto.setOrigiRecoverType("");//原交易业务恢复类型
        cmisLmt0033DealBizListReqDto.setCusId(pvpAccpApp.getCusId());//客户编号
        cmisLmt0033DealBizListReqDto.setCusName(pvpAccpApp.getCusName());//客户名称
        cmisLmt0033DealBizListReqDto.setPrdNo(pvpAccpApp.getPrdId());//产品编号
        cmisLmt0033DealBizListReqDto.setPrdName(pvpAccpApp.getPrdName());//产品名称
        cmisLmt0033DealBizListReqDto.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());//授信品种类型
        cmisLmt0033DealBizListReqDto.setDealBizAmtCny(BizAmtCny);//台账占用总额(人民币)
        cmisLmt0033DealBizListReqDto.setDealBizSpacAmtCny(bizSpacAmtCny);//台账占用敞口(人民币)
        cmisLmt0033DealBizListReqDto.setStartDate(stringRedisTemplate.opsForValue().get("openDay"));//台账起始日
        cmisLmt0033DealBizListReqDto.setEndDate(endDate);//台账到期日
        cmisLmt0033DealBizListReqDtoList.add(cmisLmt0033DealBizListReqDto);
        cmisLmt0033ReqDto.setDealBizList(cmisLmt0033DealBizListReqDtoList);

        //是否他行代签为是时，需要占用同业客户额度
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(pvpAccpApp.getIsOtherSign())){
            List<CmisLmt0033OccRelListReqDto> cmisLmt0033OccRelListReqDtoList = new ArrayList<CmisLmt0033OccRelListReqDto>();
            CmisLmt0033OccRelListReqDto cmisLmt0033OccRelListReqDto = new CmisLmt0033OccRelListReqDto();
            cmisLmt0033OccRelListReqDto.setDealBizNo(pvpAccpApp.getPvpSerno());//台账编号
            cmisLmt0033OccRelListReqDto.setLmtCusId("8001284352");//同业客户编号
            cmisLmt0033OccRelListReqDto.setBizSpacAmtCny(pvpAccpApp.getAppAmt());//占用敞口
            cmisLmt0033OccRelListReqDtoList.add(cmisLmt0033OccRelListReqDto);
            cmisLmt0033ReqDto.setOccRelList(cmisLmt0033OccRelListReqDtoList);
        }
        log.info("银承出账申请【{}】，前往额度系统进行额度校验开始,请求报文为:【{}】", pvpSerno, JSON.toJSONString(cmisLmt0033ReqDto));
        ResultDto<CmisLmt0033RespDto> resultDto = cmisLmtClientService.cmislmt0033(cmisLmt0033ReqDto);
        log.info("银承出账申请【{}】，前往额度系统进行额度校验结束,响应报文为:【{}】", pvpSerno, JSON.toJSONString(resultDto));
        if(!"0".equals(resultDto.getCode())){
            log.error("银承出账申请【{}】前往额度系统进行额度校验异常！",pvpSerno);
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        // 响应内容为空
        if(Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02901);
            return riskResultDto;
        }
        String code = resultDto.getData().getErrorCode();
        if (!Objects.equals("0000",code)) {
            log.info("根据银承出账申请【{}】,前往额度系统-额度校验失败！",pvpSerno);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(resultDto.getData().getErrorMsg());
            return riskResultDto;
        }
        return riskResultDto;
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("最高额授信协议申请打回流程-获取最高额授信协议申请" + iqpSerno + "申请信息");
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(iqpSerno);
            if (iqpHighAmtAgrApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpHighAmtAgrAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("最高额授信协议申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("最高额授信协议申请否决流程-获取最高额授信协议申请" + iqpSerno + "申请信息");
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(iqpSerno);
            if (iqpHighAmtAgrApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (iqpHighAmtAgrApp.getLmtAccNo() != null && !"".equals(iqpHighAmtAgrApp.getLmtAccNo())) {
                String guarMode = iqpHighAmtAgrApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                } else {
                    recoverSpacAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                    recoverAmtCny = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpHighAmtAgrApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpHighAmtAgrApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpHighAmtAgrApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpHighAmtAgrApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("最高额授信协议申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("最高额授信协议申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }


    /**
     * @方法名称: queryCusLowRiskUseAmt
     * @方法描述: 查询客户低风险存量已用金额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public BigDecimal queryCusLowRiskUseAmt(String cusId, String managerId) {
        BigDecimal lowRiskUseAmt = new BigDecimal("0");
        log.info("查询客户低风险存量已用金额,客户号【{}】", cusId);
        // 组装额度报文
        CmisLmt0022ReqDto cmisLmt0022ReqDto = new CmisLmt0022ReqDto();
        cmisLmt0022ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerId));//金融机构代码
        List<CmisLmt0022CusListReqDto> cmisLmt0022CusListReqDtoList = new ArrayList<CmisLmt0022CusListReqDto>();
        CmisLmt0022CusListReqDto cmisLmt0022CusListReqDto = new CmisLmt0022CusListReqDto();
        cmisLmt0022CusListReqDto.setCusId(cusId);
        cmisLmt0022CusListReqDtoList.add(cmisLmt0022CusListReqDto);
        cmisLmt0022ReqDto.setCusList(cmisLmt0022CusListReqDtoList);

        log.info("根据客户编号【{}】查询低风险存量授信额度,额度系统请求数据【{}】", cusId, cmisLmt0022ReqDto.toString());
        CmisLmt0022RespDto cmisLmt0022RespDto = cmisLmtClientService.cmisLmt0022(cmisLmt0022ReqDto).getData();
        log.info("根据客户编号【{}】查询低风险存量授信额度,额度系统返回数据【{}】", cusId, cmisLmt0022RespDto.toString());
        if(cmisLmt0022RespDto != null && cmisLmt0022RespDto.getLmtList().size() > 0 ){
            for (CmisLmt0022LmtListRespDto cmisLmt0022LmtListRespDto : cmisLmt0022RespDto.getLmtList()) {
                if(CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_22.equals(cmisLmt0022LmtListRespDto.getLmtCatalog())){
                    lowRiskUseAmt = cmisLmt0022LmtListRespDto.getOutstandAmt();
                    log.info("根据客户编号【{}】查询低风险存量授信额度,余额为【{}】", lowRiskUseAmt);
                    break;
                }
            }
        }else{
            log.error("根据客户编号【{}】查询低风险存量授信额度,额度系统返回数据异常【{}】", cusId, cmisLmt0022RespDto.toString());
        }
        return lowRiskUseAmt;
    }

    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 最高额授信协议判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("最高额授信协议判断是否足额,流水号【{}】", iqpHighAmtAgrApp.getSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpHighAmtAgrApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpHighAmtAgrApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpHighAmtAgrApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" + iqpHighAmtAgrApp.getLmtAccNo());
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        if(cmisLmt0026RespDto != null && iqpHighAmtAgrApp.getAgrContHighAvlAmt().compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 最高额授信协议流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("最高额授信协议更新流程参数,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpHighAmtAgrApp iqpHighAmtAgrApp = this.queryIqpHighAmtAgrAppDataBySerno(serno);
        Map<String, Object> params = new HashMap<>();


        if(CmisCommonConstants.GUAR_MODE_60.equals(iqpHighAmtAgrApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_21.equals(iqpHighAmtAgrApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_40.equals(iqpHighAmtAgrApp.getGuarMode())){
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", iqpHighAmtAgrApp.getAgrContHighAvlAmt().add(queryCusLowRiskUseAmt(iqpHighAmtAgrApp.getCusId(), iqpHighAmtAgrApp.getManagerBrId())));
        }else{
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", new BigDecimal("0"));
        }
        // 是否足额
        params.put("isAmtEnough", checkLmtAmtIsEnough(iqpHighAmtAgrApp));
        // 申请金额
        params.put("amt", iqpHighAmtAgrApp.getAgrContHighAvlAmt());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    /**
     * @方法名称: iqpHighAmtSubmitNoFlow
     * @方法描述: 村镇银行无流程提交后续处理
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map iqpHighAmtSubmitNoFlow(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            log.info("村镇银行最高额授信协议申请提交审批无流程处理开始：" + serno);
            //发起节点执行下面的逻辑
            this.handleBusinessDataAfterStart(serno);
            //针对流程到办结节点，进行以下处理
            //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
            this.handleBusinessDataAfterEnd(serno);
            //微信通知
            IqpHighAmtAgrApp iqpHighAmtAgrApp = this.selectByHighAmtAgrSernoKey(serno);
            String managerId = iqpHighAmtAgrApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", iqpHighAmtAgrApp.getCusName());
                paramMap.put("prdName", "一般合同申请");
                paramMap.put("result", "通过");
                log.info("村镇银行普通合同贷款申请提交审批无流程微信通知结束：");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            }
            log.info("村镇银行最高额授信协议申请提交审批无流程处理结束：" + serno);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("村镇银行最高额授信协议申请提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: sendOnlinePldRemind
     * @方法描述: 首页消息提醒
     * @参数与返回说明:
     * @创建者：qw
     * @算法描述: 无
     */
    public void sendOnlinePldRemind(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        log.info("业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
        //针对流程到办结节点，进行以下处理
        //首页消息提醒
        IqpHighAmtAgrApp iqpHighAmtAgrApp = this.selectByHighAmtAgrSernoKey(serno);
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpHighAmtAgrApp.getIsOlPld())){
            String managerId = iqpHighAmtAgrApp.getManagerId();
            String managerBrId = iqpHighAmtAgrApp.getManagerBrId();
            if (StringUtil.isNotEmpty(managerId)) {
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_DG_M_0001";//短信编号
                    // 翻译管护经理名称和管护经理机构名称
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
                    if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                    if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                    //短信填充参数
                    Map paramMap = new HashMap();
                    paramMap.put("cusName", iqpHighAmtAgrApp.getCusName());
                    paramMap.put("managerId", managerName);
                    paramMap.put("managerBrId", managerBrName);
                    paramMap.put("instanceId", resultInstanceDto.getInstanceId());
                    paramMap.put("nodeSign", resultInstanceDto.getNodeSign());
                    //执行发送客户经理操作
                    messageCommonService.sendonlinepldremind(messageType, receivedUserType, paramMap);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
    }

    /**
     * @param orgCode
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author qw
     * @date 2021-10-15 16:19:38
     * @version 1.0.0
     * @desc 根据机构以及业务类型返回对应的合同序列号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getSuitableContNo(String orgCode,String busiType) {
        String dkSeq = "";
        if(CmisCommonConstants.STD_BUSI_TYPE_01.equals(busiType)){//最高额授信协议
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCXY_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCXY_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCXY_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_02.equals(busiType)){//普通贷款合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCDK_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCDK_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCDK_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_03.equals(busiType)){//贴现协议
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCTX_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCTX_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCTX_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_04.equals(busiType)){//贸易融资合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCMR_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCMR_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCMR_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_05.equals(busiType)){//福费廷合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCFFT_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCFFT_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCFFT_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_06.equals(busiType)){//开证合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCXYZ_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCXYZ_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCXYZ_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_07.equals(busiType)){//银承合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCYC_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCYC_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCYC_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_08.equals(busiType)){//保函合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCBH_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCBH_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCBH_SEQ;
            }
        }else if(CmisCommonConstants.STD_BUSI_TYPE_09.equals(busiType)){//委托贷款合同
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCWT_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCWT_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCWT_SEQ;
            }
        }else{//资产池
            if (orgCode.startsWith("80")) {//寿光
                dkSeq = SeqConstant.SGCZC_SEQ;
            }else if (orgCode.startsWith("81")) {//东海
                dkSeq = SeqConstant.DHCZC_SEQ;
            }else {//张家港
                dkSeq = SeqConstant.ZRCZC_SEQ;
            }
        }
        return dkSeq;
    }
}
