package cn.com.yusys.yusp.web.server.xdcz0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0005.req.Xdcz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.resp.Xdcz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0005.Xdcz0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:资产池入池接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0005:资产池入池接口")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0005.BizXdcz0005Resource.class);
    @Autowired
    private Xdcz0005Service xdcz0005Service;
    /**
     * 交易码：xdcz0005
     * 交易描述：资产池入池接口
     *
     * @param xdcz0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池入池接口")
    @PostMapping("/xdcz0005")
    protected @ResponseBody
    ResultDto<Xdcz0005DataRespDto> xdcz0005(@Validated @RequestBody Xdcz0005DataReqDto xdcz0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataReqDto));
        Xdcz0005DataRespDto xdcz0005DataRespDto = new Xdcz0005DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0005DataRespDto> xdcz0005DataResultDto = new ResultDto<>();

        try {
            // 从xdcz0005DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataReqDto));
            xdcz0005DataRespDto = xdcz0005Service.xdcz0005(xdcz0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataRespDto));


            // 封装xdcz0005DataResultDto中正确的返回码和返回信息
            xdcz0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, e.getMessage());
            // 封装xdcz0005DataResultDto中异常返回码和返回信息
            xdcz0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0005DataRespDto到xdcz0005DataResultDto中
        xdcz0005DataResultDto.setData(xdcz0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataRespDto));
        return xdcz0005DataResultDto;
    }
}
