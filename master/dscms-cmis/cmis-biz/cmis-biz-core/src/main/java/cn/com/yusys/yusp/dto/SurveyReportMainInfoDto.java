package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyReportMainInfo
 * @类描述: survey_report_main_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-12 10:02:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class SurveyReportMainInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveyNo;
	
	/** 调查类型 **/
	private String surveyType;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品编号 **/
	private String prdCode;
	
	/** 产品类型 **/
	private String prdType;
	
	/** 客户编号 **/
	private String cusNo;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certNo;
	
	/** 调查表类型 **/
	private String surveyTableType;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;
	
	/** 审批状态 **/
	private String apprStatus;
	
	/** 数据来源 **/
	private String dataSour;
	
	/** 进件时间 **/
	private String intoTime;
	
	/** 营销工号 **/
	private String marketJobNo;
	
	/** 登记人工号 **/
	private String inputIdJobNo;
	
	/** 责任人工号 **/
	private String managerIdJobNo;
	
	/** 营销人 **/
	private String marketId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 责任人 **/
	private String managerId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 所属团队 **/
	private String belgTeam;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo == null ? null : surveyNo.trim();
	}
	
    /**
     * @return SurveyNo
     */	
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param surveyType
	 */
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType == null ? null : surveyType.trim();
	}
	
    /**
     * @return SurveyType
     */	
	public String getSurveyType() {
		return this.surveyType;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode == null ? null : prdCode.trim();
	}
	
    /**
     * @return PrdCode
     */	
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType == null ? null : prdType.trim();
	}
	
    /**
     * @return PrdType
     */	
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param cusNo
	 */
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo == null ? null : cusNo.trim();
	}
	
    /**
     * @return CusNo
     */	
	public String getCusNo() {
		return this.cusNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo == null ? null : certNo.trim();
	}
	
    /**
     * @return CertNo
     */	
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param surveyTableType
	 */
	public void setSurveyTableType(String surveyTableType) {
		this.surveyTableType = surveyTableType == null ? null : surveyTableType.trim();
	}
	
    /**
     * @return SurveyTableType
     */	
	public String getSurveyTableType() {
		return this.surveyTableType;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param dataSour
	 */
	public void setDataSour(String dataSour) {
		this.dataSour = dataSour == null ? null : dataSour.trim();
	}
	
    /**
     * @return DataSour
     */	
	public String getDataSour() {
		return this.dataSour;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime == null ? null : intoTime.trim();
	}
	
    /**
     * @return IntoTime
     */	
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param marketJobNo
	 */
	public void setMarketJobNo(String marketJobNo) {
		this.marketJobNo = marketJobNo == null ? null : marketJobNo.trim();
	}
	
    /**
     * @return MarketJobNo
     */	
	public String getMarketJobNo() {
		return this.marketJobNo;
	}
	
	/**
	 * @param inputIdJobNo
	 */
	public void setInputIdJobNo(String inputIdJobNo) {
		this.inputIdJobNo = inputIdJobNo == null ? null : inputIdJobNo.trim();
	}
	
    /**
     * @return InputIdJobNo
     */	
	public String getInputIdJobNo() {
		return this.inputIdJobNo;
	}
	
	/**
	 * @param managerIdJobNo
	 */
	public void setManagerIdJobNo(String managerIdJobNo) {
		this.managerIdJobNo = managerIdJobNo == null ? null : managerIdJobNo.trim();
	}
	
    /**
     * @return ManagerIdJobNo
     */	
	public String getManagerIdJobNo() {
		return this.managerIdJobNo;
	}
	
	/**
	 * @param marketId
	 */
	public void setMarketId(String marketId) {
		this.marketId = marketId == null ? null : marketId.trim();
	}
	
    /**
     * @return MarketId
     */	
	public String getMarketId() {
		return this.marketId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param belgTeam
	 */
	public void setBelgTeam(String belgTeam) {
		this.belgTeam = belgTeam == null ? null : belgTeam.trim();
	}
	
    /**
     * @return BelgTeam
     */	
	public String getBelgTeam() {
		return this.belgTeam;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}