package cn.com.yusys.yusp.web.server.xdxw0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0010.req.Xdxw0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0010.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0010.resp.Xdxw0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0010.Xdxw0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Arrays;
import java.util.Optional;

/**
 * 接口处理类:勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0010:勘验列表信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0010Resource.class);

    @Autowired
    private Xdxw0010Service xdxw0010Service;

    /**
     * 交易码：xdxw0010
     * 交易描述：勘验列表信息查询
     *
     * @param xdxw0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("勘验列表信息查询")
    @PostMapping("/xdxw0010")
    protected @ResponseBody
    ResultDto<Xdxw0010DataRespDto> xdxw0010(@Validated @RequestBody Xdxw0010DataReqDto xdxw0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010DataReqDto));
        Xdxw0010DataRespDto xdxw0010DataRespDto = new Xdxw0010DataRespDto();// 响应Dto:勘验列表信息查询
        ResultDto<Xdxw0010DataRespDto> xdxw0010DataResultDto = new ResultDto<>();
        // 从xdxw0010DataReqDto获取业务值进行业务逻辑处理
        String certNo = xdxw0010DataReqDto.getCertNo();//勘验人身份证号（客户）
        Integer startPageNum = xdxw0010DataReqDto.getStartPageNum();//开始页数
        Integer pageSize = xdxw0010DataReqDto.getPageSize();//每页数
        String status = xdxw0010DataReqDto.getStatus();//状态
        String surveySerno = xdxw0010DataReqDto.getSurveySerno();
        if(StringUtil.isEmpty(certNo)&&StringUtil.isEmpty(surveySerno)){
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }

        try {
            // 调用xdxw0010Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010DataReqDto));
            java.util.List<List> result = Optional.ofNullable(xdxw0010Service.getInspectInfoList(xdxw0010DataReqDto)).orElseGet(() -> {
                List list = new List();
                list.setSerno(StringUtils.EMPTY);// 业务编号
                list.setCheckMan(StringUtils.EMPTY);// 勘验人（客户）
                list.setCertNo(StringUtils.EMPTY);// 勘验人身份证号（客户）
                list.setManagerId(StringUtils.EMPTY);// 客户经理号
                list.setManagerName(StringUtils.EMPTY);// 客户经理名
                list.setGuarantyId(StringUtils.EMPTY);// 抵押物编号
                list.setEstateName(StringUtils.EMPTY);// 小区名称
                list.setAddr(StringUtils.EMPTY);// 地址
                list.setBuilding(StringUtils.EMPTY);// 楼栋
                list.setSqu(StringUtils.EMPTY);// 面积
                list.setOwner(StringUtils.EMPTY);// 所有权人
                list.setStatus(StringUtils.EMPTY);// 状态
                return Arrays.asList(list);
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(result));
            xdxw0010DataRespDto.setList(result);
            // 封装xdxw0010DataResultDto中正确的返回码和返回信息
            xdxw0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, e.getMessage());
            // 封装xdxw0010DataResultDto中异常返回码和返回信息
            xdxw0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0010DataRespDto到xdxw0010DataResultDto中
        xdxw0010DataResultDto.setData(xdxw0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010DataResultDto));
        return xdxw0010DataResultDto;
    }
}
