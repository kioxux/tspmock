package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkDebtCfrm
 * @类描述: iqp_risk_chk_debt_cfrm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-25 17:33:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpRiskChkDebtCfrmDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** APPT_CODE **/
	private String apptCode;
	
	/** 客户编号 **/
	private String custId;
	
	/** 场景 **/
	private String secen;
	
	/** 有负债金额汇总 **/
	private java.math.BigDecimal debtAmtSum;
	
	/** 有负债金额汇总(折算后) **/
	private java.math.BigDecimal debtAmtSumz;
	
	/** 已有负债月还款额汇总 **/
	private java.math.BigDecimal mDebtAmtSum;
	
	/** 已有负债月还款额汇总(折算后) **/
	private java.math.BigDecimal mDebtAmtSumz;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId == null ? null : custId.trim();
	}
	
    /**
     * @return CustId
     */	
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param secen
	 */
	public void setSecen(String secen) {
		this.secen = secen == null ? null : secen.trim();
	}
	
    /**
     * @return Secen
     */	
	public String getSecen() {
		return this.secen;
	}
	
	/**
	 * @param debtAmtSum
	 */
	public void setDebtAmtSum(java.math.BigDecimal debtAmtSum) {
		this.debtAmtSum = debtAmtSum;
	}
	
    /**
     * @return DebtAmtSum
     */	
	public java.math.BigDecimal getDebtAmtSum() {
		return this.debtAmtSum;
	}
	
	/**
	 * @param debtAmtSumz
	 */
	public void setDebtAmtSumz(java.math.BigDecimal debtAmtSumz) {
		this.debtAmtSumz = debtAmtSumz;
	}
	
    /**
     * @return DebtAmtSumz
     */	
	public java.math.BigDecimal getDebtAmtSumz() {
		return this.debtAmtSumz;
	}
	
	/**
	 * @param mDebtAmtSum
	 */
	public void setMDebtAmtSum(java.math.BigDecimal mDebtAmtSum) {
		this.mDebtAmtSum = mDebtAmtSum;
	}
	
    /**
     * @return MDebtAmtSum
     */	
	public java.math.BigDecimal getMDebtAmtSum() {
		return this.mDebtAmtSum;
	}
	
	/**
	 * @param mDebtAmtSumz
	 */
	public void setMDebtAmtSumz(java.math.BigDecimal mDebtAmtSumz) {
		this.mDebtAmtSumz = mDebtAmtSumz;
	}
	
    /**
     * @return MDebtAmtSumz
     */	
	public java.math.BigDecimal getMDebtAmtSumz() {
		return this.mDebtAmtSumz;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}