package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;

/**
 * @param 
 * @return 
 * @author shenli
 * @date 2021/4/27 0027 16:03
 * @version 1.0.0
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class SignCtrLoanContDto implements Serializable {

    @JsonProperty(value = "CtrLoanCont")
    private CtrLoanCont ctrLoanCont;

    public CtrLoanCont getCtrLoanCont() {
        return ctrLoanCont;
    }

    public void setCtrLoanCont(CtrLoanCont ctrLoanCont) {
        this.ctrLoanCont = ctrLoanCont;
    }

    public List<GrtGuarCont> getGrtGuarContList() {
        return grtGuarContList;
    }

    public void setGrtGuarContList(List<GrtGuarCont> grtGuarContList) {
        this.grtGuarContList = grtGuarContList;
    }

    @JsonProperty(value = "GrtGuarContList")
    private java.util.List<GrtGuarCont> grtGuarContList;

}
