package cn.com.yusys.yusp.service.server.xdht0034;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0034.req.Xdht0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0034.resp.Xdht0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 业务逻辑类:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdht0034Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0034Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易码：xdht0034
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0034DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0034DataRespDto xdht0034(Xdht0034DataReqDto xdht0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataReqDto));
        Xdht0034DataRespDto xdht0034DataRespDto = new Xdht0034DataRespDto();
        try {
            String queryType = xdht0034DataReqDto.getQueryType();//查询类型
            String cusId = xdht0034DataReqDto.getCusId();//客户号
            QueryModel queryModel = new QueryModel();
            BigDecimal applyAmt = BigDecimal.ZERO;// 申请金额
            List<BigDecimal> applyAmtList = new ArrayList<>();
            if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_LASTCONTAMT.key, queryType)) {//查询优企贷
                queryModel.addCondition("cus_id", cusId);
                logger.info("根据核心客户号查询最近生效的一笔优企贷合同金额开始,查询参数为:{}", JSON.toJSONString(queryModel));
                applyAmt = ctrLoanContMapper.queryLastContAmt(queryModel);
                logger.info("根据核心客户号查询最近生效的一笔优企贷合同金额结束,返回结果为:{}", JSON.toJSONString(applyAmt));
            } else if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_LASTYNDCONTAMT.key, queryType)) {//查询优农贷
                queryModel.addCondition("cus_id", cusId);
                queryModel.setSort(" sign_date desc");
                logger.info("根据核心客户号查询最近生效的一笔优农贷合同金额开始,查询参数为:{}", JSON.toJSONString(queryModel));
                applyAmt = ctrLoanContMapper.queryLastYndContAmt(queryModel);
                logger.info("根据核心客户号查询最近生效的一笔优农贷合同金额结束,返回结果为:{}", JSON.toJSONString(applyAmt));
            }
            xdht0034DataRespDto.setApplyAmt(applyAmt.toEngineeringString());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataRespDto));
        return xdht0034DataRespDto;
    }
}
