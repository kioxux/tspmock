package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 流程处理类:抵押物信息创建流程（对公）
 */
@Service
public class DBGL01BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DBGL01BizService.class);

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        // 对公抵押录入申请流水号
        String serno = instanceInfo.getBizId();

        String logPrefix = instanceInfo.getFlowName()+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");

        //获取节点标识  todo
//        String fromNodeSign = "0";
//        String toNodeSign = "1";

        try {
            GuarBaseInfo guarBaseInfo = new GuarBaseInfo() ;
            //guarBaseInfo.setSerno(serno);
            guarBaseInfo = guarBaseInfoService.selectByPrimaryKey(serno);
//            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
//            creditReportQryLst.setCrqlSerno(serno);
            //  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
            // N-> 不做任何处理,一直为审批中的状态

            //将参数放入Param里
            put2VarParam(instanceInfo,guarBaseInfo);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "运行操作:" + instanceInfo);

                if ("7_15".equals(instanceInfo.getCurrentNodeId())){
                    guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                }else if ("7_16".equals(instanceInfo.getCurrentNodeId())){
                    log.info("对公抵押录入申请流水号【"+serno+"】,资料扫描岗将是否扫描资料字段改为1--是");
                    guarBaseInfo.setIsScanMater(CmisCommonConstants.YES_NO_1);
                }else if ("7_17".equals(instanceInfo.getCurrentNodeId())){
                    log.info("对公抵押录入申请流水号【"+serno+"】,集中作业押品信息录入岗将是否扫描资料字段改为0--否");
                    guarBaseInfo.setIsScanMater(CmisCommonConstants.YES_NO_0);
                }

                guarBaseInfoService.updateSelective(guarBaseInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ instanceInfo);
                guarBaseInfoService.handleBusinessAfterEnd(serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作:" + instanceInfo);
                //判定节点标识 是否业务处理
                // 否决改变标志 审批中 111 -> 打回 992
                guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                guarBaseInfoService.updateSelective(guarBaseInfo);
                put2VarParam(instanceInfo, guarBaseInfo);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info(logPrefix + "打回操作:" + instanceInfo);
                //判定节点标识 是否业务处理
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    guarBaseInfoService.updateSelective(guarBaseInfo);
                    put2VarParam(instanceInfo, guarBaseInfo);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info(logPrefix + "拿回操作:" + instanceInfo);
                guarBaseInfo.setApproveStatus("991");
                guarBaseInfoService.updateSelective(guarBaseInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info(logPrefix + "拿回初始节点操作:" + instanceInfo);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                guarBaseInfo.setApproveStatus("998");
                guarBaseInfoService.updateSelective(guarBaseInfo);
            } else {
                log.warn(logPrefix + "未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils BizCommonUtils = new BizCommonUtils();
                BizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DBGL01.equals(bizType);
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: mashun
     * @创建时间: 2021-07-12 10:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, GuarBaseInfo guarBaseInfo) {
        WFBizParamDto wfBizParam = new WFBizParamDto();
        wfBizParam.setBizId(resultInstanceDto.getBizId());
        wfBizParam.setInstanceId(resultInstanceDto.getInstanceId());

        Map<String, Object> params = new HashMap<>();
        params.put("approveStatus",guarBaseInfo.getApproveStatus());
        params.put("isScanMater",guarBaseInfo.getIsScanMater());
        wfBizParam.setParam(params);

        //押品所有人编号
        String guarCusId = guarBaseInfo.getGuarCusId();
        //押品所有人名称
        String guarCusName = guarBaseInfo.getGuarCusName();

        log.info("押品所有人编号:"+guarCusId+";押品所有人名称:"+guarCusName+";流程客户编号:"+wfBizParam.getBizUserId());

        if (StringUtils.nonEmpty(guarCusId) && StringUtils.isEmpty(wfBizParam.getBizUserId())){
            //如果押品信息里的押品所有人编号有值且流程实例里的biz_user_id没值，则更新biz_user_id和biz_user_name的值
            wfBizParam.setBizUserId(guarCusId);
            wfBizParam.setBizUserName(guarCusName);
        }

        workflowCoreClient.updateFlowParam(wfBizParam);
    }
}