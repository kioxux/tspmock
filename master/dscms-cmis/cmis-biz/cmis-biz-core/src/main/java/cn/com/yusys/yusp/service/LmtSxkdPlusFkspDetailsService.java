/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.EntlQyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSxkdPlusFkspDetails;
import cn.com.yusys.yusp.repository.mapper.LmtSxkdPlusFkspDetailsMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSxkdPlusFkspDetailsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-10-15 19:56:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSxkdPlusFkspDetailsService {

    @Autowired
    private LmtSxkdPlusFkspDetailsMapper lmtSxkdPlusFkspDetailsMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSxkdPlusFkspDetails selectByPrimaryKey(String pkId) {
        return lmtSxkdPlusFkspDetailsMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSxkdPlusFkspDetails> selectAll(QueryModel model) {
        List<LmtSxkdPlusFkspDetails> records = (List<LmtSxkdPlusFkspDetails>) lmtSxkdPlusFkspDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtSxkdPlusFkspDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSxkdPlusFkspDetails> list = lmtSxkdPlusFkspDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtSxkdPlusFkspDetails record) {
        return lmtSxkdPlusFkspDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtSxkdPlusFkspDetails record) {
        return lmtSxkdPlusFkspDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtSxkdPlusFkspDetails record) {
        return lmtSxkdPlusFkspDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtSxkdPlusFkspDetails record) {
        return lmtSxkdPlusFkspDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSxkdPlusFkspDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSxkdPlusFkspDetailsMapper.deleteByIds(ids);
    }


    /**
     * @创建人 zhangliang15
     * @创建时间 2021-10-02 15:35
     * @注释
     */
    public List<LmtSxkdPlusFkspDetails> queryLmtSxkdPlusFkspDetails(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSxkdPlusFkspDetails> list = lmtSxkdPlusFkspDetailsMapper.queryLmtSxkdPlusFkspDetails(model);
        PageHelper.clearPage();
        return list;
    }
}
