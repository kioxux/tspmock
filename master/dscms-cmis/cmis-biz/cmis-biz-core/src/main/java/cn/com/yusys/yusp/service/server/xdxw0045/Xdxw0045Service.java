package cn.com.yusys.yusp.service.server.xdxw0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.domain.LmtModelApprResultInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.server.xdxw0045.req.Xdxw0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0045.resp.Xdxw0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtModelApprResultInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0045Service
 * @类描述: #服务类
 * @功能描述:零售智能风控，跑模型失败了，发送02否决，信贷把调查报告表的状态更新成否决
 * 零售智能风控，客户经理人工作废，发送01撤回，信贷把调查报告表的状态更新成作废，通过流水号删除批复表一条记录
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0045Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0045Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper;

    /**
     * 查询调查表和其他关联信息
     *
     * @param Xdxw0045DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0045DataRespDto xdxw0045(Xdxw0045DataReqDto Xdxw0045DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value);
        Xdxw0045DataRespDto xdxw0045DataRespDto = new Xdxw0045DataRespDto();
        try {
            String opFlag = Xdxw0045DataReqDto.getOp_flag();//操作标识
            String surveySerno = Xdxw0045DataReqDto.getSurvey_serno();//流水号
            String taxGradeResult = Xdxw0045DataReqDto.getTax_grade_result();//模型评级结果
            String taxScore = Xdxw0045DataReqDto.getTax_score();//模型评分

            //查询授信调查表
            logger.info("***********XDXW0045***查询授信调查表开始,查询参数为:{}", JSON.toJSONString(surveySerno));
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(surveySerno);
            logger.info("**********XDXW0045***查询授信调查表结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));

            //根据调查流水号查询合同表是否存在已签订的第合同
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("surveySerno", surveySerno);
            List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByModel(queryModel);

            if (DscmsBizXwEnum.OPFLG_01.key.equals(opFlag)) { //撤回操作
                logger.info("***********XDXW0045***撤回操作*根据调查流水号查询合同表是否存在已签订的合同");
                if (!CollectionUtils.nonEmpty(ctrLoanContList)) {//不存在对应合同信息
                    //修改授信调查表作废状态
                    logger.info("***********XDXW0045***修改授信调查表作废状态");
                    if (lmtSurveyReportMainInfo != null && StringUtil.isNotEmpty(lmtSurveyReportMainInfo.getSurveySerno())) {
                        lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998);//作废

                        logger.info("***********XDXW0045***修改授信调查表作废状态开始,修改参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                        int count = lmtSurveyReportMainInfoMapper.updateByPrimaryKey(lmtSurveyReportMainInfo);
                        logger.info("**********XDXW0045***修改授信调查表作废状态结束,返回结果为:{}", JSON.toJSONString(count));
                        //删除授信批复表
                        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectByPrimaryKey(surveySerno);

                        if (lmtCrdReplyInfo != null) {
                            int result = lmtCrdReplyInfoMapper.deleteByPrimaryKey(surveySerno);
                        }

                        if (count > 0) {
                            xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                            xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                        } else {//同步更新失败
                            xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                            xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.FAIL.value);
                        }
                    } else {//不存在对应流水
                        xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.MESSAGE_03.value);
                    }
                } else {//已签订，不能作废
                    xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.MESSAGE_02.value);
                }
            } else if (DscmsBizXwEnum.OPFLG_02.key.equals(opFlag)) { //否决操作
                logger.info("***********XDXW0045**否决操作***************");
                if (lmtSurveyReportMainInfo != null && StringUtil.isNotEmpty(lmtSurveyReportMainInfo.getSurveySerno())) {//存在授信调查信息
                    if (!CollectionUtils.nonEmpty(ctrLoanContList)) {//不存在对应合同信息
                        logger.info("***********XDXW0045**不存在对应合同信息，执行否决操作***************");
                        lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998);//否决
                        int count = lmtSurveyReportMainInfoMapper.updateByPrimaryKey(lmtSurveyReportMainInfo);
                        logger.info("**********XDXW0045**不存在对应合同信息，执行否决操作结束,返回结果为:{}", JSON.toJSONString(count));
                        if (count > 0) {//修改成功
                            xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                            xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                        } else {
                            xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                            xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.FAIL.value);
                        }
                    } else {//已签订合同！无法否决！
                        xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.MESSAGE_02.value);
                    }
                } else {//不存在对应流水
                    xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.MESSAGE_03.value);
                }
            } else if (DscmsBizXwEnum.OPFLG_03.key.equals(opFlag)) {//正常同步更新评级信息
                //更新模型评级结果；模型评分
                logger.info("**********XDXW0045**查询lmtModelApprResultInfo开始,查询参数为:{}", JSON.toJSONString(surveySerno));
                LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoMapper.selectByPrimaryKey(surveySerno);
                logger.info("**********XDXW0045**查询lmtModelApprResultInfo结束,返回结果为:{}", JSON.toJSONString(lmtModelApprResultInfo));

                if (lmtModelApprResultInfo != null && StringUtil.isNotEmpty(lmtModelApprResultInfo.getSurveySerno())) {//存在授信调查信息
                    lmtModelApprResultInfo.setModelGrade(taxGradeResult);
                    lmtModelApprResultInfo.setModelScore(taxScore);
                    logger.info("**********XDXW0045**更新模型评级结果；模型评分开始,更新参数为:{}", JSON.toJSONString(lmtModelApprResultInfo));
                    int count = lmtModelApprResultInfoMapper.updateByPrimaryKey(lmtModelApprResultInfo);
                    logger.info("**********XDXW0045**更新模型评级结果；模型评分结束,返回结果为:{}", JSON.toJSONString(count));
                    if (count > 0) {//更新成功
                        xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                        xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                    } else {
                        xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.FAIL.value);
                    }
                } else {//不存在对应评级流水信息
                    xdxw0045DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0045DataRespDto.setOpMsg(DscmsBizXwEnum.MESSAGE_04.value);
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value);
        return xdxw0045DataRespDto;
    }

}
