/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtMachineSchedRel
 * @类描述: lmt_machine_sched_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 11:45:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_machine_sched_rel")
public class LmtMachineSchedRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 项目编号 **/
	@Column(name = "pro_no", unique = false, nullable = true, length = 40)
	private String proNo;
	
	/** 设备名称 **/
	@Column(name = "equip_name", unique = false, nullable = true, length = 80)
	private String equipName;
	
	/** 设备品牌 **/
	@Column(name = "equip_brand", unique = false, nullable = true, length = 80)
	private String equipBrand;
	
	/** 型号 **/
	@Column(name = "model", unique = false, nullable = true, length = 40)
	private String model;
	
	/** 销售价格 **/
	@Column(name = "sale_price", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal salePrice;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param equipName
	 */
	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}
	
    /**
     * @return equipName
     */
	public String getEquipName() {
		return this.equipName;
	}
	
	/**
	 * @param equipBrand
	 */
	public void setEquipBrand(String equipBrand) {
		this.equipBrand = equipBrand;
	}
	
    /**
     * @return equipBrand
     */
	public String getEquipBrand() {
		return this.equipBrand;
	}
	
	/**
	 * @param model
	 */
	public void setModel(String model) {
		this.model = model;
	}
	
    /**
     * @return model
     */
	public String getModel() {
		return this.model;
	}
	
	/**
	 * @param salePrice
	 */
	public void setSalePrice(java.math.BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	
    /**
     * @return salePrice
     */
	public java.math.BigDecimal getSalePrice() {
		return this.salePrice;
	}


}