/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateAppr
 * @类描述: retail_prime_rate_appr数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:20:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "retail_prime_rate_appr")
public class RetailPrimeRateAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 报价利率 **/
	@Column(name = "OFFER_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal offerRate;
	
	/** 申请执行利率 **/
	@Column(name = "APP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appRate;
	
	/** 批复利率 **/
	@Column(name = "REPLY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyRate;
	
	/** 审批结论 **/
	@Column(name = "APPROVE_CONCLUSION", unique = false, nullable = true, length = 100)
	private String approveConclusion;
	
	/** 审批意见 **/
	@Column(name = "APPROVE_ADVICE", unique = false, nullable = true, length = 2000)
	private String approveAdvice;
	
	/** 审核岗位 **/
	@Column(name = "APPROVE_POST", unique = false, nullable = true, length = 100)
	private String approvePost;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param offerRate
	 */
	public void setOfferRate(java.math.BigDecimal offerRate) {
		this.offerRate = offerRate;
	}
	
    /**
     * @return offerRate
     */
	public java.math.BigDecimal getOfferRate() {
		return this.offerRate;
	}
	
	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}
	
    /**
     * @return appRate
     */
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}
	
	/**
	 * @param replyRate
	 */
	public void setReplyRate(java.math.BigDecimal replyRate) {
		this.replyRate = replyRate;
	}
	
    /**
     * @return replyRate
     */
	public java.math.BigDecimal getReplyRate() {
		return this.replyRate;
	}
	
	/**
	 * @param approveConclusion
	 */
	public void setApproveConclusion(String approveConclusion) {
		this.approveConclusion = approveConclusion;
	}
	
    /**
     * @return approveConclusion
     */
	public String getApproveConclusion() {
		return this.approveConclusion;
	}
	
	/**
	 * @param approveAdvice
	 */
	public void setApproveAdvice(String approveAdvice) {
		this.approveAdvice = approveAdvice;
	}
	
    /**
     * @return approveAdvice
     */
	public String getApproveAdvice() {
		return this.approveAdvice;
	}
	
	/**
	 * @param approvePost
	 */
	public void setApprovePost(String approvePost) {
		this.approvePost = approvePost;
	}
	
    /**
     * @return approvePost
     */
	public String getApprovePost() {
		return this.approvePost;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}