/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.BlankCertiInfo;
import cn.com.yusys.yusp.repository.mapper.BlankCertiInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BlankCertiInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:59:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BlankCertiInfoService {
    private static final Logger log = LoggerFactory.getLogger(BlankCertiInfoService.class);

    @Autowired
    private BlankCertiInfoMapper blankCertiInfoMapper;

    @Autowired
    private BlankCertiModifyAppService blankCertiModifyAppService;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BlankCertiInfo selectByPrimaryKey(String pkId) {
        return blankCertiInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectByCertiNo
     * @方法描述: 根据凭证编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BlankCertiInfo selectByCertiNo(String certiNo) {
        return blankCertiInfoMapper.selectByCertiNo(certiNo);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BlankCertiInfo> selectAll(QueryModel model) {
        List<BlankCertiInfo> records = blankCertiInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public List<BlankCertiInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("input_date,certi_no");
        List<BlankCertiInfo> list = blankCertiInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BlankCertiInfo record) {
        return blankCertiInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BlankCertiInfo record) {
        return blankCertiInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BlankCertiInfo record) {
        return blankCertiInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BlankCertiInfo record) {
        return blankCertiInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return blankCertiInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * 校验凭证编号是否存在
     * @param queryModel
     * @return
     */
    public String checkCertiNoIsExist(QueryModel queryModel){
        //凭证编号
        String certiNo = (String) queryModel.getCondition().get("certiNo");
        //查询该凭证编号是否已存在于空白凭证信息表里
        BlankCertiInfo blankCertiInfo = blankCertiInfoMapper.selectByCertiNo(certiNo);

        if (blankCertiInfo!=null){
            return "凭证编号【"+certiNo+"】已存在于空白凭证信息表里！";
        }
        //查询该凭证编号是否已存在于空白凭证变更申请表里
        String serno = blankCertiModifyAppService.selectOnTheWaySernoByModel(queryModel);

        if(StringUtils.nonEmpty(serno)){
            return "凭证编号【"+certiNo+"】与空白凭证变更申请里的流水号为【"+serno+"】的修改后凭证编号一致！";
        }
        return null;
    }

    /**
     * 校验凭证编号是否存在
     * @param queryModel
     * @return
     */
    public String checkCertiNoIsUsed(QueryModel queryModel){
        //凭证编号
        String certiNo = (String) queryModel.getCondition().get("certiNo");
        //查询该凭证编号是否被在途的权证入库引用
        String serno = guarWarrantManageAppService.selectOnTheWaySernoByCertiRecordId(certiNo);

        if(StringUtils.nonEmpty(serno)){
            return "该凭证编号已被流水号为【"+serno+"】的权证入库所引用，无法变更！";
        }
        return null;
    }

    /**
     * 空白凭证登记簿批量导入
     *
     * @param perCertiInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertBlankCertiInfo(List<Object> perCertiInfoList) {
        List<BlankCertiInfo> certiInfoList = (List<BlankCertiInfo>) BeanUtils.beansCopy(perCertiInfoList, BlankCertiInfo.class);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();

        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            BlankCertiInfoMapper blankCertiInfoMapper = sqlSession.getMapper(BlankCertiInfoMapper.class);

            for (BlankCertiInfo certiInfo : certiInfoList) {
                QueryModel query = new QueryModel();
                query.addCondition("certiNo", certiInfo.getCertiNo());
                //校验凭证编号是否存在
                String result = checkCertiNoIsExist(query);

                if (result != null) {
                    throw new BizException(null, "9999", null, result);
                }
            }

            for (BlankCertiInfo certiInfo : certiInfoList) {
                BlankCertiInfo blankCertiInfo = new BlankCertiInfo();
                //凭证编号
                blankCertiInfo.setCertiNo(certiInfo.getCertiNo());
                //凭证状态 默认 01--未用
                blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_01);
                blankCertiInfo.setInputId(userInfo.getLoginCode());
                blankCertiInfo.setInputBrId(userInfo.getOrg().getCode());
                blankCertiInfo.setInputDate(sdf.format(new Date()));
                blankCertiInfo.setUpdId(userInfo.getLoginCode());
                blankCertiInfo.setUpdBrId(userInfo.getOrg().getCode());
                blankCertiInfo.setUpdDate(sdf.format(new Date()));
                blankCertiInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                blankCertiInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                blankCertiInfo.setOprType(CmisBizConstants.OPR_TYPE_01);
                //插入空白凭证信息表
                blankCertiInfoMapper.insert(blankCertiInfo);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perCertiInfoList.size();
    }
}