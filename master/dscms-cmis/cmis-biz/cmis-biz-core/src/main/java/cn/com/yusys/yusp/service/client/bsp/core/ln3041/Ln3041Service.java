package cn.com.yusys.yusp.service.client.bsp.core.ln3041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款归还
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3041Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：贷款归还
     *
     * @param ln3041ReqDto
     * @return
     */
    @Transactional
    public Ln3041RespDto ln3041(Ln3041ReqDto ln3041ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value, JSON.toJSONString(ln3041ReqDto));
        ResultDto<Ln3041RespDto> ln3041ResultDto = dscms2CoreLnClientService.ln3041(ln3041ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value, JSON.toJSONString(ln3041ResultDto));
        String ln3041Code = Optional.ofNullable(ln3041ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3041Meesage = Optional.ofNullable(ln3041ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3041RespDto ln3041RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3041ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3041RespDto = ln3041ResultDto.getData();
        } else {
            // 测试非要让我们判断出余额不足的异常 又没有错误码可以区分 只能匹配文字信息 于是产生以下代码
            if (ln3041Meesage.contains("可支付余额不足")) {
                ln3041Meesage = "还款账户余额不足";
            }
            //  抛出错误异常
            throw BizException.error(null, ln3041Code, ln3041Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value);
        return ln3041RespDto;
    }

    /**
     * 根据 [请求Data：提前还款 ]组装[请求Dto：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；]
     *
     * @param xddh0005DataReqDto
     * @return
     */
    public Ln3041ReqDto buildLn3041ReqDto(Xddh0005DataReqDto xddh0005DataReqDto) {
        String billNo = xddh0005DataReqDto.getBillNo();//借据号
        String repayType = xddh0005DataReqDto.getRepayType();//还款模式 (1-结清贷款,2-归还欠款,3-提前还款)
        BigDecimal repayAmt = xddh0005DataReqDto.getRepayAmt();//还款金额
        String repayAcctNo = xddh0005DataReqDto.getRepayAcctNo();//还款账号
        String curType = xddh0005DataReqDto.getCurType();//币种
        BigDecimal repayTotlCap = xddh0005DataReqDto.getRepayTotlCap();//还款总本金
        String tranSerno = xddh0005DataReqDto.getTranSerno();//交易流水号
        String yjhkms = xddh0005DataReqDto.getFstRepayMode();//一级还款模式 01-提前还款 02-归还拖欠
        String ejhkmx = xddh0005DataReqDto.getSedRepayMode();//二级还款模式 03-提前归还本金及全部利息 04-提前归还本金及本金对应利息 05-归还拖欠本息 08-灵活还款

        String Tqhktzfs = StringUtils.EMPTY;//提前还款调整计划方式 核心码值（0：不变 1： 摊薄 2:缩期 3:自动缩期）
        String tDktqhkzl = StringUtils.EMPTY;//提前还款种类 核心码值（1：指定本金 2：指定总额 3:全部结清 4:只还息）
        String Tqhkhxfs = StringUtils.EMPTY;//提前还款还息方式 核心码值（0：不还息 1： 部分还息 2:全部还息 ）

        /************************前还款种类*（1：指定本金 2：指定总额 3:全部结清 4:只还息）***************************/
        if("03".equals(ejhkmx)){//03-提前归还本金及全部利息
            tDktqhkzl = "2";
            Tqhktzfs = "1";
            Tqhkhxfs = "2";
        }else if("04".equals(ejhkmx)){//04-提前归还本金及本金对应利息
            tDktqhkzl = "2";
            Tqhktzfs = "1";
            Tqhkhxfs = "1";
        }else if("05".equals(ejhkmx)){// 05-归还拖欠本息
            tDktqhkzl = "";
            Tqhktzfs = "1";
            Tqhkhxfs = "";
        }else if("08".equals(ejhkmx)){// 05-灵活还款
            tDktqhkzl = "2";
            Tqhktzfs = "0";
            Tqhkhxfs = "0";
        }
        /******************还款模式 (1-结清贷款,2-归还欠款,3-提前还款)**********************/
        if("01".equals(yjhkms)){//提前还款
            repayType = "3";
        }else if("02".equals(yjhkms)){
            repayType = "2";
        }else{
            repayType = "1";
        }


        Ln3041ReqDto ln3041ReqDto = GenericBuilder.of(Ln3041ReqDto::new)//请求DTO:支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还
                .with(Ln3041ReqDto::setDaikczbz, "3")// 业务操作标志
                .with(Ln3041ReqDto::setDkjiejuh, billNo)// 贷款借据号
                .with(Ln3041ReqDto::setDkzhangh, StringUtils.EMPTY)// 贷款账号
                .with(Ln3041ReqDto::setHetongbh, StringUtils.EMPTY)// 合同编号
                .with(Ln3041ReqDto::setKehuhaoo, StringUtils.EMPTY)// 客户号
                .with(Ln3041ReqDto::setKehuzwmc, StringUtils.EMPTY)// 客户名
                .with(Ln3041ReqDto::setHuobdhao, changCurType(curType))// 货币代号
                .with(Ln3041ReqDto::setQixiriqi, StringUtils.EMPTY)// 起息日期
                .with(Ln3041ReqDto::setDkqixian, StringUtils.EMPTY)// 贷款期限,月)
                .with(Ln3041ReqDto::setDaoqriqi, StringUtils.EMPTY)// 到期日期
                .with(Ln3041ReqDto::setZhchbjin, null)// 正常本金
                .with(Ln3041ReqDto::setYuqibjin, null)// 逾期本金
                .with(Ln3041ReqDto::setDzhibjin, null)// 呆滞本金
                .with(Ln3041ReqDto::setDaizbjin, null)// 呆账本金
                .with(Ln3041ReqDto::setYsyjlixi, null)// 应收应计利息
                .with(Ln3041ReqDto::setCsyjlixi, null)// 催收应计利息
                .with(Ln3041ReqDto::setYsqianxi, null)// 应收欠息
                .with(Ln3041ReqDto::setCsqianxi, null)// 催收欠息
                .with(Ln3041ReqDto::setYsyjfaxi, null)// 应收应计罚息
                .with(Ln3041ReqDto::setCsyjfaxi, null)// 催收应计罚息
                .with(Ln3041ReqDto::setYshofaxi, null)// 应收罚息
                .with(Ln3041ReqDto::setCshofaxi, null)// 催收罚息
                .with(Ln3041ReqDto::setYingjifx, null)// 应计复息
                .with(Ln3041ReqDto::setFuxiiiii, null)// 复息
                .with(Ln3041ReqDto::setBenjheji, repayTotlCap)// 本金合计
                .with(Ln3041ReqDto::setLixiheji, null)// 利息合计
                .with(Ln3041ReqDto::setQiankzee, null)// 欠款总额
                .with(Ln3041ReqDto::setHuankzle, repayType)// 还款种类
                .with(Ln3041ReqDto::setDktqhkzl, tDktqhkzl)// 提前还款种类
                .with(Ln3041ReqDto::setTqhktysx, null)// 退客户预收息金额
                .with(Ln3041ReqDto::setYshxhbje, null)// 预收息还本金额
                .with(Ln3041ReqDto::setTqhktzfs, Tqhktzfs)// 提前还款调整计划方式
                .with(Ln3041ReqDto::setHuankjee, repayAmt)// 还款金额
                .with(Ln3041ReqDto::setZijnlaiy, "1")// 资金来源 0--现金 1--转账2--待销账3--内部资金账4--内部会计账
                .with(Ln3041ReqDto::setHuankzhh, repayAcctNo)// 还款账号
                .with(Ln3041ReqDto::setSfqzjieq, StringUtils.EMPTY)// 强制结清标志
                .with(Ln3041ReqDto::setHkzhhzxh, StringUtils.EMPTY)// 还款账号子序号
                .with(Ln3041ReqDto::setTqhkfjje, null)// 提前还款罚金金额
                .with(Ln3041ReqDto::setDaikghfs, StringUtils.EMPTY)// 贷款归还方式    01--借款人还款02--担保人还款03--第三方还款04--以物抵债05--资产转让06--转让资产回购07--资产证券化08--证券化资产回购09--化债10--再化债11--借新还旧12--政策性还款13--资产重组14--核销15--债权减免16--其他
                .with(Ln3041ReqDto::setHkbeizhu, StringUtils.EMPTY)// 还款备注
                .with(Ln3041ReqDto::setFajiftbl, null)// 罚金分摊比例
                .with(Ln3041ReqDto::setFajirzzh, StringUtils.EMPTY)// 罚金入账账号
                .with(Ln3041ReqDto::setFajirzxh, StringUtils.EMPTY)// 罚金入账子序号
                .with(Ln3041ReqDto::setTqhkhxfs, Tqhkhxfs)// 提前还款还息方式
                .with(Ln3041ReqDto::setPingzhzl, StringUtils.EMPTY)// 凭证种类
                .with(Ln3041ReqDto::setPingzhma, StringUtils.EMPTY)// 凭证批号
                .with(Ln3041ReqDto::setPngzxhao, StringUtils.EMPTY)// 凭证序号
                .with(Ln3041ReqDto::setZhfutojn, StringUtils.EMPTY)// 支付条件
                .with(Ln3041ReqDto::setJiaoymma, StringUtils.EMPTY)// 交易密码
                .with(Ln3041ReqDto::setMimazlei, StringUtils.EMPTY)// 密码种类
                .with(Ln3041ReqDto::setZhjnzlei, StringUtils.EMPTY)// 证件种类
                .with(Ln3041ReqDto::setZhjhaoma, StringUtils.EMPTY)// 证件号码
                .with(Ln3041ReqDto::setKehuzhlx, StringUtils.EMPTY)// 还款账号类型    0--对公账户1--卡2--活期一本通3--定期一本通4--活期存折5--存单7--股金账户8--贷款账户9--内部账A--组合账户B--一号通F--一证通存折G--国债N--电子储蓄国债X--待销账K--会计账户D--电子账户J--快捷账户
                .with(Ln3041ReqDto::setYanmjine, null)// 验密金额
                .with(Ln3041ReqDto::setHuandzms, StringUtils.EMPTY)// 还贷证明书
                .build();
        return ln3041ReqDto;
    }

    /*****
     * 核心系统与信贷币种码值转换
     * *********/
    public String changCurType(String curType) {
        if ("CNY".equals(curType)) {
            curType = "01";//人民币
        } else if ("GBP".equals(curType)) {
            curType = "12";//英镑
        } else if ("HKD".equals(curType)) {
            curType = "13";//港元
        } else if ("USD".equals(curType)) {
            curType = "14";//美元
        } else if ("CHF".equals(curType)) {
            curType = "15";//瑞士法郎
        } else if ("JPY".equals(curType)) {
            curType = "27";//日元
        } else if ("EUR".equals(curType)) {
            curType = "38";//欧元
        } else if ("CAD".equals(curType)) {
            curType = "28";//加元
        } else if ("AUD".equals(curType)) {
            curType = "29";//澳大利亚元
        } else if ("SEK".equals(curType)) {
            curType = "21";//瑞典克郎
        }
        return curType;
    }


}
