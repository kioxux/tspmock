package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class LmtGrpSubDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //成员名称
    private String subName;
    //分项流水号
    private String subSerno;
    //授信品种
    private String preLmtBizType;
    //是否预授信额度
    private String  isPreLmt;
    //担保方式
    private  String  guarMode;
    // 授信额度
    private String lmtAmt;

    // 额度期限
    private String lmtTerm;


    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getPreLmtBizType() {
        return preLmtBizType;
    }

    public void setPreLmtBizType(String preLmtBizType) {
        this.preLmtBizType = preLmtBizType;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(String lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(String lmtTerm) {
        this.lmtTerm = lmtTerm;
    }
}
