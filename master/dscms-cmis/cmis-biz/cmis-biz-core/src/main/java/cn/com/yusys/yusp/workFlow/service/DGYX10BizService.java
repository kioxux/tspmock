package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.FdyddWhbxdApproval;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req.Fb1146ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp.Fb1146RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 房抵e点贷无还本续贷审核流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX10BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX10BizService.class);//定义log

    @Autowired
    private FdyddWhbxdApprovalService fdyddWhbxdApprovalService;
    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 房贷无还本续贷
        if ("YX018".equals(bizType)) {
            fdyddWhbxdApprovalBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 房贷无还本续贷
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void fdyddWhbxdApprovalBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            FdyddWhbxdApproval fdyddWhbxdApproval = fdyddWhbxdApprovalService.selectByPrimaryKey(serno);
            log.info("房贷无还本续贷:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("房贷无还本续贷:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("房贷无还本续贷:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("房贷无还本续贷:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("房贷无还本续贷:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("房贷无还本续贷:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("房贷无还本续贷:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_997);
                // 调用fb1168 交易描述：抵押查封结果推送
                String flag = this.sendfb1146(fdyddWhbxdApproval);
                if(!"success".equals(flag)){
                    throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "风控返回："+flag);
                }
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("房贷无还本续贷:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(fdyddWhbxdApproval, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("房贷无还本续贷:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 房抵e点贷无还本续贷审核
     * sfResultInfo 业务申请信息
     * */
    public String sendfb1146(FdyddWhbxdApproval record){
        /*
         *************************************************************************
         * 调用风控fb1146房抵e点贷信息审核
         * *************************************************************************/
        // 获取营业日期
        log.info("房抵e点贷客户经理否决调用受托信息审核开始,流水号【{}】", record.getSerno());
        Fb1146ReqDto fb1146ReqDto = new Fb1146ReqDto();
        fb1146ReqDto.setApp_no(record.getSerno()); //申请流水号
        fb1146ReqDto.setExec_rate(record.getAdjustInterestRate()); //调整后利率
        fb1146ReqDto.setVrify_task (record.getApproveResult()); //受托审核状态
        log.info("房抵e点贷客户经理否决调用受托信息审核请求：" + fb1146ReqDto);
        ResultDto<Fb1146RespDto> fb1146RespResultDto = dscms2CircpClientService.fb1146(fb1146ReqDto);
        log.info("房抵e点贷客户经理否决调用受托信息审核返回：" + fb1146RespResultDto);
        String fb1146Code = Optional.ofNullable(fb1146RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String fb1146Meesage = Optional.ofNullable(fb1146RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        log.info("房抵e点贷客户经理否决调用受托信息审核返回信息：" + fb1146Meesage);
        if (Objects.equals(fb1146Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取相关的值并解析
            return "success";
        } else {
            return fb1146Meesage;
        }
    }

    /***
     * 流程审批状态更新
     * 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (FdyddWhbxdApproval fdyddWhbxdApproval, String approveStatus){
        fdyddWhbxdApproval.setApproveStatus(approveStatus);
        fdyddWhbxdApprovalService.updateSelective(fdyddWhbxdApproval);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX10).equals(flowCode);
    }
}
