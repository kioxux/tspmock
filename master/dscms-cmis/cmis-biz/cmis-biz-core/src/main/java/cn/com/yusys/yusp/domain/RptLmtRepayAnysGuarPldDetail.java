/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPldDetail
 * @类描述: rpt_lmt_repay_anys_guar_pld_detail数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-02 10:17:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_pld_detail")
public class RptLmtRepayAnysGuarPldDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 流水号
     **/
    @Id
    @Column(name = "SERNO")
    private String serno;

    /**
     * 授信分项流水号
     **/
    @Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
    private String subSerno;

    /**
     * 抵押物名称
     **/
    @Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 80)
    private String pldimnMemo;


    /**
     * 抵质押品编号
     **/
    @Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
    private String guarNo;

    /**
     * 担保分类编号
     **/
    @Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 40)
    private String guarTypeCd;

    /**
     * 担保分类名称
     **/
    @Column(name = "GUAR_TYPE_CD_NAME", unique = false, nullable = true, length = 80)
    private String guarTypeCdName;

    /**
     * 押品所有人编号
     **/
    @Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 20)
    private String guarCusId;

    /**
     * 押品所有人名称
     **/
    @Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
    private String guarCusName;

    /**
     * 评估价值
     **/
    @Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalAmt;

    /**
     * 最高可抵质押金额
     **/
    @Column(name = "MAX_MORTAGAGE_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal maxMortagageAmt;

    /**
     * 对应融资金额
     **/
    @Column(name = "CORRE_FIN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal correFinAmt;

    /**
     * 抵押率
     **/
    @Column(name = "MORTAGAGE_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal mortagageRate;

    /**
     * 是否顺位抵押
     **/
    @Column(name = "IS_PLD_ORDER", unique = false, nullable = true, length = 5)
    private String isPldOrder;

    /**
     * 剩余价值
     **/
    @Column(name = "SURPLUS_VALUE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal surplusValue;

    /**
     * 抵押物地址
     **/
    @Column(name = "PLD_ADDRESS", unique = false, nullable = true, length = 100)
    private String pldAddress;

    /**
     * 与借款人关系
     **/
    @Column(name = "BORROW_REL", unique = false, nullable = true, length = 20)
    private String borrowRel;

    /**
     * 抵押物面积
     **/
    @Column(name = "PLD_SQU", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal pldSqu;

    /**
     * 抵押物是否出租
     **/
    @Column(name = "IS_LEASE", unique = false, nullable = true, length = 5)
    private String isLease;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param subSerno
     */
    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    /**
     * @return subSerno
     */
    public String getSubSerno() {
        return this.subSerno;
    }

    /**
     * @param guarNo
     */
    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    /**
     * @return guarNo
     */
    public String getGuarNo() {
        return this.guarNo;
    }

    /**
     * @param guarTypeCd
     */
    public void setGuarTypeCd(String guarTypeCd) {
        this.guarTypeCd = guarTypeCd;
    }

    /**
     * @return guarTypeCd
     */
    public String getGuarTypeCd() {
        return this.guarTypeCd;
    }

    /**
     * @param guarTypeCdName
     */
    public void setGuarTypeCdName(String guarTypeCdName) {
        this.guarTypeCdName = guarTypeCdName;
    }

    /**
     * @return guarTypeCdName
     */
    public String getGuarTypeCdName() {
        return this.guarTypeCdName;
    }

    /**
     * @param guarCusId
     */
    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId;
    }

    /**
     * @return guarCusId
     */
    public String getGuarCusId() {
        return this.guarCusId;
    }

    /**
     * @param guarCusName
     */
    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    /**
     * @return guarCusName
     */
    public String getGuarCusName() {
        return this.guarCusName;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return evalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    public BigDecimal getCorreFinAmt() {
        return correFinAmt;
    }

    public void setCorreFinAmt(BigDecimal correFinAmt) {
        this.correFinAmt = correFinAmt;
    }

    /**
     * @param mortagageRate
     */
    public void setMortagageRate(java.math.BigDecimal mortagageRate) {
        this.mortagageRate = mortagageRate;
    }

    /**
     * @return mortagageRate
     */
    public java.math.BigDecimal getMortagageRate() {
        return this.mortagageRate;
    }

    /**
     * @param isPldOrder
     */
    public void setIsPldOrder(String isPldOrder) {
        this.isPldOrder = isPldOrder;
    }

    /**
     * @return isPldOrder
     */
    public String getIsPldOrder() {
        return this.isPldOrder;
    }

    /**
     * @param surplusValue
     */
    public void setSurplusValue(java.math.BigDecimal surplusValue) {
        this.surplusValue = surplusValue;
    }

    /**
     * @return surplusValue
     */
    public java.math.BigDecimal getSurplusValue() {
        return this.surplusValue;
    }

    /**
     * @param pldAddress
     */
    public void setPldAddress(String pldAddress) {
        this.pldAddress = pldAddress;
    }

    /**
     * @return pldAddress
     */
    public String getPldAddress() {
        return this.pldAddress;
    }

    /**
     * @param borrowRel
     */
    public void setBorrowRel(String borrowRel) {
        this.borrowRel = borrowRel;
    }

    /**
     * @return borrowRel
     */
    public String getBorrowRel() {
        return this.borrowRel;
    }

    /**
     * @param pldSqu
     */
    public void setPldSqu(java.math.BigDecimal pldSqu) {
        this.pldSqu = pldSqu;
    }

    /**
     * @return pldSqu
     */
    public java.math.BigDecimal getPldSqu() {
        return this.pldSqu;
    }

    /**
     * @param isLease
     */
    public void setIsLease(String isLease) {
        this.isLease = isLease;
    }

    /**
     * @return isLease
     */
    public String getIsLease() {
        return this.isLease;
    }

    public String getPldimnMemo() {
        return pldimnMemo;
    }

    public void setPldimnMemo(String pldimnMemo) {
        this.pldimnMemo = pldimnMemo;
    }

    public BigDecimal getMaxMortagageAmt() {
        return maxMortagageAmt;
    }

    public void setMaxMortagageAmt(BigDecimal maxMortagageAmt) {
        this.maxMortagageAmt = maxMortagageAmt;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

}