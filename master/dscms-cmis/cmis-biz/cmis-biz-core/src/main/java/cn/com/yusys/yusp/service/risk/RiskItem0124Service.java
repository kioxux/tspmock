package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.LmtAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class RiskItem0124Service {

    @Autowired
    private LmtAppService lmtAppService;

    public RiskResultDto riskItem0124(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String cusId = (String)queryModel.getCondition().get("bizUserId");
        if (StringUtils.isBlank(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10003);
            return riskResultDto;
        }
        // 根据客户号查询授信申请
        boolean flg = lmtAppService.judgeIsExistOnWayApp(cusId);
        if (flg) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("存在在途授信申请");
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
