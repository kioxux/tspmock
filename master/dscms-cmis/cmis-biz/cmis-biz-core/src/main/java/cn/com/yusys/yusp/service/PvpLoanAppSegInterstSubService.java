/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.PvpLoanAppSegInterstSub;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppSegInterstSubMapper;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppSegInterstSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-21 10:25:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpLoanAppSegInterstSubService {

    @Autowired
    private PvpLoanAppSegInterstSubMapper pvpLoanAppSegInterstSubMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppSegInterstSubService.class);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpLoanAppSegInterstSub selectByPrimaryKey(String pkId) {
        return pvpLoanAppSegInterstSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpLoanAppSegInterstSub> selectAll(QueryModel model) {
        List<PvpLoanAppSegInterstSub> records = (List<PvpLoanAppSegInterstSub>) pvpLoanAppSegInterstSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpLoanAppSegInterstSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpLoanAppSegInterstSub record) {
        return pvpLoanAppSegInterstSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpLoanAppSegInterstSub record) {
        return pvpLoanAppSegInterstSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpLoanAppSegInterstSub record) {
        return pvpLoanAppSegInterstSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpLoanAppSegInterstSub record) {
        return pvpLoanAppSegInterstSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pvpLoanAppSegInterstSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpLoanAppSegInterstSubMapper.deleteByIds(ids);
    }

    /**
     *分段计息,新增
     * @param pvpLoanAppSegInterstSub
     * @return
     * @author:zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveSegInterstSub(PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpLoanAppSegInterstSub == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpLoanAppSegInterstSub.setInputId(userInfo.getLoginCode());
                pvpLoanAppSegInterstSub.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanAppSegInterstSub.setManagerId(userInfo.getLoginCode());
                pvpLoanAppSegInterstSub.setManagerBrId(userInfo.getOrg().getCode());
                pvpLoanAppSegInterstSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();

            //String serno = StringUtils.uuid(true);
            // sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ,seqMap);
            //pvpLoanAppSegInterstSub.setSerno(serno);
            String serno = pvpLoanAppSegInterstSub.getSerno();
            pvpLoanAppSegInterstSub.setPkId(StringUtils.uuid(true));
            int insertCount = pvpLoanAppSegInterstSubMapper.insertSelective(pvpLoanAppSegInterstSub);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("serno",serno);
            log.info("分段计息"+serno+"-保存成功！");
        }catch(YuspException e){
            log.error("分段计息新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("分段计息异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }
    /**
     *分段计息,修改
     * @param pvpLoanAppSegInterstSub
     * @return
     * @author:zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonUpdateSegInterstSubInfo(PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpLoanAppSegInterstSub == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpLoanAppSegInterstSub.setUpdId(userInfo.getLoginCode());
                pvpLoanAppSegInterstSub.setUpdBrId(userInfo.getOrg().getCode());
                pvpLoanAppSegInterstSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanAppSegInterstSub.setOprType("02");
            }
            Map seqMap = new HashMap();
            int updCount = pvpLoanAppSegInterstSubMapper.updateByPrimaryKeySelective(pvpLoanAppSegInterstSub);
            if(updCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
        }catch(YuspException e){
            log.error("分段计息新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("分段计息异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 贷款出账申请分段计息明细，新增
     * @param pvpLoanAppSegInterstSub
     * @author  zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map savePvpLoanAppSegInterstSub(PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try{
            if(pvpLoanAppSegInterstSub ==null){
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo==null){
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            }else{
                pvpLoanAppSegInterstSub.setInputId(userInfo.getLoginCode());
                pvpLoanAppSegInterstSub.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanAppSegInterstSub.setManagerId(userInfo.getLoginCode());
                pvpLoanAppSegInterstSub.setManagerBrId(userInfo.getOrg().getCode());
                pvpLoanAppSegInterstSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                //pvpLoanApp.setApprStatus("000");
                pvpLoanAppSegInterstSub.setOprType("01");
            }
            Map seqMap = new HashMap();
            String  pkId = StringUtils.uuid(true);
            pvpLoanAppSegInterstSub.setPkId(pkId);//主键为PK_ID
            int insertCount = pvpLoanAppSegInterstSubMapper.insertSelective(pvpLoanAppSegInterstSub);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("pkId",pkId);
            log.info("贷款出账申请分段计息明细"+pkId+"-保存成功！");
        }catch(YuspException e){
            log.error("贷款出账申请分段计息明细新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("贷款出账申请分段计息明细异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-05-05 15:35
     * @注释 分段计息分页查询
     */
    public List<PvpLoanAppSegInterstSub> querySegInterstSub(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //HashMap<String, String > queyParam = new HashMap<String, String>();
        //model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000);
        //model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
}