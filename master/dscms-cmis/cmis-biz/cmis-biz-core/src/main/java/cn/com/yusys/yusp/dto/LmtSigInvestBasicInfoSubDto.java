package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSub
 * @类描述: lmt_sig_invest_basic_info_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-23 21:25:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestBasicInfoSubDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 底层申请流水号 **/
	private String basicSerno;
	
	/** 底层客户编号 **/
	private String basicCusId;
	
	/** 底层客户名称 **/
	private String basicCusName;
	
	/** 底层客户大类 **/
	private String basicCusCatalog;
	
	/** 底层客户类型 **/
	private String basicCusType;
	
	/** 是否申报底层授信 **/
	private String isAppBasicLmt;
	
	/** 占用底层授信分项编号 **/
	private String useBasicLmtSubSerno;
	
	/** 占用底层授信分项品种编号 **/
	private String useBasicLmtItemNo;
	
	/** 占用底层授信分项品种名称 **/
	private String useBasicLmtItemName;
	
	/** 底层资产类型 **/
	private String basicAssetType;
	
	/** 本项底层资产余额 **/
	private java.math.BigDecimal basicAssetBalanceAmt;
	
	/** 底层资产名称 **/
	private String basicAssetName;
	
	/** 底层资产到期日 **/
	private String basicAssetEndDate;
	
	/** 底层资产剩余期限 **/
	private Integer basicAssetBalanceTerm;
	
	/** 原底层关联台账号 **/
	private String origiBasicAccNo;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param basicSerno
	 */
	public void setBasicSerno(String basicSerno) {
		this.basicSerno = basicSerno == null ? null : basicSerno.trim();
	}
	
    /**
     * @return BasicSerno
     */	
	public String getBasicSerno() {
		return this.basicSerno;
	}
	
	/**
	 * @param basicCusId
	 */
	public void setBasicCusId(String basicCusId) {
		this.basicCusId = basicCusId == null ? null : basicCusId.trim();
	}
	
    /**
     * @return BasicCusId
     */	
	public String getBasicCusId() {
		return this.basicCusId;
	}
	
	/**
	 * @param basicCusName
	 */
	public void setBasicCusName(String basicCusName) {
		this.basicCusName = basicCusName == null ? null : basicCusName.trim();
	}
	
    /**
     * @return BasicCusName
     */	
	public String getBasicCusName() {
		return this.basicCusName;
	}
	
	/**
	 * @param basicCusCatalog
	 */
	public void setBasicCusCatalog(String basicCusCatalog) {
		this.basicCusCatalog = basicCusCatalog == null ? null : basicCusCatalog.trim();
	}
	
    /**
     * @return BasicCusCatalog
     */	
	public String getBasicCusCatalog() {
		return this.basicCusCatalog;
	}
	
	/**
	 * @param basicCusType
	 */
	public void setBasicCusType(String basicCusType) {
		this.basicCusType = basicCusType == null ? null : basicCusType.trim();
	}
	
    /**
     * @return BasicCusType
     */	
	public String getBasicCusType() {
		return this.basicCusType;
	}
	
	/**
	 * @param isAppBasicLmt
	 */
	public void setIsAppBasicLmt(String isAppBasicLmt) {
		this.isAppBasicLmt = isAppBasicLmt == null ? null : isAppBasicLmt.trim();
	}
	
    /**
     * @return IsAppBasicLmt
     */	
	public String getIsAppBasicLmt() {
		return this.isAppBasicLmt;
	}
	
	/**
	 * @param useBasicLmtSubSerno
	 */
	public void setUseBasicLmtSubSerno(String useBasicLmtSubSerno) {
		this.useBasicLmtSubSerno = useBasicLmtSubSerno == null ? null : useBasicLmtSubSerno.trim();
	}
	
    /**
     * @return UseBasicLmtSubSerno
     */	
	public String getUseBasicLmtSubSerno() {
		return this.useBasicLmtSubSerno;
	}
	
	/**
	 * @param useBasicLmtItemNo
	 */
	public void setUseBasicLmtItemNo(String useBasicLmtItemNo) {
		this.useBasicLmtItemNo = useBasicLmtItemNo == null ? null : useBasicLmtItemNo.trim();
	}
	
    /**
     * @return UseBasicLmtItemNo
     */	
	public String getUseBasicLmtItemNo() {
		return this.useBasicLmtItemNo;
	}
	
	/**
	 * @param useBasicLmtItemName
	 */
	public void setUseBasicLmtItemName(String useBasicLmtItemName) {
		this.useBasicLmtItemName = useBasicLmtItemName == null ? null : useBasicLmtItemName.trim();
	}
	
    /**
     * @return UseBasicLmtItemName
     */	
	public String getUseBasicLmtItemName() {
		return this.useBasicLmtItemName;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType == null ? null : basicAssetType.trim();
	}
	
    /**
     * @return BasicAssetType
     */	
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param basicAssetBalanceAmt
	 */
	public void setBasicAssetBalanceAmt(java.math.BigDecimal basicAssetBalanceAmt) {
		this.basicAssetBalanceAmt = basicAssetBalanceAmt;
	}
	
    /**
     * @return BasicAssetBalanceAmt
     */	
	public java.math.BigDecimal getBasicAssetBalanceAmt() {
		return this.basicAssetBalanceAmt;
	}
	
	/**
	 * @param basicAssetName
	 */
	public void setBasicAssetName(String basicAssetName) {
		this.basicAssetName = basicAssetName == null ? null : basicAssetName.trim();
	}
	
    /**
     * @return BasicAssetName
     */	
	public String getBasicAssetName() {
		return this.basicAssetName;
	}
	
	/**
	 * @param basicAssetEndDate
	 */
	public void setBasicAssetEndDate(String basicAssetEndDate) {
		this.basicAssetEndDate = basicAssetEndDate == null ? null : basicAssetEndDate.trim();
	}
	
    /**
     * @return BasicAssetEndDate
     */	
	public String getBasicAssetEndDate() {
		return this.basicAssetEndDate;
	}
	
	/**
	 * @param basicAssetBalanceTerm
	 */
	public void setBasicAssetBalanceTerm(Integer basicAssetBalanceTerm) {
		this.basicAssetBalanceTerm = basicAssetBalanceTerm;
	}
	
    /**
     * @return BasicAssetBalanceTerm
     */	
	public Integer getBasicAssetBalanceTerm() {
		return this.basicAssetBalanceTerm;
	}
	
	/**
	 * @param origiBasicAccNo
	 */
	public void setOrigiBasicAccNo(String origiBasicAccNo) {
		this.origiBasicAccNo = origiBasicAccNo == null ? null : origiBasicAccNo.trim();
	}
	
    /**
     * @return OrigiBasicAccNo
     */	
	public String getOrigiBasicAccNo() {
		return this.origiBasicAccNo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}