/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRediDetail
 * @类描述: lmt_redi_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-31 21:00:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_redi_detail")
public class LmtRediDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = false, length = 40)
	private String lmtSerno;
	
	/** 上期申请情况及总行审批意见 **/
	@Column(name = "LAST_LMT_CONDITION", unique = false, nullable = true, length = 65535)
	private String lastLmtCondition;
	
	/** 本次授信复议内容 **/
	@Column(name = "LMT_REDI_CONTENT", unique = false, nullable = true, length = 65535)
	private String lmtRediContent;
	
	/** 发放该笔融资原因 **/
	@Column(name = "KEEP_FINA_REASON", unique = false, nullable = true, length = 65535)
	private String keepFinaReason;
	
	/** 风险防范措施 **/
	@Column(name = "RISK_GUARD_MEASU", unique = false, nullable = true, length = 65535)
	private String riskGuardMeasu;
	
	/** 其他理由 **/
	@Column(name = "OTHER_RESN", unique = false, nullable = true, length = 4000)
	private String otherResn;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param lastLmtCondition
	 */
	public void setLastLmtCondition(String lastLmtCondition) {
		this.lastLmtCondition = lastLmtCondition;
	}
	
    /**
     * @return lastLmtCondition
     */
	public String getLastLmtCondition() {
		return this.lastLmtCondition;
	}
	
	/**
	 * @param lmtRediContent
	 */
	public void setLmtRediContent(String lmtRediContent) {
		this.lmtRediContent = lmtRediContent;
	}
	
    /**
     * @return lmtRediContent
     */
	public String getLmtRediContent() {
		return this.lmtRediContent;
	}
	
	/**
	 * @param keepFinaReason
	 */
	public void setKeepFinaReason(String keepFinaReason) {
		this.keepFinaReason = keepFinaReason;
	}
	
    /**
     * @return keepFinaReason
     */
	public String getKeepFinaReason() {
		return this.keepFinaReason;
	}
	
	/**
	 * @param riskGuardMeasu
	 */
	public void setRiskGuardMeasu(String riskGuardMeasu) {
		this.riskGuardMeasu = riskGuardMeasu;
	}
	
    /**
     * @return riskGuardMeasu
     */
	public String getRiskGuardMeasu() {
		return this.riskGuardMeasu;
	}
	
	/**
	 * @param otherResn
	 */
	public void setOtherResn(String otherResn) {
		this.otherResn = otherResn;
	}
	
    /**
     * @return otherResn
     */
	public String getOtherResn() {
		return this.otherResn;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}