/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContRelWarrant
 * @类描述: guar_cont_rel_warrant数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-13 15:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_cont_rel_warrant")
public class GuarContRelWarrant extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 核心担保编号 **/
	@Column(name = "CORE_GUARANTY_NO", unique = false, nullable = true, length = 40)
	private String coreGuarantyNo;
	
	/** 权证编号 **/
	@Column(name = "WARRANT_NO", unique = false, nullable = true, length = 200)
	private String warrantNo;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 押品名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;
	
	/** 押品类型 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 押品所有人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 押品所有人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}
	
    /**
     * @return coreGuarantyNo
     */
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo;
	}
	
    /**
     * @return warrantNo
     */
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}