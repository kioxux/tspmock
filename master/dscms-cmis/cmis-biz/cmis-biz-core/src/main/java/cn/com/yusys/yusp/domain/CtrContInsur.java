/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContInsur
 * @类描述: ctr_cont_insur数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-05 16:30:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_cont_insur")
public class CtrContInsur extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 是否投保 STD_ZB_IS_INSUR **/
	@Column(name = "IS_INSUR", unique = false, nullable = true, length = 5)
	private String isInsur;
	
	/** 保险种类 STD_ZB_INSUR_TYPE **/
	@Column(name = "INSUR_TYPE", unique = false, nullable = true, length = 5)
	private String insurType;
	
	/** 保险单编号 **/
	@Column(name = "INSUR_NO", unique = false, nullable = true, length = 32)
	private String insurNo;
	
	/** 保险起始日期 **/
	@Column(name = "INSUR_BEG_DATE", unique = false, nullable = true, length = 10)
	private String insurBegDate;
	
	/** 保险截止日期 **/
	@Column(name = "INSUR_END_DATE", unique = false, nullable = true, length = 10)
	private String insurEndDate;
	
	/** 保险单签订日期 **/
	@Column(name = "INSUR_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String insurSignDate;
	
	/** 保险机构名称 **/
	@Column(name = "INSUR_ORG_NAME", unique = false, nullable = true, length = 100)
	private String insurOrgName;
	
	/** 保险金额（元） **/
	@Column(name = "INSUR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal insurAmt;
	
	/** 保险费（元） **/
	@Column(name = "INSUR_FEE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal insurFee;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param isInsur
	 */
	public void setIsInsur(String isInsur) {
		this.isInsur = isInsur;
	}
	
    /**
     * @return isInsur
     */
	public String getIsInsur() {
		return this.isInsur;
	}
	
	/**
	 * @param insurType
	 */
	public void setInsurType(String insurType) {
		this.insurType = insurType;
	}
	
    /**
     * @return insurType
     */
	public String getInsurType() {
		return this.insurType;
	}
	
	/**
	 * @param insurNo
	 */
	public void setInsurNo(String insurNo) {
		this.insurNo = insurNo;
	}
	
    /**
     * @return insurNo
     */
	public String getInsurNo() {
		return this.insurNo;
	}
	
	/**
	 * @param insurBegDate
	 */
	public void setInsurBegDate(String insurBegDate) {
		this.insurBegDate = insurBegDate;
	}
	
    /**
     * @return insurBegDate
     */
	public String getInsurBegDate() {
		return this.insurBegDate;
	}
	
	/**
	 * @param insurEndDate
	 */
	public void setInsurEndDate(String insurEndDate) {
		this.insurEndDate = insurEndDate;
	}
	
    /**
     * @return insurEndDate
     */
	public String getInsurEndDate() {
		return this.insurEndDate;
	}
	
	/**
	 * @param insurSignDate
	 */
	public void setInsurSignDate(String insurSignDate) {
		this.insurSignDate = insurSignDate;
	}
	
    /**
     * @return insurSignDate
     */
	public String getInsurSignDate() {
		return this.insurSignDate;
	}
	
	/**
	 * @param insurOrgName
	 */
	public void setInsurOrgName(String insurOrgName) {
		this.insurOrgName = insurOrgName;
	}
	
    /**
     * @return insurOrgName
     */
	public String getInsurOrgName() {
		return this.insurOrgName;
	}
	
	/**
	 * @param insurAmt
	 */
	public void setInsurAmt(java.math.BigDecimal insurAmt) {
		this.insurAmt = insurAmt;
	}
	
    /**
     * @return insurAmt
     */
	public java.math.BigDecimal getInsurAmt() {
		return this.insurAmt;
	}
	
	/**
	 * @param insurFee
	 */
	public void setInsurFee(java.math.BigDecimal insurFee) {
		this.insurFee = insurFee;
	}
	
    /**
     * @return insurFee
     */
	public java.math.BigDecimal getInsurFee() {
		return this.insurFee;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}