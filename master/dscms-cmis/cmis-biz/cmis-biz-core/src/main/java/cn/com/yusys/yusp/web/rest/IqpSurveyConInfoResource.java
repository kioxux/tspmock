/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpSurveyConInfo;
import cn.com.yusys.yusp.service.IqpSurveyConInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpSurveyConInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-05-07 10:27:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "零售授信调查结论")
@RequestMapping("/api/iqpsurveyconinfo")
public class IqpSurveyConInfoResource {
    @Autowired
    private IqpSurveyConInfoService iqpSurveyConInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpSurveyConInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpSurveyConInfo> list = iqpSurveyConInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpSurveyConInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpSurveyConInfo>> index(QueryModel queryModel) {
        List<IqpSurveyConInfo> list = iqpSurveyConInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpSurveyConInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpSurveyConInfo> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpSurveyConInfo iqpSurveyConInfo = iqpSurveyConInfoService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpSurveyConInfo>(iqpSurveyConInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpSurveyConInfo> create(@RequestBody IqpSurveyConInfo iqpSurveyConInfo) throws URISyntaxException {
        iqpSurveyConInfoService.insert(iqpSurveyConInfo);
        return new ResultDto<IqpSurveyConInfo>(iqpSurveyConInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpSurveyConInfo iqpSurveyConInfo) throws URISyntaxException {
        int result = iqpSurveyConInfoService.update(iqpSurveyConInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpSurveyConInfoService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpSurveyConInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:根据申请流水号查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据申请流水号查询单个对象")
    @PostMapping("/selectByIqpSerno")
    protected ResultDto<IqpSurveyConInfo> selectByIqpSerno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        IqpSurveyConInfo iqpSurveyConInfoRespDto = iqpSurveyConInfoService.selectByIqpSerno(lmtSurveyReportDto);
        return new ResultDto<IqpSurveyConInfo>(iqpSurveyConInfoRespDto);
    }

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<IqpSurveyConInfo> selectByPrimaryKey(@RequestBody IqpSurveyConInfo iqpSurveyConInfo) {
        return new ResultDto<IqpSurveyConInfo>(iqpSurveyConInfoService.selectByPrimaryKey(iqpSurveyConInfo.getIqpSerno()));
    }

}
