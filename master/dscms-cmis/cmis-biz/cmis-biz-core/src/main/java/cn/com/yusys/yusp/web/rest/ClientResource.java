/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;


import cn.com.yusys.yusp.dto.BizGuarExchangeDto;
import cn.com.yusys.yusp.util.BizUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @类描述: #接口处理类
 * @功能描述:

 */
@RestController
@RequestMapping("/api/client")
public class ClientResource {
    private final static Logger log = LoggerFactory.getLogger(ClientResource.class);
    @Autowired
    private BizUtils bizUtils;
    
    /**
     * 获取押品所在业务阶段
     * @param
     * @return
     */
    @PostMapping ("/queryBizServiceStates")
    public BizGuarExchangeDto queryBizServiceStates(@RequestBody BizGuarExchangeDto bizGuarExchangeDto){
        BizGuarExchangeDto  bizGuarExchangeDtos = bizUtils.getGuarBusistate(bizGuarExchangeDto);
        return bizGuarExchangeDtos;
    }
}
