package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetConInfo
 * @类描述: iqp_dis_asset_con_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-06 16:37:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDisAssetConInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 家庭合计月收入 **/
	private java.math.BigDecimal fearnMearn;
	
	/** 该笔贷款月支出与家庭月收入比例 **/
	private java.math.BigDecimal incomePerc1;
	
	/** 所有消费贷款月支出与家庭月收入比 **/
	private java.math.BigDecimal incomePerc2;
	
	/** 还款能力 **/
	private String repayAbi;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param fearnMearn
	 */
	public void setFearnMearn(java.math.BigDecimal fearnMearn) {
		this.fearnMearn = fearnMearn;
	}
	
    /**
     * @return FearnMearn
     */	
	public java.math.BigDecimal getFearnMearn() {
		return this.fearnMearn;
	}
	
	/**
	 * @param incomePerc1
	 */
	public void setIncomePerc1(java.math.BigDecimal incomePerc1) {
		this.incomePerc1 = incomePerc1;
	}
	
    /**
     * @return IncomePerc1
     */	
	public java.math.BigDecimal getIncomePerc1() {
		return this.incomePerc1;
	}
	
	/**
	 * @param incomePerc2
	 */
	public void setIncomePerc2(java.math.BigDecimal incomePerc2) {
		this.incomePerc2 = incomePerc2;
	}
	
    /**
     * @return IncomePerc2
     */	
	public java.math.BigDecimal getIncomePerc2() {
		return this.incomePerc2;
	}
	
	/**
	 * @param repayAbi
	 */
	public void setRepayAbi(String repayAbi) {
		this.repayAbi = repayAbi == null ? null : repayAbi.trim();
	}
	
    /**
     * @return RepayAbi
     */	
	public String getRepayAbi() {
		return this.repayAbi;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}