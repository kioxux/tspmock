package cn.com.yusys.yusp.web.server.xdtz0029;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0029.req.Xdtz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.resp.Xdtz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0029.Xdtz0029Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:查询指定票号在信贷台账中是否已贴现
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0029:查询指定票号在信贷台账中是否已贴现")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0029Resource.class);

    @Autowired
    private Xdtz0029Service xdtz0029Service;

    /**
     * 交易码：xdtz0029
     * 交易描述：查询指定票号在信贷台账中是否已贴现
     *
     * @param xdtz0029DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询指定票号在信贷台账中是否已贴现")
    @PostMapping("/xdtz0029")
    protected @ResponseBody
    ResultDto<Xdtz0029DataRespDto> xdtz0029(@Validated @RequestBody Xdtz0029DataReqDto xdtz0029DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataReqDto));
        Xdtz0029DataRespDto xdtz0029DataRespDto = new Xdtz0029DataRespDto();// 响应Dto:查询指定票号在信贷台账中是否已贴现
        ResultDto<Xdtz0029DataRespDto> xdtz0029DataResultDto = new ResultDto<>();
        try {
			String porderNo = xdtz0029DataReqDto.getPorderNo();//汇票号码
            // 从xdtz0029DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, porderNo);
            xdtz0029DataRespDto = Optional.ofNullable(xdtz0029Service.getPorderByPorderNo(porderNo))
                    .orElse(new Xdtz0029DataRespDto(StringUtils.EMPTY, new BigDecimal(CmisBizConstants.NUM_ZERO), StringUtils.EMPTY));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataRespDto));
            // 封装xdtz0029DataResultDto中正确的返回码和返回信息
            xdtz0029DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0029DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, e.getMessage());
            // 封装xdtz0029DataResultDto中异常返回码和返回信息
            xdtz0029DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0029DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0029DataRespDto到xdtz0029DataResultDto中
        xdtz0029DataResultDto.setData(xdtz0029DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataRespDto));
        return xdtz0029DataResultDto;
    }
}
