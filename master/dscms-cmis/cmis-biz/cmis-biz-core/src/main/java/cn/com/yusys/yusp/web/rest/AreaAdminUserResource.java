/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.AreaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.service.AreaAdminUserService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaAdminUserResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-27 15:34:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/areaadminuser")
public class AreaAdminUserResource {
    @Autowired
    private AreaAdminUserService areaAdminUserService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AreaAdminUser>> query() {
        QueryModel queryModel = new QueryModel();
        List<AreaAdminUser> list = areaAdminUserService.selectAll(queryModel);
        return new ResultDto<List<AreaAdminUser>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AreaAdminUser>> index(QueryModel queryModel) {
        List<AreaAdminUser> list = areaAdminUserService.selectByModel(queryModel);
        return new ResultDto<List<AreaAdminUser>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{userNo}")
    protected ResultDto<AreaAdminUser> show(@PathVariable("userNo") String userNo) {
        AreaAdminUser areaAdminUser = areaAdminUserService.selectByPrimaryKey(userNo);
        return new ResultDto<AreaAdminUser>(areaAdminUser);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AreaAdminUser> create(@RequestBody AreaAdminUser areaAdminUser) throws URISyntaxException {
        areaAdminUserService.insert(areaAdminUser);
        return new ResultDto<AreaAdminUser>(areaAdminUser);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AreaAdminUser areaAdminUser) throws URISyntaxException {
        int result = areaAdminUserService.update(areaAdminUser);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{userNo}")
    protected ResultDto<Integer> delete(@PathVariable("userNo") String userNo) {
        int result = areaAdminUserService.deleteByPrimaryKey(userNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = areaAdminUserService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
    * @author zlf
    * @date 2021/5/27 15:39
    * @version 1.0.0
    * @desc  从表维护
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/save")
    protected ResultDto<AreaAdminUser> save(@RequestBody AreaAdminUser areaAdminUser) {
        areaAdminUserService.save(areaAdminUser);
        return new ResultDto<AreaAdminUser>(areaAdminUser);
    }


    @PostMapping("/showone")
    protected ResultDto<AreaAdminUser> showOne(@RequestBody AreaAdminUser areaAdminUser) {
        AreaAdminUser areaAdminUsers = areaAdminUserService.selectByPrimaryKey(areaAdminUser.getUserNo());
        return new ResultDto<AreaAdminUser>(areaAdminUsers);
    }
    /**
    * @author zlf
    * @date 2021/6/9 16:31
    * @version 1.0.0
    * @desc     删除单条
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/deleteone")
    protected ResultDto<Integer> deleteOne(@RequestBody AreaAdminUser areaAdminUser) {
        int result = areaAdminUserService.deleteOne(areaAdminUser.getUserNo());
        return new ResultDto<Integer>(result);
    }

}
