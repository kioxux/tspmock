package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstMclWhbxd
 * @类描述: cus_lst_mcl_whbxd数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-23 15:36:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusLstMclWhbxdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	/** 借据编号 **/
	private String billNo;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 联系方式 **/
	private String linkMode;
	
	/** 借据金额 **/
	private java.math.BigDecimal billAmt;
	
	/** 借据余额 **/
	private java.math.BigDecimal billBal;
	
	/** 借据起始日 **/
	private String billStartDate;
	
	/** 借据到期日 **/
	private String billEndDate;
	
	/** 合同到期日 **/
	private String contEndDate;
	
	/** 婚姻状况 **/
	private String marStatus;
	
	/** 配偶姓名 **/
	private String spouseName;
	
	/** 配偶身份证 **/
	private String spouseCertCode;
	
	/** 配偶电话 **/
	private String spousePhone;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 名单归属 **/
	private String listBelg;
	
	/** 办理状态 **/
	private String applyStatus;
	
	/** 状态 **/
	private String status;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkMode
	 */
	public void setLinkMode(String linkMode) {
		this.linkMode = linkMode == null ? null : linkMode.trim();
	}
	
    /**
     * @return LinkMode
     */	
	public String getLinkMode() {
		return this.linkMode;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return BillAmt
     */	
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBal
	 */
	public void setBillBal(java.math.BigDecimal billBal) {
		this.billBal = billBal;
	}
	
    /**
     * @return BillBal
     */	
	public java.math.BigDecimal getBillBal() {
		return this.billBal;
	}
	
	/**
	 * @param billStartDate
	 */
	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate == null ? null : billStartDate.trim();
	}
	
    /**
     * @return BillStartDate
     */	
	public String getBillStartDate() {
		return this.billStartDate;
	}
	
	/**
	 * @param billEndDate
	 */
	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate == null ? null : billEndDate.trim();
	}
	
    /**
     * @return BillEndDate
     */	
	public String getBillEndDate() {
		return this.billEndDate;
	}
	
	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate == null ? null : contEndDate.trim();
	}
	
    /**
     * @return ContEndDate
     */	
	public String getContEndDate() {
		return this.contEndDate;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus == null ? null : marStatus.trim();
	}
	
    /**
     * @return MarStatus
     */	
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName == null ? null : spouseName.trim();
	}
	
    /**
     * @return SpouseName
     */	
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode == null ? null : spouseCertCode.trim();
	}
	
    /**
     * @return SpouseCertCode
     */	
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone == null ? null : spousePhone.trim();
	}
	
    /**
     * @return SpousePhone
     */	
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param listBelg
	 */
	public void setListBelg(String listBelg) {
		this.listBelg = listBelg == null ? null : listBelg.trim();
	}
	
    /**
     * @return ListBelg
     */	
	public String getListBelg() {
		return this.listBelg;
	}
	
	/**
	 * @param applyStatus
	 */
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus == null ? null : applyStatus.trim();
	}
	
    /**
     * @return ApplyStatus
     */	
	public String getApplyStatus() {
		return this.applyStatus;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}