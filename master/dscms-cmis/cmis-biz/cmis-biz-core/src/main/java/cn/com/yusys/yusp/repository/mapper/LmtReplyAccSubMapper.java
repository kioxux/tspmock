/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyAccSub;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplyAccSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtReplyAccSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtReplyAccSub> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtReplyAccSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtReplyAccSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtReplyAccSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtReplyAccSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);




    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: queryLmtReplyAccSubByParams
     * @方法描述: 根据传入参数获取对应授信分项台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReplyAccSub> queryLmtReplyAccSubByParams(Map queryMap);

    /**
     * @方法名称: queryLmtReplyAccSubByParams
     * @方法描述: 根据传入参数获取对应授信分项台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReplyAccSub> selectBySubserno(Map queryMap);

    /**
     * @方法名称：insertlmtReplyAccSubList
     * @方法描述：
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-21 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    int insertlmtReplyAccSubList(@Param("list")List<LmtReplyAccSub> lmtReplyAccSubsList);

    /**
     * @方法名称：selectBySerno
     * @方法描述：根据 授信流水号查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-05-27 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtReplyAccSub selectBySerno(@Param("serno") String serno);

    /**
     * @方法名称：updateLmtReplyAccSub
     * @方法描述：根据授信流水号逻辑删除
     * @参数与返回说明：
     * @算法描述：
     * @创建人：xs
     * @创建时间：2021-05-31 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    int updateLmtReplyAccSub(@Param("serno") String serno);
    /**
     * @函数名称:deleteByAccSubNo
     * @函数描述:根据授信协议分项流水号关联删除授信协议分项
     * @参数与返回说明:accSubNo 授信协议分项流水号
     * @算法描述:
     */
    int deleteByAccSubNo(String accSubNo);

    /**
     * 根据授权额度编号获取省心快贷批复额度
     * @param lmt_acc_no
     * @return
     */

    Map selectLmtReplyAccBylmtaccNo(@Param("lmt_acc_no") String lmt_acc_no);

    /**
     * @方法名称：getLmtReplyAccSubByAccNo
     * @方法描述：根据授信台账号查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-06-28 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtReplyAccSub getLmtReplyAccSubByAccSubNo(Map map);

    /**
     * @方法名称：selectByAccSubNo
     * @方法描述：根据授信台账号查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-06-28 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtReplyAccSub selectByAccSubNo(@Param("accSubNo") String accSubNo);

    /**
     * @方法名称: getLmtReplyAccSubByAccNo
     * @方法描述: 根据分项额度号获取房抵e抵贷授信批复台账分项信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：zl
     */
    List<LmtReplyAccSub> getfdyddLmtReplyAccSubByAccSubNo(QueryModel model);

    /**
     * @方法名称: selectLmtReplyAccSubDataByGrpAccNo
     * @方法描述: 根据集团台账流水号查询成员客户授信台账分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     */
    List<LmtReplyAccSub> selectLmtReplyAccSubDataByGrpAccNo(@Param("grpAccNo") String grpAccNo);

    /**
     * 根据批复流水号查询批复分项流水号
     * @param replySerno
     * @return
     */
    List<String> getReplySubSernosByReplySerno(@Param("replySerno") String replySerno);
}