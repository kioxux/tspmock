/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyApp
 * @类描述: coop_reply_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_reply_app")
public class CoopReplyApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = false, length = 40)
	private String replyNo;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方类型 **/
	@Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 10)
	private String partnerType;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;
	
	/** 合作类型 **/
	@Column(name = "COOP_TYPE", unique = false, nullable = true, length = 10)
	private String coopType;
	
	/** 是否全行适用 **/
	@Column(name = "IS_WHOLE_BANK_SUIT", unique = false, nullable = true, length = 10)
	private String isWholeBankSuit;
	
	/** 总合作额度（元） **/
	@Column(name = "TOTL_COOP_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totlCoopLmtAmt;
	
	/** 一般担保额度（元） **/
	@Column(name = "COMMON_GRT_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commonGrtLmtAmt;
	
	/** 合作期限(月) **/
	@Column(name = "COOP_TERM", unique = false, nullable = true, length = 10)
	private Integer coopTerm;
	
	/** 单户合作限度（元） **/
	@Column(name = "SINGLE_COOP_QUOTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleCoopQuota;
	
	/** 单笔业务合作限额（元） **/
	@Column(name = "SIG_BUSI_COOP_QUOTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigBusiCoopQuota;
	
	/** 合作起始日 **/
	@Column(name = "COOP_START_DATE", unique = false, nullable = true, length = 20)
	private String coopStartDate;
	
	/** 合作到期日 **/
	@Column(name = "COOP_END_DATE", unique = false, nullable = true, length = 20)
	private String coopEndDate;
	
	/** 是否白名单控制 **/
	@Column(name = "IS_WHITE_LIST_CTRL", unique = false, nullable = true, length = 10)
	private String isWhiteListCtrl;
	
	/** 代偿宽限期(天) **/
	@Column(name = "SUBPAY_GRAPER", unique = false, nullable = true, length = 10)
	private Integer subpayGraper;
	
	/** 代偿比例 **/
	@Column(name = "SUBPAY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subpayPerc;
	
	/** 对外担保放大倍数 **/
	@Column(name = "OUTGUAR_MULTIPLE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outguarMultiple;
	
	/** 其他相关说明 **/
	@Column(name = "OTHER_CORRE_DESC", unique = false, nullable = true, length = 500)
	private String otherCorreDesc;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金账户最低金额(元) **/
	@Column(name = "BAIL_ACC_LOW_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAccLowAmt;
	
	/** 单笔最低缴存金额(元) **/
	@Column(name = "SIG_LOW_DEPOSIT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金透支上限(元) **/
	@Column(name = "BAIL_OVERDRAFT_MAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailOverdraftMax;
	
	/** 保证金缴存方式 **/
	@Column(name = "BAIL_DEPOSIT_MODE", unique = false, nullable = true, length = 10)
	private String bailDepositMode;
	
	/** 保证金账号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 60)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB_SEQ", unique = false, nullable = true, length = 60)
	private String bailAccNoSubSeq;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 10)
	private String indgtResult;
	
	/** 调查意见 **/
	@Column(name = "INDGT_ADVICE", unique = false, nullable = true, length = 500)
	private String indgtAdvice;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="managerIdName")
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="managerBrIdName" )
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	
    /**
     * @return partnerType
     */
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType;
	}
	
    /**
     * @return coopType
     */
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param isWholeBankSuit
	 */
	public void setIsWholeBankSuit(String isWholeBankSuit) {
		this.isWholeBankSuit = isWholeBankSuit;
	}
	
    /**
     * @return isWholeBankSuit
     */
	public String getIsWholeBankSuit() {
		return this.isWholeBankSuit;
	}
	
	/**
	 * @param totlCoopLmtAmt
	 */
	public void setTotlCoopLmtAmt(java.math.BigDecimal totlCoopLmtAmt) {
		this.totlCoopLmtAmt = totlCoopLmtAmt;
	}
	
    /**
     * @return totlCoopLmtAmt
     */
	public java.math.BigDecimal getTotlCoopLmtAmt() {
		return this.totlCoopLmtAmt;
	}
	
	/**
	 * @param commonGrtLmtAmt
	 */
	public void setCommonGrtLmtAmt(java.math.BigDecimal commonGrtLmtAmt) {
		this.commonGrtLmtAmt = commonGrtLmtAmt;
	}
	
    /**
     * @return commonGrtLmtAmt
     */
	public java.math.BigDecimal getCommonGrtLmtAmt() {
		return this.commonGrtLmtAmt;
	}
	
	/**
	 * @param coopTerm
	 */
	public void setCoopTerm(Integer coopTerm) {
		this.coopTerm = coopTerm;
	}
	
    /**
     * @return coopTerm
     */
	public Integer getCoopTerm() {
		return this.coopTerm;
	}
	
	/**
	 * @param singleCoopQuota
	 */
	public void setSingleCoopQuota(java.math.BigDecimal singleCoopQuota) {
		this.singleCoopQuota = singleCoopQuota;
	}
	
    /**
     * @return singleCoopQuota
     */
	public java.math.BigDecimal getSingleCoopQuota() {
		return this.singleCoopQuota;
	}
	
	/**
	 * @param sigBusiCoopQuota
	 */
	public void setSigBusiCoopQuota(java.math.BigDecimal sigBusiCoopQuota) {
		this.sigBusiCoopQuota = sigBusiCoopQuota;
	}
	
    /**
     * @return sigBusiCoopQuota
     */
	public java.math.BigDecimal getSigBusiCoopQuota() {
		return this.sigBusiCoopQuota;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate;
	}
	
    /**
     * @return coopStartDate
     */
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate;
	}
	
    /**
     * @return coopEndDate
     */
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param isWhiteListCtrl
	 */
	public void setIsWhiteListCtrl(String isWhiteListCtrl) {
		this.isWhiteListCtrl = isWhiteListCtrl;
	}
	
    /**
     * @return isWhiteListCtrl
     */
	public String getIsWhiteListCtrl() {
		return this.isWhiteListCtrl;
	}
	
	/**
	 * @param subpayGraper
	 */
	public void setSubpayGraper(Integer subpayGraper) {
		this.subpayGraper = subpayGraper;
	}
	
    /**
     * @return subpayGraper
     */
	public Integer getSubpayGraper() {
		return this.subpayGraper;
	}
	
	/**
	 * @param subpayPerc
	 */
	public void setSubpayPerc(java.math.BigDecimal subpayPerc) {
		this.subpayPerc = subpayPerc;
	}
	
    /**
     * @return subpayPerc
     */
	public java.math.BigDecimal getSubpayPerc() {
		return this.subpayPerc;
	}
	
	/**
	 * @param outguarMultiple
	 */
	public void setOutguarMultiple(java.math.BigDecimal outguarMultiple) {
		this.outguarMultiple = outguarMultiple;
	}
	
    /**
     * @return outguarMultiple
     */
	public java.math.BigDecimal getOutguarMultiple() {
		return this.outguarMultiple;
	}
	
	/**
	 * @param otherCorreDesc
	 */
	public void setOtherCorreDesc(String otherCorreDesc) {
		this.otherCorreDesc = otherCorreDesc;
	}
	
    /**
     * @return otherCorreDesc
     */
	public String getOtherCorreDesc() {
		return this.otherCorreDesc;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAccLowAmt
	 */
	public void setBailAccLowAmt(java.math.BigDecimal bailAccLowAmt) {
		this.bailAccLowAmt = bailAccLowAmt;
	}
	
    /**
     * @return bailAccLowAmt
     */
	public java.math.BigDecimal getBailAccLowAmt() {
		return this.bailAccLowAmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return sigLowDepositAmt
     */
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailOverdraftMax
	 */
	public void setBailOverdraftMax(java.math.BigDecimal bailOverdraftMax) {
		this.bailOverdraftMax = bailOverdraftMax;
	}
	
    /**
     * @return bailOverdraftMax
     */
	public java.math.BigDecimal getBailOverdraftMax() {
		return this.bailOverdraftMax;
	}
	
	/**
	 * @param bailDepositMode
	 */
	public void setBailDepositMode(String bailDepositMode) {
		this.bailDepositMode = bailDepositMode;
	}
	
    /**
     * @return bailDepositMode
     */
	public String getBailDepositMode() {
		return this.bailDepositMode;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSubSeq
	 */
	public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
		this.bailAccNoSubSeq = bailAccNoSubSeq;
	}
	
    /**
     * @return bailAccNoSubSeq
     */
	public String getBailAccNoSubSeq() {
		return this.bailAccNoSubSeq;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param indgtAdvice
	 */
	public void setIndgtAdvice(String indgtAdvice) {
		this.indgtAdvice = indgtAdvice;
	}
	
    /**
     * @return indgtAdvice
     */
	public String getIndgtAdvice() {
		return this.indgtAdvice;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}