/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.batch.dto.batch.CmisBatchDkkhmxDto;
import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.AccLoanAndPvpDto;
import cn.com.yusys.yusp.domain.dto.IqpContExtDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.IqpContExtService;
import cn.com.yusys.yusp.service.server.xddh0007.Xddh0007Service;
import cn.com.yusys.yusp.service.server.xddh0008.Xddh0008Service;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-09 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accloan")
public class AccLoanResource {
    private static final Logger log = LoggerFactory.getLogger(AccLoanResource.class);

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private IqpContExtService iqpContExtService;


    @Autowired
    private Xddh0008Service xddh0008Service;

    @Autowired
    private Xddh0007Service xddh0007Service;

    @Autowired
    private ICusClientService iCusClientService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccLoan> list = accLoanService.selectAll(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }


    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<AccLoan>> index(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.selectByModel(queryModel);
        if(list.size()>0){
            queryModel.addCondition("approveStatus","997");
            for(int i=0;i<list.size();i++){
                String bill_no = list.get(i).getBillNo();
                queryModel.addCondition("billNo",bill_no);
                queryModel.addCondition("status","1");
                List<IqpContExtDto> iqpContExtDtos = iqpContExtService.selectByModelForZhcx(queryModel);
                String size = String.valueOf(iqpContExtDtos.size()+1);
                list.get(i).setExtTimes(size);
            }
        }

        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{billNo}")
    protected ResultDto<AccLoan> show(@PathVariable("billNo") String billNo) {
        AccLoan accLoan = accLoanService.selectByPrimaryKey(billNo);
        return new ResultDto<AccLoan>(accLoan);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<AccLoan> create(@RequestBody AccLoan accLoan) throws URISyntaxException {
        accLoanService.insert(accLoan);
        return new ResultDto<AccLoan>(accLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccLoan accLoan) throws URISyntaxException {
        int result = accLoanService.update(accLoan);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updateselective")
    protected ResultDto<Integer> updateSelective(@RequestBody AccLoan accLoan) throws URISyntaxException {
        int result = accLoanService.updateSelective(accLoan);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{billNo}")
    protected ResultDto<Integer> delete(@PathVariable("billNo") String billNo) {
        int result = accLoanService.deleteByPrimaryKey(billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accLoanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取贷款台账信息
     * @param bizAccLoanDto
     * @return
     */
    @PostMapping(value = "/getAccLoan")
    protected List<BizAccLoanDto> getAccLoan(@RequestBody BizAccLoanDto bizAccLoanDto){
        List<BizAccLoanDto> ls = accLoanService.getAccLoan(bizAccLoanDto);
        return ls;
    }

    /**
     * 业务统计-余额（万元）
     * @param map
     * @return
     */
    @PostMapping("/getbusbalance")
    protected List<Map> getBusBalance(@RequestBody Map map){
        log.info("业务统计-余额（万元）获取..............");
        return accLoanService.getBusBalance(map);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectaccdto")
    protected ResultDto<List<AccLoanToppDto>> selectAccDto(@RequestBody QueryModel queryModel) {
        List<AccLoanToppDto> list = accLoanService.selectAccDto(queryModel);
        return new ResultDto<List<AccLoanToppDto>>(list);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("偿还借据关联贷款台账页查询")
    @PostMapping("/selectaccloanrepay")
    protected ResultDto<List<AccLoan>> selectAccLoanRepay(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.selectAccLoanRepay(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }
    /**
     * @函数名称:queryAccLoanByContNo
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:accLoan
     * @创建人:周茂伟
     */
    @ApiOperation("根据合同编号查询借据")
    @PostMapping("/queryAccLoanByContNo")
    protected ResultDto<List<AccLoan>> queryAccLoanByContNo(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.queryAccLoanByCont(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * @方法名称：queryStockBill
     * @方法描述：获取存量借据
     * @创建人：zhangming12
     * @创建时间：2021/5/22 11:03
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/querystockbill")
    protected ResultDto<List<AllAccContDto>> queryStockBill(@RequestBody QueryModel queryModel){
        List<AllAccContDto> allAccContDtoList = accLoanService.getStockBill(queryModel);
        return new ResultDto<>(allAccContDtoList);
    }
    /**
     * @author zlf
     * @date 2021/5/25 10:48
     * @version 1.0.0
     * @desc     根据借据号查询单条信息
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/selectbillno/{billNo}")
    protected ResultDto<AccLoan> showOne(@PathVariable("billNo") String billNo) {
        AccLoan accLoan = accLoanService.selectByBillNo(billNo);
        return new ResultDto<AccLoan>(accLoan);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/8 9:41
     * @注释 借据编号查询借据详情页面
     */
    @PostMapping("/selectbybill")
    protected ResultDto<AccLoan> showOne(@RequestBody AccLoan accLoan) {
        AccLoan accLoan1 = accLoanService.selectByBillNo(accLoan.getBillNo());
        return new ResultDto<AccLoan>(accLoan1);
    }
    /**
     * @author zlf
     * @date 2021/5/31 14:03
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/querymodel")
    protected ResultDto<List<AccLoan>> selectQueryModel(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.selectByModel(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * 非不良贷款查询.
     *
     * @param
     * @return
     */
    @PostMapping("/selectByCusIdOnAssetType")
    protected ResultDto<List<AccLoanDto>> selectByCusIdOnAssetType(@RequestBody String cusId) {
        List<AccLoanDto> list = accLoanService.selectByCusIdOnAssetType(cusId);
        return new ResultDto<List<AccLoanDto>>(list);
    }

    /**
     * 根据客户编号查询 清收计划管理Excel导入.
     *
     * @param
     * @return
     */
    @PostMapping("/selectByCusId")
    protected ResultDto<List<AccLoanDto>> selectByCusId(@RequestBody AccLoanDto accLoandto ) {
        List<AccLoanDto> list = accLoanService.selectByCusId(accLoandto);
        return new ResultDto<List<AccLoanDto>>(list);
    }

    /**
     * 不良贷款查询.
     *不包括台账状态字段
     * @param
     * @return
     */
    @PostMapping("/selectByCusIdNotType")
    protected ResultDto<List<AccLoanDto>> selectByCusIdNotType(@RequestBody String cusId) {
        List<AccLoanDto> list = accLoanService.selectByCusIdNotType(cusId);
        return new ResultDto<List<AccLoanDto>>(list);
    }

    /**
     * 查询合同所用币种是否为人民币
     * @param
     * @return
     */
    @PostMapping("/selectCurType")
    protected ResultDto<String> selectCurType(@RequestBody AccLoan accLoan) {
    	String billNo = accLoan.getBillNo();
    	String contNo = accLoan.getContNo();
        String contCurType = accLoanService.selectCurType(billNo,contNo);
        return new ResultDto<String>(contCurType);
    }

    /**
     * @author zhoumw
     * @date 2021/5/31 14:03
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @ApiOperation("根据借款合同号查询借据信息")
    @PostMapping("/queryAccLoanByContNoForNpam")
    protected ResultDto<List<AccLoanDto>>queryAccLoanByContNoForNpam(@RequestBody List<String> contNoList) {
        List<AccLoanDto> list = accLoanService.queryAccLoanByContNoForNpam(contNoList);
        return new ResultDto<List<AccLoanDto>>(list);
    }
    /**
     * @author zhoumw
     * @date 2021/5/31 14:03
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @ApiOperation("根据借款客户号查询未结清借据")
    @PostMapping("/queryAccLoanByCusId")
    protected ResultDto<List<AccLoanDto>>queryAccLoanByCusId(@RequestBody QueryModel queryModel) {
        List<AccLoanDto> list = accLoanService.queryAccLoanByCusId(queryModel);
        return new ResultDto<List<AccLoanDto>>(list);
    }

    /**
     * 异步下载对公贷款实时报表
     */
    @PostMapping("/exportaccloan")
    public ResultDto<ProgressDto> asyncExportAccLoan(@RequestBody QueryModel model) {
        ProgressDto progressDto = accLoanService.asyncExportAccLoan(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 异步下载对公贷款实时报表
     */
    @PostMapping("/exportaccloancus")
    public ResultDto<ProgressDto> asyncExportAccLoanCus(@RequestBody QueryModel model) {
        ProgressDto progressDto = accLoanService.asyncExportAccLoanCus(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @author zlf
     * @date 2021/5/31 14:03
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccLoan>> querymodelByCondition(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * 异步下载贷款台账列表
     */
    @PostMapping("/exportAccLoanList")
    public ResultDto<ProgressDto> asyncExportAccLoanList(@RequestBody QueryModel model) {
        ProgressDto progressDto = accLoanService.asyncExportAccLoanList(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:selectAccLoanInfoByParams
     * @函数描述:偿还借据关联贷款台账页查询
     * @参数与返回说明:queryModel
     * @创建人:qw
     */
    @ApiOperation("偿还借据关联贷款台账页查询")
    @PostMapping("/selectaccloaninffobyparams")
    protected ResultDto<List<AccLoan>> selectAccLoanInfoByParams(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.selectAccLoanInfoByParams(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * @author zlf
     * @date 2021/7/12 14:03
     * @version 1.0.0(pvp_authorize)
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/querymodelall")
    protected ResultDto<List<AccLoan>> selectQueryModelAll(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.selectByModelAll(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/8 9:41
     * @注释 借据编号查询借据详情页面
     */
    @PostMapping("/selectbybillpvp")
    protected ResultDto<AccLoanAndPvpDto> showOnePvp(@RequestBody AccLoan accLoan) {
        AccLoanAndPvpDto accLoan1 = accLoanService.selectByBillNoPvp(accLoan);
        return new ResultDto<AccLoanAndPvpDto>(accLoan1);
    }

    /**
     * @创建时间 2021/7/8 9:41
     * @注释 借据编号查询借据详情页面
     */
    @PostMapping("/selectByBillNo")
    protected ResultDto<Xddh0008DataRespDto> selectByBillNo(@RequestBody QueryModel queryModel) {
        Xddh0008DataReqDto xddh0008DataReqDto = new Xddh0008DataReqDto();
        xddh0008DataReqDto.setBillNo(queryModel.getCondition().get("billNo").toString());
        xddh0008DataReqDto.setStartPageNum(queryModel.getPage());
        xddh0008DataReqDto.setPageSize(queryModel.getSize());
        ResultDto<Xddh0008DataRespDto> xddh0008DataRespDto = xddh0008Service.getRepayList(xddh0008DataReqDto);
//        ResultDto result = new ResultDto();
//        result.setData(xddh0008DataRespDto);
        return xddh0008DataRespDto;
    }

    /**
     * @注释 信贷客户核心业绩统计查询列表
     */
    @PostMapping("/dscms2sjzt/xdhxQueryTotalList")
    protected ResultDto<java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.List>> xdhxQueryTotalList(@RequestBody XdhxQueryTotalListReqDto reqDto) {
        ResultDto<XdhxQueryTotalListRespDto>  xdhxQueryTotalListRespDto = new ResultDto<XdhxQueryTotalListRespDto>();
        java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.List> list = null;
        xdhxQueryTotalListRespDto =  accLoanService.xdhxQueryTotalList(reqDto);
        log.info("接口返回数据:[{}]",xdhxQueryTotalListRespDto);
        if(null != xdhxQueryTotalListRespDto && null != xdhxQueryTotalListRespDto.getData()){
            list = xdhxQueryTotalListRespDto.getData().getList();
        }
        log.info("处理后返回前端数据:[{}]",list);
        return new ResultDto<java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.List>>(list);
    }

    /**
     * 根据借据号获取还款明细
     * @param map
     * @author lyj
     * @return
     */
    @PostMapping("/getRepayList")
    protected ResultDto<Xddh0007DataRespDto> getRepayList(@RequestBody Map<String,String> map){
        Xddh0007DataReqDto xddh0007DataReqDto = new Xddh0007DataReqDto();
        xddh0007DataReqDto.setBillNo(map.get("billNo"));
        ResultDto<Xddh0007DataRespDto> xddh0007DataRespDto = xddh0007Service.getRepayList(xddh0007DataReqDto,map);
        return xddh0007DataRespDto;
    }

    /**
     * 根据借据号获取还款明细
     * 从日终落表获取客户借据下全量还款明细
     * @author jijian_yx
     * @date 2021/10/20 21:52
     **/
    @PostMapping("/getRepayListFromBatch")
    protected ResultDto<List<CmisBatchDkkhmxDto>> getRepayListFromBatch(@RequestBody QueryModel model){
        return accLoanService.getRepayListFromBatch(model);
    }


    /**
     * @函数名称: updateAccStatusByBillNo
     * @函数描述:  根据借据编号修改台账状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateAccStatusByBillNo")
    protected ResultDto<Integer> updateAccStatusByBillNo(@RequestBody AccLoan accLoan) throws URISyntaxException {
        int result = accLoanService.updateAccStatusByBillNo(accLoan);
        return new ResultDto<Integer>(result);
    }

    /**
     * 更新台账五级/十级分类
     * @author jijian_yx
     * @date 2021/8/10 23:26
     **/
    @PostMapping("/updateAccLoanByBillNo")
    protected ResultDto<Integer> updateAccLoanByBillNo(@RequestBody Map<String,String> map){
        int result = accLoanService.updateAccLoanByBillNo(map);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/queryaccloanlistbycusid")
    protected ResultDto<List<AccLoan>> queryAccLoanListByCusId(@RequestBody QueryModel model) {
        List<AccLoan> list = accLoanService.queryAccLoanListByCusId(model);
        return new ResultDto<>(list);
    }

    @PostMapping("/querycuslistbycusid")
    protected ResultDto<List<CusBaseClientDto>> queryCusListByCusId(@RequestBody QueryModel model) {
        List<CusBaseClientDto> list = new ArrayList<>();
        if (accLoanService.queryAccLoanListByCusId(model).size() > 0) {
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus((String) model.getCondition().get("cusId"));
            String managerIdName = OcaTranslatorUtils.getUserName(cusBaseClientDto.getManagerId());// 管户经理名称
            String managerBrIdName = OcaTranslatorUtils.getOrgName(cusBaseClientDto.getManagerBrId());// 管户机构名称
            cusBaseClientDto.setManagerIdName(managerIdName);
            cusBaseClientDto.setManagerBrIdName(managerBrIdName);
            list.add(cusBaseClientDto);
        }
        return new ResultDto<>(list);
    }

    /**
     * 通过客户编号查询台账的五级分类
     *
     * @param
     * @return
     */
    @PostMapping("/selectFiveClassByCusId")
    protected ResultDto<AccLoan> selectFiveClassByCusId(@RequestBody String cusId) {
        AccLoan accLoan = accLoanService.selectFiveClassByCusId(cusId);
        return new ResultDto<AccLoan>(accLoan);
    }

    /**
     * @函数名称:queryAssociatedAccLoan
     * @函数描述:查询关联的贷款台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     */
    @ApiOperation("查询关联的贷款台账")
    @PostMapping("/queryassociatedaccloan")
    public ResultDto<List<AccLoan>> queryAssociatedAccLoan(@RequestBody QueryModel queryModel) throws Exception {
        List<AccLoan> accLoanList = new ArrayList<>();
        if(queryModel.getCondition().get("contNo") == null || "".equals(queryModel.getCondition().get("contNo"))){
            return new ResultDto<List<AccLoan>>(accLoanList);
        }
        accLoanList = accLoanService.queryAssociatedAccLoan(queryModel);
        return new ResultDto<List<AccLoan>>(accLoanList);
    }

    /**
     * @author zhoumw
     * @date 2021年9月14日16:06:47
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @ApiOperation("根据借款客户号查询未结清借据")
    @PostMapping("/queryBadCus")
    protected ResultDto<List<AccLoanDto>>queryBadCus(@RequestBody QueryModel queryModel) {
        List<AccLoanDto> list = accLoanService.queryBadCus(queryModel);
        return new ResultDto<List<AccLoanDto>>(list);
    }

    /**
     * 根据借据号从核心同步贷款台账金额及状态
     * @author jijian_yx
     * @date 2021/10/7 14:17
     **/
    @ApiOperation("根据借据号从核心同步贷款台账金额及状态")
    @PostMapping("/synchronizeAccLoan")
    protected ResultDto synchronizeAccLoan(@RequestBody String billNo){
        ResultDto result = accLoanService.synchronizeAccLoan(billNo);
        return result;
    }

    /**
     * 修改借据信息
     * @param accLoanDto
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/updateAccLoan")
    protected ResultDto<Integer> updateAccLoan(@RequestBody List<AccLoanDto> accLoanDto) throws URISyntaxException {
        int result = accLoanService.updateAccLoan(accLoanDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取多笔借据号下最大逾期天数
     * @author jijian_yx
     * @date 2021/10/13 21:16
     **/
    @PostMapping("/getmaxoverdueday")
    protected ResultDto<Integer> getMaxOverdueTimes(@RequestBody List<String> billNos){
        int result = accLoanService.getMaxOverdueDay(billNos);
        return new ResultDto<>(result);
    }

    /**
     * 根据借据编号查询五级十级分类
     * @param accLoanDto
     * @return
     */
    @PostMapping("/selectclassbybillno")
    public ResultDto<AccLoanDto> selectClassByBillNo(@RequestBody AccLoanDto accLoanDto){
        AccLoanDto result = accLoanService.selectClassByBillNo(accLoanDto.getBillNo());
        return new ResultDto<AccLoanDto>(result);
    }

    @ApiOperation("偿还借据分页查询")
    @PostMapping("/queryAccLoanByPvpserno")
    protected ResultDto<List<AccLoan>> queryAccLoanByPvpserno(@RequestBody QueryModel queryModel) {
        List<AccLoan> list = accLoanService.queryAccLoanByPvpserno(queryModel);
        return new ResultDto<List<AccLoan>>(list);
    }

    @ApiOperation("展期修改")
    @PostMapping("/updateAccLoanForZQ")
    protected ResultDto<Integer> updateAccLoanForZQ(@RequestBody QueryModel queryModel) {
        Integer count = accLoanService.updateAccLoanForZQ(queryModel);
        return new ResultDto<Integer>(count);
    }

    @ApiOperation("根据传参返回台账笔数")
    @PostMapping("/selectaccloanbyparamcount")
    protected ResultDto<Integer> selectaccloanbyparamcount(@RequestBody AccLoan accLoan) {
        Integer count = accLoanService.selectAccLoanByParamCount(accLoan);
        return new ResultDto<Integer>(count);
    }


}
