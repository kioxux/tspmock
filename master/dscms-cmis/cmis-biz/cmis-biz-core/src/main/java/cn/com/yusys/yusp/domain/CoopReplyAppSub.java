/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAppSub
 * @类描述: coop_reply_app_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_reply_app_sub")
public class CoopReplyAppSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 分项编号 **/
	@Column(name = "SUB_NO", unique = false, nullable = false, length = 40)
	private String subNo;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作产品编号 **/
	@Column(name = "COOP_PRD_ID", unique = false, nullable = true, length = 60)
	private String coopPrdId;
	
	/** 产品属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 10)
	private String prdTypeProp;
	
	/** 单个产品合作额度(元) **/
	@Column(name = "SINGLE_PRD_COOP_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singlePrdCoopLmt;
	
	/** 单笔最低缴存金额(元) **/
	@Column(name = "SIG_LOW_DEPOSIT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subNo
	 */
	public void setSubNo(String subNo) {
		this.subNo = subNo;
	}
	
    /**
     * @return subNo
     */
	public String getSubNo() {
		return this.subNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param coopPrdId
	 */
	public void setCoopPrdId(String coopPrdId) {
		this.coopPrdId = coopPrdId;
	}
	
    /**
     * @return coopPrdId
     */
	public String getCoopPrdId() {
		return this.coopPrdId;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	
    /**
     * @return prdTypeProp
     */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param singlePrdCoopLmt
	 */
	public void setSinglePrdCoopLmt(java.math.BigDecimal singlePrdCoopLmt) {
		this.singlePrdCoopLmt = singlePrdCoopLmt;
	}
	
    /**
     * @return singlePrdCoopLmt
     */
	public java.math.BigDecimal getSinglePrdCoopLmt() {
		return this.singlePrdCoopLmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return sigLowDepositAmt
     */
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}