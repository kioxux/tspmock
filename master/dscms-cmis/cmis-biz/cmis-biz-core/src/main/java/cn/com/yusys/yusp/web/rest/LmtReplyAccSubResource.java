/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReplyAcc;
import cn.com.yusys.yusp.dto.LmtReplyAccSubDto;
import cn.com.yusys.yusp.dto.LmtReplySubDto;
import cn.com.yusys.yusp.service.LmtReplyAccSubService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAccSub;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyaccsub")
public class LmtReplyAccSubResource {
    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAccSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAccSub> list = lmtReplyAccSubService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAccSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAccSub>> index(QueryModel queryModel) {
        List<LmtReplyAccSub> list = lmtReplyAccSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAccSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAccSub> show(@PathVariable("pkId") String pkId) {
        LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAccSub>(lmtReplyAccSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAccSub> create(@RequestBody LmtReplyAccSub lmtReplyAccSub) throws URISyntaxException {
        lmtReplyAccSubService.insert(lmtReplyAccSub);
        return new ResultDto<LmtReplyAccSub>(lmtReplyAccSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAccSub lmtReplyAccSub) throws URISyntaxException {
        int result = lmtReplyAccSubService.update(lmtReplyAccSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getreplysubinfobygrpserno
     * @方法描述：根据集团申请流水号获取批复分项信息
     * @创建人：yangwl
     * @创建时间：2021/5/24 14:12
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/getlmtreplyaccsubbygrpserno")
    protected ResultDto<List<LmtReplyAccSubDto>> getLmtReplyAccSubByGrpSerno(@RequestBody QueryModel queryModel) throws Exception {
        String grpAccNo = (String) queryModel.getCondition().get("grpAccNo");
        List<LmtReplyAccSubDto> lmtReplySubDtoList = lmtReplyAccSubService.getLmtReplyAccSubByGrpSerno(grpAccNo);
        return new ResultDto<>(lmtReplySubDtoList);
    }

    /**
     * @函数名称:getSubandPrdBySingleSerno
     * @函数描述:根据申请流水号获取批复分项信息
     * @参数与返回说明:
     * @创建人: yangwl
     * @创建时间: 2021-6-26
     * @算法描述:
     */
    @PostMapping("/getsubandprdbysingleserno")
    protected ResultDto<List<LmtReplyAccSubDto>> getSubandPrdBySingleSerno(@RequestBody String singleSerno) throws Exception {
        List<LmtReplyAccSubDto> lmtReplyAccSubByGrpSerno = lmtReplyAccSubService.getSubandPrdBySingleSerno(singleSerno);
        return new ResultDto<>(lmtReplyAccSubByGrpSerno);
    }

    /**
     * @方法名称: getLmtReplyAccSubByAccSubNo
     * @方法描述: 根据台账编号获取授信批复台账分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtreplyaccsubbyaccsubno")
    protected ResultDto<LmtReplyAccSub> getLmtReplyAccSubByAccSubNo(@RequestBody Map map) {
        LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
        return new ResultDto<LmtReplyAccSub>(lmtReplyAccSub);
    }

    /**
     * @函数名称:getlmtReplyAccSubByCusId
     * @函数描述:根据客户编号获取批复分项信息
     * @参数与返回说明:
     * @创建人: qw
     * @创建时间: 2021-7-13
     * @算法描述:
     */
    @PostMapping("/getlmtreplyaccsubbyparams")
    protected ResultDto<List<LmtReplyAccSubDto>> getLmtReplyAccSubByCusId(@RequestBody String singleSerno) throws Exception {
        List<LmtReplyAccSubDto> lmtReplyAccSubByGrpSerno = lmtReplyAccSubService.getSubandPrdBySingleSerno(singleSerno);
        return new ResultDto<>(lmtReplyAccSubByGrpSerno);
    }

    /**
     * @函数名称:getfdyddLmtReplyAccSubByAccSubNo
     * @函数描述:获取房抵e抵贷授信批复台账分项信息
     * @参数与返回说明:
     * 分页查询类
     * @算法描述:
     * @创建人：zhangliang15
     */
    @ApiOperation(value = "获取房抵e抵贷授信批复台账分项信息")
    @PostMapping("/getfdyddLmtReplyAccSubByAccSubNo")
    protected ResultDto<List<LmtReplyAccSub>> getfdyddLmtReplyAccSubByAccSubNo(@RequestBody QueryModel queryModel) {
        List<LmtReplyAccSub> list = lmtReplyAccSubService.getfdyddLmtReplyAccSubByAccSubNo(queryModel);
        return new ResultDto<List<LmtReplyAccSub>>(list);
    }
}
