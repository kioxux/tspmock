/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanAppSub;
import cn.com.yusys.yusp.service.OtherRecordAccpSignPlanAppSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignPlanAppSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-09 20:29:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherrecordaccpsignplanappsub")
public class OtherRecordAccpSignPlanAppSubResource {
    @Autowired
    private OtherRecordAccpSignPlanAppSubService otherRecordAccpSignPlanAppSubService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherRecordAccpSignPlanAppSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherRecordAccpSignPlanAppSub> list = otherRecordAccpSignPlanAppSubService.selectAll(queryModel);
        return new ResultDto<List<OtherRecordAccpSignPlanAppSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherRecordAccpSignPlanAppSub>> index(QueryModel queryModel) {
        List<OtherRecordAccpSignPlanAppSub> list = otherRecordAccpSignPlanAppSubService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordAccpSignPlanAppSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherRecordAccpSignPlanAppSub> show(@PathVariable("serno") String serno) {
        OtherRecordAccpSignPlanAppSub otherRecordAccpSignPlanAppSub = otherRecordAccpSignPlanAppSubService.selectByPrimaryKey(serno);
        return new ResultDto<OtherRecordAccpSignPlanAppSub>(otherRecordAccpSignPlanAppSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherRecordAccpSignPlanAppSub> create(@RequestBody OtherRecordAccpSignPlanAppSub otherRecordAccpSignPlanAppSub) throws URISyntaxException {
        otherRecordAccpSignPlanAppSubService.insert(otherRecordAccpSignPlanAppSub);
        return new ResultDto<OtherRecordAccpSignPlanAppSub>(otherRecordAccpSignPlanAppSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherRecordAccpSignPlanAppSub otherRecordAccpSignPlanAppSub) throws URISyntaxException {
        int result = otherRecordAccpSignPlanAppSubService.update(otherRecordAccpSignPlanAppSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherRecordAccpSignPlanAppSubService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherRecordAccpSignPlanAppSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
