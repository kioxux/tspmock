package cn.com.yusys.yusp.service.client.bsp.core.ln3251;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.req.Ln3251ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp.Ln3251RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/5/21 10:33
 * @desc 贷款自动追缴明细查询
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ln3251Service {
    private static final Logger logger = LoggerFactory.getLogger(Ln3251Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param ln3251ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3251.Ln3251RespDto
     * @author 王玉坤
     * @date 2021/8/27 22:44
     * @version 1.0.0
     * @desc 贷款自动追缴明细查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Ln3251RespDto ln3251(Ln3251ReqDto ln3251ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value, JSON.toJSONString(ln3251ReqDto));
        ResultDto<Ln3251RespDto> ln3251ResultDto = dscms2CoreLnClientService.ln3251(ln3251ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value, JSON.toJSONString(ln3251ResultDto));
        String ln3251Code = Optional.ofNullable(ln3251ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3251Meesage = Optional.ofNullable(ln3251ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3251RespDto ln3251RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3251ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3251RespDto = ln3251ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(ln3251Code, ln3251Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value);
        return ln3251RespDto;
    }
}
