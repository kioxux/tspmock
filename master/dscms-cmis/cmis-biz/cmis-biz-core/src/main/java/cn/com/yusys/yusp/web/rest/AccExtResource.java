/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccExt;
import cn.com.yusys.yusp.service.AccExtService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccExtResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-23 16:31:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accext")
public class AccExtResource {
    private final static Logger log = LoggerFactory.getLogger(AccExtResource.class);
    @Autowired
    private AccExtService accExtService;

	/**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccExt>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccExt> list = accExtService.selectAll(queryModel);
        return new ResultDto<List<AccExt>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccExt>> index(QueryModel queryModel) {
        List<AccExt> list = accExtService.selectByModel(queryModel);
        return new ResultDto<List<AccExt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{extBillNo}")
    protected ResultDto<AccExt> show(@PathVariable("extBillNo") String extBillNo) {
        AccExt accExt = accExtService.selectByPrimaryKey(extBillNo);
        return new ResultDto<AccExt>(accExt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccExt> create(@RequestBody AccExt accExt) throws URISyntaxException {
        accExtService.insert(accExt);
        return new ResultDto<AccExt>(accExt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccExt accExt) throws URISyntaxException {
        int result = accExtService.update(accExt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{extBillNo}")
    protected ResultDto<Integer> delete(@PathVariable("extBillNo") String extBillNo) {
        int result = accExtService.deleteByPrimaryKey(extBillNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accExtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 校验协议是否已经被引用过
     * @param extCtrNo
     * @return
     */
    @GetMapping("/checkEffectiveCtrInfo/{extCtrNo}")
    protected ResultDto<Integer> checkEffectiveCtrInfo(@PathVariable("extCtrNo") String extCtrNo){
        log.info("校验协议是否已经被引用过start{}"+ JSON.toJSONString(extCtrNo));
        int result = accExtService.checkEffectiveCtrInfo(extCtrNo);
        return new ResultDto<Integer>(result);
    }
}
