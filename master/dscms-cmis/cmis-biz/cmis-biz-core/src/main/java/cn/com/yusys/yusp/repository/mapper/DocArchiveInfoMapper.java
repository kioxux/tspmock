/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.DocArchiveInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 09:51:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocArchiveInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocArchiveInfo selectByPrimaryKey(@Param("docSerno") String docSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocArchiveInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocArchiveInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocArchiveInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocArchiveInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocArchiveInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("docSerno") String docSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据档案类型和客户号获取业务流水号
     * @author jijian_yx
     * @date 2021/6/22 14:11
     **/
    List<DocArchiveInfo> selectSernoByDocType(QueryModel model);

    /**
     * 根据业务流水号和档案状态获取归档任务数
     * @author jijian_yx
     * @date 2021/6/29 20:34
     **/
    int selectByBizSerno(Map<String,String> map);

    /**
     * 根据业务流水号获取在途归档任务数
     * @author jijian_yx
     * @date 2021/8/30 10:46
     **/
    int queryDocTaskInWayCount(String bizSerno);

    /**
     * 作废合同阶段等待入库归档任务
     * @author jijian_yx
     * @date 2021/9/14 21:06
     **/
    int invalidContDocInfo(Map<String,String> map);

    /**
     * 作废合同阶段等待入库档案台账
     * @author jijian_yx
     * @date 2021/9/14 21:06
     **/
    int invalidContDocAcc(Map<String,String> map);

    /**
     * 根据合同查询是否有等待入库的最高额授信协议档案归档任务
     * @author jijian_yx
     * @date 2021/10/23 20:04
     **/
    DocArchiveInfo queryHighAmtAgrCount(@Param("contNo") String contNo);

    /**
     * 根据授信流水号获取房抵e点贷授信批复信息数
     * @author jijian_yx
     * @date 2021/11/17 21:40
     **/
    int getP034LmtCount(@Param("serno") String bizSerno);

    String selectBelgLineForYWBG(@Param("serno") String serno,@Param("docType") String docType);
}