/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InPoolAssetListExport
 * @类描述:
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "入池资产清单", fileType = ExcelCsv.ExportFileType.XLS)
public class InPoolAssetListExport {

	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 20)
	private String cusId;

	/** 合同编号 **/
	@ExcelField(title = "合同编号", viewLength = 20)
	private String contNo;

	/** 资产编号 **/
	@ExcelField(title = "资产编号", viewLength = 40)
	private String assetNo;

	/** 统一押品编号 **/
	@ExcelField(title = "统一押品编号", viewLength = 40)
	private String guarNo;

	/** 资产类型 **/
	@ExcelField(title = "资产类型", viewLength = 20, dictCode = "STD_ASPL_ASSET_TYPE")
	private String assetType;

	/** 资产价值 **/
	@ExcelField(title = "资产价值", viewLength = 20)
	private java.math.BigDecimal assetValue;

	/** 资产到期日 **/
	@ExcelField(title = "资产到期日", viewLength = 20)
	private String assetEndDate;

	/** 是否入池 **/
	@ExcelField(title = "是否入池", viewLength = 10, dictCode = "STD_ZB_YES_NO")
	private String isPool;

	/** 是否入池质押 **/
	@ExcelField(title = "是否入池质押", viewLength = 10, dictCode = "STD_ZB_YES_NO")
	private String isPledge;

	/** 入池时间 **/
	@ExcelField(title = "入池时间", viewLength = 20)
	private String inpTime;

	/** 出池时间 **/
	@ExcelField(title = "出池时间", viewLength = 20)
	private String outpTime;

	/** 承兑行行号 **/
	@ExcelField(title = "承兑行行号", viewLength = 20)
	private String aorgNo;

	/** 承兑行名称 **/
	@ExcelField(title = "承兑行名称", viewLength = 40)
	private String aorgName;

//	/** 操作类型 **/
//	@ExcelField(title = "承兑行名称", viewLength = 40)
//	private String oprType;

	/** 登记人 **/
	@ExcelField(title = "登记人", viewLength = 20)
	private String inputId;

	/** 登记机构 **/
	@ExcelField(title = "登记机构", viewLength = 20)
	private String inputBrId;

	/** 登记日期 **/
	@ExcelField(title = "登记日期", viewLength = 20)
	private String inputDate;

	/** 最近修改人 **/
	@ExcelField(title = "最近修改人", viewLength = 20)
	private String updId;

	/** 最近修改机构 **/
	@ExcelField(title = "最近修改机构", viewLength = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@ExcelField(title = "最近修改日期", viewLength = 20)
	private String updDate;

	/** 主管客户经理 **/
	@ExcelField(title = "主管客户经理", viewLength = 20)
	private String managerId;

	/** 主管机构 **/
	@ExcelField(title = "主管机构", viewLength = 20)
	private String managerBrId;

	/** 创建时间 **/
	@ExcelField(title = "创建时间", viewLength = 20)
	private java.util.Date createTime;

	/** 修改时间 **/
	@ExcelField(title = "修改时间", viewLength = 20)
	private java.util.Date updateTime;

	/** 资产状态 **/
	@ExcelField(title = "资产状态", viewLength = 20)
	private String assetStatus;

	/** 资产来源 **/
	@ExcelField(title = "资产来源", viewLength = 20)
	private String assteSour;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getAssetNo() {
		return assetNo;
	}

	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public BigDecimal getAssetValue() {
		return assetValue;
	}

	public void setAssetValue(BigDecimal assetValue) {
		this.assetValue = assetValue;
	}

	public String getAssetEndDate() {
		return assetEndDate;
	}

	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}

	public String getIsPool() {
		return isPool;
	}

	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}

	public String getIsPledge() {
		return isPledge;
	}

	public void setIsPledge(String isPledge) {
		this.isPledge = isPledge;
	}

	public String getInpTime() {
		return inpTime;
	}

	public void setInpTime(String inpTime) {
		this.inpTime = inpTime;
	}

	public String getOutpTime() {
		return outpTime;
	}

	public void setOutpTime(String outpTime) {
		this.outpTime = outpTime;
	}

	public String getAorgNo() {
		return aorgNo;
	}

	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}

	public String getAorgName() {
		return aorgName;
	}

	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getAssetStatus() {
		return assetStatus;
	}

	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}

	public String getAssteSour() {
		return assteSour;
	}

	public void setAssteSour(String assteSour) {
		this.assteSour = assteSour;
	}
}