/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydCreditline
 * @类描述: tmp_wyd_creditline数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_creditline_tmp")
public class TmpWydCreditlineTmp {
	
	/** 额度编号 **/
	@Id
	@Column(name = "LIMIT_NO")
	private String limitNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 客户号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 40)
	private String custId;
	
	/** 客户证件类型 **/
	@Column(name = "CUST_IDTYPE", unique = false, nullable = true, length = 20)
	private String custIdtype;
	
	/** 客户证件号码 **/
	@Column(name = "CUST_IDNO", unique = false, nullable = true, length = 30)
	private String custIdno;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 60)
	private String custName;
	
	/** 币种 **/
	@Column(name = "CCY_CD", unique = false, nullable = true, length = 10)
	private String ccyCd;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 循环额度标志 **/
	@Column(name = "CIRCUL_FLG", unique = false, nullable = true, length = 1)
	private String circulFlg;
	
	/** 授信起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 额度到期日期 **/
	@Column(name = "MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String maturityDate;
	
	/** 授信额度 **/
	@Column(name = "CREDIT_LINE", unique = false, nullable = true, length = 20)
	private String creditLine;
	
	/** 未动拨授信额度 **/
	@Column(name = "UN_CREDIT_LINE", unique = false, nullable = true, length = 20)
	private String unCreditLine;
	
	/** 授信业务类型 **/
	@Column(name = "CREDIT_TYPE", unique = false, nullable = true, length = 10)
	private String creditType;
	
	/** 生效日期 **/
	@Column(name = "BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String beginDate;
	
	/** 发生日期 **/
	@Column(name = "TRAN_DATE", unique = false, nullable = true, length = 10)
	private String tranDate;
	
	/** 授信开始日期 **/
	@Column(name = "INIT_DATE", unique = false, nullable = true, length = 10)
	private String initDate;
	
	/** 授信协议号 **/
	@Column(name = "LIMIT_REPORT_NO", unique = false, nullable = true, length = 32)
	private String limitReportNo;
	
	/** 协议状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 10)
	private String status;
	
	/** 冻结标志 **/
	@Column(name = "FREEZEFLAG", unique = false, nullable = true, length = 10)
	private String freezeflag;
	
	/** 续期日期 **/
	@Column(name = "ADJUST_DATE", unique = false, nullable = true, length = 10)
	private String adjustDate;
	
	/** 更新时间 **/
	@Column(name = "EXTEND_DATE", unique = false, nullable = true, length = 10)
	private String extendDate;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param limitNo
	 */
	public void setLimitNo(String limitNo) {
		this.limitNo = limitNo;
	}
	
    /**
     * @return limitNo
     */
	public String getLimitNo() {
		return this.limitNo;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custIdtype
	 */
	public void setCustIdtype(String custIdtype) {
		this.custIdtype = custIdtype;
	}
	
    /**
     * @return custIdtype
     */
	public String getCustIdtype() {
		return this.custIdtype;
	}
	
	/**
	 * @param custIdno
	 */
	public void setCustIdno(String custIdno) {
		this.custIdno = custIdno;
	}
	
    /**
     * @return custIdno
     */
	public String getCustIdno() {
		return this.custIdno;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param ccyCd
	 */
	public void setCcyCd(String ccyCd) {
		this.ccyCd = ccyCd;
	}
	
    /**
     * @return ccyCd
     */
	public String getCcyCd() {
		return this.ccyCd;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param circulFlg
	 */
	public void setCirculFlg(String circulFlg) {
		this.circulFlg = circulFlg;
	}
	
    /**
     * @return circulFlg
     */
	public String getCirculFlg() {
		return this.circulFlg;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param maturityDate
	 */
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
    /**
     * @return maturityDate
     */
	public String getMaturityDate() {
		return this.maturityDate;
	}
	
	/**
	 * @param creditLine
	 */
	public void setCreditLine(String creditLine) {
		this.creditLine = creditLine;
	}
	
    /**
     * @return creditLine
     */
	public String getCreditLine() {
		return this.creditLine;
	}
	
	/**
	 * @param unCreditLine
	 */
	public void setUnCreditLine(String unCreditLine) {
		this.unCreditLine = unCreditLine;
	}
	
    /**
     * @return unCreditLine
     */
	public String getUnCreditLine() {
		return this.unCreditLine;
	}
	
	/**
	 * @param creditType
	 */
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	
    /**
     * @return creditType
     */
	public String getCreditType() {
		return this.creditType;
	}
	
	/**
	 * @param beginDate
	 */
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	
    /**
     * @return beginDate
     */
	public String getBeginDate() {
		return this.beginDate;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	
    /**
     * @return tranDate
     */
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param initDate
	 */
	public void setInitDate(String initDate) {
		this.initDate = initDate;
	}
	
    /**
     * @return initDate
     */
	public String getInitDate() {
		return this.initDate;
	}
	
	/**
	 * @param limitReportNo
	 */
	public void setLimitReportNo(String limitReportNo) {
		this.limitReportNo = limitReportNo;
	}
	
    /**
     * @return limitReportNo
     */
	public String getLimitReportNo() {
		return this.limitReportNo;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param freezeflag
	 */
	public void setFreezeflag(String freezeflag) {
		this.freezeflag = freezeflag;
	}
	
    /**
     * @return freezeflag
     */
	public String getFreezeflag() {
		return this.freezeflag;
	}
	
	/**
	 * @param adjustDate
	 */
	public void setAdjustDate(String adjustDate) {
		this.adjustDate = adjustDate;
	}
	
    /**
     * @return adjustDate
     */
	public String getAdjustDate() {
		return this.adjustDate;
	}
	
	/**
	 * @param extendDate
	 */
	public void setExtendDate(String extendDate) {
		this.extendDate = extendDate;
	}
	
    /**
     * @return extendDate
     */
	public String getExtendDate() {
		return this.extendDate;
	}


}