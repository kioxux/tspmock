package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusHouseInfo
 * @类描述: cus_house_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:43:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusHouseInfoQueryVo extends PageQuery{
	
	/** 流水号 **/
	private String serno;
	
	/** 面签流水号 **/
	private String signatureSerno;
	
	/** 省编号 **/
	private String provinceId;
	
	/** 省名称 **/
	private String provinceName;
	
	/** 省别名 **/
	private String provinceAlias;
	
	/** 省国际编码 **/
	private String provinceInterCode;
	
	/** 城市编号 **/
	private String cityId;
	
	/** 城市名称 **/
	private String cityName;
	
	/** 城市别名 **/
	private String cityAlias;
	
	/** 城市国际编码 **/
	private String cityCode;
	
	/** 区县编号 **/
	private String countyId;
	
	/** 区县名称 **/
	private String countyName;
	
	/** 区县国际编码 **/
	private String countyCode;
	
	/** 楼盘编号 **/
	private String communityId;
	
	/** 楼盘名称 **/
	private String communityName;
	
	/** 楼栋编号 **/
	private String buildingId;
	
	/** 楼栋名称 **/
	private String buildingName;
	
	/** 楼层(实际层) **/
	private Integer floor;
	
	/** 总层 **/
	private Integer generalFloor;
	
	/** 房号编号 **/
	private String roomNum;
	
	/** 房号名称 **/
	private String roomName;
	
	/** 房产类型 **/
	private String houseType;
	
	/** 房产面积 **/
	private java.math.BigDecimal houseSqu;
	
	/** 产权证号 **/
	private String houseLandNo;
	
	/** 区位信息 **/
	private String locationInfo;
	
	/** 土地面积 **/
	private java.math.BigDecimal landSqu;
	
	/** 是否出租 **/
	private String isLease;
	
	/** 评估方式 **/
	private String evalType;
	
	/** 评估价值 **/
	private java.math.BigDecimal evalAmt;
	
	/** 抵押物所有权人 **/
	private String pldimnOwner;
	
	/** 自行车库面积 **/
	private java.math.BigDecimal bicycleParkingSqu;
	
	/** 车位面积 **/
	private java.math.BigDecimal carportSqu;
	
	/** 阁楼面积 **/
	private java.math.BigDecimal atticSqu;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 自行车库价值 **/
	private java.math.BigDecimal bicycleParkingAmt;
	
	/** 车位价值 **/
	private java.math.BigDecimal carportAmt;
	
	/** 阁楼价值 **/
	private java.math.BigDecimal atticAmt;
	
	/** 总评估价值 **/
	private java.math.BigDecimal evalTotalAmt;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param signatureSerno
	 */
	public void setSignatureSerno(String signatureSerno) {
		this.signatureSerno = signatureSerno == null ? null : signatureSerno.trim();
	}
	
    /**
     * @return SignatureSerno
     */	
	public String getSignatureSerno() {
		return this.signatureSerno;
	}
	
	/**
	 * @param provinceId
	 */
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId == null ? null : provinceId.trim();
	}
	
    /**
     * @return ProvinceId
     */	
	public String getProvinceId() {
		return this.provinceId;
	}
	
	/**
	 * @param provinceName
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName == null ? null : provinceName.trim();
	}
	
    /**
     * @return ProvinceName
     */	
	public String getProvinceName() {
		return this.provinceName;
	}
	
	/**
	 * @param provinceAlias
	 */
	public void setProvinceAlias(String provinceAlias) {
		this.provinceAlias = provinceAlias == null ? null : provinceAlias.trim();
	}
	
    /**
     * @return ProvinceAlias
     */	
	public String getProvinceAlias() {
		return this.provinceAlias;
	}
	
	/**
	 * @param provinceInterCode
	 */
	public void setProvinceInterCode(String provinceInterCode) {
		this.provinceInterCode = provinceInterCode == null ? null : provinceInterCode.trim();
	}
	
    /**
     * @return ProvinceInterCode
     */	
	public String getProvinceInterCode() {
		return this.provinceInterCode;
	}
	
	/**
	 * @param cityId
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId == null ? null : cityId.trim();
	}
	
    /**
     * @return CityId
     */	
	public String getCityId() {
		return this.cityId;
	}
	
	/**
	 * @param cityName
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName == null ? null : cityName.trim();
	}
	
    /**
     * @return CityName
     */	
	public String getCityName() {
		return this.cityName;
	}
	
	/**
	 * @param cityAlias
	 */
	public void setCityAlias(String cityAlias) {
		this.cityAlias = cityAlias == null ? null : cityAlias.trim();
	}
	
    /**
     * @return CityAlias
     */	
	public String getCityAlias() {
		return this.cityAlias;
	}
	
	/**
	 * @param cityCode
	 */
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode == null ? null : cityCode.trim();
	}
	
    /**
     * @return CityCode
     */	
	public String getCityCode() {
		return this.cityCode;
	}
	
	/**
	 * @param countyId
	 */
	public void setCountyId(String countyId) {
		this.countyId = countyId == null ? null : countyId.trim();
	}
	
    /**
     * @return CountyId
     */	
	public String getCountyId() {
		return this.countyId;
	}
	
	/**
	 * @param countyName
	 */
	public void setCountyName(String countyName) {
		this.countyName = countyName == null ? null : countyName.trim();
	}
	
    /**
     * @return CountyName
     */	
	public String getCountyName() {
		return this.countyName;
	}
	
	/**
	 * @param countyCode
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode == null ? null : countyCode.trim();
	}
	
    /**
     * @return CountyCode
     */	
	public String getCountyCode() {
		return this.countyCode;
	}
	
	/**
	 * @param communityId
	 */
	public void setCommunityId(String communityId) {
		this.communityId = communityId == null ? null : communityId.trim();
	}
	
    /**
     * @return CommunityId
     */	
	public String getCommunityId() {
		return this.communityId;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName == null ? null : communityName.trim();
	}
	
    /**
     * @return CommunityName
     */	
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param buildingId
	 */
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId == null ? null : buildingId.trim();
	}
	
    /**
     * @return BuildingId
     */	
	public String getBuildingId() {
		return this.buildingId;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName == null ? null : buildingName.trim();
	}
	
    /**
     * @return BuildingName
     */	
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	
    /**
     * @return Floor
     */	
	public Integer getFloor() {
		return this.floor;
	}
	
	/**
	 * @param generalFloor
	 */
	public void setGeneralFloor(Integer generalFloor) {
		this.generalFloor = generalFloor;
	}
	
    /**
     * @return GeneralFloor
     */	
	public Integer getGeneralFloor() {
		return this.generalFloor;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum == null ? null : roomNum.trim();
	}
	
    /**
     * @return RoomNum
     */	
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param roomName
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName == null ? null : roomName.trim();
	}
	
    /**
     * @return RoomName
     */	
	public String getRoomName() {
		return this.roomName;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(java.math.BigDecimal houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return HouseSqu
     */	
	public java.math.BigDecimal getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo == null ? null : houseLandNo.trim();
	}
	
    /**
     * @return HouseLandNo
     */	
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param locationInfo
	 */
	public void setLocationInfo(String locationInfo) {
		this.locationInfo = locationInfo == null ? null : locationInfo.trim();
	}
	
    /**
     * @return LocationInfo
     */	
	public String getLocationInfo() {
		return this.locationInfo;
	}
	
	/**
	 * @param landSqu
	 */
	public void setLandSqu(java.math.BigDecimal landSqu) {
		this.landSqu = landSqu;
	}
	
    /**
     * @return LandSqu
     */	
	public java.math.BigDecimal getLandSqu() {
		return this.landSqu;
	}
	
	/**
	 * @param isLease
	 */
	public void setIsLease(String isLease) {
		this.isLease = isLease == null ? null : isLease.trim();
	}
	
    /**
     * @return IsLease
     */	
	public String getIsLease() {
		return this.isLease;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType == null ? null : evalType.trim();
	}
	
    /**
     * @return EvalType
     */	
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param pldimnOwner
	 */
	public void setPldimnOwner(String pldimnOwner) {
		this.pldimnOwner = pldimnOwner == null ? null : pldimnOwner.trim();
	}
	
    /**
     * @return PldimnOwner
     */	
	public String getPldimnOwner() {
		return this.pldimnOwner;
	}
	
	/**
	 * @param bicycleParkingSqu
	 */
	public void setBicycleParkingSqu(java.math.BigDecimal bicycleParkingSqu) {
		this.bicycleParkingSqu = bicycleParkingSqu;
	}
	
    /**
     * @return BicycleParkingSqu
     */	
	public java.math.BigDecimal getBicycleParkingSqu() {
		return this.bicycleParkingSqu;
	}
	
	/**
	 * @param carportSqu
	 */
	public void setCarportSqu(java.math.BigDecimal carportSqu) {
		this.carportSqu = carportSqu;
	}
	
    /**
     * @return CarportSqu
     */	
	public java.math.BigDecimal getCarportSqu() {
		return this.carportSqu;
	}
	
	/**
	 * @param atticSqu
	 */
	public void setAtticSqu(java.math.BigDecimal atticSqu) {
		this.atticSqu = atticSqu;
	}
	
    /**
     * @return AtticSqu
     */	
	public java.math.BigDecimal getAtticSqu() {
		return this.atticSqu;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param bicycleParkingAmt
	 */
	public void setBicycleParkingAmt(java.math.BigDecimal bicycleParkingAmt) {
		this.bicycleParkingAmt = bicycleParkingAmt;
	}
	
    /**
     * @return BicycleParkingAmt
     */	
	public java.math.BigDecimal getBicycleParkingAmt() {
		return this.bicycleParkingAmt;
	}
	
	/**
	 * @param carportAmt
	 */
	public void setCarportAmt(java.math.BigDecimal carportAmt) {
		this.carportAmt = carportAmt;
	}
	
    /**
     * @return CarportAmt
     */	
	public java.math.BigDecimal getCarportAmt() {
		return this.carportAmt;
	}
	
	/**
	 * @param atticAmt
	 */
	public void setAtticAmt(java.math.BigDecimal atticAmt) {
		this.atticAmt = atticAmt;
	}
	
    /**
     * @return AtticAmt
     */	
	public java.math.BigDecimal getAtticAmt() {
		return this.atticAmt;
	}
	
	/**
	 * @param evalTotalAmt
	 */
	public void setEvalTotalAmt(java.math.BigDecimal evalTotalAmt) {
		this.evalTotalAmt = evalTotalAmt;
	}
	
    /**
     * @return EvalTotalAmt
     */	
	public java.math.BigDecimal getEvalTotalAmt() {
		return this.evalTotalAmt;
	}


}