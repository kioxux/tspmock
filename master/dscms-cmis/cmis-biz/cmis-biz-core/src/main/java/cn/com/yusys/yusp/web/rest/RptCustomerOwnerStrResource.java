/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCustomerOwnerStr;
import cn.com.yusys.yusp.service.RptCustomerOwnerStrService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCustomerOwnerStrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 19:34:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcustomerownerstr")
public class RptCustomerOwnerStrResource {
    @Autowired
    private RptCustomerOwnerStrService rptCustomerOwnerStrService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCustomerOwnerStr>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCustomerOwnerStr> list = rptCustomerOwnerStrService.selectAll(queryModel);
        return new ResultDto<List<RptCustomerOwnerStr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCustomerOwnerStr>> index(QueryModel queryModel) {
        List<RptCustomerOwnerStr> list = rptCustomerOwnerStrService.selectByModel(queryModel);
        return new ResultDto<List<RptCustomerOwnerStr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptCustomerOwnerStr> show(@PathVariable("pkId") String pkId) {
        RptCustomerOwnerStr rptCustomerOwnerStr = rptCustomerOwnerStrService.selectByPrimaryKey(pkId);
        return new ResultDto<RptCustomerOwnerStr>(rptCustomerOwnerStr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCustomerOwnerStr> create(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr) throws URISyntaxException {
        rptCustomerOwnerStrService.insert(rptCustomerOwnerStr);
        return new ResultDto<RptCustomerOwnerStr>(rptCustomerOwnerStr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr) throws URISyntaxException {
        int result = rptCustomerOwnerStrService.update(rptCustomerOwnerStr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptCustomerOwnerStrService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptCustomerOwnerStrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptCustomerOwnerStr>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptCustomerOwnerStr>>(rptCustomerOwnerStrService.selectByModel(model));
    }

    @PostMapping("/updateCustomerOwnerStr")
    protected ResultDto<Integer> updateCustomerOwnerStr(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr){
        return new ResultDto<Integer>(rptCustomerOwnerStrService.updateSelective(rptCustomerOwnerStr));
    }
    @PostMapping("/insertCustomerOwnerStr")
    protected ResultDto<Integer> insertCustomerOwnerStr(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr){
        rptCustomerOwnerStr.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptCustomerOwnerStrService.insertSelective(rptCustomerOwnerStr));
    }

    @PostMapping("/deleteCustomerOwnerStr")
    protected ResultDto<Integer> deleteCustomerOwnerStr(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr){
        String pkId = rptCustomerOwnerStr.getPkId();
        return new ResultDto<Integer>(rptCustomerOwnerStrService.deleteByPrimaryKey(pkId));
    }
    @PostMapping("/initOwner")
    protected ResultDto<List<RptCustomerOwnerStr>> initOwner(@RequestBody Map map){
        return new ResultDto<List<RptCustomerOwnerStr>>(rptCustomerOwnerStrService.initOwner(map));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptCustomerOwnerStr rptCustomerOwnerStr){
        return new ResultDto<Integer>(rptCustomerOwnerStrService.save(rptCustomerOwnerStr));
    }
}
