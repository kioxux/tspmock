/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherCnyRateApprSub
 * @类描述: other_cny_rate_appr_sub数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-16 10:24:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_appr_sub")
public class OtherCnyRateApprSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 审批流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "APPROVE_SERNO")
	private String approveSerno;
	
	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = false, length = 40)
	private String subSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 分项品种流水号 **/
	@Column(name = "SUB_PRD_SERNO", unique = false, nullable = false, length = 40)
	private String subPrdSerno;
	
	/** 分项额度品种编号 **/
	@Column(name = "ACC_SUB_PRD_NO", unique = false, nullable = false, length = 40)
	private String accSubPrdNo;
	
	/** 分项额度品种名称 **/
	@Column(name = "ACC_SUB_PRD_NAME", unique = false, nullable = true, length = 80)
	private String accSubPrdName;
	
	/** 批准贷款利率 **/
	@Column(name = "APPR_LOAN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal apprLoanRate;
	
	/** 批准定价方式 **/
	@Column(name = "APPR_RATE_TYPE", unique = false, nullable = true, length = 5)
	private String apprRateType;
	
	/** LPR利率 **/
	@Column(name = "LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lprRate;
	
	/** 利率调整周期 **/
	@Column(name = "RATE_ADJUST_CYCLE", unique = false, nullable = true, length = 5)
	private String rateAdjustCycle;
	
	/** 固定日期 **/
	@Column(name = "FIXED_DATE", unique = false, nullable = true, length = 20)
	private String fixedDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}
	
    /**
     * @return approveSerno
     */
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subPrdSerno
	 */
	public void setSubPrdSerno(String subPrdSerno) {
		this.subPrdSerno = subPrdSerno;
	}
	
    /**
     * @return subPrdSerno
     */
	public String getSubPrdSerno() {
		return this.subPrdSerno;
	}
	
	/**
	 * @param accSubPrdNo
	 */
	public void setAccSubPrdNo(String accSubPrdNo) {
		this.accSubPrdNo = accSubPrdNo;
	}
	
    /**
     * @return accSubPrdNo
     */
	public String getAccSubPrdNo() {
		return this.accSubPrdNo;
	}
	
	/**
	 * @param accSubPrdName
	 */
	public void setAccSubPrdName(String accSubPrdName) {
		this.accSubPrdName = accSubPrdName;
	}
	
    /**
     * @return accSubPrdName
     */
	public String getAccSubPrdName() {
		return this.accSubPrdName;
	}
	
	/**
	 * @param apprLoanRate
	 */
	public void setApprLoanRate(java.math.BigDecimal apprLoanRate) {
		this.apprLoanRate = apprLoanRate;
	}
	
    /**
     * @return apprLoanRate
     */
	public java.math.BigDecimal getApprLoanRate() {
		return this.apprLoanRate;
	}
	
	/**
	 * @param apprRateType
	 */
	public void setApprRateType(String apprRateType) {
		this.apprRateType = apprRateType;
	}
	
    /**
     * @return apprRateType
     */
	public String getApprRateType() {
		return this.apprRateType;
	}
	
	/**
	 * @param lprRate
	 */
	public void setLprRate(java.math.BigDecimal lprRate) {
		this.lprRate = lprRate;
	}
	
    /**
     * @return lprRate
     */
	public java.math.BigDecimal getLprRate() {
		return this.lprRate;
	}
	
	/**
	 * @param rateAdjustCycle
	 */
	public void setRateAdjustCycle(String rateAdjustCycle) {
		this.rateAdjustCycle = rateAdjustCycle;
	}
	
    /**
     * @return rateAdjustCycle
     */
	public String getRateAdjustCycle() {
		return this.rateAdjustCycle;
	}
	
	/**
	 * @param fixedDate
	 */
	public void setFixedDate(String fixedDate) {
		this.fixedDate = fixedDate;
	}
	
    /**
     * @return fixedDate
     */
	public String getFixedDate() {
		return this.fixedDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}