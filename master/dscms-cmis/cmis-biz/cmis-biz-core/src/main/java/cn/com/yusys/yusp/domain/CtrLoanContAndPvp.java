package cn.com.yusys.yusp.domain;

public class CtrLoanContAndPvp {
    private String contNo;
    private String contStatus;
    private String pvpSerno;
    private String approveStatus;

    @Override
    public String toString() {
        return "CtrLoanContAndPvp{" +
                "contNo='" + contNo + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                '}';
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }
}
