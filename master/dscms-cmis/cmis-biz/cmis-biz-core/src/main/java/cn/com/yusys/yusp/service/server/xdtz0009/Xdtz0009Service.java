package cn.com.yusys.yusp.service.server.xdtz0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0009.req.Xdtz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.resp.Xdtz0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:查询指定票号在信贷台账中是否已贴现
 *
 * @author leehuang
 * @version 1.0
 */
@Service

public class Xdtz0009Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0009Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：xdtz0009
     * 交易描述：查询客户经理不良率
     *
     * @param xdtz0009DataReqDo
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0009DataRespDto xdtz0009(Xdtz0009DataReqDto xdtz0009DataReqDo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataReqDo));
        Xdtz0009DataRespDto xdtz0009DataRespDto = new Xdtz0009DataRespDto();//响应Data：查询客户经理不良率
        String managerId = xdtz0009DataReqDo.getManagerId();//客户经理工号
        Map queryMap = new HashMap();
        queryMap.put("manager_id", managerId);//客户号
        logger.info("查询客户经理不良率开始,查询参数为:{}", JSON.toJSONString(queryMap));
        BigDecimal badRate = accLoanMapper.queryBadRate(queryMap);
        logger.info("查询客户经理不良率结束,返回结果为:{}", JSON.toJSONString(badRate));
        xdtz0009DataRespDto.setBadRate(badRate);//不良率
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataRespDto));
        return xdtz0009DataRespDto;
    }


}
