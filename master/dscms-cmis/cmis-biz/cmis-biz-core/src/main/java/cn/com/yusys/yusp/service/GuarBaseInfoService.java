/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.XdjzzyReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.resp.XdjzzyRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.req.YpztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.YpztcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.BusconRespDto;
import cn.com.yusys.yusp.dto.client.http.ypxt.callguar.CallGuarReqDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.util.CmisBizGuarUtils;
import cn.com.yusys.yusp.util.PUBUtilTools;
import cn.com.yusys.yusp.vo.GuarBasicBuildingVo;
import cn.com.yusys.yusp.vo.GuarBasicCargoPledgeVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfBuildUseVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfBusinessIndustryHousrVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfLivingRoomVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfMachEquiVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfOtherHouseVo;
import cn.com.yusys.yusp.vo.GuarBasicGuarInfUsufLandVo;
import cn.com.yusys.yusp.vo.GuarBasicInfoVo;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-12-01 21:10:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class GuarBaseInfoService {
    private static final Logger log = LoggerFactory.getLogger(GuarBaseInfoService.class);
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Resource
    private GuarInfBuilProjectMapper guarInfBuilProjectMapper;

    @Resource
    private GuarInfCargoPledgeMapper guarInfCargoPledgeMapper;

    @Resource
    private GuarInfBuildUseMapper guarInfBuildUseMapper;

    @Resource
    private GuarInfBusinessIndustryHousrMapper guarInfBusinessIndustryHousrMapper;

    @Resource
    private GuarInfLivingRoomMapper guarInfLivingRoomMapper;

    @Resource
    private GuarInfMachEquiMapper guarInfMachEquiMapper;

    @Resource
    private GuarInfOtherHouseMapper guarInfOtherHouseMapper;

    @Resource
    private GuarInfUsufLandMapper guarInfUsufLandMapper;

    @Resource
    private RptLmtRepayAnysGuarPldDetailMapper rptLmtRepayAnysGuarPldDetailMapper;

    @Autowired
    private GuarInfBuilProjectService guarInfBuilProjectService;

    @Autowired
    private GuarInfCargoPledgeService guarInfCargoPledgeService;

    @Autowired
    private GuarInfBuildUseService guarInfBuildUseService;

    @Autowired
    private GuarInfBusinessIndustryHousrService guarInfBusinessIndustryHousrService;

    @Autowired
    private GuarInfLivingRoomService guarInfLivingRoomService;

    @Autowired
    private GuarInfMachEquiService guarInfMachEquiService;

    @Autowired
    private GuarInfOtherHouseService guarInfOtherHouseService;

    @Autowired
    private GuarInfUsufLandService guarInfUsufLandService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService; // 放款申请表

    @Autowired
    private CtrLoanContService ctrLoanContService; // 合同主表

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private LmtApprService lmtApprService;
    @Autowired
    private GuarBizRelService guarBizRelService;
    @Resource
    private GrtGuarContRelMapper grtGuarContRelMapper;


    @Value("${application.ypxtUrl.url}")
    private String url;
//private String url = "http://10.85.10.31:7001/cms/callPage.do";


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public GuarBaseInfo selectByPrimaryKey(String serno) {
        return guarBaseInfoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<GuarBaseInfo> selectAll(QueryModel model) {
        List<GuarBaseInfo> records = (List<GuarBaseInfo>) guarBaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarBaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GuarBaseInfo record) {
        return guarBaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GuarBaseInfo record) {
        return guarBaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GuarBaseInfo record) {
        return guarBaseInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GuarBaseInfo record) {
        return guarBaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarBaseInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarBaseInfoMapper.deleteByIds(ids);
    }

    /**
     * 获取押品信息
     *
     * @param guarBaseInfoDto
     * @return
     */
    public List<GuarBaseInfoDto> getGuarBaseInfo(GuarBaseInfoDto guarBaseInfoDto) {
        return guarBaseInfoMapper.getGuarBaseInfo(guarBaseInfoDto);
    }

    /**
     * 根据押品统一代码获取押品基本和担保分类信息
     *
     * @param guarNo
     * @return
     */
    public GuarBaseInfoDto getGuarBaseAndGuarClassByGuarNo(String guarNo) {
        return guarBaseInfoMapper.getGuarBaseAndGuarClassByGuarNo(guarNo);
    }

    /**
     * 押品确认匹配
     *
     * @param bizGuarExchangeDto
     * @return
     */
    public BizGuarExchangeDto guarConfirMatching(BizGuarExchangeDto bizGuarExchangeDto) {
        return null;//// TODO 仰雪龙确认逻辑 cmisBizClientService.saveGrtGuarContRelInter(bizGuarExchangeDto);
    }

    /**
     * 删除押品信息
     *
     * @param bizGuarExchangeDto
     * @return
     */
    public int deleteGuar(BizGuarExchangeDto bizGuarExchangeDto) {
/*        //1、向BIZ服务请求查询该押品是否有存在的业务/授信/借据相关联的数据
        BizGuarExchangeDto rsBizGuarExchangeDto = cmisBizClientService.queryGrtGuarContRelInter(bizGuarExchangeDto);
        if(rsBizGuarExchangeDto != null && rsBizGuarExchangeDto.isExistsFlag()){
            log.info("该押品【押品编号：{}】存在业务/授信/借据相关联的数据，无法删除。",rsBizGuarExchangeDto.getGuarNo());
            throw new YuspException(EcbEnum.E_GUAR_BASE_CHECK_DELETE.key,EcbEnum.E_GUAR_BASE_CHECK_DELETE.value);
        }
        return guarBaseInfoMapper.deleteGuarByGuarNo(bizGuarExchangeDto.getGuarNo());*/
        //根据押品所在业务阶段判断是否存在业务/授信/借据相关联的数据
        GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.selectByPrimaryKey(bizGuarExchangeDto.getGuarNo());
        if (guarBaseInfo != null) {
            String guarBusistate = guarBaseInfo.getGuarBusistate();
            if (!CmisBizConstants.GUAR_BUSISTATE_01.equals(guarBusistate)) {
                log.info("该押品【押品编号：{}】存在业务/授信/借据相关联的数据，无法删除。", bizGuarExchangeDto.getGuarNo());
                throw new YuspException(EcbEnum.E_GUAR_BASE_CHECK_DELETE.key, EcbEnum.E_GUAR_BASE_CHECK_DELETE.value);
            }
        }
        //需做关联子表删除待开发
        return guarBaseInfoMapper.deleteGuarByGuarNo(bizGuarExchangeDto.getGuarNo());
    }

    /**
     * 获取满足当前查询条件的押品信息(权证出入库申请)
     *
     * @param map
     * @return
     */
    @Transactional(readOnly = true)
    public GuarClientRsDto getGuarBaseInfoByParams(Map map) {
        GuarClientRsDto guarClientRsDto = new GuarClientRsDto();
        guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_SUCCESS);
        guarClientRsDto.setRtnMsg("获取权证查询信息成功");
        String guarTypeCd = (String) map.get("guarTypeCd");
        String pldimnMemo = (String) map.get("pldimnMemo");
        String guarNo = (String) map.get("guarNo");
        if (StringUtils.nonBlank(guarTypeCd) || StringUtils.nonBlank(pldimnMemo) || StringUtils.nonBlank(guarNo)) {
            List<GuarBaseInfoDto> guarBaseInfoDtos = guarBaseInfoMapper.selectGuarBaseInfoByParams(map);
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_SUCCESS);
            guarClientRsDto.setRtnMsg("查询成功");
            guarClientRsDto.setData(guarBaseInfoDtos);
        } else {
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_FAIL);
            guarClientRsDto.setRtnMsg("查询失败");
        }
        return guarClientRsDto;
    }

    /**
     * 接口调用更新押品所在业务阶段
     *
     * @param guarBaseInfoClientDto
     * @return
     */

    public GuarClientRsDto updateGuarBusistate(GuarBaseInfoClientDto guarBaseInfoClientDto) {
        GuarClientRsDto guarClientRsDto = new GuarClientRsDto();
        int count = 0;
        String opeType = guarBaseInfoClientDto.getOpeType();
        GuarBaseInfoClientDto guarBaseInfoClientDtos = new GuarBaseInfoClientDto();
        List<GuarBaseInfoClientDto> list = guarBaseInfoClientDto.getList();
        if (CollectionUtils.isEmpty(list) || list.size() == 0) {
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_FAIL);
            guarClientRsDto.setRtnMsg("接口返回null！");
            return guarClientRsDto;
        }
        for (GuarBaseInfoClientDto guarBaseInfo : list) {
            guarBaseInfoClientDtos.setGuarNo(guarBaseInfo.getGuarNo());
            guarBaseInfoClientDtos.setGuarBusistate(guarBaseInfo.getGuarBusistate());
            count += guarBaseInfoMapper.updateGuarBusistate(guarBaseInfoClientDtos);
        }
        if (count <= 0) {
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_FAIL);
            guarClientRsDto.setRtnMsg("更新失败,更新条数为0");
            return guarClientRsDto;
        }
        guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_SUCCESS);
        guarClientRsDto.setRtnMsg("更新成功,更新条数为" + count);
        guarClientRsDto.setData(count);
        return guarClientRsDto;
    }

    /**
     * 更新押品所在业务阶段
     *
     * @param guarBaseInfoClientDto
     * @return
     */

    public GuarClientRsDto updateGuarBusistateInfo(GuarBaseInfoClientDto guarBaseInfoClientDto) {
        GuarClientRsDto guarClientRsDto = new GuarClientRsDto();
        int count = 0;
        String opeType = guarBaseInfoClientDto.getOpeType();
        if (CmisBizConstants.OPE_TYPE_ONRESET.equals(opeType)) {
            BizGuarExchangeDto bizGuarExchangeDto = new BizGuarExchangeDto();
            String guarNo = guarBaseInfoClientDto.getGuarNo();
            bizGuarExchangeDto.setGuarNo(guarNo);
            log.info("接口请求入参" + JSON.toJSONString(bizGuarExchangeDto));
            log.info("重置押品所在业务阶段" + guarBaseInfoClientDto + "调用业务端接口开始");
            BizGuarExchangeDto bizGuarExchangeDtos = null;// TODO 仰雪龙确认逻辑 cmisBizClientService.queryBizServiceStates(bizGuarExchangeDto);
            log.info("重置押品所在业务阶段" + guarBaseInfoClientDto + "调用业务端接口结束");
            if (bizGuarExchangeDtos == null) {
                throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
            }
            if (CmisBizConstants.GUAR_CLIENT_RETURN_CODE_FAIL.equals(bizGuarExchangeDtos.getRtnCode())) {
                GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                guarBaseInfo.setGuarBusistate(bizGuarExchangeDtos.getGuarBusistate());
                guarBaseInfo.setGuarNo(guarNo);
                guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                count = guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            } else {
                throw new YuspException(EcbEnum.GRT_CHECK_IGBANOTEXISTS_EXCEPTION.key, EcbEnum.GRT_CHECK_IGBANOTEXISTS_EXCEPTION.value);
            }
        }
        if (count <= 0) {
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_FAIL);
            guarClientRsDto.setRtnMsg("更新失败,更新条数为0");
        }
        if (count > 0) {
            guarClientRsDto.setRtnCode(CmisBizConstants.GUAR_CLIENT_RETURN_CODE_SUCCESS);
            guarClientRsDto.setRtnMsg("更新成功,更新条数为" + count);
            guarClientRsDto.setData(count);
        }
        return guarClientRsDto;
    }

    /*    *//**
     * 更新押品所在业务阶段
     * @param guarRelotInfoClientDto
     * @return
     *//*
    public  List<GuarEvalRelotResultDto> getGuarRelotInfo(GuarRelotInfoClientDto guarRelotInfoClientDto){
        List<GuarEvalRelotResultDto> list = guarBaseInfoMapper.getGuarRelotInfo(guarRelotInfoClientDto);
        return list;
    }*/

    /**
     * 通过主键逻辑删除
     *
     * @param pkId
     * @return
     */
    public int deleteOnLogic(String pkId) {
        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
        guarBaseInfo.setSerno(pkId);
        guarBaseInfo.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
    }

    /**
     * 获取押品系统Url
     *
     * @param
     * @return
     */
    public String getGuarInfoUrl(GuarBaseInfoDto guarBaseInfoDto) {
        CmisBizGuarUtils cmisGuarUtils = new CmisBizGuarUtils();
        CallGuarReqDto callGuarReqDto = new CallGuarReqDto();
        String callMethod = PUBUtilTools.getString(guarBaseInfoDto.getCallMethod(), true);
        //菜单标示:初估
        callGuarReqDto.setCallMethod(callMethod);
        //url
        log.info("押品系统URL为[{}]", url);
        callGuarReqDto.setPrefixUrl(url);
        //请求参数
        Map<String, Object> parameter = new HashMap<>();
        //客户经理进行押品价值的初估
        if (DscmsBizDbEnum.CALLMETHOD_TGUARFIRSTEVAL.key.equals(callMethod)) {
            parameter.put("SystemFlag", DscmsBizDbEnum.SYSTEMFLAG_01.key);//系统标识
            parameter.put("OperUser", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//当前操作用户ID
            parameter.put("OperOrg", PUBUtilTools.getString(guarBaseInfoDto.getManagerBrId(), true));//当前操作用户所属机构
            parameter.put("GuarNo", PUBUtilTools.getString(guarBaseInfoDto.getGuarNo(), true));//押品统一编号
            //重估
        } else if (DscmsBizDbEnum.CALLMETHOD_TGUARREEVALAPPLY.key.equals(callMethod)) {
            parameter.put("SystemFlag", DscmsBizDbEnum.SYSTEMFLAG_01.key);//系统标识
            parameter.put("OperUser", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//当前操作用户ID
            parameter.put("OperOrg", PUBUtilTools.getString(guarBaseInfoDto.getManagerBrId(), true));//当前操作用户所属机构
            parameter.put("GuarNo", PUBUtilTools.getString(guarBaseInfoDto.getGuarNo(), true));//押品统一编号
            //查看押品详细信息界面
        } else if (DscmsBizDbEnum.CALLMETHOD_TGUARDETAILINFO.key.equals(callMethod)) {
            parameter.put("SystemNo", DscmsBizDbEnum.SYSTEMNO_01.key);//请求的系统标识
            parameter.put("OperUser", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//当前操作用户ID
            parameter.put("OperOrg", PUBUtilTools.getString(guarBaseInfoDto.getManagerBrId(), true));//当前操作用户所属机构
            parameter.put("GuarNo", PUBUtilTools.getString(guarBaseInfoDto.getGuarNo(), true));//押品统一编号
            parameter.put("DataFlag", DscmsBizDbEnum.DATAFLAG_01.key);//数据修改标识
            parameter.put("PageMark", DscmsBizDbEnum.PAGEMARK_01.key);//页面跳转标识
            if(guarBaseInfoDto.getSerno()!=null){
                parameter.put("BusNo", PUBUtilTools.getString(guarBaseInfoDto.getSerno(), true));//业务关联流水号
            }
        } else if (DscmsBizDbEnum.CALLMETHOD_TGUAREXISTQRY.key.equals(callMethod)) {
            //新增界面
            parameter.put("SystemNo", DscmsBizDbEnum.SYSTEMNO_01.key);//请求的系统标识
            parameter.put("OperUser", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//当前操作用户ID
            parameter.put("OperOrg", PUBUtilTools.getString(guarBaseInfoDto.getManagerBrId(), true));//当前操作用户所属机构
            parameter.put("GuarWay", PUBUtilTools.changeGuarWay(PUBUtilTools.getString(guarBaseInfoDto.getGrtFlag(), true)));//担保方式标识
            parameter.put("BusNo", PUBUtilTools.getString(guarBaseInfoDto.getSerno(), true));//业务关联流水号
            parameter.put("GrtFlg", DscmsBizDbEnum.GRTFLG_1.key);//主担保或副担保
            parameter.put("Usage", "");//使用情况
            parameter.put("Propert", "");//物业情况
            parameter.put("EvalAmt", "");//评估金额
            parameter.put("Area", "");//面积
            parameter.put("CustNo", "");//押品所有人编号
            parameter.put("EvalType", DscmsBizDbEnum.EVALTYPE_02.key);//评估方式
            parameter.put("Address", "");//
            //parameter.put("BorrowNo", PUBUtilTools.getString(guarBaseInfoDto.getBorrowNo()));//保证人时为担保合同借款人编号
            parameter.put("BorrowNo", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//传入客户经理编号
        } else if (DscmsBizDbEnum.CALLMETHOD_TGUARUPDATE.key.equals(callMethod)) {
            //修改界面
            parameter.put("SystemFlag", DscmsBizDbEnum.SYSTEMFLAG_01.key);//系统标识
            parameter.put("OperUser", PUBUtilTools.getString(guarBaseInfoDto.getManagerId(), true));//当前操作用户ID
            parameter.put("OperOrg", PUBUtilTools.getString(guarBaseInfoDto.getManagerBrId(), true));//当前操作用户所属机构
            parameter.put("GuarNo", PUBUtilTools.getString(guarBaseInfoDto.getGuarNo(), true));//押品统一编号
            parameter.put("BusNo", PUBUtilTools.getString(guarBaseInfoDto.getSerno(), true));//业务关联流水号
            parameter.put("DataFlag", DscmsBizDbEnum.DATAFLAG_01.key);//数据修改标识
            parameter.put("PageMark", DscmsBizDbEnum.PAGEMARK_01.key);//页面跳转标识
        } else {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"");
        }
        callGuarReqDto.setParameter(parameter);
        String callguarUrl = cmisGuarUtils.callguar(callGuarReqDto);
        return callguarUrl;
    }

//    /**
//     * 获取影像系统Url
//     * @param
//     * @return
//     */
//    public String getImageSysUrl(CallImageReqDto callImageReqDto){
//        callImageReqDto = new CallImageReqDto();
//        callImageReqDto.setPrefixUrl("http://10.28.122.198/image.html?");
//        Para para = new Para();
//        Map<String, Object> index = new HashMap<>();
//        index.put("contid", "testcontid");
//        para.setIndex(index);
//        para.setTop_outsystem_code("CMIS");
//        para.setOutsystem_code("xxd");
//        callImageReqDto.setPara(para);
//        callImageReqDto.setAuthorization("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTYxOTQyNDIwMH0.6m2e_IwZBmYcZzV5iIxyF7uf9knd_Jx_c24-YjAP6KxGIyhIZOpSUhuYUqcF0b7z1QUCFvD21DaLUIq1L23o7Q");
//        callImageReqDto.setS(DscmsBizZxEnum.IMAGE_S_1.key);
//        callImageReqDto.setAuthority("download;scan;import;delImg;insert;replace;copy;cut;setValidDate;order");
//        String callimage = CmisBizImageUtils.callimage(callImageReqDto);
//        return callimage;
//    }


    /**
     * 对GuarBaseInfo单个对象保存(新增或修改),如果没有数据新增,有数据修改
     *
     * @param guarBaseInfo
     * @return
     */
    public int preserveGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        String serno = guarBaseInfo.getSerno();
        //查询该对象是否存在
        GuarBaseInfo queryGuarBaseInfo = guarBaseInfoMapper.selectByPrimaryKey(serno);
        if (null == queryGuarBaseInfo) {
            return guarBaseInfoMapper.insert(guarBaseInfo);
        } else {
            return guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
        }
    }

    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明: 存在 为 1 ,不存在为 0
     * @算法描述:
     */
    public int checkGuarInfoIsExist(String serno) {
        GuarBaseInfo queryGuarBaseInfo = guarBaseInfoMapper.selectByPrimaryKey(serno);
        return (null == queryGuarBaseInfo || null == queryGuarBaseInfo.getSerno() || "".equals(queryGuarBaseInfo.getSerno())) ? 0 : 1;
    }


    /**
     * @方法名称: selectByGuarContNoModel
     * @方法描述: 根据担保合同编号条件查询 ，若需要分页将分页内容写在map中
     * @参数与返回说明:guarContNo 担保合同号，map分页
     * @算法描述: 无
     */
    public List<GuarBaseInfo> selectByGuarContNoModel(String guarContNo,Map map) {
        List <GuarBaseInfo> list = new ArrayList<GuarBaseInfo>();
        List<String> guarNos = new ArrayList<String>();
        if(!StringUtils.isEmpty(guarContNo)){
            guarNos = grtGuarContRelMapper.selectGuarNoByGuarContNo(guarContNo);
        }
        if(guarNos!=null && guarNos.size()>0 && guarNos.get(0)!=null){
            QueryModel model = new QueryModel();
            model.addCondition("guarNos", guarNos);
            if(map!=null){//分页
                int page = Integer.parseInt(Objects.toString(map.get("page")));
                int size = Integer.parseInt(Objects.toString(map.get("size")));
                PageHelper.startPage(page, size);
                list = guarBaseInfoMapper.selectByGuarContNoModel(model);
                PageHelper.clearPage();
            }else{//不分页
                list = guarBaseInfoMapper.selectByGuarContNoModel(model);
            }

        }

        return list;
    }

    /**
     * @方法名称: saveByGuarBasicBuildingVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicBuildingVo(GuarBasicBuildingVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfBuilProject guarInfBuilProject = record.getGuarInfBuilProject();
        int numGuarInfBuilProject = guarInfBuilProjectService.checkGuarInfoIsExist(guarInfBuilProject.getSerno());

        if (0 < numGuarInfBuilProject) {
            guarInfBuilProjectService.update(guarInfBuilProject);
        } else {
            guarInfBuilProjectMapper.insert(guarInfBuilProject);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo){
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else {
            //在建工程,k1_val取建设工程规划许可证号
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicCargoPledgeVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicCargoPledgeVo(GuarBasicCargoPledgeVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfCargoPledge guarInfCargoPledge = record.getGuarInfCargoPledge();
        int numGuarInfBuilProject = guarInfCargoPledgeService.checkGuarInfoIsExist(guarInfCargoPledge.getSerno());
        if (0 < numGuarInfBuilProject) {
            guarInfCargoPledgeService.update(guarInfCargoPledge);
        } else {
            guarInfCargoPledgeMapper.insert(guarInfCargoPledge);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else{
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }


        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicGuarInfBuildUseVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicGuarInfBuildUseVo(GuarBasicGuarInfBuildUseVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfBuildUse guarInfBuildUse = record.getGuarInfBuildUse();
        int numGuarInfBuilProject = guarInfBuildUseService.checkGuarInfoIsExist(guarInfBuildUse.getSerno());

        if (0 < numGuarInfBuilProject){
            guarInfBuildUseService.update(guarInfBuildUse);
        } else {
            guarInfBuildUseMapper.insert(guarInfBuildUse);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else {
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByInfBusinessIndustryHousrVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByInfBusinessIndustryHousrVo(GuarBasicGuarInfBusinessIndustryHousrVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr = record.getGuarInfBusinessIndustryHousr();
        int numGuarInfBuilProject = guarInfBusinessIndustryHousrService.checkGuarInfoIsExist(guarInfBusinessIndustryHousr.getSerno());

        if (0 < numGuarInfBuilProject){
            guarInfBusinessIndustryHousrMapper.updateByPrimaryKey(guarInfBusinessIndustryHousr);
        } else {
            guarInfBusinessIndustryHousrMapper.insert(guarInfBusinessIndustryHousr);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else {
            //TODO
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),guarInfBusinessIndustryHousr.getBusinessHouseNo());
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicGuarInfLivingRoomVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicGuarInfLivingRoomVo(GuarBasicGuarInfLivingRoomVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfLivingRoom guarInfLivingRoom = record.getGuarInfLivingRoom();
        int numGuarInfBuilProject = guarInfLivingRoomService.checkGuarInfoIsExist(guarInfLivingRoom.getSerno());

        if (0 < numGuarInfBuilProject) {
            guarInfLivingRoomMapper.updateByPrimaryKey(guarInfLivingRoom);
        } else {
            guarInfLivingRoomMapper.insert(guarInfLivingRoom);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else {
            //TODO
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicGuarInfMachEquiVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicGuarInfMachEquiVo(GuarBasicGuarInfMachEquiVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfMachEqui guarInfMachEqui = record.getGuarInfMachEqui();
        int numGuarInfBuilProject = guarInfMachEquiService.checkGuarInfoIsExist(guarInfMachEqui.getSerno());

        if (0 < numGuarInfBuilProject) {
            guarInfMachEquiMapper.updateByPrimaryKey(guarInfMachEqui);
        }else {
            guarInfMachEquiMapper.insert(guarInfMachEqui);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else {
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicGuarInfOtherHouseVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicGuarInfOtherHouseVo(GuarBasicGuarInfOtherHouseVo record) {
        //TODO..修改封装逻辑
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfOtherHouse guarInfOtherHouse = record.getGuarInfOtherHouse();
        int numGuarInfBuilProject = guarInfOtherHouseService.checkGuarInfoIsExist(guarInfOtherHouse.getSerno());

        if (0 < numGuarInfBuilProject){
            guarInfOtherHouseMapper.updateByPrimaryKey(guarInfOtherHouse);
        } else {
            guarInfOtherHouseMapper.insert(guarInfOtherHouse);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else{
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),guarInfOtherHouse.getBusinessHouseNo());
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @方法名称: saveByGuarBasicGuarInfUsufLandVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarBasicGuarInfUsufLandVo(GuarBasicGuarInfUsufLandVo record) {
        ResultDto resultDto = new ResultDto("0", "保存成功");

        GuarInfUsufLand guarInfUsufLand = record.getGuarInfUsufLand();
        int numGuarInfBuilProject = guarInfUsufLandService.checkGuarInfoIsExist(guarInfUsufLand.getSerno());

        if (0 < numGuarInfBuilProject){
            guarInfUsufLandMapper.updateByPrimaryKey(guarInfUsufLand);
        } else {
            guarInfUsufLandMapper.insert(guarInfUsufLand);
        }

        GuarBaseInfo guarBaseInfo = record.getGuarBaseInfo();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarBaseInfo.getSerno());

        if (0 < numGuarBaseInfo) {
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
            guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_02,"","");
        }else{
            String guarNo = guarInfoSyn(guarBaseInfo,CmisBizConstants.OPER_FLAG_01,guarBaseInfo.getRightOtherNo(),"");
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarBaseInfoMapper.insert(guarBaseInfo);
        }
        return resultDto;
    }

    /**
     * @函数名称:querFddGuarinforel
     * @函数描述:获取业务与押品关系表当前业务流水号下optype为01,02的抵押物信息
     * @算法描述:
     * @创建者：zhnagliang15
     * @参数与返回说明: 押品信息
     */
    public List<GuarBizRelGuarBaseDto> querFddGuarinforel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBizRelGuarBaseDto> guarBaseInfo = guarBaseInfoMapper.querFddGuarinforel(queryModel);
        PageHelper.clearPage();
        return guarBaseInfo;
    }


    /**
     * @函数名称:queryGuarInfoSellNoPage
     * @函数描述:根据业务流水号查询押品信息
     * @算法描述:
     * @参数与返回说明: 押品信息
     */
    public List<GuarBizRelGuarBaseDto> queryGuarInfoSell(QueryModel queryModel) {

        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBizRelGuarBaseDto> guarBaseInfo = guarBaseInfoMapper.selectByIqpSernoModel(queryModel);
        PageHelper.clearPage();
        return guarBaseInfo;
    }

    /**
     * @函数名称:queryGuarInfoSellNoPage
     * @函数描述:根据业务流水号查询押品信息
     * @算法描述:
     * @参数与返回说明: 押品信息
     */
    public List<GuarBizRelGuarBaseDto> queryGuarInfoSellNoPage(QueryModel queryModel) {
        List<GuarBizRelGuarBaseDto> guarBaseInfo = guarBaseInfoMapper.selectByIqpSernoModel(queryModel);
        return guarBaseInfo;
    }

    /**
     * @方法名称: selectDetailsByGuarContNoModel
     * @方法描述: 根据担保合同编号条件查询未关联当前抵押登记申请的押品明细 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarBaseInfo> selectDetailsByGuarContNoModel(String guarContNo) {
        QueryModel model = new QueryModel();
        model.addCondition("guarContNo", guarContNo);
        model.addCondition("optype", "01");
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectByGuarContNoModel(model);//此方法未被调用不做改造
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByGuarContNoModel
     * @方法描述: 根据担保合同编号条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarBaseInfo> selectByGuarContNoModel(GuarBasicInfoVo guarBasicInfoVo) {
        QueryModel model = new QueryModel();
        model.addCondition("guarContNo", guarBasicInfoVo.getGuarContNo());
        model.addCondition("oprType", guarBasicInfoVo.getOprType());
        model.addCondition("regSerno", guarBasicInfoVo.getRegSerno());
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectDetailsByGuarContNoModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：queryBaseInfoByGuarId
     * @方法描述：根据担保号获取押品信息
     * @创建人：zhangming12
     * @创建时间：2021/5/15 10:38
     * @修改记录：修改时间 修改人员 修改时间
     */
    public GuarBaseInfo queryBaseInfoByGuarId(String guarId) {
        if (StringUtils.isBlank(guarId)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.queryBaseInfoByGuarId(guarId);
        return guarBaseInfo;
    }

    /**
     * @函数名称:getGuarBaseInfoByGuarNo
     * @函数描述:根据押品编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    public GuarBaseInfo getGuarBaseInfoByGuarNo(String guarNo) {
        if (StringUtils.isBlank(guarNo)) {
            throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value + ",传入参数异常！");
        }
        GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.getGuarBaseInfoByGuarNo(guarNo);
        return guarBaseInfo;
    }

    /**
     * @函数名称:selectGuarBaseInfoByGuarContNo
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GuarBaseInfo> selectGuarBaseInfoByGuarContNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectGuarBaseInfoByGuarContNo(queryModel);
        PageHelper.clearPage();
        return guarBaseInfoList;
    }

    /**
     * @函数名称:selectGrtGuarContRelByGuarContNo
     * @函数描述:根据担保合同编号获取担保合同关联押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GuarBaseInfoDto2> selectGrtGuarContRelByGuarContNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfoDto2> guarBaseInfoDto2s = guarBaseInfoMapper.selectGrtGuarContRelByGuarContNo(queryModel);
        PageHelper.clearPage();
        return guarBaseInfoDto2s;
    }

    /**
     * @函数名称:selectGuarBaseInfoForMortgageLogOut
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GuarBaseInfoDto> selectGuarBaseInfoForMortgageLogOut(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfoDto> guarBaseInfoDtoList = guarBaseInfoMapper.selectGuarBaseInfoForMortgageLogOut(queryModel);
        return guarBaseInfoDtoList;
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 抵押信息创建通过处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.
     * @创建人: zhengfq
     * @创建时间: 2021-06-17
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno) {
        GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.selectByPrimaryKey(serno);
        guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        guarBaseInfoMapper.updateByPrimaryKey(guarBaseInfo);
    }

    /**
     * 押品信息同步押品系统
     *
     * @param guarBaseInfo
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 抵押信息创建通过处理
     * @参数与返回说明: 押品编号
     */
    public String guarInfoSyn(GuarBaseInfo guarBaseInfo,String operFlag,String k1Val,String k2Val) {
        String serno = guarBaseInfo.getSerno();
        //同步信息到押品系统获取押品编号
        log.info("流水号【"+serno+"】，同步信息到押品系统获取押品编号 开始");
        GuarBaseInfoDetailDto guarBaseInfoDetail = getGuarBaseInfoDetail(guarBaseInfo);

        //押品信息同步及引入
        XdjzzyReqDto xdjzzyReqDto = new XdjzzyReqDto();
        List<cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.List> XdjzzyList = new ArrayList<>();
        cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.List XdjzzyData = new cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.List();
        //基本信息
        XdjzzyData.setGuar_no("");

        if (CmisBizConstants.OPER_FLAG_02.equals(operFlag)){
            //修改押品信息时需要传押品编号
            if(StringUtils.isEmpty(guarBaseInfo.getGuarNo())){
                String guarNo = guarBaseInfoMapper.selectGuarNoBySerno(serno);
                XdjzzyData.setGuar_no(guarNo);
            }else{
                XdjzzyData.setGuar_no(guarBaseInfo.getGuarNo());
            }
        }
        //操作标识
        XdjzzyData.setOper_flag(operFlag);
        //流水号
        XdjzzyData.setBus_no(PUBUtilTools.getString(serno, true));
        //抵押物类型
        XdjzzyData.setGuar_type_cd(PUBUtilTools.getString(guarBaseInfo.getGuarTypeCd()));
        //抵押物类型名称
        XdjzzyData.setGuar_type_name(PUBUtilTools.getString(guarBaseInfo.getPldimnMemo()));
        //权证编号及其他编号
        XdjzzyData.setQuanzheng_no(PUBUtilTools.getString(guarBaseInfo.getRightOtherNo()));
        //是否共有财产
        XdjzzyData.setIs_common(PUBUtilTools.getString(guarBaseInfo.getCommonAssetsInd()));
        //是否权属清晰
        XdjzzyData.setIs_ownership_clear(PUBUtilTools.getString(guarBaseInfo.getIsOwnershipClear()));
        //是否需要办理保险
        XdjzzyData.setIs_insurance(PUBUtilTools.getString(guarBaseInfo.getInsuranceInd()));
        //是否实质正相关
        XdjzzyData.setIs_relation(PUBUtilTools.getString(guarBaseInfo.getRelationInt()));
        //法定优先受偿款（元）
        XdjzzyData.setLegal_pri_payment(PUBUtilTools.getString(guarBaseInfo.getLegalPriPayment()));
        //担保生效方式01 抵质押登记  02 移交占有生效  03 冻结/止付生效 99 其它
        XdjzzyData.setEffect_type(guarBaseInfo.getDefEffectType());
        //他行是否已设定担保权
        XdjzzyData.setIs_other_back_guar(PUBUtilTools.getString(guarBaseInfo.getOtherBackGuarInd()));
        //是否抵债资产
        XdjzzyData.setIs_deal(PUBUtilTools.getString(guarBaseInfo.getIsDebtAsset()));
        //抵质押物与借款人相关性
        XdjzzyData.setGuar_borrower_rela(PUBUtilTools.getString(guarBaseInfo.getPldimnDebitRelative()));
        //查封便利性
        XdjzzyData.setShut_down_conv(PUBUtilTools.getString(guarBaseInfo.getSupervisionConvenience()));
        //法律有效性
        XdjzzyData.setLegal_validity(PUBUtilTools.getString(guarBaseInfo.getLawValidity()));
        //抵质押品通用性
        XdjzzyData.setUniversality(PUBUtilTools.getString(guarBaseInfo.getPldimnCommon()));
        //抵质押品变现能力
        XdjzzyData.setAbility(PUBUtilTools.getString(guarBaseInfo.getPldimnCashability()));
        //价格波动性
        XdjzzyData.setPrice_volatility(PUBUtilTools.getString(guarBaseInfo.getPriceWave()));
        //管护人
        XdjzzyData.setAccount_manager(PUBUtilTools.getString(guarBaseInfo.getAccountManager()));
        //抵押住房是否权属人唯一住所 1是 0否
        XdjzzyData.setHouse_ownership(PUBUtilTools.getString(guarBaseInfoDetail.getHouseOwnershipInd()));
        //房屋产权期限信息（年）
        XdjzzyData.setHouse_pr_desc(PUBUtilTools.stringToBigDecimal(guarBaseInfoDetail.getHousePrDesc()));
        //楼盘（社区）名称
        XdjzzyData.setCommunity_name(PUBUtilTools.getString(guarBaseInfoDetail.getCommunityName()));
        //所属地段
        XdjzzyData.setArea_location(PUBUtilTools.getString(guarBaseInfoDetail.getBelongArea()));
        //周边环境
        XdjzzyData.setArr_env(PUBUtilTools.getString(guarBaseInfoDetail.getArrEnv()));
        //套型
        XdjzzyData.setF_sty(PUBUtilTools.getString(guarBaseInfoDetail.getfSty()));
        //房屋状态
        XdjzzyData.setHouse_sta(PUBUtilTools.getString(guarBaseInfoDetail.getHouseSta()));
        //房屋结构
        XdjzzyData.setHouse_structure(PUBUtilTools.getString(guarBaseInfoDetail.getHouseStructure()));
        //地面构造
        XdjzzyData.setGround_structure(PUBUtilTools.getString(guarBaseInfoDetail.getGroundStructure()));
        //屋顶构造
        XdjzzyData.setRoof_structure(PUBUtilTools.getString(guarBaseInfoDetail.getRoofStructure()));
        //公共配套
        XdjzzyData.setPublic_facilities(PUBUtilTools.getString(guarBaseInfoDetail.getPublicFacilities()));
        //装修状况
        XdjzzyData.setDecoration(PUBUtilTools.getString(guarBaseInfoDetail.getDecoration()));
        //平面布局
        XdjzzyData.setPlane_layout(PUBUtilTools.getString(guarBaseInfoDetail.getPlaneLayout()));
        //朝向
        XdjzzyData.setOrientations(PUBUtilTools.getString(guarBaseInfoDetail.getOrientations()));
        //通风采光
        XdjzzyData.setVentilation_and_lighting(PUBUtilTools.getString(guarBaseInfoDetail.getVentilationAndLighting()));
        //临街状况
        XdjzzyData.setStreet_situation(PUBUtilTools.getString(guarBaseInfoDetail.getStreetSituation()));
        //街道/村镇/路名
        XdjzzyData.setStreet(PUBUtilTools.getString(guarBaseInfoDetail.getStreet()));
        //物业情况
        XdjzzyData.setProperty(PUBUtilTools.getString(guarBaseInfoDetail.getPropertyCase()));
        //使用情况
        XdjzzyData.setUsage(PUBUtilTools.getString(guarBaseInfoDetail.getHouseProperty()));
        //车库/车位类型
        XdjzzyData.setCarport_type(PUBUtilTools.getString(guarBaseInfoDetail.getCarportType()));
        //该产权证所属房产是否全部抵押
        XdjzzyData.setIs_house_all_pledge(PUBUtilTools.getString(guarBaseInfoDetail.getHouseAllPledgeInd()));
        //工业房地产开发模式
        XdjzzyData.setIndustry_decelop_model(PUBUtilTools.getString(guarBaseInfoDetail.getIndustryDecelopModel()));
        //竣工日期
        XdjzzyData.setEnd_date(PUBUtilTools.getString(guarBaseInfoDetail.getEndDate()));
        //所属地理位置
        XdjzzyData.setLocated_position(PUBUtilTools.getString(guarBaseInfoDetail.getBelongArea()));
        //地是否均已抵押我行
        XdjzzyData.setIs_house_land_pledge(PUBUtilTools.getString(guarBaseInfoDetail.getHouseLandPledgeInd()));
        //墙壁结构
        XdjzzyData.setWall_structure(PUBUtilTools.getString(guarBaseInfoDetail.getWallStructure()));
        //是否包含土地
        XdjzzyData.setIs_full_land(PUBUtilTools.getString(guarBaseInfoDetail.getFullLand()));
        //所在/注册省
        XdjzzyData.setProvince_cd(PUBUtilTools.getString(guarBaseInfoDetail.getProvinceCd()));
        //所在/注册市
        XdjzzyData.setCity_cd(PUBUtilTools.getString(guarBaseInfoDetail.getCityCd()));
        //所在县（区）
        XdjzzyData.setCounty_cd(PUBUtilTools.getString(guarBaseInfoDetail.getCountyCd()));
        //房产类型
        XdjzzyData.setHouse_type(PUBUtilTools.getString(guarBaseInfoDetail.getRealproCls()));
        //土地说明
        XdjzzyData.setLand_explain(PUBUtilTools.getString(guarBaseInfoDetail.getLandExplain()));
        //闲置土地类型
        XdjzzyData.setLand_notinuse_type(PUBUtilTools.getString(guarBaseInfoDetail.getLandNotinuseType()));
        //土地所在地段情况
        XdjzzyData.setLand_p_info(PUBUtilTools.getString(guarBaseInfoDetail.getLandPInfo()));
        //宗地号
        XdjzzyData.setParcel_no(PUBUtilTools.getString(guarBaseInfoDetail.getParcelNo()));
        //是否有土地定着物
        XdjzzyData.setIs_land_up(PUBUtilTools.getString(guarBaseInfoDetail.getLandUp()));
        //定着物种类
        XdjzzyData.setLand_up_type(PUBUtilTools.getString(guarBaseInfoDetail.getLandUpType()));
        //地上建筑物项数
        XdjzzyData.setLand_build_amount(PUBUtilTools.integerToBigDecimal(guarBaseInfoDetail.getLandBuildAmount()));
        //地上定着物总面积（m2)
        XdjzzyData.setLand_up_all_area(PUBUtilTools.stringToBigDecimal(guarBaseInfoDetail.getLandUpAllArea()));
        //定着物所有权人名称
        XdjzzyData.setLand_up_ownership_name(PUBUtilTools.getString(guarBaseInfoDetail.getLandUpOwnershipName()));
        //定着物所有权人范围
        XdjzzyData.setLand_up_ownership_scope(PUBUtilTools.getString(guarBaseInfoDetail.getLandUpOwnershipScope()));
        //地上定着物说明
        XdjzzyData.setLand_up_explain(PUBUtilTools.getString(guarBaseInfoDetail.getLandUpExplain()));
        //买卖合同编号
        XdjzzyData.setBusiness_no(PUBUtilTools.getString(guarBaseInfoDetail.getBusinessHouseNo()));
        //林地名称
        XdjzzyData.setFoeest_right(PUBUtilTools.getString(guarBaseInfoDetail.getForestName()));
        //林种
        XdjzzyData.setFoeest_type(PUBUtilTools.getString(guarBaseInfoDetail.getForestVariet()));
        //主要树种
        XdjzzyData.setMain_tree_type(PUBUtilTools.getString(guarBaseInfoDetail.getForestMain()));
        //取得方式s
        XdjzzyData.setGet_type(PUBUtilTools.getString(guarBaseInfoDetail.getAcquMode()));
        //一手/二手标识
        XdjzzyData.setIs_used(PUBUtilTools.getString(guarBaseInfoDetail.getIsUsed()));
        //发票编号
        XdjzzyData.setInvoice_no(PUBUtilTools.getString(guarBaseInfoDetail.getInvoiceNo()));
        //发票日期
        XdjzzyData.setInvoice_date(PUBUtilTools.getString(guarBaseInfoDetail.getInvoiceDate()));
        //设备铭牌编号
        XdjzzyData.setEquip_no(PUBUtilTools.getString(guarBaseInfoDetail.getEquipNo()));
        //设备类型
        XdjzzyData.setMachine_type(PUBUtilTools.getString(guarBaseInfoDetail.getMachineType()));
        //设备分类
        XdjzzyData.setMachine_code(PUBUtilTools.getString(guarBaseInfoDetail.getMachineCode()));
        //型号/规格
        XdjzzyData.setSpec_model(PUBUtilTools.getString(guarBaseInfoDetail.getSpecModel()));
        //品牌/厂家/产地
        XdjzzyData.setVehicle_brand(PUBUtilTools.getString(guarBaseInfoDetail.getVehicleBrand()));
        //设备数量
        XdjzzyData.setMach_num(PUBUtilTools.stringToBigDecimal(guarBaseInfoDetail.getEquipQnt()));
        //是否有产品合格证
        XdjzzyData.setIs_eligible_cerit(PUBUtilTools.getString(guarBaseInfoDetail.getIndEligibleCerti()));
        //出厂日期或报关日期
        XdjzzyData.setFactory_date(PUBUtilTools.getString(guarBaseInfoDetail.getFactoryDate()));
        //设计使用到期日期
        XdjzzyData.setMature_date(PUBUtilTools.getString(guarBaseInfoDetail.getMatureDate()));
        //发票金额（元）
        XdjzzyData.setBuy_price(PUBUtilTools.getString(guarBaseInfoDetail.getBuyPrice()));
        //土地用途
        XdjzzyData.setLand_purp(PUBUtilTools.getString(guarBaseInfoDetail.getLandPurp()));
        //是否欠工程款
        XdjzzyData.setIs_arrearage(PUBUtilTools.getString(guarBaseInfoDetail.getIsDelayProjectAmt()));
        //欠工程款金额
        XdjzzyData.setArrearage_amt(PUBUtilTools.stringToBigDecimal(guarBaseInfoDetail.getDelayProjectAmt()));
        //地址
        XdjzzyData.setAddress(PUBUtilTools.getString(guarBaseInfoDetail.getDetailsAddress()));
        //供应链质押价值
        XdjzzyData.setGyl_val(PUBUtilTools.getString(guarBaseInfoDetail.getGylVal()));
        //是否有监管公司
        XdjzzyData.setIs_has_supervision(PUBUtilTools.getString(guarBaseInfoDetail.getHasSupervision()));
        //监管公司名称
        XdjzzyData.setSupervision_company_name(PUBUtilTools.getString(guarBaseInfoDetail.getSupervisionCompanyName()));
        //监管公司组织机构代码
        XdjzzyData.setSupervision_org_code(PUBUtilTools.getString(guarBaseInfoDetail.getSupervisionOrgCode()));
        //协议生效日
        XdjzzyData.setAgreement_begin_date(PUBUtilTools.getString(guarBaseInfoDetail.getAgreementBeginDate()));
        //协议到期日
        XdjzzyData.setAgreement_end_date(PUBUtilTools.getString(guarBaseInfoDetail.getAgreementEndDate()));
        //金额
        XdjzzyData.setCargo_amt(PUBUtilTools.getString(guarBaseInfoDetail.getAmt()));
        //保管人
        XdjzzyData.setKeep_user(PUBUtilTools.getString(guarBaseInfoDetail.getKeepId()));
        //货物详细类型
        XdjzzyData.setCargo_class(PUBUtilTools.getString(guarBaseInfoDetail.getCoodsDetailType()));
        //货物名称
        XdjzzyData.setCargo_name(PUBUtilTools.getString(guarBaseInfoDetail.getCargoName()));
        //货物数量
        XdjzzyData.setCargo_amount(PUBUtilTools.getString(guarBaseInfoDetail.getCargoAmount()).toString());
        //最新核定单价
        XdjzzyData.setLatest_approved_price(PUBUtilTools.getString(guarBaseInfoDetail.getLatestApprovedPrice()).toString());
        //货物计量单位
        XdjzzyData.setCargo_measure_unit(PUBUtilTools.getString(guarBaseInfoDetail.getCargoMeasureUnit()));
        //货物型号
        XdjzzyData.setCargo_type(PUBUtilTools.getString(guarBaseInfoDetail.getGoodsModel()));
        //建设工程规划许可证号
        XdjzzyData.setLayout_licence(PUBUtilTools.getString(guarBaseInfoDetail.getLayoutLicence()));
        //抵质押品名称
        XdjzzyData.setGuar_name(PUBUtilTools.getString(guarBaseInfo.getPldimnMemo()));
        //押品所在业务阶段
        XdjzzyData.setGuar_busistate("00");//00以创建
        //人工认定结果
        XdjzzyData.setManual_affirm_rs("03");//01 确认匹配押品 02 强制创建新押品 03 新增
        //创建系统
        XdjzzyData.setCreate_sys(DscmsBizDbEnum.SYSTEMNO_01.key);//01 信贷系统  02 小贷系统
        //创建时间
        XdjzzyData.setGuar_create_date(guarBaseInfo.getInputDate().substring(0,10));
        //创建人
        XdjzzyData.setCreate_userid(PUBUtilTools.getString(guarBaseInfo.getInputId()));
        //创建机构
        XdjzzyData.setCreate_orgid(PUBUtilTools.getString(guarBaseInfo.getInputBrId()));
        //最后更新时间
        XdjzzyData.setGuar_lastupdate_date(guarBaseInfo.getUpdDate().substring(0,10));
        //最后修改人
        XdjzzyData.setLastmodify_userid(PUBUtilTools.getString(guarBaseInfo.getUpdId()));
        //最后修改人机构
        XdjzzyData.setLastmodify_orgid(PUBUtilTools.getString(guarBaseInfo.getUpdBrId()));
        XdjzzyData.setK1_val(k1Val);
        XdjzzyData.setK2_val(k2Val);

        XdjzzyList.add(XdjzzyData);
        xdjzzyReqDto.setList(XdjzzyList);
        log.info("流水号【" + serno + "】，调xdjzzy押品信息同步及引入接口 开始，请求报文：" + xdjzzyReqDto);
        ResultDto<XdjzzyRespDto> xdjzzyRespDto = dscms2YphsxtClientService.xdjzzy(xdjzzyReqDto);
        log.info("流水号【" + serno + "】，调xdjzzy押品信息同步及引入接口 结束，响应报文：" + xdjzzyRespDto);

        String code = Optional.ofNullable(xdjzzyRespDto.getCode()).orElse(StringUtils.EMPTY);
        String message = Optional.ofNullable(xdjzzyRespDto.getMessage()).orElse(StringUtils.EMPTY);
        XdjzzyRespDto xdjzzyRespDtodata;

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            xdjzzyRespDtodata = xdjzzyRespDto.getData();
            log.info("抵押物信息同步 成功" + xdjzzyRespDtodata);
        } else {
            log.error("抵押物信息同步 失败" + message);
            throw new BizException(null,"",null,message);
        }
        log.info("流水号【"+guarBaseInfo.getSerno()+"】，同步信息到押品系统获取押品编号 结束");

        if(CmisBizConstants.OPER_FLAG_01.equals(operFlag)){
            //新增押品
            String guarNo = xdjzzyRespDtodata.getGuar_no();
            //校验押品编号是否存在
            int count = guarBaseInfoMapper.countByGuarNo(guarNo);

            if(count>0){
                throw new BizException(null,"",null,"押品编号已存在！");
            }
        }
        return xdjzzyRespDtodata.getGuar_no();
    }

    /**
     * 根据押品编号,担保分类代码 获取统一押品详情dto
     *
     * @param guarBaseInfo
     * @return
     */
    public GuarBaseInfoDetailDto getGuarBaseInfoDetail(GuarBaseInfo guarBaseInfo) {
        String guarTypeCd = PUBUtilTools.getString(guarBaseInfo.getGuarTypeCd(), true);
        if (StringUtils.isBlank(guarTypeCd) || guarTypeCd.length() < 7) {
            throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value + ",传入参数异常！");
        }
        //截取担保分类代码的前六位,获取担保二级码值
        String guarTypeCdFlag = guarTypeCd.substring(0, 6);
        String serno = PUBUtilTools.getString(guarBaseInfo.getSerno(), true);

        GuarBaseInfoDetailDto guarBaseInfoDetailDto = new GuarBaseInfoDetailDto();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);

        //抵押树 三级码值
        switch (guarTypeCdFlag) {
            case "DY0801":// DY0801 存货
                List<GuarInfCargoPledge> guarInfCargoPledges = guarInfCargoPledgeService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfCargoPledges.get(0), guarBaseInfoDetailDto);
                break;
            case "DY0601":// DY0601居住用房类在建工程 DY0602 商业用房类在建工程 DY0603 工业用房类在建工程   getGuarInfBuilProjectService
            case "DY0602":
            case "DY0603":
                List<GuarInfBuilProject> guarInfBuilProjects = guarInfBuilProjectService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfBuilProjects.get(0), guarBaseInfoDetailDto);
                guarBaseInfoDetailDto.setRealproCls(guarInfBuilProjects.get(0).getHouseType());
                break;
            case "DY0501":// DY0501  机器设备  GuarInfMachEqui
                List<GuarInfMachEqui> guarInfMachEquis = guarInfMachEquiService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfMachEquis.get(0), guarBaseInfoDetailDto);
                // 押品使用情况 STD_ZB_GUAR_UTIL_CASE
                guarBaseInfoDetailDto.setHouseProperty(guarInfMachEquis.get(0).getGuarUtilCase());
                guarBaseInfoDetailDto.setDetailsAddress(guarInfMachEquis.get(0).getEquipDepo());
                break;
            case "DY0299":// DY0299 其他使用权   guarUsufLandIndex
                List<GuarInfUsufLand> guarInfUsufLands = guarInfUsufLandService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfUsufLands.get(0), guarBaseInfoDetailDto);
                guarBaseInfoDetailDto.setHouseProperty(guarInfUsufLands.get(0).getGuarUtilCase());
                guarBaseInfoDetailDto.setBusinessHouseNo(guarInfUsufLands.get(0).getBusinessContractNo());
                break;
            case "DY0201":// DY0201 建筑用地使用权    guarBuildUseIndex
                List<GuarInfBuildUse> guarInfBuildUses = guarInfBuildUseService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfBuildUses.get(0), guarBaseInfoDetailDto);
                // 房产类别 STD_ZB_HOUSE_PROPERTY_CLS
                guarBaseInfoDetailDto.setRealproCls(guarInfBuildUses.get(0).getHouseType());
                guarBaseInfoDetailDto.setHouseProperty(guarInfBuildUses.get(0).getGuarUtilCase());
                break;
            case "DY0199":// DY0199  其他用房 guarOtherHouseIndex
                List<GuarInfOtherHouse> guarInfOtherHouses = guarInfOtherHouseService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfOtherHouses.get(0), guarBaseInfoDetailDto);
                break;
            case "DY0102":// DY0102  商业用房 DY0103  工业用房   guarBusinessIndustryIndex
            case "DY0103":
                List<GuarInfBusinessIndustryHousr> guarInfBusinessIndustryHousrs = guarInfBusinessIndustryHousrService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfBusinessIndustryHousrs.get(0), guarBaseInfoDetailDto);
                break;
            case "DY0101":// DY0101  居住用房   guarLivingRoomIndex
                List<GuarInfLivingRoom> guarInfLivingRooms = guarInfLivingRoomService.selectByModel(queryModel);
                BeanUtils.copyProperties(guarInfLivingRooms.get(0), guarBaseInfoDetailDto);
                break;
            default:
                throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
        }
        return guarBaseInfoDetailDto;
    }

    /**
     * @函数名称:queryGuarInfoIsUnderLmt
     * @函数描述:根据业务流水号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarInfoIsUnderLmt(QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoMapper.queryGuarInfoIsUnderLmt(queryModel);
        return list;
    }

    /**
     * @方法名称: queryManagerIdByContNo
     * @方法描述: 根据押品编号查询客户经理工号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String queryManagerIdByContNo(String guarNo) {
        return guarBaseInfoMapper.queryManagerIdByContNo(guarNo);
    }

    /**
     * @方法名称: buscon
     * @方法描述: 信贷业务与押品关联关系信息同步
     * @参数与返回说明:参数Map bizType 业务类型 01授信 02业务
     * isFlag  是否有效 1有效 0无效 默认1
     * bizSerno  业务编号
     * guarContNo 担保合同编号  02业务 必填
     * serno  业务与押品关联流水号 01授信 必填
     * @算法描述: 无
     */
    public String buscon(Map<String, String> map) {
        //获取参数
        String bizType = PUBUtilTools.getString(map.get("bizType"), true);//业务类型  01授信 02业务
        String isFlag = PUBUtilTools.getString(map.get("isFlag"), true);//业务类型  1有效 0无效
        String bizSerno = PUBUtilTools.getString(map.get("bizSerno"), true);//业务编号

        QueryModel queryModel = new QueryModel();
        List<cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List> reqDtos = new ArrayList<>();
        cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List reqDto = null;
        BusconReqDto busconReqDto = new BusconReqDto();
        if ("01".equals(bizType)) {
            String serno = PUBUtilTools.getString(map.get("serno"), true);//业务与押品关联流水号
            queryModel.addCondition("subSerno", serno);
            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectByModel(queryModel);
            String guarWay = lmtAppSubs.get(0).getGuarMode();
            //根据担保方式判断  00	信用  10	抵押  20	质押  30	保证
            //担保方式  10	抵押  20	质押
            if ("10".equals(guarWay) || "20".equals(guarWay)) {
                queryModel.addCondition("serno", serno);
                List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseDtoList = guarBaseInfoMapper.selectByIqpSernoModel(queryModel);
                for (GuarBizRelGuarBaseDto guarBizRelGuarBaseDto : guarBizRelGuarBaseDtoList) {
                    reqDto = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                    reqDto.setYwbhyp(bizSerno);//业务编号
                    reqDto.setYwlxyp(bizType);//业务类型 01-授信；02-业务
                    reqDto.setYptybh(guarBizRelGuarBaseDto.getGuarNo());//押品统一编号
                    reqDto.setIsflag(isFlag);//是否有效 1有效 0无效
                    reqDtos.add(reqDto);
                }
                //担保方式为   保证
            } else if ("30".equals(guarWay)) {
                queryModel.addCondition("serno", serno);
                List<GuarBizRelGuaranteeDto> guarBizRelGuaranteeDtoList = guarGuaranteeService.queryGuarInfoSell(queryModel);
                for (GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : guarBizRelGuaranteeDtoList) {
                    reqDto = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                    reqDto.setYwbhyp(bizSerno);//业务编号
                    reqDto.setYwlxyp(bizType);//业务类型 01-授信；02-业务
                    reqDto.setYptybh(guarBizRelGuaranteeDto.getGuarantyId());//押品统一编号
                    reqDto.setIsflag(isFlag);//是否有效 1有效 0无效
                    reqDtos.add(reqDto);
                }
            } else {
                return "";
            }

        } else if ("02".equals(bizType)) {
            String guarContNo = PUBUtilTools.getString(map.get("guarContNo"), true);//担保合同编号
            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
            String guarWay = grtGuarCont.getGuarWay();
            //根据担保方式判断  00	信用  10	抵押  20	质押  30	保证
            //担保方式  10	抵押  20	质押
            if ("10".equals(guarWay) || "20".equals(guarWay)) {
                queryModel.addCondition("guarContNo", guarContNo);
                List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectGuarBaseInfoByGuarContNo(queryModel);
                for (GuarBaseInfo guarBaseInfo : guarBaseInfoList) {
                    reqDto = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                    reqDto.setYwbhyp(bizSerno);//业务编号
                    reqDto.setYwlxyp(bizType);//业务类型 01-授信；02-业务
                    reqDto.setYptybh(guarBaseInfo.getGuarNo());//押品统一编号
                    reqDto.setIsflag(isFlag);//是否有效 1有效 0无效
                    reqDtos.add(reqDto);
                }
                //担保方式为   保证
            } else if ("30".equals(guarWay)) {
                queryModel.addCondition("guarContNo", map.get("guarContNo"));
                List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
                for (GuarGuarantee guarGuarantee : guarGuaranteeList) {
                    reqDto = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                    reqDto.setYwbhyp(bizSerno);//业务编号
                    reqDto.setYwlxyp(bizType);//业务类型 01-授信；02-业务
                    reqDto.setYptybh(guarGuarantee.getGuarantyId());//押品统一编号
                    reqDto.setIsflag(isFlag);//是否有效 1有效 0无效
                    reqDtos.add(reqDto);
                }
            } else {
                return "";
            }

        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        busconReqDto.setList(reqDtos);
        //信贷业务与押品关联关系信息同步接口
        ResultDto<BusconRespDto> busconRespDto = dscms2YpxtClientService.buscon(busconReqDto);
        String code = Optional.ofNullable(busconRespDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(busconRespDto.getMessage()).orElse(StringUtils.EMPTY);
        BusconRespDto busconRespDto1 = null;
        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            busconRespDto1 = busconRespDto.getData();
            log.info("信贷业务与押品关联关系信息同步押品系统成功! " + String.valueOf(busconRespDto1));
        } else {
            log.error("信贷业务与押品关联关系信息同步押品系统失败! " + meesage);
            //throw BizException.error(null, EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
        }
        return code;
    }


    /**
     * @方法名称: riskItem0027
     * @方法描述: 抵押物查封校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuqi
     * @创建时间: 2021-06-24 19:44:44
     * @修改记录: 修改时间：2021年7月7日17:13:17    修改人员：hubp    修改原因
     */
    public RiskResultDto riskItem0027(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("抵押物查封校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if ("LS005".equals(bizType) || "LS006".equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType) || "XW002".equals(bizType)) {
            // 小微，零售放款进来
            /** 通过放款流水号获取合同编号，再获取合同编号挂的担保合同，如果担保合同为抵押，则开始校验，为其他，则直接通过 */
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 2021年8月20日15:19:28  hubp   贷款担保方式为抵押时才校验
            if(CmisCommonConstants.GUAR_MODE_10.equals(pvpLoanApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_20.equals(pvpLoanApp.getGuarMode())){
                if (Objects.isNull(pvpLoanApp) || StringUtils.isEmpty(pvpLoanApp.getContNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02702);
                    return riskResultDto;
                }
                // 2021年9月29日19:20:33 hubp
                riskResultDto = riskItem0027(pvpLoanApp.getContNo());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX011,bizType)){ //对公贷款出账流程
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 抵押或者质押的时候，才进行拦截校验
            if(Objects.equals(CmisCommonConstants.GUAR_MODE_10,pvpLoanApp.getGuarMode()) || Objects.equals(CmisCommonConstants.GUAR_MODE_20,pvpLoanApp.getGuarMode())) {
                riskResultDto = riskItem0027(pvpLoanApp.getContNo());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX012,bizType)) { // 银承出账申请
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            if (Objects.isNull(pvpAccpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 抵押或者质押的时候，才进行拦截校验
            if (Objects.equals(CmisCommonConstants.GUAR_MODE_10, pvpAccpApp.getGuarMode()) || Objects.equals(CmisCommonConstants.GUAR_MODE_20, pvpAccpApp.getGuarMode())) {
                riskResultDto = riskItem0027(pvpAccpApp.getContNo());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX013,bizType)) { // 委托贷款出账申请
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 抵押或者质押的时候，才进行拦截校验
            if (Objects.equals(CmisCommonConstants.GUAR_MODE_10, pvpEntrustLoanApp.getGuarMode()) || Objects.equals(CmisCommonConstants.GUAR_MODE_20, pvpEntrustLoanApp.getGuarMode())) {
                riskResultDto = riskItem0027(pvpEntrustLoanApp.getContNo());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        }
        log.info("抵押物查封校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 根据合同编号去查询抵押物信息
     * @param contNo
     * @return
     */
    public RiskResultDto riskItem0027(String contNo) {
        RiskResultDto riskResultDto = new RiskResultDto();
        log.info("**************信贷押品状态查询开始,合同号【{}】****************", contNo);
        YpztcxReqDto ypztcxReqDto = new YpztcxReqDto();
        ypztcxReqDto.setDistco("GJ"); // 国结调用此接口
        ypztcxReqDto.setCertnu("1"); //不动产权证书号 调用接口国结时，老代码逻辑此字段未使用因校验必输，传值1
        ypztcxReqDto.setMocenu(contNo); //不动产权登记证明号
        log.info("信贷押品状态查询请求报文：【{}】" , JSON.toJSONString(ypztcxReqDto));
        ResultDto<YpztcxRespDto> ypztcxReqResultDto = dscms2YphsxtClientService.ypztcx(ypztcxReqDto);
        log.info("信贷押品状态查询返回报文：【{}】" , JSON.toJSONString(ypztcxReqResultDto));
        String ypztcxCode = Optional.ofNullable(ypztcxReqResultDto.getCode()).orElse(StringUtils.EMPTY);

        if("9999".equals(ypztcxCode)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02701);
            log.info("**************信贷押品状态查询结束，存在已查封抵押物，合同号【{}】****************", contNo);
            return riskResultDto;
        }else if("0001".equals(ypztcxCode)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02704);
            log.info("**************未查询到查封信息，请线下查询！，合同号【{}】****************", contNo);
            return riskResultDto;
        }else if ("5555".equals(ypztcxCode)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(ypztcxReqResultDto.getMessage());
            log.info("**************"+ypztcxReqResultDto.getMessage()+"，合同号【{}】****************", contNo);
            return riskResultDto;
        }else if("0002".equals(ypztcxCode)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02705);
            log.info("**************"+ypztcxReqResultDto.getMessage()+"，合同号【{}】****************", contNo);
            return riskResultDto;
        }else if ("0003".equals(ypztcxCode)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02706);
            log.info("**************抵押物类型非不动产类型！合同号【{}】****************", contNo);
            return riskResultDto;
        }
        log.info("**************信贷押品状态查询结束，不存在已查封抵押物，合同号【{}】****************", contNo);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
    /**
     * 通过合同编号，获取抵质押基本信息
     *
     * @param guarContNo
     * @return
     */
    public List<GuarBaseInfo> queryBaseInfobyGuarContOn(String guarContNo) {
        return guarBaseInfoMapper.queryBaseInfobyGuarContOn(guarContNo);
    }

    /**
     * 根据申请流水号查询抵质押品信息
     * @param map
     * @return
     */
    public List<LmtReViewPortDto> queryGuarBaseInfo(QueryModel map) {
        // 取当前申请流水号项下的最新的一条审批数据
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno((String) map.getCondition().get("serno"));
        log.info("当前申请项下的最新一条审批表数据:"+ JSON.toJSONString(lmtAppr));
        /*Map mapQuery = new HashMap();
        mapQuery.put("approveSerno",lmtAppr.getApproveSerno());
        List<LmtReViewPortDto> mapList = guarBaseInfoMapper.queryGuarBaseInfo(mapQuery);*/
        HashMap<String, String> queryMap = new HashMap();
        queryMap.put("apprSerno",lmtAppr.getApproveSerno());
        queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
        List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
        List<LmtReViewPortDto> mapList = new ArrayList<>();
        for (LmtApprSub lmtApprSub : approveSubList) {
            LmtReViewPortDto lmtReViewPortDto = new LmtReViewPortDto();
            lmtReViewPortDto.setApproveSubSerno(lmtApprSub.getApproveSubSerno());
            lmtReViewPortDto.setSubSerno(lmtApprSub.getSubSerno());
            List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtApprSub.getSubSerno());
            if(CollectionUtils.nonEmpty(guarBizRelList)){
                GuarBizRel guarBizRel = guarBizRelList.get(0);
                lmtReViewPortDto.setGuarNo(guarBizRel.getGuarNo());
                lmtReViewPortDto.setCorreFinAmt(guarBizRel.getCorreFinAmt()!=null ? guarBizRel.getCorreFinAmt().doubleValue() + "" : "");
                lmtReViewPortDto.setMaxMortagageAmt(guarBizRel.getMaxMortagageAmt()!=null ? guarBizRel.getMaxMortagageAmt().doubleValue() + "" : "");
                if(StringUtils.isEmpty(guarBizRel.getGuarNo())){
                    continue;
                }
                List<String> guarNoList = new ArrayList<>();
                guarNoList.add(guarBizRel.getGuarNo());
                List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectByGuarNoList(guarNoList);
                if(CollectionUtils.nonEmpty(guarBaseInfoList)){
                    GuarBaseInfo guarBaseInfo = guarBaseInfoList.get(0);
                    lmtReViewPortDto.setGuarTypeCd(guarBaseInfo.getGuarTypeCd());
                    lmtReViewPortDto.setPldimnMemo(guarBaseInfo.getPldimnMemo());
                    lmtReViewPortDto.setPldLocation(guarBaseInfo.getPldLocation());
                    lmtReViewPortDto.setGuarCusId(guarBaseInfo.getGuarCusId());
                    lmtReViewPortDto.setGuarCusName(guarBaseInfo.getGuarCusName());
                    lmtReViewPortDto.setSqu(guarBaseInfo.getSqu());
                    lmtReViewPortDto.setEvalAmt(guarBaseInfo.getEvalAmt()!=null ? guarBaseInfo.getEvalAmt().doubleValue() + "" : "");
                    HashMap<String, String> queryMap1 = new HashMap();
                    queryMap1.put("guarNo",guarBaseInfo.getGuarNo());
                    queryMap1.put("subSerno",lmtApprSub.getSubSerno());
                    String isPldOrder = rptLmtRepayAnysGuarPldDetailMapper.getIsPldOrderByGuarNoAndSubNo(queryMap1);
                    lmtReViewPortDto.setIsPldOrder(isPldOrder);
                }else{
                    continue;
                }
            }else{
                continue;
            }
            mapList.add(lmtReViewPortDto);
        }

        if(CollectionUtils.nonEmpty(mapList)){
            for(LmtReViewPortDto lmtReViewPortDto : mapList){
                try{
                    BigDecimal evalAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank(lmtReViewPortDto.getMaxMortagageAmt()) ? "0" : lmtReViewPortDto.getMaxMortagageAmt()));
                    BigDecimal correFinAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank(lmtReViewPortDto.getCorreFinAmt()) ? "0" : lmtReViewPortDto.getCorreFinAmt()));
                    if(evalAmt.compareTo(BigDecimal.ZERO) == 0 || correFinAmt.compareTo(BigDecimal.ZERO) == 0){
                        lmtReViewPortDto.setMortagageRate("0");
                    }else{
                        lmtReViewPortDto.setMortagageRate(String.valueOf(correFinAmt.divide(evalAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP)));
                    }
                }catch(Exception e){
                    log.info("当前数据取值有误,请核实数据是否正确:"+JSON.toJSONString(mapList));
                    lmtReViewPortDto.setMortagageRate("N/A");
                }finally {

                }
            }
        }
        return mapList;
    }

    /**
     * 根据申请流水号查询抵质押品信息
     * @param map
     * @return
     */
    public List<LmtReViewPortDto> queryGuarBaseInfoByApprSerno(QueryModel map) {
        List<LmtReViewPortDto> mapList = new ArrayList<>();
        HashMap<String, String> queryMap = new HashMap();
        queryMap.put("apprSerno",(String) map.getCondition().get("apprSerno"));
        queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
        log.info("传入参数:"+JSON.toJSONString(map));
        if(StringUtils.isBlank((String) map.getCondition().get("apprSerno"))){
            return mapList;
        }
        List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
        log.info("当前审批流水号[{}]对应项下的分项列表[{}]",(String) map.getCondition().get("apprSerno"),JSON.toJSONString(approveSubList));
        for (LmtApprSub lmtApprSub : approveSubList) {
            List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtApprSub.getSubSerno());
            log.info("当前申请分项流水号[{}]对应项下的押品与业务关系列表[{}]",lmtApprSub.getSubSerno(),JSON.toJSONString(guarBizRelList));
            if(CollectionUtils.nonEmpty(guarBizRelList)){
                for(GuarBizRel guarBizRel : guarBizRelList){
                    LmtReViewPortDto lmtReViewPortDto = new LmtReViewPortDto();
                    lmtReViewPortDto.setApproveSubSerno(lmtApprSub.getApproveSubSerno());
                    lmtReViewPortDto.setSubSerno(lmtApprSub.getSubSerno());
                    lmtReViewPortDto.setGuarNo(guarBizRel.getGuarNo());
                    lmtReViewPortDto.setCorreFinAmt(guarBizRel.getCorreFinAmt()!=null ? String.valueOf(guarBizRel.getCorreFinAmt()) + "" : "");
                    lmtReViewPortDto.setMaxMortagageAmt(guarBizRel.getMaxMortagageAmt()!=null ? String.valueOf(guarBizRel.getMaxMortagageAmt()) + "" : "");
                    GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.selectInfoByGuarNo(guarBizRel.getGuarNo());
                    log.info("当前押品编号[{}]对应项下的押品基本信息[{}]",guarBizRel.getGuarNo(),JSON.toJSONString(guarBaseInfo));
                    if(!Objects.isNull(guarBaseInfo)){
                        lmtReViewPortDto.setGuarTypeCd(guarBaseInfo.getGuarTypeCd());
                        lmtReViewPortDto.setPldimnMemo(guarBaseInfo.getPldimnMemo());
                        lmtReViewPortDto.setPldLocation(guarBaseInfo.getPldLocation());
                        lmtReViewPortDto.setGuarCusId(guarBaseInfo.getGuarCusId());
                        lmtReViewPortDto.setGuarCusName(guarBaseInfo.getGuarCusName());
                        lmtReViewPortDto.setSqu(guarBaseInfo.getSqu());
                        lmtReViewPortDto.setEvalAmt(guarBaseInfo.getEvalAmt()!=null ? String.valueOf(guarBaseInfo.getEvalAmt()) + "" : "");
                        HashMap<String, String> queryMap1 = new HashMap();
                        queryMap1.put("guarNo",guarBaseInfo.getGuarNo());
                        queryMap1.put("subSerno",lmtApprSub.getSubSerno());
                        String isPldOrder = rptLmtRepayAnysGuarPldDetailMapper.getIsPldOrderByGuarNoAndSubNo(queryMap1);
                        lmtReViewPortDto.setIsPldOrder(isPldOrder);
                        mapList.add(lmtReViewPortDto);
                    }else{
                        continue;
                    }
                }
            }else{
                continue;
            }

        }

        if(CollectionUtils.nonEmpty(mapList)){
            for(LmtReViewPortDto lmtReViewPortDto : mapList){
                try{
                    BigDecimal evalAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank(lmtReViewPortDto.getMaxMortagageAmt()) ? "0" : lmtReViewPortDto.getMaxMortagageAmt()));
                    BigDecimal correFinAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank(lmtReViewPortDto.getCorreFinAmt()) ? "0" : lmtReViewPortDto.getCorreFinAmt()));
                    if(evalAmt.compareTo(BigDecimal.ZERO) == 0 || correFinAmt.compareTo(BigDecimal.ZERO) == 0){
                        lmtReViewPortDto.setMortagageRate("0");
                    }else{
                        lmtReViewPortDto.setMortagageRate(String.valueOf(correFinAmt.divide(evalAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP)));
                    }
                }catch(Exception e){
                    log.info("当前数据取值有误,请核实数据是否正确:"+JSON.toJSONString(mapList));
                    lmtReViewPortDto.setMortagageRate("N/A");
                }finally {

                }
            }
        }
        return mapList;
    }

    /**
     * 校验该统一押品编号是已否存在
     *
     * @param guarNoList
     * @return
     */
    public List<String> selectByGuarNoKList(List<String> guarNoList) {
        return guarBaseInfoMapper.selectByGuarNoKList(guarNoList);
    }

    /**
     * 押品批量插入
     *
     * @param guarBaseInfos
     * @return
     */
    public boolean insertGuarBaseInfoList(List<GuarBaseInfo> guarBaseInfos) {
        return guarBaseInfoMapper.insertGuarBaseInfoList(guarBaseInfos) == guarBaseInfos.size() ? true : false;
    }

    public List<GuarBaseInfo> selectByGuarNoList(List<String> guarNoList) {
        return guarBaseInfoMapper.selectByGuarNoList(guarNoList);
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByParams
     * @函数描述:根据业务流水号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarBaseInfoDataByParams(QueryModel queryModel) {
        List<GuarBaseInfo> list  = guarBaseInfoMapper.queryGuarBaseInfoDataByParams(queryModel);
        return list;
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByParams
     * @函数描述:根据业务流水号查询授信项下的押品信息及关联信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfoRelDto> queryGuarBaseInfoRelDtoByParams(QueryModel queryModel){
        List<GuarBaseInfoRelDto> list  = guarBaseInfoMapper.queryGuarBaseInfoRelDtoByParams(queryModel);
        return list;
    }
    /**
     * @函数名称:queryGuarBaseInfoByWarrantInSerno
     * @函数描述:根据权证入库流水号查询权证入库里的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarBaseInfoByWarrantInSerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfo> list  = guarBaseInfoMapper.queryGuarBaseInfoByWarrantInSerno(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:queryGuarBaseInfoByWarrantNo
     * @函数描述:根据权证编号查询押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarBaseInfoByCoreGuarantyNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfo> list  = guarBaseInfoMapper.queryGuarBaseInfoByCoreGuarantyNo(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByGuarContNo
     * @函数描述:根据担保合同编号查询关联的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarBaseInfoDataByGuarContNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfo> list  = guarBaseInfoMapper.queryGuarBaseInfoDataByGuarContNo(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByGuarContNo
     * @函数描述:根据担保合同编号查询关联的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarBaseInfo> queryGuarBaseInfoDataBySerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBaseInfo> list  = guarBaseInfoMapper.queryGuarBaseInfoDataByGuarContNo(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述:
     * @创建人：刘权
     */

    public List<GuarBaseInfo> selectByCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectByCusId(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByGuarNo
     * @方法描述: 根据押品编号查询抵质押信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人：刘权
     */

    public List<GuarBaseInfo> selectBaseInfoByGuarNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectBaseInfoByGuarNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByGuarBaseInfo
     * @方法描述: 通过担保合同编号查询抵质押基本信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人： 周茂伟
     */

    public List<GuarBaseInfoClientDto> selectByGuarBaseInfo(String guarContNo) {
        List<GuarBaseInfoClientDto> list = guarBaseInfoMapper.selectByGuarBaseInfo(guarContNo);
        return list;
    }

    /**
     * @方法名称: queryGuarBaseInfoByLmtSerno
     * @方法描述: 通过授信申请流水号查询申请时的抵质押基本信息
     * @参数与返回说明:
     * @创建人： css
     */

    public List<Map> queryGuarBaseInfoByLmtSerno(String serno) {
        List<Map> list = guarBaseInfoMapper.queryGuarBaseInfoByLmtSerno(serno);
        return list;
    }

    /**
     * 根据统一押品编号查询
     * @param guarNo
     * @return
     */
    public GuarBaseInfo selectInfoByGuarNo(String guarNo) {
        return guarBaseInfoMapper.selectInfoByGuarNo(guarNo);
    }

    /**
     * 根据入参查询
     * @param map
     * @return
     */
    public GuarBaseInfo queryByGuarNoAndGrtFlag(HashMap<String,String> map) {
        return guarBaseInfoMapper.queryByGuarNoAndGrtFlag(map);
    }


    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/10/9 23:13
     * @version 1.0.0
     * @desc  根据流水号查询是否存在质押存单
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int getBaseInfo(String serno){
        return guarBaseInfoMapper.getBaseInfo(serno);
    }
    /**
     * 根据授信分项编号查询关联的押品
     * @param queryModel
     * @return
     */
    public String selectGuarNosByLmtAccNo(QueryModel queryModel){
        return guarBaseInfoMapper.selectGuarNosByLmtAccNo(queryModel);
    }

}