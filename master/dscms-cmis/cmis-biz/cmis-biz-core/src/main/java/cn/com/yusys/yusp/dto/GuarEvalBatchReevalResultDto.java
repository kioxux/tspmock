package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalBatchReevalResult
 * @类描述: guar_eval_batch_reeval_result数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-31 10:04:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalBatchReevalResultDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 任务ID **/
	private String taskId;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 押品所有人编号 **/
	private String guarCusId;
	
	/** 押品所有人名称 **/
	private String guarCusName;
	
	/** 担保分类 **/
	private String guarTypeCd;
	
	/** 重估我行确认价值 **/
	private java.math.BigDecimal newEvalAmt;
	
	/** 重估结果 **/
	private String evalResult;
	
	/** 失败原因 **/
	private String evalFault;
	
	/** 重估日期 **/
	private String evalDate;
	
	/** 确认币种 **/
	private String currencyType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param taskId
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId == null ? null : taskId.trim();
	}
	
    /**
     * @return TaskId
     */	
	public String getTaskId() {
		return this.taskId;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd == null ? null : guarTypeCd.trim();
	}
	
    /**
     * @return GuarTypeCd
     */	
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param newEvalAmt
	 */
	public void setNewEvalAmt(java.math.BigDecimal newEvalAmt) {
		this.newEvalAmt = newEvalAmt;
	}
	
    /**
     * @return NewEvalAmt
     */	
	public java.math.BigDecimal getNewEvalAmt() {
		return this.newEvalAmt;
	}
	
	/**
	 * @param evalResult
	 */
	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult == null ? null : evalResult.trim();
	}
	
    /**
     * @return EvalResult
     */	
	public String getEvalResult() {
		return this.evalResult;
	}
	
	/**
	 * @param evalFault
	 */
	public void setEvalFault(String evalFault) {
		this.evalFault = evalFault == null ? null : evalFault.trim();
	}
	
    /**
     * @return EvalFault
     */	
	public String getEvalFault() {
		return this.evalFault;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate == null ? null : evalDate.trim();
	}
	
    /**
     * @return EvalDate
     */	
	public String getEvalDate() {
		return this.evalDate;
	}
	
	/**
	 * @param currencyType
	 */
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType == null ? null : currencyType.trim();
	}
	
    /**
     * @return CurrencyType
     */	
	public String getCurrencyType() {
		return this.currencyType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}