package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtNpGreenApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.LmtNpGreenAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class DGSX06BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGSX06BizService.class);//定义log

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private LmtNpGreenAppService lmtNpGreenAppService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_SX032.equals(bizType)){
            handleSX032Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 贴现贷款申请流程
    private void handleSX032Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("对公内评低准入例外审批流程" + serno + "流程操作");
        try {
            String currNodeId = resultInstanceDto.getCurrentNodeId();
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                if(CmisBizConstants.DGSX06_START.equals(currNodeId)){
                    //发起节点执行下面的逻辑
                    LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
                    if (lmtNpGreenApp == null) {
                        throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                    }
                    lmtNpGreenApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                    log.info("流程发起-内评低准入例外申请" + serno + "流程审批状态为【111】-审批中");
                    int updateCount = lmtNpGreenAppService.updateSelective(lmtNpGreenApp);
                    if (updateCount <= 0) {
                        throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
                    }
                    log.info("流程发起-内评低准入例外申请" + serno + "结束");
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //.更新申请表的审批状态 由审批中111 -> 审批通过 997
                LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
                if (lmtNpGreenApp == null) {
                    throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                }
                lmtNpGreenApp.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                log.info("流程结束-内评低准入例外申请" + serno + "流程审批状态为【997】-通过");
                int updateCount = lmtNpGreenAppService.updateSelective(lmtNpGreenApp);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
                }
                log.info("流程结束-内评低准入例外申请" + serno + "结束");

            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
                    if (lmtNpGreenApp == null) {
                        throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                    }
                    lmtNpGreenApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("内评低准入例外申请"+serno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
                    if (lmtNpGreenApp == null) {
                        throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                    }
                    lmtNpGreenApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("内评低准入例外申请"+serno+"拿回操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("内评低准入例外申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
                if (lmtNpGreenApp == null) {
                    throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                }
                lmtNpGreenApp.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                int updateCount = lmtNpGreenAppService.updateSelective(lmtNpGreenApp);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
                }
            } else {
                log.warn("内评低准入例外申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGSX06.equals(flowCode);
    }
}
