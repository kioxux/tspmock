package cn.com.yusys.yusp.web.server.xdxw0063;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0063.req.Xdxw0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.resp.Xdxw0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0063.Xdxw0063Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0063:调查基本信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0063Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0063Resource.class);

    @Autowired
    private Xdxw0063Service xdxw0063Service;

    /**
     * 交易码：xdxw0063
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0063DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0063")
    protected @ResponseBody
    ResultDto<Xdxw0063DataRespDto> xdxw0063(@Validated @RequestBody Xdxw0063DataReqDto xdxw0063DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063DataReqDto));
        Xdxw0063DataRespDto xdxw0063DataRespDto = new Xdxw0063DataRespDto();// 响应Dto:调查基本信息查询
        ResultDto<Xdxw0063DataRespDto> xdxw0063DataResultDto = new ResultDto<>();
		String cert_code = xdxw0063DataReqDto.getCert_code();//证件号
        try {
            // 从xdxw0063DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0063Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0063DataReqDto));
            xdxw0063DataRespDto = Optional.ofNullable(xdxw0063Service.getReportBasicInfo(xdxw0063DataReqDto)).orElseGet(() -> {
                Xdxw0063DataRespDto temp = new Xdxw0063DataRespDto();
                temp.setSpouse_name(StringUtils.EMPTY);// 配偶名称
                temp.setSpouse_cert_code(StringUtils.EMPTY);// 配偶身份证
                temp.setSurvey_serno(StringUtils.EMPTY);// 授信调查流水号
                temp.setBorrower(StringUtils.EMPTY);// 借款人名称
                temp.setBorrower_cert_code(StringUtils.EMPTY);// 借款人身份证号码
                temp.setApplyer(StringUtils.EMPTY);// 申请人
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0063DataRespDto));
            // 封装xdxw0063DataResultDto中正确的返回码和返回信息
            xdxw0063DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0063DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value , e.getMessage());
            // 封装xdxw0063DataResultDto中异常返回码和返回信息
            xdxw0063DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0063DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0063DataRespDto到xdxw0063DataResultDto中
        xdxw0063DataResultDto.setData(xdxw0063DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063DataResultDto));
        return xdxw0063DataResultDto;
    }
}
