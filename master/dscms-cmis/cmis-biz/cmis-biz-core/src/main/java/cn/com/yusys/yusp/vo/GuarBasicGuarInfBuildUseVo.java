package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfBuildUse;

public class GuarBasicGuarInfBuildUseVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfBuildUse guarInfBuildUse;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfBuildUse getGuarInfBuildUse() {
        return guarInfBuildUse;
    }

    public void setGuarInfBuildUse(GuarInfBuildUse guarInfBuildUse) {
        this.guarInfBuildUse = guarInfBuildUse;
    }
}
