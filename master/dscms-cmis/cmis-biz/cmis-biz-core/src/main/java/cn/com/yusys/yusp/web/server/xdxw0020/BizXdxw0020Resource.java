package cn.com.yusys.yusp.web.server.xdxw0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0020.req.Xdxw0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0020.resp.Xdxw0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0020.Xdxw0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:客户调查撤销
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0020:客户调查撤销")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0020Resource.class);

    @Resource
    private Xdxw0020Service xdxw0020Service;
    /**
     * 交易码：xdxw0020
     * 交易描述：客户调查撤销
     *
     * @param xdxw0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户调查撤销")
    @PostMapping("/xdxw0020")
    protected @ResponseBody
    ResultDto<Xdxw0020DataRespDto> xdxw0020(@Validated @RequestBody Xdxw0020DataReqDto xdxw0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataReqDto));
        Xdxw0020DataRespDto xdxw0020DataRespDto = new Xdxw0020DataRespDto();// 响应Dto:客户调查撤销
        ResultDto<Xdxw0020DataRespDto> xdxw0020DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0020DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdxw0020DataRespDto = xdxw0020Service.xdxw0020(xdxw0020DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0020DataRespDto对象开始
//			xdxw0020DataRespDto.setCancelStatus(StringUtils.EMPTY);// 撤销状态
			// TODO 封装xdxw0020DataRespDto对象结束
            // 封装xdxw0020DataResultDto中正确的返回码和返回信息
            xdxw0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, e.getMessage());
            // 封装xdxw0020DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdxw0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdxw0020DataRespDto到xdxw0020DataResultDto中
        xdxw0020DataResultDto.setData(xdxw0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataResultDto));
        return xdxw0020DataResultDto;
    }
}
