/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtInspectInfo;
import cn.com.yusys.yusp.repository.mapper.LmtInspectInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtInspectInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtInspectInfoService {

    @Autowired
    private LmtInspectInfoMapper lmtInspectInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtInspectInfo selectByPrimaryKey(String inspectSerno) {
        return lmtInspectInfoMapper.selectByPrimaryKey(inspectSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtInspectInfo> selectAll(QueryModel model) {
        List<LmtInspectInfo> records = (List<LmtInspectInfo>) lmtInspectInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtInspectInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtInspectInfo> list = lmtInspectInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtInspectInfo record) {
        return lmtInspectInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtInspectInfo record) {
        return lmtInspectInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtInspectInfo record) {
        return lmtInspectInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtInspectInfo record) {
        return lmtInspectInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String inspectSerno) {
        return lmtInspectInfoMapper.deleteByPrimaryKey(inspectSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtInspectInfoMapper.deleteByIds(ids);
    }

    /**
     * @author hubp
     * @param surveySerno
     * @return ResultDto<List<LmtInspectInfo>>
     * @desc 根据流水号查询关联勘验信息
     */
    public List<LmtInspectInfo> selectBySurveySerno(String surveySerno){
        return lmtInspectInfoMapper.selectBySurveySerno(surveySerno);
    }
    /**
     * @author hubp
     * @param queryModel
     * @return ResultDto<List<LmtInspectInfo>>
     * @desc 根据登入人ID查询关联勘验信息
     */
    public List<LmtInspectInfo> selectByLoginId(QueryModel queryModel){
        User user = SessionUtils.getUserInformation();
        queryModel.getCondition().put("inputId",user.getLoginCode());
        queryModel.getCondition().put("inspectStatus","03");
        return lmtInspectInfoMapper.selectByModel(queryModel);
    }
}
