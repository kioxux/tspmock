package cn.com.yusys.yusp.web.server.xdzc0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0015.req.Xdzc0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.resp.Xdzc0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0015.Xdzc0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:保证金查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0015:保证金查询接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0015Resource.class);

    @Autowired
    private Xdzc0015Service xdzc0015Service;
    /**
     * 交易码：xdzc0015
     * 交易描述：保证金查询接口
     *
     * @param xdzc0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金查询接口")
    @PostMapping("/xdzc0015")
    protected @ResponseBody
    ResultDto<Xdzc0015DataRespDto> xdzc0015(@Validated @RequestBody Xdzc0015DataReqDto xdzc0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015DataReqDto));
        Xdzc0015DataRespDto xdzc0015DataRespDto = new Xdzc0015DataRespDto();// 响应Dto:保证金查询接口
        ResultDto<Xdzc0015DataRespDto> xdzc0015DataResultDto = new ResultDto<>();
        try {
            xdzc0015DataRespDto = xdzc0015Service.xdzc0015Service(xdzc0015DataReqDto);
            xdzc0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, e.getMessage());
            // 封装xdzc0015DataResultDto中异常返回码和返回信息
            xdzc0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0015DataRespDto到xdzc0015DataResultDto中
        xdzc0015DataResultDto.setData(xdzc0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015DataResultDto));
        return xdzc0015DataResultDto;
    }
}
