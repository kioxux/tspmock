package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPld;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPldDetail;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarZy;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPld
 * @类描述: rpt_lmt_repay_anys_guar_pld数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 15:03:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptLmtRepayAnysGuarPldDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<RptLmtRepayAnysGuarPldDetail> rptLmtRepayAnysGuarPldDetailList;
	private List<RptLmtRepayAnysGuarZy> rptLmtRepayAnysGuarZyList;

	public List<RptLmtRepayAnysGuarPldDetail> getRptLmtRepayAnysGuarPldDetailList() {
		return rptLmtRepayAnysGuarPldDetailList;
	}

	public void setRptLmtRepayAnysGuarPldDetailList(List<RptLmtRepayAnysGuarPldDetail> rptLmtRepayAnysGuarPldDetailList) {
		this.rptLmtRepayAnysGuarPldDetailList = rptLmtRepayAnysGuarPldDetailList;
	}

	public List<RptLmtRepayAnysGuarZy> getRptLmtRepayAnysGuarZyList() {
		return rptLmtRepayAnysGuarZyList;
	}

	public void setRptLmtRepayAnysGuarZyList(List<RptLmtRepayAnysGuarZy> rptLmtRepayAnysGuarZyList) {
		this.rptLmtRepayAnysGuarZyList = rptLmtRepayAnysGuarZyList;
	}
}