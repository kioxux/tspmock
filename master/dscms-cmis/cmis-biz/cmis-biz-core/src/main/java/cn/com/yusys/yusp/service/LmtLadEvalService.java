/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.irs.common.*;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.req.CmisLmt0066ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.resp.CmisLmt0066RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtLadEvalMapper;
import cn.com.yusys.yusp.service.client.bsp.irs.irs21.Irs21Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: LmtLadEvalService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-02 08:37:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtLadEvalService {

    private static final Logger log = LoggerFactory.getLogger(LmtLadEvalService.class);

    @Autowired
    private LmtLadEvalMapper lmtLadEvalMapper;
    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private GuarBizRelService guarBizRelService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CtrDiscContService ctrDiscContService;
    @Autowired
    private CtrCvrgContService ctrCvrgContService;
    @Autowired
    private CtrTfLocContService ctrTfLocContService;
    @Autowired
    private CtrAccpContService ctrAccpContService;
    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;
    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private LmtReplyAccService lmtReplyAccService;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private AccCvrsService accCvrsService;
    @Autowired
    private AccTfLocService accTfLocService;
    @Autowired
    private AccDiscService accDiscService;
    @Autowired
    private GrtGuarContService grtGuarContService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private BailAccInfoService bailAccInfoService;
    @Autowired
    private IqpDiscAppService iqpDiscAppService;
    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;
    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;
    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;
    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;
    @Autowired
    private IqpAccpAppService iqpAccpAppService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private AccAccpDrftSubService accAccpDrftSubService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private Irs21Service irs21Service;//业务逻辑处理类：单一客户限额测算信息同步
    @Autowired
    private GuarInfBuilProjectService guarInfBuilProjectService;
    @Autowired
    private GuarInfBuildUseService guarInfBuildUseService;
    @Autowired
    private GuarInfBusinessIndustryHousrService guarInfBusinessIndustryHousrService;
    @Autowired
    private GuarInfLivingRoomService guarInfLivingRoomService;
    @Autowired
    private GuarInfOtherHouseService guarInfOtherHouseService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;
    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtLadEval selectByPrimaryKey(String pkId) {
        return lmtLadEvalMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtLadEval> selectAll(QueryModel model) {
        List<LmtLadEval> records = (List<LmtLadEval>) lmtLadEvalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtLadEval> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtLadEval> list = lmtLadEvalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtLadEval record) {
        return lmtLadEvalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtLadEval lmtLadEval) {
        User userInfo = SessionUtils.getUserInformation();
        LmtLadEval lmtLadEval1 = selectSingleBySerno(lmtLadEval.getSerno());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if (userInfo == null) {
            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
        } else {
            lmtLadEval.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtLadEval.setInputId(userInfo.getLoginCode());
            lmtLadEval.setInputBrId(userInfo.getOrg().getCode());
            lmtLadEval.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            lmtLadEval.setUpdId(userInfo.getLoginCode());
            lmtLadEval.setUpdBrId(userInfo.getOrg().getCode());
            lmtLadEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            lmtLadEval.setCreateTime(DateUtils.getCurrDate());
            lmtLadEval.setUpdateTime(DateUtils.getCurrDate());
        }
        if(Objects.isNull(lmtLadEval1)){
            lmtLadEval.setPkId(UUID.randomUUID().toString());
            return lmtLadEvalMapper.insertSelective(lmtLadEval);
        }
        return lmtLadEvalMapper.updateByPrimaryKey(lmtLadEval);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtLadEval record) {
        return lmtLadEvalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtLadEval record) {
        return lmtLadEvalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtLadEvalMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtLadEvalMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据多授信申请流水号查询多笔客户评级和债项评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtLadEval> selectBySerno(String serno) {
        if(StringUtils.isBlank(serno)){
            log.info("传入参数为空!"+serno);
            return new ArrayList<>();
        }
        List<LmtLadEval> lmtLadEvals = lmtLadEvalMapper.selectBySerno(serno);
        return lmtLadEvals;
    }

    /**
     * @方法名称: selectSingleBySerno
     * @方法描述: 根据授信申请流水号查询单个客户评级和债项评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtLadEval selectSingleBySerno(String serno) {
        return lmtLadEvalMapper.selectSingleBySerno(serno);
    }


    /**
     * @方法名称：calSingleLmt
     * @方法描述：单一客户限额测算
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-13 下午 4:48
     * @修改记录：修改时间 修改人员  修改原因
     */
    public LmtLadEval calSingleLmt(LmtLadEval lmtLadEval) {
        Irs21ReqDto reqDto = new Irs21ReqDto();
        //授信申请流水号
        reqDto.setSxserialno(lmtLadEval.getSerno());
        //客户编号
        reqDto.setCustid(lmtLadEval.getCusId());
        //reqDto.setCustid("8400435648");
        //客户名称
        reqDto.setCustname(lmtLadEval.getCusName());
        //reqDto.setCustname("昆山黑斑马装饰设计有限公司");
        //客户对外负债
        reqDto.setExtdebt(lmtLadEval.getCusOutsideDebt().toString());
        //现有他行负债
        reqDto.setOtherdebt(lmtLadEval.getCurOtherBankDebt().toString());
        //对外抵押金额
        reqDto.setGuarsum(lmtLadEval.getOuterPldAmt().toString());
        //对外质押金额
        reqDto.setImpsum(lmtLadEval.getOuterImnAmt().toString());
        //对外保证金额
        reqDto.setCashsum(lmtLadEval.getOuterGrtAmt().toString());

        ResultDto<Irs21RespDto> irs21RespDtoResultDto = dscms2IrsClientService.irs21(reqDto);
        log.info("单一客户限额测算返回:{}" + irs21RespDtoResultDto.toString());
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(irs21RespDtoResultDto.getCode())) {
            Irs21RespDto irs21RespDto = irs21RespDtoResultDto.getData();
            if (irs21RespDto != null) {
                lmtLadEval.setSingleEvalLimit(new BigDecimal(irs21RespDto.getCalvalue()));
                lmtLadEval.setSingleManualLimit(new BigDecimal(irs21RespDto.getCalvalue()));
                if(StringUtils.isBlank(lmtLadEval.getPkId())){
                    lmtLadEval.setPkId(UUID.randomUUID().toString());
                }
                return lmtLadEval;
            }else{
                throw BizException.error(null, EcbEnum.ECB010076.key, EcbEnum.ECB010076.value);
            }
        }else{
            throw BizException.error(null, irs21RespDtoResultDto.getCode(), irs21RespDtoResultDto.getMessage());
        }
    }

    ;

    /**
     * @方法名称：loanRating
     * @方法描述：債項評級測算
     * @创建人：zhangming12
     * @创建时间：2021/5/14 16:42
     * @修改记录：修改时间 修改人员 修改时间
     */
    public Map loanRating(LmtLadEval lmtLadEval)  throws Exception {
        List<BusContInfoDto> busContInfoList = ctrCvrgContService.getAllBusContInfo(lmtLadEval.getCusId());
        if(busContInfoList != null && busContInfoList.size() >0 ){
             return this.loanRating98(lmtLadEval);
        }else{
            return this.loanRating97(lmtLadEval);
        }
    }

    /**
     * @方法名称：loanRating
     * @方法描述：債項評級測算 -新增97
     * @创建人：zhangming12
     * @创建时间：2021/5/14 16:42
     * @修改记录：修改时间 修改人员 修改时间
     */
    public Map loanRating97(LmtLadEval lmtLadEval)  throws Exception {// 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        LmtApp lmtApp = lmtAppService.selectBySerno(lmtLadEval.getSerno());
        List<LmtAppSub> lmtAppSubs = lmtAppSubService.queryLmtAppSubBySerno(lmtLadEval.getSerno());
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
        }

        try{
            log.info("开始组装Irs97报文信息");
            Set<String> guarContSet = new HashSet<>();
            Set<String> guarDYSet = new HashSet<>();
            Set<String> guarZYSet = new HashSet<>();
            Set<String> guarBZSet = new HashSet<>();
            Set<String> grtContSet = new HashSet<>();
            Set<String> guarBaseSet = new HashSet<>();
            Irs97ReqDto irs97ReqDto = new Irs97ReqDto();
            irs97ReqDto.setUserid(userInfo.getUserId());//请求发起人编号
            irs97ReqDto.setUsername(userInfo.getUserName());//	请求发起人名称
            irs97ReqDto.setBrchno(userInfo.getOrg().getId());//请求机构编号
            irs97ReqDto.setBrchnm(userInfo.getOrg().getName());//请求机构名

            // 综合授信申请信息 LimitApplyInfo
            List<LimitApplyInfo> limitApplyInfos = new ArrayList<>();
            LimitApplyInfo limitApplyInfo = new LimitApplyInfo();
            limitApplyInfo.setSerno(lmtApp.getSerno());// 授信申请流水号
            limitApplyInfo.setFlag("1");// 新增变更标志 1:新增
            limitApplyInfo.setCus_id(lmtApp.getCusId());// 客户编号
            limitApplyInfo.setCus_name(lmtApp.getCusName());// 客户名称
            limitApplyInfo.setCus_type(lmtApp.getCusType());// 客户类型
            limitApplyInfo.setCur_type(lmtApp.getCurType());// 币种
            BigDecimal revoAmt = new BigDecimal("0.0");//循环额度总额
            BigDecimal notRevoAmt = new BigDecimal("0.0");//非循环额度
            for(LmtAppSub lmtAppSub : lmtAppSubs){
                if(!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())){
                    if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit())){
                        revoAmt = revoAmt.add(lmtAppSub.getLmtAmt());
                    }else{
                        notRevoAmt = notRevoAmt.add(lmtAppSub.getLmtAmt());
                    }
                }
            }
            limitApplyInfo.setApp_crd_totl_amt(revoAmt);
            limitApplyInfo.setApp_temp_crd_totl_amt(notRevoAmt);
            limitApplyInfo.setCrd_totl_sum_amt(new BigDecimal("0.0").add(lmtApp.getOpenTotalLmtAmt()).add(lmtApp.getLowRiskTotalLmtAmt())); // 授信总额（元）
            limitApplyInfo.setApp_crd_guar_amt(lmtApp.getLowRiskTotalLmtAmt());//低风险额度（元）
            limitApplyInfo.setStart_date(stringRedisTemplate.opsForValue().get("openDay"));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            if(lmtApp.getLmtTerm() == null){
                throw BizException.error(null, EcbEnum.ECB010096.key, EcbEnum.ECB010096.value);
            }
            limitApplyInfo.setEnd_date(simpleDateFormat.format(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()),lmtApp.getLmtTerm())));
            limitApplyInfo.setDelay_months(lmtApp.getLmtGraperTerm());
            limitApplyInfo.setInput_id(lmtApp.getInputId());
            limitApplyInfo.setInput_date(stringRedisTemplate.opsForValue().get("openDay"));
            limitApplyInfo.setInput_br_id(lmtApp.getInputBrId());
            limitApplyInfo.setManager_id(lmtApp.getManagerId());
            limitApplyInfo.setManager_br_id(lmtApp.getManagerBrId());
            limitApplyInfos.add(limitApplyInfo);
            irs97ReqDto.setLimitApplyInfo(limitApplyInfos);

            // 分项额度信息
            List<LimitDetailsInfo> limitDetailsInfos = new ArrayList<>();
            // 产品额度信息
            List<LimitProductInfo> limitProductInfos = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsPreLmt())||lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                    continue;
                }
                // 分项额度信息
                LimitDetailsInfo limitDetailsInfo = new LimitDetailsInfo();
                limitDetailsInfo.setDetail_serno(lmtAppSub.getSubSerno());
                limitDetailsInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                limitDetailsInfo.setSerno(lmtApp.getSerno());
                limitDetailsInfo.setLmt_serno(lmtApp.getSerno());
                limitDetailsInfo.setCus_id(lmtApp.getCusId());
                limitDetailsInfo.setCus_name(lmtApp.getCusName());
                limitDetailsInfo.setAssure_means_main(lmtAppSub.getGuarMode());
                String isRevolvLimit = "";
                if("1".equals(lmtAppSub.getIsRevolvLimit())){
                    isRevolvLimit = "10";
                }else if ("0".equals(lmtAppSub.getIsRevolvLimit())){
                    isRevolvLimit = "20";
                }
                limitDetailsInfo.setCrd_lmt_type(isRevolvLimit);
                limitDetailsInfo.setCur_type(lmtAppSub.getCurType());
                limitDetailsInfo.setCrd_lmt(lmtAppSub.getLmtAmt());
                limitDetailsInfo.setStart_date(stringRedisTemplate.opsForValue().get("openDay"));
                if(lmtAppSub.getLmtTerm() == null){
                    throw BizException.error(null, EcbEnum.ECB010097.key, EcbEnum.ECB010097.value);
                }
                limitDetailsInfo.setExpi_date(simpleDateFormat.format(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()),lmtAppSub.getLmtTerm())));
                // TIPS：新信贷的宽限期在授信及产品层，分项层空
                limitDetailsInfo.setDelay_months(lmtApp.getLmtGraperTerm());
                limitDetailsInfos.add(limitDetailsInfo);

                List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                    if(lmtAppSubPrd.getLmtBizType().startsWith("140203")||lmtAppSubPrd.getLmtBizType().startsWith("12010103")){
                        continue;
                    }
                    // 产品额度信息
                    LimitProductInfo limitProductInfo = new LimitProductInfo();
                    limitProductInfo.setDetail_serno(lmtAppSub.getSubSerno());
                    limitProductInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                    limitProductInfo.setPro_no(lmtAppSubPrd.getLmtBizType());
                    limitProductInfo.setPro_name(lmtAppSubPrd.getLmtBizTypeName());
                    limitProductInfo.setCrd_lmt(lmtAppSubPrd.getLmtAmt());
                    //TODO 取产品层下合同金额之和还是取分项层下合同金额之和
                    if(lmtAppSubPrd.getOrigiLmtAccSubPrdNo() != null){
                        String lmtAccNo = lmtAppSubPrd.getOrigiLmtAccSubPrdNo();
                        BigDecimal ctrAccpAmt = ctrAccpContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrLoanAmt = ctrLoanContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrDiscAmt = ctrDiscContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrCvrgAmt = ctrCvrgContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrTfLocAmt = ctrTfLocContService.getSumContAmt(lmtAccNo);
                        limitProductInfo.setSum_balance(ctrAccpAmt.add(ctrLoanAmt).add(ctrDiscAmt).add(ctrCvrgAmt).add(ctrTfLocAmt));
                    } else {
                        limitProductInfo.setSum_balance(new BigDecimal(0));
                    }
                    limitProductInfo.setMargin_ratio(lmtAppSubPrd.getBailPreRate());
                    limitProductInfos.add(limitProductInfo);
                }
            }
            if(limitDetailsInfos.size() > 0 && limitProductInfos.size() >0){
                irs97ReqDto.setLimitDetailsInfo(limitDetailsInfos);
                irs97ReqDto.setLimitProductInfo(limitProductInfos);
            }else{
                rtnCode = EcbEnum.ECB020057.key;
                rtnMsg = EcbEnum.ECB020057.value;
                return rtnData;
            }

            log.info("开始组装Irs97质押物信息");
            //质押物信息
            List<PledgeInfo> pledgeInfoList = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.GUAR_MODE_20.equals(lmtAppSub.getGuarMode())){
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("grtFlag","02");
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        hashMap.put("guarNo",guarBizRel.getGuarNo());
                        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                        if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                            if(guarZYSet.contains(guarBizRel.getGuarNo())){
                                continue;
                            }
                            guarZYSet.add(guarBizRel.getGuarNo());
                            PledgeInfo pledgeInfo = new PledgeInfo();
                            pledgeInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                            pledgeInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                            pledgeInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                            pledgeInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                            pledgeInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                            pledgeInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                            pledgeInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                            String guarStatus = "10001";
                            QueryModel queryModel = new QueryModel();
                            queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                            guarStatus = "10006";
                                            break;
                                        }
                                    }
                                }
                            }
                            pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                            pledgeInfo.setBond_type("");//债券类型
                            pledgeInfo.setOutrate_org(guarBaseInfo.getOuterLevelOrg());//外部评级机构
                            pledgeInfo.setOutrate_grade(guarBaseInfo.getOuterLevel());//外部评级等级
                            pledgeInfo.setInrate_grade("");
                            pledgeInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                            pledgeInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                            pledgeInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                            pledgeInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                            pledgeInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                            pledgeInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                            pledgeInfoList.add(pledgeInfo);
                        }
                    }
                }
            }
            irs97ReqDto.setPledgeInfo(pledgeInfoList);
            log.info("开始组装Irs97抵押物信息}");
            //抵押物信息
            List<MortgageInfo> mortgageInfoList = new ArrayList<>();
            int j = 0;
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.GUAR_MODE_10.equals(lmtAppSub.getGuarMode())){
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("grtFlag","01");
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        hashMap.put("guarNo",guarBizRel.getGuarNo());
                        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                        if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                            if(guarDYSet.contains(guarBizRel.getGuarNo())){
                                continue;
                            }
                            guarDYSet.add(guarBizRel.getGuarNo());
                            //抵押物信息
                            MortgageInfo mortgageInfo = new MortgageInfo();
                            mortgageInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                            mortgageInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                            mortgageInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                            mortgageInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                            mortgageInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                            mortgageInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                            mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                            String guarStatus = "10001";
                            QueryModel queryModel = new QueryModel();
                            queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                            guarStatus = "10006";
                                            break;
                                        }
                                    }
                                }
                            }
                            mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                            mortgageInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                            mortgageInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                            mortgageInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                            mortgageInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                            mortgageInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                            mortgageInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                            mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//评估价值
                            if(guarBaseInfo.getGuarTypeCd().startsWith("DY06")||guarBaseInfo.getGuarTypeCd().startsWith("DY02")||guarBaseInfo.getGuarTypeCd().startsWith("DY0102")||guarBaseInfo.getGuarTypeCd().startsWith("DY0103")||guarBaseInfo.getGuarTypeCd().startsWith("DY0101")||guarBaseInfo.getGuarTypeCd().startsWith("DY0199")){
                                j++;
                                Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                xddb01ReqDto.setGuaranty_id(guarBaseInfo.getGuarNo());
                                xddb01ReqDto.setType(guarBaseInfo.getGuarTypeCd());
                                ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                    Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                    if(!Objects.isNull(xddb01RespDto)){
                                        mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                        mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                        String landUp = xddb01RespDto.getLand_up();
                                        if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                            mortgageInfo.setAdhesion("010");
                                        }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                            mortgageInfo.setAdhesion("020");
                                        }else{
                                            mortgageInfo.setAdhesion("");
                                        }
                                        String remainYear = xddb01RespDto.getHouse_remainder_year();
                                        if("01".equals(remainYear)){
                                            mortgageInfo.setUse_year("010");
                                        }else if("02".equals(remainYear)){
                                            mortgageInfo.setUse_year("020");
                                        }else if("03".equals(remainYear)){
                                            mortgageInfo.setUse_year("030");
                                        }else if("04".equals(remainYear)){
                                            mortgageInfo.setUse_year("040");
                                        }else if("05".equals(remainYear)){
                                            mortgageInfo.setUse_year("050");
                                        }else if("06".equals(remainYear)){
                                            mortgageInfo.setUse_year("060");
                                        }else if("07".equals(remainYear)){
                                            mortgageInfo.setUse_year("070");
                                        }else{
                                            mortgageInfo.setUse_year("");
                                        }
                                        mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                        mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                        mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                    }
                                }else{
                                    log.error("接口【xddb01】发送【押品系统】异常！");
                                }
                            }
                            mortgageInfoList.add(mortgageInfo);
                        }
                    }
                }
            }
            irs97ReqDto.setMortgageInfo(mortgageInfoList);
            log.info("开始组装Irs97保证人信息");
            //保证人信息
            List<AssurePersonInfo> assurePersonInfoList = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                for (GuarBizRel guarBizRel : guarBizRelList) {
                    GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarBizRel.getGuarNo());
                    if (guarGuarantee != null) {
                        if(guarBZSet.contains(guarBizRel.getGuarNo())){
                            continue;
                        }
                        guarBZSet.add(guarBizRel.getGuarNo());
                        AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                        assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                        assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                        assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                        assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                        assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                        String comQysyz = guarGuarantee.getComQysyz();
                        if("01".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("010");//企业所有制
                        }else if("02".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("020");
                        }else if("03".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("030");
                        }else if("04".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("040");
                        }else if("05".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("050");
                        }
                        assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                        assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                        String isGuarCom = guarGuarantee.getIsGuarCom();
                        if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                            assurePersonInfo.setIsin_major("010");//是否专业担保公司
                        }else{
                            assurePersonInfo.setIsin_major("020");
                        }
                        assurePersonInfoList.add(assurePersonInfo);
                    }
                }
            }
            irs97ReqDto.setAssurePersonInfo(assurePersonInfoList);
            log.info("开始组装Irs97合同信息等");
            //合同信息
            List<BusContInfoDto> busContInfoList = ctrCvrgContService.getAllBusContInfo(lmtApp.getCusId());
            List<BusinessContractInfo> businessContractInfos = new ArrayList<>();
            //担保合同与合同关联信息
            //担保合同与抵质押、保证人关联信息
            List<BusinessAssureInfo> businessAssureInfos = new ArrayList<>();
            List<GuaranteePleMortInfo> guaranteePleMortInfos = new ArrayList<>();
            //担保合同信息(GuaranteeContrctInfo)
            List<GuaranteeContrctInfo> guaranteeContrctInfoList = new ArrayList<>();
            int i = 0;
            String itemId = "";
            LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
            for(BusContInfoDto busContInfoDto :busContInfoList){
                CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                reqDto.setDealBizNo(busContInfoDto.getContNo());
                ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                    log.info("额度编号查询成功");
                    itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                } else {
                    throw new Exception("查询额度接口异常");
                }
                BusinessContractInfo businessContractInfo = new BusinessContractInfo();
                Map<String,String> subMap = new HashMap<>();
                if(StringUtils.nonEmpty(itemId)){
                    LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                    if(lmtReplyAccSubPrd != null){
                        subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                        lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                        businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                    }else{
                        subMap.put("accSubNo",itemId);
                        lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                        businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                    }
                }
                businessContractInfo.setSerno(busContInfoDto.getSerno());
                businessContractInfo.setCont_no(busContInfoDto.getContNo());
                businessContractInfo.setCus_id(busContInfoDto.getCusId());
                businessContractInfo.setCus_name(busContInfoDto.getCusName());
                businessContractInfo.setLoan_form(busContInfoDto.getLoanModal());
                businessContractInfo.setPro_no(busContInfoDto.getPrdId());
                businessContractInfo.setPrd_name(busContInfoDto.getPrdName());
                businessContractInfo.setLoan_direction(busContInfoDto.getLoanTer());
                businessContractInfo.setCont_type(busContInfoDto.getContType());
                businessContractInfo.setAssure_means_main(busContInfoDto.getGuarMode());
                businessContractInfo.setCur_type(busContInfoDto.getCurType());
                businessContractInfo.setAcc_amount(busContInfoDto.getContAmt());
                businessContractInfo.setStart_date(busContInfoDto.getStartDate());
                businessContractInfo.setExpi_date(busContInfoDto.getEndDate());
                //businessContractInfo.setSum_low_balance(busContInfoDto.getContAmt);
                businessContractInfo.setCont_state(busContInfoDto.getContStatus());
                businessContractInfo.setInput_id(busContInfoDto.getInputId());
                businessContractInfo.setManager_id(busContInfoDto.getManagerId());
                businessContractInfo.setManager_br_id(busContInfoDto.getManagerBrId());
                businessContractInfo.setInput_id(busContInfoDto.getInputId());
                businessContractInfo.setFina_br_id(busContInfoDto.getFinaBrId());
                businessContractInfo.setGuarantee_no(busContInfoDto.getGuaranteeType());
                businessContractInfo.setGuarantee_name(busContInfoDto.getGuaranteeName());
                businessContractInfo.setLoancard_due(busContInfoDto.getIocTerm());
                businessContractInfo.setForward_days(busContInfoDto.getFastDay());
                businessContractInfo.setPro_details(busContInfoDto.getProDetails());
                businessContractInfo.setIsin_revocation(busContInfoDto.getIsinRevocation());
                businessContractInfo.setLoan_due(busContInfoDto.getContTerm());
                businessContractInfos.add(businessContractInfo);
                List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.getByContNo(busContInfoDto.getContNo());
                for(GrtGuarBizRstRel grtGuarBizRstRel:grtGuarBizRstRelList){
                    BusinessAssureInfo businessAssureInfo = new BusinessAssureInfo();
                    businessAssureInfo.setCont_no(grtGuarBizRstRel.getContNo());
                    businessAssureInfo.setGuar_cont_no(grtGuarBizRstRel.getGuarContNo());
                    businessAssureInfo.setSerialno(busContInfoDto.getSerno());
                    businessAssureInfos.add(businessAssureInfo);
                    if(grtGuarBizRstRel.getGuarContNo() != null && !"".equals(grtGuarBizRstRel.getGuarContNo())){
                        if(guarContSet.contains(grtGuarBizRstRel.getGuarContNo())){
                           continue;
                        }
                        guarContSet.add(grtGuarBizRstRel.getGuarContNo());
                        List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.getByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                        for(GrtGuarContRel grtGuarContRel : grtGuarContRels){
                            GuaranteePleMortInfo guaranteePleMortInfo = new GuaranteePleMortInfo();
                            guaranteePleMortInfo.setGuar_cont_no(grtGuarContRel.getGuarContNo());
                            guaranteePleMortInfo.setGuaranty_id(grtGuarContRel.getGuarNo());
                            guaranteePleMortInfos.add(guaranteePleMortInfo);
                            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarContRel.getGuarContNo());
                            GuaranteeContrctInfo guaranteeContrctInfo = new GuaranteeContrctInfo();
                            if(grtGuarCont != null){
                                if(grtContSet.contains(grtGuarCont.getGuarContNo())){
                                    continue;
                                }
                                grtContSet.add(grtGuarCont.getGuarContNo());
                                guaranteeContrctInfo.setGuar_cont_no(grtGuarCont.getGuarContNo());// 担保合同流水号
                                guaranteeContrctInfo.setItem_id(lmtReplyAccSub.getAccSubNo());// 授信台账编号
                                guaranteeContrctInfo.setLmt_serno(lmtApp.getSerno());// 授信协议编号
                                String guarContType = grtGuarCont.getGuarContType();
                                if("B".equals(guarContType)){
                                    guaranteeContrctInfo.setGuar_cont_type("2");// 担保合同类型
                                }else{
                                    guaranteeContrctInfo.setGuar_cont_type("1");
                                }
                                guaranteeContrctInfo.setGuar_way(grtGuarCont.getGuarWay());// 担保方式
                                guaranteeContrctInfo.setCur_type(grtGuarCont.getCurType());// 币种
                                guaranteeContrctInfo.setGuar_amt(grtGuarCont.getGuarAmt());// 担保金额（元）
                                guaranteeContrctInfo.setGuar_start_date(grtGuarCont.getGuarStartDate());// 担保起始日
                                guaranteeContrctInfo.setGuar_end_date(grtGuarCont.getGuarEndDate());// 担保到期日
                                guaranteeContrctInfo.setGuar_cont_state(grtGuarCont.getGuarContState());// 担保合同状态
                                guaranteeContrctInfoList.add(guaranteeContrctInfo);
                                if(grtGuarContRel.getGuarNo() != null && !"".equals(grtGuarContRel.getGuarNo())){
                                    //以质押物条件查询
                                    HashMap<String,String> hashMapZY = new HashMap<>();
                                    hashMapZY.put("grtFlag","02");
                                    hashMapZY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoZY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapZY);
                                    //以抵押物条件查询
                                    HashMap<String,String> hashMapDY = new HashMap<>();
                                    hashMapDY.put("grtFlag","01");
                                    hashMapDY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoDY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapDY);
                                    //以保证人条件查询
                                    GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());
                                    if(guarBaseInfoZY != null && StringUtils.nonEmpty(guarBaseInfoZY.getGuarNo())){ //若为质押物
                                        if(guarZYSet.contains(guarBaseInfoZY.getGuarNo())){
                                            continue;
                                        }
                                        guarZYSet.add(guarBaseInfoZY.getGuarNo());
                                        PledgeInfo pledgeInfo = new PledgeInfo();
                                        pledgeInfo.setGuaranty_id(guarBaseInfoZY.getGuarNo());// 担保ID
                                        pledgeInfo.setGage_type(guarBaseInfoZY.getGuarType());// 质押物类型
                                        pledgeInfo.setGuide_type(guarBaseInfoZY.getGuarTypeCd());// 担保品类型细分
                                        pledgeInfo.setGage_name(guarBaseInfoZY.getPldimnMemo());// 质押物名称
                                        pledgeInfo.setCurrency(guarBaseInfoZY.getCurType());// 币种
                                        pledgeInfo.setBook_amt(guarBaseInfoZY.getMaxMortagageAmt());// 权利价值（元）
                                        pledgeInfo.setEval_amt(guarBaseInfoZY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoZY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                                        pledgeInfo.setBond_type("");//债券类型
                                        pledgeInfo.setOutrate_org(guarBaseInfoZY.getOuterLevelOrg());//外部评级机构
                                        pledgeInfo.setOutrate_grade(guarBaseInfoZY.getOuterLevel());//外部评级等级
                                        pledgeInfo.setInrate_grade("");
                                        pledgeInfo.setEasy_nature(guarBaseInfoZY.getSupervisionConvenience());// 查封便利性
                                        pledgeInfo.setLaw_validity((guarBaseInfoZY.getLawValidity()));// 法律有效性
                                        pledgeInfo.setPle_cust_type(guarBaseInfoZY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        pledgeInfo.setPle_curr(guarBaseInfoZY.getPldimnCommon());// 抵质押品通用性
                                        pledgeInfo.setPle_cash(guarBaseInfoZY.getPldimnCashability());// 抵质押品变现能力
                                        pledgeInfo.setValue_wave(guarBaseInfoZY.getPriceWave());// 价格波动性
                                        pledgeInfoList.add(pledgeInfo);
                                    }else if (guarBaseInfoDY != null && StringUtils.nonEmpty(guarBaseInfoDY.getGuarNo())){//若为抵押物
                                        if(guarDYSet.contains(guarBaseInfoDY.getGuarNo())){
                                            continue;
                                        }
                                        guarDYSet.add(guarBaseInfoDY.getGuarNo());
                                        MortgageInfo mortgageInfo = new MortgageInfo();
                                        mortgageInfo.setGuaranty_id(guarBaseInfoDY.getGuarNo());// 担保ID
                                        mortgageInfo.setGage_type(guarBaseInfoDY.getGuarType());// 质押物类型
                                        mortgageInfo.setGuide_type(guarBaseInfoDY.getGuarTypeCd());// 担保品类型细分
                                        mortgageInfo.setGage_name(guarBaseInfoDY.getPldimnMemo());// 质押物名称
                                        mortgageInfo.setCurrency(guarBaseInfoDY.getCurType());// 币种
                                        mortgageInfo.setBook_amt(guarBaseInfoDY.getMaxMortagageAmt());// 权利价值（元）
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoDY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                                        mortgageInfo.setEasy_nature(guarBaseInfoDY.getSupervisionConvenience());// 查封便利性
                                        mortgageInfo.setLaw_validity((guarBaseInfoDY.getLawValidity()));// 法律有效性
                                        mortgageInfo.setPle_cust_type(guarBaseInfoDY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        mortgageInfo.setPle_curr(guarBaseInfoDY.getPldimnCommon());// 抵质押品通用性
                                        mortgageInfo.setPle_cash(guarBaseInfoDY.getPldimnCashability());// 抵质押品变现能力
                                        mortgageInfo.setValue_wave(guarBaseInfoDY.getPriceWave());// 价格波动性
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//评估价值
                                        if(guarBaseInfoDY.getGuarTypeCd().startsWith("DY06")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY02")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0102")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0103")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0101")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0199")){
                                            i ++;
                                            Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                            xddb01ReqDto.setGuaranty_id(guarBaseInfoDY.getGuarNo());
                                            xddb01ReqDto.setType(guarBaseInfoDY.getGuarTypeCd());
                                            ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                            log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                            if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                                Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                                if(!Objects.isNull(xddb01RespDto)){
                                                    mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                                    mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                                    String landUp = xddb01RespDto.getLand_up();
                                                    if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                                        mortgageInfo.setAdhesion("010");
                                                    }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                                        mortgageInfo.setAdhesion("020");
                                                    }else{
                                                        mortgageInfo.setAdhesion("");
                                                    }
                                                    String remainYear = xddb01RespDto.getHouse_remainder_year();
                                                    if("01".equals(remainYear)){
                                                        mortgageInfo.setUse_year("010");
                                                    }else if("02".equals(remainYear)){
                                                        mortgageInfo.setUse_year("020");
                                                    }else if("03".equals(remainYear)){
                                                        mortgageInfo.setUse_year("030");
                                                    }else if("04".equals(remainYear)){
                                                        mortgageInfo.setUse_year("040");
                                                    }else if("05".equals(remainYear)){
                                                        mortgageInfo.setUse_year("050");
                                                    }else if("06".equals(remainYear)){
                                                        mortgageInfo.setUse_year("060");
                                                    }else if("07".equals(remainYear)){
                                                        mortgageInfo.setUse_year("070");
                                                    }else{
                                                        mortgageInfo.setUse_year("");
                                                    }
                                                    mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                                    mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                                    mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                                }
                                            }else{
                                                log.error("接口【xddb01】发送【押品系统】异常！");
                                            }
                                        }
                                        mortgageInfoList.add(mortgageInfo);
                                    }else if (guarGuarantee != null){
                                        if(guarBZSet.contains(guarGuarantee.getGuarantyId())){
                                            continue;
                                        }
                                        guarBZSet.add(guarGuarantee.getGuarantyId());
                                        AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                                        assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                                        assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                                        assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                                        assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                                        assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                                        String comQysyz = guarGuarantee.getComQysyz();
                                        if("01".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("010");//企业所有制
                                        }else if("02".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("020");
                                        }else if("03".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("030");
                                        }else if("04".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("040");
                                        }else if("05".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("050");
                                        }
                                        assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                                        assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                                        String isGuarCom = guarGuarantee.getIsGuarCom();
                                        if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                                            assurePersonInfo.setIsin_major("010");//是否专业担保公司
                                        }else{
                                            assurePersonInfo.setIsin_major("020");
                                        }
                                        assurePersonInfoList.add(assurePersonInfo);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            irs97ReqDto.setBusinessContractInfo(businessContractInfos);
            irs97ReqDto.setPledgeInfo(pledgeInfoList);
            irs97ReqDto.setMortgageInfo(mortgageInfoList);
            log.info("发送押品系统次数："+j+","+i);
            irs97ReqDto.setAssurePersonInfo(assurePersonInfoList);
            irs97ReqDto.setBusinessAssureInfo(businessAssureInfos);
            irs97ReqDto.setGuaranteePleMortInfo(guaranteePleMortInfos);
            irs97ReqDto.setGuaranteeContrctInfo(guaranteeContrctInfoList);

            log.info("开始组装Irs97台账相关信息等");
            //非垫款借据信息(AccLoanInfo)
            String cusId = lmtApp.getCusId();
            List<AccLoanInfo> accLoanInfoList = new ArrayList<>();
            log.info("组装Irs97台账相关信息查询开始");
            List<AccLoanInfoDto> accLoanInfoDtoList = accDiscService.getAllBusAccInfo(cusId);
            log.info("组装Irs97台账相关信息查询完毕");

            //保证金信息
            List<AssureAccInfo> assureAccInfos = new ArrayList<>();
            List<BailAccInfo> bailAccInfoList = new ArrayList<>();
            List<String> guarContNoList = new ArrayList<>();
            for(AccLoanInfoDto accLoanInfoDto:accLoanInfoDtoList){
                if(StringUtil.isNotEmpty(accLoanInfoDto.getPvpSerno())){
                    AccLoanInfo accLoanInfo = new AccLoanInfo();
                    accLoanInfo.setBill_no(accLoanInfoDto.getBillNo());
                    accLoanInfo.setCont_no(accLoanInfoDto.getContNo());
                    CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                    reqDto.setDealBizNo(accLoanInfoDto.getContNo());
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                    if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                        log.info("额度编号查询成功");
                        itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                    } else {
                        throw new Exception("查询额度接口异常");
                    }
                    Map<String,String> subMap = new HashMap<>();
                    if(StringUtils.nonEmpty(itemId)){
                        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                        if(lmtReplyAccSubPrd != null){
                            subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        }else{
                            subMap.put("accSubNo",itemId);
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        }
                    }
                    //accLoanInfo.setLmt_serno(lmtApp.getSerno());
                    accLoanInfo.setPro_no(accLoanInfoDto.getPrdId());
                    accLoanInfo.setPrd_name(accLoanInfoDto.getPrdName());
                    accLoanInfo.setCus_name(accLoanInfoDto.getCusName());
                    accLoanInfo.setLoan_form(accLoanInfoDto.getLoanModal());
                    accLoanInfo.setAssure_means_main(accLoanInfoDto.getGuarMode());
                    accLoanInfo.setMargin_ratio(accLoanInfoDto.getMarginRatio());
                    accLoanInfo.setMargin_balance(accLoanInfoDto.getMarginBalance());
                    accLoanInfo.setBalance(accLoanInfoDto.getBalance());
                    accLoanInfo.setLoan_balance(accLoanInfoDto.getLoanBalance());
                    accLoanInfo.setPad_balance(accLoanInfoDto.getPadBalance());
                    accLoanInfo.setInterest_balance(accLoanInfoDto.getInterestBalance());
                    accLoanInfo.setHand_balance(accLoanInfoDto.getHandBalance());
                    accLoanInfo.setLoan_start_date(accLoanInfoDto.getLoanStartDate());
                    accLoanInfo.setLoan_end_date(accLoanInfoDto.getLoanEndDate());
                    accLoanInfo.setLoan_direction(accLoanInfoDto.getLoanTer());
                    accLoanInfo.setGuarantee_no(accLoanInfoDto.getGuaranteeNo());
                    accLoanInfo.setGuarantee_name(accLoanInfoDto.getGuaranteeName());
                    accLoanInfo.setLoancard_due(accLoanInfoDto.getLoancardDue());
                    accLoanInfo.setPro_details(accLoanInfoDto.getProDetails());
                    accLoanInfo.setLoan_paym_mtd(accLoanInfoDto.getLoanPaymMtd());
                    accLoanInfo.setIsin_card(accLoanInfoDto.getIsinCard());
                    accLoanInfo.setIsin_revocation(accLoanInfoDto.getIsinRevocation());
                    accLoanInfo.setLoan_due(accLoanInfoDto.getLoanDue());
                    accLoanInfo.setStatus(accLoanInfoDto.getStatus());
                    accLoanInfo.setBankstatus(accLoanInfoDto.getBankstatus());
                    accLoanInfo.setClear_status(accLoanInfoDto.getClearStatus());
                    accLoanInfo.setLow_cost(accLoanInfoDto.getLowCost());
                    accLoanInfo.setInput_id(accLoanInfoDto.getInputId());
                    accLoanInfo.setInput_br_id(accLoanInfoDto.getInputBrId());
                    accLoanInfo.setManager_id(accLoanInfoDto.getManagerId());
                    accLoanInfo.setManager_br_id(accLoanInfoDto.getManagerBrId());
                    accLoanInfo.setFina_br_id(accLoanInfoDto.getFinaBrId());
                    accLoanInfo.setForward_days(accLoanInfoDto.getForwardDays());
                    accLoanInfoList.add(accLoanInfo);
                }
            }

            List<BailInfoDto> bailInfoDtoList = accCvrsService.getBailInfoDto(cusId);
            for(BailInfoDto bailInfoDto : bailInfoDtoList){
                AssureAccInfo assureAccInfo = new AssureAccInfo();
                assureAccInfo.setSerialno(bailInfoDto.getSerialno());// 流水号
                assureAccInfo.setBill_no(bailInfoDto.getBillNo());// 借据编号
                assureAccInfo.setCur_type(bailInfoDto.getCurType()); // 保证金币种
                assureAccInfo.setSecurity_money_amt(bailInfoDto.getSecurityMoneyAmt()); // 保证金金额
                assureAccInfo.setCont_state("0");// 状态
                assureAccInfos.add(assureAccInfo);
            }
            irs97ReqDto.setAccLoanInfo(accLoanInfoList);
            irs97ReqDto.setAssureAccInfo(assureAccInfos);
            log.info("开始组装Irs97客户信息");
            //客户信息
            List<CustomerInfo> customerInfos = new ArrayList<>();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
            ResultDto<Map<String,String>> levelResult = iCusClientService.selectGradeInfoByCusId(lmtApp.getCusId());
            Map<String, String> data = new HashMap<>();
            if(levelResult != null){
                data = levelResult.getData();
            }
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId()).getData();
            if (cusCorpDto != null) {
                CustomerInfo customerInfo = new CustomerInfo();
                customerInfo.setCus_id(lmtApp.getCusId());//客户号
                customerInfo.setCus_name(lmtApp.getCusName());//客户名称  cuscorp这表数据库没有cusName字段
                customerInfo.setCus_type(cusCorpDto.getCusType());//客户类型
                customerInfo.setCert_code(cusBaseClientDto.getCertCode());//证件号码
                String cusType = cusBaseClientDto.getCertType();
                switch (cusBaseClientDto.getCertType()) {
                    case "M":
                        cusType = "24";
                        break;
                    case "N":
                        cusType = "26";
                        break;
                    case "P":
                        cusType = "27";
                        break;
                    case "Q":
                        cusType = "20";
                        break;
                    case "R":
                        cusType = "25";
                        break;
                    case "U":
                        cusType = "23";
                        break;
                    case "V":
                        cusType = "22";
                        break;
                }
                customerInfo.setCert_type(cusType);//证件类型
                String comQysyz = cusCorpDto.getCorpOwnersType();
                if("01".equals(comQysyz)){
                    customerInfo.setBus_owner("010");//企业所有制
                }else if("02".equals(comQysyz)){
                    customerInfo.setBus_owner("020");
                }else if("03".equals(comQysyz)){
                    customerInfo.setBus_owner("030");
                }else if("04".equals(comQysyz)){
                    customerInfo.setBus_owner("040");
                }else if("05".equals(comQysyz)){
                    customerInfo.setBus_owner("050");
                }
                customerInfo.setNew_industry_type(cusCorpDto.getTradeClass());//所属国标行业
                if("0".equals(cusCorpDto.getIsBankBasicDepAccNo())){
                    customerInfo.setBas_acc_flg("2");
                }else{
                    customerInfo.setBas_acc_flg(cusCorpDto.getIsBankBasicDepAccNo());//基本户是否在本行
                }
                ResultDto<Map<String, FinanIndicAnalyDto>> resultDto = iCusClientService.getFinRepRetProAndRatOfLia(cusId);
                log.info("当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(resultDto)+"】}");
                if(Objects.nonNull(resultDto) && Objects.nonNull(resultDto.getData())) {
                    Map<String, FinanIndicAnalyDto> resultMap = resultDto.getData();
                    BigDecimal assets = NumberUtils.nullDefaultZero(resultMap.get("lastYearSaleProfitRate").getCurYmValue()).divide(new BigDecimal("100"));
                    if(assets.compareTo(new BigDecimal(0)) >= 0 && assets.compareTo(new BigDecimal("0.5")) < 0){
                        customerInfo.setAssets("010"); //资产负债率
                    }else if (assets.compareTo(new BigDecimal("0.5")) >= 0 && assets.compareTo(new BigDecimal("0.7")) < 0){
                        customerInfo.setAssets("020");
                    }else if (assets.compareTo(new BigDecimal("0.7")) >= 0 && assets.compareTo(new BigDecimal("0.9")) < 0){
                        customerInfo.setAssets("030");
                    }else if (assets.compareTo(new BigDecimal("0.7")) >= 0){
                        customerInfo.setAssets("040");
                    }
                } else{
                    customerInfo.setAssets("");//资产负债率
                }
                String rank = "";
                if(data != null){
                    if(StringUtils.nonEmpty(data.get("finalRank"))){
                        rank = data.get("finalRank");
                    }
                }else {
                    if (StringUtils.nonEmpty(cusCorpDto.getBankLoanLevel())) {
                        rank = cusCorpDto.getBankLoanLevel();
                    }
                }
                if(StringUtils.nonEmpty(rank)){
                    customerInfo.setGrade(rank);//本行即期信用等级
                }else{
                    throw BizException.error(null, EcbEnum.ECB020071.key, EcbEnum.ECB020071.value);
                }
                customerInfo.setReg_area_code(cusCorpDto.getRegiAreaCode());//注册地行政区划代码
                customerInfo.setReg_area_name(cusCorpDto.getRegiAddr());//注册地行政区划代码
                customerInfo.setCust_mgr(lmtApp.getManagerId());//主管客户经理
                customerInfo.setMain_br_id(lmtApp.getManagerBrId());//主管机构
                customerInfos.add(customerInfo);
            }else{
                rtnCode = EcbEnum.ECB020058.key;
                rtnMsg = EcbEnum.ECB020058.value;
                return rtnData;
            }
            irs97ReqDto.setCustomerInfo(customerInfos);
            log.info("开始组装Irs97授信分项额度与抵质押、保证人关系信息");
            // 授信分项额度与抵质押、保证人关系信息
            List<LimitPleMortInfo> limitPleMortInfos = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                for (GuarBizRel guarBizRel : guarBizRelList) {
                    LimitPleMortInfo limitPleMortInfo = new LimitPleMortInfo();
                    limitPleMortInfo.setDetail_serno(guarBizRel.getSerno());
                    limitPleMortInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                    limitPleMortInfo.setGuaranty_id(guarBizRel.getGuarNo());
                    limitPleMortInfos.add(limitPleMortInfo);
                }
            }
            irs97ReqDto.setLimitPleMortInfo(limitPleMortInfos);
            log.info("开始组装Irs97最高额授信协议信息");
           // 最高额授信协议 CTR_HIGH_AMT_AGR_CONT
            List<CtrHighAmtAgrCont> ctrHighAmtAgrContList = ctrHighAmtAgrContService.selectDataByCusId(cusId);
            List<UpApplyInfo> upApplyInfos = new ArrayList<>();
            for (CtrHighAmtAgrCont ctrHighAmtAgrCont : ctrHighAmtAgrContList) {
                CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                reqDto.setDealBizNo(ctrHighAmtAgrCont.getContNo());
                ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                    log.info("额度编号查询成功");
                    itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                } else {
                    throw new Exception("查询额度接口异常");
                }
                UpApplyInfo upApplyInfo = new UpApplyInfo();
                upApplyInfo.setLmt_serno(ctrHighAmtAgrCont.getSerno());
                upApplyInfo.setItem_id(itemId);
                upApplyInfo.setCus_id(ctrHighAmtAgrCont.getCusId());
                upApplyInfo.setCus_name(ctrHighAmtAgrCont.getCusName());
                upApplyInfo.setCur_type(ctrHighAmtAgrCont.getAgrType());
                upApplyInfo.setAmt(ctrHighAmtAgrCont.getAgrAmt());
                if("600".equals(ctrHighAmtAgrCont.getContStatus())){
                    upApplyInfo.setStatus("300");
                }else{
                    upApplyInfo.setStatus(ctrHighAmtAgrCont.getContStatus());
                }
                upApplyInfo.setStart_date(ctrHighAmtAgrCont.getStartDate());
                upApplyInfo.setEnd_date(ctrHighAmtAgrCont.getEndDate());
                upApplyInfos.add(upApplyInfo);
            }
            irs97ReqDto.setUpApplyInfo(upApplyInfos);

            //交易对手信息

            //汇率信息
            List<CurInfo> curInfoList = new ArrayList<>();
            QueryModel model = new QueryModel();
            List<CfgTfRateDto> result = new ArrayList<>();
            ResultDto<List<CfgTfRateDto>> resultDto = iCmisCfgClientService.queryAllCfgTfRate(model);
            result = resultDto.getData();
            List<CfgTfRateDto> cfgTfRateDtos = JSONArray.parseArray(JSON.toJSONString(result), CfgTfRateDto.class);
            if(cfgTfRateDtos != null && cfgTfRateDtos.size()>0){
                for(CfgTfRateDto cfgTfRateDto : cfgTfRateDtos){
                    CurInfo curInfo = new CurInfo();
                    curInfo.setCurtype(cfgTfRateDto.getCurType());
                    BigDecimal EXUNIT = new BigDecimal(cfgTfRateDto.getExunit());
                    BigDecimal CSBYPR = cfgTfRateDto.getCsbypr();
                    curInfo.setCurvalue(CSBYPR.divide(EXUNIT));
                    curInfoList.add(curInfo);
                }
            }else{
                CurInfo curInfo = new CurInfo();
                curInfo.setCurtype("CNY");
                curInfo.setCurvalue(new BigDecimal(1));
                curInfoList.add(curInfo);
            }

            irs97ReqDto.setCurInfo(curInfoList);
            log.info("组装Irs97报文信息结束");
            log.info("Irs97报文为"+ JSON.toJSONString(irs97ReqDto)+"】}");
            ResultDto<Irs97RespDto> irs97RespDtoResultDto = dscms2IrsClientService.irs97(irs97ReqDto);
            log.info("单一客户债项评级新增测算返回:{}" + irs97RespDtoResultDto.toString());
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(irs97RespDtoResultDto.getCode())) {
                Irs97RespDto irs97RespDto = irs97RespDtoResultDto.getData();
                if (irs97RespDto != null) {
                    if(irs97RespDto.getAppMessageInfo() != null && irs97RespDto.getAppMessageInfo().size() > 0){
                        AppMessageInfo appMessageInfo = irs97RespDto.getAppMessageInfo().get(0);
                        lmtLadEval.setDebtLevel(appMessageInfo.getGuaranteeGrade());
                        lmtLadEval.setEad(appMessageInfo.getEad());
                        lmtLadEval.setLgd(appMessageInfo.getLgd());
                        List<AllMessageInfo> allMessageInfoList = irs97RespDto.getAllMessageInfo();
                        for (AllMessageInfo allMessageInfo : allMessageInfoList) {
                            LmtLadEval lmtLadEvalSub = BeanUtils.beanCopy(lmtLadEval, LmtLadEval.class);
                            lmtLadEvalSub.setSerno(allMessageInfo.getDetail_serno());
                            lmtLadEvalSub.setDebtLevel(allMessageInfo.getGuaranteeGrade());
                            lmtLadEvalSub.setEad(allMessageInfo.getEad());
                            lmtLadEvalSub.setLgd(allMessageInfo.getLgd());
                            this.insertSelective(lmtLadEvalSub);
                        }
                        List<MessageInfo> messageInfoList = irs97RespDto.getMessageInfo();
                        for (MessageInfo messageInfo : messageInfoList) {
                            LmtLadEval lmtLadEvalSubPrd = BeanUtils.beanCopy(lmtLadEval, LmtLadEval.class);
                            lmtLadEvalSubPrd.setSerno(messageInfo.getDetail_serno());
                            lmtLadEvalSubPrd.setDebtLevel(messageInfo.getGuaranteeGrade());
                            lmtLadEvalSubPrd.setEad(messageInfo.getEad());
                            lmtLadEvalSubPrd.setLgd(messageInfo.getLgd());
                            lmtLadEvalSubPrd.setBizType(messageInfo.getPro_no());
                            this.insertSelective(lmtLadEvalSubPrd);
                        }
                    }else{
                        throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                    }
                }else{
                    throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                }
            }else{
                throw BizException.error(null, irs97RespDtoResultDto.getCode(), irs97RespDtoResultDto.getMessage());
            }
            } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("lmtLadEval",lmtLadEval);
        }
        return rtnData;
    }


    /**
     * @方法名称：loanRating98
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/15 14:35
     * @修改记录：修改时间 修改人员 修改时间
     */
    public Map loanRating98(LmtLadEval lmtLadEval)  throws Exception {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        LmtApp lmtApp = lmtAppService.selectBySerno(lmtLadEval.getSerno());
        List<LmtAppSub> lmtAppSubs = lmtAppSubService.queryLmtAppSubBySerno(lmtLadEval.getSerno());
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
        }
        try{
            log.info("开始组装Irs98报文信息");
            Set<String> guarContSet = new HashSet<>();
            Set<String> guarDYSet = new HashSet<>();
            Set<String> guarZYSet = new HashSet<>();
            Set<String> guarBZSet = new HashSet<>();
            Set<String> grtContSet = new HashSet<>();
            Set<String> guarBaseSet = new HashSet<>();
            Irs98ReqDto irs98ReqDto = new Irs98ReqDto();
            irs98ReqDto.setUserid(userInfo.getUserId());//请求发起人编号
            irs98ReqDto.setUsername(userInfo.getUserName());//	请求发起人名称
            irs98ReqDto.setBrchno(userInfo.getOrg().getId());//请求机构编号
            irs98ReqDto.setBrchnm(userInfo.getOrg().getName());//请求机构名

            // 综合授信申请信息 LimitApplyInfo
            List<LimitApplyInfo> limitApplyInfos = new ArrayList<>();
            LimitApplyInfo limitApplyInfo = new LimitApplyInfo();
            limitApplyInfo.setSerno(lmtApp.getSerno());// 授信申请流水号
            limitApplyInfo.setFlag("2");// 新增变更标志 2:变更
            limitApplyInfo.setCus_id(lmtApp.getCusId());// 客户编号
            limitApplyInfo.setCus_name(lmtApp.getCusName());// 客户名称
            limitApplyInfo.setCus_type(lmtApp.getCusType());// 客户类型
            limitApplyInfo.setCur_type(lmtApp.getCurType());// 币种
            BigDecimal revoAmt = new BigDecimal("0.0");//循环额度总额
            BigDecimal notRevoAmt = new BigDecimal("0.0");//非循环额度
            for(LmtAppSub lmtAppSub : lmtAppSubs){
                if(!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) {
                    if (CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsRevolvLimit())) {
                        revoAmt = revoAmt.add(lmtAppSub.getLmtAmt());
                    } else {
                        notRevoAmt = notRevoAmt.add(lmtAppSub.getLmtAmt());
                    }
                }
            }
            limitApplyInfo.setApp_crd_totl_amt(revoAmt);
            limitApplyInfo.setApp_temp_crd_totl_amt(notRevoAmt);
            limitApplyInfo.setCrd_totl_sum_amt(new BigDecimal("0.0").add(lmtApp.getOpenTotalLmtAmt()).add(lmtApp.getLowRiskTotalLmtAmt())); // 授信总额（元）
            limitApplyInfo.setApp_crd_guar_amt(lmtApp.getLowRiskTotalLmtAmt());//低风险额度（元）
            limitApplyInfo.setStart_date(stringRedisTemplate.opsForValue().get("openDay"));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            if(lmtApp.getLmtTerm() == null){
                throw BizException.error(null, EcbEnum.ECB010096.key, EcbEnum.ECB010096.value);
            }
            limitApplyInfo.setEnd_date(simpleDateFormat.format(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()),lmtApp.getLmtTerm())));
            limitApplyInfo.setDelay_months(lmtApp.getLmtGraperTerm());
            limitApplyInfo.setInput_id(lmtApp.getInputId());
            limitApplyInfo.setInput_date(stringRedisTemplate.opsForValue().get("openDay"));
            limitApplyInfo.setInput_br_id(lmtApp.getInputBrId());
            limitApplyInfo.setManager_id(lmtApp.getManagerId());
            limitApplyInfo.setManager_br_id(lmtApp.getManagerBrId());
            limitApplyInfos.add(limitApplyInfo);
            irs98ReqDto.setLimitApplyInfo(limitApplyInfos);


            // 分项额度信息
            List<LimitDetailsInfo> limitDetailsInfos = new ArrayList<>();
            // 产品额度信息
            List<LimitProductInfo> limitProductInfos = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsPreLmt())||lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                    continue;
                }
                // 分项额度信息
                LimitDetailsInfo limitDetailsInfo = new LimitDetailsInfo();
                limitDetailsInfo.setDetail_serno(lmtAppSub.getSubSerno());
                limitDetailsInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                limitDetailsInfo.setSerno(lmtApp.getSerno());
                limitDetailsInfo.setLmt_serno(lmtApp.getSerno());
                limitDetailsInfo.setCus_id(lmtApp.getCusId());
                limitDetailsInfo.setCus_name(lmtApp.getCusName());
                limitDetailsInfo.setAssure_means_main(lmtAppSub.getGuarMode());
                String isRevolvLimit = "";
                if("1".equals(lmtAppSub.getIsRevolvLimit())){
                    isRevolvLimit = "10";
                }else if ("0".equals(lmtAppSub.getIsRevolvLimit())){
                    isRevolvLimit = "20";
                }
                limitDetailsInfo.setCrd_lmt_type(isRevolvLimit);
                limitDetailsInfo.setCur_type(lmtAppSub.getCurType());
                limitDetailsInfo.setCrd_lmt(lmtAppSub.getLmtAmt());
                limitDetailsInfo.setStart_date(stringRedisTemplate.opsForValue().get("openDay"));
                if(lmtAppSub.getLmtTerm() == null){
                    throw BizException.error(null, EcbEnum.ECB010097.key, EcbEnum.ECB010097.value);
                }
                limitDetailsInfo.setExpi_date(simpleDateFormat.format(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()),lmtAppSub.getLmtTerm())));

                // TIPS：新信贷的宽限期在授信及产品层，分项层空
                limitDetailsInfo.setDelay_months(lmtApp.getLmtGraperTerm());
                limitDetailsInfos.add(limitDetailsInfo);
                List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                    if(lmtAppSubPrd.getLmtBizType().startsWith("140203")||lmtAppSubPrd.getLmtBizType().startsWith("12010103")){
                        continue;
                    }
                    // 产品额度信息
                    LimitProductInfo limitProductInfo = new LimitProductInfo();
                    limitProductInfo.setDetail_serno(lmtAppSub.getSubSerno());
                    limitProductInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                    limitProductInfo.setPro_no(lmtAppSubPrd.getLmtBizType());
                    limitProductInfo.setPro_name(lmtAppSubPrd.getLmtBizTypeName());
                    limitProductInfo.setCrd_lmt(lmtAppSubPrd.getLmtAmt());
                    //TODO 取产品层下合同金额之和还是取分项层下合同金额之和
                    if(lmtAppSubPrd.getOrigiLmtAccSubPrdNo() != null){
                        String lmtAccNo = lmtAppSubPrd.getOrigiLmtAccSubPrdNo();
                        BigDecimal ctrAccpAmt = ctrAccpContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrLoanAmt = ctrLoanContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrDiscAmt = ctrDiscContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrCvrgAmt = ctrCvrgContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrTfLocAmt = ctrTfLocContService.getSumContAmt(lmtAccNo);
                        limitProductInfo.setSum_balance(ctrAccpAmt.add(ctrLoanAmt).add(ctrDiscAmt).add(ctrCvrgAmt).add(ctrTfLocAmt));
                    } else {
                        limitProductInfo.setSum_balance(new BigDecimal(0));
                    }
                    limitProductInfo.setMargin_ratio(lmtAppSubPrd.getBailPreRate());
                    limitProductInfos.add(limitProductInfo);
                }
            }
            if(limitDetailsInfos.size() > 0 && limitProductInfos.size() >0){
                irs98ReqDto.setLimitDetailsInfo(limitDetailsInfos);
                irs98ReqDto.setLimitProductInfo(limitProductInfos);
            }else{
                rtnCode = EcbEnum.ECB020057.key;
                rtnMsg = EcbEnum.ECB020057.value;
                return rtnData;
            }

            log.info("开始组装Irs98质押物信息信息");
            //质押物信息
            List<PledgeInfo> pledgeInfoList = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.GUAR_MODE_20.equals(lmtAppSub.getGuarMode())){
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("grtFlag","02");
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        hashMap.put("guarNo",guarBizRel.getGuarNo());
                        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                        if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                            if(guarZYSet.contains(guarBizRel.getGuarNo())){
                                continue;
                            }
                            guarZYSet.add(guarBizRel.getGuarNo());
                            PledgeInfo pledgeInfo = new PledgeInfo();
                            pledgeInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                            pledgeInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                            pledgeInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                            pledgeInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                            pledgeInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                            pledgeInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                            pledgeInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                            String guarStatus = "10001";
                            QueryModel queryModel = new QueryModel();
                            queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                            guarStatus = "10006";
                                            break;
                                        }
                                    }
                                }
                            }
                            pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                            pledgeInfo.setBond_type("");//债券类型
                            pledgeInfo.setOutrate_org(guarBaseInfo.getOuterLevelOrg());//外部评级机构
                            pledgeInfo.setOutrate_grade(guarBaseInfo.getOuterLevel());//外部评级等级
                            pledgeInfo.setInrate_grade("");//内部评级等级
                            pledgeInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                            pledgeInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                            pledgeInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                            pledgeInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                            pledgeInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                            pledgeInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                            pledgeInfoList.add(pledgeInfo);
                        }
                    }
                }
            }
            irs98ReqDto.setPledgeInfo(pledgeInfoList);
            log.info("开始组装Irs98抵押物信息");
            //抵押物信息
            List<MortgageInfo> mortgageInfoList = new ArrayList<>();
            int j = 0;
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                if(CmisCommonConstants.GUAR_MODE_10.equals(lmtAppSub.getGuarMode())){
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("grtFlag","01");
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        hashMap.put("guarNo",guarBizRel.getGuarNo());
                        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                        if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                            if(guarDYSet.contains(guarBizRel.getGuarNo())){
                                continue;
                            }
                            guarDYSet.add(guarBizRel.getGuarNo());
                            //抵押物信息
                            MortgageInfo mortgageInfo = new MortgageInfo();
                            mortgageInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                            mortgageInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                            mortgageInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                            mortgageInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                            mortgageInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                            mortgageInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                            mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                            String guarStatus = "10001";
                            QueryModel queryModel = new QueryModel();
                            queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                            guarStatus = "10006";
                                            break;
                                        }
                                    }
                                }
                            }
                            mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                            mortgageInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                            mortgageInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                            mortgageInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                            mortgageInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                            mortgageInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                            mortgageInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                            mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//评估价值
                            if(guarBaseInfo.getGuarTypeCd().startsWith("DY06")||guarBaseInfo.getGuarTypeCd().startsWith("DY02")||guarBaseInfo.getGuarTypeCd().startsWith("DY0102")||guarBaseInfo.getGuarTypeCd().startsWith("DY0103")||guarBaseInfo.getGuarTypeCd().startsWith("DY0101")||guarBaseInfo.getGuarTypeCd().startsWith("DY0199")){
                                j++;
                                Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                xddb01ReqDto.setGuaranty_id(guarBaseInfo.getGuarNo());
                                xddb01ReqDto.setType(guarBaseInfo.getGuarTypeCd());
                                ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                    Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                    if(!Objects.isNull(xddb01RespDto)){
                                        mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                        mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                        String landUp = xddb01RespDto.getLand_up();
                                        if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                            mortgageInfo.setAdhesion("010");
                                        }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                            mortgageInfo.setAdhesion("020");
                                        }else{
                                            mortgageInfo.setAdhesion("");
                                        }
                                        String remainYear = xddb01RespDto.getHouse_remainder_year();
                                        if("01".equals(remainYear)){
                                            mortgageInfo.setUse_year("010");
                                        }else if("02".equals(remainYear)){
                                            mortgageInfo.setUse_year("020");
                                        }else if("03".equals(remainYear)){
                                            mortgageInfo.setUse_year("030");
                                        }else if("04".equals(remainYear)){
                                            mortgageInfo.setUse_year("040");
                                        }else if("05".equals(remainYear)){
                                            mortgageInfo.setUse_year("050");
                                        }else if("06".equals(remainYear)){
                                            mortgageInfo.setUse_year("060");
                                        }else if("07".equals(remainYear)){
                                            mortgageInfo.setUse_year("070");
                                        }else{
                                            mortgageInfo.setUse_year("");
                                        }
                                        mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                        mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                        mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                    }
                                }else{
                                    log.error("接口【xddb01】发送【押品系统】异常！");
                                }
                            }
                            mortgageInfoList.add(mortgageInfo);
                        }
                    }
                }
            }
            irs98ReqDto.setMortgageInfo(mortgageInfoList);
            log.info("开始组装Irs98保证人信息");
            //保证人信息
            List<AssurePersonInfo> assurePersonInfoList = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                for (GuarBizRel guarBizRel : guarBizRelList) {
                    GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarBizRel.getGuarNo());
                    if (guarGuarantee != null) {
                        if(guarBZSet.contains(guarBizRel.getGuarNo())){
                            continue;
                        }
                        guarBZSet.add(guarBizRel.getGuarNo());
                        AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                        assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                        assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                        assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                        assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                        assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                        String comQysyz = guarGuarantee.getComQysyz();
                        if("01".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("010");//企业所有制
                        }else if("02".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("020");
                        }else if("03".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("030");
                        }else if("04".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("040");
                        }else if("05".equals(comQysyz)){
                            assurePersonInfo.setBus_owner("050");
                        }
                        assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                        assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                        String isGuarCom = guarGuarantee.getIsGuarCom();
                        if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                            assurePersonInfo.setIsin_major("010");//是否专业担保公司
                        }else{
                            assurePersonInfo.setIsin_major("020");
                        }
                        assurePersonInfoList.add(assurePersonInfo);
                    }
                }
            }
            irs98ReqDto.setAssurePersonInfo(assurePersonInfoList);
            log.info("开始组装Irs98合同信息");
            //合同信息
            List<BusContInfoDto> busContInfoList = ctrCvrgContService.getAllBusContInfo(lmtApp.getCusId());
            List<BusinessContractInfo> businessContractInfos = new ArrayList<>();
            //担保合同与合同关联信息
            //担保合同与抵质押、保证人关联信息
            List<BusinessAssureInfo> businessAssureInfos = new ArrayList<>();
            List<GuaranteePleMortInfo> guaranteePleMortInfos = new ArrayList<>();
            //担保合同信息(GuaranteeContrctInfo)
            List<GuaranteeContrctInfo> guaranteeContrctInfoList = new ArrayList<>();
            String itemId = "";
            int i = 0;
            LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
            for(BusContInfoDto busContInfoDto :busContInfoList){
                CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                reqDto.setDealBizNo(busContInfoDto.getContNo());
                ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                    log.info("额度编号查询成功");
                    itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                } else {
                    throw new Exception("查询额度接口异常");
                }
                BusinessContractInfo businessContractInfo = new BusinessContractInfo();
                Map<String,String> subMap = new HashMap<>();
                if(StringUtils.nonEmpty(itemId)){
                    LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                    if(lmtReplyAccSubPrd != null){
                        subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                        lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                        businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                    }else{
                        subMap.put("accSubNo",itemId);
                        lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                        businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                    }
                }else{
                    businessContractInfo.setItem_id("");
                    businessContractInfo.setLmt_serno("");
                }
                businessContractInfo.setSerno(busContInfoDto.getSerno());
                businessContractInfo.setCont_no(busContInfoDto.getContNo());
                businessContractInfo.setCus_id(busContInfoDto.getCusId());
                businessContractInfo.setCus_name(busContInfoDto.getCusName());
                businessContractInfo.setLoan_form(busContInfoDto.getLoanModal());
                businessContractInfo.setPro_no(busContInfoDto.getPrdId());
                businessContractInfo.setPrd_name(busContInfoDto.getPrdName());
                businessContractInfo.setLoan_direction(busContInfoDto.getLoanTer());
                businessContractInfo.setCont_type(busContInfoDto.getContType());
                businessContractInfo.setAssure_means_main(busContInfoDto.getGuarMode());
                businessContractInfo.setCur_type(busContInfoDto.getCurType());
                businessContractInfo.setAcc_amount(busContInfoDto.getContAmt());
                businessContractInfo.setStart_date(busContInfoDto.getStartDate());
                businessContractInfo.setExpi_date(busContInfoDto.getEndDate());
                //businessContractInfo.setSum_low_balance(busContInfoDto.getContAmt);
                businessContractInfo.setCont_state(busContInfoDto.getContStatus());
                businessContractInfo.setInput_id(busContInfoDto.getInputId());
                businessContractInfo.setManager_id(busContInfoDto.getManagerId());
                businessContractInfo.setManager_br_id(busContInfoDto.getManagerBrId());
                businessContractInfo.setInput_id(busContInfoDto.getInputId());
                businessContractInfo.setFina_br_id(busContInfoDto.getFinaBrId());
                businessContractInfo.setGuarantee_no(busContInfoDto.getGuaranteeType());
                businessContractInfo.setGuarantee_name(busContInfoDto.getGuaranteeName());
                businessContractInfo.setLoancard_due(busContInfoDto.getIocTerm());
                businessContractInfo.setForward_days(busContInfoDto.getFastDay());
                businessContractInfo.setPro_details(busContInfoDto.getProDetails());
                businessContractInfo.setIsin_revocation(busContInfoDto.getIsinRevocation());
                businessContractInfo.setLoan_due(busContInfoDto.getContTerm());
                businessContractInfos.add(businessContractInfo);
                List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.getByContNo(busContInfoDto.getContNo());
                for(GrtGuarBizRstRel grtGuarBizRstRel:grtGuarBizRstRelList){
                    BusinessAssureInfo businessAssureInfo = new BusinessAssureInfo();
                    businessAssureInfo.setCont_no(grtGuarBizRstRel.getContNo());
                    businessAssureInfo.setGuar_cont_no(grtGuarBizRstRel.getGuarContNo());
                    businessAssureInfo.setSerialno(busContInfoDto.getSerno());
                    businessAssureInfos.add(businessAssureInfo);
                    if(grtGuarBizRstRel.getGuarContNo() != null && !"".equals(grtGuarBizRstRel.getGuarContNo())){
                        if(guarContSet.contains(grtGuarBizRstRel.getGuarContNo())){
                            continue;
                        }
                        guarContSet.add(grtGuarBizRstRel.getGuarContNo());
                        List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.getByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                        for(GrtGuarContRel grtGuarContRel : grtGuarContRels){
                            GuaranteePleMortInfo guaranteePleMortInfo = new GuaranteePleMortInfo();
                            guaranteePleMortInfo.setGuar_cont_no(grtGuarContRel.getGuarContNo());
                            guaranteePleMortInfo.setGuaranty_id(grtGuarContRel.getGuarNo());
                            guaranteePleMortInfos.add(guaranteePleMortInfo);
                            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarContRel.getGuarContNo());
                            GuaranteeContrctInfo guaranteeContrctInfo = new GuaranteeContrctInfo();
                            if(grtGuarCont != null){
                                if(grtContSet.contains(grtGuarCont.getGuarContNo())){
                                    continue;
                                }
                                grtContSet.add(grtGuarCont.getGuarContNo());
                                guaranteeContrctInfo.setGuar_cont_no(grtGuarCont.getGuarContNo());// 担保合同流水号
                                guaranteeContrctInfo.setItem_id(lmtReplyAccSub.getAccSubNo());// 授信台账编号
                                guaranteeContrctInfo.setLmt_serno(lmtApp.getSerno());// 授信协议编号
                                String guarContType = grtGuarCont.getGuarContType();
                                if("B".equals(guarContType)){
                                    guaranteeContrctInfo.setGuar_cont_type("2");// 担保合同类型
                                }else{
                                    guaranteeContrctInfo.setGuar_cont_type("1");
                                }
                                guaranteeContrctInfo.setGuar_way(grtGuarCont.getGuarWay());// 担保方式
                                guaranteeContrctInfo.setCur_type(grtGuarCont.getCurType());// 币种
                                guaranteeContrctInfo.setGuar_amt(grtGuarCont.getGuarAmt());// 担保金额（元）
                                guaranteeContrctInfo.setGuar_start_date(grtGuarCont.getGuarStartDate());// 担保起始日
                                guaranteeContrctInfo.setGuar_end_date(grtGuarCont.getGuarEndDate());// 担保到期日
                                guaranteeContrctInfo.setGuar_cont_state(grtGuarCont.getGuarContState());// 担保合同状态
                                guaranteeContrctInfoList.add(guaranteeContrctInfo);
                                if(grtGuarContRel.getGuarNo() != null && StringUtils.nonEmpty(grtGuarContRel.getGuarNo())){
                                    //以质押物条件查询
                                    HashMap<String,String> hashMapZY = new HashMap<>();
                                    hashMapZY.put("grtFlag","02");
                                    hashMapZY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoZY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapZY);
                                    //以抵押物条件查询
                                    HashMap<String,String> hashMapDY = new HashMap<>();
                                    hashMapDY.put("grtFlag","01");
                                    hashMapDY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoDY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapDY);
                                    //以保证人条件查询
                                    GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());
                                    if(guarBaseInfoZY != null &&StringUtils.nonEmpty(guarBaseInfoZY.getGuarNo())){ //若为质押物
                                        if(guarZYSet.contains(guarBaseInfoZY.getGuarNo())){
                                            continue;
                                        }
                                        guarZYSet.add(guarBaseInfoZY.getGuarNo());
                                        PledgeInfo pledgeInfo = new PledgeInfo();
                                        pledgeInfo.setGuaranty_id(guarBaseInfoZY.getGuarNo());// 担保ID
                                        pledgeInfo.setGage_type(guarBaseInfoZY.getGuarType());// 质押物类型
                                        pledgeInfo.setGuide_type(guarBaseInfoZY.getGuarTypeCd());// 担保品类型细分
                                        pledgeInfo.setGage_name(guarBaseInfoZY.getPldimnMemo());// 质押物名称
                                        pledgeInfo.setCurrency(guarBaseInfoZY.getCurType());// 币种
                                        pledgeInfo.setBook_amt(guarBaseInfoZY.getMaxMortagageAmt());// 权利价值（元）
                                        pledgeInfo.setEval_amt(guarBaseInfoZY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoZY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                                        pledgeInfo.setBond_type("");//债券类型
                                        pledgeInfo.setOutrate_org(guarBaseInfoZY.getOuterLevelOrg());//外部评级机构
                                        pledgeInfo.setOutrate_grade(guarBaseInfoZY.getOuterLevel());//外部评级等级
                                        pledgeInfo.setInrate_grade("");
                                        pledgeInfo.setEasy_nature(guarBaseInfoZY.getSupervisionConvenience());// 查封便利性
                                        pledgeInfo.setLaw_validity((guarBaseInfoZY.getLawValidity()));// 法律有效性
                                        pledgeInfo.setPle_cust_type(guarBaseInfoZY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        pledgeInfo.setPle_curr(guarBaseInfoZY.getPldimnCommon());// 抵质押品通用性
                                        pledgeInfo.setPle_cash(guarBaseInfoZY.getPldimnCashability());// 抵质押品变现能力
                                        pledgeInfo.setValue_wave(guarBaseInfoZY.getPriceWave());// 价格波动性
                                        pledgeInfoList.add(pledgeInfo);
                                    }else if (guarBaseInfoDY != null && StringUtils.nonEmpty(guarBaseInfoDY.getGuarNo())){//若为抵押物
                                        if(guarDYSet.contains(guarBaseInfoDY.getGuarNo())){
                                            continue;
                                        }
                                        guarDYSet.add(guarBaseInfoDY.getGuarNo());
                                        MortgageInfo mortgageInfo = new MortgageInfo();
                                        mortgageInfo.setGuaranty_id(guarBaseInfoDY.getGuarNo());// 担保ID
                                        mortgageInfo.setGage_type(guarBaseInfoDY.getGuarType());// 质押物类型
                                        mortgageInfo.setGuide_type(guarBaseInfoDY.getGuarTypeCd());// 担保品类型细分
                                        mortgageInfo.setGage_name(guarBaseInfoDY.getPldimnMemo());// 质押物名称
                                        mortgageInfo.setCurrency(guarBaseInfoDY.getCurType());// 币种
                                        mortgageInfo.setBook_amt(guarBaseInfoDY.getMaxMortagageAmt());// 权利价值（元）
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoDY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                                        mortgageInfo.setEasy_nature(guarBaseInfoDY.getSupervisionConvenience());// 查封便利性
                                        mortgageInfo.setLaw_validity((guarBaseInfoDY.getLawValidity()));// 法律有效性
                                        mortgageInfo.setPle_cust_type(guarBaseInfoDY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        mortgageInfo.setPle_curr(guarBaseInfoDY.getPldimnCommon());// 抵质押品通用性
                                        mortgageInfo.setPle_cash(guarBaseInfoDY.getPldimnCashability());// 抵质押品变现能力
                                        mortgageInfo.setValue_wave(guarBaseInfoDY.getPriceWave());// 价格波动性
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//评估价值
                                        if(guarBaseInfoDY.getGuarTypeCd().startsWith("DY06")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY02")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0102")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0103")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0101")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0199")){
                                            i ++;
                                            Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                            xddb01ReqDto.setGuaranty_id(guarBaseInfoDY.getGuarNo());
                                            xddb01ReqDto.setType(guarBaseInfoDY.getGuarTypeCd());
                                            ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                            log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                            if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                                Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                                if(!Objects.isNull(xddb01RespDto)){
                                                    mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                                    mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                                    String landUp = xddb01RespDto.getLand_up();
                                                    if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                                        mortgageInfo.setAdhesion("010");
                                                    }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                                        mortgageInfo.setAdhesion("020");
                                                    }else{
                                                        mortgageInfo.setAdhesion("");
                                                    }
                                                    String remainYear = xddb01RespDto.getHouse_remainder_year();
                                                    if("01".equals(remainYear)){
                                                        mortgageInfo.setUse_year("010");
                                                    }else if("02".equals(remainYear)){
                                                        mortgageInfo.setUse_year("020");
                                                    }else if("03".equals(remainYear)){
                                                        mortgageInfo.setUse_year("030");
                                                    }else if("04".equals(remainYear)){
                                                        mortgageInfo.setUse_year("040");
                                                    }else if("05".equals(remainYear)){
                                                        mortgageInfo.setUse_year("050");
                                                    }else if("06".equals(remainYear)){
                                                        mortgageInfo.setUse_year("060");
                                                    }else if("07".equals(remainYear)){
                                                        mortgageInfo.setUse_year("070");
                                                    }else{
                                                        mortgageInfo.setUse_year("");
                                                    }
                                                    mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                                    mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                                    mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                                }
                                            }else{
                                                log.error("接口【xddb01】发送【押品系统】异常！");
                                            }
                                        }
                                        mortgageInfoList.add(mortgageInfo);
                                    }else if (guarGuarantee != null){
                                        if(guarBZSet.contains(guarGuarantee.getGuarantyId())){
                                            continue;
                                        }
                                        guarBZSet.add(guarGuarantee.getGuarantyId());
                                        AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                                        assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                                        assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                                        assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                                        assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                                        assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                                        String comQysyz = guarGuarantee.getComQysyz();
                                        if("01".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("010");//企业所有制
                                        }else if("02".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("020");
                                        }else if("03".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("030");
                                        }else if("04".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("040");
                                        }else if("05".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("050");
                                        }
                                        assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                                        assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                                        String isGuarCom = guarGuarantee.getIsGuarCom();
                                        if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                                            assurePersonInfo.setIsin_major("010");//是否专业担保公司
                                        }else{
                                            assurePersonInfo.setIsin_major("020");
                                        }
                                        assurePersonInfoList.add(assurePersonInfo);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            irs98ReqDto.setBusinessContractInfo(businessContractInfos);
            irs98ReqDto.setPledgeInfo(pledgeInfoList);
            irs98ReqDto.setMortgageInfo(mortgageInfoList);
            log.info("发送押品系统次数："+j+","+i);
            irs98ReqDto.setAssurePersonInfo(assurePersonInfoList);
            irs98ReqDto.setBusinessAssureInfo(businessAssureInfos);
            irs98ReqDto.setGuaranteePleMortInfo(guaranteePleMortInfos);
            irs98ReqDto.setGuaranteeContrctInfo(guaranteeContrctInfoList);
            log.info("开始组装Irs98台账相关信息");
            //非垫款借据信息(AccLoanInfo)
            String cusId = lmtApp.getCusId();
            List<AccLoanInfo> accLoanInfoList = new ArrayList<>();
            log.info("组装Irs98台账相关信息查询开始");
            List<AccLoanInfoDto> accLoanInfoDtoList = accDiscService.getAllBusAccInfo(cusId);
            log.info("组装Irs98台账相关信息查询开始");

            //保证金信息
            List<AssureAccInfo> assureAccInfos = new ArrayList<>();
            List<BailAccInfo> bailAccInfoList = new ArrayList<>();
            List<String> guarContNoList = new ArrayList<>();
            for(AccLoanInfoDto accLoanInfoDto:accLoanInfoDtoList){
                if(StringUtil.isNotEmpty(accLoanInfoDto.getPvpSerno())){
                    AccLoanInfo accLoanInfo = new AccLoanInfo();
                    accLoanInfo.setBill_no(accLoanInfoDto.getBillNo());
                    accLoanInfo.setCont_no(accLoanInfoDto.getContNo());
                    Map<String,String> subMap = new HashMap<>();
                    CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                    reqDto.setDealBizNo(accLoanInfoDto.getContNo());
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                    if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                        log.info("额度编号查询成功");
                        itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                    } else {
                        throw new Exception("查询额度接口异常");
                    }
                    subMap.put("accSubNo",accLoanInfoDto.getLmtAccNo());
                    if(StringUtils.nonEmpty(itemId)){
                        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                        if(lmtReplyAccSubPrd != null){
                            subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        }else{
                            subMap.put("accSubNo",itemId);
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                        }
                    }
                    //accLoanInfo.setLmt_serno(lmtApp.getSerno());
                    accLoanInfo.setPro_no(accLoanInfoDto.getPrdId());
                    accLoanInfo.setPrd_name(accLoanInfoDto.getPrdName());
                    accLoanInfo.setCus_name(accLoanInfoDto.getCusName());
                    accLoanInfo.setLoan_form(accLoanInfoDto.getLoanModal());
                    accLoanInfo.setAssure_means_main(accLoanInfoDto.getGuarMode());
                    accLoanInfo.setMargin_ratio(accLoanInfoDto.getMarginRatio());
                    accLoanInfo.setMargin_balance(accLoanInfoDto.getMarginBalance());
                    accLoanInfo.setBalance(accLoanInfoDto.getBalance());
                    accLoanInfo.setLoan_balance(accLoanInfoDto.getLoanBalance());
                    accLoanInfo.setPad_balance(accLoanInfoDto.getPadBalance());
                    accLoanInfo.setInterest_balance(accLoanInfoDto.getInterestBalance());
                    accLoanInfo.setHand_balance(accLoanInfoDto.getHandBalance());
                    accLoanInfo.setLoan_start_date(accLoanInfoDto.getLoanStartDate());
                    accLoanInfo.setLoan_end_date(accLoanInfoDto.getLoanEndDate());
                    accLoanInfo.setLoan_direction(accLoanInfoDto.getLoanTer());
                    accLoanInfo.setGuarantee_no(accLoanInfoDto.getGuaranteeNo());
                    accLoanInfo.setGuarantee_name(accLoanInfoDto.getGuaranteeName());
                    accLoanInfo.setLoancard_due(accLoanInfoDto.getLoancardDue());
                    accLoanInfo.setPro_details(accLoanInfoDto.getProDetails());
                    accLoanInfo.setLoan_paym_mtd(accLoanInfoDto.getLoanPaymMtd());
                    accLoanInfo.setIsin_card(accLoanInfoDto.getIsinCard());
                    accLoanInfo.setIsin_revocation(accLoanInfoDto.getIsinRevocation());
                    accLoanInfo.setLoan_due(accLoanInfoDto.getLoanDue());
                    accLoanInfo.setStatus(accLoanInfoDto.getStatus());
                    accLoanInfo.setBankstatus(accLoanInfoDto.getBankstatus());
                    accLoanInfo.setClear_status(accLoanInfoDto.getClearStatus());
                    accLoanInfo.setLow_cost(accLoanInfoDto.getLowCost());
                    accLoanInfo.setInput_id(accLoanInfoDto.getInputId());
                    accLoanInfo.setInput_br_id(accLoanInfoDto.getInputBrId());
                    accLoanInfo.setManager_id(accLoanInfoDto.getManagerId());
                    accLoanInfo.setManager_br_id(accLoanInfoDto.getManagerBrId());
                    accLoanInfo.setFina_br_id(accLoanInfoDto.getFinaBrId());
                    accLoanInfo.setForward_days(accLoanInfoDto.getForwardDays());
                    accLoanInfoList.add(accLoanInfo);
                }
            }

            List<BailInfoDto> bailInfoDtoList = accCvrsService.getBailInfoDto(cusId);
            for(BailInfoDto bailInfoDto : bailInfoDtoList){
                AssureAccInfo assureAccInfo = new AssureAccInfo();
                assureAccInfo.setSerialno(bailInfoDto.getSerialno());// 流水号
                assureAccInfo.setBill_no(bailInfoDto.getBillNo());// 借据编号
                assureAccInfo.setCur_type(bailInfoDto.getCurType()); // 保证金币种
                assureAccInfo.setSecurity_money_amt(bailInfoDto.getSecurityMoneyAmt()); // 保证金金额
                assureAccInfo.setCont_state("0");// 状态
                assureAccInfos.add(assureAccInfo);
            }
            irs98ReqDto.setAccLoanInfo(accLoanInfoList);
            irs98ReqDto.setAssureAccInfo(assureAccInfos);
            log.info("开始组装Irs98客户信息");
            //客户信息
            List<CustomerInfo> customerInfos = new ArrayList<>();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtApp.getCusId());
            ResultDto<Map<String,String>> levelResult = iCusClientService.selectGradeInfoByCusId(lmtApp.getCusId());
            Map<String, String> data = new HashMap<>();
            if(levelResult != null){
                data = levelResult.getData();
            }
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId()).getData();
            if (cusCorpDto != null) {
                CustomerInfo customerInfo = new CustomerInfo();
                customerInfo.setCus_id(lmtApp.getCusId());//客户号
                customerInfo.setCus_name(lmtApp.getCusName());//客户名称  cuscorp这表数据库没有cusName字段
                customerInfo.setCus_type(cusCorpDto.getCusType());//客户类型
                customerInfo.setCert_code(cusBaseClientDto.getCertCode());//证件号码
                String cusType = cusBaseClientDto.getCertType();
                switch (cusBaseClientDto.getCertType()) {
                    case "M":
                        cusType = "24";
                        break;
                    case "N":
                        cusType = "26";
                        break;
                    case "P":
                        cusType = "27";
                        break;
                    case "Q":
                        cusType = "20";
                        break;
                    case "R":
                        cusType = "25";
                        break;
                    case "U":
                        cusType = "23";
                        break;
                    case "V":
                        cusType = "22";
                        break;
                }
                customerInfo.setCert_type(cusType);//证件类型
                String comQysyz = cusCorpDto.getCorpOwnersType();
                if("01".equals(comQysyz)){
                    customerInfo.setBus_owner("010");//企业所有制
                }else if("02".equals(comQysyz)){
                    customerInfo.setBus_owner("020");
                }else if("03".equals(comQysyz)){
                    customerInfo.setBus_owner("030");
                }else if("04".equals(comQysyz)){
                    customerInfo.setBus_owner("040");
                }else if("05".equals(comQysyz)){
                    customerInfo.setBus_owner("050");
                }
                customerInfo.setNew_industry_type(cusCorpDto.getTradeClass());//所属国标行业
                if("0".equals(cusCorpDto.getIsBankBasicDepAccNo())){
                    customerInfo.setBas_acc_flg("2");
                }else{
                    customerInfo.setBas_acc_flg(cusCorpDto.getIsBankBasicDepAccNo());//基本户是否在本行
                }
                ResultDto<Map<String, FinanIndicAnalyDto>> resultDto = iCusClientService.getFinRepRetProAndRatOfLia(cusId);
                log.info("当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(resultDto)+"】}");
                if(Objects.nonNull(resultDto) && Objects.nonNull(resultDto.getData())) {
                    Map<String, FinanIndicAnalyDto> resultMap = resultDto.getData();
                    BigDecimal assets = NumberUtils.nullDefaultZero(resultMap.get("lastYearSaleProfitRate").getCurYmValue()).divide(new BigDecimal(100));
                    log.info("当前客户资产负债率"+ JSON.toJSONString(assets)+"】}");
                    if(assets.compareTo(new BigDecimal(0)) >= 0 && assets.compareTo(new BigDecimal("0.5")) < 0){
                        customerInfo.setAssets("010"); //资产负债率
                    }else if (assets.compareTo(new BigDecimal("0.5")) >= 0 && assets.compareTo(new BigDecimal("0.7")) < 0){
                        customerInfo.setAssets("020");
                    }else if (assets.compareTo(new BigDecimal("0.7")) >= 0 && assets.compareTo(new BigDecimal("0.9")) < 0){
                        customerInfo.setAssets("030");
                    }else if (assets.compareTo(new BigDecimal("0.7")) >= 0){
                        customerInfo.setAssets("040");
                    }
                } else{
                    customerInfo.setAssets("");
                }
                String rank = "";
                if(data != null){
                    if(StringUtils.nonEmpty(data.get("finalRank"))){
                        rank = data.get("finalRank");
                    }
                }else {
                    if (StringUtils.nonEmpty(cusCorpDto.getBankLoanLevel())) {
                        rank = cusCorpDto.getBankLoanLevel();
                    }
                }
                if(StringUtils.nonEmpty(rank)){
                    customerInfo.setGrade(rank);//本行即期信用等级
                }else{
                    throw BizException.error(null, EcbEnum.ECB020071.key, EcbEnum.ECB020071.value);
                }
                customerInfo.setReg_area_code(cusCorpDto.getRegiAreaCode());//注册地行政区划代码
                customerInfo.setReg_area_name(cusCorpDto.getRegiAddr());//注册地行政区划代码
                customerInfo.setCust_mgr(lmtApp.getManagerId());//主管客户经理
                customerInfo.setMain_br_id(lmtApp.getManagerBrId());//主管机构
                customerInfos.add(customerInfo);
            }
            irs98ReqDto.setCustomerInfo(customerInfos);
            log.info("开始组装Irs98授信分项额度与抵质押、保证人关系信息");
            // 授信分项额度与抵质押、保证人关系信息
            List<LimitPleMortInfo> limitPleMortInfos = new ArrayList<>();
            for (LmtAppSub lmtAppSub : lmtAppSubs) {
                List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
                for (GuarBizRel guarBizRel : guarBizRelList) {
                    LimitPleMortInfo limitPleMortInfo = new LimitPleMortInfo();
                    limitPleMortInfo.setDetail_serno(guarBizRel.getSerno());
                    limitPleMortInfo.setItem_id(lmtAppSub.getOrigiLmtAccSubNo());
                    limitPleMortInfo.setGuaranty_id(guarBizRel.getGuarNo());
                    limitPleMortInfos.add(limitPleMortInfo);
                }
            }
            irs98ReqDto.setLimitPleMortInfo(limitPleMortInfos);
            log.info("开始组装Irs98最高额授信协议信息");
            // 最高额授信协议 CTR_HIGH_AMT_AGR_CONT
            List<CtrHighAmtAgrCont> ctrHighAmtAgrContList = ctrHighAmtAgrContService.selectDataByCusId(cusId);
            List<UpApplyInfo> upApplyInfos = new ArrayList<>();
            for (CtrHighAmtAgrCont ctrHighAmtAgrCont : ctrHighAmtAgrContList) {
                CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                reqDto.setDealBizNo(ctrHighAmtAgrCont.getContNo());
                ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                    log.info("额度编号查询成功");
                    itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                } else {
                    throw new Exception("查询额度接口异常");
                }
                UpApplyInfo upApplyInfo = new UpApplyInfo();
                upApplyInfo.setLmt_serno(ctrHighAmtAgrCont.getSerno());
                upApplyInfo.setItem_id(itemId);
                upApplyInfo.setCus_id(ctrHighAmtAgrCont.getCusId());
                upApplyInfo.setCus_name(ctrHighAmtAgrCont.getCusName());
                upApplyInfo.setCur_type(ctrHighAmtAgrCont.getAgrType());
                upApplyInfo.setAmt(ctrHighAmtAgrCont.getAgrAmt());
                if("600".equals(ctrHighAmtAgrCont.getContStatus())){
                    upApplyInfo.setStatus("300");
                }else{
                    upApplyInfo.setStatus(ctrHighAmtAgrCont.getContStatus());
                }
                upApplyInfo.setStart_date(ctrHighAmtAgrCont.getStartDate());
                upApplyInfo.setEnd_date(ctrHighAmtAgrCont.getEndDate());
                upApplyInfos.add(upApplyInfo);
            }
            irs98ReqDto.setUpApplyInfo(upApplyInfos);

            //交易对手信息

            //汇率信息
            List<CurInfo> curInfoList = new ArrayList<>();
            QueryModel model = new QueryModel();
            List<CfgTfRateDto> result = new ArrayList<>();
            ResultDto<List<CfgTfRateDto>> resultDto = iCmisCfgClientService.queryAllCfgTfRate(model);
            result = resultDto.getData();
            List<CfgTfRateDto> cfgTfRateDtos = JSONArray.parseArray(JSON.toJSONString(result), CfgTfRateDto.class);
            if(cfgTfRateDtos != null && cfgTfRateDtos.size()>0){
                for(CfgTfRateDto cfgTfRateDto : cfgTfRateDtos){
                    CurInfo curInfo = new CurInfo();
                    curInfo.setCurtype(cfgTfRateDto.getCurType());
                    BigDecimal EXUNIT = new BigDecimal(cfgTfRateDto.getExunit());
                    BigDecimal CSBYPR = cfgTfRateDto.getCsbypr();
                    curInfo.setCurvalue(CSBYPR.divide(EXUNIT));
                    curInfoList.add(curInfo);
                }
            }else{
                CurInfo curInfo = new CurInfo();
                curInfo.setCurtype("CNY");
                curInfo.setCurvalue(new BigDecimal(1));
                curInfoList.add(curInfo);
            }

            irs98ReqDto.setCurInfo(curInfoList);
            log.info("组装Irs98报文信息结束");
            log.info("Irs98报文为"+ JSON.toJSONString(irs98ReqDto)+"】}");
            ResultDto<Irs98RespDto> irs98RespDtoResultDto = dscms2IrsClientService.irs98(irs98ReqDto);
            log.info("单一客户债项评级变更测算返回:{}" + irs98RespDtoResultDto.toString());
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(irs98RespDtoResultDto.getCode())) {
                Irs98RespDto irs98RespDto = irs98RespDtoResultDto.getData();
                if (irs98RespDto != null) {
                    if(irs98RespDto.getAppMessageInfo() != null && irs98RespDto.getAppMessageInfo().size() > 0){
                        AppMessageInfo appMessageInfo = irs98RespDto.getAppMessageInfo().get(0);
                        lmtLadEval.setDebtLevel(appMessageInfo.getGuaranteeGrade());
                        lmtLadEval.setEad(appMessageInfo.getEad());
                        lmtLadEval.setLgd(appMessageInfo.getLgd());

                        List<AllMessageInfo> allMessageInfoList = irs98RespDto.getAllMessageInfo();
                        for (AllMessageInfo allMessageInfo : allMessageInfoList) {
                            LmtLadEval lmtLadEvalSub = BeanUtils.beanCopy(lmtLadEval, LmtLadEval.class);
                            lmtLadEvalSub.setPkId(UUID.randomUUID().toString());
                            lmtLadEvalSub.setSerno(allMessageInfo.getDetail_serno());
                            lmtLadEvalSub.setDebtLevel(allMessageInfo.getGuaranteeGrade());
                            lmtLadEvalSub.setEad(allMessageInfo.getEad());
                            lmtLadEvalSub.setLgd(allMessageInfo.getLgd());
                            this.insertSelective(lmtLadEvalSub);
                        }
                        List<MessageInfo> messageInfoList = irs98RespDto.getMessageInfo();
                        for (MessageInfo messageInfo : messageInfoList) {
                            LmtLadEval lmtLadEvalSubPrd = BeanUtils.beanCopy(lmtLadEval, LmtLadEval.class);
                            lmtLadEvalSubPrd.setPkId(UUID.randomUUID().toString());
                            lmtLadEvalSubPrd.setSerno(messageInfo.getDetail_serno());
                            lmtLadEvalSubPrd.setDebtLevel(messageInfo.getGuaranteeGrade());
                            lmtLadEvalSubPrd.setEad(messageInfo.getEad());
                            lmtLadEvalSubPrd.setLgd(messageInfo.getLgd());
                            lmtLadEvalSubPrd.setBizType(messageInfo.getPro_no());
                            this.insertSelective(lmtLadEvalSubPrd);
                        }
                    }else{
                        throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                    }
                }else{
                    throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                }
            }else{
                throw BizException.error(null, irs98RespDtoResultDto.getCode(), irs98RespDtoResultDto.getMessage());
            }
            } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("lmtLadEval",lmtLadEval);
        }
            return rtnData;
    }

        /**
         * @方法名称：loanRating99
         * @方法描述：
         * @创建人：quwen
         * @创建时间：2021/6/21 14:35
         * @修改记录：修改时间 修改人员 修改时间
         */
        public Map loanRating99(LmtLadEval lmtLadEval) throws Exception {
            Map rtnData = new HashMap();
            String rtnCode = EcbEnum.ECB010000.key;
            String rtnMsg = EcbEnum.ECB010000.value;
            String appType = lmtLadEval.getBizType();
            IqpLoanApp iqpLoanApp = null;
            IqpDiscApp iqpDiscApp= null;
            IqpTfLocApp iqpTfLocApp= null;
            IqpCvrgApp iqpCvrgApp= null;
            IqpAccpApp iqpAccpApp = null;
            String cusId = lmtLadEval.getCusId();
            if ("1".equals(appType) || "3".equals(appType)){
                iqpLoanApp = iqpLoanAppService.selectByIqpSerno(lmtLadEval.getSerno());
            }else if("2".equals(appType)){
                iqpDiscApp = iqpDiscAppService.selectBySerno(lmtLadEval.getSerno());
            }else if("4".equals(appType)){
                iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(lmtLadEval.getSerno());
            }else if("5".equals(appType)){
                iqpCvrgApp = iqpCvrgAppService.selectBySerno(lmtLadEval.getSerno());
            }else if("6".equals(appType)){
                iqpAccpApp = iqpAccpAppService.selectByIqpSerno(lmtLadEval.getSerno());
            }
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(lmtLadEval.getCusId());
            List<LmtReplyAccSub> accSubList = lmtReplyAccSubService.selectByAccNo(lmtReplyAcc.getAccNo());
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }

            try{
                log.info("开始组装Irs99报文信息");
                // 组装报文头信息 LimitApplyInfo
                Set<String> guarContSet = new HashSet<>();
                Set<String> guarDYSet = new HashSet<>();
                Set<String> guarZYSet = new HashSet<>();
                Set<String> guarBZSet = new HashSet<>();
                Set<String> grtContSet = new HashSet<>();
                Set<String> guarBaseSet = new HashSet<>();
                Irs99ReqDto irs99ReqDto = new Irs99ReqDto();
                irs99ReqDto.setUserid(userInfo.getUserId());//请求发起人编号
                irs99ReqDto.setUsername(userInfo.getUserName());//	请求发起人名称
                irs99ReqDto.setBrchno(userInfo.getOrg().getId());//请求机构编号
                irs99ReqDto.setBrchnm(userInfo.getOrg().getName());//请求机构名
                String accSubPrdNo = "";
                String contNo = "";
                //合同申请信息
                List<BusinessInfo> businessInfos = new ArrayList<>();
                BusinessInfo businessInfo = new BusinessInfo();
                if(iqpLoanApp != null){
                    businessInfo.setSerno(iqpLoanApp.getIqpSerno());
                    businessInfo.setItem_id(iqpLoanApp.getLmtAccNo());//是否是最外层台账编号
                    businessInfo.setLmt_serno(iqpLoanApp.getReplyNo());//是否是最外层批复编号
                    businessInfo.setCus_id(iqpLoanApp.getCusId());
                    businessInfo.setCus_name(iqpLoanApp.getCusName());
                    businessInfo.setLoan_form(iqpLoanApp.getLoanModal());
                    businessInfo.setPro_no(iqpLoanApp.getPrdId());
                    businessInfo.setPrd_name(iqpLoanApp.getPrdName());
                    businessInfo.setLoan_direction(iqpLoanApp.getLoanTer());
                    businessInfo.setCont_type(iqpLoanApp.getContType());
                    businessInfo.setAssure_means_main(iqpLoanApp.getGuarWay());
                    businessInfo.setCur_type(iqpLoanApp.getCurType());
                    businessInfo.setAcc_amount(iqpLoanApp.getContHighAvlAmt());
                    businessInfo.setStart_date(iqpLoanApp.getStartDate());
                    businessInfo.setExpi_date(iqpLoanApp.getEndDate());
                    businessInfo.setCont_state(iqpLoanApp.getApproveStatus());
                    businessInfo.setInput_id(iqpLoanApp.getInputId());
                    businessInfo.setManager_id(iqpLoanApp.getManagerId());
                    businessInfo.setManager_br_id(iqpLoanApp.getManagerBrId());
                    businessInfo.setFina_br_id("");
                    businessInfo.setGuarantee_no("");
                    businessInfo.setGuarantee_name("");
                    businessInfo.setLoancard_due(iqpLoanApp.getAppTerm());
                    businessInfo.setForward_days(new BigDecimal(0));
                    businessInfo.setPro_details("");
                    businessInfo.setIsin_revocation("");
                    if(iqpLoanApp.getContTerm() != null){
                        businessInfo.setLoan_due(new BigDecimal(iqpLoanApp.getContTerm()));
                    }
                    contNo = iqpLoanApp.getContNo();
                }else if (iqpDiscApp != null){
                    businessInfo.setSerno(iqpDiscApp.getSerno());
                    businessInfo.setItem_id(iqpDiscApp.getLmtAccNo());
                    businessInfo.setLmt_serno(iqpDiscApp.getReplyNo());
                    businessInfo.setCus_id(iqpDiscApp.getCusId());
                    businessInfo.setCus_name(iqpDiscApp.getCusName());
                    businessInfo.setLoan_form("");
                    businessInfo.setPro_no(iqpDiscApp.getPrdId());
                    businessInfo.setPrd_name(iqpDiscApp.getPrdName());
                    businessInfo.setLoan_direction("");
                    businessInfo.setCont_type(iqpDiscApp.getContType());
                    businessInfo.setAssure_means_main(iqpDiscApp.getGuarMode());
                    businessInfo.setCur_type(iqpDiscApp.getCurType());
                    businessInfo.setAcc_amount(iqpDiscApp.getDiscContType().equals("01")?iqpDiscApp.getDrftTotalAmt():iqpDiscApp.getContAmt());
                    businessInfo.setStart_date(iqpDiscApp.getStartDate());
                    businessInfo.setExpi_date(iqpDiscApp.getEndDate());
                    businessInfo.setCont_state(iqpDiscApp.getApproveStatus());
                    businessInfo.setInput_id(iqpDiscApp.getInputId());
                    businessInfo.setManager_id(iqpDiscApp.getManagerId());
                    businessInfo.setManager_br_id(iqpDiscApp.getManagerBrId());
                    businessInfo.setFina_br_id("");
                    businessInfo.setGuarantee_no("");
                    businessInfo.setGuarantee_name("");
                    businessInfo.setForward_days(new BigDecimal(0));
                    businessInfo.setPro_details("");
                    businessInfo.setIsin_revocation("");
                    if(iqpDiscApp.getContTerm() != null){
                        businessInfo.setLoan_due(new BigDecimal(iqpDiscApp.getContTerm()));
                    }
                    contNo = iqpDiscApp.getContNo();
                }else if (iqpTfLocApp != null){
                    businessInfo.setSerno(iqpTfLocApp.getSerno());
                    businessInfo.setItem_id(iqpTfLocApp.getLmtAccNo());
                    businessInfo.setLmt_serno(iqpTfLocApp.getReplyNo());
                    businessInfo.setCus_id(iqpTfLocApp.getCusId());
                    businessInfo.setCus_name(iqpTfLocApp.getCusName());
                    businessInfo.setLoan_form("");
                    businessInfo.setPro_no(iqpTfLocApp.getPrdId());
                    businessInfo.setPrd_name(iqpTfLocApp.getPrdName());
                    businessInfo.setLoan_direction("");
                    businessInfo.setCont_type(iqpTfLocApp.getContType());
                    businessInfo.setAssure_means_main(iqpTfLocApp.getGuarMode());
                    businessInfo.setCur_type(iqpTfLocApp.getCurType());
                    businessInfo.setAcc_amount(iqpTfLocApp.getContHighAvlAmt());
                    businessInfo.setStart_date(iqpTfLocApp.getStartDate());
                    businessInfo.setExpi_date(iqpTfLocApp.getEndDate());
                    businessInfo.setCont_state(iqpTfLocApp.getApproveStatus());
                    businessInfo.setInput_id(iqpTfLocApp.getInputId());
                    businessInfo.setManager_id(iqpTfLocApp.getManagerId());
                    businessInfo.setManager_br_id(iqpTfLocApp.getManagerBrId());
                    businessInfo.setFina_br_id("");
                    businessInfo.setGuarantee_no("");
                    businessInfo.setGuarantee_name("");
                    businessInfo.setForward_days(new BigDecimal(0));
                    businessInfo.setPro_details("");
                    businessInfo.setIsin_revocation("");
                    if(iqpTfLocApp.getContTerm() != null){
                        businessInfo.setLoan_due(new BigDecimal(iqpTfLocApp.getContTerm()));
                    }
                    contNo = iqpTfLocApp.getContNo();
                }else if (iqpCvrgApp != null){
                    businessInfo.setSerno(iqpCvrgApp.getSerno());
                    businessInfo.setItem_id(iqpCvrgApp.getLmtAccNo());
                    businessInfo.setLmt_serno(iqpCvrgApp.getReplyNo());
                    businessInfo.setCus_id(iqpCvrgApp.getCusId());
                    businessInfo.setCus_name(iqpCvrgApp.getCusName());
                    businessInfo.setLoan_form("");
                    businessInfo.setPro_no(iqpCvrgApp.getPrdId());
                    businessInfo.setPrd_name(iqpCvrgApp.getPrdName());
                    businessInfo.setLoan_direction("");
                    businessInfo.setCont_type(iqpCvrgApp.getContType());
                    businessInfo.setAssure_means_main(iqpCvrgApp.getGuarMode());
                    businessInfo.setCur_type(iqpCvrgApp.getCurType());
                    businessInfo.setAcc_amount(iqpCvrgApp.getContHighAvlAmt());
                    businessInfo.setStart_date(iqpCvrgApp.getStartDate());
                    businessInfo.setExpi_date(iqpCvrgApp.getEndDate());
                    businessInfo.setCont_state(iqpCvrgApp.getApproveStatus());
                    businessInfo.setInput_id(iqpCvrgApp.getInputId());
                    businessInfo.setManager_id(iqpCvrgApp.getManagerId());
                    businessInfo.setManager_br_id(iqpCvrgApp.getManagerBrId());
                    businessInfo.setFina_br_id("");
                    businessInfo.setGuarantee_no(iqpCvrgApp.getGuarantType());
                    businessInfo.setGuarantee_name("");
                    businessInfo.setForward_days(new BigDecimal(0));
                    businessInfo.setPro_details("");
                    businessInfo.setIsin_revocation("");
                    if(iqpCvrgApp.getContTerm() != null){
                        businessInfo.setLoan_due(new BigDecimal(iqpCvrgApp.getContTerm()));
                    }
                    contNo = iqpCvrgApp.getContNo();
                }else if (iqpAccpApp != null){
                    businessInfo.setSerno(iqpAccpApp.getSerno());
                    businessInfo.setItem_id(iqpAccpApp.getLmtAccNo());
                    businessInfo.setLmt_serno(iqpAccpApp.getReplyNo());
                    businessInfo.setCus_id(iqpAccpApp.getCusId());
                    businessInfo.setCus_name(iqpAccpApp.getCusName());
                    businessInfo.setLoan_form("");
                    businessInfo.setPro_no(iqpAccpApp.getPrdId());
                    businessInfo.setPrd_name(iqpAccpApp.getPrdName());
                    businessInfo.setLoan_direction("");
                    businessInfo.setCont_type(iqpAccpApp.getContType());
                    businessInfo.setAssure_means_main(iqpAccpApp.getGuarMode());
                    businessInfo.setCur_type(iqpAccpApp.getCurType());
                    businessInfo.setAcc_amount(iqpAccpApp.getContHighAvlAmt());
                    businessInfo.setStart_date(iqpAccpApp.getStartDate());
                    businessInfo.setExpi_date(iqpAccpApp.getEndDate());
                    businessInfo.setCont_state(iqpAccpApp.getApproveStatus());
                    businessInfo.setInput_id(iqpAccpApp.getInputId());
                    businessInfo.setManager_id(iqpAccpApp.getManagerId());
                    businessInfo.setManager_br_id(iqpAccpApp.getManagerBrId());
                    businessInfo.setFina_br_id("");
                    businessInfo.setGuarantee_no("");
                    businessInfo.setGuarantee_name("");
                    businessInfo.setForward_days(new BigDecimal(0));
                    businessInfo.setPro_details("");
                    businessInfo.setIsin_revocation("");
                    if(iqpAccpApp.getContTerm() != null){
                        businessInfo.setLoan_due(new BigDecimal(iqpAccpApp.getContTerm()));
                    }
                    contNo = iqpAccpApp.getContNo();
                }
                if(StringUtils.nonEmpty(businessInfo.getItem_id())){
                    Map<String,String> subMap = new HashMap<>();
                    if(StringUtils.nonEmpty(businessInfo.getItem_id())){
                        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(businessInfo.getItem_id());
                        if(lmtReplyAccSubPrd != null){
                            subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            businessInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                            businessInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                        }
                    }
                }
                businessInfos.add(businessInfo);
                irs99ReqDto.setBusinessInfo(businessInfos);
                // 综合授信申请信息 LimitApplyInfo
                List<LimitApplyInfo> limitApplyInfos = new ArrayList<>();
                LimitApplyInfo limitApplyInfo = new LimitApplyInfo();
                limitApplyInfo.setSerno(lmtReplyAcc.getSerno());// 授信申请流水号
                limitApplyInfo.setLmt_serno(lmtReplyAcc.getAccNo());
                //limitApplyInfo.setFlag("1");// 新增变更标志 1:新增
                limitApplyInfo.setCus_id(lmtReplyAcc.getCusId());// 客户编号
                limitApplyInfo.setCus_name(lmtReplyAcc.getCusName());// 客户名称
                limitApplyInfo.setCus_type(lmtReplyAcc.getCusType());// 客户类型
                limitApplyInfo.setCur_type(lmtReplyAcc.getCurType());// 币种
                BigDecimal revoAmt = new BigDecimal("0.0");//循环额度总额
                BigDecimal notRevoAmt = new BigDecimal("0.0");//非循环额度
                for(LmtReplyAccSub lmtReplyAccSub : accSubList){
                    if(!CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSub.getGuarMode())) {
                        if (CmisCommonConstants.YES_NO_1.equals(lmtReplyAccSub.getIsRevolvLimit())) {
                            revoAmt = revoAmt.add(lmtReplyAccSub.getLmtAmt());
                        } else {
                            notRevoAmt = notRevoAmt.add(lmtReplyAccSub.getLmtAmt());
                        }
                    }
                }
                limitApplyInfo.setApp_crd_totl_amt(revoAmt);
                limitApplyInfo.setApp_temp_crd_totl_amt(notRevoAmt);
                limitApplyInfo.setCrd_totl_sum_amt(new BigDecimal("0.0").add(lmtReplyAcc.getOpenTotalLmtAmt()).add(lmtReplyAcc.getLowRiskTotalLmtAmt())); // 授信总额（元）
                limitApplyInfo.setApp_crd_guar_amt(lmtReplyAcc.getLowRiskTotalLmtAmt());//低风险额度（元）
                limitApplyInfo.setStart_date(lmtReplyAcc.getInureDate());
                limitApplyInfo.setEnd_date(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReplyAcc.getInureDate(), DateFormatEnum.DEFAULT.getValue()),lmtReplyAcc.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                limitApplyInfo.setDelay_months(lmtReplyAcc.getLmtGraperTerm());
                limitApplyInfo.setInput_id(lmtReplyAcc.getInputId());
                limitApplyInfo.setInput_date(lmtReplyAcc.getInputDate());
                limitApplyInfo.setInput_br_id(lmtReplyAcc.getInputBrId());
                limitApplyInfo.setManager_id(lmtReplyAcc.getManagerId());
                limitApplyInfo.setManager_br_id(lmtReplyAcc.getManagerBrId());
                limitApplyInfos.add(limitApplyInfo);
                irs99ReqDto.setLimitApplyInfo(limitApplyInfos);


                // 分项额度信息
                List<LimitDetailsInfo> limitDetailsInfos = new ArrayList<>();
                // 产品额度信息
                List<LimitProductInfo> limitProductInfos = new ArrayList<>();
                for (LmtReplyAccSub lmtReplyAccSub : accSubList) {
                    if(CmisCommonConstants.YES_NO_1.equals(lmtReplyAccSub.getIsPreLmt())||lmtReplyAccSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                        continue;
                    }
                    // 分项额度信息
                    LimitDetailsInfo limitDetailsInfo = new LimitDetailsInfo();
                    limitDetailsInfo.setDetail_serno(lmtReplyAccSub.getSubSerno());
                    limitDetailsInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                    limitDetailsInfo.setSerno("");
                    limitDetailsInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                    limitDetailsInfo.setCus_id(lmtReplyAccSub.getCusId());
                    limitDetailsInfo.setCus_name(lmtReplyAccSub.getCusName());
                    limitDetailsInfo.setAssure_means_main(lmtReplyAccSub.getGuarMode());
                    String isRevolvLimit = "";
                    if("1".equals(lmtReplyAccSub.getIsRevolvLimit())){
                        isRevolvLimit = "10";
                    }else if ("0".equals(lmtReplyAccSub.getIsRevolvLimit())){
                        isRevolvLimit = "20";
                    }
                    limitDetailsInfo.setCrd_lmt_type(isRevolvLimit);
                    limitDetailsInfo.setCur_type(lmtReplyAccSub.getCurType());
                    limitDetailsInfo.setCrd_lmt(lmtReplyAccSub.getLmtAmt());
                    limitDetailsInfo.setStart_date(lmtReplyAcc.getInureDate());
                    Map<String,String> mapQuery = new HashMap<>();
                    mapQuery.put("accSubNo",lmtReplyAccSub.getAccSubNo());
                    mapQuery.put("isRevolvLimit",lmtReplyAccSub.getIsRevolvLimit());
                    String lastDate = lmtReplyAccSubPrdService.getLastEndDate(mapQuery);
                    limitDetailsInfo.setExpi_date(lastDate);

                    // TIPS：新信贷的宽限期在授信及产品层，分项层空
                    limitDetailsInfo.setDelay_months(lmtReplyAcc.getLmtGraperTerm());
                    limitDetailsInfos.add(limitDetailsInfo);
                    List<LmtReplyAccSubPrd> accSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(lmtReplyAccSub.getAccSubNo());
                    for (LmtReplyAccSubPrd lmtReplyAccSubPrd : accSubPrdList) {
                        if(lmtReplyAccSubPrd.getLmtBizType().startsWith("140203")||lmtReplyAccSubPrd.getLmtBizType().startsWith("12010103")){
                            continue;
                        }
                        // 产品额度信息
                        LimitProductInfo limitProductInfo = new LimitProductInfo();
                        limitProductInfo.setDetail_serno(lmtReplyAccSubPrd.getSubSerno());
                        limitProductInfo.setItem_id(lmtReplyAccSubPrd.getAccSubNo());
                        limitProductInfo.setPro_no(lmtReplyAccSubPrd.getLmtBizType());
                        limitProductInfo.setPro_name(lmtReplyAccSubPrd.getLmtBizTypeName());
                        limitProductInfo.setCrd_lmt(lmtReplyAccSubPrd.getLmtAmt());
                        String lmtAccNo = lmtReplyAccSubPrd.getAccSubPrdNo();
                        BigDecimal ctrAccpAmt = ctrAccpContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrLoanAmt = ctrLoanContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrDiscAmt = ctrDiscContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrCvrgAmt = ctrCvrgContService.getSumContAmt(lmtAccNo);
                        BigDecimal ctrTfLocAmt = ctrTfLocContService.getSumContAmt(lmtAccNo);
                        limitProductInfo.setSum_balance(ctrAccpAmt.add(ctrLoanAmt).add(ctrDiscAmt).add(ctrCvrgAmt).add(ctrTfLocAmt));
                        limitProductInfo.setMargin_ratio(lmtReplyAccSubPrd.getBailPreRate());
                        limitProductInfos.add(limitProductInfo);
                    }
                }
                if(limitDetailsInfos.size() > 0 && limitProductInfos.size() >0){
                    irs99ReqDto.setLimitDetailsInfo(limitDetailsInfos);
                    irs99ReqDto.setLimitProductInfo(limitProductInfos);
                }else{
                    rtnCode = EcbEnum.ECB020057.key;
                    rtnMsg = EcbEnum.ECB020057.value;
                    return rtnData;
                }

                log.info("开始组装Irs99质押物信息");
                //质押物信息
                List<PledgeInfo> pledgeInfoList = new ArrayList<>();
                for (LmtReplyAccSub lmtReplyAccSub : accSubList) {
                    if(CmisCommonConstants.GUAR_MODE_20.equals(lmtReplyAccSub.getGuarMode())){
                        List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtReplyAccSub.getSubSerno());
                        HashMap<String,String> hashMap = new HashMap<>();
                        hashMap.put("grtFlag","02");
                        for (GuarBizRel guarBizRel : guarBizRelList) {
                            hashMap.put("guarNo",guarBizRel.getGuarNo());
                            GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                            if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                                if(guarZYSet.contains(guarBizRel.getGuarNo())){
                                    continue;
                                }
                                guarZYSet.add(guarBizRel.getGuarNo());
                                PledgeInfo pledgeInfo = new PledgeInfo();
                                pledgeInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                                pledgeInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                                pledgeInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                                pledgeInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                                pledgeInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                                pledgeInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                                pledgeInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                                String guarStatus = "10001";
                                QueryModel queryModel = new QueryModel();
                                queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                                queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                    for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                        if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                            GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                            if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                guarStatus = "10006";
                                                break;
                                            }
                                        }
                                    }
                                }
                                pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                                pledgeInfo.setBond_type("");//债券类型
                                pledgeInfo.setOutrate_org(guarBaseInfo.getOuterLevelOrg());//外部评级机构
                                pledgeInfo.setOutrate_grade(guarBaseInfo.getOuterLevel());//外部评级等级
                                pledgeInfo.setInrate_grade("");
                                pledgeInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                                pledgeInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                                pledgeInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                pledgeInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                                pledgeInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                                pledgeInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                                pledgeInfoList.add(pledgeInfo);
                            }
                        }
                    }
                }
                irs99ReqDto.setPledgeInfo(pledgeInfoList);
                log.info("开始组装Irs99抵押物信息}");
                //抵押物信息
                List<MortgageInfo> mortgageInfoList = new ArrayList<>();
                int j = 0;
                for (LmtReplyAccSub lmtReplyAccSub : accSubList) {
                    if(CmisCommonConstants.GUAR_MODE_10.equals(lmtReplyAccSub.getGuarMode())){
                        List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtReplyAccSub.getSubSerno());
                        HashMap<String,String> hashMap = new HashMap<>();
                        hashMap.put("grtFlag","01");
                        for (GuarBizRel guarBizRel : guarBizRelList) {
                            hashMap.put("guarNo",guarBizRel.getGuarNo());
                            GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMap);
                            if (guarBaseInfo != null && StringUtils.nonEmpty(guarBaseInfo.getGuarNo())) {
                                if(guarDYSet.contains(guarBizRel.getGuarNo())){
                                    continue;
                                }
                                guarDYSet.add(guarBizRel.getGuarNo());
                                //抵押物信息
                                MortgageInfo mortgageInfo = new MortgageInfo();
                                mortgageInfo.setGuaranty_id(guarBaseInfo.getGuarNo());// 担保ID
                                mortgageInfo.setGage_type(guarBaseInfo.getGuarType());// 质押物类型
                                mortgageInfo.setGuide_type(guarBaseInfo.getGuarTypeCd());// 担保品类型细分
                                mortgageInfo.setGage_name(guarBaseInfo.getPldimnMemo());// 质押物名称
                                mortgageInfo.setCurrency(guarBaseInfo.getCurType());// 币种
                                mortgageInfo.setBook_amt(guarBaseInfo.getMaxMortagageAmt());// 权利价值（元）
                                mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//押品评估价值
                                String guarStatus = "10001";
                                QueryModel queryModel = new QueryModel();
                                queryModel.getCondition().put("guarNo",guarBaseInfo.getGuarNo());
                                queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                    for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                        if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                            GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                            if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                guarStatus = "10006";
                                                break;
                                            }
                                        }
                                    }
                                }
                                mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                                mortgageInfo.setEasy_nature(guarBaseInfo.getSupervisionConvenience());// 查封便利性
                                mortgageInfo.setLaw_validity((guarBaseInfo.getLawValidity()));// 法律有效性
                                mortgageInfo.setPle_cust_type(guarBaseInfo.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                mortgageInfo.setPle_curr(guarBaseInfo.getPldimnCommon());// 抵质押品通用性
                                mortgageInfo.setPle_cash(guarBaseInfo.getPldimnCashability());// 抵质押品变现能力
                                mortgageInfo.setValue_wave(guarBaseInfo.getPriceWave());// 价格波动性
                                mortgageInfo.setEval_amt(guarBaseInfo.getEvalAmt());//评估价值
                                if(guarBaseInfo.getGuarTypeCd().startsWith("DY06")||guarBaseInfo.getGuarTypeCd().startsWith("DY02")||guarBaseInfo.getGuarTypeCd().startsWith("DY0102")||guarBaseInfo.getGuarTypeCd().startsWith("DY0103")||guarBaseInfo.getGuarTypeCd().startsWith("DY0101")||guarBaseInfo.getGuarTypeCd().startsWith("DY0199")){
                                    j++;
                                    Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                    xddb01ReqDto.setGuaranty_id(guarBaseInfo.getGuarNo());
                                    xddb01ReqDto.setType(guarBaseInfo.getGuarTypeCd());
                                    ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                    log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                    if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                        Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                        if(!Objects.isNull(xddb01RespDto)){
                                            mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                            mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                            String landUp = xddb01RespDto.getLand_up();
                                            if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                                mortgageInfo.setAdhesion("010");
                                            }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                                mortgageInfo.setAdhesion("020");
                                            }else{
                                                mortgageInfo.setAdhesion("");
                                            }
                                            String remainYear = xddb01RespDto.getHouse_remainder_year();
                                            if("01".equals(remainYear)){
                                                mortgageInfo.setUse_year("010");
                                            }else if("02".equals(remainYear)){
                                                mortgageInfo.setUse_year("020");
                                            }else if("03".equals(remainYear)){
                                                mortgageInfo.setUse_year("030");
                                            }else if("04".equals(remainYear)){
                                                mortgageInfo.setUse_year("040");
                                            }else if("05".equals(remainYear)){
                                                mortgageInfo.setUse_year("050");
                                            }else if("06".equals(remainYear)){
                                                mortgageInfo.setUse_year("060");
                                            }else if("07".equals(remainYear)){
                                                mortgageInfo.setUse_year("070");
                                            }else{
                                                mortgageInfo.setUse_year("");
                                            }
                                            mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                            mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                            mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                        }
                                    }else{
                                        log.error("接口【xddb01】发送【押品系统】异常！");
                                    }
                                }
                                mortgageInfoList.add(mortgageInfo);
                            }
                        }
                    }
                }
                irs99ReqDto.setMortgageInfo(mortgageInfoList);
                log.info("开始组装Irs99保证人信息");
                //保证人信息
                List<AssurePersonInfo> assurePersonInfoList = new ArrayList<>();
                for (LmtReplyAccSub lmtReplyAccSub : accSubList) {
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtReplyAccSub.getSubSerno());
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarBizRel.getGuarNo());
                        if (guarGuarantee != null) {
                            if(guarBZSet.contains(guarBizRel.getGuarNo())){
                                continue;
                            }
                            guarBZSet.add(guarBizRel.getGuarNo());
                            AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                            assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                            assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                            assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                            assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                            assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                            String comQysyz = guarGuarantee.getComQysyz();
                            if("01".equals(comQysyz)){
                                assurePersonInfo.setBus_owner("010");//企业所有制
                            }else if("02".equals(comQysyz)){
                                assurePersonInfo.setBus_owner("020");
                            }else if("03".equals(comQysyz)){
                                assurePersonInfo.setBus_owner("030");
                            }else if("04".equals(comQysyz)){
                                assurePersonInfo.setBus_owner("040");
                            }else if("05".equals(comQysyz)){
                                assurePersonInfo.setBus_owner("050");
                            }
                            assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                            assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                            String isGuarCom = guarGuarantee.getIsGuarCom();
                            if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                                assurePersonInfo.setIsin_major("010");//是否专业担保公司
                            }else{
                                assurePersonInfo.setIsin_major("020");
                            }
                            assurePersonInfoList.add(assurePersonInfo);
                        }
                    }
                }
                irs99ReqDto.setAssurePersonInfo(assurePersonInfoList);
                log.info("开始组装Irs99合同信息等");
                //合同信息
                List<BusContInfoDto> busContInfoList = ctrCvrgContService.getAllBusContInfo(lmtReplyAcc.getCusId());
                List<BusinessContractInfo> businessContractInfos = new ArrayList<>();
                //担保合同与合同关联信息
                //担保合同与抵质押、保证人关联信息
                List<BusinessAssureInfo> businessAssureInfos = new ArrayList<>();
                List<GuaranteePleMortInfo> guaranteePleMortInfos = new ArrayList<>();
                //担保合同信息(GuaranteeContrctInfo)
                List<GuaranteeContrctInfo> guaranteeContrctInfoList = new ArrayList<>();

                int i = 0;
                LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
                String itemId = "";
                List<GrtGuarBizRstRel> grtGuarBizRstRelListOne = grtGuarBizRstRelService.getByContNo(contNo);
                for(GrtGuarBizRstRel grtGuarBizRstRel:grtGuarBizRstRelListOne){
                    BusinessAssureInfo businessAssureInfo = new BusinessAssureInfo();
                    businessAssureInfo.setCont_no(grtGuarBizRstRel.getContNo());
                    businessAssureInfo.setGuar_cont_no(grtGuarBizRstRel.getGuarContNo());
                    businessAssureInfo.setSerialno(businessInfo.getSerno());
                    businessAssureInfos.add(businessAssureInfo);
                    if(grtGuarBizRstRel.getGuarContNo() != null && !"".equals(grtGuarBizRstRel.getGuarContNo())){
                        if(guarContSet.contains(grtGuarBizRstRel.getGuarContNo())){
                            continue;
                        }
                        guarContSet.add(grtGuarBizRstRel.getGuarContNo());
                        List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.getByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                        for(GrtGuarContRel grtGuarContRel : grtGuarContRels){
                            GuaranteePleMortInfo guaranteePleMortInfo = new GuaranteePleMortInfo();
                            guaranteePleMortInfo.setGuar_cont_no(grtGuarContRel.getGuarContNo());
                            guaranteePleMortInfo.setGuaranty_id(grtGuarContRel.getGuarNo());
                            guaranteePleMortInfos.add(guaranteePleMortInfo);
                            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarContRel.getGuarContNo());
                            GuaranteeContrctInfo guaranteeContrctInfo = new GuaranteeContrctInfo();
                            if(grtGuarCont != null){
                                if(grtContSet.contains(grtGuarCont.getGuarContNo())){
                                    continue;
                                }
                                grtContSet.add(grtGuarCont.getGuarContNo());
                                guaranteeContrctInfo.setGuar_cont_no(grtGuarCont.getGuarContNo());// 担保合同流水号
                                guaranteeContrctInfo.setItem_id(grtGuarCont.getLmtAccNo());// 授信台账编号
                                guaranteeContrctInfo.setLmt_serno(lmtReplyAcc.getSerno());// 授信协议编号
                                String guarContType = grtGuarCont.getGuarContType();
                                if("B".equals(guarContType)){
                                    guaranteeContrctInfo.setGuar_cont_type("2");// 担保合同类型
                                }else{
                                    guaranteeContrctInfo.setGuar_cont_type("1");
                                }
                                guaranteeContrctInfo.setGuar_way(grtGuarCont.getGuarWay());// 担保方式
                                guaranteeContrctInfo.setCur_type(grtGuarCont.getCurType());// 币种
                                guaranteeContrctInfo.setGuar_amt(grtGuarCont.getGuarAmt());// 担保金额（元）
                                guaranteeContrctInfo.setGuar_start_date(grtGuarCont.getGuarStartDate());// 担保起始日
                                guaranteeContrctInfo.setGuar_end_date(grtGuarCont.getGuarEndDate());// 担保到期日
                                guaranteeContrctInfo.setGuar_cont_state(grtGuarCont.getGuarContState());// 担保合同状态
                                guaranteeContrctInfoList.add(guaranteeContrctInfo);
                                if(grtGuarContRel.getGuarNo() != null && !"".equals(grtGuarContRel.getGuarNo())){
                                    //以质押物条件查询
                                    HashMap<String,String> hashMapZY = new HashMap<>();
                                    hashMapZY.put("grtFlag","02");
                                    hashMapZY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoZY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapZY);
                                    //以抵押物条件查询
                                    HashMap<String,String> hashMapDY = new HashMap<>();
                                    hashMapDY.put("grtFlag","01");
                                    hashMapDY.put("guarNo",grtGuarContRel.getGuarNo());
                                    GuarBaseInfo guarBaseInfoDY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapDY);
                                    //以保证人条件查询
                                    GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());
                                    if(guarBaseInfoZY != null && StringUtils.nonEmpty(guarBaseInfoZY.getGuarNo())){ //若为质押物
                                        if(guarZYSet.contains(guarBaseInfoZY.getGuarNo())){
                                            continue;
                                        }
                                        guarZYSet.add(guarBaseInfoZY.getGuarNo());
                                        PledgeInfo pledgeInfo = new PledgeInfo();
                                        pledgeInfo.setGuaranty_id(guarBaseInfoZY.getGuarNo());// 担保ID
                                        pledgeInfo.setGage_type(guarBaseInfoZY.getGuarType());// 质押物类型
                                        pledgeInfo.setGuide_type(guarBaseInfoZY.getGuarTypeCd());// 担保品类型细分
                                        pledgeInfo.setGage_name(guarBaseInfoZY.getPldimnMemo());// 质押物名称
                                        pledgeInfo.setCurrency(guarBaseInfoZY.getCurType());// 币种
                                        pledgeInfo.setBook_amt(guarBaseInfoZY.getMaxMortagageAmt());// 权利价值（元）
                                        pledgeInfo.setEval_amt(guarBaseInfoZY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoZY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                                        pledgeInfo.setBond_type("");//债券类型
                                        pledgeInfo.setOutrate_org(guarBaseInfoZY.getOuterLevelOrg());//外部评级机构
                                        pledgeInfo.setOutrate_grade(guarBaseInfoZY.getOuterLevel());//外部评级等级
                                        pledgeInfo.setInrate_grade("");
                                        pledgeInfo.setEasy_nature(guarBaseInfoZY.getSupervisionConvenience());// 查封便利性
                                        pledgeInfo.setLaw_validity((guarBaseInfoZY.getLawValidity()));// 法律有效性
                                        pledgeInfo.setPle_cust_type(guarBaseInfoZY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        pledgeInfo.setPle_curr(guarBaseInfoZY.getPldimnCommon());// 抵质押品通用性
                                        pledgeInfo.setPle_cash(guarBaseInfoZY.getPldimnCashability());// 抵质押品变现能力
                                        pledgeInfo.setValue_wave(guarBaseInfoZY.getPriceWave());// 价格波动性
                                        pledgeInfoList.add(pledgeInfo);
                                    }else if (guarBaseInfoDY != null && StringUtils.nonEmpty(guarBaseInfoDY.getGuarNo())){//若为抵押物
                                        if(guarDYSet.contains(guarBaseInfoDY.getGuarNo())){
                                            continue;
                                        }
                                        guarDYSet.add(guarBaseInfoDY.getGuarNo());
                                        MortgageInfo mortgageInfo = new MortgageInfo();
                                        mortgageInfo.setGuaranty_id(guarBaseInfoDY.getGuarNo());// 担保ID
                                        mortgageInfo.setGage_type(guarBaseInfoDY.getGuarType());// 质押物类型
                                        mortgageInfo.setGuide_type(guarBaseInfoDY.getGuarTypeCd());// 担保品类型细分
                                        mortgageInfo.setGage_name(guarBaseInfoDY.getPldimnMemo());// 质押物名称
                                        mortgageInfo.setCurrency(guarBaseInfoDY.getCurType());// 币种
                                        mortgageInfo.setBook_amt(guarBaseInfoDY.getMaxMortagageAmt());// 权利价值（元）
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//押品评估价值
                                        String guarStatus = "10001";
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.getCondition().put("guarNo",guarBaseInfoDY.getGuarNo());
                                        queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                        List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                        if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                            for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                    GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                    if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                        guarStatus = "10006";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                                        mortgageInfo.setEasy_nature(guarBaseInfoDY.getSupervisionConvenience());// 查封便利性
                                        mortgageInfo.setLaw_validity((guarBaseInfoDY.getLawValidity()));// 法律有效性
                                        mortgageInfo.setPle_cust_type(guarBaseInfoDY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                        mortgageInfo.setPle_curr(guarBaseInfoDY.getPldimnCommon());// 抵质押品通用性
                                        mortgageInfo.setPle_cash(guarBaseInfoDY.getPldimnCashability());// 抵质押品变现能力
                                        mortgageInfo.setValue_wave(guarBaseInfoDY.getPriceWave());// 价格波动性
                                        mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//评估价值
                                        if(guarBaseInfoDY.getGuarTypeCd().startsWith("DY06")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY02")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0102")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0103")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0101")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0199")){
                                            i ++;
                                            Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                            xddb01ReqDto.setGuaranty_id(guarBaseInfoDY.getGuarNo());
                                            xddb01ReqDto.setType(guarBaseInfoDY.getGuarTypeCd());
                                            ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                            log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                            if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                                Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                                if(!Objects.isNull(xddb01RespDto)){
                                                    mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                                    mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                                    String landUp = xddb01RespDto.getLand_up();
                                                    if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                                        mortgageInfo.setAdhesion("010");
                                                    }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                                        mortgageInfo.setAdhesion("020");
                                                    }else{
                                                        mortgageInfo.setAdhesion("");
                                                    }
                                                    String remainYear = xddb01RespDto.getHouse_remainder_year();
                                                    if("01".equals(remainYear)){
                                                        mortgageInfo.setUse_year("010");
                                                    }else if("02".equals(remainYear)){
                                                        mortgageInfo.setUse_year("020");
                                                    }else if("03".equals(remainYear)){
                                                        mortgageInfo.setUse_year("030");
                                                    }else if("04".equals(remainYear)){
                                                        mortgageInfo.setUse_year("040");
                                                    }else if("05".equals(remainYear)){
                                                        mortgageInfo.setUse_year("050");
                                                    }else if("06".equals(remainYear)){
                                                        mortgageInfo.setUse_year("060");
                                                    }else if("07".equals(remainYear)){
                                                        mortgageInfo.setUse_year("070");
                                                    }else{
                                                        mortgageInfo.setUse_year("");
                                                    }
                                                    mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                                    mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                                    mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                                }
                                            }else{
                                                log.error("接口【xddb01】发送【押品系统】异常！");
                                            }
                                        }
                                        mortgageInfoList.add(mortgageInfo);
                                    }else if (guarGuarantee != null){
                                        if(guarBZSet.contains(guarGuarantee.getGuarantyId())){
                                            continue;
                                        }
                                        guarBZSet.add(guarGuarantee.getGuarantyId());
                                        AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                                        assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                                        assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                                        assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                                        assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                                        assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                                        String comQysyz = guarGuarantee.getComQysyz();
                                        if("01".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("010");//企业所有制
                                        }else if("02".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("020");
                                        }else if("03".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("030");
                                        }else if("04".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("040");
                                        }else if("05".equals(comQysyz)){
                                            assurePersonInfo.setBus_owner("050");
                                        }
                                        assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                                        assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                                        String isGuarCom = guarGuarantee.getIsGuarCom();
                                        if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                                            assurePersonInfo.setIsin_major("010");//是否专业担保公司
                                        }else{
                                            assurePersonInfo.setIsin_major("020");
                                        }
                                        assurePersonInfoList.add(assurePersonInfo);
                                    }
                                }
                            }
                        }
                    }
                }
                for(BusContInfoDto busContInfoDto :busContInfoList){
                    CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                    reqDto.setDealBizNo(busContInfoDto.getContNo());
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                    if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                        log.info("额度编号查询成功");
                        itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                    } else {
                        throw new Exception("查询额度接口异常");
                    }
                    BusinessContractInfo businessContractInfo = new BusinessContractInfo();
                    Map<String,String> subMap = new HashMap<>();
                    if(StringUtils.nonEmpty(itemId)){
                        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                        if(lmtReplyAccSubPrd != null){
                            subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                            businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                        }else{
                            subMap.put("accSubNo",itemId);
                            lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                            businessContractInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                            businessContractInfo.setLmt_serno(lmtReplyAccSub.getAccNo());
                        }
                    }else{
                        businessContractInfo.setItem_id("");
                        businessContractInfo.setLmt_serno("");
                    }
                    businessContractInfo.setSerno(busContInfoDto.getSerno());
                    businessContractInfo.setCont_no(busContInfoDto.getContNo());
                    businessContractInfo.setCus_id(busContInfoDto.getCusId());
                    businessContractInfo.setCus_name(busContInfoDto.getCusName());
                    businessContractInfo.setLoan_form(busContInfoDto.getLoanModal());
                    businessContractInfo.setPro_no(busContInfoDto.getPrdId());
                    businessContractInfo.setPrd_name(busContInfoDto.getPrdName());
                    businessContractInfo.setLoan_direction(busContInfoDto.getLoanTer());
                    businessContractInfo.setCont_type(busContInfoDto.getContType());
                    businessContractInfo.setAssure_means_main(busContInfoDto.getGuarMode());
                    businessContractInfo.setCur_type(busContInfoDto.getCurType());
                    businessContractInfo.setAcc_amount(busContInfoDto.getContAmt());
                    businessContractInfo.setStart_date(busContInfoDto.getStartDate());
                    businessContractInfo.setExpi_date(busContInfoDto.getEndDate());
                    //businessContractInfo.setSum_low_balance(busContInfoDto.getContAmt);
                    businessContractInfo.setCont_state(busContInfoDto.getContStatus());
                    businessContractInfo.setInput_id(busContInfoDto.getInputId());
                    businessContractInfo.setManager_id(busContInfoDto.getManagerId());
                    businessContractInfo.setManager_br_id(busContInfoDto.getManagerBrId());
                    businessContractInfo.setInput_id(busContInfoDto.getInputId());
                    businessContractInfo.setFina_br_id(busContInfoDto.getFinaBrId());
                    businessContractInfo.setGuarantee_no(busContInfoDto.getGuaranteeType());
                    businessContractInfo.setGuarantee_name(busContInfoDto.getGuaranteeName());
                    businessContractInfo.setLoancard_due(busContInfoDto.getIocTerm());
                    businessContractInfo.setForward_days(busContInfoDto.getFastDay());
                    businessContractInfo.setPro_details(busContInfoDto.getProDetails());
                    businessContractInfo.setIsin_revocation(busContInfoDto.getIsinRevocation());
                    businessContractInfo.setLoan_due(busContInfoDto.getContTerm());
                    businessContractInfos.add(businessContractInfo);
                    List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.getByContNo(busContInfoDto.getContNo());
                    for(GrtGuarBizRstRel grtGuarBizRstRel:grtGuarBizRstRelList){
                        BusinessAssureInfo businessAssureInfo = new BusinessAssureInfo();
                        businessAssureInfo.setCont_no(grtGuarBizRstRel.getContNo());
                        businessAssureInfo.setGuar_cont_no(grtGuarBizRstRel.getGuarContNo());
                        businessAssureInfo.setSerialno(busContInfoDto.getSerno());
                        businessAssureInfos.add(businessAssureInfo);
                        if(grtGuarBizRstRel.getGuarContNo() != null && !"".equals(grtGuarBizRstRel.getGuarContNo())){
                            if(guarContSet.contains(grtGuarBizRstRel.getGuarContNo())){
                                continue;
                            }
                            guarContSet.add(grtGuarBizRstRel.getGuarContNo());
                            List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.getByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                            for(GrtGuarContRel grtGuarContRel : grtGuarContRels){
                                GuaranteePleMortInfo guaranteePleMortInfo = new GuaranteePleMortInfo();
                                guaranteePleMortInfo.setGuar_cont_no(grtGuarContRel.getGuarContNo());
                                guaranteePleMortInfo.setGuaranty_id(grtGuarContRel.getGuarNo());
                                guaranteePleMortInfos.add(guaranteePleMortInfo);
                                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarContRel.getGuarContNo());
                                GuaranteeContrctInfo guaranteeContrctInfo = new GuaranteeContrctInfo();
                                if(grtGuarCont != null){
                                    if(grtContSet.contains(grtGuarCont.getGuarContNo())){
                                        continue;
                                    }
                                    grtContSet.add(grtGuarCont.getGuarContNo());
                                    guaranteeContrctInfo.setGuar_cont_no(grtGuarCont.getGuarContNo());// 担保合同流水号
                                    guaranteeContrctInfo.setItem_id(lmtReplyAccSub.getAccSubNo());// 授信台账编号
                                    guaranteeContrctInfo.setLmt_serno(lmtReplyAcc.getSerno());// 授信协议编号
                                    String guarContType = grtGuarCont.getGuarContType();
                                    if("B".equals(guarContType)){
                                        guaranteeContrctInfo.setGuar_cont_type("2");// 担保合同类型
                                    }else{
                                        guaranteeContrctInfo.setGuar_cont_type("1");
                                    }
                                    guaranteeContrctInfo.setGuar_way(grtGuarCont.getGuarWay());// 担保方式
                                    guaranteeContrctInfo.setCur_type(grtGuarCont.getCurType());// 币种
                                    guaranteeContrctInfo.setGuar_amt(grtGuarCont.getGuarAmt());// 担保金额（元）
                                    guaranteeContrctInfo.setGuar_start_date(grtGuarCont.getGuarStartDate());// 担保起始日
                                    guaranteeContrctInfo.setGuar_end_date(grtGuarCont.getGuarEndDate());// 担保到期日
                                    guaranteeContrctInfo.setGuar_cont_state(grtGuarCont.getGuarContState());// 担保合同状态
                                    guaranteeContrctInfoList.add(guaranteeContrctInfo);
                                    if(grtGuarContRel.getGuarNo() != null && !"".equals(grtGuarContRel.getGuarNo())){
                                        //以质押物条件查询
                                        HashMap<String,String> hashMapZY = new HashMap<>();
                                        hashMapZY.put("grtFlag","02");
                                        hashMapZY.put("guarNo",grtGuarContRel.getGuarNo());
                                        GuarBaseInfo guarBaseInfoZY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapZY);
                                        //以抵押物条件查询
                                        HashMap<String,String> hashMapDY = new HashMap<>();
                                        hashMapDY.put("grtFlag","01");
                                        hashMapDY.put("guarNo",grtGuarContRel.getGuarNo());
                                        GuarBaseInfo guarBaseInfoDY = guarBaseInfoService.queryByGuarNoAndGrtFlag(hashMapDY);
                                        //以保证人条件查询
                                        GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());
                                        if(guarBaseInfoZY != null &&StringUtils.nonEmpty(guarBaseInfoZY.getGuarNo())){ //若为质押物
                                            if(guarZYSet.contains(guarBaseInfoZY.getGuarNo())){
                                                continue;
                                            }
                                            guarZYSet.add(guarBaseInfoZY.getGuarNo());
                                            PledgeInfo pledgeInfo = new PledgeInfo();
                                            pledgeInfo.setGuaranty_id(guarBaseInfoZY.getGuarNo());// 担保ID
                                            pledgeInfo.setGage_type(guarBaseInfoZY.getGuarType());// 质押物类型
                                            pledgeInfo.setGuide_type(guarBaseInfoZY.getGuarTypeCd());// 担保品类型细分
                                            pledgeInfo.setGage_name(guarBaseInfoZY.getPldimnMemo());// 质押物名称
                                            pledgeInfo.setCurrency(guarBaseInfoZY.getCurType());// 币种
                                            pledgeInfo.setBook_amt(guarBaseInfoZY.getMaxMortagageAmt());// 权利价值（元）
                                            pledgeInfo.setEval_amt(guarBaseInfoZY.getEvalAmt());//押品评估价值
                                            String guarStatus = "10001";
                                            QueryModel queryModel = new QueryModel();
                                            queryModel.getCondition().put("guarNo",guarBaseInfoZY.getGuarNo());
                                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                            guarStatus = "10006";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            pledgeInfo.setStatus(guarStatus);// 抵质押物状态
                                            pledgeInfo.setBond_type("");//债券类型
                                            pledgeInfo.setOutrate_org(guarBaseInfoZY.getOuterLevelOrg());//外部评级机构
                                            pledgeInfo.setOutrate_grade(guarBaseInfoZY.getOuterLevel());//外部评级等级
                                            pledgeInfo.setInrate_grade("");
                                            pledgeInfo.setEasy_nature(guarBaseInfoZY.getSupervisionConvenience());// 查封便利性
                                            pledgeInfo.setLaw_validity((guarBaseInfoZY.getLawValidity()));// 法律有效性
                                            pledgeInfo.setPle_cust_type(guarBaseInfoZY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                            pledgeInfo.setPle_curr(guarBaseInfoZY.getPldimnCommon());// 抵质押品通用性
                                            pledgeInfo.setPle_cash(guarBaseInfoZY.getPldimnCashability());// 抵质押品变现能力
                                            pledgeInfo.setValue_wave(guarBaseInfoZY.getPriceWave());// 价格波动性
                                            pledgeInfoList.add(pledgeInfo);
                                        }else if (guarBaseInfoDY != null && StringUtils.nonEmpty(guarBaseInfoDY.getGuarNo())){//若为抵押物
                                            if(guarDYSet.contains(guarBaseInfoDY.getGuarNo())){
                                                continue;
                                            }
                                            guarDYSet.add(guarBaseInfoDY.getGuarNo());
                                            MortgageInfo mortgageInfo = new MortgageInfo();
                                            mortgageInfo.setGuaranty_id(guarBaseInfoDY.getGuarNo());// 担保ID
                                            mortgageInfo.setGage_type(guarBaseInfoDY.getGuarType());// 质押物类型
                                            mortgageInfo.setGuide_type(guarBaseInfoDY.getGuarTypeCd());// 担保品类型细分
                                            mortgageInfo.setGage_name(guarBaseInfoDY.getPldimnMemo());// 质押物名称
                                            mortgageInfo.setCurrency(guarBaseInfoDY.getCurType());// 币种
                                            mortgageInfo.setBook_amt(guarBaseInfoDY.getMaxMortagageAmt());// 权利价值（元）
                                            mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//押品评估价值
                                            String guarStatus = "10001";
                                            QueryModel queryModel = new QueryModel();
                                            queryModel.getCondition().put("guarNo",guarBaseInfoDY.getGuarNo());
                                            queryModel.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
                                            List<GuarContRelWarrant> guarContRelWarrantList = guarContRelWarrantService.selectAll(queryModel);
                                            if(guarContRelWarrantList != null && guarContRelWarrantList.size()>0){
                                                for(GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList){
                                                    if(StringUtils.nonEmpty(guarContRelWarrant.getWarrantNo())){
                                                        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.queryByWarrantNo(guarContRelWarrant.getWarrantNo());
                                                        if(guarWarrantInfo != null && ("04".equals(guarWarrantInfo.getCertiState()) || "06".equals(guarWarrantInfo.getCertiState()) || "09".equals(guarWarrantInfo.getCertiState()))){
                                                            guarStatus = "10006";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            mortgageInfo.setStatus(guarStatus);// 抵质押物状态
                                            mortgageInfo.setEasy_nature(guarBaseInfoDY.getSupervisionConvenience());// 查封便利性
                                            mortgageInfo.setLaw_validity((guarBaseInfoDY.getLawValidity()));// 法律有效性
                                            mortgageInfo.setPle_cust_type(guarBaseInfoDY.getPldimnDebitRelative());// 抵质押物与借款人相关性
                                            mortgageInfo.setPle_curr(guarBaseInfoDY.getPldimnCommon());// 抵质押品通用性
                                            mortgageInfo.setPle_cash(guarBaseInfoDY.getPldimnCashability());// 抵质押品变现能力
                                            mortgageInfo.setValue_wave(guarBaseInfoDY.getPriceWave());// 价格波动性
                                            mortgageInfo.setEval_amt(guarBaseInfoDY.getEvalAmt());//评估价值
                                            if(guarBaseInfoDY.getGuarTypeCd().startsWith("DY06")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY02")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0102")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0103")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0101")||guarBaseInfoDY.getGuarTypeCd().startsWith("DY0199")){
                                                i ++;
                                                Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                                                xddb01ReqDto.setGuaranty_id(guarBaseInfoDY.getGuarNo());
                                                xddb01ReqDto.setType(guarBaseInfoDY.getGuarTypeCd());
                                                ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                                                log.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                                                if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                                                    Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                                                    if(!Objects.isNull(xddb01RespDto)){
                                                        mortgageInfo.setSoil_nature(xddb01RespDto.getLand_use_qual());// 土地使用权性质
                                                        mortgageInfo.setHouse_type(xddb01RespDto.getLand_use_way());// 房产类型
                                                        String landUp = xddb01RespDto.getLand_up();
                                                        if(CmisCommonConstants.YES_NO_1.equals(landUp)){
                                                            mortgageInfo.setAdhesion("010");
                                                        }else if (CmisCommonConstants.YES_NO_0.equals(landUp)){
                                                            mortgageInfo.setAdhesion("020");
                                                        }else{
                                                            mortgageInfo.setAdhesion("");
                                                        }
                                                        String remainYear = xddb01RespDto.getHouse_remainder_year();
                                                        if("01".equals(remainYear)){
                                                            mortgageInfo.setUse_year("010");//企业所有制
                                                        }else if("02".equals(remainYear)){
                                                            mortgageInfo.setUse_year("020");
                                                        }else if("03".equals(remainYear)){
                                                            mortgageInfo.setUse_year("030");
                                                        }else if("04".equals(remainYear)){
                                                            mortgageInfo.setUse_year("040");
                                                        }else if("05".equals(remainYear)){
                                                            mortgageInfo.setUse_year("050");
                                                        }else if("06".equals(remainYear)){
                                                            mortgageInfo.setUse_year("060");
                                                        }else if("07".equals(remainYear)){
                                                            mortgageInfo.setUse_year("070");
                                                        }else{
                                                            mortgageInfo.setUse_year("");
                                                        }
                                                        mortgageInfo.setTenement(xddb01RespDto.getProperty());// 物业情况
                                                        mortgageInfo.setSection(xddb01RespDto.getArea_location());// 所处地段
                                                        mortgageInfo.setLocation(xddb01RespDto.getLocated_position());// 所处地理位置
                                                    }
                                                }else{
                                                    log.error("接口【xddb01】发送【押品系统】异常！");
                                                }
                                            }
                                            mortgageInfoList.add(mortgageInfo);
                                        }else if (guarGuarantee != null){
                                            if(guarBZSet.contains(guarGuarantee.getGuarantyId())){
                                                continue;
                                            }
                                            guarBZSet.add(guarGuarantee.getGuarantyId());
                                            AssurePersonInfo assurePersonInfo = new AssurePersonInfo();
                                            assurePersonInfo.setGuaranty_id(guarGuarantee.getGuarantyId());// 保证群编号
                                            assurePersonInfo.setCus_id(guarGuarantee.getCusId());// 保证人客户号
                                            assurePersonInfo.setCus_name(guarGuarantee.getAssureName());// 保证人客户名称
                                            assurePersonInfo.setCurrency(guarGuarantee.getCurType());// 币种
                                            assurePersonInfo.setGuarantee_amt(guarGuarantee.getGuarAmt());// 担保金额
                                            String comQysyz = guarGuarantee.getComQysyz();
                                            if("01".equals(comQysyz)){
                                                assurePersonInfo.setBus_owner("010");//企业所有制
                                            }else if("02".equals(comQysyz)){
                                                assurePersonInfo.setBus_owner("020");
                                            }else if("03".equals(comQysyz)){
                                                assurePersonInfo.setBus_owner("030");
                                            }else if("04".equals(comQysyz)){
                                                assurePersonInfo.setBus_owner("040");
                                            }else if("05".equals(comQysyz)){
                                                assurePersonInfo.setBus_owner("050");
                                            }
                                            assurePersonInfo.setGuaranty_type(guarGuarantee.getGuaranteeType());//保证类型
                                            assurePersonInfo.setLaw_validity(guarGuarantee.getEnsureLegalValidity());//保证法律有效性
                                            String isGuarCom = guarGuarantee.getIsGuarCom();
                                            if(CmisCommonConstants.YES_NO_1.equals(isGuarCom)){
                                                assurePersonInfo.setIsin_major("010");//是否专业担保公司
                                            }else{
                                                assurePersonInfo.setIsin_major("020");
                                            }
                                            assurePersonInfoList.add(assurePersonInfo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                irs99ReqDto.setBusinessContractInfo(businessContractInfos);
                irs99ReqDto.setPledgeInfo(pledgeInfoList);
                irs99ReqDto.setMortgageInfo(mortgageInfoList);
                log.info("发送押品系统次数："+j+","+i);
                irs99ReqDto.setAssurePersonInfo(assurePersonInfoList);
                irs99ReqDto.setBusinessAssureInfo(businessAssureInfos);
                irs99ReqDto.setGuaranteePleMortInfo(guaranteePleMortInfos);
                irs99ReqDto.setGuaranteeContrctInfo(guaranteeContrctInfoList);

                log.info("开始组装Irs99台账相关信息等");
                //非垫款借据信息(AccLoanInfo)
                List<AccLoanInfo> accLoanInfoList = new ArrayList<>();
                log.info("组装Irs99台账相关信息查询开始");
                List<AccLoanInfoDto> accLoanInfoDtoList = accDiscService.getAllBusAccInfo(cusId);
                log.info("组装Irs99台账相关信息查询完毕");

                //保证金信息
                List<AssureAccInfo> assureAccInfos = new ArrayList<>();
                List<BailAccInfo> bailAccInfoList = new ArrayList<>();
                List<String> guarContNoList = new ArrayList<>();
                for(AccLoanInfoDto accLoanInfoDto:accLoanInfoDtoList){
                    CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                    reqDto.setDealBizNo(accLoanInfoDto.getContNo());
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                    if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                        log.info("额度编号查询成功");
                        itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                    } else {
                        throw new Exception("查询额度接口异常");
                    }
                    if(StringUtil.isNotEmpty(accLoanInfoDto.getPvpSerno())){
                        AccLoanInfo accLoanInfo = new AccLoanInfo();
                        accLoanInfo.setBill_no(accLoanInfoDto.getBillNo());
                        accLoanInfo.setCont_no(accLoanInfoDto.getContNo());
                        Map<String,String> subMap = new HashMap<>();
                        if(StringUtils.nonEmpty(itemId)){
                            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(itemId);
                            if(lmtReplyAccSubPrd != null){
                                subMap.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                                lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                                accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                            }else{
                                subMap.put("accSubNo",itemId);
                                lmtReplyAccSub = lmtReplyAccSubService.selectLmtReplyAccSubByAccSubNo(subMap);
                                accLoanInfo.setItem_id(lmtReplyAccSub.getAccSubNo());
                            }
                        }
                        accLoanInfo.setLmt_serno(lmtReplyAcc.getAccNo());
                        accLoanInfo.setPro_no(accLoanInfoDto.getPrdId());
                        accLoanInfo.setPrd_name(accLoanInfoDto.getPrdName());
                        accLoanInfo.setCus_name(accLoanInfoDto.getCusName());
                        accLoanInfo.setLoan_form(accLoanInfoDto.getLoanModal());
                        accLoanInfo.setAssure_means_main(accLoanInfoDto.getGuarMode());
                        accLoanInfo.setMargin_ratio(accLoanInfoDto.getMarginRatio());
                        accLoanInfo.setMargin_balance(accLoanInfoDto.getMarginBalance());
                        accLoanInfo.setBalance(accLoanInfoDto.getBalance());
                        accLoanInfo.setLoan_balance(accLoanInfoDto.getLoanBalance());
                        accLoanInfo.setPad_balance(accLoanInfoDto.getPadBalance());
                        accLoanInfo.setInterest_balance(accLoanInfoDto.getInterestBalance());
                        accLoanInfo.setHand_balance(accLoanInfoDto.getHandBalance());
                        accLoanInfo.setLoan_start_date(accLoanInfoDto.getLoanStartDate());
                        accLoanInfo.setLoan_end_date(accLoanInfoDto.getLoanEndDate());
                        accLoanInfo.setLoan_direction(accLoanInfoDto.getLoanTer());
                        accLoanInfo.setGuarantee_no(accLoanInfoDto.getGuaranteeNo());
                        accLoanInfo.setGuarantee_name(accLoanInfoDto.getGuaranteeName());
                        accLoanInfo.setLoancard_due(accLoanInfoDto.getLoancardDue());
                        accLoanInfo.setPro_details(accLoanInfoDto.getProDetails());
                        accLoanInfo.setLoan_paym_mtd(accLoanInfoDto.getLoanPaymMtd());
                        accLoanInfo.setIsin_card(accLoanInfoDto.getIsinCard());
                        accLoanInfo.setIsin_revocation(accLoanInfoDto.getIsinRevocation());
                        accLoanInfo.setLoan_due(accLoanInfoDto.getLoanDue());
                        accLoanInfo.setStatus(accLoanInfoDto.getStatus());
                        accLoanInfo.setBankstatus(accLoanInfoDto.getBankstatus());
                        accLoanInfo.setClear_status(accLoanInfoDto.getClearStatus());
                        accLoanInfo.setLow_cost(accLoanInfoDto.getLowCost());
                        accLoanInfo.setInput_id(accLoanInfoDto.getInputId());
                        accLoanInfo.setInput_br_id(accLoanInfoDto.getInputBrId());
                        accLoanInfo.setManager_id(accLoanInfoDto.getManagerId());
                        accLoanInfo.setManager_br_id(accLoanInfoDto.getManagerBrId());
                        accLoanInfo.setFina_br_id(accLoanInfoDto.getFinaBrId());
                        accLoanInfo.setForward_days(accLoanInfoDto.getForwardDays());
                        accLoanInfoList.add(accLoanInfo);
                    }
                }

                List<BailInfoDto> bailInfoDtoList = accCvrsService.getBailInfoDto(cusId);
                for(BailInfoDto bailInfoDto : bailInfoDtoList){
                    AssureAccInfo assureAccInfo = new AssureAccInfo();
                    assureAccInfo.setSerialno(bailInfoDto.getSerialno());// 流水号
                    assureAccInfo.setBill_no(bailInfoDto.getBillNo());// 借据编号
                    assureAccInfo.setCur_type(bailInfoDto.getCurType()); // 保证金币种
                    assureAccInfo.setSecurity_money_amt(bailInfoDto.getSecurityMoneyAmt()); // 保证金金额
                    assureAccInfo.setCont_state("0");// 状态
                    assureAccInfos.add(assureAccInfo);
                }
                irs99ReqDto.setAccLoanInfo(accLoanInfoList);
                irs99ReqDto.setAssureAccInfo(assureAccInfos);
                log.info("开始组装Irs99客户信息");
                //客户信息
                List<CustomerInfo> customerInfos = new ArrayList<>();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtLadEval.getCusId());
                ResultDto<Map<String,String>> levelResult = iCusClientService.selectGradeInfoByCusId(lmtLadEval.getCusId());
                Map<String, String> data = new HashMap<>();
                if(levelResult != null){
                    data = levelResult.getData();
                }
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtReplyAcc.getCusId()).getData();
                if (cusCorpDto != null) {
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setCus_id(lmtReplyAcc.getCusId());//客户号
                    customerInfo.setCus_name(lmtReplyAcc.getCusName());//客户名称  cuscorp这表数据库没有cusName字段
                    customerInfo.setCus_type(cusCorpDto.getCusType());//客户类型
                    customerInfo.setCert_code(cusBaseClientDto.getCertCode());//证件号码
                    String cusType = cusBaseClientDto.getCertType();
                    switch (cusBaseClientDto.getCertType()) {
                        case "M":
                            cusType = "24";
                            break;
                        case "N":
                            cusType = "26";
                            break;
                        case "P":
                            cusType = "27";
                            break;
                        case "Q":
                            cusType = "20";
                            break;
                        case "R":
                            cusType = "25";
                            break;
                        case "U":
                            cusType = "23";
                            break;
                        case "V":
                            cusType = "22";
                            break;
                    }
                    customerInfo.setCert_type(cusType);//证件类型
                    String comQysyz = cusCorpDto.getCorpOwnersType();
                    if("01".equals(comQysyz)){
                        customerInfo.setBus_owner("010");//企业所有制
                    }else if("02".equals(comQysyz)){
                        customerInfo.setBus_owner("020");
                    }else if("03".equals(comQysyz)){
                        customerInfo.setBus_owner("030");
                    }else if("04".equals(comQysyz)){
                        customerInfo.setBus_owner("040");
                    }else if("05".equals(comQysyz)){
                        customerInfo.setBus_owner("050");
                    }
                    customerInfo.setNew_industry_type(cusCorpDto.getTradeClass());//所属国标行业
                    if("0".equals(cusCorpDto.getIsBankBasicDepAccNo())){
                        customerInfo.setBas_acc_flg("2");
                    }else{
                        customerInfo.setBas_acc_flg(cusCorpDto.getIsBankBasicDepAccNo());//基本户是否在本行
                    }
                    ResultDto<Map<String, FinanIndicAnalyDto>> resultDto = iCusClientService.getFinRepRetProAndRatOfLia(cusId);
                    log.info("当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(resultDto)+"】}");
                    if(Objects.nonNull(resultDto) && Objects.nonNull(resultDto.getData())) {
                        Map<String, FinanIndicAnalyDto> resultMap = resultDto.getData();
                        BigDecimal assets = NumberUtils.nullDefaultZero(resultMap.get("lastYearSaleProfitRate").getCurYmValue()).divide(new BigDecimal(100));
                        if(assets.compareTo(new BigDecimal(0)) >= 0 && assets.compareTo(new BigDecimal("0.5")) < 0){
                            customerInfo.setAssets("010"); //资产负债率
                        }else if (assets.compareTo(new BigDecimal("0.5")) >= 0 && assets.compareTo(new BigDecimal("0.7")) < 0){
                            customerInfo.setAssets("020");
                        }else if (assets.compareTo(new BigDecimal("0.7")) >= 0 && assets.compareTo(new BigDecimal("0.9")) < 0){
                            customerInfo.setAssets("030");
                        }else if (assets.compareTo(new BigDecimal("0.7")) >= 0){
                            customerInfo.setAssets("040");
                        }
                    } else{
                        customerInfo.setAssets("");
                    }
                    String rank = "";
                    if(data != null){
                        if(StringUtils.nonEmpty(data.get("finalRank"))){
                            rank = data.get("finalRank");
                        }
                    }else {
                        if (StringUtils.nonEmpty(cusCorpDto.getBankLoanLevel())) {
                            rank = cusCorpDto.getBankLoanLevel();
                        }
                    }
                    if(StringUtils.nonEmpty(rank)){
                        customerInfo.setGrade(rank);//本行即期信用等级
                    }else{
                        throw BizException.error(null, EcbEnum.ECB020071.key, EcbEnum.ECB020071.value);
                    }
                    customerInfo.setReg_area_code(cusCorpDto.getRegiAreaCode());//注册地行政区划代码
                    customerInfo.setReg_area_name(cusCorpDto.getRegiAddr());//注册地行政区划代码
                    customerInfo.setCust_mgr(lmtReplyAcc.getManagerId());//主管客户经理
                    customerInfo.setMain_br_id(lmtReplyAcc.getManagerBrId());//主管机构
                    customerInfos.add(customerInfo);
                }else{
                    rtnCode = EcbEnum.ECB020058.key;
                    rtnMsg = EcbEnum.ECB020058.value;
                    return rtnData;
                }
                irs99ReqDto.setCustomerInfo(customerInfos);
                log.info("开始组装Irs99授信分项额度与抵质押、保证人关系信息");
                // 授信分项额度与抵质押、保证人关系信息
                List<LimitPleMortInfo> limitPleMortInfos = new ArrayList<>();
                for (LmtReplyAccSub lmtReplyAccSubHigh : accSubList) {
                    List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtReplyAccSubHigh.getSubSerno());
                    for (GuarBizRel guarBizRel : guarBizRelList) {
                        LimitPleMortInfo limitPleMortInfo = new LimitPleMortInfo();
                        limitPleMortInfo.setDetail_serno(guarBizRel.getSerno());
                        limitPleMortInfo.setItem_id(lmtReplyAccSubHigh.getAccSubNo());
                        limitPleMortInfo.setGuaranty_id(guarBizRel.getGuarNo());
                        limitPleMortInfos.add(limitPleMortInfo);
                    }
                }
                irs99ReqDto.setLimitPleMortInfo(limitPleMortInfos);
                log.info("开始组装Irs99最高额授信协议信息");
                // 最高额授信协议 CTR_HIGH_AMT_AGR_CONT
                List<CtrHighAmtAgrCont> ctrHighAmtAgrContList = ctrHighAmtAgrContService.selectDataByCusId(cusId);
                List<UpApplyInfo> upApplyInfos = new ArrayList<>();
                for (CtrHighAmtAgrCont ctrHighAmtAgrCont : ctrHighAmtAgrContList) {
                    CmisLmt0066ReqDto reqDto = new CmisLmt0066ReqDto();
                    reqDto.setDealBizNo(ctrHighAmtAgrCont.getContNo());
                    ResultDto<CmisLmt0066RespDto> cmisLmt0066RespDto = cmisLmtClientService.cmislmt0066(reqDto);
                    if (cmisLmt0066RespDto != null && cmisLmt0066RespDto.getData() != null && "0000".equals(cmisLmt0066RespDto.getData().getErrorCode())) {
                        log.info("额度编号查询成功");
                        itemId = cmisLmt0066RespDto.getData().getLimitSubNo();
                    } else {
                        throw new Exception("查询额度接口异常");
                    }
                    UpApplyInfo upApplyInfo = new UpApplyInfo();
                    upApplyInfo.setLmt_serno(ctrHighAmtAgrCont.getSerno());
                    upApplyInfo.setItem_id(itemId);
                    upApplyInfo.setCus_id(ctrHighAmtAgrCont.getCusId());
                    upApplyInfo.setCus_name(ctrHighAmtAgrCont.getCusName());
                    upApplyInfo.setCur_type(ctrHighAmtAgrCont.getAgrType());
                    upApplyInfo.setAmt(ctrHighAmtAgrCont.getAgrAmt());
                    if("600".equals(ctrHighAmtAgrCont.getContStatus())){
                        upApplyInfo.setStatus("300");
                    }else{
                        upApplyInfo.setStatus(ctrHighAmtAgrCont.getContStatus());
                    }
                    upApplyInfo.setStart_date(ctrHighAmtAgrCont.getStartDate());
                    upApplyInfo.setEnd_date(ctrHighAmtAgrCont.getEndDate());
                    upApplyInfos.add(upApplyInfo);
                }
                irs99ReqDto.setUpApplyInfo(upApplyInfos);

                //交易对手信息

                //汇率信息
                List<CurInfo> curInfoList = new ArrayList<>();
                QueryModel model = new QueryModel();
                List<CfgTfRateDto> result = new ArrayList<>();
                ResultDto<List<CfgTfRateDto>> resultDto = iCmisCfgClientService.queryAllCfgTfRate(model);
                result = resultDto.getData();
                List<CfgTfRateDto> cfgTfRateDtos = JSONArray.parseArray(JSON.toJSONString(result), CfgTfRateDto.class);
                if(cfgTfRateDtos != null && cfgTfRateDtos.size()>0){
                    for(CfgTfRateDto cfgTfRateDto : cfgTfRateDtos){
                        CurInfo curInfo = new CurInfo();
                        curInfo.setCurtype(cfgTfRateDto.getCurType());
                        BigDecimal EXUNIT = new BigDecimal(cfgTfRateDto.getExunit());
                        BigDecimal CSBYPR = cfgTfRateDto.getCsbypr();
                        curInfo.setCurvalue(CSBYPR.divide(EXUNIT));
                        curInfoList.add(curInfo);
                    }
                }else{
                    CurInfo curInfo = new CurInfo();
                    curInfo.setCurtype("CNY");
                    curInfo.setCurvalue(new BigDecimal(1));
                    curInfoList.add(curInfo);
                }

                irs99ReqDto.setCurInfo(curInfoList);
                log.info("组装Irs99报文信息结束");
                log.info("Irs99报文为"+ JSON.toJSONString(irs99ReqDto)+"】}");
                ResultDto<Irs99RespDto> irs99RespDtoResultDto = dscms2IrsClientService.irs99(irs99ReqDto);
                log.info("单一客户债项评级新增测算返回:{}" + irs99RespDtoResultDto.toString());
                if (SuccessEnum.CMIS_SUCCSESS.key.equals(irs99RespDtoResultDto.getCode())) {
                    Irs99RespDto irs99RespDto = irs99RespDtoResultDto.getData();
                    if (irs99RespDto != null) {
                        if(irs99RespDto.getBizMessageInfo() != null && irs99RespDto.getBizMessageInfo().size() > 0){
                            BizMessageInfo bizMessageInfo = irs99RespDto.getBizMessageInfo().get(0);
                            lmtLadEval.setSerno(bizMessageInfo.getSerialno());
                            lmtLadEval.setEad(bizMessageInfo.getEad());
                            lmtLadEval.setLgd(bizMessageInfo.getLgd());
                            lmtLadEval.setDebtLevel(bizMessageInfo.getGuaranteegrade());
                        }else{
                            throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                        }
                    }else{
                        throw BizException.error(null, EcbEnum.ECB010077.key, EcbEnum.ECB010077.value);
                    }
                }else{
                    throw BizException.error(null, irs99RespDtoResultDto.getCode(), irs99RespDtoResultDto.getMessage());
                }
            } catch (YuspException e) {
                throw BizException.error(null, e.getCode(), e.getMsg());
            } finally {
                rtnData.put("rtnCode", rtnCode);
                rtnData.put("rtnMsg", rtnMsg);
                rtnData.put("lmtLadEval",lmtLadEval);
            }
            return rtnData;
        }

    /**
     * 校验是否可以新增内评低准入数据
     * @param serno
     * @return
     */
    public Map checkNpGreenApp(String serno){
        //授信申请时,PD*LGD>5.5%,PD为对公客户评级中的违约概率, LGD为债项评级中的违约损失率
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            LmtLadEval lmtLadEval = selectSingleBySerno(serno);
            if (Objects.nonNull(lmtLadEval)){
                BigDecimal lgd = lmtLadEval.getLgd();
                String cusId = lmtLadEval.getCusId();
                ResultDto<Map<String, String>> mapResultDto = iCusClientService.selectGradeInfoByCusId(cusId);
                if(mapResultDto!=null&&mapResultDto.getData()!=null){
                    String pd = mapResultDto.getData().get("pd");
                    BigDecimal PD = new BigDecimal(pd);
                    if(PD.multiply(lgd).compareTo(new BigDecimal(0.055))>0){
                        rtnCode = EcbEnum.ECB010000.key;
                        rtnMsg =  EcbEnum.ECB010000.value;
                    }else{
                        rtnCode = EcbEnum.ECB020035.key;
                        rtnMsg =  EcbEnum.ECB020035.value;
                    }
                }else {
                    rtnCode = EcbEnum.ECB020035.key;
                    rtnMsg =  EcbEnum.ECB020035.value;
                }
            }else{
                rtnCode = EcbEnum.ECB020035.key;
                rtnMsg =  EcbEnum.ECB020035.value;
            }

        } catch (YuspException e) {
                rtnCode = e.getCode();
                rtnMsg = e.getMsg();
            } catch (Exception e) {
                rtnCode = EpbEnum.EPB099999.key;
                rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
            } finally {
                rtnData.put("rtnCode", rtnCode);
                rtnData.put("rtnMsg", rtnMsg);
            }
            return rtnData;
    }

    /**
     * 获取当前客户对应的在我行的敞口余额
     * @param map
     * @return
     */

    public BigDecimal calCurOtherBankDebt(Map map) {
        BigDecimal openBalance = new BigDecimal("0.0");
        CmisLmt0052ReqDto cmisLmt0052ReqDto = new CmisLmt0052ReqDto();
        String cusId = "";
        BigDecimal cusOutsideDebt = new BigDecimal(0);
        try{
            // 金融机构代码
            String instuCde = CmisCommonConstants.INSTUCDE_001;
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo.getOrg().getId().startsWith("80")) {
                instuCde = CmisCommonConstants.INSTUCDE_002;
            } else if (userInfo.getOrg().getId().startsWith("81")) {
                instuCde = CmisCommonConstants.INSTUCDE_003;
            }
            cusId = map.get("cusId") == null ? cusId:(String) map.get("cusId");
            cusOutsideDebt = new BigDecimal((int) map.get("cusOutsideDebt")) ;
            cmisLmt0052ReqDto.setCusId(cusId);
            cmisLmt0052ReqDto.setInstuCde(instuCde);
            cmisLmt0052ReqDto.setDealBizNo(UUID.randomUUID().toString());
            log.info("根据客户号【{}】查询单一客户存量敞口余额，请求报文为{}", cusId, JSON.toJSONString(cmisLmt0052ReqDto));
            CmisLmt0052RespDto cmisLmt0052RespDto = cmisLmtClientService.cmislmt0052(cmisLmt0052ReqDto).getData();
            log.info("根据客户号【{}】查询单一客户存量敞口余额，返回为{}", cusId, JSON.toJSONString(cmisLmt0052RespDto));
            if (cmisLmt0052RespDto != null && cmisLmt0052RespDto.getSpcaBalanceAmt() != null) {
                openBalance = cusOutsideDebt.subtract(cmisLmt0052RespDto.getSpcaBalanceAmt());
            }
        }catch(Exception e){
            throw BizException.error(null, String.valueOf(e.hashCode()), String.valueOf(e.getMessage()));
        }
        return openBalance;
    }
}
