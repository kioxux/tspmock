package cn.com.yusys.yusp.service.server.xdls0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0004.resp.Xdls0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询信贷有无授信历史
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdls0004Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdls0004Service.class);

    @Autowired
    private LmtReplyMapper lmtReplyMapper;

    /**
     * 查询信贷有无授信历史
     *
     * @param cusId
     * @return
     * @author lihh
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdls0004DataRespDto hasLmtReplyByCusId(String cusId) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, cusId);
        Xdls0004DataRespDto xdls0004DataRespDto = null;
        try {
            xdls0004DataRespDto = lmtReplyMapper.hasLmtReplyByCusId(cusId);
        } catch (BizException e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, e.getMessage());
            throw new Exception(DscmsEnum.TRADE_CODE_XDLS0004.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004DataRespDto));
        return xdls0004DataRespDto;
    }
}
