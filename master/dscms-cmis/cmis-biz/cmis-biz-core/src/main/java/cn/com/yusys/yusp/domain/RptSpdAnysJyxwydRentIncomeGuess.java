/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeGuess
 * @类描述: rpt_spd_anys_jyxwyd_rent_income_guess数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jyxwyd_rent_income_guess")
public class RptSpdAnysJyxwydRentIncomeGuess extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 年份 **/
	@Column(name = "YEAR", unique = false, nullable = true, length = 10)
	private String year;
	
	/** 本金余额 **/
	@Column(name = "PRINCIPAL_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal principalBalance;
	
	/** 利息支出 **/
	@Column(name = "INTEREST_EXPENSE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal interestExpense;
	
	/** 需归还本金 **/
	@Column(name = "PRINCIPAL_TO_BE_RETURNED", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal principalToBeReturned;
	
	/** 还款本利和 **/
	@Column(name = "REPAYMENT_OF_PRINCIPAL_AND_INTEREST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repaymentOfPrincipalAndInterest;
	
	/** 租金收入 **/
	@Column(name = "RENTAL_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentalIncome;
	
	/** 差额 **/
	@Column(name = "DIFFERENCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal difference;
	
	/** 备注 **/
	@Column(name = "REMARKS", unique = false, nullable = true, length = 40)
	private String remarks;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param year
	 */
	public void setYear(String year) {
		this.year = year;
	}
	
    /**
     * @return year
     */
	public String getYear() {
		return this.year;
	}
	
	/**
	 * @param principalBalance
	 */
	public void setPrincipalBalance(java.math.BigDecimal principalBalance) {
		this.principalBalance = principalBalance;
	}
	
    /**
     * @return principalBalance
     */
	public java.math.BigDecimal getPrincipalBalance() {
		return this.principalBalance;
	}
	
	/**
	 * @param interestExpense
	 */
	public void setInterestExpense(java.math.BigDecimal interestExpense) {
		this.interestExpense = interestExpense;
	}
	
    /**
     * @return interestExpense
     */
	public java.math.BigDecimal getInterestExpense() {
		return this.interestExpense;
	}
	
	/**
	 * @param principalToBeReturned
	 */
	public void setPrincipalToBeReturned(java.math.BigDecimal principalToBeReturned) {
		this.principalToBeReturned = principalToBeReturned;
	}
	
    /**
     * @return principalToBeReturned
     */
	public java.math.BigDecimal getPrincipalToBeReturned() {
		return this.principalToBeReturned;
	}
	
	/**
	 * @param repaymentOfPrincipalAndInterest
	 */
	public void setRepaymentOfPrincipalAndInterest(java.math.BigDecimal repaymentOfPrincipalAndInterest) {
		this.repaymentOfPrincipalAndInterest = repaymentOfPrincipalAndInterest;
	}
	
    /**
     * @return repaymentOfPrincipalAndInterest
     */
	public java.math.BigDecimal getRepaymentOfPrincipalAndInterest() {
		return this.repaymentOfPrincipalAndInterest;
	}
	
	/**
	 * @param rentalIncome
	 */
	public void setRentalIncome(java.math.BigDecimal rentalIncome) {
		this.rentalIncome = rentalIncome;
	}
	
    /**
     * @return rentalIncome
     */
	public java.math.BigDecimal getRentalIncome() {
		return this.rentalIncome;
	}
	
	/**
	 * @param difference
	 */
	public void setDifference(java.math.BigDecimal difference) {
		this.difference = difference;
	}
	
    /**
     * @return difference
     */
	public java.math.BigDecimal getDifference() {
		return this.difference;
	}
	
	/**
	 * @param remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
    /**
     * @return remarks
     */
	public String getRemarks() {
		return this.remarks;
	}


}