package cn.com.yusys.yusp.service.server.xdzc0018;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.dto.server.xdzc0018.req.Xdzc0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.resp.Xdzc0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * 接口处理类:资产汇总
 *
 * @Author xs
 * @Date 2021/6/13 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0018Service {

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0018.Xdzc0018Service.class);

    /**
     * 交易码：xdzc0018
     * 交易描述:
     * 资产汇总查询
     *
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0018DataRespDto xdzc0018Service(Xdzc0018DataReqDto xdzc0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value);
        Xdzc0018DataRespDto xdzc0018DataRespDto = new Xdzc0018DataRespDto();
        String cusId = xdzc0018DataReqDto.getCusId();// 客户号
        BigDecimal assetSumAmt = BigDecimal.ZERO;// 资产总额
        BigDecimal eBillSumAmt = BigDecimal.ZERO;// 电票资产总额
        BigDecimal orderSumAmt = BigDecimal.ZERO;// 存单资产总额
        BigDecimal creditAssetAmt  = BigDecimal.ZERO;// 信用证资产总额
        try {
            //或当先生效的资产池协议
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);// 客户编码
            queryModel.addCondition("contNo",ctrAsplDetails.getContNo());// 资产池协议编号
            queryModel.addCondition("isPool", CommonConstance.STD_ZB_YES_NO_1);//入池
            queryModel.addCondition("isPledge", CommonConstance.STD_ZB_YES_NO_1);//入池质押
            queryModel.addCondition("oprType", CommonConstance.OPR_TYPE_ADD);//新增操作类型
            List<AsplAssetsList> AsplAssetsLists = asplAssetsListService.selectAll(queryModel);
            if (CollectionUtils.nonEmpty(AsplAssetsLists)){
                assetSumAmt = AsplAssetsLists.stream()
                        .map(e -> Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO) )
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
                        .setScale(4,BigDecimal.ROUND_DOWN);
                eBillSumAmt = AsplAssetsLists.stream()
                        .filter(e->CommonConstance.STD_ASPL_ASSET_TYPE_01.equals(e.getAssetType()))// 电票
                        .map(e -> Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO) )
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
                        .setScale(4,BigDecimal.ROUND_DOWN);
                orderSumAmt = AsplAssetsLists.stream()
                        .filter(e->CommonConstance.STD_ASPL_ASSET_TYPE_02.equals(e.getAssetType()))// 存单
                        .map(e -> Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO) )
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
                        .setScale(4,BigDecimal.ROUND_DOWN);
                creditAssetAmt = AsplAssetsLists.stream()
                        .filter(e->CommonConstance.STD_ASPL_ASSET_TYPE_03.equals(e.getAssetType())
                                || CommonConstance.STD_ASPL_ASSET_TYPE_04.equals(e.getAssetType()))
                        .map(e -> Optional.ofNullable(e.getAssetValue()).orElse(BigDecimal.ZERO) )
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
                        .setScale(4,BigDecimal.ROUND_DOWN);
                xdzc0018DataRespDto.setAssetSumAmt(assetSumAmt);// 资产总额
                xdzc0018DataRespDto.setOrderSumAmt(eBillSumAmt);// 电票资产总额
                xdzc0018DataRespDto.setEBillSumAmt(orderSumAmt);// 存单资产总额
                xdzc0018DataRespDto.setCreditAssetAmt(creditAssetAmt);// 信用证资产总额
                xdzc0018DataRespDto.setOrderPer(orderSumAmt.divide(assetSumAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 存单资产占比
                xdzc0018DataRespDto.setEBillPer(eBillSumAmt.divide(assetSumAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 电票资产占比
                xdzc0018DataRespDto.setCreditPer(creditAssetAmt.divide(assetSumAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 信用证资产占比
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value);
        return xdzc0018DataRespDto;
    }
}