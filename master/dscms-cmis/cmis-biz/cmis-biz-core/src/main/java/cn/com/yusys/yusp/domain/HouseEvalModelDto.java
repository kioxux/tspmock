package cn.com.yusys.yusp.domain;


/**
 * @version 1.0.0
 * @项目名称:eval
 * @类名称:EvalRequestDTO
 * @类描述:市场比较法估值计算请求DTO
 * @功能描述:
 * @创建时间:2021年3月24日
 * @修改备注:
 * @修改日期 修改人员        修改原因 --------    --------		----------------------------------------
 * @Copyright (c) 2018宇信科技-版权所有
 */
public class HouseEvalModelDto {
    //面积
    private double area;
    //单价
    private double a3;
    private double b3;
    private double c3;

    //权重
    private double area_busy;
    private double traffic;
    private double environ;
    private double publics;
    private double building;
    private double orient;
    private double fitment;
    private double equip;
    private double property;
    private double projects;
    private double remainyear;
    private double poltratio;

    //对比实例一
    private double a4;
    private double a5;
    private double a6;
    private double a7;
    private double a8;
    private double a9;
    private double a10;
    private double a11;
    private double a12;
    private double a13;
    private double a14;
    private double a15;

    //对比实例二
    private double b4;
    private double b5;
    private double b6;
    private double b7;
    private double b8;
    private double b9;
    private double b10;
    private double b11;
    private double b12;
    private double b13;
    private double b14;
    private double b15;

    //对比实例三
    private double c4;
    private double c5;
    private double c6;
    private double c7;
    private double c8;
    private double c9;
    private double c10;
    private double c11;
    private double c12;
    private double c13;
    private double c14;
    private double c15;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getA3() {
        return a3;
    }

    public void setA3(double a3) {
        this.a3 = a3;
    }

    public double getB3() {
        return b3;
    }

    public void setB3(double b3) {
        this.b3 = b3;
    }

    public double getC3() {
        return c3;
    }

    public void setC3(double c3) {
        this.c3 = c3;
    }

    public double getArea_busy() {
        return area_busy;
    }

    public void setArea_busy(double area_busy) {
        this.area_busy = area_busy;
    }

    public double getTraffic() {
        return traffic;
    }

    public void setTraffic(double traffic) {
        this.traffic = traffic;
    }

    public double getEnviron() {
        return environ;
    }

    public void setEnviron(double environ) {
        this.environ = environ;
    }

    public double getPublics() {
        return publics;
    }

    public void setPublics(double publics) {
        this.publics = publics;
    }

    public double getBuilding() {
        return building;
    }

    public void setBuilding(double building) {
        this.building = building;
    }

    public double getOrient() {
        return orient;
    }

    public void setOrient(double orient) {
        this.orient = orient;
    }

    public double getFitment() {
        return fitment;
    }

    public void setFitment(double fitment) {
        this.fitment = fitment;
    }

    public double getEquip() {
        return equip;
    }

    public void setEquip(double equip) {
        this.equip = equip;
    }

    public double getProperty() {
        return property;
    }

    public void setProperty(double property) {
        this.property = property;
    }

    public double getProjects() {
        return projects;
    }

    public void setProjects(double projects) {
        this.projects = projects;
    }

    public double getRemainyear() {
        return remainyear;
    }

    public void setRemainyear(double remainyear) {
        this.remainyear = remainyear;
    }

    public double getPoltratio() {
        return poltratio;
    }

    public void setPoltratio(double poltratio) {
        this.poltratio = poltratio;
    }

    public double getA4() {
        return a4;
    }

    public void setA4(double a4) {
        this.a4 = a4;
    }

    public double getA5() {
        return a5;
    }

    public void setA5(double a5) {
        this.a5 = a5;
    }

    public double getA6() {
        return a6;
    }

    public void setA6(double a6) {
        this.a6 = a6;
    }

    public double getA7() {
        return a7;
    }

    public void setA7(double a7) {
        this.a7 = a7;
    }

    public double getA8() {
        return a8;
    }

    public void setA8(double a8) {
        this.a8 = a8;
    }

    public double getA9() {
        return a9;
    }

    public void setA9(double a9) {
        this.a9 = a9;
    }

    public double getA10() {
        return a10;
    }

    public void setA10(double a10) {
        this.a10 = a10;
    }

    public double getA11() {
        return a11;
    }

    public void setA11(double a11) {
        this.a11 = a11;
    }

    public double getA12() {
        return a12;
    }

    public void setA12(double a12) {
        this.a12 = a12;
    }

    public double getA13() {
        return a13;
    }

    public void setA13(double a13) {
        this.a13 = a13;
    }

    public double getA14() {
        return a14;
    }

    public void setA14(double a14) {
        this.a14 = a14;
    }

    public double getA15() {
        return a15;
    }

    public void setA15(double a15) {
        this.a15 = a15;
    }

    public double getB4() {
        return b4;
    }

    public void setB4(double b4) {
        this.b4 = b4;
    }

    public double getB5() {
        return b5;
    }

    public void setB5(double b5) {
        this.b5 = b5;
    }

    public double getB6() {
        return b6;
    }

    public void setB6(double b6) {
        this.b6 = b6;
    }

    public double getB7() {
        return b7;
    }

    public void setB7(double b7) {
        this.b7 = b7;
    }

    public double getB8() {
        return b8;
    }

    public void setB8(double b8) {
        this.b8 = b8;
    }

    public double getB9() {
        return b9;
    }

    public void setB9(double b9) {
        this.b9 = b9;
    }

    public double getB10() {
        return b10;
    }

    public void setB10(double b10) {
        this.b10 = b10;
    }

    public double getB11() {
        return b11;
    }

    public void setB11(double b11) {
        this.b11 = b11;
    }

    public double getB12() {
        return b12;
    }

    public void setB12(double b12) {
        this.b12 = b12;
    }

    public double getB13() {
        return b13;
    }

    public void setB13(double b13) {
        this.b13 = b13;
    }

    public double getB14() {
        return b14;
    }

    public void setB14(double b14) {
        this.b14 = b14;
    }

    public double getB15() {
        return b15;
    }

    public void setB15(double b15) {
        this.b15 = b15;
    }

    public double getC4() {
        return c4;
    }

    public void setC4(double c4) {
        this.c4 = c4;
    }

    public double getC5() {
        return c5;
    }

    public void setC5(double c5) {
        this.c5 = c5;
    }

    public double getC6() {
        return c6;
    }

    public void setC6(double c6) {
        this.c6 = c6;
    }

    public double getC7() {
        return c7;
    }

    public void setC7(double c7) {
        this.c7 = c7;
    }

    public double getC8() {
        return c8;
    }

    public void setC8(double c8) {
        this.c8 = c8;
    }

    public double getC9() {
        return c9;
    }

    public void setC9(double c9) {
        this.c9 = c9;
    }

    public double getC10() {
        return c10;
    }

    public void setC10(double c10) {
        this.c10 = c10;
    }

    public double getC11() {
        return c11;
    }

    public void setC11(double c11) {
        this.c11 = c11;
    }

    public double getC12() {
        return c12;
    }

    public void setC12(double c12) {
        this.c12 = c12;
    }

    public double getC13() {
        return c13;
    }

    public void setC13(double c13) {
        this.c13 = c13;
    }

    public double getC14() {
        return c14;
    }

    public void setC14(double c14) {
        this.c14 = c14;
    }

    public double getC15() {
        return c15;
    }

    public void setC15(double c15) {
        this.c15 = c15;
    }
}
