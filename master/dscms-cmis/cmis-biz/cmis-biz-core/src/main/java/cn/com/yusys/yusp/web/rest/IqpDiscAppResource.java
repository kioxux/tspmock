/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDiscApp;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.service.IqpDiscAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiscAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:03:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "贴现协议申请")
@RequestMapping("/api/iqpdiscapp")
public class IqpDiscAppResource {
    @Autowired
    private IqpDiscAppService iqpDiscAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDiscApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDiscApp> list = iqpDiscAppService.selectAll(queryModel);
        return new ResultDto<List<IqpDiscApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpDiscApp>> index(QueryModel queryModel) {
        List<IqpDiscApp> list = iqpDiscAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpDiscApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpDiscApp> show(@PathVariable("pkId") String pkId) {
        IqpDiscApp iqpAppDisc = iqpDiscAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpDiscApp>(iqpAppDisc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpDiscApp> create(@RequestBody IqpDiscApp iqpAppDisc) throws URISyntaxException {
        iqpDiscAppService.insert(iqpAppDisc);
        return new ResultDto<IqpDiscApp>(iqpAppDisc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpDiscApp iqpAppDisc) throws URISyntaxException {
        int result = iqpDiscAppService.updateSelective(iqpAppDisc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpDiscAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDiscAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 贴现申请保存方法
     * @param iqpDiscApp
     * @return
     */
    @ApiOperation("贴现申请保存方法")
    @PostMapping("/saveiqpdiscappinfo")
    public ResultDto<Map> saveIqpDiscAppInfo(@RequestBody IqpDiscApp iqpDiscApp) throws Exception {
        Map result = iqpDiscAppService.saveIqpDiscAppInfo(iqpDiscApp);
        return new ResultDto<>(result);
    }

    /**
     * 贴现申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("贴现申请通用的保存方法")
    @PostMapping("/commonsaveiqpdiscappinfo")
    public ResultDto<Map> commonSaveIqpDiscAppInfo(@RequestBody Map params){
        Map rtnData = iqpDiscAppService.commonSaveIqpDiscAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贴现申请待发起列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贴现申请待发起列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpDiscApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpDiscApp> list = iqpDiscAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpDiscApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:银承申请历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贴现申请历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpDiscApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpDiscApp> list = iqpDiscAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpDiscApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贴现协议申请明细展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpDiscApp iqpDiscApp = iqpDiscAppService.selectByDiscSernoKey((String)map.get("serno"));
        if (iqpDiscApp != null) {
            resultDto.setCode(0);
            resultDto.setData(iqpDiscApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(0);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpDiscAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * 贴现协议申请逻辑删除方法
     * @param iqpDiscApp
     * @return
     */
    @ApiOperation("贴现协议申请逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpDiscApp iqpDiscApp){
        Integer result = iqpDiscAppService.logicDelete(iqpDiscApp);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:getAccNoInfo
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贴现协议申请账户获取")
    @PostMapping("/getaccnoinfo")
    protected ResultDto<Ib1253RespDto> getAccNoInfo(@RequestBody  Map map) {
        ResultDto<Ib1253RespDto> resultDto = iqpDiscAppService.getAccNoInfo(map);
        return resultDto;
    }

    /**
     * @函数名称:iqpDiscAppSubmitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/iqpDiscAppSubmitNoFlow")
    protected ResultDto<Map> iqpDiscAppSubmitNoFlow(@RequestBody IqpDiscApp iqpDiscApp) throws URISyntaxException {
        Map rtnData = iqpDiscAppService.iqpDiscAppSubmitNoFlow(iqpDiscApp.getSerno());
        return new ResultDto<>(rtnData);
    }
}
