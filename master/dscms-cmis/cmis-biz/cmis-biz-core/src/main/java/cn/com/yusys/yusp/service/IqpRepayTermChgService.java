/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpRepayTermChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpRepayTermChgMapper;
import cn.com.yusys.yusp.util.BizUtils;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayTermChgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-18 19:24:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpRepayTermChgService {

    @Autowired
    private IqpRepayTermChgMapper iqpRepayTermChgMapper;
    private static final Logger log = LoggerFactory.getLogger(IqpRepayTermChgService.class);

    // 贷款台账
    @Autowired
    private AccLoanService accLoanService;
    //还款方式变更接口
    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpRepayTermChg selectByPrimaryKey(String iqpSerno) {
        return iqpRepayTermChgMapper.selectByPrimaryKey(iqpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpRepayTermChg> selectAll(QueryModel model) {
        List<IqpRepayTermChg> records = iqpRepayTermChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpRepayTermChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayTermChg> list = iqpRepayTermChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpRepayTermChg record) {
        return iqpRepayTermChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpRepayTermChg record) {
        return iqpRepayTermChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpRepayTermChg record) {
        return iqpRepayTermChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpRepayTermChg record) {
        return iqpRepayTermChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpRepayTermChgMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRepayTermChgMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertIqpRepayTermChgByBillNo
     * @方法描述: 新增还款间隔周期变更申请表数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertIqpRepayTermChgByBillNo(IqpRepayTermChg iqpRepayTermChg) throws Exception {
        int result = 0;
        try {
            log.info("根据借据编号新增还款间隔周期变更开始【{}】", JSONObject.toJSON(iqpRepayTermChg));
            // 获取借据号对应的贷款台账信息
            AccLoan accLoan = accLoanService.selectByPrimaryKey(iqpRepayTermChg.getBillNo());
            if (Objects.isNull(accLoan)) {
                throw new Exception("借据编号不存在！");
            }
            BizUtils bizUtils = new BizUtils();
            iqpRepayTermChg = (IqpRepayTermChg) bizUtils.getMappingValueBySourceAndDisTable(accLoan, iqpRepayTermChg);
            // 默认值信息
            iqpRepayTermChg.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpRepayTermChg.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 登录人信息
            log.info(String.format("获取当前登录用户数据"));
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key, EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value);
            }
            iqpRepayTermChg.setManagerId(userInfo.getLoginCode());
            iqpRepayTermChg.setManagerBrId(userInfo.getOrg().getCode());
            iqpRepayTermChg.setInputId(userInfo.getLoginCode());
            iqpRepayTermChg.setInputBrId(userInfo.getOrg().getCode());
            iqpRepayTermChg.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            iqpRepayTermChg.setUpdId(userInfo.getLoginCode());
            iqpRepayTermChg.setUpdBrId(userInfo.getOrg().getCode());
            iqpRepayTermChg.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            result = iqpRepayTermChgMapper.insertSelective(iqpRepayTermChg);
            // result = iqpRepayTermChgMapper.insertLoanInfoByBillNoIntoIqpRepayTermChg(iqpRepayTermChg);
            if(result == 0){
                throw new YuspException(EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
            }
            log.info("根据借据编号新增还款间隔周期变更结束【{}】", JSONObject.toJSON(iqpRepayTermChg));
        } catch (YuspException e) {
            log.error("还款间隔周期变更申请新增失败！", e);
            return -1;
        }
        return result;
    }

    /**
     * @方法名称: checkIsExistIqpRepayTermChgBizByBillNo
     * @方法描述: 通过借据号，查询是否存在在途的还款间隔周期变更业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int checkIsExistIqpRepayTermChgBizByBillNo(String iqpSerno, String billNo) {
        int result = 0;
        try {
            log.info("校验是否存在在途的还款间隔周期变更申请【{}】", JSONObject.toJSON(iqpSerno));
            HashMap<String,String> param = new HashMap<String,String >();
            // 获取 当前流水号对应的 还款间隔周期 信息
            IqpRepayTermChg iqpRepayTermChg = selectByPrimaryKey(iqpSerno);
            // 判断是否需要排除当前流水号对应的  还款间隔周期变更信息
            if(iqpRepayTermChg != null ){
                param.put("iqp_serno",iqpSerno);
            }
            param.put("wfExistsFlag", CmisFlowConstants.REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME);
            // 放入需要的操作类型
            param.put("opr_type", CmisCommonConstants.OPR_TYPE_ADD);
            // 流水号
            param.put("bill_no",billNo);
            result = iqpRepayTermChgMapper.checkIsExistIqpRepayTermChgBizByBillNo(param);
            return result;
        } catch (YuspException e) {
            log.error("校验在途的还款间隔周期变更失败！", e);
            // throw new YuspException(EcbEnum.IQP_REPAY_TERM_CHG_CHECK_EXCEPTION_4.key,EcbEnum.IQP_REPAY_TERM_CHG_CHECK_EXCEPTION_4.value);
            return -1;
        }
    }

    /**
     * @方法名称: updateApproveStatusByIqpSerno
     * @方法描述: 更新表中申请状态数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateApproveStatusByIqpSerno(String iqpSerno, String wfStatus111) {
        int result = 0;
        try {
            IqpRepayTermChg iqpRepayTermChg = new IqpRepayTermChg();
            iqpRepayTermChg.setIqpSerno(iqpSerno);
            iqpRepayTermChg.setApproveStatus(wfStatus111);
            result = iqpRepayTermChgMapper.updateByPrimaryKeySelective(iqpRepayTermChg);
        } catch (YuspException e) {
            log.error("更新申请状态失败！", e);
            return -1;
        }
        return result;
    }

    /**
     * @方法名称: updateRepayTermAfterWfAppr
     * @方法描述: 更新贷款台账表数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void updateRepayTermAfterWfAppr(String iqpSerno) {
        IqpRepayTermChg iqpRepayTermChg = new IqpRepayTermChg();
        try {
            // 获取还款间隔周期变更数据
            iqpRepayTermChg = selectByPrimaryKey(iqpSerno);
            // 更新贷款台账表
            AccLoan accLoan = new AccLoan();
            accLoan.setBillNo(iqpRepayTermChg.getBillNo());
            /**
             * zhanyb修改20210427。以下为原内容
             accLoan.setRepayTerm(iqpRepayTermChg.getRepayTerm());
             accLoan.setRepaySpace(iqpRepayTermChg.getRepaySpace());
             accLoan.setRepayDate(Integer.parseInt(iqpRepayTermChg.getRepayDate()));
             */
            accLoan.setEiIntervalCycle(iqpRepayTermChg.getRepayTerm());//结息间隔周期
            accLoan.setEiIntervalUnit(iqpRepayTermChg.getRepaySpace());  //结息间隔周期单位
            accLoan.setDeductDay(iqpRepayTermChg.getRepayDate()); //扣款日
            // 最后更新人信息
            accLoan.setUpdId(iqpRepayTermChg.getUpdId());
            accLoan.setUpdBrId(iqpRepayTermChg.getUpdBrId());
            accLoan.setUpdDate(iqpRepayTermChg.getUpdDate());
            accLoanService.updateSelective(accLoan);
        } catch (YuspException e) {
            log.error("更新贷款台账还款间隔周期信息处理发生异常！",e);
            throw new YuspException(EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_2.key, EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_2.value);
        }

    }

    /**
     * @方法名称: checkIsExistChgBizByBillNo
     * @方法描述: 调用通用的  在途变更业务校验 处理方法
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int checkIsExistChgBizByBillNo(String iqpSerno,String billNo) {
        int result = 0;
//        result = iqpRepayWayChgService.checkIsExistChgBizByBillNo(iqpSerno,billNo);
        return result;
    }
}
