/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CoopProAccInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopProAccInfo;
import cn.com.yusys.yusp.repository.mapper.CoopProAccInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopProAccInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-14 23:45:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopProAccInfoService {

    @Autowired
    private CoopProAccInfoMapper coopProAccInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopProAccInfo selectByPrimaryKey(String proNo) {
        return coopProAccInfoMapper.selectByPrimaryKey(proNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopProAccInfo> selectAll(QueryModel model) {
        List<CoopProAccInfo> records = (List<CoopProAccInfo>) coopProAccInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopProAccInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopProAccInfo> list = coopProAccInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopProAccInfo record) {
        return coopProAccInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopProAccInfo record) {
        return coopProAccInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopProAccInfo record) {
        return coopProAccInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopProAccInfo record) {
        return coopProAccInfoMapper.updateByPrimaryKeySelective(record);
    }

    public int updateStatusByProNo(CoopProAccInfoDto record) {
        CoopProAccInfo coopProAccInfo = new CoopProAccInfo();
        coopProAccInfo.setProNo(record.getProNo());
        coopProAccInfo.setProStatus(record.getProStatus());
        return coopProAccInfoMapper.updateByPrimaryKeySelective(coopProAccInfo);
    }
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String proNo) {
        return coopProAccInfoMapper.deleteByPrimaryKey(proNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopProAccInfoMapper.deleteByIds(ids);
    }
}
