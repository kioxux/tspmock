/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BizCorreManagerInfo
 * @类描述: biz_corre_manager_info数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-18 20:20:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_corre_manager_info")
public class BizCorreManagerInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务申请编号 **/
	@Column(name = "biz_serno", unique = false, nullable = true, length = 50)
	private String bizSerno;
	
	/** 人员工号 **/
	@Column(name = "corre_manager_id", unique = false, nullable = true, length = 50)
	private String correManagerId;
	
	/** 人员类型 STD_ZB_MGR_TYPE **/
	@Column(name = "corre_mgr_type", unique = false, nullable = true, length = 5)
	private String correMgrType;
	
	/** 所属机构编码 **/
	@Column(name = "corre_manager_br_id", unique = false, nullable = true, length = 50)
	private String correManagerBrId;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param correManagerId
	 */
	public void setCorreManagerId(String correManagerId) {
		this.correManagerId = correManagerId;
	}
	
    /**
     * @return correManagerId
     */
	public String getCorreManagerId() {
		return this.correManagerId;
	}
	
	/**
	 * @param correMgrType
	 */
	public void setCorreMgrType(String correMgrType) {
		this.correMgrType = correMgrType;
	}
	
    /**
     * @return correMgrType
     */
	public String getCorreMgrType() {
		return this.correMgrType;
	}
	
	/**
	 * @param correManagerBrId
	 */
	public void setCorreManagerBrId(String correManagerBrId) {
		this.correManagerBrId = correManagerBrId;
	}
	
    /**
     * @return correManagerBrId
     */
	public String getCorreManagerBrId() {
		return this.correManagerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}