package cn.com.yusys.yusp.web.server.xdxw0059;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0059.req.Xdxw0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0059.resp.Xdxw0059DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0059.Xdxw0059Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据业务唯一编号查询在信贷系统中的抵押率
 *
 * @author zhangpeng
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDXW0059:根据业务唯一编号查询在信贷系统中的抵押率")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0059Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0059Resource.class);
    @Autowired
    private Xdxw0059Service xdxw0059Service;

    /**
     * 交易码：xdxw0059
     * 交易描述：根据业务唯一编号查询在信贷系统中的抵押率
     *
     * @param xdxw0059DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据业务唯一编号查询在信贷系统中的抵押率")
    @PostMapping("/xdxw0059")
    protected @ResponseBody
    ResultDto<Xdxw0059DataRespDto> xdxw0059(@Validated @RequestBody Xdxw0059DataReqDto xdxw0059DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059DataReqDto));
        Xdxw0059DataRespDto xdxw0059DataRespDto = new Xdxw0059DataRespDto();// 响应Dto:根据业务唯一编号查询在信贷系统中的抵押率
        ResultDto<Xdxw0059DataRespDto> xdxw0059DataResultDto = new ResultDto<>();

        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059DataReqDto));
            xdxw0059DataRespDto = xdxw0059Service.xdxw0059(xdxw0059DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059DataRespDto));

            // 封装xdxw0059DataResultDto中正确的返回码和返回信息
            xdxw0059DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0059DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, e.getMessage());
            // 封装xdxw0059DataResultDto中异常返回码和返回信息
            xdxw0059DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0059DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0059DataRespDto到xdxw0059DataResultDto中
        xdxw0059DataResultDto.setData(xdxw0059DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059DataResultDto));
        return xdxw0059DataResultDto;
    }
}
