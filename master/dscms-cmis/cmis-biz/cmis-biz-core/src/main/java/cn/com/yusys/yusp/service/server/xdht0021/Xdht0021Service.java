package cn.com.yusys.yusp.service.server.xdht0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009RespDto;
import cn.com.yusys.yusp.dto.server.xdht0021.req.Xdht0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0021.resp.Xdht0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.Dscms2XwywglptClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.LmtCrdReplyInfoService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 接口处理类:合同签订
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdht0021Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0021Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;

    @Autowired
    private SenddxService senddxService;

    @Autowired
    private Dscms2XwywglptClientService dscms2XwywglptClientService;

    @Autowired
    private AccLoanMapper accLoanMapper;

    private static final int INT6 = 6;

    private static final int INT14 = 14;

    private static final int INT600000 = 600000;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    MessageCommonService messageCommonService;

    public Xdht0021Service() {
    }

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    /**
     * 合同签订
     *
     * @param xdht0021DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0021DataRespDto signCont(Xdht0021DataReqDto xdht0021DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataReqDto));
        Xdht0021DataRespDto xdht0021DataRespDto = new Xdht0021DataRespDto();
        String contNo = xdht0021DataReqDto.getContNo();
        String dealType = xdht0021DataReqDto.getDealType();
        String turnover = xdht0021DataReqDto.getTurnover();
        String loanEndDate = StringUtils.EMPTY;

        try {
            if (Objects.equals("1", dealType)) {//签订
                //1、获取合同信息
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.queryCtrLoanContInfoByContNo(contNo);
                logger.info("************XHT0021*第一步合同编号【{}】查询合同信息结束,返回信息【{}】", contNo, JSON.toJSONString(ctrLoanCont));
                String prd_id = ctrLoanCont.getPrdId();//产品编号
                String cusId = ctrLoanCont.getCusId();//客户号

                loanEndDate = ctrLoanCont.getContEndDate();
                if(Objects.nonNull(ctrLoanCont) && !StringUtils.isBlank(ctrLoanCont.getReplyNo())){
                    LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(ctrLoanCont.getReplyNo());
                    logger.info("************XHT0021*批复编号【{}】查询合同信息结束,返回信息【{}】", ctrLoanCont.getReplyNo(), JSON.toJSONString(lmtCrdReplyInfo));
                    loanEndDate = lmtCrdReplyInfo.getReplyEndDate();
                }
                /****************************/
                if ("S".equals(turnover) || "H".equals(turnover)) {//新增 或 还本续贷
                    BigDecimal loanBalance = accLoanMapper.getLoanBalanceBycusIdforYqd(cusId);
                    if (loanBalance.compareTo(BigDecimal.ZERO) > 0) {//借据余额大于0
                        xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                        xdht0021DataRespDto.setOpMsg("您有未还清的借据，无法签订合同！");
                        return xdht0021DataRespDto;
                    }
                }

                //SC020001优抵贷(线下)、SC020009优抵贷(线上)、SC020010优农贷(线上)
                //2、如果不是上面这三个产品，则校验合同到期日不能大于用户60岁生日！
                if (!"SC020001".equals(prd_id) && !"SC020009".equals(prd_id) && !"SC020010".equals(prd_id)) {

                    if (!"".equals(loanEndDate)) {
                        String certCode = ctrLoanCont.getCertCode();
                        if (!"".equals(certCode)) {
                            certCode = certCode.substring(INT6, INT14); //320582198507091718  -->19850709
                            int birth60 = Integer.parseInt(certCode) + INT600000;//19850709 -->20450709
                            int end = Integer.parseInt(loanEndDate.replaceAll("-", ""));//2016-10-17  -->20161017
                            if (birth60 < end) {
                                xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                                xdht0021DataRespDto.setOpMsg("合同到期日不能大于用户60岁生日！");
                                return xdht0021DataRespDto;
                            }
                        }
                    }
                }

                //3、更新iqp_loan_app审批状态为997
                int iqpFlag = iqpLoanAppMapper.updateByContNo(contNo);
                logger.info("************XHT0021*第二步合同编号【{}】更新iqp_loan_app审批状态为997结束,返回信息【{}】", contNo, JSON.toJSONString(iqpFlag));

                //4、查询当前日期
                String openDay = stringRedisTemplate.opsForValue().get("openDay");

                int contFlag;
                //5、如果是PW01000优享贷,更新合同状态、签订日期、合同起始日（今天）、合同到期日（今天+3年）、还款方式
                //并将影像值更新到表中:credit_image_no 个人征信授权影像流水，BIG_DATA_CREDIT_NO	大数据个人信用信息授权影像流水，cont_image_no 合同影像流水
                if ("PW010004".equals(prd_id)) {
                    SimpleDateFormat sdfymd = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar signDate = Calendar.getInstance();
                    String loanEnd = "";
                    signDate.setTime(sdfymd.parse(openDay));
                    signDate.add(Calendar.YEAR, +3);
                    signDate.add(Calendar.DAY_OF_MONTH, -1);
                    loanEnd = sdfymd.format(signDate.getTime());
                    ctrLoanCont.setContStatus("200");
                    ctrLoanCont.setContStartDate(openDay);
                    ctrLoanCont.setContEndDate(loanEnd);
                    ctrLoanCont.setSignDate(openDay);
                    String repayType = xdht0021DataReqDto.getRepayType();
                    ctrLoanCont.setRepayMode(repayType);
                    contFlag = ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
                    logger.info("************XHT0021*处理PW01000优享贷返回信息【{}】", contFlag);
                    //TODO 三个影像流水不知道放在哪
                    String bigDataCreditNo = xdht0021DataReqDto.getBigDataCreditNo();
                    String creditImageNo = xdht0021DataReqDto.getCreditImageNo();
                    String loanDirectionNo = xdht0021DataReqDto.getLoanDirectionNo();

                    //6、如果是增享贷,更新合同状态、签订日期、合同起始日（今天）
                    //并将影像值更新到表中:loan_direction_no 贷款用途说明书影像流水
                } else if ("SC010014".equals(prd_id)) {
                    ctrLoanCont.setContStatus("200");
                    ctrLoanCont.setContStartDate(openDay);
                    ctrLoanCont.setSignDate(openDay);
                    contFlag = ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
                    logger.info("************XHT0021*处理SC010014增享贷返回信息【{}】", contFlag);
                    //TODO 1个影像流水不知道放在哪
                    String contImageNo = xdht0021DataReqDto.getContImageNo();

                    //7、其他贷款则只更新合同状态、签订日期
                } else {
                    Map queryMap = new HashMap();
                    queryMap.put("contNo", contNo);
                    queryMap.put("openday", openDay);
                    contFlag = ctrLoanContMapper.updateByContNo(queryMap);
                    logger.info("************XHT0021*处理其他产品返回信息【{}】", contFlag);
                }

                //8、如果是优企贷推送合同信息到微银
                if ("SC010008".equals(prd_id) && iqpFlag > 0 && contFlag > 0) {
                    Wxd009ReqDto wxd009ReqDto = ctrLoanContMapper.getWxd009RedDto(contNo);
                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, JSON.toJSONString(wxd009ReqDto));
                    ResultDto<Wxd009RespDto> wxd009RespDtoResultDto = dscms2XwywglptClientService.wxd009(wxd009ReqDto);
                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, JSON.toJSONString(wxd009RespDtoResultDto));
                    if (Objects.equals(EpbEnum.EPB099999.key, wxd009RespDtoResultDto.getCode())) {
                        //推送失败,还原合同状态-未签订
                        iqpFlag = ctrLoanContMapper.updateforContStatusByContNo(contNo);
                        logger.info("************XHT0021*推送合同信息到微银失败还原合同状态-未签订100结束,返回信息【{}】", JSON.toJSONString(iqpFlag));
                        xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                        xdht0021DataRespDto.setOpMsg("推送合同信息到微银失败");
                        return xdht0021DataRespDto;
                    }
                }

                //9、如果有号码就给客户发短信
                //查询客户电话
                CusIndivContactDto cusIndivContactDto = Optional.ofNullable(iCusClientService.queryCusIndivByCusId(ctrLoanCont.getCusId())).orElse(new CusIndivContactDto());
                String phone = cusIndivContactDto.getMobileNo(); //客户电话
                String prdName = ctrLoanCont.getPrdName();
                String cusName = ctrLoanCont.getCusName();
                String managerId = ctrLoanCont.getManagerId();
                if (StringUtils.isNotBlank(phone)) {
                    logger.info("***********************************发送短信***********************************");
                    String messageType = "MSG_XW_C_0012";// 短信编号
                    String receivedUserType = "2";//接收人员类型 1--客户经理 2--借款人
                    Map paramMap = new HashMap();//短信填充参数
                    paramMap.put("cusName", cusName);
                    paramMap.put("prdName", prdName);
                    //执行发送借款人操作
                    ResultDto<Integer> resultDto1 = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, phone);
                    //执行发送客户经理操作
                    String messageType2 = "MSG_XW_M_0012";// 短信编号
                    String receivedUserType2 = "1";//接收人员类型 1--客户经理 2--借款人
                    ResultDto<Integer> resultDto2 = messageCommonService.sendMessage(messageType2, paramMap, receivedUserType2, managerId, "");
                }

                // 合同校验接口开设
                logger.info("************XDHT0021*发送额度系统cmislmt0009合同校验接口开始,校验信息【{}】", JSON.toJSONString(ctrLoanCont));
                ResultDto cmisLmt0009RespDto = cmisBizXwCommonService.contLmtForXw(ctrLoanCont);
                // CmisLmt0009RespDto cmisLmt0009RespDto = resultLmt0009Dto.getData();
                logger.info("************XDHT0021*发送额度系统cmislmt0009合同校验接口结束,返回信息【{}】", JSON.toJSONString(cmisLmt0009RespDto));

            } else if (Objects.equals("2", dealType)) {//撤销
                //先检测有没有未还清的借据
                BigDecimal loanBalance = accLoanMapper.getLoanBalanceByContNo(contNo);
                if (BigDecimal.ZERO.compareTo(loanBalance) < 0) {
                    xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdht0021DataRespDto.setOpMsg("您有未还清的借据，无法撤销合同！");
                    return xdht0021DataRespDto;
                } else {
                    //更新ctr_loan_cont
                    ctrLoanContMapper.setContStatus300ByContNo(contNo);
                }
            }
            xdht0021DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdht0021DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, e.getMessage());
            xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdht0021DataRespDto.setOpMsg(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, e.getMessage());
            xdht0021DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdht0021DataRespDto.setOpMsg("系统异常");
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataRespDto));
        return xdht0021DataRespDto;
    }
}
