package cn.com.yusys.yusp.service.server.xddb0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0022.req.Xddb0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0022.resp.Xddb0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 接口处理类:根据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Service
public class Xddb0022Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0022Service.class);

    @Resource
    private GuarWarrantInfoService guarWarrantInfoService;


    /**
     * 根据押品编号查询核心及信贷系统有无押品数据
     *
     * @param xddb0022DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0022DataRespDto xddb0022(Xddb0022DataReqDto xddb0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value);
        Xddb0022DataRespDto xddb0022DataRespDto = new Xddb0022DataRespDto();
        String flag = "0"; //有无押品数据: 默认无

        String bookSerno = xddb0022DataReqDto.getBook_serno();//核心押品编号
        int count = guarWarrantInfoService.selectIsExistByCoreGuarantyNo(bookSerno);

        if (count>0){
            flag = "1";
        }

        xddb0022DataRespDto.setFlag(flag);

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value);
        return xddb0022DataRespDto;
    }
}
