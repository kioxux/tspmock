/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PrintRecord;
import cn.com.yusys.yusp.repository.mapper.PrintRecordMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrintRecordService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-13 13:50:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PrintRecordService {
    private Logger logger = LoggerFactory.getLogger(PrintRecordService.class);

    @Autowired
    private PrintRecordMapper printRecordMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PrintRecord selectByPrimaryKey(String batchNo, String bizNo) {
        return printRecordMapper.selectByPrimaryKey(batchNo, bizNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PrintRecord> selectAll(QueryModel model) {
        List<PrintRecord> records = (List<PrintRecord>) printRecordMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PrintRecord> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PrintRecord> list = printRecordMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PrintRecord record) {
        return printRecordMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PrintRecord record) {
        return printRecordMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PrintRecord record) {
        return printRecordMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PrintRecord record) {
        return printRecordMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String batchNo, String bizNo) {
        return printRecordMapper.deleteByPrimaryKey(batchNo, bizNo);
    }

    /**
     * @param printRecord
     * @return int
     * @author 王玉坤
     * @date 2021/10/13 23:38
     * @version 1.0.0
     * @desc  保存合同打印信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int addPrintRecord(PrintRecord printRecord) {
        logger.info("合同号【{}】，保存合同打印记录信息开始", printRecord.getBizNo());
        // 获取流水号信息
        String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_PC_SEQ, new HashMap<>());
        // 批次号
        printRecord.setBatchNo(pkId);
        // 打印时间
        printRecord.setPrintTime(DateUtils.getCurrDateTimeStr());
        // 创建时间
        printRecord.setCreatTime(DateUtils.getCurrDate());
        // 更新时间
        printRecord.setUpdateTime(DateUtils.getCurrDate());

        return this.insert(printRecord);
    }
}
