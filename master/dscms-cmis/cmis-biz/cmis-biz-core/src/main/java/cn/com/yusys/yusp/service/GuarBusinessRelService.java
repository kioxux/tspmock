package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.BusinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfReqDto;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.service.client.bsp.ypxt.buscon.BusconService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.businf.BusinfService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.coninf.ConinfService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.contra.ContraService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.lmtinf.LmtinfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/2823:48
 * @desc 授信、合同与押品系统同步处理类
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class GuarBusinessRelService {

    private static final Logger logger = LoggerFactory.getLogger(GuarBusinessRelService.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CtrDiscContService ctrDiscContService;

    @Autowired
    private CtrCvrgContService ctrCvrgContService;

    @Autowired
    private CtrTfLocContService ctrTfLocContService;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private LmtinfService lmtinfService;

    @Autowired
    private BusinfService businfService;

    @Autowired
    private ConinfService coninfService;

    @Autowired
    private BusconService busconService;

    @Autowired
    private ContraService contraService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    /**
     * @param belgLine, serNo
     * @return void
     * @author 王玉坤
     * @date 2021/6/28 23:56
     * @version 1.0.0
     * @desc 信贷授信协议信息同步
     * belgLine[所属条线], serNo【业务流水号】
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendLmtinf(String belgLine, String serNo) {
        logger.info("根据流水号【{}】同步授信信息至押品系统开始，所属业务条线【{}】", serNo, belgLine);

        // 授信协议请求对象
        LmtinfReqDto lmtinfReqDto = null;
        // 请求对象循环体
        List list = null;
        // 请求单体对象
        cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.List listReq = null;
        // 零售业务信息
        IqpLoanApp iqpLoanApp = null;
        try {
            // 初始化对象
            list = new ArrayList();
            listReq = new cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.List();
            lmtinfReqDto = new LmtinfReqDto();
            // 根据所属业务条线做不同逻辑处理
            if ("02".equals(belgLine)) {// 零售业务条线
                // 根据业务流水号查询业务申请信息
                iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serNo);

                if (Objects.nonNull(iqpLoanApp)) {
                    // 授信协议号
                    listReq.setSxxybh(serNo);
                    // 授信客户码
                    listReq.setSxkhbh(iqpLoanApp.getCusId());
                    // 授信金额
                    listReq.setSxjeyp(iqpLoanApp.getAppAmt());
                    // 授信币种
                    listReq.setSxbzyp(iqpLoanApp.getCurType());
                    // 授信起始日期
                    listReq.setSxqsrq(iqpLoanApp.getStartDate());
                    // 授信到期日期
                    listReq.setSxdqrq(iqpLoanApp.getEndDate());
                    // 授信协议状态 1-正常；2-冻结；3-解冻；4-终止
                    listReq.setSxxyzt("1");
                    // 管户客户经理
                    listReq.setKhghjl(iqpLoanApp.getManagerId());
                    // 管护机构
                    listReq.setKhghjg(iqpLoanApp.getManagerBrId());
                } else {
                    throw BizException.error(null, null, "未查询到业务申请信息！");
                }
            } else if ("03".equals(belgLine)) {// 对公业务条线
                // 根据业务流水号查询授信协议信息
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectBySerno(serNo);

                if (Objects.nonNull(lmtReplyAcc)) {
                    // 授信协议号
                    listReq.setSxxybh(serNo);
                    // 授信客户码
                    listReq.setSxkhbh(lmtReplyAcc.getCusId());
                    // 授信金额
                    listReq.setSxjeyp(new BigDecimal("0.0").add(lmtReplyAcc.getLowRiskTotalLmtAmt()).add(lmtReplyAcc.getOpenTotalLmtAmt()));
                    // 授信币种
                    listReq.setSxbzyp(lmtReplyAcc.getCurType());
                    // 授信起始日期
                    listReq.setSxqsrq(lmtReplyAcc.getInureDate());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = com.alibaba.excel.util.DateUtils.parseDate(lmtReplyAcc.getInureDate());
                    // 授信到期日期
                    listReq.setSxdqrq(simpleDateFormat.format(DateUtils.addMonth(date, lmtReplyAcc.getLmtTerm())));
                    // 授信协议状态 1-正常；2-冻结；3-解冻；4-终止
                    listReq.setSxxyzt("1");
                    // 管户客户经理
                    listReq.setKhghjl(lmtReplyAcc.getManagerId());
                    // 管护机构
                    listReq.setKhghjg(lmtReplyAcc.getManagerBrId());
                } else {
                    throw BizException.error(null, null, "未查询到授信台账申请信息！");
                }
            }
            list.add(listReq);
            lmtinfReqDto.setList(list);

            // 调用押品同步接口
            lmtinfService.lmtinf(lmtinfReqDto);
        } catch (Exception e) {
            logger.error("同步授信协议至押品系统异常！业务编号【" + serNo + "】", e);
        }
        logger.info("根据流水号【{}】同步授信信息至押品系统结束，所属业务条线【{}】", serNo, belgLine);
    }

    /**
     * @param belgLine, bizType, contNo
     * @return void
     * @author 王玉坤
     * @date 2021/6/29 0:50
     * @version 1.0.0
     * @desc 信贷业务合同信息同步
     * belgLine【所属条线】, bizType【合同申请类型】, contNo【合同号】
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendBusinf(String belgLine, String bizType, String contNo) {
        logger.info("根据合同号【{}】同步合同信息至押品系统开始，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
        // 合同信息请求对象
        BusinfReqDto businfReqDto = new BusinfReqDto();
        // 请求对象循环体
        List list = new ArrayList();
        // 请求单体对象
        cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.List businfList = new cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.List();

        String ywhtbh = "";//业务合同编号
        String sxxybh = "";
        ;//授信协议号
        String jkrkhh = "";
        ;//借款人客户码
        BigDecimal ywhtje = new BigDecimal("0.0");//业务合同金额
        String ywhtbz = "";//业务合同币种
        BigDecimal htyeyp = new BigDecimal("0.0");//合同余额
        BigDecimal ywck = new BigDecimal("0.0");//业务敞口
        String ywpzyp = "";//业务品种
        String dbfsyp = "";//担保方式
        BigDecimal bzjbly = new BigDecimal("0.0");//保证金比例
        String ksrqyp = "";//开始日期
        String dqrqyp = "";//到期日期
        String ywhtzt = "";//业务合同状态
        String khghjl = "";//管户客户经理
        String khghjg = "";//管理机构

        /*01最高额授信协议 02普通贷款合同 03贴现协议 04贸易融资合同 05福费廷合同 06开证合同 07银承合同 08保函合同 09委托贷款合同 10资产池协议*/
        try {
            if (CmisCommonConstants.STD_BUSI_TYPE_01.equals(bizType)) { // 最高额授信协议
                CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
                if (!Objects.nonNull(ctrHighAmtAgrCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrHighAmtAgrCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrHighAmtAgrCont.getCusId();
                // 业务合同金额
                ywhtje = ctrHighAmtAgrCont.getAgrAmt();
                // 业务合同币种
                ywhtbz = ctrHighAmtAgrCont.getAgrType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrHighAmtAgrCont.getAgrAmt();
                // 业务敞口
                ywck = BigDecimal.ZERO;
                // 业务品种
                ywpzyp = "";
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrHighAmtAgrCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrHighAmtAgrCont.getStartDate();
                // 到期日期
                dqrqyp = ctrHighAmtAgrCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrHighAmtAgrCont.getContStatus();
                // 管户客户经理
                khghjl = ctrHighAmtAgrCont.getManagerId();
                // 管理机构
                khghjg = ctrHighAmtAgrCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_02.equals(bizType)
                    || CmisCommonConstants.STD_BUSI_TYPE_04.equals(bizType)
                    || CmisCommonConstants.STD_BUSI_TYPE_05.equals(bizType)) { // 02普通贷款合同 04贸易融资合同 05福费廷合同
                // 根据合同编号查询合同信息
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                if (!Objects.nonNull(ctrLoanCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                if ("02".equals(belgLine)) { // 零售条线
                    sxxybh = ctrLoanCont.getIqpSerno();
                } else if ("01".equals(belgLine)) { // 小微条线
                    sxxybh = ctrLoanCont.getSurveySerno();
                } else if ("03".equals(belgLine)) { // 对公条线
                    sxxybh = ctrLoanCont.getLmtAccNo();
                }
                // 借款人客户码
                jkrkhh = ctrLoanCont.getCusId();
                // 业务合同金额
                ywhtje = ctrLoanCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrLoanCont.getCurType();
                // 合同余额 TODO 待优化取数逻辑
                htyeyp = ctrLoanCont.getContBalance() == null ? new BigDecimal("0") : ctrLoanCont.getContBalance();
                // 业务敞口
                ywck = BigDecimal.ZERO;
                // 业务品种
                ywpzyp = ctrLoanCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrLoanCont.getGuarWay());
                //保证金比例
                bzjbly = BigDecimal.ZERO;
                // 开始日期
                ksrqyp = ctrLoanCont.getContStartDate();
                // 到期日期
                dqrqyp = ctrLoanCont.getContEndDate();
                // 业务合同状态
                ywhtzt = ctrLoanCont.getContStatus();
                //信贷码值：
                // 100 未生效
                // 200 生效
                // 500 中止
                // 600 注销
                // 700 撤回
                // 800作废
                //押品码值：0-作废；1-正常；2-已结清
                if ("200".equals(ywhtzt)) {
                    ywhtzt = "1";
                } else if ("600".equals(ywhtzt) || "800".equals(ywhtzt)) {
                    ywhtzt = "0";
                }

                // 管户客户经理
                khghjl = ctrLoanCont.getManagerId();
                // 管理机构
                khghjg = ctrLoanCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_03.equals(bizType)) { // 03贴现协议
                CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(contNo);
                if (!Objects.nonNull(ctrDiscCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrDiscCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrDiscCont.getCusId();
                // 业务合同金额
                ywhtje = ctrDiscCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrDiscCont.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrDiscCont.getContAmt();
                // 业务敞口
                ywck = ctrDiscCont.getContAmt();
                // 业务品种
                ywpzyp = ctrDiscCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrDiscCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrDiscCont.getStartDate();
                // 到期日期
                dqrqyp = ctrDiscCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrDiscCont.getContStatus();
                // 管户客户经理
                khghjl = ctrDiscCont.getManagerId();
                // 管理机构
                khghjg = ctrDiscCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_06.equals(bizType)) { // 06开证合同
                CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByContNo(contNo);
                if (!Objects.nonNull(ctrTfLocCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrTfLocCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrTfLocCont.getCusId();
                // 业务合同金额
                ywhtje = ctrTfLocCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrTfLocCont.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrTfLocCont.getContAmt();
                // 业务敞口
                ywck = ctrTfLocCont.getContAmt();
                // 业务品种
                ywpzyp = ctrTfLocCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrTfLocCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrTfLocCont.getStartDate();
                // 到期日期
                dqrqyp = ctrTfLocCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrTfLocCont.getContStatus();
                // 管户客户经理
                khghjl = ctrTfLocCont.getManagerId();
                // 管理机构
                khghjg = ctrTfLocCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_07.equals(bizType)) { // 07银承合同
                CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(contNo);
                if (!Objects.nonNull(ctrAccpCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrAccpCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrAccpCont.getCusId();
                // 业务合同金额
                ywhtje = ctrAccpCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrAccpCont.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrAccpCont.getContAmt();
                // 业务敞口
                ywck = ctrAccpCont.getContAmt();
                // 业务品种
                ywpzyp = ctrAccpCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrAccpCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrAccpCont.getStartDate();
                // 到期日期
                dqrqyp = ctrAccpCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrAccpCont.getContStatus();
                // 管户客户经理
                khghjl = ctrAccpCont.getManagerId();
                // 管理机构
                khghjg = ctrAccpCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_08.equals(bizType)) { // 08保函合同
                CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByContNo(contNo);
                if (!Objects.nonNull(ctrCvrgCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrCvrgCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrCvrgCont.getCusId();
                // 业务合同金额
                ywhtje = ctrCvrgCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrCvrgCont.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrCvrgCont.getContAmt();
                // 业务敞口
                ywck = ctrCvrgCont.getContAmt();
                // 业务品种
                ywpzyp = ctrCvrgCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrCvrgCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrCvrgCont.getStartDate();
                // 到期日期
                dqrqyp = ctrCvrgCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrCvrgCont.getContStatus();
                // 管户客户经理
                khghjl = ctrCvrgCont.getManagerId();
                // 管理机构
                khghjg = ctrCvrgCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_09.equals(bizType)) { // 09委托贷款合同
                CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(contNo);
                if (!Objects.nonNull(ctrEntrustLoanCont)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrEntrustLoanCont.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrEntrustLoanCont.getCusId();
                // 业务合同金额
                ywhtje = ctrEntrustLoanCont.getContAmt();
                // 业务合同币种
                ywhtbz = ctrEntrustLoanCont.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrEntrustLoanCont.getContAmt();
                // 业务敞口
                ywck = ctrEntrustLoanCont.getContAmt();
                // 业务品种
                ywpzyp = ctrEntrustLoanCont.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrEntrustLoanCont.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrEntrustLoanCont.getStartDate();
                // 到期日期
                dqrqyp = ctrEntrustLoanCont.getEndDate();
                // 业务合同状态
                ywhtzt = ctrEntrustLoanCont.getContStatus();
                // 管户客户经理
                khghjl = ctrEntrustLoanCont.getManagerId();
                // 管理机构
                khghjg = ctrEntrustLoanCont.getManagerBrId();
            } else if (CmisCommonConstants.STD_BUSI_TYPE_10.equals(bizType)) { // 10资产池协议
                // 获取资产池协议
                CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
                if (!Objects.nonNull(ctrAsplDetails)) {
                    logger.info("根据合同号【{}】未查询到合同信息，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
                    throw BizException.error(null, null, "未查询到合同申请信息！");
                }
                //业务合同编号
                ywhtbh = contNo;
                //授信协议号
                sxxybh = ctrAsplDetails.getLmtAccNo();
                //借款人客户码
                jkrkhh = ctrAsplDetails.getCusId();
                // 业务合同金额
                ywhtje = ctrAsplDetails.getContAmt();
                // 业务合同币种
                ywhtbz = ctrAsplDetails.getCurType();
                // 合同余额  TODO 待优化取数逻辑
                htyeyp = ctrAsplDetails.getContAmt();
                // 业务敞口
                ywck = ctrAsplDetails.getContAmt();
                // 业务品种
                ywpzyp = ctrAsplDetails.getPrdId();
                // 担保方式
                dbfsyp = DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + ctrAsplDetails.getGuarMode());
                //保证金比例
                bzjbly = new BigDecimal("0.0");
                ;
                // 开始日期
                ksrqyp = ctrAsplDetails.getStartDate();
                // 到期日期
                dqrqyp = ctrAsplDetails.getEndDate();
                // 业务合同状态
                ywhtzt = ctrAsplDetails.getContStatus();
                // 管户客户经理
                khghjl = ctrAsplDetails.getManagerId();
                // 管理机构
                khghjg = ctrAsplDetails.getManagerBrId();
            }
            // 组装报文
            businfList.setYwhtbh(ywhtbh);
            businfList.setJkrkhh(jkrkhh);
            businfList.setSxxybh(sxxybh);
            businfList.setJkrkhh(jkrkhh);
            businfList.setYwhtje(ywhtje);
            businfList.setYwhtbz(ywhtbz);
            businfList.setHtyeyp(htyeyp);
            businfList.setYwck(ywck);
            businfList.setYwpzyp(ywpzyp);
            businfList.setDbfsyp(dbfsyp);
            businfList.setBzjbly(bzjbly);
            businfList.setKsrqyp(ksrqyp);
            businfList.setDqrqyp(dqrqyp);
            businfList.setYwhtzt(ywhtzt);
            businfList.setKhghjl(khghjl);
            businfList.setKhghjg(khghjg);
            list.add(businfList);
            businfReqDto.setList(list);
            // 调用押品接口
            businfService.businf(businfReqDto);
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            //throw e;
        } catch (Exception e) {
            logger.error("同步合同至押品系统异常！合同编号【{}】", contNo, e.getMessage(), e);
            //throw e;
        }
        logger.info("根据合同号【{}】同步合同信息至押品系统结束，所属业务条线【{}】，申请合同类型【{}】", contNo, belgLine, bizType);
    }

    /**
     * @param belgLine, bizType, contNo
     * @return void
     * @author 王玉坤
     * @date 2021/6/29 0:50
     * @version 1.0.0
     * @desc 信贷担保合同信息同步
     * belgLine【所属条线】 01--小微, bizType【合同申请类型】, contNo【合同号】
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendConinf(String belgLine, String guarContNo) {
        logger.info("根据担保合同号【{}】同步合同信息至押品系统结束，所属业务条线【{}】", guarContNo, belgLine);

        // 合同信息请求对象
        ConinfReqDto cusinfReqDto = new ConinfReqDto();
        // 请求对象循环体
        List list = new ArrayList();
        // 请求单体对象
        ConinfListInfo coninfListInfo = new ConinfListInfo();

        // 担保合同信息
        GrtGuarCont grtGuarCont = null;

        try {
            // 根据担保合同编号查询担保合同信息
            grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
            if (!Objects.nonNull(grtGuarCont)) {
                logger.info("根据担保合同号【{}】未查询到合同信息，所属业务条线【{}】", guarContNo, belgLine);
                throw BizException.error(null, null, "未查询到担保合同申请信息！");
            }

            // 拼装请求报文
            // 业务系统 01-信贷；02-小微
            coninfListInfo.setYwxtyp("01".equals(belgLine) ? "02" : "01");
            // 合同编号
            coninfListInfo.setHtbhyp(grtGuarCont.getGuarContNo());
            // 合同类型 押品字典项 010-一般担保；020-最高额担保 信贷字典项 A--一般 B--最高额
            coninfListInfo.setHtlxyp("A".equals(grtGuarCont.getGuarContType()) ? "010" : "020");
            // 核心担保编号 信贷生成YP编号 TODO
            coninfListInfo.setSernoy("");
            // 担保方式
            coninfListInfo.setDbfsyp(DicTranEnum.lookup("GUAR_MODE_XDTOYP_" + grtGuarCont.getGuarWay()));
            // 合同签订起始日期
            coninfListInfo.setHtqsrq(grtGuarCont.getGuarStartDate());
            // 合同签订到期日期
            coninfListInfo.setHtjsrq(grtGuarCont.getGuarEndDate());
            // 担保合同状态 010-未签合同；020-已签合同；030-已失效
//            100	未生效
//            101	生效
//            104	注销
//            107	作废
            String guarContState = grtGuarCont.getGuarContState();
            if("104".equals(guarContState) || "107".equals(guarContState)){
                coninfListInfo.setDbhtzt("030");
            }else {
                coninfListInfo.setDbhtzt("020");
            }
            // 担保合同金额
            coninfListInfo.setDbhtje(grtGuarCont.getGuarAmt());
            // 币种
            coninfListInfo.setBzypxt(grtGuarCont.getCurType());
            // 担保合同对应融资余额
            coninfListInfo.setDbrzye("0");
            // 管户客户经理
            coninfListInfo.setKhghjl(grtGuarCont.getManagerId());
            // 管护机构
            coninfListInfo.setKhghjg(grtGuarCont.getManagerBrId());

            list.add(coninfListInfo);
            cusinfReqDto.setConinfListInfo(list);

            // 调用担保合同同步信息接口
            coninfService.coninf(cusinfReqDto);
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            //throw e;
        } catch (Exception e) {
            logger.error("同步合同至押品系统异常！担保合同编号【{}】", guarContNo, e.getMessage(), e);
            //throw e;
        }
        logger.info("根据合同号【{}】同步合同信息至押品系统结束，所属业务条线【{}】", guarContNo, belgLine);
    }

    /**
     * @param serno, bizType
     * @return void
     * @author 王玉坤
     * @date 2021/7/6 18:43
     * @version 1.0.0
     * @desc 根据业务流水号同步押品与业务关联信息
     * serno【业务流水号】, bizType【业务类型：01--授信，02--业务】
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendBuscon(String serno, String bizType) {
        logger.info("根据业务流水号【{}】同步业务与押品关系至押品系统开始，所属业务类型【{}】", serno, bizType);

        // 请求对象信息
        BusconReqDto busconReqDto = new BusconReqDto();
        // 循环体
        List list = new ArrayList();

        try {
            // 根据业务流水号查询押品列表信息
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("serno", serno);
            List<GuarBizRel> guarBizRelList = guarBizRelService.selectAll(queryModel);

            if (CollectionUtils.isEmpty(guarBizRelList)) {
                logger.info("根据业务流水号【{}】未查询到押品信息，所属业务类型【{}】", serno, bizType);
                throw BizException.error(null, null, "未查询到押品信息！");
            }

            // 拼装请求报文
            guarBizRelList.stream().forEach(s -> {
                // 单循环体
                cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List busconList = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                // 业务编号
                busconList.setYwbhyp(serno);
                // 业务类型
                busconList.setYwlxyp(bizType);
                // 押品统一编号
                busconList.setYptybh(s.getGuarNo());
                // 是否有效 1-有效；0-无效
                busconList.setIsflag("1");
                list.add(busconList);
            });
            busconReqDto.setList(list);

            // 请求押品系统
            busconService.buscon(busconReqDto);
        } catch (BizException e) {
            logger.error("同步业务与押品关系至押品系统异常！业务流水号【{}】", serno, e.getMessage(), e);
            //throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //throw e;
        }
        logger.info("根据业务流水号【{}】同步业务与押品关系至押品系统结束，所属业务类型【{}】", serno, bizType);
    }

    /**
     * @param serno, bizType
     * @return void
     * @author 王浩
     * @date 2021/7/6 18:43
     * @desc 因为某种历史遗留bug 导致这里要拉出来单写
     * serno【业务流水号】, bizType【业务类型：01--授信，02--业务】
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendBusconXw(String serno, String bizType) {
        logger.info("根据业务流水号【{}】同步业务与押品关系至押品系统开始，所属业务类型【{}】", serno, bizType);

        // 请求对象信息
        BusconReqDto busconReqDto = new BusconReqDto();
        // 循环体
        List list = new ArrayList();

        try {
            // 根据业务流水号查询押品列表信息
            List<GrtGuarContRel> guarBizRelList = grtGuarContRelService.selectForXw(serno);

            if (CollectionUtils.isEmpty(guarBizRelList)) {
                logger.info("根据业务流水号【{}】未查询到押品信息，所属业务类型【{}】", serno, bizType);
                throw BizException.error(null, null, "未查询到押品信息！");
            }

            // 拼装请求报文
            guarBizRelList.stream().forEach(s -> {
                // 单循环体
                cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List busconList = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                // 业务编号
                busconList.setYwbhyp(serno);
                // 业务类型
                busconList.setYwlxyp(bizType);
                // 押品统一编号
                busconList.setYptybh(s.getGuarNo());
                // 是否有效 1-有效；0-无效
                busconList.setIsflag("1");
                list.add(busconList);
            });
            busconReqDto.setList(list);

            // 请求押品系统
            busconService.buscon(busconReqDto);
        } catch (BizException e) {
            logger.error("同步业务与押品关系至押品系统异常！业务流水号【{}】", serno, e.getMessage(), e);
            //throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //throw e;
        }
        logger.info("根据业务流水号【{}】同步业务与押品关系至押品系统结束，所属业务类型【{}】", serno, bizType);
    }

    /**
     * @param guarContNo
     * @return void
     * @author 王玉坤
     * @date 2021/7/6 19:26
     * @version 1.0.0
     * @desc 根据担保合同编号同步担保合同与押品关系
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendContra(String guarContNo) {
        logger.info("根据担保合同号【{}】同步担保合同与押品关系至押品系统开始", guarContNo);

        // 请求体信息
        ContraReqDto contraReqDto = new ContraReqDto();
        // 循环体信息
        List list = new ArrayList();

        try {
            // 查询担保合同信息
            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
            if (Objects.isNull(grtGuarCont)) {
                logger.info("根据担保合同编号【{}】未查询到担保合同信息", guarContNo);
                throw BizException.error(null, null, "未查询到担保合同信息！");
            }
            // 根据担保合同编号查询担保合同下押品信息
            List<GrtGuarContRel> grtGuarContRels = grtGuarContRelService.getByGuarContNo(guarContNo);

            if (CollectionUtils.isEmpty(grtGuarContRels)) {
                logger.info("根据担保合同编号【{}】未查询到押品信息", guarContNo);
                throw BizException.error(null, null, "未查询到押品信息！");
            }

            // 拼装请求报文
            for (GrtGuarContRel grtGuarContRel : grtGuarContRels) {
                // 单体信息
                ContraListInfo contraListInfo = new ContraListInfo();
                // 担保合同编号
                contraListInfo.setDbhtbh(grtGuarContRel.getGuarContNo());

                // 押品统一编号
                // 查询该押品信息是否是保证人信息
//                GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());

                // 若为保证人取保证人统一编号（押品系统生成）、否则取押品编号
//                if (Objects.isNull(guarGuarantee)) {
//                    contraListInfo.setYptybh(grtGuarContRel.getGuarNo());
//                } else {
//                    contraListInfo.setYptybh(guarGuarantee.getGuarantyIdNew());
//                }
//
//                // 保证人流水号
//                contraListInfo.setSernbz(grtGuarContRel.getGuarNo());
//                // 保证人组编号 TODO 不知道填啥
//                contraListInfo.setBzrzbh(grtGuarContRel.getGuarNo());
                contraListInfo.setYptybh(grtGuarContRel.getGuarNo());

                contraListInfo.setIsflag("1");

                list.add(contraListInfo);
            }
            contraReqDto.setContraListInfo(list);

            // 调用押品系统接口
            contraService.contra(contraReqDto);
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            //throw e;
        } catch (YuspException e) {
            logger.error(e.getMessage(), e);
            //throw e;
        } catch (Exception e) {
            logger.error("同步担保合同与押品关系至押品系统异常！担保合同流失很高【{}】", guarContNo, e.getMessage(), e);
            //throw e;
        }
    }

}
