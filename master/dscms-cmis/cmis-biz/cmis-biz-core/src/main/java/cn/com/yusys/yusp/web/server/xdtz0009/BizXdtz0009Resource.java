package cn.com.yusys.yusp.web.server.xdtz0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0009.req.Xdtz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.resp.Xdtz0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0009.Xdtz0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户经理不良率
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0009:查询客户经理不良率")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0009Resource.class);
    @Autowired
    private Xdtz0009Service xdtz0009Service;

    /**
     * 交易码：xdtz0009
     * 交易描述：查询客户经理不良率
     *
     * @param xdtz0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户经理不良率")
    @PostMapping("/xdtz0009")
    protected @ResponseBody
    ResultDto<Xdtz0009DataRespDto> xdtz0009(@Validated @RequestBody Xdtz0009DataReqDto xdtz0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataReqDto));
        Xdtz0009DataRespDto xdtz0009DataRespDto = new Xdtz0009DataRespDto();// 响应Dto:查询客户经理不良率
        ResultDto<Xdtz0009DataRespDto> xdtz0009DataResultDto = new ResultDto<>();
        // 从xdtz0009DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0009DataReqDto));
            xdtz0009DataRespDto = xdtz0009Service.xdtz0009(xdtz0009DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0009DataRespDto));
            // 封装xdtz0009DataResultDto中正确的返回码和返回信息
            xdtz0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, e.getMessage());
            xdtz0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0009DataRespDto到xdtz0009DataResultDto中
        xdtz0009DataResultDto.setData(xdtz0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataResultDto));
        return xdtz0009DataResultDto;
    }
}
