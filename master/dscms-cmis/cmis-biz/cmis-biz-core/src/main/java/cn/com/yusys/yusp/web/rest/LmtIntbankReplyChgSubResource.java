/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChgSub;
import cn.com.yusys.yusp.service.LmtIntbankReplyChgSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyChgSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 21:18:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankreplychgsub")
public class LmtIntbankReplyChgSubResource {
    @Autowired
    private LmtIntbankReplyChgSubService lmtIntbankReplyChgSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankReplyChgSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankReplyChgSub> list = lmtIntbankReplyChgSubService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankReplyChgSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankReplyChgSub>> index(QueryModel queryModel) {
        List<LmtIntbankReplyChgSub> list = lmtIntbankReplyChgSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankReplyChgSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankReplyChgSub> show(@PathVariable("pkId") String pkId) {
        LmtIntbankReplyChgSub lmtIntbankReplyChgSub = lmtIntbankReplyChgSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankReplyChgSub>(lmtIntbankReplyChgSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankReplyChgSub> create(@RequestBody LmtIntbankReplyChgSub lmtIntbankReplyChgSub) throws URISyntaxException {
        lmtIntbankReplyChgSubService.insert(lmtIntbankReplyChgSub);
        return new ResultDto<LmtIntbankReplyChgSub>(lmtIntbankReplyChgSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankReplyChgSub lmtIntbankReplyChgSub) throws URISyntaxException {
        int result = lmtIntbankReplyChgSubService.update(lmtIntbankReplyChgSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankReplyChgSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankReplyChgSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询
     * @param model
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<List<LmtIntbankReplyChgSub>> selectBySerno(@RequestBody QueryModel model){
        String serno = model.getCondition().get("serno").toString();
        return new ResultDto<List<LmtIntbankReplyChgSub>>(lmtIntbankReplyChgSubService.selectBySerno(serno));
    }

    /**
     * 更改分项批复申请并动态更改授信总额度
     * @param lmtIntbankReplyChgSub
     * @return
     */
    @PostMapping("/updateReplyChgSub")
    protected  ResultDto<Integer> updateReplyChgSub(@RequestBody LmtIntbankReplyChgSub lmtIntbankReplyChgSub){
        System.out.println(lmtIntbankReplyChgSub);
        return new ResultDto<Integer>(lmtIntbankReplyChgSubService.updateReplyChgSub(lmtIntbankReplyChgSub));
    }
}
