package cn.com.yusys.yusp.service.client.bsp.yk.yky001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.Yky001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YkClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author xuwh
 * @version 1.0.0
 * @date 2021/8/18 10:13
 * @desc    印控系统
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Yky001Service {
    private static final Logger logger = LoggerFactory.getLogger(Yky001Service.class);

    // 1）注入：印控系统的接口
    @Autowired
    private Dscms2YkClientService dscms2YkClientService;

    /**
     * @param yky001ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto
     * @author xuwh
     * @date 2021/8/18 10:17
     * @version 1.0.0
     * @desc    发送印控系统用印申请
     */
    @Transactional
    public Yky001RespDto yky001(Yky001ReqDto yky001ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value, JSON.toJSONString(yky001ReqDto));
        ResultDto<Yky001RespDto> yky001ResultDto = dscms2YkClientService.yky001(yky001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value, JSON.toJSONString(yky001ResultDto));

        String yky001Code = Optional.ofNullable(yky001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String yky001Meesage = Optional.ofNullable(yky001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Yky001RespDto yky001RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, yky001ResultDto.getCode())) {
            //  获取相关的值并解析
            yky001RespDto = yky001ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(yky001Code, yky001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value);
        return yky001RespDto;
    }

}
