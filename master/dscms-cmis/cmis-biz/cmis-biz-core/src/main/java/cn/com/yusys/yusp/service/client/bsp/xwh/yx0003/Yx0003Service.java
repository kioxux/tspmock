package cn.com.yusys.yusp.service.client.bsp.xwh.yx0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req.Yx0003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp.Yx0003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2XwhClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：推送优惠券信息至小微公众号
 *
 * @author 王玉坤
 * @version 1.0
 * @since 2021年10月10日 下午1:22:06
 */
@Service
public class Yx0003Service {
    private static final Logger logger = LoggerFactory.getLogger(Yx0003Service.class);

    @Autowired
    private Dscms2XwhClientService dscms2XwhClientService;

    /**
     * @param Yx0003ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.xwh.Yx0003.resp.Yx0003RespDto
     * @author 王玉坤
     * @date 2021/10/8 22:10
     * @version 1.0.0
     * @desc 推送优惠券信息至小微公众号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Yx0003RespDto Yx0003(Yx0003ReqDto Yx0003ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value, JSON.toJSONString(Yx0003ReqDto));
        ResultDto<Yx0003RespDto> Yx0003ResultDto = dscms2XwhClientService.yx0003(Yx0003ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value, JSON.toJSONString(Yx0003ResultDto));
        String Yx0003Code = Optional.ofNullable(Yx0003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String Yx0003Meesage = Optional.ofNullable(Yx0003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Yx0003RespDto Yx0003RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, Yx0003ResultDto.getCode())) {
            //  获取相关的值并解析
            Yx0003RespDto = Yx0003ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, Yx0003Code, Yx0003Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value);
        return Yx0003RespDto;
    }
}
