/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.service.GrtGuarBizRstRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpGuarChgAppRel;
import cn.com.yusys.yusp.service.IqpGuarChgAppRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarChgAppRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-21 14:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpguarchgapprel")
public class IqpGuarChgAppRelResource {
    @Autowired
    private IqpGuarChgAppRelService iqpGuarChgAppRelService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpGuarChgAppRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpGuarChgAppRel> list = iqpGuarChgAppRelService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpGuarChgAppRel>> index(@RequestBody QueryModel queryModel) {
        List<IqpGuarChgAppRel> list = iqpGuarChgAppRelService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    @PostMapping("/grt")
    protected ResultDto<List<IqpGuarChgAppRel>> indexgrt(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("guarContState","101");
        List<IqpGuarChgAppRel> list = iqpGuarChgAppRelService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>>
     * @author tangxun
     * @date 2021/5/18 10:57
     * @version 1.0.0
     * @desc 查询部分担保合同字段
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cont")
    protected ResultDto<List<Map<String,Object>>> indexGuarCont(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = iqpGuarChgAppRelService.selectGuarContByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{igcarSerno}")
    protected ResultDto<IqpGuarChgAppRel> show(@PathVariable("igcarSerno") String igcarSerno) {
        IqpGuarChgAppRel iqpGuarChgAppRel = iqpGuarChgAppRelService.selectByPrimaryKey(igcarSerno);
        return new ResultDto<>(iqpGuarChgAppRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpGuarChgAppRel> create(@RequestBody IqpGuarChgAppRel iqpGuarChgAppRel) throws URISyntaxException {
        iqpGuarChgAppRelService.insertSelective(iqpGuarChgAppRel);
        return new ResultDto<>(iqpGuarChgAppRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpGuarChgAppRel iqpGuarChgAppRel) throws URISyntaxException {
        int result = iqpGuarChgAppRelService.updateSelective(iqpGuarChgAppRel);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{igcarSerno}")
    protected ResultDto<Integer> delete(@PathVariable("igcarSerno") String igcarSerno) {
        int result = iqpGuarChgAppRelService.deleteByPrimaryKey(igcarSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) throws Exception {
        IqpGuarChgAppRel iqpGuarChgAppRel = iqpGuarChgAppRelService.selectByPrimaryKey(ids);
        if(StringUtils.isBlank(iqpGuarChgAppRel.getGuarContNo())){
            throw new Exception("担保编号");
        }
        int result1 = grtGuarBizRstRelService.deleteByGuarNo(iqpGuarChgAppRel.getGuarContNo());
        int result = iqpGuarChgAppRelService.deleteByIds(ids);
        return new ResultDto<>(result1);
    }
}
