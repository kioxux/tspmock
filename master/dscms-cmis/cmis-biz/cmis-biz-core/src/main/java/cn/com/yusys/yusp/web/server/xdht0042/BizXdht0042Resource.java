package cn.com.yusys.yusp.web.server.xdht0042;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0042.req.Xdht0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0042.resp.Xdht0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0042.Xdht0042Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:优企贷、优农贷合同信息查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0042:优企贷、优农贷合同信息查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0042Resource.class);

    @Resource
    private Xdht0042Service xdht0042Service;

    /**
     * 交易码：xdht0042
     * 交易描述：优企贷、优农贷合同信息查询
     *
     * @param xdht0042DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷合同信息查询")
    @PostMapping("/xdht0042")
    protected @ResponseBody
    ResultDto<Xdht0042DataRespDto> xdht0042(@Validated @RequestBody Xdht0042DataReqDto xdht0042DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042DataReqDto));
        Xdht0042DataRespDto xdht0042DataRespDto = new Xdht0042DataRespDto();// 响应Dto:优企贷、优农贷合同信息查询
        ResultDto<Xdht0042DataRespDto> xdht0042DataResultDto = new ResultDto<>();
        // 从xdht0042DataReqDto获取业务值进行业务逻辑处理
        String query_type = xdht0042DataReqDto.getQuery_type();//查询类型
        String cert_code = xdht0042DataReqDto.getCert_code();//证件号
        String cus_id = xdht0042DataReqDto.getCus_id();//客户号
        try {
            // 从xdht0042DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042DataReqDto));
            xdht0042DataRespDto = xdht0042Service.queryContDeTailsInfoByCertCode(xdht0042DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042DataRespDto));
            // 封装xdht0027DataResultDto中正确的返回码和返回信息
            // 封装xdht0042DataResultDto中正确的返回码和返回信息
            xdht0042DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0042DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            // 封装xdht0042DataResultDto中异常返回码和返回信息
            xdht0042DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0042DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0042DataRespDto到xdht0042DataResultDto中
        xdht0042DataResultDto.setData(xdht0042DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042DataResultDto));
        return xdht0042DataResultDto;
    }
}
