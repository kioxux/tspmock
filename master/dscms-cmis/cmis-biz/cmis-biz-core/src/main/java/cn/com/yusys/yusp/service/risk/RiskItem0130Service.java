package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.RptSpdAnysJxd;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RiskItem0130Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0130Service.class);

    @Autowired
    private RptSpdAnysJxdService rptSpdAnysJxdService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    /**
     * @方法名称: riskItem0130
     * @方法描述: 结息贷评分卡校验
     * @参数与返回说明:
     * @算法描述:
     * 1、评分低于70分 拦截
     * 2、实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等 若是，则拦截；
     * 3、企业及实际控制人他行信用贷款授信额+我行信用贷款+信用卡用信额+结息贷申请额大于500万元 若是，则拦截；
     * 4、企业自制报表资产负债率超过75% 若是，则拦截；
     * 5、企业及实际控制人贷款银行超过3家 若是，则拦截；
     * 6、企业成立时间<=3年  若是，则拦截；
     * @创建人: zhangliang15
     * @创建时间: 2021-10-19 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0130(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("结息贷评分卡校验开始*******************业务流水号：【{}】，客户号：【{}】",serno,cusId);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //获取客户信息失败
            return riskResultDto;
        }

        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 根据申请流水号获取当前授信申请对应的授信金额大于0的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        paramsSub.put("lmtAmt", "0");
        List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        boolean isExitFlag = false; //判断是否存在特色产品：结息贷
        for (LmtAppSub lmtAppSub : subList){
            Map paramsSubPrd = new HashMap();
            // 获取分项产品明细
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            for (LmtAppSubPrd lmtAppSubPrd : subListPrd){
                //结息贷：P013
                if (Objects.equals("P013", lmtAppSubPrd.getLmtBizTypeProp())) {
                    isExitFlag = true;
                    break;
                } else {
                    isExitFlag = false;
                }
            }
            if(isExitFlag) {
                break;
            }
        }
        // 判断若为特色产品：结息贷，则进行后续校验。
        if(isExitFlag) {
            //  1、评分低于70分 拦截
            // 获取调查报告特定产品分析结息贷信息
            RptSpdAnysJxd rptSpdAnysJxd = rptSpdAnysJxdService.selectByPrimaryKey(serno);
            if (Objects.isNull(rptSpdAnysJxd)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013001);
                return riskResultDto;
            }

            // 评分卡分数未满70分不予准入
            if (70 > (Optional.ofNullable(rptSpdAnysJxd.getPfkJxd1Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd2Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd3Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd4Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd5Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd6Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd7Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd8Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd9Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd10Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd11Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd12Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd13Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysJxd.getPfkJxd14Grade1()).orElse(0))) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013002);
                return riskResultDto;
            }

//        // 1.企业有无违规排污，有无被环保部门查处和处罚
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd1())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
//
//        // 2.企业员工人数是否稳定，员工待遇是否合理
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd2())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
//
//        // 3.企业有无被税务机关查处和处罚
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd3())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
//
//        // 4.有无异常工商股权变更情况
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd4())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }

            // 5.企业贷款银行是否超过3家
            if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd5())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013004);
                return riskResultDto;
            }

            // 6.企业及实际控制人他行信用贷款授信额+我行信用贷款+信用卡用信额+结息贷申请额是否大于500万元
            if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd6())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013005);
                return riskResultDto;
            }

//        // 7.合规经营其它不利情况请简述
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd7())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }

            // 8.实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等
            if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd8())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013003);
                return riskResultDto;
            }
//        // 9.实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd9())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
//        // 10.实际控制人是否参与民间融资、投资高风险行业等行为
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd10())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
//        // 11.有无其他影响企业稳定经营的情况
//        if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd11())) {
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840802);
//            return riskResultDto;
//        }
            // 12.企业自制报表资产负债率是否超过75%
            if (!Objects.equals("0", rptSpdAnysJxd.getFocusJxd12())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013006);
                return riskResultDto;
            }

            //查询对公客户基本信息
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
            if (Objects.isNull(cusCorpDtoResultDto) || Objects.isNull(cusCorpDtoResultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840701);
                return riskResultDto;
            }
            CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
            // 判断公司成立日期是否为空
            if (StringUtils.isBlank(cusCorpDto.getBuildDate())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840702);
                return riskResultDto;
            }
            // 获取公司成立日期
            Date buildDate = DateUtils.parseDate(cusCorpDto.getBuildDate(), "yyyy-MM-dd");
            log.info("结息贷评分卡校验*******************公司成立日期：【{}】", buildDate);

            // 若公司成立日期小于等于三年，则拦截
            if (DateUtils.compare(DateUtils.addMonth(buildDate, 36), DateUtils.getCurrDate()) >= 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013007);
                return riskResultDto;
            }
        }
        log.info("结息贷评分卡校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}