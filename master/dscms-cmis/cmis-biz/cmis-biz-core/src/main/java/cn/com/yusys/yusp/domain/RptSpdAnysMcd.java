/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcd
 * @类描述: rpt_spd_anys_mcd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-23 23:15:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_mcd")
public class RptSpdAnysMcd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 市场方编号 **/
	@Column(name = "PARTNER_ID", unique = false, nullable = true, length = 40)
	private String partnerId;
	
	/** 市场方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 80)
	private String partnerName;
	
	/** 市场方额度 **/
	@Column(name = "PARTNER_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal partnerAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 剩余额度 **/
	@Column(name = "LEFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal leftAmt;
	
	/** 市场方授信到期日 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 20)
	private String lmtTerm;
	
	/** 上期授信木材质押情况 **/
	@Column(name = "LAST_LMT_WOOD_ZY_CONDITION", unique = false, nullable = true, length = 65535)
	private String lastLmtWoodZyCondition;
	
	/** 转让木材品种 **/
	@Column(name = "TRANSFER_WOOD_TYPE", unique = false, nullable = true, length = 40)
	private String transferWoodType;
	
	/** 转让木材数量 **/
	@Column(name = "TRANSFER_WOOD_NUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal transferWoodNum;
	
	/** 转让木材原货值 **/
	@Column(name = "TRANSFER_ORIGINAL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal transferOriginalValue;
	
	/** 转让木材目前货值 **/
	@Column(name = "TRANSFER_PRESENT_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal transferPresentValue;
	
	/** 转让木材有无补货情况 **/
	@Column(name = "TRANSFER_WOOD_DESC", unique = false, nullable = true, length = 65535)
	private String transferWoodDesc;
	
	/** 本期授信木材质押情况 **/
	@Column(name = "LMT_WOOD_ZY_CONDITION", unique = false, nullable = true, length = 65535)
	private String lmtWoodZyCondition;
	
	/** 拟转让木材品种 **/
	@Column(name = "INTEND_TRANSFER_WOOD_TYPE", unique = false, nullable = true, length = 40)
	private String intendTransferWoodType;
	
	/** 拟转让木材数量 **/
	@Column(name = "INTEND_TRANSFER_WOOD_NUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intendTransferWoodNum;
	
	/** 拟转让木材货值 **/
	@Column(name = "INTEND_TRANSFER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intendTransferValue;
	
	/** 最高可授信金额 **/
	@Column(name = "MAX_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maxLmtAmt;
	
	/** 租赁面积 **/
	@Column(name = "RENT_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentSqu;
	
	/** 租赁期限 **/
	@Column(name = "RENT_TERM", unique = false, nullable = true, length = 10)
	private Integer rentTerm;
	
	/** 租金 **/
	@Column(name = "RENT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentAmt;
	
	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 80)
	private String payMode;
	
	/** 租赁位置 **/
	@Column(name = "RENT_AREA", unique = false, nullable = true, length = 200)
	private String rentArea;
	
	/** 仓储费市场内发生费用 **/
	@Column(name = "STORAGE_CHARGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal storageChargeAmt;
	
	/** 列支费市场内发生费用 **/
	@Column(name = "CHARGE_FEE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chargeFeeAmt;
	
	/** 仓储费销售金额 **/
	@Column(name = "STORAGE_CHARGE_SALES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal storageChargeSalesAmt;
	
	/** 列支费销售金额 **/
	@Column(name = "CHARGE_FEE_SALES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chargeFeeSalesAmt;
	
	/** 仓储费与出入库情况核对及简要说明 **/
	@Column(name = "STORAGE_CHARGE_DESC", unique = false, nullable = true, length = 65535)
	private String storageChargeDesc;
	
	/** 列支费与出入库情况核对及简要说明 **/
	@Column(name = "CHARGE_FEE_DESC", unique = false, nullable = true, length = 65535)
	private String chargeFeeDesc;
	
	/** 市场外经营情况 **/
	@Column(name = "MARKET_OUT_DESC", unique = false, nullable = true, length = 65535)
	private String marketOutDesc;
	
	/** 其他情况说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param partnerId
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	
    /**
     * @return partnerId
     */
	public String getPartnerId() {
		return this.partnerId;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param partnerAmt
	 */
	public void setPartnerAmt(java.math.BigDecimal partnerAmt) {
		this.partnerAmt = partnerAmt;
	}
	
    /**
     * @return partnerAmt
     */
	public java.math.BigDecimal getPartnerAmt() {
		return this.partnerAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param leftAmt
	 */
	public void setLeftAmt(java.math.BigDecimal leftAmt) {
		this.leftAmt = leftAmt;
	}
	
    /**
     * @return leftAmt
     */
	public java.math.BigDecimal getLeftAmt() {
		return this.leftAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(String lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public String getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param lastLmtWoodZyCondition
	 */
	public void setLastLmtWoodZyCondition(String lastLmtWoodZyCondition) {
		this.lastLmtWoodZyCondition = lastLmtWoodZyCondition;
	}
	
    /**
     * @return lastLmtWoodZyCondition
     */
	public String getLastLmtWoodZyCondition() {
		return this.lastLmtWoodZyCondition;
	}
	
	/**
	 * @param transferWoodType
	 */
	public void setTransferWoodType(String transferWoodType) {
		this.transferWoodType = transferWoodType;
	}
	
    /**
     * @return transferWoodType
     */
	public String getTransferWoodType() {
		return this.transferWoodType;
	}
	
	/**
	 * @param transferWoodNum
	 */
	public void setTransferWoodNum(java.math.BigDecimal transferWoodNum) {
		this.transferWoodNum = transferWoodNum;
	}
	
    /**
     * @return transferWoodNum
     */
	public java.math.BigDecimal getTransferWoodNum() {
		return this.transferWoodNum;
	}
	
	/**
	 * @param transferOriginalValue
	 */
	public void setTransferOriginalValue(java.math.BigDecimal transferOriginalValue) {
		this.transferOriginalValue = transferOriginalValue;
	}
	
    /**
     * @return transferOriginalValue
     */
	public java.math.BigDecimal getTransferOriginalValue() {
		return this.transferOriginalValue;
	}
	
	/**
	 * @param transferPresentValue
	 */
	public void setTransferPresentValue(java.math.BigDecimal transferPresentValue) {
		this.transferPresentValue = transferPresentValue;
	}
	
    /**
     * @return transferPresentValue
     */
	public java.math.BigDecimal getTransferPresentValue() {
		return this.transferPresentValue;
	}
	
	/**
	 * @param transferWoodDesc
	 */
	public void setTransferWoodDesc(String transferWoodDesc) {
		this.transferWoodDesc = transferWoodDesc;
	}
	
    /**
     * @return transferWoodDesc
     */
	public String getTransferWoodDesc() {
		return this.transferWoodDesc;
	}
	
	/**
	 * @param lmtWoodZyCondition
	 */
	public void setLmtWoodZyCondition(String lmtWoodZyCondition) {
		this.lmtWoodZyCondition = lmtWoodZyCondition;
	}
	
    /**
     * @return lmtWoodZyCondition
     */
	public String getLmtWoodZyCondition() {
		return this.lmtWoodZyCondition;
	}
	
	/**
	 * @param intendTransferWoodType
	 */
	public void setIntendTransferWoodType(String intendTransferWoodType) {
		this.intendTransferWoodType = intendTransferWoodType;
	}
	
    /**
     * @return intendTransferWoodType
     */
	public String getIntendTransferWoodType() {
		return this.intendTransferWoodType;
	}
	
	/**
	 * @param intendTransferWoodNum
	 */
	public void setIntendTransferWoodNum(java.math.BigDecimal intendTransferWoodNum) {
		this.intendTransferWoodNum = intendTransferWoodNum;
	}
	
    /**
     * @return intendTransferWoodNum
     */
	public java.math.BigDecimal getIntendTransferWoodNum() {
		return this.intendTransferWoodNum;
	}
	
	/**
	 * @param intendTransferValue
	 */
	public void setIntendTransferValue(java.math.BigDecimal intendTransferValue) {
		this.intendTransferValue = intendTransferValue;
	}
	
    /**
     * @return intendTransferValue
     */
	public java.math.BigDecimal getIntendTransferValue() {
		return this.intendTransferValue;
	}
	
	/**
	 * @param maxLmtAmt
	 */
	public void setMaxLmtAmt(java.math.BigDecimal maxLmtAmt) {
		this.maxLmtAmt = maxLmtAmt;
	}
	
    /**
     * @return maxLmtAmt
     */
	public java.math.BigDecimal getMaxLmtAmt() {
		return this.maxLmtAmt;
	}
	
	/**
	 * @param rentSqu
	 */
	public void setRentSqu(java.math.BigDecimal rentSqu) {
		this.rentSqu = rentSqu;
	}
	
    /**
     * @return rentSqu
     */
	public java.math.BigDecimal getRentSqu() {
		return this.rentSqu;
	}
	
	/**
	 * @param rentTerm
	 */
	public void setRentTerm(Integer rentTerm) {
		this.rentTerm = rentTerm;
	}
	
    /**
     * @return rentTerm
     */
	public Integer getRentTerm() {
		return this.rentTerm;
	}
	
	/**
	 * @param rentAmt
	 */
	public void setRentAmt(java.math.BigDecimal rentAmt) {
		this.rentAmt = rentAmt;
	}
	
    /**
     * @return rentAmt
     */
	public java.math.BigDecimal getRentAmt() {
		return this.rentAmt;
	}
	
	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	
    /**
     * @return payMode
     */
	public String getPayMode() {
		return this.payMode;
	}
	
	/**
	 * @param rentArea
	 */
	public void setRentArea(String rentArea) {
		this.rentArea = rentArea;
	}
	
    /**
     * @return rentArea
     */
	public String getRentArea() {
		return this.rentArea;
	}
	
	/**
	 * @param storageChargeAmt
	 */
	public void setStorageChargeAmt(java.math.BigDecimal storageChargeAmt) {
		this.storageChargeAmt = storageChargeAmt;
	}
	
    /**
     * @return storageChargeAmt
     */
	public java.math.BigDecimal getStorageChargeAmt() {
		return this.storageChargeAmt;
	}
	
	/**
	 * @param chargeFeeAmt
	 */
	public void setChargeFeeAmt(java.math.BigDecimal chargeFeeAmt) {
		this.chargeFeeAmt = chargeFeeAmt;
	}
	
    /**
     * @return chargeFeeAmt
     */
	public java.math.BigDecimal getChargeFeeAmt() {
		return this.chargeFeeAmt;
	}
	
	/**
	 * @param storageChargeSalesAmt
	 */
	public void setStorageChargeSalesAmt(java.math.BigDecimal storageChargeSalesAmt) {
		this.storageChargeSalesAmt = storageChargeSalesAmt;
	}
	
    /**
     * @return storageChargeSalesAmt
     */
	public java.math.BigDecimal getStorageChargeSalesAmt() {
		return this.storageChargeSalesAmt;
	}
	
	/**
	 * @param chargeFeeSalesAmt
	 */
	public void setChargeFeeSalesAmt(java.math.BigDecimal chargeFeeSalesAmt) {
		this.chargeFeeSalesAmt = chargeFeeSalesAmt;
	}
	
    /**
     * @return chargeFeeSalesAmt
     */
	public java.math.BigDecimal getChargeFeeSalesAmt() {
		return this.chargeFeeSalesAmt;
	}
	
	/**
	 * @param storageChargeDesc
	 */
	public void setStorageChargeDesc(String storageChargeDesc) {
		this.storageChargeDesc = storageChargeDesc;
	}
	
    /**
     * @return storageChargeDesc
     */
	public String getStorageChargeDesc() {
		return this.storageChargeDesc;
	}
	
	/**
	 * @param chargeFeeDesc
	 */
	public void setChargeFeeDesc(String chargeFeeDesc) {
		this.chargeFeeDesc = chargeFeeDesc;
	}
	
    /**
     * @return chargeFeeDesc
     */
	public String getChargeFeeDesc() {
		return this.chargeFeeDesc;
	}
	
	/**
	 * @param marketOutDesc
	 */
	public void setMarketOutDesc(String marketOutDesc) {
		this.marketOutDesc = marketOutDesc;
	}
	
    /**
     * @return marketOutDesc
     */
	public String getMarketOutDesc() {
		return this.marketOutDesc;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}