package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.RptBasicInfoAsso;
import cn.com.yusys.yusp.domain.RptPersonalBusiness;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoAsso
 * @类描述: rpt_basic_info_asso数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:15:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptBasicInfoAssoAndPersonalBusinessDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private RptBasicInfoAsso rptBasicInfoAsso;

	private RptPersonalBusiness rptPersonalBusiness;

	public RptBasicInfoAsso getRptBasicInfoAsso() {
		return rptBasicInfoAsso;
	}

	public RptPersonalBusiness getRptPersonalBusiness() {
		return rptPersonalBusiness;
	}

	public void setRptBasicInfoAsso(RptBasicInfoAsso rptBasicInfoAsso) {
		this.rptBasicInfoAsso = rptBasicInfoAsso;
	}

	public void setRptPersonalBusiness(RptPersonalBusiness rptPersonalBusiness) {
		this.rptPersonalBusiness = rptPersonalBusiness;
	}
}