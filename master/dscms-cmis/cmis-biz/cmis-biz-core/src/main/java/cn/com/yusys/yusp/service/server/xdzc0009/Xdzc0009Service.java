package cn.com.yusys.yusp.service.server.xdzc0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.Xdpj03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.req.Xdzc0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.resp.Xdzc0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:资产池出票接口
 *
 * @Author xs
 * @Date 2021/09/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0009Service {

    @Autowired
    private AsplAorgListService asplAorgListService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0009.Xdzc0009Service.class);

    /**
     * 交易码：xdzc0009
     * 交易描述：
     *
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0009DataRespDto xdzc0009Service(Xdzc0009DataReqDto xdzc0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value);
        Xdzc0009DataRespDto xdzc0009DataRespDto = new Xdzc0009DataRespDto();
        // 获取操作类型
        String opType = xdzc0009DataReqDto.getOpType();
        List<cn.com.yusys.yusp.dto.server.xdzc0009.req.List> list = xdzc0009DataReqDto.getList();
        xdzc0009DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0009DataRespDto.setOpMsg("承兑行信用等级维护失败");// 描述信息
        try{
            if (Objects.equals(opType,"0")){
                // 0、更新
                list.forEach(e->{
                    updateXdzc0009(e);
                });
            }else if(Objects.equals(opType,"1")){
                // 1、新增
                list.forEach(e->{
                    insertXdzc0009(e);
                });
            }else{
                // 2、删除
                list.forEach(e->{
                    if(asplAorgListService.deleteByHeadBankNo(e.getAorgNo())<1){
                        throw BizException.error("9999","数据:【{}】删除失败"+Objects.toString(e));
                    }
                });
            }
            xdzc0009DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
            xdzc0009DataRespDto.setOpMsg("承兑行信用等级维护成功");// 描述信息
        } catch (BizException e) {
            xdzc0009DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0009DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdzc0009DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0009DataRespDto.setOpMsg(e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value);
        return xdzc0009DataRespDto;
    }

    private void updateXdzc0009(cn.com.yusys.yusp.dto.server.xdzc0009.req.List e) {
        AsplAorgList asplAorgList = new AsplAorgList();
        //asplAorgList.setPkId(StringUtils.getUUID());// Generated
        asplAorgList.setHeadBankNo(e.getAorgNo());// 总行行号
        //asplAorgList.setEcifNo("");// ECIF编号
        asplAorgList.setIntbankName(e.getAorgName());// 同业名称
        asplAorgList.setCreditLevel(e.getCreditLevel());// 信用等级
        //asplAorgList.setPldimnRate("");// 抵质押率
        asplAorgList.setStatus(e.getStatus());// 状态
        //asplAorgList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 操作类型
        //asplAorgList.setInputId(e.getInputId());// 登记人
        //asplAorgList.setInputBrId(e.getInputBrId());// 登记机构
        //asplAorgList.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 登记日期
        asplAorgList.setUpdId(e.getInputId());// 最近修改人
        asplAorgList.setUpdBrId(e.getInputBrId());// 最近修改机构
         asplAorgList.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 最近修改日期
        //asplAorgList.setManagerId("");// 主管客户经理
        //asplAorgList.setManagerBrId("");// 主管机构
        //asplAorgList.setCreateTime(DateUtils.getCurrDate());// 创建时间
        asplAorgList.setUpdateTime(DateUtils.getCurrDate());// 修改时间
        asplAorgListService.updateByHeadBankNo(asplAorgList);

    }

    private void insertXdzc0009(cn.com.yusys.yusp.dto.server.xdzc0009.req.List e) {
        AsplAorgList asplAorgList = new AsplAorgList();
        asplAorgList.setPkId(StringUtils.getUUID());// Generated
        asplAorgList.setHeadBankNo(e.getAorgNo());// 总行行号
        asplAorgList.setEcifNo("");// ECIF编号
        asplAorgList.setIntbankName(e.getAorgName());// 同业名称
        asplAorgList.setCreditLevel(e.getCreditLevel());// 信用等级
        //asplAorgList.setPldimnRate("");// 抵质押率
        asplAorgList.setStatus(e.getStatus());// 状态
        asplAorgList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 操作类型
        asplAorgList.setInputId(e.getInputId());// 登记人
        asplAorgList.setInputBrId(e.getInputBrId());// 登记机构
        asplAorgList.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 登记日期
        //asplAorgList.setUpdId("");// 最近修改人
        //asplAorgList.setUpdBrId("");// 最近修改机构
        // asplAorgList.setUpdDate("");// 最近修改日期
        //asplAorgList.setManagerId("");// 主管客户经理
        //asplAorgList.setManagerBrId("");// 主管机构
        asplAorgList.setCreateTime(DateUtils.getCurrDate());// 创建时间
        asplAorgList.setUpdateTime(DateUtils.getCurrDate());// 修改时间
        asplAorgListService.insertSelective(asplAorgList);
    }
}
