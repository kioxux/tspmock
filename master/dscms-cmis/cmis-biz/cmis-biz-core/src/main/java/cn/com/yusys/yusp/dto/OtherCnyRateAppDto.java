package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.yusys.yusp.domain.*;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateApp
 * @类描述: other_cny_rate_app数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 10:14:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class OtherCnyRateAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//申请基本信息
	private OtherCnyRateApp otherCnyRateApp;
	
	//税务信息
	private List<OtherCnyRateAppTax> otherCnyRateAppTaxList;
	
	//融资信息明细
	private List<OtherCnyRateAppFinDetails> finDetailsList;
	
	//审批表
	private List<OtherCnyRateApprSub> apprSubList;

	private List<OtherItemApp> otherItemAppList;
	
	public OtherCnyRateApp getOtherCnyRateApp() {
		return otherCnyRateApp;
	}

	public void setOtherCnyRateApp(OtherCnyRateApp otherCnyRateApp) {
		this.otherCnyRateApp = otherCnyRateApp;
	}

	public List<OtherCnyRateAppTax> getOtherCnyRateAppTaxList() {
		return otherCnyRateAppTaxList;
	}

	public void setOtherCnyRateAppTaxList(List<OtherCnyRateAppTax> otherCnyRateAppTaxList) {
		this.otherCnyRateAppTaxList = otherCnyRateAppTaxList;
	}

	public List<OtherCnyRateAppFinDetails> getFinDetailsList() {
		return finDetailsList;
	}

	public void setFinDetailsList(List<OtherCnyRateAppFinDetails> finDetailsList) {
		this.finDetailsList = finDetailsList;
	}

	public List<OtherCnyRateApprSub> getApprSubList() {
		return apprSubList;
	}

	public void setApprSubList(List<OtherCnyRateApprSub> apprSubList) {
		this.apprSubList = apprSubList;
	}

	public List<OtherItemApp> getOtherItemAppList() { return otherItemAppList;}

	public void setOtherItemAppList(List<OtherItemApp> selectByLmtSerno) { this.otherItemAppList=selectByLmtSerno;}


}