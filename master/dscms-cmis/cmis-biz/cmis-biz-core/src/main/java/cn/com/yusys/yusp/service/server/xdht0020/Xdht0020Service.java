package cn.com.yusys.yusp.service.server.xdht0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtCobInfo;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdht0020.req.Xdht0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020LmtDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCobInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import cn.com.yusys.yusp.service.client.cus.cmiscus0007.CmisCus0007Service;
import cn.com.yusys.yusp.service.impl.CmisCusClientServiceImpl;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * 接口处理类:合同信息查看
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdht0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0020Service.class);
    @Autowired
    private CmisCus0006Service cmisCus0006Service;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CmisCus0007Service cmisCus0007Service;
    @Resource
    private CmisCusClientServiceImpl cmisCusClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private LmtCobInfoMapper lmtCobInfoMapper;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Resource
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    /**
     * 合同信息查看
     *
     * @param xdht0020DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public ResultDto<Xdht0020DataRespDto> queryContStatusByCertCode(Xdht0020DataReqDto xdht0020DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataReqDto));
        //定义返回信息
        Xdht0020DataRespDto xdht0020DataRespDto = new Xdht0020DataRespDto();
        ResultDto<Xdht0020DataRespDto> xdht0020DataResultDto = new ResultDto<>();
        Xdht0020LmtDto xdht0020LmtDto = new Xdht0020LmtDto();

        //获取请求参数
        String cusCertNo = xdht0020DataReqDto.getCusCertNo();//客户证件号
        String loanUseType = xdht0020DataReqDto.getLoanUseType();//贷款用途
        String contState = xdht0020DataReqDto.getContState();//合同状态
        String isWhb = xdht0020DataReqDto.getIsWhb();//是否无还本
        String lmt = "";//可用余额
        String loanStartDate = "";//合同起始日期
        String loanEndDate = "";//合同到期日
        String curType = "";//币种
        String cnContNo = "";
        BigDecimal applyAmt = null;
        BigDecimal lprBaseRate = null;
        String preferCode = "";
        String approvalEndDate = "";
        String repaytype = "";
        BigDecimal rate = BigDecimal.ZERO;
        //业务处理
        try {
            if (StringUtil.isEmpty(cusCertNo)) {
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, "身份证号不能为空！");
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            if (StringUtil.isEmpty(loanUseType)) {
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, "贷款用途不能为空！");
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            if (StringUtil.isEmpty(contState)) {
                contState = "100";
            }
            String bizTypeInit = loanUseType;
            if ("2".equals(loanUseType)) {
                loanUseType = "022032";// 工资贷
            } else if ("3".equals(loanUseType)) {
                loanUseType = "022033";// 公积金贷
            } else if ("4".equals(loanUseType)) {
                loanUseType = "SC010008";// 优企贷
            } else if ("6".equals(loanUseType)) {
                loanUseType = "SC020009";// 优抵贷
            } else if ("5".equals(loanUseType)) {
                loanUseType = "SC020010";// 优农贷
            }
            //通过客户证件号查询客户信息
            logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(cusCertNo));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(cusCertNo)).orElse(new CusBaseClientDto());
            logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(cusCertNo), JSON.toJSONString(cusCertNo));
            // 客户编号
            String cusId = cusBaseClientDto.getCusId();
            if (StringUtil.isEmpty(cusId)) {
                logger.info("根据证件号码【{}】未查询到该客户信息,该客户为新客户！", cusCertNo);
            }
            //根据输入参数,查询合同信息
            Map queryMap = new HashMap();
            queryMap.put("cusId", cusId);//客户号
            queryMap.put("loanUseType", loanUseType);//贷款用途
            queryMap.put("cont_state", contState);//申请金额
            logger.info("***************XDHT0020*根据输入参数【{}】查询合同表信息开始", JSON.toJSONString(queryMap));
            if ("5".equals(bizTypeInit)) {// 优农贷-查询合同信息
                xdht0020DataRespDto = ctrLoanContMapper.queryCtrLoanContdetailsFor5(queryMap);
            } else if ("6".equals(bizTypeInit)) {//优抵贷
                xdht0020DataRespDto = ctrLoanContMapper.queryCtrLoanContdetailsFor6(queryMap);
            } else {
                xdht0020DataRespDto = ctrLoanContMapper.queryCtrLoanContdetails(queryMap);
            }
            logger.info("***************XDHT0020*根据输入参数【{}】查询合同表信息结束，,查询结果信息【{}】", JSON.toJSONString(queryMap), JSON.toJSONString(xdht0020DataRespDto));
            if (Objects.isNull(xdht0020DataRespDto) || StringUtils.isBlank(xdht0020DataRespDto.getContNo())) {
                xdht0020DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0020DataResultDto.setMessage("未查到对应的合同！");
                return xdht0020DataResultDto;
            }
            // 起始日,到期日
            loanStartDate = xdht0020DataRespDto.getContStartDate();
            if (StringUtil.isNotEmpty(loanStartDate)) {
                loanStartDate = loanStartDate.replaceAll("-", "");//网银只要8位
            }
            loanEndDate = xdht0020DataRespDto.getContEndDate();
            if (StringUtil.isNotEmpty(loanEndDate)) {
                loanEndDate = loanEndDate.replaceAll("-", "");//网银只要8位
            }
            curType = xdht0020DataRespDto.getCurType();
            cnContNo = xdht0020DataRespDto.getCnContNo();
            applyAmt = xdht0020DataRespDto.getApplyAmt();
            lprBaseRate = Optional.ofNullable(xdht0020DataRespDto.getLprRate()).orElse(BigDecimal.ZERO);
            rate = xdht0020DataRespDto.getRate();
            preferCode = xdht0020DataRespDto.getPreferCode();
            String contNo = xdht0020DataRespDto.getContNo();
            String contType = xdht0020DataRespDto.getContType();
            String contSignStatus = "";//合同签订状态
            String commonDebitName = "";//共借人姓名
            String commonDebitCertCode = "";//共借人证件号
            String oldContNo = "";//原合同号
            String oldSurveySerno = "";//原调查流水
            if (StringUtil.isNotEmpty(contNo)) {//合同不为空
                Map map = new HashMap();
                map.put("contNo", contNo);//合同号
                if ("SC010008".equals(loanUseType)) {//优企贷
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优企贷合同表信息开始", JSON.toJSONString(map));
                    // 根据是否无还本续贷、合同类型确认可用额度计算借据范围
                    if("Y".equals(isWhb)){//是否无还本续贷
                        xdht0020LmtDto = Optional.ofNullable(ctrLoanContMapper.queryCtrLoanLmtByContTypeFor3(map)).orElse(new Xdht0020LmtDto());
                    }else{
                        if ("2".equals(contType)) {//最高额合同
                            xdht0020LmtDto = Optional.ofNullable(ctrLoanContMapper.queryCtrLoanLmtByContTypeFor2(map)).orElse(new Xdht0020LmtDto());
                        } else {// 一般合同
                            xdht0020LmtDto = Optional.ofNullable(ctrLoanContMapper.queryCtrLoanLmtByContTypeFor1(map)).orElse(new Xdht0020LmtDto());
                        }
                    }
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优企贷合同表信息结束，,查询结果信息【{}】", JSON.toJSONString(map), JSON.toJSONString(xdht0020LmtDto));
                    approvalEndDate = xdht0020LmtDto.getApprovalEndDate();
                    repaytype = xdht0020LmtDto.getRepayMode();

                    //关联优企贷续贷申请名单信息查询原合同号
                    Map selectMap = new HashMap();
                    selectMap.put("cusId", cusId);
                    Map<String, String> returnMap = ctrLoanContMapper.selectOldContNofromCtrLoanCont(selectMap);
                    if (returnMap != null) {
                        if (returnMap.containsKey("oldcontno")) {
                            oldContNo = returnMap.get("oldcontno").toString();
                        }
                        if (returnMap.containsKey("surveyserno")) {
                            oldSurveySerno = returnMap.get("surveyserno").toString();
                        }
                    }
                    //根据原合同编号查询对应的借据的到期日期
                    String billEndDate = "";
                    if (StringUtil.isNotEmpty(oldContNo)) {
                        billEndDate = accLoanMapper.selectLoanEndDateBycontno(oldContNo);
                    }

                    //取系统日期
                    String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                    Date opendate = DateUtils.parseDate(openday, "yyyy-MM-dd");
                    Date contStartDate = DateUtils.parseDate(xdht0020DataRespDto.getContStartDate(), "yyyy-MM-dd");
                    Date contEndDate = DateUtils.parseDate(xdht0020DataRespDto.getContEndDate(), "yyyy-MM-dd");
                    BigDecimal days3 = new BigDecimal(DateUtils.getDaysByTwoDates(contStartDate, opendate));//合同起始日期减去当前日期(20210922按天计算)
                    BigDecimal days5 = new BigDecimal(DateUtils.getDaysByTwoDates(opendate, contEndDate));//当前日期减去到期日期

                    //是否无还本
                    if ("Y".equals(isWhb)) {
                        if (StringUtil.isNotEmpty(billEndDate)) {//原借据的到期日期不为空
                            if (days3.compareTo(new BigDecimal(30)) <= 0 && billEndDate.compareTo(openday) >= 0) {
                                lmt = xdht0020LmtDto.getLmt();
                            } else {
                                lmt = "0";
                            }
                        } else {
                            lmt = "0";
                        }
                    } else {
                        if ("1".equals(contType)) {//一般合同
                            if (days3.compareTo(new BigDecimal(30)) > 0) {
                                lmt = "0";
                            } else {
                                lmt = xdht0020LmtDto.getLmt();
                            }
                        } else if ("2".equals(contType)) {//最高额合同
                            if (days5.compareTo(new BigDecimal(30)) > 0 && days3.compareTo(new BigDecimal(365)) <= 0) {
                                lmt = xdht0020LmtDto.getLmt();
                            } else {
                                lmt = "0";
                            }
                        }
                    }

                    //查询优企贷共借人信息
                    String surveySerno = xdht0020DataRespDto.getPkId();//获取授信调查流水号
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优企贷共借人信息开始", JSON.toJSONString(surveySerno));
                    List<LmtCobInfo> lmtCobInfoList = lmtCobInfoMapper.selectByIqpSerno(surveySerno);
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优企贷共借人信息开始结束，,返回信息【{}】", JSON.toJSONString(surveySerno), JSON.toJSONString(lmtCobInfoList));
                    if (CollectionUtils.nonEmpty(lmtCobInfoList)) {
                        contSignStatus = "";//签约状态
                        for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                            if ("1".equals(lmtCobInfo.getSignState()) || StringUtil.isEmpty(lmtCobInfo.getSignState())) {//存在未签约的,即为未签约
                                contSignStatus = "1";//未签约
                            } else if ("2".equals(lmtCobInfo.getSignState())) {
                                contSignStatus = "2";//已签约
                            }
                            commonDebitName = lmtCobInfo.getCommonDebitName();//共借人
                            commonDebitCertCode = lmtCobInfo.getCommonDebitCertCode();//共借人证件号
                        }
                    }

                    //查询批复表,获取还款方式
                    logger.info("**************XDHT0020**根据合同号【{}】查询授信批复信息开始", surveySerno);
                    LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectBySurveySerno(surveySerno);
                    if (lmtCrdReplyInfo != null) {
                        repaytype = lmtCrdReplyInfo.getRepayMode();//还款方式
                        approvalEndDate = lmtCrdReplyInfo.getReplyEndDate();//批复到期日
                    }
                    if (StringUtil.isNotEmpty(approvalEndDate)) {//批复到期日
                        approvalEndDate = approvalEndDate.replaceAll("-", "");//渠道只要8位,不要-
                    }
                }
                if ("5".equals(bizTypeInit) || "6".equals(bizTypeInit)) {
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优抵贷、优农贷合同表信息开始", JSON.toJSONString(map));
                    xdht0020LmtDto = Optional.ofNullable(ctrLoanContMapper.queryCtrLoanLmtByContNo(map)).orElse(new Xdht0020LmtDto());
                    logger.info("***************XDHT0020*根据输入参数【{}】查询优抵贷、优农贷合同表信息结束，,返回结果信息【{}】", JSON.toJSONString(map), JSON.toJSONString(xdht0020LmtDto));
                    lmt = xdht0020LmtDto.getLmt();
                }
            } else {
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, "未查到对应的的合同！");
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            xdht0020DataRespDto.setApplyAmt(applyAmt);//申请金额
            xdht0020DataRespDto.setAvlAmt(StringUtils.isBlank(lmt) ? BigDecimal.ZERO : new BigDecimal(lmt));
            xdht0020DataRespDto.setCnContNo(contNo);//中文合同号（新信贷取消中文合同）
            xdht0020DataRespDto.setContEndDate(loanEndDate);//合同结束日期
            xdht0020DataRespDto.setContStartDate(loanStartDate);//合同起始日
            xdht0020DataRespDto.setContSignStatus(contSignStatus);//合同签订状态
            xdht0020DataRespDto.setContNo(contNo);//合同号
            xdht0020DataRespDto.setContType(contType);//合同类型
            xdht0020DataRespDto.setCurType(curType);//币种
            xdht0020DataRespDto.setCusName(commonDebitName);//共借人
            xdht0020DataRespDto.setCertNo(commonDebitCertCode);//共借人证件号
            double avg_rate = Double.parseDouble(applyAmt.toString()) * Double.parseDouble(lprBaseRate.toString()) / 360;
            //日息精确到角，四舍五入
            int sswr = (int) (Math.round(avg_rate * 100));
            avg_rate = sswr / 100;
            xdht0020DataRespDto.setDayInt(BigDecimal.valueOf(avg_rate));
            xdht0020DataRespDto.setReplyContEndDate(approvalEndDate);
            xdht0020DataRespDto.setRepayMode(repaytype);
            xdht0020DataRespDto.setLprBpType("加");
            xdht0020DataRespDto.setPreferCode(preferCode);
            xdht0020DataRespDto.setLprTerm("1年期");
            DecimalFormat sf = new DecimalFormat("#.0000");//格式化
            BigDecimal rateTemp = new BigDecimal(sf.format(Double.parseDouble(rate.toString()) * 100));
            xdht0020DataRespDto.setRate(rateTemp);
            xdht0020DataRespDto.setOldContNo(oldContNo);
            xdht0020DataRespDto.setOldSurveySerno(oldSurveySerno);
            //返回信息
            xdht0020DataResultDto.setData(xdht0020DataRespDto);
            xdht0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, e.getMessage());
            xdht0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, e.getMessage());
            xdht0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataRespDto));
        return xdht0020DataResultDto;
    }
}
