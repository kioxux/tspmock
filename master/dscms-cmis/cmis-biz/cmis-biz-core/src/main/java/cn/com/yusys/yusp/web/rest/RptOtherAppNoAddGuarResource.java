/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOtherAppNoAddGuar;
import cn.com.yusys.yusp.service.RptOtherAppNoAddGuarService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOtherAppNoAddGuarResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-30 21:23:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptotherappnoaddguar")
public class RptOtherAppNoAddGuarResource {
    @Autowired
    private RptOtherAppNoAddGuarService rptOtherAppNoAddGuarService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOtherAppNoAddGuar>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOtherAppNoAddGuar> list = rptOtherAppNoAddGuarService.selectAll(queryModel);
        return new ResultDto<List<RptOtherAppNoAddGuar>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOtherAppNoAddGuar>> index(QueryModel queryModel) {
        List<RptOtherAppNoAddGuar> list = rptOtherAppNoAddGuarService.selectByModel(queryModel);
        return new ResultDto<List<RptOtherAppNoAddGuar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOtherAppNoAddGuar> show(@PathVariable("pkId") String pkId) {
        RptOtherAppNoAddGuar rptOtherAppNoAddGuar = rptOtherAppNoAddGuarService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOtherAppNoAddGuar>(rptOtherAppNoAddGuar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOtherAppNoAddGuar> create(@RequestBody RptOtherAppNoAddGuar rptOtherAppNoAddGuar) throws URISyntaxException {
        rptOtherAppNoAddGuarService.insert(rptOtherAppNoAddGuar);
        return new ResultDto<RptOtherAppNoAddGuar>(rptOtherAppNoAddGuar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOtherAppNoAddGuar rptOtherAppNoAddGuar) throws URISyntaxException {
        int result = rptOtherAppNoAddGuarService.update(rptOtherAppNoAddGuar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOtherAppNoAddGuarService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOtherAppNoAddGuarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 条件查询
     *
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOtherAppNoAddGuar>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptOtherAppNoAddGuar>>(rptOtherAppNoAddGuarService.selectByModel(model));
    }

    /**
     * 根据流水号查询
     *
     * @param map
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptOtherAppNoAddGuar>> selectBySerno(@RequestBody Map<String,Object> map) {
        String serno = map.get("serno").toString();
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        return new ResultDto<List<RptOtherAppNoAddGuar>>(rptOtherAppNoAddGuarService.selectByModel(model));
    }

    /**
     * 新增
     *
     * @param rptOtherAppNoAddGuar
     * @return
     */
    @PostMapping("/insertGuar")
    protected ResultDto<Integer> insertGuar(@RequestBody RptOtherAppNoAddGuar rptOtherAppNoAddGuar) {
        rptOtherAppNoAddGuar.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOtherAppNoAddGuarService.insertSelective(rptOtherAppNoAddGuar));
    }

    /**
     * 修改
     *
     * @param rptOtherAppNoAddGuar
     * @return
     */
    @PostMapping("/updateGuar")
    protected ResultDto<Integer> updateGuar(@RequestBody RptOtherAppNoAddGuar rptOtherAppNoAddGuar) {
        return new ResultDto<Integer>(rptOtherAppNoAddGuarService.updateSelective(rptOtherAppNoAddGuar));
    }

    /**
     * 删除
     *
     * @param rptOtherAppNoAddGuar
     * @return
     */
    @PostMapping("/deleteGuar")
    protected ResultDto<Integer> deleteGuar(@RequestBody RptOtherAppNoAddGuar rptOtherAppNoAddGuar) {
        String pkId = rptOtherAppNoAddGuar.getPkId();
        return new ResultDto<Integer>(rptOtherAppNoAddGuarService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/selectByGrp")
    protected ResultDto<List<Map>> selectByGrp(@RequestBody QueryModel model){
        return new ResultDto<List<Map>>(rptOtherAppNoAddGuarService.selectByGrp(model));
    }
}
