/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtLadEval;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.MajorGradeInfo;
import cn.com.yusys.yusp.repository.mapper.MajorGradeInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-30 10:43:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class MajorGradeInfoService {
    private static final Logger log = LoggerFactory.getLogger(MajorGradeInfoService.class);

    @Autowired
    private MajorGradeInfoMapper majorGradeInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public MajorGradeInfo selectByPrimaryKey(String pkId) {
        return majorGradeInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<MajorGradeInfo> selectAll(QueryModel model) {
        List<MajorGradeInfo> records = (List<MajorGradeInfo>) majorGradeInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<MajorGradeInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<MajorGradeInfo> list = majorGradeInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(MajorGradeInfo record) {
        return majorGradeInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(MajorGradeInfo record) {
        return majorGradeInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(MajorGradeInfo record) {
        return majorGradeInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(MajorGradeInfo record) {
        return majorGradeInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return majorGradeInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return majorGradeInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据多授信申请流水号查询专业贷款评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<MajorGradeInfo> selectBySerno(String lmtSerno) {
        if(StringUtils.isBlank(lmtSerno)){
            log.info("传入参数为空!"+lmtSerno);
            return new ArrayList<>();
        }
        List<MajorGradeInfo> majorGradeInfos = majorGradeInfoMapper.selectByLmtSerno(lmtSerno);
        return majorGradeInfos;
    }
}
