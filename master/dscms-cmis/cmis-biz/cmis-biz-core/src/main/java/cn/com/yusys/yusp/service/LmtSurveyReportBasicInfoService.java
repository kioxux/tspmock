package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constant.LmtSurveyEnums;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.BasicAndSingDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04RespDto;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.LmtPerferRateApplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.bsp.ecif.s00101.S00101Service;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw04.Fbxw04Service;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportBasicInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSurveyReportBasicInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    @Autowired
    private LmtSurveyReportComInfoService lmtSurveyReportComInfoService;

    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    @Autowired
    private LmtModelApprResultInfoService lmtModelApprResultInfoService;

    @Autowired
    private LmtPerferRateApplyInfoService lmtPerferRateApplyInfoService;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private LmtPerferRateApplyInfoMapper lmtPerferRateApplyInfoMapper;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private Fbxw04Service fbxw04Service;
    @Autowired
    private S00101Service s00101Service;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Autowired
    private AreaAdminUserService areaAdminUserService;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSurveyReportBasicInfo selectByPrimaryKey(String surveySerno) {
        return lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSurveyReportBasicInfo> selectAll(QueryModel model) {
        List<LmtSurveyReportBasicInfo> records = (List<LmtSurveyReportBasicInfo>) lmtSurveyReportBasicInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyReportBasicInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyReportBasicInfo> list = lmtSurveyReportBasicInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSurveyReportBasicInfo record) {
        return lmtSurveyReportBasicInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSurveyReportBasicInfo record) {
        return lmtSurveyReportBasicInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSurveyReportBasicInfo record) {
        return lmtSurveyReportBasicInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSurveyReportBasicInfo record) {
        return lmtSurveyReportBasicInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveySerno) {
        return lmtSurveyReportBasicInfoMapper.deleteByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSurveyReportBasicInfoMapper.deleteByIds(ids);
    }

    /**
     * @param surveyReportBasicAndCom
     * @return int
     * @author 王玉坤
     * @date 2021/4/23 16:40
     * @version 1.0.0
     * @desc 模型审批<br>
     * 1、前往征信系统查询客户有无近30天征信报告
     * 2、若无，提示用户“客户无最近30天征信调查报告”
     * 3、若有，拼装参数请求零售智能风控进行模型审批
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto modelapprove(SurveyReportBasicAndCom surveyReportBasicAndCom) throws BizException, Exception {
        // 调查编号
        String surveySerno = "";
        // 有无近30天征信报告标识
//        Boolean creditFlag = Boolean.TRUE;
        // 模型审批临时对象
        LmtModelApprResultInfo lmtModelApprResultInfo = null;
        try {
            // 获取调查流水号
            surveySerno = surveyReportBasicAndCom.getSurveyReportBasicInfo().getSurveySerno();

            // 查询是否已经进行模型审批
            logger.info("调查流水号{}，查询是否已进行模型审批开始..............", surveySerno);
            lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);
            logger.info("调查流水号{}，查询是否已进行模型审批结束..............", surveySerno);
            if (lmtModelApprResultInfo != null) { // 若已进行模型审批，直接返回结果
                if (LmtSurveyEnums.STD_XD_JD_MODEL_STATUS_003.getValue().equals(lmtModelApprResultInfo.getModelRstStatus())) {
                    logger.info("调查流水号{}，模型审批已通过，请勿重新发起..............", surveySerno);
                    return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("模型审批已通过，请勿重新发起");
                } else if (LmtSurveyEnums.STD_XD_JD_MODEL_STATUS_004.getValue().equals(lmtModelApprResultInfo.getModelRstStatus())) {
                    logger.info("调查流水号{}，模型审批中，请勿重新发起..............", surveySerno);
                    return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("模型审批中，请勿重新发起");
                } else {
                    return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("模型审批拒绝，请勿重新发起");
                }
            }

            logger.info("调查流水号{}，保存调查报告基本信息、企业信息开始..............", surveySerno);
            // 1、保存调查报告基本信息、企业信息
            this.savebasicandcom(surveyReportBasicAndCom);
            logger.info("调查流水号{}，保存调查报告基本信息、企业信息结束..............", surveySerno);

            // 2、先查客户30天内征信报告信息 TODO 征信接口暂未提供
            logger.info("调查流水号{}，前往征信系统查询客户近30天征信信息开始..............", surveySerno);
            Boolean b = creditReportQryLstService.selectbyxw(surveyReportBasicAndCom.getSurveyReportBasicInfo().getCertCode());
            if (!b) {
                logger.info("调查流水号{}，未查询到客户近30天征信报告..............", surveySerno);
                return new ResultDto(Boolean.FALSE).code(-1).message("未查询到用户三十天内征信报告");
            }
            // 未查询到近30天征信报告


//                return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("未查询到客户近30天征信报告！");

            logger.info("调查流水号{}，前往征信系统查询客户近30天征信信息结束..............", surveySerno);

            // 3、前往风控进行模型审批调用 fbxw04 异步接口等待风控通知
            logger.info("调查流水号{}，前往零售智能风空进行模型审批开始..............", surveySerno);
            // 接口暂未通，做挡板 TODO
            LmtSurveyReportBasicInfo surveyReportBasicInfo = surveyReportBasicAndCom.getSurveyReportBasicInfo();
            LmtSurveyReportComInfo surveyReportComInfo = surveyReportBasicAndCom.getSurveyReportComInfo();
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveySerno);
            // 获客来源
            String cusChannel = lmtSurveyReportMainInfo.getCusChannel();// A-唤醒贷
            //参数拼接
            Fbxw04ReqDto fbxw04ReqDto = new Fbxw04ReqDto();
            //渠道来源
            fbxw04ReqDto.setChannel_type("11");
            //合作平台
            fbxw04ReqDto.setCo_platform("1002");
            //产品类别
            fbxw04ReqDto.setPrd_type("000020");
            //产品代码（零售智能风控内部代码）prd_code
            fbxw04ReqDto.setPrd_code("1002000020");
            // 产品代码（零售智能风控内部代码）prd_sub_code
            if("A".equals(cusChannel)){//(唤醒贷需要prd_sub_code)
                fbxw04ReqDto.setPrd_sub_code("1002100020");
            }
            //证件类型cert_type
            fbxw04ReqDto.setCert_type("10");
            //证件号码cert_code
            fbxw04ReqDto.setCert_code(surveyReportBasicInfo.getCertCode());
            //客户姓名cust_name
            fbxw04ReqDto.setCust_name(surveyReportBasicInfo.getCusName());
            //客户编号
            fbxw04ReqDto.setCust_id_core(surveyReportBasicInfo.getCusId());
            //移动电话phone
            fbxw04ReqDto.setPhone(surveyReportBasicInfo.getPhone());
            //核心客户号cust_id_core
            fbxw04ReqDto.setCust_id_core(surveyReportBasicInfo.getCusId());
            //企业名称company_name
            fbxw04ReqDto.setCompany_name(surveyReportComInfo.getConName());
            //企业证件号码company_cert_code
            fbxw04ReqDto.setCompany_cert_code(surveyReportComInfo.getCorpCertCode());
            //企业证件类型company_cert_type
            // 2021年11月18日19:42:13 hubp 问题编号：20211118-00023 转换企业证件类型码值
            //零售智能风控码值--》 05:统一社会信用代码    03：工商注册码   02：组织机构代码
            // 新信贷码值 --》 Q：组织机构代码  R：统一社会信用代码  M：工商注册码
            String corpCertType = surveyReportComInfo.getCorpCertType();
            String Company_cert_type = StringUtils.EMPTY;
            if("Q".equals(corpCertType)){
                Company_cert_type = "02";
            } else if ("R".equals(corpCertType)) {
                Company_cert_type = "05";
            } else if ("M".equals(corpCertType)) {
                Company_cert_type = "03";
            } else {
                Company_cert_type = corpCertType;
            }
            fbxw04ReqDto.setCompany_cert_type(Company_cert_type);
            //企业类型company_type
            fbxw04ReqDto.setCompany_type(surveyReportComInfo.getCorpType());
            //客户经理姓名biz_manager_name
            String name = OcaTranslatorUtils.getUserName(lmtSurveyReportMainInfo.getManagerId());
            fbxw04ReqDto.setBiz_manager_name(name);

            //客户经理号biz_manager_id
            fbxw04ReqDto.setBiz_manager_id(lmtSurveyReportMainInfo.getManagerId());
            //归属机构biz_org_id
            fbxw04ReqDto.setBiz_org_id(lmtSurveyReportMainInfo.getManagerBrId());
            //调查表编号survey_serno
            fbxw04ReqDto.setSurvey_serno(surveyReportBasicInfo.getSurveySerno());

            Fbxw04RespDto fbxw04RespDto = fbxw04Service.fbxw04(fbxw04ReqDto);

            lmtSurveyReportMainInfo.setBizSerno(fbxw04RespDto.getApp_no());
            int i = lmtSurveyReportMainInfoService.updateSelective(lmtSurveyReportMainInfo);
            if (i != 1) {
                logger.info("回存第三方流水号失败..............", surveySerno);
                return new ResultDto(false).message("回存第三方流水号失败");
            }
            logger.info("调查流水号{}，前往零售智能风空进行模型审批结束..............", surveySerno);
            //查询模型审批结果

            LmtModelApprResultInfo model = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);
            // 4、保存审批结果，挡板作用真正保存结果信息在接收风控信息时保存 TODO 现在更改状态为审批中
            if (model == null) {
                model = new LmtModelApprResultInfo();
                model.setSurveySerno(surveySerno);
                model.setModelRstStatus(LmtSurveyEnums.STD_XD_JD_MODEL_STATUS_004.getValue());
                int nums = lmtModelApprResultInfoService.insert(model);
                Asserts.isTrue(nums > 0, "模型结果数据库操作失败");
            } else {
                model.setModelRstStatus(LmtSurveyEnums.STD_XD_JD_MODEL_STATUS_004.getValue());
                int nums = lmtModelApprResultInfoService.update(model);
                Asserts.isTrue(nums > 0, "模型结果数据库操作失败");
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("调查流水号{}，模型审批结束..............", surveySerno);
        }
        return new ResultDto<Boolean>(Boolean.TRUE).code(0);
    }

    /**
     * @param surveyReportBasicAndCom
     * @return int
     * @author 王玉坤
     * @date 2021/4/23 16:42
     * @version 1.0.0
     * @desc 保存企业信息、调查报告基本信息
     * @修改历史: V1.0
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Integer> savebasicandcom(SurveyReportBasicAndCom surveyReportBasicAndCom) {
        logger.info("保存调查报告基本信息、企业信息开始..................");
        if (surveyReportBasicAndCom.getSurveyReportBasicInfo() != null) {
            //查询主表信息
            LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveyReportBasicAndCom.getSurveyReportBasicInfo().getSurveySerno());
            if (mainInfo != null) {
                if ("997".equals(mainInfo.getApproveStatus()) || "111".equals(mainInfo.getApproveStatus())) {
                    logger.info("审批中或审批通过的数据禁止修改.................");
                    return new ResultDto(0).message("审批中或审批通过的数据禁止修改");
                }
            }
        } else {
            if (surveyReportBasicAndCom.getSurveyReportComInfo() != null) {
                LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveyReportBasicAndCom.getSurveyReportBasicInfo().getSurveySerno());
                if (mainInfo != null) {
                    if ("997".equals(mainInfo.getApproveStatus()) || "111".equals(mainInfo.getApproveStatus())) {
                        logger.info("审批中或审批通过的数据禁止修改.................");
                        return new ResultDto(0).message("审批中或审批通过的数据禁止修改");
                    }
                }
            } else {
                return new ResultDto(0).message("保存参数为空");
            }
        }

        // 调查报告基本信息临时对象
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = null;
        // 调查报告企业信息临时对象
        LmtSurveyReportComInfo lmtSurveyReportComInfo = null;
        int result = 0;
        try {
            // 获取调查报告基本信息
            lmtSurveyReportBasicInfo = this.selectByPrimaryKey(surveyReportBasicAndCom.getSurveyReportBasicInfo().getSurveySerno());
            // 获取调查报告企业信息
            lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(surveyReportBasicAndCom.getSurveyReportComInfo().getSurveySerno());

            // 基本信息有无创建
            if (lmtSurveyReportBasicInfo == null) {
                //无基本信息
                result = this.insertSelective(surveyReportBasicAndCom.getSurveyReportBasicInfo());
                Asserts.isTrue(result > 0, "调查报告基本信息插入失败！");
            } else {

                result = this.updateSelective(surveyReportBasicAndCom.getSurveyReportBasicInfo());
                Asserts.isTrue(result > 0, "调查报告基本信息更新失败！");
            }

            // 企业信息有则更新，无责插入
            if (lmtSurveyReportComInfo == null) {
                //无基本信息
                result = lmtSurveyReportComInfoService.insertSelective(surveyReportBasicAndCom.getSurveyReportComInfo());
                Asserts.isTrue(result > 0, "调查报告企业信息更新失败！");
            } else {
                result = lmtSurveyReportComInfoService.updateSelective(surveyReportBasicAndCom.getSurveyReportComInfo());
                Asserts.isTrue(result > 0, "调查报告企业信息更新失败！");
            }

            //if

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("保存调查报告基本信息、企业信息结束..................");
        }
        return new ResultDto(result).message("操作成功");
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/21 14:14
     * @注释 无还本新增
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto savebasicandcomwhb(SurveyReportBasicAndCom surveyReportBasicAndCom) {
        int prdId = surveyReportBasicAndCom.getI();
        if (prdId != 7 && prdId != 14) {
            return new ResultDto(0).message("产品类型不合规范");
        }
        LmtSurveyReportBasicInfo surveyReportBasicInfo = surveyReportBasicAndCom.getSurveyReportBasicInfo();
        if (surveyReportBasicInfo == null) {
            return new ResultDto(0).message("基本信息为空");
        }
        if (StringUtils.isEmpty(surveyReportBasicInfo.getCusId())) {
            return new ResultDto(0).message("客户编号为空");
        }
        if (StringUtils.isEmpty(surveyReportBasicInfo.getCertCode())) {
            return new ResultDto(0).message("证件号码为空");
        }
        //先获取流水号
        String surveySerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
        surveyReportBasicAndCom.getSurveyReportComInfo().setSurveySerno(surveySerno);
        surveyReportBasicAndCom.getSurveyReportBasicInfo().setSurveySerno(surveySerno);
        ResultDto dto = this.savebasicandcom(surveyReportBasicAndCom);
        int i = 0;
        i = Integer.parseInt(dto.getData().toString());
        if (i == 1) {
            //去增加主表信息
            LmtSurveyReportMainInfo mainInfo = new LmtSurveyReportMainInfo();
            BeanUtils.beanCopy(surveyReportBasicAndCom.getSurveyReportBasicInfo(), mainInfo);
            mainInfo.setSurveySerno(surveySerno);
            //审批状态
            mainInfo.setApproveStatus("000");
            mainInfo.setCertType("A");
            //新增
            mainInfo.setOprType("01");
            //数据来源 人工新增
            mainInfo.setDataSource("01");
            if (prdId == 7) {
                //无还本普转
                mainInfo.setPrdName("无还本续贷-普转");
                //调查报告类型
                mainInfo.setSurveyType("7");
            } else {
                //无还本普转
                mainInfo.setPrdName("无还本续贷-优转");
                //调查报告类型
                mainInfo.setSurveyType("14");
            }

            //产品编号
            mainInfo.setPrdId("SC010010");
            //需要线下调查  是
            mainInfo.setIsStopOffline("1");
            //是否自动分配 否
            mainInfo.setIsAutodivis("0");

            LmtSurveyReportMainInfo mainInfo1 = lmtSurveyReportMainInfoService.setData(mainInfo);
            i = lmtSurveyReportMainInfoService.insert(mainInfo1);
        }
        return new ResultDto(i);
    }


    /**
     * @创建人 WH
     * @创建时间 2021-05-06 20:22
     * @注释 查询单条数据 返回给前端展示 包括模型审批结果之类的种种
     */
    public ResultDto selectbasicandcom(String surveySerno) {
        //先new返回对象
        LmtSurveyReportDto lmtSurveyReportDto = new LmtSurveyReportDto();

        //查询基本信息
        LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(surveySerno);
        if (basicInfo != null) {
            //放入基本信息
            lmtSurveyReportDto.setLmtSurveyReportBasicInfo(basicInfo);
        } else {

        }
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveySerno);
        if (mainInfo != null) {
            //是否双人调查  TODO 2021年6月23日17:21:08  这个时候去查询是否为双人调查
            AreaAdminUser areaAdminUser = areaAdminUserService.selectByPrimaryKey(mainInfo.getManagerId());
            if (areaAdminUser != null && areaAdminUser.getSurveyMode() != null) {
                lmtSurveyReportDto.setSurveyModel(areaAdminUser.getSurveyMode());
            }

            if (areaAdminUser != null && areaAdminUser.getIsNewEmployee() != null) {
                lmtSurveyReportDto.setIsNewEmployee(areaAdminUser.getIsNewEmployee());
            }
            //责任人工号
            if (!StringUtils.isEmpty(mainInfo.getManagerId())){
                lmtSurveyReportDto.setManagerId(mainInfo.getManagerId());
            }
            String marId = mainInfo.getMarId();
            //营销人工号
            if (!StringUtils.isEmpty(marId)){
                lmtSurveyReportDto.setMarId(marId);
            }
        }

        //查询企业信息
        LmtSurveyReportComInfo comInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(surveySerno);
        if (comInfo != null) {
            //放入企业信息
            lmtSurveyReportDto.setLmtSurveyReportComInfo(comInfo);
        }
        //查询模型审批信息
        LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);
        if (lmtModelApprResultInfo != null) {
            //放入模型审批结果
            lmtSurveyReportDto.setModel(lmtModelApprResultInfo.getModelRstStatus());
        }
        //查询利率审批信息
        LmtPerferRateApplyInfo lmtPerferRateApplyInfo = lmtPerferRateApplyInfoMapper.selectBySurveySerNo(surveySerno);
        if (lmtPerferRateApplyInfo != null) {
            lmtSurveyReportDto.setPerfer(lmtPerferRateApplyInfo.getApproveStatus());
        }
        return new ResultDto(lmtSurveyReportDto).code(0);
    }

    /**
     * @return 新增 修改  基本信息 企业信息逻辑
     * @创建人 wzy
     * @创建时间 15:57 2021-04-12
     **/
    @Transactional(rollbackFor = Exception.class)
    public int savebasic(LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        //基本信息有无创建
        int result;
        if (this.selectByPrimaryKey(lmtSurveyReportBasicInfo.getSurveySerno()) == null) {
            //String surveySerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            lmtSurveyReportBasicInfo.setSurveySerno(lmtSurveyReportBasicInfo.getSurveySerno());
            //无基本信息
            result = this.insertSelective(lmtSurveyReportBasicInfo);
        } else {
            result = this.updateSelective(lmtSurveyReportBasicInfo);
        }
        //企业信息有无创建
        return result;
    }

    /**
     * @param lmtSurveyReportBasicInfo
     * @return
     * @创建人 hubp
     * @创建时间 15:57 2021-05-13
     * @注释 查询双录系统，根据借款人证件类型+证件号码，返回调查流水号
     */
    @Transactional(rollbackFor = Exception.class)
    public List<BasicAndSingDto> selectSing(LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        String certCode = "";
        List<BasicAndSingDto> list = new ArrayList<>();
        try {
            certCode = lmtSurveyReportBasicInfo.getCertCode();
            if (StringUtils.isNotEmpty(certCode)) {
                //待接口提供数据
                BasicAndSingDto basicAndSingDto1 = new BasicAndSingDto();
                basicAndSingDto1.setSingNo("0000001");
                basicAndSingDto1.setSingDate("2021-03-11");
                BasicAndSingDto basicAndSingDto2 = new BasicAndSingDto();
                basicAndSingDto2.setSingNo("0000002");
                basicAndSingDto2.setSingDate("2021-03-12");
                list.add(basicAndSingDto1);
                list.add(basicAndSingDto2);
            }
            return list;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * @param lmtSurveyReportBasicInfo
     * @return cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto
     * @author hubp
     * @date 2021/5/22 14:54
     * @version 1.0.0
     * @desc 通过客户姓名和证件号码，查询客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public S00101RespDto selectCusInfo(LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        logger.info("查询客户信息开始..................证件号码：" + lmtSurveyReportBasicInfo.getSpouseCertCode());
        S00101RespDto s00101RespDto = null;
        try {
            S00101ReqDto s00101ReqDto = new S00101ReqDto();
            //根据证件类型和姓名查询
            s00101ReqDto.setResotp("3");
            //姓名
            s00101ReqDto.setCustna(lmtSurveyReportBasicInfo.getSpouseName());
            //证件类型
            s00101ReqDto.setIdtftp("100");
            //证件号码
            s00101ReqDto.setIdtfno(lmtSurveyReportBasicInfo.getSpouseCertCode());
            s00101RespDto = s00101Service.s00101(s00101ReqDto);
        } catch (YuspException e) {
            logger.info("查询客户信息失败..................证件号码：" + lmtSurveyReportBasicInfo.getSpouseCertCode());
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("查询客户信息结束..................证件号码：" + lmtSurveyReportBasicInfo.getSpouseCertCode());
        }
        return s00101RespDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/8 9:17
     * @注释 保存
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Integer> saveorupdate(LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyReportBasicInfo.getSurveySerno());
        if (mainInfo == null) {
            return new ResultDto<>(0).message("数据不存在，该数据可能已被删除");
        }
        LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(lmtSurveyReportBasicInfo.getSurveySerno());
        int i = 0;
        if (basicInfo == null) {
            //新增
            i = lmtSurveyReportBasicInfoMapper.insert(lmtSurveyReportBasicInfo);

        } else {
            //修改
            i = lmtSurveyReportBasicInfoMapper.updateByPrimaryKey(lmtSurveyReportBasicInfo);
        }
        // 复制数据，填充调查结论表
        LmtSurveyConInfo conInfo = new LmtSurveyConInfo();
        BeanUtils.beanCopy(lmtSurveyReportBasicInfo, conInfo);
        LmtSurveyConInfo conInfoTemp = lmtSurveyConInfoService.selectByPrimaryKey(lmtSurveyReportBasicInfo.getSurveySerno());
        if (Objects.isNull(conInfoTemp)) {
            i = lmtSurveyConInfoService.insert(conInfo);
        } else {
            i = lmtSurveyConInfoService.updateSelective(conInfo);
        }
        return new ResultDto<>(i).message("操作成功");
    }

    /**
     * @创建人 WB
     * @创建时间 2021-06-08 10:57
     * @注释 根据客户号查询单条数据
     */
    public List<LmtSurveyReportBasicInfo> selectBasiciInfoByCus(String certCoe) {
        QueryModel model = new QueryModel();
        model.addCondition("certCode", certCoe);
        model.addCondition("approveStatus", "111");
        List<LmtSurveyReportBasicInfo> list = lmtSurveyReportBasicInfoMapper.selectByModel(model);
        return list;
    }
    /**
     * @创建人 zrcbank-fengjj
     * @创建时间 2021-08-16 19:37
     * @注释 通过传入调查编号去找借据编号
     */
    public ResultDto<List<AccLoan>> selectOldBillBySurveySerno(String surveySerno){
        ResultDto<List<AccLoan>> result = new ResultDto<List<AccLoan>>();
        List<AccLoan> list = new ArrayList<>();
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(surveySerno);
        if(lmtSurveyReportBasicInfo != null && !StringUtils.isEmpty(lmtSurveyReportBasicInfo.getOldBillNo())){
            AccLoan accLoan = accLoanMapper.selectByBillNo(lmtSurveyReportBasicInfo.getOldBillNo());
            list.add(accLoan);
        }
        result.data(list);
        return  result;
    }

}
