package cn.com.yusys.yusp.web.server.xdht0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0008.req.Xdht0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0008.resp.Xdht0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdht0008.Xdht0008Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:借款合同双录流水同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0008:借款合同双录流水同步")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0008Resource.class);

    @Autowired
    private Xdht0008Service xdht0008Service;

    /**
     * 交易码：xdht0008
     * 交易描述：借款合同双录流水同步
     *
     * @param xdht0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款合同双录流水同步")
    @PostMapping("/xdht0008")
    protected @ResponseBody
    ResultDto<Xdht0008DataRespDto> xdht0008(@Validated @RequestBody Xdht0008DataReqDto xdht0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataReqDto));
        Xdht0008DataRespDto xdht0008DataRespDto = new Xdht0008DataRespDto();// 响应Dto:借款合同双录流水同步
        ResultDto<Xdht0008DataRespDto> xdht0008DataResultDto = new ResultDto<>();
        try {
            // 从xdht0008DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataReqDto));
            xdht0008DataRespDto=xdht0008Service.getXdht0008(xdht0008DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataRespDto));
            // TODO 调用XXXXXService层结束
            // TODO 封装xdht0008DataRespDto对象开始
            // TODO 封装xdht0008DataRespDto对象结束
            // 封装xdht0008DataResultDto中正确的返回码和返回信息
            xdht0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, e.getMessage());
            // 封装xdht0008DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0008DataRespDto到xdht0008DataResultDto中
        xdht0008DataResultDto.setData(xdht0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataRespDto));
        return xdht0008DataResultDto;
    }
}
