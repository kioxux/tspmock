package cn.com.yusys.yusp.service.client.lmt.cmislmt0010;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class CmisLmt0010Service
 * @date 2021/8/25 19:10
 * @desc 校验台账额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0010Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0010Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0010ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0010.resp.CmisLmt0010RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 校验台账额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0010RespDto cmisLmt0010(CmisLmt0010ReqDto cmisLmt0010ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010ReqDto));
        ResultDto<CmisLmt0010RespDto> cmisLmt0010ResultDto = cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010ResultDto));

        String cmisLmt0010Code = Optional.ofNullable(cmisLmt0010ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0010Meesage = Optional.ofNullable(cmisLmt0010ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0010RespDto cmisLmt0010RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0010ResultDto.getCode()) && cmisLmt0010ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0010ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0010RespDto = cmisLmt0010ResultDto.getData();
        } else {
            if (cmisLmt0010ResultDto.getData() != null) {
                cmisLmt0010Code = cmisLmt0010ResultDto.getData().getErrorCode();
                cmisLmt0010Meesage = cmisLmt0010ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0010Code = EpbEnum.EPB099999.key;
                cmisLmt0010Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0010Code, cmisLmt0010Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value);
        return cmisLmt0010RespDto;
    }
}
