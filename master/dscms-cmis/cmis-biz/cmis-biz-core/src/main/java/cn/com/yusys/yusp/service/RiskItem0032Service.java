package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Objects;

@Service
@Transactional
public class RiskItem0032Service {

    @Resource
    private ICusClientService iCusClientService;//注入客户服务接口

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0032Service.class);

    public RiskResultDto riskItem0032(String cusId) {
        String openday = stringRedisTemplate.opsForValue().get("openDay");

        RiskResultDto riskResultDto = new RiskResultDto();
        log.info("借款人企业营业执照过期校验开始*******************客户号：【{}】",cusId);
        if (StringUtils.isBlank(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //业务流水号为空
            return riskResultDto;
        }
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        if(Objects.isNull(cusBaseClientDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
            return riskResultDto;
        }
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
            if(Objects.isNull(cusCorpDto)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0012);
                return riskResultDto;
            }
            // 证件有效期
            if(Objects.isNull(DateUtils.parseDate(cusCorpDto.getCertIdate(),"yyyy-MM-dd"))) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03201);
                return riskResultDto;
            }
            // 证件有效期
            if(DateUtils.compare(DateUtils.parseDate(cusCorpDto.getCertIdate(),"yyyy-MM-dd"),DateUtils.parseDate(openday, "yyyy-MM-dd")) < 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03202);
                return riskResultDto;
            }
        }
        log.info("借款人企业营业执照过期校验结束*******************客户号：【{}】",cusId);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
