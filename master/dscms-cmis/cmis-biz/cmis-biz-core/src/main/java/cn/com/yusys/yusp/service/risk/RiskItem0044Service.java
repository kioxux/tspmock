package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.LmtCrdReplyInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/19 19:06
 * @desc 合同日期与批复日期校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0044Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0044Service.class);
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/7/19 19:08
     * @version 1.0.0
     * @desc 合同日期与批复日期校验
     * 1. 批复起始日+宽限期 <当前日  拦截
     * 2. 合同起止日，不在在批复期有效内 拦截
     * 3. 零售按揭类产品，批复超过三个月自动失效，非按揭类产品，批复超过一个月自动失效   需添加批量任务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0044(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = queryModel.getCondition().get("bizId").toString();
        log.info("*************合同日期与批复日期校验***********【{}】", contNo);
        String replySerno = StringUtils.EMPTY;
        String prdType = StringUtils.EMPTY;
        if (StringUtils.isBlank(contNo)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        // 通过合同信息获取批复编号
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(contNo);
        if (null == ctrLoanCont || StringUtils.isBlank(ctrLoanCont.getReplyNo()) || StringUtils.isBlank(ctrLoanCont.getPrdId())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("合同信息获取异常！");
            return riskResultDto;
        } else {
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(ctrLoanCont.getPrdId());
            //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
            //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
            prdType = CfgPrdBasicinfoDto.getData().getPrdType();;
            replySerno = ctrLoanCont.getReplyNo();
        }
        // 通过流水号去获取批复信息
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replySerno);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (StringUtils.isBlank(ctrLoanCont.getContStartDate()) || StringUtils.isBlank(lmtCrdReplyInfo.getReplyEndDate())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("合同起始日期或批复到期日获取异常！");
            return riskResultDto;
        } else {
            try {
                // 1. 零售按揭类产品，批复超过三个月自动失效，非按揭类产品，批复超过一个月自动失效   需添加批量任务
                if(prdType.equals("10")){
                    if(dateFormat.parse(ctrLoanCont.getContStartDate()).compareTo(DateUtils.addDay(dateFormat.parse(lmtCrdReplyInfo.getReplyEndDate()),90)) > 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("批复超过三个月自动失效！");
                        return riskResultDto;
                    }
                } else if (prdType.equals("11")){
                    if(dateFormat.parse(ctrLoanCont.getContStartDate()).compareTo(DateUtils.addDay(dateFormat.parse(lmtCrdReplyInfo.getReplyEndDate()),30)) > 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("批复超过一个月自动失效！");
                        return riskResultDto;
                    }
                }
                // 2. 批复起始日+宽限期 <当前日  拦截 宽限期默认30天每月
                Date date = DateUtils.addDay(dateFormat.parse(lmtCrdReplyInfo.getReplyStartDate()), lmtCrdReplyInfo.getLmtGraper() * 30);
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                log.info("合同日期与批复日期校验,当前日期：【{}】,批复起始日+宽限期：【{}】", openday,date.toString());
                if (dateFormat.parse(openday).compareTo(date) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc("批复起始日加宽限期必须大于当前日期！");
                    return riskResultDto;
                }
                // 3. 合同起止日，不在在批复期有效内 拦截
                if (dateFormat.parse(lmtCrdReplyInfo.getReplyEndDate()).compareTo(DateUtils.addDay(dateFormat.parse(ctrLoanCont.getContStartDate()), lmtCrdReplyInfo.getLmtGraper() * 30)) < 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc("贷款合同起始日大于批复到期日！");
                    return riskResultDto;
                }
            } catch (ParseException e) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("日期格式不正确！");
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************合同日期与批复日期校验***********【{}】", contNo);
        return riskResultDto;
    }
}
