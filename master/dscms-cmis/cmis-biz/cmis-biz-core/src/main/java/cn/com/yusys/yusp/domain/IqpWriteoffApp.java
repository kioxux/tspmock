/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpWriteoffApp
 * @类描述: iqp_writeoff_app数据实体类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-01-26 16:40:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_writeoff_app")
public class IqpWriteoffApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户主管客户经理 **/
	@Column(name = "ACC_MANAGER_ID", unique = false, nullable = true, length = 20)
	private String accManagerId;

	/** 核销笔数 **/
	@Column(name = "WRITEOFF_QNT", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal writeoffQnt;

	/** 借据总金额(元) **/
	@Column(name = "TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalAmt;

	/** 借据总余额(元) **/
	@Column(name = "TOTAL_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalBal;

	/** 总表内欠息 **/
	@Column(name = "TOTAL_INNER_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalInnerInt;

	/** 总表外欠息 **/
	@Column(name = "TOTAL_OUT_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalOutInt;

	/** 总核销本金 **/
	@Column(name = "TOTAL_WRITEOFF_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWriteoffCap;

	/** 总核销利息 **/
	@Column(name = "TOTAL_WRITEOFF_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWriteoffInt;

	/** 核销理由 **/
	@Column(name = "WRITEOFF_RESN", unique = false, nullable = true, length = 250)
	private String writeoffResn;

	/** 是否保留追诉权 STD_ZB_YES_NO **/
	@Column(name = "IS_SAVE_RGT_RES", unique = false, nullable = true, length = 5)
	private String isSaveRgtRes;

	/** 备注 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 250)
	private String memo;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;


	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	/**
	 * @return iqpSerno
	 */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param accManagerId
	 */
	public void setAccManagerId(String accManagerId) {
		this.accManagerId = accManagerId;
	}

	/**
	 * @return accManagerId
	 */
	public String getAccManagerId() {
		return this.accManagerId;
	}

	/**
	 * @param writeoffQnt
	 */
	public void setWriteoffQnt(java.math.BigDecimal writeoffQnt) {
		this.writeoffQnt = writeoffQnt;
	}

	/**
	 * @return writeoffQnt
	 */
	public java.math.BigDecimal getWriteoffQnt() {
		return this.writeoffQnt;
	}

	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}

	/**
	 * @return totalAmt
	 */
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}

	/**
	 * @param totalBal
	 */
	public void setTotalBal(java.math.BigDecimal totalBal) {
		this.totalBal = totalBal;
	}

	/**
	 * @return totalBal
	 */
	public java.math.BigDecimal getTotalBal() {
		return this.totalBal;
	}

	/**
	 * @param totalInnerInt
	 */
	public void setTotalInnerInt(java.math.BigDecimal totalInnerInt) {
		this.totalInnerInt = totalInnerInt;
	}

	/**
	 * @return totalInnerInt
	 */
	public java.math.BigDecimal getTotalInnerInt() {
		return this.totalInnerInt;
	}

	/**
	 * @param totalOutInt
	 */
	public void setTotalOutInt(java.math.BigDecimal totalOutInt) {
		this.totalOutInt = totalOutInt;
	}

	/**
	 * @return totalOutInt
	 */
	public java.math.BigDecimal getTotalOutInt() {
		return this.totalOutInt;
	}

	/**
	 * @param totalWriteoffCap
	 */
	public void setTotalWriteoffCap(java.math.BigDecimal totalWriteoffCap) {
		this.totalWriteoffCap = totalWriteoffCap;
	}

	/**
	 * @return totalWriteoffCap
	 */
	public java.math.BigDecimal getTotalWriteoffCap() {
		return this.totalWriteoffCap;
	}

	/**
	 * @param totalWriteoffInt
	 */
	public void setTotalWriteoffInt(java.math.BigDecimal totalWriteoffInt) {
		this.totalWriteoffInt = totalWriteoffInt;
	}

	/**
	 * @return totalWriteoffInt
	 */
	public java.math.BigDecimal getTotalWriteoffInt() {
		return this.totalWriteoffInt;
	}

	/**
	 * @param writeoffResn
	 */
	public void setWriteoffResn(String writeoffResn) {
		this.writeoffResn = writeoffResn;
	}

	/**
	 * @return writeoffResn
	 */
	public String getWriteoffResn() {
		return this.writeoffResn;
	}

	/**
	 * @param isSaveRgtRes
	 */
	public void setIsSaveRgtRes(String isSaveRgtRes) {
		this.isSaveRgtRes = isSaveRgtRes;
	}

	/**
	 * @return isSaveRgtRes
	 */
	public String getIsSaveRgtRes() {
		return this.isSaveRgtRes;
	}

	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return memo
	 */
	public String getMemo() {
		return this.memo;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}


}