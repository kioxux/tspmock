/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRelatedGrpCustomers
 * @类描述: rpt_basic_info_related_grp_customers数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:30:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_related_grp_customers")
public class RptBasicInfoRelatedGrpCustomers extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 集团或关联企业简介 **/
	@Column(name = "GRP_BRIEF", unique = false, nullable = true, length = 65535)
	private String grpBrief;
	
	/** 集团全称 **/
	@Column(name = "GRP_NAME_TOTAL", unique = false, nullable = true, length = 40)
	private String grpNameTotal;
	
	/** 集团经营模式 **/
	@Column(name = "GRP_OPER_MODE", unique = false, nullable = true, length = 5)
	private String grpOperMode;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 经营所涉行业 **/
	@Column(name = "OPER_TRADE", unique = false, nullable = true, length = 5)
	private String operTrade;
	
	/** 主体评级 **/
	@Column(name = "SUBJECT_LEVEL", unique = false, nullable = true, length = 5)
	private String subjectLevel;
	
	/** 现五级分类 **/
	@Column(name = "CURRENT_FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String currentFiveClass;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCap;
	
	/** 实际控制人客户号 **/
	@Column(name = "REAL_OPER_CUS_ID", unique = false, nullable = true, length = 40)
	private String realOperCusId;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 40)
	private String legal;
	
	/** 注册地址 **/
	@Column(name = "COM_REG_ADD", unique = false, nullable = true, length = 200)
	private String comRegAdd;
	
	/** 实际经营地址 **/
	@Column(name = "OPER_ADDR_ACT", unique = false, nullable = true, length = 200)
	private String operAddrAct;
	
	/** 租用土地面积 **/
	@Column(name = "RENT_LAND_SQU", unique = false, nullable = true, length = 40)
	private String rentLandSqu;
	
	/** 租用厂房面积 **/
	@Column(name = "RENT_WORKSHOP_SQU", unique = false, nullable = true, length = 40)
	private String rentWorkshopSqu;
	
	/** 租用年租金 **/
	@Column(name = "RENT_AMT_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentAmtYear;
	
	/** 自有土地面积 **/
	@Column(name = "SELF_LAND_SQU", unique = false, nullable = true, length = 40)
	private String selfLandSqu;
	
	/** 自有有证土地面积 **/
	@Column(name = "SELF_CERT_LAND_SQU", unique = false, nullable = true, length = 40)
	private String selfCertLandSqu;
	
	/** 自有有证土地性质 **/
	@Column(name = "SELF_CERT_LAND_CHA", unique = false, nullable = true, length = 5)
	private String selfCertLandCha;
	
	/** 自有房产面积 **/
	@Column(name = "SELF_HOUSE_SQU", unique = false, nullable = true, length = 40)
	private String selfHouseSqu;
	
	/** 自有有证房产面积 **/
	@Column(name = "SELF_CERT_HOUSE_SQU", unique = false, nullable = true, length = 40)
	private String selfCertHouseSqu;
	
	/** 集团经营范围 **/
	@Column(name = "GRP_NAT_BUSI", unique = false, nullable = true, length = 40)
	private String grpNatBusi;
	
	/** 主营业务1 **/
	@Column(name = "MAIN_BUSI1", unique = false, nullable = true, length = 40)
	private String mainBusi1;
	
	/** 主营业务1占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI1_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi1Memo;
	
	/** 主营业务2 **/
	@Column(name = "MAIN_BUSI2", unique = false, nullable = true, length = 40)
	private String mainBusi2;
	
	/** 主营业务2占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI2_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi2Memo;
	
	/** 主营业务3 **/
	@Column(name = "MAIN_BUSI3", unique = false, nullable = true, length = 40)
	private String mainBusi3;
	
	/** 主营业务3占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI3_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi3Memo;
	
	/** 其他需说明的事项 **/
	@Column(name = "OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String otherNeedDesc;
	
	/** 说明近期增减资情况 **/
	@Column(name = "RECEND_RAISE_REDCE_AMT_CASE", unique = false, nullable = true, length = 65535)
	private String recendRaiseRedceAmtCase;

	/** 说明近期增减资情况 **/
	@Column(name = "ENTERPRISES_OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String enterprisesOtherDesc;

	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param grpBrief
	 */
	public void setGrpBrief(String grpBrief) {
		this.grpBrief = grpBrief;
	}
	
    /**
     * @return grpBrief
     */
	public String getGrpBrief() {
		return this.grpBrief;
	}
	
	/**
	 * @param grpNameTotal
	 */
	public void setGrpNameTotal(String grpNameTotal) {
		this.grpNameTotal = grpNameTotal;
	}
	
    /**
     * @return grpNameTotal
     */
	public String getGrpNameTotal() {
		return this.grpNameTotal;
	}
	
	/**
	 * @param grpOperMode
	 */
	public void setGrpOperMode(String grpOperMode) {
		this.grpOperMode = grpOperMode;
	}
	
    /**
     * @return grpOperMode
     */
	public String getGrpOperMode() {
		return this.grpOperMode;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param operTrade
	 */
	public void setOperTrade(String operTrade) {
		this.operTrade = operTrade;
	}
	
    /**
     * @return operTrade
     */
	public String getOperTrade() {
		return this.operTrade;
	}
	
	/**
	 * @param subjectLevel
	 */
	public void setSubjectLevel(String subjectLevel) {
		this.subjectLevel = subjectLevel;
	}
	
    /**
     * @return subjectLevel
     */
	public String getSubjectLevel() {
		return this.subjectLevel;
	}
	
	/**
	 * @param currentFiveClass
	 */
	public void setCurrentFiveClass(String currentFiveClass) {
		this.currentFiveClass = currentFiveClass;
	}
	
    /**
     * @return currentFiveClass
     */
	public String getCurrentFiveClass() {
		return this.currentFiveClass;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param paidCap
	 */
	public void setPaidCap(java.math.BigDecimal paidCap) {
		this.paidCap = paidCap;
	}
	
    /**
     * @return paidCap
     */
	public java.math.BigDecimal getPaidCap() {
		return this.paidCap;
	}
	
	/**
	 * @param realOperCusId
	 */
	public void setRealOperCusId(String realOperCusId) {
		this.realOperCusId = realOperCusId;
	}
	
    /**
     * @return realOperCusId
     */
	public String getRealOperCusId() {
		return this.realOperCusId;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param comRegAdd
	 */
	public void setComRegAdd(String comRegAdd) {
		this.comRegAdd = comRegAdd;
	}
	
    /**
     * @return comRegAdd
     */
	public String getComRegAdd() {
		return this.comRegAdd;
	}
	
	/**
	 * @param operAddrAct
	 */
	public void setOperAddrAct(String operAddrAct) {
		this.operAddrAct = operAddrAct;
	}
	
    /**
     * @return operAddrAct
     */
	public String getOperAddrAct() {
		return this.operAddrAct;
	}
	
	/**
	 * @param rentLandSqu
	 */
	public void setRentLandSqu(String rentLandSqu) {
		this.rentLandSqu = rentLandSqu;
	}
	
    /**
     * @return rentLandSqu
     */
	public String getRentLandSqu() {
		return this.rentLandSqu;
	}
	
	/**
	 * @param rentWorkshopSqu
	 */
	public void setRentWorkshopSqu(String rentWorkshopSqu) {
		this.rentWorkshopSqu = rentWorkshopSqu;
	}
	
    /**
     * @return rentWorkshopSqu
     */
	public String getRentWorkshopSqu() {
		return this.rentWorkshopSqu;
	}
	
	/**
	 * @param rentAmtYear
	 */
	public void setRentAmtYear(java.math.BigDecimal rentAmtYear) {
		this.rentAmtYear = rentAmtYear;
	}
	
    /**
     * @return rentAmtYear
     */
	public java.math.BigDecimal getRentAmtYear() {
		return this.rentAmtYear;
	}
	
	/**
	 * @param selfLandSqu
	 */
	public void setSelfLandSqu(String selfLandSqu) {
		this.selfLandSqu = selfLandSqu;
	}
	
    /**
     * @return selfLandSqu
     */
	public String getSelfLandSqu() {
		return this.selfLandSqu;
	}
	
	/**
	 * @param selfCertLandSqu
	 */
	public void setSelfCertLandSqu(String selfCertLandSqu) {
		this.selfCertLandSqu = selfCertLandSqu;
	}
	
    /**
     * @return selfCertLandSqu
     */
	public String getSelfCertLandSqu() {
		return this.selfCertLandSqu;
	}
	
	/**
	 * @param selfCertLandCha
	 */
	public void setSelfCertLandCha(String selfCertLandCha) {
		this.selfCertLandCha = selfCertLandCha;
	}
	
    /**
     * @return selfCertLandCha
     */
	public String getSelfCertLandCha() {
		return this.selfCertLandCha;
	}
	
	/**
	 * @param selfHouseSqu
	 */
	public void setSelfHouseSqu(String selfHouseSqu) {
		this.selfHouseSqu = selfHouseSqu;
	}
	
    /**
     * @return selfHouseSqu
     */
	public String getSelfHouseSqu() {
		return this.selfHouseSqu;
	}
	
	/**
	 * @param selfCertHouseSqu
	 */
	public void setSelfCertHouseSqu(String selfCertHouseSqu) {
		this.selfCertHouseSqu = selfCertHouseSqu;
	}
	
    /**
     * @return selfCertHouseSqu
     */
	public String getSelfCertHouseSqu() {
		return this.selfCertHouseSqu;
	}
	
	/**
	 * @param grpNatBusi
	 */
	public void setGrpNatBusi(String grpNatBusi) {
		this.grpNatBusi = grpNatBusi;
	}
	
    /**
     * @return grpNatBusi
     */
	public String getGrpNatBusi() {
		return this.grpNatBusi;
	}
	
	/**
	 * @param mainBusi1
	 */
	public void setMainBusi1(String mainBusi1) {
		this.mainBusi1 = mainBusi1;
	}
	
    /**
     * @return mainBusi1
     */
	public String getMainBusi1() {
		return this.mainBusi1;
	}
	
	/**
	 * @param mainBusi1Memo
	 */
	public void setMainBusi1Memo(String mainBusi1Memo) {
		this.mainBusi1Memo = mainBusi1Memo;
	}
	
    /**
     * @return mainBusi1Memo
     */
	public String getMainBusi1Memo() {
		return this.mainBusi1Memo;
	}
	
	/**
	 * @param mainBusi2
	 */
	public void setMainBusi2(String mainBusi2) {
		this.mainBusi2 = mainBusi2;
	}
	
    /**
     * @return mainBusi2
     */
	public String getMainBusi2() {
		return this.mainBusi2;
	}
	
	/**
	 * @param mainBusi2Memo
	 */
	public void setMainBusi2Memo(String mainBusi2Memo) {
		this.mainBusi2Memo = mainBusi2Memo;
	}
	
    /**
     * @return mainBusi2Memo
     */
	public String getMainBusi2Memo() {
		return this.mainBusi2Memo;
	}
	
	/**
	 * @param mainBusi3
	 */
	public void setMainBusi3(String mainBusi3) {
		this.mainBusi3 = mainBusi3;
	}
	
    /**
     * @return mainBusi3
     */
	public String getMainBusi3() {
		return this.mainBusi3;
	}
	
	/**
	 * @param mainBusi3Memo
	 */
	public void setMainBusi3Memo(String mainBusi3Memo) {
		this.mainBusi3Memo = mainBusi3Memo;
	}
	
    /**
     * @return mainBusi3Memo
     */
	public String getMainBusi3Memo() {
		return this.mainBusi3Memo;
	}
	
	/**
	 * @param otherNeedDesc
	 */
	public void setOtherNeedDesc(String otherNeedDesc) {
		this.otherNeedDesc = otherNeedDesc;
	}
	
    /**
     * @return otherNeedDesc
     */
	public String getOtherNeedDesc() {
		return this.otherNeedDesc;
	}
	
	/**
	 * @param recendRaiseRedceAmtCase
	 */
	public void setRecendRaiseRedceAmtCase(String recendRaiseRedceAmtCase) {
		this.recendRaiseRedceAmtCase = recendRaiseRedceAmtCase;
	}
	
    /**
     * @return recendRaiseRedceAmtCase
     */
	public String getRecendRaiseRedceAmtCase() {
		return this.recendRaiseRedceAmtCase;
	}

	public String getEnterprisesOtherDesc() {
		return enterprisesOtherDesc;
	}

	public void setEnterprisesOtherDesc(String enterprisesOtherDesc) {
		this.enterprisesOtherDesc = enterprisesOtherDesc;
	}
}