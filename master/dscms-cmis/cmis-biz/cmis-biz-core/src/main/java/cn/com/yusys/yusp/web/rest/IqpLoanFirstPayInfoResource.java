/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanFirstPayInfo;
import cn.com.yusys.yusp.service.IqpLoanFirstPayInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanFirstPayInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-27 16:46:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "购房首付款来源情况信息")
@RequestMapping("/api/iqploanfirstpayinfo")
public class IqpLoanFirstPayInfoResource {
    @Autowired
    private IqpLoanFirstPayInfoService iqpLoanFirstPayInfoService;

	/**
     *
     * 全表查询.
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanFirstPayInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanFirstPayInfo> list = iqpLoanFirstPayInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanFirstPayInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanFirstPayInfo>> index(QueryModel queryModel) {
        List<IqpLoanFirstPayInfo> list = iqpLoanFirstPayInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanFirstPayInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{bizNo}")
    protected ResultDto<IqpLoanFirstPayInfo> show(@PathVariable("bizNo") String bizNo) {
        IqpLoanFirstPayInfo iqpLoanFirstPayInfo = iqpLoanFirstPayInfoService.selectByPrimaryKey(bizNo);
        return new ResultDto<IqpLoanFirstPayInfo>(iqpLoanFirstPayInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanFirstPayInfo> create(@RequestBody IqpLoanFirstPayInfo iqpLoanFirstPayInfo) throws URISyntaxException {
        iqpLoanFirstPayInfoService.insert(iqpLoanFirstPayInfo);
        return new ResultDto<IqpLoanFirstPayInfo>(iqpLoanFirstPayInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanFirstPayInfo iqpLoanFirstPayInfo) throws URISyntaxException {
        int result = iqpLoanFirstPayInfoService.update(iqpLoanFirstPayInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{bizNo}")
    protected ResultDto<Integer> delete(@PathVariable("bizNo") String bizNo) {
        int result = iqpLoanFirstPayInfoService.deleteByPrimaryKey(bizNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteByPrimaryKey
     * @函数描述: 根据主键删除购房首付款来源情况信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPrimaryKey")
    protected ResultDto<Integer> deleteByPrimaryKey(@RequestBody IqpLoanFirstPayInfo iqpLoanFirstPayInfo) {
        int result = iqpLoanFirstPayInfoService.deleteByPrimaryKey(iqpLoanFirstPayInfo.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanFirstPayInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /***
     * @param iqpLoanFirstPayInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author wzy
     * @date 2021/4/24 9:43
     * @version 1.0.0
     * @desc    首付款来源情况核实-修改-保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("首付款来源情况核实-修改-保存")
    @PostMapping("/saveiqploanfirstpayinfo")
    protected ResultDto<Integer> queryIqpLoanAppDramPlanSub(@RequestBody IqpLoanFirstPayInfo iqpLoanFirstPayInfo) throws URISyntaxException {
        int result = iqpLoanFirstPayInfoService.saveiqploanfirstpayinfo(iqpLoanFirstPayInfo);
        return new ResultDto<Integer>(result);
    }

    /***
     * @param iqpLoanFirstPayInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub>>
     * @author wzy
     * @date 2021/4/24 11:06
     * @version 1.0.0
     * @desc    根据流水号查首付款来源列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据流水号查首付款来源列表")
    @PostMapping("/selectBySerno")
    protected ResultDto<List<IqpLoanFirstPayInfo>> selectBySerno(@RequestBody IqpLoanFirstPayInfo iqpLoanFirstPayInfo) {
        List<IqpLoanFirstPayInfo> list = (List<IqpLoanFirstPayInfo>) iqpLoanFirstPayInfoService.selectBySerno(iqpLoanFirstPayInfo.getIqpSerno());
        return new ResultDto<List<IqpLoanFirstPayInfo>>(list);
    }
}
