package cn.com.yusys.yusp.web.server.xdxw0052;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0052.Xdxw0052Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0052.req.Xdxw0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0052.resp.Xdxw0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:查询优抵贷客户调查表
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0052:查询优抵贷客户调查表")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0052Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0052Resource.class);

    @Autowired
    private Xdxw0052Service xdxw0052Service;

    /**
     * 交易码：xdxw0052
     * 交易描述：查询优抵贷客户调查表
     *
     * @param xdxw0052DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷客户调查表")
    @PostMapping("/xdxw0052")
    protected @ResponseBody
    ResultDto<Xdxw0052DataRespDto> xdxw0052(@Validated @RequestBody Xdxw0052DataReqDto xdxw0052DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataReqDto));
        Xdxw0052DataRespDto xdxw0052DataRespDto = new Xdxw0052DataRespDto();// 响应Dto:查询优抵贷客户调查表
        ResultDto<Xdxw0052DataRespDto> xdxw0052DataResultDto = new ResultDto<>();
        String queryType = xdxw0052DataReqDto.getQueryType();//查询类型
        String indgtSerno = xdxw0052DataReqDto.getIndgtSerno();//客户调查表编号
        String applySerno = xdxw0052DataReqDto.getApplySerno();//业务唯一编号
        try {
            // 从xdxw0052DataReqDto获取业务值进行业务逻辑处理
            // 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataReqDto));
            xdxw0052DataRespDto = xdxw0052Service.xdxw0052(xdxw0052DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataRespDto));
            // 调用XXXXXService层结束
            // 封装xdxw0052DataResultDto中正确的返回码和返回信息
            xdxw0052DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0052DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, e.getMessage());
            // 封装xdxw0052DataResultDto中异常返回码和返回信息
            xdxw0052DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0052DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0052DataRespDto到xdxw0052DataResultDto中
        xdxw0052DataResultDto.setData(xdxw0052DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataResultDto));
        return xdxw0052DataResultDto;
    }
}
