package cn.com.yusys.yusp.service.client.lmt.cmislmt0014;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class CmisLmt0014Service
 * @date 2021/8/25 19:10
 * @desc 台账恢复交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0014Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0014Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param [cmisLmt0014ReqDto]
     * @return cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 台账恢复交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0014RespDto cmisLmt0014(CmisLmt0014ReqDto cmisLmt0014ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(cmisLmt0014ReqDto));
        ResultDto<CmisLmt0014RespDto> cmisLmt0014ResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(cmisLmt0014ResultDto));

        String cmisLmt0014Code = Optional.ofNullable(cmisLmt0014ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0014Meesage = Optional.ofNullable(cmisLmt0014ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0014RespDto cmisLmt0014RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014ResultDto.getCode()) && cmisLmt0014ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0014RespDto = cmisLmt0014ResultDto.getData();
        } else {
            if (cmisLmt0014ResultDto.getData() != null) {
                cmisLmt0014Code = cmisLmt0014ResultDto.getData().getErrorCode();
                cmisLmt0014Meesage = cmisLmt0014ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0014Code = EpbEnum.EPB099999.key;
                cmisLmt0014Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0014Code, cmisLmt0014Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value);
        return cmisLmt0014RespDto;
    }
}
