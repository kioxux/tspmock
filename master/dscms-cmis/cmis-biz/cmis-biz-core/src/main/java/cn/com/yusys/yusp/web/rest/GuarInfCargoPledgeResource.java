/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarBaseInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import cn.com.yusys.yusp.service.GuarInfCargoPledgeService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfCargoPledgeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfcargopledge")
public class GuarInfCargoPledgeResource {
    @Autowired
    private GuarInfCargoPledgeService guarInfCargoPledgeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfCargoPledge>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfCargoPledge> list = guarInfCargoPledgeService.selectAll(queryModel);
        return new ResultDto<List<GuarInfCargoPledge>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfCargoPledge>> index(QueryModel queryModel) {
        List<GuarInfCargoPledge> list = guarInfCargoPledgeService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfCargoPledge>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfCargoPledge> show(@PathVariable("serno") String serno) {
        GuarInfCargoPledge guarInfCargoPledge = guarInfCargoPledgeService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfCargoPledge>(guarInfCargoPledge);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfCargoPledge> create(@RequestBody GuarInfCargoPledge guarInfCargoPledge) throws URISyntaxException {
        guarInfCargoPledgeService.insert(guarInfCargoPledge);
        return new ResultDto<GuarInfCargoPledge>(guarInfCargoPledge);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfCargoPledge guarInfCargoPledge) throws URISyntaxException {
        int result = guarInfCargoPledgeService.update(guarInfCargoPledge);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfCargoPledgeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfCargoPledgeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfo
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarCargoPledge")
    protected ResultDto<Integer> preserveGuarCargoPledge(@RequestBody  GuarInfCargoPledge guarInfCargoPledge) {
        int result = guarInfCargoPledgeService.preserveGuarCargoPledge(guarInfCargoPledge);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfCargoPledge> queryBySerno(@RequestBody GuarInfCargoPledge record) {
        GuarInfCargoPledge guarInfCargoPledge = guarInfCargoPledgeService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfCargoPledge>(guarInfCargoPledge);
    }
}
