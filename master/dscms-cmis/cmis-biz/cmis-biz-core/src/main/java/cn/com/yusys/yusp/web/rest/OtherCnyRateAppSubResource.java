/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateAppSub;
import cn.com.yusys.yusp.service.OtherCnyRateAppSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 17:22:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othercnyrateappsub")
public class OtherCnyRateAppSubResource {
    @Autowired
    private OtherCnyRateAppSubService otherCnyRateAppSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherCnyRateAppSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherCnyRateAppSub> list = otherCnyRateAppSubService.selectAll(queryModel);
        return new ResultDto<List<OtherCnyRateAppSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherCnyRateAppSub>> index(QueryModel queryModel) {
        List<OtherCnyRateAppSub> list = otherCnyRateAppSubService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateAppSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{subSerno}")
    protected ResultDto<OtherCnyRateAppSub> show(@PathVariable("subSerno") String subSerno) {
        OtherCnyRateAppSub otherCnyRateAppSub = otherCnyRateAppSubService.selectByPrimaryKey(subSerno);
        return new ResultDto<OtherCnyRateAppSub>(otherCnyRateAppSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherCnyRateAppSub> create(@RequestBody OtherCnyRateAppSub otherCnyRateAppSub) throws URISyntaxException {
        otherCnyRateAppSubService.insert(otherCnyRateAppSub);
        return new ResultDto<OtherCnyRateAppSub>(otherCnyRateAppSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherCnyRateAppSub otherCnyRateAppSub) throws URISyntaxException {
        int result = otherCnyRateAppSubService.update(otherCnyRateAppSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{subSerno}")
    protected ResultDto<Integer> delete(@PathVariable("subSerno") String subSerno) {
        int result = otherCnyRateAppSubService.deleteByPrimaryKey(subSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherCnyRateAppSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    
    @PostMapping("/updateAll/{subSerno}")
    protected ResultDto<Integer> updateAll(@PathVariable String subSerno) {
    	int result = otherCnyRateAppSubService.updateAll(subSerno);
    	return new ResultDto<Integer>(result);
    }
    /**
     * @函数描述: 根据申请流水号查询
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryModel")
    protected ResultDto<List<OtherCnyRateAppSub>> selectOtherCnyRateAppSubByQueryModel(@RequestBody QueryModel queryModel) {
        List<OtherCnyRateAppSub> list = otherCnyRateAppSubService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateAppSub>>(list);
    }
}
