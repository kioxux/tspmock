package cn.com.yusys.yusp.web.server.xdht0033;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0033.req.Xdht0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0033.resp.Xdht0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0033.Xdht0033Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0033:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0033Resource.class);
    @Autowired
    private Xdht0033Service xdht0033Service;
    /**
     * 交易码：xdht0033
     * 交易描述：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
     *
     * @param xdht0033DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询小贷是否具有客户调查合同并返回合同金额")
    @PostMapping("/xdht0033")
    protected @ResponseBody
    ResultDto<Xdht0033DataRespDto> xdht0033(@Validated @RequestBody Xdht0033DataReqDto xdht0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataReqDto));
        Xdht0033DataRespDto xdht0033DataRespDto = new Xdht0033DataRespDto();// 响应Dto:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
        ResultDto<Xdht0033DataRespDto> xdht0033DataResultDto = new ResultDto<>();
        try {
            // 从xdht0033DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0033DataReqDto));
            xdht0033DataRespDto = xdht0033Service.xdht0033(xdht0033DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0033DataResultDto));
            // 封装xdht0033DataResultDto中正确的返回码和返回信息
            xdht0033DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0033DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, e.getMessage());
            // 封装xdht0033DataResultDto中异常返回码和返回信息
            xdht0033DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0033DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0033DataRespDto到xdht0033DataResultDto中
        xdht0033DataResultDto.setData(xdht0033DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataResultDto));
        return xdht0033DataResultDto;
    }
}