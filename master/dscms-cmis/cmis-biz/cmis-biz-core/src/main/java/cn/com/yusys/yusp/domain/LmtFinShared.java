/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: LmtFinShared
 * @类描述: lmt_fin_shared数据实体类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-03-03 10:32:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fin_shared")
public class LmtFinShared extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated("GenerationType.UUID")
	@Column(name = "SERNO")
	private String serno;
	
	/** 融资协议编号 **/
	@Column(name = "FIN_CTR_NO", unique = false, nullable = true, length = 40)
	private String finCtrNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 32)
	private String certCode;
	
	/** 授信类型 STD_ZB_LMT_APP_TYPE **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 担保类别 STD_ZB_GUAR_TYP **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 评级结果 STD_ZB_EVAL_RST **/
	@Column(name = "EVAL_RESULT", unique = false, nullable = true, length = 5)
	private String evalResult;
	
	/** 共享范围 STD_ZB_SHR_SCOPE **/
	@Column(name = "SHARED_SCOPE", unique = false, nullable = true, length = 5)
	private String sharedScope;
	
	/** 担保放大倍数 **/
	@Column(name = "GUAR_BAIL_MULTIPLE", unique = false, nullable = true, length = 16)
	private BigDecimal guarBailMultiple;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal guarAmt;
	
	/** 单户限额 **/
	@Column(name = "SINGLE_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal singleAmt;
	
	/** 担保总敞口限额 **/
	@Column(name = "GUAR_TOTL_SPAC", unique = false, nullable = true, length = 16)
	private BigDecimal guarTotlSpac;
	
	/** 授信起始日期 **/
	@Column(name = "LMT_STAR_DATE", unique = false, nullable = true, length = 10)
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	@Column(name = "LMT_END_DATE", unique = false, nullable = true, length = 10)
	private String lmtEndDate;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param finCtrNo
	 */
	public void setFinCtrNo(String finCtrNo) {
		this.finCtrNo = finCtrNo;
	}
	
    /**
     * @return finCtrNo
     */
	public String getFinCtrNo() {
		return this.finCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param evalResult
	 */
	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult;
	}
	
    /**
     * @return evalResult
     */
	public String getEvalResult() {
		return this.evalResult;
	}
	
	/**
	 * @param sharedScope
	 */
	public void setSharedScope(String sharedScope) {
		this.sharedScope = sharedScope;
	}
	
    /**
     * @return sharedScope
     */
	public String getSharedScope() {
		return this.sharedScope;
	}
	
	/**
	 * @param guarBailMultiple
	 */
	public void setGuarBailMultiple(BigDecimal guarBailMultiple) {
		this.guarBailMultiple = guarBailMultiple;
	}
	
    /**
     * @return guarBailMultiple
     */
	public BigDecimal getGuarBailMultiple() {
		return this.guarBailMultiple;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public BigDecimal getGuarAmt() {
		return this.guarAmt;
	}

	public BigDecimal getSingleAmt() {
		return singleAmt;
	}

	public void setSingleAmt(BigDecimal singleAmt) {
		this.singleAmt = singleAmt;
	}

	/**
	 * @param guarTotlSpac
	 */
	public void setGuarTotlSpac(BigDecimal guarTotlSpac) {
		this.guarTotlSpac = guarTotlSpac;
	}
	
    /**
     * @return guarTotlSpac
     */
	public BigDecimal getGuarTotlSpac() {
		return this.guarTotlSpac;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate;
	}
	
    /**
     * @return lmtStarDate
     */
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}