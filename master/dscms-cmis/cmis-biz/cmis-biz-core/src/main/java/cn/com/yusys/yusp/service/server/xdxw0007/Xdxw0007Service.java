package cn.com.yusys.yusp.service.server.xdxw0007;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0007.resp.Xdxw0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.web.server.xdxw0007.BizXdxw0007Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 批复信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0007Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0007Service.class);

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    /**
     * 批复信息查询
     * @param contNo
     * @return
     */
    @Transactional
    public Xdxw0007DataRespDto queryByContNo(String contNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, contNo);
        Xdxw0007DataRespDto xdxw0007DataRespDto = null;
        try {
            xdxw0007DataRespDto = lmtCrdReplyInfoMapper.queryByContNo(contNo);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007DataRespDto));
        return xdxw0007DataRespDto;
    }
}
