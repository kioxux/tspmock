package cn.com.yusys.yusp.service.server.xdtz0058;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.req.Xdtz0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.resp.Xdtz0058DataRespDto;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 业务逻辑类:通过借据号查询借据信息
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdtz0058Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0058Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CmisCus0006Service cmisCus0006Service;

    /**
     * 交易码：xdtz0058
     * 交易描述：通过借据号查询借据信息
     *
     * @param xdtz0058DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0058DataRespDto xdtz0058(Xdtz0058DataReqDto xdtz0058DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataReqDto));
        Xdtz0058DataRespDto xdtz0058DataRespDto = new Xdtz0058DataRespDto();
        CmisCus0006ReqDto cmisCus0006ReqDto = new CmisCus0006ReqDto();//请求Dto：查询客户基本信息
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();//响应Dto：查询客户基本信息
        try {
            // 从xdtz0058DataReqDto获取业务值进行业务逻辑处理
            String queryType = xdtz0058DataReqDto.getQueryType();//查询类型
            String billNo = xdtz0058DataReqDto.getBillNo(); // 借据编号
            String certNo = xdtz0058DataReqDto.getCertNo();
            Map queryMap = new HashMap<>();
            queryMap.put("bill_no", billNo);//借据编号
            if (Objects.equals(queryType, DscmsBizTzEnum.QUERY_TYPE_LOANMONTHFORMACCLOAN.key)) {
                //根据借据号查询信贷房贷贷款期限
                int loanMonth = accLoanMapper.queryLoanMonthFormAccLoan(queryMap);
                xdtz0058DataRespDto.setLoanTerm(String.valueOf(loanMonth));////贷款期限
            } else if (Objects.equals(queryType, DscmsBizTzEnum.QUERY_TYPE_LOANAMOUNTFORMACCLOAN.key)) {
                //根据借据号查询信贷房贷借据金额
                BigDecimal loanAmt = accLoanMapper.queryLoanAmountFormAccLoan(queryMap);
                xdtz0058DataRespDto.setLoanAmt(String.valueOf(loanAmt));//借据金额
            } else if (Objects.equals(queryType, DscmsBizTzEnum.QUERY_TYPE_FINABRIDFROMACCLOAN.key)) {
                //根据借据号取信贷账务机构
                String finaBrId = accLoanMapper.queryFinaBrIdFromAccLoan(queryMap);
                xdtz0058DataRespDto.setFinaBrId(finaBrId);////账务机构编号
            } else if (Objects.equals(queryType, DscmsBizTzEnum.QUERY_TYPE_OVERTIMESTOTALFROMACCLOAN.key)) {
                //通过客户证件号查询客户信息
                cmisCus0006ReqDto.setCertCode(certNo);
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
                cmisCus0006RespDto = cmisCus0006Service.cmisCus0006(cmisCus0006ReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
                List<CusBaseDto> cusBaseList = cmisCus0006RespDto.getCusBaseList();
                // 客户ids
                List<String> cusIds = cusBaseList.parallelStream()
                        .filter(cusBaseDto -> Objects.equals(certNo, cusBaseDto.getCertCode()))//判断 请求参数中客户证件号和cusBaseDto中证件号是否相等
                        .map(cusBaseDto -> cusBaseDto.getCusId()) // 获取cusBaseDto中客户号
                        .collect(Collectors.toList());// 转换成List
                // 根据借据号查询本行其他贷款拖欠情况
                queryMap.put("cusIds", cusIds);//客户号
                int overTimesTotal = accLoanMapper.queryOverTimesTotalFromAccLoan(queryMap);
                xdtz0058DataRespDto.setOverdueTimesTotal(String.valueOf(overTimesTotal));
            }
            xdtz0058DataRespDto.setBillNo(billNo);
        }catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataRespDto));
        return xdtz0058DataRespDto;
    }
}
