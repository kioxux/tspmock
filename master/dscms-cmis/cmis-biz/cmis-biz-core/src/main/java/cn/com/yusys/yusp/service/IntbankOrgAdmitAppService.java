/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAcc;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAppr;
import cn.com.yusys.yusp.domain.LmtAppRelCusInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.IntbankOrgAdmitAppMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:26:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IntbankOrgAdmitAppService extends BizInvestCommonService{
    // 日志
    private static final Logger log = LoggerFactory.getLogger(IntbankOrgAdmitAppService.class);

    @Autowired
    private IntbankOrgAdmitAppMapper intbankOrgAdmitAppMapper;

    @Autowired
    private IntbankOrgAdmitApprService intbankOrgAdmitApprService;

    @Autowired
    private IntbankOrgAdmitAccService intbankOrgAdmitAccService;

    @Autowired
    private IntbankOrgAdmitReplyService intbankOrgAdmitReplyService;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService ;

    @Autowired
    private LmtAppRelGrpLimitService lmtAppRelGrpLimitService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IntbankOrgAdmitApp selectByPrimaryKey(String pkId) {
        return intbankOrgAdmitAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IntbankOrgAdmitApp> selectAll(QueryModel model) {
        List<IntbankOrgAdmitApp> records = (List<IntbankOrgAdmitApp>) intbankOrgAdmitAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IntbankOrgAdmitApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IntbankOrgAdmitApp> list = intbankOrgAdmitAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectApproveByCusId
     * @方法描述: 查询在途的申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IntbankOrgAdmitApp> selectApproveByCusId(QueryModel model) {
        List<IntbankOrgAdmitApp> list = intbankOrgAdmitAppMapper.selectApproveByCusId(model);
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IntbankOrgAdmitApp record) {
        return intbankOrgAdmitAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IntbankOrgAdmitApp record) {
        return intbankOrgAdmitAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IntbankOrgAdmitApp record) {
        //增加更新信息
        //初始化（更新）通用domain信息
        initUpdateDomainProperties(record);
        return intbankOrgAdmitAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IntbankOrgAdmitApp record) {
        return intbankOrgAdmitAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return intbankOrgAdmitAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return intbankOrgAdmitAppMapper.deleteByIds(ids);
    }

    /**
     * 新增、保存
     * 1.保存同业机构准入(申请、年审、调整)信息<br/>
     * 2.保存同业机构准入申请（企业基本信息-申请）
     * 3.保存同业机构准入申请（企业股东信息-申请）
     * @param intbankOrgAdmitAppMap
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertIntbankOrgAdmitAppApply(Map intbankOrgAdmitAppMap) {
        /**
         * 判断是否有在途的同业机构准入申请、调整申请、年审申请
         * add by zhangjw 20210813
         */
        IntbankOrgAdmitApp intbankOrgAdmitApp = new IntbankOrgAdmitApp();
        mapToBean(intbankOrgAdmitAppMap,intbankOrgAdmitApp);
        String cusId = intbankOrgAdmitApp.getCusId();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId", cusId);
        List<IntbankOrgAdmitApp> intbankOrgAdmitApps = selectApproveByCusId(queryModel);
        if(intbankOrgAdmitApps!=null && intbankOrgAdmitApps.size()>0){
            IntbankOrgAdmitApp app = intbankOrgAdmitApps.get(0);
            String appType = app.getAppType();
            if(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_01.equals(appType)){
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000008.key, EclEnum.INTBANK_ERROR_000008.value);
            }else if(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_02.equals(appType)){
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000009.key, EclEnum.INTBANK_ERROR_000009.value);
            }else if(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_03.equals(appType)){
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000010.key, EclEnum.INTBANK_ERROR_000010.value);
            }else{
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000011.key, EclEnum.INTBANK_ERROR_000011.value);
            }
        }


        /**
         * 当申请类型是新增时，判断客户是否存在生效的准入名单信息，若存在，则不允许新增
         * 当申请类型是调整时，判断客户是否存在生效的准入名单信息，若不存在，则不允许新增
         * add by zhangjw 20210813
         */
        if (CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADD.equals(intbankOrgAdmitApp.getAppType()) || CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADJUST.equals(intbankOrgAdmitApp.getAppType())) {
            QueryModel model = new QueryModel();
            model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
            model.addCondition("cusId",cusId);
            model.addCondition("accStatus",CmisBizConstants.STD_REPLY_STATUS_01);
            List<IntbankOrgAdmitAcc> accs = intbankOrgAdmitAccService.selectByModel(model);

            if (accs != null && accs.size() > 0 && CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADD.equals(intbankOrgAdmitApp.getAppType())) {
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000012.key, EclEnum.INTBANK_ERROR_000012.value);
            }

            if((accs == null || accs.size() <= 0) && CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADJUST.equals(intbankOrgAdmitApp.getAppType()) ){
                throw BizException.error(null, EclEnum.INTBANK_ERROR_000013.key, EclEnum.INTBANK_ERROR_000013.value);
            }
        }

        //1.保存同业机构准入申请信息
        String newSerno = (String) intbankOrgAdmitAppMap.get("newSerno");
        String newPkId = (String) intbankOrgAdmitAppMap.get("newPkId");


        insertIntbanlOrgAdmitApp(intbankOrgAdmitApp,newSerno,newPkId);
        //2.保存同业机构准入申请（企业基本信息-申请） -- 同业机构没有客户类型
        lmtAppRelCusInfoService.insertCusInfoApp(newSerno,intbankOrgAdmitApp.getCusId(),null,CmisBizConstants.STD_ZB_CUS_CATALOG_3,true);

        //添加集团额度信息（lmt_app_rel_grp_limit）
        lmtAppRelGrpLimitService.insertCusLmtInfo(newSerno, intbankOrgAdmitApp.getCusId(), null, CmisBizConstants.STD_ZB_CUS_CATALOG_3);

        //判断是否为申请类型
        if (CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADD.equals(intbankOrgAdmitApp.getAppType())){

//            lmtAppRelCusInfoService.insertlmtAppRelCusInfoByIntbankOrgAdmitAppApply(appSerno,intbankOrgAdmitApp.getCusId());
            //3.保存同业机构准入申请（企业股东信息-申请）
//            lmtAppCorreShdService.insertLmtAppCorreShdByIntbankOrgAdmitAppApply(appSerno,intbankOrgAdmitApp.getCusId());
        }
    }

    /**
     * 保存同业机构准入申请信息
     * @param intbankOrgAdmitApp
     */
    private void insertIntbanlOrgAdmitApp(IntbankOrgAdmitApp intbankOrgAdmitApp, String newSerno, String newPkId) {
        //准入到期日默认为原到期日顺延12个月
        intbankOrgAdmitApp.setTerm(12);
        //【经办机构】默认为当前登记机构
        intbankOrgAdmitApp.setManagerBrId(getCurrentUser().getOrg().getCode());
        //【投资经理】默认显示为当前登陆
        intbankOrgAdmitApp.setManagerId(getCurrentUser().getLoginCode());
        //初始化通用属性
        initInsertDomainProperties(intbankOrgAdmitApp);
        //使用前台获取 ，防止重复提交
        intbankOrgAdmitApp.setPkId(newPkId);
        //使用前台获取 ，防止重复提交
        intbankOrgAdmitApp.setSerno(newSerno);

        intbankOrgAdmitAppMapper.insert(intbankOrgAdmitApp);
    }

    /**
     * 同业机构准入申请(通过、否决 录入批复表中)
     * @param intbankOrgAdmitApp
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleAfterEnd(IntbankOrgAdmitApp intbankOrgAdmitApp, String currentUserId, String currentOrgId,String approveStatus) {
        //准入申请流水号
        String serno = intbankOrgAdmitApp.getSerno();
        //客户编号
        String cusId = intbankOrgAdmitApp.getCusId();

        try {
            //针对流程到办结节点，进行以下处理
            //更新同业机构准入申请表-申请状态
            intbankOrgAdmitApp.setApproveStatus(approveStatus);
            update(intbankOrgAdmitApp);

            /**
             *  获取最新准入信息
             *  1.对比准入申请表（intbank_org_admit_app）和准入审批表(intbank_org_admit_appr)，优先获取准入审批表中的数据进入到准入批复表和台账表
             *  2.台账表 判断是否存在有效数据，是 则新增该记录，否则，更新有效数据
             */
            //批复流水号
            String replySerno = "";
            //获取最新同业机构准入审批记录
            IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprService.queryFinalApprBySerno(serno);
            //存在审批记录
            replySerno = intbankOrgAdmitReplyService.generateAdmitReplyHandleByAppr(intbankOrgAdmitAppr, currentUserId, currentOrgId,intbankOrgAdmitApp.getApproveStatus());
            //update by lizx 20210720 流程审批结束，审批表中一定会存在记录
            /*// 3.根据最新的授信审批表中的数据生成批复数据
            if (intbankOrgAdmitAppr != null){
            }else{
                //不存在审批记录 查询申请表
                replySerno = intbankOrgAdmitReplyService.generateAdmitReplyHandleByAppr(intbankOrgAdmitApp, currentUserId, currentOrgId,intbankOrgAdmitApp.getApproveStatus());
            }*/
            // 4.根据最新的批复数据生成台账数据(拒绝不操作台账数据)
            if (CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
                intbankOrgAdmitAccService.generateAdmitAccHandleByReply(replySerno,cusId, currentUserId, currentOrgId);
            }

            //推送首页提醒事项 机构准入不需要消息推送
            ///this.sendWbMsgNotice(intbankOrgAdmitAppr);

        } catch (Exception e) {
            log.error("同业机构准入申请监听操作失败！==》",e.getMessage());
            if (e instanceof BizException){
                throw e;
            }
            throw BizException.error(null,EclEnum.INTBANK_ORG_APP_WORKFLOW_FAILED.key,EclEnum.INTBANK_ORG_APP_WORKFLOW_FAILED.value);
        }
    }

    private void sendWbMsgNotice(IntbankOrgAdmitAppr intbankOrgAdmitAppr) {
        log.info("同业机构准入审批流程申请启用授信申报审批流程【{}】，流程打回或退回操作，推送首页提醒事项", intbankOrgAdmitAppr.getSerno());
        Map<String, String> map = new HashMap<>();
        map.put("cusName", intbankOrgAdmitAppr.getCusName());
        if (CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_01.equals(intbankOrgAdmitAppr.getAppType())){
            map.put("prdName", "同业机构准入");
        }
        if (CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_02.equals(intbankOrgAdmitAppr.getAppType())){
            map.put("prdName", "准入名单年审");
        }
        if (CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE_01.equals(intbankOrgAdmitAppr.getAppType())){
            map.put("prdName", "准入名单调整");
        }
        ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(intbankOrgAdmitAppr.getInputId());
        sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1",intbankOrgAdmitAppr.getInputId(),byLoginCode.getData().getUserMobilephone());
    }

    /**
     * 根据申请流水号获取同业准入申请详情
     * @param serno
     * @return
     */
    public IntbankOrgAdmitApp selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<IntbankOrgAdmitApp> intbankOrgAdmitApps = intbankOrgAdmitAppMapper.selectByModel(queryModel);
        if (intbankOrgAdmitApps!= null && intbankOrgAdmitApps.size()>0){
            return intbankOrgAdmitApps.get(0);
        }
        return null;
    }

    /**
     * 逻辑删除
     * 1.判断当前申请状态是否为未发起 逻辑删除
     * 2.判断当前申请状态是否为退回，修改为自行退出
     * @param serno
     * @return
     */
    @Transactional
    public int deleteLogicBySerno(String serno) {
        log.info("同业机构准入申请删除开始。。。。");
        IntbankOrgAdmitApp intbankOrgAdmitApp = selectBySerno(serno);
        if (intbankOrgAdmitApp != null){
            //2.判断当前申请状态是否为退回，修改为自行退出
            if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(intbankOrgAdmitApp.getApproveStatus())){
                log.info("流程删除==》bizId->{}",serno);
                workflowCoreClient.deleteByBizId(serno);
                intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                return intbankOrgAdmitAppMapper.updateByPrimaryKey(intbankOrgAdmitApp);
            }
            //1.判断当前申请状态是否为未发起 逻辑删除
            if (CmisBizConstants.APPLY_STATE_TODO.equals(intbankOrgAdmitApp.getApproveStatus())){
                intbankOrgAdmitApp.setOprType(CmisBizConstants.OPR_TYPE_02);
                return intbankOrgAdmitAppMapper.updateByPrimaryKey(intbankOrgAdmitApp);
            }
        }else{
            log.error("未找到该记录，删除失败！====>{}",serno);
            BizException.error(null,EclEnum.INTBANK_ERROR_000005.key,EclEnum.INTBANK_ERROR_000005.value);
        }
        log.info("同业机构准入申请删除结束。。。。");
        return 0;
    }

    /**
     * 获取同业机构准入基本信息
     * @param serno
     * @return
     */
    public Map<String,Object> selectBasicInfoBySerno(String serno) {
        Map<String,Object> result = new HashMap<String,Object>();
        LmtAppRelCusInfo lmtAppRelCusInfo = null;
        IntbankOrgAdmitApp intbankOrgAdmitApp = selectBySerno(serno);
        if (intbankOrgAdmitApp != null) {
            Map condition = new HashMap();
            condition.put("serno", serno);
            condition.put("cusId", intbankOrgAdmitApp.getCusId());
            lmtAppRelCusInfo = lmtAppRelCusInfoService.selectBySernoAndCusId(condition);
            //单独设置，用于接口返回框架进行数据查询Name反显
            result.put("inputBrId",intbankOrgAdmitApp.getInputBrId());
            result.put("inputId",intbankOrgAdmitApp.getInputId());
            result.put("updBrId",intbankOrgAdmitApp.getUpdBrId());
            result.put("updId",intbankOrgAdmitApp.getUpdId());
        }
        result.put("intbankOrgAdmitApp", intbankOrgAdmitApp);
        result.put("lmtAppRelCusInfo", lmtAppRelCusInfo);
        return result;
    }


    /**
     * 审批送下一步生成审批表
     *
     * @param intbankOrgAdmitApp
     * @param currentUserId
     * @param currentOrgId
     */
    @Transactional(rollbackFor=Exception.class)
    public void generateIntbankOrgAdmitApprInfo(IntbankOrgAdmitApp intbankOrgAdmitApp, String currentUserId, String currentOrgId,String issueReportType) {
        //更新申请表至审批中状态
        intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
        intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
        intbankOrgAdmitApprService.insertOrgAmditApprInfo(intbankOrgAdmitApp,currentUserId,currentOrgId,issueReportType);
    }

    /**
     * 检测是否重复申请
     * @param cusId
     * @return
     */
    public int selectRecordsByCusId(String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<String> status = new ArrayList<>();
        status.add(CmisBizConstants.APPLY_STATE_QUIT);
        status.add(CmisBizConstants.APPLY_STATE_PASS);
        status.add(CmisBizConstants.APPLY_STATE_REFUSE);
        queryModel.addCondition("noApproves",status);
        List<IntbankOrgAdmitApp> intbankOrgAdmitApps = selectAll(queryModel);
        if (intbankOrgAdmitApps!=null){
            return intbankOrgAdmitApps.size();
        }
        return 0;
    }

    /**
     * 准入名单调整
     * @param intbankOrgAdmitAccMap
     * @return
     */
    @Transactional
    public IntbankOrgAdmitApp adjustIntbankOrgAdmitAppApply(Map intbankOrgAdmitAccMap) {
        IntbankOrgAdmitAcc intbankOrgAdmitAcc = new IntbankOrgAdmitAcc();
        mapToBean(intbankOrgAdmitAccMap,intbankOrgAdmitAcc);

        String newSerno = (String) intbankOrgAdmitAccMap.get("newSerno");
        String newPkId = (String) intbankOrgAdmitAccMap.get("newPkId");


        IntbankOrgAdmitApp intbankOrgAdmitApp = null;
        try {
            String origiReplySerno = intbankOrgAdmitAcc.getReplySerno();
            //检测当前申请是否存在正在流转的调整流程
            checkIsExistRunningAdjust(origiReplySerno);

            //1.保存同业机构准入申请信息
            intbankOrgAdmitApp = new IntbankOrgAdmitApp();
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

            //复制相同属性
            BeanUtils.copyProperties(intbankOrgAdmitAcc,intbankOrgAdmitApp);
            //使用前台生成，防止重复提交
            intbankOrgAdmitApp.setPkId(newPkId);
            //使用前台生成，防止重复提交
            intbankOrgAdmitApp.setSerno(newSerno);

            intbankOrgAdmitApp.setOrigiReplySerno(intbankOrgAdmitAcc.getReplySerno());
            //申请类型为 调整
            intbankOrgAdmitApp.setAppType(CmisBizConstants.STD_ZB_ORG_ADMIT_TYPE.ADJUST);
            intbankOrgAdmitApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //申请状态初始化为 待发起
            intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
            //【经办机构】默认为当前登记机构
            intbankOrgAdmitApp.setManagerBrId(getCurrentUser().getOrg().getCode());
            //【投资经理】默认显示为当前登陆
            intbankOrgAdmitApp.setManagerId(getCurrentUser().getLoginCode());
            intbankOrgAdmitApp.setInputId(getCurrentUser().getLoginCode());
            intbankOrgAdmitApp.setInputBrId(getCurrentUser().getOrg().getCode());
            //modify by zhangjw 20210819  准入名单调整发起时，登记日期默认为当天
            intbankOrgAdmitApp.setInputDate(openDay);
            intbankOrgAdmitApp.setUpdId(getCurrentUser().getLoginCode());
            intbankOrgAdmitApp.setUpdBrId(getCurrentUser().getOrg().getCode());
            intbankOrgAdmitApp.setUpdDate(openDay);
            intbankOrgAdmitApp.setUpdateTime(getCurrrentDate());
            intbankOrgAdmitApp.setCreateTime(getCurrrentDate());
            insert(intbankOrgAdmitApp);
            //2.保存同业机构准入申请（企业基本信息-申请） -- 同业机构没有客户类型
            lmtAppRelCusInfoService.insertCusInfoApp(newSerno,intbankOrgAdmitApp.getCusId(),null,CmisBizConstants.STD_ZB_CUS_CATALOG_3,true);

            //添加集团额度信息（lmt_app_rel_grp_limit）
            lmtAppRelGrpLimitService.insertCusLmtInfo(newSerno, intbankOrgAdmitApp.getCusId(), null, CmisBizConstants.STD_ZB_CUS_CATALOG_3);

        } catch (Exception e) {
            log.error("同业机构准入调整报错==》",e.getMessage());
            if (e instanceof BizException){
                throw e;
            }
            throw BizException.error(null,EclEnum.INTBANK_ORG_APP_ADJUST_FAILED.key,EclEnum.INTBANK_ORG_APP_ADJUST_FAILED.value);
        }
        return intbankOrgAdmitApp;
    }

    /**
     * 检测当前申请是否存在正在流转的调整流程
     * @param origiReplySerno
     */
    private void checkIsExistRunningAdjust(String origiReplySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("origiReplySerno",origiReplySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<IntbankOrgAdmitApp> intbankOrgAdmitApps = selectAll(queryModel);
        for (IntbankOrgAdmitApp intbankOrgAdmitApp : intbankOrgAdmitApps) {
            String approveStatus = intbankOrgAdmitApp.getApproveStatus();
            if (!CmisBizConstants.APPLY_STATE_REFUSE.equals(approveStatus)
                    && !CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)
                    && !CmisBizConstants.APPLY_STATE_QUIT.equals(approveStatus)){
                throw BizException.error(null, EclEnum.INTBANK_ORG_APP_ADJUST_EXIST_RUN.key,EclEnum.INTBANK_ORG_APP_ADJUST_EXIST_RUN.value);
            }
        }
    }

    /**
     * 审批打回至客户经理处，将审批表数据copy至申请表
     * @param intbankOrgAdmitApp
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterCallBack(IntbankOrgAdmitApp intbankOrgAdmitApp, String currentUserId, String currentOrgId, String approveStatus) {
        log.info(this.getClass().getName()+"流程打回退回到发起人，数据从审批表，更新到申请表中");
        String serno = intbankOrgAdmitApp.getSerno();
        //针对流程到办结节点，进行以下处理
        intbankOrgAdmitApp.setApproveStatus(approveStatus);
//        update(intbankOrgAdmitApp);

        IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprService.queryFinalApprBySerno(serno);
        if (intbankOrgAdmitAppr == null ){
            throw BizException.error(null, EclEnum.INTBANK_ERROR_000006.key,EclEnum.INTBANK_ERROR_000006.value);
        }

        copyProperties(intbankOrgAdmitAppr,intbankOrgAdmitApp,"pkId");
        //初始化（更新）通用domain信息
        initUpdateDomainProperties(intbankOrgAdmitApp);
        update(intbankOrgAdmitApp);
    }

    /**
     * @方法名称: getRouterMapResult
     * @方法描述: 获取路由结果集-同业授信申报审批流程
     * @参数与返回说明: map
     * @算法描述:
     * @创建人: zhangjw
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> getRouterMapResult(ResultInstanceDto resultInstanceDto, String serno) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        //流程路由1：金融市场总部风险合规部负责人是否等于金融市场总部总裁
        String isEqualJRSCBZC = commonService.getIsEqualJRSCBZC();
        resultMap.put("isEqualJRSCBZC", isEqualJRSCBZC);

        return resultMap;
    }

    /**
     * 更新图片绝对路径
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        IntbankOrgAdmitApp intbankOrgAdmitApp = new IntbankOrgAdmitApp();
        intbankOrgAdmitApp.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/" + getCurrrentDateStr();
        if (!cn.com.yusys.yusp.commons.util.StringUtils.isBlank(fileId)) {
            picAbsolutePath = getFileAbsolutePath(fileId, serverPath, relativePath, fanruanFileTemplate);
        }
        setValByKey(intbankOrgAdmitApp, key, picAbsolutePath);
        updateSelective(intbankOrgAdmitApp);
        return picAbsolutePath;


    }
}
