/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprBackItem
 * @类描述: lmt_appr_back_item数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 11:03:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_appr_back_item")
public class LmtApprBackItem extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 扣分项目编号 **/
	@Column(name = "BACK_ITEM_ID", unique = false, nullable = true, length = 40)
	private String backItemId;
	
	/** 扣分项目名称 **/
	@Column(name = "BACK_ITEM_NAME", unique = false, nullable = true, length = 512)
	private String backItemName;
	
	/** 扣分标准 **/
	@Column(name = "STD_BACK_ITEM_POINT", unique = false, nullable = true, length = 10)
	private Integer stdBackItemPoint;
	
	/** 扣分分值 **/
	@Column(name = "BACK_ITEM_POINT", unique = false, nullable = true, length = 10)
	private Integer backItemPoint;
	
	/** 是否存在问题 **/
	@Column(name = "IS_EXIST_ISSUE", unique = false, nullable = true, length = 5)
	private String isExistIssue;
	
	/** 存在问题描述 **/
	@Column(name = "EXIST_ISSUE_DESC", unique = false, nullable = true, length = 2000)
	private String existIssueDesc;
	
	/** 扣分项目排序 **/
	@Column(name = "BACK_ITEM_ORDER", unique = false, nullable = true, length = 10)
	private Integer backItemOrder;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param backItemId
	 */
	public void setBackItemId(String backItemId) {
		this.backItemId = backItemId;
	}
	
    /**
     * @return backItemId
     */
	public String getBackItemId() {
		return this.backItemId;
	}
	
	/**
	 * @param backItemName
	 */
	public void setBackItemName(String backItemName) {
		this.backItemName = backItemName;
	}
	
    /**
     * @return backItemName
     */
	public String getBackItemName() {
		return this.backItemName;
	}
	
	/**
	 * @param stdBackItemPoint
	 */
	public void setStdBackItemPoint(Integer stdBackItemPoint) {
		this.stdBackItemPoint = stdBackItemPoint;
	}
	
    /**
     * @return stdBackItemPoint
     */
	public Integer getStdBackItemPoint() {
		return this.stdBackItemPoint;
	}
	
	/**
	 * @param backItemPoint
	 */
	public void setBackItemPoint(Integer backItemPoint) {
		this.backItemPoint = backItemPoint;
	}
	
    /**
     * @return backItemPoint
     */
	public Integer getBackItemPoint() {
		return this.backItemPoint;
	}
	
	/**
	 * @param isExistIssue
	 */
	public void setIsExistIssue(String isExistIssue) {
		this.isExistIssue = isExistIssue;
	}
	
    /**
     * @return isExistIssue
     */
	public String getIsExistIssue() {
		return this.isExistIssue;
	}
	
	/**
	 * @param existIssueDesc
	 */
	public void setExistIssueDesc(String existIssueDesc) {
		this.existIssueDesc = existIssueDesc;
	}
	
    /**
     * @return existIssueDesc
     */
	public String getExistIssueDesc() {
		return this.existIssueDesc;
	}
	
	/**
	 * @param backItemOrder
	 */
	public void setBackItemOrder(Integer backItemOrder) {
		this.backItemOrder = backItemOrder;
	}
	
    /**
     * @return backItemOrder
     */
	public Integer getBackItemOrder() {
		return this.backItemOrder;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}