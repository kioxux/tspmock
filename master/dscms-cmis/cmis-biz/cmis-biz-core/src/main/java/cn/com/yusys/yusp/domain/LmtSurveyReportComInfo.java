/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportComInfo
 * @类描述: lmt_survey_report_com_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 10:59:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_survey_report_com_info")
public class LmtSurveyReportComInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_SERNO")
	private String surveySerno;
	
	/** 企业名称 **/
	@Column(name = "CON_NAME", unique = false, nullable = true, length = 80)
	private String conName;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 80)
	private String legal;
	
	/** 经营地址 **/
	@Column(name = "OPER_ADDR", unique = false, nullable = true, length = 500)
	private String operAddr;
	
	/** 经营期限 **/
	@Column(name = "OPER_TERM", unique = false, nullable = true, length = 20)
	private String operTerm;
	
	/** 主营业务 **/
	@Column(name = "MAIN_BUSI", unique = false, nullable = true, length = 500)
	private String mainBusi;
	
	/** 企业类型 **/
	@Column(name = "CORP_TYPE", unique = false, nullable = true, length = 5)
	private String corpType;
	
	/** 营业执照年限 **/
	@Column(name = "BLIC_YEARS", unique = false, nullable = true, length = 20)
	private String blicYears;
	
	/** 行业 **/
	@Column(name = "TRADE", unique = false, nullable = true, length = 2000)
	private String trade;
	
	/** 企业证件类型 **/
	@Column(name = "CORP_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String corpCertType;
	
	/** 企业证件号码 **/
	@Column(name = "CORP_CERT_CODE", unique = false, nullable = true, length = 40)
	private String corpCertCode;
	
	/** 实际经营地址 **/
	@Column(name = "ACT_MNG_ADDR", unique = false, nullable = true, length = 80)
	private String actMngAddr;
	
	/** 实际行业 **/
	@Column(name = "ACT_OPER_TRADE", unique = false, nullable = true, length = 800)
	private String actOperTrade;
	
	/** 是否实际经营人 **/
	@Column(name = "IS_ACT_OPER_PERSON", unique = false, nullable = true, length = 1)
	private String isActOperPerson;
	
	/** 经营是否正常 **/
	@Column(name = "IS_OPER_NORMAL", unique = false, nullable = true, length = 1)
	private String isOperNormal;
	
	/** 司法审核结果 **/
	@Column(name = "JUDICIAL_AUDIT_RESULT", unique = false, nullable = true, length = 20)
	private String judicialAuditResult;
	
	/** 企业完整纳税月份数 **/
	@Column(name = "CORP_FULL_TAX_MONTHS", unique = false, nullable = true, length = 255)
	private String corpFullTaxMonths;
	
	/** 企业当前税务信用评级 **/
	@Column(name = "CORP_CURT_TXT_CDT_EVAL", unique = false, nullable = true, length = 5)
	private String corpCurtTxtCdtEval;
	
	/** 企业近1年综合应纳税额 **/
	@Column(name = "CORP_LT_1YEAR_INTE_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpLt1yearInteTax;
	
	/** 企业近1年税前利润率 **/
	@Column(name = "CORP_LT_1YEAR_PRETAX_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpLt1yearPretaxProfit;
	
	/** 企业近1年销售收入 **/
	@Column(name = "CORP_LT_1YEAR_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpLt1yearSaleIncome;
	
	/** 税务模型评级结果 **/
	@Column(name = "TAX_MODEL_EVAL_RESULT", unique = false, nullable = true, length = 5)
	private String taxModelEvalResult;
	
	/** 税务模型评分 **/
	@Column(name = "TAX_MODEL_GRADE", unique = false, nullable = true, length = 5)
	private String taxModelGrade;
	
	/** 负债收入比 **/
	@Column(name = "DEBT_EARNING_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtEarningPerc;
	
	/** 企业描述 **/
	@Column(name = "CORP_DESC", unique = false, nullable = true, length = 500)
	private String corpDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param conName
	 */
	public void setConName(String conName) {
		this.conName = conName;
	}
	
    /**
     * @return conName
     */
	public String getConName() {
		return this.conName;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr;
	}
	
    /**
     * @return operAddr
     */
	public String getOperAddr() {
		return this.operAddr;
	}
	
	/**
	 * @param operTerm
	 */
	public void setOperTerm(String operTerm) {
		this.operTerm = operTerm;
	}
	
    /**
     * @return operTerm
     */
	public String getOperTerm() {
		return this.operTerm;
	}
	
	/**
	 * @param mainBusi
	 */
	public void setMainBusi(String mainBusi) {
		this.mainBusi = mainBusi;
	}
	
    /**
     * @return mainBusi
     */
	public String getMainBusi() {
		return this.mainBusi;
	}
	
	/**
	 * @param corpType
	 */
	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}
	
    /**
     * @return corpType
     */
	public String getCorpType() {
		return this.corpType;
	}
	
	/**
	 * @param blicYears
	 */
	public void setBlicYears(String blicYears) {
		this.blicYears = blicYears;
	}
	
    /**
     * @return blicYears
     */
	public String getBlicYears() {
		return this.blicYears;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}
	
    /**
     * @return trade
     */
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param corpCertType
	 */
	public void setCorpCertType(String corpCertType) {
		this.corpCertType = corpCertType;
	}
	
    /**
     * @return corpCertType
     */
	public String getCorpCertType() {
		return this.corpCertType;
	}
	
	/**
	 * @param corpCertCode
	 */
	public void setCorpCertCode(String corpCertCode) {
		this.corpCertCode = corpCertCode;
	}
	
    /**
     * @return corpCertCode
     */
	public String getCorpCertCode() {
		return this.corpCertCode;
	}
	
	/**
	 * @param actMngAddr
	 */
	public void setActMngAddr(String actMngAddr) {
		this.actMngAddr = actMngAddr;
	}
	
    /**
     * @return actMngAddr
     */
	public String getActMngAddr() {
		return this.actMngAddr;
	}
	
	/**
	 * @param actOperTrade
	 */
	public void setActOperTrade(String actOperTrade) {
		this.actOperTrade = actOperTrade;
	}
	
    /**
     * @return actOperTrade
     */
	public String getActOperTrade() {
		return this.actOperTrade;
	}
	
	/**
	 * @param isActOperPerson
	 */
	public void setIsActOperPerson(String isActOperPerson) {
		this.isActOperPerson = isActOperPerson;
	}
	
    /**
     * @return isActOperPerson
     */
	public String getIsActOperPerson() {
		return this.isActOperPerson;
	}
	
	/**
	 * @param isOperNormal
	 */
	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal;
	}
	
    /**
     * @return isOperNormal
     */
	public String getIsOperNormal() {
		return this.isOperNormal;
	}
	
	/**
	 * @param judicialAuditResult
	 */
	public void setJudicialAuditResult(String judicialAuditResult) {
		this.judicialAuditResult = judicialAuditResult;
	}
	
    /**
     * @return judicialAuditResult
     */
	public String getJudicialAuditResult() {
		return this.judicialAuditResult;
	}
	
	/**
	 * @param corpFullTaxMonths
	 */
	public void setCorpFullTaxMonths(String corpFullTaxMonths) {
		this.corpFullTaxMonths = corpFullTaxMonths;
	}
	
    /**
     * @return corpFullTaxMonths
     */
	public String getCorpFullTaxMonths() {
		return this.corpFullTaxMonths;
	}
	
	/**
	 * @param corpCurtTxtCdtEval
	 */
	public void setCorpCurtTxtCdtEval(String corpCurtTxtCdtEval) {
		this.corpCurtTxtCdtEval = corpCurtTxtCdtEval;
	}
	
    /**
     * @return corpCurtTxtCdtEval
     */
	public String getCorpCurtTxtCdtEval() {
		return this.corpCurtTxtCdtEval;
	}
	
	/**
	 * @param corpLt1yearInteTax
	 */
	public void setCorpLt1yearInteTax(java.math.BigDecimal corpLt1yearInteTax) {
		this.corpLt1yearInteTax = corpLt1yearInteTax;
	}
	
    /**
     * @return corpLt1yearInteTax
     */
	public java.math.BigDecimal getCorpLt1yearInteTax() {
		return this.corpLt1yearInteTax;
	}
	
	/**
	 * @param corpLt1yearPretaxProfit
	 */
	public void setCorpLt1yearPretaxProfit(java.math.BigDecimal corpLt1yearPretaxProfit) {
		this.corpLt1yearPretaxProfit = corpLt1yearPretaxProfit;
	}
	
    /**
     * @return corpLt1yearPretaxProfit
     */
	public java.math.BigDecimal getCorpLt1yearPretaxProfit() {
		return this.corpLt1yearPretaxProfit;
	}
	
	/**
	 * @param corpLt1yearSaleIncome
	 */
	public void setCorpLt1yearSaleIncome(java.math.BigDecimal corpLt1yearSaleIncome) {
		this.corpLt1yearSaleIncome = corpLt1yearSaleIncome;
	}
	
    /**
     * @return corpLt1yearSaleIncome
     */
	public java.math.BigDecimal getCorpLt1yearSaleIncome() {
		return this.corpLt1yearSaleIncome;
	}
	
	/**
	 * @param taxModelEvalResult
	 */
	public void setTaxModelEvalResult(String taxModelEvalResult) {
		this.taxModelEvalResult = taxModelEvalResult;
	}
	
    /**
     * @return taxModelEvalResult
     */
	public String getTaxModelEvalResult() {
		return this.taxModelEvalResult;
	}
	
	/**
	 * @param taxModelGrade
	 */
	public void setTaxModelGrade(String taxModelGrade) {
		this.taxModelGrade = taxModelGrade;
	}
	
    /**
     * @return taxModelGrade
     */
	public String getTaxModelGrade() {
		return this.taxModelGrade;
	}
	
	/**
	 * @param debtEarningPerc
	 */
	public void setDebtEarningPerc(java.math.BigDecimal debtEarningPerc) {
		this.debtEarningPerc = debtEarningPerc;
	}
	
    /**
     * @return debtEarningPerc
     */
	public java.math.BigDecimal getDebtEarningPerc() {
		return this.debtEarningPerc;
	}
	
	/**
	 * @param corpDesc
	 */
	public void setCorpDesc(String corpDesc) {
		this.corpDesc = corpDesc;
	}
	
    /**
     * @return corpDesc
     */
	public String getCorpDesc() {
		return this.corpDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}