package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.TmpWydHkPlan;
import cn.com.yusys.yusp.repository.mapper.wyd.TmpWydHkPlanMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: WydRepayPlanService
 * @类描述: #微业贷还款计划查询
 * @功能描述:
 * @创建人: zrcbank-fengjj
 * @创建时间: 2021-08-04 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class WydRepayPlanService {

    private static final Logger log = LoggerFactory.getLogger(WydRepayPlanService.class);

    @Resource
    private TmpWydHkPlanMapper tmpWydHkPlanMapper;

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<TmpWydHkPlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TmpWydHkPlan> list = tmpWydHkPlanMapper.selectByCondition(model);
        PageHelper.clearPage();
        return list;
    }



}
