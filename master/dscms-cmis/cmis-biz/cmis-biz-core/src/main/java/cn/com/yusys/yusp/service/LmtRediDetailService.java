/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtRediDetail;
import cn.com.yusys.yusp.repository.mapper.LmtRediDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRediDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-31 21:00:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRediDetailService extends BizInvestCommonService{

    @Autowired
    private LmtRediDetailMapper lmtRediDetailMapper;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRediDetail selectByPrimaryKey(String pkId) {
        return lmtRediDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRediDetail> selectAll(QueryModel model) {
        List<LmtRediDetail> records = (List<LmtRediDetail>) lmtRediDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRediDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRediDetail> list = lmtRediDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRediDetail record) {
        return lmtRediDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRediDetail record) {
        return lmtRediDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int update(LmtRediDetail record) {
        record.setUpdateTime(getCurrrentDate());
        record.setUpdDate(getCurrrentDateStr());
        record.setUpdId(getCurrentUser().getLoginCode());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        return lmtRediDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRediDetail record) {
        return lmtRediDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtRediDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtRediDetailMapper.deleteByIds(ids);
    }

    /**
     * 主体授信-授信复议-授信复议申请书
     * @param serno
     * @return
     */
    public int insertLmtRediDetail(String serno) {
        LmtRediDetail lmtRediDetail = new LmtRediDetail();
        lmtRediDetail.setLmtSerno(serno);
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtRediDetail);
        return insert(lmtRediDetail);
    }

    /**
     * 根据serno获取
     * @param lmtSerno
     * @return
     */
    public LmtRediDetail selectByLmtSerno(String lmtSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("lmtSerno",lmtSerno);
        queryModel.setSize(1);
        queryModel.setPage(1);
        List<LmtRediDetail> lmtRediDetails = selectByModel(queryModel);
        if (lmtRediDetails!=null && lmtRediDetails.size()>0){
            return lmtRediDetails.get(0);
        }
        return null;
    }

    /**
     * 保存复议申请表
     * @param lmtRediDetail
     * @return
     */
    public int updateFysqb(LmtRediDetail lmtRediDetail) {
        String lmtSerno = lmtRediDetail.getLmtSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(lmtSerno,"fysqb");
        return update(lmtRediDetail);
    }
}
