/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplRateInfo
 * @类描述: iqp_appl_rate_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 15:08:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appl_rate_info")
public class IqpApplRateInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 32)
	private String iqpSerno;
	
	/** 固定利率期限（月) **/
	@Column(name = "RATE_TERM", unique = false, nullable = true, length = 10)
	private Integer rateTerm;
	
	/** 固定利率% **/
	@Column(name = "RATE_PERCENT", unique = false, nullable = true, length = 13)
	private java.math.BigDecimal ratePercent;
	
	/** 登记人机构 **/
	@Column(name = "CREATE_ORG", unique = false, nullable = true, length = 32)
	private String createOrg;
	
	/** 登记时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private String createTime;
	
	/** 登记人 **/
	@Column(name = "CREATE_USR", unique = false, nullable = true, length = 20)
	private String createUsr;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param rateTerm
	 */
	public void setRateTerm(Integer rateTerm) {
		this.rateTerm = rateTerm;
	}
	
    /**
     * @return rateTerm
     */
	public Integer getRateTerm() {
		return this.rateTerm;
	}
	
	/**
	 * @param ratePercent
	 */
	public void setRatePercent(java.math.BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
    /**
     * @return ratePercent
     */
	public java.math.BigDecimal getRatePercent() {
		return this.ratePercent;
	}
	
	/**
	 * @param createOrg
	 */
	public void setCreateOrg(String createOrg) {
		this.createOrg = createOrg;
	}
	
    /**
     * @return createOrg
     */
	public String getCreateOrg() {
		return this.createOrg;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param createUsr
	 */
	public void setCreateUsr(String createUsr) {
		this.createUsr = createUsr;
	}
	
    /**
     * @return createUsr
     */
	public String getCreateUsr() {
		return this.createUsr;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}