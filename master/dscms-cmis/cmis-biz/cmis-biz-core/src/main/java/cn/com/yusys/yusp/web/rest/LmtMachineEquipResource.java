/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtMachineEquip;
import cn.com.yusys.yusp.service.LmtMachineEquipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtMachineEquipResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtmachineequip")
public class LmtMachineEquipResource {
    @Autowired
    private LmtMachineEquipService lmtMachineEquipService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtMachineEquip>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtMachineEquip> list = lmtMachineEquipService.selectAll(queryModel);
        return new ResultDto<List<LmtMachineEquip>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtMachineEquip>> index(QueryModel queryModel) {
        List<LmtMachineEquip> list = lmtMachineEquipService.selectByModel(queryModel);
        return new ResultDto<List<LmtMachineEquip>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtMachineEquip> show(@PathVariable("serno") String serno) {
        LmtMachineEquip lmtMachineEquip = lmtMachineEquipService.selectByPrimaryKey(serno);
        return new ResultDto<LmtMachineEquip>(lmtMachineEquip);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtMachineEquip> create(@RequestBody LmtMachineEquip lmtMachineEquip) throws URISyntaxException {
        lmtMachineEquipService.insert(lmtMachineEquip);
        return new ResultDto<LmtMachineEquip>(lmtMachineEquip);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtMachineEquip lmtMachineEquip) throws URISyntaxException {
        int result = lmtMachineEquipService.update(lmtMachineEquip);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtMachineEquipService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtMachineEquipService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
