package cn.com.yusys.yusp.web.server.xdxw0066;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0066.req.Xdxw0066DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0066.resp.Xdxw0066DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0066.Xdxw0066Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0066:调查基本信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0066Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0066Resource.class);

    @Autowired
    private Xdxw0066Service xdxw0066Service;

    /**
     * 交易码：xdxw0066
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0066DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0066")
    protected @ResponseBody
    ResultDto<Xdxw0066DataRespDto> xdxw0066(@Validated @RequestBody Xdxw0066DataReqDto xdxw0066DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataReqDto));
        Xdxw0066DataRespDto xdxw0066DataRespDto = new Xdxw0066DataRespDto();// 响应Dto:调查基本信息查询
        ResultDto<Xdxw0066DataRespDto> xdxw0066DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0066DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0066Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataReqDto));
            java.util.List<List> result = Optional.ofNullable(xdxw0066Service.getSurveyReportList(xdxw0066DataReqDto)).orElseGet(() -> {
                List list = new List();
                list.setSurvey_serno(StringUtils.EMPTY);// 主键流水号
                list.setCus_name(StringUtils.EMPTY);// 申请人姓名
                list.setCert_code(StringUtils.EMPTY);// 身份证号码
                list.setMarry_status(StringUtils.EMPTY);// 婚姻状况STD_XD_MAR
                list.setHouse_prop(StringUtils.EMPTY);// 居住场所类型 XD_LIVECS_TYPE
                list.setAddr(StringUtils.EMPTY);// 家庭地址
                list.setLive_year(StringUtils.EMPTY);// 本地居住年限
                list.setLocal_resident(StringUtils.EMPTY);// 本地户口 XD_LOCAL_RESIDENT
                list.setTel(StringUtils.EMPTY);// 手机
                list.setSpouse_name(StringUtils.EMPTY);// 配偶姓名
                list.setSpouse_cert_code(StringUtils.EMPTY);// 配偶身份证
                list.setSpouse_work_type(StringUtils.EMPTY);// 配偶工作情况 XD_WORK_TYPE
                list.setSpouse_tel(StringUtils.EMPTY);// 配偶电话
                list.setPer_zx(StringUtils.EMPTY);// 央行个人征信系统（含配偶）XD_RE_GEL
                list.setPer_remark(StringUtils.EMPTY);// 备注（个人）
                list.setCom_zx(StringUtils.EMPTY);// 央行企业征信系统XD_RE_GEL
                list.setCom_remark(StringUtils.EMPTY);// 备注（企业）
                list.setMain_business(StringUtils.EMPTY);// 主营业务
                list.setYears_operations(StringUtils.EMPTY);// 经营年限
                list.setBusiness_addres(StringUtils.EMPTY);// 经营地址
                list.setEducation(StringUtils.EMPTY);// 学历 STD_ZX_EDU
                list.setSex(StringUtils.EMPTY);// 性别 STD_ZX_SEX
                list.setCus_list_serno(StringUtils.EMPTY);// 名单表流水号
                list.setApp_amt(new BigDecimal(0L));// 申请金额
                list.setApp_limit(StringUtils.EMPTY);// 申请期限
                list.setLoan_reason(StringUtils.EMPTY);// 贷款原因
                list.setLoan_used(StringUtils.EMPTY);// 贷款用途
                list.setCus_list_serno(StringUtils.EMPTY);// 名单表流水号
                return Arrays.asList(list);
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(result));
            xdxw0066DataRespDto.setList(result);
            // 封装xdxw0066DataResultDto中正确的返回码和返回信息
            xdxw0066DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0066DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, e.getMessage());
            // 封装xdxw0066DataResultDto中异常返回码和返回信息
            xdxw0066DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0066DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0066DataRespDto到xdxw0066DataResultDto中
        xdxw0066DataResultDto.setData(xdxw0066DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataResultDto));
        return xdxw0066DataResultDto;
    }
}
