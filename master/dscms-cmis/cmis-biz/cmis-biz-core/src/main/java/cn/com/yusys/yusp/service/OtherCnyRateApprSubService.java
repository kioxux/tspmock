/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateAppFinDetailsMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherCnyRateApprSub;
import cn.com.yusys.yusp.domain.OtherCnyRateApprSubRequestDto;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateApprSubMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateApprSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 17:23:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateApprSubService {

    @Autowired
    private OtherCnyRateApprSubMapper otherCnyRateApprSubMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private OtherCnyRateAppFinDetailsMapper otherCnyRateAppFinDetailsMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateApprSub selectByPrimaryKey(String approveSerno) {
        return otherCnyRateApprSubMapper.selectByPrimaryKey(approveSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateApprSub> selectAll(QueryModel model) {
        List<OtherCnyRateApprSub> records = (List<OtherCnyRateApprSub>) otherCnyRateApprSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateApprSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateApprSub> list = otherCnyRateApprSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateApprSub record) {
        return otherCnyRateApprSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateApprSub record) {
        return otherCnyRateApprSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateApprSub record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateApprSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateApprSub record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateApprSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String approveSerno) {
        return otherCnyRateApprSubMapper.deleteByPrimaryKey(approveSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateApprSubMapper.deleteByIds(ids);
    }

    /**
     * 根据本期融资信息获取审批信息
     * @param
     * @return
     */
    public List<OtherCnyRateApprSub> queryByCurrFinDetails(QueryModel queryModel) {
        return otherCnyRateApprSubMapper.queryByCurrFinDetails(queryModel);
    }
    
    /**
     * 根据本期融资信息获取审批信息
     * @param
     * @return
     */
    public List<OtherCnyRateApprSubRequestDto> queryByCurrFin(QueryModel queryModel) {
    	return otherCnyRateApprSubMapper.queryByCurrFin(queryModel);
    }
    /**
     * @方法名称: updaterateCnySub
     * @方法描述: 更新权限申请表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updaterateCnySub(OtherCnyRateApprSubRequestDto otherCnyRateApprSubRequestDto) {
        int count = 0;
        String serno = otherCnyRateApprSubRequestDto.getSerno();
        String subSerno = otherCnyRateApprSubRequestDto.getSubSerno();
        BigDecimal apprLoanRate = otherCnyRateApprSubRequestDto.getApprLoanRate();
        String apprRateType = otherCnyRateApprSubRequestDto.getApprRateType();
        String rateAdjustCycleApprSub = otherCnyRateApprSubRequestDto.getRateAdjustCycleApprSub();
        BigDecimal lprRate = otherCnyRateApprSubRequestDto.getLprRate();
        String fixedDate = otherCnyRateApprSubRequestDto.getFixedDate();
        BigDecimal rateFloatPoint = otherCnyRateApprSubRequestDto.getRateFloatPoint();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("subSerno",subSerno);
        queryModel.addCondition("serno",serno);
        List<OtherCnyRateAppFinDetails> otherCnyRateAppFinDetails1 = otherCnyRateAppFinDetailsMapper.selectByModel(queryModel);
        List<OtherCnyRateApprSub> otherCnyRateApprSub1 = otherCnyRateApprSubMapper.selectByModel(queryModel);

        if(otherCnyRateAppFinDetails1.size()>0 && otherCnyRateApprSub1.size()>0){
            OtherCnyRateAppFinDetails otherCnyRateAppFinDetails2 = otherCnyRateAppFinDetails1.get(0);
            otherCnyRateAppFinDetails2.setRateFloatPoint(rateFloatPoint);
            OtherCnyRateApprSub otherCnyRateApprSub2 = otherCnyRateApprSub1.get(0);
            otherCnyRateApprSub2.setApprLoanRate(apprLoanRate);
            otherCnyRateApprSub2.setApprRateType(apprRateType);
            otherCnyRateApprSub2.setRateAdjustCycle(rateAdjustCycleApprSub);
            otherCnyRateApprSub2.setLprRate(lprRate);
            otherCnyRateApprSub2.setFixedDate(fixedDate);
            otherCnyRateAppFinDetailsMapper.updateByPrimaryKey(otherCnyRateAppFinDetails2);
            count =  otherCnyRateApprSubMapper.updateByPrimaryKey(otherCnyRateApprSub2);
        }else if(otherCnyRateAppFinDetails1.size()>0 && otherCnyRateApprSub1.size()<=0){
            OtherCnyRateAppFinDetails otherCnyRateAppFinDetails2 = otherCnyRateAppFinDetails1.get(0);
            otherCnyRateAppFinDetails2.setRateFloatPoint(rateFloatPoint);
            OtherCnyRateApprSub otherCnyRateApprSub2 = new OtherCnyRateApprSub();
            String apprSubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());

            otherCnyRateApprSub2.setApproveSerno(apprSubSerno);
            otherCnyRateApprSub2.setSerno(serno);
            otherCnyRateApprSub2.setSubSerno(subSerno);
            otherCnyRateApprSub2.setSubPrdSerno(otherCnyRateAppFinDetails2.getSubPrdSerno());
            otherCnyRateApprSub2.setAccSubPrdNo(otherCnyRateAppFinDetails2.getLmtBizType());
            otherCnyRateApprSub2.setAccSubPrdName(otherCnyRateAppFinDetails2.getLmtBizTypeName());
            otherCnyRateApprSub2.setApprLoanRate(apprLoanRate);
            otherCnyRateApprSub2.setApprRateType(apprRateType);
            otherCnyRateApprSub2.setRateAdjustCycle(rateAdjustCycleApprSub);
            otherCnyRateApprSub2.setLprRate(lprRate);
            otherCnyRateApprSub2.setFixedDate(fixedDate);
            otherCnyRateApprSub2.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            otherCnyRateAppFinDetailsMapper.updateByPrimaryKey(otherCnyRateAppFinDetails2);
            count =  otherCnyRateApprSubMapper.insertSelective(otherCnyRateApprSub2);
        }
        return  count;
    }
}
