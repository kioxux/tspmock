package cn.com.yusys.yusp.web.server.xdxt0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0006.req.Xdxt0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.resp.Xdxt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxt0006.Xdxt0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询信贷用户的特定岗位信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0006:查询信贷用户的特定岗位信息")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0006Resource.class);
    @Autowired
    private Xdxt0006Service xdxt0006Service;

    /**
     * 交易码：xdxt0006
     * 交易描述：查询信贷用户的特定岗位信息
     *
     * @param xdxt0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷用户的特定岗位信息")
    @PostMapping("/xdxt0006")
    protected @ResponseBody
    ResultDto<Xdxt0006DataRespDto> xdxt0006(@Validated @RequestBody Xdxt0006DataReqDto xdxt0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataReqDto));
        Xdxt0006DataRespDto xdxt0006DataRespDto = new Xdxt0006DataRespDto();// 响应Dto:查询信贷用户的特定岗位信息
        ResultDto<Xdxt0006DataRespDto> xdxt0006DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataReqDto));
            xdxt0006DataRespDto = xdxt0006Service.getXdxt0006(xdxt0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataRespDto));

            // 封装xdxt0006DataResultDto中正确的返回码和返回信息
            xdxt0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, e.getMessage());
            xdxt0006DataResultDto.setCode(e.getErrorCode());
            xdxt0006DataResultDto.setMessage(e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, e.getMessage());
            // 封装xdxt0006DataResultDto中异常返回码和返回信息
            xdxt0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0006DataRespDto到xdxt0006DataResultDto中
        xdxt0006DataResultDto.setData(xdxt0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataResultDto));
        return xdxt0006DataResultDto;
    }
}
