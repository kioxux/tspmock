package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCar
 * @类描述: iqp_car数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:32:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCarDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 汽车类型 STD_ZB_CAR_TYP **/
	private String carType;
	
	/** 所购车辆品牌型号 **/
	private String carBrandModel;
	
	/** 车辆总价 **/
	private java.math.BigDecimal carTotal;
	
	/** 首付金额 **/
	private java.math.BigDecimal firstpayAmt;
	
	/** 销售商名称 **/
	private String saleName;
	
	/** 是否进口车型 **/
	private String isInletCar;
	
	/** 汽车品牌名称 **/
	private String carBrandName;
	
	/** 车架号 **/
	private String vin;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param carType
	 */
	public void setCarType(String carType) {
		this.carType = carType == null ? null : carType.trim();
	}
	
    /**
     * @return CarType
     */	
	public String getCarType() {
		return this.carType;
	}
	
	/**
	 * @param carBrandModel
	 */
	public void setCarBrandModel(String carBrandModel) {
		this.carBrandModel = carBrandModel == null ? null : carBrandModel.trim();
	}
	
    /**
     * @return CarBrandModel
     */	
	public String getCarBrandModel() {
		return this.carBrandModel;
	}
	
	/**
	 * @param carTotal
	 */
	public void setCarTotal(java.math.BigDecimal carTotal) {
		this.carTotal = carTotal;
	}
	
    /**
     * @return CarTotal
     */	
	public java.math.BigDecimal getCarTotal() {
		return this.carTotal;
	}
	
	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}
	
    /**
     * @return FirstpayAmt
     */	
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}
	
	/**
	 * @param saleName
	 */
	public void setSaleName(String saleName) {
		this.saleName = saleName == null ? null : saleName.trim();
	}
	
    /**
     * @return SaleName
     */	
	public String getSaleName() {
		return this.saleName;
	}
	
	/**
	 * @param isInletCar
	 */
	public void setIsInletCar(String isInletCar) {
		this.isInletCar = isInletCar == null ? null : isInletCar.trim();
	}
	
    /**
     * @return IsInletCar
     */	
	public String getIsInletCar() {
		return this.isInletCar;
	}
	
	/**
	 * @param carBrandName
	 */
	public void setCarBrandName(String carBrandName) {
		this.carBrandName = carBrandName == null ? null : carBrandName.trim();
	}
	
    /**
     * @return CarBrandName
     */	
	public String getCarBrandName() {
		return this.carBrandName;
	}
	
	/**
	 * @param vin
	 */
	public void setVin(String vin) {
		this.vin = vin == null ? null : vin.trim();
	}
	
    /**
     * @return Vin
     */	
	public String getVin() {
		return this.vin;
	}


}