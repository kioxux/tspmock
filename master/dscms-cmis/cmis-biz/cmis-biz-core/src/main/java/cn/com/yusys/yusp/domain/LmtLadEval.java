/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtLadEval
 * @类描述: lmt_lad_eval数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-13 20:04:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_lad_eval")
public class LmtLadEval extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 业务品种 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 客户对外负债 **/
	@Column(name = "CUS_OUTSIDE_DEBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cusOutsideDebt;

	/** 现有他行负债 **/
	@Column(name = "CUR_OTHER_BANK_DEBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curOtherBankDebt;

	/** 对外抵押金额 **/
	@Column(name = "OUTER_PLD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerPldAmt;

	/** 对外质押金额 **/
	@Column(name = "OUTER_IMN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerImnAmt;

	/** 对外保证金额 **/
	@Column(name = "OUTER_GRT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerGrtAmt;

	/** 单一客户测算限额 **/
	@Column(name = "SINGLE_EVAL_LIMIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleEvalLimit;

	/** 单一客户人工限额 **/
	@Column(name = "SINGLE_MANUAL_LIMIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleManualLimit;

	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;

	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;

	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;

	/** 生效日期 **/
	@Column(name = "INURE_DATE", unique = false, nullable = true, length = 10)
	private String inureDate;

	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param cusOutsideDebt
	 */
	public void setCusOutsideDebt(java.math.BigDecimal cusOutsideDebt) {
		this.cusOutsideDebt = cusOutsideDebt;
	}

	/**
	 * @return cusOutsideDebt
	 */
	public java.math.BigDecimal getCusOutsideDebt() {
		return this.cusOutsideDebt;
	}

	/**
	 * @param curOtherBankDebt
	 */
	public void setCurOtherBankDebt(java.math.BigDecimal curOtherBankDebt) {
		this.curOtherBankDebt = curOtherBankDebt;
	}

	/**
	 * @return curOtherBankDebt
	 */
	public java.math.BigDecimal getCurOtherBankDebt() {
		return this.curOtherBankDebt;
	}

	/**
	 * @param outerPldAmt
	 */
	public void setOuterPldAmt(java.math.BigDecimal outerPldAmt) {
		this.outerPldAmt = outerPldAmt;
	}

	/**
	 * @return outerPldAmt
	 */
	public java.math.BigDecimal getOuterPldAmt() {
		return this.outerPldAmt;
	}

	/**
	 * @param outerImnAmt
	 */
	public void setOuterImnAmt(java.math.BigDecimal outerImnAmt) {
		this.outerImnAmt = outerImnAmt;
	}

	/**
	 * @return outerImnAmt
	 */
	public java.math.BigDecimal getOuterImnAmt() {
		return this.outerImnAmt;
	}

	/**
	 * @param outerGrtAmt
	 */
	public void setOuterGrtAmt(java.math.BigDecimal outerGrtAmt) {
		this.outerGrtAmt = outerGrtAmt;
	}

	/**
	 * @return outerGrtAmt
	 */
	public java.math.BigDecimal getOuterGrtAmt() {
		return this.outerGrtAmt;
	}

	/**
	 * @param singleEvalLimit
	 */
	public void setSingleEvalLimit(java.math.BigDecimal singleEvalLimit) {
		this.singleEvalLimit = singleEvalLimit;
	}

	/**
	 * @return singleEvalLimit
	 */
	public java.math.BigDecimal getSingleEvalLimit() {
		return this.singleEvalLimit;
	}

	/**
	 * @param singleManualLimit
	 */
	public void setSingleManualLimit(java.math.BigDecimal singleManualLimit) {
		this.singleManualLimit = singleManualLimit;
	}

	/**
	 * @return singleManualLimit
	 */
	public java.math.BigDecimal getSingleManualLimit() {
		return this.singleManualLimit;
	}

	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}

	/**
	 * @return debtLevel
	 */
	public String getDebtLevel() {
		return this.debtLevel;
	}

	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}

	/**
	 * @return ead
	 */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}

	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}

	/**
	 * @return lgd
	 */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}

	/**
	 * @param inureDate
	 */
	public void setInureDate(String inureDate) {
		this.inureDate = inureDate;
	}

	/**
	 * @return inureDate
	 */
	public String getInureDate() {
		return this.inureDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}