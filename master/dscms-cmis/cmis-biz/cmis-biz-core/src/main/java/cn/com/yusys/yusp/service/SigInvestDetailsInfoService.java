/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.domain.SigInvestDetailsInfo;
import cn.com.yusys.yusp.repository.mapper.SigInvestDetailsInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.SigInvestDetailsExportVo;
import cn.com.yusys.yusp.vo.SigInvestDetailsImportVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SigInvestDetailsInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 10:10:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class SigInvestDetailsInfoService {

    @Autowired
    private SigInvestDetailsInfoMapper sigInvestDetailsInfoMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public SigInvestDetailsInfo selectByPrimaryKey(String pkId) {
        return sigInvestDetailsInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<SigInvestDetailsInfo> selectAll(QueryModel model) {
        List<SigInvestDetailsInfo> records = (List<SigInvestDetailsInfo>) sigInvestDetailsInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<SigInvestDetailsInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<SigInvestDetailsInfo> list = sigInvestDetailsInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(SigInvestDetailsInfo record) {
        return sigInvestDetailsInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(SigInvestDetailsInfo record) {
        return sigInvestDetailsInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(SigInvestDetailsInfo record) {
        return sigInvestDetailsInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(SigInvestDetailsInfo record) {
        return sigInvestDetailsInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return sigInvestDetailsInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return sigInvestDetailsInfoMapper.deleteByIds(ids);
    }

    /**
     * 异步下载底层资产明细模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportSigInvestDetailsTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(SigInvestDetailsImportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步导出底层资产明细数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportSigInvestDetailsInfo(SigInvestDetailsInfo sigInvestDetailsInfo) {
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("proNo", sigInvestDetailsInfo.getProNo());
        queryModel.getCondition().put("status", CmisBizConstants.STD_ZB_STATUS_01);
        queryModel.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectByModel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(SigInvestDetailsExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入底层资产明细数据
     * @param sigInvestDetailsList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertSigInvestDetails(List<Object> sigInvestDetailsList) {
        List<SigInvestDetailsInfo> sigInvestDetailsInfoList = (List<SigInvestDetailsInfo>) BeanUtils.beansCopy(sigInvestDetailsList, SigInvestDetailsInfo.class);
        //模板导入时，增加判断，项目编号是否存在单笔投资授信台账或者单笔投资授信申请信息，若不存在则报错并予以提示“不存在项目编号为XXXXX的授信，请核实”
        if (CollectionUtils.isNotEmpty(sigInvestDetailsList)){
            Map<String,String> map = new HashMap();
            for (SigInvestDetailsInfo sigInvestDetailsInfo : sigInvestDetailsInfoList) {
                if (StringUtils.isBlank(sigInvestDetailsInfo.getProNo())){
                    throw new BizException(null, "9999", null, "存在项目编号为空的记录，请核实！");
                }
                String proNo = sigInvestDetailsInfo.getProNo();
                if (map.containsKey(proNo)){
                    continue;
                }
                QueryModel query = new QueryModel() ;
                query.addCondition("proNo", sigInvestDetailsInfo.getProNo());
                List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppService.selectByModel(query);
                if (CollectionUtils.isEmpty(lmtSigInvestApps)){
                    throw new BizException(null, "9999", null, "不存在项目编号为"+sigInvestDetailsInfo.getProNo()+"的授信，请核实！");
                }else{
                    //清空当前项目编号的底层资产列表
                    sigInvestDetailsInfoMapper.deleteByProNo(proNo);
                    map.put(proNo,lmtSigInvestApps.get(0).getAssetNo());
                }
            }

            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            User userInfo = SessionUtils.getUserInformation();
            try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
                SigInvestDetailsInfoMapper sigInvestDetailsInfoMapper = sqlSession.getMapper(SigInvestDetailsInfoMapper.class);
                for (SigInvestDetailsInfo sigInvestDetailsInfo : sigInvestDetailsInfoList) {
                    String proNo = sigInvestDetailsInfo.getProNo();
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

                    sigInvestDetailsInfo.setPkId(pkValue);
                    //状态
                    sigInvestDetailsInfo.setStatus(CmisBizConstants.STD_ZB_STATUS_01);
                    //操作类型
                    sigInvestDetailsInfo.setOprType(CmisBizConstants.OPR_TYPE_01);
                    //登记人
                    sigInvestDetailsInfo.setInputId(userInfo.getLoginCode());
                    //登记机构
                    sigInvestDetailsInfo.setInputBrId(userInfo.getOrg().getCode());
                    //登记日期
                    sigInvestDetailsInfo.setInputDate(openDay);
                    //更新日期
                    sigInvestDetailsInfo.setUpdDate(openDay);
                    //创建时间
                    sigInvestDetailsInfo.setCreateTime(new Date());
                    //判断是否存在资产编号，不存在则获取当前授信的资产编号
                    if (StringUtils.isBlank(sigInvestDetailsInfo.getAssetNo())){
                        sigInvestDetailsInfo.setAssetNo(map.get(proNo));
                    }
                    sigInvestDetailsInfoMapper.insertSelective(sigInvestDetailsInfo);

                }
                sqlSession.flushStatements();
                sqlSession.commit();
            }
        }

        return sigInvestDetailsList.size();
    }

    public Integer updateSigInvestDetail(SigInvestDetailsInfo sigInvestDetailsInfo){
        String pkId = sigInvestDetailsInfo.getPkId();
        SigInvestDetailsInfo info = sigInvestDetailsInfoMapper.selectByPrimaryKey(pkId);
        sigInvestDetailsInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        Date date = new Date();
        int count = 0;
        //如果存在数据则修改，不存在则新增
        if(info != null){
            sigInvestDetailsInfo.setUpdateTime(date);
            count = sigInvestDetailsInfoMapper.updateByPrimaryKeySelective(sigInvestDetailsInfo);
        }else {
            sigInvestDetailsInfo.setCreateTime(date);
            sigInvestDetailsInfo.setStatus(CmisBizConstants.STD_ZB_STATUS_01);
            count = sigInvestDetailsInfoMapper.insertSelective(sigInvestDetailsInfo);
        }
        return  count;
    }
}
