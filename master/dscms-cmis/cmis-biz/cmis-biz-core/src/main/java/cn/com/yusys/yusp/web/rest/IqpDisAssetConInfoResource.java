/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDisAssetConInfo;
import cn.com.yusys.yusp.service.IqpDisAssetConInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetConInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-05-06 16:37:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "还款能力分析-结论")
@RequestMapping("/api/iqpdisassetconinfo")
public class IqpDisAssetConInfoResource {
    @Autowired
    private IqpDisAssetConInfoService iqpDisAssetConInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDisAssetConInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDisAssetConInfo> list = iqpDisAssetConInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpDisAssetConInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpDisAssetConInfo>> index(QueryModel queryModel) {
        List<IqpDisAssetConInfo> list = iqpDisAssetConInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpDisAssetConInfo>>(list);
    }

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<IqpDisAssetConInfo> selectByPrimaryKey(@RequestBody IqpDisAssetConInfo iqpDisAssetConInfo) {
        return new ResultDto<IqpDisAssetConInfo>(iqpDisAssetConInfoService.selectByPrimaryKey(iqpDisAssetConInfo.getIqpSerno()));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpDisAssetConInfo> create(@RequestBody IqpDisAssetConInfo iqpDisAssetConInfo) throws URISyntaxException {
        iqpDisAssetConInfoService.insert(iqpDisAssetConInfo);
        return new ResultDto<IqpDisAssetConInfo>(iqpDisAssetConInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpDisAssetConInfo iqpDisAssetConInfo) throws URISyntaxException {
        int result = iqpDisAssetConInfoService.update(iqpDisAssetConInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:updateiqpDisAssetConInfo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateiqpDisAssetConInfo")
    protected ResultDto<Integer> updateiqpDisAssetConInfo(@RequestBody IqpDisAssetConInfo iqpDisAssetConInfo) throws URISyntaxException {
        int result = iqpDisAssetConInfoService.updateIqpDisAssetConInfo(iqpDisAssetConInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpDisAssetConInfoService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDisAssetConInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
