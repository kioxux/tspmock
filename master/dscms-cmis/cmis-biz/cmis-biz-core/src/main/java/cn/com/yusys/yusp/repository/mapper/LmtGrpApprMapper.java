/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpAppr;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpApprMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-14 09:54:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpApprMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtGrpAppr selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpAppr> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtGrpAppr record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtGrpAppr record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtGrpAppr record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtGrpAppr record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectLmtGrpApprByParams
     * @方法描述: 通过条件查询授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtGrpAppr> selectLmtGrpApprByParams(HashMap paramsMap);

    /**
     * 根据集团申请流水号查询数据
     * @param grpSerno
     * @return
     */
    LmtGrpAppr queryInfoByGrpSerno(String grpSerno);

    /**
     * @函数名称: queryLmtApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询成员客户审批信息
     * @参数与返回说明:
     * @算法描述:
     */

    List<LmtGrpMemRel> queryLmtApprByGrpSerno(@Param("serno") String serno);

    /**
     * @函数名称: selectLmtGrpApprByGrpApproveSerno
     * @函数描述: 根据集团审批流水号查询审批信息
     * @参数与返回说明:
     * @算法描述:
     */

    LmtGrpAppr selectLmtGrpApprByGrpApproveSerno(Map<String, String> params);
}