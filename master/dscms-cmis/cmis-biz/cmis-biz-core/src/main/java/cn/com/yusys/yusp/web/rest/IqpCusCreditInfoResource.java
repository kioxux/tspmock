/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.IqpCusCreditInfoRetailDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCusCreditInfo;
import cn.com.yusys.yusp.service.IqpCusCreditInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-24 15:44:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "客户及关联人信用信息")
@RequestMapping("/api/iqpcuscreditinfo")
public class IqpCusCreditInfoResource {
    @Autowired
    private IqpCusCreditInfoService iqpCusCreditInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCusCreditInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCusCreditInfo> list = iqpCusCreditInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpCusCreditInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/selectquery")
    protected ResultDto<List<IqpCusCreditInfo>> index(QueryModel queryModel) {
        List<IqpCusCreditInfo> list = iqpCusCreditInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpCusCreditInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{bizNo}")
    protected ResultDto<IqpCusCreditInfo> show(@PathVariable("bizNo") String bizNo) {
        IqpCusCreditInfo iqpCusCreditInfo = iqpCusCreditInfoService.selectByPrimaryKey(bizNo);
        return new ResultDto<IqpCusCreditInfo>(iqpCusCreditInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCusCreditInfo> create(@RequestBody IqpCusCreditInfo iqpCusCreditInfo) throws URISyntaxException {
        iqpCusCreditInfoService.insert(iqpCusCreditInfo);
        return new ResultDto<IqpCusCreditInfo>(iqpCusCreditInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCusCreditInfo iqpCusCreditInfo) throws URISyntaxException {
        int result = iqpCusCreditInfoService.update(iqpCusCreditInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{bizNo}")
    protected ResultDto<Integer> delete(@PathVariable("bizNo") String bizNo) {
        int result = iqpCusCreditInfoService.deleteByPrimaryKey(bizNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCusCreditInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectserno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象")
    @GetMapping("/selectserno/{serno}")
    protected ResultDto<IqpCusCreditInfo> selectSerno(@PathVariable("serno") String serno) {
        IqpCusCreditInfo iqpCusCreditInfo = iqpCusCreditInfoService.selectSerno(serno);
        return new ResultDto<IqpCusCreditInfo>(iqpCusCreditInfo);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/selectbymodel")
    protected ResultDto<List<IqpCusCreditInfo>> selectByModel(QueryModel queryModel) {
        List<IqpCusCreditInfo> list = iqpCusCreditInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpCusCreditInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:根据征信风险指标查询信息
     * @参数与返回说明:
     * @param iqpCusCreditInfoRetailDto
     * @算法描述:
     */
    @PostMapping("/insertiqpcuscreditinfo")
    protected ResultDto<Integer> insertIqpCusCreditInfo(@RequestBody IqpCusCreditInfoRetailDto iqpCusCreditInfoRetailDto) {
        return new ResultDto<Integer>(iqpCusCreditInfoService.insertIqpCusCreditInfo(iqpCusCreditInfoRetailDto));
    }


}
