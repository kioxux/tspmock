/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanContResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-09 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrloancont")
public class CtrLoanContResource {
    @Autowired
    private CtrLoanContService ctrLoanContService;
    private static final Logger log = LoggerFactory.getLogger(CtrLoanContResource.class);

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 全表查询.
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrLoanCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrLoanCont> list = ctrLoanContService.selectAll(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }


    /**
     * @函数名称:index selectctrloancontlistdata
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CtrLoanCont>> index(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>>
     * @author hubp
     * @date 2021/11/8 21:13
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectxw")
    protected ResultDto<List<CtrLoanCont>> selectXw(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectByModelXw(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:index selectctrloancontlistdata
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querylist")
    protected ResultDto<List<CtrLoanCont>> queryList(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }
    /**
     * @函数名称:index selectctrloancontlistdata
     * @函数描述:查询对象列表，台账为正常的
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/cStatus")
    protected ResultDto<List<CtrLoanCont>> indexcStatus(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("contStatus","200");
        User userInfo = SessionUtils.getUserInformation();
        String managerId = userInfo.getLoginCode();
        queryModel.addCondition("managerId",managerId);
        List<CtrLoanCont> list = ctrLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:selectctrloancontlistdata
     * @函数描述:重写列表查询方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectctrloancontlistdata")
    protected ResultDto<List<CtrLoanCont>> selectCtrLoanContListData(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectCtrLoanContListData(queryModel);
        ResultDto<List<CtrLoanCont>> resultDto = new ResultDto<>(list);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:selectCtrLoanContHisListData
     * @函数描述:重写历史列表查询方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectctrloanconthislistdata")
    protected ResultDto<List<CtrLoanCont>> selectCtrLoanContHisListData(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectCtrLoanContHisListData(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:selectWorryFreeLoanByContNo
     * @函数描述:根据合同号查询省心快贷查封查验中符合条件的合同
     * @参数与返回说明:
     * @算法描述:
     * @创建者：zl
     */
    @PostMapping("/selectWorryFreeLoanContinfo")
    protected ResultDto<List<CtrLoanCont>> selectWorryFreeLoanContinfo(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list  = ctrLoanContService.selectWorryFreeLoanContinfo(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contNo}")
    protected ResultDto<CtrLoanCont> show(@PathVariable("contNo") String contNo) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        return new ResultDto<CtrLoanCont>(ctrLoanCont);
    }

    /**
     * @函数名称:selectByContNo
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbycontno")
    protected ResultDto<CtrLoanCont> selectByContNo(@RequestBody String contNo) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        return new ResultDto<CtrLoanCont>(ctrLoanCont);
    }

    @PostMapping("/querybycontno")
    protected ResultDto<CtrLoanContEdit> queryBycontNo(@RequestBody Map params) {
        CtrLoanContEdit ctrLoanContEdit = ctrLoanContService.queryBycontNo(params);
        return new ResultDto<CtrLoanContEdit>(ctrLoanContEdit);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CtrLoanCont> create(@RequestBody CtrLoanCont ctrLoanCont) throws URISyntaxException {
        ctrLoanContService.insert(ctrLoanCont);
        return new ResultDto<CtrLoanCont>(ctrLoanCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:/ctrloancont
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrLoanCont ctrLoanCont) throws URISyntaxException {
        int result = ctrLoanContService.updateSelective(ctrLoanCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateContStatus
     * @函数描述:普通贷款合同签订
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateContStatus")
    protected ResultDto<Map> updateContStatus(@RequestBody CtrLoanContSingleData ctrLoanContSingleData) throws Exception {
        CtrLoanCont ctrLoanCont = ctrLoanContSingleData.getCtrLoanCont();
        List<GrtGuarCont> list = ctrLoanContSingleData.getGuarCont();
        Map result = ctrLoanContService.updateContStatus(ctrLoanCont,list);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:onLogOut
     * @函数描述:普通贷款合同注销
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onlogout")
    protected ResultDto<Map> onLogOut(@RequestBody Map params)  {
        Map rtnData= ctrLoanContService.onLogOut(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:contReSign
     * @函数描述:普通贷款合同重签
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/contReSign")
    protected ResultDto<Map> contReSign(@RequestBody Map params)  {
        Map rtnData= ctrLoanContService.contReSign(params);
        return new ResultDto<>(rtnData);
    }



    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrLoanContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称: updateSelective
     * @函数描述: 担保合同申请更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CtrLoanCont ctrLoanCont) {
        int rtnData = ctrLoanContService.updateSelective(ctrLoanCont);
        return new ResultDto<Integer>(rtnData);
    }

    /**
     * @函数名称: getLmtAcctInfoByContNo
     * @函数描述: 通过合同号获取授信额度信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getLmtAcctInfoByContNo")
    protected ResultDto<Map> getLmtAcctInfoByContNo(@RequestBody Map params) {
        Map resultInfo = ctrLoanContService.getLmtAcctInfoByContNo(params);
        return new ResultDto<>(resultInfo);
    }


    /**
     * 业务流水号 获取 合同号  和 放款申请 号
     *
     * @param iqpSerno
     * @return
     */
    @GetMapping("/singleBatchGetContAndPvpInfo/{iqpSerno}")
    protected ResultDto<Map> singleBatchGetContAndPvpInfo(@PathVariable("iqpSerno") String iqpSerno) {
        Map<String, String> map = ctrLoanContService.singleBatchGetContAndPvpInfo(iqpSerno);
        return new ResultDto<>(map);
    }


    /**
     * @param iqpLoanApp
     * @return int
     * @author shenli
     * @date 2021/4/26 0026 18:58
     * @version 1.0.0
     * @desc 小微-贷款申请：提交 生成合同信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/createctrloancontxw")
    protected ResultDto createCtrLoanContXw(@RequestBody IqpLoanApp iqpLoanApp) {
        return ctrLoanContService.createCtrLoanContXw(iqpLoanApp);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/23 0023 15:56
     * @version 1.0.0
     * @desc 小微合同列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/xw/queryCtrLoanContList")
    protected ResultDto<List<CtrLoanCont>> queryCtrLoanContListXw (@RequestBody QueryModel queryModel) {
        return new ResultDto<List<CtrLoanCont>>(ctrLoanContService.queryCtrLoanContListXw(queryModel));
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021-4-27 14:55:22
     * @version 1.0.0
     * @desc 小微贷款合同：合同打印（电子骑缝章）
     * @修改历史: 修改时间:2021年6月17日15:29:41    修改人员：hubp    修改原因:修改入参
     */
    @PostMapping("/xw/printingSeal1")
    protected ResultDto contdoPrintingSeal1Xw(@RequestBody LmtSurveyReportDto lmtSurveyReportDto){
        return ctrLoanContService.contdoPrintingSeal1Xw(lmtSurveyReportDto.getContNo());
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021-4-27
     * @version 1.0.0
     * @desc 小微贷款合同: 合同打印（银行电子公章）
     * @修改历史: 修改时间:2021年6月17日15:29:41    修改人员：hubp    修改原因:修改入参
     */
    @PostMapping("/xw/printingSeal2")
    protected ResultDto contdoPrintingSeal2Xw(@RequestBody LmtSurveyReportDto lmtSurveyReportDto){
        return ctrLoanContService.contdoPrintingSeal2Xw(lmtSurveyReportDto.getContNo());
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/24 0024 11:23
     * @version 1.0.0
     * @desc 小微合同管理列表-签订按钮: 根据合同编号，查询合同基本信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/xw/queryContInfo")
    protected ResultDto<Map<String,Object>> queryContInfoXw(@RequestBody CtrLoanCont ctrLoanCont) {
        return new ResultDto<Map<String,Object>>(ctrLoanContService.queryContInfoXw(ctrLoanCont.getContNo()));
    }


    /**
     * @param signCtrLoanContDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/23 0023 15:56
     * @version 1.0.0
     * @desc 小微-贷款合同: 合同签订
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/xw/signCont")
    protected ResultDto<Integer> signContXw(@RequestBody SignCtrLoanContDto signCtrLoanContDto) throws Exception {
        return new ResultDto<Integer>(ctrLoanContService.signContXw(signCtrLoanContDto));
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/26 0026 21:19
     * @version 1.0.0
     * @desc 小微-合同管理：合同重签
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/xw/contReSign")
    protected ResultDto<Map<String,String>> contReSignXw(@RequestBody CtrLoanCont ctrLoanCont){
        return new ResultDto<Map<String,String>>(ctrLoanContService.contReSignXw(ctrLoanCont.getContNo()));
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/24 0024 16:00
     * @version 1.0.0
     * @desc 小微贷款合同:注销方法
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/xw/cancelCont")
    protected ResultDto<Map<String,String>> cancelContXw(@RequestBody CtrLoanCont ctrLoanCont){
        return new ResultDto<Map<String,String>>(ctrLoanContService.cancelContXw(ctrLoanCont.getContNo()));
    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/24 0024 16:04
     * @version 1.0.0
     * @desc 小微贷款合同: 作废签章审批
     * @修改历史: 修改时间:2021年6月17日15:29:41    修改人员：hubp    修改原因:修改入参和审批状态参数
     */
    @PostMapping("/xw/cancelSign")
    protected ResultDto<Map<String,String>> cancelSignXw(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        return new ResultDto<Map<String,String>>(ctrLoanContService.cancelSignXw(lmtSurveyReportDto.getContNo()));
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map<java.lang.String,java.lang.String>>
     * @author shenli
     * @date 2021/5/4 0004 16:13
     * @version 1.0.0
     * @desc 零售-合同申请列表 flag：1-待完善。2-待签订，3-已签订
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("零售-合同申请列表")
    @PostMapping("/retail/qyeryCtrLoanContList")
    protected ResultDto<List<CtrLoanCont>> qyeryCtrLoanContListRetail(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.qyeryCtrLoanContListRetail(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }


    /**
     * @param ctrLoanContRetailDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/5/5 0005 9:56
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-待完善： 保存功能
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/updateCtrLoanCont")
    protected ResultDto<Map<String,Object>> updateCtrLoanContRetail(@RequestBody CtrLoanContRetailDto ctrLoanContRetailDto) {
        return new ResultDto<Map<String,Object>>(ctrLoanContService.updateCtrLoanContRetail(ctrLoanContRetailDto));
    }

    /**
     * @param signCtrLoanContDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map<java.lang.String,java.lang.Object>>
     * @author shenli
     * @date 2021/5/5 0005 11:02
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-待签订： 签订
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/signCtrLoanCont")
    protected ResultDto<Map<String,Object>> signCtrLoanContRetail(@RequestBody SignCtrLoanContDto signCtrLoanContDto) {
        return new ResultDto<Map<String,Object>>(ctrLoanContService.signCtrLoanContRetail(signCtrLoanContDto));
    }

    /**
     * @param contNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map<java.lang.String,java.lang.Object>>
     * @author shenli
     * @date 2021-5-5 11:07:05
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-已签约：合同打印
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/printing/{contNo}")
    protected ResultDto<Map<String,Object>> contPrintingRetail(@PathVariable("contNo") String contNo){
        return new ResultDto<Map<String,Object>>(ctrLoanContService.contPrintingRetail(contNo));
    }


    /**
     * @param contNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map<java.lang.String,java.lang.Object>>
     * @author shenli
     * @date 2021/5/5 0005 14:07
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-已签订：撤销
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/contReSign/{contNo}")
    protected ResultDto<Map<String,Object>> contReSignRetail(@PathVariable("contNo") String contNo){
        return new ResultDto<Map<String,Object>>(ctrLoanContService.contReSignRetail(contNo));
    }


    /**
     * @param contNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map<java.lang.String,java.lang.String>>
     * @author shenli
     * @date 2021/5/5 0005 14:16
     * @version 1.0.0
     * @desc 零售-合同申请管理-已签订 ：作废
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/cancelCont/{contNo}")
    protected ResultDto<Map<String,String>> cancelContRetail(@PathVariable("contNo") String contNo){
        return new ResultDto<Map<String,String>>(ctrLoanContService.cancelContRetail(contNo));
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021/4/24 0024 11:23
     * @version 1.0.0
     * @desc 小微合同管理列表-签订按钮: 根据合同编号，查询合同基本信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/retail/queryContInfo")
    protected ResultDto<Map<String,Object>> queryContInfoRetail(@RequestBody CtrLoanCont ctrLoanCont) {
        return new ResultDto<Map<String,Object>>(ctrLoanContService.queryContInfoRetail(ctrLoanCont.getContNo()));
    }

    @ApiOperation("贷款出账申请关联合同主表")
    @PostMapping("/selectctrloan")
    protected ResultDto<List<CtrLoanContDto>> getSubInfo(@RequestBody QueryModel queryModel) throws URISyntaxException {
        queryModel.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        List<CtrLoanContDto> result = ctrLoanContService.selectCtrCont(queryModel);
        return new ResultDto<List<CtrLoanContDto>>(result);
    }
    @ApiOperation("贷款出账申请关联合同主表")
    @PostMapping("/selectCtrLoanContAll")
    protected ResultDto<List<CtrLoanContDto>> selectCtrLoanContAll(@RequestBody QueryModel queryModel) throws URISyntaxException {
        List<CtrLoanContDto> result = ctrLoanContService.selectCtrLoanContAll(queryModel);
        return new ResultDto<List<CtrLoanContDto>>(result);
    }

    /**
     * @创建人 zhangliang15
     * @创建时间 2021-09-24 17:27
     * @注释 根据贷款出账信息获取合同或最高额授信协议合同信息
     */
    @PostMapping("/getContInfobyPvpContNo")
    protected ResultDto<CtrLoanContEdit> getContInfobyPvpContNo(@RequestBody Map params) {
        CtrLoanContEdit ctrLoanContEdit = ctrLoanContService.getContInfobyPvpContNo(params);
        return new ResultDto<CtrLoanContEdit>(ctrLoanContEdit);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 17:27
     * @注释 线上提款启用保存
     */
    @PostMapping("/updatabyloandirection")
    protected ResultDto updatabyloandirection(@RequestBody CtrLoanCont ctrLoanCont){
        return   ctrLoanContService.updatabyloandirection(ctrLoanCont);

    }

    /**
     * @创建人
     * @创建时间 2021-10-24 17:27
     * @注释 根据合同号查询合同信息（XDKH0023获取还款日合同到期日）
     */
    @PostMapping("/selectCtrLoanContByContNo")
    protected ResultDto<BizCtrLoanContDto> selectCtrLoanContByContNo(@RequestBody String contNo) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
        BizCtrLoanContDto bizCtrLoanContDto = new BizCtrLoanContDto();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrLoanCont, bizCtrLoanContDto);
        return new ResultDto<BizCtrLoanContDto>(bizCtrLoanContDto);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-07 17:27
     * @注释 影像申请审核合同选择
     */
    @ApiOperation("影像申请审核合同选择")
    @PostMapping("/selectimagectrloan")
    protected ResultDto<List<CtrLoanContDto>> selectImageCtrLoan(@RequestBody QueryModel queryModel) throws URISyntaxException {
        queryModel.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryModel.getCondition().put("belgLine", CmisCommonConstants.STD_BELG_LINE_03);
        List<CtrLoanContDto> result = ctrLoanContService.selectImageCtrLoan(queryModel);
        return new ResultDto<List<CtrLoanContDto>>(result);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-07 17:27
     * @注释 影像申请审核合同选择
     */
    @ApiOperation("诚易融关联合同选择")
    @PostMapping("/selectctrloanCyrRel")
    protected ResultDto<List<CtrLoanContDto>> selectctrloanCyrRel(@RequestBody QueryModel queryModel) throws URISyntaxException {
        queryModel.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryModel.getCondition().put("belgLine", CmisCommonConstants.STD_BELG_LINE_03);
        queryModel.getCondition().put("isCyr", CmisBizConstants.STD_ZB_YES_NO_Y);
        queryModel.getCondition().put("guarMode", CmisBizConstants.STD_ZB_ASSURE_MEANS.DI_YA);
        List<CtrLoanContDto> result = ctrLoanContService.selectctrloanCyrRel(queryModel);
        return new ResultDto<List<CtrLoanContDto>>(result);
    }

    /**
     * @函数名称:toSignListForftin
     * @函数描述:福费廷待签订列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷待签订列表")
    @PostMapping("/forftin/tosignlist")
    protected ResultDto<List<CtrLoanCont>> toSignListForftin(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanCont> list = ctrLoanContService.toSignListForftin(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:福费廷历史合同列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷历史合同列表")
    @PostMapping("/forftin/donesignlist")
    protected ResultDto<List<CtrLoanCont>> doneSignListForftin(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanCont> list = ctrLoanContService.doneSignListForftin(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:logoutForftin
     * @函数描述:福费廷注销
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷注销")
    @PostMapping("/forftin/logout/{contNo}")
    public ResultDto<Map> logoutForftin(@PathVariable("contNo") String contNo) {
        Map params = new HashMap();
        params.put("contNo",contNo);
        Map rtnData= ctrLoanContService.onLogOut(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:logoutForftin
     * @函数描述:福费廷重签
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷重签")
    @PostMapping("/forftin/againsign/{contNo}")
    public ResultDto<Map> againSignForftin(@PathVariable("contNo") String contNo) {
        Map params = new HashMap();
        params.put("contNo",contNo);
        Map rtnData= ctrLoanContService.contReSign(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过合同号查询单条数据
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过合同号查询单条数据")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("contNo",(String)map.get("contNo"));
        ResultDto<Object> resultDto = new ResultDto<Object>();
        CtrLoanCont ctrLoanCont = ctrLoanContService.queryCtrForftinContDataByParams(queryData);
        if (ctrLoanCont != null) {
            resultDto.setCode(200);
            resultDto.setData(ctrLoanCont);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(300);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:contSign
     * @函数描述:合同签订，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("福费廷合同签订")
    @PostMapping("/forftin/contsign")
    public ResultDto<Integer> contSign(@RequestBody CtrLoanCont params) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(params.getContNo());
        if (ctrLoanCont != null) {
            int sign = ctrLoanContService.contSign(ctrLoanCont);

            if (sign > 0) {
                resultDto.setCode(200);
                resultDto.setData(1);
                resultDto.setMessage("合同签订成功！");
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(ctrLoanCont.getIqpSerno());

                // 判断是否为合同续签,若为续签合同则恢复原合同的占额
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsRenew())){
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getOrigiContNo());//合同编号
                    cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
                    cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
                    cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                    cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                    cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                    log.info("福费廷合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",iqpLoanApp.getContNo(), iqpLoanApp.getOrigiContNo(),JSON.toJSONString(cmisLmt0012ReqDto));
                    ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    log.info("福费廷合同【{}】前往额度系统恢复原合同【{}】额度开始,响应报文为：【{}】",iqpLoanApp.getContNo(), iqpLoanApp.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
                    if (!"0".equals(resultDtoDto.getCode())) {
                        log.error("福费廷合同【{}】签订异常");
                        resultDto.setCode(EcbEnum.ECB019999.key);
                        resultDto.setMessage(EcbEnum.ECB019999.value);
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    String code = resultDtoDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.error("福费廷合同【{}】恢复原合同【{}】额度及第三方额度异常,异常信息为【{}】！",iqpLoanApp.getContNo(),iqpLoanApp.getOrigiContNo(),resultDtoDto.getData().getErrorMsg());
                        resultDto.setCode(code);
                        resultDto.setMessage(resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                    }
                }

                // 生成归档任务
                log.info("开始系统生成档案归档信息");
                String cusId = params.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("02");
                docArchiveClientDto.setDocClass("03");
                docArchiveClientDto.setDocType("15");// 15国际业务
                docArchiveClientDto.setDocBizType("05");// 05福费廷
                docArchiveClientDto.setBizSerno(params.getIqpSerno());
                docArchiveClientDto.setCusId(cusId);
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(params.getManagerId());
                docArchiveClientDto.setManagerBrId(params.getManagerBrId());
                docArchiveClientDto.setInputId(params.getInputId());
                docArchiveClientDto.setInputBrId(params.getInputBrId());
                docArchiveClientDto.setContNo(params.getContNo());
                docArchiveClientDto.setLoanAmt(params.getContAmt());
                docArchiveClientDto.setStartDate(params.getContStartDate());
                docArchiveClientDto.setEndDate(params.getContEndDate());
                docArchiveClientDto.setPrdId(params.getPrdId());
                docArchiveClientDto.setPrdName(params.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if(num < 1){
                    log.info("系统生成档案归档信息失败");
                } else {
                    guarBusinessRelService.sendBusinf("03", params.getBizType(), params.getContNo());
                }
            } else {
                resultDto.setCode(300);
                resultDto.setData(0);
                resultDto.setMessage("合同签订失败！");
            }
        } else {
            resultDto.setCode(400);
            resultDto.setData(0);
            resultDto.setMessage("数据不存在！");
        }
        return resultDto;
    }


    /**
     * @函数名称:toSignListTCont
     * @函数描述:贸易融资待签订列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资待签订列表")
    @PostMapping("/tcont/tosignlist")
    protected ResultDto<List<CtrLoanCont>> toSignListTCont(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanCont> list = ctrLoanContService.toSignListTCont(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:doneSignListTCont
     * @函数描述:贸易融资历史合同列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资历史合同列表")
    @PostMapping("/tcont/donesignlist")
    protected ResultDto<List<CtrLoanCont>> doneSignListTCont(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanCont> list = ctrLoanContService.doneSignListTCont(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:logoutForftin
     * @函数描述:福费廷注销
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资注销")
    @PostMapping("/tcont/logout/{contNo}")
    public ResultDto<Map> logoutTCont(@PathVariable("contNo") String contNo) {
        Map params = new HashMap();
        params.put("contNo",contNo);
        Map rtnData= ctrLoanContService.onLogOut(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:logoutForftin
     * @函数描述:贸易融资重签
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资重签")
    @PostMapping("/tcont/againsign/{contNo}")
    public ResultDto<Map> againSignTCont(@PathVariable("contNo") String contNo) {
        Map params = new HashMap();
        params.put("contNo",contNo);
        Map rtnData= ctrLoanContService.contReSign(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:contSign
     * @函数描述:贸易融资合同签订
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贸易融资合同签订")
    @PostMapping("/tcont/contsign")
    public ResultDto<Map> contSignTCont(@RequestBody CtrLoanCont params) {
        log.info("贸易融资合同签订开始");
        ResultDto<Map> resultDto = new ResultDto<Map>();
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(params.getContNo());
        if (ctrLoanCont != null) {
            int sign = ctrLoanContService.contSignTCont(ctrLoanCont);
            QueryModel  model = new QueryModel();
            // 根据业务流水号查询所有担保合同
            model.addCondition("iqpSerno",ctrLoanCont.getIqpSerno());
            model.addCondition("contNo",ctrLoanCont.getContNo());
            List<GrtGuarCont> list = grtGuarContService.selectGuarContByContNo(model);
            if(CollectionUtils.nonEmpty(list)){
                // 主合同下所有的担保合同更新为生效
                for(GrtGuarCont grtGuarCont : list){
                    grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                    if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                        log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                    }
                    grtGuarContService.updateSelective(grtGuarCont);
                    // 信贷业务与押品关联关系信息同步
                    HashMap map =  new HashMap();
                    map.put("bizType","02");
                    map.put("isFlag","1");
                    map.put("bizSerno",ctrLoanCont.getIqpSerno());
                    map.put("guarContNo",grtGuarCont.getGuarContNo());
                    guarBaseInfoService.buscon(map);
                }
            }
            // 除外币 电子保函
            if (sign > 0 && !("610001".equals(ctrLoanCont.getPrdId()) && !"CNY".equals(ctrLoanCont.getCurType()))) {
                resultDto.setCode(200);
                resultDto.setMessage("合同签订成功！");
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(ctrLoanCont.getIqpSerno());

                // 判断是否为合同续签,若为续签合同则恢复原合同的占额
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsRenew())){
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getOrigiContNo());//合同编号
                    cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
                    cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
                    cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                    cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                    cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                    log.info("贸易融资合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",iqpLoanApp.getContNo(), iqpLoanApp.getOrigiContNo(),JSON.toJSONString(cmisLmt0012ReqDto));
                    ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    log.info("贸易融资合同【{}】前往额度系统恢复原合同【{}】额度开始,响应报文为：【{}】",iqpLoanApp.getContNo(), iqpLoanApp.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
                    if (!"0".equals(resultDtoDto.getCode())) {
                        log.error("贸易融资合同【{}】签订异常");
                        resultDto.setCode(EcbEnum.ECB019999.key);
                        resultDto.setMessage(EcbEnum.ECB019999.value);
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    String code = resultDtoDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.error("贸易融资合同【{}】恢复原合同【{}】额度异常！,异常信息为【{}】",iqpLoanApp.getContNo(), iqpLoanApp.getOrigiContNo(),resultDtoDto.getData().getErrorMsg());
                        resultDto.setCode(code);
                        resultDto.setMessage(resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                    }
                }

                // 生成归档任务
                log.info("开始系统生成档案归档信息");
                String cusId = params.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("02");
                docArchiveClientDto.setDocClass("03");
                docArchiveClientDto.setDocType("15");// 贸易融资 => 15国际业务
                docArchiveClientDto.setDocBizType("07");// 07贸易融资
                docArchiveClientDto.setBizSerno(params.getIqpSerno());
                docArchiveClientDto.setCusId(cusId);
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(params.getManagerId());
                docArchiveClientDto.setManagerBrId(params.getManagerBrId());
                docArchiveClientDto.setInputId(params.getInputId());
                docArchiveClientDto.setInputBrId(params.getInputBrId());
                docArchiveClientDto.setContNo(params.getContNo());
                docArchiveClientDto.setLoanAmt(params.getContAmt());
                docArchiveClientDto.setStartDate(params.getContStartDate());
                docArchiveClientDto.setEndDate(params.getContEndDate());
                docArchiveClientDto.setPrdId(params.getPrdId());
                docArchiveClientDto.setPrdName(params.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if(num < 1){
                    log.info("系统生成档案归档信息失败");
                } else {
                    // 信贷业务合同信息同步
                    guarBusinessRelService.sendBusinf("03", params.getBizType(), params.getContNo());
                    // 根据业务流水号同步押品与业务关联信息
                    // TODO (逻辑需要调整)guarBusinessRelService.sendBuscon(params.getIqpSerno(), CmisCommonConstants.STD_BUSI_TYPE_04);
                }
            } else {
                resultDto.setCode(300);
                resultDto.setMessage("合同签订失败！");
                return resultDto;
            }
        } else {
            resultDto.setCode(400);
            resultDto.setMessage("数据不存在！");
            return resultDto;
        }
        return resultDto;
    }

    /**
     * @方法名称：stockContract
     * @方法描述：存量合同
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:53
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/stockcontract")
    public ResultDto<List<AllContDto>> stockContract(@RequestBody QueryModel queryModel) {
        List<AllContDto> allContDtoList = ctrLoanContService.stockContract(queryModel);
        return new ResultDto<>(allContDtoList);
    }


    /**
     * @author zlf
     * @date 2021/5/21 10:50
     * @version 1.0.0
     * @desc     根据账号查询帐户信息
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/opanorgname")
    public ResultDto getOpanOrgName(@RequestBody IqpLoanApp iqpLoanApp) {
        ResultDto rusult = ctrLoanContService.getOpanOrgName(iqpLoanApp.getLoanPayoutAccno());
        return rusult;
    }

    /**
     * @方法名称：stockContract
     * @方法描述：存量合同
     * @创建人：yangwl
     * @创建时间：2021/5/24 20:53
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/stockcontractacc")
    public ResultDto<List<AllContDto>> stockContractAcc(@RequestBody QueryModel queryModel) {
        List<AllContDto> allContDtoList = ctrLoanContService.stockContractAcc(queryModel);
        return new ResultDto<>(allContDtoList);
    }

    @ApiOperation("普通贷款合同注销")
    @PostMapping("/logout")
    protected ResultDto<Integer> logout(@RequestBody CtrLoanCont ctrLoanCont) throws URISyntaxException {
        int result = ctrLoanContService.logout(ctrLoanCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询待处理数据")
    @PostMapping("/selectNumByInputId")
    protected ResultDto<List<Map<String, Object>>> selectNumByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = ctrLoanContService.selectNumByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:getAllContByInputId
     * @函数描述:根据客户经理工号查询合同待签订数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询合同待签订数据")
    @PostMapping("/getAllContByInputId")
    protected ResultDto<List<Map<String, Object>>> getAllContByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = ctrLoanContService.getAllContByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }


    /**
     * @函数名称:selectCtrLoanContListDataForZhcx
     * @函数描述:普通贷款合同查询方法
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectctrloancontlistdataforzhcx")
    protected ResultDto<List<CtrLoanContForZhcxDto>> selectCtrLoanContListDataForZhcx(@RequestBody QueryModel queryModel) {
        List<CtrLoanContForZhcxDto> list = ctrLoanContService.selectCtrLoanContListDataForZhcx(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述: 重写列表
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CtrLoanCont>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectByModelCtr(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }
    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrLoanCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("iqpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrLoanCont> list = ctrLoanContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrLoanCont>>(list);
    }

    /**
     * @函数名称:checkGuarRelContStatus
     * @函数描述:根据押品编号查询押品对应的合同状态
     * @参数与返回说明:
     * @param guarNo
     * @算法描述:
     */
    @GetMapping("/selectContStatusByGuarNo/{guarNo}")
    protected ResultDto<String> checkGuarRelContStatus(@PathVariable("guarNo") String guarNo) {
        String contStatus = ctrLoanContService.selectContStatusByGuarNo(guarNo);
        return new ResultDto<String>(contStatus);
    }

    /**
     * @函数名称:queryCtrLoanContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrloancontdatabyiqperno")
    protected ResultDto<CtrLoanCont> queryCtrLoanContDataBySerno(@RequestBody String iqpSerno) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByIqpSerno(iqpSerno);
        return new ResultDto<CtrLoanCont>(ctrLoanCont);
    }

    /**
     * @函数名称:queryCtrLoanContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据担保合同编号更新关联主合同为线上启用")
    @PostMapping("/updateisonlinedraw")
    protected ResultDto<Integer> updateisOnlineDraw(@RequestBody GrtGuarCont grtGuarCont) {
        return new ResultDto<Integer>(ctrLoanContService.updateisOnlineDraw(grtGuarCont.getGuarContNo()));
    }

    /**
     * @函数名称:queryCtrLoanContDataBySerno
     * @函数描述:零售-业务申请历史-作废
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("零售-业务申请历史-作废 ")
    @PostMapping("/canceliqpretail")
    protected ResultDto<Integer> cancelIqpRetail(@RequestBody String IqpSerno) {
        return new ResultDto<Integer>(ctrLoanContService.cancelIqpRetail(IqpSerno));
    }

    /**
     * @函数名称:queryCtrLoanListByCusId
     * @函数描述:根据客户ID查询合同列表信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryctrloanlistbycusid")
    protected ResultDto<List<CtrLoanCont>> queryCtrLoanListByCusId(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.queryCtrLoanListByCusId(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }
    /**
     * @函数名称:selectSxkdContList
     * @函数描述: 获取省心快贷合同信息
     * @参数与返回说明:
     * 分页查询类
     * @算法描述:
     * @创建人：zl
     */
    @ApiOperation(value = "获取省心快贷合同信息")
    @PostMapping("/selectSxkdContList")
    protected ResultDto<List<CtrLoanCont>> selectSxkdContList(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectSxkdContList(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }


    @PostMapping("/judgeQF")
    public ResultDto<String> judgeQFXX(@RequestBody IqpLoanApp iqpLoanApp) {
        String qfxx = ctrLoanContService.judgeQFXXSer(iqpLoanApp.getIqpSerno());
        return new ResultDto<String>(qfxx);
    }

    /**
     * @创建人 css
     * @创建时间 2021-09-14 15:27
     * @注释 其他事项申报合同选择
     */
    @ApiOperation("其他事项申报合同选择")
    @PostMapping("/selectotherappctrloan")
    protected ResultDto<Map> selectOtherAppCtrLoan(@RequestBody QueryModel queryModel) throws URISyntaxException {
        List<Map> result = ctrLoanContService.selectOtherThingsAppLoan(queryModel);
        if(CollectionUtils.isEmpty(result)){
            List<CtrLoanContDto> result1 = ctrLoanContService.selectImageCtrLoan(queryModel);
            if(CollectionUtils.isEmpty(result1)){
                return new ResultDto<>(null);
            }else{
                return new ResultDto<Map>((Map)ctrLoanContService.selectImageCtrLoan(queryModel).get(0));
            }
        }else{
            return new ResultDto<>(result.get(0));
        }

    }

    /**
     * @函数名称:selectUnClearContByCusId
     * @函数描述:查询借款人名下未结清的合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectUnClearContByCusId/{cusId}")
    protected ResultDto<List<CtrLoanCont>> selectUnClearContByCusId(@PathVariable("cusId") String cusId) {
        List<CtrLoanCont> list = ctrLoanContService.selectUnClearContByCusId(cusId);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:selectPrdIdByContNo
     * @函数描述:根据合同编号查询产品编号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectprdidbycontno")
    @ApiOperation("根据合同编号查询产品编号")
    protected ResultDto<String> selectPrdIdByContNo(@RequestBody String contNo) {
        String prdId = ctrLoanContService.selectPrdIdByContNo(contNo);
        return new ResultDto<>(prdId);
    }
    /**
     * @函数名称:selectbycusid
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述: 根据客户号，产品编号固定资产贷款信息
     */
    @PostMapping("/selectctrloancontbycusid")
    protected ResultDto<List<CtrLoanCont>> selectCtrLoanContByCusId(@RequestBody QueryModel queryModel) {
        List<CtrLoanCont> list = ctrLoanContService.selectCtrLoanContByCusId(queryModel);
        return new ResultDto<List<CtrLoanCont>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<CtrLoanCont> selectByPrimaryKey(@RequestBody CtrLoanCont ctrLoanCont ) {
        return new ResultDto<CtrLoanCont>(ctrLoanContService.selectByPrimaryKey(ctrLoanCont.getContNo()));
    }

    /**
     * @函数名称:updatepapercontsigndate
     * @函数描述:根据合同号更新纸质签订日期
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatepapercontsigndate")
    protected ResultDto<Integer> updatePaperContSignDate(@RequestBody CtrLoanCont ctrLoanCont) throws URISyntaxException {
        int result = ctrLoanContService.updatePaperContSignDate(ctrLoanCont);
        return new ResultDto<Integer>(result);
    }


}