/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpLoanAppMergerConSub;
import cn.com.yusys.yusp.domain.PvpLoanAppSegInterstSub;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.service.ToppAcctSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAcctSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:12:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "交易对手账户")
@RequestMapping("/api/toppacctsub")
public class ToppAcctSubResource {
    @Autowired
    private ToppAcctSubService toppAcctSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ToppAcctSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<ToppAcctSub> list = toppAcctSubService.selectAll(queryModel);
        return new ResultDto<List<ToppAcctSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ToppAcctSub>> index(QueryModel queryModel) {
        List<ToppAcctSub> list = toppAcctSubService.selectByModel(queryModel);
        return new ResultDto<List<ToppAcctSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ToppAcctSub> show(@PathVariable("pkId") String pkId) {
        ToppAcctSub toppAcctSub = toppAcctSubService.selectByPrimaryKey(pkId);
        return new ResultDto<ToppAcctSub>(toppAcctSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<ToppAcctSub> create(@RequestBody ToppAcctSub toppAcctSub) throws URISyntaxException {
        toppAcctSubService.insert(toppAcctSub);
        return new ResultDto<ToppAcctSub>(toppAcctSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ToppAcctSub toppAcctSub) throws URISyntaxException {
        int result = toppAcctSubService.updateSelective(toppAcctSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = toppAcctSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = toppAcctSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 17:01
     * @注释 改为POST的条件查询
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<ToppAcctSub>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<ToppAcctSub> list = toppAcctSubService.selectByModel(queryModel);
        return new ResultDto<List<ToppAcctSub>>(list);
    }

    /**
     * 贷款出账申请交易对手账户明细,新增弹框的保存
     *
     * @param toppAcctSub
     * @return
     * @Author:zxz
     */
    @PostMapping("/savetoppacctsub")
    protected ResultDto<Map> savetoppacctsub(@RequestBody ToppAcctSub toppAcctSub) throws URISyntaxException {
        Map result = toppAcctSubService.saveToppAcctSub(toppAcctSub);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("交易对手账户分页查询")
    @PostMapping("/querytoppacctsub")
    protected ResultDto<List<ToppAcctSub>> queryToppAcctSub(@RequestBody QueryModel queryModel) {
        List<ToppAcctSub> list = toppAcctSubService.queryToppAcctSub(queryModel);
        return new ResultDto<List<ToppAcctSub>>(list);
    }

    /**
     * @函数名称:saveToppAcctSub
     * @函数描述:交易对手账户新增
     * @参数与返回说明:toppAcctSub
     * @创建人:zhanyb
     */
    @ApiOperation("交易对手账户新增")
    @PostMapping("/saveToppAcctSub")
    protected ResultDto<Map> saveToppAcctSub(@RequestBody ToppAcctSub toppAcctSub) throws URISyntaxException {
        ResultDto<ToppAcctSub> resultDto = new ResultDto<ToppAcctSub>();
        Map result = toppAcctSubService.saveToppAcctSub(toppAcctSub);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:saveToppAcctSub
     * @函数描述:交易对手账户修改
     * @参数与返回说明:toppAcctSub
     * @创建人:zhanyb
     */
    @ApiOperation("交易对手账户修改")
    @PostMapping("/commonupdatetoppacctsub")
    public ResultDto<Map> commonUpdateToppAcctSub(@RequestBody ToppAcctSub toppAcctSub){
        Map rtnData = toppAcctSubService.commonUpdateToppAcctSub(toppAcctSub);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:params
     * @函数描述:交易对手账户查看
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("交易对手账户查看")
    @PostMapping("/showdetialsub")
    protected ResultDto<Object> showDetialSub(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        ToppAcctSub temp = new ToppAcctSub();
        ToppAcctSub studyDemo = toppAcctSubService.selectByPrimaryKey((String)params.get(("pkId")));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<List<ToppAcctSub>> selectBySerno(@RequestBody QueryModel model) {
        List<ToppAcctSub> list = toppAcctSubService.selectBySerno(model);
        return new ResultDto<List<ToppAcctSub>>(list);
    }

    /**
     * @函数名称:insertBySerno
     * @函数描述:通过流水号新增参与行信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertBySerno")
    protected ResultDto<Integer> insertBySerno(@RequestBody ToppAcctSub ToppAcctSub) {
        int result = toppAcctSubService.insertBySerno(ToppAcctSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/22 19:49
     * @注释 逻辑删除 -》修改
     */
    @PostMapping("/deleteByPkId")
    protected ResultDto<Integer> deleteByPkId(@RequestBody ToppAcctSub toppAcctSub) {
        int result = toppAcctSubService.deleteByPkId(toppAcctSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletebyprimarykey")
    protected ResultDto<Integer> deleteByPrimaryKey(@RequestBody ToppAcctSub toppAcctSub) {
        int result = toppAcctSubService.deleteByPrimaryKey(toppAcctSub.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 mashun
     * @创建时间 2021-05-08 17:01
     * @注释
     */
    @PostMapping("/selectdistinctacctbymodel")
    protected ResultDto<List<ToppAcctSub>> selectDistinctAcctByModel(@RequestBody QueryModel queryModel) {
        List<ToppAcctSub> list = toppAcctSubService.selectDistinctAcctByModel(queryModel);
        return new ResultDto<List<ToppAcctSub>>(list);
    }

}
