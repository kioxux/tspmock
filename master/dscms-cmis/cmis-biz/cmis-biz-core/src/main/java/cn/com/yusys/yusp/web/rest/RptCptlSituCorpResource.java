/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCptlSituCorp;
import cn.com.yusys.yusp.service.RptCptlSituCorpService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituCorpResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 20:35:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcptlsitucorp")
public class RptCptlSituCorpResource {
    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCptlSituCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCptlSituCorp> list = rptCptlSituCorpService.selectAll(queryModel);
        return new ResultDto<List<RptCptlSituCorp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCptlSituCorp>> index(QueryModel queryModel) {
        List<RptCptlSituCorp> list = rptCptlSituCorpService.selectByModel(queryModel);
        return new ResultDto<List<RptCptlSituCorp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptCptlSituCorp> show(@PathVariable("pkId") String pkId) {
        RptCptlSituCorp rptCptlSituCorp = rptCptlSituCorpService.selectByPrimaryKey(pkId);
        return new ResultDto<RptCptlSituCorp>(rptCptlSituCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCptlSituCorp> create(@RequestBody RptCptlSituCorp rptCptlSituCorp) throws URISyntaxException {
        rptCptlSituCorpService.insert(rptCptlSituCorp);
        return new ResultDto<RptCptlSituCorp>(rptCptlSituCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCptlSituCorp rptCptlSituCorp) throws URISyntaxException {
        int result = rptCptlSituCorpService.update(rptCptlSituCorp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptCptlSituCorpService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptCptlSituCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptCptlSituCorp>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptCptlSituCorp>>(rptCptlSituCorpService.selectByModel(model));
    }

    @PostMapping("/insertCorp")
    protected ResultDto<Integer> insertCorp(@RequestBody RptCptlSituCorp rptCptlSituCorp){
        rptCptlSituCorp.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptCptlSituCorpService.insertSelective(rptCptlSituCorp));
    }

    @PostMapping("/updateCorp")
    protected ResultDto<Integer> updateCorp(@RequestBody RptCptlSituCorp rptCptlSituCorp){
        return new ResultDto<Integer>(rptCptlSituCorpService.updateSelective(rptCptlSituCorp));
    }

    @PostMapping("/deleteCorp")
    protected ResultDto<Integer> deleteCorp(@RequestBody RptCptlSituCorp rptCptlSituCorp){
        String pkId = rptCptlSituCorp.getPkId();
        return new ResultDto<Integer>(rptCptlSituCorpService.deleteByPrimaryKey(pkId));
    }

    /**
     * 查询集团融资情况
     * @param model
     * @return
     */
    @PostMapping("/selectGrpCorp")
    protected ResultDto<List<Map<String,Object>>> selectGrpCorp(@RequestBody QueryModel model){
        return new ResultDto<List<Map<String, Object>>>(rptCptlSituCorpService.selectGrpCorp(model));
    }
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptCptlSituCorp>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptCptlSituCorp>>(rptCptlSituCorpService.selectBySerno(serno));
    }
}
