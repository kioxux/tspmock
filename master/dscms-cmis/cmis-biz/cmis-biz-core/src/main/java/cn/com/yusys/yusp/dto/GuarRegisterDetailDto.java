package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarRegisterDetail
 * @类描述: guar_register_detail数据实体类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-04-19 11:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarRegisterDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	private String serno;

	/** 抵押登记流水号 **/
	private String regSerno;

	/** 担保合同编号 **/
	private String guarContNo;

	/** 押品编号 **/
	private String guarNo;

	/** 担保分类代码 **/
	private String guarTypeCd;

	/** 押品名称 **/
	private String pldimnMemo;

	/** 登记办理状态 STD_REG_STATUS **/
	private String regStatus;

	/** 押品任务状态STD_TASK_STATUS_YP **/
	private String taskStatus;

	/** 操作类型 **/
	private String oprType;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 主办人 **/
	private String managerId;

	/** 主办机构 **/
	private String managerBrId;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param regSerno
	 */
	public void setRegSerno(String regSerno) {
		this.regSerno = regSerno == null ? null : regSerno.trim();
	}

	/**
	 * @return RegSerno
	 */
	public String getRegSerno() {
		return this.regSerno;
	}

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}

	/**
	 * @return GuarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}

	/**
	 * @return GuarNo
	 */
	public String getGuarNo() {
		return this.guarNo;
	}

	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd == null ? null : guarTypeCd.trim();
	}

	/**
	 * @return GuarTypeCd
	 */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}

	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}

	/**
	 * @return PldimnMemo
	 */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}

	/**
	 * @param regStatus
	 */
	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus == null ? null : regStatus.trim();
	}

	/**
	 * @return RegStatus
	 */
	public String getRegStatus() {
		return this.regStatus;
	}

	/**
	 * @param taskStatus
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus == null ? null : taskStatus.trim();
	}

	/**
	 * @return TaskStatus
	 */
	public String getTaskStatus() {
		return this.taskStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return CreateTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return UpdateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}