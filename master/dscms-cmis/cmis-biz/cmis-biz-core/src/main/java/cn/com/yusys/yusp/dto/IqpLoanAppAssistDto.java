package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.IqpLoanAppAssist;

public class IqpLoanAppAssistDto extends IqpLoanAppAssist {

    private String managerBrId;

    private String managerBrName;

    private String inputId;

    private String inputBrId;

    private String inputDate;

    private String stdZbOprType;

    private String updId;

    private String updBrId;

    private String updDate;

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerBrName() {
        return managerBrName;
    }

    public void setManagerBrName(String managerBrName) {
        this.managerBrName = managerBrName;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getStdZbOprType() {
        return stdZbOprType;
    }

    public void setStdZbOprType(String stdZbOprType) {
        this.stdZbOprType = stdZbOprType;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }
}
