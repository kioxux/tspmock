package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.CommonUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CardPrdCode;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.req.Lsnp02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.resp.Lsnp02RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.req.D11005ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.resp.D11005RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.resp.Xdkh0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.*;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultNodeDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credxx.CredxxService;
import cn.com.yusys.yusp.service.client.cfg.AdminSmTreeDicService;
import cn.com.yusys.yusp.service.client.cus.xdkh0019.Xdkh0019Service;
import cn.com.yusys.yusp.util.CmisBizCiis2ndUtils;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-05-24 11:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardAppInfoService {

    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private CreditCardAppInfoMapper creditCardAppInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CreditCardAttachmentInfoService creditCardAttachmentInfoService;
    @Autowired
    private CreditCardFirstJudgInifoService creditCardFirstJudgInifoService;
    @Autowired
    private CreditCardFinalJudgInifoService creditCardFinalJudgInifoservice;
    @Autowired
    private IqpCusLsnpInfoService iqpCusLsnpInfoService;
    @Autowired
    private Dscms2LsnpClientService dscms2LsnpClientService;
    @Autowired
    private IqpCusLsnpInfoMapper iqpCusLsnpInfoMapper;
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;
    @Autowired
    private CreditCardTelvPerDetailService creditCardTelvPerDetailService;
    @Autowired
    private CreditCardTelvQuestionDetailService creditCardTelvQuestionDetailService;
    @Autowired
    private CreditCardTelvOtherInfoService creditCardTelvOtherInfoService;
    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CredxxService credxxService;
    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;
    @Autowired
    private CreditQryBizRealService creditQryBizRealService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Value("${application.creditUrl.url}")
    private String creditUrl;
    @Autowired
    private  CreditCardAmtInfoService creditCardAmtInfoService;
    @Autowired
    private AdminSmTreeDicService adminSmTreeDicService;
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;
    @Autowired
    private Xdkh0019Service xdkh0019Service;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private CreditCardAmtInfoMapper creditCardAmtInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditCardAppInfo selectByPrimaryKey(String serno) {
        return creditCardAppInfoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CreditCardAppInfo> selectAll(QueryModel model) {
        List<CreditCardAppInfo> records = (List<CreditCardAppInfo>) creditCardAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditCardAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAppInfo> list = creditCardAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CreditCardAppInfo record) {
        return creditCardAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CreditCardAppInfo record) {
        return creditCardAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CreditCardAppInfo record) {
        String serno = record.getSerno();
        List<CreditCardFinalJudgInifo>  finalJudgInifo = creditCardFinalJudgInifoservice.selectByIqpSerno(serno);
        for(int i=0;i<finalJudgInifo.size();i++){
            CreditCardFinalJudgInifo creditCardFinalJudgInifo = finalJudgInifo.get(i);
            creditCardFinalJudgInifo.setIsApplyCommonCard(record.getIsApplyCommonCard());
            creditCardFinalJudgInifo.setApplyCardPrd(record.getApplyCardPrd());
            creditCardFinalJudgInifo.setCommonCardPrd(record.getApplyCommonCardPrd());
            int result = creditCardFinalJudgInifoservice.updateSelective(creditCardFinalJudgInifo);
            if(result !=1){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "更新数据异常！");
            }
        }
        return creditCardAppInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CreditCardAppInfo record) {
        return creditCardAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        CreditCardAppInfo creditCardAppInfo = creditCardAppInfoMapper.selectByPrimaryKey(serno);
        if (CmisCommonConstants.WF_STATUS_992.equals(creditCardAppInfo.getApproveStatus())) {
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId："+serno);
            // 删除流程实例
            workflowCoreClient.deleteByBizId(serno);
            //更改状态为自行退出，删除
            creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            creditCardAppInfo.setOprType("02");
            return creditCardAppInfoMapper.updateByPrimaryKey(creditCardAppInfo);
        }else{
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：",serno);
            // 删除流程实例
            workflowCoreClient.deleteByBizId(serno);
            return creditCardAppInfoMapper.deleteByPrimaryKey(serno);
        }
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardAppInfoMapper.deleteByIds(ids);
    }

    /**
     * @param creditCardAppInfo
     * @return CreditCardAppInfo
     * @author wzy
     * @date 2021/5/24 16:06
     * @version 1.0.0
     * @desc 信用卡申请信息预录入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public CreditCardAppInfo addCreditAppInfo(CreditCardAppInfo creditCardAppInfo) {
        int result;
        try {
            String serno = creditCardAppInfo.getSerno();
            CreditCardAppInfo record = this.selectByPrimaryKey(serno);
            if (record == null) {
                log.info("信用卡申请信息预录入开始【{}】", serno);
                //证件号
                String certCode = creditCardAppInfo.getCertCode();
                //申请卡产品
                String applyCardPrd = creditCardAppInfo.getApplyCardPrd();
                log.info("校验该客户是否存在相同产品在途申请");
                Map param = new HashMap();
                param.put("certCode", certCode);
                param.put("applyCardPrd", applyCardPrd);
                result = creditCardAppInfoMapper.queryByCusAndPrd(param);
                if (result > 0) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该客户下已存在正在审批中的相同产品！");
                }
                creditCardAppInfo.setSerno(serno);
                creditCardAppInfo.setBizSerno(serno);
                creditCardAppInfo.setOprType("01");//新增
                creditCardAppInfo.setApproveStatus("000");//待发起
                String applyType = creditCardAppInfo.getApplyType();
                //独立附卡
                if( "S".equals(applyType)){
                    //副卡申请人信息
                    creditCardAppInfo.setSubCardCusName(creditCardAppInfo.getCusName());
                    creditCardAppInfo.setSubCardCusPhone(creditCardAppInfo.getPhone());
                    creditCardAppInfo.setSubCardCertType(creditCardAppInfo.getCertType());
                    creditCardAppInfo.setSubCardCertCode(creditCardAppInfo.getCertCode());
                    creditCardAppInfo.setSubCardCusSex(creditCardAppInfo.getSex());
                }
                //约定还款人扣款姓名
                creditCardAppInfo.setAgreedRepayCusName(creditCardAppInfo.getCusName());
                //取系统日期
                String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                creditCardAppInfo.setAppDate(openDay);//申请日期
                creditCardAppInfo.setBusinessStage("A01");//信息预录入
                result = this.insertSelective(creditCardAppInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡向导信息新增异常！");
                }
                CreditCardAttachmentInfo creditCardAttachmentInfo = new CreditCardAttachmentInfo();
                creditCardAttachmentInfo.setSerno(serno);
                result = creditCardAttachmentInfoService.insertSelective(creditCardAttachmentInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡附件信息新增异常！");
                }
                log.info("信用卡申请信息预录入结束【{}】", serno);
            } else {
                if("S".equals(creditCardAppInfo.getApplyType())){
                    //约定还款人扣款姓名
                    creditCardAppInfo.setAgreedRepayCusName(creditCardAppInfo.getCusName());
                    creditCardAppInfo.setSubCardCusName(creditCardAppInfo.getCusName());
                    creditCardAppInfo.setSubCardCusPhone(creditCardAppInfo.getPhone());
                    creditCardAppInfo.setSubCardCertType(creditCardAppInfo.getCertType());
                    creditCardAppInfo.setSubCardCertCode(creditCardAppInfo.getCertCode());
                    creditCardAppInfo.setSubCardCusSex(creditCardAppInfo.getSex());
                }
                result = this.updateSelective(creditCardAppInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡向导信息新增异常！");
                }
            }
        } catch (Exception e) {
            log.info("申请向导新增报错{}", e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return creditCardAppInfo;
    }

    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDatFirst(String serno,String nodeId) {
        log.info("流程发起-获取业务申请" + serno + "申请主表信息");
        CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
        //是否申请普卡
        String isapplycommon = creditCardAppInfo.getIsApplyCommonCard();
        //审批结果
        String approveResult ="00";
        //普卡审批结果
        String commonApproveResult="00";
        //审批金额
        BigDecimal approveAmt =new BigDecimal("0");
        //普卡审批金额
        BigDecimal commonCardApproveAmt=new BigDecimal("0");
        //日费率
        BigDecimal dailyRate = null;
        //查询终审二审信息
        CreditCardFinalJudgInifo param = new CreditCardFinalJudgInifo();
        //如果在初审一岗就结束,无需判断终审审批结果，审批金额取零售内评建议额度
        if("242_7".equals(nodeId)){
            approveResult = "00";
            commonApproveResult = "00";
            IqpCusLsnpInfo record = new IqpCusLsnpInfo();
            record.setIqpSerno(serno);
            record.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
            IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSernoAndPrd(record);
            if(iqpCusLsnpInfo != null){
                approveAmt = iqpCusLsnpInfo.getLmtAdvice();
            }
            if("1".equals(isapplycommon)){
                record.setApplyCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSernoAndPrd(record);
                commonCardApproveAmt = iqpCusLsnpInfo.getLmtAdvice();
            }
        }else if("242_12".equals(nodeId)) {
            //如果在终审一岗就结束审批,查询终审一岗审批数据
            param.setSerno(serno);
            param.setFinalPostFlag("00");
            CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(param);
            //审批结果
            approveResult = creditCardFinalJudgInifo.getApproveResult();
            //普卡审批结果
            commonApproveResult = creditCardFinalJudgInifo.getCommonCardApproveResult();
            //审批金额
            approveAmt = creditCardFinalJudgInifo.getApproveAmt();
            //普卡审批金额
            commonCardApproveAmt = creditCardFinalJudgInifo.getCommonCardApproveAmt();
            dailyRate = creditCardFinalJudgInifo.getDailyFeeRate();
        }else{
            //终审二岗结束查询终审二岗审批结果
            param.setSerno(serno);
            param.setFinalPostFlag("01");
            CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(param);
            //审批结果
            approveResult = creditCardFinalJudgInifo.getApproveResult();
            //普卡审批结果
            commonApproveResult = creditCardFinalJudgInifo.getCommonCardApproveResult();
            //审批金额
            approveAmt = creditCardFinalJudgInifo.getApproveAmt();
            //普卡审批金额
            commonCardApproveAmt = creditCardFinalJudgInifo.getCommonCardApproveAmt();
            dailyRate = creditCardFinalJudgInifo.getDailyFeeRate();
        }
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CreditCardAmtInfoMapper creditCardAmtInfoMapper = sqlSession.getMapper(CreditCardAmtInfoMapper.class);
            //生成额度信息
            log.info("生成额度信息开始！"+serno);
            CreditCardAmtInfo creditCardAmtInfo = new CreditCardAmtInfo();
            creditCardAmtInfo.setPkId(UUID.randomUUID().toString());
            creditCardAmtInfo.setSerno(serno);
            if("00".equals(approveResult)){
                creditCardAmtInfo.setOprType("P");
            }else{
                creditCardAmtInfo.setOprType("R");
            }
            creditCardAmtInfo.setAprroveAmt(approveAmt);
            creditCardAmtInfo.setCardPrd(creditCardAppInfo.getApplyCardPrd());
            creditCardAmtInfo.setDailyFeeRate(dailyRate);
            creditCardAmtInfo.setInputId(creditCardAppInfo.getInputId());
            creditCardAmtInfo.setInputBrId(creditCardAppInfo.getInputBrId());
            creditCardAmtInfoMapper.insertSelective(creditCardAmtInfo);
            if("1".equals(isapplycommon)){
                //生成额度信息
                log.info("生成普卡额度信息开始！"+serno);
                CreditCardAmtInfo creditCardAmtInfo2 = new CreditCardAmtInfo();
                creditCardAmtInfo2.setPkId(UUID.randomUUID().toString());
                creditCardAmtInfo2.setSerno(serno);
                creditCardAmtInfo2.setAprroveAmt(commonCardApproveAmt);
                creditCardAmtInfo2.setCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                creditCardAmtInfo2.setInputId(creditCardAppInfo.getInputId());
                creditCardAmtInfo2.setInputBrId(creditCardAppInfo.getInputBrId());
                if("00".equals(commonApproveResult)){
                    creditCardAmtInfo2.setOprType("P");
                }else{
                    creditCardAmtInfo2.setOprType("R");
                }
               creditCardAmtInfoMapper.insertSelective(creditCardAmtInfo2);
             }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        log.info("信用卡生成额度信息结束："+serno);
    }

    /**
     * @param serno
     * @return
     * @author wzy
     * @date 2021/5/25 9:57
     * @version 1.0.0
     * @desc 信用卡申请流程处理类, 信用卡开卡，如果同时申请普卡，进行普卡开卡
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String serno, String firstflag,String nodeId) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
            //是否申请普卡
            String isapplycommon = creditCardAppInfo.getIsApplyCommonCard();
            //审批结果
            String approveResult ="00";
            //普卡审批结果
            String commonApproveResult="00";
            //审批金额
            BigDecimal approveAmt =new BigDecimal("0");
            //普卡审批金额
            BigDecimal commonCardApproveAmt=new BigDecimal("0");
            //日费率
            BigDecimal dailyRate = new BigDecimal(0);
            //查询终审二审信息
            CreditCardFinalJudgInifo param = new CreditCardFinalJudgInifo();
            //如果在初审一岗就结束,无需判断终审审批结果，审批金额取零售内评建议额度
            if("242_7".equals(nodeId)){
                approveResult = "00";
                commonApproveResult = "00";
                IqpCusLsnpInfo record = new IqpCusLsnpInfo();
                record.setIqpSerno(serno);
                record.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
                IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSernoAndPrd(record);
                if(iqpCusLsnpInfo != null){
                    approveAmt = iqpCusLsnpInfo.getLmtAdvice();
                    dailyRate = iqpCusLsnpInfo.getDailyFeeRate();
                }
               if("1".equals(isapplycommon)){
                   record.setApplyCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                   iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSernoAndPrd(record);
                   commonCardApproveAmt = iqpCusLsnpInfo.getLmtAdvice();
               }
            }else if("242_12".equals(nodeId)) {
            //如果在终审一岗就结束审批,查询终审一岗审批数据
                param.setSerno(serno);
                param.setFinalPostFlag("00");
                CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(param);
                //审批结果
                approveResult = creditCardFinalJudgInifo.getApproveResult();
                //普卡审批结果
                commonApproveResult = creditCardFinalJudgInifo.getCommonCardApproveResult();
                //审批金额
                approveAmt = creditCardFinalJudgInifo.getApproveAmt();
                //普卡审批金额
                commonCardApproveAmt = creditCardFinalJudgInifo.getCommonCardApproveAmt();
                dailyRate = creditCardFinalJudgInifo.getDailyFeeRate();
            }else{
                //终审二岗结束查询终审二岗审批结果
                param.setSerno(serno);
                param.setFinalPostFlag("01");
                CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(param);
                //审批结果
                approveResult = creditCardFinalJudgInifo.getApproveResult();
                //普卡审批结果
                commonApproveResult = creditCardFinalJudgInifo.getCommonCardApproveResult();
                //审批金额
                approveAmt = creditCardFinalJudgInifo.getApproveAmt();
                //普卡审批金额
                commonCardApproveAmt = creditCardFinalJudgInifo.getCommonCardApproveAmt();
                dailyRate = creditCardFinalJudgInifo.getDailyFeeRate();
            }
            if (!"2".equals(firstflag)) {
                //拼装请求报文参数
                D11005ReqDto reqDto = new D11005ReqDto();
                //申请类型
                String applyType = creditCardAppInfo.getApplyType();
                reqDto.setApptyp(applyType);
                //主附同申,独立主卡
                if("A".equals(applyType) || "B".equals(applyType)){
                    reqDto.setAppnal(creditCardAppInfo.getSerno());//外部编号
                }
                //主附同申,独立附卡
                if("A".equals(applyType) || "S".equals(applyType)){
                    reqDto.setAppnaf(creditCardAppInfo.getSerno());//附卡外部编号
                }
                //证件号码
                reqDto.setIdtfno(creditCardAppInfo.getCertCode());
                //证件类型
                String certType = creditCardAppInfo.getCertType();
                if ("A".equals(certType)) {
                    certType = "I";
                } else if ("B".equals(certType)) {
                    certType = "P";
                } else if ("C".equals(certType)) {
                    certType = "R";
                } else if ("D".equals(certType)) {
                    certType = "H";
                } else if ("E".equals(certType)) {
                    certType = "W";
                } else if ("X".equals(certType)) {
                    certType = "T";
                } else if ("G".equals(certType)) {
                    certType = "A";
                } else if ("H".equals(certType)) {
                    certType = "S";
                } else if ("S".equals(certType)) {
                    certType = "F";
                } else if ("W".equals(certType)) {
                    certType = "B";
                } else {
                    certType = "O";
                }
                reqDto.setIdtftp(certType);
                String isCustomizeCardNo = creditCardAppInfo.getIsCustomizeCardNo();
                if ("1".equals(isCustomizeCardNo)) {
                    isCustomizeCardNo = "Y";
                } else {
                    isCustomizeCardNo = "N";
                }
                reqDto.setSecard(isCustomizeCardNo);//是否自选卡号
                reqDto.setCardno(creditCardAppInfo.getCustomizeCardNo());//自选卡号
                //卡面代码
                reqDto.setCardfc("");
                //行内客户号
                reqDto.setBankid(creditCardAppInfo.getCusId());
                //卡片寄送地址标志
                reqDto.setCardtn(creditCardAppInfo.getCardPostAddr());
                reqDto.setQuestr(creditCardAppInfo.getCreditAuthDate());
                //卡产品代码映射
                String applyCardPrd = creditCardAppInfo.getApplyCardPrd();
                reqDto.setProdtp(applyCardPrd);
                //申请额度
                reqDto.setApplmt(String.valueOf(approveAmt));
                //受理网点\发卡网点
                reqDto.setBkbrch("019804");
                //客户类型
                reqDto.setCusttp(creditCardAppInfo.getCardCusType());
                //推广渠道
                reqDto.setSpretp(creditCardAppInfo.getRecomChnl());
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditCardAppInfo.getInputId());
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                reqDto.setCtmgna(adminSmUserDto.getUserName());//推广人姓名
                reqDto.setCtmgno(creditCardAppInfo.getInputId());//推广人编号
                reqDto.setCtmgtl("");//推广人手机号
                String appChnl = creditCardAppInfo.getAppChnl();
                reqDto.setSource("00");//申请渠道，纸质进件
                reqDto.setTaskip("");//影像批次号
                String startDate = creditCardAppInfo.getCertStartDate();
                if (startDate == null || "".equals(startDate)) {
                    startDate = "";
                }
                String endDate = creditCardAppInfo.getCertEndDate();
                if (endDate == null || "".equals(endDate)) {
                    endDate = "";
                }
                //是否新信贷
                reqDto.setZjgcre("Y");
                dailyRate= dailyRate.setScale(6,BigDecimal.ROUND_HALF_UP);
                //只有乐悠金卡和乐悠金电子卡才发送日费率，其它为空
                if(CardPrdCode.CARD_PRD_CODE_LYJK.equals(applyCardPrd) || CardPrdCode.CARD_PRD_CODE_LYJDZK.equals(applyCardPrd)){
                    reqDto.setDarate(dailyRate.toPlainString());
                }else {
                    reqDto.setDarate("");
                }
                reqDto.setIddate(startDate.replace("-", ""));//证件起始日
                reqDto.setIdtfdt(endDate.replace("-", ""));//证件到期日
                reqDto.setEnname(creditCardAppInfo.getPinyin());//凸印姓-姓名拼音
                String marStatus = creditCardAppInfo.getMarStatus();
                reqDto.setMtstat(marStatus);//婚姻状况
                reqDto.setForver("Y");//是否永久居住
                reqDto.setQlftin(creditCardAppInfo.getQualification());
                reqDto.setBrtday(creditCardAppInfo.getBirthday());//生日
                reqDto.setCorpnm(creditCardAppInfo.getCorpName());//公司名称
                //职务映射
                String duty = creditCardAppInfo.getDuty();
                reqDto.setEmplvr(duty);//职务
                String sex = creditCardAppInfo.getSex();
                if ("1".equals(sex)) {
                    sex = "M";
                } else {
                    sex = "F";
                }
                //名族
                String folk = creditCardAppInfo.getFolk();
                if("01".equals(folk)){
                    folk ="1";
                }else if("02".equals(folk)){
                    folk ="2";
                }else if("03".equals(folk)){
                    folk ="3";
                }else if("04".equals(folk)){
                    folk ="4";
                }else if("05".equals(folk)){
                    folk ="5";
                }else if("06".equals(folk)){
                    folk ="6";
                }else if("07".equals(folk)){
                    folk ="7";
                }else if("08".equals(folk)){
                    folk ="8";
                }else if("09".equals(folk)){
                    folk ="9";
                }else{
                    folk = creditCardAppInfo.getFolk();
                }
                reqDto.setNation(folk);
                reqDto.setGender(sex);//性别
                reqDto.setCustnm(creditCardAppInfo.getCusName());//姓名
                reqDto.setEmail(creditCardAppInfo.getEmail());//电子邮箱
                reqDto.setNtnaty(creditCardAppInfo.getNation());//国籍
                String occu = creditCardAppInfo.getOccu();
                reqDto.setOcpatn(occu);//职业
                reqDto.setHmphon(creditCardAppInfo.getFamilyPhone());//家庭电话
                reqDto.setHourst(creditCardAppInfo.getResiStatus());//住宅状况\住宅持有类型
                reqDto.setIncome(String.valueOf(creditCardAppInfo.getYearIncome().multiply(new BigDecimal(10000))));//年收入
                reqDto.setTeleno(creditCardAppInfo.getPhone());//移动电话

                AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
                adminSmTreeDicDto.setOptType("STD_ZB_AREA_CODE");
                if(creditCardAppInfo.getFamilyAddrProvince() !=null  && !"".equals(creditCardAppInfo.getFamilyAddrProvince())){
                    adminSmTreeDicDto.setEnName(creditCardAppInfo.getFamilyAddrProvince());
                    adminSmTreeDicDto = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDto);
                    reqDto.setHmprov(adminSmTreeDicDto.getCnName());//家庭所在省
                }
                if(creditCardAppInfo.getFamilyAddrCity() !=null  && !"".equals(creditCardAppInfo.getFamilyAddrCity())){
                    adminSmTreeDicDto.setEnName(creditCardAppInfo.getFamilyAddrCity());
                    adminSmTreeDicDto = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDto);
                    reqDto.setHmcity(adminSmTreeDicDto.getCnName());//家庭所在市
                }
                if(creditCardAppInfo.getFamilyAddrZone() !=null && !"".equals(creditCardAppInfo.getFamilyAddrZone())){
                    adminSmTreeDicDto.setEnName(creditCardAppInfo.getFamilyAddrZone());
                    adminSmTreeDicDto = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDto);
                    reqDto.setHmqury(adminSmTreeDicDto.getCnName());//家庭所在区县
                }
                reqDto.setHmaddr(creditCardAppInfo.getFamilyDetailAddr());//家庭地址
                reqDto.setHmpost(creditCardAppInfo.getFamilyAddrPost());//家庭住宅邮编
                reqDto.setTitlal("");//职称
                reqDto.setEmpstr(creditCardAppInfo.getCorpCategory());//公司性质
                reqDto.setEmptpy(creditCardAppInfo.getTrade());//公司行业类别
                AdminSmTreeDicDto adminSmTreeDicDtoUnit = new AdminSmTreeDicDto();
                adminSmTreeDicDtoUnit.setOptType("STD_ZB_AREA_CODE");
                if(creditCardAppInfo.getUnitAddrProvince() !=null && !"".equals(creditCardAppInfo.getUnitAddrProvince())){
                    adminSmTreeDicDtoUnit.setEnName(creditCardAppInfo.getUnitAddrProvince());
                    adminSmTreeDicDtoUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoUnit);
                    reqDto.setCoprov(adminSmTreeDicDtoUnit.getCnName());//公司所在省
                }
                if(creditCardAppInfo.getUnitAddrCity() !=null && !"".equals(creditCardAppInfo.getUnitAddrCity())){
                    adminSmTreeDicDtoUnit.setEnName(creditCardAppInfo.getUnitAddrCity());
                    adminSmTreeDicDtoUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoUnit);
                    reqDto.setCocity(adminSmTreeDicDtoUnit.getCnName());//公司所在市
                }
                if(creditCardAppInfo.getUnitAddrZone() !=null  && !"".equals(creditCardAppInfo.getUnitAddrZone())){
                    adminSmTreeDicDtoUnit.setEnName(creditCardAppInfo.getUnitAddrZone());
                    adminSmTreeDicDtoUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoUnit);
                    reqDto.setCoqury(adminSmTreeDicDtoUnit.getCnName());//公司所在区/县
                }
                reqDto.setCoaddr(creditCardAppInfo.getUnitDetailAddr());//公司地址
                reqDto.setCopost(creditCardAppInfo.getUnitAddrPost());//公司邮编
                reqDto.setCophon(creditCardAppInfo.getUnitPhone());//公司电话
                reqDto.setCmname(creditCardAppInfo.getRelaLinkmanName());//联系人中文姓名
                reqDto.setCmrela(creditCardAppInfo.getRelaCusCorre());//联系人与申请人关系
                reqDto.setCmmobi(creditCardAppInfo.getRelaLinkmanPhone());//联系人移动电话
                reqDto.setOtname(creditCardAppInfo.getOtherLinkmanName());//其他联系人中文姓名
                String otherLinkmanSex = creditCardAppInfo.getOtherLinkmanSex();
                if ("1".equals(otherLinkmanSex)) {
                    otherLinkmanSex = "M";
                } else {
                    otherLinkmanSex = "F";
                }
                reqDto.setQtsext(otherLinkmanSex);//其他联系人性别
                reqDto.setOtrela(creditCardAppInfo.getOtherLinkmanCorre());//其他联系人与申请人关系
                reqDto.setOtmobi(creditCardAppInfo.getOtherLinkmanPhone());//其他联系人移动电话
                reqDto.setAnswer(creditCardAppInfo.getReserveQuestion());//预留问题
                reqDto.setQuesti(creditCardAppInfo.getAnswer());//预留答案
                String isLongVld = creditCardAppInfo.getIsLongVld();
                if ("1".equals(isLongVld)) {
                    isLongVld = "Y";
                } else {
                    isLongVld = "N";
                }
                reqDto.setIslong(isLongVld);//证件是否永久有效
                reqDto.setFkname(creditCardAppInfo.getSubCardCusName());//附卡持卡人姓名
                //证件类型
                String subCertType = creditCardAppInfo.getSubCardCertType();
                if ("A".equals(subCertType)) {
                    subCertType = "I";
                } else if ("B".equals(subCertType)) {
                    subCertType = "P";
                } else if ("C".equals(subCertType)) {
                    subCertType = "R";
                } else if ("D".equals(subCertType)) {
                    subCertType = "H";
                } else if ("E".equals(subCertType)) {
                    subCertType = "W";
                } else if ("X".equals(subCertType)) {
                    subCertType = "T";
                } else if ("G".equals(subCertType)) {
                    subCertType = "A";
                } else if ("H".equals(subCertType)) {
                    subCertType = "S";
                } else if ("S".equals(subCertType)) {
                    subCertType = "F";
                } else if ("W".equals(subCertType)) {
                    subCertType = "B";
                } else {
                    subCertType = "O";
                }
                String isSubCustomizeCardNo = creditCardAppInfo.getIsSubCustomizeCardNo();
                if ("1".equals(isSubCustomizeCardNo)) {
                    isSubCustomizeCardNo = "Y";
                } else {
                    isSubCustomizeCardNo = "N";
                }
                reqDto.setFicard(isSubCustomizeCardNo);//附卡是否自选卡号
                reqDto.setFscano(creditCardAppInfo.getSubCustomizeCardNo());//附卡自选卡号
                reqDto.setFkidtp(subCertType);//附卡证件类型O
                reqDto.setFkidno(creditCardAppInfo.getSubCardCertCode());//附卡证件号码
                reqDto.setFktobs("O");//与主卡持卡人关系
                String subCerdCode = creditCardAppInfo.getSubCardCertCode();
                if (subCerdCode != null && !"".equals(subCerdCode)) {
                    reqDto.setFkbrdt(subCerdCode.substring(6, 14));//附卡生日
                } else {
                    reqDto.setFkbrdt("");//附卡生日
                }
                String subSex = creditCardAppInfo.getSubCardCusSex();
                if ("1".equals(subSex)) {
                    subSex = "M";
                } else {
                    subSex = "F";
                }
                reqDto.setFksext(subSex);//附卡性别
                reqDto.setFkennm(creditCardAppInfo.getSubCardCusPinyin());//附卡姓名拼音
                String subStartDate = creditCardAppInfo.getSubCardStartDate();
                if (subStartDate == null || "".equals(subStartDate)) {
                    subStartDate = "";
                }
                String subEndDate = creditCardAppInfo.getSubCardEndDate();
                if (subEndDate == null || "".equals(subEndDate)) {
                    subEndDate = "";
                }
                reqDto.setFkiddt(subEndDate.replace("-", ""));//附卡证件到期日
                reqDto.setFksddt(subStartDate.replace("-", ""));//附卡证件起始日
                reqDto.setFknaty(creditCardAppInfo.getSubCardCusNation());//附卡国籍
                reqDto.setFktele(creditCardAppInfo.getSubCardCusPhone());//附卡家庭电话
                reqDto.setFkmobi(creditCardAppInfo.getSubCardCusPhone());//附卡移动电话
                AdminSmTreeDicDto adminSmTreeDicDtoSub = new AdminSmTreeDicDto();
                adminSmTreeDicDtoSub.setOptType("STD_ZB_AREA_CODE");
                if(creditCardAppInfo.getSubCardFamilyAddrProvince() !=null){
                    adminSmTreeDicDtoSub.setEnName(creditCardAppInfo.getSubCardFamilyAddrProvince());
                    adminSmTreeDicDtoSub = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSub);
                    reqDto.setFkprov(adminSmTreeDicDtoSub.getCnName());//附卡家庭所在省
                }
                if(creditCardAppInfo.getSubCardFamilyAddrCity() !=null){
                    adminSmTreeDicDtoSub.setEnName(creditCardAppInfo.getSubCardFamilyAddrCity());
                    adminSmTreeDicDtoSub = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSub);
                    reqDto.setFkcity(adminSmTreeDicDtoSub.getCnName());//附卡家庭所在市
                }
                if(creditCardAppInfo.getSubCardFamilyAddrZone() !=null){
                    adminSmTreeDicDtoSub.setEnName(creditCardAppInfo.getSubCardFamilyAddrZone());
                    adminSmTreeDicDtoSub = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSub);
                    reqDto.setFkqury(adminSmTreeDicDtoSub.getCnName());//附卡家庭所在区县
                }
                reqDto.setFkaddr(creditCardAppInfo.getSubCardFamilyDetailAddr());//附卡家庭地址
                reqDto.setFoprov(creditCardAppInfo.getSubCardPostAddr());//附卡家庭住宅邮编

                AdminSmTreeDicDto adminSmTreeDicDtoSubUnit = new AdminSmTreeDicDto();
                adminSmTreeDicDtoSubUnit.setOptType("STD_ZB_AREA_CODE");
                if(creditCardAppInfo.getSubCardUnitAddrProvince() !=null){
                    adminSmTreeDicDtoSubUnit.setEnName(creditCardAppInfo.getSubCardUnitAddrProvince());
                    adminSmTreeDicDtoSubUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSubUnit);
                    reqDto.setFocity(adminSmTreeDicDtoSubUnit.getCnName());//附卡公司所在省
                }
                if(creditCardAppInfo.getSubCardUnitAddrCity() !=null){
                    adminSmTreeDicDtoSubUnit.setEnName(creditCardAppInfo.getSubCardUnitAddrCity());
                    adminSmTreeDicDtoSubUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSubUnit);
                    reqDto.setFoqury(adminSmTreeDicDtoSubUnit.getCnName());//附卡公司所在市
                }
                if(creditCardAppInfo.getSubCardUnitAddrZone() !=null){
                    adminSmTreeDicDtoSubUnit.setEnName(creditCardAppInfo.getSubCardUnitAddrZone());
                    adminSmTreeDicDtoSubUnit = adminSmTreeDicService.queryCfgTreeByCode(adminSmTreeDicDtoSubUnit);
                    reqDto.setFoaddr(adminSmTreeDicDtoSubUnit.getCnName());//附卡公司所在区/县
                }
                reqDto.setFopost(creditCardAppInfo.getUnitAddrPost());// 附卡公司邮编
                String isSubCardCertLongVld  = creditCardAppInfo.getIsSubCardCertLongVld();//1:是 0：否 通联Y :是 N:否
                if ("1".equals(isSubCardCertLongVld)) {
                    isSubCardCertLongVld = "Y";
                } else {
                    isSubCardCertLongVld = "N";
                }
                reqDto.setFolong(isSubCardCertLongVld);//附卡证件是否长期有效
                reqDto.setDdind(creditCardAppInfo.getAgreedRepayType());//约定还款类型
                reqDto.setIsjstj(creditCardAppInfo.getIsAgreeCardDown());//是否同意卡片自动降级
                reqDto.setCarsst(creditCardAppInfo.getCarStatus());//车辆状况
                reqDto.setCarval(new BigDecimal(0));//车辆价值
                String otherSex = creditCardAppInfo.getRelaLinkmanSex();
                if ("1".equals(otherSex)) {
                    otherSex = "M";
                } else {
                    otherSex = "F";
                }
                reqDto.setQssext(otherSex);//联系人性别R
                reqDto.setQsphon(creditCardAppInfo.getRelaLinkmanFixedPhone());// 联系人联系电话
                reqDto.setQtphon(creditCardAppInfo.getOtherLinkmanFixedPhone());//其他联系人固定电话
                reqDto.setFkcdno(creditCardAppInfo.getMainCardNo());//其申请人主卡卡号
                reqDto.setDbacna("A");
                //名族
                String subCardIndivFolk = creditCardAppInfo.getSubCardIndivFolk();
                if("01".equals(subCardIndivFolk)){
                    subCardIndivFolk ="1";
                }else if("02".equals(subCardIndivFolk)){
                    subCardIndivFolk ="2";
                }else if("03".equals(subCardIndivFolk)){
                    subCardIndivFolk ="3";
                }else if("04".equals(subCardIndivFolk)){
                    subCardIndivFolk ="4";
                }else if("05".equals(subCardIndivFolk)){
                    subCardIndivFolk ="5";
                }else if("06".equals(subCardIndivFolk)){
                    subCardIndivFolk ="6";
                }else if("07".equals(subCardIndivFolk)){
                    subCardIndivFolk ="7";
                }else if("08".equals(subCardIndivFolk)){
                    subCardIndivFolk ="8";
                }else if("09".equals(subCardIndivFolk)){
                    subCardIndivFolk ="9";
                }
                reqDto.setFntion(subCardIndivFolk);
                reqDto.setDbkact(creditCardAppInfo.getAgreedRepayCardNo());//约定还款扣款账号
                reqDto.setTaskid("");//影像批次号
                reqDto.setFkcatn(creditCardAppInfo.getSubCardPostAddr());//附卡卡片寄送地址标志
                reqDto.setSubmit("");//提交操作
                reqDto.setIdress(creditCardAppInfo.getRegiOrg());//发证机关所在地址dbacna
                reqDto.setFachno("5");//附卡编号
                reqDto.setFxdage("");//附卡年龄
                reqDto.setMaritu("");//附卡婚姻状况
                reqDto.setFcntry("Y");//附卡是否永久居住
                reqDto.setFccode("156");//附卡永久居住地国家代码默认中国
                reqDto.setFqtion("");//附卡教育状况
                reqDto.setFegree("");//附卡学位
                reqDto.setFdress(creditCardAppInfo.getSubCardRegiOrg());//附卡发证机关所在地址
                reqDto.setFicome("");//附卡年收入
                reqDto.setFmlogo("");//附卡母亲姓氏
                reqDto.setFemail("");//附卡电子邮箱
                reqDto.setFhouse("");//附卡住宅状况
                reqDto.setFshome("");//附卡现住址居住起始年月
                reqDto.setFccity("156");//附卡家庭国家代码
                reqDto.setFhpedn("");//附卡家庭电话区号
                reqDto.setFvenue("");//附卡家庭年收入
                reqDto.setFbflag("");//附卡是否行内员工
                reqDto.setFbanno("");//附卡本行员工号
                reqDto.setFcname("");//附卡公司名称
                reqDto.setFactry("156");//附卡公司国家代码
                reqDto.setFcture("");//附卡公司性质
                reqDto.setFetype("");//附卡公司行业类别
                reqDto.setFphone("");//附卡公司电话
                reqDto.setFcofax("");//附卡公司传真
                reqDto.setFefrom("");//附卡现单位工作起始年月
                reqDto.setFpment("");//附卡任职部门
                String subOccu = creditCardAppInfo.getSubCardOccu();
                reqDto.setFation(subOccu);//附卡职业
                reqDto.setFnical("");//附卡职称
                reqDto.setFemppo(creditCardAppInfo.getDuty());//附卡职务
                reqDto.setFpjuse("");//附卡是否彩照卡
                reqDto.setFverif("");//附卡是否消费凭密
                reqDto.setFstatu("");//附卡是否在职
                reqDto.setFstaby("");//附卡工作稳定性
                reqDto.setFotask("");//附卡预留问题
                reqDto.setFanswe("");//附卡预留答案
                reqDto.setFappno(serno);//附卡申请编号_外部送入
                reqDto.setFcusno("");//附卡行内客户号
                reqDto.setFsmamt("");//附卡小额免密
                reqDto.setFcatch("");//附卡领卡方式
                reqDto.setFfanch("");//附卡领卡网点
                reqDto.setFicity("");//发卡城市
                reqDto.setFyears("");//居住年限
                //调用信用卡开卡接口
                log.info("信用卡开卡接口开始"+serno);
                log.info("发送报文：" + reqDto);
                ResultDto<D11005RespDto> d11005RespDto = dscms2TonglianClientService.d11005(reqDto);
                log.info("返回报文：" + d11005RespDto);
                if ("0".equals(d11005RespDto.getCode())) {
                    String appNo = d11005RespDto.getData().getAppno();
                    creditCardAppInfo.setTlAppNo(appNo);
                    this.updateSelective(creditCardAppInfo);
                    log.info("信用卡开卡成功！,返回申请件编号{}", appNo);
                } else {
                    log.info("信用卡开卡失败:" + d11005RespDto.getMessage());
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, d11005RespDto.getMessage());
                }
                //是否同时申请普卡
                if ("1".equals(isapplycommon)) {
                    //普卡申请额度
                    reqDto.setApplmt(String.valueOf(commonCardApproveAmt));
                    reqDto.setProdtp(creditCardAppInfo.getApplyCommonCardPrd());
                    reqDto.setApptyp("B");
                    reqDto.setDarate("");
                    log.info("普卡发送报文：" + reqDto);
                    ResultDto<D11005RespDto> d11005CommonRespDto = dscms2TonglianClientService.d11005(reqDto);
                    if ("0".equals(d11005CommonRespDto.getCode())) {
                        String appNo = d11005CommonRespDto.getData().getAppno();
                        log.info("信用卡普卡开卡成功！,返回普卡申请件编号{}", appNo);
                    } else {
                        log.info("信用卡普卡开卡失败:" + d11005CommonRespDto.getMessage());
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, d11005CommonRespDto.getMessage());
                    }
                }
                //更改审批状态为通过
                creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                this.updateSelective(creditCardAppInfo);
            } else {
                //快速拒绝时不开卡，更改状态为待复议
                creditCardAppInfo.setApproveStatus("998");
                creditCardAppInfo.setIsReconsid("1");
                int result = this.updateSelective(creditCardAppInfo);
                if (result < 0) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
                }
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【997】-已通过");
            /**修改审批状态为已通过,并修改台账表的数据 维护最后修改人信息**/
            //TODO 保留接口  更新核算系统数据
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/26 16:42
     * @version 1.0.0
     * @desc 信用卡征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int getCreditReportInfo(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("查询信用卡信息" + serno + "申请主表信息");
            CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
            if (creditCardAppInfo == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            //征信关联表
            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
            //征信详情表
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            //生成征信流水号
            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            //生成关联征信数据
            creditQryBizReal.setBizSerno(serno);
            creditQryBizReal.setCusId(creditCardAppInfo.getCusId());
            creditQryBizReal.setCusName(creditCardAppInfo.getCusName());
            creditQryBizReal.setCertCode(creditCardAppInfo.getCertCode());
            creditQryBizReal.setCrqlSerno(crqlSerno);
            creditQryBizReal.setCertType("A");
            creditQryBizReal.setBorrowRel("001");
            creditQryBizReal.setScene("01");
            int result = creditQryBizRealService.insertSelective(creditQryBizReal);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信关联数据异常！");
            }
            //生成征信详情数据
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setCusId(creditCardAppInfo.getCusId());
            creditReportQryLst.setCusName(creditCardAppInfo.getCusName());
            creditReportQryLst.setCertCode(creditCardAppInfo.getCertCode());
            creditReportQryLst.setCertType("A");
            creditReportQryLst.setBorrowRel("001");
            creditReportQryLst.setQryCls("0");
            creditReportQryLst.setApproveStatus("997");
            creditReportQryLst.setQryFlag("04");
            creditReportQryLst.setQryResn("03");
            creditReportQryLst.setAuthbookContent("002;007");
            result = creditReportQryLstService.insertSelective(creditReportQryLst);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信详情数据异常！");
            }

            int updateCount = 0;
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditCardAppInfo.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            CredxxReqDto credxxReqDto = new CredxxReqDto();
            credxxReqDto.setCreditType("0");//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(creditCardAppInfo.getCusName());//客户名称
            credxxReqDto.setCertificateType(toConverCurrency(creditCardAppInfo.getCertType()));//证件类型
            credxxReqDto.setCertificateNum(creditCardAppInfo.getCertCode());//证件号
            credxxReqDto.setQueryReason("03");//查询原因  03：信用卡审批
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserId(creditCardAppInfo.getManagerId());//客户经理工号
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("30");//信用报告复用策略
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            //总行：朱真尧 东海：邵雯
            if (creditCardAppInfo.getInputBrId().startsWith("81")) {
                credxxReqDto.setApprovalName("邵雯");//审批人姓名
                credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
            } else {
                credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
            }
            // credxxReqDto.setApprovalName(adminSmUserDto.getUserName());//审批人姓名
            //取系统日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            String creditAuthDate = creditCardAppInfo.getCreditAuthDate();
            //授权结束日期=授权日期+30天
            String expireDate = DateUtils.addDay(creditAuthDate, "yyyy-MM-dd", 30);
            credxxReqDto.setArchiveCreateDate(creditAuthDate);//授权书签订日期
            credxxReqDto.setCreditDocId(crqlSerno);//授权影像编
            credxxReqDto.setArchiveExpireDate(expireDate);//授权结束日期
            credxxReqDto.setBorrowPersonRelationName(creditCardAppInfo.getCusName());//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber(creditCardAppInfo.getCertCode());//主借款人证件号
            credxxReqDto.setAuditReason("002;004");//授权书内容
            credxxReqDto.setBrchno(creditCardAppInfo.getInputBrId());
            // credxxReqDto.setApprovalIdCard(adminSmUserDto.getCertNo());
            credxxReqDto.setBorrowPersonRelation("001");
            log.info("发送征信接口" + credxxReqDto);
            CredxxRespDto credxxRespDto = credxxService.credxx(credxxReqDto);
            log.info("返回征信接口:" + credxxRespDto);
            String zxSerno = credxxRespDto.getReportId();
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setReportNo(zxSerno);
            if (!StringUtils.isEmpty(zxSerno)) {
                creditReportQryLst.setImageNo(crqlSerno);
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setQryStatus("003");
                CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
                callCiis2ndReqDto.setPrefixUrl(creditUrl);
                callCiis2ndReqDto.setReqId(credxxRespDto.getReqId());
                callCiis2ndReqDto.setOrgName("信贷");
                callCiis2ndReqDto.setUserCode(creditCardAppInfo.getManagerId());
                callCiis2ndReqDto.setUserName(adminSmUserDto.getUserName());
                callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
                String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
                creditReportQryLst.setCreditUrl(url);
                creditReportQryLst.setCertType(creditCardAppInfo.getCertType());
                creditReportQryLst.setCertCode(creditCardAppInfo.getCertCode());
                creditReportQryLst.setCusId(creditCardAppInfo.getCusId());
                creditReportQryLst.setCusName(creditCardAppInfo.getCusName());
                creditReportQryLst.setBorrowerCusId(creditCardAppInfo.getCusId());
                creditReportQryLst.setBorrowerCusName(creditCardAppInfo.getCusName());
                creditReportQryLst.setBorrowerCertCode(creditCardAppInfo.getCertCode());
                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                creditReportQryLst.setAuthbookNo(authbookNoSeq);
                creditReportQryLst.setAuthbookDate(creditAuthDate);
                creditReportQryLst.setAuthbookContent("001");
                creditReportQryLst.setManagerId(creditCardAppInfo.getInputId());
                creditReportQryLst.setManagerBrId(creditCardAppInfo.getInputBrId());
                //生产征信报告
                result = creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                creditAuthbookInfo.setAuthMode("02");
                creditAuthbookInfo.setAuthbookContent("002;007");
                result = creditAuthbookInfoService.insert(creditAuthbookInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                return result;
            } else {
                throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "该客户未查询到征信信息！");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, e.getMessage());
        }
    }

    /**
     * @param serno
     * @return
     * @author wzy
     * @date 2021/5/25 9:57
     * @version 1.0.0
     * @desc 信用卡申请零售内评处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map handleBusinessDataAfterRun(String serno) {
        int result = 0;
        Map param = new HashMap();
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            //如果已有内评信息，先删除，再新增
            iqpCusLsnpInfoMapper.deleteByIqpSerno(serno);
            log.info("信用卡申请" + serno + "零售内评开始");
            CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
            //调用零售内评接口进行零售评级
            Lsnp02ReqDto reqDto = new Lsnp02ReqDto();
            //流水号
            reqDto.setApply_seq(serno);
            CreditCardAttachmentInfo  creditCardAttachmentInfo = creditCardAttachmentInfoService.selectByPrimaryKey(serno);
            if(creditCardAttachmentInfo != null){
                BigDecimal pundDepositBase = creditCardAttachmentInfo.getPundDepositBase();
                if(pundDepositBase == null){
                    reqDto.setFund_deposite_base("");
                }else{
                    reqDto.setFund_deposite_base(pundDepositBase.toPlainString());
                }
            }else{
                reqDto.setFund_deposite_base("");
            }
            reqDto.setEmail_flag("1");
            //性别
            String sex = creditCardAppInfo.getSex();
            if("1".equals(sex)){
                sex = "M";
            }else{
                sex = "F";
            }
            reqDto.setSex(sex);
            //申请卡产品
            reqDto.setProduct_cd(creditCardAppInfo.getApplyCardPrd());
            //客户类型
            reqDto.setCust_type(creditCardAppInfo.getCardCusType());
            //建议额度
            reqDto.setApplye_amt(creditCardAppInfo.getSuggestLmt());
            //客户名称
            reqDto.setCust_name(creditCardAppInfo.getCusName());
            //证件号码
            reqDto.setCert_no(creditCardAppInfo.getCertCode());
            reqDto.setBusiness_seq(serno);
            //申请类型
            if ("A".equals(creditCardAppInfo.getApplyType()) || "B".equals(creditCardAppInfo.getApplyType())) {
                //教育程度
                String edu = creditCardAppInfo.getQualification();
                if("A".equals(edu) || "B".equals(edu)){
                    edu ="1";
                }else if("C".equals(edu)){
                    edu ="2";
                }else if("D".equals(edu)){
                    edu ="3";
                }else if("F".equals(edu)){
                    edu ="4";
                }else{
                    edu ="7";
                }
                reqDto.setMax_edu(edu);
                //根据出生日期计算年龄
                int age = 0;
                String birthday = creditCardAppInfo.getBirthday();
                if (birthday != null && !"".equals(birthday)) {
                    int birthYear = Integer.valueOf(birthday.substring(0, 4));
                    //取系统日期
                    String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                    int year = Integer.valueOf(openDay.substring(0, 4));
                    age = year - birthYear;
                }
                reqDto.setApply_age(new BigDecimal(age));
                Integer workYear = creditCardAppInfo.getWorkYears();
                if (workYear == null) {
                    workYear = 0;
                }
                //工作年限
                BigDecimal workYears = new BigDecimal(workYear);
                reqDto.setJob_year(workYears);
                //公司单位性质
                String CorpCategory = creditCardAppInfo.getCorpCategory();
                /*1  机关事业国有企业
                2  独资企业
                3  合资合作企业
                4  股份制企业
                5  私营企业
                6  其他F
                7  未知*/
                if("B".equals(CorpCategory) || "F".equals(CorpCategory)|| "D".equals(CorpCategory)|| "A".equals(CorpCategory)|| "C".equals(CorpCategory) ){
                    CorpCategory ="1";
                }else if("P".equals(CorpCategory) || "M".equals(CorpCategory)|| "Q".equals(CorpCategory)) {
                    CorpCategory ="2";
                }else if("N".equals(CorpCategory) || "O".equals(CorpCategory)|| "E".equals(CorpCategory)|| "I".equals(CorpCategory)) {
                    CorpCategory ="3";
                }else if("J".equals(CorpCategory) || "K".equals(CorpCategory)) {
                    CorpCategory ="4";
                }else if("L".equals(CorpCategory) ) {
                    CorpCategory ="5";
                }else{
                    CorpCategory ="6";
                }
                reqDto.setCorp_prop(CorpCategory);
                //行业
                reqDto.setCorp_indus_type(creditCardAppInfo.getTrade());
                //婚姻状态
                String marStatus = creditCardAppInfo.getMarStatus();
                if(marStatus !=null && !"".equals(marStatus)){
                    if(!"M".equals(marStatus) && !"S".equals(marStatus)){
                        marStatus ="O";
                    }
                }
                reqDto.setMarry_stat(marStatus);
                reqDto.setMax_degree(creditCardAppInfo.getQualification());
                //计算月收入
                BigDecimal yearIncome = creditCardAppInfo.getYearIncome();
                BigDecimal monIncome = yearIncome.divide(new BigDecimal(12), 2);
                reqDto.setMon_income(monIncome.multiply(new BigDecimal(10000)));
                //联系电话
                reqDto.setTel(creditCardAppInfo.getPhone());
                //其它联系电话
                reqDto.setFirst_rel_tel(creditCardAppInfo.getRelaLinkmanPhone());
                reqDto.setSecond_rel_tel(creditCardAppInfo.getRelaLinkmanPhone());
                //根据证件号计算年龄
                reqDto.setAttach_card_age(new BigDecimal(age));
                //单位全称
                reqDto.setCorp_name(creditCardAppInfo.getCorpName());
                //发送接口进行零售评级
                log.info("请求发送参数：" + reqDto);
                ResultDto<Lsnp02RespDto> lsnp02RespDto = dscms2LsnpClientService.lsnp02(reqDto);
                log.info("请求返回参数：" + lsnp02RespDto);
                if ("0".equals(lsnp02RespDto.getCode())) {
                    BigDecimal inpretVal = lsnp02RespDto.getData().getInpret_val();
                    String inpretValRisk = lsnp02RespDto.getData().getInpret_val_risk_lvl();
                    BigDecimal applyCore = lsnp02RespDto.getData().getApply_score();
                    String applyCoreRisk = lsnp02RespDto.getData().getApply_score_risk_lvl();
                    //核准额度
                    BigDecimal lmtAdvice = lsnp02RespDto.getData().getLmt_advice();
                    //建议额度
                    BigDecimal priceAdvice = lsnp02RespDto.getData().getPrice_advice();
                    String ruleRislLvl = lsnp02RespDto.getData().getRule_risk_lvl();
                    String fundDeposit = lsnp02RespDto.getData().getFund_deposit_base();
                    if (fundDeposit == null || "".equals(fundDeposit)) {
                        fundDeposit = "0";
                    }
                    String aum = lsnp02RespDto.getData().getAum();
                    //日费率
                    String dayRate = lsnp02RespDto.getData().getDay_rate();
                    if (dayRate == null || "".equals(dayRate)) {
                        dayRate = "0";
                    } else {
                        dayRate = dayRate.replace("%", "");
                    }

                    String limitFlag = lsnp02RespDto.getData().getLimit_flag();
                    String complexRisk = lsnp02RespDto.getData().getComplex_risk_lvl();
                    //1，快速通过或建议通过并且limitFlag==1时，流程结束
                    if (("A".equals(complexRisk) || "RA".equals(complexRisk)) && "1".equals(limitFlag) && !"0".equals(lmtAdvice)) {
                        param.put("firstflag", "1");
                    } else if ("D".equals(complexRisk)) {
                        //2，快速拒绝，转至零售客户经理发起复议
                        param.put("firstflag", "2");
                    } else{
                        //3，人工审核或审慎审核时且limitFlag！=1时，跳转到信贷管理部零售业务审批人
                        param.put("firstflag", "3");
                    }
                    String custLvl = lsnp02RespDto.getData().getCust_lvl();
                    IqpCusLsnpInfo Lsnp = new IqpCusLsnpInfo();
                    Lsnp.setIqpSerno(serno);
                    Lsnp.setDigIntVal(String.valueOf(inpretVal));//数字解读值
                    Lsnp.setDigIntValRiskLvl(inpretValRisk);//数字解读值风险等级
                    Lsnp.setAppScore(String.valueOf(applyCore));//申请评分风险等级
                    Lsnp.setAppScoreRiskLvl(applyCoreRisk);//申请评分风险等级
                    Lsnp.setLmtAdvice(lmtAdvice);
                    Lsnp.setPriceAdvice(priceAdvice);
                    Lsnp.setCusLvl(custLvl);
                    Lsnp.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
                    Lsnp.setPundDepositBase(new BigDecimal(fundDeposit));
                    Lsnp.setAum(aum);
                    Lsnp.setDailyFeeRate(new BigDecimal(dayRate).divide(new BigDecimal(100)));
                    Lsnp.setRuleRiskLvl(ruleRislLvl);
                    Lsnp.setInteRiskLvl(complexRisk);

                    //更新终审一审数据
                    CreditCardFinalJudgInifo creditCardFinalJudgInifo2 = new CreditCardFinalJudgInifo();
                    creditCardFinalJudgInifo2.setSerno(serno);
                    creditCardFinalJudgInifo2.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
                    creditCardFinalJudgInifo2.setFinalPostFlag("00");
                    CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(creditCardFinalJudgInifo2);
                    if (creditCardFinalJudgInifo == null) {
                        creditCardFinalJudgInifo = new CreditCardFinalJudgInifo();
                        creditCardFinalJudgInifo.setFinalPostFlag("00");
                        creditCardFinalJudgInifo.setIsApplyCommonCard(creditCardAppInfo.getIsApplyCommonCard());
                        creditCardFinalJudgInifo.setSerno(serno);
                        creditCardFinalJudgInifo.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
                        creditCardFinalJudgInifo.setCommonCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                        //核准额度
                        creditCardFinalJudgInifo.setApproveAmt(lmtAdvice);
                        // 日费率
                        creditCardFinalJudgInifo.setDailyFeeRate(new BigDecimal(dayRate).divide(new BigDecimal(100)));
                        result = creditCardFinalJudgInifoservice.insertSelective(creditCardFinalJudgInifo);
                        creditCardFinalJudgInifo.setPkId(UUID.randomUUID().toString());
                        creditCardFinalJudgInifo.setFinalPostFlag("01");
                        result = creditCardFinalJudgInifoservice.insertSelective(creditCardFinalJudgInifo);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "终审信息修改异常！");
                        }
                    } else {
                        //核准额度
                        creditCardFinalJudgInifo.setApproveAmt(lmtAdvice);
                        creditCardFinalJudgInifo.setApplyCardPrd(creditCardAppInfo.getApplyCardPrd());
                        creditCardFinalJudgInifo.setCommonCardPrd(creditCardAppInfo.getApplyCardPrd());
                        // 日费率
                        creditCardFinalJudgInifo.setDailyFeeRate(new BigDecimal(dayRate).divide(new BigDecimal(100)));
                        creditCardFinalJudgInifo.setFinalPostFlag("00");
                        result = creditCardFinalJudgInifoservice.updateSelective(creditCardFinalJudgInifo);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "终审信息修改异常！");
                        }
                    }
                    result = iqpCusLsnpInfoService.insertSelective(Lsnp);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "零售内评信息新增异常！");
                    }
                } else {
                    log.info("信用卡零售评级异常:" + lsnp02RespDto.getMessage());
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, lsnp02RespDto.getMessage());
                }
                //是否同时申请普通信用卡,如果有，讲普通卡发送至零售内评进行评级
                String flag = creditCardAppInfo.getIsApplyCommonCard();
                if ("1".equals(flag)) {
                    reqDto.setProduct_cd(creditCardAppInfo.getApplyCommonCardPrd());
                    //发送接口进行零售评级
                    log.info("请求发送参数：" + reqDto);
                    ResultDto<Lsnp02RespDto> commonlsnp02RespDto = dscms2LsnpClientService.lsnp02(reqDto);
                    log.info("请求返回参数：" + commonlsnp02RespDto);
                    if ("0".equals(lsnp02RespDto.getCode())) {
                        BigDecimal inpretVal = commonlsnp02RespDto.getData().getInpret_val();
                        String inpretValRisk = commonlsnp02RespDto.getData().getInpret_val_risk_lvl();
                        BigDecimal applyCore = commonlsnp02RespDto.getData().getApply_score();
                        String applyCoreRisk = commonlsnp02RespDto.getData().getApply_score_risk_lvl();
                        BigDecimal lmtAdvice = commonlsnp02RespDto.getData().getLmt_advice();
                        BigDecimal priceAdvice = commonlsnp02RespDto.getData().getPrice_advice();
                        String ruleRislLvl = commonlsnp02RespDto.getData().getRule_risk_lvl();
                        String fundDeposit = commonlsnp02RespDto.getData().getFund_deposit_base();
                        if (fundDeposit == null || "".equals(fundDeposit)) {
                            fundDeposit = "0";
                        }
                        String aum = commonlsnp02RespDto.getData().getAum();
                        String dayRate = commonlsnp02RespDto.getData().getDay_rate();
                        if (dayRate == null || "".equals(dayRate)) {
                            dayRate = "0";
                        } else {
                            dayRate = dayRate.replace("%", "");
                        }
                        String limitFlag = commonlsnp02RespDto.getData().getLimit_flag();
                        String complexRisk = commonlsnp02RespDto.getData().getComplex_risk_lvl();
                        String custLvl = commonlsnp02RespDto.getData().getCust_lvl();
                        IqpCusLsnpInfo lsnpInfo = new IqpCusLsnpInfo();
                        lsnpInfo.setIqpSerno(serno);
                        lsnpInfo.setDigIntVal(String.valueOf(inpretVal));
                        lsnpInfo.setDigIntValRiskLvl(inpretValRisk);
                        lsnpInfo.setAppScore(String.valueOf(applyCore));
                        lsnpInfo.setAppScoreRiskLvl(applyCoreRisk);
                        lsnpInfo.setLmtAdvice(lmtAdvice);
                        lsnpInfo.setPriceAdvice(priceAdvice);
                        lsnpInfo.setCusLvl(custLvl);
                        lsnpInfo.setApplyCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                        lsnpInfo.setPundDepositBase(new BigDecimal(fundDeposit));
                        lsnpInfo.setAum(aum);
                        lsnpInfo.setDailyFeeRate(new BigDecimal(dayRate).divide(new BigDecimal(100)));
                        lsnpInfo.setRuleRiskLvl(ruleRislLvl);
                        lsnpInfo.setInteRiskLvl(complexRisk);
                        //更新终审一审数据
                        CreditCardFinalJudgInifo creditCardFinalJudgInifo2 = new CreditCardFinalJudgInifo();
                        creditCardFinalJudgInifo2.setSerno(serno);
                        creditCardFinalJudgInifo2.setFinalPostFlag("00");
                        CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoservice.selectBySerno(creditCardFinalJudgInifo2);
                        //核准额度
                        creditCardFinalJudgInifo.setCommonCardApproveAmt(lmtAdvice);
                        creditCardFinalJudgInifo.setCommonCardPrd(creditCardAppInfo.getApplyCommonCardPrd());
                        result = creditCardFinalJudgInifoservice.updateSelective(creditCardFinalJudgInifo);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "普卡终审信息修改异常！");
                        }
                        result = iqpCusLsnpInfoService.insertSelective(lsnpInfo);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "普通卡零售内评信息新增异常！");
                        }
                    } else {
                        log.info("普通信用卡零售评级异常:" + lsnp02RespDto.getMessage());
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, lsnp02RespDto.getMessage());
                    }
                }
                log.info("信用卡申请" + serno + "零售内评结束");
            } else {
                //独立副卡无需零售内评
                param.put("firstflag", "1");
            }
        } catch (YuspException e) {
            e.printStackTrace();
            log.info("信用卡零售评级异常:" + e.getMessage());
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("信用卡零售评级异常:" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return param;
    }

    /**
     * @param creditCardAppInfo
     * @return Map
     * @author wzy
     * @date 2021/5/25 9:57
     * @version 1.0.0
     * @desc 校验卡号的有效性及状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map confirmCard(CreditCardAppInfo creditCardAppInfo) {
        Map param = new HashMap();
        try {
            //TODO 校验卡号的有效性及状态
            D14020ReqDto reqDto = new D14020ReqDto();
            reqDto.setCardno(creditCardAppInfo.getMainCardNo());
            log.info("卡片信息查询发送报文：" + reqDto);
            ResultDto<D14020RespDto> repDto = dscms2TonglianClientService.d14020(reqDto);
            log.info("卡片信息查询返回报文：" + repDto);
            if ("0".equals(repDto.getCode())) {
                String actvin = repDto.getData().getActvin();
                String bkcode = repDto.getData().getBkcode();
//                String cdhdnm =  repDto.getData().getCdhdnm();
//                if(!cdhdnm.equals(creditCardAppInfo.getCusName())){
//                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key,"卡号:"+creditCardAppInfo.getMainCardNo() +"所属客户:"+cdhdnm+"与申请人不一致！");
//                }
                if (!"Y".equals(actvin)) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "卡号无效！");
                }
                //锁定码值判断账户状态
                if (!"".equals(bkcode) && bkcode != null) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该账户状态异常！");
                }
                String cdepdt =  repDto.getData().getCdepdt();//卡片有效期
                //取系统日期
                String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                SimpleDateFormat  formate = new SimpleDateFormat("yyyy-MM-dd");
                Date nowDay = formate.parse(openDay);

                Date cardDay = formate.parse(cdepdt);
                if(nowDay.compareTo(cardDay)<0){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "卡号已过有效期！");
                }
            } else {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, repDto.getMessage());
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("校验卡号的有效性及状态校验异常！", e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return param;
    }

    /**
     * @param creditCardAppInfo
     * @return Map
     * @author wzy
     * @date 2021/5/25 9:57
     * @version 1.0.0
     * @desc 校验卡号是否已存在
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map confirmCardAlive(CreditCardAppInfo creditCardAppInfo) {
        Map param = new HashMap();
        try {
            //TODO 校验卡号的有效性及状态
            D14020ReqDto reqDto = new D14020ReqDto();
            reqDto.setCardno(creditCardAppInfo.getMainCardNo());
            log.info("卡片信息查询发送报文：" + reqDto);
            ResultDto<D14020RespDto> repDto = dscms2TonglianClientService.d14020(reqDto);
            log.info("卡片信息查询返回报文：" + repDto);
            if ("0".equals(repDto.getCode())) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "卡号已存在！");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("校验卡号的有效性及状态校验异常！", e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return param;
    }
    /**
     * @param agreedRepayCardNo
     * @return Map
     * @author wzy
     * @date 2021/5/25 9:57
     * @version 1.0.0
     * @desc 约定还款扣款账号：客户名下我行借记卡一类户，调核心系统接口校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map confirmRepayCard(String agreedRepayCardNo) {
        Map param = new HashMap();
        try {
            //TODO 约定还款扣款账号：客户名下我行借记卡一类户，调核心系统接口校验
            param.put("status", "正常");
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("约定还款扣款账号：客户名下我行借记卡一类户，调核心系统接口校验异常！", e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "约定还款扣款账号：客户名下我行借记卡一类户，调核心系统接口校验异常！");
        }
        return param;
    }

    /**
     * @param serno
     * @return Map
     * @author wzy
     * @date 2021/6/5 15:13
     * @version 1.0.0
     * @desc 信用卡发起复议
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map reconsider(String serno) {
        Map param = new HashMap();
        int result = 0;
        try {
            log.info("复制业务申请信息" + serno);
            //查询原申请信息
            CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
            //新业务流水号
            String sernoNew = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            creditCardAppInfo.setSerno(sernoNew);
            creditCardAppInfo.setOldSerno(serno);
            creditCardAppInfo.setApproveStatus("000");
            creditCardAppInfo.setIsReconsid("1");
            //更改状态为待复议
            result = this.insertSelective(creditCardAppInfo);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制业务申请信息异常！");
            }
            log.info("复制附件信息" + sernoNew);
            CreditCardAttachmentInfo creditCardAttachmentInfo = creditCardAttachmentInfoService.selectByPrimaryKey(serno);
            creditCardAttachmentInfo.setSerno(sernoNew);
            result = creditCardAttachmentInfoService.insertSelective(creditCardAttachmentInfo);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-附件信息异常！");
            }
            log.info("复制信用卡申请-零售内评信息" + sernoNew);
            List<IqpCusLsnpInfo> iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSerno(serno);
            for (int i = 0; i < iqpCusLsnpInfo.size(); i++) {
                IqpCusLsnpInfo lsnp = iqpCusLsnpInfo.get(i);
                lsnp.setIqpSerno(sernoNew);
                lsnp.setPkId(UUID.randomUUID().toString());
                result = iqpCusLsnpInfoService.insertSelective(lsnp);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-零售内评信息异常！");
                }
            }
            log.info("复制信用卡申请-电核对象信息" + sernoNew);
            List<CreditCardTelvPerDetail> creditCardTelvPerDetail = creditCardTelvPerDetailService.selectBySerno(serno);
            for (int i = 0; i < creditCardTelvPerDetail.size(); i++) {
                CreditCardTelvPerDetail telPer = creditCardTelvPerDetail.get(i);
                telPer.setSerno(sernoNew);
                telPer.setPkId(UUID.randomUUID().toString());
                result = creditCardTelvPerDetailService.insertSelective(telPer);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-电核对象信息异常！");
                }
            }
            log.info("复制信用卡申请-电核问题明细信息" + sernoNew);
            List<CreditCardTelvQuestionDetail> creditCardTelvQuestionDetail = creditCardTelvQuestionDetailService.selectBySerno(serno);
            for (int i = 0; i < creditCardTelvQuestionDetail.size(); i++) {
                CreditCardTelvQuestionDetail telPerQuestionDetail = creditCardTelvQuestionDetail.get(i);
                telPerQuestionDetail.setSerno(sernoNew);
                telPerQuestionDetail.setPkId(UUID.randomUUID().toString());
                result = creditCardTelvQuestionDetailService.insertSelective(telPerQuestionDetail);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-电核问题明细信息异常！");
                }
            }
            log.info("复制信用卡申请-电核其他信息" + sernoNew);
            CreditCardTelvOtherInfo creditCardTelvOtherInfo = creditCardTelvOtherInfoService.selectByPrimaryKey(serno);
            if(creditCardTelvOtherInfo != null ){
                creditCardTelvOtherInfo.setSerno(sernoNew);
                creditCardTelvOtherInfoService.insertSelective(creditCardTelvOtherInfo);
                CreditCardFirstJudgInifo creditCardFirstJudgInifo = creditCardFirstJudgInifoService.selectBySerno(serno);
                if(creditCardFirstJudgInifo != null){
                    creditCardFirstJudgInifo.setSerno(sernoNew);
                    creditCardFirstJudgInifo.setPkId(UUID.randomUUID().toString());
                    result = creditCardFirstJudgInifoService.insertSelective(creditCardFirstJudgInifo);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-电核其他信息异常！");
                    }
                }
            }
            log.info("复制信息结束");
            log.info("复制审批信息");
            List<CreditCardFinalJudgInifo> creditCardFinalJudgInifoList = creditCardFinalJudgInifoservice.selectByIqpSerno(serno);
            for (int i = 0; i < creditCardFinalJudgInifoList.size(); i++) {
                CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoList.get(i);
                creditCardFinalJudgInifo.setPkId(UUID.randomUUID().toString());
                creditCardFinalJudgInifo.setSerno(sernoNew);
                result = creditCardFinalJudgInifoservice.insertSelective(creditCardFinalJudgInifo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "复制信用卡申请-审批信息异常！");
                }
            }
            //物理删除原业务信息
            CreditCardAppInfo oldCard = this.selectByPrimaryKey(serno);
            oldCard.setOprType("02");
            result = this.updateSelective(oldCard);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "物理删除异常！");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("信用卡复议异常：", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
        return param;
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/5 18:03
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map queryLsnp(String serno) {
        Map param = new HashMap();
        try {
            CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
            List<IqpCusLsnpInfo> iqpCusLsnpInfo = iqpCusLsnpInfoService.selectByIqpSerno(serno);
            //建议额度
            BigDecimal lmtSuggest = creditCardAppInfo.getSuggestLmt();
            if(lmtSuggest == null || "".equals(lmtSuggest)){
                lmtSuggest = new BigDecimal(0);
            }
            //零售内评额度
            BigDecimal lmtAdvice = iqpCusLsnpInfo.get(0).getLmtAdvice();
            if (lmtSuggest.compareTo(new BigDecimal(50000)) < 0 && lmtSuggest.compareTo(lmtAdvice) < 0) {
                param.put("secondFlag", "1");
            } else {
                //50000以上，超过零售内评额度
                param.put("secondFlag", "2");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("获取零售内评额度异常" + e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
        return param;
    }

    /**
     * @param param
     * @return
     * @author wzy
     * @date 2021/6/9 16:00
     * @version 1.0.0
     * @desc 更新退回原因
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int returnMain(CreditCardAppInfo param) {
        String serno = param.getSerno();
        String returnReason = param.getReturnReason();
        CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
        creditCardAppInfo.setReturnReason(returnReason);
        return this.updateSelective(creditCardAppInfo);
    }

    /**
     * @param param
     * @return
     * @author wzy
     * @date 2021/6/9 16:00
     * @version 1.0.0
     * @desc 根据业务流水号查询征信地址
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public CreditReportQryLst selectReportUrlBySerno(CreditCardAppInfo param) {
        String serno = param.getSerno();
        CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectReportUrlBySerno(serno);
        return creditReportQryLst;
    }

    /*
     * 证件映射 信贷--->征信
     * @param xdbz 信贷证件码值
     * @return hxbz 征信证件码值
     */
    public static String toConverCurrency(String xdbz) {
        String hxbz = "";
        if ("C".equals(xdbz)) { //户口簿
            hxbz = "1";
        } else if ("B".equals(xdbz)) {//护照
            hxbz = "2";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            hxbz = "5";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            hxbz = "6";
        } else if ("12".equals(xdbz)) { // 外国人居留证
            hxbz = "8";
        } else if ("Y".equals(xdbz)) {//警官证
            hxbz = "9";
        } else if ("13".equals(xdbz)) {//香港身份证
            hxbz = "A";
        } else if ("14".equals(xdbz)) {//澳门身份证
            hxbz = "B";
        } else if ("15".equals(xdbz)) {//台湾身份证
            hxbz = "C";
        } else if ("16".equals(xdbz)) {//其他证件
            hxbz = "X";
        } else if ("A".equals(xdbz)) {//居民身份证及其他以公民身份证号为标识的证件
            hxbz = "10";
        } else if ("11".equals(xdbz)) {//军人身份证件
            hxbz = "20";
        } else if ("06".equals(xdbz)) {//工商注册号
            hxbz = "01";
        } else if ("01".equals(xdbz)) {//机关和事业单位登记号
            hxbz = "02";
        } else if ("02".equals(xdbz)) {//社会团体登记号
            hxbz = "03";
        } else if ("03".equals(xdbz)) {//民办非企业登记号
            hxbz = "04";
        } else if ("04".equals(xdbz)) {//基金会登记号
            hxbz = "05";
        } else if ("05".equals(xdbz)) {//宗教证书登记号
            hxbz = "06";
        } else if ("P2".equals(xdbz)) {//中征码
            hxbz = "10";
        } else if ("R".equals(xdbz)) {//统一社会信用代码
            hxbz = "20";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            hxbz = "30";
        } else if ("07".equals(xdbz)) {//纳税人识别号（国税）
            hxbz = "41";
        } else if ("08".equals(xdbz)) {//纳税人识别号（地税）
            hxbz = "42";
        } else {
            hxbz = xdbz;//未匹配到的证件类型
        }
        return hxbz;
    }

    /**
     * @param CreditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:00
     * @version 1.0.0
     * @desc 生成归档信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public CreditCardAppInfo getDocArchiveClient(CreditCardAppInfo CreditCardAppInfo) {
        String serno = CreditCardAppInfo.getSerno();
        CreditCardAppInfo creditCardAppInfo = this.selectByPrimaryKey(serno);
        try {
            // 生成归档任务
            log.info("开始系统生成档案归档信息");
            String cusId = creditCardAppInfo.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setArchiveMode("02");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
            docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
            docArchiveClientDto.setDocType("20");// 20信用卡
            docArchiveClientDto.setDocBizType("03");// 03进件
            docArchiveClientDto.setBizSerno(serno);
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(creditCardAppInfo.getInputId());
            docArchiveClientDto.setManagerBrId(creditCardAppInfo.getInputBrId());
            docArchiveClientDto.setInputId(creditCardAppInfo.getInputId());
            docArchiveClientDto.setInputBrId(creditCardAppInfo.getInputBrId());
            docArchiveClientDto.setPrdName("信用卡申请");
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if (num < 1) {
                log.info("系统生成信用卡档案归档信息失败,业务流水号[{}]", serno);
            }
        } catch (Exception e) {
            log.info("系统生成信用卡档案归档信息失败" + e);
        }
        return CreditCardAppInfo;
    }

    /**
     * @param sequenceDto
     * @return
     * @author wzy
     * @date 2021/6/9 16:00
     * @version 1.0.0
     * @desc 自动生成流水号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public SequenceDto getSerno(SequenceDto sequenceDto) {
        SequenceDto param = new SequenceDto();
        String code = sequenceDto.getCode();
        try {
            // 生成归档任务
            log.info("开始系统生成档案归档信息");
            //新业务流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(code, new HashMap<>());
            param.setSerno(serno);
        } catch (Exception e) {
            log.info("系统生成档案归档信息失败" + e);
        }
        return param;
    }

    /**
     * 获取客户经理名称
     *
     * @param record
     * @return
     */
    public AdminSmUserDto getManagerName(AdminSmUserDto record) {
        String managerName = null;
        AdminSmUserDto adminSmUserDto = null;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(record.getLoginCode())) {
            ResultDto<cn.com.yusys.yusp.dto.AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(record.getLoginCode());
            if (adminSmUserDtoResultDto != null) {
                String adminSmUserCode = adminSmUserDtoResultDto.getCode();
                adminSmUserDto = adminSmUserDtoResultDto.getData();
            }
        }
        return adminSmUserDto;
    }

    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/9/13 15:49
     * @version 1.0.0
     * @desc app集中处理方法，0,发送ecif开户，1，征信查询 2，发送lsnp 3,保存数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map handAppTogether(CreditCardAppInfo creditCardAppInfo) {
        String  serno =   creditCardAppInfo.getSerno();
        CreditCardAppInfo record = selectByPrimaryKey(serno);
        Xdkh0019DataReqDto xdkh0019DataReqDto = new Xdkh0019DataReqDto();
        xdkh0019DataReqDto.setCertNo(creditCardAppInfo.getCertCode());
        xdkh0019DataReqDto.setCertType(creditCardAppInfo.getCertType());
        xdkh0019DataReqDto.setManagerBrId(creditCardAppInfo.getInputBrId());
        xdkh0019DataReqDto.setCusName(creditCardAppInfo.getCusName());
        xdkh0019DataReqDto.setManagerId(creditCardAppInfo.getInputId());
        xdkh0019DataReqDto.setOpType("01");
        Xdkh0019DataRespDto xdkh0019DataRespDto = xdkh0019Service.xdkh0019(xdkh0019DataReqDto);
        String cusId = xdkh0019DataRespDto.getCusId();
        creditCardAppInfo.setCusId(cusId);
        if(record == null){
            creditCardAppInfo.setApproveStatus("111");
            creditCardAppInfo.setOprType("01");
            //取系统日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            creditCardAppInfo.setAppDate(openDay);
            int result = insertSelective(creditCardAppInfo);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "新增信用卡信息异常！");
            }
        }else{
            int result = updateSelective(creditCardAppInfo);
            if (result != 1) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "更新信用卡信息异常！");
            }
        }
        // 获取系统时间
        log.info("调用征信查询接口开始，业务流水号【{}】", serno);
        //取系统日期
        Date openDay = new Date();
        SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd");
        String nowDay =sdf.format(openDay);
        // 30天前
        Date oldDay = DateUtils.addDay(openDay, -30);
        // 调用通用接口
        CredzbReqDto credzbReqDto = new CredzbReqDto();
        credzbReqDto.setRuleCode("R003");
        credzbReqDto.setReqId(creditCardAppInfo.getSerno());
        credzbReqDto.setBrchno(creditCardAppInfo.getInputBrId());
        credzbReqDto.setCertificateNum(creditCardAppInfo.getCertCode());
        credzbReqDto.setCustomName(creditCardAppInfo.getCusName());
        credzbReqDto.setStartDate(DateUtils.formatDate(oldDay,"yyyy-MM-dd"));
        credzbReqDto.setEndDate(nowDay);
        log.info("征信查询发送报文："+credzbReqDto);
//        ResultDto<CredzbRespDto> credzbRespDto = dscms2Ciis2ndClientService.credzb(credzbReqDto);
//        log.info("征信查询返回报文："+credzbRespDto);
//        if (credzbRespDto != null) {
//            log.info("调用征信查询接口成功，业务流水号【{}】", serno);
//            if (CommonUtils.nonNull(credzbRespDto.getData()) && CommonUtils.nonNull(credzbRespDto.getData().getR003()) && StringUtils.nonBlank(credzbRespDto.getData().getR003().getREPORTID())) {
//                log.info("30天内征信报告编号【{}】", credzbRespDto.getData().getR003().getREPORTID());
//            } else {
//                log.info("不存在30天内有效的征信报告");
//                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key,credzbRespDto.getMessage());
//            }
//        } else {
//            String errorMsg = "";
//            if(credzbRespDto!=null){
//                errorMsg = credzbRespDto.getMessage();
//            }
//            log.info("征信查询接口调用失败");
//            throw new YuspException("征信查询接口调用失败",errorMsg);
//        }
        log.info("调用征信查询接口结束，业务流水号【{}】",serno);
        Map param = handleBusinessDataAfterRun(serno);
        submitMortgage(creditCardAppInfo,param);
        return param;
    }


    public void submitMortgage(CreditCardAppInfo creditCardAppInfo,Map param){
        String serno = creditCardAppInfo.getSerno();
        String cardCusType = creditCardAppInfo.getCardCusType();
        String applyCardPrd = creditCardAppInfo.getApplyCardPrd();
        String outsystemcode = "XXD_PK_B0601,XXD_PK_B0602,XXD_PK_B0603,XXD_PK_B0604,XXD_PK_B0605";
        String topoutsystemcode = "XXD_PK";
        // 客户类型与影像目录映射
        if (!CardPrdCode.CARD_PRD_CODE_LYJK.equals(applyCardPrd) && !CardPrdCode.CARD_PRD_CODE_LYJDZK.equals(applyCardPrd)) {
            if ("B06".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0601,XXD_PK_B0602,XXD_PK_B0603,XXD_PK_B0604,XXD_PK_B0605";
            } else if ("B01".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0101,XXD_PK_B0102,XXD_PK_B0103,XXD_PK_B0104,XXD_PK_B0105";
            } else if ("B02".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0201,XXD_PK_B0202,XXD_PK_B0203,XXD_PK_B0204,XXD_PK_B0205";
            } else if ("B07".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0701,XXD_PK_B0702,XXD_PK_B0703,XXD_PK_B0704,XXD_PK_B0705";
            } else if ("B03".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0301,XXD_PK_B0302,XXD_PK_B0303,XXD_PK_B0304,XXD_PK_B0305";
            } else if ("C09".equals(cardCusType)) {
                outsystemcode = "XXD_PK_C0901,XXD_PK_C0902,XXD_PK_C0903,XXD_PK_C0904,XXD_PK_C0905,XXD_PK_C0906";
            } else if ("C06".equals(cardCusType)) {
                outsystemcode = "XXD_PK_C0601,XXD_PK_C0602,XXD_PK_C0603,XXD_PK_C0604,XXD_PK_C0605,XXD_PK_C0606";
            } else if ("H07".equals(cardCusType)) {
                outsystemcode = "XXD_PK_H0701,XXD_PK_H0702,XXD_PK_H0703,XXD_PK_H0704,XXD_PK_H0705,XXD_PK_H0706";
            } else if ("E01".equals(cardCusType)) {
                outsystemcode = "XXD_PK_E0101,XXD_PK_E0102,XXD_PK_E0103,XXD_PK_E0104,XXD_PK_E0105,XXD_PK_E0106";
            } else if ("H04".equals(cardCusType)) {
                outsystemcode = "XXD_PK_H0401,XXD_PK_H0402,XXD_PK_H0403,XXD_PK_H0404,XXD_PK_H0405,XXD_PK_H0406";
            } else if ("B14".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B1401,XXD_PK_B1402,XXD_PK_B1403,XXD_PK_B1404,XXD_PK_B1405,XXD_PK_B1406";
            } else if ("X01".equals(cardCusType)) {
                outsystemcode = "XXD_PK_X0101,XXD_PK_X0102,XXD_PK_X0103,XXD_PK_X0104,XXD_PK_X0105,XXD_PK_X0106";
            } else if ("C08".equals(cardCusType)) {
                outsystemcode = "XXD_PK_C0801,XXD_PK_C0802,XXD_PK_C0803,XXD_PK_C084,XXD_PK_C0805,XXD_PK_C0806,XXD_PK_C0807,XXD_PK_C0808";
            } else if ("B15".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B1501,XXD_PK_B1502,XXD_PK_B1503,XXD_PK_B1504,XXD_PK_B1505,XXD_PK_B1506";
            } else if ("C04".equals(cardCusType)) {
                outsystemcode = "XXD_PK_C0401,XXD_PK_C0402,XXD_PK_C0403,XXD_PK_C0404,XXD_PK_C0405,XXD_PK_C0406,XXD_PK_C0407";
            } else if ("B04".equals(cardCusType)) {
                outsystemcode = "XXD_PK_B0401,XXD_PK_B0402,XXD_PK_B0403,XXD_PK_B0404,XXD_PK_B0405,XXD_PK_B0406";
            } else if ("H05".equals(cardCusType)) {
                outsystemcode = "XXD_PK_H0501,XXD_PK_H0502,XXD_PK_H0503,XXD_PK_H0504,XXD_PK_H0505,XXD_PK_H0506";
            }
        } else {
            // 乐悠金电子卡影像目录
            topoutsystemcode = "XXD_LYJ";
            if ("B06".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B0601,XXD_LYJ_B0602,XXD_LYJ_B0603,XXD_LYJ_B0604,XXD_LYJ_B0605";
            } else if ("B01".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B0101,XXD_LYJ_B0102,XXD_LYJ_B0103,XXD_LYJ_B0104,XXD_LYJ_B0105";
            } else if ( "B02".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B0201,XXD_LYJ_B0202,XXD_LYJ_B0203,XXD_LYJ_B0204,XXD_LYJ_B0205";
            } else if ("B03".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B0301,XXD_LYJ_B0302,XXD_LYJ_B0303,XXD_LYJ_B0304,XXD_LYJ_B0305";
            } else if ("C09".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_C0901,XXD_LYJ_C0902,XXD_LYJ_C0903,XXD_LYJ_C0904,XXD_LYJ_C0905,,XXD_LYJ_C0906";
            } else if ("E01".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_E0101,XXD_LYJ_E0102,XXD_LYJ_E0103,XXD_LYJ_E0104,XXD_LYJ_E0105,XXD_LYJ_E0106,XXD_LYJ_E0107,XXD_LYJ_E0108,XXD_LYJ_E0109,XXD_LYJ_E0110";
            } else if ("H04".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_H0401,XXD_LYJ_H0402,XXD_LYJ_H0403,XXD_LYJ_H0404,XXD_LYJ_H0405,XXD_LYJ_H0406,XXD_LYJ_H0407,XXD_LYJ_H0408,XXD_LYJ_H0409,XXD_LYJ_H0410";
            } else if ("B14".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B1401,XXD_LYJ_B1402,XXD_LYJ_B1403,XXD_LYJ_B1404,XXD_LYJ_B1405,XXD_LYJ_B1406";
            } else if ("C08".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_C0801,XXD_LYJ_C0802,XXD_LYJ_C0803,XXD_LYJ_C0804,XXD_LYJ_C0805,XXD_LYJ_C0806,XXD_LYJ_C0807,XXD_LYJ_C0808";
            } else if ("C03".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_C0301,XXD_LYJ_C0302,XXD_LYJ_C0303,XXD_LYJ_C0304,XXD_LYJ_C0305,XXD_LYJ_C0306,XXD_LYJ_C0307";
            } else if ("C02".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_C0401,XXD_LYJ_C0402,XXD_LYJ_C0403,XXD_LYJ_C0404,XXD_LYJ_C0405,XXD_LYJ_C0406";
            } else if ("B08".equals(cardCusType)) {
                outsystemcode = "XXD_LYJ_B0801,XXD_LYJ_B0802,XXD_LYJ_B0803,XXD_LYJ_B0804,XXD_LYJ_B0805,XXD_LYJ_B0806";
            }
        }
        log.info("流水号【"+serno+"】,流程发起操作 开始");
        WFStratDto wfStratDto = new WFStratDto();
        wfStratDto.setSystemId("cmis");
        wfStratDto.setOrgId(creditCardAppInfo.getInputBrId());
        wfStratDto.setUserId(creditCardAppInfo.getInputId());
        wfStratDto.setBizId(creditCardAppInfo.getSerno());
        wfStratDto.setBizType("XK002");
        wfStratDto.setBizUserName(creditCardAppInfo.getCusName());
        wfStratDto.setBizUserId(creditCardAppInfo.getCusId());
        param.put("topoutsystemcode",topoutsystemcode);
        param.put("outsystemcode",outsystemcode);
        param.put("certCode",creditCardAppInfo.getCertCode());
        param.put("applyCardType",creditCardAppInfo.getApplyType());
        param.put("isConsider","0");
        wfStratDto.setParam(param);
        log.info("流程参数为:"+wfStratDto);
        ResultDto<ResultInstanceDto> resultDto = workflowCoreClient.start(wfStratDto);
        log.info("流水号【"+serno+"】,流程发起操作 结束");

        String instanceId = resultDto.getData().getInstanceId();
        String nodeId = resultDto.getData().getNodeId();

        ResultDto<ResultInstanceDto> instanceResultDto = workflowCoreClient.instanceInfo(instanceId, nodeId,null);
        ResultInstanceDto instanceInfo = instanceResultDto.getData();

        log.info("流水号【"+serno+"】,获取下一节点 开始");
        WFNextNodeDto wFNextNodeDto = new WFNextNodeDto();
        wFNextNodeDto.setInstanceId(instanceInfo.getInstanceId());
        wFNextNodeDto.setNodeId(instanceInfo.getNodeId());
        wFNextNodeDto.setParam(instanceInfo.getParam());
        ResultDto<List<ResultNodeDto>> resultNodeDto = workflowCoreClient.getNextNodeInfos(wFNextNodeDto);

        log.info("流水号【"+serno+"】,获取下一节点 结束");

        if("0".equals(resultNodeDto.getCode()) && cn.com.yusys.yusp.commons.util.collection.CollectionUtils.nonEmpty(resultNodeDto.getData())) {
            List<NextNodeInfoDto> submitNextNodeInfoList = new ArrayList<>();
            List<ResultNodeDto> nextAccessNodeList = resultNodeDto.getData();

            for (ResultNodeDto resultNode : nextAccessNodeList) {
                NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
                nextNodeInfoDto.setNextNodeId(resultNode.getNodeId());
                List<String> userIdsList = new ArrayList<>();
                if (cn.com.yusys.yusp.commons.util.collection.CollectionUtils.nonEmpty(resultNode.getUsers())) {
                    resultNode.getUsers().stream().forEach(item -> {
                        userIdsList.add(item.getUserId());
                    });
                } else {
                    throw BizException.error(null, "999999", "节点办理人为空，提交失败！");
                }
                nextNodeInfoDto.setNextNodeUserIds(userIdsList);
                submitNextNodeInfoList.add(nextNodeInfoDto);
            }

            log.info("流水号【"+serno+"】,流程提交操作 开始");
            //流程提交
            WFSubmitDto wFSubmitDto = new WFSubmitDto();
            wFSubmitDto.setOrgId(instanceInfo.getOrgId());
            wFSubmitDto.setNextNodeInfos(submitNextNodeInfoList);
            wFSubmitDto.setParam(instanceInfo.getParam());
            WFCommentDto wFCommentDto = new WFCommentDto();
            wFCommentDto.setCommentSign("O-12");
            wFCommentDto.setUserComment("同意");
            wFCommentDto.setInstanceId(instanceInfo.getInstanceId());
            wFCommentDto.setNodeId(instanceInfo.getNodeId());
            wFCommentDto.setUserId(creditCardAppInfo.getInputId());
            wFSubmitDto.setComment(wFCommentDto);
            ResultDto<List<ResultMessageDto>> commmitResultDto = workflowCoreClient.submit(wFSubmitDto);

            log.info("流程提交响应结果：{}", commmitResultDto);
            if (!"0".equals(commmitResultDto.getCode())) {
                throw BizException.error(null, "999999", "流程提交失败！");
            } else {
                if (commmitResultDto.getData().size() > 0 && "提交完成".equals(commmitResultDto.getData().get(0).getTip())) {
                    log.info("流程提交成功：{}", commmitResultDto.getData().get(0));
                } else {
                    throw BizException.error(null, "999999", commmitResultDto.getData().get(0).getTip());
                }

                log.info("流水号【"+serno+"】,集中作业中心档案池生成档案接受任务 开始");
                createCentralFileTask(creditCardAppInfo,serno,instanceId,submitNextNodeInfoList.get(0).getNextNodeId(),"DB009");
                log.info("流水号【"+serno+"】,集中作业中心档案池生成档案接受任务 结束");
            }
        }
    }
    /**
     * 生成集中作业档案池任务
     * @param taskSerno
     * @param bizType
     */
    public void createCentralFileTask(CreditCardAppInfo creditCardAppInfo,String taskSerno,String instanceId,String nodeId,String bizType){
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(taskSerno);
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(creditCardAppInfo.getInputId());
        centralFileTaskdto.setInputBrId(creditCardAppInfo.getInputBrId());
        //档案任务操作类型 01--纯指令
        centralFileTaskdto.setOptType(CmisBizConstants.STD_OPT_TYPE_01);
        centralFileTaskdto.setInstanceId(instanceId);
        centralFileTaskdto.setNodeId(nodeId);
        //档案任务类型 03--派发
        centralFileTaskdto.setTaskType(CmisBizConstants.STD_FILE_TASK_TYPE_03);
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急

        centralFileTaskService.insertSelective(centralFileTaskdto);
    }
}
