package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarMortgageRegisterApp;
import cn.com.yusys.yusp.domain.GuarRegisterDetail;

import java.util.List;
import java.util.Objects;

public class GuarMortgageRegisterVo extends PageQuery {
    private GuarMortgageRegisterApp guarMortgageRegisterApp;
    private List<GuarRegisterDetail> guarRegisterDetail;

    public GuarMortgageRegisterApp getGuarMortgageRegisterApp() {
        return guarMortgageRegisterApp;
    }

    public void setGuarMortgageRegisterApp(GuarMortgageRegisterApp guarMortgageRegisterApp) {
        this.guarMortgageRegisterApp = guarMortgageRegisterApp;
    }

    public List<GuarRegisterDetail> getGuarRegisterDetail() {
        return guarRegisterDetail;
    }

    public void setGuarRegisterDetail(List<GuarRegisterDetail> guarRegisterDetail) {
        this.guarRegisterDetail = guarRegisterDetail;
    }
}
