package cn.com.yusys.yusp.service.server.xdxw0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0011.req.Xdxw0011DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtInspectInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:提交勘验信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0011Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0011Service.class);

    @Autowired
    private LmtInspectInfoMapper lmtInspectInfoMapper;

    /**
     * 提交勘验信息
     * @param xdxw0011DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int updateLmtInspectInfo(Xdxw0011DataReqDto xdxw0011DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataReqDto));
        int result = 0;
        try {
            result = lmtInspectInfoMapper.updateLmtInspectInfo(xdxw0011DataReqDto);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, result);
        return result;
    }
}
