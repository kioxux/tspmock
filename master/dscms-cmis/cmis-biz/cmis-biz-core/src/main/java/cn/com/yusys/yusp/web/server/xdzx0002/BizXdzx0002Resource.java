package cn.com.yusys.yusp.web.server.xdzx0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.Xdzx0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzx0002.Xdzx0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:征信授权查看
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0002:征信授权查看")
@RestController
@RequestMapping("/api/bizzx4bsp")
public class BizXdzx0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzx0002Resource.class);

    @Autowired
    private Xdzx0002Service xdzx0002Service;

    /**
     * 交易码：xdzx0002
     * 交易描述：征信授权查看
     *
     * @param xdzx0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("征信授权查看")
    @PostMapping("/xdzx0002")
    protected @ResponseBody
    ResultDto<Xdzx0002DataRespDto> xdzx0002(@Validated @RequestBody Xdzx0002DataReqDto xdzx0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataReqDto));
        Xdzx0002DataRespDto xdzx0002DataRespDto = new Xdzx0002DataRespDto();// 响应Dto:征信授权查看
        ResultDto<Xdzx0002DataRespDto> xdzx0002DataResultDto = new ResultDto<>();
        try {
            String certCdoe = xdzx0002DataReqDto.getCertCode();
            if (StringUtil.isEmpty(certCdoe)) {
                xdzx0002DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdzx0002DataResultDto.setMessage("证件号【certCdoe】不能为空！");
                return xdzx0002DataResultDto;
            }
            // 从xdzx0002DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataReqDto));
            xdzx0002DataRespDto = xdzx0002Service.xdzx0002(xdzx0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataRespDto));
            // 封装xdzx0002DataResultDto中正确的返回码和返回信息
            xdzx0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzx0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, e.getMessage());
            // 封装xdzx0002DataResultDto中异常返回码和返回信息
            xdzx0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzx0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzx0002DataRespDto到xdzx0002DataResultDto中
        xdzx0002DataResultDto.setData(xdzx0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataResultDto));
        return xdzx0002DataResultDto;
    }
}
