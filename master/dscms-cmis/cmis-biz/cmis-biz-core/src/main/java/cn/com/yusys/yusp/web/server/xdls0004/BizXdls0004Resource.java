package cn.com.yusys.yusp.web.server.xdls0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0004.req.Xdls0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0004.resp.Xdls0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdls0004.Xdls0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询信贷有无授信历史
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDLS0004:查询信贷有无授信历史")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0004Resource.class);

    @Autowired
    private Xdls0004Service xdls0004Service;
    /**
     * 交易码：xdls0004
     * 交易描述：查询信贷有无授信历史
     *
     * @param xdls0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷有无授信历史")
    @PostMapping("/xdls0004")
    protected @ResponseBody
    ResultDto<Xdls0004DataRespDto> xdls0004(@Validated @RequestBody Xdls0004DataReqDto xdls0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004DataReqDto));
        Xdls0004DataRespDto xdls0004DataRespDto = new Xdls0004DataRespDto();// 响应Dto:查询信贷有无授信历史
        ResultDto<Xdls0004DataRespDto> xdls0004DataResultDto = new ResultDto<>();
        try {
            String cusId = xdls0004DataReqDto.getCusId();//核心客户号
            // 从xdls0004DataReqDto获取业务值进行业务逻辑处理
            xdls0004DataRespDto = xdls0004Service.hasLmtReplyByCusId(cusId);
            // 封装xdls0004DataResultDto中正确的返回码和返回信息
            xdls0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, e.getMessage());
            // 封装xdls0004DataResultDto中异常返回码和返回信息
            xdls0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdls0004DataRespDto到xdls0004DataResultDto中
        xdls0004DataResultDto.setData(xdls0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004DataRespDto));
        return xdls0004DataResultDto;
    }
}
