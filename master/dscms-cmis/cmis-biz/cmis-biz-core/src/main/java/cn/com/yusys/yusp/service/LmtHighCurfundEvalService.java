/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtHighCurfundEval;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtHighCurfundEvalMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: LmtHighCurfundEvalService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-01 19:14:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtHighCurfundEvalService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);
    @Resource
    private LmtHighCurfundEvalMapper lmtHighCurfundEvalMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private LmtAppService lmtAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtHighCurfundEval selectByPrimaryKey(String pkId) {
        return lmtHighCurfundEvalMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtHighCurfundEval> selectAll(QueryModel model) {
        List<LmtHighCurfundEval> records = (List<LmtHighCurfundEval>) lmtHighCurfundEvalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtHighCurfundEval> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtHighCurfundEval> list = lmtHighCurfundEvalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtHighCurfundEval record) {
        return lmtHighCurfundEvalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtHighCurfundEval lmtHighCurfundEval) {
        //String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_HIGH_CURFUND_EVAL, new HashMap<>());
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerBrId = userInfo.getOrg().getCode();
        if (StringUtils.isBlank(managerBrId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String curTime= sdf.format(date);
        // 现根据serno去判断是否存在  右侧更新 无则插入
        LmtHighCurfundEval isExistLmtHighCurfundEval = lmtHighCurfundEvalMapper.selectBySerno(lmtHighCurfundEval.getSerno());
        if(isExistLmtHighCurfundEval == null){
            lmtHighCurfundEval.setPkId(UUID.randomUUID().toString());
            lmtHighCurfundEval.setOprType("01");
            lmtHighCurfundEval.setInputId(managerId);
            lmtHighCurfundEval.setInputBrId(managerBrId);
            lmtHighCurfundEval.setInputDate(curTime);
            lmtHighCurfundEval.setUpdId(managerId);
            lmtHighCurfundEval.setUpdBrId(managerBrId);
            lmtHighCurfundEval.setUpdDate(curTime);
            lmtHighCurfundEval.setCreateTime(date);
            lmtHighCurfundEval.setUpdateTime(date);
            return lmtHighCurfundEvalMapper.insertSelective(lmtHighCurfundEval);
        }else{
            lmtHighCurfundEval.setUpdId(managerId);
            lmtHighCurfundEval.setUpdBrId(managerBrId);
            lmtHighCurfundEval.setUpdDate(curTime);
            lmtHighCurfundEval.setUpdateTime(date);
            return updateSelective(lmtHighCurfundEval);
        }


    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtHighCurfundEval record) {
        return lmtHighCurfundEvalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtHighCurfundEval record) {
        return lmtHighCurfundEvalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtHighCurfundEvalMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtHighCurfundEvalMapper.deleteByIds(ids);
    }


    /**
     * @方法名称：calOptAmt
     * @方法描述：测算运营资金量
     * @参数与返回说明：
     * @算法描述：调用智能风控接口
     * @创建人：zhangming12
     * @创建时间：2021-04-10 下午 1:34
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map calOptAmt(LmtHighCurfundEval lmtHighCurfundEval) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        BigDecimal oprCap = new BigDecimal(0);

        try {
            BigDecimal ltYearSaleIncome = lmtHighCurfundEval.getLtYearSaleIncome();//上年度销售收入
            BigDecimal ltYearSaleProfitRate = lmtHighCurfundEval.getLtYearSaleProfitRate();//上年度销售利润率
            BigDecimal foreSaleIncomeGrowRate = lmtHighCurfundEval.getForeSaleIncomeGrowRate();//预计销售收入年增长率
            BigDecimal ltYearOprfundsTurnovDay = lmtHighCurfundEval.getLtYearOprfundsTurnovDay();//上年度营运资金周转次数
            if (ltYearSaleIncome == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if (ltYearSaleProfitRate == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if (foreSaleIncomeGrowRate == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if ("0".equals(String.valueOf(ltYearOprfundsTurnovDay)) || "null".equals(String.valueOf(ltYearOprfundsTurnovDay))) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            //营运资金量
            //        营运资金量=上年度销售收入*（1-上年度销售利润率）*（1+预计销售收入年增长率%）/上年度营运资金周转次数
            oprCap = ltYearSaleIncome.multiply(new BigDecimal(1).subtract(ltYearSaleProfitRate))
                    .multiply(new BigDecimal(1).add(foreSaleIncomeGrowRate)).divide(ltYearOprfundsTurnovDay, 2, RoundingMode.HALF_UP);

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存授信申请数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            //rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("oprCap", oprCap);
        }
        return rtnData;
    }

    /**
     * @方法名称：calLimit
     * @方法描述：测算额度
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-15 下午 2:06
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map calLimit(LmtHighCurfundEval lmtHighCurfundEval) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        BigDecimal curfundLoanLmt = new BigDecimal(0);

        try {
            BigDecimal oprCap = lmtHighCurfundEval.getOprCap();//营运资金量
            BigDecimal borrowerCap = lmtHighCurfundEval.getBorrowerCap();//借款人自有资金
            BigDecimal curfundLoan = lmtHighCurfundEval.getCurfundLoan();//本行现有流动资金贷款
            BigDecimal otherChnlProvidOprCap = lmtHighCurfundEval.getOtherChnlProvidOprCap();//其他渠道提供的运营资金

            if (oprCap == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if (borrowerCap == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if (curfundLoan == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            if (otherChnlProvidOprCap == null) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }

            //测算额度
                  /*流动资金贷款额度=营运资金量-（借款人“货币资金”-保证金存款）-（本行银承敞口余额+他行银承敞口余额）
                    -（本行现有流动资金贷款+他行现有流动资金贷款）-其他渠道提供的运营资金*/
            //其中保证金存款、本行银承敞口余额、他行银承敞口余额、他行现有流动资金贷款均没有值，所以这里没有将其纳入运算
            curfundLoanLmt = oprCap.subtract(borrowerCap).subtract(curfundLoan).subtract(otherChnlProvidOprCap);

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存授信申请数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            //rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("curfundLoanLmt", curfundLoanLmt);
        }
        return rtnData;
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */

    public LmtHighCurfundEval selectBySerno(String serno) {
        LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalMapper.selectBySerno(serno);
        if(Objects.isNull(lmtHighCurfundEval)) {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            if(Objects.nonNull(lmtApp) && StringUtils.nonBlank(lmtApp.getCusId())) {
                ResultDto<Map<String, FinanIndicAnalyDto>> resultDto =  icusClientService.getRptFncTotalProfitForLmtHighCurfundEval(lmtApp.getCusId());
                log.info("当前客户{【"+lmtApp.getCusId()+"】}获取的财报信息{【"+ JSON.toJSONString(resultDto)+"】}");
                if(Objects.nonNull(resultDto) && Objects.nonNull(resultDto.getData())) {
                    lmtHighCurfundEval = new LmtHighCurfundEval();
                    Map<String, FinanIndicAnalyDto> resultMap = resultDto.getData();
                    // lmtHighCurfundEval.setBorrowerCap(NumberUtils.nullDefaultZero(0));
                    // 初始值
                    //流动资金测算规则处理:
                    //营运资金量=上年度销售收入*（1-上年度销售利润率）*（1+预计销售收入年增长率）/营运资金周转次数
                    //营运资金周转次数=360/(存货周转天数+应收账款周转天数-应付账款周转天数+预付账款周转天数-预收账款周转天数)
                    //周转天数=360/周转次数
                    //应收账款周转次数=销售收入/平均应收账款余额，平均应收账款余额=（应收账款年初数+应收账款年末数）/2
                    //预收账款周转次数=销售收入/平均预收账款余额，平均预收账款余额=（预收账款年初数+预收账款年末数）/2
                    //存货周转次数=销售成本/平均存货余额，平均存货余额=（存货年初数+存货年末数）/2
                    //预付账款周转次数=销售成本/平均预付账款余额，平均预付账款余额=（预付账款年初数+预付账款年末数）/2
                    //应付账款周转次数=销售成本/平均应付账款余额，平均应付账款余额=（应付账款年初数+应付账款年末数）/2
                    //新增流动资金贷款额度=营运资金量-借款人自有资金-现有流动资金贷款-其他渠道提供的营运资金
                        // 上年度销售收入
                        lmtHighCurfundEval.setLtYearSaleIncome(NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("salesIncome")).get().getCurYmValue()));
                        // 上年度销售利润率
                        lmtHighCurfundEval.setLtYearSaleProfitRate(NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("saleProfitRate")).get().getCurYmValue()));
                        // 销售收入是否为0
                        if (BigDecimal.ZERO.compareTo(lmtHighCurfundEval.getLtYearSaleIncome()) == 0) {
                            // 上年度应收款周转天数
                            lmtHighCurfundEval.setLtYearRcvTurnovDay(new BigDecimal(0));
                            // 上年度预收款周转天数
                            lmtHighCurfundEval.setLtYearPpmTurnoDay(new BigDecimal(0));
                        } else {
                            // 上年度应收款周转天数
                            lmtHighCurfundEval.setLtYearRcvTurnovDay(BigDecimal.valueOf(180)
                                    .multiply(Optional.ofNullable(resultMap.get("rcvTurnovYearBegin")).get().getCurYmValue()
                                            .add(Optional.ofNullable(resultMap.get("rcvTurnovYearEnd")).get().getCurYmValue()))
                                    .divide(lmtHighCurfundEval.getLtYearSaleIncome(),2, BigDecimal.ROUND_HALF_UP));
                            // 上年度预收款周转天数
                            lmtHighCurfundEval.setLtYearPpmTurnoDay(BigDecimal.valueOf(180)
                                    .multiply(Optional.ofNullable(resultMap.get("ppmTurnoYearBegin")).get().getCurYmValue()
                                            .add(Optional.ofNullable(resultMap.get("ppmTurnoYearEnd")).get().getCurYmValue()))
                                    .divide(lmtHighCurfundEval.getLtYearSaleIncome(),2, BigDecimal.ROUND_HALF_UP));
                        }
                        BigDecimal salesCost = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("salesCost")).get().getCurYmValue());
                        BigDecimal salesIncome = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("salesIncome")).get().getCurYmValue());
                        // 销售收入是否为0
                        if (BigDecimal.ZERO.compareTo(salesCost) == 0) {
                            // 上年度存货周转天数
                            lmtHighCurfundEval.setLtYearIvtTurnovDay(new BigDecimal(0));
                            // 上年度应付账款周转天数
                            lmtHighCurfundEval.setLtYearAcpTurnovDay(new BigDecimal(0));
                            // 上年度预付款周转天数
                            lmtHighCurfundEval.setLtYearAdvanceTurnovDay(new BigDecimal(0));
                        } else {
                            // 上年度存货周转天数
                            lmtHighCurfundEval.setLtYearIvtTurnovDay(BigDecimal.valueOf(180)
                                    .multiply(Optional.ofNullable(resultMap.get("ivtTurnovYearBegin")).get().getCurYmValue()
                                    .add(Optional.ofNullable(resultMap.get("ivtTurnovYearEnd")).get().getCurYmValue()))
                                    .divide(salesCost,2, BigDecimal.ROUND_HALF_UP));
                            // 上年度应付账款周转天数
                            lmtHighCurfundEval.setLtYearAcpTurnovDay(BigDecimal.valueOf(180)
                                    .multiply(Optional.ofNullable(resultMap.get("acpTurnovYearBegin")).get().getCurYmValue()
                                    .add(Optional.ofNullable(resultMap.get("acpTurnovYearEnd")).get().getCurYmValue()))
                                    .divide(salesCost, 2,BigDecimal.ROUND_HALF_UP));
                            // 上年度预付款周转天数
                            lmtHighCurfundEval.setLtYearAdvanceTurnovDay(BigDecimal.valueOf(180)
                                    .multiply(Optional.ofNullable(resultMap.get("advanceTurnovYearBegin")).get().getCurYmValue()
                                    .add(Optional.ofNullable(resultMap.get("advanceTurnovYearEnd")).get().getCurYmValue()))
                                    .divide(salesCost,2, BigDecimal.ROUND_HALF_UP));
                        }
                        // 上年度营运资金周转次数
                        // 营运资金周转次数=360/(存货周转天数+应收账款周转天数-应付账款周转天数+预付账款周转天数-预收账款周转天数)
                        BigDecimal ltYearOprfundsTurnovDayTemp = NumberUtils.nullDefaultZero(lmtHighCurfundEval.getLtYearIvtTurnovDay())
                                .add(NumberUtils.nullDefaultZero(lmtHighCurfundEval.getLtYearRcvTurnovDay()))
                                .subtract(NumberUtils.nullDefaultZero(lmtHighCurfundEval.getLtYearAcpTurnovDay()))
                                .add(NumberUtils.nullDefaultZero(lmtHighCurfundEval.getLtYearAdvanceTurnovDay()))
                                .subtract(NumberUtils.nullDefaultZero(lmtHighCurfundEval.getLtYearPpmTurnoDay()));
                        if (BigDecimal.ZERO.compareTo(NumberUtils.nullDefaultZero(ltYearOprfundsTurnovDayTemp)) == 0) {
                            lmtHighCurfundEval.setLtYearOprfundsTurnovDay(new BigDecimal(0));
                        } else {
                            if(BigDecimal.valueOf(360).divide(ltYearOprfundsTurnovDayTemp,2,BigDecimal.ROUND_HALF_UP).compareTo(new BigDecimal(0)) == 0){
                                lmtHighCurfundEval.setLtYearOprfundsTurnovDay(BigDecimal.valueOf(Double.parseDouble(String.valueOf(0.01))));
                            }else{
                                lmtHighCurfundEval.setLtYearOprfundsTurnovDay(BigDecimal.valueOf(360).divide(ltYearOprfundsTurnovDayTemp,2,BigDecimal.ROUND_HALF_UP));
                            }
                        }
                        //借款人自有资金
                        try{
                            if (BigDecimal.ZERO.compareTo(NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("loanerSelfAmt")).get().getCurYmValue()))== 0) {
                                lmtHighCurfundEval.setBorrowerCap(BigDecimal.ZERO);
                            } else {
                                lmtHighCurfundEval.setBorrowerCap(NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("loanerSelfAmt")).get().getCurYmValue()));
                            }
                        }catch(Exception e){
                            lmtHighCurfundEval.setBorrowerCap(BigDecimal.ZERO);
                        }

                    }
                }
            }

        return lmtHighCurfundEval;
    }

    public static void main(String[] args) {

        BigDecimal a1 = BigDecimal.valueOf(180);
        BigDecimal a2 = BigDecimal.valueOf(Double.parseDouble(String.valueOf(29266967.01)));
        BigDecimal a3 = BigDecimal.valueOf(Double.parseDouble(String.valueOf(31107559.75)));
        BigDecimal a4 = BigDecimal.valueOf(Double.parseDouble(String.valueOf(59034334.89)));

        System.out.println(a1.multiply(a2.add(a3)).divide(a4,2,BigDecimal.ROUND_HALF_UP));

    }
}



