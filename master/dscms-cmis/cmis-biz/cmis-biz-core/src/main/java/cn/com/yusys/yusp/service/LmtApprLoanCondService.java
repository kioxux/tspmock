/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtApproveSubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtApprLoanCondMapper;
import cn.com.yusys.yusp.repository.mapper.LmtApprMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import net.sf.cglib.beans.BeanMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprLoanCondService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:21:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtApprLoanCondService {

    @Resource
    private LmtApprLoanCondMapper lmtApprLoanCondMapper;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private LmtGrpApprService lmtGrpApprService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtApprLoanCond selectByPrimaryKey(String pkId) {
        return lmtApprLoanCondMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtApprLoanCond> selectAll(QueryModel model) {
        List<LmtApprLoanCond> records = (List<LmtApprLoanCond>) lmtApprLoanCondMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApprLoanCond> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtApprLoanCond> list = lmtApprLoanCondMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtApprLoanCond record) {
        return lmtApprLoanCondMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtApprLoanCond record) {
        return lmtApprLoanCondMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtApprLoanCond record) {
        return lmtApprLoanCondMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtApprLoanCond record) {
        return lmtApprLoanCondMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtApprLoanCondMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtApprLoanCondMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectLmtApprLoanCondByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprLoanCond> selectLmtApprLoanCondByParams(HashMap paramsMap) {
        return lmtApprLoanCondMapper.selectLmtApprLoanCondByParams(paramsMap);
    }

    /**
     * @方法名称:
     * @方法描述: 通过授信审批条件复制新的一套审批条件数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprLoanCondByCopyLast(LmtAppr newLmtAppr, LmtAppr lmtAppr) {
        log.info("更新审批用信条件信息----------start----------------");
        // 管理条件信息重新
        HashMap<String, String> querySubLoanCond = new HashMap<String, String>();
        querySubLoanCond.put("apprSerno", lmtAppr.getApproveSerno());
        querySubLoanCond.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("根据原审批表数据查询审批用信条件信息");
        List<LmtApprLoanCond> oldLmtApprLoanCondList = this.selectLmtApprLoanCondByParams(querySubLoanCond);
        if (oldLmtApprLoanCondList != null && oldLmtApprLoanCondList.size() > 0) {
            List<LmtApprLoanCond> lmtApprLoanCondList = this.queryLmtApproveLoanCondDataByApprSerno(newLmtAppr.getApproveSerno());
            log.info("查询是否已存在用信条件!"+JSON.toJSONString(lmtApprLoanCondList));
            if(CollectionUtils.isEmpty(lmtApprLoanCondList)){
                for (LmtApprLoanCond lmtApprLoanCond : oldLmtApprLoanCondList) {
                    LmtApprLoanCond newApprLoanCond = new LmtApprLoanCond();
                    BeanUtils.copyProperties(lmtApprLoanCond, newApprLoanCond);
                    newApprLoanCond.setPkId(UUID.randomUUID().toString());
                    newApprLoanCond.setApproveSerno(newLmtAppr.getApproveSerno());
                    newApprLoanCond.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    newApprLoanCond.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(newApprLoanCond);
                }
            }
        }
        log.info("更新审批用信条件信息----------end----------------");
    }

    /**
     * @方法名称:
     * @方法描述: 通过授信审批条件复制新的一套审批条件数据--资金同业模块
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangjw
     * @创建时间: 2021-08-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprLoanCondByCopyLast(String newApprApproveSerno, String lmtSigApprApproveSerno) {
        // 管理条件信息重新
        HashMap<String, String> querySubLoanCond = new HashMap<String, String>();
        if(StringUtils.isBlank(lmtSigApprApproveSerno)) throw new BizException(null, "", null, "审批流水号不允许为空");
        querySubLoanCond.put("apprSerno", lmtSigApprApproveSerno);
        querySubLoanCond.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("根据原审批表数据查询审批用信条件信息");
        List<LmtApprLoanCond> oldLmtApprLoanCondList = this.selectLmtApprLoanCondByParams(querySubLoanCond);
        if (oldLmtApprLoanCondList != null && oldLmtApprLoanCondList.size() > 0) {
            for (LmtApprLoanCond lmtApprLoanCond : oldLmtApprLoanCondList) {
                LmtApprLoanCond newApprLoanCond = new LmtApprLoanCond();
                BeanUtils.copyProperties(lmtApprLoanCond, newApprLoanCond);
                newApprLoanCond.setPkId(UUID.randomUUID().toString());
                newApprLoanCond.setApproveSerno(newApprApproveSerno);
                this.insert(newApprLoanCond);
            }
        }
    }

    /**
     * @方法名称：queryLmtApproveLoanCondDataByParams
     * @方法描述：通过条件查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-05-13 上午 9:31
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtApprLoanCond> queryLmtApproveLoanCondDataByParams(HashMap<String, String> queryMap) {
        return lmtApprLoanCondMapper.queryLmtApproveLoanCondDataByParams(queryMap);
    }

    /**
     * @方法名称：queryLmtApproveLoanCondDataByApprSerno
     * @方法描述：通过审批流水号查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhuzr
     * @创建时间：2021-05-21
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtApprLoanCond> queryLmtApproveLoanCondDataByApprSerno(String apprSerno) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", CmisCommonConstants.ADD_OPR);
        queryMap.put("apprSerno", apprSerno);
        return lmtApprLoanCondMapper.queryLmtApproveLoanCondDataByParams(queryMap);
    }


    /**
     * @方法名称：deleteByPkId
     * @方法描述：通过主键删除
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhuzr
     * @创建时间：2021-05-21
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map deleteByPkId(String pkId) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            int count = lmtApprLoanCondMapper.deleteByPrimaryKey(pkId);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除用信条件情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    public Map saveLmtApprLoanCond(LmtApprLoanCond lmtApprLoanCond) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            lmtApprLoanCond.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtApprLoanCond.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            int count = lmtApprLoanCondMapper.insert(lmtApprLoanCond);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成用信条件失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("生成用信条件出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 新增其他要求
     *
     * @param model
     * @return
     */
    public int insertLoanCond(QueryModel model) {
        LmtApprLoanCond lmtApprLoanCond = new LmtApprLoanCond();
        Date date = new Date();
        String condDesc = model.getCondition().get("condDesc").toString();
        String approveSerno = model.getCondition().get("approveSerno").toString();
        String condType = model.getCondition().get("condType").toString();
        lmtApprLoanCond.setPkId(UUID.randomUUID().toString());
        lmtApprLoanCond.setCreateTime(date);
        lmtApprLoanCond.setCondDesc(condDesc);
        lmtApprLoanCond.setApproveSerno(approveSerno);
        lmtApprLoanCond.setCondType(condType);
        return lmtApprLoanCondMapper.insert(lmtApprLoanCond);
    }

    /**
     * 根据集团申请流水号获取用信条件
     * @param grpSerno
     * @return
     */
    public ArrayList<LmtApprLoanCond> queryLmtApprLoanCondByGrpSerno(String grpSerno,String condType) {
        return lmtApprLoanCondMapper.queryLmtApprLoanCondByGrpSerno(grpSerno,condType);
    }

    /**
     * 根据申请流水号获取用信条件
     * @param serno
     * @param condType
     * @return
     */
    public ArrayList<LmtApprLoanCond> queryLoanCond(String serno, String condType) {
        ArrayList<LmtApprLoanCond> list = null;
        LmtAppr lmtApprBySerno = lmtApprService.queryInfoBySerno(serno);
        if(lmtApprBySerno != null  && !StringUtils.isEmpty(lmtApprBySerno.getApproveSerno())) {
            HashMap<String, String> params = new HashMap<>();
            params.put("apprSerno", lmtApprBySerno.getApproveSerno());
            params.put("condType",condType);
            list = lmtApprLoanCondMapper.queryLmtApproveLoanCondTypeByParams(params);
        }
        return list;
    }

    /**
     * 根据审批流水号获取用信条件
     * @param apprSerno
     * @param condType
     * @return
     */
    public ArrayList<LmtApprLoanCond> queryLoanCondByApprSerno(String apprSerno, String condType) {
        ArrayList<LmtApprLoanCond> list = null;
        HashMap<String, String> params = new HashMap<>();
        params.put("apprSerno", apprSerno);
        params.put("condType",condType);
        if(StringUtils.isBlank(apprSerno)){
            return list;
        }
        list = lmtApprLoanCondMapper.queryLmtApproveLoanCondTypeByParams(params);
        return list;
    }

    /**
     * 根据集团审批流水号获取用信条件
     * @param grpApproveSerno
     * @param condType
     * @return
     */
    public ArrayList<Map> queryLoanCondByGrpApprSerno(String grpApproveSerno, String condType) {
        ArrayList<Map> list = new ArrayList<>();
        if(StringUtils.nonBlank(grpApproveSerno)){
            List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpApproveSerno);
            if(CollectionUtils.isEmpty(lmtGrpMemRels)){
                return list;
            }
            for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels){
                HashMap<String, String> params = new HashMap<>();
                params.put("apprSerno", lmtGrpMemRel.getSingleSerno());
                params.put("condType",condType);
                ArrayList<LmtApprLoanCond> singleList = lmtApprLoanCondMapper.queryLmtApproveLoanCondTypeByParams(params);
                if(CollectionUtils.nonEmpty(singleList)){
                    for(LmtApprLoanCond lmtApprLoanCond : singleList){
                        Map map = new HashMap();
                        map.put("pkId",lmtApprLoanCond.getPkId());
                        map.put("approveSerno",lmtApprLoanCond.getApproveSerno());
                        map.put("condType",lmtApprLoanCond.getCondType());
                        map.put("cusId",lmtGrpMemRel.getCusId());
                        map.put("cusName",lmtGrpMemRel.getCusName());
                        map.put("condDesc",lmtApprLoanCond.getCondDesc());
                        list.add(map);
                    }
                }

            }
            return list;
        }else{
            return list;
        }

    }

    /**
     * 根据集团申请流水号查询用信条件信息
     * @param serno
     * @param condType
     * @return
     */
    public ArrayList<Map> queryLoanCondByGrpSerno(String serno, String condType) {
        ArrayList<Map> returnList = new ArrayList<>();
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(serno);
        log.info("集团申请数据:"+ JSON.toJSONString(lmtGrpApp));
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
        log.info("集团申请成员关系数据:"+ JSON.toJSONString(lmtGrpMemRels));
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels){
            ArrayList<LmtApprLoanCond> list = this.queryLoanCond(lmtGrpMemRel.getSingleSerno(),condType);
            log.info("成员客户[{}]项下的最新审批数据下的用信条件数据[{}]",JSON.toJSONString(lmtGrpMemRel.getCusId()),JSON.toJSONString(list));
            for(LmtApprLoanCond lmtApprLoanCond : list){
                HashMap map = new HashMap();
                LmtAppr lmtAppr = lmtApprService.selectByApproveSerno(lmtApprLoanCond.getApproveSerno());
                map.put("pkId",lmtApprLoanCond.getPkId());
                map.put("approveSerno",lmtAppr.getApproveSerno());
                map.put("cusId",lmtAppr.getCusId());
                map.put("cusName",lmtAppr.getCusName());
                map.put("condType",lmtApprLoanCond.getCondType());
                map.put("condDesc",lmtApprLoanCond.getCondDesc());
                returnList.add(map);
            }

        }
        return returnList;
    }

    /**
     * 批量保存
     * @param list
     * @return
     */

    public Integer batchSaveLmtApprLoanCond(List<LmtApprLoanCond> list) {
        int num = 0;
        if(list.size()>0 && !list.isEmpty()){
            for(LmtApprLoanCond lmtApprLoanCond : list){
                // 新增时操作
                lmtApprLoanCond.setApproveSerno(lmtApprLoanCond.getApproveSerno());
                LmtApprLoanCond lmtApprLoanCond1 = this.selectByPrimaryKey(lmtApprLoanCond.getPkId());
                if(lmtApprLoanCond1 != null ){
                    num = lmtApprLoanCondMapper.updateByPrimaryKey(lmtApprLoanCond);
                }else{
                    num = lmtApprLoanCondMapper.insert(lmtApprLoanCond);
                }
            }
        }
        return num;
    }

    /**
     * 根据集团申请流水号查询还款计划信息
     * @param hashMap
     * @return
     */
    public ArrayList<Map> queryRepayPlanByGrpApproveSerno(Map<String,String> hashMap) {
        ArrayList<Map> returnList = new ArrayList<>();
        // 避免传入值为空的情况
        if(StringUtils.isBlank(hashMap.get("grpApproveSerno"))){
            return returnList;
        }
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(hashMap.get("grpApproveSerno"));
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels){
            hashMap.put("apprSerno",lmtGrpMemRel.getSingleSerno());
            List<LmtApprSubPrd> apprSubPrdList = lmtApprSubPrdService.queryLmtApprSubDataByApprSerno(hashMap);
            for(LmtApprSubPrd lmtApprSubPrd : apprSubPrdList){
                List<RepayCapPlan> list = repayCapPlanService.selectBySerno(lmtApprSubPrd.getApprSubPrdSerno());
                for(RepayCapPlan repayCapPlan : list){
                    Map map1 = new HashMap();
                    map1 = cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(repayCapPlan);
                    Map map2 = new HashMap();
                    map2.putAll(map1);
                    map2.put("subPrdSerno",lmtApprSubPrd.getSubPrdSerno());
                    map2.put("lmtBizTypeName",lmtApprSubPrd.getLmtBizTypeName());
                    returnList.add(map2);
                }
            }
        }
        return returnList;
    }
}
