package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrdRateStd
 * @类描述: prd_rate_std数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-07 10:59:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PrdRateStdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 利率类型代码 **/
	private String rateTypeId;
	
	/** 利率类型 **/
	private String rateTypeName;
	
	/** 利率值 **/
	private java.math.BigDecimal rate;
	
	/** 生效日期 **/
	private String validDate;
	
	/** 最小期限 **/
	private Integer termMin;
	
	/** 最大期限 **/
	private Integer termMax;
	
	/** 利率基准 **/
	private String rateDatum;
	
	/** 是否启用 **/
	private String usedInd;
	
	/** 币种 **/
	private String currency;
	
	/** 利率名称 **/
	private String rateName;
	
	
	/**
	 * @param rateTypeId
	 */
	public void setRateTypeId(String rateTypeId) {
		this.rateTypeId = rateTypeId == null ? null : rateTypeId.trim();
	}
	
    /**
     * @return RateTypeId
     */	
	public String getRateTypeId() {
		return this.rateTypeId;
	}
	
	/**
	 * @param rateTypeName
	 */
	public void setRateTypeName(String rateTypeName) {
		this.rateTypeName = rateTypeName == null ? null : rateTypeName.trim();
	}
	
    /**
     * @return RateTypeName
     */	
	public String getRateTypeName() {
		return this.rateTypeName;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return Rate
     */	
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param validDate
	 */
	public void setValidDate(String validDate) {
		this.validDate = validDate == null ? null : validDate.trim();
	}
	
    /**
     * @return ValidDate
     */	
	public String getValidDate() {
		return this.validDate;
	}
	
	/**
	 * @param termMin
	 */
	public void setTermMin(Integer termMin) {
		this.termMin = termMin;
	}
	
    /**
     * @return TermMin
     */	
	public Integer getTermMin() {
		return this.termMin;
	}
	
	/**
	 * @param termMax
	 */
	public void setTermMax(Integer termMax) {
		this.termMax = termMax;
	}
	
    /**
     * @return TermMax
     */	
	public Integer getTermMax() {
		return this.termMax;
	}
	
	/**
	 * @param rateDatum
	 */
	public void setRateDatum(String rateDatum) {
		this.rateDatum = rateDatum == null ? null : rateDatum.trim();
	}
	
    /**
     * @return RateDatum
     */	
	public String getRateDatum() {
		return this.rateDatum;
	}
	
	/**
	 * @param usedInd
	 */
	public void setUsedInd(String usedInd) {
		this.usedInd = usedInd == null ? null : usedInd.trim();
	}
	
    /**
     * @return UsedInd
     */	
	public String getUsedInd() {
		return this.usedInd;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}
	
    /**
     * @return Currency
     */	
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param rateName
	 */
	public void setRateName(String rateName) {
		this.rateName = rateName == null ? null : rateName.trim();
	}
	
    /**
     * @return RateName
     */	
	public String getRateName() {
		return this.rateName;
	}


}