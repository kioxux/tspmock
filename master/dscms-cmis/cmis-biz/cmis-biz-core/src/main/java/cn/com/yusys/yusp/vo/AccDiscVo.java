package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "贴现台账列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccDiscVo {

    /*
  借据编号
   */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    票据种类
     */
    @ExcelField(title = "票据种类", viewLength = 20, dictCode = "STD_DRFT_TYPE")
    private String drftType;

    /*
    汇票号码
     */
    @ExcelField(title = "汇票号码", viewLength = 20)
    private String porderNo;

    /*
    票面金额
     */
    @ExcelField(title = "票面金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal drftAmt;

    /*
    票据签发日
     */
    @ExcelField(title = "票据签发日", viewLength = 20)
    private String draftStartDate;

    /*
    票据到期日
     */
    @ExcelField(title = "票据到期日", viewLength = 20)
    private String draftEndDate;

    /*
    实付贴现金额
     */
    @ExcelField(title = "实付贴现金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal rpayDscntAmt;

    /*
    贴现日期
     */
    @ExcelField(title = "贴现日期", viewLength = 20)
    private String dscntDate;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
     */
    @ExcelField(title = "台账状态", viewLength = 20, dictCode = "STD_ACC_STATUS")
    private String accStatus;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDrftType() {
        return drftType;
    }

    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public BigDecimal getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(BigDecimal drftAmt) {
        this.drftAmt = drftAmt;
    }

    public String getDraftStartDate() {
        return draftStartDate;
    }

    public void setDraftStartDate(String draftStartDate) {
        this.draftStartDate = draftStartDate;
    }

    public String getDraftEndDate() {
        return draftEndDate;
    }

    public void setDraftEndDate(String draftEndDate) {
        this.draftEndDate = draftEndDate;
    }

    public BigDecimal getRpayDscntAmt() {
        return rpayDscntAmt;
    }

    public void setRpayDscntAmt(BigDecimal rpayDscntAmt) {
        this.rpayDscntAmt = rpayDscntAmt;
    }

    public String getDscntDate() {
        return dscntDate;
    }

    public void setDscntDate(String dscntDate) {
        this.dscntDate = dscntDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

}
