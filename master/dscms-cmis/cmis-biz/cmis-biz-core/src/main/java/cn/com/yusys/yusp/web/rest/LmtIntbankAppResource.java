/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import cn.com.yusys.yusp.service.LmtIntbankAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 19:09:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankapp")
public class LmtIntbankAppResource {
    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankApp> list = lmtIntbankAppService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankApp>> index(QueryModel queryModel) {
        List<LmtIntbankApp> list = lmtIntbankAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtIntbankApp>> selectByModel(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" serno desc ");
        }
        List<LmtIntbankApp> list = lmtIntbankAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankApp> show(@PathVariable("pkId") String pkId) {
        LmtIntbankApp lmtIntbankApp = lmtIntbankAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankApp>(lmtIntbankApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankApp> create(@RequestBody LmtIntbankApp lmtIntbankApp) throws URISyntaxException {
        lmtIntbankAppService.insert(lmtIntbankApp);
        return new ResultDto<LmtIntbankApp>(lmtIntbankApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankApp lmtIntbankApp) throws URISyntaxException {
        int result = lmtIntbankAppService.update(lmtIntbankApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtIntbankApp lmtIntbankApp) throws URISyntaxException {
        int result = lmtIntbankAppService.updateSelective(lmtIntbankApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/valilmttype")
    protected ResultDto<Map> valilmttype(@RequestBody QueryModel queryModel) {
        Map rtnData = new HashMap();
        rtnData = lmtIntbankAppService.valilmttype(queryModel);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:onReconside
     * @函数描述:新增单一客户复议申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onreconside")
    protected ResultDto<Map> onReconside(@RequestBody LmtIntbankApp lmtIntbankApp) throws URISyntaxException {
        Map rtnData = new HashMap();
        rtnData= lmtIntbankAppService.onReconside(lmtIntbankApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:addProjectBasicInfo
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addProjectBasicInfo")
    protected ResultDto<Map> addProjectBasicInfo(@RequestBody Map info) throws URISyntaxException {
        Map result = lmtIntbankAppService.addProjectBasicInfo(info);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody QueryModel queryModel ) {
        String pkId = (String) queryModel.getCondition().get("pkId");
        int result = lmtIntbankAppService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:onModify
     * @函数描述:新增客户授信变更申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onmodify")
    protected ResultDto<Map> onModify(@RequestBody Map params) throws URISyntaxException {
        if (StringUtils.isBlank(String.valueOf(params.get("newSerno")))
                || StringUtils.isBlank(String.valueOf(params.get("newPkId")))){
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
        }
        Map rtnData = lmtIntbankAppService.onModify(params);
        return new ResultDto<>(rtnData);
    }


    /**
     * 图片
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtIntbankAppService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:更新主体分析
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateZtfx")
    protected ResultDto<Integer> updateZtfx(@RequestBody LmtIntbankApp lmtIntbankApp) throws URISyntaxException {
        //不更新该字段
        lmtIntbankApp.setOperFinaSituPicturePath(null);
        int result = lmtIntbankAppService.updateZtfx(lmtIntbankApp);
        return new ResultDto<Integer>(result);
    }


}
