package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.dto.BizGuarExchangeDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarInfoCommMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarInfoCommService
 * @类描述: #服务类（押品信息公共功能）
 * @功能描述:
 * @创建人: yangx
 * @创建时间: 2020-12-04 16:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class GuarInfoCommService {
    private static final Logger log = LoggerFactory.getLogger(GuarInfoCommService.class);

    @Autowired
    private GuarInfoCommMapper guarInfoCommMapper;
    // TODO 仰雪龙确认逻辑
    // @Autowired
    // private CmisBizClientService cmisBizClientService;

    public Map getGuarInfo(String tableName, String guarNo) {
        return guarInfoCommMapper.getGuarInfo(tableName, guarNo);
    }

    public boolean checkGuarIsExist(String tableName, String guarNo) {
        Map guarMap = getGuarInfo(tableName, guarNo);
        if (guarMap != null && !guarMap.isEmpty()) {
            log.info("押品信息已经存在，押品统一编号【{}】", guarNo);
            return true;
        } else {
            return false;
        }
    }

    public Map saveGuarDetail(Map map) {
        Map param = new HashMap();
        String guarContNo = (String) map.get("guarContNo");
        guarInfoCommMapper.saveGuarDetail(map);
        if(!StringUtils.isEmpty(guarContNo)){
            BizGuarExchangeDto bizGuarExchangeDto = new BizGuarExchangeDto();
            bizGuarExchangeDto.setGuarContNo(guarContNo);
            bizGuarExchangeDto.setGuarNo((String) map.get("guarNo"));
            log.info("接口请求入参" + JSON.toJSONString(bizGuarExchangeDto));
            log.info("存在性查询新增"+ map.get("guarNo") +"担保合同编号"+guarContNo+"获取业务申请额度数据-获取业务申请额度数据-调用接口开始");
            BizGuarExchangeDto bizGuarExchangeDtos = null ;//  // TODO 仰雪龙确认逻辑 cmisBizClientService.saveGrtGuarContRelInter(bizGuarExchangeDto);
            log.info("存在性查询新增"+ map.get("guarNo") +"担保合同编号"+guarContNo+"获取业务申请额度数据-获取业务申请额度数据-调用接口结束");
            if(bizGuarExchangeDtos == null){
                throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
            }
        }
        param.put("rtnCode", CmisBizConstants.GUAR_CLIENT_RETURN_CODE_SUCCESS);
        return param;
    }
}
