package cn.com.yusys.yusp.web.server.xdcz0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0021.req.Xdcz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.resp.Xdcz0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0021.Xdcz0021Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:支用申请
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0021:支用申请")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0021Resource.class);

    @Autowired
    private Xdcz0021Service xdcz0021Service;
    /**
     * 交易码：xdcz0021
     * 交易描述：支用申请
     *
     * @param xdcz0021DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("支用申请")
    @PostMapping("/xdcz0021")
    protected @ResponseBody
    ResultDto<Xdcz0021DataRespDto>  xdcz0021(@Validated @RequestBody Xdcz0021DataReqDto xdcz0021DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021DataReqDto));
        Xdcz0021DataRespDto  xdcz0021DataRespDto  = new Xdcz0021DataRespDto();// 响应Dto:支用申请
        ResultDto<Xdcz0021DataRespDto>xdcz0021DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0021DataReqDto获取业务值进行业务逻辑处理
            xdcz0021DataRespDto = xdcz0021Service.xdcz0021(xdcz0021DataReqDto);
            // 封装xdcz0021DataResultDto中正确的返回码和返回信息
            String opFlag = xdcz0021DataRespDto.getOpFlag();
            String opMessage = xdcz0021DataRespDto.getOpMsg();
            //如果失败，返回9999
            if("F".equals(opFlag)){//出账失败
                xdcz0021DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0021DataResultDto.setMessage(opMessage);
            }else{
                xdcz0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdcz0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value,e.getMessage());
            // 封装xdcz0021DataResultDto中异常返回码和返回信息
            xdcz0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0021DataRespDto到xdcz0021DataResultDto中
        xdcz0021DataResultDto.setData(xdcz0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021DataRespDto));
        return xdcz0021DataResultDto;
    }
}
