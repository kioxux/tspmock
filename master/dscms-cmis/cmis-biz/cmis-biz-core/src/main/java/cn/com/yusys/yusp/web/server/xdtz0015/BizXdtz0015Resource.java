package cn.com.yusys.yusp.web.server.xdtz0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0015.req.Xdtz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.resp.Xdtz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0015.Xdtz0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:贴现记账结果通知
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "xdtz0015:贴现记账结果通知")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0015Resource.class);

    @Autowired
    private Xdtz0015Service xdtz0015Service;

    /**
     * 交易码：xdtz0015
     * 交易描述：贴现记账结果通知
     *
     * @param xdtz0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贴现记账结果通知")
    @PostMapping("/xdtz0015")
    protected @ResponseBody
    ResultDto<Xdtz0015DataRespDto> xdtz0015(@Validated @RequestBody Xdtz0015DataReqDto xdtz0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataReqDto));
        Xdtz0015DataRespDto xdtz0015DataRespDto = new Xdtz0015DataRespDto();// 响应Dto:贴现记账结果通知
        ResultDto<Xdtz0015DataRespDto> xdtz0015DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataReqDto));
            xdtz0015DataRespDto = xdtz0015Service.getXdtz0015(xdtz0015DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataRespDto));

            // 封装xdtz0015DataResultDto中正确的返回码和返回信息
            xdtz0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, e.getMessage());
            // 封装xdtz0015DataResultDto中异常返回码和返回信息
            xdtz0015DataResultDto.setCode(e.getErrorCode());
            xdtz0015DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, e.getMessage());
            // 封装xdtz0015DataResultDto中异常返回码和返回信息
            xdtz0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0015DataRespDto到xdtz0015DataResultDto中
        xdtz0015DataResultDto.setData(xdtz0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataRespDto));
        return xdtz0015DataResultDto;
    }
}
