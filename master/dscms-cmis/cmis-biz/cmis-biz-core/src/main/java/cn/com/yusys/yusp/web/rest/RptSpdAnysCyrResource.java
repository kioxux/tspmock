/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.RptSpdAnysWtdk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysCyr;
import cn.com.yusys.yusp.service.RptSpdAnysCyrService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysCyrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-21 20:19:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyscyr")
public class RptSpdAnysCyrResource {
    @Autowired
    private RptSpdAnysCyrService rptSpdAnysCyrService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysCyr>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysCyr> list = rptSpdAnysCyrService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysCyr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysCyr>> index(QueryModel queryModel) {
        List<RptSpdAnysCyr> list = rptSpdAnysCyrService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysCyr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysCyr> show(@PathVariable("serno") String serno) {
        RptSpdAnysCyr rptSpdAnysCyr = rptSpdAnysCyrService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysCyr>(rptSpdAnysCyr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysCyr> create(@RequestBody RptSpdAnysCyr rptSpdAnysCyr) throws URISyntaxException {
        rptSpdAnysCyrService.insert(rptSpdAnysCyr);
        return new ResultDto<RptSpdAnysCyr>(rptSpdAnysCyr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysCyr rptSpdAnysCyr) throws URISyntaxException {
        int result = rptSpdAnysCyrService.update(rptSpdAnysCyr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysCyrService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysCyrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysCyr> selectBySerno(@RequestBody String serno) {
        RptSpdAnysCyr rptSpdAnysCyr = rptSpdAnysCyrService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysCyr);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysCyr> save(@RequestBody RptSpdAnysCyr rptSpdAnysCyr) throws URISyntaxException {
        RptSpdAnysCyr rptSpdAnysCyr1 = rptSpdAnysCyrService.selectByPrimaryKey(rptSpdAnysCyr.getSerno());
        if(rptSpdAnysCyr1!=null){
            rptSpdAnysCyrService.update(rptSpdAnysCyr);
        }else{
            rptSpdAnysCyrService.insert(rptSpdAnysCyr);
        }
        return ResultDto.success(rptSpdAnysCyr);
    }
}
