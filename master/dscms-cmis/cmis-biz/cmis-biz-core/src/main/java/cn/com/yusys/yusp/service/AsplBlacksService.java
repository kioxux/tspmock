/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AsplBlacks;
import cn.com.yusys.yusp.repository.mapper.AsplBlacksMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplBlacksService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-23 15:44:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplBlacksService {

    @Autowired
    private AsplBlacksMapper asplBlacksMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplBlacks selectByPrimaryKey(String pkId, String cusId) {
        return asplBlacksMapper.selectByPrimaryKey(pkId, cusId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplBlacks> selectAll(QueryModel model) {
        List<AsplBlacks> records = (List<AsplBlacks>) asplBlacksMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplBlacks> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplBlacks> list = asplBlacksMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplBlacks record) {
        return asplBlacksMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplBlacks record) {
        return asplBlacksMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplBlacks record) {
        return asplBlacksMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplBlacks record) {
        return asplBlacksMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String cusId) {
        return asplBlacksMapper.deleteByPrimaryKey(pkId, cusId);
    }

    /**
     * 拦截名单新增
     * @param asplBlacks
     * @return
     */
    public ResultDto<Map<String,String>> add(AsplBlacks asplBlacks) {
        Map<String,String> reultMap = new HashMap<String, String>();
        String recode = "0000";
        String msg = "拦截名单，新增成功";
        // 新增之前先校验是否存在该用户 该业务的拦截名单
        try{
            if (asplBlacksMapper.checkBlacks(asplBlacks.getCusId(),asplBlacks.getBizType())>0){
                throw BizException.error(null,"9999","该客户下已经存在该业务类型的拦截名单");
            }
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            asplBlacks.setSerno(serno);
            asplBlacks.setPkId(StringUtils.getUUID());
            asplBlacks.setCreateTime(DateUtils.getCurrTimestamp());
            asplBlacks.setUpdateTime(DateUtils.getCurrTimestamp());
            insertSelective(asplBlacks);
        }catch (BizException e){
            recode = e.getErrorCode();
            msg = e.getMessage();
        }finally {
            reultMap.put("recode",recode);
            reultMap.put("msg",msg);
            return new ResultDto<>(reultMap);
        }
    }

    /**
     * 根据流水号查询拦截名单信息
     * @param serno
     * @return
     */
    public AsplBlacks selectBySerno(String serno) {
        return asplBlacksMapper.selectBySerno(serno);
    }

    /**
     * 拦截名单新增校验
     * @param map
     * @return
     */
    public ResultDto<Map<String,String>> checkBlacks(Map<String,String> map) {
        Map<String,String> reultMap = new HashMap<String, String>();
        String recode = "0000";
        String msg = "拦截名单新增校验通过成功";
        try {
            String cusId = map.get("cusId");
            if(StringUtils.isEmpty(cusId)){
                throw BizException.error(null,"9999","客户编号【cusId】为空");
            }
            String bizType = map.get("bizType");
            if(StringUtils.isEmpty(bizType)){
                throw BizException.error(null,"9999","业务类型【bizType】为空");
            }
            if (asplBlacksMapper.checkBlacks(cusId,bizType)>0){
                throw BizException.error(null,"9999","该客户下已经存在该业务类型的拦截名单");
            }
        }catch(BizException e){
            recode = e.getErrorCode();
            msg = e.getMessage();
        } finally {
            reultMap.put("recode",recode);
            reultMap.put("msg",msg);
            return new ResultDto<>(reultMap);
        }
    }

    public ResultDto<Map<String, String>> deleteBySerno(String serno) {
        Map<String,String> reultMap = new HashMap<String, String>();
        String recode = "0000";
        String msg = "拦截名单删除成功";
        try {
            if(StringUtils.isEmpty(serno)){
                throw BizException.error(null,"9999","流水号【serno】为空");
            }
            if (asplBlacksMapper.deleteBySerno(serno)<1){
                throw BizException.error(null,"9999","拦截名单删除失败");
            }
        }catch(BizException e){
            recode = e.getErrorCode();
            msg = e.getMessage();
        } finally {
            reultMap.put("recode",recode);
            reultMap.put("msg",msg);
            return new ResultDto<>(reultMap);
        }
    }

    /**
     * 根据流水号更新
     * @param asplBlacks
     * @return
     */
    public ResultDto<Map<String, String>> updateByserno(AsplBlacks asplBlacks) {
        Map<String,String> reultMap = new HashMap<String, String>();
        String recode = "0000";
        String msg = "拦截名单更新成功";
        try {
            if(StringUtils.isEmpty(asplBlacks.getSerno())){
                throw BizException.error(null,"9999","流水号【serno】为空");
            }
            if (asplBlacksMapper.updateByserno(asplBlacks)<1){
                throw BizException.error(null,"9999","拦截名单更新失败");
            }
        }catch(BizException e){
            recode = e.getErrorCode();
            msg = e.getMessage();
        } finally {
            reultMap.put("recode",recode);
            reultMap.put("msg",msg);
            return new ResultDto<>(reultMap);
        }
    }

}
