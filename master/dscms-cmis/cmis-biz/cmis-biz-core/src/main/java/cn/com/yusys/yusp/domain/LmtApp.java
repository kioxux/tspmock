/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: LmtApp
 * @类描述: lmt_app数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-24 17:03:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app")
public class LmtApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;

	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = false, length = 5)
	private String lmtType;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 原授信流水号 **/
	@Column(name = "OGRIGI_LMT_SERNO", unique = false, nullable = true, length = 40)
	private String ogrigiLmtSerno;

	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;

	/** 原授信期限 **/
	@Column(name = "ORIGI_LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtTerm;

	/** 原授信宽限期 **/
	@Column(name = "ORIGI_LMT_GRAPER_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtGraperTerm;

	/** 原敞口额度合计 **/
	@Column(name = "ORIGI_OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenTotalLmtAmt;

	/** 原低风险额度合计 **/
	@Column(name = "ORIGI_LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLowRiskTotalLmtAmt;

	/** 测算最高流动资金贷款额度 **/
	@Column(name = "EVAL_HIGH_CURFUND_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalHighCurfundLmtAmt;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 敞口额度合计 **/
	@Column(name = "OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openTotalLmtAmt;

	/** 低风险额度合计 **/
	@Column(name = "LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskTotalLmtAmt;

	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;

	/** 授信宽限期 **/
	@Column(name = "LMT_GRAPER_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtGraperTerm;

	/** 调查报告类型 **/
	@Column(name = "RPT_TYPE", unique = false, nullable = true, length = 5)
	private String rptType;

	/** 是否集团授信 **/
	@Column(name = "IS_GRP", unique = false, nullable = false, length = 5)
	private String isGrp;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = false, length = 5)
	private String approveStatus;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 省心快贷风险拦截结果 **/
	@Column(name = "SXKD_RISK_RESULT", unique = false, nullable = false, length = 5)
	private String sxkdRiskResult;

	/** 是否提交自动化审批 **/
	@Column(name = "IS_SUB_AUTO_APPR", unique = false, nullable = false, length = 5)
	private String isSubAutoAppr;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}

	/**
	 * @return lmtType
	 */
	public String getLmtType() {
		return this.lmtType;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	/**
	 * @return cusType
	 */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param ogrigiLmtSerno
	 */
	public void setOgrigiLmtSerno(String ogrigiLmtSerno) {
		this.ogrigiLmtSerno = ogrigiLmtSerno;
	}

	/**
	 * @return ogrigiLmtSerno
	 */
	public String getOgrigiLmtSerno() {
		return this.ogrigiLmtSerno;
	}

	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}

	/**
	 * @return origiLmtReplySerno
	 */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}

	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(Integer origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}

	/**
	 * @return origiLmtTerm
	 */
	public Integer getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}

	/**
	 * @param origiLmtGraperTerm
	 */
	public void setOrigiLmtGraperTerm(Integer origiLmtGraperTerm) {
		this.origiLmtGraperTerm = origiLmtGraperTerm;
	}

	/**
	 * @return origiLmtGraperTerm
	 */
	public Integer getOrigiLmtGraperTerm() {
		return this.origiLmtGraperTerm;
	}

	/**
	 * @param origiOpenTotalLmtAmt
	 */
	public void setOrigiOpenTotalLmtAmt(java.math.BigDecimal origiOpenTotalLmtAmt) {
		this.origiOpenTotalLmtAmt = origiOpenTotalLmtAmt;
	}

	/**
	 * @return origiOpenTotalLmtAmt
	 */
	public java.math.BigDecimal getOrigiOpenTotalLmtAmt() {
		return this.origiOpenTotalLmtAmt;
	}

	/**
	 * @param origiLowRiskTotalLmtAmt
	 */
	public void setOrigiLowRiskTotalLmtAmt(java.math.BigDecimal origiLowRiskTotalLmtAmt) {
		this.origiLowRiskTotalLmtAmt = origiLowRiskTotalLmtAmt;
	}

	/**
	 * @return origiLowRiskTotalLmtAmt
	 */
	public java.math.BigDecimal getOrigiLowRiskTotalLmtAmt() {
		return this.origiLowRiskTotalLmtAmt;
	}

	/**
	 * @param evalHighCurfundLmtAmt
	 */
	public void setEvalHighCurfundLmtAmt(java.math.BigDecimal evalHighCurfundLmtAmt) {
		this.evalHighCurfundLmtAmt = evalHighCurfundLmtAmt;
	}

	/**
	 * @return evalHighCurfundLmtAmt
	 */
	public java.math.BigDecimal getEvalHighCurfundLmtAmt() {
		return this.evalHighCurfundLmtAmt;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param openTotalLmtAmt
	 */
	public void setOpenTotalLmtAmt(java.math.BigDecimal openTotalLmtAmt) {
		this.openTotalLmtAmt = openTotalLmtAmt;
	}

	/**
	 * @return openTotalLmtAmt
	 */
	public java.math.BigDecimal getOpenTotalLmtAmt() {
		return this.openTotalLmtAmt;
	}

	/**
	 * @param lowRiskTotalLmtAmt
	 */
	public void setLowRiskTotalLmtAmt(java.math.BigDecimal lowRiskTotalLmtAmt) {
		this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
	}

	/**
	 * @return lowRiskTotalLmtAmt
	 */
	public java.math.BigDecimal getLowRiskTotalLmtAmt() {
		return this.lowRiskTotalLmtAmt;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	/**
	 * @return lmtTerm
	 */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}

	/**
	 * @param lmtGraperTerm
	 */
	public void setLmtGraperTerm(Integer lmtGraperTerm) {
		this.lmtGraperTerm = lmtGraperTerm;
	}

	/**
	 * @return lmtGraperTerm
	 */
	public Integer getLmtGraperTerm() {
		return this.lmtGraperTerm;
	}

	/**
	 * @param rptType
	 */
	public void setRptType(String rptType) {
		this.rptType = rptType;
	}

	/**
	 * @return rptType
	 */
	public String getRptType() {
		return this.rptType;
	}

	/**
	 * @param isGrp
	 */
	public void setIsGrp(String isGrp) {
		this.isGrp = isGrp;
	}

	/**
	 * @return isGrp
	 */
	public String getIsGrp() {
		return this.isGrp;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param sxkdRiskResult
	 */
	public void setSxkdRiskResult(String sxkdRiskResult) {
		this.sxkdRiskResult = sxkdRiskResult;
	}

	/**
	 * @return sxkdRiskResult
	 */
	public String getSxkdRiskResult() {
		return this.sxkdRiskResult;
	}

	/**
	 * @param isSubAutoAppr
	 */
	public void setIsSubAutoAppr(String isSubAutoAppr) {
		this.isSubAutoAppr = isSubAutoAppr;
	}

	/**
	 * @return isSubAutoAppr
	 */
	public String getIsSubAutoAppr() {
		return this.isSubAutoAppr;
	}


}