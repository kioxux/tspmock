/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAcctSubMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:12:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ToppAcctSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ToppAcctSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ToppAcctSub> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ToppAcctSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ToppAcctSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ToppAcctSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ToppAcctSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    ToppAcctSub queryToppAcctSubByDataParams(HashMap<String, String>  queryMap);

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */

    List<ToppAcctSub> selectBySerno(QueryModel model);

    /**
     * @方法名称: queryToppAcctSubDataByKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    ToppAcctSub queryToppAcctSubDataByKey(@Param("bizSerno") String bizSerno);



    /**
     * @创建人 WH
     * @创建时间 2021/6/3 19:36
     * @注释 查询有无交易对手数据
     */
    List selectByBizId(@Param("pvpSerno")String pvpSerno);

    /**
     * @创建人 YD
     * @创建时间 2021/6/9 20:36
     * @注释 查询对手数据
     */
    List queryToppAcctSubByBillNo(@Param("billNo")String billNo);

    /**
     * @方法名称: selectDistinctAcctByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ToppAcctSub> selectDistinctAcctByModel(QueryModel model);

    /**
     * 根据流水号补充entruPayAcctNo、entruPayAcctName，entruAcctb字段
     * @author zdl
     **/
    @IgnoredDataAuthority
    List<LoanContList> get3EleByIqpSernos(@Param("iqpSernos") List<String> iqpSernos);

}