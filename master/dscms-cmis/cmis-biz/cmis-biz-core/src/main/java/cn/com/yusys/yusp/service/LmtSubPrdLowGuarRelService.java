/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import com.jcraft.jsch.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSubPrdLowGuarRel;
import cn.com.yusys.yusp.repository.mapper.LmtSubPrdLowGuarRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubPrdLowGuarRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 19:22:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSubPrdLowGuarRelService {

    @Autowired
    private LmtSubPrdLowGuarRelMapper lmtSubPrdLowGuarRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSubPrdLowGuarRel selectByPrimaryKey(String pkId, String subPrdSerno) {
        return lmtSubPrdLowGuarRelMapper.selectByPrimaryKey(pkId, subPrdSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSubPrdLowGuarRel> selectAll(QueryModel model) {
        List<LmtSubPrdLowGuarRel> records = (List<LmtSubPrdLowGuarRel>) lmtSubPrdLowGuarRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSubPrdLowGuarRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSubPrdLowGuarRel> list = lmtSubPrdLowGuarRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSubPrdLowGuarRel record) {
        return lmtSubPrdLowGuarRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSubPrdLowGuarRel record) {
        return lmtSubPrdLowGuarRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSubPrdLowGuarRel record) {
        return lmtSubPrdLowGuarRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSubPrdLowGuarRel record) {
        return lmtSubPrdLowGuarRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String subPrdSerno) {
        return lmtSubPrdLowGuarRelMapper.deleteByPrimaryKey(pkId, subPrdSerno);
    }

    public int insertLowGuar(LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel){
        lmtSubPrdLowGuarRel.setPkId(StringUtils.getUUID());
        lmtSubPrdLowGuarRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        User userInfo = SessionUtils.getUserInformation();
        lmtSubPrdLowGuarRel.setInputId(userInfo.getLoginCode());
        lmtSubPrdLowGuarRel.setInputBrId(userInfo.getOrg().getCode());
        lmtSubPrdLowGuarRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtSubPrdLowGuarRel.setUpdId(userInfo.getLoginCode());
        lmtSubPrdLowGuarRel.setUpdBrId(userInfo.getOrg().getCode());
        lmtSubPrdLowGuarRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtSubPrdLowGuarRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtSubPrdLowGuarRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        int count = insertSelective(lmtSubPrdLowGuarRel);
        if(count>0){
            return count;
        }else{
            return 0;
        }
    }

    /**
     * @方法名称: queryLmtSubPrdLowGuarRelListByPrdSerno
     * @方法描述: 根据分项品种编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSubPrdLowGuarRel> queryLmtSubPrdLowGuarRelListByPrdSerno(String subPrdSerno){
        return lmtSubPrdLowGuarRelMapper.queryLmtSubPrdLowGuarRelListByPrdSerno(subPrdSerno);
    }

}
