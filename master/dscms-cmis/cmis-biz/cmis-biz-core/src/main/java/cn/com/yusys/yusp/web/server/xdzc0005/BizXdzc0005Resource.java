package cn.com.yusys.yusp.web.server.xdzc0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0005.req.Xdzc0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.resp.Xdzc0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0005.Xdzc0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池入池接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDZC0005:资产池入池接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0005Resource.class);

    @Autowired
    private Xdzc0005Service xdzc0005Service;
    /**
     * 交易码：xdzc0005
     * 交易描述：资产池入池接口
     *
     * @param xdzc0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池入池接口")
    @PostMapping("/xdzc0005")
    protected @ResponseBody
    ResultDto<Xdzc0005DataRespDto> xdzc0005(@Validated @RequestBody Xdzc0005DataReqDto xdzc0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005DataReqDto));
        Xdzc0005DataRespDto xdzc0005DataRespDto = new Xdzc0005DataRespDto();// 响应Dto:抵质押物明细查询
        ResultDto<Xdzc0005DataRespDto> xdzc0005DataResultDto = new ResultDto<>();
        try {
            xdzc0005DataRespDto = xdzc0005Service.xdzc0005Service(xdzc0005DataReqDto);
            // 封装xdzc0005DataResultDto中正确的返回码和返回信息
            xdzc0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, e.getMessage());
            // 封装xdzc0005DataResultDto中异常返回码和返回信息
            xdzc0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0005DataRespDto到xdzc0005DataResultDto中
        xdzc0005DataResultDto.setData(xdzc0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005DataResultDto));
        return xdzc0005DataResultDto;
    }
}
