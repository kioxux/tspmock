package cn.com.yusys.yusp.service.server.xdtz0053;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusIndivSocialResp;
import cn.com.yusys.yusp.dto.server.xdtz0053.req.Xdtz0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.Xdtz0053DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;


/**
 * 接口处理类:个人社会关系查询
 * @author xs
 * @version 1.0
 * @des
 * 通过借据号查询客户信息。 cusIndivSocial
 */
@Service
public class Xdtz0053Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdtz0053.Xdtz0053Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0053DataRespDto xdtz0053(Xdtz0053DataReqDto xdtz0053DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataReqDto));
        Xdtz0053DataRespDto xdtz0053DataRespDto = new Xdtz0053DataRespDto();
        try {
            String BillNo = xdtz0053DataReqDto.getBillNo();
            if(!"".equals(BillNo)){
                String cusId = accLoanMapper.queryCusIdByBillNo(BillNo);
                //调用cus服务
                java.util.List<CusIndivSocialResp> cusIndivSocialDtoList = cmisCusClientService.selectCusIndivSocialDtoList(cusId).getData();
                java.util.List<CusIndivSocialResp> dtos =JSON.parseObject(JSON.toJSONString(cusIndivSocialDtoList), new TypeReference<java.util.List<CusIndivSocialResp>>(){});
                java.util.List<List> lists = dtos.stream().map(e->{
                    List list = new List();
                    list.setCertNo(e.getCertCode());
                    list.setName(e.getName());
                    list.setMainLoanManRela(e.getIndivCusRel());//与主贷人关系
                    return list;
                }).collect(Collectors.toList());
                xdtz0053DataRespDto.setList(lists);
            }else{
                logger.info("个人社会关系查询,查询参数billNo为空", JSON.toJSONString(xdtz0053DataReqDto));
                return xdtz0053DataRespDto;
            }
            //查询参数
            logger.info("个人社会关系查询,查询参数为:{}", JSON.toJSONString(xdtz0053DataReqDto));
            logger.info("个人社会关系查询,反回参数为:{}", JSON.toJSONString(xdtz0053DataRespDto));
            logger.info("个人社会关系查询,反回参数为:{}", JSON.toJSONString(xdtz0053DataRespDto));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataRespDto));
        return xdtz0053DataRespDto;
    }
}

