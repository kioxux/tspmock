package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncome
 * @类描述: iqp_dis_asset_income数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-05-06 16:33:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDisAssetIncomeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 月收入表主键 **/
	private String incomePk;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	private String apptCode;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 资产折算收入类型 **/
	private String discountAssetType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 客户经理认定收入金额 **/
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	private java.math.BigDecimal headExIncome;
	
	/** 借款人月收入 **/
	private java.math.BigDecimal mearn;
	
	/** 配偶月收入 **/
	private java.math.BigDecimal spouseMearn;
	
	/** 所有共同借款人月收入小计 **/
	private java.math.BigDecimal commonMearn;
	
	/** 数据来源 **/
	private String dataSour;
	
	/** 收入来源 **/
	private String incomeSour;
	
	/** 分析时间段 **/
	private String analyTime;
	
	/** 销售额(元) **/
	private java.math.BigDecimal saleAmt;
	
	/** 推算年销售额(元) **/
	private java.math.BigDecimal yearSaleAmt;
	
	/** 利润率(%) **/
	private java.math.BigDecimal profitRate;
	
	/** 年净利润(元) **/
	private java.math.BigDecimal yearProfit;
	
	/** 小计(元) **/
	private java.math.BigDecimal subtotal;
	
	/** 调查核实情况说明 **/
	private String indgtExpl;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param incomePk
	 */
	public void setIncomePk(String incomePk) {
		this.incomePk = incomePk == null ? null : incomePk.trim();
	}
	
    /**
     * @return IncomePk
     */	
	public String getIncomePk() {
		return this.incomePk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param discountAssetType
	 */
	public void setDiscountAssetType(String discountAssetType) {
		this.discountAssetType = discountAssetType == null ? null : discountAssetType.trim();
	}
	
    /**
     * @return DiscountAssetType
     */	
	public String getDiscountAssetType() {
		return this.discountAssetType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return ManagerIncome
     */	
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return BranchExIncome
     */	
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return HeadExIncome
     */	
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param mearn
	 */
	public void setMearn(java.math.BigDecimal mearn) {
		this.mearn = mearn;
	}
	
    /**
     * @return Mearn
     */	
	public java.math.BigDecimal getMearn() {
		return this.mearn;
	}
	
	/**
	 * @param spouseMearn
	 */
	public void setSpouseMearn(java.math.BigDecimal spouseMearn) {
		this.spouseMearn = spouseMearn;
	}
	
    /**
     * @return SpouseMearn
     */	
	public java.math.BigDecimal getSpouseMearn() {
		return this.spouseMearn;
	}
	
	/**
	 * @param commonMearn
	 */
	public void setCommonMearn(java.math.BigDecimal commonMearn) {
		this.commonMearn = commonMearn;
	}
	
    /**
     * @return CommonMearn
     */	
	public java.math.BigDecimal getCommonMearn() {
		return this.commonMearn;
	}
	
	/**
	 * @param dataSour
	 */
	public void setDataSour(String dataSour) {
		this.dataSour = dataSour == null ? null : dataSour.trim();
	}
	
    /**
     * @return DataSour
     */	
	public String getDataSour() {
		return this.dataSour;
	}
	
	/**
	 * @param incomeSour
	 */
	public void setIncomeSour(String incomeSour) {
		this.incomeSour = incomeSour == null ? null : incomeSour.trim();
	}
	
    /**
     * @return IncomeSour
     */	
	public String getIncomeSour() {
		return this.incomeSour;
	}
	
	/**
	 * @param analyTime
	 */
	public void setAnalyTime(String analyTime) {
		this.analyTime = analyTime == null ? null : analyTime.trim();
	}
	
    /**
     * @return AnalyTime
     */	
	public String getAnalyTime() {
		return this.analyTime;
	}
	
	/**
	 * @param saleAmt
	 */
	public void setSaleAmt(java.math.BigDecimal saleAmt) {
		this.saleAmt = saleAmt;
	}
	
    /**
     * @return SaleAmt
     */	
	public java.math.BigDecimal getSaleAmt() {
		return this.saleAmt;
	}
	
	/**
	 * @param yearSaleAmt
	 */
	public void setYearSaleAmt(java.math.BigDecimal yearSaleAmt) {
		this.yearSaleAmt = yearSaleAmt;
	}
	
    /**
     * @return YearSaleAmt
     */	
	public java.math.BigDecimal getYearSaleAmt() {
		return this.yearSaleAmt;
	}
	
	/**
	 * @param profitRate
	 */
	public void setProfitRate(java.math.BigDecimal profitRate) {
		this.profitRate = profitRate;
	}
	
    /**
     * @return ProfitRate
     */	
	public java.math.BigDecimal getProfitRate() {
		return this.profitRate;
	}
	
	/**
	 * @param yearProfit
	 */
	public void setYearProfit(java.math.BigDecimal yearProfit) {
		this.yearProfit = yearProfit;
	}
	
    /**
     * @return YearProfit
     */	
	public java.math.BigDecimal getYearProfit() {
		return this.yearProfit;
	}
	
	/**
	 * @param subtotal
	 */
	public void setSubtotal(java.math.BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
    /**
     * @return Subtotal
     */	
	public java.math.BigDecimal getSubtotal() {
		return this.subtotal;
	}
	
	/**
	 * @param indgtExpl
	 */
	public void setIndgtExpl(String indgtExpl) {
		this.indgtExpl = indgtExpl == null ? null : indgtExpl.trim();
	}
	
    /**
     * @return IndgtExpl
     */	
	public String getIndgtExpl() {
		return this.indgtExpl;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}