/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AsplBlacks;
import cn.com.yusys.yusp.service.AsplBlacksService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplBlacksResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-23 15:44:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/asplblacks")
public class AsplBlacksResource {
    @Autowired
    private AsplBlacksService asplBlacksService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplBlacks>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplBlacks> list = asplBlacksService.selectAll(queryModel);
        return new ResultDto<List<AsplBlacks>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplBlacks>> index(QueryModel queryModel) {
        List<AsplBlacks> list = asplBlacksService.selectByModel(queryModel);
        return new ResultDto<List<AsplBlacks>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<AsplBlacks>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<AsplBlacks> list = asplBlacksService.selectByModel(queryModel);
        return new ResultDto<List<AsplBlacks>>(list);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplBlacks> create(@RequestBody AsplBlacks asplBlacks) throws URISyntaxException {
        asplBlacks.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 登记日期
        asplBlacks.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 最近修改日期
        asplBlacks.setCreateTime(DateUtils.getCurrDate());// 创建时间
        asplBlacksService.insert(asplBlacks);
        return new ResultDto<AsplBlacks>(asplBlacks);
    }
    /**
     * @函数名称:add
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/add")
    protected ResultDto<Map<String,String>> add(@RequestBody AsplBlacks asplBlacks) throws URISyntaxException {
        return asplBlacksService.add(asplBlacks);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplBlacks asplBlacks) throws URISyntaxException {
        int result = asplBlacksService.update(asplBlacks);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String cusId) {
        int result = asplBlacksService.deleteByPrimaryKey(pkId, cusId);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno/{serno}")
    protected ResultDto<AsplBlacks> selectBySerno(@PathVariable("serno") String serno) {
        AsplBlacks asplBlacks = asplBlacksService.selectBySerno(serno);
        return new ResultDto<AsplBlacks>(asplBlacks);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkBlacks")
    protected ResultDto<Map<String,String>> checkBlacks(@RequestBody  Map<String,String> map) {
        return asplBlacksService.checkBlacks(map);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteBySerno/{serno}")
    protected ResultDto<Map<String,String>> deleteBySerno(@PathVariable("serno") String serno) {
        return asplBlacksService.deleteBySerno(serno);
    }

    /**
     * @函数名称:updateByserno
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateByserno")
    protected ResultDto<Map<String,String>> updateByserno(@RequestBody AsplBlacks asplBlacks) {
        return asplBlacksService.updateByserno(asplBlacks);
    }
}
