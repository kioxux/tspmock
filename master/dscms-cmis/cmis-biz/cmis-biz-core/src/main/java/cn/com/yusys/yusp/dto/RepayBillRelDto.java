package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.math.BigDecimal;

public class RepayBillRelDto {


    /** 出账流水号 **/
    @Column(name = "SERNO", unique = false, nullable = false, length = 40)
    private String serno;

    /** 借据编号 **/
    @Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
    private String billNo;

    /** 合同编号 **/
    @Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
    private String contNo;

    /** 担保方式 **/
    @Column(name = "GUAR_MODE", unique = false, nullable = false, length = 5)
    private String guarMode;

    /** 产品名称 **/
    @Column(name = "PRD_NAME", unique = false, nullable = false, length = 80)
    private String prdName;

    /** 贷款发放币种 **/
    @Column(name = "CONT_CUR_TYPE", unique = false, nullable = false, length = 5)
    private String contCurType;

    /** 贷款金额 **/
    @Column(name = "LOAN_AMT", unique = false, nullable = false, length = 16)
    private java.math.BigDecimal loanAmt;

    /** 贷款余额 **/
    @Column(name = "LOAN_BAL", unique = false, nullable = false, length = 16)
    private java.math.BigDecimal loanBal;

    /** 贷款起始日 **/
    @Column(name = "LOAN_START_DATE", unique = false, nullable = false, length = 20)
    private String loanStartDate;

    /** 贷款到期日 **/
    @Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
    private String loanEndDate;

    /** 台账状态 **/
    @Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
    private String accStatus;

    public RepayBillRelDto(String serno,String billNo, String contNo, String guarMode, String prdName, String contCurType, BigDecimal loanAmt, BigDecimal loanBal, String loanStartDate, String loanEndDate, String accStatus) {
        this.serno = serno;
        this.billNo = billNo;
        this.contNo = contNo;
        this.guarMode = guarMode;
        this.prdName = prdName;
        this.contCurType = contCurType;
        this.loanAmt = loanAmt;
        this.loanBal = loanBal;
        this.loanStartDate = loanStartDate;
        this.loanEndDate = loanEndDate;
        this.accStatus = accStatus;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}