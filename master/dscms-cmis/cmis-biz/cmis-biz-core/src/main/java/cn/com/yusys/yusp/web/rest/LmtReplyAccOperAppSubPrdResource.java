/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSubPrd;
import cn.com.yusys.yusp.service.LmtReplyAccOperAppSubPrdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppSubPrdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:30:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyaccoperappsubprd")
public class LmtReplyAccOperAppSubPrdResource {
    @Autowired
    private LmtReplyAccOperAppSubPrdService lmtReplyAccOperAppSubPrdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAccOperAppSubPrd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAccOperAppSubPrd> list = lmtReplyAccOperAppSubPrdService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAccOperAppSubPrd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAccOperAppSubPrd>> index(QueryModel queryModel) {
        List<LmtReplyAccOperAppSubPrd> list = lmtReplyAccOperAppSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAccOperAppSubPrd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAccOperAppSubPrd> show(@PathVariable("pkId") String pkId) {
        LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd = lmtReplyAccOperAppSubPrdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAccOperAppSubPrd>(lmtReplyAccOperAppSubPrd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAccOperAppSubPrd> create(@RequestBody LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd) throws URISyntaxException {
        lmtReplyAccOperAppSubPrdService.insert(lmtReplyAccOperAppSubPrd);
        return new ResultDto<LmtReplyAccOperAppSubPrd>(lmtReplyAccOperAppSubPrd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd) throws URISyntaxException {
        int result = lmtReplyAccOperAppSubPrdService.updateSelective(lmtReplyAccOperAppSubPrd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccOperAppSubPrdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccOperAppSubPrdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatesubprd")
    protected ResultDto<Integer> updateSubPrd(@RequestBody LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd) throws URISyntaxException {
        int result = lmtReplyAccOperAppSubPrdService.updateSubPrd(lmtReplyAccOperAppSubPrd);
        return new ResultDto<Integer>(result);
    }

}
