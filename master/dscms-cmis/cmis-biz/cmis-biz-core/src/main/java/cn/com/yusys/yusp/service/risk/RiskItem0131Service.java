package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class RiskItem0131Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0131Service.class);

    @Autowired
    private RptSpdAnysYsdService rptSpdAnysYsdService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    /**
     * @方法名称: riskItem0131
     * @方法描述: 优税贷评分卡校验
     * @参数与返回说明:
     * @算法描述:
     * 1、评分低于70分 拦截；
     * 2、纳税评级小于B，且上年度纳税总额小于10万元 是则拦截
     * 3、存在工商，税务，银行等不良记录 是则拦截
     * 4、银行家数超过3家或资产负债率超过75%  是则拦截
     * 5、企业或法人及其家庭没有在业务申办行本地有住宅，商铺，厂房等实物资产 是则拦截
     * 6、企业及实际控制人他行信用贷款余额，我行信用贷款余额，信用卡用信额，优税贷申请额(含小贷)合计是否高于500万元  是则拦截
     * 7、实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等 是则拦截
     * 8、企业持续经营时间<=3年  是则拦截
     * @创建人: zhangliang15
     * @创建时间: 2021-10-19 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0131(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("优税贷评分卡校验开始*******************业务流水号：【{}】，客户号：【{}】",serno,cusId);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //获取客户信息失败
            return riskResultDto;
        }

        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        paramsSub.put("lmtAmt", "0");
        List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        boolean isYsdExitFlag = false; //判断是否存在特色产品：结息贷
        for (LmtAppSub lmtAppSub : subList){
            Map paramsSubPrd = new HashMap();
            // 获取分项产品明细
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            for (LmtAppSubPrd lmtAppSubPrd : subListPrd){
                //优税贷：P014
                if (Objects.equals("P014", lmtAppSubPrd.getLmtBizTypeProp())) {
                    isYsdExitFlag = true;
                    break;
                } else {
                    isYsdExitFlag = false;
                }
            }
            if(isYsdExitFlag) {
                break;
            }
        }
        // 判断若为特色产品：结息贷，则进行后续校验。
        if(isYsdExitFlag) {
            //  1、评分低于70分 拦截
            // 获取调查报告特定产品分析结息贷信息
            RptSpdAnysYsd rptSpdAnysYsd = rptSpdAnysYsdService.selectByPrimaryKey(serno);
            if (Objects.isNull(rptSpdAnysYsd)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013101);
                return riskResultDto;
            }

            // 评分卡分数未满70分不予准入
            if (70 > (Optional.ofNullable(rptSpdAnysYsd.getPfkYsd1Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd2Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd3Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd4Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd5Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd6Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd7Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd8Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd9Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd10Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd11Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd12Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd13Grade1()).orElse(0)
                    + Optional.ofNullable(rptSpdAnysYsd.getPfkYsd14Grade1()).orElse(0))) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013102);
                return riskResultDto;
            }

            // 2、纳税评级小于B，STD_TAX_GRADE，且上年度纳税总额小于10万元 是则拦截
            if(rptSpdAnysYsd.getTaxRate().compareTo("B") > 0 && rptSpdAnysYsd.getLastYearTaxTotalAmt().compareTo(new BigDecimal(100000)) < 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013103);
                return riskResultDto;
            }
            // 3、存在工商，税务，银行等不良记录 是则拦截
            if (!Objects.equals("0", rptSpdAnysYsd.getFocusYsd5())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013104);
                return riskResultDto;
            }
            // 4、银行家数超过3家或资产负债率超过75%  是则拦截
            if (!Objects.equals("0", rptSpdAnysYsd.getFocusYsd12())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013105);
                return riskResultDto;
            }
            // 5、企业或法人及其家庭有在业务申办行本地有住宅，商铺，厂房等实物资产 否则拦截
            if (!Objects.equals("1", rptSpdAnysYsd.getFocusYsd6())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013106);
                return riskResultDto;
            }
            // 6、企业及实际控制人他行信用贷款余额，我行信用贷款余额，信用卡用信额，优税贷申请额(含小贷)合计是否高于500万元
            if (!Objects.equals("0", rptSpdAnysYsd.getFocusYsd13())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013108);
                return riskResultDto;
            }
            // 7、实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等 是则拦截
            if (!Objects.equals("0", rptSpdAnysYsd.getFocusYsd2())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013109);
                return riskResultDto;
            }
            // 8、企业持续经营时间<=3年  是则拦截
            //查询对公客户基本信息
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
            if (Objects.isNull(cusCorpDtoResultDto) || Objects.isNull(cusCorpDtoResultDto.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840701);
                return riskResultDto;
            }
            CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
            // 判断公司成立日期是否为空
            if (StringUtils.isBlank(cusCorpDto.getBuildDate())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0840702);
                return riskResultDto;
            }
            // 获取公司成立日期
            Date buildDate = DateUtils.parseDate(cusCorpDto.getBuildDate(), "yyyy-MM-dd");
            log.info("结息贷评分卡校验*******************公司成立日期：【{}】", buildDate);

            // 若公司成立日期小于等于三年，则拦截
            if (DateUtils.compare(DateUtils.addMonth(buildDate, 36), DateUtils.getCurrDate()) >= 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_013107);
                return riskResultDto;
            }
        }
        log.info("优税贷评分卡校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}