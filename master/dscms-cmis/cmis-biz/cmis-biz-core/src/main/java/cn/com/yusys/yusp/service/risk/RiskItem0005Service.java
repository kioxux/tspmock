package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 借款人绿色信用等级校验（表待建）
 */

@Service
public class RiskItem0005Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0005Service.class);

    /**
     * @方法名称: riskItem0005
     * @方法描述: 借款人绿色信用等级校验
     * @参数与返回说明:
     * @算法描述:
     * 若借款人绿色信用等级是红色或黑色时，则拦截，并给出提示"请联系公司金融总部" 绿色信贷工作人员
     * --解释说明：信贷系统绿色信用等级管理表
     * @创建人: 刘奇
     * @创建时间: 2021-06-29
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0005(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        QueryModel queryModel = new QueryModel();
        //借款人绿色信用等级是红色或黑色时(false)
        boolean b = false;
        if (queryModel!=null){
            if (!b){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            }else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0501); //请联系公司金融总部
                return riskResultDto;
            }
        }
        return riskResultDto;
    }
}
