/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApprReport
 * @类描述: iqp_appr_report数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:17:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appr_report")
public class IqpApprReport extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 发放方式 STD_ZB_GRANT_TYPE **/
	@Column(name = "GRANT_TYPE", unique = false, nullable = true, length = 5)
	private String grantType;
	
	/** 放款模式 STD_ZB_LOAN_MODE **/
	@Column(name = "LOAN_MODE", unique = false, nullable = true, length = 5)
	private String loanMode;
	
	/** 支付方式 STD_ZB_PAT_TYPE **/
	@Column(name = "PAT_TYPE", unique = false, nullable = true, length = 5)
	private String patType;
	
	/** 贷款审核方式 STD_ZB_LOAN_CHECK_TYPE **/
	@Column(name = "LOAN_CHK_TYP", unique = false, nullable = true, length = 5)
	private String loanChkTyp;
	
	/** 核准意见 STD_ZB_APPRV_OPT **/
	@Column(name = "APPRV_OPT", unique = false, nullable = true, length = 5)
	private String apprvOpt;
	
	/** 具体核准意见  **/
	@Column(name = "APPRV_OPT_DESC", unique = false, nullable = true, length = 500)
	private String apprvOptDesc;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param grantType
	 */
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	
    /**
     * @return grantType
     */
	public String getGrantType() {
		return this.grantType;
	}
	
	/**
	 * @param loanMode
	 */
	public void setLoanMode(String loanMode) {
		this.loanMode = loanMode;
	}
	
    /**
     * @return loanMode
     */
	public String getLoanMode() {
		return this.loanMode;
	}
	
	/**
	 * @param patType
	 */
	public void setPatType(String patType) {
		this.patType = patType;
	}
	
    /**
     * @return patType
     */
	public String getPatType() {
		return this.patType;
	}
	
	/**
	 * @param loanChkTyp
	 */
	public void setLoanChkTyp(String loanChkTyp) {
		this.loanChkTyp = loanChkTyp;
	}
	
    /**
     * @return loanChkTyp
     */
	public String getLoanChkTyp() {
		return this.loanChkTyp;
	}
	
	/**
	 * @param apprvOpt
	 */
	public void setApprvOpt(String apprvOpt) {
		this.apprvOpt = apprvOpt;
	}
	
    /**
     * @return apprvOpt
     */
	public String getApprvOpt() {
		return this.apprvOpt;
	}
	
	/**
	 * @param apprvOptDesc
	 */
	public void setApprvOptDesc(String apprvOptDesc) {
		this.apprvOptDesc = apprvOptDesc;
	}
	
    /**
     * @return apprvOptDesc
     */
	public String getApprvOptDesc() {
		return this.apprvOptDesc;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}