/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CentralFileInfo;
import cn.com.yusys.yusp.domain.CentralFileOptRecord;
import cn.com.yusys.yusp.domain.CentralFileTask;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.*;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultNodeDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.CentralFileInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CentralFileTaskMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CentralFileVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileTaskService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-22 15:19:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CentralFileTaskService {

    @Autowired
    private CentralFileTaskMapper centralFileTaskMapper;
    @Autowired
    private CentralFileInfoService centralFileInfoService;
    @Autowired
    private CentralFileOptRecordService centralFileOptRecordService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;
    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;

    private static final Logger log = LoggerFactory.getLogger(CentralFileTaskService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CentralFileTask selectByPrimaryKey(String taskNo) {
        return centralFileTaskMapper.selectByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CentralFileTask> selectAll(QueryModel model) {
        List<CentralFileTask> records = (List<CentralFileTask>) centralFileTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CentralFileTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CentralFileTask> list = centralFileTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CentralFileTask record) {
        return centralFileTaskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(CentralFileTaskDto recordDto) {
        CentralFileTask centralFileTask = new CentralFileTask();
        BeanUtils.copyProperties(recordDto, centralFileTask);

        int rest = 0;

        Map map =  new HashMap<>();
        map.put("org",centralFileTask.getInputBrId());
        //判断是否是异步机构
        ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(centralFileTask.getInputBrId());
        AdminSmOrgDto adminSmOrgDto = resultDto.getData();
        //        0-总行部室
        //        1-异地支行（有分行）
        //        2-异地支行（无分行）
        //        3-异地分行
        //        4-中心支行
        //        5-综合支行
        //        6-对公支行
        //        7-零售支行
        //        8-小额贷款管理部
        //        9-小贷分中心
        //        A-村镇银行
        if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())
        || "8".equals(adminSmOrgDto.getOrgType()) || "9".equals(adminSmOrgDto.getOrgType()) || "A".equals(adminSmOrgDto.getOrgType())){
            log.info("异地机构无法生成档案池任务");
            // 不要异常抛出
            //throw BizException.error(null, EcbEnum.E_CentralFileTask_YD.key, EcbEnum.E_CentralFileTask_YD.value);
        } else {
            centralFileTask.setTaskNo(sequenceTemplateClient.getSequenceTemplate(SeqConstant.FILE_TASK_NO, map));
            centralFileTask.setTaskStatus("01"); //待领取
            centralFileTask.setTaskStartTime(new Date());
            centralFileTask.setInputDate(DateUtils.getCurrDateStr());
            // 登记人相关字段不赋值，仅更新时才开始赋值
            centralFileTask.setUpdId(" ");
            centralFileTask.setUpdBrId(" ");
            centralFileTask.setUpdDate(" ");
            rest = centralFileTaskMapper.insertSelective(centralFileTask);
        }
        /**BG101TODO消息发送**/
        //如果档案任务池中有流水号一致的数据，更新档案任务池中的traceId
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",centralFileTask.getSerno());
        //queryModel.addCondition("bizType",centralFileTask.getBizType());
        List<CentralFileTask> centralFileTaskList = centralFileTaskMapper.selectByModel(queryModel1);
        if(centralFileTaskList!=null && centralFileTaskList.size() >0){//大于0,临时档案档案任务池存在信息
            for (int i = 0; i < centralFileTaskList.size(); i++) {
                CentralFileTask tmp = centralFileTaskList.get(i);
                tmp.setTraceId(centralFileTask.getTraceId());
                centralFileTaskMapper.updateByPrimaryKeySelective(tmp);
            }
        }

        //如果档案台账中有流水号+业务类型一致的数据，更新档案台账的traceId
        QueryModel queryModel2 = new QueryModel();
        queryModel2.addCondition("serno",centralFileTask.getSerno());
        //queryModel.addCondition("bizType",centralFileTask.getBizType());
        List<CentralFileInfo> centralFileInfoList = centralFileInfoService.selectByModel(queryModel2);
        if(centralFileInfoList!=null && centralFileInfoList.size() >0){//大于0,临时档案台账存在信息
            for (int i = 0; i < centralFileInfoList.size(); i++) {
                CentralFileInfo centralFileInfo = centralFileInfoList.get(i);
                centralFileInfo.setTraceId(centralFileTask.getTraceId());
                centralFileInfoService.updateSelective(centralFileInfo);
            }
        }

        if("3".equals(centralFileTask.getTaskUrgentFlag())){ // 如果为系统加急，同步在加急任务中生成一条记录
            TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
            taskUrgentAppDto.setBizType(centralFileTask.getBizType());
            taskUrgentAppDto.setCusId(centralFileTask.getCusId());
            taskUrgentAppDto.setCusName(centralFileTask.getCusName());
            taskUrgentAppDto.setSerno(centralFileTask.getSerno());
            taskUrgentAppDto.setPwbrSerno(centralFileTask.getInstanceId());
            taskUrgentAppDto.setUrgentType(centralFileTask.getTaskUrgentFlag());
            taskUrgentAppDto.setManagerBrId(recordDto.getInputBrId());
            taskUrgentAppDto.setManagerId(recordDto.getInputId());
            taskUrgentAppDto.setInputBrId(recordDto.getInputBrId());
            taskUrgentAppDto.setInputId(recordDto.getInputId());
            taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
            taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
            taskUrgentAppDto.setUrgentResn("系统加急");
            ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
            if(!"0".equals(result.getCode())){
                log.error("同步新增档案任务的系统加急记录失败！"+centralFileTask.getSerno());
            }
        }
        return rest;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CentralFileTask record) {
        return centralFileTaskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateSelective(CentralFileTask centralFileTask) {
        centralFileTask.setUpdId(SessionUtils.getLoginCode());
        centralFileTask.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
        centralFileTask.setUpdateTime(new Date());
        centralFileTask.setUpdDate(DateUtils.getCurrDateTimeStr());
        return centralFileTaskMapper.updateByPrimaryKeySelective(centralFileTask);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskNo) {
        return centralFileTaskMapper.deleteByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return centralFileTaskMapper.deleteByIds(ids);
    }

    /***
     * 批量领取与分配
     * @param lists 列表对象
     * @return
     * @author tangxun
     * @date 2021/5/7 11:58 下午
     * @version 1.0.0
     * @desc 并行流处理列表对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int batchreceive(List<CentralFileTask> lists) {
        int result = 0;
        if(CollectionUtils.nonEmpty(lists)){
            for (CentralFileTask item: lists) {
                //1、【领取】
                if ("02".equals(item.getTaskStatus())) {
                    // 1.1 instanceid不为空，在流程中，去项目池中领取
                    if (Objects.nonNull(item.getInstanceId()) && !"".equals(item.getInstanceId())) {
                        // update by liucheng3  暂不去工作流的项目池中认领该待办；待派发提交的时候再去领取然后提交；
                        // 避免集中作业待处理和我的待办中同时出现同一待办任务
                        // signTaskPool(item); //有审批流的，调用工作流接口从项目池领取
                    }
                    // 1.2 // 【接收】类型的，直接更改任务状态为已完成
                    if("01".equals(item.getTaskType())) {
                        item.setTaskStatus("04"); // 已完成
                    }
                }
                int i = updateSelective(item);
                result = result + i;
            }
        }
        return result;
    }

    /***
     * 审批池批量领取与分配
     * @param lists 列表对象
     * @return
     * @author wzy
     * @date 2021/5/7 11:58 下午
     * @version 1.0.0
     * @desc 并行流处理列表对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int commitNextNode(List<CentralFileTask> lists) {
        int result = 0;
        if(CollectionUtils.nonEmpty(lists)){
            for (CentralFileTask item: lists) {
                //1、【领取】
                if ("02".equals(item.getTaskStatus())) {
                    // 1.1 instanceid不为空，在流程中，去项目池中领取
                    if (Objects.nonNull(item.getInstanceId()) && !"".equals(item.getInstanceId())) {
                        // update by liucheng3  暂不去工作流的项目池中认领该待办；待派发提交的时候再去领取然后提交；
                        // 避免集中作业待处理和我的待办中同时出现同一待办任务
                        signXYKTaskPool(item); //有审批流的，调用工作流接口从项目池领取
                    }
                }

            }
        }
        return result;
    }
    /***
     * 批量废止
     * @param lists 列表对象
     * @return
     * @author tangxun
     * @date 2021/5/7 11:58 下午
     * @version 1.0.0
     * @desc 并行流处理列表对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int batchabolish(List<CentralFileTask> lists) {
        int result = 0;
        if (CollectionUtils.nonEmpty(lists)) {
            for (CentralFileTask item : lists) {
                // 流程审批中的，直接将流程否决掉
                String taskType = item.getTaskType();
                //03-派发、04-暂存及派发的否决流程，02-暂存的只需要更新任务状态
                if(StringUtils.nonEmpty(item.getInstanceId()) && ("03".equals(taskType) || "04".equals(taskType))) {
                    ResultDto<ResultInstanceDto> instanceResultDto = workflowCoreClient.instanceInfo(item.getInstanceId(), item.getNodeId(), null);
                    if(!"0".equals(instanceResultDto.getCode()) || instanceResultDto.getData() == null){
                        log.error("未获取到流程实例信息,任务编号:"+item.getTaskNo());
                    }else{
                        // 4.1 签收流程
                        signTaskPool(item);
                        // 4.2 提交流程
                        WFRefuseDto wFRefuseDto = new WFRefuseDto();
                        WFCommentDto wfCommentDto = new WFCommentDto();
                        wfCommentDto.setInstanceId(item.getInstanceId());
                        wfCommentDto.setNodeId(item.getNodeId());
                        wfCommentDto.setUserId(SessionUtils.getLoginCode());
                        wfCommentDto.setCommentSign(OpType.REFUSE);
                        wfCommentDto.setUserComment(item.getCancelResn());
                        wFRefuseDto.setComment(wfCommentDto);
                        wFRefuseDto.setOrgId(SessionUtils.getUserOrganizationId());
                        ResultDto<ResultMessageDto> wfresult = workflowCoreClient.refuse(wFRefuseDto);
                        if(!"0".equals(wfresult.getCode()) && !"0".equals(wfresult.getData().getCode())){
                            // 否决失败
                            throw BizException.error(null, "999999", "档案任务【{}】废止时，同步执行流程【{}】否决失败！失败原因：{}", item.getTaskNo(), item.getInstanceId(), wfresult.getData().getTip());
                        }else{
                            log.info("档案任务【{}】废止时，同步执行流程【{}】否决成功！", item.getTaskNo(), item.getInstanceId());
                        }
                    }
                 }
                // 更新任务状态
                int i = updateSelective(item);
                result = result + i;
            }
        }
        return result;
    }

    /**
     * 暂存、派发、暂存及派发  提交处理
     * @param centralFileVo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String savecommit(CentralFileVo centralFileVo){
        CentralFileTask centralFileTask = centralFileVo.getCentralFileTask();
        CentralFileInfo centralFileInfo = centralFileVo.getCentralFileInfo();
        if(Objects.nonNull(centralFileTask)){

            /**
             * 1、【纯指令】
             * 目前只有【领取】(领取不进此逻辑)和【派发】两种；不需要临时库位号，也不会回收资料，不记台账；直接更改任务状态
             */
            String optType = centralFileTask.getOptType(); // 操作类型
            String taskType = centralFileTask.getTaskType();
//            if("01".equals(optType)){
//
//            }

            /**
             * 2、【非纯指令】的【暂存】、【派发】、【暂存及派发】
             */
            if("02".equals(optType)){
                // 2.1 不同任务类型，设置档案状态
                //临时档案任务类型=【暂存】：档案暂存成功，状态【已暂存】。
                if ("02".equals(taskType)) {
                    centralFileInfo.setAccStatus("01"); // 已暂存
                }
                //临时档案任务类型=【派发】且操作类型=非纯指令：档案派发成功，更新被派发的档案台账为状态【已派发】。
                else if ("03".equals(taskType)) {
                    centralFileInfo.setAccStatus("02"); // 已派发
                }
                //临时档案任务类型=【暂存及派发】：档案暂存成功，状态【已派发】。
                else if ("04".equals(taskType)) {
                    centralFileInfo.setAccStatus("02"); // 已派发
                }
                // 2.2 判断档案编号是否已存在在台账中，
                String fileNo = centralFileInfo.getFileNo();
                CentralFileInfo oldCentralFileInfo = centralFileInfoService.selectByPrimaryKey(fileNo);
                boolean isMerge = Objects.nonNull(oldCentralFileInfo);

                if(isMerge){
                    // 2.2.1 是：说明是合并或者更新操作,合并后的档案台账状态同步更新
                    oldCentralFileInfo.setAccStatus(centralFileInfo.getAccStatus());
                    centralFileInfoService.updateSelective(oldCentralFileInfo);
                }else{
                    // 2.2.2 否：新增一条暂存台账
                    Integer tempLocationNO = centralFileInfoService.queryTempLocationNo(); // 临时库位号 :“是否与现有库位合并=否”：提交成功时，自动赋值
                    centralFileInfo.setTempLocationNo(String.valueOf(tempLocationNO));
                    centralFileInfo.setFileReceiverDate(DateUtils.formatDateByDef(centralFileTask.getTaskStartTime())); // 档案接收日期=任务生成日期
                    centralFileInfoService.insert(centralFileInfo);
                    // 2.2.3 新增一条档案台账的同时生成一条【接收】的操作记录
                    CentralFileOptRecord centralFileOptRecord = new CentralFileOptRecord();
                    centralFileOptRecord.setFileNo(centralFileInfo.getFileNo());
                    centralFileOptRecord.setOptUsr(centralFileInfo.getReceiverId());
                    centralFileOptRecord.setOptOrg(centralFileInfo.getReceiverOrg());
                    centralFileOptRecord.setOptTime(centralFileTask.getTaskStartTime()); // 接收时间同任务推送时间
                    centralFileOptRecord.setFileOptType("00");// 档案领取
                    centralFileOptRecord.setOptReason("档案领取");
                    centralFileOptRecordService.insert(centralFileOptRecord);
                }

                // 2.2 记录该台账的操作记录
                CentralFileOptRecord centralFileOptRecord = new CentralFileOptRecord();
                centralFileOptRecord.setFileNo(centralFileInfo.getFileNo());
                centralFileOptRecord.setOptUsr(centralFileInfo.getReceiverId());
                centralFileOptRecord.setOptOrg(centralFileInfo.getReceiverOrg());
                // 任务类型=【暂存】
                if ("02".equals(taskType)) {
                    if(isMerge){
                        centralFileOptRecord.setFileOptType("04");// 合并暂存
                        centralFileOptRecord.setOptReason("档案合并暂存");
                    }else{
                        centralFileOptRecord.setFileOptType("01");// 暂存
                        centralFileOptRecord.setOptReason("档案暂存");
                    }
                    centralFileOptRecord.setOptTime(new Date());
                    centralFileOptRecordService.insert(centralFileOptRecord);
                }
                //任务类型=【派发】
                else if ("03".equals(taskType)) {
//                    centralFileOptRecord.setFileOptType("02");// 派发
//                    centralFileOptRecord.setOptReason("档案派发");
                }
                //任务类型=【暂存及派发】
                else if ("04".equals(taskType)) {
                    if(!isMerge){
                        // 先生成一条【档案暂存】操作记录
                        CentralFileOptRecord centralFileOptRecordTemp = new CentralFileOptRecord();
                        BeanUtils.copyProperties(centralFileOptRecord, centralFileOptRecordTemp);
                        centralFileOptRecordTemp.setFileOptType("01");// 暂存
                        centralFileOptRecordTemp.setOptReason("档案暂存");
                        centralFileOptRecordTemp.setOptTime(new Date());
                        centralFileOptRecordService.insert(centralFileOptRecordTemp);
                    }else{
                        // 先生成一条【档案合并暂存】操作记录
                        CentralFileOptRecord centralFileOptRecordTemp = new CentralFileOptRecord();
                        BeanUtils.copyProperties(centralFileOptRecord, centralFileOptRecordTemp);
                        centralFileOptRecordTemp.setFileOptType("04");// 合并暂存
                        centralFileOptRecordTemp.setOptReason("档案合并暂存");
                        centralFileOptRecordTemp.setOptTime(new Date());
                        centralFileOptRecordService.insert(centralFileOptRecordTemp);
                    }
//                    centralFileOptRecord.setFileOptType("02");// 派发
//                    centralFileOptRecord.setOptReason("档案派发");
                }
//                centralFileOptRecord.setOptTime(new Date());
//                centralFileOptRecordService.insert(centralFileOptRecord);
            }

            /**
             * 3、更新任务状态
             */
            centralFileTask.setTaskStatus("04"); // 已完成
            updateSelective(centralFileTask);

            /**
             * 4、处于审批流中的（一般是【派发类】任务）  提交流程
             */
            if(StringUtils.nonEmpty(centralFileTask.getInstanceId()) && ("03".equals(taskType) || "04".equals(taskType))){
                // 4.1 签收流程
                signTaskPool(centralFileTask);
                // 4.2 提交流程
                ResultDto<ResultInstanceDto> instanceResultDto = workflowCoreClient.instanceInfo(centralFileTask.getInstanceId(), centralFileTask.getNodeId(), null);
                if("0".equals(instanceResultDto.getCode()) && instanceResultDto.getData() != null){
                    ResultInstanceDto instanceInfo = instanceResultDto.getData();;
                    //获取下一处理节点，执行过路由脚本过滤的
                    WFNextNodeDto  wFNextNodeDto = new WFNextNodeDto();
                    wFNextNodeDto.setInstanceId(instanceInfo.getInstanceId());
                    wFNextNodeDto.setNodeId(instanceInfo.getNodeId());
                    wFNextNodeDto.setParam(instanceInfo.getParam());
                    ResultDto<List<ResultNodeDto>> resultNodeDto= workflowCoreClient.getNextNodeInfos(wFNextNodeDto);
                    if("0".equals(resultNodeDto.getCode()) && CollectionUtils.nonEmpty(resultNodeDto.getData())){
                        List<NextNodeInfoDto> submitNextNodeInfoList = new ArrayList<>();
                        List<ResultNodeDto> nextAccessNodeList = resultNodeDto.getData();
                        for (ResultNodeDto resultNode : nextAccessNodeList){
                            NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
                            nextNodeInfoDto.setNextNodeId(resultNode.getNodeId());
                            List<String> userIdsList = new ArrayList<>();
                            if(CollectionUtils.nonEmpty(resultNode.getUsers())){
                                resultNode.getUsers().stream().forEach(item -> {
                                    userIdsList.add(item.getUserId());
                                });
                            }else{
                                throw BizException.error(null, "999999", "节点办理人为空，提交失败！");
                            }
                            nextNodeInfoDto.setNextNodeUserIds(userIdsList);
                            submitNextNodeInfoList.add(nextNodeInfoDto);
                        }
                        //流程提交
                        WFSubmitDto wFSubmitDto = new WFSubmitDto();
                        wFSubmitDto.setOrgId(instanceInfo.getOrgId());
                        wFSubmitDto.setNextNodeInfos(submitNextNodeInfoList);
                        wFSubmitDto.setParam(instanceInfo.getParam());
                        WFCommentDto wFCommentDto = new WFCommentDto();
                        wFCommentDto.setCommentSign("O-12");
                        wFCommentDto.setUserComment("同意");
                        wFCommentDto.setInstanceId(instanceInfo.getInstanceId());
                        wFCommentDto.setNodeId(instanceInfo.getNodeId());
                        wFCommentDto.setUserId(SessionUtils.getLoginCode());
                        wFSubmitDto.setComment(wFCommentDto);
                        ResultDto<List<ResultMessageDto>> commmitResultDto = workflowCoreClient.submit(wFSubmitDto);
                        log.info("流程提交响应结果：{}", commmitResultDto);
                        if(!"0".equals(commmitResultDto.getCode())){
                            throw BizException.error(null, "999999", "流程提交失败！");
                        }else{
                            if(commmitResultDto.getData().size() > 0 &&  "提交完成".equals(commmitResultDto.getData().get(0).getTip())){
                                log.info("流程提交成功：{}", commmitResultDto.getData().get(0));
                            }else{
                                throw BizException.error(null, "999999", commmitResultDto.getData().get(0).getTip());
                            }
                            String resultStr = commmitResultDto.getData().get(0).getNodeName() + "-" + commmitResultDto.getData().get(0).getUserNames().get(0);
                            if(!"01".equals(optType)){
                                ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getTodoUserInfo(centralFileTask.getInstanceId());
                                if("0".equals(adminSmUserDtoResultDto.getCode()) && adminSmUserDtoResultDto.getData() != null){
                                    AdminSmUserDto adminSmUserDto = adminSmUserDtoResultDto.getData();
                                    CentralFileOptRecord centralFileOptRecord = new CentralFileOptRecord();
                                    centralFileOptRecord.setFileNo(centralFileInfo.getFileNo());
                                    centralFileOptRecord.setFileOptType("02");// 派发
                                    centralFileOptRecord.setOptReason("档案派发");
                                    centralFileOptRecord.setOptUsr(adminSmUserDto.getUserCode());
                                    centralFileOptRecord.setOptOrg(adminSmUserDto.getOrgId());
                                    centralFileOptRecord.setOptTime(new Date());
                                    centralFileOptRecordService.insert(centralFileOptRecord);
                                    // 更新台账接收人、接收机构
                                    CentralFileInfo oldCentralFileInfo = centralFileInfoService.selectByPrimaryKey(centralFileInfo.getFileNo());
                                    oldCentralFileInfo.setAccStatus(centralFileInfo.getAccStatus());
                                    oldCentralFileInfo.setReceiverId(adminSmUserDto.getUserCode());
                                    oldCentralFileInfo.setReceiverOrg(adminSmUserDto.getOrgId());
                                    oldCentralFileInfo.setFileReceiverDate(DateUtils.getCurrDateStr());
                                    centralFileInfoService.updateSelective(oldCentralFileInfo);
                                }else{
                                    throw BizException.error(null, "999999", "提交成功后获取办理人员详细信息失败！");
                                }
                            }

                            return "提交成功，下一节点处理人：" + resultStr;
                        }
                    }else{
                        throw BizException.error(null, "999999", "未获取到下一节点，提交失败！");
                    }
                }else{
                    throw BizException.error(null, "999999", "未获取到流程实例信息，提交失败！");
                }
            }

        }else{
            throw BizException.error(null, "999999", "未获取到任务信息！");
        }
        return "提交成功";
    }


    /***
     * 工作流项目池领取
     * @param item 任务对象
     * @return void
     * @author tangxun
     * @date 2021/5/7 11:52 下午
     * @version 1.0.0
     * @desc 调用流程签收接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void signTaskPool(CentralFileTask item) {
        WFTaskpoolDto wFTaskpoolDto = new WFTaskpoolDto();
        wFTaskpoolDto.setInstanceId(item.getInstanceId());
        wFTaskpoolDto.setNodeId(item.getNodeId());
        wFTaskpoolDto.setUserId(item.getReceiverId());
        wFTaskpoolDto.setPoolId("JZZYDA"); // 工作流-集中作业档案项目池
        ResultDto<ResultMessageDto> result = workflowCoreClient.signTaskPool(wFTaskpoolDto);

        if (Objects.nonNull(result) && !"0".equals(result.getCode())) {
            log.info("领取流程失败:{}", result.getData());
            throw new RuntimeException(result.getData().getTip());
        }
    }

    /***
     * 工作流项目池领取
     * @param item 任务对象
     * @return void
     * @author wzy
     * @date 2021/5/7 11:52 下午
     * @version 1.0.0
     * @desc 调用流程签收接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void signXYKTaskPool(CentralFileTask item) {
        WFTaskpoolDto wFTaskpoolDto = new WFTaskpoolDto();
        wFTaskpoolDto.setInstanceId(item.getInstanceId());
        wFTaskpoolDto.setNodeId(item.getNodeId());
        wFTaskpoolDto.setUserId(item.getReceiverId());
        wFTaskpoolDto.setPoolId("XYKXXC"); // 工作流-信用卡项目池
        ResultDto<ResultMessageDto> result = workflowCoreClient.signTaskPool(wFTaskpoolDto);

        if (Objects.nonNull(result) && !"0".equals(result.getCode())) {
            log.info("领取流程失败:{}", result.getData());
            throw new RuntimeException(result.getData().getTip());
        }
    }
    /***
     * 含审批流的业务，流程否决或者自行退出时；调用此接口更新档案任务状态
     * @param taskNo 任务编号
     * @return void
     * @author tangxun
     * @date 2021/5/11 11:57 上午
     * @version 1.0.0
     * @desc 档案审批否决
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Deprecated
    public void taskRefuse(String taskNo, String resn) {
        CentralFileTask centralFileTask = new CentralFileTask();
        centralFileTask.setCancelResn(resn);
        centralFileTask.setSerno(taskNo);
        centralFileTask.setTaskStatus("02");
        centralFileTaskMapper.updateByPrimaryKeySelective(centralFileTask);
    }
}
