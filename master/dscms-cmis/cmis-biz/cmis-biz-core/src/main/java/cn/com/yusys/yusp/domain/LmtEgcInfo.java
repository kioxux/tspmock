/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtEgcInfo
 * @类描述: lmt_egc_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-18 19:50:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_egc_info")
public class LmtEgcInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 联系人电话 **/
	@Column(name = "LINK_PHONE", unique = false, nullable = true, length = 11)
	private String linkPhone;
	
	/** 工作单位 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 200)
	private String workUnit;
	
	/** 共借人关系 **/
	@Column(name = "COMMON_DEBIT_RELA", unique = false, nullable = true, length = 6)
	private String commonDebitRela;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param linkPhone
	 */
	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone;
	}
	
    /**
     * @return linkPhone
     */
	public String getLinkPhone() {
		return this.linkPhone;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param commonDebitRela
	 */
	public void setCommonDebitRela(String commonDebitRela) {
		this.commonDebitRela = commonDebitRela;
	}
	
    /**
     * @return commonDebitRela
     */
	public String getCommonDebitRela() {
		return this.commonDebitRela;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}