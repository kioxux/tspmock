package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarRegisterDetail;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarRegisterDetailMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarRegisterDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-17 22:01:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarRegisterDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuarRegisterDetailService.class);

    @Autowired
    private GuarRegisterDetailMapper guarRegisterDetailMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarRegisterDetail selectByPrimaryKey(String serno) {
        return guarRegisterDetailMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<GuarRegisterDetail> selectAll(QueryModel model) {
        List<GuarRegisterDetail> records = (List<GuarRegisterDetail>) guarRegisterDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarRegisterDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarRegisterDetail> list = guarRegisterDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(GuarRegisterDetail record) {
        return guarRegisterDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(GuarRegisterDetail record) {
        return guarRegisterDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(GuarRegisterDetail record) {
        return guarRegisterDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(GuarRegisterDetail record) {
        return guarRegisterDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return guarRegisterDetailMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarRegisterDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertGuarRegisterDetail
     * @方法描述: 根据主键新增
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertGuarRegisterDetail(List<GuarRegisterDetail> record) {
        AtomicInteger flag = new AtomicInteger(1);
        record.stream().forEach(guarRegisterDetail -> {
            int numFlag = checkGuarInfoIsExist(guarRegisterDetail.getSerno());
            int result = -1;
            if (0 < numFlag) {
                result = guarRegisterDetailMapper.updateByPrimaryKey(guarRegisterDetail);
            } else {
                result = guarRegisterDetailMapper.insertSelective(guarRegisterDetail);
            }
            if (0 > result) {
                flag.set(-1);
            }
        });
        return flag.intValue();
    }

    /**
     * @方法名称: insertGuarRegisterDetail
     * @方法描述: 根据主键新增
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveGuarRegisterDetail(List<GuarRegisterDetail> record) {
        AtomicInteger flag = new AtomicInteger(1);
        record.stream().forEach(guarRegisterDetail -> {
            int numFlag = checkGuarInfoIsExist(guarRegisterDetail.getSerno());
            int result = -1;
            if (0 < numFlag) {
                result = guarRegisterDetailMapper.updateByPrimaryKey(guarRegisterDetail);
            } else {
                result = guarRegisterDetailMapper.insertSelective(guarRegisterDetail);
            }
            if (0 > result) {
                flag.set(-1);
            }
        });
        return flag.intValue();
    }

    /**
     * @方法名称: selectByRegSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarRegisterDetail> selectByRegSerno(String regSerno) {
        return guarRegisterDetailMapper.selectByRegSerno(regSerno);
    }


    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明: 存在 为 1 ,不存在为 0
     * @算法描述:
     */
    public int checkGuarInfoIsExist(String serno) {
        GuarRegisterDetail guarRegisterDetail = guarRegisterDetailMapper.selectByPrimaryKey(serno);
        return (null == guarRegisterDetail || null == guarRegisterDetail.getSerno() || "".equals(guarRegisterDetail.getSerno())) ? 0 : 1;
    }

    /**
     * @方法名称: selectByRegSernoModel
     * @方法描述: 根据抵押登记流水号条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto<List<GuarRegisterDetail>> selectByRegSernoModel(String regSerno) {
        ResultDto<List<GuarRegisterDetail>> listResultDto = new ResultDto<>();
        try {
            QueryModel model = new QueryModel();
            model.addCondition("regSerno", regSerno);
            PageHelper.startPage(model.getPage(), model.getSize());
            List<GuarRegisterDetail> list = guarRegisterDetailMapper.selectByRegSernoModel(model);
            if (!list.isEmpty()) {
                listResultDto.setData(list);
            }
            PageHelper.clearPage();
        } catch (Exception e) {
            listResultDto.setCode(EcbEnum.COMMON_EXCEPTION_DEF.key);
            listResultDto.setMessage(e.getMessage());
            LOGGER.error(e.getMessage(), e);
        }
        return listResultDto;
    }
}
