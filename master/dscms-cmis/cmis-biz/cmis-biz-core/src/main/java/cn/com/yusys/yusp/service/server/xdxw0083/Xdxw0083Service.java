package cn.com.yusys.yusp.service.server.xdxw0083;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0083.req.Xdxw0083DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0083.resp.Xdxw0083DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:优企贷还款账号变更
 *
 * @author zrcbank
 * @version 1.0
 */
@Service
public class Xdxw0083Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0083Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 优企贷还款账号变更
     *
     * @param xdxw0083DataReqDto
     * @return
     */
    public Xdxw0083DataRespDto xdxw0083(Xdxw0083DataReqDto xdxw0083DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataReqDto));
        Xdxw0083DataRespDto xdxw0083DataRespDto = new Xdxw0083DataRespDto();
        //获取请求参数
        String billNo = xdxw0083DataReqDto.getBillNo();
        String repayNo = xdxw0083DataReqDto.getRepayAcctNo();
        Map queryMap = new HashMap();
        queryMap.put("billNo", billNo);
        queryMap.put("repayNo", repayNo);
        int count = accLoanMapper.updateAccLoanRepayAcctNo(queryMap);
        if (count > 0) {
            xdxw0083DataRespDto.setOpFlag("S");
            xdxw0083DataRespDto.setOpMsg("优企贷还款账号变更成功");
        } else {
            xdxw0083DataRespDto.setOpFlag("F");
            xdxw0083DataRespDto.setOpMsg("优企贷还款账号变更失败");
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataRespDto));
        return xdxw0083DataRespDto;
    }
}
