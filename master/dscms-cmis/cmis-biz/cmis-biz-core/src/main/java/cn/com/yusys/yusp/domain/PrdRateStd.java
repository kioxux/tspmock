/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrdRateStd
 * @类描述: prd_rate_std数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-07 10:59:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "prd_rate_std")
public class PrdRateStd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 利率类型代码 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "rate_type_id")
	private String rateTypeId;
	
	/** 利率类型 **/
	@Column(name = "rate_type_name", unique = false, nullable = true, length = 2)
	private String rateTypeName;
	
	/** 利率值 **/
	@Column(name = "rate", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rate;
	
	/** 生效日期 **/
	@Column(name = "valid_date", unique = false, nullable = true, length = 10)
	private String validDate;
	
	/** 最小期限 **/
	@Column(name = "term_min", unique = false, nullable = true, length = 10)
	private Integer termMin;
	
	/** 最大期限 **/
	@Column(name = "term_max", unique = false, nullable = true, length = 10)
	private Integer termMax;
	
	/** 利率基准 **/
	@Column(name = "rate_datum", unique = false, nullable = true, length = 1)
	private String rateDatum;
	
	/** 是否启用 **/
	@Column(name = "used_ind", unique = false, nullable = true, length = 1)
	private String usedInd;
	
	/** 币种 **/
	@Column(name = "currency", unique = false, nullable = true, length = 3)
	private String currency;
	
	/** 利率名称 **/
	@Column(name = "rate_name", unique = false, nullable = true, length = 200)
	private String rateName;
	
	
	/**
	 * @param rateTypeId
	 */
	public void setRateTypeId(String rateTypeId) {
		this.rateTypeId = rateTypeId;
	}
	
    /**
     * @return rateTypeId
     */
	public String getRateTypeId() {
		return this.rateTypeId;
	}
	
	/**
	 * @param rateTypeName
	 */
	public void setRateTypeName(String rateTypeName) {
		this.rateTypeName = rateTypeName;
	}
	
    /**
     * @return rateTypeName
     */
	public String getRateTypeName() {
		return this.rateTypeName;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return rate
     */
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param validDate
	 */
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	
    /**
     * @return validDate
     */
	public String getValidDate() {
		return this.validDate;
	}
	
	/**
	 * @param termMin
	 */
	public void setTermMin(Integer termMin) {
		this.termMin = termMin;
	}
	
    /**
     * @return termMin
     */
	public Integer getTermMin() {
		return this.termMin;
	}
	
	/**
	 * @param termMax
	 */
	public void setTermMax(Integer termMax) {
		this.termMax = termMax;
	}
	
    /**
     * @return termMax
     */
	public Integer getTermMax() {
		return this.termMax;
	}
	
	/**
	 * @param rateDatum
	 */
	public void setRateDatum(String rateDatum) {
		this.rateDatum = rateDatum;
	}
	
    /**
     * @return rateDatum
     */
	public String getRateDatum() {
		return this.rateDatum;
	}
	
	/**
	 * @param usedInd
	 */
	public void setUsedInd(String usedInd) {
		this.usedInd = usedInd;
	}
	
    /**
     * @return usedInd
     */
	public String getUsedInd() {
		return this.usedInd;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param rateName
	 */
	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	
    /**
     * @return rateName
     */
	public String getRateName() {
		return this.rateName;
	}


}