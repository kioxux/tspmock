package cn.com.yusys.yusp.service.server.xddb0023;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.server.xddb0023.req.Xddb0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0023.resp.Xddb0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.*;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultNodeDto;
import cn.com.yusys.yusp.repository.mapper.GuarMortgageManageAppMapper;
import cn.com.yusys.yusp.repository.mapper.GuarMortgageManageRelMapper;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizUtils;
import io.netty.util.internal.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 响应Dto：更新抵质押品状态
 *
 * @author zdl
 * @version 1.0
 */
@Service
public class Xddb0023Service {
    private static final Logger log= LoggerFactory.getLogger(Xddb0023Service.class);

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;
    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;
    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private Dscms2YpqzxtClientService dscms2YpqzxtClientService;
    @Autowired
    private AdminSmTreeDicClientService adminSmTreeDicClientService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private GuarWarrantInfoMapper guarWarrantInfoMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private GuarMortgageManageAppService guarMortgageManageAppService;

    @Autowired
    private GuarMortgageManageRelMapper guarMortgageManageRelMapper;

    @Autowired
    private GuarMortgageManageAppMapper guarMortgageManageAppMapper;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private GuarMortgageManageRelService guarMortgageManageRelService;

    /**
     * 更新抵质押品状态
     *
     * @param xddb0023DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0023DataRespDto xddb0023(Xddb0023DataReqDto xddb0023DataReqDto) throws Exception {
        log.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value);
        Xddb0023DataRespDto xddb0023DataRespDto = new Xddb0023DataRespDto();
        String ypseno = xddb0023DataReqDto.getYpseno();//核心担保编号
        String status = xddb0023DataReqDto.getStatus();//出入库状态
        String inputDate = stringRedisTemplate.opsForValue().get("openDay");//营业日期

        try {
            //todo 押品表结构改造中，暂时无法参照老信贷逻辑，暂时只能先把接口调通。
            //通知押品缓释  cetinf certis
            //通知押品权证  cwm003  出库cwm004
            String inOuntFlag = "";

            List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectByCoreGuarantyNo(ypseno);
            if(guarContRelWarrants == null ||guarContRelWarrants.size() == 0){
                throw BizException.error(null, "9999", "未找到核心担保编号："+ypseno+"对应的数据");
            }
            GuarContRelWarrant guarContRelWarrant = guarContRelWarrants.get(0);
            //押品编号
            String guarNo = guarContRelWarrant.getGuarNo();
            GuarBaseInfo guarBaseInfoDto = guarBaseInfoService.queryBaseInfoByGuarId(guarNo);

            GuarWarrantInfo warrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(ypseno);

            if("X".equals(status)){//老代码没有注释 ，X是什么状态？撤销至未入库？
                //原逻辑根据核心担保编号将grt_gp_inout_info、grt_g_basic_info、grt_p_basic_info三张表的status_code更新成10001
            }else{
                inOuntFlag = "1".equals(status)? "I" : "O";
                if("I".equals(inOuntFlag)){//入库
                    //3.权证入库权证信息发送押品，权证状态同步给押品系统
                    //3.1权证信息同步押品系统
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("coreGuarantyNo",ypseno);
                    queryModel.addCondition("warrantAppType",CmisBizConstants.STD_WARRANT_APP_TYPE_01);
                    queryModel.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_997);
                    queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                    GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectAll(queryModel).get(0);

                    log.info("权证入库调cetinf接口将权证信息同步押品系统 开始，流水号【"+ypseno+"】");
                    this.sendCetinf(warrantInfo,guarContRelWarrants,inputDate,ypseno);
                    log.info("权证入库调cetinf接口将权证信息同步押品系统 结束，流水号【"+ypseno+"】");

                    //3.2权证状态同步押品
                    log.info("权证入库调certis接口将权证状态同步押品系统 开始，流水号【"+ypseno+"】");
                    this.sendCertis(warrantInfo,inputDate,ypseno,"10006","","","","","");
                    log.info("权证入库调certis接口将权证状态同步押品系统 结束，流水号【"+ypseno+"】");

                    //4.通知押品权证入库 cwm003
                    //发送权证信息到权证系统
                    log.info("权证入库调cwm003接口将权证信息同步权证系统 开始，流水号【"+ypseno+"】");
                    this.sendCwm003(guarWarrantManageApp,guarBaseInfoDto,warrantInfo,inputDate,ypseno);
                    log.info("权证入库调cwm003接口将权证信息同步权证系统 结束，流水号【"+ypseno+"】");

                }else if ("O".equals(inOuntFlag)) {//出库
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("coreGuarantyNo",ypseno);
                    queryModel.addCondition("warrantAppType",CmisBizConstants.STD_WARRANT_APP_TYPE_02);
                    queryModel.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_997);
                    queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                    GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectAll(queryModel).get(0);
                    //权证出库原因细类
                    String warrantOutTypeSub = guarWarrantManageApp.getWarrantOutTypeSub();
                    //权证出库流水号
                    String serno = guarWarrantManageApp.getSerno();
                    //权证外借原因
                    String qzwjyy = "";
                    //权证外借日期
                    String qzwjrq = "";
                    //权证临时借用人名称
                    String qzjyrm = "";
                    //预计归还日期
                    String yjghrq = "";
                    //权证状态
                    String qzztyp = "";
                    //权证正常出库日期
                    String qzckrq = warrantInfo.getOutDate();

                    if(CmisBizConstants.STD_WARRANT_OUT_TYPE_04.equals(warrantOutTypeSub) || CmisBizConstants.STD_WARRANT_OUT_TYPE_05.equals(warrantOutTypeSub)){
                        log.info("权证出入库续借管理申请流水号："+serno+"的权证出库原因细类是权证借阅");
                        warrantInfo.setCertiState("07");//07出借已记账
                        qzwjrq = inputDate;
                        yjghrq = guarWarrantManageApp.getPreBackDate();
                        qzjyrm = guarWarrantManageApp.getInputId();

                        if (CmisBizConstants.STD_WARRANT_OUT_TYPE_05.equals(guarWarrantManageApp.getWarrantOutTypeSub())){
                            qzwjyy = "09";
                        }else{
                            qzwjyy = "08";
                        }
                        qzztyp = "10019";
                    }else{
                        log.info("权证出入库续借管理申请流水号："+serno+"的权证出库原因细类不是权证借阅");
                        warrantInfo.setCertiState("10");//10出库已记账
                        //10008-正常出库
                        qzztyp = "10008";
                    }

                    //3.1权证信息同步押品系统
                    log.info("权证出库调certis接口将权证信息同步押品系统 开始，流水号【"+serno+"】");
                    this.sendCertis(warrantInfo,inputDate,serno,qzztyp,qzwjyy,yjghrq,qzjyrm,qzwjrq,qzckrq);
                    log.info("权证出库调certis接口将权证信息同步押品系统 结束，流水号【"+serno+"】");

                    warrantInfo.setOutDate(inputDate);
                    guarWarrantInfoService.updateSelective(warrantInfo);
                    log.info("权证出库调certis接口成功，将权证状态改为"+warrantInfo.getCertiState());
                    
                    //如果是出库到集中作业，则生成抵押注销任务并自动提交到集中作业
                    if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(guarWarrantManageApp.getIsZhblzx())){
                        createGuarMortgageLogOut(guarWarrantManageApp);
                    }

                    if(CmisBizConstants.STD_WARRANT_OUT_TYPE_04.equals(warrantOutTypeSub) || CmisBizConstants.STD_WARRANT_OUT_TYPE_05.equals(warrantOutTypeSub)){
                        log.info("权证出库流水号："+serno+"的权证出库原因细类 是 权证借阅，调cwm001发送权证出库信息到权证系统 开始");
                        sendCwm001(guarWarrantManageApp,guarBaseInfoDto,warrantInfo,serno);
                        log.info("权证出库流水号："+serno+"的权证出库原因细类 是 权证借阅，调cwm001发送权证出库信息到权证系统 结束");
                    }else{
                        log.info("权证出库流水号："+serno+"的权证出库原因细类 不是 权证借阅，调cwm004发送权证出库信息到权证系统 开始");
                        sendCwm004(guarWarrantManageApp,guarBaseInfoDto,warrantInfo,inputDate,serno);
                        log.info("权证出库流水号："+serno+"的权证出库原因细类 不是 权证借阅，调cwm004发送权证出库信息到权证系统 结束");
                    }
                }
            }

            //通知印控yky001 todo
        } catch (BizException e) {
            log.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            log.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        log.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value);
        return xddb0023DataRespDto;
    }


    /**
     * 权证入库调cetinf接口将权证信息同步押品系统
     * @param warrantInfo
     * @param guarContRelWarrants
     * @param inputDate
     * @param ypseno
     */
    public void sendCetinf(GuarWarrantInfo warrantInfo, List<GuarContRelWarrant> guarContRelWarrants, String inputDate,String ypseno){
        try {
            GuarContRelWarrant guarContRelWarrant = guarContRelWarrants.get(0);
            CetinfReqDto reqDto = new CetinfReqDto();
            //担保合同编号
            reqDto.setDbhtbh(guarContRelWarrant.getGuarContNo());
            //核心担保编号/抵押登记编号YP号
            reqDto.setSernoy(warrantInfo.getCoreGuarantyNo());
            //抵押顺位标识
            reqDto.setDyswbs(warrantInfo.getMortOrderFlag());
            //权利凭证号
            reqDto.setQlpzhm(warrantInfo.getWarrantNo());
            //权证类型
            reqDto.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(warrantInfo.getCertiTypeCd()));
            //权证发证机关名称
            reqDto.setQzfzjg(warrantInfo.getCertiOrgName());
            //权证发证日期
            reqDto.setQzffrq(warrantInfo.getCertiStartDate());
            //权证到期日期
            reqDto.setQzdqrq(warrantInfo.getCertiEndDate());
            //权利金额
            reqDto.setQljeyp(warrantInfo.getCertiAmt().toString());
            //权证状态
            reqDto.setQzztyp("10006");
            //登记人
            reqDto.setDjrmyp(warrantInfo.getInputId());
            //登记机构
            reqDto.setDjjgyp(warrantInfo.getInputBrId());
            //登记日期
            reqDto.setDjrqyp(inputDate);
            //操作
            reqDto.setOperat("01");
            //押品编号
            List<CetinfListInfo> cetinfListInfo = new ArrayList<>();

            for (GuarContRelWarrant guarContRel : guarContRelWarrants) {
                String guarNo = guarContRel.getGuarNo();
                CetinfListInfo cetinfInfo = new CetinfListInfo();
                cetinfInfo.setYptybh(guarNo);//押品编号
                cetinfListInfo.add(cetinfInfo);
            }

            reqDto.setList(cetinfListInfo);

            log.info("流水号【"+ypseno+"】，权证入库发押品系统开始："+ reqDto);
            ResultDto<CetinfRespDto> cetinfRespDto = dscms2YpxtClientService.cetinf(reqDto);
            log.info("流水号【"+ypseno+"】，权证入库发押品系统结束："+ cetinfRespDto);

            String code = Optional.ofNullable(cetinfRespDto.getCode()).orElse(StringUtils.EMPTY);
            String meesage = Optional.ofNullable(cetinfRespDto.getMessage()).orElse(StringUtils.EMPTY);
            CetinfRespDto cetinfRespDto1;

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // TODO 获取相关的值并解析
                cetinfRespDto1 = cetinfRespDto.getData();
                log.info("流水号【"+ypseno+"】，权证信息同步接口 成功"+ cetinfRespDto1);
            } else {
                // TODO 抛出错误异常
                log.error("流水号【"+ypseno+"】，权证信息同步接口 失败"+meesage);
                throw new BizException(null, "", null, meesage);
            }
        } catch (Exception e) {
            log.info("权证信息同步押品失败", e.getMessage());
            throw new BizException(null, "", null, "权证信息同步押品失败");
        }
    }

    /**
     * 权证入库调certis接口将权证状态同步押品系统
     * @param warrantInfo
     * @param inputDate
     * @param serno
     * @param qzztyp
     */
    public void sendCertis(GuarWarrantInfo warrantInfo,String inputDate,String serno,String qzztyp,String qzwjyy,String yjghrq,String qzjyrm,String qzwjrq,String qzckrq){
        try {
            CertisReqDto certisReqDto = new CertisReqDto();
            List<CertisListInfo> certisListInfo = new ArrayList();
            CertisListInfo certisInfo = new CertisListInfo();

            //权证流水号
            certisInfo.setSernoy(warrantInfo.getCoreGuarantyNo());
            //权利凭证号
            certisInfo.setQlpzhm(warrantInfo.getWarrantNo());
            //权证类型
            certisInfo.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(warrantInfo.getCertiTypeCd()));
            //权证出入库状态
            certisInfo.setQzztyp(qzztyp);
            //权证入库日期 状态为10006-已入库时必输
            certisInfo.setQzrkrq(inputDate);
            //权证正常出库日期    状态为10008-正常出库时必输
            certisInfo.setQzckrq(qzckrq);
            //权证临时借用人名称    状态为10019-已借阅时，必输
            certisInfo.setQzjyrm(qzjyrm);
            //权证外借日期			状态为10019-已借阅时，必输
            certisInfo.setQzwjrq(qzwjrq);
            //预计归还日期			状态为10019-已借阅时，必输
            certisInfo.setYjghrq(yjghrq);
            //权证实际归还日期		状态为10020-外借归还是，必输
            certisInfo.setSjghrq("");
            //权证外借原因			状态为10019-已借阅时，必输
            certisInfo.setQzwjyy(qzwjyy);
            //其他文本输入
            certisInfo.setQtwbsr("");
            certisListInfo.add(certisInfo);
            certisReqDto.setList(certisListInfo);

            log.info("流水号【"+serno+"】，调押品系统certis进行权证状态同步 开始："+ certisReqDto);
            ResultDto<CertisRespDto> certisRespDto = dscms2YpxtClientService.certis(certisReqDto);
            log.info("流水号【"+serno+"】，调押品系统certis进行权证状态同步 结束："+ certisRespDto);

            String code = Optional.ofNullable(certisRespDto.getCode()).orElse(StringUtils.EMPTY);
            String meesage = Optional.ofNullable(certisRespDto.getMessage()).orElse(StringUtils.EMPTY);

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                CertisRespDto data = certisRespDto.getData();
                log.info("流水号【"+serno+"】，调押品系统certis进行权证状态同步 成功："+ data);
            } else {
                log.error("流水号【"+serno+"】，调押品系统certis进行权证状态同步 失败："+meesage);
                throw new BizException(null, "", null, meesage);
            }
        } catch (Exception e) {
            log.info("流水号【"+serno+"】，调押品系统certis进行权证状态同步", e.getMessage());
            throw new BizException(null, "", null, "流水号【"+serno+"】，调押品系统certis进行权证状态同步 失败："+e.getMessage());
        }
    }

    /**
     * 权证入库调cwm003接口将权证信息同步权证系统
     * @param guarBaseInfoDto
     * @param warrantInfo
     * @param inputDate
     * @param ypseno
     */
    public void sendCwm003(GuarWarrantManageApp guarWarrantManageApp,GuarBaseInfo guarBaseInfoDto,GuarWarrantInfo warrantInfo,String inputDate,String ypseno){
        Cwm003ReqDto cwm003ReqDto = new Cwm003ReqDto();
        String orgName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getInputBrId());
        String usrName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());
        String managerBrId = guarWarrantManageApp.getManagerBrId();
        String managerBrName = OcaTranslatorUtils.getOrgName(managerBrId);

        //担保分类代码
        String qzSysGageType = guarBaseInfoDto.getGuarTypeCd();
        //柜员名称
        cwm003ReqDto.setUserName(usrName);
        //申请时间
        cwm003ReqDto.setApplyTime(inputDate);
        //核心担保品编号
        cwm003ReqDto.setGageId(ypseno);
        //抵质押类型
        String gageType = guarWarrantManageAppService.transferGageTypeDict(qzSysGageType);
        cwm003ReqDto.setGageType(gageType);
        //抵质押类型名称
        String gageTypeName = guarWarrantManageAppService.getGageTypeNameFromAdminSmTreeDic(qzSysGageType);
        cwm003ReqDto.setGageTypeName(gageTypeName);
        //权利价值
        cwm003ReqDto.setMaxAmt(warrantInfo.getCertiAmt().toString());
        //抵押人
        cwm003ReqDto.setGageUser(guarBaseInfoDto.getGuarCusName());

        String applyBrchNo = managerBrId;

        if (applyBrchNo.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            applyBrchNo = "810100";
            managerBrName = "东海村镇银行营业部";
        }else if(applyBrchNo.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            applyBrchNo = "800100";
            managerBrName = "寿光村镇银行营业部";
        }
        applyBrchNo = applyBrchNo.substring(0, applyBrchNo.length()-1) +"1";

        //账务机构号
        cwm003ReqDto.setAcctBrch(applyBrchNo);
        //账务机构名称
        cwm003ReqDto.setAcctBrchName(managerBrName);
        //申请支行号
        cwm003ReqDto.setApplyBrchNo(guarWarrantManageApp.getInputBrId());
        //申请支行名称
        cwm003ReqDto.setApplyBrchName(orgName);
        //申请人操作号
        cwm003ReqDto.setApplyUserCode(guarWarrantManageApp.getInputId());
        //申请人姓名
        cwm003ReqDto.setApplyUserName(usrName);
        //是否住房按揭
        String isMortgage = getIsMortgage(guarWarrantManageApp.getCoreGuarantyNo());
        cwm003ReqDto.setIsMortgage(isMortgage);
        //备注
        cwm003ReqDto.setRemark("");
        //传入抵押品名称
        cwm003ReqDto.setGageName(guarBaseInfoDto.getPldimnMemo());
        //传入本地标志
//        String is_local = guarBaseInfoDto.getAreaCode();
//        if (is_local != null && !"".equals(is_local) && is_local.length() > 5) {
//            is_local = is_local.substring(0, 6);
//        }
//        is_local = "320582".equals(is_local) ? "1" : "0";
        //柜面审核的，is_local送0，不会走到集中作业洪向阳那边
        cwm003ReqDto.setIsLocal("0");

        //传入是否电子权证 1--是 2--否
        String isElectronic = "2";

        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(warrantInfo.getIsEWarrant())) {
            isElectronic = "1";
        }
        cwm003ReqDto.setIsElectronic(isElectronic);

        //是否柜面审核 默认 Y：柜面审核
//        cwm003ReqDto.setIsgm("Y");

        log.info("核心担保编号【"+ypseno+"】，权证入库调权证系统cwm003接口进行入库申请，请求报文："+ cwm003ReqDto);
        ResultDto<Cwm003RespDto> cwm003RespResultDto = dscms2YpqzxtClientService.cwm003(cwm003ReqDto);
        log.info("核心担保编号【"+ypseno+"】，权证入库调权证系统cwm003接口进行入库申请，响应报文："+ cwm003RespResultDto);

        String code = Optional.ofNullable(cwm003RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(cwm003RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        Cwm003RespDto cwm003RespDto1;

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            cwm003RespDto1 = cwm003RespResultDto.getData();
            warrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_04);
            guarWarrantInfoMapper.updateByPrimaryKey(warrantInfo);
            log.info("将核心担保编号为【"+warrantInfo.getCoreGuarantyNo()+"】的权证状态由入库未记账改为入库已记账");

            log.info("核心担保编号【"+ypseno+"】，权证入库调权证系统cwm003接口进行入库申请 成功："+ cwm003RespDto1);
        } else {
            log.error("核心担保编号【"+ypseno+"】，权证入库调权证系统cwm003接口进行入库申请 失败："+meesage);
            throw new BizException(null, "", null, "核心担保编号【"+ypseno+"】，权证入库调权证系统cwm003接口进行入库申请 失败："+meesage);
        }
    }
    /**
     * 权证入库调cwm004接口将权证信息同步权证系统
     * @param guarBaseInfoDto
     * @param warrantInfo
     * @param inputDate
     * @param serno
     */
    public void sendCwm004(GuarWarrantManageApp guarWarrantManageApp,GuarBaseInfo guarBaseInfoDto,GuarWarrantInfo warrantInfo,String inputDate,String serno){
        //4.发送权证出库信息到权证系统
        Cwm004ReqDto cwm004ReqDto = new Cwm004ReqDto();
        String applyBrchNo = guarWarrantManageApp.getInputBrId();
        String orgName = OcaTranslatorUtils.getOrgName(applyBrchNo);
        String usrName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());
        String managerBrName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getManagerBrId());

        //是否总行办理注销登记 //TODO 017001总行营业部 待确定
//        String iszhRegist = "";
//        if (!StringUtil.isNullOrEmpty(guarBaseInfoDto.getInputBrId())) {
//            iszhRegist = "017001".equals(guarBaseInfoDto.getInputBrId()) ? "1" : "2";
//        }
        //柜员名称
        cwm004ReqDto.setUserName(usrName);
        //申请时间
        cwm004ReqDto.setApplyTime(inputDate);
        //核心担保品编号
        cwm004ReqDto.setGageId(guarWarrantManageApp.getCoreGuarantyNo());
        //担保分类代码
        String guarTypeCd = guarBaseInfoDto.getGuarTypeCd();

        //抵质押类型
        String gageType = guarWarrantManageAppService.transferGageTypeDict(guarTypeCd);
        cwm004ReqDto.setGageType(gageType);

        //抵质押类型名称
        String gageTypeName = guarWarrantManageAppService.getGageTypeNameFromAdminSmTreeDic(guarTypeCd);
        cwm004ReqDto.setGageTypeName(gageTypeName);

        //权利价值
        cwm004ReqDto.setMaxAmt(warrantInfo.getCertiAmt());
        //抵押人
        cwm004ReqDto.setGageUser(guarBaseInfoDto.getGuarCusName());
        //账务机构
        String finaBrId = bizCommonService.getFinaBrNoByManagerBrId(guarWarrantManageApp.getManagerBrId());
        log.info("管理机构【"+guarWarrantManageApp.getManagerBrId()+"】的账务机构是【"+finaBrId+"】");
        cwm004ReqDto.setAcctBrch(finaBrId);

        if (finaBrId.startsWith("81")){
            //东海村镇默认东海村镇银行营业部
            managerBrName = "东海村镇银行营业部";
            applyBrchNo = finaBrId;
        }else if(finaBrId.startsWith("80")){
            //寿光村镇默认寿光村镇银行营业部
            managerBrName = "寿光村镇银行营业部";
            applyBrchNo = finaBrId;
        }
        //账务机构名称
        cwm004ReqDto.setAcctBrchName(managerBrName);
        //申请支行号
        cwm004ReqDto.setApplyBrchNo(applyBrchNo);
        //申请支行名称
        cwm004ReqDto.setApplyBrchName(orgName);
        //申请人操作号
        cwm004ReqDto.setApplyUserCode(guarWarrantManageApp.getInputId());
        //申请人名称
        cwm004ReqDto.setApplyUserName(usrName);
        //是否住房按揭
        String isMortgage = getIsMortgage(guarWarrantManageApp.getCoreGuarantyNo());
        cwm004ReqDto.setIsMortgage(isMortgage);
        //描述
        cwm004ReqDto.setRemark("");
        //抵押物名称
        cwm004ReqDto.setGageName(guarBaseInfoDto.getPldimnMemo());
        //柜面审核的，is_local送0，不会走到集中作业洪向阳那边
        cwm004ReqDto.setIsLocal("0");
        //是否电子类押品 1--电子 2--实物
        String isElectronic = "2";

        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(warrantInfo.getIsEWarrant())) {
            isElectronic = "1";
        }
        cwm004ReqDto.setIsElectronic(isElectronic);
        //是否总行办理注销登记
        cwm004ReqDto.setIszhRegist(guarWarrantManageApp.getIsZhblzx());

        log.info("流水号【"+serno+"】，发送权证出库信息到权证系统开始："+ cwm004ReqDto);
        ResultDto<Cwm004RespDto> cwm004RespResultDto = dscms2YpqzxtClientService.cwm004(cwm004ReqDto);
        log.info("流水号【"+serno+"】，发送权证出库信息到权证系统结束："+ cwm004RespResultDto);

        String code = Optional.ofNullable(cwm004RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(cwm004RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        Cwm004RespDto cwm004RespDto1;
        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            cwm004RespDto1 = cwm004RespResultDto.getData();
            log.info("流水号【"+serno+"】，发送权证出库信息到权证系统 成功"+ cwm004RespDto1);
        } else {
            log.error("流水号【"+serno+"】，发送权证出库信息到权证系统 失败"+meesage);
        }
    }

    public void sendCwm001(GuarWarrantManageApp guarWarrantManageApp,GuarBaseInfo guarBaseInfo,GuarWarrantInfo guarWarrantInfo,String ypseno){
        //是否张家港地区不动产
        String is_local = guarBaseInfo.getAreaCode();

        if (is_local != null && !"".equals(is_local) && is_local.length() > 5) {
            is_local = is_local.substring(0, 6);
        }
        is_local = "320582".equals(is_local) ? "1" : "0";

        //登记机构
        String inputBrId = guarWarrantManageApp.getInputBrId();
        String orgName = OcaTranslatorUtils.getOrgName(inputBrId);
        String usrName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());
        String managerBrName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getManagerBrId());

        //4.发送权证出库信息到权证系统
        Cwm001ReqDto cwm001ReqDto = new Cwm001ReqDto();

        //操作员机构号
        cwm001ReqDto.setOrgcode(inputBrId);
        //操作员机构名称
        cwm001ReqDto.setOrgname(orgName);

        if (!StringUtils.isEmpty(inputBrId) && inputBrId.startsWith("01")){
            //本地机构以01开头，是否异地支行为0--否
            cwm001ReqDto.setIsyd(CmisCommonConstants.YES_NO_0);
        }else{
            //是否异地支行为1--是
            cwm001ReqDto.setIsyd(CmisCommonConstants.YES_NO_1);
        }

        //权证预计归还时间
        String preBackDate = guarWarrantManageApp.getPreBackDate();
        //归还时间
        cwm001ReqDto.setBacktime(dateFormat(preBackDate));

        String qzSysGageType = guarBaseInfo.getGuarTypeCd();
        //柜员名称
        cwm001ReqDto.setUserName(usrName);

        List<ListArrayInfo> listArrayInfos = new ArrayList<>();
        ListArrayInfo listArrayInfo = new ListArrayInfo();
        //核心担保品编号
        listArrayInfo.setGageId(guarWarrantManageApp.getCoreGuarantyNo());
        //抵质押类型
        String gageType = transferGageTypeDict(qzSysGageType);
        listArrayInfo.setGageType(gageType);

        //抵质押类型名称
        String gageTypeName = getGageTypeNameFromAdminSmTreeDic(gageType);
        listArrayInfo.setGageTypeName(gageTypeName);

        //权利价值
        listArrayInfo.setMaxAmt(guarWarrantInfo.getCertiAmt());
        //抵押人
        listArrayInfo.setGageUser(guarBaseInfo.getGuarCusName());
        //账务机构
        String managerBrId = guarWarrantManageApp.getManagerBrId();
        managerBrId = managerBrId.substring(0, managerBrId.length()-1) +"1";
        listArrayInfo.setAcctBrch(managerBrId);
        //账务机构名称
        listArrayInfo.setAcctBrchName(managerBrName);
        //取件人 非必输
//            listArrayInfo.setFetchUser();
        //取件时间 非必输
//            listArrayInfo.setFetchTime();
        //取件人证件号 非必输
//            listArrayInfo.setFetchCardno();
        //是否住房按揭
        String isMortgage = getIsMortgage(guarWarrantManageApp.getCoreGuarantyNo());
        listArrayInfo.setIsMortgage(isMortgage);
        //库位 TODO
//            listArrayInfo.setLocale();
        //档案盒号 TODO
//            listArrayInfo.setBoxid();
        //抵押物名称
        listArrayInfo.setPawn(guarBaseInfo.getPldimnMemo());
        //是否张家港地区不动产
        listArrayInfo.setIsEstate(is_local);
        //是否电子类押品 1--电子 2--实物
        String isElectronic = "2";

        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(guarWarrantInfo.getIsEWarrant())) {
            isElectronic = "1";
        }
        listArrayInfo.setIsElectronic(isElectronic);
        //备注描述
        listArrayInfo.setRemark("");

        listArrayInfos.add(listArrayInfo);

        cwm001ReqDto.setList(listArrayInfos);

        log.info("流水号【"+ypseno+"】，发送权证借阅信息到权证系统开始："+ cwm001ReqDto);
        ResultDto<Cwm001RespDto> cwm001RespResultDto = dscms2YpqzxtClientService.cwm001(cwm001ReqDto);
        log.info("流水号【"+ypseno+"】，发送权证借阅信息到权证系统结束："+ cwm001RespResultDto);

        String code = Optional.ofNullable(cwm001RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(cwm001RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        Cwm001RespDto cwm001RespDto1;

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            cwm001RespDto1 = cwm001RespResultDto.getData();
            log.info("流水号【"+ypseno+"】，发送权证借阅信息到权证系统 成功"+ cwm001RespDto1);
        } else {
            log.error("流水号【"+ypseno+"】，发送权证借阅信息到权证系统 失败"+meesage);
            throw new BizException(null, "", null, meesage);
        }
    }

    /**
     * 将形如2021-07-01转换为形如2021/7/1的格式
     * @param preBackDate
     * @return
     */
    public String dateFormat(String preBackDate){

        if (!StringUtils.isEmpty(preBackDate)){
            preBackDate = preBackDate.replaceAll("-","/");

            if (preBackDate.charAt(5)=='0'){
                preBackDate = preBackDate.substring(0,5)+preBackDate.substring(6);
            }

            if (preBackDate.charAt(7)=='0'){
                preBackDate = preBackDate.substring(0,7)+preBackDate.substring(8);
            }
        }

        return preBackDate;
    }

    /**
     * 抵质押类型转换：押品转信贷，然后信贷转权证
     * @param qzSysGageType
     * @return
     */
    public String transferGageTypeDict(String qzSysGageType){
        //押品转信贷
        String gageType = CmisBizConstants.gageTypeMap.get(qzSysGageType);

        //抵押
        if (gageType==null){
            gageType = qzSysGageType;
        }else if(gageType.equals("10001")){
            gageType="DZY002";
        }else if(gageType.equals("10002")){
            gageType="DZY005";
        }else if(gageType.equals("20001")){
            gageType="DZY007";
        }else if(gageType.equals("20002")){
            gageType="DZY007";
        }else if(gageType.equals("10003")){
            gageType="DZY001";
        }else if(gageType.equals("10004")){
            gageType="DZY006";
        }else if(gageType.equals("10005")){
            gageType="DZY004";
        }else if(gageType.equals("10006")){
            gageType="DZY006";
        }else if(gageType.equals("10007")){
            gageType="DZY003";
        }else if(gageType.equals("10008")){
            gageType="DZY007";
        }else if(gageType.equals("10009")){
            gageType="DZY007";
        }else if(gageType.equals("10010")){
            gageType="DZY007";
        }
        else if(gageType.equals("10040")){
            gageType="DZY027";//不动产
        }
        //质押
        else if(gageType.equals("10024")){
            gageType="DZY008";
        }else if(gageType.equals("10025")){
            gageType="DZY008";
        }else if(gageType.equals("10011")){
            gageType="DZY019";
            //押品改造后，10012仅代表债券，国债变更为10042
        }else if(gageType.equals("10012")||gageType.equals("10042")){
            gageType="DZY011";
        }else if(gageType.equals("10013")){
            gageType="DZY019";
        }else if(gageType.equals("10014")){
            gageType="DZY008";
        }else if(gageType.equals("10015")){
            gageType="DZY013";
        }else if(gageType.equals("10016")){
            gageType="DZY015";
        }else if(gageType.equals("10017")){
            gageType="DZY012";
        }else if(gageType.equals("10018")){
            gageType="DZY017";
        }else if(gageType.equals("10019")){
            gageType="DZY019";
        }else if(gageType.equals("10021")){
            gageType="DZY019";
        }else if(gageType.equals("10022")){
            gageType="DZY016";
        }else if(gageType.equals("10023")){
            gageType="DZY016";
        }else if(gageType.equals("10028")){
            gageType="DZY025";
        }else if(gageType.equals("10029")){
            gageType="DZY016";
        }else if(gageType.equals("10026")){
            gageType="DZY016";
        }else if(gageType.equals("10030")){
            gageType="DZY020";
        }else if(gageType.equals("10031")){
            gageType="DZY021";
        }else if(gageType.equals("10032")){
            gageType="DZY022";
        }else if(gageType.equals("10033")){
            gageType="DZY023";
        }else if(gageType.equals("10034")){
            gageType="DZY024";
        }else if(gageType.equals("10041")){
            gageType="DZY004";
        }

        return gageType;
    }

    /**
     * 获取抵质押物名称
     * @param gageType
     * @return
     */
    public String getGageTypeNameFromAdminSmTreeDic(String gageType){
        AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
        adminSmTreeDicDto.setOptType("STD_DZY_TYPE");
        adminSmTreeDicDto.setCode(gageType);
        List<Map<String, String>> listMap = adminSmTreeDicClientService.querySingle(adminSmTreeDicDto).getData();
        if(listMap != null && listMap.size()>0){
            return listMap.get(0).get("label");
        }
        return "";
    }

    /**
     * 查询核心担保编号对应的产品是否为住房按揭贷款
     * @param coreGuarantyNo
     * @return
     */
    public String getIsMortgage(String coreGuarantyNo){
        int count = guarWarrantInfoService.selectAccommoDation(coreGuarantyNo);

        if (count>0){
            //是住房按揭贷款
            return "1";
        }
        return "0";
    }

    /**
     * 生成抵押注销任务并自动提交到集中作业
     * @param guarWarrantManageApp
     */
    public void createGuarMortgageLogOut(GuarWarrantManageApp guarWarrantManageApp){
        //权证出库流水号
        String serno = guarWarrantManageApp.getSerno();
        try {
            log.info("权证出库流水号【" + serno + "】,信贷已调押品系统和权证系统成功，是否出库到集中作业为是，生成抵押注销申请开始");
            //是否出库到集中作业为是时，生成抵押注销申请
            GuarMortgageManageApp guarMortgageManageApp = new GuarMortgageManageApp();
            //核心担保编号
            String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
            //权证编号
            String warrantNo = guarWarrantManageApp.getWarrantNo();
            //担保合同编号
            String guarContNo = guarWarrantManageApp.getGuarContNo();

            if (StringUtils.isEmpty(guarContNo)) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("coreGuarantyNo", coreGuarantyNo);
                queryModel.addCondition("warrantAppType", CmisBizConstants.STD_WARRANT_APP_TYPE_01);
                queryModel.addCondition("approveStatus", CmisCommonConstants.WF_STATUS_997);
                queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);

                //根据核心担保编号查询该笔权证出库对应的权证入库记录
                GuarWarrantManageApp guarWarrantIn = guarWarrantManageAppService.selectAll(queryModel).get(0);

                if (guarWarrantIn != null) {
                    guarContNo = guarWarrantIn.getGuarContNo();
                }
            }
            //担保合同类型
            String guarContType = null;
            //担保方式
            String guarWay = null;
            //担保金额
            BigDecimal guarAmt = null;
            //客户编号
            String cusId = null;
            //客户名称
            String cusName = null;

            if (!StringUtils.isEmpty(guarContNo)) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
                guarContType = grtGuarCont.getGuarContType();
                guarWay = grtGuarCont.getGuarWay();
                guarAmt = grtGuarCont.getGuarAmt();
            }

            //申请流水号
            String mortSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
            guarMortgageManageApp.setSerno(mortSerno);
            //抵押办理类型 默认 02--抵押注销
            guarMortgageManageApp.setRegType("02");
            //是否还款即解押
            guarMortgageManageApp.setIsRepayRemoveGuar(guarWarrantManageApp.getIsRepayRemoveGuar());
            //根据核心担保编号获取权证信息
            GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

            //抵押注销类型 01--本地电子权证注销（系统自动生成并发起流程）
            guarMortgageManageApp.setRegSubType("01");

            if (CmisCommonConstants.YES_NO_0.equals(guarWarrantInfo.getIsEWarrant())) {
                //如果是否电子权证为0--否，则抵押注销类型为02--本地纸质权证集中作业注销（纸质权证出库到集中作业的，系统自动生成并发起流程）
                guarMortgageManageApp.setRegSubType("02");
            }
            //担保合同编号
            guarMortgageManageApp.setGuarContNo(guarContNo);
            //担保合同类型
            guarMortgageManageApp.setGuarContType(guarContType);
            //担保方式
            guarMortgageManageApp.setGuarWay(guarWay);
            //担保金额
            guarMortgageManageApp.setGuarAmt(guarAmt);
            //申请原因
            guarMortgageManageApp.setRegReason("");
            //是否在线办理抵押登记/注销
            guarMortgageManageApp.setIsRegOnline(CmisCommonConstants.YES_NO_1);
            //是否先放款后抵押
            guarMortgageManageApp.setBeforehandInd("");
            //审批状态 默认000--待发起
            guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            //登记人
            guarMortgageManageApp.setInputId(guarWarrantManageApp.getManagerId());
            //登记机构
            guarMortgageManageApp.setInputBrId(guarWarrantManageApp.getManagerBrId());
            //登记日期
            guarMortgageManageApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            //最近修改人
            guarMortgageManageApp.setUpdId(guarWarrantManageApp.getManagerId());
            //最近修改机构
            guarMortgageManageApp.setUpdBrId(guarWarrantManageApp.getManagerBrId());
            //最后修改日期
            guarMortgageManageApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            //操作类型
            guarMortgageManageApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //创建时间
            guarMortgageManageApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //主管客户经理
            guarMortgageManageApp.setManagerId(guarWarrantManageApp.getManagerId());
            //主管机构
            guarMortgageManageApp.setManagerBrId(guarWarrantManageApp.getManagerBrId());
            //取该担保合同最早的一笔借款合同
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarContNo", guarContNo);
            String mainContNo = grtGuarBizRstRelService.selectContNoByGuarContNo(queryModel);

            if (StringUtils.isEmpty(mainContNo)) {
                mainContNo = guarMortgageManageAppService.selectMainContNoByGuarContNo(guarContNo);
            }
            guarMortgageManageApp.setMainContNo(mainContNo);
            //TODO 自动提交事务，防止监听到抵押注销流程后查不到该笔抵押注销申请的记录导致无法将其审批状态改成审批中
            //                guarMortgageManageAppService.insertAutoCommit(guarMortgageManageApp);
            guarMortgageManageAppService.insert(guarMortgageManageApp);

            QueryModel model = new QueryModel();
            model.addCondition("coreGuarantyNo", coreGuarantyNo);
            model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
            List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectAll(model);

            if (CollectionUtils.isNotEmpty(guarContRelWarrants)) {

                for (GuarContRelWarrant guarContRelWarrant : guarContRelWarrants) {
                    GuarMortgageManageRel guarMortgageManageRel = new GuarMortgageManageRel();
                    //押品编号
                    String guarNo = guarContRelWarrant.getGuarNo();
                    GuarBaseInfo guarBaseInfo = guarBaseInfoService.getGuarBaseInfoByGuarNo(guarNo);
                    //申请流水号
                    guarMortgageManageRel.setSerno(guarMortgageManageApp.getSerno());
                    //押品编号
                    guarMortgageManageRel.setGuarNo(guarNo);
                    //押品名称
                    guarMortgageManageRel.setPldimnMemo(guarBaseInfo.getPldimnMemo());
                    //抵押/质押标识
                    guarMortgageManageRel.setGuarType(guarWarrantManageApp.getGrtFlag());
                    //押品所有人编号
                    guarMortgageManageRel.setGuarCusId(guarBaseInfo.getGuarCusId());
                    cusId = guarBaseInfo.getGuarCusId();
                    //押品所有人名称
                    guarMortgageManageRel.setGuarCusName(guarBaseInfo.getGuarCusName());
                    cusName = guarBaseInfo.getGuarCusName();
                    //押品所有人证件类型
                    guarMortgageManageRel.setGuarCertType(guarBaseInfo.getGuarCertType());
                    //押品所有人证件号码
                    guarMortgageManageRel.setGuarCertCode(guarBaseInfo.getGuarCertCode());
                    //押品评估价值
                    guarMortgageManageRel.setEvalAmt(guarBaseInfo.getEvalAmt());
                    //押品认定价值
                    guarMortgageManageRel.setConfirmAmt(guarBaseInfo.getConfirmAmt());
                    //抵质押标识
                    guarMortgageManageRel.setGrtFlag(guarBaseInfo.getGrtFlag());
                    //担保分类代码
                    guarMortgageManageRel.setGuarTypeCd(guarBaseInfo.getGuarTypeCd());
                    String gageTypeName = getGageTypeNameFromAdminSmTreeDic(guarBaseInfo.getGuarTypeCd());
                    //担保分类名称
                    guarMortgageManageRel.setGuarTypeCdName(gageTypeName);
                    //权证编号
                    guarMortgageManageRel.setWarrantNo(warrantNo);
                    //核心担保编号
                    guarMortgageManageRel.setCoreGuarantyNo(coreGuarantyNo);
                    guarMortgageManageRelMapper.insert(guarMortgageManageRel);
                }
            }
            log.info("流水号【" + serno + "】,是否出库到集中作业为是，生成抵押注销申请结束");

            //提交抵押注销流程
            submitMortgage(guarMortgageManageApp, mortSerno, cusId, cusName);

            guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
            guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
        }catch (BizException e){
            log.error("流水号【" + serno + "】，自动生成抵押注销申请失败，BizException：",e.getMessage());
        }catch (Exception e){
            log.error("流水号【" + serno + "】，自动生成抵押注销申请失败，Exception：",e.getMessage());
        }
    }

    /**
     * 提交抵押注销流程
     * @param guarMortgageManageApp
     * @param serno
     * @param cusId
     * @param cusName
     */
    public void submitMortgage(GuarMortgageManageApp guarMortgageManageApp,String serno,String cusId,String cusName){
        log.info("流水号【"+serno+"】,流程发起操作 开始");
        WFStratDto wfStratDto = new WFStratDto();
        wfStratDto.setSystemId("cmis");
        wfStratDto.setOrgId(guarMortgageManageApp.getManagerBrId());
        wfStratDto.setUserId(guarMortgageManageApp.getManagerId());
        wfStratDto.setBizId(guarMortgageManageApp.getSerno());
        wfStratDto.setBizType("DB009");
        wfStratDto.setBizUserName(cusName);
        wfStratDto.setBizUserId(cusId);

        Map<String,Object> param = new HashMap<>();
        param.put("regSubType",guarMortgageManageApp.getRegSubType());
        param.put("isRegOnline",guarMortgageManageApp.getIsRegOnline());
        param.put("isAutoCommit",CmisBizConstants.STD_ZB_YES_NO_Y);

        wfStratDto.setParam(param);

        log.info("WFStratDto:"+wfStratDto);

        ResultDto<ResultInstanceDto> resultDto = workflowCoreClient.start(wfStratDto);
        log.info("流水号【"+serno+"】,流程发起操作 结束");

        log.info("resultDto:"+resultDto);

        String instanceId = resultDto.getData().getInstanceId();
        String nodeId = resultDto.getData().getNodeId();

        ResultDto<ResultInstanceDto> instanceResultDto = workflowCoreClient.instanceInfo(instanceId, nodeId,null);
        ResultInstanceDto instanceInfo = instanceResultDto.getData();

        log.info("流水号【"+serno+"】,获取下一节点 开始");
        WFNextNodeDto wFNextNodeDto = new WFNextNodeDto();
        wFNextNodeDto.setInstanceId(instanceInfo.getInstanceId());
        wFNextNodeDto.setNodeId(instanceInfo.getNodeId());
        wFNextNodeDto.setParam(instanceInfo.getParam());
        ResultDto<List<ResultNodeDto>> resultNodeDto = workflowCoreClient.getNextNodeInfos(wFNextNodeDto);

        log.info("流水号【"+serno+"】,获取下一节点 结束");

        if("0".equals(resultNodeDto.getCode()) && cn.com.yusys.yusp.commons.util.collection.CollectionUtils.nonEmpty(resultNodeDto.getData())) {
            List<NextNodeInfoDto> submitNextNodeInfoList = new ArrayList<>();
            List<ResultNodeDto> nextAccessNodeList = resultNodeDto.getData();

            for (ResultNodeDto resultNode : nextAccessNodeList) {
                NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
                nextNodeInfoDto.setNextNodeId(resultNode.getNodeId());
                List<String> userIdsList = new ArrayList<>();
                if (cn.com.yusys.yusp.commons.util.collection.CollectionUtils.nonEmpty(resultNode.getUsers())) {
                    resultNode.getUsers().stream().forEach(item -> {
                        userIdsList.add(item.getUserId());
                    });
                } else {
                    throw BizException.error(null, "999999", "节点办理人为空，提交失败！");
                }
                nextNodeInfoDto.setNextNodeUserIds(userIdsList);
                submitNextNodeInfoList.add(nextNodeInfoDto);
            }

            log.info("流水号【"+serno+"】,流程提交操作 开始");
            //流程提交
            WFSubmitDto wFSubmitDto = new WFSubmitDto();
            wFSubmitDto.setOrgId(instanceInfo.getOrgId());
            wFSubmitDto.setNextNodeInfos(submitNextNodeInfoList);
            wFSubmitDto.setParam(instanceInfo.getParam());
            WFCommentDto wFCommentDto = new WFCommentDto();
            wFCommentDto.setCommentSign("O-12");
            wFCommentDto.setUserComment("同意");
            wFCommentDto.setInstanceId(instanceInfo.getInstanceId());
            wFCommentDto.setNodeId(instanceInfo.getNodeId());
            wFCommentDto.setUserId(guarMortgageManageApp.getManagerId());
            wFSubmitDto.setComment(wFCommentDto);

            log.info("wFCommentDto:"+wFCommentDto);
            log.info("wFSubmitDto:"+wFSubmitDto);
            ResultDto<List<ResultMessageDto>> commmitResultDto = workflowCoreClient.submit(wFSubmitDto);

            log.info("流程提交响应结果：{}", commmitResultDto);
            if (!"0".equals(commmitResultDto.getCode())) {
                throw BizException.error(null, "999999", "流程提交失败！");
            } else {
                if (commmitResultDto.getData().size() > 0 && "提交完成".equals(commmitResultDto.getData().get(0).getTip())) {
                    log.info("流程提交成功：{}", commmitResultDto.getData().get(0));
                } else {
                    throw BizException.error(null, "999999", commmitResultDto.getData().get(0).getTip());
                }

                log.info("流水号【"+serno+"】,集中作业中心档案池生成档案接受任务 开始");
                createCentralFileTask(guarMortgageManageApp,serno,instanceId,submitNextNodeInfoList.get(0).getNextNodeId(),"DB009");
                log.info("流水号【"+serno+"】,集中作业中心档案池生成档案接受任务 结束");
            }
        }
    }

    /**
     * 生成集中作业档案池任务
     * @param taskSerno
     * @param bizType
     */
    public void createCentralFileTask(GuarMortgageManageApp guarMortgageManageApp,String taskSerno,String instanceId,String nodeId,String bizType){
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(taskSerno);

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",guarMortgageManageApp.getSerno());
        List<GuarMortgageManageRel> guarMortgageManageRels = guarMortgageManageRelService.selectAll(queryModel);

        if (CollectionUtils.isNotEmpty(guarMortgageManageRels)){
            GuarMortgageManageRel guarMortgageManageRel = guarMortgageManageRels.get(0);
            //客户编号取押品所有人客户编号
            centralFileTaskdto.setCusId(guarMortgageManageRel.getGuarCusId());
            //客户名称取押品所有人客户名称
            centralFileTaskdto.setCusName(guarMortgageManageRel.getGuarCusName());
            //全局流水号取核心担保编号
            centralFileTaskdto.setTraceId(guarMortgageManageRel.getCoreGuarantyNo());
        }

        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(guarMortgageManageApp.getInputId());
        centralFileTaskdto.setInputBrId(guarMortgageManageApp.getInputBrId());

        //档案任务操作类型 01--纯指令
        centralFileTaskdto.setOptType(CmisBizConstants.STD_OPT_TYPE_01);
        centralFileTaskdto.setInstanceId(instanceId);
        centralFileTaskdto.setNodeId(nodeId);
        //档案任务类型 03--派发
        centralFileTaskdto.setTaskType(CmisBizConstants.STD_FILE_TASK_TYPE_03);
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急

        centralFileTaskService.insertSelective(centralFileTaskdto);
    }
}