package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAptiInfo
 * @类描述: coop_plan_apti_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-13 21:32:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanAptiInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 合作方类型 **/
	private String partnerType;
	
	/** 合作方编号 **/
	private String partnerNo;

	/** 合作方编号 **/
	private String partnerName;
	
	/** 证书类型 **/
	private String liceType;
	
	/** 证书名称 **/
	private String liceName;
	
	/** 证书编号 **/
	private String liceNo;
	
	/** 资质等级 **/
	private String aptiLvl;
	
	/** 资质证书登记机构 **/
	private String aptiLiceRegiOrg;
	
	/** 资质证书登记日期 **/
	private String aptiLiceRegiDate;
	
	/** 资质证书到期日期 **/
	private String aptiLiceEndDate;
	
	/** 资质说明 **/
	private String aptiDesc;
	
	/** 是否我行信贷客户 **/
	private String isBankCretCus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType == null ? null : partnerType.trim();
	}
	
    /**
     * @return PartnerType
     */	
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo == null ? null : partnerNo.trim();
	}
	
    /**
     * @return PartnerNo
     */	
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param liceType
	 */
	public void setLiceType(String liceType) {
		this.liceType = liceType == null ? null : liceType.trim();
	}
	
    /**
     * @return LiceType
     */	
	public String getLiceType() {
		return this.liceType;
	}
	
	/**
	 * @param liceName
	 */
	public void setLiceName(String liceName) {
		this.liceName = liceName == null ? null : liceName.trim();
	}
	
    /**
     * @return LiceName
     */	
	public String getLiceName() {
		return this.liceName;
	}
	
	/**
	 * @param liceNo
	 */
	public void setLiceNo(String liceNo) {
		this.liceNo = liceNo == null ? null : liceNo.trim();
	}
	
    /**
     * @return LiceNo
     */	
	public String getLiceNo() {
		return this.liceNo;
	}
	
	/**
	 * @param aptiLvl
	 */
	public void setAptiLvl(String aptiLvl) {
		this.aptiLvl = aptiLvl == null ? null : aptiLvl.trim();
	}
	
    /**
     * @return AptiLvl
     */	
	public String getAptiLvl() {
		return this.aptiLvl;
	}
	
	/**
	 * @param aptiLiceRegiOrg
	 */
	public void setAptiLiceRegiOrg(String aptiLiceRegiOrg) {
		this.aptiLiceRegiOrg = aptiLiceRegiOrg == null ? null : aptiLiceRegiOrg.trim();
	}
	
    /**
     * @return AptiLiceRegiOrg
     */	
	public String getAptiLiceRegiOrg() {
		return this.aptiLiceRegiOrg;
	}
	
	/**
	 * @param aptiLiceRegiDate
	 */
	public void setAptiLiceRegiDate(String aptiLiceRegiDate) {
		this.aptiLiceRegiDate = aptiLiceRegiDate == null ? null : aptiLiceRegiDate.trim();
	}
	
    /**
     * @return AptiLiceRegiDate
     */	
	public String getAptiLiceRegiDate() {
		return this.aptiLiceRegiDate;
	}
	
	/**
	 * @param aptiLiceEndDate
	 */
	public void setAptiLiceEndDate(String aptiLiceEndDate) {
		this.aptiLiceEndDate = aptiLiceEndDate == null ? null : aptiLiceEndDate.trim();
	}
	
    /**
     * @return AptiLiceEndDate
     */	
	public String getAptiLiceEndDate() {
		return this.aptiLiceEndDate;
	}
	
	/**
	 * @param aptiDesc
	 */
	public void setAptiDesc(String aptiDesc) {
		this.aptiDesc = aptiDesc == null ? null : aptiDesc.trim();
	}
	
    /**
     * @return AptiDesc
     */	
	public String getAptiDesc() {
		return this.aptiDesc;
	}
	
	/**
	 * @param isBankCretCus
	 */
	public void setIsBankCretCus(String isBankCretCus) {
		this.isBankCretCus = isBankCretCus == null ? null : isBankCretCus.trim();
	}
	
    /**
     * @return IsBankCretCus
     */	
	public String getIsBankCretCus() {
		return this.isBankCretCus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}