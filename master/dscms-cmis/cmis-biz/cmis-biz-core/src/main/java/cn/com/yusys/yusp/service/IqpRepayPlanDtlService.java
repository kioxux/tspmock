/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRepayPlanDtl;
import cn.com.yusys.yusp.repository.mapper.IqpRepayPlanDtlMapper;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpRepayPlanDtlService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-03 15:10:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpRepayPlanDtlService {

    @Autowired
    private IqpRepayPlanDtlMapper iqpRepayPlanDtlMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpRepayPlanDtl selectByPrimaryKey(String pkId) {
        return iqpRepayPlanDtlMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpRepayPlanDtl> selectAll(QueryModel model) {
        List<IqpRepayPlanDtl> records = (List<IqpRepayPlanDtl>) iqpRepayPlanDtlMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpRepayPlanDtl> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayPlanDtl> list = iqpRepayPlanDtlMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpRepayPlanDtl record) {
        return iqpRepayPlanDtlMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpRepayPlanDtl record) {
        return iqpRepayPlanDtlMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpRepayPlanDtl record) {
        return iqpRepayPlanDtlMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpRepayPlanDtl record) {
        return iqpRepayPlanDtlMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpRepayPlanDtlMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRepayPlanDtlMapper.deleteByIds(ids);
    }

    /**
     * 保存还款计划信息
     * @param iqpRepayPlanDtl
     * @return
     */
    public int insertIqpRepayPlanDtl(IqpRepayPlanDtl iqpRepayPlanDtl) {
        return iqpRepayPlanDtlMapper.insert(iqpRepayPlanDtl);
    }
}
