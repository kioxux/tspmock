/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtFinGuarList;
import cn.com.yusys.yusp.service.LmtFinGuarListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtfinguarlist")
public class LmtFinGuarListResource {
    @Autowired
    private LmtFinGuarListService lmtFinGuarListService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtFinGuarList>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtFinGuarList> list = lmtFinGuarListService.selectAll(queryModel);
        return new ResultDto<List<LmtFinGuarList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtFinGuarList>> index(QueryModel queryModel) {
        List<LmtFinGuarList> list = lmtFinGuarListService.selectByModel(queryModel);
        return new ResultDto<List<LmtFinGuarList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtFinGuarList> show(@PathVariable("pkId") String pkId) {
        LmtFinGuarList lmtFinGuarList = lmtFinGuarListService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtFinGuarList>(lmtFinGuarList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtFinGuarList> create(@RequestBody LmtFinGuarList lmtFinGuarList) throws URISyntaxException {
        lmtFinGuarListService.insert(lmtFinGuarList);
        return new ResultDto<LmtFinGuarList>(lmtFinGuarList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtFinGuarList lmtFinGuarList) throws URISyntaxException {
        int result = lmtFinGuarListService.update(lmtFinGuarList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtFinGuarListService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtFinGuarListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: selectBySerno
     * @函数描述: 根据流水号查询担保公司授信明细
     * @参数: serno
     * */
    @PostMapping("/selectbyserno")
    protected ResultDto<List<LmtFinGuarList>> selectBySerno(String serno){
        List<LmtFinGuarList> list = lmtFinGuarListService.selectBySerno(serno);
        return new ResultDto<List<LmtFinGuarList>>(list);
    }
}
