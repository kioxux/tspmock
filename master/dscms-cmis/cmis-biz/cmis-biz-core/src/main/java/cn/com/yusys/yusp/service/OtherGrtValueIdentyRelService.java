/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import cn.com.yusys.yusp.domain.OtherGrtValueBranchIdentySub;
import cn.com.yusys.yusp.domain.OtherGrtValueCadaIdentySub;
import cn.com.yusys.yusp.dto.OtherGrtDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueBranchIdentySubMapper;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueCadaIdentySubMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherGrtValueIdentyRel;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueIdentyRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueIdentyRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 20:39:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherGrtValueIdentyRelService {
    private static final Logger log = LoggerFactory.getLogger(OtherGrtValueIdentyRelService.class);

    @Autowired
    private OtherGrtValueIdentyRelMapper otherGrtValueIdentyRelMapper;

    @Autowired
    private OtherGrtValueCadaIdentySubMapper otherGrtValueCadaIdentySubMapper;

    @Autowired
    private OtherGrtValueBranchIdentySubMapper otherGrtValueBranchIdentySubMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherGrtValueIdentyRel selectByPrimaryKey(String pkId) {
        return otherGrtValueIdentyRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherGrtValueIdentyRel> selectAll(QueryModel model) {
        List<OtherGrtValueIdentyRel> records = (List<OtherGrtValueIdentyRel>) otherGrtValueIdentyRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherGrtValueIdentyRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherGrtValueIdentyRel> list = otherGrtValueIdentyRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherGrtValueIdentyRel record) {
        return otherGrtValueIdentyRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherGrtValueIdentyRel record) {
        return otherGrtValueIdentyRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherGrtValueIdentyRel record) {
        return otherGrtValueIdentyRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherGrtValueIdentyRel record) {
        return otherGrtValueIdentyRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return otherGrtValueIdentyRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherGrtValueIdentyRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: addothergrtvalueidentyrel
     * @方法描述: 新增抵押物情况
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addothergrtvalueidentyrel(OtherGrtDto otherGrtDto) {
        {
            try {
                //1、授信抵质押物价值认定与抵质押物关联表
                OtherGrtValueIdentyRel otherGrtValueIdentyRel = new OtherGrtValueIdentyRel();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueIdentyRel);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherGrtValueIdentyRel.setOprType(CmisBizConstants.OPR_TYPE_01);
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherGrtValueIdentyRel.setInputId(user.getLoginCode());
                otherGrtValueIdentyRel.setInputBrId(user.getOrg().getCode());
                otherGrtValueIdentyRel.setInputDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyRel.setCreateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyRel.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyRel.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyRel.setUpdId(user.getLoginCode());
                otherGrtValueIdentyRel.setUpdBrId(user.getOrg().getCode());
                int i = otherGrtValueIdentyRelMapper.insertSelective(otherGrtValueIdentyRel);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：授信抵质押物价值认定与抵质押物关联表异常");
                }

                //2、授信抵质押物价值云评估明细
                OtherGrtValueCadaIdentySub otherGrtValueCadaIdentySub = new OtherGrtValueCadaIdentySub();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueCadaIdentySub);
                //调整克隆对象
                /** 房产评估价值**/
                BigDecimal realproEvalValue  = (otherGrtDto.getRealproEvalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproEvalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setRealproEvalValue(realproEvalValue);
                /** 土地评估价值 **/
                BigDecimal landEvalValue = (otherGrtDto.getLandEvalValue()==null?BigDecimal.ZERO:otherGrtDto.getLandEvalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setLandEvalValue(landEvalValue);
                /** 房地产评估总价 **/
                BigDecimal realproEvalTotalValue = (otherGrtDto.getRealproEvalTotalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproEvalTotalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setRealproEvalTotalValue(realproEvalTotalValue);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherGrtValueCadaIdentySub.setOprType(CmisBizConstants.OPR_TYPE_01);
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                otherGrtValueCadaIdentySub.setInputId(user.getLoginCode());
                otherGrtValueCadaIdentySub.setInputBrId(user.getOrg().getCode());
                otherGrtValueCadaIdentySub.setInputDate(dateFormat.format(new Date()) );
                otherGrtValueCadaIdentySub.setCreateTime(DateUtils.getCurrDate());
                otherGrtValueCadaIdentySub.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueCadaIdentySub.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueCadaIdentySub.setUpdId(user.getLoginCode());
                otherGrtValueCadaIdentySub.setUpdBrId(user.getOrg().getCode());
                int j = otherGrtValueCadaIdentySubMapper.insertSelective(otherGrtValueCadaIdentySub);
                if (j != 1) {
                    throw BizException.error(null, "999999", "新增：授信抵质押物价值云评估明细异常");
                }

                //3、授信抵质押物价值支行明细
                OtherGrtValueBranchIdentySub otherGrtValueBranchIdentySub = new OtherGrtValueBranchIdentySub();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueBranchIdentySub);
                //调整克隆对象
                /** 房产确认价值**/
                BigDecimal realproCfirmValue  = (otherGrtDto.getRealproCfirmValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproCfirmValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setRealproCfirmValue(realproCfirmValue);
                /** 土地确认价值 **/
                BigDecimal landCfirmValue = (otherGrtDto.getLandCfirmValue()==null?BigDecimal.ZERO:otherGrtDto.getLandCfirmValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setLandCfirmValue(landCfirmValue);
                /** 房地产确认总价 **/
                BigDecimal realproCfirmTotalValue = (otherGrtDto.getRealproCfirmTotalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproCfirmTotalValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setRealproCfirmTotalValue(realproCfirmTotalValue);
                /** 该抵押物项下申请的融资金额 **/
                BigDecimal pldAppFinPrice = (otherGrtDto.getPldAppFinPrice()==null?BigDecimal.ZERO:otherGrtDto.getPldAppFinPrice()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setPldAppFinPrice(pldAppFinPrice);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherGrtValueBranchIdentySub.setOprType(CmisBizConstants.OPR_TYPE_01);
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                otherGrtValueBranchIdentySub.setInputId(user.getLoginCode());
                otherGrtValueBranchIdentySub.setInputBrId(user.getOrg().getCode());
                otherGrtValueBranchIdentySub.setInputDate(dateFormat.format(new Date()) );
                otherGrtValueBranchIdentySub.setCreateTime(DateUtils.getCurrDate());
                otherGrtValueBranchIdentySub.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueBranchIdentySub.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueBranchIdentySub.setUpdId(user.getLoginCode());
                otherGrtValueBranchIdentySub.setUpdBrId(user.getOrg().getCode());
                int l = otherGrtValueBranchIdentySubMapper.insertSelective(otherGrtValueBranchIdentySub);
                if (l != 1) {
                    throw BizException.error(null, "999999", "新增：授信抵质押物价值支行明细异常");
                }
            }catch (Exception e) {
                log.error("抵押物新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherGrtDto);

        }
    }

    /**
     * @方法名称: updateothergrtvalueidentyrel
     * @方法描述: 更新抵押物情况
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateothergrtvalueidentyrel(OtherGrtDto otherGrtDto) {
            try {
                //1、授信抵质押物价值认定与抵质押物关联表
                OtherGrtValueIdentyRel otherGrtValueIdentyRel = new OtherGrtValueIdentyRel();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueIdentyRel);
                //补充 克隆缺少数据
                OtherGrtValueIdentyRel oldotherGrtValueIdentyRel = otherGrtValueIdentyRelMapper.selectByPrimaryKey(otherGrtValueIdentyRel.getPkId());
                //放入必填参数 操作类型 新增
                otherGrtValueIdentyRel.setOprType(oldotherGrtValueIdentyRel.getOprType());
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherGrtValueIdentyRel.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyRel.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyRel.setUpdId(user.getLoginCode());
                otherGrtValueIdentyRel.setUpdBrId(user.getOrg().getCode());
                int i = otherGrtValueIdentyRelMapper.updateByPrimaryKeySelective(otherGrtValueIdentyRel);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：抵押物申请异常");
                }

                //2、授信抵质押物价值云评估明细
                OtherGrtValueCadaIdentySub otherGrtValueCadaIdentySub = new OtherGrtValueCadaIdentySub();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueCadaIdentySub);
                //调整克隆对象
                /** 房产评估价值**/
                BigDecimal realproEvalValue  = (otherGrtDto.getRealproEvalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproEvalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setRealproEvalValue(realproEvalValue);
                /** 土地评估价值 **/
                BigDecimal landEvalValue = (otherGrtDto.getLandEvalValue()==null?BigDecimal.ZERO:otherGrtDto.getLandEvalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setLandEvalValue(landEvalValue);
                /** 房地产评估总价 **/
                BigDecimal realproEvalTotalValue = (otherGrtDto.getRealproEvalTotalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproEvalTotalValue()).multiply(new BigDecimal(10000));
                otherGrtValueCadaIdentySub.setRealproEvalTotalValue(realproEvalTotalValue);
                //补充 克隆缺少数据
                OtherGrtValueCadaIdentySub oldotherGrtValueCadaIdentySub = otherGrtValueCadaIdentySubMapper.selectByPrimaryKey(otherGrtValueCadaIdentySub.getPkId());
                //放入必填参数 操作类型 新增
                otherGrtValueCadaIdentySub.setOprType(oldotherGrtValueCadaIdentySub.getOprType());
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                otherGrtValueCadaIdentySub.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueCadaIdentySub.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueCadaIdentySub.setUpdId(user.getLoginCode());
                otherGrtValueCadaIdentySub.setUpdBrId(user.getOrg().getCode());
                int j = otherGrtValueCadaIdentySubMapper.updateByPrimaryKeySelective(otherGrtValueCadaIdentySub);
                if (j != 1) {
                    throw BizException.error(null, "999999", "修改：抵押物申请异常");
                }

                //3、授信抵质押物价值支行明细
                OtherGrtValueBranchIdentySub otherGrtValueBranchIdentySub = new OtherGrtValueBranchIdentySub();
                //克隆对象
                BeanUtils.copyProperties(otherGrtDto, otherGrtValueBranchIdentySub);
                //调整克隆对象
                /** 房产确认价值**/
                BigDecimal realproCfirmValue  = (otherGrtDto.getRealproCfirmValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproCfirmValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setRealproCfirmValue(realproCfirmValue);
                /** 土地确认价值 **/
                BigDecimal landCfirmValue = (otherGrtDto.getLandCfirmValue()==null?BigDecimal.ZERO:otherGrtDto.getLandCfirmValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setLandCfirmValue(landCfirmValue);
                /** 房地产确认总价 **/
                BigDecimal realproCfirmTotalValue = (otherGrtDto.getRealproCfirmTotalValue()==null?BigDecimal.ZERO:otherGrtDto.getRealproCfirmTotalValue()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setRealproCfirmTotalValue(realproCfirmTotalValue);
                /** 该抵押物项下申请的融资金额 **/
                BigDecimal pldAppFinPrice = (otherGrtDto.getPldAppFinPrice()==null?BigDecimal.ZERO:otherGrtDto.getPldAppFinPrice()).multiply(new BigDecimal(10000));
                otherGrtValueBranchIdentySub.setPldAppFinPrice(pldAppFinPrice);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                //补充 克隆缺少数据
                OtherGrtValueBranchIdentySub oldotherGrtValueBranchIdentySub = otherGrtValueBranchIdentySubMapper.selectByPrimaryKey(otherGrtValueBranchIdentySub.getPkId());
                //放入必填参数 操作类型 新增
                otherGrtValueBranchIdentySub.setOprType(oldotherGrtValueBranchIdentySub.getOprType());
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                otherGrtValueBranchIdentySub.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueBranchIdentySub.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueBranchIdentySub.setUpdId(user.getLoginCode());
                otherGrtValueBranchIdentySub.setUpdBrId(user.getOrg().getCode());
                int l = otherGrtValueBranchIdentySubMapper.updateByPrimaryKeySelective(otherGrtValueBranchIdentySub);
                if (l != 1) {
                    throw BizException.error(null, "999999", "修改：授信抵质押物价值支行明细异常");
                }

            }catch (Exception e) {
                log.error("外币利率修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherGrtDto);

        }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteInfo(String pkId) {
        //转换日期 ASSURE_CERT_CODE
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        User user = SessionUtils.getUserInformation();
        int num = 0;
        //1、授信抵质押物价值认定与抵质押物关联表
        OtherGrtValueIdentyRel otherGrtValueIdentyRel = otherGrtValueIdentyRelMapper.selectByPrimaryKey(pkId);
        if(Objects.nonNull(otherGrtValueIdentyRel)){
            //操作类型  删除
            otherGrtValueIdentyRel.setOprType(CmisBizConstants.OPR_TYPE_02);
            otherGrtValueIdentyRel.setUpdateTime(DateUtils.getCurrDate());
            otherGrtValueIdentyRel.setUpdDate(dateFormat.format(new Date()) );
            otherGrtValueIdentyRel.setUpdId(user.getLoginCode());
            otherGrtValueIdentyRel.setUpdBrId(user.getOrg().getCode());

            num = otherGrtValueIdentyRelMapper.updateByPrimaryKeySelective(otherGrtValueIdentyRel);
            if (num != 1) {
                throw BizException.error(null, "999999", "删除：抵押物申请异常");
            }
        }

        //2、授信抵质押物价值云评估明细
        OtherGrtValueCadaIdentySub otherGrtValueCadaIdentySub = otherGrtValueCadaIdentySubMapper.selectByPrimaryKey(pkId);
        if(Objects.nonNull(otherGrtValueCadaIdentySub)){
            //放入必填参数 操作类型 新增
            otherGrtValueCadaIdentySub.setOprType(CmisBizConstants.OPR_TYPE_02);
            //时间 登记人 等级机构相关
            //转换日期 ASSURE_CERT_CODE
            otherGrtValueCadaIdentySub.setUpdateTime(DateUtils.getCurrDate());
            otherGrtValueCadaIdentySub.setUpdDate(dateFormat.format(new Date()) );
            otherGrtValueCadaIdentySub.setUpdId(user.getLoginCode());
            otherGrtValueCadaIdentySub.setUpdBrId(user.getOrg().getCode());
            num = otherGrtValueCadaIdentySubMapper.updateByPrimaryKeySelective(otherGrtValueCadaIdentySub);
            if (num != 1) {
                throw BizException.error(null, "999999", "删除：抵押物申请异常");
            }
        }
        //3、授信抵质押物价值支行明细
        OtherGrtValueBranchIdentySub otherGrtValueBranchIdentySub = otherGrtValueBranchIdentySubMapper.selectByPrimaryKey(pkId);
        if(Objects.nonNull(otherGrtValueBranchIdentySub)){
            //放入必填参数 操作类型 新增
            otherGrtValueBranchIdentySub.setOprType(CmisBizConstants.OPR_TYPE_02);
            //时间 登记人 等级机构相关
            //转换日期 ASSURE_CERT_CODE
            otherGrtValueBranchIdentySub.setUpdateTime(DateUtils.getCurrDate());
            otherGrtValueBranchIdentySub.setUpdDate(dateFormat.format(new Date()) );
            otherGrtValueBranchIdentySub.setUpdId(user.getLoginCode());
            otherGrtValueBranchIdentySub.setUpdBrId(user.getOrg().getCode());
            num = otherGrtValueBranchIdentySubMapper.updateByPrimaryKeySelective(otherGrtValueBranchIdentySub);
            if (num != 1) {
                throw BizException.error(null, "999999", "删除：授信抵质押物价值支行明细异常");
            }
        }

        return num;
    }
}
