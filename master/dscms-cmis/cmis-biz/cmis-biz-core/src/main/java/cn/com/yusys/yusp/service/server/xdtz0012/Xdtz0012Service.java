package cn.com.yusys.yusp.service.server.xdtz0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.req.Xdtz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.resp.Xdtz0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdtz0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-08 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0012Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0012Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Resource
    private LmtAppMapper lmtAppMapper;//授信申请
    @Resource
    private LmtReplyMapper lmtReplyMapper;//授信批复
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;//合同主表
    @Autowired
    private CommonService commonService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    /**
     * 根据借款人证件号，判断配偶经营性贷款是否存在余额
     *
     * @param xdtz0012DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0012DataRespDto getXdtz0012(Xdtz0012DataReqDto xdtz0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataReqDto));
        Xdtz0012DataRespDto xdtz0012DataRespDto = new Xdtz0012DataRespDto();
        try {
            String certNo = xdtz0012DataReqDto.getCertNo();
            String certType = xdtz0012DataReqDto.getCertNo();
            String cusId = "";//配偶客户号
            String cusIdlist = "";//配偶客户号(及作为高管或法人的对公客户号)
            String spCertNo = "";//配偶证件号
            String managerId = "";
            String managerBrId = "";
            //翻译用户名称和机构名称
            String managerName = "";
            String orgName = "";
            //根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号
            logger.info("**********XDTZ0012**根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号开始,查询参数为:{}", JSON.toJSONString(certNo));
            ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(certNo);
            logger.info("**********XDTZ0012**根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号结束,返回结果为:{}", JSON.toJSONString(cmisCus0013RespDtoResultDto));
            if (ResultDto.success().getCode().equals(cmisCus0013RespDtoResultDto.getCode())) {
                CmisCus0013RespDto cmisCus0013RespDto = cmisCus0013RespDtoResultDto.getData();
                if (!Objects.isNull(cmisCus0013RespDto)) {
                    cusId = cmisCus0013RespDto.getCusId();
                    cusIdlist = cusId;
                    spCertNo = cmisCus0013RespDto.getCertCode();
                    //通过客户证件号查询客户信息
                    logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(spCertNo));
                    CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(spCertNo)).orElse(new CusBaseClientDto());
                    logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(spCertNo), JSON.toJSONString(cusBaseClientDto));
                    managerId = cusBaseClientDto.getManagerId();
                    managerBrId = cusBaseClientDto.getManagerBrId();
                }
            }
            if (StringUtil.isEmpty(cusId)) {//不存在配偶信息
                xdtz0012DataRespDto.setIsHavingSpouse(CommonConstance.STD_YES_OR_NO_N);
                xdtz0012DataRespDto.setIsHavingOperBillBal(CommonConstance.STD_YES_OR_NO_N);
            } else {
                //存在配偶信息
                xdtz0012DataRespDto.setIsHavingSpouse(CommonConstance.STD_YES_OR_NO_Y);
                //翻译用户名称和机构名称
                if (StringUtils.isNotEmpty(managerId) && StringUtils.isNotEmpty(managerBrId)) {
                    AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(managerId);
                    logger.info("根据客户经理号【{}】查询用户信息结束,返回用户信息【{}】", managerId, JSON.toJSONString(adminSmUserDto));
                    if (Objects.nonNull(adminSmUserDto)) {
                        managerName = adminSmUserDto.getUserName();
                    }
                    AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(managerBrId);
                    logger.info("根据机构号【{}】查询机构信息结束,返回机构信息【{}】", managerBrId, JSON.toJSONString(adminSmOrgDto));
                    if (Objects.nonNull(adminSmOrgDto)) {
                        orgName = adminSmOrgDto.getOrgName();
                    }
                }
                xdtz0012DataRespDto.setManagerId(managerId);
                xdtz0012DataRespDto.setManagerName(managerName);
                xdtz0012DataRespDto.setOrgNo(managerBrId);
                xdtz0012DataRespDto.setOrgName(orgName);

                /*************************************查询是否存在配偶作为法人或者实际控制人的对公客户信息************************************************/
                // 客户列表
                List<String> cusIdList = new ArrayList<String>();
                cusIdList.add(cusId);
                CmisCus0012ReqDto cmisCus0012ReqDto = new CmisCus0012ReqDto();
                cmisCus0012ReqDto.setCusId(cusId);
                cmisCus0012ReqDto.setCertCode(spCertNo);
                // 法人代表为200400;实际控制人201200
                cmisCus0012ReqDto.setMrgType("200400,201200");
                cmisCus0012ReqDto.setQueryType("1");//多高管类别查询
                // 1、查询担任法人的企业客户号
                String cusIds = "";
                logger.info("***********调用客户服务cmiscus0012查询担任法人的企业客户号开始,查询参数为:{}", JSON.toJSONString(cmisCus0012ReqDto));
                ResultDto<CmisCus0012RespDto> cmisCus0012RespDtoResultDto = cmisCusClientService.cmiscus0012(cmisCus0012ReqDto);
                logger.info("***********调用客户服务cmiscus0012查询担任法人的企业客户号结束*END,返回结果为:{}", JSON.toJSONString(cmisCus0012RespDtoResultDto));
                if (cmisCus0012RespDtoResultDto != null) {
                    CmisCus0012RespDto cmisCus0012RespDto = cmisCus0012RespDtoResultDto.getData();
                    String code = cmisCus0012RespDtoResultDto.getCode();
                    if (StringUtils.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        if (cmisCus0012RespDto != null) {
                            List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> list = cmisCus0012RespDto.getList();
                            for (int i = 0; i < list.size(); i++) {
                                cusIds += list.get(i).getCusId();
                                if (i < list.size() - 1) {
                                    cusIds += ",";
                                }
                            }
                        }
                    }
                }
                //如果高管信息不为空
                if (StringUtils.isNotEmpty(cusIds)) {
                    cusIdlist += "," + cusIds;
                }
                /*****************************查询出所有经营性贷款的产品编号***********************************/
                String prdType = "01,02,03,04,05,06,07,08";//对公贷款+小微经营性贷款
                String prdId = "";
                logger.info("***********调用iCmisCfgClientService查询产品类别*START**************产品类型" + prdType);
                ResultDto<List<CfgPrdBasicinfoDto>> resultDto = iCmisCfgClientService.queryCfgPrdBasicInfoByPrdType(prdType);
                String prdCode = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoList = JSON.parseObject(JSON.toJSONString(resultDto.getData()), new TypeReference<List<CfgPrdBasicinfoDto>>() {
                    });
                    for (int i = 0; i < cfgPrdBasicinfoDtoList.size(); i++) {
                        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoList.get(i);
                        prdId += cfgPrdBasicinfoDto.getPrdId();
                        if (i < cfgPrdBasicinfoDtoList.size() - 1) {
                            prdId += "','";
                        }
                    }
                }
                logger.info("***********调用iCmisCfgClientService查询产品类别*END**************产品号" + prdId);
                /*****************************查询出所有经营性贷款的产品编号结束***********************************/

                /******************
                 * 查询客户配偶经营性贷款是否存在余额 （包括担任法人和实际控制人的贷款余额）
                 * ******************/
                BigDecimal sumLoanBal = BigDecimal.ZERO;
                sumLoanBal = accLoanMapper.getCoupleLoanBalList(cusIdlist, prdId);
                logger.info("根据客户号【{}】查询客户台账信息结束,返回台账信息【{}】", cusId, JSON.toJSONString(sumLoanBal));
                if (CollectionUtils.nonNull(sumLoanBal)) {
                    if (sumLoanBal.compareTo(BigDecimal.ZERO) > 0) {
                        xdtz0012DataRespDto.setBillBal(sumLoanBal);
                        xdtz0012DataRespDto.setIsHavingOperBillBal(CommonConstance.STD_YES_OR_NO_Y);
                    } else {
                        xdtz0012DataRespDto.setIsHavingOperBillBal(CommonConstance.STD_YES_OR_NO_N);
                    }
                } else {
                    xdtz0012DataRespDto.setIsHavingOperBillBal(CommonConstance.STD_YES_OR_NO_N);
                }
                //查询授信审批状态是否“审批中”
                String isCrdApprStatusPending = "N";
                //查询对公授信
                int num11 = lmtAppMapper.selectAppingByCusId(cusIdlist);
                logger.info("*************************XDTZ0012**查询审批中的对公授信记录数为:" + num11);
                //查询个人授信
                int num12 = lmtSurveyReportMainInfoMapper.findSmlnApplyListxdtzByCusId(cusId, prdId);
                logger.info("*************************XDTZ0012**查询个人授信记录数为:" + num12);
                if (num11 > 0 || num12 > 0) {
                    isCrdApprStatusPending = "Y";
                }
                //查询是否存在有效批复
                String isApprVaild = "N";
                int num2 = lmtReplyMapper.selectEffectiveByCusId(cusId);
                //查询是否存在有效的授信批复(个人)
                int num4 = lmtCrdReplyInfoMapper.selectLmtCrdReplySernoByCusId(cusId);

                logger.info("*************************XDTZ0012**查询是否存在有效批复对公为:" + num2+"个人为:"+num4);
                if (num2 > 0 || num4 > 0) {//查询是否存在有效批复
                    isApprVaild = "Y";
                }
                //查询是否存在有效合同(经营性借款合同)
                String isContVaild = "N";
                int num3 = ctrLoanContMapper.selectEffectiveContByCusId(cusId, prdId);
                logger.info("*************************XDTZ0012**查询是否存在有效合同为:" + num2);
                if (num3 > 0) {
                    isContVaild = "Y";
                }
                xdtz0012DataRespDto.setIsCrdApprStatusPending(isCrdApprStatusPending);
                xdtz0012DataRespDto.setIsApprVaild(isApprVaild);
                xdtz0012DataRespDto.setIsContVaild(isContVaild);
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataRespDto));
        return xdtz0012DataRespDto;
    }

}
