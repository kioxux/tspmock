/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGrade
 * @类描述: major_grade数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 20:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "major_grade")
public class MajorGrade extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = true, length = 40)
	private String lmtSerno;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
	private String loanPurp;
	
	/** 项目融资具体用途 **/
	@Column(name = "USE_RZ_TYPE", unique = false, nullable = true, length = 5)
	private String useRzType;
	
	/** 房地产融资具体用途 **/
	@Column(name = "USE_FDC_TYPE", unique = false, nullable = true, length = 5)
	private String useFdcType;
	
	/** 项目融资还款来源 **/
	@Column(name = "RZ_REPAY_WAY", unique = false, nullable = true, length = 5)
	private String rzRepayWay;
	
	/** 房地产融资还款来源 **/
	@Column(name = "FDC_REPAY_WAY", unique = false, nullable = true, length = 5)
	private String fdcRepayWay;
	
	/** 债务人是否基本没有其它实质性资产和业务 **/
	@Column(name = "HAS_OTHER_ASSET", unique = false, nullable = true, length = 5)
	private String hasOtherAsset;
	
	/** 银行对融资形成的资产控制权 **/
	@Column(name = "BANK_CONTRL_RZ", unique = false, nullable = true, length = 5)
	private String bankContrlRz;
	
	/** 专业贷款类型 **/
	@Column(name = "MAJOR_LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String majorLoanType;
	
	/** 专业贷款评级模型 **/
	@Column(name = "MAJOR_GRADE_MODE", unique = false, nullable = true, length = 20)
	private String majorGradeMode;
	
	/** 专业贷款逾期损失率 **/
	@Column(name = "MAJOR_PRE_LOST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal majorPreLost;
	
	/** 专业贷款本行既期信用等级 **/
	@Column(name = "CREDIT_GRADE", unique = false, nullable = true, length = 5)
	private String creditGrade;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 风险大类 **/
	@Column(name = "BIG_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String bigRiskClass;
	
	/** 风险中类 **/
	@Column(name = "MID_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String midRiskClass;
	
	/** 风险小类 **/
	@Column(name = "FEW_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String fewRiskClass;
	
	/** 风险划分日期 **/
	@Column(name = "RISK_DIVIDE_DATE", unique = false, nullable = true, length = 10)
	private String riskDivideDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}
	
    /**
     * @return loanPurp
     */
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param useRzType
	 */
	public void setUseRzType(String useRzType) {
		this.useRzType = useRzType;
	}
	
    /**
     * @return useRzType
     */
	public String getUseRzType() {
		return this.useRzType;
	}
	
	/**
	 * @param useFdcType
	 */
	public void setUseFdcType(String useFdcType) {
		this.useFdcType = useFdcType;
	}
	
    /**
     * @return useFdcType
     */
	public String getUseFdcType() {
		return this.useFdcType;
	}
	
	/**
	 * @param rzRepayWay
	 */
	public void setRzRepayWay(String rzRepayWay) {
		this.rzRepayWay = rzRepayWay;
	}
	
    /**
     * @return rzRepayWay
     */
	public String getRzRepayWay() {
		return this.rzRepayWay;
	}
	
	/**
	 * @param fdcRepayWay
	 */
	public void setFdcRepayWay(String fdcRepayWay) {
		this.fdcRepayWay = fdcRepayWay;
	}
	
    /**
     * @return fdcRepayWay
     */
	public String getFdcRepayWay() {
		return this.fdcRepayWay;
	}
	
	/**
	 * @param hasOtherAsset
	 */
	public void setHasOtherAsset(String hasOtherAsset) {
		this.hasOtherAsset = hasOtherAsset;
	}
	
    /**
     * @return hasOtherAsset
     */
	public String getHasOtherAsset() {
		return this.hasOtherAsset;
	}
	
	/**
	 * @param bankContrlRz
	 */
	public void setBankContrlRz(String bankContrlRz) {
		this.bankContrlRz = bankContrlRz;
	}
	
    /**
     * @return bankContrlRz
     */
	public String getBankContrlRz() {
		return this.bankContrlRz;
	}
	
	/**
	 * @param majorLoanType
	 */
	public void setMajorLoanType(String majorLoanType) {
		this.majorLoanType = majorLoanType;
	}
	
    /**
     * @return majorLoanType
     */
	public String getMajorLoanType() {
		return this.majorLoanType;
	}
	
	/**
	 * @param majorGradeMode
	 */
	public void setMajorGradeMode(String majorGradeMode) {
		this.majorGradeMode = majorGradeMode;
	}
	
    /**
     * @return majorGradeMode
     */
	public String getMajorGradeMode() {
		return this.majorGradeMode;
	}
	
	/**
	 * @param majorPreLost
	 */
	public void setMajorPreLost(java.math.BigDecimal majorPreLost) {
		this.majorPreLost = majorPreLost;
	}
	
    /**
     * @return majorPreLost
     */
	public java.math.BigDecimal getMajorPreLost() {
		return this.majorPreLost;
	}
	
	/**
	 * @param creditGrade
	 */
	public void setCreditGrade(String creditGrade) {
		this.creditGrade = creditGrade;
	}
	
    /**
     * @return creditGrade
     */
	public String getCreditGrade() {
		return this.creditGrade;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bigRiskClass
	 */
	public void setBigRiskClass(String bigRiskClass) {
		this.bigRiskClass = bigRiskClass;
	}
	
    /**
     * @return bigRiskClass
     */
	public String getBigRiskClass() {
		return this.bigRiskClass;
	}
	
	/**
	 * @param midRiskClass
	 */
	public void setMidRiskClass(String midRiskClass) {
		this.midRiskClass = midRiskClass;
	}
	
    /**
     * @return midRiskClass
     */
	public String getMidRiskClass() {
		return this.midRiskClass;
	}
	
	/**
	 * @param fewRiskClass
	 */
	public void setFewRiskClass(String fewRiskClass) {
		this.fewRiskClass = fewRiskClass;
	}
	
    /**
     * @return fewRiskClass
     */
	public String getFewRiskClass() {
		return this.fewRiskClass;
	}
	
	/**
	 * @param riskDivideDate
	 */
	public void setRiskDivideDate(String riskDivideDate) {
		this.riskDivideDate = riskDivideDate;
	}
	
    /**
     * @return riskDivideDate
     */
	public String getRiskDivideDate() {
		return this.riskDivideDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}