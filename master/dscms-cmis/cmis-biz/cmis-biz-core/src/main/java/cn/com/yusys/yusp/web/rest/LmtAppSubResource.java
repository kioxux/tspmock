/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.service.LmtAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.service.LmtAppSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zhuzr
 * @创建时间: 2021-04-07 19:49:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtappsub")
public class LmtAppSubResource {
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppService lmtAppService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppSub> list = lmtAppSubService.selectAll(queryModel);
        return new ResultDto<List<LmtAppSub>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAppSub>> index(QueryModel queryModel) {
        List<LmtAppSub> list = lmtAppSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppSub> show(@PathVariable("pkId") String pkId) {
        LmtAppSub lmtAppSub = lmtAppSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppSub>(lmtAppSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppSub> create(@RequestBody LmtAppSub lmtAppSub) throws URISyntaxException {
        lmtAppSubService.insert(lmtAppSub);
        return new ResultDto<LmtAppSub>(lmtAppSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppSub lmtAppSub) throws URISyntaxException {
        int result = lmtAppSubService.update(lmtAppSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteLmtAppSub
     * @函数描述:删除授信分项
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletelmtappsub")
    protected ResultDto<Map> deleteLmtAppSub(@RequestBody String pkId) throws URISyntaxException{
        Map result = lmtAppSubService.updateByPkId(pkId);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:saveLmtAppSub
     * @函数描述:保存授信分项新增数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savelmtappsub")
    protected ResultDto<Map> saveLmtApp(@RequestBody LmtAppSub jsoPar) {
        Map map = lmtAppSubService.saveLmtAppSub(jsoPar);
        return ResultDto.success(map);
    }

    /**
     * @函数名称:updateLmtAppSub
     * @函数描述:保存授信分项修改数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatelmtappsub")
    protected ResultDto<Map> updateLmtAppSub(@RequestBody LmtAppSub lmtAppSub) {
        Map map = lmtAppSubService.updateLmtAppSub(lmtAppSub);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getsubandprd")
    protected ResultDto<Map> getSubAndPrd(@RequestBody String serno) {
        Map map = lmtAppSubService.getSubAndPrd(serno);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getSubAndPrdForReCheck
     * @函数描述:获取复审表中当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getsubandprdforrecheck")
    protected ResultDto<Map> getSubAndPrdForReCheck(@RequestBody String serno) {
        Map map = lmtAppSubService.getSubAndPrdForReCheck(serno);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前客户授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getcussubandprd")
    protected ResultDto<Map> getCusSubAndPrd(@RequestBody Map<String, String> map) {
        QueryModel model = new QueryModel();
        model.addCondition("cusId",map.get("cusId"));
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApp> list = lmtAppService.getLmtAppByModel1(model);
        if (list!=null && list.size()>0) {
            String serno = list.get(0).getSerno();
            Map replyMap = lmtAppSubService.getSubAndPrd(serno);
            return new ResultDto<Map>(replyMap);
        }
        return new ResultDto<Map>(null);
    }



    /**
     * @函数名称:getSubAndPrd
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getsubandprdForGrpSerno")
    protected ResultDto<Map> getsubandprdForGrpSerno(@RequestBody String serno) {
        Map map = lmtAppSubService.getSubAndPrdForGrpSerno(serno);
        return new ResultDto<>(map);
    }



    /**
     * @函数名称:queryByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @PostMapping("/querybymodel")
    protected ResultDto<List<LmtAppSub>> queryByModel(@RequestBody QueryModel queryModel) {
        List<LmtAppSub> list = lmtAppSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppSub>>(list);
    }

    /**
     * @函数名称:getBySubSerno
     * @函数描述:根据分项流水号得到该条分项信息
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @PostMapping("/getbysubserno")
    protected ResultDto<LmtAppSub> getBySubSerno(@RequestBody String subSerno) {
        LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(subSerno.replaceAll("\"", ""));
        return new ResultDto<LmtAppSub>(lmtAppSub);
    }

    /**
     * @函数名称:selectlmtappdatabysubserno
     * @函数描述:根据分项流水号查询 授信类型
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    @PostMapping("/selectlmtappdatabysubserno")
    public ResultDto<LmtApp> selectLmtAppDataBySubSerno(@RequestBody String subSerno) {
        LmtApp lmtApp = lmtAppSubService.selectLmtAppDataBySubSerno(subSerno);
        return new ResultDto<LmtApp>(lmtApp);
    }


    /**
     * @函数名称:getSubAndPrdByMap
     * @函数描述:获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getsubandprdbymap")
    protected ResultDto<Map> getSubAndPrdByMap(@RequestBody QueryModel queryModel) {
        String serno = (String) queryModel.getCondition().get("serno");
        Map map = lmtAppSubService.getSubAndPrd(serno);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:judgesubandprd
     * @函数描述:判断当前授信申请对应的授信分项及授信分项下的适用授信品种是否包含信保贷
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/judgesubandprd")
    protected ResultDto<Map> judgesubandprd(@RequestBody String serno) {
        Map map = lmtAppSubService.judgesubandprd(serno);
        return new ResultDto<>(map);
    }
}
