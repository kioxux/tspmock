/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAccRel
 * @类描述: lmt_fr_ufr_acc_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:39:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fr_ufr_acc_rel")
public class LmtFrUfrAccRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 冻结/解冻流水号 **/
	@Column(name = "FR_UFR_SERNO", unique = false, nullable = false, length = 40)
	private String frUfrSerno;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_LIMIT_NO", unique = false, nullable = false, length = 40)
	private String lmtLimitNo;
	
	/** 授信协议编号 **/
	@Column(name = "LMT_CTR_NO", unique = false, nullable = false, length = 40)
	private String lmtCtrNo;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param frUfrSerno
	 */
	public void setFrUfrSerno(String frUfrSerno) {
		this.frUfrSerno = frUfrSerno;
	}
	
    /**
     * @return frUfrSerno
     */
	public String getFrUfrSerno() {
		return this.frUfrSerno;
	}
	
	/**
	 * @param lmtLimitNo
	 */
	public void setLmtLimitNo(String lmtLimitNo) {
		this.lmtLimitNo = lmtLimitNo;
	}
	
    /**
     * @return lmtLimitNo
     */
	public String getLmtLimitNo() {
		return this.lmtLimitNo;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}

}