/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyDetailList
 * @类描述: doc_destroy_detail_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-20 14:01:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_destroy_detail_list")
public class DocDestroyDetailList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 销毁明细流水号 **/
	@Id
	@Column(name = "DDDL_SERNO")
	private String dddlSerno;
	
	/** 销毁申请流水号 **/
	@Id
	@Column(name = "DDAL_SERNO")
	private String ddalSerno;
	
	/** 档案流水号 **/
	@Column(name = "DOC_SERNO", unique = false, nullable = true, length = 40)
	private String docSerno;
	
	/** 档案编号 **/
	@Column(name = "DOC_NO", unique = false, nullable = true, length = 40)
	private String docNo;
	
	/** 归档模式 **/
	@Column(name = "ARCHIVE_MODE", unique = false, nullable = true, length = 5)
	private String archiveMode;
	
	/** 档案类型 **/
	@Column(name = "DOC_TYPE", unique = false, nullable = true, length = 5)
	private String docType;
	
	/** 档案分类 **/
	@Column(name = "DOC_CLASS", unique = false, nullable = true, length = 5)
	private String docClass;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 关联业务流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 资料件数 **/
	@Column(name = "DOC_NUM", unique = false, nullable = true, length = 20)
	private String docNum;
	
	/** 生成日期 **/
	@Column(name = "CREATE_DATE", unique = false, nullable = true, length = 20)
	private String createDate;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="belgOrgName")
	private String belgOrg;
	
	/** 已保年限 **/
	@Column(name = "IN_KEEP_YEARS", unique = false, nullable = true, length = 20)
	private String inKeepYears;
	
	/** 销毁状态 **/
	@Column(name = "DESTROY_STATUS", unique = false, nullable = true, length = 5)
	private String destroyStatus;
	
	/** 销毁日期 **/
	@Column(name = "DESTROY_DATE", unique = false, nullable = true, length = 10)
	private String destroyDate;
	
	/** 档案状态 **/
	@Column(name = "DOC_STAUTS", unique = false, nullable = true, length = 5)
	private String docStauts;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param dddlSerno
	 */
	public void setDddlSerno(String dddlSerno) {
		this.dddlSerno = dddlSerno;
	}
	
    /**
     * @return dddlSerno
     */
	public String getDddlSerno() {
		return this.dddlSerno;
	}
	
	/**
	 * @param ddalSerno
	 */
	public void setDdalSerno(String ddalSerno) {
		this.ddalSerno = ddalSerno;
	}
	
    /**
     * @return ddalSerno
     */
	public String getDdalSerno() {
		return this.ddalSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno;
	}
	
    /**
     * @return docSerno
     */
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	
    /**
     * @return docNo
     */
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param archiveMode
	 */
	public void setArchiveMode(String archiveMode) {
		this.archiveMode = archiveMode;
	}
	
    /**
     * @return archiveMode
     */
	public String getArchiveMode() {
		return this.archiveMode;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
    /**
     * @return docType
     */
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass;
	}
	
    /**
     * @return docClass
     */
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param docNum
	 */
	public void setDocNum(String docNum) {
		this.docNum = docNum;
	}
	
    /**
     * @return docNum
     */
	public String getDocNum() {
		return this.docNum;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
    /**
     * @return createDate
     */
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param inKeepYears
	 */
	public void setInKeepYears(String inKeepYears) {
		this.inKeepYears = inKeepYears;
	}
	
    /**
     * @return inKeepYears
     */
	public String getInKeepYears() {
		return this.inKeepYears;
	}
	
	/**
	 * @param destroyStatus
	 */
	public void setDestroyStatus(String destroyStatus) {
		this.destroyStatus = destroyStatus;
	}
	
    /**
     * @return destroyStatus
     */
	public String getDestroyStatus() {
		return this.destroyStatus;
	}
	
	/**
	 * @param destroyDate
	 */
	public void setDestroyDate(String destroyDate) {
		this.destroyDate = destroyDate;
	}
	
    /**
     * @return destroyDate
     */
	public String getDestroyDate() {
		return this.destroyDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts;
	}
	
    /**
     * @return docStauts
     */
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}