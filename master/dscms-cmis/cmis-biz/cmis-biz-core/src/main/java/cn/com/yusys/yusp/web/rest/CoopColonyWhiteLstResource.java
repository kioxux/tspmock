/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.CoopColonyWhiteLstVo;
import cn.com.yusys.yusp.vo.IqpAccpAppPorderSubImportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopColonyWhiteLst;
import cn.com.yusys.yusp.service.CoopColonyWhiteLstService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopColonyWhiteLstResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: AbsonZ
 * @创建时间: 2021-04-16 15:47:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopcolonywhitelst")
public class CoopColonyWhiteLstResource {
    @Autowired
    private CoopColonyWhiteLstService coopColonyWhiteLstService;

    private final Logger logger = LoggerFactory.getLogger(CoopColonyWhiteLstResource.class);

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopColonyWhiteLst>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopColonyWhiteLst> list = coopColonyWhiteLstService.selectAll(queryModel);
        return new ResultDto<List<CoopColonyWhiteLst>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopColonyWhiteLst>> index(@RequestBody QueryModel queryModel) {
        List<CoopColonyWhiteLst> list = coopColonyWhiteLstService.selectByModel(queryModel);
        return new ResultDto<List<CoopColonyWhiteLst>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{coopPlanNo}")
    protected ResultDto<CoopColonyWhiteLst> show(@PathVariable("coopPlanNo") String coopPlanNo) {
        CoopColonyWhiteLst coopColonyWhiteLst = coopColonyWhiteLstService.selectByPrimaryKey(coopPlanNo);
        return new ResultDto<CoopColonyWhiteLst>(coopColonyWhiteLst);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopColonyWhiteLst> create(@RequestBody CoopColonyWhiteLst coopColonyWhiteLst) throws URISyntaxException {
        coopColonyWhiteLstService.insert(coopColonyWhiteLst);
        return new ResultDto<CoopColonyWhiteLst>(coopColonyWhiteLst);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CoopColonyWhiteLst coopColonyWhiteLst) throws URISyntaxException {
        int result = coopColonyWhiteLstService.updateSelective(coopColonyWhiteLst);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopColonyWhiteLst coopColonyWhiteLst) throws URISyntaxException {
        int result = coopColonyWhiteLstService.update(coopColonyWhiteLst);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{coopPlanNo}")
    protected ResultDto<Integer> delete(@PathVariable("coopPlanNo") String coopPlanNo) {
        int result = coopColonyWhiteLstService.deleteByPrimaryKey(coopPlanNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopColonyWhiteLstService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = coopColonyWhiteLstService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            throw BizException.error(null, EclEnum.ECL070097.key,EclEnum.ECL070097.value);
        }
        ExcelUtils.syncImport(CoopColonyWhiteLstVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
            try {          //上行PlaPlanDetailVo为Excel中的字段
                return coopColonyWhiteLstService.insertInBatch(dataList);
            } catch (Exception e) {
                throw BizException.error(null, EcnEnum.ECN069999.key,e.getMessage());
            }
        }), true);
        return ResultDto.success().message("导入成功！");
    }

    /**
     * 一键更新客户号
     */
    @PostMapping("/updatecusid")
    public ResultDto<Integer> updateCusId() {
        int result = coopColonyWhiteLstService.updateCusId();
        return new ResultDto<Integer>(result);
    }
}
