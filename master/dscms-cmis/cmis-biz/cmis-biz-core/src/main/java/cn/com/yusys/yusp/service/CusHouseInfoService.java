/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusHouseInfo;
import cn.com.yusys.yusp.repository.mapper.CusHouseInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusHouseInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusHouseInfoService {

    @Autowired
    private CusHouseInfoMapper cusHouseInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusHouseInfo selectByPrimaryKey(String serno) {
        return cusHouseInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusHouseInfo> selectAll(QueryModel model) {
        List<CusHouseInfo> records = (List<CusHouseInfo>) cusHouseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusHouseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusHouseInfo> list = cusHouseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 zhangl
     * @创建时间 2021-08-02 15:35
     * @注释
     */
    public List<CusHouseInfo> queryCusHouseInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusHouseInfo> list = cusHouseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusHouseInfo record) {
        return cusHouseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusHouseInfo record) {
        return cusHouseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusHouseInfo record) {
        return cusHouseInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusHouseInfo record) {
        return cusHouseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusHouseInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusHouseInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryCusHouseInfoByParams
     * @方法描述: 根据入参查询房屋信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zl
     * @创建时间: 2021-08-02 14:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CusHouseInfo queryCusHouseInfoByParams(Map queryMap) {
        return cusHouseInfoMapper.queryCusHouseInfoByParams(queryMap);
    }
}
