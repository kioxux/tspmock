/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtAppGuarRel;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppGuarRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppGuarRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-13 11:04:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppGuarRelService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppGuarRelService.class);

    @Autowired
    private LmtAppGuarRelMapper lmtAppGuarRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppGuarRel selectByPrimaryKey(String pkId) {
        return lmtAppGuarRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppGuarRel> selectAll(QueryModel model) {
        List<LmtAppGuarRel> records = (List<LmtAppGuarRel>) lmtAppGuarRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppGuarRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppGuarRel> list = lmtAppGuarRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppGuarRel record) {
        return lmtAppGuarRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppGuarRel record) {
        return lmtAppGuarRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAppGuarRel record) {
        return lmtAppGuarRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppGuarRel record) {
        return lmtAppGuarRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppGuarRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppGuarRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: importGuarColl
     * @方法描述: 根据前端传输数据，完成记录新增
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map importGuarColl(LmtAppGuarRel lmtAppGuarRel) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{
            // TODO 保存前校验 待补充
            String guar_no = lmtAppGuarRel.getGuarNo();
            if(StringUtils.isEmpty(guar_no)){
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            lmtAppGuarRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            log.info(String.format("保存担保关系数据%s-获取当前登录用户数据", guar_no));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtAppGuarRel.setPkId(UUID.randomUUID().toString());
                lmtAppGuarRel.setInputId(userInfo.getLoginCode());
                lmtAppGuarRel.setInputBrId(userInfo.getOrg().getCode());
                lmtAppGuarRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppGuarRel.setUpdId(userInfo.getLoginCode());
                lmtAppGuarRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtAppGuarRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppGuarRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtAppGuarRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }

            int count = lmtAppGuarRelMapper.insert(lmtAppGuarRel);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",保存失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存授信分项担保信息数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: importGuarColl
     * @方法描述: 根据前端传输数据，完成记录新增
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map deleteGuarRel(LmtAppGuarRel lmtAppGuarRel) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{
            // TODO 待补充   删除担保信息


            int count = lmtAppGuarRelMapper.updateByPkId(lmtAppGuarRel);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除担保关系数据！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据入参获取担保关系信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtAppGuarRel> selectByParams(Map params) {
        List<LmtAppGuarRel> list = lmtAppGuarRelMapper.selectByParams(params);
        return list;
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据授信流水号获取担保关系信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtAppGuarRel> selectByLmtSerno(String lmtSerno) {
        Map map = new HashMap();
        map.put("lmtSerno",lmtSerno);
        List<LmtAppGuarRel> list = lmtAppGuarRelMapper.selectByParams(map);
        return list;
    }

    /**
     * @方法名称: selectByGuarNo
     * @方法描述: 根据入参获取担保关系信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtAppGuarRel selectByGuarNo(String guarNo) {
        LmtAppGuarRel lmtAppGuarRel = lmtAppGuarRelMapper.selectByGuarNo(guarNo);
        return lmtAppGuarRel;
    }

    /**
     * @方法名称: copyLmtAppGuarRel
     * @方法描述: 复议，复审，变更时将原担保关系挂靠在新的授信分项流水号下
     * @参数与返回说明: 已废除 -- css 2021-8-23
     * @算法描述: 无
     */
    public boolean copyLmtAppGuarRel(String originSubSerno,String newSubSerno) {
        LmtAppGuarRel lmtAppGuarRel = null;
        Map map = new HashMap();
        map.put("lmtSerno",originSubSerno);
        List<LmtAppGuarRel> list = lmtAppGuarRelMapper.selectByParams(map);
        if(list != null && list.size() >0){
            for (int i = 0; i < list.size(); i++) {
                lmtAppGuarRel = list.get(i);
                lmtAppGuarRel.setLmtSerno(newSubSerno);
                int count = lmtAppGuarRelMapper.insert(lmtAppGuarRel);
                if (count != 1){
                    return false;
                }
            }
        }
        return true;
    }
}
