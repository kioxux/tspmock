package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayDateChg
 * @类描述: iqp_repay_date_chg数据实体类
 * @功能描述:
 * @创建人: 方圳
 * @创建时间: 2020-1-14 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_date_chg")
public class IqpRepayDateChg extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "IQP_SERNO")
    private String iqpSerno;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;
    /**
     * 贷款金额
     */
    @Column(name = "Loan_Amt", unique = false, nullable = true)
    private BigDecimal loanAmt;

    /**
     * 借款金额
     */
    @Column(name = "LOAN_BALANCE",unique = false,nullable = true)
    private BigDecimal loanBalance;

    /**
     * 发放日期
     */
    @Column(name = "DISTR_DATE",unique = false,nullable = true,length = 10)
    private String startDate;

    /**
     * 到期日期
     */
    @Column(name = "END_DATE",unique = false,nullable = true,length = 10)
    private String endDate;

    /**
     * 原还款方式OLD_REPAY_MODE
     */
    @Column(name = "OLD_REPAY_MODE",unique = false,nullable = true,length = 5)
    private String oldRepayMode;

    /**
     * OLD_STOP_PINT_TERM 原停本付息期间
     */
    @Column(name = "OLD_STOP_PINT_TERM",unique = false,nullable = true,length = 5)
    private String oldStopPintTerm;

    /**
     * OLD_REPAY_TERM 原还款间隔周期
     */
    @Column(name = "OLD_REPAY_TERM",unique = false,nullable = true,length = 5)
    private String oldRepayTerm;

    /**
     * OLD_REPAY_SPACE 原还款间隔
     */
    @Column(name = "OLD_REPAY_SPACE",unique = false,nullable = true,length = 5)
    private String oldRepaySpace;

    /**
     * OLD_REPAY_DATE 原还款日
     */
    @Column(name = "OLD_REPAY_DATE",unique = false,nullable = true)
    private BigDecimal oldRepayDate;

    /**
     * REPAY_DATE 还款日
     */
    @Column(name = "REPAY_DATE",unique = false,nullable = true)
    private Integer repayDate;

    /**
     * CHANGE_RESN 调整原因
     */
    @Column(name = "CHANGE_RESN",unique = false,nullable = true,length = 250)
    private String changeResn;

    /**
     * MANAGER_ID 主办人
     */
    @Column(name = "MANAGER_ID",unique = false,nullable = true,length = 20  )
    private String managerId;

    /**
     * MANAGER_BR_ID 主办机构
     */
    @Column(name = "MANAGER_BR_ID",unique = false,nullable = true,length = 20  )
    private String managerBrId;

    /**
     * INPUT_ID 登记人
     */
    @Column(name = "INPUT_ID",unique = false,nullable = true,length = 20  )
    private String inputId;

    /**
     * INPUT_BR_ID 登记机构
     */
    @Column(name = "INPUT_BR_ID",unique = false,nullable = true,length = 20  )
    private String inputBrId;
    /**
     * INPUT_DATE 登记日期
     */
    @Column(name = "INPUT_DATE",unique = false,nullable = true,length = 20  )
    private String inputDate;

    /**
     * UPD_ID 最后修改人
     */
    @Column(name = "UPD_ID",unique = false,nullable = true,length = 20  )
    private String updId;
    /**
     * UPD_BR_ID 最后修改机构
     */
    @Column(name = "UPD_BR_ID",unique = false,nullable = true,length = 20  )
    private String updBrId;

    /**
     * UPD_DATE 最后修改日期
     */
    @Column(name = "UPD_DATE",unique = false,nullable = true,length = 20  )
    private String updDate;

    /**
     * APPROVE_STATUS 申请状态
     */
    @Column(name = "APPROVE_STATUS",unique = false,nullable = true,length = 5  )
    private String approveStatus;

    /**
     * OPR_TYPE 操作类型
     */
    @Column(name = "OPR_TYPE",unique = false,nullable = true,length = 5  )
    private String oprType;

    /**
     * OLD_REPAY_RULE 原还款日确定规则
     */
    @Column(name = "OLD_REPAY_RULE",unique = false,nullable = true,length = 10 )
    private String oldRepayRule;

    /**
     * OLD_REPAY_DT_TYPE 原还款日类型
     */
    @Column(name = "OLD_REPAY_DT_TYPE",unique = false,nullable = true,length = 10  )
    private String oldRepayDtType;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOldRepayMode() {
        return oldRepayMode;
    }

    public void setOldRepayMode(String oldRepayMode) {
        this.oldRepayMode = oldRepayMode;
    }

    public String getOldStopPintTerm() {
        return oldStopPintTerm;
    }

    public void setOldStopPintTerm(String oldStopPintTerm) {
        this.oldStopPintTerm = oldStopPintTerm;
    }

    public String getOldRepayTerm() {
        return oldRepayTerm;
    }

    public void setOldRepayTerm(String oldRepayTerm) {
        this.oldRepayTerm = oldRepayTerm;
    }

    public String getOldRepaySpace() {
        return oldRepaySpace;
    }

    public void setOldRepaySpace(String oldRepaySpace) {
        this.oldRepaySpace = oldRepaySpace;
    }

    public BigDecimal getOldRepayDate() {
        return oldRepayDate;
    }

    public void setOldRepayDate(BigDecimal oldRepayDate) {
        this.oldRepayDate = oldRepayDate;
    }

    public Integer getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(Integer repayDate) {
        this.repayDate = repayDate;
    }

    public String getChangeResn() {
        return changeResn;
    }

    public void setChangeResn(String changeResn) {
        this.changeResn = changeResn;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOldRepayRule() {
        return oldRepayRule;
    }

    public void setOldRepayRule(String oldRepayRule) {
        this.oldRepayRule = oldRepayRule;
    }

    public String getOldRepayDtType() {
        return oldRepayDtType;
    }

    public void setOldRepayDtType(String oldRepayDtType) {
        this.oldRepayDtType = oldRepayDtType;
    }
}
