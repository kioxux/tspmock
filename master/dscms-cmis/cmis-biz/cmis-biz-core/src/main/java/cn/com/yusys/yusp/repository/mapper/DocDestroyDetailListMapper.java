/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.DocDestroyDetailList;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyDetailListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 20:16:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocDestroyDetailListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocDestroyDetailList selectByPrimaryKey(@Param("dddlSerno") String dddlSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocDestroyDetailList> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocDestroyDetailList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocDestroyDetailList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocDestroyDetailList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocDestroyDetailList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("dddlSerno") String dddlSerno);

    /**
     * 根据档案销毁流水号进行删除
     * @param ddalSerno
     * @return
     */
    int deleteByDdalSerno(@Param("ddalSerno") String ddalSerno);

    /**
     * 档案销毁明细件：更新为已销毁
     * @param docNoList
     * @return
     */
    int updateAppDestroyDetailStatus(@Param("list") List<String> docNoList,@Param("status") String status);

    /**
     * 根据档案销毁流水号进行查询：查询所有的档案编号
     * @param ddalSerno
     * @return
     */
    List<String> selectDocNoListByDdalSerno(@Param("ddalSerno") String ddalSerno);

    /**
     * 根据档案编号查询
     * @param docNo
     * @return
     */
    List<DocDestroyDetailList> selectBydocNo(String docNo);
}