package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanCont
 * @类描述: ctr_loan_cont数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-09 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrEntrustLoanContShowDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 合同编号 **/
	private String contNo;

	/** 中文合同编号 **/
	private String cnContNo;

	/** 全局流水号 **/
	private String serno;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 合同金额 **/
	private String contAmt;

	/** 合同余额 **/
	private String contHighAvlAmt;

	/** 合同起始日 **/
	private String startDate;

	/** 合同到期日 **/
	private String endDate;

	/** 担保方式 STD_ZB_GUAR_WAY **/
	private String guarMode;

	public CtrEntrustLoanContShowDto(String contNo, String cnContNo, String serno, String cusId, String cusName, String prdId, String prdName, String contAmt, String contHighAvlAmt, String startDate, String endDate, String grtMode) {
		this.contNo = contNo;
		this.cnContNo = cnContNo;
		this.serno = serno;
		this.cusId = cusId;
		this.cusName = cusName;
		this.prdId = prdId;
		this.prdName = prdName;
		this.contAmt = contAmt;
		this.contHighAvlAmt = contHighAvlAmt;
		this.startDate = startDate;
		this.endDate = endDate;
		this.guarMode = guarMode;
	}

	/**
	 * @return Serno
	 */
	public String getContNo() {
		return this.contNo;
	}
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	/**
	 * @return cnContNo
	 */
	public String getCnContNo() {
		return this.cnContNo;
	}
	/**
	 * @param cnContNo
	 */
	public void setCnContNo(String cnContNo) {
		this.cnContNo = cnContNo == null ? null : cnContNo.trim();
	}
	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	/**
	 * @return cusId
	 */
	public String getCusId() {
		return cusId;
	}
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return prdId;
	}
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return guarMode;
	}
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	/**
	 * @return cusName
	 */
	public String getCusName() {
		return cusName;
	}
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	/**
	 * @return prName
	 */
	public String getPrdName() {
		return prdName;
	}
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(String contAmt) {
		this.contAmt = contAmt == null ? null : contAmt.trim();
	}
	/**
	 * @return contAmt
	 */
	public String getContAmt() {
		return contAmt;
	}

	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(String contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt == null ? null : contHighAvlAmt.trim();
	}
	/**
	 * @return contHighAvlAmt
	 */
	public String getContHighAvlAmt() {
		return contHighAvlAmt;
	}
}