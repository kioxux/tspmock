/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.collection.MapUtils;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppr;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasic;
import cn.com.yusys.yusp.repository.mapper.RptBasicMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-28 16:32:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicService {

    @Autowired
    private RptBasicMapper rptBasicMapper;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtReplyService lmtReplyService;
    @Autowired
    private LmtApprService lmtApprService;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptBasic selectByPrimaryKey(String serno) {
        return rptBasicMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptBasic> selectAll(QueryModel model) {
        List<RptBasic> records = (List<RptBasic>) rptBasicMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptBasic> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasic> list = rptBasicMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptBasic record) {
        return rptBasicMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptBasic record) {
        return rptBasicMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptBasic record) {
        return rptBasicMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptBasic record) {
        return rptBasicMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptBasicMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicMapper.deleteByIds(ids);
    }

    /**
     * 修改授信调查报告
     *
     * @param rptBasic
     * @return
     */
    public int updateRptBasic(RptBasic rptBasic) {
        if (rptBasic == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String serno = rptBasic.getSerno();
        RptBasic rpt = rptBasicMapper.selectByPrimaryKey(serno);
        int count = 0;
        if (rpt != null) {
            count = rptBasicMapper.updateByPrimaryKeySelective(rptBasic);
        } else {
            count = rptBasicMapper.insertSelective(rptBasic);
        }
        return count;
    }

    public Map initRptBasic(String serno) {
        Map result = new HashMap();
        RptBasic rptBasic = selectByPrimaryKey(serno);
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        model.setSort("inputDate");
        String cusId = lmtApp.getCusId();
        List<LmtAppr> lmtApprs = lmtApprService.selectByModel(model);
        if (Objects.nonNull(rptBasic)) {
            if (StringUtils.isBlank(rptBasic.getInputDate())) {
                if (CollectionUtils.nonEmpty(lmtApprs)) {
                    rptBasic.setInputDate(lmtApprs.get(0).getInputDate());
                }
                rptBasicMapper.updateByPrimaryKey(rptBasic);
            }
            result.put("serno", serno);
            result.put("lmtNeedPact", rptBasic.getLmtNeedPact());
            result.put("pdoActCase", rptBasic.getPdoActCase());
            result.put("lastLmtNeedPactCase", rptBasic.getLastLmtNeedPactCase());
            result.put("lastPdoActCaseMemo", rptBasic.getLastPdoActCaseMemo());
            result.put("otherRiskEventExpl", rptBasic.getOtherRiskEventExpl());
            result.put("newFinaReason", rptBasic.getNewFinaReason());
            result.put("lmtRationAnaly", rptBasic.getLmtRationAnaly());
            result.put("lmtType", lmtApp.getLmtType());
            result.put("cusId", cusId);
            result.put("inputDate", rptBasic.getInputDate());
        } else {
            RptBasic temp = new RptBasic();
            temp.setSerno(serno);
            String origiLmtReplySerno = lmtApp.getOrigiLmtReplySerno();
            LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(origiLmtReplySerno);
            if (Objects.nonNull(lmtReply)) {
                //贷后管理需求
                String pspManaNeed = lmtReply.getPspManaNeed();
                temp.setLmtNeedPact(pspManaNeed);
            }
            //压降计划
            Map map = iCusClientService.queryNewDekhByCusId(cusId);
            if (MapUtils.isNotEmpty(map)) {
                StringBuffer str = new StringBuffer();
                if (Objects.nonNull(map.get("pressureDropYearly"))) {
                    str.append(map.get("pressureDropYearly").toString());
                    str.append("年");
                }
                if (Objects.nonNull(map.get("planAmt"))) {
                    str.append("压降金额");
                    str.append(map.get("planAmt").toString());
                    str.append("万元");
                }
                if (StringUtils.nonBlank(str)) {
                    if (Objects.nonNull(map.get("riskRelieveStep"))) {
                        str.append(",");
                        str.append(map.get("riskRelieveStep").toString());
                    } else {
                        str.append(", 无");
                    }
                } else {
                    if (Objects.nonNull(map.get("riskRelieveStep"))) {
                        str.append(map.get("riskRelieveStep").toString());
                    } else {
                        str.append("无");
                    }
                }
                temp.setPdoActCase(str.toString());
            }
            rptBasicMapper.insert(temp);
            result.put("serno", serno);
            result.put("lmtNeedPact", temp.getLmtNeedPact());
            result.put("pdoActCase", temp.getPdoActCase());
            result.put("lastLmtNeedPactCase", temp.getLastLmtNeedPactCase());
            result.put("lastPdoActCaseMemo", temp.getLastPdoActCaseMemo());
            result.put("otherRiskEventExpl", temp.getOtherRiskEventExpl());
            result.put("newFinaReason", temp.getNewFinaReason());
            result.put("lmtRationAnaly", temp.getLmtRationAnaly());
            result.put("lmtType", lmtApp.getLmtType());
            result.put("cusId", cusId);
            result.put("inputDate", temp.getInputDate());
        }

        return result;
    }
}
