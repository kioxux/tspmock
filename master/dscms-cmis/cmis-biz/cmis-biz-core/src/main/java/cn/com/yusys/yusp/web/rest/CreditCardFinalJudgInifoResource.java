/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardFinalJudgInifo;
import cn.com.yusys.yusp.service.CreditCardFinalJudgInifoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFinalJudgInifoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-27 14:33:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "信用卡终审意见")
@RestController
@RequestMapping("/api/creditcardfinaljudginifo")
public class CreditCardFinalJudgInifoResource {
    @Autowired
    private CreditCardFinalJudgInifoService creditCardFinalJudgInifoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardFinalJudgInifo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardFinalJudgInifo> list = creditCardFinalJudgInifoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardFinalJudgInifo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardFinalJudgInifo>> index(QueryModel queryModel) {
        List<CreditCardFinalJudgInifo> list = creditCardFinalJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardFinalJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardFinalJudgInifo> show(@PathVariable("pkId") String pkId) {
        CreditCardFinalJudgInifo creditCardFinalJudgInifo = creditCardFinalJudgInifoService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardFinalJudgInifo>(creditCardFinalJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("终审意见新增")
    @PostMapping("/")
    protected ResultDto<CreditCardFinalJudgInifo> create(@RequestBody CreditCardFinalJudgInifo creditCardFinalJudgInifo) throws URISyntaxException {
        creditCardFinalJudgInifoService.insert(creditCardFinalJudgInifo);
        return new ResultDto<CreditCardFinalJudgInifo>(creditCardFinalJudgInifo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("终审一审意见修改")
    @PostMapping("/updatefirst")
    protected ResultDto<Integer> updateFirst(@RequestBody CreditCardFinalJudgInifo creditCardFinalJudgInifo) throws URISyntaxException {
        int result = creditCardFinalJudgInifoService.updateFirst(creditCardFinalJudgInifo);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("终审意见修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardFinalJudgInifo creditCardFinalJudgInifo) throws URISyntaxException {
        int result = creditCardFinalJudgInifoService.update(creditCardFinalJudgInifo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardFinalJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardFinalJudgInifoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/29 11:08
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号查询信用卡终审信息")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardFinalJudgInifo> selectBySerno(@RequestBody CreditCardFinalJudgInifo creditCardFinalJudgInifo) {
        CreditCardFinalJudgInifo creditCardFinalJudgInifo2 = creditCardFinalJudgInifoService.selectBySerno(creditCardFinalJudgInifo);
        return new ResultDto<CreditCardFinalJudgInifo>(creditCardFinalJudgInifo2);
    }

}
