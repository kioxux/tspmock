/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysDbrCorp
 * @类描述: rpt_spd_anys_dbr_corp数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-24 16:51:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_dbr_corp")
public class RptSpdAnysDbrCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 主要实际控制人是否常住本地 **/
	@Column(name = "IS_RESIDENT_LOCALLY", unique = false, nullable = true, length = 40)
	private String isResidentLocally;
	
	/** 本地居住年限 **/
	@Column(name = "LOCAL_RESID_YEARS", unique = false, nullable = true, length = 40)
	private String localResidYears;
	
	/** 主要实际控制人从业年限 **/
	@Column(name = "ACTUAL_MAJOR_WORK_YEARS", unique = false, nullable = true, length = 40)
	private String actualMajorWorkYears;
	
	/** 担保公司名称 **/
	@Column(name = "GUAR_CORP_NAME", unique = false, nullable = true, length = 80)
	private String guarCorpName;
	
	/** 担保公司内部评级 **/
	@Column(name = "GUAR_CORP_INNER_GRADE", unique = false, nullable = true, length = 40)
	private String guarCorpInnerGrade;
	
	/** 担保公司外部评级 **/
	@Column(name = "GUAR_CORP_OUTER_GRADE", unique = false, nullable = true, length = 40)
	private String guarCorpOuterGrade;
	
	/** 经营场所类型备注 **/
	@Column(name = "BUSI_PREMISE_TYPE_REMARK", unique = false, nullable = true, length = 65535)
	private String busiPremiseTypeRemark;
	
	/** 经营场所类型 **/
	@Column(name = "BUSI_PREMISE_TYPE", unique = false, nullable = true, length = 40)
	private String busiPremiseType;
	
	/** 经营场所变更次数 **/
	@Column(name = "BUSI_PREMISE_CHG_TIMES", unique = false, nullable = true, length = 40)
	private String busiPremiseChgTimes;
	
	/** 经营场所变更次数备注 **/
	@Column(name = "BUSI_PREMISE_CHG_TIMES_REMARK", unique = false, nullable = true, length = 65535)
	private String busiPremiseChgTimesRemark;
	
	/** 区域多元化省份个数 **/
	@Column(name = "RDP_COUNT", unique = false, nullable = true, length = 40)
	private String rdpCount;
	
	/** 区域多元化省份个数备注 **/
	@Column(name = "RDP_COUNT_REMARK", unique = false, nullable = true, length = 65535)
	private String rdpCountRemark;
	
	/** 区域多元化城市个数 **/
	@Column(name = "RDC_COUNT", unique = false, nullable = true, length = 40)
	private String rdcCount;
	
	/** 区域多元化城市个数备注 **/
	@Column(name = "RDC_COUNT_REMARK", unique = false, nullable = true, length = 65535)
	private String rdcCountRemark;
	
	/** 企业上年度所有供应商的总供应量 **/
	@Column(name = "LAST_YEAR_SUP_SUM", unique = false, nullable = true, length = 40)
	private String lastYearSupSum;
	
	/** 企业上年度所有供应商的总供应量备注 **/
	@Column(name = "LAST_YEAR_SUP_SUM_REMARK", unique = false, nullable = true, length = 65535)
	private String lastYearSupSumRemark;
	
	/** 供应商1名称 **/
	@Column(name = "TOPSUP1_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup1CusName;
	
	/** 供应商1合作年限 **/
	@Column(name = "TOPSUP1_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup1CoopTerm;
	
	/** 供应商1上年供应量 **/
	@Column(name = "TOPSUP1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup1Income;
	
	/** 供应商2名称 **/
	@Column(name = "TOPSUP2_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup2CusName;
	
	/** 供应商2合作年限 **/
	@Column(name = "TOPSUP2_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup2CoopTerm;
	
	/** 供应商2上年供应量 **/
	@Column(name = "TOPSUP2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup2Income;
	
	/** 供应商3名称 **/
	@Column(name = "TOPSUP3_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup3CusName;
	
	/** 供应商3合作年限 **/
	@Column(name = "TOPSUP3_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup3CoopTerm;
	
	/** 供应商3上年供应量 **/
	@Column(name = "TOPSUP3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup3Income;
	
	/** 供应商备注 **/
	@Column(name = "SUPPLIER_DESC", unique = false, nullable = true, length = 65535)
	private String supplierDesc;
	
	/** 客户1名称 **/
	@Column(name = "TOPSELL1_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell1CusName;
	
	/** 客户1合作年限 **/
	@Column(name = "TOPSELL1_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell1CoopTerm;
	
	/** 客户1销售收入 **/
	@Column(name = "TOPSELL1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell1Income;
	
	/** 客户2名称 **/
	@Column(name = "TOPSELL2_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell2CusName;
	
	/** 客户2合作年限 **/
	@Column(name = "TOPSELL2_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell2CoopTerm;
	
	/** 客户2销售收入 **/
	@Column(name = "TOPSELL2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell2Income;
	
	/** 客户3名称 **/
	@Column(name = "TOPSELL3_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell3CusName;
	
	/** 客户3合作年限 **/
	@Column(name = "TOPSELL3_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell3CoopTerm;
	
	/** 客户3销售收入 **/
	@Column(name = "TOPSELL3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell3Income;
	
	/** 销售客户备注 **/
	@Column(name = "SELLCUS_DESC", unique = false, nullable = true, length = 65535)
	private String sellcusDesc;
	
	/** 超过90天应收账款余额 **/
	@Column(name = "ARBO_90_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo90Days;
	
	/** 超过90天应收账款余额备注 **/
	@Column(name = "ARBO_90_DAYS_DESC", unique = false, nullable = true, length = 65535)
	private String arbo90DaysDesc;
	
	/** 超过180天应收账款余额 **/
	@Column(name = "ARBO_180_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo180Days;
	
	/** 超过180天应收账款余额备注 **/
	@Column(name = "ARBO_180_DAYS_DESC", unique = false, nullable = true, length = 65535)
	private String arbo180DaysDesc;
	
	/** 去年电力耗值 **/
	@Column(name = "LAST_YEAR_EL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearElValue;
	
	/** 去年煤炭耗值 **/
	@Column(name = "LAST_YEAR_COL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearColValue;
	
	/** 去年天然气耗值 **/
	@Column(name = "LAST_YEAR_GAS_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearGasValue;
	
	/** 去年水耗值 **/
	@Column(name = "LAST_YEAR_WATER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearWaterValue;
	
	/** 去年其他能源耗值 **/
	@Column(name = "LAST_YEAR_OTHER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearOtherValue;
	
	/** 去年能耗说明 **/
	@Column(name = "LAST_YEAR_ENERGY_DESC", unique = false, nullable = true, length = 65535)
	private String lastYearEnergyDesc;
	
	/** 前年电力耗值 **/
	@Column(name = "LAST_TWO_YEAR_EL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearElValue;
	
	/** 前年煤炭耗值 **/
	@Column(name = "LAST_TWO_YEAR_COL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearColValue;
	
	/** 前年天然气耗值 **/
	@Column(name = "LAST_TWO_YEAR_GAS_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearGasValue;
	
	/** 前年水耗值 **/
	@Column(name = "LAST_TWO_YEAR_WATER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearWaterValue;
	
	/** 前年其他能源耗值 **/
	@Column(name = "LAST_TWO_YEAR_OTHER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearOtherValue;
	
	/** 前年能耗说明 **/
	@Column(name = "LAST_TWO_YEAR_ENERGY_DESC", unique = false, nullable = true, length = 65535)
	private String lastTwoYearEnergyDesc;
	
	/** 发放工资当期月份 **/
	@Column(name = "PAY_WAGES_MONTH", unique = false, nullable = true, length = 40)
	private String payWagesMonth;
	
	/** 发放工资当前总额 **/
	@Column(name = "PAY_WAGES_CUR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payWagesCur;
	
	/** 发放工资总额 **/
	@Column(name = "PAY_WAGES_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payWagesTotal;
	
	/** 发放工资备注 **/
	@Column(name = "PAY_WAGES_REMARK", unique = false, nullable = true, length = 65535)
	private String payWagesRemark;
	
	/** 企业去年纳税总额 **/
	@Column(name = "LAST_YEAR_TAX_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTaxTotal;
	
	/** 企业去年纳税总额说明 **/
	@Column(name = "LAST_YEAR_TAX_DESC", unique = false, nullable = true, length = 65535)
	private String lastYearTaxDesc;
	
	/** 企业前年纳税总额 **/
	@Column(name = "LAST_TWO_YEAR_TAX_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearTaxTotal;
	
	/** 企业前年纳税总额说明 **/
	@Column(name = "LAST_TWO_YEAR_TAX_DESC", unique = false, nullable = true, length = 65535)
	private String lastTwoYearTaxDesc;
	
	/** 营业收入当前月份 **/
	@Column(name = "BUSINESS_INCOME_CUR_MONTH", unique = false, nullable = true, length = 40)
	private String businessIncomeCurMonth;
	
	/** 营业收入当前月 **/
	@Column(name = "CUR_MONTH_BUSINESS_INCOME", unique = false, nullable = true, length = 40)
	private String curMonthBusinessIncome;
	
	/** 营业收入去年 **/
	@Column(name = "LAST_YEAR_BUSINESS_INCOME", unique = false, nullable = true, length = 40)
	private String lastYearBusinessIncome;
	
	/** 营业收入说明 **/
	@Column(name = "BUSINESS_INCOME_REMARK", unique = false, nullable = true, length = 65535)
	private String businessIncomeRemark;
	
	/** 企业在我行结算账户的销售回款 **/
	@Column(name = "SCOSAOEIOB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal scosaoeiob;
	
	/** 企业在我行结算账户的销售回款说明 **/
	@Column(name = "SCOSAOEIOB_DESC", unique = false, nullable = true, length = 65535)
	private String scosaoeiobDesc;
	
	/** 贷款本金逾期次数 **/
	@Column(name = "OVERDUE_LOAN_PRINCIPAL_TIMES", unique = false, nullable = true, length = 40)
	private String overdueLoanPrincipalTimes;
	
	/** 贷款本金逾期最长天数 **/
	@Column(name = "LOAN_PRINCIPAL_LONGEST_OVERDUE_DAYS", unique = false, nullable = true, length = 40)
	private String loanPrincipalLongestOverdueDays;
	
	/** 贷款欠息记录次数 **/
	@Column(name = "RECORD_TIMES_OF_LOAN_INTEREST", unique = false, nullable = true, length = 40)
	private String recordTimesOfLoanInterest;
	
	/** 信用卡累积逾期次数 **/
	@Column(name = "CREDIT_CARD_ACCUMULATED_OVERDUE_TIMES", unique = false, nullable = true, length = 40)
	private String creditCardAccumulatedOverdueTimes;
	
	/** 最高累积逾期次数 **/
	@Column(name = "MAXIMUM_CUMULATIVE_OVERDUE_TIMES", unique = false, nullable = true, length = 40)
	private String maximumCumulativeOverdueTimes;
	
	/** 企业及实际控制人征信信用情况说明 **/
	@Column(name = "CIOEAAC", unique = false, nullable = true, length = 65535)
	private String cioeaac;
	
	/** 对外保证金额 **/
	@Column(name = "EXT_GUARANTEE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extGuaranteeAmt;
	
	/** 对外保证金额说明 **/
	@Column(name = "EXT_GUARANTEE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extGuaranteeAmtDesc;
	
	/** 对外抵押金额 **/
	@Column(name = "EXT_MORTGAGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extMortgageAmt;
	
	/** 对外抵押金额说明 **/
	@Column(name = "EXT_MORTGAGE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extMortgageAmtDesc;
	
	/** 对外质押金额 **/
	@Column(name = "EXT_PLEDGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extPledgeAmt;
	
	/** 对外质押金额说明 **/
	@Column(name = "EXT_PLEDGE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extPledgeAmtDesc;
	
	/** 企业是否按时交纳各项税款，有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_DDRYQ_1", unique = false, nullable = true, length = 65535)
	private String focusDdryq1;
	
	/** 企业有无违规排污，是否切实做好环保治理工作，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_DDRYQ_2", unique = false, nullable = true, length = 65535)
	private String focusDdryq2;
	
	/** 企业实际控制人有无吸毒、赌博等不良嗜好，有无经常往返澳门等地，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_DDRYQ_3", unique = false, nullable = true, length = 65535)
	private String focusDdryq3;
	
	/** 企业实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_DDRYQ_4", unique = false, nullable = true, length = 65535)
	private String focusDdryq4;
	
	/** 企业员工人数是否稳定，有无出现明显裁员情况，员工待遇是否合理 **/
	@Column(name = "FOCUS_DDRYQ_5", unique = false, nullable = true, length = 65535)
	private String focusDdryq5;
	
	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_DDRYQ_6", unique = false, nullable = true, length = 65535)
	private String focusDdryq6;
	
	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_DDRYQ_7", unique = false, nullable = true, length = 65535)
	private String focusDdryq7;
	
	/** 其它不利情况请简述 **/
	@Column(name = "FOCUS_DDRYQ_8", unique = false, nullable = true, length = 65535)
	private String focusDdryq8;
	
	/** 反担保实物资产状态查询结果、变现能力评价 **/
	@Column(name = "FOCUS_DDRYQ_9", unique = false, nullable = true, length = 65535)
	private String focusDdryq9;
	
	/** 反担保人评价（如有） **/
	@Column(name = "FOCUS_DDRYQ_10", unique = false, nullable = true, length = 65535)
	private String focusDdryq10;
	
	/** 担保融企业其他 **/
	@Column(name = "FOCUS_DDRYQ_11", unique = false, nullable = true, length = 65535)
	private String focusDdryq11;
	
	/** 担保融企业调查结论 **/
	@Column(name = "FOCUS_DDRYQ_12", unique = false, nullable = true, length = 65535)
	private String focusDdryq12;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param isResidentLocally
	 */
	public void setIsResidentLocally(String isResidentLocally) {
		this.isResidentLocally = isResidentLocally;
	}
	
    /**
     * @return isResidentLocally
     */
	public String getIsResidentLocally() {
		return this.isResidentLocally;
	}
	
	/**
	 * @param localResidYears
	 */
	public void setLocalResidYears(String localResidYears) {
		this.localResidYears = localResidYears;
	}
	
    /**
     * @return localResidYears
     */
	public String getLocalResidYears() {
		return this.localResidYears;
	}
	
	/**
	 * @param actualMajorWorkYears
	 */
	public void setActualMajorWorkYears(String actualMajorWorkYears) {
		this.actualMajorWorkYears = actualMajorWorkYears;
	}
	
    /**
     * @return actualMajorWorkYears
     */
	public String getActualMajorWorkYears() {
		return this.actualMajorWorkYears;
	}
	
	/**
	 * @param guarCorpName
	 */
	public void setGuarCorpName(String guarCorpName) {
		this.guarCorpName = guarCorpName;
	}
	
    /**
     * @return guarCorpName
     */
	public String getGuarCorpName() {
		return this.guarCorpName;
	}
	
	/**
	 * @param guarCorpInnerGrade
	 */
	public void setGuarCorpInnerGrade(String guarCorpInnerGrade) {
		this.guarCorpInnerGrade = guarCorpInnerGrade;
	}
	
    /**
     * @return guarCorpInnerGrade
     */
	public String getGuarCorpInnerGrade() {
		return this.guarCorpInnerGrade;
	}
	
	/**
	 * @param guarCorpOuterGrade
	 */
	public void setGuarCorpOuterGrade(String guarCorpOuterGrade) {
		this.guarCorpOuterGrade = guarCorpOuterGrade;
	}
	
    /**
     * @return guarCorpOuterGrade
     */
	public String getGuarCorpOuterGrade() {
		return this.guarCorpOuterGrade;
	}
	
	/**
	 * @param busiPremiseTypeRemark
	 */
	public void setBusiPremiseTypeRemark(String busiPremiseTypeRemark) {
		this.busiPremiseTypeRemark = busiPremiseTypeRemark;
	}
	
    /**
     * @return busiPremiseTypeRemark
     */
	public String getBusiPremiseTypeRemark() {
		return this.busiPremiseTypeRemark;
	}
	
	/**
	 * @param busiPremiseType
	 */
	public void setBusiPremiseType(String busiPremiseType) {
		this.busiPremiseType = busiPremiseType;
	}
	
    /**
     * @return busiPremiseType
     */
	public String getBusiPremiseType() {
		return this.busiPremiseType;
	}
	
	/**
	 * @param busiPremiseChgTimes
	 */
	public void setBusiPremiseChgTimes(String busiPremiseChgTimes) {
		this.busiPremiseChgTimes = busiPremiseChgTimes;
	}
	
    /**
     * @return busiPremiseChgTimes
     */
	public String getBusiPremiseChgTimes() {
		return this.busiPremiseChgTimes;
	}
	
	/**
	 * @param busiPremiseChgTimesRemark
	 */
	public void setBusiPremiseChgTimesRemark(String busiPremiseChgTimesRemark) {
		this.busiPremiseChgTimesRemark = busiPremiseChgTimesRemark;
	}
	
    /**
     * @return busiPremiseChgTimesRemark
     */
	public String getBusiPremiseChgTimesRemark() {
		return this.busiPremiseChgTimesRemark;
	}
	
	/**
	 * @param rdpCount
	 */
	public void setRdpCount(String rdpCount) {
		this.rdpCount = rdpCount;
	}
	
    /**
     * @return rdpCount
     */
	public String getRdpCount() {
		return this.rdpCount;
	}
	
	/**
	 * @param rdpCountRemark
	 */
	public void setRdpCountRemark(String rdpCountRemark) {
		this.rdpCountRemark = rdpCountRemark;
	}
	
    /**
     * @return rdpCountRemark
     */
	public String getRdpCountRemark() {
		return this.rdpCountRemark;
	}
	
	/**
	 * @param rdcCount
	 */
	public void setRdcCount(String rdcCount) {
		this.rdcCount = rdcCount;
	}
	
    /**
     * @return rdcCount
     */
	public String getRdcCount() {
		return this.rdcCount;
	}
	
	/**
	 * @param rdcCountRemark
	 */
	public void setRdcCountRemark(String rdcCountRemark) {
		this.rdcCountRemark = rdcCountRemark;
	}
	
    /**
     * @return rdcCountRemark
     */
	public String getRdcCountRemark() {
		return this.rdcCountRemark;
	}
	
	/**
	 * @param lastYearSupSum
	 */
	public void setLastYearSupSum(String lastYearSupSum) {
		this.lastYearSupSum = lastYearSupSum;
	}
	
    /**
     * @return lastYearSupSum
     */
	public String getLastYearSupSum() {
		return this.lastYearSupSum;
	}
	
	/**
	 * @param lastYearSupSumRemark
	 */
	public void setLastYearSupSumRemark(String lastYearSupSumRemark) {
		this.lastYearSupSumRemark = lastYearSupSumRemark;
	}
	
    /**
     * @return lastYearSupSumRemark
     */
	public String getLastYearSupSumRemark() {
		return this.lastYearSupSumRemark;
	}
	
	/**
	 * @param topsup1CusName
	 */
	public void setTopsup1CusName(String topsup1CusName) {
		this.topsup1CusName = topsup1CusName;
	}
	
    /**
     * @return topsup1CusName
     */
	public String getTopsup1CusName() {
		return this.topsup1CusName;
	}
	
	/**
	 * @param topsup1CoopTerm
	 */
	public void setTopsup1CoopTerm(String topsup1CoopTerm) {
		this.topsup1CoopTerm = topsup1CoopTerm;
	}
	
    /**
     * @return topsup1CoopTerm
     */
	public String getTopsup1CoopTerm() {
		return this.topsup1CoopTerm;
	}
	
	/**
	 * @param topsup1Income
	 */
	public void setTopsup1Income(java.math.BigDecimal topsup1Income) {
		this.topsup1Income = topsup1Income;
	}
	
    /**
     * @return topsup1Income
     */
	public java.math.BigDecimal getTopsup1Income() {
		return this.topsup1Income;
	}
	
	/**
	 * @param topsup2CusName
	 */
	public void setTopsup2CusName(String topsup2CusName) {
		this.topsup2CusName = topsup2CusName;
	}
	
    /**
     * @return topsup2CusName
     */
	public String getTopsup2CusName() {
		return this.topsup2CusName;
	}
	
	/**
	 * @param topsup2CoopTerm
	 */
	public void setTopsup2CoopTerm(String topsup2CoopTerm) {
		this.topsup2CoopTerm = topsup2CoopTerm;
	}
	
    /**
     * @return topsup2CoopTerm
     */
	public String getTopsup2CoopTerm() {
		return this.topsup2CoopTerm;
	}
	
	/**
	 * @param topsup2Income
	 */
	public void setTopsup2Income(java.math.BigDecimal topsup2Income) {
		this.topsup2Income = topsup2Income;
	}
	
    /**
     * @return topsup2Income
     */
	public java.math.BigDecimal getTopsup2Income() {
		return this.topsup2Income;
	}
	
	/**
	 * @param topsup3CusName
	 */
	public void setTopsup3CusName(String topsup3CusName) {
		this.topsup3CusName = topsup3CusName;
	}
	
    /**
     * @return topsup3CusName
     */
	public String getTopsup3CusName() {
		return this.topsup3CusName;
	}
	
	/**
	 * @param topsup3CoopTerm
	 */
	public void setTopsup3CoopTerm(String topsup3CoopTerm) {
		this.topsup3CoopTerm = topsup3CoopTerm;
	}
	
    /**
     * @return topsup3CoopTerm
     */
	public String getTopsup3CoopTerm() {
		return this.topsup3CoopTerm;
	}
	
	/**
	 * @param topsup3Income
	 */
	public void setTopsup3Income(java.math.BigDecimal topsup3Income) {
		this.topsup3Income = topsup3Income;
	}
	
    /**
     * @return topsup3Income
     */
	public java.math.BigDecimal getTopsup3Income() {
		return this.topsup3Income;
	}
	
	/**
	 * @param supplierDesc
	 */
	public void setSupplierDesc(String supplierDesc) {
		this.supplierDesc = supplierDesc;
	}
	
    /**
     * @return supplierDesc
     */
	public String getSupplierDesc() {
		return this.supplierDesc;
	}
	
	/**
	 * @param topsell1CusName
	 */
	public void setTopsell1CusName(String topsell1CusName) {
		this.topsell1CusName = topsell1CusName;
	}
	
    /**
     * @return topsell1CusName
     */
	public String getTopsell1CusName() {
		return this.topsell1CusName;
	}
	
	/**
	 * @param topsell1CoopTerm
	 */
	public void setTopsell1CoopTerm(String topsell1CoopTerm) {
		this.topsell1CoopTerm = topsell1CoopTerm;
	}
	
    /**
     * @return topsell1CoopTerm
     */
	public String getTopsell1CoopTerm() {
		return this.topsell1CoopTerm;
	}
	
	/**
	 * @param topsell1Income
	 */
	public void setTopsell1Income(java.math.BigDecimal topsell1Income) {
		this.topsell1Income = topsell1Income;
	}
	
    /**
     * @return topsell1Income
     */
	public java.math.BigDecimal getTopsell1Income() {
		return this.topsell1Income;
	}
	
	/**
	 * @param topsell2CusName
	 */
	public void setTopsell2CusName(String topsell2CusName) {
		this.topsell2CusName = topsell2CusName;
	}
	
    /**
     * @return topsell2CusName
     */
	public String getTopsell2CusName() {
		return this.topsell2CusName;
	}
	
	/**
	 * @param topsell2CoopTerm
	 */
	public void setTopsell2CoopTerm(String topsell2CoopTerm) {
		this.topsell2CoopTerm = topsell2CoopTerm;
	}
	
    /**
     * @return topsell2CoopTerm
     */
	public String getTopsell2CoopTerm() {
		return this.topsell2CoopTerm;
	}
	
	/**
	 * @param topsell2Income
	 */
	public void setTopsell2Income(java.math.BigDecimal topsell2Income) {
		this.topsell2Income = topsell2Income;
	}
	
    /**
     * @return topsell2Income
     */
	public java.math.BigDecimal getTopsell2Income() {
		return this.topsell2Income;
	}
	
	/**
	 * @param topsell3CusName
	 */
	public void setTopsell3CusName(String topsell3CusName) {
		this.topsell3CusName = topsell3CusName;
	}
	
    /**
     * @return topsell3CusName
     */
	public String getTopsell3CusName() {
		return this.topsell3CusName;
	}
	
	/**
	 * @param topsell3CoopTerm
	 */
	public void setTopsell3CoopTerm(String topsell3CoopTerm) {
		this.topsell3CoopTerm = topsell3CoopTerm;
	}
	
    /**
     * @return topsell3CoopTerm
     */
	public String getTopsell3CoopTerm() {
		return this.topsell3CoopTerm;
	}
	
	/**
	 * @param topsell3Income
	 */
	public void setTopsell3Income(java.math.BigDecimal topsell3Income) {
		this.topsell3Income = topsell3Income;
	}
	
    /**
     * @return topsell3Income
     */
	public java.math.BigDecimal getTopsell3Income() {
		return this.topsell3Income;
	}
	
	/**
	 * @param sellcusDesc
	 */
	public void setSellcusDesc(String sellcusDesc) {
		this.sellcusDesc = sellcusDesc;
	}
	
    /**
     * @return sellcusDesc
     */
	public String getSellcusDesc() {
		return this.sellcusDesc;
	}
	
	/**
	 * @param arbo90Days
	 */
	public void setArbo90Days(java.math.BigDecimal arbo90Days) {
		this.arbo90Days = arbo90Days;
	}
	
    /**
     * @return arbo90Days
     */
	public java.math.BigDecimal getArbo90Days() {
		return this.arbo90Days;
	}
	
	/**
	 * @param arbo90DaysDesc
	 */
	public void setArbo90DaysDesc(String arbo90DaysDesc) {
		this.arbo90DaysDesc = arbo90DaysDesc;
	}
	
    /**
     * @return arbo90DaysDesc
     */
	public String getArbo90DaysDesc() {
		return this.arbo90DaysDesc;
	}
	
	/**
	 * @param arbo180Days
	 */
	public void setArbo180Days(java.math.BigDecimal arbo180Days) {
		this.arbo180Days = arbo180Days;
	}
	
    /**
     * @return arbo180Days
     */
	public java.math.BigDecimal getArbo180Days() {
		return this.arbo180Days;
	}
	
	/**
	 * @param arbo180DaysDesc
	 */
	public void setArbo180DaysDesc(String arbo180DaysDesc) {
		this.arbo180DaysDesc = arbo180DaysDesc;
	}
	
    /**
     * @return arbo180DaysDesc
     */
	public String getArbo180DaysDesc() {
		return this.arbo180DaysDesc;
	}
	
	/**
	 * @param lastYearElValue
	 */
	public void setLastYearElValue(java.math.BigDecimal lastYearElValue) {
		this.lastYearElValue = lastYearElValue;
	}
	
    /**
     * @return lastYearElValue
     */
	public java.math.BigDecimal getLastYearElValue() {
		return this.lastYearElValue;
	}
	
	/**
	 * @param lastYearColValue
	 */
	public void setLastYearColValue(java.math.BigDecimal lastYearColValue) {
		this.lastYearColValue = lastYearColValue;
	}
	
    /**
     * @return lastYearColValue
     */
	public java.math.BigDecimal getLastYearColValue() {
		return this.lastYearColValue;
	}
	
	/**
	 * @param lastYearGasValue
	 */
	public void setLastYearGasValue(java.math.BigDecimal lastYearGasValue) {
		this.lastYearGasValue = lastYearGasValue;
	}
	
    /**
     * @return lastYearGasValue
     */
	public java.math.BigDecimal getLastYearGasValue() {
		return this.lastYearGasValue;
	}
	
	/**
	 * @param lastYearWaterValue
	 */
	public void setLastYearWaterValue(java.math.BigDecimal lastYearWaterValue) {
		this.lastYearWaterValue = lastYearWaterValue;
	}
	
    /**
     * @return lastYearWaterValue
     */
	public java.math.BigDecimal getLastYearWaterValue() {
		return this.lastYearWaterValue;
	}
	
	/**
	 * @param lastYearOtherValue
	 */
	public void setLastYearOtherValue(java.math.BigDecimal lastYearOtherValue) {
		this.lastYearOtherValue = lastYearOtherValue;
	}
	
    /**
     * @return lastYearOtherValue
     */
	public java.math.BigDecimal getLastYearOtherValue() {
		return this.lastYearOtherValue;
	}
	
	/**
	 * @param lastYearEnergyDesc
	 */
	public void setLastYearEnergyDesc(String lastYearEnergyDesc) {
		this.lastYearEnergyDesc = lastYearEnergyDesc;
	}
	
    /**
     * @return lastYearEnergyDesc
     */
	public String getLastYearEnergyDesc() {
		return this.lastYearEnergyDesc;
	}
	
	/**
	 * @param lastTwoYearElValue
	 */
	public void setLastTwoYearElValue(java.math.BigDecimal lastTwoYearElValue) {
		this.lastTwoYearElValue = lastTwoYearElValue;
	}
	
    /**
     * @return lastTwoYearElValue
     */
	public java.math.BigDecimal getLastTwoYearElValue() {
		return this.lastTwoYearElValue;
	}
	
	/**
	 * @param lastTwoYearColValue
	 */
	public void setLastTwoYearColValue(java.math.BigDecimal lastTwoYearColValue) {
		this.lastTwoYearColValue = lastTwoYearColValue;
	}
	
    /**
     * @return lastTwoYearColValue
     */
	public java.math.BigDecimal getLastTwoYearColValue() {
		return this.lastTwoYearColValue;
	}
	
	/**
	 * @param lastTwoYearGasValue
	 */
	public void setLastTwoYearGasValue(java.math.BigDecimal lastTwoYearGasValue) {
		this.lastTwoYearGasValue = lastTwoYearGasValue;
	}
	
    /**
     * @return lastTwoYearGasValue
     */
	public java.math.BigDecimal getLastTwoYearGasValue() {
		return this.lastTwoYearGasValue;
	}
	
	/**
	 * @param lastTwoYearWaterValue
	 */
	public void setLastTwoYearWaterValue(java.math.BigDecimal lastTwoYearWaterValue) {
		this.lastTwoYearWaterValue = lastTwoYearWaterValue;
	}
	
    /**
     * @return lastTwoYearWaterValue
     */
	public java.math.BigDecimal getLastTwoYearWaterValue() {
		return this.lastTwoYearWaterValue;
	}
	
	/**
	 * @param lastTwoYearOtherValue
	 */
	public void setLastTwoYearOtherValue(java.math.BigDecimal lastTwoYearOtherValue) {
		this.lastTwoYearOtherValue = lastTwoYearOtherValue;
	}
	
    /**
     * @return lastTwoYearOtherValue
     */
	public java.math.BigDecimal getLastTwoYearOtherValue() {
		return this.lastTwoYearOtherValue;
	}
	
	/**
	 * @param lastTwoYearEnergyDesc
	 */
	public void setLastTwoYearEnergyDesc(String lastTwoYearEnergyDesc) {
		this.lastTwoYearEnergyDesc = lastTwoYearEnergyDesc;
	}
	
    /**
     * @return lastTwoYearEnergyDesc
     */
	public String getLastTwoYearEnergyDesc() {
		return this.lastTwoYearEnergyDesc;
	}
	
	/**
	 * @param payWagesMonth
	 */
	public void setPayWagesMonth(String payWagesMonth) {
		this.payWagesMonth = payWagesMonth;
	}
	
    /**
     * @return payWagesMonth
     */
	public String getPayWagesMonth() {
		return this.payWagesMonth;
	}
	
	/**
	 * @param payWagesCur
	 */
	public void setPayWagesCur(java.math.BigDecimal payWagesCur) {
		this.payWagesCur = payWagesCur;
	}
	
    /**
     * @return payWagesCur
     */
	public java.math.BigDecimal getPayWagesCur() {
		return this.payWagesCur;
	}
	
	/**
	 * @param payWagesTotal
	 */
	public void setPayWagesTotal(java.math.BigDecimal payWagesTotal) {
		this.payWagesTotal = payWagesTotal;
	}
	
    /**
     * @return payWagesTotal
     */
	public java.math.BigDecimal getPayWagesTotal() {
		return this.payWagesTotal;
	}
	
	/**
	 * @param payWagesRemark
	 */
	public void setPayWagesRemark(String payWagesRemark) {
		this.payWagesRemark = payWagesRemark;
	}
	
    /**
     * @return payWagesRemark
     */
	public String getPayWagesRemark() {
		return this.payWagesRemark;
	}
	
	/**
	 * @param lastYearTaxTotal
	 */
	public void setLastYearTaxTotal(java.math.BigDecimal lastYearTaxTotal) {
		this.lastYearTaxTotal = lastYearTaxTotal;
	}
	
    /**
     * @return lastYearTaxTotal
     */
	public java.math.BigDecimal getLastYearTaxTotal() {
		return this.lastYearTaxTotal;
	}
	
	/**
	 * @param lastYearTaxDesc
	 */
	public void setLastYearTaxDesc(String lastYearTaxDesc) {
		this.lastYearTaxDesc = lastYearTaxDesc;
	}
	
    /**
     * @return lastYearTaxDesc
     */
	public String getLastYearTaxDesc() {
		return this.lastYearTaxDesc;
	}
	
	/**
	 * @param lastTwoYearTaxTotal
	 */
	public void setLastTwoYearTaxTotal(java.math.BigDecimal lastTwoYearTaxTotal) {
		this.lastTwoYearTaxTotal = lastTwoYearTaxTotal;
	}
	
    /**
     * @return lastTwoYearTaxTotal
     */
	public java.math.BigDecimal getLastTwoYearTaxTotal() {
		return this.lastTwoYearTaxTotal;
	}
	
	/**
	 * @param lastTwoYearTaxDesc
	 */
	public void setLastTwoYearTaxDesc(String lastTwoYearTaxDesc) {
		this.lastTwoYearTaxDesc = lastTwoYearTaxDesc;
	}
	
    /**
     * @return lastTwoYearTaxDesc
     */
	public String getLastTwoYearTaxDesc() {
		return this.lastTwoYearTaxDesc;
	}
	
	/**
	 * @param businessIncomeCurMonth
	 */
	public void setBusinessIncomeCurMonth(String businessIncomeCurMonth) {
		this.businessIncomeCurMonth = businessIncomeCurMonth;
	}
	
    /**
     * @return businessIncomeCurMonth
     */
	public String getBusinessIncomeCurMonth() {
		return this.businessIncomeCurMonth;
	}
	
	/**
	 * @param curMonthBusinessIncome
	 */
	public void setCurMonthBusinessIncome(String curMonthBusinessIncome) {
		this.curMonthBusinessIncome = curMonthBusinessIncome;
	}
	
    /**
     * @return curMonthBusinessIncome
     */
	public String getCurMonthBusinessIncome() {
		return this.curMonthBusinessIncome;
	}
	
	/**
	 * @param lastYearBusinessIncome
	 */
	public void setLastYearBusinessIncome(String lastYearBusinessIncome) {
		this.lastYearBusinessIncome = lastYearBusinessIncome;
	}
	
    /**
     * @return lastYearBusinessIncome
     */
	public String getLastYearBusinessIncome() {
		return this.lastYearBusinessIncome;
	}
	
	/**
	 * @param businessIncomeRemark
	 */
	public void setBusinessIncomeRemark(String businessIncomeRemark) {
		this.businessIncomeRemark = businessIncomeRemark;
	}
	
    /**
     * @return businessIncomeRemark
     */
	public String getBusinessIncomeRemark() {
		return this.businessIncomeRemark;
	}
	
	/**
	 * @param scosaoeiob
	 */
	public void setScosaoeiob(java.math.BigDecimal scosaoeiob) {
		this.scosaoeiob = scosaoeiob;
	}
	
    /**
     * @return scosaoeiob
     */
	public java.math.BigDecimal getScosaoeiob() {
		return this.scosaoeiob;
	}
	
	/**
	 * @param scosaoeiobDesc
	 */
	public void setScosaoeiobDesc(String scosaoeiobDesc) {
		this.scosaoeiobDesc = scosaoeiobDesc;
	}
	
    /**
     * @return scosaoeiobDesc
     */
	public String getScosaoeiobDesc() {
		return this.scosaoeiobDesc;
	}
	
	/**
	 * @param overdueLoanPrincipalTimes
	 */
	public void setOverdueLoanPrincipalTimes(String overdueLoanPrincipalTimes) {
		this.overdueLoanPrincipalTimes = overdueLoanPrincipalTimes;
	}
	
    /**
     * @return overdueLoanPrincipalTimes
     */
	public String getOverdueLoanPrincipalTimes() {
		return this.overdueLoanPrincipalTimes;
	}
	
	/**
	 * @param loanPrincipalLongestOverdueDays
	 */
	public void setLoanPrincipalLongestOverdueDays(String loanPrincipalLongestOverdueDays) {
		this.loanPrincipalLongestOverdueDays = loanPrincipalLongestOverdueDays;
	}
	
    /**
     * @return loanPrincipalLongestOverdueDays
     */
	public String getLoanPrincipalLongestOverdueDays() {
		return this.loanPrincipalLongestOverdueDays;
	}
	
	/**
	 * @param recordTimesOfLoanInterest
	 */
	public void setRecordTimesOfLoanInterest(String recordTimesOfLoanInterest) {
		this.recordTimesOfLoanInterest = recordTimesOfLoanInterest;
	}
	
    /**
     * @return recordTimesOfLoanInterest
     */
	public String getRecordTimesOfLoanInterest() {
		return this.recordTimesOfLoanInterest;
	}
	
	/**
	 * @param creditCardAccumulatedOverdueTimes
	 */
	public void setCreditCardAccumulatedOverdueTimes(String creditCardAccumulatedOverdueTimes) {
		this.creditCardAccumulatedOverdueTimes = creditCardAccumulatedOverdueTimes;
	}
	
    /**
     * @return creditCardAccumulatedOverdueTimes
     */
	public String getCreditCardAccumulatedOverdueTimes() {
		return this.creditCardAccumulatedOverdueTimes;
	}
	
	/**
	 * @param maximumCumulativeOverdueTimes
	 */
	public void setMaximumCumulativeOverdueTimes(String maximumCumulativeOverdueTimes) {
		this.maximumCumulativeOverdueTimes = maximumCumulativeOverdueTimes;
	}
	
    /**
     * @return maximumCumulativeOverdueTimes
     */
	public String getMaximumCumulativeOverdueTimes() {
		return this.maximumCumulativeOverdueTimes;
	}
	
	/**
	 * @param cioeaac
	 */
	public void setCioeaac(String cioeaac) {
		this.cioeaac = cioeaac;
	}
	
    /**
     * @return cioeaac
     */
	public String getCioeaac() {
		return this.cioeaac;
	}
	
	/**
	 * @param extGuaranteeAmt
	 */
	public void setExtGuaranteeAmt(java.math.BigDecimal extGuaranteeAmt) {
		this.extGuaranteeAmt = extGuaranteeAmt;
	}
	
    /**
     * @return extGuaranteeAmt
     */
	public java.math.BigDecimal getExtGuaranteeAmt() {
		return this.extGuaranteeAmt;
	}
	
	/**
	 * @param extGuaranteeAmtDesc
	 */
	public void setExtGuaranteeAmtDesc(String extGuaranteeAmtDesc) {
		this.extGuaranteeAmtDesc = extGuaranteeAmtDesc;
	}
	
    /**
     * @return extGuaranteeAmtDesc
     */
	public String getExtGuaranteeAmtDesc() {
		return this.extGuaranteeAmtDesc;
	}
	
	/**
	 * @param extMortgageAmt
	 */
	public void setExtMortgageAmt(java.math.BigDecimal extMortgageAmt) {
		this.extMortgageAmt = extMortgageAmt;
	}
	
    /**
     * @return extMortgageAmt
     */
	public java.math.BigDecimal getExtMortgageAmt() {
		return this.extMortgageAmt;
	}
	
	/**
	 * @param extMortgageAmtDesc
	 */
	public void setExtMortgageAmtDesc(String extMortgageAmtDesc) {
		this.extMortgageAmtDesc = extMortgageAmtDesc;
	}
	
    /**
     * @return extMortgageAmtDesc
     */
	public String getExtMortgageAmtDesc() {
		return this.extMortgageAmtDesc;
	}
	
	/**
	 * @param extPledgeAmt
	 */
	public void setExtPledgeAmt(java.math.BigDecimal extPledgeAmt) {
		this.extPledgeAmt = extPledgeAmt;
	}
	
    /**
     * @return extPledgeAmt
     */
	public java.math.BigDecimal getExtPledgeAmt() {
		return this.extPledgeAmt;
	}
	
	/**
	 * @param extPledgeAmtDesc
	 */
	public void setExtPledgeAmtDesc(String extPledgeAmtDesc) {
		this.extPledgeAmtDesc = extPledgeAmtDesc;
	}
	
    /**
     * @return extPledgeAmtDesc
     */
	public String getExtPledgeAmtDesc() {
		return this.extPledgeAmtDesc;
	}
	
	/**
	 * @param focusDdryq1
	 */
	public void setFocusDdryq1(String focusDdryq1) {
		this.focusDdryq1 = focusDdryq1;
	}
	
    /**
     * @return focusDdryq1
     */
	public String getFocusDdryq1() {
		return this.focusDdryq1;
	}
	
	/**
	 * @param focusDdryq2
	 */
	public void setFocusDdryq2(String focusDdryq2) {
		this.focusDdryq2 = focusDdryq2;
	}
	
    /**
     * @return focusDdryq2
     */
	public String getFocusDdryq2() {
		return this.focusDdryq2;
	}
	
	/**
	 * @param focusDdryq3
	 */
	public void setFocusDdryq3(String focusDdryq3) {
		this.focusDdryq3 = focusDdryq3;
	}
	
    /**
     * @return focusDdryq3
     */
	public String getFocusDdryq3() {
		return this.focusDdryq3;
	}
	
	/**
	 * @param focusDdryq4
	 */
	public void setFocusDdryq4(String focusDdryq4) {
		this.focusDdryq4 = focusDdryq4;
	}
	
    /**
     * @return focusDdryq4
     */
	public String getFocusDdryq4() {
		return this.focusDdryq4;
	}
	
	/**
	 * @param focusDdryq5
	 */
	public void setFocusDdryq5(String focusDdryq5) {
		this.focusDdryq5 = focusDdryq5;
	}
	
    /**
     * @return focusDdryq5
     */
	public String getFocusDdryq5() {
		return this.focusDdryq5;
	}
	
	/**
	 * @param focusDdryq6
	 */
	public void setFocusDdryq6(String focusDdryq6) {
		this.focusDdryq6 = focusDdryq6;
	}
	
    /**
     * @return focusDdryq6
     */
	public String getFocusDdryq6() {
		return this.focusDdryq6;
	}
	
	/**
	 * @param focusDdryq7
	 */
	public void setFocusDdryq7(String focusDdryq7) {
		this.focusDdryq7 = focusDdryq7;
	}
	
    /**
     * @return focusDdryq7
     */
	public String getFocusDdryq7() {
		return this.focusDdryq7;
	}
	
	/**
	 * @param focusDdryq8
	 */
	public void setFocusDdryq8(String focusDdryq8) {
		this.focusDdryq8 = focusDdryq8;
	}
	
    /**
     * @return focusDdryq8
     */
	public String getFocusDdryq8() {
		return this.focusDdryq8;
	}
	
	/**
	 * @param focusDdryq9
	 */
	public void setFocusDdryq9(String focusDdryq9) {
		this.focusDdryq9 = focusDdryq9;
	}
	
    /**
     * @return focusDdryq9
     */
	public String getFocusDdryq9() {
		return this.focusDdryq9;
	}
	
	/**
	 * @param focusDdryq10
	 */
	public void setFocusDdryq10(String focusDdryq10) {
		this.focusDdryq10 = focusDdryq10;
	}
	
    /**
     * @return focusDdryq10
     */
	public String getFocusDdryq10() {
		return this.focusDdryq10;
	}
	
	/**
	 * @param focusDdryq11
	 */
	public void setFocusDdryq11(String focusDdryq11) {
		this.focusDdryq11 = focusDdryq11;
	}
	
    /**
     * @return focusDdryq11
     */
	public String getFocusDdryq11() {
		return this.focusDdryq11;
	}
	
	/**
	 * @param focusDdryq12
	 */
	public void setFocusDdryq12(String focusDdryq12) {
		this.focusDdryq12 = focusDdryq12;
	}
	
    /**
     * @return focusDdryq12
     */
	public String getFocusDdryq12() {
		return this.focusDdryq12;
	}


}