/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.IqpSurveyConInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCusCreditConInfo;
import cn.com.yusys.yusp.service.IqpCusCreditConInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditConInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-15 15:09:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcuscreditconinfo")
public class IqpCusCreditConInfoResource {
    @Autowired
    private IqpCusCreditConInfoService iqpCusCreditConInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCusCreditConInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCusCreditConInfo> list = iqpCusCreditConInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpCusCreditConInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpCusCreditConInfo>> index(QueryModel queryModel) {
        List<IqpCusCreditConInfo> list = iqpCusCreditConInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpCusCreditConInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpCusCreditConInfo> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpCusCreditConInfo iqpCusCreditConInfo = iqpCusCreditConInfoService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpCusCreditConInfo>(iqpCusCreditConInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCusCreditConInfo> create(@RequestBody IqpCusCreditConInfo iqpCusCreditConInfo) throws URISyntaxException {
        iqpCusCreditConInfoService.insert(iqpCusCreditConInfo);
        return new ResultDto<IqpCusCreditConInfo>(iqpCusCreditConInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCusCreditConInfo iqpCusCreditConInfo) throws URISyntaxException {
        int result = iqpCusCreditConInfoService.update(iqpCusCreditConInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpCusCreditConInfoService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCusCreditConInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectbyprimarykey
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyprimarykey")
    protected ResultDto<IqpCusCreditConInfo> selectbyprimarykey(@RequestBody IqpCusCreditConInfo iqpCusCreditConInfo) {
        return new ResultDto<IqpCusCreditConInfo>(iqpCusCreditConInfoService.selectByPrimaryKey(iqpCusCreditConInfo.getIqpSerno()));
    }


}
