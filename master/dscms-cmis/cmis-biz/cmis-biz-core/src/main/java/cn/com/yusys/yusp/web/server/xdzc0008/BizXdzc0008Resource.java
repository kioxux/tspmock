package cn.com.yusys.yusp.web.server.xdzc0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0008.req.Xdzc0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.resp.Xdzc0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0008.Xdzc0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:资产池出票校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0008:出票校验（开立银行承兑汇票校验）")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0008Resource.class);

    @Autowired
    private Xdzc0008Service xdzc0008Service;



    /**
     * 交易码：xdzc0008
     * 交易描述：
     * ►资产池协议状态为有效；
     *  100 未生效  200 生效   500 中止 600 注销  700 撤回  800作废
     * ►本次开票金额*（1 - 保证金留存比例）<= 资产池可用融资额度；
     * ►到期日<=资产池协议到期日 + 授信批复宽限期。
     * @param xdzc0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出票校验（开立银行承兑汇票校验）")
    @PostMapping("/xdzc0008")
    protected @ResponseBody
    ResultDto<Xdzc0008DataRespDto> xdzc0008(@Validated @RequestBody Xdzc0008DataReqDto xdzc0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008DataReqDto));
        Xdzc0008DataRespDto xdzc0008DataRespDto = new Xdzc0008DataRespDto();// 响应Dto:资产池出票校验接口
        ResultDto<Xdzc0008DataRespDto> xdzc0008DataResultDto = new ResultDto<>();
        String contNo = xdzc0008DataReqDto.getContNo();//协议编号
        BigDecimal sumAmt = xdzc0008DataReqDto.getBillAmt();//票面总金额
        try {

            xdzc0008DataRespDto = xdzc0008Service.xdzc0008Service(xdzc0008DataReqDto);
            xdzc0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, e.getMessage());
            // 封装xdzc0008DataResultDto中异常返回码和返回信息
            xdzc0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0008DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0008DataRespDto到xdzc0008DataResultDto中
        xdzc0008DataResultDto.setData(xdzc0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008DataResultDto));
        return xdzc0008DataResultDto;
    }
}
