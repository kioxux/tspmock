package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.RetailPrimeRateApprMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 零售优惠利率申请审批流程业务处理类
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class QTSX01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(QTSX01BizService.class);

    @Autowired
    private BGYW01BizService bgyw01BizService;
    @Autowired
    private RetailPrimeRateAppService retailPrimeRateAppService;
    @Autowired
    private RetailPrimeRateApprService retailPrimeRateApprService;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private RetailPrimeRateReplyInfoService retailPrimeRateReplyInfoService;
    @Autowired
    private RetailPrimeRateApprMapper retailPrimeRateApprMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG003.equals(bizType)) {
            //BG003零售利率变更（对公人民币）
            bgyw01BizService.bizOp(resultInstanceDto);
        }else if("QT001".equals(bizType)){
            this.iRetailPrimeRateBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    private void iRetailPrimeRateBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppService.selectByPrimaryKey(serno);
            WFBizParamDto wfbizParamDtoParam = new WFBizParamDto();
            wfbizParamDtoParam.setBizId(instanceInfo.getBizId());
            wfbizParamDtoParam.setInstanceId(instanceInfo.getInstanceId());
            Map<String, Object> params = new HashMap<>();
            params = instanceInfo.getParam();
            log.info("流程变量前:" + params.toString());
            if (!OpType.REFUSE.equals(instanceInfo.getCurrentOpType())) { // 否决除外
                params.put("nextSubmitNodeId", instanceInfo.getNextNodeInfos().get(0).getNextNodeId());
            }
            log.info("流程变量后:" + params.toString());
            wfbizParamDtoParam.setParam(params);
            workflowCoreClient.updateFlowParam(wfbizParamDtoParam);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                if("193_6".equals(instanceInfo.getNodeId()) ||"193_5".equals(instanceInfo.getNodeId())  ){
                    Map param = new HashMap();
                    param.put("serno",instanceInfo.getBizId());
                    param.put("approvePost",instanceInfo.getNodeId());
                    RetailPrimeRateAppr record = retailPrimeRateApprMapper.selectBySernoAndNode(param);
                    if(record !=null){
                        record.setApproveConclusion(instanceInfo.getComment().getCommentSign());
                        record.setApproveAdvice(instanceInfo.getComment().getUserComment());
                        retailPrimeRateApprMapper.updateByPrimaryKeySelective(record);
                    }
                }
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                RetailPrimeRateAppr retailPrimeRateApprs = retailPrimeRateApprService.selectBySernoOrderByDate(serno);
                //TODO 将申请表和审批表中的数据保存在复批表中
                RetailPrimeRateReplyInfo retailPrimeRateReplyInfo = new RetailPrimeRateReplyInfo();
                BeanUtils.copyProperties(retailPrimeRateApp, retailPrimeRateReplyInfo);
                retailPrimeRateReplyInfo.setReplySerno(serno);
                retailPrimeRateReplyInfo.setSerno(serno);
                retailPrimeRateReplyInfo.setReplyRate(retailPrimeRateApprs.getReplyRate());
                int result = retailPrimeRateReplyInfoService.insertSelective(retailPrimeRateReplyInfo);
                if(result !=1){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "优惠利率批复信息新增异常！");
                }
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);

                //将优惠利率更新到申请与合同
                String iqpSerno = retailPrimeRateApp.getIqpSerno() == null ?"" : retailPrimeRateApp.getIqpSerno();
                if(!"".equals(iqpSerno)){
                    IqpLoanApp IqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);

                    String  approveStatus = IqpLoanApp.getApproveStatus();

                    if("000".equals(approveStatus)){//待发起：更新申请利率
                        IqpLoanApp.setExecRateYear(retailPrimeRateApp.getAppRate());
                        iqpLoanAppService.updateSelective(IqpLoanApp);

                    }else if("997".equals(approveStatus)){//审批通过：更新合同利率
                        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByIqpSerno(iqpSerno);
                        String contStatus = ctrLoanCont.getContStatus() == null ?"":ctrLoanCont.getContStatus();
                        if("100".equals(ctrLoanCont.getContStatus()) || "".equals(contStatus)){
                            ctrLoanCont.setExecRateYear(retailPrimeRateApp.getAppRate());
                            ctrLoanContService.updateSelective(ctrLoanCont);
                            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(iqpSerno);
                            lmtCrdReplyInfo.setExecRateYear(retailPrimeRateApp.getAppRate());
                            lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
                        }
                    }
                }
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回操作:" + instanceInfo);
                    retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    retailPrimeRateAppService.updateSelective(retailPrimeRateApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);

                log.info("-------业务否决：-- ----" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.QTSX01.equals(flowCode);
    }
}
