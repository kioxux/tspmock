package cn.com.yusys.yusp.web.server.xdtz0063;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0063.req.Xdtz0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.resp.Xdtz0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0063.Xdtz0063Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:他行退汇冻结编号更新
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0063:他行退汇冻结编号更新")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0063Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0063Resource.class);
    @Autowired
    private Xdtz0063Service xdtz0063Service;

    /**
     * 交易码：xdtz0063
     * 交易描述：他行退汇冻结编号更新
     *
     * @param xdtz0063DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("他行退汇冻结编号更新")
    @PostMapping("/xdtz0063")
    protected @ResponseBody
    ResultDto<Xdtz0063DataRespDto> xdtz0063(@Validated @RequestBody Xdtz0063DataReqDto xdtz0063DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataReqDto));
        Xdtz0063DataRespDto xdtz0063DataRespDto = new Xdtz0063DataRespDto();// 响应Dto:更新信贷台账信息
        ResultDto<Xdtz0063DataRespDto> xdtz0063DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0063DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataReqDto));
            xdtz0063DataRespDto = xdtz0063Service.xdtz0063(xdtz0063DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataRespDto));
            // 封装xdtz0063DataResultDto中正确的返回码和返回信息
            xdtz0063DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0063DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, e.getMessage());
            // 封装xdtz0063DataResultDto中异常返回码和返回信息
            xdtz0063DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0063DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0063DataRespDto到xdtz0063DataResultDto中
        xdtz0063DataResultDto.setData(xdtz0063DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataResultDto));
        return xdtz0063DataResultDto;
    }
}
