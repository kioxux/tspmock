/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysJqdCustomer;
import cn.com.yusys.yusp.service.RptSpdAnysJqdCustomerService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJqdCustomerResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:30:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysjqdcustomer")
public class RptSpdAnysJqdCustomerResource {
    @Autowired
    private RptSpdAnysJqdCustomerService rptSpdAnysJqdCustomerService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysJqdCustomer>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysJqdCustomer> list = rptSpdAnysJqdCustomerService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysJqdCustomer>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysJqdCustomer>> index(QueryModel queryModel) {
        List<RptSpdAnysJqdCustomer> list = rptSpdAnysJqdCustomerService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysJqdCustomer>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptSpdAnysJqdCustomer> show(@PathVariable("pkId") String pkId) {
        RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer = rptSpdAnysJqdCustomerService.selectByPrimaryKey(pkId);
        return new ResultDto<RptSpdAnysJqdCustomer>(rptSpdAnysJqdCustomer);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysJqdCustomer> create(@RequestBody RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer) throws URISyntaxException {
        rptSpdAnysJqdCustomerService.insert(rptSpdAnysJqdCustomer);
        return new ResultDto<RptSpdAnysJqdCustomer>(rptSpdAnysJqdCustomer);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer) throws URISyntaxException {
        int result = rptSpdAnysJqdCustomerService.update(rptSpdAnysJqdCustomer);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptSpdAnysJqdCustomerService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysJqdCustomerService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptSpdAnysJqdCustomer>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptSpdAnysJqdCustomer>>(rptSpdAnysJqdCustomerService.selectByModel(model));
    }

    @PostMapping("/updateCustomer")
    protected ResultDto<Integer> updateCustomer(@RequestBody RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer){
        return new ResultDto<Integer>(rptSpdAnysJqdCustomerService.updateSelective(rptSpdAnysJqdCustomer));
    }

    @PostMapping("/insertCustomer")
    protected ResultDto<Integer> insertCustomer(@RequestBody RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer){
        rptSpdAnysJqdCustomer.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptSpdAnysJqdCustomerService.insertSelective(rptSpdAnysJqdCustomer));
    }

    @PostMapping("/deleteCustomer")
    protected ResultDto<Integer> deleteCustomer(@RequestBody RptSpdAnysJqdCustomer rptSpdAnysJqdCustomer){
        String pkId = rptSpdAnysJqdCustomer.getPkId();
        return new ResultDto<Integer>(rptSpdAnysJqdCustomerService.deleteByPrimaryKey(pkId));
    }
}
