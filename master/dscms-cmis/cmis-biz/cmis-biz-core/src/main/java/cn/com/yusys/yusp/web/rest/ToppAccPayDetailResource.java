/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ToppAccPayDetail;
import cn.com.yusys.yusp.service.ToppAccPayDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAccPayDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 22:53:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/toppaccpaydetail")
public class ToppAccPayDetailResource {
    @Autowired
    private ToppAccPayDetailService toppAccPayDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ToppAccPayDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<ToppAccPayDetail> list = toppAccPayDetailService.selectAll(queryModel);
        return new ResultDto<List<ToppAccPayDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ToppAccPayDetail>> index(QueryModel queryModel) {
        List<ToppAccPayDetail> list = toppAccPayDetailService.selectByModel(queryModel);
        return new ResultDto<List<ToppAccPayDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<ToppAccPayDetail> show(@PathVariable("serno") String serno) {
        ToppAccPayDetail toppAccPayDetail = toppAccPayDetailService.selectByPrimaryKey(serno);
        return new ResultDto<ToppAccPayDetail>(toppAccPayDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ToppAccPayDetail> create(@RequestBody ToppAccPayDetail toppAccPayDetail) throws URISyntaxException {
        toppAccPayDetailService.insert(toppAccPayDetail);
        return new ResultDto<ToppAccPayDetail>(toppAccPayDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ToppAccPayDetail toppAccPayDetail) throws URISyntaxException {
        int result = toppAccPayDetailService.update(toppAccPayDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = toppAccPayDetailService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = toppAccPayDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 受托支付模块列表
     * @param queryModel
     * @return
     */
    @ApiOperation("受托支付模块列表")
    @PostMapping("/toppDetailList")
    protected ResultDto<List<ToppAccPayDetail>> toppDetailList(@RequestBody QueryModel queryModel) {
        List<ToppAccPayDetail> list = toppAccPayDetailService.toppDetailList(queryModel);
        return new ResultDto<List<ToppAccPayDetail>>(list);
    }

    /**
     * 受托支付模块历史列表
     * @param queryModel
     * @return
     */
    @ApiOperation("受托支付模块历史列表")
    @PostMapping("/toppDetailHisList")
    protected ResultDto<List<ToppAccPayDetail>> toppDetailHisList(@RequestBody QueryModel queryModel) {
        List<ToppAccPayDetail> list = toppAccPayDetailService.toppDetailHisList(queryModel);
        return new ResultDto<List<ToppAccPayDetail>>(list);
    }

    /**
     * 查询详细的信息
     * @param map
     * @return
     */
    @PostMapping("/showdetial")
    protected ResultDto<ToppAccPayDetail> showDetail(@RequestBody Map map) {
        String serno =(String) map.get("serno");
        ToppAccPayDetail toppAccPayDetail = toppAccPayDetailService.selectByPrimaryKey(serno);
        return new ResultDto<ToppAccPayDetail>(toppAccPayDetail);
    }

    /**
     * @函数名称:sendToppAccPay
     * @函数描述:发送二代受托支付
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendToppAccPay")
    protected ResultDto<ToppAccPayDetail> sendToppAccPay(@RequestBody String serno) {
        return  ResultDto.success(toppAccPayDetailService.sendToppAccPay(serno));
    }

    /**
     * @函数名称:sendToppAccPay
     * @函数描述:查询二代受托支付结果
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectToppAccPayResult")
    protected ResultDto<Integer> selectToppAccPayResult(@RequestBody String serno) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        ToppAccPayDetail toppAccPayDetail = toppAccPayDetailService.selectToppAccPayResult(serno);
        if(Objects.equals("2",toppAccPayDetail.getStatus())) {
            resultDto.setCode(0);
            resultDto.setData(1);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(300);
            resultDto.setData(0);
            resultDto.setMessage(Objects.equals("已清算",toppAccPayDetail.getReasonFirst()) ? toppAccPayDetail.getReasonSecond() : toppAccPayDetail.getReasonFirst());
        }
        return resultDto;
    }

    /**
     * @函数名称:sendToppAccPay
     * @函数描述:轮询查询任务-从二代支付系统中查询受托支付结果
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateToppAccPayDetailBatch")
    protected ResultDto<Integer> selectToppAccPayResult() {
        return  ResultDto.success(toppAccPayDetailService.updateToppAccPayDetailBatch());
    }


    /**
     * @函数名称:sendToppAccPay
     * @函数描述:发送受托支付信息变更
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/toppChgAppAdd")
    protected ResultDto<ToppAccPayDetail> toppChgAppAdd(@RequestBody String serno) {
        return  ResultDto.success(toppAccPayDetailService.toppChgAppAdd(serno));
    }
}
