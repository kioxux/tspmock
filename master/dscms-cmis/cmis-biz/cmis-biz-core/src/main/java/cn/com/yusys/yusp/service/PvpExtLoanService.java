/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpExtLoanMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpExtLoanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-13 14:33:32
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PvpExtLoanService {
    private static  final Logger log = LoggerFactory.getLogger(PvpExtLoanService.class);
    @Autowired
    private PvpExtLoanMapper pvpExtLoanMapper;
    @Autowired
    private CtrLoanExtService ctrLoanExtService;
    @Autowired
    private BizCorreManagerInfoService bizCorreManagerInfoService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private AccExtService accExtService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpExtLoan selectByPrimaryKey(String pvpSerno) {
        return pvpExtLoanMapper.selectByPrimaryKey(pvpSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpExtLoan> selectAll(QueryModel model) {
        List<PvpExtLoan> records = (List<PvpExtLoan>) pvpExtLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpExtLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpExtLoan> list = pvpExtLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpExtLoan record) {
        return pvpExtLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PvpExtLoan record) {
        return pvpExtLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpExtLoan record) {
        return pvpExtLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PvpExtLoan record) {
        return pvpExtLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pvpSerno) {
        return pvpExtLoanMapper.deleteByPrimaryKey(pvpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpExtLoanMapper.deleteByIds(ids);
    }

    /**
     * 合同撤销时,检查是否放款
     * 展期出账表存在状态不为否决998的记录 opr_type=01
     * @param extCtrNo
     * @return
     */
    public int chechExtAccLoan(String extCtrNo) {
        return pvpExtLoanMapper.chechExtAccLoan(extCtrNo);
    }

    /**
     * 新增展期放款信息
     * 新增办理人员信息
     * @param pvpExtLoan
     * @return
     */
    public int addIPvpExtLoan(PvpExtLoan pvpExtLoan) {
        //新增展期放款信息
        int result = 0;
        String pvpSerno = pvpExtLoan.getPvpSerno();
        String extCtrNo = pvpExtLoan.getExtCtrNo();
        String billNo = pvpExtLoan.getOldBillNo();
        if(StringUtils.isBlank(billNo)){
            throw new YuspException(EcbEnum.E_IQP_INSERT_FAILED.key, "原借据信息不存在！");
        }
        if(StringUtils.isBlank(extCtrNo)){
            throw new YuspException(EcbEnum.E_IQP_INSERT_FAILED.key, "展期协议信息不存在！");
        }
        if(StringUtils.isBlank(pvpSerno)){
            throw new YuspException(EcbEnum.E_IQP_INSERT_FAILED.key, "展期放款编号不存在！");
        }
        AccLoan accLoan = accLoanService.selectByPrimaryKey(billNo);

        //查询展期协议信息
        CtrLoanExt ctrLoanExt = ctrLoanExtService.selectByPrimaryKey(extCtrNo);
        PvpExtLoan pvpExtLoanReal = new PvpExtLoan();
        //塞入原出账流水号
        pvpExtLoanReal.setOldPvpSerno(accLoan.getPvpSerno());

        BeanUtils.copyProperties(ctrLoanExt,pvpExtLoanReal);
        BigDecimal extRealityIrY = ctrLoanExt.getExtRealityIrY();
        pvpExtLoanReal.setRealityIrY(extRealityIrY);
        BigDecimal extRealityIrM = ctrLoanExt.getExtRealityIrM();
        pvpExtLoanReal.setRealityIrM(extRealityIrM);
        //处理登记人等相同属性的字段 不能用展期协议里面的
        pvpExtLoanReal.setPvpSerno(pvpSerno);
        pvpExtLoanReal.setManagerBrId(pvpExtLoan.getManagerBrId());
        pvpExtLoanReal.setInputId(pvpExtLoan.getInputId());
        pvpExtLoanReal.setInputBrId(pvpExtLoan.getInputBrId());
        pvpExtLoanReal.setInputDate(pvpExtLoan.getInputDate());
        pvpExtLoanReal.setUpdId(pvpExtLoan.getUpdId());
        pvpExtLoanReal.setUpdBrId(pvpExtLoan.getUpdBrId());
        pvpExtLoanReal.setUpdDate(pvpExtLoan.getUpdDate());
        pvpExtLoanReal.setApproveStatus(pvpExtLoan.getApproveStatus());
        pvpExtLoanReal.setOprType(pvpExtLoan.getOprType());
        //保存信息 先删除后保存
        PvpExtLoan pvpExtLoanOld = pvpExtLoanMapper.selectByPrimaryKey(pvpSerno);
        if(pvpExtLoanOld == null){
            result = pvpExtLoanMapper.insertSelective(pvpExtLoanReal);

            //展期办理人员 登记
            log.info("同步更新办理人员表"+pvpExtLoanReal.getPvpSerno()+"-获取当前登录用户数据");
            User userInfo = null;
            userInfo = SessionUtils.getUserInformation();
            if(userInfo==null){
                throw new YuspException(EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key, EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value);
            }
            BizCorreManagerInfo bizCorreManagerInfo = new BizCorreManagerInfo();
            bizCorreManagerInfo.setCorreMgrType(CmisBizConstants.CORRE_REL_TYPE_MAIN);
            bizCorreManagerInfo.setBizSerno(pvpExtLoanReal.getPvpSerno());
            bizCorreManagerInfo.setCorreManagerId(userInfo.getLoginCode());
            bizCorreManagerInfo.setCorreManagerBrId(userInfo.getOrg().getCode());
            bizCorreManagerInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            bizCorreManagerInfo.setPkId(StringUtils.uuid(true));
            int insertCount = bizCorreManagerInfoService.insertSelective(bizCorreManagerInfo);
            if(insertCount<0){
                throw new YuspException(EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.key, EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.value);
            }
        } else {
            pvpExtLoanMapper.deleteByPrimaryKey(pvpSerno);
            result = pvpExtLoanMapper.insertSelective(pvpExtLoanReal);
        }
        return  result;
    }

    /**
     * 更新出账状态为 997
     * 更新台账信息为出账未确认
     * 还得把manager_id 取出来，烦不烦
     * @param serno
     */
    public void handleBusinessDataAfterEnd(String serno) {
        //更新申请状态为 通过997
        PvpExtLoan pvpExtLoan = pvpExtLoanMapper.selectByPrimaryKey(serno);
        if (pvpExtLoan == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, "展期出账信息不存在,无法更新！ ");
        }
        pvpExtLoan.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        pvpExtLoanMapper.updateByPrimaryKeySelective(pvpExtLoan);
        //取出manager_id
        Map<String, String> params = new HashMap();
        params.put("bizSerno", serno);
        params.put("correMgrType", CmisBizConstants.CORRE_REL_TYPE_MAIN);
        List<BizCorreManagerInfo> bizCorreManagerInfos = bizCorreManagerInfoService.selectBizCorreManagerInfoByBizSerno(params);
        String managerId = bizCorreManagerInfos.get(0).getCorreManagerId();
        //获取序列号
        String extCtrNo = pvpExtLoan.getExtCtrNo();
        params.clear();
        params.put("contNo", extCtrNo);
        String extBillNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, params);
        //塞值
        AccExt accExt = new AccExt();
        accExt.setExtBillNo(extBillNo);
        accExt.setManagerId(managerId);
        BeanUtils.copyProperties(pvpExtLoan,accExt);
        //字段不同,重新塞一下
        accExt.setExtRealityIrM(pvpExtLoan.getRealityIrM());
        accExt.setExtRealityIrY(pvpExtLoan.getRealityIrY());
        //塞入台账状态出账未确认
        accExt.setAccStatus(CmisBizConstants.IQP_ACC_STATUS_0);
        accExtService.insertSelective(accExt);
    }
}
