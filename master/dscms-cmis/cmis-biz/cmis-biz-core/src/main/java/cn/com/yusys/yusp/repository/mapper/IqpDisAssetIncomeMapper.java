/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDisAssetIncome;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncomeMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-12 09:54:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpDisAssetIncomeMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpDisAssetIncome selectByPrimaryKey(@Param("incomePk") String incomePk);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpDisAssetIncome> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpDisAssetIncome record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpDisAssetIncome record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpDisAssetIncome record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpDisAssetIncome record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("incomePk") String incomePk);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    List<IqpDisAssetIncome> selectByIqpSerno(@Param("iqpSernoOld") String iqpSernoOld);

    String getSumIncome(@Param("iqpSerno") String iqpSerno);

    IqpDisAssetIncome selectIqpSerno(@Param("iqpSerno") String iqpSerno);

    List<IqpDisAssetIncome> selectMore(@Param("iqpSerno") String iqpSerno);

    List<IqpDisAssetIncome> selectMoreOther(@Param("iqpSerno") String iqpSerno);


    //根据流水号查询经营与其他收入总计
    BigDecimal selectSubtotalSumByAssetType(@Param("iqpSerno") String iqpSerno);

}