/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.DocImageSpplInfo;
import cn.com.yusys.yusp.dto.DocImageSpplClientDto;
import cn.com.yusys.yusp.dto.DocImageSpplInfoPoDto;
import cn.com.yusys.yusp.service.DocImageSpplInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocImageSpplInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docimagespplinfo")
public class DocImageSpplInfoResource {
    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DocImageSpplInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocImageSpplInfo> list = docImageSpplInfoService.selectAll(queryModel);
        return new ResultDto<List<DocImageSpplInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<DocImageSpplInfo>> index(QueryModel queryModel) {
        List<DocImageSpplInfo> list = docImageSpplInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocImageSpplInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{disiSerno}")
    protected ResultDto<DocImageSpplInfo> show(@PathVariable("disiSerno") String disiSerno) {
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoService.selectByPrimaryKey(disiSerno);
        return new ResultDto<DocImageSpplInfo>(docImageSpplInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DocImageSpplInfo> create(@RequestBody DocImageSpplInfo docImageSpplInfo) throws URISyntaxException {
        docImageSpplInfoService.insert(docImageSpplInfo);
        return new ResultDto<DocImageSpplInfo>(docImageSpplInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocImageSpplInfo docImageSpplInfo) throws URISyntaxException {
        int result = docImageSpplInfoService.update(docImageSpplInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{disiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("disiSerno") String disiSerno) {
        int result = docImageSpplInfoService.deleteByPrimaryKey(disiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docImageSpplInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByCondition
     * @函数描述:根据条件查询影像补扫数据
     * @参数与返回说明:
     * @算法描述:
     * @author cainingbo_yx
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<DocImageSpplInfo>> selectByCondition(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort("inputDate desc");
        }
        List<DocImageSpplInfo> list = docImageSpplInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocImageSpplInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectSernoByCusId
     * @函数描述:根据客户编号查询业务流水号--档案管理/影像补扫
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectSernoByCusId")
    protected ResultDto<List<DocImageSpplInfoPoDto>> selectSernoByCusId(@RequestBody QueryModel queryModel) {
        List<DocImageSpplInfoPoDto> list = docImageSpplInfoService.selectSernoByCusId(queryModel);
        return new ResultDto<List<DocImageSpplInfoPoDto>>(list);
    }

    /**
     * @函数名称:saveDoImageSpplInfo
     * @函数描述:实体类创建
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    @ApiOperation("影像补扫申请新增")
    @PostMapping("/saveDoImageSpplInfo")
    protected ResultDto<String> saveDoImageSpplInfo(@RequestBody DocImageSpplInfo docImageSpplInfo) {
        return docImageSpplInfoService.saveDoImageSpplInfo(docImageSpplInfo);
    }

    /**
     * @函数名称:selectDetailBySerno
     * @函数描述:查询单个对象
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailBySerno")
    protected ResultDto<DocImageSpplInfo> selectDetailBySerno(@RequestBody String disiSerno) {
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoService.selectByPrimaryKey(disiSerno);
        return new ResultDto<DocImageSpplInfo>(docImageSpplInfo);
    }

    /**
     * @函数名称:updateDoImageSpplInfo
     * @函数描述:实体类创建
     * @参数与返回说明:
     * @算法描述:
     * @author :cainingbo_yx
     */
    @PostMapping("/updateDocImageSpplInfo")
    protected ResultDto<Map> updateDocImageSpplInfo(@RequestBody DocImageSpplInfo docImageSpplInfo) throws URISyntaxException {
        Map result = docImageSpplInfoService.updateDocImageSpplInfo(docImageSpplInfo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteBySerno")
    protected ResultDto<Integer> deleteBySerno(@RequestBody String disiSerno) {
        int result = docImageSpplInfoService.deleteByPrimaryKey(disiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectDetailByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公授信
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailByBizSerno")
    protected ResultDto<DocImageSpplInfoPoDto> selectDetailByBizSerno(@RequestBody String bizSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailByBizSerno(bizSerno);
        return new ResultDto<DocImageSpplInfoPoDto>(docImageSpplInfoPoDto);
    }

    /**
     * @函数名称:selectDetailHTByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公合同
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailHTByBizSerno")
    protected ResultDto<DocImageSpplInfoPoDto> selectDetailHTByBizSerno(@RequestBody String disiSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailHTByBizSerno(disiSerno);
        return new ResultDto<DocImageSpplInfoPoDto>(docImageSpplInfoPoDto);
    }

    /**
     * @函数名称:selectDetailFKByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公放款
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailFKByBizSerno")
    protected ResultDto<DocImageSpplInfoPoDto> selectDetailFKByBizSerno(@RequestBody String disiSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailFKByBizSerno(disiSerno);
        return new ResultDto<DocImageSpplInfoPoDto>(docImageSpplInfoPoDto);
    }

    /**
     * @函数名称:selectDetailFKByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公展期
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailZQByBizSerno")
    protected ResultDto<DocImageSpplInfoPoDto> selectDetailZQByBizSerno(@RequestBody String bizSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailZQByBizSerno(bizSerno);
        return new ResultDto<DocImageSpplInfoPoDto>(docImageSpplInfoPoDto);
    }

    /**
     * @函数名称:selectDetailFKByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公担保变更
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    @PostMapping("/selectDetailDBBGByBizSerno")
    protected ResultDto<DocImageSpplInfoPoDto> selectDetailDBBGByBizSerno(@RequestBody String bizSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = docImageSpplInfoService.selectDetailDBBGByBizSerno(bizSerno);
        return new ResultDto<DocImageSpplInfoPoDto>(docImageSpplInfoPoDto);
    }

    /**
     * 系统生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/6/30 10:52
     **/
    @PostMapping("/createDocImageSpplBySys")
    protected ResultDto<Integer> createDocImageSpplBySys(@RequestBody DocImageSpplClientDto docImageSpplClientDto) {
        int result = docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 查询客户大类
     *
     * @author jijian_yx
     * @date 2021/8/30 15:03
     **/
    @PostMapping("/queryCusCatalog")
    protected ResultDto<String> queryCusCatalog(@RequestBody String disiSerno) {
        String result = docImageSpplInfoService.queryCusCatalog(disiSerno);
        return new ResultDto<String>(result);
    }

    /**
     * 获取合作方准入/协议影像目录
     *
     * @author jijian_yx
     * @date 2021/9/8 14:33
     **/
    @PostMapping("/getCoopImageCode")
    protected ResultDto<String> getCoopImageCode(@RequestBody String disiSerno) {
        String result = docImageSpplInfoService.getCoopImageCode(disiSerno);
        return new ResultDto<>(result);
    }

    /**
     * 根据借据号获取出账流水号
     *
     * @author jijian_yx
     * @date 2021/9/16 14:21
     **/
    @PostMapping("/selectPvpSernoByBillNo")
    protected ResultDto<String> selectPvpSernoByBillNo(@RequestBody String billNo) {
        String result = docImageSpplInfoService.selectPvpSernoByBillNo(billNo);
        return new ResultDto<>(result);
    }

    /**
     * 获取合同影像审核业务类型
     *
     * @author jijian_yx
     * @date 2021/10/31 10:33
     **/
    @PostMapping("/getCtrContImageBizType")
    protected ResultDto<String> getCtrContImageBizType(@RequestBody String disiSerno) {
        String result = docImageSpplInfoService.getCtrContImageBizType(disiSerno);
        return new ResultDto<>(result);
    }
}
