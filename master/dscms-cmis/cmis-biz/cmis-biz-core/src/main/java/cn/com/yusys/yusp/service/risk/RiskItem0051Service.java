package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0051Service
 * @类描述: 展期次数校验
 * @功能描述: 展期次数校验
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0051Service {

    @Autowired
    private IqpContExtBillService iqpContExtBillService;

    /**
     * @方法名称: riskItem0051
     * @方法描述: 展期次数校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: macm
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0051(QueryModel queryModel) {
        // 执行结果集
        RiskResultDto riskResultDto = new RiskResultDto();
        // 业务流水号
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        // 执行结果 默认不通过
        String riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_1;
        // 执行结果描述
        String riskResultDesc ="";
        //  根据流水号获取展期申请中借据列表
        QueryModel queryMap = new QueryModel();
        queryMap.addCondition("iqpSerno",iqpSerno);
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryMap);
        for (int i = 0; i < list.size() ; i++) {
            // 获取借据信息
            IqpContExtBill iqpContExtBill = list.get(i);
            // 获取展期次数
            String extTiomes = iqpContExtBill.getExtTimes();
            riskResultDesc += "借据["+iqpContExtBill.getBillNo()+"]展期次数:"+extTiomes+".";
        }
        riskResultDto.setRiskResultType(riskResultType);
        riskResultDto.setRiskResultDesc(riskResultDesc);
        return riskResultDto;
    }
}
