package cn.com.yusys.yusp.service.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * riskItem0011 借款人关联方校验
 */
@Service
public class RiskItem0011Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0011Service.class);


    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private IqpDiscAppService iqpDiscAppService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请

    /**
     * @方法名称: riskItem0011
     * @方法描述: 借款人关联方校验
     * @参数与返回说明:
     * @算法描述: 担保方式=信用时，触发以下拦截规则：若借款人存在客户模块的关联方表中，则拦截
     * 若为授信环节，每一个授信分项中如涉及信用担保均需要校验
     * @创建人: lixy
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0011(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("借款人关联方校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno) || StringUtils.isEmpty(bizType) ) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }

        // 单一客户授信 、集团客户授信
        if (CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX008_TO_SX014.indexOf(bizType + ",") != -1) {
            log.info("借款人关联方校验执行中（单一客户授信 、集团客户授信）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            //LMT_APP_SUB
            List<LmtAppSub> lmtAppSubs = lmtAppSubService.queryLmtAppSubBySerno(serno);
            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                for (LmtAppSub lmtAppSub : lmtAppSubs) {
                    //判断担保方式
                    String guarMode = lmtAppSub.getGuarMode();
                    String cusId1 = lmtAppSub.getCusId();
                    if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(guarMode)) {
                        riskResultDto = checkIsExistRelTable(cusId1);
                        if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                            return riskResultDto;
                        }
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                return riskResultDto;
            }
        }
        // 小微授信调查
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_XW001.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW004.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW005.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW006.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW007.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW008.equals(bizType)) {
            log.info("借款人关联方校验执行中（小微授信调查）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
//            LMT_SURVEY_REPORT_BASIC_INFO
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(serno);
            if (lmtSurveyReportBasicInfo != null) {
                String cusId1 = lmtSurveyReportBasicInfo.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(lmtSurveyReportBasicInfo.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }

        }

        // 零售业务申请
        else if ("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType) || "SGF03".equals(bizType) || "DHF03".equals(bizType)) {
            log.info("借款人关联方校验执行中（零售业务申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null){
                String cusId1 = iqpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpLoanApp.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 最高额授信协议申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX001.equals(bizType)) {
            log.info("借款人关联方校验执行中（最高额授信协议申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
            if (iqpHighAmtAgrApp != null) {
                String cusId1 = iqpHighAmtAgrApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpHighAmtAgrApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 普通贷款合同申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType)) {
            log.info("借款人关联方校验执行中（普通贷款合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                String cusId1 = iqpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpLoanApp.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        // 银承合同申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX006.equals(bizType)) {
            log.info("借款人关联方校验执行中（银承合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByIqpSerno(serno);
            if (iqpAccpApp != null) {
                String cusId1 = iqpAccpApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpAccpApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        // 开证合同申请 YX005
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX005.equals(bizType)) {
            log.info("借款人关联方校验执行中（开证合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByTfLocSernoKey(serno);
            if (iqpTfLocApp != null) {
                String cusId1 = iqpTfLocApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpTfLocApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        // 保函合同申请 YX007iqpcvrgapp
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX007.equals(bizType)) {
            log.info("借款人关联方校验执行中（保函合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if (iqpCvrgApp != null) {
                String cusId1 = iqpCvrgApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpCvrgApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 贴现协议申请 YX009 DGYX02
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX009.equals(bizType)) {
            log.info("借款人关联方校验执行中（贴现协议申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpDiscApp iqpDiscApp = iqpDiscAppService.selectByDiscSernoKey(serno);
            if (iqpDiscApp != null) {
                String cusId1 = iqpDiscApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpDiscApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        // 贸易融资合同申请 YX003
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX003.equals(bizType)) {
            log.info("借款人关联方校验执行中（贸易融资合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                String cusId1 = iqpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpLoanApp.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 福费廷合同申请 YX004
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX004.equals(bizType)) {
            log.info("借款人关联方校验执行中（福费廷合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                String cusId1 = iqpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpLoanApp.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 委托贷款合同申请 YX008
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)) {
            log.info("借款人关联方校验执行中（委托贷款合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(serno);
            if (iqpEntrustLoanApp != null) {
                String cusId1 = iqpEntrustLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpEntrustLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 小微放款申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)) {
            log.info("借款人关联方校验执行中（小微合同申请(小微放款申请)）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String cusId1 = pvpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        else if ("XW003".equals(bizType)) {
            log.info("借款人关联方校验执行中（小微合同申请(小微放款申请)）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                String cusId1 = iqpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(iqpLoanApp.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 零售合同申请 LSYW02
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType) || "LS008".equals(bizType)) {
            log.info("借款人关联方校验执行中（零售合同申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            if (ctrLoanCont != null) {
                String cusId1 = ctrLoanCont.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(ctrLoanCont.getGuarWay())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 对公贷款出账申请 YX011
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType)) {
            log.info("借款人关联方校验执行中（对公贷款出账申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String cusId1 = pvpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 银承出账申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX012.equals(bizType)) {
            log.info("借款人关联方校验执行中（银承出账申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            if (pvpAccpApp != null) {
                String cusId1 = pvpAccpApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpAccpApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        // 委托贷款出账申请 YX008 DGYX04
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)) {
            log.info("借款人关联方校验执行中（委托贷款出账申请）*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String cusId1 = pvpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        //零售放款申请（空白合同模式）
        else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType)){
            log.info("借款人关联方校验执行中（零售放款申请（空白合同模式））*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String cusId1 = pvpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        //零售放款申请（生成打印模式）
        else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType)){
            log.info("借款人关联方校验执行中（零售放款申请（生成打印模式））*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String cusId1 = pvpLoanApp.getCusId();
                if (CmisBizConstants.STD_ZB_GUAR_WAY_00.equals(pvpLoanApp.getGuarMode())) {
                    riskResultDto = checkIsExistRelTable(cusId1);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        log.info("借款人关联方校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 根据客户号查询关联方名单信息记录数
     *
     * @param cusId
     * @return
     */
    private RiskResultDto checkIsExistRelTable(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        CmisCus0020ReqDto reqDto = new CmisCus0020ReqDto();
        reqDto.setCusId(cusId);
        ResultDto<CmisCus0020RespDto> cmisCus0020RespDtoResultDto = cmisCusClientService.cmiscus0020(reqDto);
        //请求成功
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(cmisCus0020RespDtoResultDto.getCode())) {
            CmisCus0020RespDto data = cmisCus0020RespDtoResultDto.getData();
            Integer count = data.getCount();
            if (count != null && count > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01101);
                return riskResultDto;
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
             riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01102);
            return riskResultDto;
        }
        return riskResultDto;
    }
}
