/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.dto.AccAccpDrftSubDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.service.AccAccpDrftSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpDrftSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 10:19:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accaccpdrftsub")
public class AccAccpDrftSubResource {
    @Autowired
    private AccAccpDrftSubService accAccpDrftSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccAccpDrftSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccAccpDrftSub> list = accAccpDrftSubService.selectAll(queryModel);
        return new ResultDto<List<AccAccpDrftSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccAccpDrftSub>> index(QueryModel queryModel) {
        List<AccAccpDrftSub> list = accAccpDrftSubService.selectByModel(queryModel);
        return new ResultDto<List<AccAccpDrftSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccAccpDrftSub> show(@PathVariable("pkId") String pkId) {
        AccAccpDrftSub accAccpDrftSub = accAccpDrftSubService.selectByPrimaryKey(pkId);
        return new ResultDto<AccAccpDrftSub>(accAccpDrftSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccAccpDrftSub> create(@RequestBody AccAccpDrftSub accAccpDrftSub) throws URISyntaxException {
        accAccpDrftSubService.insert(accAccpDrftSub);
        return new ResultDto<AccAccpDrftSub>(accAccpDrftSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccAccpDrftSub accAccpDrftSub) throws URISyntaxException {
        int result = accAccpDrftSubService.update(accAccpDrftSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accAccpDrftSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accAccpDrftSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccAccpDrftSub>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccAccpDrftSub> list = accAccpDrftSubService.selectByModel(queryModel);
        return new ResultDto<List<AccAccpDrftSub>>(list);
    }


    /**
     * @函数名称:coreBillNo
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据核心银承编号回显银承台账票据明细")
    @PostMapping("/selectByCoreBillNo")
    protected ResultDto<AccAccpDrftSubDto> selectByCoreBillNo(@RequestBody AccAccpDrftSub accAccpDrftSub) {
        AccAccpDrftSubDto accAccpDrftSubInfoDto = accAccpDrftSubService.selectByCoreBillNo(accAccpDrftSub);
        return new ResultDto<AccAccpDrftSubDto>(accAccpDrftSubInfoDto);
    }

    /**
     * 异步下载银承台账列表
     */
    @PostMapping("/exportAccAccpDrftSub")
    public ResultDto<ProgressDto> asyncExportAccAccpDrftSub(@RequestBody QueryModel model) {
        ProgressDto progressDto = accAccpDrftSubService.asyncExportAccAccpDrftSub(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称: querymodelByCondition
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据查询条件查询台账信息并返回
     * @算法描述:
     */
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccAccpDrftSub>> querymodelByCondition(@RequestBody QueryModel queryModel) {
        List<AccAccpDrftSub> list = accAccpDrftSubService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccAccpDrftSub>>(list);
    }

    /**
     * @函数名称: selectByContNo
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据合同编号查询票据明细台账信息
     * @算法描述:
     */
    @PostMapping("/selectByContNo")
    protected ResultDto<List<AccAccpDrftSub>> selectByContNo(@RequestBody QueryModel queryModel) {
        List<AccAccpDrftSub> list = accAccpDrftSubService.selectByContNo(queryModel);
        return new ResultDto<List<AccAccpDrftSub>>(list);
    }

    /**
     * @函数名称: updateByAccStatus
     * @函数描述: 更新台账状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateByAccStatus")
    protected ResultDto<Integer> updateByAccStatus(@RequestBody AccAccpDrftSub accAccpDrftSub) throws URISyntaxException {
        int result = accAccpDrftSubService.updateByAccStatus(accAccpDrftSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: updateByPorderNo
     * @函数描述: selectByContNo
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateByPorderNo")
    protected ResultDto<Integer> updateByPorderNo(@RequestBody AccAccpDrftSub accAccpDrftSub) throws URISyntaxException {
        int result = accAccpDrftSubService.updateByPorderNo(accAccpDrftSub);
        return new ResultDto<Integer>(result);
    }

}
