/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.BusiImageRelInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CoopPartnerAgrAccInfoService;
import cn.com.yusys.yusp.service.CoopPartnerAgrAppService;
import feign.QueryMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 10:44:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cooppartneragrapp")
public class CoopPartnerAgrAppResource {
    @Autowired
    private CoopPartnerAgrAppService coopPartnerAgrAppService;

    @Autowired
    private CoopPartnerAgrAccInfoService coopPartnerAgrAccInfoService;

    @Autowired
    private BusiImageRelInfoMapper busiImageRelInfoMapper;

    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("/tempsave")
    @Transactional(rollbackFor = Exception.class)
    protected ResultDto<CoopPartnerAgrApp> tempCreate(@RequestBody CoopPartnerAgrApp coopPartnerAgrApp) throws URISyntaxException {
        User user = SessionUtils.getUserInformation();

        if ("1".equals(coopPartnerAgrApp.getCoopAgrOprType()) || "3".equals(coopPartnerAgrApp.getCoopAgrOprType())) { // 新增
            //存在性校验
            QueryModel model = new QueryModel();
            model.addCondition("partnerNo", coopPartnerAgrApp.getPartnerNo());
            model.addCondition("coopAgrOprType", coopPartnerAgrApp.getCoopAgrOprType());
            model.addCondition("apprStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            List list = coopPartnerAgrAppService.selectByModel(model);
            if (!list.isEmpty()) {
                //已存在准入申请
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP.key, EcbEnum.LMT_COOP_PLAN_APP.value);
            }
            coopPartnerAgrApp.setApprStatus(BizFlowConstant.WF_STATUS_000);
            coopPartnerAgrApp.setManagerBrId(user.getOrg().getCode());
            coopPartnerAgrApp.setManagerId(user.getLoginCode());
            coopPartnerAgrApp.setInputId(user.getLoginCode());
            coopPartnerAgrApp.setInputBrId(user.getOrg().getCode());
            coopPartnerAgrApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            coopPartnerAgrApp.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
            coopPartnerAgrApp.setCreateTime(DateUtils.getCurrDate());
            coopPartnerAgrApp.setUpdateTime(DateUtils.getCurrDate());
            coopPartnerAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 生成申请流水
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            coopPartnerAgrApp.setSerno(serno);
            // 生成协议编号
            String coopAgrNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_COOP_CTR_NO, new HashMap<>());
            coopPartnerAgrApp.setCoopAgrNo(coopAgrNo);
            //查询合作方案台账信息
            QueryModel qm = new QueryModel();
            qm.addCondition("coopPlanNo",coopPartnerAgrApp.getCoopPlanNo());
            List<CoopPlanAccInfo> coopPlanAccInfos = coopPlanAccInfoMapper.selectByModel(qm);
            CoopPlanAccInfo coopPlanAccInfo = null;
            if (!CollectionUtils.isEmpty(coopPlanAccInfos)) {
                coopPlanAccInfo = coopPlanAccInfos.get(0);
            }else{
                throw BizException.error(null, EcbEnum.LMT_COOP_APP_CTR_FAILED_02.key, EcbEnum.LMT_COOP_APP_CTR_FAILED_02.value);
            }

            coopPartnerAgrApp.setCoopPlanSerno(coopPlanAccInfo.getSerno());
            coopPartnerAgrApp.setBailAccLowAmt(coopPlanAccInfo.getBailAccLowAmt());
            coopPartnerAgrApp.setBailAccNo(coopPlanAccInfo.getBailAccNo());
            coopPartnerAgrApp.setBailAccNoSubSeq(coopPlanAccInfo.getBailAccNoSubSeq());
            coopPartnerAgrApp.setBailPerc(coopPlanAccInfo.getBailPerc());
            coopPartnerAgrApp.setBailDepositMode(coopPlanAccInfo.getBailDepositMode());
            coopPartnerAgrApp.setBailOverdraftMax(coopPlanAccInfo.getBailOverdraftMax());
            coopPartnerAgrApp.setSigLowDepositAmt(coopPlanAccInfo.getSigLowDepositAmt());
            coopPartnerAgrAppService.insert(coopPartnerAgrApp);
            //初始化合作方协议关联的影像信息
            insertRelInfo(coopPartnerAgrApp);
        } else if ("2".equals(coopPartnerAgrApp.getCoopAgrOprType())) { // 变更
            //存在性校验
            QueryModel model = new QueryModel();
            model.addCondition("partnerNo", coopPartnerAgrApp.getPartnerNo());
            model.addCondition("coopAgrOprType", coopPartnerAgrApp.getCoopAgrOprType());
            model.addCondition("apprStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            List list = coopPartnerAgrAppService.selectByModel(model);
            if (!list.isEmpty()) {
                //已存在准入申请
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP.key, EcbEnum.LMT_COOP_PLAN_APP.value);
            }
            // 查询协议台账原数据
            CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = coopPartnerAgrAccInfoService.selectByPrimaryKey(coopPartnerAgrApp.getCoopAgrNo());
            BeanUtils.beanCopy(coopPartnerAgrAccInfo, coopPartnerAgrApp);

            coopPartnerAgrApp.setApprStatus(BizFlowConstant.WF_STATUS_000);
            coopPartnerAgrApp.setManagerBrId(user.getOrg().getCode());
            coopPartnerAgrApp.setManagerId(user.getLoginCode());
            coopPartnerAgrApp.setInputId(user.getLoginCode());
            coopPartnerAgrApp.setInputBrId(user.getOrg().getCode());
            coopPartnerAgrApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            coopPartnerAgrApp.setCreateTime(DateUtils.getCurrDate());
            coopPartnerAgrApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            coopPartnerAgrApp.setUpdId(user.getLoginCode());
            coopPartnerAgrApp.setUpdBrId(user.getOrg().getCode());
            coopPartnerAgrApp.setUpdateTime(DateUtils.getCurrDate());
            coopPartnerAgrApp.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
            coopPartnerAgrApp.setSerno(SequenceUtils.getSequence("CAP")); // 设置新的申请流水
            insertRelInfo(coopPartnerAgrApp);
            coopPartnerAgrAppService.insert(coopPartnerAgrApp);

            //查询原协议关联的分项信息

            //查询原协议关联的项目信息
        }
        return new ResultDto<CoopPartnerAgrApp>(coopPartnerAgrApp);
    }

    /**
     * @方法名称: insertRelInfo
     * @方法描述: 插入关联表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void insertRelInfo(CoopPartnerAgrApp coopPartnerAgrApp) {
        //初始化协议信息
        BusiImageRelInfo busiImageRelInfo = new BusiImageRelInfo();
        busiImageRelInfo.setSerno(coopPartnerAgrApp.getSerno());
        busiImageRelInfo.setImageDesc("合作方协议资料");
        busiImageRelInfo.setImageNo(coopPartnerAgrApp.getCoopAgrNo());//按照协议编号关联影像，变更时协议不变，影像目录不变
        busiImageRelInfo.setTopOutsystemCode("DBGSXYQD");//20211030-00223修改合作方协议影像目录
        busiImageRelInfo.setKeywordType("contid");
        busiImageRelInfo.setAuthority("import;insert;download;scan;delImg");
        busiImageRelInfo.setImageOrder(99);
        busiImageRelInfoMapper.insert(busiImageRelInfo);
        //复制影像信息
        QueryModel qm = new QueryModel();
        qm.addCondition("serno",coopPartnerAgrApp.getCoopPlanSerno());
        List<BusiImageRelInfo> imageList = busiImageRelInfoMapper.selectByModel(qm);
        imageList.stream().forEach(image -> {
            BusiImageRelInfo imageInfo = new BusiImageRelInfo();
            BeanUtils.beanCopy(image, imageInfo);
            imageInfo.setSerno(coopPartnerAgrApp.getSerno());
            imageInfo.setAuthority("download");
            imageInfo.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            busiImageRelInfoMapper.insert(imageInfo);
        });
        //复制征信查询信息
        qm.getCondition().put("bizSerno", coopPartnerAgrApp.getCoopPlanSerno());
        List<CreditQryBizReal> creditList = creditQryBizRealMapper.selectByModel(qm);
        creditList.stream().forEach(credit -> {
            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
            String authbookNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            CreditQryBizReal creditInfo = new CreditQryBizReal();
            BeanUtils.beanCopy(credit, creditInfo);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(creditInfo.getCrqlSerno());
            creditInfo.setBizSerno(coopPartnerAgrApp.getSerno());
            creditInfo.setCrqlSerno(crqlSerno);
            creditInfo.setCqbrSerno(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            creditQryBizRealMapper.insert(creditInfo);
            if (creditReportQryLst != null) {
                creditReportQryLst.setCrqlSerno(crqlSerno);
                creditReportQryLst.setAuthbookNo(authbookNo);
                creditReportQryLstMapper.insert(creditReportQryLst);
            }
        });
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPartnerAgrApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPartnerAgrApp> list = coopPartnerAgrAppService.selectAll(queryModel);
        return new ResultDto<List<CoopPartnerAgrApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPartnerAgrApp>> index(QueryModel queryModel) {
        List<CoopPartnerAgrApp> list = coopPartnerAgrAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerAgrApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPartnerAgrApp>>
    query(@RequestBody QueryModel queryModel) {
        List<CoopPartnerAgrApp> list = coopPartnerAgrAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerAgrApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CoopPartnerAgrApp> show(@PathVariable("serno") String serno) {
        CoopPartnerAgrApp coopPartnerAgrApp = coopPartnerAgrAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopPartnerAgrApp>(coopPartnerAgrApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPartnerAgrApp> create(@RequestBody CoopPartnerAgrApp coopPartnerAgrApp) throws URISyntaxException {
        coopPartnerAgrAppService.insert(coopPartnerAgrApp);
        return new ResultDto<CoopPartnerAgrApp>(coopPartnerAgrApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPartnerAgrApp coopPartnerAgrApp) throws URISyntaxException {
        CoopPartnerAgrApp preCoopPartnerAgrApp = coopPartnerAgrAppService.selectByPrimaryKey(coopPartnerAgrApp.getSerno());
        User user = SessionUtils.getUserInformation();
        coopPartnerAgrApp.setUpdId(user.getLoginCode());
        coopPartnerAgrApp.setUpdBrId(user.getOrg().getCode());
        coopPartnerAgrApp.setUpdateTime(DateUtils.getCurrDate());
        coopPartnerAgrApp.setUpdDate(DateUtils.getCurrDateStr());
        coopPartnerAgrApp.setCreateTime(preCoopPartnerAgrApp.getCreateTime());
        int result = coopPartnerAgrAppService.update(coopPartnerAgrApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopPartnerAgrAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPartnerAgrAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:checkAmt
     * @函数描述:检查中合作协议额度
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkAmt")
    protected ResultDto checkAmt(@RequestBody CoopPartnerAgrApp coopPartnerAgrApp) {
        ResultDto dto = coopPartnerAgrAppService.checkAmt(coopPartnerAgrApp);
        return dto;
    }


    /**
     * @函数名称:checkSubmit
     * @函数描述:协议提交之前检查必输项以及合作额度
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSubmit")
    protected ResultDto checkSubmit(@RequestBody CoopPartnerAgrApp coopPartnerAgrApp) {
        ResultDto dto = coopPartnerAgrAppService.checkSubmit(coopPartnerAgrApp);
        return dto;
    }
}
