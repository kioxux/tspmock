package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuareInfo
 * @类描述: guare_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-16 15:29:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PfkInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 具体项目名称1 **/
	private String projectName1;

	/** 具体项目名称2 **/
	private String projectName2;

	/** 项目具体情况 **/
	private String projectCondition;

	/** 分值 **/
	private String course;

	/** 简要情况描述 **/
	private String pfkJxdDesc;

	/** 主办客户经理评分 **/
	private String pfkJxdGrade1;

	/** 协办客户经理评分 **/
	private String pfkJxdGrade2;

	public String getProjectName1() {
		return projectName1;
	}

	public void setProjectName1(String projectName1) {
		this.projectName1 = projectName1;
	}

	public String getProjectName2() {
		return projectName2;
	}

	public void setProjectName2(String projectName2) {
		this.projectName2 = projectName2;
	}

	public String getProjectCondition() {
		return projectCondition;
	}

	public void setProjectCondition(String projectCondition) {
		this.projectCondition = projectCondition;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getPfkJxdDesc() {
		return pfkJxdDesc;
	}

	public void setPfkJxdDesc(String pfkJxdDesc) {
		this.pfkJxdDesc = pfkJxdDesc;
	}

	public String getPfkJxdGrade1() {
		return pfkJxdGrade1;
	}

	public void setPfkJxdGrade1(String pfkJxdGrade1) {
		this.pfkJxdGrade1 = pfkJxdGrade1;
	}

	public String getPfkJxdGrade2() {
		return pfkJxdGrade2;
	}

	public void setPfkJxdGrade2(String pfkJxdGrade2) {
		this.pfkJxdGrade2 = pfkJxdGrade2;
	}
}