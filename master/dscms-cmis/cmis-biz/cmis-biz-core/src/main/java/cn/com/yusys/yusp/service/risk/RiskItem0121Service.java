package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.CusLstDedkkhYjsxTaskDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0121Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0121Service.class);

    @Resource
    private CmisCusClientService cmisCusClientService;//注入客户服务接口

    /**
     * @方法名称: riskItem0121
     * @方法描述: 压降任务落实情况检查
     * @参数与返回说明:
     * @算法描述:
     * 如果客户存在到期未完成的压降任务（指任务到期日期<当前日期 且 审批状态为待发起或审批中或退回）则拦截。
     * @创建人: zhangliang15
     * @创建时间: 2021-10-08 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0121(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("压降任务落实情况检查校验开始*******************客户号：【{}】",cusId);
        RiskResultDto riskResultDto = new RiskResultDto();

        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0099);
            return riskResultDto;
        }

        QueryModel queryModelCus = new QueryModel();
        queryModelCus.addCondition("cusId", cusId);
        queryModelCus.addCondition("openDay", DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        log.info("根据客户号查找出该客户存在到期未完成的压降任务开始,查询参数为:{}", JSON.toJSONString(queryModelCus));
        ResultDto<List<CusLstDedkkhYjsxTaskDto>> cusLstDedkkhYjsxTaskDtoList = cmisCusClientService.selectcusLstDedkkhYjsxTaskDataByParams(queryModelCus);
        log.info("根据客户号查找出该客户存在到期未完成的压降任务结束");
        if (cusLstDedkkhYjsxTaskDtoList.getData().size() > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00095);
            return riskResultDto;
        }

        log.info("压降任务落实情况检查校验结束*******************客户号：【{}】",cusId);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}