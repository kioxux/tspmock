package cn.com.yusys.yusp.service.server.xddb0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0001.req.Xddb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0001.resp.Xddb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddb0001Service
 * @类描述: #服务类
 * @功能描述:查询押品是否已入库
 * @创建人: xull2
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0001Service.class);
    @Resource
    private GuarWarrantInfoService guarWarrantInfoService;

    /**
     * 查询押品是否按揭
     *
     * @param xdkh0001DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0001DataRespDto getIsWarrantSatte(Xddb0001DataReqDto xdkh0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value);
        Xddb0001DataRespDto xddb0001DataRespDto = new Xddb0001DataRespDto();
        try {
            String coreGrtNo = xdkh0001DataReqDto.getGuarNo();//押品编号
            int result = 0;//查询结果记录数
            if (StringUtil.isNotEmpty(coreGrtNo)) {//押品编号非空校验
                logger.info("**************************首先根据要编号查询是否存在指定的记录**************************");
                result = guarWarrantInfoService.selectIsExistByCoreGuarantyNo(coreGrtNo);
                if (result > 0) {//存在指定记录
                    result = guarWarrantInfoService.selectAccommoDation(coreGrtNo);
                    if (result > 0) {//押品存在按揭记录
                        xddb0001DataRespDto.setOpFlag(DscmsBizDbEnum.YESNO_YES.key);
                        xddb0001DataRespDto.setOpMsg(DscmsBizDbEnum.EXIST_OPMSG.value);
                    } else {
                        xddb0001DataRespDto.setOpFlag(DscmsBizDbEnum.YESNO_NO.key);
                        xddb0001DataRespDto.setOpMsg(DscmsBizDbEnum.NOEXIST_OPMSG.value);
                    }
                } else {
                    logger.info("**************************不存在对应押品记录,查询结束*END*************************");
                    xddb0001DataRespDto.setOpFlag(DscmsBizDbEnum.YESNO_NO.key);
                    xddb0001DataRespDto.setOpMsg(DscmsBizDbEnum.NOEXIST_OPMSG.value);
                }
                logger.info("**************************查询押品是否按揭结束*END*************************");
            } else {
                //请求参数不存在
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value);
        return xddb0001DataRespDto;
    }
}
