/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpBillAcctChgApp;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpBillAcctChgAppMapper;
import cn.com.yusys.yusp.util.BizUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillAcctChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-19 21:31:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpBillAcctChgAppService {

    @Autowired
    private IqpBillAcctChgAppMapper iqpBillAcctChgAppMapper;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private cn.com.yusys.yusp.workFlow.service.BGYW04Bizservice BGYW04Bizservice;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpBillAcctChgApp selectByPrimaryKey(String iqpSerno) {
        return iqpBillAcctChgAppMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpBillAcctChgApp> selectAll(QueryModel model) {
        List<IqpBillAcctChgApp> records = (List<IqpBillAcctChgApp>) iqpBillAcctChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpBillAcctChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpBillAcctChgApp> list = iqpBillAcctChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpBillAcctChgApp record) {
        return iqpBillAcctChgAppMapper.insert(record);
    }

    @Autowired
    private AccLoanMapper accLoanMapper;
    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpBillAcctChgApp iqpBillAcctChgApp) {

        QueryModel model1 = new QueryModel();
        model1.addCondition("billNo", iqpBillAcctChgApp.getBillNo());
        model1.addCondition("apply","Y");
        List<IqpBillAcctChgApp> list1 = this.selectByModel(model1);
        if(list1.size()>0){
            throw BizException.error(null, "999999","\"该借据存在在途还款账号变更申请，请勿重复发起！\"" + "保存失败！");
        }

        QueryModel model = new QueryModel();
        model.addCondition("billNo",iqpBillAcctChgApp.getBillNo());
        List<AccLoan> list =  accLoanMapper.selectByModel(model);
        if(list.isEmpty()){
            //借据号不存在
            throw BizException.error(null, EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
        }
        //TODO 存在性校验
        AccLoan accLoan = list.get(0);
        iqpBillAcctChgApp.setBillNo(accLoan.getBillNo());                                //借据编号
        iqpBillAcctChgApp.setCusId(accLoan.getCusId());                                  //客户编号
        iqpBillAcctChgApp.setCusName(accLoan.getCusName());                              //客户名称
        iqpBillAcctChgApp.setContNo(accLoan.getContNo());                                //合同编号
        iqpBillAcctChgApp.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
        iqpBillAcctChgApp.setPrdId(accLoan.getPrdId());                                  //产品ID
        iqpBillAcctChgApp.setPrdName(accLoan.getPrdName());                              //产品名称
        iqpBillAcctChgApp.setLoanStartDate(accLoan.getLoanStartDate());                  //贷款起始日
        iqpBillAcctChgApp.setLoanEndDate(accLoan.getLoanEndDate());                      //贷款到期日
        iqpBillAcctChgApp.setOldRepayAccno(accLoan.getRepayAccno());                     //还款账号
        iqpBillAcctChgApp.setOldRepaySubAccno(accLoan.getRepaySubAccno());               //账号子序号
        iqpBillAcctChgApp.setOldRepayAcctName(accLoan.getRepayAcctName());               //账号户名
        iqpBillAcctChgApp.setApproveStatus("000");
        iqpBillAcctChgApp.setAppChnl("02"); // 申请渠道 02-PC端
        return iqpBillAcctChgAppMapper.insertSelective(iqpBillAcctChgApp);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpBillAcctChgApp record) {
        return iqpBillAcctChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpBillAcctChgApp record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return iqpBillAcctChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpBillAcctChgAppMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpBillAcctChgAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称: updateApproveStatus
     * @函数描述: 还款账户变更为本人本行其他账号,不需要审批
     * @参数与返回说明:
     * @算法描述:
     */
    public int updateApproveStatus(String iqpSerno) {
        IqpBillAcctChgApp iqpBillAcctChgApp = this.selectByPrimaryKey(iqpSerno);
        String billNo = iqpBillAcctChgApp.getBillNo();
        AccLoan accLoan = accLoanService.selectByBillNo(billNo);
        /*
         *************************************************************************
         * 审批通过调用 贷款信息维护    ln3030
         * *************************************************************************/
        Ln3030ReqDto reqDto = new Ln3030ReqDto();
        reqDto.setDkkhczbz("4");//操作标志 4 直通
        reqDto.setDkjiejuh(iqpBillAcctChgApp.getBillNo());
        reqDto.setKehuhaoo(iqpBillAcctChgApp.getCusId());
        reqDto.setKehmingc(iqpBillAcctChgApp.getCusName());
        reqDto.setHuankzhh(iqpBillAcctChgApp.getRepayAccno());
        reqDto.setHkzhhzxh(iqpBillAcctChgApp.getRepaySubAccno());
        reqDto.setHuobdhao(BizUtils.changCurType(accLoan.getContCurType()));
        ResultDto<Ln3030RespDto> ln3030ResultDto = dscms2CoreLnClientService.ln3030(reqDto);
        String ln3030Code = Optional.ofNullable(ln3030ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3030Meesage = Optional.ofNullable(ln3030ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3030RespDto l = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3030ResultDto.getCode())) {
            //  获取相关的值并解析
            l = ln3030ResultDto.getData();
            BGYW04Bizservice.changeAccLoan(iqpSerno);
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3030Code, ln3030Meesage);
        }
        /*
         *************************************************************************
         * 审批通过调用 贷款信息维护    ln3030
         * *************************************************************************/

        iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);

        return  this.updateSelective(iqpBillAcctChgApp);
    }
}
