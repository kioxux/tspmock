/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrContInsur;
import cn.com.yusys.yusp.domain.CtrContNotar;
import cn.com.yusys.yusp.dto.CtrContInsurDto;
import cn.com.yusys.yusp.dto.CtrContNotarDto;
import cn.com.yusys.yusp.dto.CtrContNotarSaveDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrContNotarMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContNotarService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-30 19:17:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CtrContNotarService {
    private static final Logger log = LoggerFactory.getLogger(CtrContNotarService.class);

    @Autowired
    private CtrContNotarMapper ctrContNotarMapper;
    @Autowired
    private CtrContInsurService ctrContInsurService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrContNotar selectByPrimaryKey(String pkId) {
        return ctrContNotarMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CtrContNotar> selectAll(QueryModel model) {
        List<CtrContNotar> records = (List<CtrContNotar>) ctrContNotarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrContNotar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrContNotar> list = ctrContNotarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CtrContNotar record) {
        return ctrContNotarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CtrContNotar record) {
        return ctrContNotarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CtrContNotar record) {
        return ctrContNotarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CtrContNotar record) {
        return ctrContNotarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return ctrContNotarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrContNotarMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertSelectiveByIqpSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelectiveByIqpSerno(String iqpSerno) {
        return ctrContNotarMapper.insertSelectiveByIqpSerno(iqpSerno);
    }

    /**
     * @方法名称: savectrcontnotar
     * @方法描述: 合同公证、保险信息保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int savectrcontnotar(CtrContNotarSaveDto ctrContNotarSaveDto) {
        int rtnData = 0;
        CtrContNotarDto ctrContNotarDto = ctrContNotarSaveDto.getCtrContNotarDto();
        CtrContInsurDto ctrContInsurDto = ctrContNotarSaveDto.getCtrContInsurDto();
        if ((ctrContNotarDto != null || ctrContInsurDto != null) && ctrContNotarSaveDto != null) {
            String contNo = ctrContNotarSaveDto.getCtrContNotarDto().getContNo();
            CtrContNotar ctrContNotar = new CtrContNotar();
            CtrContInsur ctrContInsur = new CtrContInsur();
            ctrContNotar = ctrContNotarMapper.selectByContNoKey(contNo);
            ctrContInsur = ctrContInsurService.selectByContNoKey(contNo);
            if (ctrContNotar == null) {
                log.info("合同公证信息" + contNo + "新增保存公证信息");
                ctrContNotar = JSONObject.parseObject(JSON.toJSONString(ctrContNotarDto), CtrContNotar.class);
                ctrContNotar.setPkId(StringUtils.uuid(true));
                ctrContNotar.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                rtnData = ctrContNotarMapper.insert(ctrContNotar);
            } else {
                log.info("合同保险信息" + contNo + "修改保存保险信息");
                ctrContInsur = JSONObject.parseObject(JSON.toJSONString(ctrContInsurDto), CtrContInsur.class);
                rtnData = ctrContInsurService.updateSelective(ctrContInsur);
            }
            if (rtnData > 0) {
                if (ctrContInsur == null) {
                    log.info("合同保险信息" + contNo + "新增保存保险信息");
                    ctrContInsur = JSONObject.parseObject(JSON.toJSONString(ctrContInsurDto), CtrContInsur.class);
                    ctrContInsur.setPkId(StringUtils.uuid(true));
                    ctrContInsur.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    rtnData = ctrContInsurService.insert(ctrContInsur);
                } else {
                    log.info("合同公证信息" + contNo + "修改保存公证信息");
                    ctrContNotar = JSONObject.parseObject(JSON.toJSONString(ctrContNotarDto), CtrContNotar.class);
                    rtnData = ctrContNotarMapper.updateByPrimaryKeySelective(ctrContNotar);
                }
            }
            if (rtnData == 0) {
                throw new YuspException(EcbEnum.CTR_CONT_INSUR_EXCEPTION.key, EcbEnum.CTR_CONT_INSUR_EXCEPTION.value);
            }
        }
        return rtnData;
    }

    /**
     * @方法名称: selectByContNoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrContNotar selectByContNoKey(String contNo) {
        return ctrContNotarMapper.selectByContNoKey(contNo);
    }

}
