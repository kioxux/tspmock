/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;


import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailDepositInfo
 * @类描述: bail_deposit_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-09 21:06:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name="bail_deposit_info")
public class BailDepositInfo extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 65535)
	private String cusId;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 65535)
	private String contNo;
	
	/** 保证金类型 **/
	@Column(name = "BAIL_TYPE", unique = false, nullable = true, length = 65535)
	private String bailType;
	
	/** 保证金帐号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 65535)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB", unique = false, nullable = true, length = 65535)
	private String bailAccNoSub;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 65535)
	private String curType;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 65535)
	private java.math.BigDecimal bailAmt;
	
	/** 保证金汇率 **/
	@Column(name = "BAIL_RATE", unique = false, nullable = true, length = 65535)
	private java.math.BigDecimal bailRate;
	
	/** 保证金利息存入帐号 **/
	@Column(name = "BAIL_INTEREST_DEP_ACCT", unique = false, nullable = true, length = 65535)
	private String bailInterestDepAcct;
	
	/** 登记银承承兑汇票保证金的流水号 **/
	@Column(name = "INPUT_ACCP_BAIL_SERNO", unique = false, nullable = true, length = 65535)
	private String inputAccpBailSerno;
	
	/** 票汇盈保证金释放账号 **/
	@Column(name = "PHY_BAIL_RELEASE_ACC_NO", unique = false, nullable = true, length = 65535)
	private String phyBailReleaseAccNo;
	
	/** 计息方式 **/
	@Column(name = "INTEREST_MODE", unique = false, nullable = true, length = 65535)
	private String interestMode;
	
	/** 申请人账号 **/
	@Column(name = "RQSTR_ACC_NO", unique = false, nullable = true, length = 65535)
	private String rqstrAccNo;
	
	/** 申请人账户名 **/
	@Column(name = "RQSTR_ACC_NAME", unique = false, nullable = true, length = 65535)
	private String rqstrAccName;
	
	/** 申请人账户子序号 **/
	@Column(name = "RQSTR_ACC_NO_SUB", unique = false, nullable = true, length = 65535)
	private String rqstrAccNoSub;
	
	/** 核心账户子序号 **/
	@Column(name = "CORE_ACC_NO_SUB", unique = false, nullable = true, length = 65535)
	private String coreAccNoSub;
	
	/** 合同部门 **/
	@Column(name = "CONT_DEP", unique = false, nullable = true, length = 65535)
	private String contDep;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 65535)
	private String endDate;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 65535)
	private String status;
	
	/** 是否释放 **/
	@Column(name = "IS_RELEASE", unique = false, nullable = true, length = 65535)
	private String isRelease;
	
	/** 部提次数 **/
	@Column(name = "MINISTRY_TIMES", unique = false, nullable = true, length = 65535)
	private String ministryTimes;
	
	/** 是否为定期 **/
	@Column(name = "IS_REGULAR", unique = false, nullable = true, length = 65535)
	private String isRegular;
	
	/** 缴存登记日期 **/
	@Column(name = "DEPOSIT_INPUT_DATE", unique = false, nullable = true, length = 65535)
	private String depositInputDate;
	
	/** 缴存登记人 **/
	@Column(name = "DEPOSIT_INPUT_ID", unique = false, nullable = true, length = 65535)
	private String depositInputId;
	
	/** 缴存登记机构 **/
	@Column(name = "DEPOSIT_INPUT_BR_ID", unique = false, nullable = true, length = 65535)
	private String depositInputBrId;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 65535)
	private String approveStatus;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param bailType
	 */
	public void setBailType(String bailType) {
		this.bailType = bailType;
	}
	
    /**
     * @return bailType
     */
	public String getBailType() {
		return this.bailType;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSub
	 */
	public void setBailAccNoSub(String bailAccNoSub) {
		this.bailAccNoSub = bailAccNoSub;
	}
	
    /**
     * @return bailAccNoSub
     */
	public String getBailAccNoSub() {
		return this.bailAccNoSub;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return bailRate
     */
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param bailInterestDepAcct
	 */
	public void setBailInterestDepAcct(String bailInterestDepAcct) {
		this.bailInterestDepAcct = bailInterestDepAcct;
	}
	
    /**
     * @return bailInterestDepAcct
     */
	public String getBailInterestDepAcct() {
		return this.bailInterestDepAcct;
	}
	
	/**
	 * @param inputAccpBailSerno
	 */
	public void setInputAccpBailSerno(String inputAccpBailSerno) {
		this.inputAccpBailSerno = inputAccpBailSerno;
	}
	
    /**
     * @return inputAccpBailSerno
     */
	public String getInputAccpBailSerno() {
		return this.inputAccpBailSerno;
	}
	
	/**
	 * @param phyBailReleaseAccNo
	 */
	public void setPhyBailReleaseAccNo(String phyBailReleaseAccNo) {
		this.phyBailReleaseAccNo = phyBailReleaseAccNo;
	}
	
    /**
     * @return phyBailReleaseAccNo
     */
	public String getPhyBailReleaseAccNo() {
		return this.phyBailReleaseAccNo;
	}
	
	/**
	 * @param interestMode
	 */
	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode;
	}
	
    /**
     * @return interestMode
     */
	public String getInterestMode() {
		return this.interestMode;
	}
	
	/**
	 * @param rqstrAccNo
	 */
	public void setRqstrAccNo(String rqstrAccNo) {
		this.rqstrAccNo = rqstrAccNo;
	}
	
    /**
     * @return rqstrAccNo
     */
	public String getRqstrAccNo() {
		return this.rqstrAccNo;
	}
	
	/**
	 * @param rqstrAccName
	 */
	public void setRqstrAccName(String rqstrAccName) {
		this.rqstrAccName = rqstrAccName;
	}
	
    /**
     * @return rqstrAccName
     */
	public String getRqstrAccName() {
		return this.rqstrAccName;
	}
	
	/**
	 * @param rqstrAccNoSub
	 */
	public void setRqstrAccNoSub(String rqstrAccNoSub) {
		this.rqstrAccNoSub = rqstrAccNoSub;
	}
	
    /**
     * @return rqstrAccNoSub
     */
	public String getRqstrAccNoSub() {
		return this.rqstrAccNoSub;
	}
	
	/**
	 * @param coreAccNoSub
	 */
	public void setCoreAccNoSub(String coreAccNoSub) {
		this.coreAccNoSub = coreAccNoSub;
	}
	
    /**
     * @return coreAccNoSub
     */
	public String getCoreAccNoSub() {
		return this.coreAccNoSub;
	}
	
	/**
	 * @param contDep
	 */
	public void setContDep(String contDep) {
		this.contDep = contDep;
	}
	
    /**
     * @return contDep
     */
	public String getContDep() {
		return this.contDep;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param isRelease
	 */
	public void setIsRelease(String isRelease) {
		this.isRelease = isRelease;
	}
	
    /**
     * @return isRelease
     */
	public String getIsRelease() {
		return this.isRelease;
	}
	
	/**
	 * @param ministryTimes
	 */
	public void setMinistryTimes(String ministryTimes) {
		this.ministryTimes = ministryTimes;
	}
	
    /**
     * @return ministryTimes
     */
	public String getMinistryTimes() {
		return this.ministryTimes;
	}
	
	/**
	 * @param isRegular
	 */
	public void setIsRegular(String isRegular) {
		this.isRegular = isRegular;
	}
	
    /**
     * @return isRegular
     */
	public String getIsRegular() {
		return this.isRegular;
	}
	
	/**
	 * @param depositInputDate
	 */
	public void setDepositInputDate(String depositInputDate) {
		this.depositInputDate = depositInputDate;
	}
	
    /**
     * @return depositInputDate
     */
	public String getDepositInputDate() {
		return this.depositInputDate;
	}
	
	/**
	 * @param depositInputId
	 */
	public void setDepositInputId(String depositInputId) {
		this.depositInputId = depositInputId;
	}
	
    /**
     * @return depositInputId
     */
	public String getDepositInputId() {
		return this.depositInputId;
	}
	
	/**
	 * @param depositInputBrId
	 */
	public void setDepositInputBrId(String depositInputBrId) {
		this.depositInputBrId = depositInputBrId;
	}
	
    /**
     * @return depositInputBrId
     */
	public String getDepositInputBrId() {
		return this.depositInputBrId;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}


}