/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAcc;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.service.IntbankOrgAdmitAppService;
import org.springframework.web.servlet.ModelAndView;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:26:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/intbankorgadmitapp")
public class IntbankOrgAdmitAppResource {
    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IntbankOrgAdmitApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IntbankOrgAdmitApp> list = intbankOrgAdmitAppService.selectAll(queryModel);
        return new ResultDto<List<IntbankOrgAdmitApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<IntbankOrgAdmitApp>> index(@RequestBody QueryModel queryModel) {
        //设置默认排序方式
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" serno desc");
        }
        List<IntbankOrgAdmitApp> list = intbankOrgAdmitAppService.selectByModel(queryModel);
        return new ResultDto<List<IntbankOrgAdmitApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{pkId}")
    protected ResultDto<IntbankOrgAdmitApp> show(@PathVariable("pkId") String pkId) {
        IntbankOrgAdmitApp intbankOrgAdmitApp = intbankOrgAdmitAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IntbankOrgAdmitApp>(intbankOrgAdmitApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<Map> create(@RequestBody Map intbankOrgAdmitApp) throws URISyntaxException {
        //校验前台pkId 和serno 是否生成
        if (StringUtils.isBlank(String.valueOf(intbankOrgAdmitApp.get("newSerno"))) || StringUtils.isBlank(String.valueOf(intbankOrgAdmitApp.get("newPkId")))){
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
        }
        intbankOrgAdmitAppService.insertIntbankOrgAdmitAppApply(intbankOrgAdmitApp);
        return new ResultDto<Map>(intbankOrgAdmitApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IntbankOrgAdmitApp intbankOrgAdmitApp) throws URISyntaxException {
        int result = intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteLogicBySerno")
    protected ResultDto<Integer> delete(@RequestBody Map condtion) {
        String serno = (String) condtion.get("serno");
        int result = intbankOrgAdmitAppService.deleteLogicBySerno(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = intbankOrgAdmitAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 生成流水号
     * @return
     */
    @PostMapping("/code")
    protected  ResultDto<String> code(){
        String code = intbankOrgAdmitAppService.generateSerno(CmisBizConstants.BIZ_SERNO);
        return new ResultDto<>(code);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<IntbankOrgAdmitApp> showBySerno(@RequestBody Map condtion) {
        String serno = (String) condtion.get("serno");
        IntbankOrgAdmitApp intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(serno);
        return new ResultDto<IntbankOrgAdmitApp>(intbankOrgAdmitApp);
    }

    /**
     * @函数名称:show
     * @函数描述:获取同业机构准入基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBasicInfoBySerno")
    protected ResultDto<List> selectBasicInfoBySerno(@RequestBody Map condtion) {
        String serno = (String) condtion.get("serno");
        List result = new ArrayList();
        Map<String,Object> intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBasicInfoBySerno(serno);
        result.add(intbankOrgAdmitApp);
        return new ResultDto<List>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查该客户是否已经有承兑行白名单记录
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameOrgCusIdIsExist")
    protected ResultDto<Integer> checkSameOrgCusIdIsExist(@RequestBody QueryModel queryModel) {
        String cusId = (String) queryModel.getCondition().get("cusId");
        int result = intbankOrgAdmitAppService.selectRecordsByCusId(cusId);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/adjust")
    protected ResultDto<IntbankOrgAdmitApp> adjust(@RequestBody Map intbankOrgAdmitAcc) throws URISyntaxException {
        //校验前台pkId 和serno 是否生成
        if (StringUtils.isBlank(String.valueOf(intbankOrgAdmitAcc.get("newSerno")))
                || StringUtils.isBlank(String.valueOf(intbankOrgAdmitAcc.get("newPkId")))){
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000029.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000029.value);
        }

        IntbankOrgAdmitApp result = intbankOrgAdmitAppService.adjustIntbankOrgAdmitAppApply(intbankOrgAdmitAcc);
        return new ResultDto<IntbankOrgAdmitApp>(result);
    }

    /**
     * 图片
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = intbankOrgAdmitAppService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }

}
