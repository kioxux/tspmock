package cn.com.yusys.yusp.web.server.xdht0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.resp.Xdht0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0005.Xdht0005Service;
import cn.com.yusys.yusp.web.server.xdht0004.BizXdht0004Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:合同面签信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0005:合同面签信息列表查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0004Resource.class);

    @Resource
    private Xdht0005Service xdht0005Service;

    /**
     * 交易码：xdht0005
     * 交易描述：合同面签信息列表查询
     *
     * @param xdht0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同面签信息列表查询")
    @PostMapping("/xdht0005")
    protected @ResponseBody
    ResultDto<Xdht0005DataRespDto> xdht0005(@Validated @RequestBody Xdht0005DataReqDto xdht0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataReqDto));
        Xdht0005DataRespDto xdht0005DataRespDto = new Xdht0005DataRespDto();// 响应Dto:合同面签信息列表查询
        ResultDto<Xdht0005DataRespDto> xdht0005DataResultDto = new ResultDto<>();
        // 从xdht0005DataReqDto获取业务值进行业务逻辑处理

        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataReqDto));
            xdht0005DataRespDto = xdht0005Service.getXdht0005(xdht0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataRespDto));
            // 封装xdht0005DataResultDto中正确的返回码和返回信息
            xdht0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, e.getMessage());
            // 封装xdht0005DataResultDto中异常返回码和返回信息
            xdht0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0005DataRespDto到xdht0005DataResultDto中
        xdht0005DataResultDto.setData(xdht0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataResultDto));
        return xdht0005DataResultDto;
    }
}
