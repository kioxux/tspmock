/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AreaOrg;
import cn.com.yusys.yusp.repository.mapper.AreaOrgMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaOrgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-12 15:43:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AreaOrgService {

    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    @Autowired
    private AreaOrgMapper areaOrgMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AreaOrg selectByPrimaryKey(String pkId) {
        return areaOrgMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AreaOrg> selectAll(QueryModel model) {
        List<AreaOrg> records = (List<AreaOrg>) areaOrgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AreaOrg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AreaOrg> list = areaOrgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AreaOrg record) {
        record.setPkId(StringUtils.getUUID());
        return areaOrgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AreaOrg record) {
        return areaOrgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AreaOrg record) {
        return areaOrgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AreaOrg record) {
        return areaOrgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return areaOrgMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @author zlf
     * @date 2021/4/26 14:06
     * @version 1.0.0
     * @desc 根据区域编号删除
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @Transactional(rollbackFor=Exception.class)
    public Map deleteByAreaNo(String areaNo) {

        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{
            int count = areaOrgMapper.deleteByAreaNo(areaNo);
            if (count < 1) {
                //需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.COMMON_EXCEPTION_DEF.key,  EcbEnum.COMMON_EXCEPTION_DEF.value+ ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("根据区域编号删除！", e);
            rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
            rtnMsg = EcbEnum.COMMON_SUCCESS_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return areaOrgMapper.deleteByIds(ids);
    }

    /**
     * @author zlf
     * @date 2021/4/26 14:06
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员  ·  修改原因
    */
    public List<AreaOrg> selectByAreaNo(String areaNo) {
        List<AreaOrg> recordsArea = (List<AreaOrg>) areaOrgMapper.selectByAreaNo(areaNo);
        return recordsArea;
    }

    /**
     * @author zlf
     * @date 2021/4/26 14:06
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员  ·  修改原因
     */
    public List<AreaOrg> selectByFbCode(String fbCode) {
        List<AreaOrg> recordsArea = (List<AreaOrg>) areaOrgMapper.selectByFbCode(fbCode);
        return recordsArea;
    }



    /**
     * @author zlf
     * @date 2021/4/26 14:06
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员  ·  修改原因
     */
    public List<AreaOrg> selectByOrgId(String orgId) {
        List<AreaOrg> recordsArea = (List<AreaOrg>) areaOrgMapper.selectByOrgId(orgId);
        return recordsArea;
    }
}
