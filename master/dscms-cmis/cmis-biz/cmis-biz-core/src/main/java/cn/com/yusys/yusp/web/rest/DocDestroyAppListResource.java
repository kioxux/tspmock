/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import cn.com.yusys.yusp.dto.req.DeleteDocDetailParam;
import cn.com.yusys.yusp.dto.req.DocDestroyDetailListDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocDestroyAppList;
import cn.com.yusys.yusp.service.DocDestroyAppListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyAppListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 15:08:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "档案销毁")
@RestController
@RequestMapping("/api/docDestroyAppList")
public class DocDestroyAppListResource {

    @Autowired
    private DocDestroyAppListService docDestroyAppListService;

    /**
     * @函数名称:pageList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("档案销毁_查询档案销毁列表（分页查询）")
    @PostMapping("/query/pageList")
    protected ResultDto<List<DocDestroyAppList>> pageList(@RequestBody QueryModel queryModel) {
        List<DocDestroyAppList> list = docDestroyAppListService.selectByModel(queryModel);
        return new ResultDto<List<DocDestroyAppList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_查看档案销毁内容")
    @PostMapping("/showDocDestroyApp")
    protected ResultDto<DocDestroyAppList> showDocDestroyApp(@RequestBody String ddalSerno) {
        DocDestroyAppList docDestroyAppList = docDestroyAppListService.selectByPrimaryKey(ddalSerno);
        return new ResultDto<DocDestroyAppList>(docDestroyAppList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_新增档案销毁内容(新增按钮的时候，后台插入的记录)")
    @PostMapping("/create")
        protected ResultDto<DocDestroyAppList> createDocDestroyApp(@RequestBody DocDestroyAppList docDestroyAppList) throws URISyntaxException {
        docDestroyAppListService.insert(docDestroyAppList);
        return new ResultDto<DocDestroyAppList>(docDestroyAppList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_引入档案销毁明细内容(保存操作)")
    @PostMapping("/introduceDocDestroyDetails")
    protected ResultDto<Integer> introduceDocDestroyDetails(@RequestBody DocDestroyDetailListDTO docDestroyDetailListDTO) throws URISyntaxException {
        return new ResultDto<Integer>(docDestroyAppListService.introduceDocDestroyDetails(docDestroyDetailListDTO));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_提交（发起提交审批流程）")
    @PostMapping("/commit")
    protected ResultDto<Boolean> commitDocDestroyApp(@RequestBody String ddalSerno) throws URISyntaxException {
        return new ResultDto<Boolean>(docDestroyAppListService.commitDocDestroyApp(ddalSerno));
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_保存档案销毁内容")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody DocDestroyAppList docDestroyAppList) throws URISyntaxException {
        int result = docDestroyAppListService.updateSelective(docDestroyAppList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_删除档案销毁内容（仅待发起状态的数据才可以删除）")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody String ddalSerno) {
        int result = docDestroyAppListService.deleteByPrimaryKey(ddalSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_删除档案销毁明细内容（仅待发起状态的数据才可以删除）")
    @PostMapping("/deleteDocDetail")
    protected ResultDto<Integer> deleteDocDetail(@RequestBody DeleteDocDetailParam param) {
        int result = docDestroyAppListService.deleteDocDetail(param.getDdalSerno(),param.getDddlSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("档案销毁_更改为已销毁")
    @PostMapping("/changeDestroyed")
    protected ResultDto<Integer> changeDestroyed(@RequestBody String ddalSerno) {
        int result = docDestroyAppListService.changeDestroyed(ddalSerno);
        return new ResultDto<Integer>(result);
    }
}
