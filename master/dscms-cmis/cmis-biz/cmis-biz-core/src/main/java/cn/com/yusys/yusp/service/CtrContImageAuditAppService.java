package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dblist;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dylist;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Fb1170ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.resp.Fb1170RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContImageAuditAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-24 10:06:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrContImageAuditAppService {

    private static final Logger logger = LoggerFactory.getLogger(CtrContImageAuditAppService.class);

    @Autowired
    private CtrContImageAuditAppMapper ctrContImageAuditAppMapper;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CtrDiscContService ctrDiscContService;

    @Autowired
    private CtrTfLocContService ctrTfLocContService;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private CtrCvrgContService ctrCvrgContService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrContImageAuditApp selectByPrimaryKey(String serno) {
        return ctrContImageAuditAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrContImageAuditApp> selectAll(QueryModel model) {
        List<CtrContImageAuditApp> records = (List<CtrContImageAuditApp>) ctrContImageAuditAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrContImageAuditApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrContImageAuditApp> list = ctrContImageAuditAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CtrContImageAuditApp record) {
        return ctrContImageAuditAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CtrContImageAuditApp record) {
        return ctrContImageAuditAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CtrContImageAuditApp record) {
        return ctrContImageAuditAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CtrContImageAuditApp record) {
        return ctrContImageAuditAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {

        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppMapper.selectByPrimaryKey(serno);
        //判断当前申请状态是否为退回，修改为自行退出
        if (ctrContImageAuditApp != null){
            if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(ctrContImageAuditApp.getApproveStatus())){
                //流程删除 修改为自行退出
                logger.info("流程删除==》bizId：",serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
            }
        }
        return ctrContImageAuditAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return ctrContImageAuditAppMapper.deleteByIds(ids);
    }

    /**
     * @param contNo
     * @return int
     * @author 王玉坤
     * @date 2021/5/3 17:42
     * @version 1.0.0
     * @desc 根据合同号、申请状态查询申请信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int queryByContNo(String contNo) {
        return ctrContImageAuditAppMapper.queryByContNo(contNo);
    }

    /**
     * @param ctrLoanCont
     * @return ResultDto<java.lang.Boolean>
     * @author 王玉坤
     * @date 2021/5/3 17:12
     * @version 1.0.0
     * @desc 点击下一步插入线上合同提款申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<CtrContImageAuditApp> doNextStep(CtrLoanCont ctrLoanCont) {
        logger.info("新增线上提款启用申请开始");
        CtrContImageAuditApp ctrContImageAuditApp = new CtrContImageAuditApp();
        try {
            // 1、根据合同号查询是否已存在线上提款启用申请信息，若存在则提示用户，否则插入数据
            logger.info("根据合同号【{}】查询线上提款启用信息开始", ctrLoanCont.getContNo());
            int nums = queryByContNo(ctrLoanCont.getContNo());
            logger.info("根据合同号【{}】查询线上提款启用信息结束，查询条数【{}】", ctrLoanCont.getContNo(), nums);
            if (nums > 0) {
                throw BizException.error(null, "9999", "已存在发起中或者已发起的线上提款启用申请信息，不允许重新发起！");
            }

            logger.info("根据合同号【{}】新增线上提款启用信息开始", ctrLoanCont.getContNo());
            BeanUtils.beanCopy(ctrLoanCont, ctrContImageAuditApp);
            ctrContImageAuditApp.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>()));
            ctrContImageAuditApp.setApproveStatus("000");

            ctrContImageAuditApp.setCurType(ctrLoanCont.getCurType());
            ctrContImageAuditApp.setGuarMode(ctrLoanCont.getGuarWay());
            ctrContImageAuditApp.setCreateTime(DateUtils.getCurrDate());
            ctrContImageAuditApp.setUpdateTime(DateUtils.getCurrDate());

            User userInfo = SessionUtils.getUserInformation();

            ctrContImageAuditApp.setInputId(userInfo.getLoginCode());
            ctrContImageAuditApp.setInputBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setManagerId(userInfo.getLoginCode());
            ctrContImageAuditApp.setManagerBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ctrContImageAuditApp.setOprType(CmisCommonConstants.OP_TYPE_01);


            ctrContImageAuditAppMapper.insert(ctrContImageAuditApp);
            logger.info("根据合同号【{}】新增线上提款启用信息结束", ctrLoanCont.getContNo());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, "9999", e.getMessage());
        }
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditApp);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 19:42
     * @注释 条件历史查询
     */
    public List<CtrContImageAuditAppDomain> selectByModel2(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppMapper.selectByModel2(model);
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppMapper.selectByModel3(model);
        PageHelper.clearPage();
        if(CollectionUtils.nonEmpty(list)){
            List<String> contNoList = new ArrayList<>();
            for (CtrContImageAuditAppDomain ctrContImageAuditAppDomain: list ) {
                contNoList.add(ctrContImageAuditAppDomain.getContNo());
            }

            List<Map> ctrBeginFlags = ctrLoanContMapper.getCtrBeginFlagByContNos(contNoList);
            for (CtrContImageAuditAppDomain ctrContImageAuditAppDomain: list ) {
                for (Map ctrBeginFlagMap : ctrBeginFlags) {
                    String contNo = ctrBeginFlagMap.get("contNo").toString();
                    if(StringUtils.nonEmpty(ctrContImageAuditAppDomain.getContNo()) && ctrContImageAuditAppDomain.getContNo().equals(contNo)){
                        String ctrBeginFlag = ctrBeginFlagMap.containsKey("ctrBeginFlag") ? ctrBeginFlagMap.get("ctrBeginFlag").toString() : StringUtils.EMPTY;
                        ctrContImageAuditAppDomain.setCtrBeginFlag(ctrBeginFlag);
                        break;
                    }
                }
            }
        }

        return list;
    }

    public ResultDto updatactrbeginflag(CtrContImageAuditApp ctrContImageAuditApp) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(ctrContImageAuditApp.getContNo());
        if (ctrLoanCont != null) {
            ctrLoanCont.setCtrBeginFlag("1");
            int i = ctrLoanContService.update(ctrLoanCont);
            if (i == 1) {
                return new ResultDto(ctrLoanCont).message("操作成功");
            }
        }
        return new ResultDto().message("系统繁忙请稍后重试");
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:42
     * @注释 条件历史查询
     */
    public List<CtrContImageAuditAppDomain> selectCtrImageByMode(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("whData", CmisCommonConstants.WF_STATUS_APP_LIST);
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppMapper.selectCtrImageByMode(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:42
     * @注释 条件历史查询
     */
    public List<CtrContImageAuditAppDomain> selectCtrImageHisByMode(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("whData", CmisCommonConstants.WF_STATUS_HIS_LIST);
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppMapper.selectCtrImageHisByMode(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param ctrLoanCont
     * @return ResultDto<java.lang.Boolean>
     * @author zxz
     * @date 2021/5/11 17:12
     * @version 1.0.0
     * @desc 点击下一步插入线上合同提款申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Boolean doSavleImageStep(CtrLoanCont ctrLoanCont) {
        logger.info("新增合同影像审核申请开始");
        CtrContImageAuditApp ctrContImageAuditApp = new CtrContImageAuditApp();
        try {
            boolean checkFlg = isEXisted(ctrLoanCont);
            if (checkFlg) {
                throw BizException.error(null, EcbEnum.ECB010061.key, EcbEnum.ECB010061.value);
            }
            BeanUtils.beanCopy(ctrLoanCont, ctrContImageAuditApp);
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            ctrContImageAuditApp.setSerno(serno);
            ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            ctrContImageAuditApp.setCreateTime(DateUtils.getCurrDate());
            ctrContImageAuditApp.setUpdateTime(DateUtils.getCurrDate());
            // 币种 担保方式字段 定义不一致
            ctrContImageAuditApp.setCurType(ctrLoanCont.getCurType());
            ctrContImageAuditApp.setGuarMode(ctrLoanCont.getGuarWay());
            // 所属条线 03 - 对公
            ctrContImageAuditApp.setBelgLine(CommonConstance.STD_BELG_LINE_03);
            // 获取登录信息
            User userInfo = SessionUtils.getUserInformation();
            ctrContImageAuditApp.setInputId(userInfo.getLoginCode());
            ctrContImageAuditApp.setInputBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setManagerId(userInfo.getLoginCode());
            ctrContImageAuditApp.setManagerBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setOprType(CmisCommonConstants.OP_TYPE_01);
            ctrContImageAuditApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ctrContImageAuditApp.setUpdId(userInfo.getLoginCode());
            ctrContImageAuditApp.setUpdBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ctrContImageAuditAppMapper.insert(ctrContImageAuditApp);

            logger.info("合同影像审核信息结束", ctrLoanCont.getContNo());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return true;
    }

    /**
     * @创建人 css
     * @创建时间 2021-06-24 9:42
     * @注释 校验当前合同编号是否已存在业务
     */

    public Boolean isEXisted(CtrLoanCont ctrLoanCont) {
        boolean result = false;
        QueryModel model = new QueryModel();
        model.getCondition().put("contNo", ctrLoanCont.getContNo());
        model.getCondition().put("belgLine", CommonConstance.STD_BELG_LINE_03);// 03- 对公
        model.getCondition().put("oprType", CmisCommonConstants.ADD_OPR);
        model.getCondition().put("whData", "000,992,111");

        List<CtrContImageAuditAppDomain> list = selectCtrImageByMode(model);
        if (list.size() > 0) {
            result = true;
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【111-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            logger.info("流程发起-获取合同影像审核申请" + iqpSerno + "申请主表信息");
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppMapper.selectByPrimaryKey(iqpSerno);
            if (ctrContImageAuditApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;

            logger.info("流程发起-更新合同影像审核合同申请" + iqpSerno + "流程审批状态为【111】-审批中");
            ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
            updateCount = ctrContImageAuditAppMapper.updateByPrimaryKey(ctrContImageAuditApp);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }


        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            logger.error("合同影像审核业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【解除】，业务与担保合同关系结果表数据更新为【解除】
     * <p>
     * 放款申请拒绝后，仅将当前申请状态变更为“否决”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 拒绝
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            logger.info("流程发起-获取合同影像审核申请" + iqpSerno + "申请信息");
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppMapper.selectByPrimaryKey(iqpSerno);

            if (ctrContImageAuditApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            logger.info("流程发起-更新合同影像审核合同申请" + iqpSerno + "流程审批状态为【998】-拒绝");
            int updateCount = 0;
            ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            updateCount = ctrContImageAuditAppMapper.updateByPrimaryKey(ctrContImageAuditApp);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }

        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            logger.error("流程发起" + iqpSerno + "更新合同影像审核合同申请处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    public int updateApproveStatus(String contNo, String approveStatus) {
        return ctrContImageAuditAppMapper.updateApproveStatus(contNo, approveStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            logger.info("审批通过-获取合同影像审核合同申请" + iqpSerno + "申请主表信息");
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppMapper.selectByPrimaryKey(iqpSerno);
            if (ctrContImageAuditApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;
            ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            updateCount = ctrContImageAuditAppMapper.updateByPrimaryKey(ctrContImageAuditApp);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            //合同影像审核流程通过后
            if (CmisCommonConstants.STD_BUSI_TYPE_01.equals(ctrContImageAuditApp.getBizType())) {
                //最高额授信协议
                CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(ctrContImageAuditApp.getContNo());
                ctrHighAmtAgrCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrHighAmtAgrContService.updateSelective(ctrHighAmtAgrCont);
                if (updateCount <= 0) {
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_03.equals(ctrContImageAuditApp.getBizType())) {
                //贴现协议
                CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(ctrContImageAuditApp.getContNo());
                ctrDiscCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrDiscContService.updateSelective(ctrDiscCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_02.equals(ctrContImageAuditApp.getBizType()) || CmisCommonConstants.STD_BUSI_TYPE_04.equals(ctrContImageAuditApp.getBizType())) {
                //普通贷款、贸易融资合同
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(ctrContImageAuditApp.getContNo());
                ctrLoanCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrLoanContService.updateSelective(ctrLoanCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_06.equals(ctrContImageAuditApp.getBizType())) {
                //开证合同
                CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByContNo(ctrContImageAuditApp.getContNo());
                ctrTfLocCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrTfLocContService.updateSelective(ctrTfLocCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_07.equals(ctrContImageAuditApp.getBizType())) {
                //银承合同
                CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(ctrContImageAuditApp.getContNo());
                ctrAccpCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrAccpContService.updateSelective(ctrAccpCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_08.equals(ctrContImageAuditApp.getBizType())) {
                //保函合同
                CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByContNo(ctrContImageAuditApp.getContNo());
                ctrCvrgCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrCvrgContService.updateSelective(ctrCvrgCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else if (CmisCommonConstants.STD_BUSI_TYPE_09.equals(ctrContImageAuditApp.getBizType())) {
                //委托贷款合同
                CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(ctrContImageAuditApp.getContNo());
                ctrEntrustLoanCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_1);
                updateCount = ctrEntrustLoanContService.updateSelective(ctrEntrustLoanCont);
                if (updateCount <= 0) {
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            } else {
                throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            logger.error("合同影像审核业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 合同影像申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuzz
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String iqpSerno) {
        logger.info("合同影像申请流程" + iqpSerno + "判断是否首次提交或退回提交传递参数");
        CtrContImageAuditApp ctrContImageAuditApp1 = ctrContImageAuditAppMapper.selectByPrimaryKey(iqpSerno);
        String approveStatus = ctrContImageAuditApp1.getApproveStatus();
        BigDecimal contAmt = ctrContImageAuditApp1.getContAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        params.put("approveStatus", approveStatus);
        params.put("contAmt", contAmt);
        logger.info("路由参数：" + JSON.toJSONString(params));
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        logger.info("合同影像申请流程" + iqpSerno + "参数传递完毕");
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 合同影像申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuzz
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrContImageAuditApp queryCtrContImageAuditAppBySerno(String serno) {
        return ctrContImageAuditAppMapper.queryCtrContImageAuditAppBySerno(serno);
    }

    /**
     * 根据合同号查询影像审核申请信息
     *
     * @param contNo
     * @return
     */
    public CtrContImageAuditApp selectByContNo(String contNo) {
        return ctrContImageAuditAppMapper.selectByContNo(contNo);
    }

    /**
     * @param ctrLoanContDto
     * @return Boolean
     * @author qw
     * @date 2021/8/9 17:12
     * @version 1.0.0
     * @desc 点击下一步新增影响审核申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map saveCtrLoanContDtoData(CtrLoanContDto ctrLoanContDto) {
        logger.info("新增合同影像审核申请开始");
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        CtrContImageAuditApp ctrContImageAuditApp = new CtrContImageAuditApp();
        try {
            Boolean checkFlg = isEXistedCtrContImageAuditApp(ctrLoanContDto.getContNo());
            if (checkFlg) {
                rtnCode = EcbEnum.ECB010061.key;
                rtnMsg = EcbEnum.ECB010061.value;
                throw BizException.error(null, EcbEnum.ECB010061.key, EcbEnum.ECB010061.value);
            }
            BeanUtils.beanCopy(ctrLoanContDto, ctrContImageAuditApp);
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            ctrContImageAuditApp.setSerno(serno);
            ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            ctrContImageAuditApp.setCreateTime(DateUtils.getCurrDate());
            ctrContImageAuditApp.setUpdateTime(DateUtils.getCurrDate());
            // 币种 担保方式字段 定义不一致
            ctrContImageAuditApp.setCurType(ctrLoanContDto.getCurType());
            ctrContImageAuditApp.setGuarMode(ctrLoanContDto.getGuarWay());
            // 所属条线 03 - 对公
            ctrContImageAuditApp.setBelgLine(CommonConstance.STD_BELG_LINE_03);
            ctrContImageAuditApp.setBizType(ctrLoanContDto.getBizType());
            // 获取登录信息
            User userInfo = SessionUtils.getUserInformation();
            ctrContImageAuditApp.setInputId(userInfo.getLoginCode());
            ctrContImageAuditApp.setInputBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setManagerId(userInfo.getLoginCode());
            ctrContImageAuditApp.setManagerBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setOprType(CmisCommonConstants.OP_TYPE_01);
            ctrContImageAuditApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ctrContImageAuditApp.setUpdId(userInfo.getLoginCode());
            ctrContImageAuditApp.setUpdBrId(userInfo.getOrg().getCode());
            ctrContImageAuditApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            int insertCount = ctrContImageAuditAppMapper.insert(ctrContImageAuditApp);
            if (insertCount <= 0) {
                rtnCode = EcbEnum.ECB020064.key;
                rtnMsg = EcbEnum.ECB020064.value;
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            result.put("imageAppSerno",serno);
            logger.info("合同:" + ctrLoanContDto.getContNo() + " 影像审核申请结束");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        } finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @param contNo
     * @return Boolean
     * @author qw
     * @date 2021/8/9 17:12
     * @version 1.0.0
     * @desc 检验新的影像审核申请是否重复
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Boolean isEXistedCtrContImageAuditApp(String contNo) {
        Boolean checkFlg = false;
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", contNo);
        queryModel.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        List<CtrContImageAuditApp> ctrContImageAuditAppList = ctrContImageAuditAppMapper.selectByModel(queryModel);
        if (CollectionUtils.nonEmpty(ctrContImageAuditAppList)) {
            checkFlg = true;
            return checkFlg;
        }
        return checkFlg;
    }

    /**
     * @方法名称: pendingList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrContImageAuditApp> pendingList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        List<CtrContImageAuditApp> list = ctrContImageAuditAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: processedList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrContImageAuditApp> processedList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        List<CtrContImageAuditApp> list = ctrContImageAuditAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 20:22
     **/
    public void createImageSpplInfo(String iqpSerno, String bizType, String instanceId, String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(iqpSerno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppMapper.selectByPrimaryKey(iqpSerno);
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(iqpSerno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_02);// 影像补扫类型 02:合同审核影像补扫
            docImageSpplClientDto.setCusId(ctrContImageAuditApp.getCusId());// 客户号
            docImageSpplClientDto.setCusName(ctrContImageAuditApp.getCusName());// 客户名称
            docImageSpplClientDto.setContNo(ctrContImageAuditApp.getContNo());
            docImageSpplClientDto.setInputId(ctrContImageAuditApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(ctrContImageAuditApp.getManagerBrId());// 登记机构
            docImageSpplClientDto.setPrdId(ctrContImageAuditApp.getPrdId());
            docImageSpplClientDto.setPrdName(ctrContImageAuditApp.getPrdName());
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), ctrContImageAuditApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", ctrContImageAuditApp.getCusName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * 房抵e点贷发送风控
     *
     * @author lihh
     * @date 2021/8/27 20:22
     **/
    public ResultDto sendfk(CtrContImageAuditApp ctrContImageAuditApp) {
        Fb1170ReqDto fb1170ReqDto = new Fb1170ReqDto();
        fb1170ReqDto.setCHANNEL_TYPE("13");
        fb1170ReqDto.setCO_PLATFORM("2002");
        fb1170ReqDto.setPRD_CODE("2002000001");
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", ctrContImageAuditApp.getContNo());
        List<CtrLoanCont> ctrLoanContList = ctrLoanContService.selectByQuerymodel(queryModel);
        if (CollectionUtils.nonEmpty(ctrLoanContList)) {
            CtrLoanCont ctrLoanCont = ctrLoanContList.get(0);
            if (!Objects.equals(ctrLoanCont.getContStatus(), "200")) {
                ResultDto resultDto = new ResultDto();
                resultDto.setCode("9999");
                resultDto.setMessage("合同已注销，发送失败");
                return resultDto;
            }
            fb1170ReqDto.setCont_no(ctrLoanCont.getContNo());
            fb1170ReqDto.setCont_no_cn("空");
            fb1170ReqDto.setCus_id(ctrLoanCont.getCusId());
            fb1170ReqDto.setCus_name(ctrLoanCont.getCusName());
            fb1170ReqDto.setCont_type(ctrLoanCont.getContType());
            fb1170ReqDto.setProduct("5");
            fb1170ReqDto.setAssure_means_main(ctrLoanCont.getGuarWay());
            fb1170ReqDto.setCur_type(ctrLoanCont.getCurType());
            fb1170ReqDto.setApply_amount(Optional.ofNullable(ctrLoanCont.getHighAvlAmt()).orElse(BigDecimal.ZERO).toString());
            fb1170ReqDto.setApply_amount_cny(Optional.ofNullable(ctrLoanCont.getHighAvlAmt()).orElse(BigDecimal.ZERO).toString());
            fb1170ReqDto.setTerm_time_type("001");
            fb1170ReqDto.setLoan_term(Optional.ofNullable(ctrLoanCont.getContTerm()).orElse(0).toString());
            fb1170ReqDto.setLoan_start_date(ctrLoanCont.getContStartDate());
            fb1170ReqDto.setLoan_end_date(ctrLoanCont.getContEndDate());
            fb1170ReqDto.setLimit_acc_no(ctrLoanCont.getLmtAccNo());
            QueryModel infoModel = new QueryModel();
            infoModel.addCondition("cusId", ctrLoanCont.getCusId());
            List<VisaXdRisk> visaXdRisks = visaXdRiskMapper.selectByModel(infoModel);
            if (CollectionUtils.nonEmpty(visaXdRisks)) {
                fb1170ReqDto.setPre_app_no(visaXdRisks.get(0).getCrpSerno());
            }
            queryModel.addCondition("accSubNo", ctrLoanCont.getLmtAccNo());
            List<LmtReplySub> lmtReplySubs = lmtReplySubService.selectByModel(queryModel);
            BigDecimal sxpf_amt = lmtReplySubs.stream().map(LmtReplySub::getLmtAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            fb1170ReqDto.setSxpf_amt(sxpf_amt);
            fb1170ReqDto.setLimit_ma_no(ctrLoanCont.getReplyNo());
            fb1170ReqDto.setCont_state(ctrLoanCont.getContStatus());
            SimpleDateFormat simp = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = new Date();
            String qd_date = simp.format(date);
            fb1170ReqDto.setQd_date(qd_date);
            fb1170ReqDto.setSubmit_time(qd_date);
            AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(ctrLoanCont.getManagerId());
            fb1170ReqDto.setManager_name(adminSmUserDto.getUserName());
            fb1170ReqDto.setManager_id(ctrLoanCont.getManagerId());
            fb1170ReqDto.setManager_br_id(ctrLoanCont.getManagerBrId());

            List<GrtGuarCont> grtGuarConts = grtGuarContMapper.selectDataByContNo(ctrLoanCont.getContNo());
            List<Dblist> dblists = grtGuarConts.stream().map(grtGuarCont -> {
                Dblist dblist = new Dblist();
                dblist.setDb_cont_no(grtGuarCont.getGuarContNo());
                dblist.setDb_cur_type(grtGuarCont.getCurType());
                dblist.setDh_cont_no_cn(grtGuarCont.getGuarContCnNo());
                dblist.setGuar_amt(Optional.ofNullable(grtGuarCont.getGuarAmt()).orElse(BigDecimal.ZERO).toString());
                dblist.setGuar_cont_state(grtGuarCont.getGuarContState());
                dblist.setGuar_end_date(grtGuarCont.getGuarEndDate());
                dblist.setGuar_start_date(grtGuarCont.getGuarStartDate());
                dblist.setGuar_way(grtGuarCont.getGuarWay());
                dblist.setSign_date(grtGuarCont.getSignDate());
                return dblist;
            }).collect(Collectors.toList());
            fb1170ReqDto.setDblist(dblists);

            List<GuarBaseInfo> guarBaseInfos = guarBaseInfoMapper.selectByContNo(ctrLoanCont.getContNo());
            List<Dylist> dylists = guarBaseInfos.stream().map(guarBaseInfo -> {
                Dylist dylist = new Dylist();
                dylist.setGuaranty_amt(Optional.ofNullable(guarBaseInfo.getEvalAmt()).orElse(BigDecimal.ZERO).toString());
                dylist.setGuaranty_id(guarBaseInfo.getGuarNo());
                dylist.setGuaranty_name(guarBaseInfo.getPldimnMemo());
                dylist.setGuaranty_state(guarBaseInfo.getGuarState());
                dylist.setGuaranty_type(guarBaseInfo.getGuarTypeCd());
                return dylist;
            }).collect(Collectors.toList());
            fb1170ReqDto.setDylist(dylists);
            logger.info("发送fb1170接口开始，参数为【{}】", JSON.toJSONString(fb1170ReqDto));
            ResultDto<Fb1170RespDto> fb1170RespDtoResultDto = dscms2CircpClientService.fb1170(fb1170ReqDto);
            logger.info("发送fb1170接口结束，返回参数为【{}】", JSON.toJSONString(fb1170RespDtoResultDto));
            if (Objects.nonNull(fb1170RespDtoResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fb1170RespDtoResultDto.getCode())) {
                ctrContImageAuditAppMapper.updateIsfk(ctrLoanCont.getContNo(), "1");
            } else {
                ResultDto resultDto = new ResultDto();
                resultDto.setCode("9999");
                resultDto.setMessage(fb1170RespDtoResultDto.getMessage());
                return resultDto;
            }
        } else {
            ResultDto resultDto = new ResultDto();
            resultDto.setCode("9999");
            resultDto.setMessage("查询合同失败，请联系管理员");
            return resultDto;
        }

        return new ResultDto().message("发送成功");
    }

    /**
     * 最高额授信协议生成档案归档任务
     *
     * @author jijian_yx
     * @date 2021/10/23 19:39
     **/
    public void createDocArchive(CtrContImageAuditApp ctrContImageAuditApp) {
        if (Objects.equals(ctrContImageAuditApp.getBizType(), CmisCommonConstants.STD_BUSI_TYPE_01)) {
            logger.info("开始系统生成最高额授信协议档案归档信息");
            String cusId = ctrContImageAuditApp.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setDocClass("03");// //01:基础资料档案,02:授信资料档案,03:重要信息档案
            docArchiveClientDto.setDocType("05");// 05:对公及个人经营性贷款
            docArchiveClientDto.setDocBizType("09");// 09:最高额授信协议
            docArchiveClientDto.setBizSerno(ctrContImageAuditApp.getSerno());
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(ctrContImageAuditApp.getManagerId());
            docArchiveClientDto.setManagerBrId(ctrContImageAuditApp.getManagerBrId());
            docArchiveClientDto.setInputId(ctrContImageAuditApp.getInputId());
            docArchiveClientDto.setInputBrId(ctrContImageAuditApp.getInputBrId());
            docArchiveClientDto.setContNo(ctrContImageAuditApp.getContNo());
            docArchiveClientDto.setLoanAmt(ctrContImageAuditApp.getContAmt());
            docArchiveClientDto.setPrdId(ctrContImageAuditApp.getPrdId());
            docArchiveClientDto.setPrdName(ctrContImageAuditApp.getPrdName());
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if (num < 1) {
                logger.info("系统生成最高额授信协议档案归档信息失败,业务流水号[{}]", ctrContImageAuditApp.getSerno());
            }
        }
    }
}
