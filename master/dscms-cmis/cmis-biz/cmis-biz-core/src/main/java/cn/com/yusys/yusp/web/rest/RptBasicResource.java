/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasic;
import cn.com.yusys.yusp.service.RptBasicService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-28 16:32:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasic")
public class RptBasicResource {
    @Autowired
    private RptBasicService rptBasicService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasic>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasic> list = rptBasicService.selectAll(queryModel);
        return new ResultDto<List<RptBasic>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasic>> index(QueryModel queryModel) {
        List<RptBasic> list = rptBasicService.selectByModel(queryModel);
        return new ResultDto<List<RptBasic>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptBasic> show(@PathVariable("serno") String serno) {
        RptBasic rptBasic = rptBasicService.selectByPrimaryKey(serno);
        return new ResultDto<RptBasic>(rptBasic);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasic> create(@RequestBody RptBasic rptBasic) throws URISyntaxException {
        rptBasicService.insert(rptBasic);
        return new ResultDto<RptBasic>(rptBasic);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasic rptBasic) throws URISyntaxException {
        int result = rptBasicService.update(rptBasic);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptBasicService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 修改授信调查报告
     * @param rptBasic
     * @return
     */
    @PostMapping("/updateRptBasic")
    protected ResultDto<Integer> updateRptBasic(@RequestBody RptBasic rptBasic){
        return new ResultDto<Integer>(rptBasicService.updateRptBasic(rptBasic));
    }

    /**
     * 根据流水号查询
     * @param map
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptBasic> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptBasic>(rptBasicService.selectByPrimaryKey(serno));
    }
    @PostMapping("/initRptBasic")
    protected ResultDto<Map> initRptBasic(@RequestBody String serno){
        return new ResultDto<Map>(rptBasicService.initRptBasic(serno));
    }
}
