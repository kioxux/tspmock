package cn.com.yusys.yusp.service.client.bsp.core.ib1253;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author wangyk
 * @version 1.0.0
 * @date 2021/6/29 10:33
 * @desc 子账户信息新查询
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ib1253Service {
    private static final Logger logger = LoggerFactory.getLogger(Ib1253Service.class);
    // 1）注入：BSP封装调用核心系统的接口
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    /**
     * @param ib1253ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto
     * @author 王玉坤
     * @date 2021/6/22 20:13
     * @version 1.0.0
     * @desc 调用子账户信息新查询接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Ib1253RespDto ib1253(Ib1253ReqDto ib1253ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value, JSON.toJSONString(ib1253ReqDto));
        ResultDto<Ib1253RespDto> ib1253RespDtoResultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value, JSON.toJSONString(ib1253RespDtoResultDto));
        String ib1253Code = Optional.ofNullable(ib1253RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ib1253Meesage = Optional.ofNullable(ib1253RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ib1253RespDto ib1253RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ib1253RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            ib1253RespDto = ib1253RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(ib1253Code, ib1253Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value);
        return ib1253RespDto;
    }
}
