/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.domain.SurveyTaskDivis;
import cn.com.yusys.yusp.dto.Yqd001req;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyTaskDivis;
import cn.com.yusys.yusp.service.LmtSurveyTaskDivisService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyTaskDivisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 13:48:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "调查任务分配")
@RequestMapping("/api/lmtsurveytaskdivis")
public class LmtSurveyTaskDivisResource {
    @Autowired
    private LmtSurveyTaskDivisService lmtSurveyTaskDivisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyTaskDivis>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyTaskDivis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyTaskDivis>> index(QueryModel queryModel) {
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyTaskDivis>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSurveyTaskDivis> show(@PathVariable("pkId") String pkId) {
        LmtSurveyTaskDivis lmtSurveyTaskDivis = lmtSurveyTaskDivisService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSurveyTaskDivis>(lmtSurveyTaskDivis);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyTaskDivis> create(@RequestBody LmtSurveyTaskDivis lmtSurveyTaskDivis) throws URISyntaxException {
        lmtSurveyTaskDivisService.insert(lmtSurveyTaskDivis);
        return new ResultDto<LmtSurveyTaskDivis>(lmtSurveyTaskDivis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyTaskDivis lmtSurveyTaskDivis) throws URISyntaxException {
        int result = lmtSurveyTaskDivisService.update(lmtSurveyTaskDivis);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSurveyTaskDivisService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyTaskDivisService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }



    /**
     * @param lmtSurveyTaskDivis
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author WH
     * @date 2021/4/22 13:58
     * @version 1.0.0
     * @desc 分配负责人，公共API接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("分配负责人，公共API接口")
    @PostMapping("/allocation")
    protected ResultDto<Integer> allocation(@RequestBody LmtSurveyTaskDivis lmtSurveyTaskDivis) throws Exception{
        return lmtSurveyTaskDivisService.allocation(lmtSurveyTaskDivis);

    }
   /**
     * @创建人 WH
     * @创建时间 2021-04-28 10:50
     * @注释 条件分页查询 附加条件 业务状态->in
     */
    @ApiOperation("条件分页查询附加条件")
    @PostMapping("/findlistbymodel")
    protected ResultDto<List<LmtSurveyTaskDivis>> findlistbymodel(@RequestBody QueryModel queryModel) {
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisService.findlistbymodel(queryModel);
        return new ResultDto<List<LmtSurveyTaskDivis>>(list);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/16 11:19
     * @version 1.0.0
     * @desc 优享贷客户经理确认查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("优享贷客户经理确认查询")
    @PostMapping("/findlistbyyxd")
    protected ResultDto findlistByYxd(@RequestBody QueryModel queryModel) {
        return   lmtSurveyTaskDivisService.findlistByYxd(queryModel);

    }

    /**
     * @param lmtSurveyReportDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/16 11:23
     * @version 1.0.0
     * @desc    优享贷客户经理确认
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("优享贷客户经理确认")
    @PostMapping("/marconfirm")
    protected ResultDto marConfirm(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        return   lmtSurveyTaskDivisService.marConfirm(lmtSurveyReportDto.getSurveySerno());
    }

}
