/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.IqpAccpAppPorderSubImportVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAccpAppPorderSub;
import cn.com.yusys.yusp.service.IqpAccpAppPorderSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAccpAppPorderSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-19 10:08:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "银承协议申请汇票明细")
@RequestMapping("/api/iqpaccpapppordersub")
public class IqpAccpAppPorderSubResource {
    private static final Logger log = LoggerFactory.getLogger(IqpAccpAppPorderSubResource.class);
    @Autowired
    private IqpAccpAppPorderSubService iqpAccpAppPorderSubService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAccpAppPorderSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAccpAppPorderSub> list = iqpAccpAppPorderSubService.selectAll(queryModel);
        return new ResultDto<List<IqpAccpAppPorderSub>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAccpAppPorderSub>> index(QueryModel queryModel) {
        List<IqpAccpAppPorderSub> list = iqpAccpAppPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpAccpAppPorderSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAccpAppPorderSub> show(@PathVariable("pkId") String pkId) {
        IqpAccpAppPorderSub iqpAccpAppPorderSub = iqpAccpAppPorderSubService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAccpAppPorderSub>(iqpAccpAppPorderSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAccpAppPorderSub> create(@RequestBody IqpAccpAppPorderSub iqpAccpAppPorderSub) throws URISyntaxException {
        iqpAccpAppPorderSubService.insert(iqpAccpAppPorderSub);
        return new ResultDto<IqpAccpAppPorderSub>(iqpAccpAppPorderSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAccpAppPorderSub iqpAccpAppPorderSub) throws URISyntaxException {
        int result = iqpAccpAppPorderSubService.update(iqpAccpAppPorderSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAccpAppPorderSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAccpAppPorderSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 银承票据明细保存方法
     *
     * @param iqpAccpAppPorderSub
     * @return
     */
    @ApiOperation("银承票据明细保存方法")
    @PostMapping("/saveiaapordersubinfo")
    public ResultDto<Map> saveIqpAccpAppPorderSubInfo(@RequestBody IqpAccpAppPorderSub iqpAccpAppPorderSub) {
        Map result = iqpAccpAppPorderSubService.saveIaaPorderSubInfo(iqpAccpAppPorderSub);
        return new ResultDto<>(result);
    }

    /**
     * 银承票据明细信息逻辑删除方法
     *
     * @param iqpAccpAppPorderSub
     * @return
     */
    @ApiOperation("银承票据明细信息逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpAccpAppPorderSub iqpAccpAppPorderSub) {
        Integer result = iqpAccpAppPorderSubService.logicDelete(iqpAccpAppPorderSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询数据列表")
    @PostMapping("/selectlist")
    protected ResultDto<List<IqpAccpAppPorderSub>> selectList(@RequestBody QueryModel queryModel) {
        queryModel.setPage(1);
        queryModel.setSize(10000);
        List<IqpAccpAppPorderSub> list = iqpAccpAppPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpAccpAppPorderSub>>(list);
    }

    /**
     * Excel数据，批量导入票据明细
     *
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importIqpAccpAppPorderSub")
    public ResultDto<ProgressDto> asyncimportIqpAccpAppPorderSub(@RequestParam("fileId") String fileId, @RequestBody Map map) throws IOException {
        String serno = "";
        String msg = "导入成功";
        String code = "0";
        if (map.containsKey("serno")) {
            serno = (String) map.get("serno");
        } else {
            return ResultDto.error(500, "业务流水号缺失！导入失败");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
            String finalSerno = serno;  // recoverySerno为前端传递过来的参数
            ExcelUtils.syncImport(IqpAccpAppPorderSubImportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return iqpAccpAppPorderSubService.insertIqpAccpAppPorderSub(dataList, finalSerno);
            }), true);
        } catch (Exception e) {
            code = EclEnum.ECL070097.key;
            msg = "请确保上传Excel的单元格格式内容为“文本”";
            log.error(EclEnum.ECL070097.value, e);
            throw BizException.error(null, EclEnum.ECL070097.key, EclEnum.ECL070097.value);
        } finally {
            return new ResultDto().message(msg).code(code);
        }
    }



    /**
     * 异步下载票据明细模板
     */
    @PostMapping("/exportIqpAccpAppPorderSubTemp")
    public ResultDto<ProgressDto> asyncexportIqpAccpAppPorderSubTemp() {
        ProgressDto progressDto = iqpAccpAppPorderSubService.asyncexportIqpAccpAppPorderSubTemp();
        return ResultDto.success(progressDto);
    }
}
