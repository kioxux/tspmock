package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtFinSpApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtFinSpAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className IqpRepayWayChgBiz
 * @Description 还款方式变更申请流程处理类
 * @Date 2021/01/14 : 10:48
 */
@Service
public class LmtFinSpAppBiz implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(LmtFinSpAppBiz.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private LmtFinSpAppService lmtFinSpAppService;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String iqpSerno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            LmtFinSpApp lmtFinSpApp = new LmtFinSpApp();
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            lmtFinSpApp = lmtFinSpAppService.selectByPrimaryKey(iqpSerno);
            if(lmtFinSpApp == null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----"+instanceInfo);
                // 改变标志 -> 审批中
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //更新业务申请状态 由审批中111 -> 审批通过 997
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
                // 流程审批通过,通知核算

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                lmtFinSpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                int num = lmtFinSpAppService.updateSelective(lmtFinSpApp);
                log.info("更新了"+num+"条数据！");
                log.info("否决操作结束:" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.BIZ_TYPE_LMT_FIN_SP_APP.equals(flowCode);
    }
}
