/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AsplBailAcctDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpAppAsplMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAppAsplService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 13:52:31

 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAppAsplService {
    private static final Logger log = LoggerFactory.getLogger(IqpEntrustLoanAppService.class);

    @Autowired
    private IqpAppAsplMapper iqpAppAsplMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpAppAspl selectByPrimaryKey(String pkId) {
        return iqpAppAsplMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAppAspl> selectAll(QueryModel model) {
        List<IqpAppAspl> records = (List<IqpAppAspl>) iqpAppAsplMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpAppAspl> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAppAspl> list = iqpAppAsplMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpAppAspl record) {
        return iqpAppAsplMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpAppAspl record) {
        return iqpAppAsplMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpAppAspl record) {
            return iqpAppAsplMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpAppAspl record) {
        return iqpAppAsplMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAppAsplMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAppAsplMapper.deleteByIds(ids);
    }

    /**
     * 资产池协议申请新增页面点击下一步(同时生成担保合同和保证金账户)
     * @param iqpAppAspl
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpAppAsplInfo(IqpAppAspl iqpAppAspl) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (iqpAppAspl == null) {
                rtnCode = EcbEnum.ECB010017.key;
                rtnMsg = EcbEnum.ECB010017.value;
                return result;
            }
            String cusId = iqpAppAspl.getCusId();
            // 判断已选客户下面是否存在在途的协议申请记录
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);
            model.addCondition("applyExistsStatus",CmisCommonConstants.WF_STATUS_000992111);
            model.addCondition("chgFlag",CommonConstance.STD_ZB_YES_NO_0);// 是否协议变更
            model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpAppAspl>  iqpAppAspls = selectByModel(model);
            if(iqpAppAspls.size()>0){
                rtnCode = EcbEnum.ECB020056.key;
                rtnMsg = EcbEnum.ECB020056.value;
                return result;
            }
            // 判断已选客户名下有无资产池额度批复
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdDataByParams(cusId);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                rtnCode = EcbEnum.ECB020010.key;
                rtnMsg = EcbEnum.ECB020010.value;
                return result;
            }else{
                CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
                Map map = new HashMap();
                map.put("cusId",cusId);
                map.put("oprType", CommonConstance.OPR_TYPE_ADD);
                map.put("accStatus",CommonConstance.STD_REPLY_STATUS_01);
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                //分两种情况，借据余额为0（该客户存在生效的资产池协议，请先注销原协议）、借据余额不为0（该客户存在生效的资产池协议，请申请协议变更）
                if(Objects.nonNull(ctrAsplDetails)){
                    rtnCode = EcbEnum.ECB020011.key;
                    rtnMsg = EcbEnum.ECB020011.value;
                    return result;
                }else{
                    //生成合同编号
                    String contNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.ZRCZC_SEQ,new HashMap());
                    //客户编号
                    iqpAppAspl.setCusId(lmtReplyAccSubPrd.getCusId());
                    //客户名称
                    iqpAppAspl.setCusName(lmtReplyAccSubPrd.getCusName());
                    //合同编号
                    iqpAppAspl.setContNo(contNo);
                    // 产品编号
                    iqpAppAspl.setPrdId("012040");
                    // 产品名称
                    iqpAppAspl.setPrdName("资产池");
                    // 默认万分之五（手续费）
                    iqpAppAspl.setChrgRate(new BigDecimal(0.0005));
                    // 担保方式(21为低风险质押)
                    iqpAppAspl.setGuarMode(CommonConstance.GUAR_MODE_21);
                    // 合同类型
                    iqpAppAspl.setContType(CommonConstance.CONT_TYPE_2);
                    // 币种
                    iqpAppAspl.setCurType(CmisCommonConstants.CUR_TYPE_CNY);
                    // 申请金额
                    iqpAppAspl.setContAmt(lmtReplyAccSubPrd.getLmtAmt());
                    // 协议起始日
                    iqpAppAspl.setStartDate(lmtReplyAccSubPrd.getStartDate());
                    // 协议到期日
                    iqpAppAspl.setEndDate(lmtReplyAccSubPrd.getEndDate());
                    // 一般风险额度
                    iqpAppAspl.setCommonRiskAmt(new BigDecimal(0));
                    // 低风险额度（申请金额减去一般风险额度）
                    iqpAppAspl.setLowRiskAmt(lmtReplyAccSubPrd.getLmtAmt());
                    // 利率调整方式
                    iqpAppAspl.setRateAdjMode(CommonConstance.RATE_ADJ_MODE_01);
                    // LPR定价区间LPR_PRICE_INTERVAL_A1
                    iqpAppAspl.setLprPriceInterval(CommonConstance.LPR_PRICE_INTERVAL_A1);
                    // 垫款利率（银承）核心默认18，这里只做展示
                    iqpAppAspl.setPadRateYear(new BigDecimal(0.18));
                    // 默认浮点数15点数
                    iqpAppAspl.setRateFloatPoint(new BigDecimal(15));
                    // 授信协议编号
                    iqpAppAspl.setLmtAccNo(lmtReplyAccSubPrd.getAccSubPrdNo());
                    // 批复流水号
                    iqpAppAspl.setReplySerno(lmtReplyAcc.getReplySerno());
                    // 资产池授信额度
                    iqpAppAspl.setLmtAmt(lmtReplyAccSubPrd.getLmtAmt());
                    // 授信起始日
                    iqpAppAspl.setLmtStartDate(lmtReplyAccSubPrd.getStartDate());
                    // 授信到期日
                    iqpAppAspl.setLmtEndDate(lmtReplyAccSubPrd.getEndDate());
                    //业务类型
                    iqpAppAspl.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_10);
                    //变更标志 0否
                    iqpAppAspl.setChgFlag(CommonConstance.STD_ZB_YES_NO_0);
                    //是否续签 0否
                    iqpAppAspl.setIsRenew(CommonConstance.STD_ZB_YES_NO_0);
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo == null) {
                        rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                        rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                        return result;
                    } else {
                        iqpAppAspl.setInputId(userInfo.getLoginCode());
                        iqpAppAspl.setInputBrId(userInfo.getOrg().getCode());
                        iqpAppAspl.setManagerId(userInfo.getLoginCode());
                        iqpAppAspl.setManagerBrId(userInfo.getOrg().getCode());
                        iqpAppAspl.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    }

                    Map seqMap = new HashMap();
                    String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO,seqMap);

                    iqpAppAspl.setSerno(serno);
                    iqpAppAspl.setPkId(StringUtils.uuid(true));
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                    iqpAppAspl.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    int insertCount = iqpAppAsplMapper.insertSelective(iqpAppAspl);
                    if(insertCount<=0){
                        //若是出现异常则需要回滚，因此直接抛出异常
                        throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
                    }
                    result.put("serno",serno);
                    log.info("资产池协议申请"+serno+"-保存成功！");
                    Map queryData = new HashMap();
                    queryData.put("serno",serno);
                    IqpAppAspl iqpAppAsplData = this.queryIqpAppAsplDataByParams(queryData);

                    // 资产池协议申请新增成功后，添加担保合同业务关系
                    GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
                    //主键
                    grtGuarBizRstRel.setPkId(StringUtils.uuid(true));
                    //主合同编号
                    grtGuarBizRstRel.setContNo(iqpAppAsplData.getContNo());
                    //申请流水号
                    grtGuarBizRstRel.setSerno(serno);
                    //担保合同编号
                    HashMap<String, String> param = new HashMap<>();
                    String guarContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, param);
                    grtGuarBizRstRel.setGuarContNo(guarContNo);
                    //担保合同流水号
                    Map guarPkIdMap = new HashMap();
                    String guarPkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO,guarPkIdMap);
                    grtGuarBizRstRel.setGuarPkId(guarPkId);
                    //担保金额
                    grtGuarBizRstRel.setGuarAmt(iqpAppAsplData.getLmtAmt());
                    //是否追加担保
                    grtGuarBizRstRel.setIsAddGuar(CmisCommonConstants.YES_NO_1);
                    //关联关系(暂时默认为生效)
                    grtGuarBizRstRel.setCorreRel(CmisCommonConstants.STD_ZB_CORRE_REL_1);
                    //其它字段
                    grtGuarBizRstRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    grtGuarBizRstRel.setInputId(iqpAppAsplData.getInputId());
                    grtGuarBizRstRel.setInputBrId(iqpAppAsplData.getInputBrId());
                    grtGuarBizRstRel.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    grtGuarBizRstRel.setManagerId(userInfo.getLoginCode());
                    grtGuarBizRstRel.setManagerBrId(userInfo.getOrg().getCode());
                    grtGuarBizRstRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarBizRstRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    int insertData = grtGuarBizRstRelMapper.insertSelective(grtGuarBizRstRel);
                    if(insertData<=0){
                        //若是出现异常则需要回滚，因此直接抛出异常
                        throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
                    }else {
                        log.info("担保合同和业务关系"+serno+"-新增成功！");
                        //生成担保合同
                        GrtGuarCont grtGuarCont = new GrtGuarCont();
                        //担保合同流水号
                        grtGuarCont.setGuarPkId(guarPkId);
                        //借款人编号（客户编号）
                        grtGuarCont.setCusId(iqpAppAsplData.getCusId());
                        //借款人名称（客户名称）
                        grtGuarCont.setCusName(iqpAppAsplData.getCusName());
                        //担保合同编号
                        grtGuarCont.setGuarContNo(guarContNo);
                        //担保合同类型（默认最高额担保合同）
                        grtGuarCont.setGuarContType(CmisCommonConstants.STD_ZB_GUAR_CONT_TYPE_B);
                        //担保方式
                        grtGuarCont.setGuarWay(CmisCommonConstants.GUAR_MODE_21);
                        //币种
                        grtGuarCont.setCurType(CmisCommonConstants.CUR_TYPE_CNY);
                        //担保合同金额
                        grtGuarCont.setGuarAmt(iqpAppAsplData.getLmtAmt());
                        //担保起始日
                        grtGuarCont.setGuarStartDate(iqpAppAsplData.getStartDate());
                        //担保终止日
                        grtGuarCont.setGuarEndDate(iqpAppAsplData.getEndDate());
                        //担保合同状态
                        grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                        //其他字段
                        grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        grtGuarCont.setInputId(iqpAppAsplData.getInputId());
                        grtGuarCont.setInputBrId(iqpAppAsplData.getInputBrId());
                        grtGuarCont.setInputDate(iqpAppAsplData.getInputDate());
                        grtGuarCont.setManagerId(userInfo.getLoginCode());
                        grtGuarCont.setManagerBrId(userInfo.getOrg().getCode());
                        grtGuarCont.setBizLine("03");
                        int insertGrtGuarCont = grtGuarContMapper.insertSelective(grtGuarCont);
                        if(insertGrtGuarCont<=0){
                            //若是出现异常则需要回滚，因此直接抛出异常
                            throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
                        }
                        log.info("担保合同"+serno+"-新增成功！");
                    }

                    //生成担保合同后，生成保证金账户
                    BailAccInfo bailAccInfo = new BailAccInfo();
                    bailAccInfo.setPkId(StringUtils.uuid(true));
                    bailAccInfo.setSerno(serno);
                    bailAccInfo.setBailRate(lmtReplyAccSubPrd.getBailPreRate());
                    bailAccInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    int insertBailAccInfo = bailAccInfoService.insertSelective(bailAccInfo);
                    if(insertBailAccInfo<=0){
                        //若是出现异常则需要回滚，因此直接抛出异常
                        throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
                    }
                    log.info("保证金账户"+serno+"-新增成功！");
                }
            }
        }catch(YuspException e){
            log.error("资产池协议申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("资产池协议申请异常！",e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 资产池协议申请通用保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpAppAsplInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "资产池协议申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpAppAspl iqpAppAspl = JSONObject.parseObject(JSON.toJSONString(params), IqpAppAspl.class);
            if (iqpAppAspl == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存资产池协议申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpAppAspl.setUpdId(userInfo.getLoginCode());
                iqpAppAspl.setUpdBrId(userInfo.getOrg().getCode());
                iqpAppAspl.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "资产池协议申请数据");
            int updCount = iqpAppAsplMapper.updateByPrimaryKeySelective(iqpAppAspl);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }
            // 更新该资产池协议下担保合同到期日起始日（资产池下只有一个担保合同）
            List<GrtGuarCont> GrtGuarConts = queryGrtGuarContByParams(serno);
            if (CollectionUtils.nonEmpty(GrtGuarConts)) {
                GrtGuarCont grtGuarCont = GrtGuarConts.get(0);
                grtGuarCont.setGuarStartDate(iqpAppAspl.getStartDate());// 担保起始日
                grtGuarCont.setGuarEndDate(iqpAppAspl.getEndDate());// 担保到期日
                grtGuarContService.updateSelective(grtGuarCont);
            }
        } catch (Exception e) {
            log.error("保存资产池协议申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(IqpAppAspl iqpAppAspl) {
        int updateCount = 0;
        String serno = iqpAppAspl.getSerno();
        try{
            // 如果是打回则自行退出，不是打回自行删除
            if(Objects.equals(CmisCommonConstants.WF_STATUS_992,iqpAppAspl.getApproveStatus())){
                // 修改状态为自行退出
                iqpAppAsplMapper.updateStatus(serno,CmisCommonConstants.WF_STATUS_996);
                workflowCoreClient.deleteByBizId(iqpAppAspl.getSerno());
            }else{
                iqpAppAspl.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                updateCount = iqpAppAsplMapper.updateByPrimaryKeySelective(iqpAppAspl);
//                //查询与主合同关联业务关系数据以及对应的担保合同
//                GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelService.selectGrtGuarBizRstRelDataBySerno(iqpAppAspl.getSerno());
//                GrtGuarCont grtGuarCont = grtGuarContService.selectByPrimaryKey(grtGuarBizRstRel.getGuarPkId());
//                //删除关联的担保合同数据
//                grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
//                int updateGrtGuarCont = grtGuarContService.updateSelectiveByGuarContNo(grtGuarCont);
//                if(updateGrtGuarCont<=0){
//                    log.error("删除担保合同异常！");
//                    throw BizException.error(null, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.key, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.value);
//                }
//                //删除对应的业务关系数据
//                grtGuarBizRstRel.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
//                int updateGrtGuarBizRstRel = grtGuarBizRstRelService.updateSelective(grtGuarBizRstRel);
//                if(updateGrtGuarBizRstRel<=0){
//                    log.error("删除业务与担保合同关联关系异常！");
//                    throw BizException.error(null, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.key, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.value);
//                }
//                //删除保证金账号
//                int deleteCount = bailAccInfoService.deleteBySerno(serno);
//                if(deleteCount<=0){
//                    log.error("保证金账户删除失败！");
//                    throw BizException.error(null, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.key, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.value);
//                }
            }
        }catch (BizException e ){
            updateCount = 0;
            throw BizException.error(null, EcbEnum.BIZWORKFLOW_EXCEPTION_DEF.key, e.getMessage());
        }catch (Exception e){
            updateCount = 0;
            throw new Exception("资产池协议申请删除失败");
        }finally {
            return updateCount;
        }
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpAppAspl> toSignlist(QueryModel model) {
//        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.setSort("input_date desc");
        return iqpAppAsplMapper.selectByModel(model);
    }

    /**
     * @方法名称: queryIqpAppAsplDataByParams
     * @方法描述: 根据入参查询资产池协议申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public IqpAppAspl queryIqpAppAsplDataByParams(Map queryMap) {
        return iqpAppAsplMapper.queryIqpAppAsplDataByParams(queryMap);
    }

    /**
     * @方法名称: generateCtrAsplDetails
     * @方法描述: 生成资产池协议
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-06-04 16:10:22
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map generateCtrAsplDetails(Map map){
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            // 校验该笔资产池是否有保证金账号和结算账号
            serno = (String) map.get("serno");
            if(StringUtils.isEmpty(serno)){
                throw new BizException(null, EcbEnum.ECB010050.key,null,EcbEnum.ECB010050.value);
            }
            // 1、检验保证金账号
            String bailAccNo = bailAccInfoService.selectAccNoBySerno(serno);
            if(StringUtils.isEmpty(bailAccNo)){
                throw new BizException(null, EcbEnum.ECB020022.key,null,EcbEnum.ECB020022.value);
            }
            map.put("oprType", CommonConstance.OPR_TYPE_ADD);
            IqpAppAspl iqpAppAspl = iqpAppAsplMapper.queryIqpAppAsplDataByParams(map);
            if(iqpAppAspl == null){
                throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
            }
            // 2、检验结算账号
            String acctNo = iqpAppAspl.getAcctNo();
            if(StringUtils.isEmpty(acctNo)){
                throw new BizException(null, EcbEnum.ECB020023.key,null,EcbEnum.ECB020023.value);
            }
            //生成资产池协议前，先调额度系统接口占额
            String isLriskBiz = "";
            BigDecimal bizTotalAmt = BigDecimal.ZERO;
            BigDecimal bizSpacAmt = BigDecimal.ZERO;
            BigDecimal bizTotalAmtCny = BigDecimal.ZERO;
            BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
            String guarMode = iqpAppAspl.getGuarMode();
            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
                isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_1;
                bizTotalAmt = iqpAppAspl.getContAmt();
                bizSpacAmt = BigDecimal.ZERO;
                bizTotalAmtCny = iqpAppAspl.getContAmt();
                bizSpacAmtCny = BigDecimal.ZERO;
            }else {
                isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
                bizTotalAmt = iqpAppAspl.getContAmt();
                bizSpacAmt = iqpAppAspl.getContAmt();
                bizTotalAmtCny = iqpAppAspl.getContAmt();
                bizSpacAmtCny = iqpAppAspl.getContAmt();
            }

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAppAspl.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpAppAspl.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpAppAspl.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpAppAspl.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpAppAspl.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpAppAspl.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpAppAspl.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(isLriskBiz);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(CmisCommonConstants.STD_ZB_YES_NO_0);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(iqpAppAspl.getSerno());//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus("");//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType("");//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr("");//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpAppAspl.getContAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpAppAspl.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpAppAspl.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpAppAspl.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpAppAspl.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpAppAspl.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpAppAspl.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(bizTotalAmt);//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(bizSpacAmt);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(bizTotalAmtCny);//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(bizSpacAmtCny);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);

            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("资产池协议业务申请" + serno + "，前往额度系统进行额度占用开始");
            ResultDto<CmisLmt0011RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("资产池协议业务申请" + serno + "，前往额度系统进行额度占用结束，返回："+resultDtoDto);
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }

            //占额完成后，生成资产池协议
            CtrAsplDetails ctrAsplDetails = new CtrAsplDetails();
            BeanUtils.copyProperties(iqpAppAspl, ctrAsplDetails);

            ctrAsplDetails.setContStatus(CmisCommonConstants.CONT_STATUS_100);//资产池协议状态默认【未生效】
            int insertCount = ctrAsplDetailsService.insertSelective(ctrAsplDetails);
            if (insertCount < 0) {
                throw new YuspException(EcbEnum.E_CTR_ASPLDETAILS_FAILED.key, EcbEnum.E_CTR_ASPLDETAILS_FAILED.value);
            }
            iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            int updateCount = iqpAppAsplMapper.updateByPrimaryKey(iqpAppAspl);
            if (updateCount<0){
                throw new YuspException(EcbEnum.E_CTR_ASPLDETAILS_FAILED.key, EcbEnum.E_CTR_ASPLDETAILS_FAILED.value);
            }
            log.info("生成资产池协议" + serno + "成功");
        }catch (Exception e) {
            log.error("生成资产池协议" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: queryIqpAppAsplDataBySerno
     * @方法描述: 根据申请流水号获取申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpAppAspl> queryIqpAppAsplDataBySerno(String serno) {
        return iqpAppAsplMapper.queryIqpAppAsplDataBySerno(serno);
    }

    /**
     * @方法名称: queryAsplBailAcctDtoDataByParams
     * @方法描述: 根据入参获取账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplBailAcctDto> queryAsplBailAcctDtoDataByParams(Map map) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        queyParam.put("cusId",(String)map.get("cusId"));
        queyParam.put("serno",(String)map.get("serno"));
        return iqpAppAsplMapper.queryAsplBailAcctDtoDataByParams(queyParam);
    }

    /**
     * @方法名称: queryGrtGuarContByParams
     * @方法描述: 根据申请流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GrtGuarCont> queryGrtGuarContByParams(String serno) {
        List list = new ArrayList();
        GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelService.selectGrtGuarBizRstRelDataBySerno(serno);
        if(grtGuarBizRstRel!=null){
            QueryModel model = new QueryModel();
            model.getCondition().put("guarContNo",grtGuarBizRstRel.getGuarContNo());
            list = grtGuarContService.selectByModel(model);
        }
        return list;
    }

    /**
     * @方法名称: getGuarBaseInfoByParams
     * @方法描述: 根据申请流水号获取质押物信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GuarBaseInfo> getGuarBaseInfoByParams(Map map) {
        //int page = Integer.parseInt(Objects.toString(map.get("page")));
        //int size = Integer.parseInt(Objects.toString(map.get("size")));
        String guarContNo = (String)map.get("guarContNo");
        //PageHelper.startPage(page, size);
        //在后面方法分页
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.selectByGuarContNoModel(guarContNo,map);
        //PageHelper.clearPage();
        return guarBaseInfoList;
    }
    /**
     * 根据申请流水号查询
     * @param serno
     * @return
     */
    public IqpAppAspl selectBySerno(String serno) {
        QueryModel model = new QueryModel();
        model.addCondition("serno",serno);
        List<IqpAppAspl> list = iqpAppAsplMapper.selectByModel(model);
        if(CollectionUtils.nonEmpty(list)){
            return list.get(0);
        }else{
            return null;
        }
    }
    /**
     * 根据资产池协议原流水号变更
     * @param  serno
     * @return
     */
    @Transactional
    public ResultDto<IqpAppAspl> updateChgFlag(String serno) {
        String code = "0";
        String msg = "资产池协议变更成功";
        IqpAppAspl originalIqpAppAspl = new IqpAppAspl();

        try{
            // 获取当前营业时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 资产池协议
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoBySerno(serno);
            // 获取原资产池协议编号
            String origiContNo = ctrAsplDetails.getContNo();
            log.info("资产池协议变更开始，协议编号："+origiContNo);
            String cusId = ctrAsplDetails.getCusId();
             // 校验该留用户下是否存在其他协议变更
            IqpAppAspl iqpAppAspl= iqpAppAsplMapper.selectAvalChgByCusId(cusId);
            if(Objects.nonNull(iqpAppAspl)){
                code = "9999";
                msg = "用户："+iqpAppAspl.getCusId()+",存在在途变更，流水号："+iqpAppAspl.getSerno();
                throw BizException.error(null,code,msg);
            }
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdDataByParams(cusId);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                code = EcbEnum.ECB020061.key;
                msg = EcbEnum.ECB020061.value;
                throw BizException.error(null,code,msg);
            }
            //判断已选客户名下有无资产池额度批复
            Map map = new HashMap();
            map.put("cusId",cusId);
            map.put("oprType", CommonConstance.OPR_TYPE_ADD);
            map.put("accStatus",CommonConstance.STD_REPLY_STATUS_01);
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
            if(Objects.isNull(lmtReplyAcc)){
                code = EcbEnum.ECB020012.key;
                msg = EcbEnum.ECB020012.value;
                throw BizException.error(null,code,msg);
            }
            // 新增原资产池协议申请
            String newSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            // 生成新的协议编号
            String contNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.ZRCZC_SEQ, new HashMap<>());
            // 查询原协议
            originalIqpAppAspl = selectBySerno(serno);
            //客户编号
            originalIqpAppAspl.setCusId(lmtReplyAccSubPrd.getCusId());
            //客户名称
            originalIqpAppAspl.setCusName(lmtReplyAccSubPrd.getCusName());
            //合同编号
            originalIqpAppAspl.setContNo(contNo);
            // 申请金额
            originalIqpAppAspl.setContAmt(lmtReplyAccSubPrd.getLmtAmt());
            // 协议起始日
            originalIqpAppAspl.setStartDate(lmtReplyAccSubPrd.getStartDate());
            // 协议到期日
            originalIqpAppAspl.setEndDate(lmtReplyAccSubPrd.getEndDate());
            // 低风险额度（申请金额减去一般风险额度）
            originalIqpAppAspl.setLowRiskAmt(lmtReplyAccSubPrd.getLmtAmt());
            // 授信协议编号
            originalIqpAppAspl.setLmtAccNo(lmtReplyAccSubPrd.getAccSubPrdNo());
            // 批复流水号
            originalIqpAppAspl.setReplySerno(lmtReplyAcc.getReplySerno());
            // 资产池授信额度
            originalIqpAppAspl.setLmtAmt(lmtReplyAccSubPrd.getLmtAmt());
            // 授信起始日
            originalIqpAppAspl.setLmtStartDate(lmtReplyAccSubPrd.getStartDate());
            // 授信到期日
            originalIqpAppAspl.setLmtEndDate(lmtReplyAccSubPrd.getEndDate());
            // 生成新的业务流水号，并且标上业务变更标志，跟据业务要求，协议变更生成新的 协议编号
            originalIqpAppAspl.setSerno(newSerno);
            // 利率调整方式（默认固定利率）
            originalIqpAppAspl.setRateAdjMode(CommonConstance.RATE_ADJ_MODE_01);
            // LPR定价区间LPR_PRICE_INTERVAL_A1（默认一年期）
            originalIqpAppAspl.setLprPriceInterval(CommonConstance.LPR_PRICE_INTERVAL_A1);
            // 垫款利率（银承）核心默认18，这里只做展示
            originalIqpAppAspl.setPadRateYear(new BigDecimal(0.18));
            // 默认浮点数15点数
            originalIqpAppAspl.setRateFloatPoint(new BigDecimal(15));
            originalIqpAppAspl.setOrigiContNo(origiContNo);// 继承原先的协议编号
            originalIqpAppAspl.setChgFlag(CommonConstance.STD_ZB_YES_NO_1);// 是否变更
            originalIqpAppAspl.setIsRenew(CommonConstance.STD_ZB_YES_NO_1);//是否续签 0否
            originalIqpAppAspl.setApproveStatus(CommonConstance.APPROVE_STATUS_000);
            originalIqpAppAspl.setOprType(CmisCommonConstants.OPR_TYPE_ADD); // 操作类型
            originalIqpAppAspl.setCreateTime(DateUtils.getCurrDate());
            originalIqpAppAspl.setUpdateTime(DateUtils.getCurrDate());
            originalIqpAppAspl.setPkId(UUID.randomUUID().toString().replace("-",""));
            originalIqpAppAspl.setInputDate(openDay);
            iqpAppAsplMapper.insertSelective(originalIqpAppAspl);
            log.info("资产池协议变更，新增协议编号："+contNo);
            // 保证金账户(重新生成)
            BailAccInfo bailAccInfo = bailAccInfoService.selectInfoBySerno(serno);
            bailAccInfo.setSerno(newSerno);
            bailAccInfo.setPkId(StringUtils.getUUID());
            bailAccInfo.setBailRate(lmtReplyAccSubPrd.getBailPreRate().setScale(4,BigDecimal.ROUND_DOWN));
            bailAccInfo.setInputDate(openDay);
            bailAccInfo.setCreateTime(DateUtils.getCurrDate());
            bailAccInfo.setUpdateTime(DateUtils.getCurrDate());
            bailAccInfoService.insertSelective(bailAccInfo);
            log.info("资产池协议变更，新增保证金信息");
            // 担保合同业务关系表(资产池协议业务流水号唯一，业务合同和担保合同关系)
            GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelService.selectGrtGuarBizRstRelDataBySerno(serno);
            // 担保合同(重新生成) 根据原资产池协议的担保合同关系去寻找担保合同信息
            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
            String origiguarContNo = grtGuarCont.getGuarContNo();// 原资产池协议担保合同编号
            String guarContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, new HashMap<>());
            String guarPkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO,new HashMap<>());
            grtGuarCont.setGuarPkId(guarPkId);
            grtGuarCont.setGuarContNo(guarContNo);
            //担保合同金额
            grtGuarCont.setGuarAmt(originalIqpAppAspl.getLmtAmt());
            //担保起始日
            grtGuarCont.setGuarStartDate(originalIqpAppAspl.getStartDate());
            //担保终止日
            grtGuarCont.setGuarEndDate(originalIqpAppAspl.getEndDate());
            grtGuarCont.setBizLine("03");// 业务条线为对公 对公送03，零售送02，小微送01
            grtGuarCont.setGuarContState("101");// 默认生效
            grtGuarCont.setInputDate(openDay);
            grtGuarContService.insertSelective(grtGuarCont);
            log.info("资产池协议变更，担保合同新增");
            // 担保合同业务关系 (重新生成)
            grtGuarBizRstRel.setPkId(UUID.randomUUID().toString().replace("-",""));
            grtGuarBizRstRel.setContNo(contNo);
            grtGuarBizRstRel.setGuarContNo(guarContNo);
            grtGuarBizRstRel.setSerno(newSerno);
            grtGuarBizRstRel.setGuarPkId(guarPkId);// 担保合同流水号
            grtGuarBizRstRel.setInputDate(openDay);
            grtGuarBizRstRelService.insertSelective(grtGuarBizRstRel);

            // 平移押品  担保合同关系 1、获取原担保合同下所有的押品关系 GrtGuarContRel 2、新增
            QueryModel model =new QueryModel();
            model.addCondition("guarContNo",origiguarContNo);
            List<GrtGuarContRel> GrtGuarContRels = grtGuarContRelService.selectAll(model);
            if(CollectionUtils.nonEmpty(GrtGuarContRels)){
                GrtGuarContRels.forEach(e->{
                    e.setPkId(StringUtils.getUUID());
                    e.setGuarContNo(guarContNo);
                    e.setContNo(contNo);
                    e.setInputDate(openDay);
                    grtGuarContRelService.insertSelective(e);
                });
            }
            // 平移出入池记录（不平移）
            // 平移资产清单 1、获取原资产池协议下的所有资产
            QueryModel queryModel=new QueryModel();
            queryModel.addCondition("contNo",origiContNo);
            List<AsplAssetsList> asplAssetsLists = asplAssetsListService.selectAll(queryModel);
            if(CollectionUtils.nonEmpty(asplAssetsLists)){
                asplAssetsLists.forEach(e->{
                    e.setPkId(StringUtils.getUUID());
                    e.setContNo(contNo);
                    e.setInputDate(openDay);
                    asplAssetsListService.insertSelective(e);
                });
            }
            log.info("资产池协议变更成功");
        }catch (BizException e) {
            code = e.getErrorCode();
            msg = e.getMessage();
            log.info("资产池协议变更"+msg);
        }catch (Exception e) {
            code = "9999";
            msg = e.getMessage();
        }finally {
            return new ResultDto<IqpAppAspl>(originalIqpAppAspl).code(code).message(msg);
        }
    }
    /**
     * 根据资产池协议获取当前有效的资产池协议申请
     * @param  contNo
     * @return
     */
    public IqpAppAspl selectAvalByContNo(String contNo) {
       return iqpAppAsplMapper.selectAvalByContNo(contNo);
    }

    /**
     * 根据资产池协议流水号修改申请状态
     * @param  map
     * @return
     */
    @Transactional
    public Map changeasplApp(Map map) {
        Map<String,String> returnMap = new HashMap<String,String>();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        String pkId = "";
        String contNo = "";
        CtrAsplDetails ctrAsplDetails = null;
        try{
            serno = (String)map.get("serno");
            contNo = (String)map.get("contNo");
            pkId = (String)map.get("pkId");
            log.info("资产池协议变更-原资产协议复制新增，合同编号：" + contNo);
            ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
            // 资产池协议删除(shardkey)
            ctrAsplDetailsService.deleteByContNo(contNo);
            ctrAsplDetails.setSerno(serno);
            ctrAsplDetails.setPkId(UUID.randomUUID().toString().replace("-",""));
            ctrAsplDetailsService.insertSelective(ctrAsplDetails);
            if(iqpAppAsplMapper.updateStatus(serno,BizFlowConstant.WF_STATUS_997)>0){
                rtnMsg = "资产池业务变更成功";
            };
        }catch (Exception e) {
            log.error("生成资产池协议变更" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            returnMap.put("rtnCode", rtnCode);
            returnMap.put("rtnMsg", rtnMsg);
        }
        return returnMap;
    }

    public void handleBusinessDataAfterEnd(IqpAppAspl iqpAppAspl) {
        String serno = iqpAppAspl.getSerno();
          try{
            // 生成资产池协议
            CtrAsplDetails ctrAsplDetails = new CtrAsplDetails();
            BeanUtils.copyProperties(iqpAppAspl, ctrAsplDetails);
            ctrAsplDetails.setContStatus(CmisCommonConstants.CONT_STATUS_100);//资产池协议状态默认【未生效】
            ctrAsplDetailsService.insertSelective(ctrAsplDetails);
            iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            iqpAppAsplMapper.updateByPrimaryKey(iqpAppAspl);
            log.info("生成资产池协议" + serno + "成功");
        }catch (Exception e) {
            log.error("生成资产池协议" + serno + "异常", e);
            throw e;
        }
    }

    /**
     * 校验该资产池是否关联保证金账号，或结算账号
     * @param map
     * @return
     */
    public Map checkOutAsplAccNo(Map map) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            // 校验该笔资产池是否有保证金账号和结算账号
            serno = (String) map.get("serno");
            if(StringUtils.isEmpty(serno)){
                throw new BizException(null, EcbEnum.ECB010050.key,null,EcbEnum.ECB010050.value);
            }
            // 1、检验保证金账号
            String bailAccNo = bailAccInfoService.selectAccNoBySerno(serno);
            if(StringUtils.isEmpty(bailAccNo)){
                throw new BizException(null, EcbEnum.ECB020022.key,null,EcbEnum.ECB020022.value);
            }
            map.put("oprType", CommonConstance.OPR_TYPE_ADD);
            IqpAppAspl iqpAppAspl = iqpAppAsplMapper.queryIqpAppAsplDataByParams(map);
            if(iqpAppAspl == null){
                throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
            }
            // 2、检验结算账号
            String acctNo = iqpAppAspl.getAcctNo();
            if(StringUtils.isEmpty(acctNo)){
                throw new BizException(null, EcbEnum.ECB020023.key,null,EcbEnum.ECB020023.value);
            }

            log.info("资产池协议" + serno + "保证金账号和结算账号校验成功");
        }catch (Exception e) {
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }
    /**
     * 根据流水号更新
     * @param iqpAppAspl
     * @return
     */
    public int updateSelectiveBySerno(IqpAppAspl iqpAppAspl) {
        return iqpAppAsplMapper.updateSelectiveBySerno(iqpAppAspl);
    }

}
