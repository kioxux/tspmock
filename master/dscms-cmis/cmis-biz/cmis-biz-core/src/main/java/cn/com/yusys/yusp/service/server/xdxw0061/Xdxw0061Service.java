package cn.com.yusys.yusp.service.server.xdxw0061;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdxw0061.req.Xdxw0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0061.resp.Xdxw0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0061Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0061Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdxw0061.Xdxw0061Service.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    @Autowired
    private ICusClientService iCusClientService;


    /**
     * 通过无还本续贷调查表编号查询配偶核心客户号
     *
     * @param Xdxw0061DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0061DataRespDto xdxw0061Service(Xdxw0061DataReqDto Xdxw0061DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value);
        Xdxw0061DataRespDto xdxw0061DataRespDto = new Xdxw0061DataRespDto();
        String surveySerno = Xdxw0061DataReqDto.getIndgtSerno();//客户调查表编号
        try {
            String certCode = "";
            if (StringUtil.isNotEmpty(surveySerno)) {
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(surveySerno);
                if (lmtSurveyReportBasicInfo != null) {
                    certCode = lmtSurveyReportBasicInfo.getSpouseCertCode();
                    //通过客户证件号查询客户信息
                    logger.info("***************XDXW0061*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certCode));
                    CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certCode)).orElse(new CusBaseClientDto());
                    logger.info("***************XDXW0061*根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certCode), JSON.toJSONString(cusBaseClientDto));
                    //返回配偶客户号
                    xdxw0061DataRespDto.setCusNo(cusBaseClientDto.getCusId());
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value);
        return xdxw0061DataRespDto;
    }
}