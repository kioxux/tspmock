/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwyd
 * @类描述: rpt_spd_anys_jyxwyd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jyxwyd")
public class RptSpdAnysJyxwyd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 物业产权所有人 **/
	@Column(name = "PROPERTY_OWNER", unique = false, nullable = true, length = 80)
	private String propertyOwner;
	
	/** 物业合法经营方与产权所有人关系 **/
	@Column(name = "RBTLPATOOTP", unique = false, nullable = true, length = 65535)
	private String rbtlpatootp;
	
	/** 物业合法经营方委托经营协议情况 **/
	@Column(name = "TEOAOTLPO", unique = false, nullable = true, length = 65535)
	private String teoaotlpo;
	
	/** 物业地址 **/
	@Column(name = "PROPERTY_ADDRESS", unique = false, nullable = true, length = 65535)
	private String propertyAddress;
	
	/** 物业运营情况（承租人）、出租率、已使用期限 **/
	@Column(name = "POLRRSL", unique = false, nullable = true, length = 65535)
	private String polrrsl;
	
	/** 租金收入情况 **/
	@Column(name = "RENT_INCOME", unique = false, nullable = true, length = 65535)
	private String rentIncome;
	
	/** 融资用途 **/
	@Column(name = "FINANCING_PURPOSE", unique = false, nullable = true, length = 65535)
	private String financingPurpose;
	
	/** 抵押物评估价格房产面积 **/
	@Column(name = "MAPREA", unique = false, nullable = true, length = 40)
	private String maprea;
	
	/** 抵押物评估价格土地面积 **/
	@Column(name = "LAOMEP", unique = false, nullable = true, length = 40)
	private String laomep;
	
	/** 抵押物评估价格房产评估价 **/
	@Column(name = "MAPREAP", unique = false, nullable = true, length = 40)
	private String mapreap;
	
	/** 抵押物评估价格土地评估价 **/
	@Column(name = "MVPLVP", unique = false, nullable = true, length = 40)
	private String mvplvp;
	
	/** 贷款额度是否符合我行规定分析 **/
	@Column(name = "AOWTLLCTTPOTB", unique = false, nullable = true, length = 65535)
	private String aowtllcttpotb;
	
	/** 对租金收入是否足以覆盖历年还款本息情况进行分析： **/
	@Column(name = "AWTREIIETCTRPAIOPY", unique = false, nullable = true, length = 65535)
	private String awtreiietctrpaiopy;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param propertyOwner
	 */
	public void setPropertyOwner(String propertyOwner) {
		this.propertyOwner = propertyOwner;
	}
	
    /**
     * @return propertyOwner
     */
	public String getPropertyOwner() {
		return this.propertyOwner;
	}
	
	/**
	 * @param rbtlpatootp
	 */
	public void setRbtlpatootp(String rbtlpatootp) {
		this.rbtlpatootp = rbtlpatootp;
	}
	
    /**
     * @return rbtlpatootp
     */
	public String getRbtlpatootp() {
		return this.rbtlpatootp;
	}
	
	/**
	 * @param teoaotlpo
	 */
	public void setTeoaotlpo(String teoaotlpo) {
		this.teoaotlpo = teoaotlpo;
	}
	
    /**
     * @return teoaotlpo
     */
	public String getTeoaotlpo() {
		return this.teoaotlpo;
	}
	
	/**
	 * @param propertyAddress
	 */
	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}
	
    /**
     * @return propertyAddress
     */
	public String getPropertyAddress() {
		return this.propertyAddress;
	}
	
	/**
	 * @param polrrsl
	 */
	public void setPolrrsl(String polrrsl) {
		this.polrrsl = polrrsl;
	}
	
    /**
     * @return polrrsl
     */
	public String getPolrrsl() {
		return this.polrrsl;
	}
	
	/**
	 * @param rentIncome
	 */
	public void setRentIncome(String rentIncome) {
		this.rentIncome = rentIncome;
	}
	
    /**
     * @return rentIncome
     */
	public String getRentIncome() {
		return this.rentIncome;
	}
	
	/**
	 * @param financingPurpose
	 */
	public void setFinancingPurpose(String financingPurpose) {
		this.financingPurpose = financingPurpose;
	}
	
    /**
     * @return financingPurpose
     */
	public String getFinancingPurpose() {
		return this.financingPurpose;
	}
	
	/**
	 * @param maprea
	 */
	public void setMaprea(String maprea) {
		this.maprea = maprea;
	}
	
    /**
     * @return maprea
     */
	public String getMaprea() {
		return this.maprea;
	}
	
	/**
	 * @param laomep
	 */
	public void setLaomep(String laomep) {
		this.laomep = laomep;
	}
	
    /**
     * @return laomep
     */
	public String getLaomep() {
		return this.laomep;
	}
	
	/**
	 * @param mapreap
	 */
	public void setMapreap(String mapreap) {
		this.mapreap = mapreap;
	}
	
    /**
     * @return mapreap
     */
	public String getMapreap() {
		return this.mapreap;
	}
	
	/**
	 * @param mvplvp
	 */
	public void setMvplvp(String mvplvp) {
		this.mvplvp = mvplvp;
	}
	
    /**
     * @return mvplvp
     */
	public String getMvplvp() {
		return this.mvplvp;
	}
	
	/**
	 * @param aowtllcttpotb
	 */
	public void setAowtllcttpotb(String aowtllcttpotb) {
		this.aowtllcttpotb = aowtllcttpotb;
	}
	
    /**
     * @return aowtllcttpotb
     */
	public String getAowtllcttpotb() {
		return this.aowtllcttpotb;
	}
	
	/**
	 * @param awtreiietctrpaiopy
	 */
	public void setAwtreiietctrpaiopy(String awtreiietctrpaiopy) {
		this.awtreiietctrpaiopy = awtreiietctrpaiopy;
	}
	
    /**
     * @return awtreiietctrpaiopy
     */
	public String getAwtreiietctrpaiopy() {
		return this.awtreiietctrpaiopy;
	}


}