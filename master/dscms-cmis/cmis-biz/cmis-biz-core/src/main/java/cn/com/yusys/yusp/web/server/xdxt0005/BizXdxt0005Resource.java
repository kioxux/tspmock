package cn.com.yusys.yusp.web.server.xdxt0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdxt0005.Xdxt0005Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.dto.server.xdxt0005.req.Xdxt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.resp.Xdxt0005DataRespDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户经理所在分部编号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0005:查询客户经理所在分部编号")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0005Resource.class);

    @Autowired
    private Xdxt0005Service xdxt0005Service;
    /**
     * 交易码：xdxt0005
     * 交易描述：查询客户经理所在分部编号
     *
     * @param xdxt0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户经理所在分部编号")
    @PostMapping("/xdxt0005")
    protected @ResponseBody
    ResultDto<Xdxt0005DataRespDto> xdxt0005(@Validated @RequestBody Xdxt0005DataReqDto xdxt0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataReqDto));
        Xdxt0005DataRespDto xdxt0005DataRespDto = new Xdxt0005DataRespDto();// 响应Dto:查询客户经理所在分部编号
        ResultDto<Xdxt0005DataRespDto> xdxt0005DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0005DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataReqDto));
            xdxt0005DataRespDto = xdxt0005Service.getXdxt0005(xdxt0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataReqDto));
            // 封装xdxt0005DataResultDto中正确的返回码和返回信息
            xdxt0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, e.getMessage());
            // 封装xdxt0005DataResultDto中异常返回码和返回信息
            xdxt0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0005DataRespDto到xdxt0005DataResultDto中
        xdxt0005DataResultDto.setData(xdxt0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataResultDto));
        return xdxt0005DataResultDto;
    }
}
