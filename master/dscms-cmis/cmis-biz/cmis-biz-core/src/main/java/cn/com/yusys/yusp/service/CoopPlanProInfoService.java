/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;

import cn.com.yusys.yusp.domain.BusiImageRelInfo;
import cn.com.yusys.yusp.domain.CoopPlanApp;
import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import cn.com.yusys.yusp.repository.mapper.BusiImageRelInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAppMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPlanProInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPlanProInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanProInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-14 15:31:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanProInfoService {

    @Autowired
    private CoopPlanProInfoMapper coopPlanProInfoMapper;
    @Autowired
    private CoopPlanAppMapper coopPlanAppMapper;
    @Autowired
    private BusiImageRelInfoMapper busiImageRelInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPlanProInfo selectByPrimaryKey(String pkId) {
        return coopPlanProInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPlanProInfoDto> selectAll(QueryModel model) {
        List<CoopPlanProInfoDto> records = (List<CoopPlanProInfoDto>) coopPlanProInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPlanProInfoDto> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanProInfoDto> list = coopPlanProInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopPlanProInfoDto> queryForAcc(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanProInfoDto> list = coopPlanProInfoMapper.queryForAcc(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BigDecimal insert(CoopPlanProInfo record) {
        coopPlanProInfoMapper.insert(record);
        //每个项目独立一份授信资料
        BusiImageRelInfo busiImageRelInfo = new BusiImageRelInfo();
        busiImageRelInfo.setSerno(record.getSerno());
        busiImageRelInfo.setImageDesc("授信资料-" + record.getProName());
        busiImageRelInfo.setImageNo(record.getProNo());
        busiImageRelInfo.setTopOutsystemCode("LPHZYX;SXJBZL;JKRZL;DBZL;XMLZL;SXLLPF;XD_FZHYXCL;ZHSXPF");
        busiImageRelInfo.setKeywordType("businessid");
        busiImageRelInfo.setAuthority("import;insert;download;scan;delImg");
        busiImageRelInfo.setImageOrder(2);
        busiImageRelInfo.setPkId(null);
        busiImageRelInfoMapper.insert(busiImageRelInfo);
        return updateTotlCoopLmtAmt(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPlanProInfo record) {
        return coopPlanProInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BigDecimal update(CoopPlanProInfo record) {
        coopPlanProInfoMapper.updateByPrimaryKey(record);
        return updateTotlCoopLmtAmt(record);
    }

    public BigDecimal updateTotlCoopLmtAmt(CoopPlanProInfo record) {
        CoopPlanApp coopPlanApp = new CoopPlanApp();
        coopPlanApp.setSerno(record.getSerno());
        BigDecimal sumProLmt = new BigDecimal(coopPlanAppMapper.sumProLmt(coopPlanApp));
        coopPlanApp.setTotlCoopLmtAmt(sumProLmt);
        coopPlanAppMapper.updateTotalAmt(coopPlanApp);
        coopPlanApp = coopPlanAppMapper.selectByPrimaryKey(coopPlanApp.getSerno());
        return coopPlanApp.getTotlCoopLmtAmt();
    }
    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPlanProInfo record) {
        return coopPlanProInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BigDecimal deleteByPrimaryKey(String pkId) {
        CoopPlanProInfo coopPlanProInfo = coopPlanProInfoMapper.selectByPrimaryKey(pkId);
        coopPlanProInfoMapper.deleteByPrimaryKey(pkId);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", coopPlanProInfo.getSerno());
        queryModel.addCondition("imageNo", coopPlanProInfo.getProNo());
        List<BusiImageRelInfo> busiImageRelInfos =  busiImageRelInfoMapper.selectByModel(queryModel);
        if (busiImageRelInfos != null && busiImageRelInfos.size()>0) {
            busiImageRelInfoMapper.deleteByPrimaryKey(busiImageRelInfos.get(0).getPkId());
        }
        return updateTotlCoopLmtAmt(coopPlanProInfo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPlanProInfoMapper.deleteByIds(ids);
    }
}
