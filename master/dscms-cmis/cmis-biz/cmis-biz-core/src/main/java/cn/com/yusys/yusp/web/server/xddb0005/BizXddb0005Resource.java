package cn.com.yusys.yusp.web.server.xddb0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0005.req.ReqList;
import cn.com.yusys.yusp.dto.server.xddb0005.req.Xddb0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0005.resp.Xddb0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 接口处理类:重估任务分配同步
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0005:重估任务分配同步")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0005Resource.class);

    /**
     * 交易码：xddb0005
     * 交易描述：重估任务分配同步
     *
     * @param xddb0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("重估任务分配同步")
    @PostMapping("/xddb0005")
    protected @ResponseBody
    ResultDto<Xddb0005DataRespDto> xddb0005(@Validated @RequestBody Xddb0005DataReqDto xddb0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005DataReqDto));
        Xddb0005DataRespDto xddb0005DataRespDto = new Xddb0005DataRespDto();// 响应Dto:重估任务分配同步
        ResultDto<Xddb0005DataRespDto> xddb0005DataResultDto = new ResultDto<>();
        try {
            BigDecimal listnm = xddb0005DataReqDto.getListnm();//循环列表记录数
            List<ReqList> reqList = xddb0005DataReqDto.getReqList();
            String gagtyp = reqList.get(0).getGagtyp();//押品分类
            String guarid = reqList.get(0).getGuarid();//押品统一编号
            String mangid = reqList.get(0).getMangid();//客户经理编号
            String orgno = reqList.get(0).getOrgno();//所属机构
            String status = reqList.get(0).getStatus();//任务状态
            String stime = reqList.get(0).getStime();//生成任务时间
            String mark = reqList.get(0).getMark();//任务描述
            // 从xddb0005DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddb0005DataRespDto对象开始
            xddb0005DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xddb0005DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xddb0005DataRespDto对象结束
            // 封装xddb0005DataResultDto中正确的返回码和返回信息
            xddb0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, e.getMessage());
            // 封装xddb0005DataResultDto中异常返回码和返回信息
            // TODO EpbEnum.EPB099999 待调整 开始
            xddb0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EpbEnum.EPB099999 待调整  结束
        }
        // 封装xddb0005DataRespDto到xddb0005DataResultDto中
        xddb0005DataResultDto.setData(xddb0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005DataRespDto));
        return xddb0005DataResultDto;
    }
}
