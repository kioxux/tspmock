/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpGuarChgApp;
import cn.com.yusys.yusp.service.IqpGuarChgAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarChgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-20 16:50:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpguarchgapp")
public class IqpGuarChgAppResource {
    @Autowired
    private IqpGuarChgAppService iqpGuarChgAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpGuarChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpGuarChgApp> list = iqpGuarChgAppService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpGuarChgApp>> index(@RequestBody QueryModel queryModel) {
        List<IqpGuarChgApp> list = iqpGuarChgAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<IqpGuarChgApp> show(@PathVariable("serno") String serno) {
        IqpGuarChgApp iqpGuarChgApp = iqpGuarChgAppService.selectByPrimaryKey(serno);
        return new ResultDto<>(iqpGuarChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpGuarChgApp> create(@RequestBody IqpGuarChgApp iqpGuarChgApp) throws URISyntaxException {
        iqpGuarChgAppService.insertSelective(iqpGuarChgApp);
        return new ResultDto<>(iqpGuarChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpGuarChgApp iqpGuarChgApp) throws URISyntaxException {
        int result = iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpGuarChgAppService.deleteByPrimaryKey(serno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpGuarChgAppService.deleteByIds(ids);
        return new ResultDto<>(result);
    }
}
