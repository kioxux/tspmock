/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.dto.AccAccpDto;
import cn.com.yusys.yusp.service.AccAccpService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accaccp")
public class AccAccpResource {
    @Autowired
    private AccAccpService accAccpService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccAccp>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccAccp> list = accAccpService.selectAll(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccAccp>> index(QueryModel queryModel) {
        List<AccAccp> list = accAccpService.selectByModel(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccAccp> show(@PathVariable("pkId") String pkId) {
        AccAccp accAccp = accAccpService.selectByPrimaryKey(pkId);
        return new ResultDto<AccAccp>(accAccp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccAccp> create(@RequestBody AccAccp accAccp) throws URISyntaxException {
        accAccpService.insert(accAccp);
        return new ResultDto<AccAccp>(accAccp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccAccp accAccp) throws URISyntaxException {
        int result = accAccpService.update(accAccp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accAccpService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accAccpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("合同下关联贷款台账页查询")
    @PostMapping("/selectaccloanshow")
    protected ResultDto<List<AccAccp>> selectAccLoanShow(@RequestBody QueryModel queryModel) {
        List<AccAccp> list = accAccpService.selectAccLoanShow(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("偿还借据关联贷款台账页查询")
    @PostMapping("/selectaccaccprepay")
    protected ResultDto<List<AccAccp>> selectAccAccpRepay(@RequestBody QueryModel queryModel) {
        List<AccAccp> list = accAccpService.selectAccAccpRepay(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询
     * @算法描述:
     * liuquan
     */
    @ApiOperation("分页查询银承台账列表信息")
    @PostMapping("/queryAll")
    protected ResultDto<List<AccAccp>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccAccp> list = accAccpService.selectByModel(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }

    /**
     * @函数名称:queryByCoreBillNo
     * @函数描述:根据根据核心银承编号回显银承台账信息
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @ApiOperation("根据核心银承编号回显银承台账信息")
    @PostMapping("/queryByCoreBillNo")
    protected ResultDto<AccAccpDto> queryByCoreBillNo(@RequestBody AccAccp accAccp) {
        AccAccpDto accAccpDto = accAccpService.queryByCoreBillNo(accAccp);
        return new ResultDto<AccAccpDto>(accAccpDto);
    }

    /**
     * 异步下载银承台账列表
     */
    @PostMapping("/exportAccAccp")
    public ResultDto<ProgressDto> asyncExportAccAccp(@RequestBody QueryModel model) {
        ProgressDto progressDto = accAccpService.asyncExportAccAccp(model);
        return ResultDto.success(progressDto);
    }
    /**
     * @函数名称: querymodelByCondition
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据查询条件查询台账信息并返回
     * @算法描述:
     * liuquan
     */
    @ApiOperation("分页查询银承台账列表信息")
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccAccp>> querymodelByCondition(@RequestBody QueryModel queryModel) {
        List<AccAccp> list = accAccpService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccAccp>>(list);
    }
    /**
     * @函数名称: doSynchronize
     * @函数描述:根据条件同步银承台账信息并返回
     * @参数与返回说明:
     * @param accaccp
     * @算法描述:
     * liuquan
     */
    @ApiOperation(" 根据条件同步银承台账信息")
    @PostMapping("/doSynchronize")
    public ResultDto<Map<String,String>> doSynchronize(@RequestBody AccAccp accaccp) {
        return accAccpService.doSynchronize(accaccp);
    }
}
