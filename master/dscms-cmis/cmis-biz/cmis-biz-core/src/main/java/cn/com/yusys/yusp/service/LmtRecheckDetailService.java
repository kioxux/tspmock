/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.jcraft.jsch.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtRecheckDetailMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRecheckDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 19:41:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRecheckDetailService {
    private static final Logger log = LoggerFactory.getLogger(LmtAppSubPrdService.class);

    @Resource
    private LmtRecheckDetailMapper lmtRecheckDetailMapper;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;

    @Autowired
	private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRecheckDetail selectByPrimaryKey(String pkId) {
        return lmtRecheckDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRecheckDetail> selectAll(QueryModel model) {
        List<LmtRecheckDetail> records = (List<LmtRecheckDetail>) lmtRecheckDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRecheckDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRecheckDetail> list = lmtRecheckDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRecheckDetail record) {
        return lmtRecheckDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRecheckDetail record) {
        return lmtRecheckDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtRecheckDetail record) {
        return lmtRecheckDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRecheckDetail record) {
        return lmtRecheckDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtRecheckDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtRecheckDetailMapper.deleteByIds(ids);
    }

    /**
     * 根據申請流水號查詢授信情況
     * @param serno
     * @return
     */
    public List<LmtAppSubPrd> queryDetailByLmtSerno(String serno) {
        return lmtAppSubPrdService.queryAllLmtAppSubPrdBySerno(serno);
    }

    /**
     * 根据申请流水号查询授信情况(包含目前用信净额)
     * @param serno
     * @return
     */
    public List<Map> queryLmtAndLoanConditionByLmtSerno(String serno) {
        List<Map> returnList = new ArrayList<>();
        List<LmtAppSubPrd> list = lmtAppSubPrdService.queryAllLmtAppSubPrdBySerno(serno);
        if(CollectionUtils.nonEmpty(list)){
            User userInfo = SessionUtils.getUserInformation();
            String instuCode = "";
            if(userInfo.getOrg().getCode().startsWith("80")){
                // 寿光
                instuCode = CmisCommonConstants.INSTUCDE_002;
            }else if(userInfo.getOrg().getCode().startsWith("81")){
                // 东海
                instuCode = CmisCommonConstants.INSTUCDE_003;
            }else{
                // 张家港
                instuCode = CmisCommonConstants.INSTUCDE_001;
            }
            for(LmtAppSubPrd lmtAppSubPrd : list){
                Map map = new HashMap();
                map.put("lmtBizTypeName",lmtAppSubPrd.getLmtBizTypeName());// 授信品种名称
                map.put("lmtBizTypeProp",lmtAppSubPrd.getLmtBizTypeProp());// 产品类型属性
                map.put("guarMode",lmtAppSubPrd.getGuarMode());// 担保方式
                map.put("lmtAmt",lmtAppSubPrd.getLmtAmt());// 授信净额
                // 目前用信净额 contAmt
                CmisLmt0059ReqDto cmisLmt0059ReqDto = new CmisLmt0059ReqDto();
                cmisLmt0059ReqDto.setInstuCde(instuCode);
                cmisLmt0059ReqDto.setApprSubSerno(lmtAppSubPrd.getSubPrdSerno());
                log.info("获取分项品种编号对应的用信净额,请求报文:"+JSON.toJSONString(cmisLmt0059ReqDto));
                CmisLmt0059RespDto cmisLmt0059RespDto = cmisLmtClientService.cmislmt0059(cmisLmt0059ReqDto).getData();
                log.info("获取分项品种编号对应的用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                if(cmisLmt0059RespDto.getErrorCode().equals(EcbEnum.ECB010000)){
                    if(lmtAppSubPrd.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)){
                        log.info("低风险二级分项用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                        map.put("contAmt",cmisLmt0059RespDto.getSubLoanBal());
                    }else{
                        log.info("非低风险二级分项用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                        map.put("contAmt",cmisLmt0059RespDto.getSubSpacLoanBal());
                    }
                }else{
                    map.put("contAmt","N/A");
                }
                returnList.add(map);
            }
        }
        return returnList;
    }

    /**
     * 根据申请流水号获取近三期融资情况
     * @param serno
     * @return
     */
    public List<RptCptlSituCorp> queryFinancSituationByLmtSerno(String serno) {
        return rptCptlSituCorpService.queryFinancSituationByLmtSerno(serno);
    }

    /**
     * 根据申请流水号查询抵押担保情况
     * @param serno
     * @return
     */
    public List<RptBasicInfoPersFamilyAssets> queryGuarByLmtSerno(String serno) {
        return rptBasicInfoPersFamilyAssetsService.queryGuarByLmtSerno(serno);
    }

    /**
     * 根据申请流水号查询授信复审申请从表
     * @param lmtSerno
     * @return
     */
    public LmtRecheckDetail selectByLmtSerno(String lmtSerno) {
        // 有则返回,无则插入
        LmtRecheckDetail lmtRecheckDetail = lmtRecheckDetailMapper.selectByLmtSerno(lmtSerno);
        User userInfo = SessionUtils.getUserInformation();
        if(lmtRecheckDetail == null){
            lmtRecheckDetail = new LmtRecheckDetail();
            lmtRecheckDetail.setPkId(UUID.randomUUID().toString());
            lmtRecheckDetail.setLmtSerno(lmtSerno);
            lmtRecheckDetail.setOprType(CmisCommonConstants.OP_TYPE_01);
            lmtRecheckDetail.setInputId(userInfo.getUserId());
            lmtRecheckDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtRecheckDetail.setInputDate(DateUtils.getCurrDateStr());
            lmtRecheckDetail.setUpdId(userInfo.getUserId());
            lmtRecheckDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtRecheckDetail.setUpdDate(DateUtils.getCurrDateStr());
            lmtRecheckDetail.setCreateTime(DateUtils.getCurrTimestamp());
            insert(lmtRecheckDetail);
        }
        return lmtRecheckDetail;
    }

    /**
     * @函数名称:queryEnterpriseChangeData
     * @函数描述:查询复审表企业经营变动情况表数据
     * @参数与返回说明:
     * @算法描述:
     */

    public List<Map> queryEnterpriseChangeData(String serno,String refTable) {
        List<Map> returnMapList = new ArrayList<>();
        LmtRecheckDetail lmtRecheckDetail = lmtRecheckDetailMapper.selectByLmtSerno(serno);
        if(lmtRecheckDetail != null){
            if(CmisCommonConstants.STD_SX_FSB_TABLE_02.equals(refTable)){
                // 企业经营变动情况
                // 开票销售/缴纳增值税/缴纳所得税/税务报表净利润/自制报表销售/自制报表净利润
                Map map1 = new HashMap();
                map1.put("serno",serno);
                map1.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_01); // 开票销售
                map1.put("lastSecondValue",lmtRecheckDetail.getRecSecInvoiceSales()); // 开票第二年
                map1.put("lastFirstValue",lmtRecheckDetail.getRecFirInvoiceSales()); // 开票第一年
                map1.put("nowValue",lmtRecheckDetail.getCurrentInvoiceSales()); // 开票当前年
                returnMapList.add(map1);
                Map map2 = new HashMap();
                map2.put("serno",serno);
                map2.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_02); // 缴纳增值税
                map2.put("lastSecondValue",lmtRecheckDetail.getRecSecVat()); // 缴纳增值税第二年
                map2.put("lastFirstValue",lmtRecheckDetail.getRecFirVat()); // 缴纳增值税第一年
                map2.put("nowValue",lmtRecheckDetail.getCurrentVat()); // 缴纳增值税当前年
                returnMapList.add(map2);
                Map map3 = new HashMap();
                map3.put("serno",serno);
                map3.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_03); // 缴纳所得税
                map3.put("lastSecondValue",lmtRecheckDetail.getRecSecIncomeTax()); // 缴纳所得税第二年
                map3.put("lastFirstValue",lmtRecheckDetail.getRecFirIncomeTax()); // 缴纳所得税第一年
                map3.put("nowValue",lmtRecheckDetail.getCurrentIncomeTax()); // 缴纳所得税当前年
                returnMapList.add(map3);
                Map map4 = new HashMap();
                map4.put("serno",serno);
                map4.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_04); // 税务报表净利润
                map4.put("lastSecondValue",lmtRecheckDetail.getRecSecNetProfit()); // 税务报表净利润第二年
                map4.put("lastFirstValue",lmtRecheckDetail.getRecFirNetprofit()); // 税务报表净利润第一年
                map4.put("nowValue",lmtRecheckDetail.getCurrentNetProfit()); // 税务报表净利润当前年
                returnMapList.add(map4);
                Map map5 = new HashMap();
                map5.put("serno",serno);
                map5.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_05); // 自制报表销售
                map5.put("lastSecondValue",lmtRecheckDetail.getRecSecSelfRepSales()); // 自制报表销售第二年
                map5.put("lastFirstValue",lmtRecheckDetail.getRecFirSelfRepSales()); // 自制报表销售第一年
                map5.put("nowValue",lmtRecheckDetail.getCurrentSelfRepSales()); // 自制报表销售当前年
                returnMapList.add(map5);
                Map map6 = new HashMap();
                map6.put("serno",serno);
                map6.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_06); // 自制报表净利润
                map6.put("lastSecondValue",lmtRecheckDetail.getRecSecSelfRepProfit()); // 自制报表净利润第二年
                map6.put("lastFirstValue",lmtRecheckDetail.getRecFirSelfRepProfit()); // 自制报表净利润第一年
                map6.put("nowValue",lmtRecheckDetail.getCurrentSelfRepProfit()); // 自制报表净利润当前年
                returnMapList.add(map6);
            }else if(CmisCommonConstants.STD_SX_FSB_TABLE_03.equals(refTable)){
                // 近三期总融资情况
                List<RptCptlSituCorp> rptCptlSituCorps = queryFinancSituationByLmtSerno(serno);
                if(CollectionUtils.nonEmpty(rptCptlSituCorps)){
                    for(RptCptlSituCorp rptCptlSituCorp:rptCptlSituCorps){
                        Map map1 = new HashMap();
                        map1.put("pkId",rptCptlSituCorp.getPkId());
                        map1.put("serno",rptCptlSituCorp.getSerno());
                        map1.put("belongBank",rptCptlSituCorp.getBelongBank());
                        map1.put("lastTwoYearAmt",rptCptlSituCorp.getLastTwoYearAmt());
                        map1.put("lastYearAmt",rptCptlSituCorp.getLastYearAmt());
                        map1.put("curMonthAmt",rptCptlSituCorp.getCurMonthAmt());
                        map1.put("curMonthGuarMode",rptCptlSituCorp.getCurMonthGuarMode());
                        returnMapList.add(map1);
                    }
                }
            }else if(CmisCommonConstants.STD_SX_FSB_TABLE_04.equals(refTable)){
                // 销售及利润情况
                // 开票销售/税务报表净利润/自制报表销售/自制报表净利润/净资产情况
                Map map1 = new HashMap();
                map1.put("serno",serno);
                map1.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_01); // 开票销售
                map1.put("lastSecondValue",lmtRecheckDetail.getGeneralComRecSecInvoiceSales()); // 开票第二年
                map1.put("lastFirstValue",lmtRecheckDetail.getGeneralComRecFirInvoiceSales()); // 开票第一年
                map1.put("nowValue",lmtRecheckDetail.getGeneralComCurrentInvoiceSales()); // 开票当前年
                returnMapList.add(map1);
                Map map2 = new HashMap();
                map2.put("serno",serno);
                map2.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_04); // 税务报表净利润
                map2.put("lastSecondValue",lmtRecheckDetail.getGeneralComRecSecNetProfit()); // 税务报表净利润第二年
                map2.put("lastFirstValue",lmtRecheckDetail.getGeneralComRecFirNetProfit()); // 税务报表净利润第一年
                map2.put("nowValue",lmtRecheckDetail.getGeneralComCurrentNetProfit()); // 税务报表净利润当前年
                returnMapList.add(map2);
                Map map3 = new HashMap();
                map3.put("serno",serno);
                map3.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_05); // 自制报表销售
                map3.put("lastSecondValue",lmtRecheckDetail.getGeneralComRecSecSelfRepSales()); // 自制报表销售第二年
                map3.put("lastFirstValue",lmtRecheckDetail.getGeneralComRecFirSelfRepSales()); // 自制报表销售第一年
                map3.put("nowValue",lmtRecheckDetail.getGeneralComCurrentSelfRepSales()); // 自制报表销售当前年
                returnMapList.add(map3);
                Map map4 = new HashMap();
                map4.put("serno",serno);
                map4.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_06); // 自制报表净利润
                map4.put("lastSecondValue",lmtRecheckDetail.getGeneralComRecSecSelfRepProfit()); // 自制报表净利润第二年
                map4.put("lastFirstValue",lmtRecheckDetail.getGeneralComRecFirSelfRepProfit()); // 自制报表净利润第一年
                map4.put("nowValue",lmtRecheckDetail.getGeneralComCurrentSelfRepProfit()); // 自制报表净利润当前年
                returnMapList.add(map4);
                Map map5 = new HashMap();
                map5.put("serno",serno);
                map5.put("itemName",CmisCommonConstants.STD_SX_FSB_ITEMNAME_07); // 净资产情况
                map5.put("lastSecondValue",lmtRecheckDetail.getGeneralComRecSecNetAsset()); // 净资产情况第二年
                map5.put("lastFirstValue",lmtRecheckDetail.getGeneralComRecFirNetAsset()); // 净资产情况第一年
                map5.put("nowValue",lmtRecheckDetail.getGeneralComCurrentNetAsset()); // 净资产情况当前年
                returnMapList.add(map5);
            }else if(CmisCommonConstants.STD_SX_FSB_TABLE_05.equals(refTable)){
                List<Map> guarBaseInfoMapList = guarBaseInfoService.queryGuarBaseInfoByLmtSerno(serno);
                if(CollectionUtils.nonEmpty(guarBaseInfoMapList)){
                    for(Map map : guarBaseInfoMapList){
                        Map map1 = new HashMap();
                        map1.put("subSerno",map.get("subSerno"));
                        map1.put("guarNo",map.get("guarNo"));
                        map1.put("guarCusId",map.get("guarCusId"));
                        map1.put("guarCusName",map.get("guarCusName")); // 所有人 GUAR_CUS_ID GUAR_CUS_NAME
                        map1.put("pldLocation",map.get("pldLocation")); // 位置 PLD_LOCATION
                        map1.put("guarTypeCd",map.get("guarTypeCd")); // 押品类型 GUAR_TYPE_CD
                        map1.put("squ",map.get("squ")); // 面积 SQU
                        map1.put("evalAmt",map.get("evalAmt"));// 原评估价值 EVAL_AMT
                        map1.put("maxMortagageAmt",map.get("maxMortagageAmt")); // 最新评估价值 MAX_MORTAGAGE_AMT
                        map1.put("lmtAmt",map.get("lmtAmt"));// 贷款金额 LMT_AMT
                        map1.put("tenancyCirce",map.get("tenancyCirce"));// 出租/自用 TENANCY_CIRCE
                        returnMapList.add(map1);
                    }
                }
            }else{
                throw BizException.error(null, EcbEnum.ECB010001.key,EcbEnum.ECB010001.value+"传入参数1:"+ JSON.toJSONString(serno)+"传入参数1:"+ JSON.toJSONString(refTable));
            }
        }
        return returnMapList;
    }

    /**
     * @函数名称:saveRefTableData
     * @函数描述:保存复审表页面表格数据
     * @参数与返回说明:
     * @算法描述:
     */

    public int saveRefTableData(Map<String,String> map) {
        int returnNum = 0;
        try{
            LmtRecheckDetail lmtRecheckDetail = lmtRecheckDetailMapper.selectByLmtSerno(map.get("serno"));
            if(lmtRecheckDetail != null){
                if(CmisCommonConstants.STD_SX_FSB_TABLE_02.equals(map.get("refTable"))){
                    if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_01)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecInvoiceSales(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirInvoiceSales(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentInvoiceSales(new BigDecimal(map.get("nowValue")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_02)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecVat(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirVat(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentVat(new BigDecimal(map.get("nowValue")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_03)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecIncomeTax(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirIncomeTax(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentIncomeTax(new BigDecimal(map.get("nowValue")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_04)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecNetProfit(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirNetprofit(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentNetProfit(new BigDecimal(map.get("nowValue")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_05)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecSelfRepSales(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirSelfRepSales(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentSelfRepSales(new BigDecimal(map.get("nowValue")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_06)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setRecSecSelfRepProfit(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastFirstValue"))){
                            lmtRecheckDetail.setRecFirSelfRepProfit(new BigDecimal(map.get("lastFirstValue")));
                        }
                        if(StringUtils.nonBlank(map.get("nowValue"))){
                            lmtRecheckDetail.setCurrentSelfRepProfit(new BigDecimal(map.get("nowValue")));
                        }
                    }
                    returnNum = update(lmtRecheckDetail);
                }else if(CmisCommonConstants.STD_SX_FSB_TABLE_03.equals(map.get("refTable"))){
                    RptCptlSituCorp rptCptlSituCorp = rptCptlSituCorpService.selectByPrimaryKey(map.get("pkId"));
                    // 有则更新 无则插入
                    if(rptCptlSituCorp != null ){
                        rptCptlSituCorp.setBelongBank(map.get("belongBank"));
                        if(StringUtils.nonBlank(map.get("lastTwoYearAmt"))){
                            rptCptlSituCorp.setLastTwoYearAmt(new BigDecimal(map.get("lastTwoYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            rptCptlSituCorp.setLastYearAmt(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            rptCptlSituCorp.setCurMonthAmt(new BigDecimal(map.get("curMonthAmt")));
                        }
                        rptCptlSituCorp.setCurMonthGuarMode(map.get("curMonthGuarMode"));
                        returnNum = rptCptlSituCorpService.update(rptCptlSituCorp);
                    }else{
                        RptCptlSituCorp rptCptlSituCorp1 = new RptCptlSituCorp();
                        rptCptlSituCorp1.setBelongBank(map.get("belongBank"));
                        if(StringUtils.nonBlank(map.get("lastTwoYearAmt"))){
                            rptCptlSituCorp1.setLastTwoYearAmt(new BigDecimal(map.get("lastTwoYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            rptCptlSituCorp1.setLastYearAmt(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            rptCptlSituCorp1.setCurMonthAmt(new BigDecimal(map.get("curMonthAmt")));
                        }
                        rptCptlSituCorp1.setCurMonthGuarMode(map.get("curMonthGuarMode"));
                        rptCptlSituCorp1.setPkId(UUID.randomUUID().toString());
                        rptCptlSituCorp1.setSerno(map.get("serno"));
                        returnNum = rptCptlSituCorpService.insert(rptCptlSituCorp1);
                    }
                }else if(CmisCommonConstants.STD_SX_FSB_TABLE_04.equals(map.get("refTable"))){
                    if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_01)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setGeneralComRecSecInvoiceSales(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            lmtRecheckDetail.setGeneralComRecFirInvoiceSales(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            lmtRecheckDetail.setGeneralComCurrentInvoiceSales(new BigDecimal(map.get("curMonthAmt")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_04)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setGeneralComRecSecNetProfit(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            lmtRecheckDetail.setGeneralComRecFirNetProfit(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            lmtRecheckDetail.setGeneralComCurrentNetProfit(new BigDecimal(map.get("curMonthAmt")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_05)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setGeneralComRecSecSelfRepSales(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            lmtRecheckDetail.setGeneralComRecFirSelfRepSales(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            lmtRecheckDetail.setGeneralComCurrentSelfRepSales(new BigDecimal(map.get("curMonthAmt")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_06)){
                        if(StringUtils.nonBlank(map.get("lastSecondValue"))){
                            lmtRecheckDetail.setGeneralComRecSecSelfRepProfit(new BigDecimal(map.get("lastSecondValue")));
                        }
                        if(StringUtils.nonBlank(map.get("lastYearAmt"))){
                            lmtRecheckDetail.setGeneralComRecFirSelfRepProfit(new BigDecimal(map.get("lastYearAmt")));
                        }
                        if(StringUtils.nonBlank(map.get("curMonthAmt"))){
                            lmtRecheckDetail.setGeneralComCurrentSelfRepProfit(new BigDecimal(map.get("curMonthAmt")));
                        }
                    }else if(map.get("itemName").equals(CmisCommonConstants.STD_SX_FSB_ITEMNAME_07)){
                        lmtRecheckDetail.setGeneralComRecSecNetAsset(map.get("lastSecondValue"));
                        lmtRecheckDetail.setGeneralComRecFirNetAsset(map.get("lastFirstValue"));
                        lmtRecheckDetail.setGeneralComCurrentNetAsset(map.get("nowValue"));
                    }
                    returnNum = update(lmtRecheckDetail);
                }else if(CmisCommonConstants.STD_SX_FSB_TABLE_05.equals(map.get("refTable"))){
                    // 保存授信金额 以及 出租自用信息
                    LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(map.get("subSerno"));
                    if(lmtAppSub!=null){
                        lmtAppSub.setLmtAmt(new BigDecimal(map.get("lmtAmt")));
                        returnNum = lmtAppSubService.update(lmtAppSub);
                        GuarBaseInfo guarBaseInfo = guarBaseInfoService.getGuarBaseInfoByGuarNo(map.get("guarNo"));
                        guarBaseInfo.setTenancyCirce(map.get("tenancyCirce"));
                        returnNum += guarBaseInfoService.update(guarBaseInfo);
                    }
                }else{
                    throw BizException.error(null, EcbEnum.ECB010001.key,EcbEnum.ECB010001.value+"传入参数:"+ JSON.toJSONString(map));
                }
            }else{
                throw BizException.error(null, EcbEnum.ECB010017.key,EcbEnum.ECB010017.value+"传入参数:"+ JSON.toJSONString(map));
            }
        }catch (Exception e){
            throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        return returnNum;
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据集团上期申请流水号查询上期授信情况
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map> queryLatestInfoByGrpSerno(Map<String,String> map) {
        List<Map> latestLmtInfoDtos = new ArrayList<>();
        try{
            String grpSerno =  map.get("grpSerno");
            String originGrpSerno =  map.get("originGrpSerno");
            String originGrpReplySerno =  map.get("originGrpReplySerno");
            if(StringUtils.isBlank(originGrpReplySerno)){
                throw BizException.error(null, EcbEnum.ECB010001.key,EcbEnum.ECB010001.value+"传入参数:"+ JSON.toJSONString(map));
            }
            if(StringUtils.isBlank(grpSerno)){
                throw BizException.error(null, EcbEnum.ECB010001.key,EcbEnum.ECB010001.value+"传入参数:"+ JSON.toJSONString(map));
            }
            List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(originGrpReplySerno);
            log.info("查询原授信申请流水号[{}]对应的批复数据信息[{}]",originGrpSerno,JSON.toJSONString(lmtGrpMemRels));
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                String singleSerno = lmtGrpMemRel.getSingleSerno();
                String cusName = lmtGrpMemRel.getCusName();
                if(!StringUtils.isEmpty(singleSerno)){
                    LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(singleSerno);
                    log.info("查询当前成员客户[{}]对应的上期批复数据信息[{}]",lmtReply.getCusId(),JSON.toJSONString(lmtReply));
                    if(Objects.isNull(lmtReply)){
                        throw BizException.error(null, EcbEnum.ECB010064.key,EcbEnum.ECB010064.value+"查询参数:"+ JSON.toJSONString(singleSerno));
                    }
                    Map map1 = new HashMap();
                    map1.put("serno",lmtReply.getSerno());
                    map1.put("cusName",cusName);
                    map1.put("lmtAmt",Optional.ofNullable(lmtReply.getOpenTotalLmtAmt()).orElse(BigDecimal.ZERO).add(Optional.ofNullable(lmtReply.getLowRiskTotalLmtAmt()).orElse(BigDecimal.ZERO)));
                    // 目前用信净额 contAmt
                    User userInfo = SessionUtils.getUserInformation();
                    String instuCode = CmisCommonUtils.getInstucde(userInfo.getOrg().getCode());
                    CmisLmt0059ReqDto cmisLmt0059ReqDto = new CmisLmt0059ReqDto();
                    cmisLmt0059ReqDto.setInstuCde(instuCode);
                    cmisLmt0059ReqDto.setCusId(lmtGrpMemRel.getCusId());
                    log.info("获取客户编号对应的用信净额,请求报文:"+JSON.toJSONString(cmisLmt0059ReqDto));
                    CmisLmt0059RespDto cmisLmt0059RespDto = cmisLmtClientService.cmislmt0059(cmisLmt0059ReqDto).getData();
                    log.info("获取客户编号对应的用信净额,响应报文:"+JSON.toJSONString(cmisLmt0059RespDto));
                    if(cmisLmt0059RespDto.getErrorCode().equals(EcbEnum.ECB010000.key)){
                        map1.put("contAmt",cmisLmt0059RespDto.getSpacLoanBal());
                    }else{
                        map1.put("contAmt","");
                    }
                    LmtGrpMemRel lmtGrpMemRel1 = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSernoAndCusId(grpSerno,lmtGrpMemRel.getCusId());
                    log.info("查询当前成员客户[{}]本期复审时的数据信息[{}]",lmtGrpMemRel.getCusId(),JSON.toJSONString(lmtGrpMemRel1));
                    if(Objects.nonNull(lmtGrpMemRel1)){
                        map1.put("reconsideAmt",Optional.ofNullable(lmtGrpMemRel1.getOpenLmtAmt()).orElse(BigDecimal.ZERO).add(Optional.ofNullable(lmtGrpMemRel1.getLowRiskLmtAmt()).orElse(BigDecimal.ZERO)));
                    }
                    latestLmtInfoDtos.add(map1);
                }
            }
        }catch (Exception e){
            throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        return latestLmtInfoDtos;
    }
}
