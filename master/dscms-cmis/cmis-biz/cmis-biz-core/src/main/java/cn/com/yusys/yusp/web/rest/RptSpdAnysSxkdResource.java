/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RptSpdAnysJxd;
import cn.com.yusys.yusp.dto.PfkInfoDto;
import cn.com.yusys.yusp.dto.PfkInfosDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysSxkd;
import cn.com.yusys.yusp.service.RptSpdAnysSxkdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysSxkdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-23 16:15:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyssxkd")
public class RptSpdAnysSxkdResource {
    @Autowired
    private RptSpdAnysSxkdService rptSpdAnysSxkdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysSxkd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysSxkd> list = rptSpdAnysSxkdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysSxkd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysSxkd>> index(QueryModel queryModel) {
        List<RptSpdAnysSxkd> list = rptSpdAnysSxkdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysSxkd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysSxkd> show(@PathVariable("serno") String serno) {
        RptSpdAnysSxkd rptSpdAnysSxkd = rptSpdAnysSxkdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysSxkd>(rptSpdAnysSxkd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysSxkd> create(@RequestBody RptSpdAnysSxkd rptSpdAnysSxkd) throws URISyntaxException {
        rptSpdAnysSxkdService.insert(rptSpdAnysSxkd);
        return new ResultDto<RptSpdAnysSxkd>(rptSpdAnysSxkd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysSxkd rptSpdAnysSxkd) throws URISyntaxException {
        int result = rptSpdAnysSxkdService.update(rptSpdAnysSxkd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysSxkdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysSxkdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysSxkd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysSxkd rptSpdAnysSxkd = rptSpdAnysSxkdService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysSxkd);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysSxkd> save(@RequestBody RptSpdAnysSxkd rptSpdAnysSxkd) throws URISyntaxException {
        RptSpdAnysSxkd rptSpdAnysSxkd1 = rptSpdAnysSxkdService.selectByPrimaryKey(rptSpdAnysSxkd.getSerno());
        if(rptSpdAnysSxkd1!=null){
            rptSpdAnysSxkdService.update(rptSpdAnysSxkd);
        }else{
            rptSpdAnysSxkdService.insert(rptSpdAnysSxkd);
        }
        return ResultDto.success(rptSpdAnysSxkd);
    }
   @PostMapping("/initSxkd")
    protected ResultDto<Integer> initSxkd(@RequestBody RptSpdAnysSxkd rptSpdAnysSxkd){
        return new ResultDto<Integer>(rptSpdAnysSxkdService.insertSelective(rptSpdAnysSxkd));
   }

    /**
     * 评分卡自动打分
     * @param params
     * @return
     */
   @PostMapping("/autoValue")
    protected ResultDto<Integer> autoValue(@RequestBody Map params){
        return new ResultDto<Integer>(rptSpdAnysSxkdService.autoValue(params));
   }
}
