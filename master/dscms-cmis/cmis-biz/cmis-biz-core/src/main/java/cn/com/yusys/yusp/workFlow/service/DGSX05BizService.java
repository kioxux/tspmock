package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DGSX05BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGSX05BizService.class);//定义log


    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Resource
    private ICusClientService icusClientService;
    @Autowired
    private CentralFileTaskService centralFileTaskService;
    @Autowired
    private CmisBizClientService cmisBizClientService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if (CmisFlowConstants.FLOW_TYPE_TYPE_SX024.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX025.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX026.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX027.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX028.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX029.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX030.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX031.equals(bizType)) {
            handleLmtAppForGrpBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    // 单一客户处理
    private void handleLmtAppForGrpBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "集团成员授信申报审核子流程" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            // 获取分成比例协办客户经理编号
            String managerId = null;
            CusMgrDividePercDto cusMgrDividePercDto = icusClientService.selectXBManagerId(lmtApp.getCusId()).getData();
            log.info("根据客户号【{}】查询客户协办客户经理，返回为{}", lmtApp.getCusId(), cusMgrDividePercDto);
            if (cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())) {
                managerId = cusMgrDividePercDto.getManagerId();
            }
            WFBizParamDto param = new WFBizParamDto();
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            Map<String, Object> params = new HashMap<>();
            params.put("managerId", managerId);
            // 是否对公客户，判断协办客户经理节点
            CusBaseClientDto cusBaseClientDto = icusClientService.queryCus(lmtApp.getCusId());
            log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_1);
            } else {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
            log.info("更新流程参数");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterStart(serno);
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createCentralFileTask(serno, lmtApp.getCusId(), lmtApp.getCusName(), lmtApp.getInputId(), lmtApp.getInputBrId(), resultInstanceDto.getBizType(), resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterRefuse(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 新增档案任务
     *
     * @param serno
     * @param cusId
     * @param cusName
     * @param inputId
     * @param inputBrId
     * @param bizType
     */
    public void createCentralFileTask(String serno, String cusId, String cusName, String inputId, String inputBrId, String bizType, ResultInstanceDto resultInstanceDto) {
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(serno);
        centralFileTaskdto.setCusId(cusId);
        centralFileTaskdto.setCusName(cusName);
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        centralFileTaskdto.setInputId(inputId);
        centralFileTaskdto.setInputBrId(inputBrId);
        centralFileTaskdto.setOptType("01"); // 纯指令
        centralFileTaskdto.setTaskType("01"); // 档案接收
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        //获取分项信息
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            boolean flag = false;
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String guarMode = lmtAppSub.getGuarMode();
                if (StringUtils.nonBlank(guarMode)) {
                    //若分项担保方式全部为低风险,则为加急
                    if (!"60".equals(guarMode)) {
                        flag = true;
                    }
                }
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                        //获取分项明细产品类型属性
                        String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                        //若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
                        if (StringUtils.nonBlank(lmtBizTypeProp)) {
                            if ("P032".equals(lmtBizTypeProp) || "P011".equals(lmtBizTypeProp) || "P009".equals(lmtBizTypeProp) || "P013".equals(lmtBizTypeProp) || "P014".equals(lmtBizTypeProp) || "P016".equals(lmtBizTypeProp)) {
                                centralFileTaskdto.setTaskUrgentFlag("3"); //系统加急
                            }
                        }
                    }
                }
            }
            if (!flag) {
                centralFileTaskdto.setTaskUrgentFlag("3"); //系统加急
            }

        }
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGSX05.equals(flowCode);
    }
}
