package cn.com.yusys.yusp.web.server.xdcz0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0014.req.Xdcz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.resp.Xdcz0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0014.Xdcz0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:企业网银查询影像补录批次
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0014:企业网银查询影像补录批次")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0014.BizXdcz0014Resource.class);

    @Autowired
    private Xdcz0014Service xdcz0014Service;
    /**
     * 交易码：xdcz0014
     * 交易描述：企业网银银票查询影像补录批次
     *
     * @param xdcz0014DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("企业网银查询影像补录批次")
    @PostMapping("/xdcz0014")
    protected @ResponseBody
    ResultDto<Xdcz0014DataRespDto>  xdcz0014(@Validated @RequestBody Xdcz0014DataReqDto xdcz0014DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataReqDto));
        Xdcz0014DataRespDto  xdcz0014DataRespDto  = new Xdcz0014DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0014DataRespDto>xdcz0014DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0014DataReqDto获取业务值进行业务逻辑处理
            xdcz0014DataRespDto = xdcz0014Service.xdcz0014(xdcz0014DataReqDto);
            // 封装xdcz0014DataResultDto中正确的返回码和返回信息
            xdcz0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value,e.getMessage());
            // 封装xdcz0014DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0014DataRespDto到xdcz0014DataResultDto中
        xdcz0014DataResultDto.setData(xdcz0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataRespDto));
        return xdcz0014DataResultDto;
    }
}
