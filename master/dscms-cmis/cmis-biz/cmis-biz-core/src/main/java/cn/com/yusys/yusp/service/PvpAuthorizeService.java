package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1149.req.Fb1149ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Ln3020RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkbjfd;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkhbjh;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req.CljctzReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req.MfzjcrReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.Xdpj03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.req.Wxp003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.req.ClfxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.resp.ClfxcxRespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcfEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.repository.mapper.SxkdDrawCheckMapper;
import cn.com.yusys.yusp.repository.mapper.ToppAcctSubMapper;
import cn.com.yusys.yusp.service.client.bsp.core.ib1241.Ib1241Service;
import cn.com.yusys.yusp.service.client.bsp.core.ib1253.Ib1253Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3020.Ln3020Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3100.Ln3100Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3108.Ln3108Service;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.bsp.gaps.cljctz.CljctzService;
import cn.com.yusys.yusp.service.client.bsp.gaps.mfzjcr.MfzjcrService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorizeService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-01-08 20:46:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class PvpAuthorizeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PvpAuthorizeService.class);

    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;//合同服务

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private ToppAcctSubService toppAcctSubService;

    @Autowired
    private PvpLoanAppSegInterstSubService pvpLoanAppSegInterstSubService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;


    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private Ib1253Service ib1253Service;

    @Autowired
    private Ln3020Service ln3020Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private Ib1241Service ib1241Service;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private ToppAccPayDetailService toppAccPayDetailService;

    @Autowired
    private PvpRenewLoanInfoService pvpRenewLoanInfoService;

    @Autowired
    private PvpLoanAppRepayBillRellService pvpLoanAppRepayBillRellService;

    @Autowired
    private Dscms2ZjywxtClientService dscms2ZjywxtClientService;

    @Autowired
    private PvpSehandEsInfoService pvpSehandEsInfoService;

    @Autowired
    private Ln3100Service ln3100Service;

    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

    @Autowired
    private Dscms2WxClientService dscms2WxClientService;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private MfzjcrService mfzjcrService;
    @Autowired
    private ToppAcctSubMapper toppAcctSubMapper;
    @Autowired
    private CljctzService cljctzService;
    @Autowired
    private SxkdDrawCheckMapper sxkdDrawCheckMapper;
    @Autowired
    private AccLoanFreeInterService accLoanFreeInterService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;


    // 日志
    private static final Logger log = LoggerFactory.getLogger(IqpStpUnstpIntAppService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PvpAuthorize selectByPrimaryKey(String tranSerno) {
        return pvpAuthorizeMapper.selectByPrimaryKey(tranSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PvpAuthorize> selectAll(QueryModel model) {
        List<PvpAuthorize> records = (List<PvpAuthorize>) pvpAuthorizeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PvpAuthorize> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpAuthorize> list = pvpAuthorizeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(PvpAuthorize record) {
        return pvpAuthorizeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(PvpAuthorize record) {
        return pvpAuthorizeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(PvpAuthorize record) {
        return pvpAuthorizeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(PvpAuthorize record) {
        return pvpAuthorizeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String tranSerno) {
        return pvpAuthorizeMapper.deleteByPrimaryKey(tranSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return pvpAuthorizeMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignEstrustlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAuthorize> toSignEntrustlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_1);
        //model.getCondition().put("authStatusApply", CmisCommonConstants.AUTH_STATUS_BF);
        model.getCondition().put("tranId", CmisCommonConstants.WTDK);
        return pvpAuthorizeMapper.selectEntrustByModel(model);
    }

    /**
     * @方法名称: doneSignEnstrustlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAuthorize> doneSignEntrustlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        //model.getCondition().put("authStatusApply", CmisCommonConstants.AUTH_STATUS_AF);
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_2);
        model.getCondition().put("tranId", CmisCommonConstants.WTDK);
        return pvpAuthorizeMapper.selectEntrustByModel(model);
    }

    /**
     * @方法名称: doneSignEnstrustlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public PvpAuthorize selectPvpAuthorizeBySerno(String pvpSerno) {
        return pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
    }

    /**
     * @param billNo
     * @return cn.com.yusys.yusp.domain.PvpAuthorize
     * @author 王玉坤
     * @date 2021/10/7 0:16
     * @version 1.0.0
     * @desc 根据借据号查询借据信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public PvpAuthorize selectPvpAuthorizeByBillNo(String billNo) {
        return pvpAuthorizeMapper.selectPvpAuthorizeByBillNo(billNo);
    }


    /**
     * @方法名称: toSignAccpList
     * @方法描述: 查询银承待出账数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAuthorize> toSignAccpList(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_1);
        model.getCondition().put("prdId", "");
        return pvpAuthorizeMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignAccpList
     * @方法描述: 查询银承出账历史数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAuthorize> doneSignAccpList(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_2);
        model.getCondition().put("prdId", "");
        return pvpAuthorizeMapper.selectByModel(model);
    }

    /**
     * 银承出账发送票据系统
     *
     * @param pvpAuthorize
     * @return
     */
    public Map sendBill(PvpAuthorize pvpAuthorize) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = "发送成功";
        String isdraftpool = "1";
        String tradeStatus = CmisCommonConstants.TRADE_STATUS_1;
        log.info("银承出账发送票据系统,出账流水号：" + pvpAuthorize.getPvpSerno());
        try {
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(pvpAuthorize.getPvpSerno());
            String pvpSerno = pvpAuthorize.getPvpSerno();
            log.info("银承出账-普通银承开立-开始查询保证金账号，放款流水号：" + pvpSerno);
            List<BailAccInfo> listBailAccInfo = bailAccInfoService.selectBySerno(pvpSerno);
            Xdpj03ReqDto reqDto = new Xdpj03ReqDto();
            reqDto.setTxCode("XDPJ003");//交易码
            reqDto.setBatchNo(pvpAccpApp.getPvpSerno());//批次号
            reqDto.setIsdraftpool(isdraftpool);//是否票据池编号
            reqDto.setProtocolNo(pvpAccpApp.getContNo());//承兑协议编号
            reqDto.setTransType("1");//处理码
            reqDto.setYp_sj(pvpAccpApp.getFileSufFlag());//资料是否收集全
            //todo 银承出账流水没有 PvpAccpAppRegister老信贷
            List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.List> list = new LinkedList<>();

            for (int i = 0; i < listBailAccInfo.size(); i++) {
                BailAccInfo bailAccInfo = listBailAccInfo.get(i);
                cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.List listTemp = new cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.List();
                listTemp.setAccountType(bailAccInfo.getAccountType());//账户类型
                listTemp.setCurrency(bailAccInfo.getBailCurType());//保证金币种
                listTemp.setInterestMode(Objects.equals(bailAccInfo.getBailInterestMode(), "301") ? "207" : bailAccInfo.getBailInterestMode());//保证金计息方式
                listTemp.setAcctAmt(Objects.toString(Optional.ofNullable(bailAccInfo.getBailAmt()).orElse(BigDecimal.ZERO)));//保证金金额
                listTemp.setFirstAcctSeq(bailAccInfo.getFirstAccount());//母户序号
                listTemp.setTwoAcctSeq(bailAccInfo.getBailAccNoSub());//保证金账号子序号
                listTemp.setThreeAcctNo(bailAccInfo.getBailAccName());//保证金账号名称
                listTemp.setThreeAcctNo(bailAccInfo.getBailAccNo());//保证金账号
                listTemp.setSettleAcctNo(bailAccInfo.getSettlAccno());//结算账户
                listTemp.setSettleAcctSeq(bailAccInfo.getSettlAccnoSub());//结算账号子序号
                listTemp.setSettleAcctName(bailAccInfo.getSettlAccname());//结算账户名
                listTemp.setSettleBankCode("");//结算开户行行号
                listTemp.setClearAcctSeq(bailAccInfo.getClearAccnoSub());//待清算子序号
                listTemp.setClearAcctName(bailAccInfo.getClearAccname());//待清算账号名
                listTemp.setClearBankCode("");//待清算开户行行号
                listTemp.setZhfutojn(bailAccInfo.getZhfutojn());//支付条件
                list.add(listTemp);
            }

            reqDto.setList(list);
            ResultDto<Xdpj03RespDto> resultDto = dscms2PjxtClientService.xdpj03(reqDto);
            if (resultDto.getCode().equals("0")) {
                if ("0000".equals(resultDto.getData().getErorcd())) {
                    // 出票成功，更新状态, 出账状态
                    tradeStatus = CmisCommonConstants.TRADE_STATUS_2;
                    log.info("银承出账成功,出账流水号：" + pvpAuthorize.getPvpSerno());
                }
            } else {
                rtnCode = resultDto.getData().getErorcd();
                rtnMsg = resultDto.getData().getErortx();
                throw BizException.error(null, rtnCode, rtnMsg);
            }

            updateTradeStatusSuccess(pvpAuthorize.getPvpSerno(), tradeStatus, rtnMsg);

            if (Objects.equals(pvpAccpApp.getPvpMode(), "1")) {
                //自动出账调用额度接口占额  手动出账等票据记账之后发送xdcz0004调用额度接口
                // 银承通知票据系统出账后，通知额度系统更新台账状态
                CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
                cmisLmt0029ReqDto.setDealBizNo(pvpSerno);//交易流水号
                cmisLmt0029ReqDto.setOriginAccNo(pvpSerno);//原台账编号
                cmisLmt0029ReqDto.setNewAccNo("");//新台账编号
                cmisLmt0029ReqDto.setStartDate("");//合同起始日
                cmisLmt0029ReqDto.setEndDate("");//合同到期日
                cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
                log.info("银承出账通知【{}】，前往额度系统调额度出账通知开始,请求报文为:【{}】", pvpSerno, cmisLmt0029ReqDto.toString());
                ResultDto<CmisLmt0029RespDto> resultDtoData = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
                log.info("银承出账通知【{}】，前往额度系统调额度出账通知结束,响应报文为:【{}】", pvpSerno, resultDtoData.toString());
                if (!"0".equals(resultDto.getCode())) {
                    log.error("银承出账调额度出账通知" + pvpSerno + "前往额度系统通知出账异常！");
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoData.getData().getErrorCode();
                if (!EcbEnum.ECB010000.key.equals(code)) {
                    log.error("银承出账调额度出账通知异常！");
                    rtnCode = resultDtoData.getData().getErrorCode();
                    rtnMsg = resultDtoData.getData().getErrorMsg();
                    throw BizException.error(null, code, rtnMsg);
                }
            }
        } catch (BizException e) {
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
            log.error("银承贷款出账异常！", e.getMessage());
            throw e;
        } catch (Exception e) {
            rtnCode = "9999";
            rtnMsg = e.getMessage();
            log.error("银承异常！", e.getMessage());
            return result;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }

        return result;
    }

    /**
     * @方法名称: updateTradeStatusSuccess
     * @方法描述: 根据出账流水号更新出账状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateTradeStatusSuccess(String pvpSerno, String tradeStatus, String returnDesc) {
        return pvpAuthorizeMapper.updateTradeStatusSuccess(pvpSerno, tradeStatus, returnDesc);
    }

    /**
     * @author zlf
     * @date 2021/5/8 16:22
     * @version 1.0.0
     * @desc 零售通知核心  出账授权
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<PvpAuthorize> sendCorels(PvpAuthorize pvpAuthorize) {

        Ln3020ReqDto reqDto = null;
        try {
            reqDto = new Ln3020ReqDto();

            if (pvpAuthorize != null) {
                //贷款借据号
                reqDto.setDkjiejuh(pvpAuthorize.getBillNo());
                //合同号
                reqDto.setHetongbh(pvpAuthorize.getAcctNo());
                //产品代码
                reqDto.setChanpdma(pvpAuthorize.getPrdId());
                //产品名称
                reqDto.setChanpmch(pvpAuthorize.getPrdName());
                //客户号
                reqDto.setKehuhaoo(pvpAuthorize.getCusId());
                //客户名称
                reqDto.setKehmingc(pvpAuthorize.getCusName());
                //本次放款金额
                reqDto.setBencfkje(pvpAuthorize.getTranAmt());
                //发送拼接报文
                ResultDto<Ln3020RespDto> respDto = dscms2CoreLnClientService.ln3020(reqDto);
                if ("0000".equals(respDto.getCode())) {
                    if (respDto.getData() != null) {
                        Ln3020RespDto Ln3020RespDto = respDto.getData();
                        //获取返回报文信息保存数据  更改授权和出账状态
                        //02 已授权  2已出账
                        pvpAuthorize.setAuthStatus("02");
                        pvpAuthorize.setTradeStatus("2");
                        int i = pvpAuthorizeMapper.updateTrade(pvpAuthorize);
                        if (i == 1) {
                            return new ResultDto(pvpAuthorize).message("操作成功");
                        } else {
                            throw new YuspException("修改操作异常", "修改操作异常");
                        }
                    }
                } else {
                    return new ResultDto(pvpAuthorize).message("系统异常");
                }
            }

        } catch (YuspException e) {
            log.error("零售出账授权操作异常！", e.getMsg());
            return new ResultDto(pvpAuthorize).message("零售出账授权操作异常！");
        } catch (Exception e) {
            log.error("核心接口异常！", e.getMessage());
            return new ResultDto(pvpAuthorize).message("核心接口异常！");
        } finally {

        }
        return new ResultDto(pvpAuthorize);
    }


    /**
     * @方法名称: toAccPvpAuthorize
     * @方法描述: 贷款出账通知
     * @参数与返回说明:
     * @author: zhanyb
     */
    @Transactional
    public List<PvpAuthorize> toAccPvpAuthorize(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_1);
        model.getCondition().put("prdId", "");
        return pvpAuthorizeMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneAccPvpAuthorize
     * @方法描述: 贷款出账通知历史列表
     * @参数与返回说明:
     * @author: zhanyb
     */
    @Transactional
    public List<PvpAuthorize> doneAccPvpAuthorize(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("tradeStatus", CmisCommonConstants.TRADE_STATUS_2);
        model.getCondition().put("prdId", "");
        return pvpAuthorizeMapper.selectByModel(model);
    }

    /*
     * 820新核心改造 币种映射 信贷--->核心
     * @param xdbz 信贷币种码值
     * @return hxbz 核心币种码值
     */
    public static String toConverCurrency(String xdbz) {
        String hxbz = "";
        if ("CNY".equals(xdbz)) {
            hxbz = "01";
        } else if ("MOP".equals(xdbz)) {//澳门币
            hxbz = "81";
        } else if ("CAD".equals(xdbz)) {//加元
            hxbz = "28";
        } else if ("CHF".equals(xdbz)) {//瑞士法郎
            hxbz = "15";
        } else if ("JPY".equals(xdbz)) {//日元
            hxbz = "27";
        } else if ("EUR".equals(xdbz)) {//欧元
            hxbz = "38";
        } else if ("GBP".equals(xdbz)) {//英镑
            hxbz = "12";
        } else if ("HKD".equals(xdbz)) {//港币
            hxbz = "13";
        } else if ("AUD".equals(xdbz)) {//澳元
            hxbz = "29";
        } else if ("USD".equals(xdbz)) {//美元
            hxbz = "14";
        } else if ("SGD".equals(xdbz)) {//新加坡元
            hxbz = "18";
        } else if ("SEK".equals(xdbz)) {//瑞典克郎
            hxbz = "21";
        } else if ("DKK".equals(xdbz)) {//丹麦克朗
            hxbz = "22";
        } else if ("NOK".equals(xdbz)) {//挪威克朗
            hxbz = "23";
        } else {
            hxbz = xdbz;//未匹配到的币种发信贷的币种过去（DEM 德国马克;MSD 克鲁赛罗;NLG 荷兰盾;BEF 比利时法郎;ITL 意大利里拉;FRF 法国法郎;ATS 奥地利先令;FIM 芬兰马克）
        }
        return hxbz;
    }

    /*
     * 820新核心改造 利率映射 信贷--->核心
     * @param xdlv 信贷利率码值
     * @return hxlv 核心利率码值
     */
    public static String toConverRate(String xdlv) {
        String hxlv = "";
        //信贷利率类型与新核心映射
        if ("F".equals(xdlv) || "02".equals(xdlv)) {//本币的浮动利率，就选1-PBOC
            hxlv = "1";// 利率类型：1--PBOC、2--SHIBOR、3--HIBOR、4--LIBOR、5--SIBOR、6--固定利率
        } else if ("G".equals(xdlv) || "01".equals(xdlv)) {//固定利率，就选6--固定利率
            hxlv = "6";// 利率类型：1--PBOC、2--SHIBOR、3--HIBOR、4--LIBOR、5--SIBOR、6--固定利率
        }
        return hxlv;
    }

    /*
     * 820新核心改造 担保方式映射 信贷-->核心
     * @param xddbfs 信贷担保方式码值
     * @return hxdbfs 核心担保方式码值
     */
    public static String toConverAssureMeansMain(String xddbfs) {
        String hxdbfs = "";
        //信贷字典项：00-信用、21-低风险质押、40-全额保证金、30-保证、10-抵押、20-质押
        //新核心字典项： 1--抵押、2--质押、3--保证、4--信用、5--其他
        if ("00".equals(xddbfs)) {
            hxdbfs = "4";
        } else if ("21".equals(xddbfs)) {
            hxdbfs = "5";
        } else if ("40".equals(xddbfs)) {
            hxdbfs = "3";
        } else if ("30".equals(xddbfs)) {
            hxdbfs = "3";
        } else if ("10".equals(xddbfs)) {
            hxdbfs = "1";
        } else if ("20".equals(xddbfs)) {
            hxdbfs = "2";
        }
        return hxdbfs;
    }

    /*
     * 820新核心改造 贷款还款方式映射 信贷-->核心
     * @param xddbfs 信贷贷款还款方式码值
     * @return hxdbfs 核心贷款还款方式码值
     */
    public static String toConverLoanPaymMtd(String xdhkfs) {
        String hxhkfs = "";
/*信贷字典项：
A001-按期付息,到期还本
A002-等额本息
A003-等额本金
A004-气球还款
A009-利随本清
A012-按226比例还款
A013-按月还息按季还本
A014-按月还息按半年还本
A015-按月还息,按年还本
A016-新226
A017-前6个月按月还息，后6个月等额本息
A018-前4个月按月还息，后8个月等额本息
A019-第一年按月还息，接下来等额本息
A020-334比例还款
A021-433比例还款
A022-10年期等额本息
A023-按月付息，定期还本
A030-定制还款
A1-半年结算,到期还本
A2-等额本金,按年结算
A3-按季结算,到期还本*/
        //新核心字典项：：1--利随本清、2--多次还息一次还本、3--等额本息、4--等额本金、5--等比累进、6--等额累进、7--定制还款
        if ("A001".equals(xdhkfs)) {
            hxhkfs = "2";
        } else if ("A002".equals(xdhkfs)) {
            hxhkfs = "3";
        } else if ("A003".equals(xdhkfs)) {
            hxhkfs = "4";
        } else if ("A004".equals(xdhkfs)) {
            hxhkfs = "2";
        } else if ("A009".equals(xdhkfs)) {
            hxhkfs = "1";
        } else if ("A012".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A013".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A014".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A015".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A016".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A017".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A018".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A019".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A020".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A021".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A022".equals(xdhkfs)) {
            hxhkfs = "3";
        } else if ("A023".equals(xdhkfs)) {
            hxhkfs = "2";
        } else if ("A030".equals(xdhkfs)) {
            hxhkfs = "7";
        } else if ("A1".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A2".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A3".equals(xdhkfs)) {
            hxhkfs = "";
        } else if ("A031".equals(xdhkfs)) {
            hxhkfs = "3";
        }
        return hxhkfs;
    }

    public static Map<String, String> toConverPeriod(String xdhkfs, PvpLoanApp pvpLoanApp) {
        Map<String, String> resultMap = new HashMap<String, String>();
        String paym_freq_freq = pvpLoanApp.getEiIntervalCycle();//结息间隔
        String paym_freq_unit = pvpLoanApp.getEiIntervalUnit();//结息周期
        String due_day = pvpLoanApp.getDeductDay();//扣款日(日)
        if (due_day.length() == 1) {
            due_day = "0" + due_day;
        }
/*信贷特殊还款包含的字典项：
A012-按226比例还款
A013-按月还息按季还本
A014-按月还息按半年还本
A015-按月还息,按年还本
A016-新226
A017-前6个月按月还息，后6个月等额本息
A018-前4个月按月还息，后8个月等额本息
A019-第一年按月还息，接下来等额本息
A020-334比例还款
A021-433比例还款*/
        String hkzhouqi = paym_freq_freq + paym_freq_unit + "A" + due_day;
        if ("A012".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A013".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "3" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A014".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "6" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A015".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A016".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A017".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A018".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A019".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A020".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A021".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        }
        return resultMap;
    }

    public static Map<String, String> toConverPeriodWt(String xdhkfs, PvpEntrustLoanApp pvpEntrustLoanApp) {
        Map<String, String> resultMap = new HashMap<String, String>();
        String paym_freq_freq = pvpEntrustLoanApp.getEiIntervalCycle();//结息间隔
        String paym_freq_unit = pvpEntrustLoanApp.getEiIntervalUnit();//结息周期
        String due_day = pvpEntrustLoanApp.getDeductDay();//扣款日(日)
        if (due_day.length() == 1) {
            due_day = "0" + due_day;
        }
/*信贷特殊还款包含的字典项：
A012-按226比例还款
A013-按月还息按季还本
A014-按月还息按半年还本
A015-按月还息,按年还本
A016-新226
A017-前6个月按月还息，后6个月等额本息
A018-前4个月按月还息，后8个月等额本息
A019-第一年按月还息，接下来等额本息
A020-334比例还款
A021-433比例还款*/
        String hkzhouqi = paym_freq_freq + paym_freq_unit + "A" + due_day;
        if ("A012".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A013".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "3" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A014".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "6" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A015".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A016".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A017".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A018".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A019".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", hkzhouqi);//还本周期
        } else if ("A020".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        } else if ("A021".equals(xdhkfs)) {
            resultMap.put("hkzhouqi", hkzhouqi);//还款周期
            resultMap.put("huanbzhq", "12" + paym_freq_unit + "A" + due_day);//还本周期
        }
        return resultMap;
    }

    /**
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/6/11 14:38
     * @version 1.0.0
     * @desc 小贷放款交易处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto sendAuthToCoreForXd(PvpAuthorize pvpAuthorize) {
        log.info("出账申请交易开始，出账流水号【{}】", pvpAuthorize.getTranSerno());

        // 出账授权临时对象信息
        PvpAuthorize pvpAuthorizeTemp = null;
        // 台账对象信息
        AccLoan accLoan = null;
        // 合同对象信息
        CtrLoanCont ctrLoanCont = null;
        // 最高额授信协议对象信息
        CtrHighAmtAgrCont ctrHighAmtAgrCont = null;
        // 资产池协议
        CtrAsplDetails ctrAsplDetails = null;
        // 出账申请对象
        PvpLoanApp pvpLoanApp = null;
        // Ln3005返回对象
        Ln3005RespDto ln3005RespDto = null;
        // Ln3020请求对象
        Ln3020ReqDto ln3020ReqDto = null;
        // Ln3020返回对象
        Ln3020RespDto ln3020RespDto = null;
        try {
            // 1、查询出账通知信息
            log.info("根据出账流水号【{}】查询出账通知信息开始", pvpAuthorize.getTranSerno());
            pvpAuthorizeTemp = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
            if (pvpAuthorizeTemp == null) {
                log.info("根据放款流水号【{}】未查询到出账通知信息", pvpAuthorize.getPvpSerno());
                return new ResultDto(null).message("未查询到出账通知信息").code("9999");
            }
            log.info("根据出账流水号【{}】查询出账通知信息结束", pvpAuthorize.getTranSerno());

            // 1.1、查询台账信息
            log.info("根据借据号【{}】查询台账信息开始", pvpAuthorize.getBillNo());
            accLoan = accLoanService.selectByBillNo(pvpAuthorize.getBillNo());
            if (accLoan == null) {
                return new ResultDto(null).message("未查询到台账信息").code("9999");
            }
            log.info("根据借据号【{}】查询台账信息结束", pvpAuthorize.getBillNo());

            // 1.2、查询合同信息
            // TODO 需要考虑到多种合同
            log.info("根据合同号【{}】查询合同信息开始", pvpAuthorize.getContNo());
            ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpAuthorize.getContNo());
            if (ctrLoanCont == null) {
                ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(pvpAuthorize.getContNo());
                if (ctrHighAmtAgrCont == null) {
                    ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(pvpAuthorize.getContNo());
                    if (Objects.isNull(ctrAsplDetails)) {
                        log.info("根据放款流水号【{}】未查询到合同信息", pvpAuthorize.getPvpSerno());
                        return new ResultDto(null).message("未查询到合同信息").code("9999");
                    }
                }
            }
            log.info("根据合同号【{}】查询合同信息结束", pvpAuthorize.getContNo());

            // 1.3、查询出账申请信息
            log.info("根据放款流水号【{}】查询放款信息开始", pvpAuthorize.getPvpSerno());
            pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(pvpAuthorize.getPvpSerno());
            if (pvpLoanApp == null) {
                log.info("根据放款流水号【{}】未查询到放款信息", pvpAuthorize.getPvpSerno());
                return new ResultDto(null).message("未查询到放款申请信息").code("9999");
            }
            log.info("根据放款流水号【{}】查询放款信息结束", pvpAuthorize.getPvpSerno());
            //贷款起始日与营业日期一致性校验
            // 获取营业日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            //获取贷款起始日
            String loanStartDate = pvpLoanApp.getLoanStartDate();
            if (!loanStartDate.equals(openday)) {
                return new ResultDto(null).message("贷款起始日需与营业日期一致！").code("9999");
            }

            // 3、请求核心接口放款
            // 3.1、拼装请求参数
            log.info("请求核心接口放款【{}】", pvpAuthorize.getPvpSerno());
            ln3020ReqDto = this.buildLn3020ReqBodyForPTDK(pvpAuthorizeTemp, accLoan, ctrLoanCont, ctrHighAmtAgrCont, ctrAsplDetails, pvpLoanApp);
            // 3.2 调用核心出账接口开始
            log.info("请求核心接口放款3020请求报文是：【{}】", JSON.toJSONString(ln3020ReqDto));
            ln3020RespDto = ln3020Service.ln3020(ln3020ReqDto);
            log.info("请求核心接口放款3020响应报文是：【{}】", JSON.toJSONString(ln3020ReqDto));
            // 如果3020接口响应报文不为空，并且受托支付信息也不空，则进行二代受托支付 1--受托支付
            if (Objects.nonNull(ln3020RespDto) && (Objects.equals("1", pvpLoanApp.getIsBeEntrustedPay()))) {
                // 根据授权信息表创建受托支付登记簿名单
                toppAccPayDetailService.createToppAccPayDetail(pvpAuthorizeTemp, ln3020RespDto.getLstdkstzf());
            }

            // 4、更新授权通知信息、台账表状态
            log.info("更新授权通知信息、台账表状态【{}】", pvpAuthorize.getPvpSerno());
            // 4.1、台账信息更新
            // 台账状态 1--正常
            accLoan.setAccStatus("1");
            // 贷款余额
            accLoan.setLoanBalance(accLoan.getLoanAmt());
            // 人民币余额
            accLoan.setExchangeRmbBal(accLoan.getLoanAmt());
            int accCount = accLoanService.updateSelective(accLoan);

            // 4.2、出账授权表信息更新
            pvpAuthorize.setTranDate(openday);
            // 2--发送核心成功
            pvpAuthorize.setTradeStatus("2");

            pvpAuthorize.setAuthStatus("2");
            // 核心交易日期
            pvpAuthorize.setCoreTranDate(ln3020RespDto.getJiaoyirq());
            // 交易流水
            pvpAuthorize.setCoreTranSerno(ln3020RespDto.getJiaoyils());
            pvpAuthorize.setReturnDesc("交易成功");

            int pvpCount = this.updateSelective(pvpAuthorize);
            if (pvpCount > 0 && !"022028".equals(pvpLoanApp.getPrdId())) {//白领贷不在出账时归档
                String chnlSour = pvpLoanApp.getChnlSour();// 渠道来源
                // 出账来源为 00 微信小程序；01 手机银行;03 直销银行等时不生成档案任务(目前判断渠道来源为 02 PC端 时出账生成归档任务)
                if (Objects.equals(chnlSour, "02")) {
                    String prdTypeProp = pvpLoanApp.getPrdTypeProp();// 产品类型属性 STD_PRD_TYPE_PROP P034:房抵e点贷
                    String loanModal = pvpLoanApp.getLoanModal();// 贷款形式 STD_LOAN_MODAL 6 无还本续贷 8 小企业无还本续贷
                    // 房抵e点贷的无还本续贷不生成归档任务
                    if (!(Objects.equals(prdTypeProp, "P034") && (Objects.equals(loanModal, CmisCommonConstants.STD_LOAN_MODAL_6) || Objects.equals(loanModal, CmisCommonConstants.STD_LOAN_MODAL_8)))) {
                        // 生成档案归档信息
                        this.docArchiveTaskForAccLoan(pvpLoanApp);
                    }
                }
            }

            //个人二手住房、二手商用房(资金托管)-买方资金存入
            mfzjcr(pvpAuthorize);

            //连云港-资金缴存通知
            cljctz(pvpAuthorize);

            // 小企业无还本续贷的出账申请的业务处理
            this.repayLoanHandle(pvpLoanApp);

            //调额度出账通知接口
            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
            cmisLmt0029ReqDto.setDealBizNo(pvpLoanApp.getPvpSerno());//交易流水号
            cmisLmt0029ReqDto.setOriginAccNo(pvpLoanApp.getBillNo());//原台账编号
            cmisLmt0029ReqDto.setNewAccNo(pvpLoanApp.getBillNo());//新台账编号
            cmisLmt0029ReqDto.setStartDate("");//合同起始日
            cmisLmt0029ReqDto.setEndDate("");//合同到期日
            cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
            log.info("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "，前往额度系统通知出账开始");
            ResultDto<CmisLmt0029RespDto> resultDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
            log.info("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "，前往额度系统通知出账结束");
            if (!"0".equals(resultDto.getCode())) {
                log.error("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "前往额度系统通知出账异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDto.getData().getErrorCode();
            if (!EcbEnum.ECB010000.key.equals(code)) {
                log.error("贷款出账调额度出账通知异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }

            // 小企业无还本续贷续贷、无还本续贷、借新还旧的原借据额度恢复处理
            this.handleSpecialBusiness(pvpLoanApp);

            // 房抵e点贷 给风控发送fb1161借据信息同步
            if (Objects.equals("P034", pvpLoanApp.getPrdTypeProp())
                    && Objects.equals("1", pvpLoanApp.getLoanModal())
                    && !Objects.equals("01", pvpLoanApp.getChnlSour())) {
                pvpLoanAppService.sendFb1161(pvpLoanApp, openday);
            }
            // 小企业无还本续贷 给风控发送fb1149
            if (Objects.equals("P034", pvpLoanApp.getPrdTypeProp()) && Objects.equals("8", pvpLoanApp.getLoanModal())) {
                Fb1149ReqDto fb1149ReqDto = new Fb1149ReqDto();
                fb1149ReqDto.setCHANNEL_TYPE("13");//渠道来源
                fb1149ReqDto.setBILL_NO(pvpLoanApp.getBillNo());//新借据号
                fb1149ReqDto.setFK_STATUS("0");//放款状态
                dscms2CircpClientService.fb1149(fb1149ReqDto);
            }
            // 征信贷 信贷将放款标识推送给移动端
            if (Objects.equals("P009", pvpLoanApp.getPrdTypeProp())) {
                this.sendWxp003(pvpLoanApp);
            }

            // 仅省心快贷提款审核的出账通知数据，更新省心快贷提款审核状态为已放款
            SxkdDrawCheck sxkdDrawCheck = sxkdDrawCheckMapper.selectByPvpSerno(pvpAuthorize.getPvpSerno());
            if (Objects.nonNull(sxkdDrawCheck)) {
                sxkdDrawCheck.setPvpStatus("2"); //已放款
                sxkdDrawCheckMapper.updateByPrimaryKey(sxkdDrawCheck);
            }

        } catch (BizException e) {
            // 更新授权信息开始
            String handErrorMsg = "";
            if (e.getMessage() != null) {
                if (e.getMessage().length() > 500) {
                    handErrorMsg = e.getMessage().substring(0, 500);
                } else {
                    handErrorMsg = e.getMessage();
                }
            } else {
                handErrorMsg = null;
            }
            pvpAuthorize.setReturnDesc(handErrorMsg);
            pvpAuthorize.setReturnCode(e.getErrorCode());
            pvpAuthorize.setAuthStatus("3");
            this.updateSelective(pvpAuthorize);
            LOGGER.error(e.getMessage(), e);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
            // return new ResultDto(pvpAuthorize).message("出账失败：" + e.getMessage()).code("9999");
        } catch (Exception e) {
            // 更新授权信息开始
            pvpAuthorize.setReturnDesc(e.getMessage());
            pvpAuthorize.setAuthStatus("3");
            this.updateSelective(pvpAuthorize);
            LOGGER.error(e.getMessage(), e);
            throw BizException.error(null, "9999", "出账失败：" + e.getMessage());
            // return new ResultDto(pvpAuthorize).message("出账失败：" + e.getMessage()).code("9999");
        }
        return new ResultDto(pvpAuthorize).message("出账成功");
    }

    /***
     * @param pvpLoanApp
     * @author lihh
     * @date 2021/9/10 11:04
     * @version 1.0.0
     * @desc 征信贷 信贷将放款标识推送给移动端
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendWxp003(PvpLoanApp pvpLoanApp) {
        Wxp003ReqDto wxp003ReqDto = new Wxp003ReqDto();
        wxp003ReqDto.setApplid(pvpLoanApp.getPvpSerno());//对接记录ID
        wxp003ReqDto.setEntnam(pvpLoanApp.getCusName());//企业名称
        wxp003ReqDto.setEntpid(pvpLoanApp.getCusId());//企业代码
        wxp003ReqDto.setPostim(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//放款时间
        wxp003ReqDto.setConnum(pvpLoanApp.getContNo());//贷款合同号
        wxp003ReqDto.setPosamt(pvpLoanApp.getPvpAmt().toString());//放款金额
        wxp003ReqDto.setPosrat(pvpLoanApp.getExecRateYear().toString());//贷款利率
        wxp003ReqDto.setLoanno(pvpLoanApp.getBillNo());//贷款借据号
        wxp003ReqDto.setGuartp(pvpLoanApp.getGuarMode());//担保方式
        wxp003ReqDto.setFivecg("10");//五级分类
        wxp003ReqDto.setPosbeg(pvpLoanApp.getLoanStartDate());//贷款开始时间
        wxp003ReqDto.setPosend(pvpLoanApp.getLoanEndDate());//贷款结束时间
        wxp003ReqDto.setPosper(pvpLoanApp.getLoanTerm());//放款期限
        wxp003ReqDto.setPosrea("正常放款");//放款原因
        dscms2WxClientService.wxp003(wxp003ReqDto);
    }

    /***
     * @param pvpLoanApp
     * @return handleSpecialBusiness
     * @author quwen
     * @date 2021/9/10 11:04
     * @version 1.0.0
     * @desc 小企业无还本续贷、无还本续贷、借新还旧的业务处理
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void handleSpecialBusiness(PvpLoanApp pvpLoanApp) {
        // 根据出账申请流水查询关联的原借据
        log.info("判断该笔出账申请: " + pvpLoanApp.getPvpSerno() + " 是否为小企业无还本续贷、无还本续贷、借新还旧业务");
        if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(pvpLoanApp.getLoanModal()) ||
                CmisCommonConstants.STD_LOAN_MODAL_6.equals(pvpLoanApp.getLoanModal()) ||
                CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())) {
            log.info("开始执行小企业无还本续贷、无还本续贷、借新还旧业务: " + pvpLoanApp.getPvpSerno() + " 的业务处理");
            PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = pvpLoanAppRepayBillRellService.selectByPvpSerno(pvpLoanApp.getPvpSerno());
            if (Objects.isNull(pvpLoanAppRepayBillRell)) {
                log.error("根据出账申请流水号：" + pvpLoanApp.getPvpSerno() + "未找到引入的借据");
                throw BizException.error(null, EcbEnum.ECB020044.key, EcbEnum.ECB020044.value);
            }
            // 根据原借据编号获取对应的原台账
            AccLoan accLoan = accLoanService.selectByBillNo(pvpLoanAppRepayBillRell.getBillNo());
            if (Objects.isNull(accLoan)) {
                log.error("根据借据编号：" + pvpLoanAppRepayBillRell.getBillNo() + "未找到对应的台账信息");
                throw BizException.error(null, EcbEnum.ECB020043.key, EcbEnum.ECB020043.value);
            }
            // 组装报文查询原借据的借据状态
            Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto();
            ln3100ReqDto.setDkjiejuh(pvpLoanAppRepayBillRell.getBillNo());//贷款借据号
            log.info("查询借据状态【{}】，前往核心系统查询开始,请求报文为:【{}】", pvpLoanAppRepayBillRell.getBillNo(), ln3100ReqDto.toString());
            Ln3100RespDto ln3100RespDto = ln3100Service.ln3100(ln3100ReqDto);
            log.info("查询借据状态【{}】，前往核心系统查询结束,响应报文为:【{}】", pvpLoanAppRepayBillRell.getBillNo(), ln3100RespDto.toString());
            String status = ln3100RespDto.getDkzhhzht();
            if ("1".equals(status)) {
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                Map accLoanMap = new HashMap();
                accLoanMap.put("billNo", accLoan.getBillNo());// 借据号
                accLoanMap.put("accStatus", CmisCommonConstants.ACC_STATUS_0);// 贷款状态
                accLoanMap.put("loanBalance", BigDecimal.ZERO);// 贷款余额
                accLoanMap.put("settlDate", openDay);// 结清日期
                accLoanMap.put("exchangeRmbBal", BigDecimal.ZERO);//折合人民币余额
                // 更新贷款台账状态为结清
                int updateCount = accLoanService.updateAccLoanByBillNo(accLoanMap);
                if (updateCount <= 0) {
                    log.error("根据借据编号：" + pvpLoanAppRepayBillRell.getBillNo() + "更新台账信息异常！");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }

                String inputId = "";
                String inputBrId = "";
                String inputDate = "";
                // 获取当前的登录人信息
                User userInfo = SessionUtils.getUserInformation();
                if (Objects.nonNull(userInfo)) {
                    inputId = userInfo.getLoginCode();
                    inputBrId = userInfo.getOrg().getCode();
                    inputDate = stringRedisTemplate.opsForValue().get("openDay");
                }
                // 根据担保方式判断是否低风险
                String guarMode = accLoan.getGuarMode();
                // 恢复敞口余额
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                // 恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;

                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverAmtCny = accLoan.getExchangeRmbBal();
                    recoverSpacAmtCny = BigDecimal.ZERO;
                } else {
                    recoverAmtCny = accLoan.getExchangeRmbBal();
                    recoverSpacAmtCny = accLoan.getExchangeRmbBal();
                }

                // 组装报文进行台账恢复
                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0014ReqDto.setSerno(pvpLoanApp.getPvpSerno());//交易流水号
                cmisLmt0014ReqDto.setInputId(inputId);//登记人
                cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
                cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

                List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

                CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
                cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpLoanAppRepayBillRell.getBillNo());//台账编号
                cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_01);//恢复类型
                cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
                cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
                cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
                cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
                cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
                cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
                cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
                dealBizList.add(cmisLmt0014ReqdealBizListDto);
                cmisLmt0014ReqDto.setDealBizList(dealBizList);
                log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpLoanAppRepayBillRell.getBillNo(), cmisLmt0014ReqDto.toString());
                ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
                log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpLoanAppRepayBillRell.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
                if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
                    log.error("台账恢复，前往额度系统进行额度恢复异常");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                    log.error("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpLoanAppRepayBillRell.getBillNo());
                    throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                }
            }
        }
    }

    /***
     * @param pvpLoanApp
     * @return
     * @author quwen
     * @date 2021/9/10 11:04
     * @version 1.0.0
     * @desc 小企业无还本续贷的业务处理
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void repayLoanHandle(PvpLoanApp pvpLoanApp) {
        int result = 0;
        log.info("判断该笔出账申请: " + pvpLoanApp.getPvpSerno() + " 是否为小企业无还本续贷的业务处理");
        if (CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())) {
            // 获取当前申请借据号作为第二次无还本续贷借据查询数据
            log.info("开始执行小企业无还本续贷: " + pvpLoanApp.getPvpSerno() + " 的业务处理");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("secondBillNo", pvpLoanApp.getBillNo());
            List<PvpRenewLoanInfo> pvpRenewLoanInfoList = pvpRenewLoanInfoService.selectByModel(queryModel);
            // 如果查询数据不为空，则该借据此前做过两次小企业无还本续贷业务，不允许再做，反之则为第一次，执行正常的业务处理
            if (CollectionUtils.nonEmpty(pvpRenewLoanInfoList)) {
                log.error("该笔借据: " + pvpLoanApp.getBillNo() + "已做过两次小企业无还本续贷,不允许再做！");
                throw BizException.error(null, EcbEnum.ECB020038.key, EcbEnum.ECB020038.value);
            }
            PvpRenewLoanInfo pvpRenewLoanInfoData = new PvpRenewLoanInfo();
            // 获取当前登录人信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                pvpRenewLoanInfoData.setInputId(userInfo.getLoginCode());
                pvpRenewLoanInfoData.setInputBrId(userInfo.getOrg().getCode());
                pvpRenewLoanInfoData.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            // 根据出账申请流水查询关联的原借据
            PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = pvpLoanAppRepayBillRellService.selectByPvpSerno(pvpLoanApp.getPvpSerno());
            if (Objects.isNull(pvpLoanAppRepayBillRell)) {
                log.error("该笔小企业无还本续贷出账申请: " + pvpLoanApp.getPvpSerno() + " 未关联借据");
                throw BizException.error(null, EcbEnum.ECB020039.key, EcbEnum.ECB020039.value);
            }
            // 生成无还本续贷信息
            pvpRenewLoanInfoData.setPkId(StringUtils.uuid(true));
            pvpRenewLoanInfoData.setOldBillNo(pvpLoanAppRepayBillRell.getBillNo());
            pvpRenewLoanInfoData.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            pvpRenewLoanInfoData.setCusId(pvpLoanApp.getCusId());
            pvpRenewLoanInfoData.setCusName(pvpLoanApp.getCusName());
            pvpRenewLoanInfoData.setFirstBillNo(pvpLoanApp.getBillNo());
            pvpRenewLoanInfoData.setFirstDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            result = pvpRenewLoanInfoService.insertSelective(pvpRenewLoanInfoData);
            if (result <= 0) {
                log.error("小企业无还本续贷：" + pvpLoanApp.getPvpSerno() + " 新增异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
    }

    /***
     * @param curType  corePrdId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005RespDto>
     * @author 王玉坤
     * @date 2021/6/11 19:38
     * @version 1.0.0
     * @desc 请求核心Ln3005接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<Ln3005RespDto> sendLn3005(String curType, String corePrdId) {
        log.info("前往核心获取产品信息开始,产品编号【{}】,币种【{}】", corePrdId, curType);
        // Ln3005请求对象
        Ln3005ReqDto ln3005ReqDto = null;
        // Ln3005返回对象
        ResultDto<Ln3005RespDto> resultDtoLn3005 = null;
        try {
            // 1、根据产品代码查询核心产品代码
            ln3005ReqDto = new Ln3005ReqDto();

            // 2、拼装ln3005请求报文
            ln3005ReqDto.setChanpdma(corePrdId);
            ln3005ReqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + curType));

            // 3、请求核心获取产品信息
            resultDtoLn3005 = dscms2CoreLnClientService.ln3005(ln3005ReqDto);

            // 4、解析返回信息
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDtoLn3005.getCode())) {
                log.error("返回错误码：{}，错误信息：{}", resultDtoLn3005.getCode(), resultDtoLn3005.getMessage());
                throw BizException.error(null, resultDtoLn3005.getCode(), resultDtoLn3005.getMessage());
            }
        } catch (Exception e) {
            log.error("前往核心获取产品信息异常,产品编号【{}】,币种【{}】", corePrdId, curType, e);
            throw e;
        }
        log.info("前往核心获取产品信息结束,产品编号【{}】,币种【{}】", corePrdId, curType);
        return resultDtoLn3005;
    }

    /***
     * @param pvpAuthorize, accLoan
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Ln3020ReqDto
     * @author 王玉坤
     * @date 2021/6/11 20:25
     * @version 1.0.0
     * @desc 拼组核心请求报文
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Ln3020ReqDto buildLn3020ReqBodyForPTDK(PvpAuthorize pvpAuthorize, AccLoan accLoan, CtrLoanCont ctrLoanCont, CtrHighAmtAgrCont ctrHighAmtAgrCont, CtrAsplDetails ctrAsplDetails, PvpLoanApp pvpLoanApp) throws Exception {
        Ln3020ReqDto ln3020ReqDto = null;
        ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDto = null;
        try {
            log.info("放款流水号【{}】放款报文组装开始", pvpLoanApp.getPvpSerno());
            String catalogId;
            if (Objects.nonNull(ctrAsplDetails)) {
                // 如果是资产池，那就是资产池融资（老信贷票据融资）110021
                catalogId = "110021";
            } else {
                cfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(accLoan.getPrdId());
                if (cfgPrdBasicinfoDto.getData() != null && !"".equals(cfgPrdBasicinfoDto.getData().getCorePrdId())) {
                    catalogId = cfgPrdBasicinfoDto.getData().getCorePrdId();
                } else {
                    //  抛出错误异常
                    throw BizException.error(null, "9999", "未查询到对应核心产品映射");
                }
            }
            //合同金额
            BigDecimal hetongje = BigDecimal.ZERO;
            if (ctrLoanCont == null) {
                if (Objects.isNull(ctrHighAmtAgrCont)) {
                    hetongje = ctrAsplDetails.getContAmt();
                } else {
                    hetongje = ctrHighAmtAgrCont.getAgrAmt();
                }
            } else {
                hetongje = ctrLoanCont.getContAmt();
            }
            // 2.1、请求核心获取产品信息
            ResultDto<Ln3005RespDto> resultDtoLn3005 = this.sendLn3005(accLoan.getContCurType(), catalogId);
            // 2.2、获取返回结构体
            Ln3005RespDto ln3005RespDto = resultDtoLn3005.getData();

            ln3020ReqDto = new Ln3020ReqDto();
            // 贷款借据号
            ln3020ReqDto.setDkjiejuh(pvpAuthorize.getBillNo());
            // 营业机构
            ln3020ReqDto.setYngyjigo(pvpLoanApp.getFinaBrId());
            // 合同编号
            ln3020ReqDto.setHetongbh(pvpAuthorize.getContNo());
            // 账务机构
            ln3020ReqDto.setZhngjigo(pvpLoanApp.getFinaBrId());
            // 节假日顺延
            ln3020ReqDto.setJrlxsybz("0");
            // 货币代号
            ln3020ReqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + pvpAuthorize.getCurType()));
            // 贷款科目
            if ("01".equals(pvpLoanApp.getBelgLine())) {// 小微科目计算
                // PC端计算方式 02--PC端 01--线上（默认手机银行）
                if (Objects.equals("02", pvpLoanApp.getChnlSour())) {
                    log.info("出账流水号【{}】,PC端科目处理逻辑开始！", pvpLoanApp.getPvpSerno());
                    ln3020ReqDto.setKuaijilb(this.getClassSubNoForXw(pvpLoanApp, cfgPrdBasicinfoDto.getData()).getHxAccountClass());
                } else if (Objects.equals("01", pvpLoanApp.getChnlSour())) {
                    log.info("出账流水号【{}】,线上端科目处理逻辑开始！", pvpLoanApp.getPvpSerno());
                    ln3020ReqDto.setKuaijilb(this.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getHxAccountClass());
                } else {
                    log.info("出账流水号【{}】,渠道来源【{}】，默认科目处理逻辑开始！", pvpLoanApp.getPvpSerno());
                    ln3020ReqDto.setKuaijilb(this.getClassSubNoForXw(pvpLoanApp, cfgPrdBasicinfoDto.getData()).getHxAccountClass());
                }
            } else if ("05".equals(pvpLoanApp.getBelgLine()) || "022028".equals(pvpLoanApp.getPrdId())) {//网金 白领易贷通
                ln3020ReqDto.setKuaijilb(this.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getHxAccountClass());//获取科目号
            } else if ("02".equals(pvpLoanApp.getBelgLine()) && !Objects.equals("02", pvpLoanApp.getChnlSour())) {//零售线上产品
                ln3020ReqDto.setKuaijilb(this.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getHxAccountClass());//获取科目号
            } else {
                ln3020ReqDto.setKuaijilb(this.calHxAccountClass(pvpLoanApp.getPvpSerno()).getHxAccountClass());
            }

            //产品代码
            ln3020ReqDto.setChanpdma(catalogId);

            // 本次放款金额
            ln3020ReqDto.setBencfkje(pvpAuthorize.getTranAmt());
            // 客户号
            ln3020ReqDto.setKehuhaoo(pvpAuthorize.getCusId());
            // 客户名称
            ln3020ReqDto.setKehmingc(pvpAuthorize.getCusName());
            // 贷款出账号
            ln3020ReqDto.setDkczhzhh(pvpAuthorize.getBillNo());
            // 贷款入账账号 老信贷根据“是否监管账户”、“连云港机构” 区分 TODO
            ln3020ReqDto.setDkrzhzhh(pvpLoanApp.getLoanPayoutAccno());
            // 贷款入账账号子序号 TODO
            ln3020ReqDto.setDkrzhzxh(pvpLoanApp.getLoanPayoutSubNo());
            // 还款账号 市民贷022097、惠民贷022099、大家e贷022100 取Fldvalue03
            ln3020ReqDto.setHuankzhh(pvpLoanApp.getRepayAccno());
            // 开户操作标志1--录入、2--修改、3--复核、4--直通
            ln3020ReqDto.setDkkhczbz("4");
            // 起息日期
            ln3020ReqDto.setQixiriqi(pvpAuthorize.getTranDate().replace("-", ""));
            // 贷款期限(月)
            ln3020ReqDto.setDkqixian("");
            // 贷款天数
            ln3020ReqDto.setDktiansh("");

            // 利率浮动方式判断 若为浮动利率，目前仅支持LPR加点
            // 利率期限靠档方式判断
            //  02--浮动利率 01--固定利率
            if ("02".equals(pvpLoanApp.getRateAdjMode())) {
                // 00--LPR加点
                if ("00".equals(pvpLoanApp.getIrFloatType())) {
                    // 利率期限靠档方式1--贷款期限、2--利率期限、3--剩余期限
                    ln3020ReqDto.setLlqxkdfs("2");
                    if ("A1".equals(pvpLoanApp.getLprRateIntval())) {// 一年期
                        ln3020ReqDto.setLilvqixx("1Y");// 利率期限
                    } else if (("A2".equals(pvpLoanApp.getLprRateIntval()))) {// 五年期
                        ln3020ReqDto.setLilvqixx("5Y");// 利率期限
                    } else {
                        throw BizException.error("", "9999", "利率期限值有误，请核查！");
                    }
                } else {
                    throw BizException.error("", "9999", "利率浮动方式仅支持LPR加点方式，请核查！");
                }
            } else {
                // 利率期限靠档方式1--贷款期限、2--利率期限、3--剩余期限
                ln3020ReqDto.setLlqxkdfs("1");
                // 利率期限
                ln3020ReqDto.setLilvqixx("");
            }
            // 年/月利率标识:D--日利率\M--月利率\Y--年利率
            ln3020ReqDto.setNyuelilv("Y");
            // 合同利率
            ln3020ReqDto.setHetongll(null);
            // 还款账号子序号
            ln3020ReqDto.setHkzhhzxh(pvpLoanApp.getRepaySubAccno());
            // 备注信息
            ln3020ReqDto.setBeizhuuu("");
            // 合同金额
            ln3020ReqDto.setHetongje(hetongje);
            // 贷款账号
            ln3020ReqDto.setDkzhangh(accLoan.getLoanPayoutAccno());
            // 借据金额
            ln3020ReqDto.setJiejuuje(accLoan.getLoanAmt());
            // 到期日期
            ln3020ReqDto.setDaoqriqi(accLoan.getLoanEndDate().replace("-", ""));
            // 正常利率
            DecimalFormat df = new DecimalFormat("0.000000");
            df.setRoundingMode(RoundingMode.HALF_UP);
            ln3020ReqDto.setZhchlilv(new BigDecimal(df.format(accLoan.getExecRateYear().multiply(new BigDecimal(100)))));
            // 贷款担保方式
            ln3020ReqDto.setDkdbfshi(DicTranEnum.lookup("GUAR_MODE_XDTOHX_" + accLoan.getGuarMode()));
            // 委托存款业务编码
            ln3020ReqDto.setWtckywbm("");
            // 委托人存款账号
            ln3020ReqDto.setWtrckuzh("");
            // 委托人存款账号子序号
            ln3020ReqDto.setWtrckzxh("");
            // 委托人客户号
            ln3020ReqDto.setWtrkehuh("");
            // 委托人名称
            ln3020ReqDto.setWtrmingc("");
            // 委托存款账号
            ln3020ReqDto.setWtckzhao("");
            // 委托存款账号子序号
            ln3020ReqDto.setWtckzixh("");

            // 以下字段来源于LN3005接口返回
            // 贷款对象
            ln3020ReqDto.setDaikduix(ln3005RespDto.getDaikduix());
            // 贷款对象细分
            ln3020ReqDto.setDaikdxxf(ln3005RespDto.getDaikdxxf());
            // 业务分类
            ln3020ReqDto.setYewufenl(ln3005RespDto.getYewufenl());
            // 补贴贷款
            ln3020ReqDto.setButidaik(ln3005RespDto.getButidaik());
            // 循环贷款
            ln3020ReqDto.setXunhdaik(ln3005RespDto.getXunhdaik());
            // 承诺贷款
            ln3020ReqDto.setChendaik(ln3005RespDto.getChendaik());
            // 承诺可循环标志
            ln3020ReqDto.setCnkxhbzh("");
            // 表外产品
            ln3020ReqDto.setBwchapbz(ln3005RespDto.getBwchapbz());
            // 核算方式
            ln3020ReqDto.setHesuanfs(ln3005RespDto.getHesuanfs());
            // 按应计非应计核算
            ln3020ReqDto.setYjfyjhes(ln3005RespDto.getYiyldhes());
            // 按一逾两呆核算
            ln3020ReqDto.setYiyldhes(ln3005RespDto.getYiyldhes());
            // 形态分科目核算
            ln3020ReqDto.setDkxtkmhs(ln3005RespDto.getDkxtkmhs());
            // 允许贷款展期
            ln3020ReqDto.setYunxdkzq(ln3005RespDto.getYunxdkzq());
            // 展期最大次数
            ln3020ReqDto.setZhqzdcsh(ln3005RespDto.getZhqzdcsh().toString());

            // 可售产品代码
            ln3020ReqDto.setKshchpdm(accLoan.getPrdId());
            // 可售产品名称
            ln3020ReqDto.setKshchpmc(accLoan.getPrdName());
            // 贷款用途 TODO
            ln3020ReqDto.setDkyongtu("");
            // 表外核1码 TODO
            ln3020ReqDto.setBwhesdma("");
            // 放款记账方式：0--不过借款人存款户 、1--过借款人存款户
            ln3020ReqDto.setFkjzhfsh("1");
            // 允许对行内同名账户放款
            ln3020ReqDto.setHntmifku("");
            // 允许对行内非同名账户放款
            ln3020ReqDto.setHnftmfku("");
            // 允许对内部账户放款
            ln3020ReqDto.setNeibufku("");
            // 放款类型：1--多次放款 、2--单次放款
            ln3020ReqDto.setFangkulx("2");
            // 放款借据管理模式
            ln3020ReqDto.setZdfkjjms("");
            // 周期性放款标志
            ln3020ReqDto.setZhqifkbz("");
            // 放款周期
            ln3020ReqDto.setFkzhouqi("");
            // 放款金额方式
            ln3020ReqDto.setFkfangsh("");
            // 每次放款金额或比例
            ln3020ReqDto.setMcfkjebl(null);
            // 借新还旧控制
            ln3020ReqDto.setJxhjdkkz("");
            // 借新还旧还款控制
            ln3020ReqDto.setJxhjhkkz("");
            // 贸融ABS贷款类型
            ln3020ReqDto.setAbsdkulx("");

            // 贷款形式 1新增贷款 2收回再贷 3借新还旧 4资产重组 5转入 6无还本续贷 8小企业无还本续贷 9其他
            // TODO 缺少优转续贷
            String loanModal = pvpLoanApp.getLoanModal();
            //如果贷款形式为3-借新还旧，需查询借新还旧信息  （贷款形式 ：1-新增、3-借新还旧、4-无缝对接）
            // 核心还旧类型:0--借新还旧、1--无本续贷、2--优转续贷
            ln3020ReqDto.setHjiuleix(DicTranEnum.lookup("LOAN_MODAL_XDTOHX_" + loanModal));

            // 暂不知从何获取 如果贷款形式为 3-借新还旧，需查询借新还旧信息  老信贷从Pvp_Jxhj_Repay_Loan 借新还旧借据还款试算表 获取
            List<Lstydkjjh> lstydkjjhList = this.queryPvpJxhjRepayLoan(pvpLoanApp);
            ln3020ReqDto.setLstydkjjh(lstydkjjhList);

            // 定制还款计划 1--是、0--否
            ln3020ReqDto.setDzhifkjh("");
            List<Lstdkfkjh> lstdkfkjhList = new ArrayList<>();
            Lstdkfkjh lstdkfkjh = new Lstdkfkjh();
            // 放款日期
            lstdkfkjh.setFkriqiii("");
            // 放款金额
            lstdkfkjh.setFkjineee(null);
            // 贷款入账账号
            lstdkfkjh.setDkrzhzhh("");
            // 贷款入账账号子序号
            lstdkfkjh.setDkrzhzxh("");

            lstdkfkjhList.add(lstdkfkjh);
            ln3020ReqDto.setLstdkfkjh(lstdkfkjhList);

            // 允许特殊放款标志；0--不允许、1--允许
            ln3020ReqDto.setYxtsfkbz("");
            // 正常本金
            ln3020ReqDto.setZhchbjin(BigDecimal.ZERO);
            // 逾期本金
            ln3020ReqDto.setYuqibjin(BigDecimal.ZERO);
            // 呆滞本金
            ln3020ReqDto.setDzhibjin(BigDecimal.ZERO);
            // 呆账本金
            ln3020ReqDto.setDaizbjin(BigDecimal.ZERO);
            // 应收应计利息
            ln3020ReqDto.setYsyjlixi(BigDecimal.ZERO);
            // 催收应计利息
            ln3020ReqDto.setCsyjlixi(BigDecimal.ZERO);
            // 应收欠息
            ln3020ReqDto.setYsqianxi(BigDecimal.ZERO);
            // 催收欠息
            ln3020ReqDto.setCsqianxi(BigDecimal.ZERO);
            // 应收应计罚息
            ln3020ReqDto.setYsyjfaxi(BigDecimal.ZERO);
            // 催收应计罚息
            ln3020ReqDto.setCsyjfaxi(BigDecimal.ZERO);
            // 应收罚息
            ln3020ReqDto.setYshofaxi(BigDecimal.ZERO);
            // 催收罚息
            ln3020ReqDto.setCshofaxi(BigDecimal.ZERO);
            // 应计复息
            ln3020ReqDto.setYingjifx(BigDecimal.ZERO);
            // 复息
            ln3020ReqDto.setFuxiiiii(BigDecimal.ZERO);
            // 应计贴息
            ln3020ReqDto.setYingjitx(BigDecimal.ZERO);
            // 应收贴息
            ln3020ReqDto.setYingshtx(BigDecimal.ZERO);
            // 应收费用
            ln3020ReqDto.setYingshfy(BigDecimal.ZERO);
            // 应收罚金
            ln3020ReqDto.setYingshfj(BigDecimal.ZERO);
            // 合作方编号
            ln3020ReqDto.setHezuofbh("");
            // 合作方名称
            ln3020ReqDto.setHezuofmc("");
            // 受托支付业务编码
            ln3020ReqDto.setShtzfhxm("");

            /*******************贷款受托支付列表开始**********************/
            // 受托支付标识 1-是 0-否 光伏贷默认是022098
            // todo 个人一手住房按揭贷款 常熟个人一手住房按揭贷款（资金托管） 是否我行资金监管
            String isBeEntrustedPay = pvpLoanApp.getIsBeEntrustedPay();
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isBeEntrustedPay)) {
                //放款资金处理方式0--自主支付、1--转卖方、2--转待销账、3--临时冻结、4--受托支付
                ln3020ReqDto.setFkzjclfs("4");

                // 查询受托支付账户信息
                log.info("根据放款流水号【{}】查询交易对手信息开始", pvpLoanApp.getPvpSerno());
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("bizSerno", pvpLoanApp.getPvpSerno());
                List<ToppAcctSub> toppAcctSubs = toppAcctSubService.selectAll(queryModel);
                log.info("根据放款流水号【{}】查询交易对手信息结束", pvpLoanApp.getPvpSerno());

                // 贷款受托支付列表
                List<Lstdkstzf> lstdkstzflist = new ArrayList<>();
                if (toppAcctSubs != null && toppAcctSubs.size() > 0) {
                    log.info("根据放款流水号【{}】查询交易对手信息结束，拼装交易对手信息开始", pvpLoanApp.getPvpSerno());

                    // 拼装交易对手列表信息
                    toppAcctSubs.stream().forEach(s -> {
                        // 交易对手单条信息
                        Lstdkstzf lstdkstzf = new Lstdkstzf();

                        // 发布系统/渠道  MBL(个人网银)：表示线上 NNT：表示线下
                        if ("1".equals(s.getIsOnline())) {
                            lstdkstzf.setQudaohao("NET");
                        } else {
                            lstdkstzf.setQudaohao("NNT");
                        }
                        // 受托金额
                        lstdkstzf.setStzfjine(s.getToppAmt());
                        // 资金来源账号
                        lstdkstzf.setZjlyzhao("");
                        // 资金来源账号子序号
                        lstdkstzf.setZjlyzzxh("");
                        // 资金来源账号名称
                        lstdkstzf.setZjlyzhmc("");
                        // 资金转入账号
                        lstdkstzf.setZjzrzhao("");
                        // 资金转入账号子序号
                        lstdkstzf.setZjzrzzxh("");
                        // 资金转入账号名称
                        lstdkstzf.setZjzrzhmc("");
                        // 是否本行账户 核心字典项 1--本行账号 2--他行账户
                        lstdkstzf.setDfzhhzhl("1".equals(s.getIsBankAcct()) ? "1" : "2");
                        // 对方账号开户行
                        lstdkstzf.setDfzhhkhh(s.getAcctsvcrNo());
                        // 对方账号开户行名
                        lstdkstzf.setDfzhkhhm(s.getAcctsvcrName());
                        // 对方账号
                        lstdkstzf.setDfzhangh(s.getToppAcctNo());

                        // 根据是否本行账户查询子账户信息,若为对公账户获取子账户序号
                        if ("1".equals(s.getIsBankAcct())) { // 本行
                            // 查询子账户信息
                            Dp2021RespDto dp2021RespDto = this.sendDp2021(s.getToppAcctNo());
                            // 遍历返回信息
                            List<Lstacctinfo> lstacctinfoList = dp2021RespDto.getLstacctinfo();
                            // 核心对公一般只有一条，对私可能会有多条， 直接取 对应币种的活期子户 的第一条数据
                            if (lstacctinfoList != null && lstacctinfoList.size() > 0) {
                                // 子账户序号
                                String zzhxh = "";
                                // 遍历子账户列表，获取序号为00001的序号
                                for (Lstacctinfo lstacctinfo : lstacctinfoList) {
                                    // 账户状态为A--正常 且客户账户类型为0--对公账户，则
                                    if ("A".equals(lstacctinfo.getZhhuztai()) && "0".equals(lstacctinfo.getKehuzhlx())) {
                                        // 账户序号为”00001“
                                        if ("00001".equals(lstacctinfo.getZhhaoxuh())) {
                                            zzhxh = lstacctinfo.getZhhaoxuh();
                                            break;
                                        }
                                    }

                                    // 账户状态为J--控制 且客户账户类型为0--对公账户，则
                                    if ("".equals(zzhxh)) {
                                        if ("J".equals(lstacctinfo.getZhhuztai()) && "0".equals(lstacctinfo.getKehuzhlx())) {
                                            zzhxh = lstacctinfo.getZhhaoxuh();
                                        }
                                    }
                                }

                                // TODO 存量房专用账户到中间业务查 目前缺少个人二手房资金托管新增信息表 Pvp_Esftuoguan_Info
                                if ("801000006838588".equals(s.getToppAcctNo())) {

//                                    KeyedCollection kc = dao.queryFirst("PvpEsftuoguanInfo", null, " WHERE pvp_serno = '" + pa.getSerno() + "'", connection);
//                                    KeyedCollection reqPkgclf = new KeyedCollection();
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "prcscd","clfxcx");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servtp","XDG");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "userid","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "brchno","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "datasq",UUID.randomUUID().toString().replaceAll("-", ""));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servsq",UUID.randomUUID().toString().replaceAll("-", ""));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servdt",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servti",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "ipaddr","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "mac","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "xybhsq", KeyedCollectionUtil.getDataValueAsString(kc, "tgxy_no"));//协议号
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "fkzjh", KeyedCollectionUtil.getDataValueAsString(kc, "buyer_certcode"));//证件号
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "fkmc", KeyedCollectionUtil.getDataValueAsString(kc, "buyer_name"));//买方名称
//                                    KeyedCollection res = tradeComponent.call8HeadFormate("clfxcx", reqPkgclf, TradePubConstant.toEsb);
//
//                                    zxh=KeyedCollectionUtil.getDataValueAsString(res, "zzhxh");
//                                    if(zxh==null||"".equals(zxh)){
//                                        throw new EMPException("存量房专用账户发送中间业务查询子账户失败："+ KeyedCollectionUtil.getDataValueAsString(res, "erortx"));
//                                    }
                                    PvpSehandEsInfo pvpSehandEsInfo = pvpSehandEsInfoService.selectByPrimaryKey(pvpLoanApp.getPvpSerno());
                                    if (pvpSehandEsInfo != null) {
                                        log.info("协议信息查询开始【{}】", pvpLoanApp.getPvpSerno());
                                        ClfxcxReqDto reqDto = new ClfxcxReqDto();
                                        reqDto.setFkmc(pvpSehandEsInfo.getBuyerName());//买方名称
                                        reqDto.setFkzjh(pvpSehandEsInfo.getBuyerCertCode());//证件号
                                        reqDto.setXybhsq(pvpSehandEsInfo.getTgxyNo());//协议号
                                        ResultDto<ClfxcxRespDto> resultDto = null;
                                        try {
                                            log.info("协议信息请求报文：", reqDto.toString());
                                            resultDto = dscms2ZjywxtClientService.clfxcx(reqDto);
                                            log.info("协议信息返回报文：", resultDto.getData().toString());
                                        } catch (Exception e) {
                                            throw BizException.error(null, "9999", "协议信息查询失败:" + e.getMessage());
                                        }

                                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDto.getCode())) {
                                            zzhxh = resultDto.getData().getZzhxh();
                                            if (zzhxh == null || "".equals(zzhxh)) {
                                                throw BizException.error(null, "9999", "存量房专用账户发送中间业务查询子账户失败");
                                            }
                                        } else {
                                            throw BizException.error(null, "9999", "存量房专用账户发送中间业务查询子账户失败" + resultDto.getMessage());
                                        }
                                        log.info("协议信息查询结束【{}】", pvpLoanApp.getPvpSerno());
                                    }
                                }
                                // 对方账号子序号
                                lstdkstzf.setDfzhhzxh(zzhxh);
                            } else {
                                // 若没有查询到对方账户子账户序号，则默认空
                                lstdkstzf.setDfzhhzxh("");
                            }
                        } else {
                            // 非本行账户，对方账号子序号默认为空
                            lstdkstzf.setDfzhhzxh("");
                        }
                        // 对方账户名称
                        lstdkstzf.setDfzhhmch(s.getToppName());
                        // 受托支付方式：1--手工支付、2--自动支付、3--立即支付
                        // 本行账号立即支付，它行账号手工支付
                        lstdkstzf.setStzffshi("1".equals(s.getIsBankAcct()) ? "3" : "1");
                        // 受托支付日期
                        lstdkstzf.setStzfriqi("");
                        // 受托支付处理状态
                        lstdkstzf.setStzfclzt("");
                        // 备注
                        lstdkstzf.setBeizhuxx("");

                        // 小贷塞放贷机构到备注，结尾改为1
                        if ("016001".equals(pvpAuthorize.getDisbOrgNo()) || pvpAuthorize.getDisbOrgNo().endsWith("6")) {
                            // 获取放贷机构
                            String fdjg = pvpAuthorize.getFldvalue41();
                            if ((fdjg != null) && (fdjg.length() > 0)) {
                                lstdkstzf.setBeizhuxx(fdjg.substring(0, 5).concat("1"));
                            }
                        }

                        // 受托比例
                        lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                        // TODO 若是 我行监管账户 而且机构为连云港营业部管理部
                        try {
                            if ("067000".equals(pvpLoanApp.getManagerId()) && "1".equals(pvpLoanApp.getIsLocalManag())) {
                                lstdkstzf.setDfzhhzxh("00001");// 对方账号子序号
                                lstdkstzf.setDfzhhmch("连云港市房地产交易管理服务有限公司");//// 对方账号名称
                                lstdkstzf.setDfzhangh("802000050093188");//对方账号名称
                            }
                        } catch (Exception e) {
                            log.info(e.getMessage());
                        }
                        lstdkstzflist.add(lstdkstzf);
                    });

                    //  TODO 未查询到对方账户子序号信息还是没有交易对手信息走这段 这段逻辑是不是有问题
                    if (lstdkstzflist == null || lstdkstzflist.size() == 0) {
                        Lstdkstzf lstdkstzf = new Lstdkstzf();
                        // 发布系统/渠道
                        lstdkstzf.setQudaohao("");
                        // 受托金额
                        lstdkstzf.setStzfjine(BigDecimal.ZERO);
                        // 资金来源账号
                        lstdkstzf.setZjlyzhao("");
                        // 资金来源账号子序号
                        lstdkstzf.setZjlyzzxh("");
                        // 资金来源账号名称
                        lstdkstzf.setZjlyzhmc("");
                        // 资金转入账号
                        lstdkstzf.setZjzrzhao("");
                        // 资金转入账号子序号
                        lstdkstzf.setZjzrzzxh("");
                        // 资金转入账号名称
                        lstdkstzf.setZjzrzhmc("");
                        // 对方账号种类
                        lstdkstzf.setDfzhhzhl("");
                        // 对方账号开户行
                        lstdkstzf.setDfzhhkhh("");
                        // 对方账号开户行名
                        lstdkstzf.setDfzhkhhm("");
                        // 对方账号
                        lstdkstzf.setDfzhangh("");
                        // 对方账号子序号
                        lstdkstzf.setDfzhhzxh("");
                        // 对方账号名称
                        lstdkstzf.setDfzhhmch("");
                        // 受托支付方式
                        lstdkstzf.setStzffshi("");
                        // 受托支付日期
                        lstdkstzf.setStzfriqi("");
                        // 受托支付处理状态
                        lstdkstzf.setStzfclzt("");
                        // 备注
                        lstdkstzf.setBeizhuxx("");
                        // 受托比例
                        lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                        lstdkstzflist.add(lstdkstzf);
                    }
                }
                ln3020ReqDto.setLstdkstzf(lstdkstzflist);
            } else {// 非受托支付处理方式
                ln3020ReqDto.setFkzjclfs("0");// 放款资金处理方式

                Lstdkstzf lstdkstzf = new Lstdkstzf();
                List<Lstdkstzf> lstdkstzflist = new LinkedList<>();
                // 发布系统/渠道
                lstdkstzf.setQudaohao("");
                // 受托金额
                lstdkstzf.setStzfjine(BigDecimal.ZERO);
                // 资金来源账号
                lstdkstzf.setZjlyzhao("");
                //资金来源账号子序号
                lstdkstzf.setZjlyzzxh("");
                // 资金来源账号名称
                lstdkstzf.setZjlyzhmc("");
                // 资金转入账号
                lstdkstzf.setZjzrzhao("");
                // 资金转入账号子序号
                lstdkstzf.setZjzrzzxh("");
                // 资金转入账号名称
                lstdkstzf.setZjzrzhmc("");
                // 对方账号种类
                lstdkstzf.setDfzhhzhl("");
                // 对方账号开户行
                lstdkstzf.setDfzhhkhh("");
                // 对方账号开户行名
                lstdkstzf.setDfzhkhhm("");
                // 对方账号
                lstdkstzf.setDfzhangh("");
                // 对方账号子序号
                lstdkstzf.setDfzhhzxh("");
                // 对方账号名称
                lstdkstzf.setDfzhhmch("");
                // 受托支付方式
                lstdkstzf.setStzffshi("");
                // 受托支付日期
                lstdkstzf.setStzfriqi("");
                // 受托支付处理状态
                lstdkstzf.setStzfclzt("");
                // 备注
                lstdkstzf.setBeizhuxx("");
                // 受托比例
                lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                lstdkstzflist.add(lstdkstzf);
                ln3020ReqDto.setLstdkstzf(lstdkstzflist);
            }
            /*******************贷款受托支付列表结束**********************/

            //计息本金规则
            ln3020ReqDto.setJixibjgz("");
            //利息计算方法
            ln3020ReqDto.setLixijsff("");
            //计息头尾规则
            ln3020ReqDto.setJixitwgz("");
            //计息最小金额
            ln3020ReqDto.setJixizxje(new BigDecimal("0"));
            //计息舍入规则
            ln3020ReqDto.setJixisrgz("");
            //舍入最小单位
            ln3020ReqDto.setSrzxdanw("");
            //分段计息标志
            ln3020ReqDto.setFdjixibz("");
            //早起息标志
            ln3020ReqDto.setZaoqixbz("");
            //晚起息标志
            ln3020ReqDto.setWanqixbz("");
            //起息天数
            ln3020ReqDto.setQixitshu("");

            // 利率类型 核心字典项1--PBOC、2--SHIBOR、3--HIBOR、4--LIBOR、5--SIBOR、6--固定利率
            if (StringUtils.isEmpty(accLoan.getRateAdjMode())) {
                throw BizException.error("", "9999", "利率调整方式不能为空！");
            }
            ln3020ReqDto.setLilvleix(DicTranEnum.lookup("RATE_ADJ_MODE_XDTOHX_" + accLoan.getRateAdjMode()));
            // 利率调整周期
            ln3020ReqDto.setLilvtzzq("");

            // 根据利率类型填入利率相关参数 1--浮动利率 6--固定利率
            if ("1".equals(ln3020ReqDto.getLilvleix())) { // 非固定利率
                // 根据利率浮动方式填入参数
                if ("00".equals(pvpLoanApp.getIrFloatType())) {// LPR加点
                    // 利率浮动方式 1-按值浮动
                    ln3020ReqDto.setLilvfdfs("1");
                    // 正常利率编号
                    ln3020ReqDto.setZclilvbh("LPR1");
                    // 利率浮动值
                    ln3020ReqDto.setLilvfdzh(accLoan.getRateFloatPoint());
                } else {
                    log.info("利率浮动方式错误:" + pvpLoanApp.getIrFloatType());
                    throw BizException.error("", "", "该笔出账业务的利率浮动方式错误,请检查!");
                }

                if ("IMM".equals(accLoan.getRateAdjType())) {// 立即调整
                    log.info("利率调整模式【{}】", "立即调整");
                    ln3020ReqDto.setLilvtzfs("4");
                } else if ("DDA".equals(accLoan.getRateAdjType())) {// 满一年调整
                    log.info("利率调整模式【{}】", "满一年调整");
                    ln3020ReqDto.setLilvtzfs("1");
                } else if ("NYF".equals(accLoan.getRateAdjType())) {// 每年1.1调整
                    log.info("利率调整模式【{}】", "每年1.1调整");
                    ln3020ReqDto.setLilvtzfs("2");
                    // 利率调整周期
                    ln3020ReqDto.setLilvtzzq("1YAF");
                } else if ("FIX".equals(accLoan.getRateAdjType())) {// 固定日
                    ln3020ReqDto.setLilvtzfs("2");
                    // 下一次利率调整间隔单位
                    String unit = accLoan.getNextRateAdjUnit();
                    // 下一次利率调整间隔
                    String freq = accLoan.getNextRateAdjInterval();
                    // 第一次调整日
                    String frd = accLoan.getFirstAdjDate();

                    // 计算调整日期
                    // 按月
                    if ("M".equals(unit)) {
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(8, 10));
                    } else if ("Q".equals(unit)) {// 按季
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(8, 10) + "E");
                    } else if ("Y".equals(unit)) {// 按年
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(5, 7) + frd.substring(8, 10));
                    }
                } else if ("FIA".equals(accLoan.getRateAdjType())) {//按月
                    ln3020ReqDto.setLilvtzfs("2");
                    ln3020ReqDto.setLilvtzzq("1MA01");
                } else {
                    ln3020ReqDto.setLilvtzfs("");
                    log.info("利率调整选项错误:" + pvpLoanApp.getIrFloatType());
                    throw BizException.error("", "", "暂不支持该利率调整选项,请检查!");
                }
            } else if ("6".equals(ln3020ReqDto.getLilvleix())) {// 固定利率填充 1--浮动利率 6--固定利率
                // 正常利率编号
                ln3020ReqDto.setZclilvbh("LPR1");
                //利率类型选择固定利率时，利率浮动方式必须选择不浮动
                ln3020ReqDto.setLilvfdfs("0");
                //利率类型选择固定利率时，利率调整方式必须选择不调整
                // 利率调整方式:0--不调整、1--按对月对日调整、2--按指定周期调整、3--按次月对日调整、4--即时调整、5--次月发放日调整
                ln3020ReqDto.setLilvtzfs("0");
                // 利率浮动值
                ln3020ReqDto.setLilvfdzh(accLoan.getRateFloatPoint());
            } else {
                throw BizException.error("", "9999", "利率调整方式不符合规范，请核查！");
            }

            // 核心基本信息 取基准利率参数之一，利率类型为非6--固定利率时必输
            if ("1".equals(ln3020ReqDto.getLilvleix())) {
                // 逾期利率编号
                ln3020ReqDto.setYuqillbh("LPR1");
                // 逾期罚息浮动方式0-不浮动、1-按值浮动、2-按比例浮动
                ln3020ReqDto.setYqfxfdfs("2");
                // 逾期利率调整方式 与正常利率保持一致
                ln3020ReqDto.setYuqitzfs(ln3020ReqDto.getLilvtzfs());

                // 复利利率编号
                ln3020ReqDto.setFulilvbh("LPR1");
                // 复利利率浮动方式
                ln3020ReqDto.setFulifdfs("2");
                // 复利利率调整方式 与正常利率保持一致
                ln3020ReqDto.setFulitzfs(ln3020ReqDto.getLilvtzfs());
            } else if ("6".equals(ln3020ReqDto.getLilvleix())) { // 固定利率填充
                // 逾期利率编号
                ln3020ReqDto.setYuqillbh("LPR1");
                // 逾期罚息浮动方式 2--按比例浮动
                ln3020ReqDto.setYqfxfdfs("2");
                // 复利利率编号
                ln3020ReqDto.setFulilvbh("LPR1");
                // 复利利率浮动方式 2--按比例浮动
                ln3020ReqDto.setFulifdfs("2");
                // 逾期利率调整方式 0--不调整
                ln3020ReqDto.setYuqitzfs("0");
                // 复利利率调整方式 0--不调整
                ln3020ReqDto.setFulitzfs("0");
            } else {
                throw BizException.error("", "9999", "利率调整方式不符合规范，请核查！");
            }

            // 逾期利率参考类型 1--基准利率、2--正常利率、3--指定利率
            ln3020ReqDto.setYqllcklx("2");
            // 逾期罚息浮动值--逾期利率浮动比例
            ln3020ReqDto.setYqfxfdzh(new BigDecimal(df.format(pvpLoanApp.getOverdueRatePefloat().multiply(new BigDecimal("100")))));
            // 逾期利率
            ln3020ReqDto.setYuqililv(new BigDecimal(df.format(pvpLoanApp.getOverdueExecRate().multiply(new BigDecimal("100")))));
            // 逾期利率调整周期
            ln3020ReqDto.setYuqitzzq(ln3020ReqDto.getLilvtzzq());
            // 逾期年月利标识
            ln3020ReqDto.setYuqinyll("");

            // 复利利率参考类型 1--基准利率、2--正常利率、3--指定利率
            ln3020ReqDto.setFlllcklx("2");
            //复利利率浮动值--取复息利率浮动比例
            ln3020ReqDto.setFulifdzh(new BigDecimal(df.format(pvpLoanApp.getCiRatePefloat().multiply(new BigDecimal("100")))));
            // 复利利率
            ln3020ReqDto.setFulililv(new BigDecimal(df.format(pvpLoanApp.getCiExecRate().multiply(new BigDecimal("100")))));
            // 复利利率调整周期
            ln3020ReqDto.setFulitzzq(ln3020ReqDto.getLilvtzzq());
            // 复利月利率标识
            ln3020ReqDto.setFulilvny("");

            // 计息规则
            ln3020ReqDto.setJixiguiz("");
            // 零头计息规则
            ln3020ReqDto.setLtjixigz("");
            // 计息标志 0--不计息 1--计息
            ln3020ReqDto.setJixibzhi("");
            // 计复息标识
            ln3020ReqDto.setJfxibzhi("");
            // 复息计息标志
            ln3020ReqDto.setFxjxbzhi("");
            // 计提规则
            ln3020ReqDto.setJitiguiz("");
            // 预收息方式
            ln3020ReqDto.setYushxfsh("");
            // 预收息总额
            ln3020ReqDto.setYushxize(new BigDecimal("0"));
            // 预收息扣息来源账号
            ln3020ReqDto.setYsxlyzhh("");
            // 预收息扣息账号序号
            ln3020ReqDto.setYsxlyzxh("");
            // 利息摊销周期
            ln3020ReqDto.setLixitxzq("");
            // 每次摊销方式
            ln3020ReqDto.setMeictxfs("");
            // 每次摊销比例
            ln3020ReqDto.setMeictxbl(new BigDecimal("0"));
            // 贴息标志
            ln3020ReqDto.setTiexibzh("");
            // 补贴金额计算方式
            ln3020ReqDto.setButijejs("");
            // 首次还款日模式
            ln3020ReqDto.setScihkrbz("");

            // 借e点特殊判断
            if ("022101".equals(accLoan.getPrdId())) {
                if (Objects.equals("A002", accLoan.getRepayMode())) {// 等额本息
                    ln3020ReqDto.setScihkrbz("2"); //首次还款日模式
                } else {// 其余还款方式
                    ln3020ReqDto.setScihkrbz("1"); //首次还款日模式
                }
            }

            // 还款相关参数配置
            // 小贷特殊还款方式
            if ("01".equals(accLoan.getBelgLine()) && pvpLoanApp.getSurveySerno() != null) {
                if ("A022".equals(accLoan.getRepayMode())) {//10年期等额本息
                    //是否特殊贷款标志 1--是 0--否
                    ln3020ReqDto.setSftsdkbz("1");
                    // 特殊贷款计息总期数
                    ln3020ReqDto.setTsdkjxqs("240");
                } else if ("A031".equals(accLoan.getRepayMode())) {// 5年期等额本息
                    //是否特殊贷款标志 1--是 0--否
                    ln3020ReqDto.setSftsdkbz("1");
                    // 特殊贷款计息总期数
                    ln3020ReqDto.setTsdkjxqs("120");
                } else {
                    //是否特殊贷款标志 1--是 0--否
                    ln3020ReqDto.setSftsdkbz("0");
                    // 特殊贷款计息总期数
                    ln3020ReqDto.setTsdkjxqs("");
                }
            } else {
                //是否特殊贷款标志 1--是 0--否
                ln3020ReqDto.setSftsdkbz("0");
                // 特殊贷款计息总期数
                ln3020ReqDto.setTsdkjxqs("");
            }

            /******************贷款利率分段计息参数******************/
            // 1、先获取“是否免息”、“免息天数信息” 小微公众号项目免息需求,苏宁智能营销 白领贷免息 大家e贷
            // 是否免息天数
            boolean isLilvfend = false;
            // 免息天数
            int freeDays = 0;
            if ("01".equals(accLoan.getBelgLine()) || "022100".equals(accLoan.getPrdId()) || "022028".equals(accLoan.getPrdId())) {
                log.info("-----------------------借据号【{}】，获取免息天数信息开始----------", accLoan.getBillNo());
                AccLoanFreeInter accLoanFreeInter = accLoanFreeInterService.selectByPrimaryKey(accLoan.getBillNo());
                if (Objects.isNull(accLoanFreeInter)) {
                    log.info("-----------------------借据号【{}】，获取免息天数信息结束，未查询到免息信息！----------", accLoan.getBillNo());
                } else {
                    isLilvfend = true;
                    if (accLoanFreeInter.getFreeInterDay() != null) {
                        freeDays = accLoanFreeInter.getFreeInterDay();
                    }
                    log.info("-----------------------借据号【{}】，获取免息天数信息结束，免息天数【{}】----------", accLoan.getBillNo(), freeDays);
                }
            }

            // 2、获取是否分段计息标识
            String lilvfend = pvpLoanApp.getIsSegInterest();
            // 分段计息列表信息
            List<Lstdkllfd> lstdkllfdList = new ArrayList<>();
            // 根据分段计息标识、是否免息判断
            if ("1".equals(lilvfend)) {// 分段计息标识为“是”
                // 利率分段计息标识
                ln3020ReqDto.setLilvfend(lilvfend);

                // 分段计息查询结果进行取值
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("loanPvpSeq", pvpLoanApp.getPvpSerno());
                List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubService.querySegInterstSub(queryModel);
                if (list != null && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub = list.get(i);
                        Lstdkllfd lstdkllfd = new Lstdkllfd();
                        lstdkllfd.setLilvtzzq("");
                        String idMode = toConverRate(pvpLoanAppSegInterstSub.getRateAdjMode());
                        lstdkllfd.setXuhaoooo(i + 1);
                        lstdkllfd.setLilvfdfs("0");// 利率浮动方式
                        lstdkllfd.setLilvleix("6");// 利率类型
                        lstdkllfd.setLilvtzfs("0");// 利率调整方式
                        lstdkllfd.setQishriqi(pvpLoanAppSegInterstSub.getSegStartdate().replace("-", ""));// 起始日期
                        lstdkllfd.setDaoqriqi(pvpLoanAppSegInterstSub.getSegEnddate().replace("-", ""));// 到期日期
                        lstdkllfd.setLilvfdzh(pvpLoanAppSegInterstSub.getRateFloatPoint());// 利率浮动值--取利率浮动比例
                        lstdkllfd.setZhchlilv(pvpLoanAppSegInterstSub.getExecRateYear());// 正常利率
                        lstdkllfd.setLilvtzzq("");// 利率调整周期
                        lstdkllfd.setZclilvbh("");// 正常利率编号
                        lstdkllfdList.add(lstdkllfd);
                    }
                    ln3020ReqDto.setLstdkllfd(lstdkllfdList);
                }
            } else if (isLilvfend) { // 若免息
                // 利率分段计息标识 1--是 0--否
                ln3020ReqDto.setLilvfend("1");

                // 贷款利率分段信息
                Lstdkllfd lstdkllfdFirst = new Lstdkllfd();
                // 序号
                lstdkllfdFirst.setXuhaoooo(1);
                // 起始日期
                lstdkllfdFirst.setQishriqi(pvpAuthorize.getTranDate().replace("-", ""));
                LocalDate freeEndDate = LocalDate.parse(pvpAuthorize.getTranDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).plusDays(freeDays);
                log.info("借据号【{}】,根据免息天数【{}】，出账日期【{}】，计算免息到期日结束，到期日【{}】", pvpAuthorize.getBillNo(),
                        freeDays, pvpAuthorize.getTranDate(), freeEndDate.toString());
                // 判断免息截止日期是否大于借据截止日
                boolean secordFlag = true;
                if (freeEndDate.compareTo(LocalDate.parse(accLoan.getLoanEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"))) >= 0) {
                    secordFlag = false;
                    // 到期日期 若免息截止日期大于借据截止日 ，取借据截止日期，反之取免息截止日
                    lstdkllfdFirst.setDaoqriqi(accLoan.getLoanEndDate().replace("-", ""));
                } else {
                    // 到期日期
                    lstdkllfdFirst.setDaoqriqi(freeEndDate.toString().replace("-", ""));
                }
                // 利率浮动方式 0--不浮动
                lstdkllfdFirst.setLilvfdfs("0");
                // 利率类型 6--固定利率
                lstdkllfdFirst.setLilvleix("6");
                // 利率调整方式 0--不调整
                lstdkllfdFirst.setLilvtzfs("0");
                // 利率浮动值
                lstdkllfdFirst.setLilvfdzh(BigDecimal.ZERO);
                // 正常利率
                lstdkllfdFirst.setZhchlilv(BigDecimal.ZERO);
                // 利率调整周期
                lstdkllfdFirst.setLilvtzzq("");
                // 正常利率编号
                lstdkllfdFirst.setZclilvbh("");

                lstdkllfdList.add(lstdkllfdFirst);

                // 免息截止日期在贷款到期日之内 分两段计息
                if (secordFlag) {
                    // 第二段免息贷款利率分段
                    Lstdkllfd lstdkllfdSecond = new Lstdkllfd();
                    // 序号
                    lstdkllfdSecond.setXuhaoooo(2);
                    // 起始日期 为免息到期日期
                    lstdkllfdSecond.setQishriqi(freeEndDate.toString().replace("-", ""));
                    // 到期日期 为借据到期日期
                    lstdkllfdSecond.setDaoqriqi(accLoan.getLoanEndDate().replace("-", ""));
                    // 利率浮动方式 0--不浮动
                    lstdkllfdSecond.setLilvfdfs("0");
                    // 利率类型 6--固定利率
                    lstdkllfdSecond.setLilvleix(ln3020ReqDto.getLilvleix());
                    // 利率调整方式 0--不调整
                    lstdkllfdSecond.setLilvtzfs("0");
                    // 利率浮动值
                    lstdkllfdSecond.setLilvfdzh(BigDecimal.ZERO);
                    // 正常利率
                    lstdkllfdSecond.setZhchlilv(ln3020ReqDto.getZhchlilv());
                    // 利率调整周期
                    lstdkllfdSecond.setLilvtzzq("");
                    // 正常利率编号
                    lstdkllfdSecond.setZclilvbh("");

                    lstdkllfdList.add(lstdkllfdSecond);
                }
                ln3020ReqDto.setLstdkllfd(lstdkllfdList);
            } else { // 无分段计息与免息
                // 利率分段计息标识 1--是 0--否
                ln3020ReqDto.setLilvfend("0");
                // 第二段免息贷款利率分段
                Lstdkllfd lstdkllfd = new Lstdkllfd();
                // 序号
                lstdkllfd.setXuhaoooo(null);
                // 起始日期 为免息到期日期
                lstdkllfd.setQishriqi("");
                // 到期日期 为借据到期日期
                lstdkllfd.setDaoqriqi("");
                // 利率浮动方式 0--不浮动
                lstdkllfd.setLilvfdfs("");
                // 利率类型 6--固定利率
                lstdkllfd.setLilvleix("");
                // 利率调整方式 0--不调整
                lstdkllfd.setLilvtzfs("");
                // 利率浮动值
                lstdkllfd.setLilvfdzh(null);
                // 正常利率
                lstdkllfd.setZhchlilv(null);
                // 利率调整周期
                lstdkllfd.setLilvtzzq("");
                // 正常利率编号
                lstdkllfd.setZclilvbh("");

                lstdkllfdList.add(lstdkllfd);
            }
            ln3020ReqDto.setLstdkllfd(lstdkllfdList);
            /******************分段计息处理逻辑结束**********************/

            /****************贷款多贴息账户**************/
            List<Lstdktxzh> lstdktxzhList = new ArrayList<>();
            Lstdktxzh lstdktxzh = new Lstdktxzh();
            // 贴息比率
            lstdktxzh.setTiexibil(null);
            // 生效日期
            lstdktxzh.setShengxrq("");
            // 到期日期
            lstdktxzh.setDaoqriqi("");
            // 贴息账号
            lstdktxzh.setTiexizhh("");
            // 贴息账号子序号
            lstdktxzh.setTxzhhzxh("");
            // 贴息账户名称
            lstdktxzh.setTxzhhmch("");
            // 贴息记复利标识
            lstdktxzh.setFxjxbzhi("");
            // 自动扣款标识
            lstdktxzh.setZdkoukbz("");

            lstdktxzhList.add(lstdktxzh);
            ln3020ReqDto.setLstdktxzh(lstdktxzhList);
            /****************贷款多贴息账户**************/

            /****************还款方式信息转换**************/
            Ln3235RespDto ln3235RespDto = null;
            // 获取还款方式
            String repayMode = DicTranEnum.lookup("REPAY_MODE_XDTOHX_" + accLoan.getRepayMode());

            if ("".equals(repayMode)) { // 若转换后还款方式为空则表示特殊还款方式
                // 调用核心还款方式转换接口Ln3235
                ln3235RespDto = this.sendLn3235(pvpAuthorize, accLoan, null);

                // 根据Ln3235接口返回数据填充请求报文
                // 还款方式
                ln3020ReqDto.setHuankfsh(ln3235RespDto.getHuankfsh());
                // 定制还款计划:1--是、0--否
                ln3020ReqDto.setDzhhkjih(ln3235RespDto.getDzhhkjih());
                // 期限变更调整计划
                ln3020ReqDto.setQxbgtzjh(ln3235RespDto.getQxbgtzjh());
                // 利率变更调整计划
                ln3020ReqDto.setLlbgtzjh(ln3235RespDto.getLlbgtzjh());
                // 多次放款调整计划
                ln3020ReqDto.setDcfktzjh(ln3235RespDto.getDcfktzjh());
                // 提前还款调整计划
                ln3020ReqDto.setTqhktzjh(ln3235RespDto.getTqhktzjh());
                // 本金分段
                ln3020ReqDto.setBenjinfd(ln3235RespDto.getBenjinfd());
                // 首次还款日
                ln3020ReqDto.setScihkrbz(ln3235RespDto.getScihkrbz());

                // 贷款还本计划
                List<Lsdkhbjh> lsdkhbjhList = ln3235RespDto.getLsdkhbjh();
                if (CollectionUtils.nonEmpty(lsdkhbjhList)) {
                    List<Lstdkhbjh> lstdkhbjhList = lsdkhbjhList.parallelStream().map(lsdkhbjh -> {
                        Lstdkhbjh lstdkhbjh = new Lstdkhbjh();
                        BeanUtils.copyProperties(lsdkhbjh, lstdkhbjh);
                        return lstdkhbjh;
                    }).collect(Collectors.toList());
                    ln3020ReqDto.setLstdkhbjh(lstdkhbjhList);
                }

                // 本金分段计划
                List<Lsdkbjfd> lsdkbjfdList = ln3235RespDto.getLsdkbjfd();
                if (CollectionUtils.nonEmpty(lsdkbjfdList)) {
                    List<Lstdkbjfd> lstdkhbjhList = lsdkbjfdList.parallelStream().map(lsdkbjfd -> {
                        Lstdkbjfd lstdkbjfd = new Lstdkbjfd();
                        BeanUtils.copyProperties(lsdkbjfd, lstdkbjfd);
                        return lstdkbjfd;
                    }).collect(Collectors.toList());
                    ln3020ReqDto.setLstdkbjfd(lstdkhbjhList);
                }

                // 还款周期
                ln3020ReqDto.setHkzhouqi("");
                // 还本周期
                ln3020ReqDto.setHuanbzhq("");
                // 逾期还款周期
                ln3020ReqDto.setYuqhkzhq("");
            } else {// 若非特殊还款方式，按如下逻辑处理
                // 小微特殊还款方式判断 A023--按月付息，定期还本 经与老信贷确认，此种还款方式小贷已不使用
                if ("016000".equals(accLoan.getManagerBrId()) && !"".equals(pvpLoanApp.getSurveySerno()) && "A023".equals(accLoan.getRepayMode())) {
                    log.info("小贷已暂不支持A023--按月付息，定期还本还款方式");
                    throw BizException.error("", "", "小贷已暂不支持A023--按月付息，定期还本还款方式!");
                } else {
                    // 定制还款计划:1--是、0--否
                    ln3020ReqDto.setDzhhkjih("0");
                }

                // 还款方式：1--利随本清、2--多次还息一次还本、3--等额本息、4--等额本金、5--等比累进、6--等额累进、7--定制还款
                ln3020ReqDto.setHuankfsh(repayMode);
                // 结息间隔周期
                String eiIntervalCycle = accLoan.getEiIntervalCycle();
                // 结息周期单位
                String eiIntervalUnit = accLoan.getEiIntervalUnit();
                // 还款日
                String repayDay = accLoan.getDeductDay();
                // 计算还款日
                if (!StringUtils.isEmpty(repayDay)) {
                    repayDay = repayDay.length() == 1 ? "0".concat(repayDay) : repayDay;
                }

                // 标识位 对公客户号以8开头的
                String sy_flag = "A";
                // 节假日顺延标志位,对公客户默认展示1
                String isDelay = pvpLoanApp.getIsHolidayDelay();

                if (pvpLoanApp.getCusId() != null && pvpLoanApp.getCusId().startsWith("8") && "1".equals(isDelay)) {
                    sy_flag = "N";
                }
                // 根据还款周期单位赋值
                String temp = "";
                if ("M".equals(eiIntervalUnit)) {// 按月
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay;
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else if ("Q".equals(eiIntervalUnit)) {// 按季
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay + "E";
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else if ("Y".equals(eiIntervalUnit)) {// 按年
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + "12" + repayDay;
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else {
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi("");
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq("");
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq("");
                }
                // 期限变更调整计划
                ln3020ReqDto.setQxbgtzjh("");
                // 利率变更调整计划
                ln3020ReqDto.setLlbgtzjh("");
                // 多次放款调整计划
                ln3020ReqDto.setDcfktzjh("");
                // 提前还款调整计划
                ln3020ReqDto.setTqhktzjh("");
                // 本金分段
                ln3020ReqDto.setBenjinfd("");
            }
            // 等额处理规则
            // 还款方式 3--等额本息 4--等额本金
            if ("3".equals(repayMode) || ("4".equals(repayMode) && "022001".equals(accLoan.getPrdId()) || "022002".equals(accLoan.getPrdId()) ||
                    "022020".equals(accLoan.getPrdId()) || "022021".equals(accLoan.getPrdId()) || "022024".equals(accLoan.getPrdId()) ||
                    "022031".equals(accLoan.getPrdId()) || "022040".equals(accLoan.getPrdId()) || "022051".equals(accLoan.getPrdId()) ||
                    "022052".equals(accLoan.getPrdId()) || "022053".equals(accLoan.getPrdId()) || "022055".equals(accLoan.getPrdId()) ||
                    "022056".equals(accLoan.getPrdId()) || "022054".equals(accLoan.getPrdId()))) {
                // 20190218修改 覃明贵确定还款方式为等额本息，计息规则传01，其余传空
                ln3020ReqDto.setJixiguiz("01");
                // 核心吴高科 说等额本息时，等额处理规则=2--标准   2019-11-22焦广福要求修改为1 0--不适用、1--标准+差额、2--标准、3--标准/实际
                ln3020ReqDto.setDechligz("1");
            } else {
                ln3020ReqDto.setJixiguiz("");
                ln3020ReqDto.setDechligz("");
            }

            // 还款方式为1--利随本清 不计复利 其余为空
            if ("1".equals(repayMode)) {
                ln3020ReqDto.setJfxibzhi("0");
                ln3020ReqDto.setJixiguiz("10");
            } else {
                ln3020ReqDto.setJfxibzhi("");
            }

            // 贷款还本计划
            if (Objects.isNull(ln3020ReqDto.getLstdkhbjh())) {
                List<Lstdkhbjh> lsdkhbjhList = new ArrayList<>();
                Lstdkhbjh lstdkhbjh = new Lstdkhbjh();
                // 定制还款种类
                lstdkhbjh.setDzhhkzhl("");
                // 需足额提前还款
                lstdkhbjh.setDzhhkzhl("");
                // 定制还款日期
                lstdkhbjh.setDzhkriqi("");
                // 还本金额
                lstdkhbjh.setHuanbjee(null);
                // 还款账号
                lstdkhbjh.setHuankzhh("");
                // 还款账号子序号
                lstdkhbjh.setHkzhhzxh("");
                // 还息方式
                lstdkhbjh.setTqhkhxfs("");
                // 还款遇假日规则
                lstdkhbjh.setHkyujrgz("");
                // 是否有宽限期
                lstdkhbjh.setSfyxkuxq("");
                // 宽限期天数
                lstdkhbjh.setKuanxqts(null);
                // 宽限期节假日规则
                lstdkhbjh.setKxqjjrgz("");
                lsdkhbjhList.add(lstdkhbjh);
                ln3020ReqDto.setLstdkhbjh(lsdkhbjhList);
            }
            // 本金分段登记
            if (Objects.isNull(ln3020ReqDto.getLstdkbjfd())) {
                List<Lstdkbjfd> lstdkbjfdList = new ArrayList<>();
                Lstdkbjfd lstdkbjfd = new Lstdkbjfd();
                // 累进值
                lstdkbjfd.setLeijinzh(null);
                // 起始日期
                lstdkbjfd.setQishriqi("");
                // 本阶段还款方式
                lstdkbjfd.setBjdhkfsh("");
                // 还款周期
                lstdkbjfd.setHkzhouqi("");
                // 本阶段还款期数
                lstdkbjfd.setBjdhkqsh(null);
                // 累进区间期数
                lstdkbjfd.setLeijqjsh("");
                // 序号
                lstdkbjfd.setXuhaoooo(null);
                // 计息规则
                lstdkbjfd.setJixiguiz("");
                // 等额处理规则
                lstdkbjfd.setDechligz("");
                // 到期日期
                lstdkbjfd.setDaoqriqi("");
                // 本阶段还本金额
                lstdkbjfd.setBjdhbjee(null);
            }
            /****************还款方式信息转换结束**************/

            // 累进区间期数
            ln3020ReqDto.setLeijqjsh("");
            // 累进值
            ln3020ReqDto.setLeijinzh(null);
            // 累进首段期数
            ln3020ReqDto.setLjsxqish("");
            // 每期还本比例
            ln3020ReqDto.setMqhbbili(null);
            // 每期还款总额
            ln3020ReqDto.setMeiqhkze(null);
            // 每期还本金额
            ln3020ReqDto.setMeiqhbje(null);
            // 保留金额
            ln3020ReqDto.setBaoliuje(null);
            // 还款顺序编号
            ln3020ReqDto.setHkshxubh("");
            // 还款期限(月)
            ln3020ReqDto.setHkqixian("");
            // 频率还款标志
            ln3020ReqDto.setDuophkbz("");
            // 下一次还款日
            ln3020ReqDto.setXycihkrq("");
            // 末期还款方式
            ln3020ReqDto.setMqihkfsh("");
            // 不足额扣款方式
            ln3020ReqDto.setBzuekkfs("");

            // 扣款方式判断 核心字典项1--自动扣款
            if ("AUTO".equals(accLoan.getDeductType())) {// AUTO--自动
                // 自动扣款标志
                ln3020ReqDto.setZdkoukbz("1");
            } else if ("MANL".equals(accLoan.getDeductType())) {// MANL--手动
                // 自动扣款标志
                ln3020ReqDto.setZdkoukbz("0");
            } else {
                throw BizException.error("","9999","扣款方式不符合规范，请核查！");
            }

            // 指定文件批量扣款标识
            ln3020ReqDto.setZdplkkbz("");
            // 自动结清贷款标志
            ln3020ReqDto.setZdjqdkbz("");
            // 贷款到期假日规则
            ln3020ReqDto.setZbjrsygz("");
            // 签约循环贷款标志
            ln3020ReqDto.setQyxhdkbz("");
            // 签约账号
            ln3020ReqDto.setXhdkqyzh("");
            // 签约账号子序号
            ln3020ReqDto.setXhdkzhxh("");
            // 允许提前还款
            ln3020ReqDto.setYunxtqhk("");
            // 提前还款锁定期
            ln3020ReqDto.setTiqhksdq("");

            // 是否是资产池
//            CtrAsplDetails asplDetails = ctrAsplDetailsService.selectInfoByContNo(pvpLoanApp.getContNo());
//            Boolean isZCC = false;
//            if(asplDetails != null && !"".equals(asplDetails.getContNo())){
//                isZCC = true;
//            }

            // 多还款账户标志 1--是 0--否
            // 022097--市民贷 022099--惠民贷 022100--大家e贷  且 替补还款账户不能为空
            if (("022097".equals(accLoan.getPrdId()) || "022099".equals(accLoan.getPrdId()) || "022100".equals(accLoan.getPrdId())) && StringUtil.isNotEmpty(pvpAuthorize.getFldvalue04())) {
                // 多还款账户标志
                ln3020ReqDto.setDhkzhhbz("1");

                // 贷款多还款账户列表信息
                List<Lstdkhkzh> lstdkhkzhList = new ArrayList<>();
                // 贷款多还款账户信息
                Lstdkhkzh lstdkhkzh = new Lstdkhkzh();
                // 账户信息
                String cusAccNo = pvpAuthorize.getFldvalue04();
                // 还款基数种类 1--本金 2--利息 3--本息
                lstdkhkzh.setHkjshzhl("3");
                // 还款账户规则 1--专用账户 2--普通账户
                lstdkhkzh.setHkzhhgze("2");

                // 还款账户种类 1--本人账户 2--他人账户
                lstdkhkzh.setHkzhhzhl("1");
                // 还款比例
                lstdkhkzh.setHuankbli(new BigDecimal("100"));
                // 生效日期
                lstdkhkzh.setShengxrq(accLoan.getLoanStartDate().replace("-", ""));
                // 到期日期
                lstdkhkzh.setDaoqriqi("29991231");
                // 优先级
                lstdkhkzh.setYouxianj("2");

                // 查询账号子序号信息
                Ib1253RespDto ib1253RespDto = this.sendIb1253(cusAccNo);

                // 还款账号
                lstdkhkzh.setHuankzhh(cusAccNo);
                // 还款账户子序号
                lstdkhkzh.setHkzhhzxh(ib1253RespDto.getZhaoxhao());
                // 还款账户名称
                lstdkhkzh.setHkzhhmch(ib1253RespDto.getZhhuzwmc());

                lstdkhkzhList.add(lstdkhkzh);
                ln3020ReqDto.setLstdkhkzh(lstdkhkzhList);

            } else if (Objects.nonNull(ctrAsplDetails)) {
                // 资产池 超短贷 放款的时候 还款账户送结算户， 多还款账户送保证金户  youxianj 优先级 送 1
                // 多还款账户标志
                ln3020ReqDto.setDhkzhhbz("1");

                // 贷款多还款账户列表信息
                List<Lstdkhkzh> lstdkhkzhList = new ArrayList<>();
                // 贷款多还款账户信息
                Lstdkhkzh lstdkhkzh = new Lstdkhkzh();
                // 保证金账户信息
                BailAccInfo bailAccInfo = bailAccInfoService.selectInfoBySerno(ctrAsplDetails.getSerno());

                // 还款基数种类 1--本金 2--利息 3--本息
                lstdkhkzh.setHkjshzhl("3");
                // 还款账户规则 1--专用账户 2--普通账户
                lstdkhkzh.setHkzhhgze("2");

                // 还款账户种类 1--本人账户 2--他人账户
                lstdkhkzh.setHkzhhzhl("1");
                // 还款比例
                lstdkhkzh.setHuankbli(new BigDecimal("100"));
                // 生效日期
                lstdkhkzh.setShengxrq(accLoan.getLoanStartDate().replace("-", ""));
                // 到期日期
                lstdkhkzh.setDaoqriqi("29991231");
                // 优先级 资产池优先级为
                lstdkhkzh.setYouxianj("1");

                // 还款账号
                lstdkhkzh.setHuankzhh(bailAccInfo.getBailAccNo());
                // 还款账户子序号
                lstdkhkzh.setHkzhhzxh(bailAccInfo.getBailAccNoSub());
                // 还款账户名称
                lstdkhkzh.setHkzhhmch(bailAccInfo.getBailAccName());

                lstdkhkzhList.add(lstdkhkzh);
                ln3020ReqDto.setLstdkhkzh(lstdkhkzhList);
            } else {
                // 多还款账户标志
                ln3020ReqDto.setDhkzhhbz("");

                // 贷款多还款账户列表信息
                List<Lstdkhkzh> lstdkhkzhList = new ArrayList<>();
                // 贷款多还款账户信息
                Lstdkhkzh lstdkhkzh = new Lstdkhkzh();

                // 还款基数种类 1--本金 2--利息 3--本息
                lstdkhkzh.setHkjshzhl("");
                // 还款账户规则 1--专用账户 2--普通账户
                lstdkhkzh.setHkzhhgze("");
                // 还款账户种类 1--本人账户 2--他人账户
                lstdkhkzh.setHkzhhzhl("");
                // 还款比例
                lstdkhkzh.setHuankbli(null);
                // 生效日期
                lstdkhkzh.setShengxrq("");
                // 到期日期
                lstdkhkzh.setDaoqriqi("");
                // 优先级
                lstdkhkzh.setYouxianj("");
                // 还款账号
                lstdkhkzh.setHuankzhh("");
                // 还款账户子序号
                lstdkhkzh.setHkzhhzxh("");
                // 还款账户名称
                lstdkhkzh.setHkzhhmch("");

                lstdkhkzhList.add(lstdkhkzh);
                ln3020ReqDto.setLstdkhkzh(lstdkhkzhList);
            }

            // 多笔贷款扣款顺序
            ln3020ReqDto.setDbdkkksx("");

            /*************贷款定制期供计划开始**************/
            List<Lstdzqgjh> lstdzqgjhList = new ArrayList<>();
            // 7--定制还款
            if ("7".equals(repayMode)) {
                // 与业务商定还本计划暂时不发核心，后续如需要，放开即可
//                // 期供生成方式:1--还款规则、2--还款计划书
//                ln3020ReqDto.setQigscfsh("2");
//                // 查询定制还款计划
//                QueryModel queryModel = new QueryModel();
//                queryModel.addCondition("serno",pvpAuthorize.getPvpSerno());
//                queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
//                queryModel.setSort("repayDate asc");
//                List<RepayCapPlan> list = repayCapPlanService.selectAll(queryModel);
//                for (int i = 0; i < list.size(); i++) {
//                    RepayCapPlan repayCapPlan = list.get(i);
//                    // 起始日期
//                    String qishriqi = "";
//                    // 到期日期
//                    String daoqriqi = "";
//                    if(i == 0){
//                        // 第一期
//                        qishriqi = accLoan.getLoanStartDate();
//                        daoqriqi = repayCapPlan.getRepayDate();
//                    }else{
//                        /*
//                         * 第二期到第n期
//                         * 1.先取上一期的还款日期转化为date类型，格式转化为"yyyy-MM-dd"
//                         * 2.把处理后的日期加一天(核心的规则不需要加一天且最后一期的到期日期等于贷款到期日，先按照核心的规则)
//                         * 3.再把日期转换为字符串
//                         */
//                        RepayCapPlan repayCapPlanOther = list.get(i-1);
////                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
////                        Date qishriqiDate = sdf.parse(repayCapPlanOther.getRepayDate());
////
////                        Date fianlQishriqi = DateUtils.addDay(qishriqiDate,1);
////
////                        qishriqi = sdf.format(fianlQishriqi);
//                        if(i == list.size()-1){
//                            qishriqi = repayCapPlanOther.getRepayDate();
//                            daoqriqi = accLoan.getLoanEndDate();
//                        }else{
//                            qishriqi = repayCapPlanOther.getRepayDate();
//                            daoqriqi = repayCapPlan.getRepayDate();
//                        }
//                    }
//                    Lstdzqgjh lstdzqgjh = new Lstdzqgjh();
//                    // TODO 为何没有借据号了
//                    //lstdzqgjh.setDkjiejuh(pvpAuthorize.getBillNo());// 贷款借据号
//                    // 本期期数
//                    lstdzqgjh.setBenqqish(String.valueOf(i+1));
//                    // 起始日期
//                    lstdzqgjh.setQishriqi(qishriqi.replace("-", ""));
//                    // 到期日期
//                    lstdzqgjh.setDaoqriqi(daoqriqi.replace("-", ""));
//                    // 还本金额
//                    lstdzqgjh.setHuanbjee(repayCapPlan.getRepayAmt());
//                    // 还息金额默认0
//                    lstdzqgjh.setHxijinee(new BigDecimal("0.0"));
//                    // 最晚还款日
//                    lstdzqgjh.setZwhkriqi("");
//                    lstdzqgjhList.add(lstdzqgjh);
//                }
            } else {
                // 期供生成方式:1--还款规则、2--还款计划书
                ln3020ReqDto.setQigscfsh("");
                Lstdzqgjh lstdzqgjh = new Lstdzqgjh();

                // 本期期数
                lstdzqgjh.setBenqqish("");
                // 起始日期
                lstdzqgjh.setQishriqi("");
                // 到期日期
                lstdzqgjh.setDaoqriqi("");
                // 还本金额
                lstdzqgjh.setHuanbjee(null);
                // 还息金额默认0
                lstdzqgjh.setHxijinee(new BigDecimal("0.0"));
                // 最晚还款日
                lstdzqgjh.setZwhkriqi("");
                lstdzqgjhList.add(lstdzqgjh);
            }
            ln3020ReqDto.setLstdzqgjh(lstdzqgjhList);
            /*************贷款定制期供计划结束**************/

            // 期供利息类型 1--贷款本金利息 2--本期本金利息 3--指定本金利息
            ln3020ReqDto.setQglxleix("7".equals(repayMode) ? "1" : "");

            // 允许行内同名账户还款
            ln3020ReqDto.setHntmihku("");
            // 允许行内非同名帐户还款
            ln3020ReqDto.setHnftmhku("");
            // 允许内部账户还款
            ln3020ReqDto.setNbuzhhku("");
            // 允许缩期
            ln3020ReqDto.setYunxsuoq("");
            // 缩期最大次数
            ln3020ReqDto.setSqizdcsh("");
            // 逾期不足额扣款方式
            ln3020ReqDto.setYqbzkkfs("");
            // 逾期自动追缴标志
            ln3020ReqDto.setYqzdzjbz("");
            // 允许调整还款方式
            ln3020ReqDto.setTiaozhkf("");
            // 是否有宽限期
            ln3020ReqDto.setSfyxkuxq("");
            // 宽限期天数
            ln3020ReqDto.setKuanxqts("");
            // 宽限期计息方式
            ln3020ReqDto.setKxqjixgz("");
            // 宽限期后计息规则
            ln3020ReqDto.setKxqhjxgz("");
            // 宽限期转逾期规则
            ln3020ReqDto.setKxqzyqgz("");
            // 宽限期收息规则
            ln3020ReqDto.setKxqshxgz("");
            // 宽限期节假日规则
            ln3020ReqDto.setKxqjjrgz("");
            // 宽限期最大次数
            ln3020ReqDto.setKxqzdcsh("");
            // 展期规则编号
            ln3020ReqDto.setZqgzbhao("");
            // 展期需足额扣款
            ln3020ReqDto.setZhqxzekk("");
            // 展期最长期限(月)
            ln3020ReqDto.setZhqizcqx("");
            // 自动形态转移
            ln3020ReqDto.setZidxtzhy("");
            // 利息转出规则
            ln3020ReqDto.setLixizcgz("");
            // 利息转回规则
            ln3020ReqDto.setLixizhgz("");
            // 使用贷款承诺
            ln3020ReqDto.setSydkcnuo("");
            // 承诺贷款借据号
            ln3020ReqDto.setCndkjjho("");
            // 使用额度标志
            ln3020ReqDto.setShynedbz("");
            // 额度编号
            ln3020ReqDto.setEdbiahao("");
            // 额度币种规则
            ln3020ReqDto.setEdbizhgz("");
            // 额度指定币种
            ln3020ReqDto.setEdzdbizh("");
            // 保证人担保标志
            ln3020ReqDto.setBzhrdbbz("");

            // 贷款保证人信息
            List<Lstdkzhbz> lstdkzhbzList = new ArrayList<>();
            Lstdkzhbz lstdkzhbz = new Lstdkzhbz();
            // 保证方式
            lstdkzhbz.setBaozhfsh("");
            // 保证人客户号
            lstdkzhbz.setBzrkehuh("");
            // 保证金额
            lstdkzhbz.setBaozjine(null);
            // 客户名称
            lstdkzhbz.setKehmingc("");
            // 担保账号
            lstdkzhbz.setDanbzhao("");
            // 担保账号子序号
            lstdkzhbz.setDbzhzxuh("");
            // 备注信息
            lstdkzhbz.setBeizhuuu("");
            lstdkzhbzList.add(lstdkzhbz);
            ln3020ReqDto.setLstdkzhbz(lstdkzhbzList);

            // 代理核算方式
            ln3020ReqDto.setDlhesfsh("");
            // 代理信息指定规则
            ln3020ReqDto.setDlxxzdgz("");
            // 代理信息取值规则
            ln3020ReqDto.setDlxxqzgz("");
            // 代理序号
            ln3020ReqDto.setDailixuh("");
            // 代理描述
            ln3020ReqDto.setDailimsh("");
            // 本金归还入账账号
            ln3020ReqDto.setBjghrzzh("");
            // 本金归还入账账号子序号
            ln3020ReqDto.setBjghrzxh("");
            // 利息归还入账账号
            ln3020ReqDto.setLxghrzzh("");
            // 利息归还入账账号子序号
            ln3020ReqDto.setLxghrzxh("");
            // 多委托人标志
            ln3020ReqDto.setDuowtrbz("");

            // 贷款多委托人账户
            List<Lstdkwtxx> lstdkwtxxlist = new ArrayList<>();
            Lstdkwtxx lstdkwtxx = new Lstdkwtxx();
            // 本行出资标志
            lstdkwtxx.setBhchzibz("");
            // 本行借据号
            lstdkwtxx.setBhjiejuh("");
            // 本行账号标志
            lstdkwtxx.setBhzhaobz("");
            // 资金归集标志
            lstdkwtxx.setZjingjbz("");
            // 还款资金划转方式
            lstdkwtxx.setHkzjhzfs("");
            // 账户开户行行号
            lstdkwtxx.setZhkaihhh("");
            // 账户开户行行名
            lstdkwtxx.setZhkaihhm("");
            // 委托人客户号
            lstdkwtxx.setWtrkehuh("");
            // 委托人存款账号
            lstdkwtxx.setWtrckuzh("");
            // 委托人存款账号子序号
            lstdkwtxx.setWtrckzxh("");
            // 委托存款账号
            lstdkwtxx.setWtckzhao("");
            // 委托存款账号子序号
            lstdkwtxx.setWtckzixh("");
            // 本金归还入账账号
            lstdkwtxx.setBjghrzzh("");
            // 利息归还入账账号
            lstdkwtxx.setLxghrzzh("");
            // 本金归还入账账号子序号
            lstdkwtxx.setBjghrzxh("");
            // 利息归还入账账号子序号
            lstdkwtxx.setLxghrzxh("");
            // 委托金额
            lstdkwtxx.setWeituoje(null);
            //委托人名称
            lstdkwtxx.setWtrmingc("");
            lstdkwtxxlist.add(lstdkwtxx);
            ln3020ReqDto.setLstdkwtxx(lstdkwtxxlist);

            //联名贷款标志
            ln3020ReqDto.setLmdkbzhi("");

            // 贷款账户联名
            List<Lstdkzhlm> lstdkzhlmList = new ArrayList<>();
            Lstdkzhlm lstdkzhlm = new Lstdkzhlm();
            // 客户关系类型
            lstdkzhlm.setKehugxlx("");
            // 客户号
            lstdkzhlm.setKehuhaoo("");
            // 贷款账号
            lstdkzhlm.setDkzhangh("");
            lstdkzhlmList.add(lstdkzhlm);
            ln3020ReqDto.setLstdkzhlm(lstdkzhlmList);

            // 联合贷款
            List<Lstdklhmx> lstdklhmxlist = new ArrayList<>();
            Lstdklhmx lstdklhmx = new Lstdklhmx();

            // 参与方代码
            lstdklhmx.setCanyfdma("");
            // 参与方户名
            lstdklhmx.setCanyfhum("");
            // 联合方式
            lstdklhmx.setLianhfsh("");
            // 联合贷款类型
            lstdklhmx.setLhdkleix("");
            // 参与金额
            lstdklhmx.setCanyjine(null);
            // 参与比例
            lstdklhmx.setCanybili(null);
            // 资金来源账号
            lstdklhmx.setZjlyzhao("");
            lstdklhmx.setZjlyzzxh("");//资金来源账号子序号
            lstdklhmx.setZjzrzhao("");//资金转入账号
            lstdklhmx.setZjzrzzxh("");//资金转入账号子序号
            lstdklhmx.setDailfeil(new BigDecimal("0"));//代理费率
            lstdklhmx.setCanyjjha("");//参与借据号
            lstdklhmxlist.add(lstdklhmx);
            ln3020ReqDto.setLstdklhmx(lstdklhmxlist);

            // 提前还款罚金编号
            ln3020ReqDto.setTqhkfjbh("");
            // 提前还款罚金名称
            ln3020ReqDto.setTqhkfjmc("");
            //提前还款附加罚金金额
            ln3020ReqDto.setTqhkfjfj(null);

            // 贷款收费事件
            List<Lstdksfsj> lstdksfsjlist = new LinkedList<Lstdksfsj>();
            Lstdksfsj lstdksfsj = new Lstdksfsj();

            // 收费种类
            lstdksfsj.setShoufzhl("");
            // 收费金额/比例
            lstdksfsj.setShoufjee(null);
            // 付费账号
            lstdksfsj.setFufeizhh("");
            // 收费事件
            lstdksfsj.setShoufshj("");
            // 收费代码
            lstdksfsj.setShoufdma("");
            // 收费事件名称
            lstdksfsj.setShfshjmc("");
            // 收费代码名称
            lstdksfsj.setShfdmamc("");
            // 付费账号子序号
            lstdksfsj.setFfzhhzxh("");
            // 收费入账账号
            lstdksfsj.setSfrzhzhh("");
            // 收费入账账号子序号
            lstdksfsj.setSfrzhzxh("");
            lstdksfsjlist.add(lstdksfsj);
            ln3020ReqDto.setLstdksfsj(lstdksfsjlist);

            // 收费类型
            ln3020ReqDto.setShfleixi("");
            // 收费频率
            ln3020ReqDto.setSfpinlvv("");
            // 收费周期
            ln3020ReqDto.setSfzhouqi("");
            // 收费种类
            ln3020ReqDto.setShoufzhl("");
            // 摊销周期
            ln3020ReqDto.setTxzhouqi("");
            // 收费代码
            ln3020ReqDto.setShoufdma("");
            // 收费代码名称
            ln3020ReqDto.setShfdmamc("");
            // 收费金额/比例
            ln3020ReqDto.setShoufjee(null);
            // 付费账号
            ln3020ReqDto.setFufeizhh("");
            // 付费账号子序号
            ln3020ReqDto.setFfzhhzxh("");
            // 付费账号名称
            ln3020ReqDto.setFfzhhmch("");
            // 收费入账账号
            ln3020ReqDto.setSfrzhzhh("");
            // 收费入账账号子序号
            ln3020ReqDto.setSfrzhzxh("");
            // 末期已结清是否收费
            ln3020ReqDto.setMqjqsfbz("");
            // 费用首期是否包括当前期
            ln3020ReqDto.setFybkdqbz("");
            // 是否立即收费
            ln3020ReqDto.setSfljsfei("");
            // 费用是否摊销
            ln3020ReqDto.setShiftanx("");


            // 起始日期
            ln3020ReqDto.setQishriqi("");
            // 终止日期
            ln3020ReqDto.setZhzhriqi("");
            // 正常提前通知
            ln3020ReqDto.setZhchtqtz("");
            // 逾期催收通知
            ln3020ReqDto.setYqcshtzh("");
            // 利率变更通知
            ln3020ReqDto.setLilvbgtz("");
            // 余额变更通知
            ln3020ReqDto.setYuebgtzh("");
            // 通知提前天数
            ln3020ReqDto.setTzhtqtsh("");
            // 通知间隔天数
            ln3020ReqDto.setTzhjgtsh("");
            // 五级分类标志
            ln3020ReqDto.setWujiflbz("");
            // 五级分类日期
            ln3020ReqDto.setWujiflrq("");
            // 贷款管理机构
            ln3020ReqDto.setDkgljgou("");
            // 管理机构类别
            ln3020ReqDto.setGljgleib("");
            // 复核机构
            ln3020ReqDto.setFuhejgou("");
            // 客户经理
            ln3020ReqDto.setKhjingli(pvpLoanApp.getManagerId());
            // 凭证批号
            ln3020ReqDto.setPingzhma("");
            // 凭证序号
            ln3020ReqDto.setPngzxhao("");
            // 是否缴纳印花税标志
            ln3020ReqDto.setSfjnyhsh("");
            // 印花税率
            ln3020ReqDto.setYinhshlv(null);
            // 印花税金额
            ln3020ReqDto.setYinhshje(null);
            // 联系人名称
            ln3020ReqDto.setLxirenmc("");
            // 联系人电话
            ln3020ReqDto.setLxirendh("");
            // 是否关联抵质押物标志
            ln3020ReqDto.setSfgldzyw("");

            //抵押关联信息
            List<Lstdkdygl> lstdkdygllist = new ArrayList<>();
            Lstdkdygl lstdkdygl = new Lstdkdygl();

            // 贷款借据号
            lstdkdygl.setDkjiejuh("");
            // 关联业务种类
            lstdkdygl.setGlyewuzl("");
            // 担保方式
            lstdkdygl.setDbaofshi("");
            // 抵质押物编号
            lstdkdygl.setDzywbhao("");
            // 担保控制种类
            lstdkdygl.setDbaokzzl("");
            // 担保比例
            lstdkdygl.setDbaobili(null);
            // 联动担保控制
            lstdkdygl.setLddbkzhi("");
            // 担保币种
            lstdkdygl.setDbaobizh("");
            // 担保金额
            lstdkdygl.setDbaojine(null);
            // 借据币种
            lstdkdygl.setJijubizh("");
            // 借据金额
            lstdkdygl.setJiejuuje(null);
            // 折算汇率
            lstdkdygl.setZhshuilv(null);
            // 关联状态
            lstdkdygl.setGuanlzht("");
            // 明细序号
            lstdkdygl.setMingxixh("");
            lstdkdygllist.add(lstdkdygl);
            ln3020ReqDto.setLstdkdygl(lstdkdygllist);

            /******************账户质押信息开始*******************/
            // 是否账户质押标志
            ln3020ReqDto.setSfglzhzy("0");

            // TODO 根据合同编号查询合同下，我行本币存单和我行外币存单送存单相关信息


            /******************账户质押信息结束*******************/
        } catch (BizException bizException) {
            bizException.printStackTrace();
            throw BizException.error(null, bizException.getErrorCode(), bizException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        log.info("放款流水号【{}】放款报文组装结束", pvpLoanApp.getPvpSerno());
        return ln3020ReqDto;

    }

    /**
     * @param accNo
     * @return cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto
     * @author 王玉坤
     * @date 2021/6/22 23:48
     * @version 1.0.0
     * @desc 查询账号子账户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Ib1253RespDto sendIb1253(String accNo) {
        // Ib1253请求对象
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        // Ib1253返回对象
        Ib1253RespDto ib1253RespDto = new Ib1253RespDto();

        // 账号
        ib1253ReqDto.setKehuzhao(accNo);
        // TODO 查询结果集类型  没有该字段
        // 是否标志  1--是 0--否
        ib1253ReqDto.setShifoubz("0");
        // 子账号序号
        ib1253ReqDto.setZhhaoxuh("");

        // 调用ib1253接口
        ib1253RespDto = ib1253Service.ib1253(ib1253ReqDto);

        return ib1253RespDto;
    }

    /*
     * @param [toppAcctSub]
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto>
     * @author 王玉坤
     * @date 2021/6/12 17:32
     * @version 1.0.0
     * @desc 根据账户查询子账户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Dp2021RespDto sendDp2021(String accNo) {
        log.info("根据账号信息【{}】查询子账户信息开始", accNo);

        // 账号查询请求体
        Dp2021ReqDto dp2021ReqDto = null;
        // 子账号查询返回体
        ResultDto<Dp2021RespDto> resultDp2021 = null;
        try {
            //查询子账户信息
            dp2021ReqDto = new Dp2021ReqDto();
            // 查询类型 0--客户 1--非客户
            dp2021ReqDto.setChaxleix("0");
            // 客户号
            dp2021ReqDto.setKehuhaoo("");
            // 查询密码
            dp2021ReqDto.setChaxmima("");
            // 起始笔数
            dp2021ReqDto.setQishibis(1);
            // 起始笔数
            dp2021ReqDto.setChxunbis(10);
            // 查询范围 1--指定机构 2--直辖 3--全辖 4--本机构
            dp2021ReqDto.setChaxfanw("");
            // 负债账号
            dp2021ReqDto.setZhanghao("");
            // 客户账号
            dp2021ReqDto.setKehuzhao(accNo);
            // 产品定活标志 1--活期产品 2--定期产品
            dp2021ReqDto.setDinhuobz("");
            // 子账户序号
            dp2021ReqDto.setZhhaoxuh("");
            // 存款种类
            dp2021ReqDto.setCunkzlei("");
            // 币种
            dp2021ReqDto.setHuobdaih("");
            // 账户钞汇标志
            dp2021ReqDto.setChaohubz("");
            // 客户账户查询状态
            dp2021ReqDto.setZhcxzhzt("");
            // 是否打印
            dp2021ReqDto.setShifoudy("");
            // 机构号
            dp2021ReqDto.setJigouhao("");
            // 密码种类
            dp2021ReqDto.setMimazlei("");
            // 是否查询关联账户
            dp2021ReqDto.setSfcxglzh("");
            // 证件号码
            dp2021ReqDto.setZhjhaoma("");
            // 客户账户名称
            dp2021ReqDto.setKehuzhmc("");
            // 证件种类
            dp2021ReqDto.setZhjnzlei("");
            // 备用字段01
            dp2021ReqDto.setBeiyzd01("");

            // 请求核心查询子账户信息
            resultDp2021 = dscms2CoreDpClientService.dp2021(dp2021ReqDto);
            log.info("查询子账户信息报文返回:" + resultDp2021.getData().toString());

            // 解析返回信息
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDp2021.getCode())) {
                log.error("返回错误码：{}，错误信息：{}", resultDp2021.getCode(), resultDp2021.getMessage());
                throw BizException.error(null, resultDp2021.getCode(), resultDp2021.getMessage());
            }
        } catch (BizException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        log.info("根据账号信息【{}】查询子账户信息结束", accNo);
        return resultDp2021.getData();
    }

    /**
     * @param pvpAuthorize, accLoan]
     * @return Ln3235RespDto
     * @author 王玉坤
     * @date 2021/6/15 23:51
     * @version 1.0.0
     * @desc 查询还款方式转换接口信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Ln3235RespDto sendLn3235(PvpAuthorize pvpAuthorize, AccLoan accLoan, AccEntrustLoan accEntrustLoan) {
        // ln3235ReqDto请求对象
        Ln3235ReqDto ln3235ReqDto = null;
        // ln3235ReqDto返回对象
        ResultDto<Ln3235RespDto> respDtoResultDto = null;
        try {
            ln3235ReqDto = new Ln3235ReqDto();
            // 贷款借据号
            ln3235ReqDto.setDkjiejuh(pvpAuthorize.getBillNo());
            // 特殊还款方式
            ln3235ReqDto.setTshkfshi("");
            // 开户日期
            ln3235ReqDto.setKaihriqi(pvpAuthorize.getTranDate().replace("-", ""));

            String eiIntervalCycle = "";
            String eiIntervalUnit = "";
            String repayDay = "";
            String repayMode = "";
            // 区分委托贷款与普通贷款
            if (!Objects.isNull(accLoan)) { // 普通贷款处理逻辑
                // 到期日期
                ln3235ReqDto.setDaoqriqi(accLoan.getLoanEndDate().replace("-", ""));
                // 正常本金
                ln3235ReqDto.setZhchbjin(accLoan.getLoanAmt());
                // 还款周期与还本周期转换
                // 结息间隔周期
                eiIntervalCycle = accLoan.getEiIntervalCycle();
                // 结息周期单位
                eiIntervalUnit = accLoan.getEiIntervalUnit();
                // 还款日
                repayDay = accLoan.getDeductDay();
                // 信贷还款方式
                repayMode = accLoan.getRepayMode();
            } else if (!Objects.isNull(accEntrustLoan)) {// 委托贷款处理逻辑
                // 到期日期
                ln3235ReqDto.setDaoqriqi(accEntrustLoan.getLoanEndDate().replace("-", ""));
                // 正常本金
                ln3235ReqDto.setZhchbjin(accEntrustLoan.getLoanAmt());
                // 结息间隔周期
                eiIntervalCycle = accEntrustLoan.getEiIntervalCycle();
                // 结息周期单位
                eiIntervalUnit = accEntrustLoan.getEiIntervalUnit();
                // 还款日
                repayDay = accEntrustLoan.getDeductDay();
                // 信贷还款方式
                repayMode = accEntrustLoan.getRepayMode();
            }

            repayDay = repayDay.length() == 1 ? "0".concat(repayDay) : repayDay;

            // 还款周期
            String repayCycle = eiIntervalCycle.concat(eiIntervalUnit).concat("A").concat(repayDay);
            // 还本周期
            String repayPrinCycle = "";
            // 根据还款方式确定还款周期、还本周期
            if ("A012".equals(repayMode)) {// 按226比例还款
                // 还本周期
                repayPrinCycle = repayCycle;
            } else if ("A013".equals(repayMode)) {// 按月还息按季还本
                // 还本周期
                repayPrinCycle = "3" + eiIntervalUnit + "A" + repayDay;
            } else if ("A014".equals(repayMode)) {// 按月还息按半年还本
                // 还本周期
                repayPrinCycle = "6" + eiIntervalUnit + "A" + repayDay;
            } else if ("A015".equals(repayMode)) {// 按月还息,按年还本
                // 还本周期
                repayPrinCycle = "12" + eiIntervalUnit + "A" + repayDay;
            } else if ("A016".equals(repayMode)) {// 新226
                // 还本周期
                repayPrinCycle = "12" + eiIntervalUnit + "A" + repayDay;
            } else if ("A017".equals(repayMode)) {// 前6个月按月还息，后6个月等额本息
                // 还本周期
                repayPrinCycle = repayCycle;
            } else if ("A018".equals(repayMode)) {// 前4个月按月还息，后8个月等额本息
                // 还本周期
                repayPrinCycle = repayCycle;
            } else if ("A019".equals(repayMode)) {// 第一年按月还息，接下来等额本息
                // 还本周期
                repayPrinCycle = repayCycle;
            } else if ("A020".equals(repayMode)) {// 334比例还款
                // 还本周期
                repayPrinCycle = "12" + eiIntervalUnit + "A" + repayDay;
            } else if ("A021".equals(repayMode)) {// 433比例还款
                // 还本周期
                repayPrinCycle = "12" + eiIntervalUnit + "A" + repayDay;
            }

            // 还款周期
            ln3235ReqDto.setHkzhouqi(repayCycle);
            // 还本周期
            ln3235ReqDto.setHuanbzhq(repayPrinCycle);

            // 请求核心还款方式转换接口
            respDtoResultDto = dscms2CoreLnClientService.ln3235(ln3235ReqDto);

            // 解析返回信息
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(respDtoResultDto.getCode())) {
                log.error("返回错误码：{}，错误信息：{}", respDtoResultDto.getCode(), respDtoResultDto.getMessage());
                throw BizException.error(null, respDtoResultDto.getCode(), respDtoResultDto.getMessage());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return respDtoResultDto.getData();
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:36
     * @注释 交易冲正申请
     */
    public ResultDto jyczsq(PvpAuthorize pvpAuthorize) {
        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        //[{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
        // {"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},
        // {"key":"5","value":"已记帐已撤销"},{"key":"6","value":"抹账被核心拒绝"},
        // {"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},{"key":"9","value":"已发送核心冲正成功"}]
        if (pvpAuthorize1 == null) {
            return new ResultDto(null).message("未查询到相关放款数据");
        }
        pvpAuthorize1.setAuthStatus("8");
        int i = pvpAuthorizeMapper.updateByPrimaryKeySelective(pvpAuthorize1);
        if (i != 1) {
            return new ResultDto(null).message("冲正申请失败");
        }
        return new ResultDto(pvpAuthorize1).message("申请成功");
    }

    /**
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/10/26 23:08
     * @version 1.0.0
     * @desc 小微冲正处理
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PvpAuthorize pvpAuthorize) {
        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        //[{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
        // {"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},
        // {"key":"5","value":"已记帐已撤销"},{"key":"6","value":"抹账被核心拒绝"},
        // {"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},{"key":"9","value":"已发送核心冲正成功"}]
        if (pvpAuthorize1 == null) {
            return new ResultDto(null).message("未查询到相关放款数据");
        }

        // 小微冲正申请有流程 用A-冲正申请控制
        if (!"A".equals(pvpAuthorize1.getAuthStatus())) {
            log.info("仅可对‘已发起冲正申请’的出账业务进行冲正");
            return new ResultDto(null).message("仅可对‘已发起冲正申请’的出账业务进行冲正").code("9999");
        }

        //如何判断受托支付失败 核心自行判断
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String tranDate = pvpAuthorize1.getTranDate();
        if (!tranDate.equals(openday)) {
            log.info("只能对当天的出账且受托支付失败的业务进行冲");
            throw BizException.error(null, "9999", "只能对当天的出账且受托支付失败的业务进行冲正");
        }

        try {
            // 调用核心冲正交易
            Ib1241RespDto ib1241RespDto = sendIb1241(pvpAuthorize1);

            // 已发送核心冲正成功
            pvpAuthorize1.setAuthStatus("9");
            int i = pvpAuthorizeMapper.updateByPrimaryKeySelective(pvpAuthorize1);
            if (i != 1) {
                log.info("冲正处理失败");
                throw BizException.error(null, "9999", "冲正处理失败");
            }

            //更新台账状态
            AccLoan accLoan = accLoanService.selectByBillNo(pvpAuthorize1.getBillNo());
            accLoan.setAccStatus("0");//已关闭
            accLoanService.updateSelective(accLoan);

            //台账额度恢复
            pvpLoanAppService.retailRestore(pvpAuthorize1.getPvpSerno());

            //更新出账申请
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpAuthorize1.getPvpSerno());
            pvpLoanApp.setApproveStatus("996");//自行退出
            pvpLoanAppService.updateSelective(pvpLoanApp);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }

    /***
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto>
     * @author 王玉坤
     * @date 2021/6/28 8:37
     * @version 1.0.0
     * @desc 冲正交易
     * 1、获取出账授权通知信息的”原前置日期、原前置流水“
     * 2、调用冲正交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Ib1241RespDto sendIb1241(PvpAuthorize pvpAuthorize) throws Exception {
        // 冲正交易请求对象
        Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();

        // 1、拼装请求报文
        // 原前置日期
        ib1241ReqDto.setYqzhriqi("");
        // 原前置流水
        ib1241ReqDto.setYqzhlshu("");
        // 原交易日期
        ib1241ReqDto.setYjiaoyrq(pvpAuthorize.getCoreTranDate().replace("-", ""));
        // 原柜员流水
        ib1241ReqDto.setYgyliush(pvpAuthorize.getCoreTranSerno());
        // 产品码
        ib1241ReqDto.setChanpnma(StringUtils.EMPTY);
        // 前置日期
        ib1241ReqDto.setQianzhrq("");
        // 前置流水
        ib1241ReqDto.setQianzhls("");
        // 部门号
        ib1241ReqDto.setBrchno(pvpAuthorize.getFinaBrId());
        log.info("开始进行冲正");
        Ib1241RespDto ib1241ResultDto = ib1241Service.ib1241(ib1241ReqDto);
        log.info("冲正结束 响应信息" + ib1241ResultDto);
        return ib1241ResultDto;
    }

    /**
     * @方法名称: updateAuthStatusByPvpSerno
     * @方法描述: 作废出帐信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateAuthStatusByPvpSerno(String pvpSerno) {
        return pvpAuthorizeMapper.updateAuthStatusByPvpSerno(pvpSerno);
    }


    /**
     * @方法名称: calHxAccountClass
     * @方法描述: 测算核心科目编号
     * @参数与返回说明: 根据12个科目属性调用科目查询的接口查询返回科目数据
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto calHxAccountClass(String pvpSerno) {
        String hxAccountClass = "";
        if (StringUtils.isBlank(pvpSerno)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }

        // 客户类别 1对公，2对私
        String publicPerson = "";
        // 业务类型（产品ID）
        String bizTypeSub = "";
        // 客户类型
        String cusType = "";
        // 城乡类型
        String cityVillage = "";
        // 企业类型 对公客户才有的字段
        String factoryType = "";
        // 主营业务 行业分类
        String mainDeal = "";
        // 涉农标识 个人客户中"是否农户"字段
        String farmFlag = "";
        // 涉农投向 涉农贷款投向
        String farmDirection = "";
        // 投向行业 贷款投向编号
        String directionOption = "";
        // 担保方式 担保方式细分
        String guarMode = "";
        // 贷款类别 贷款类别
        String loanType = "";
        // 期限 借据放款期限
        String loanTerm = "";

        // 首先查询出账申请记录，如果查询不到查询委托申请记录
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        log.info("科目计算，获取出账申请数据:{}", pvpLoanApp.toString());
        // 获取贷款出账数据
        // 业务类型（产品ID）
        bizTypeSub = pvpLoanApp.getPrdId();
        // 涉农贷款投向
        farmDirection = pvpLoanApp.getAgriLoanTer();
        // 贷款投向编号
        if (pvpLoanApp.getLoanTer() != null && !"".equals(pvpLoanApp.getLoanTer())) {
            directionOption = pvpLoanApp.getLoanTer().substring(0, 1);
        }
        // 担保方式细分
        guarMode = pvpLoanApp.getGuarMode();
        // 贷款类别
        loanType = pvpLoanApp.getLoanTypeDetail();
        // 借据放款期限
        loanTerm = pvpLoanApp.getLoanTerm();
        CfgAccountClassChooseDto cfgAccountClassChooseDto = new CfgAccountClassChooseDto();

        // 查询客户类型
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());
        publicPerson = cusBaseClientDto.getCusCatalog();
        log.info("科目计算，获取客户基本信息数据:{}", cusBaseClientDto.toString());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId()).getData();
            log.info("科目计算，获取个人客户基本信息数据:{}", cusCorpDto.toString());
            cusType = cusCorpDto.getCusType();
            cityVillage = cusCorpDto.getCityType();
            factoryType = cusCorpDto.getConType();
            mainDeal = StringUtils.isBlank(cusCorpDto.getTradeClass()) ? "" : cusCorpDto.getTradeClass().substring(0, 1);
        } else {
            // 如果是个人客户
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(pvpLoanApp.getCusId());
            log.info("科目计算，获取个人客户基本信息数据:{}", cusIndivDto.toString());
            cusType = cusIndivDto.getData().getCusType();
            farmFlag = cusIndivDto.getData().getAgriFlg();
        }
        // 期限转换
        if (Integer.parseInt(loanTerm) <= 12) {
            loanTerm = "0";// 短期
        } else {
            loanTerm = "1";// 中长期
        }

        cfgAccountClassChooseDto.setPublicPerson(publicPerson);
        cfgAccountClassChooseDto.setBizTypeSub(bizTypeSub);
        cfgAccountClassChooseDto.setCusType(cusType);
        cfgAccountClassChooseDto.setCityVillage(cityVillage);
        cfgAccountClassChooseDto.setFactoryType(factoryType);
        cfgAccountClassChooseDto.setMainDeal(mainDeal);
        cfgAccountClassChooseDto.setFarmFlag(farmFlag);
        cfgAccountClassChooseDto.setFarmDirection(farmDirection);
        cfgAccountClassChooseDto.setDirectionOption(directionOption);
        cfgAccountClassChooseDto.setGuarMode(guarMode);
        cfgAccountClassChooseDto.setLoanType(loanType);
        cfgAccountClassChooseDto.setLoanTerm(loanTerm);
        System.out.println(cfgAccountClassChooseDto.toString());
        log.info("科目计算，科目计算传参为:{}", cfgAccountClassChooseDto);
        cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByProps(cfgAccountClassChooseDto).getData();
        log.info("科目计算，科目计算回参为:{}", cfgAccountClassChooseDto.toString());
        if (!EcfEnum.ECF020000.key.equals(cfgAccountClassChooseDto.getRtnCode())) {
            throw BizException.error(null, cfgAccountClassChooseDto.getRtnCode(), cfgAccountClassChooseDto.getRtnMsg());
        }
        return cfgAccountClassChooseDto;
    }

    /**
     * @param pvpLoanApp
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/10/4 16:53
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto getClassSubNoForXw(PvpLoanApp pvpLoanApp, CfgPrdBasicinfoDto cfgPrdBasicinfoDto) {
        // 映射产品代码，尽量保持与小微一致，产品大类分为 消费类贷款--022050 经营性贷款--023001
        String prdId = "";
        log.info("出账申请流水号【{}】计算科目号开始,产品代码【{}】、产品属性【{}】！", pvpLoanApp.getPvpSerno(), cfgPrdBasicinfoDto.getPrdId(),
                cfgPrdBasicinfoDto.getPrdType());
        if (Objects.equals("08", cfgPrdBasicinfoDto.getPrdType())) {// 小微经营性贷款
            prdId = "023001";
        } else if (Objects.equals("09", cfgPrdBasicinfoDto.getPrdType())) {// 小微消费性贷款
            prdId = "022050";
        } else {
            log.info("出账申请流水号【{}】！未查询到满足条件的产品属性！", pvpLoanApp.getPvpSerno());
            throw BizException.error(null, null, "未查询到满足条件的产品属性！");
        }

        // 计算科目
        String hxAccountClass = "";
        // 客户类别 1对公，2对私
        String publicPerson = "";
        // 业务类型（产品ID）
        String bizTypeSub = "";
        // 客户类型
        String cusType = "";
        // 城乡类型
        String cityVillage = "";
        // 企业类型 对公客户才有的字段
        String factoryType = "";
        // 主营业务 行业分类
        String mainDeal = "";
        // 涉农标识 个人客户中"是否农户"字段
        String farmFlag = "";
        // 涉农投向 涉农贷款投向
        String farmDirection = "";
        // 投向行业 贷款投向编号
        String directionOption = "";
        // 担保方式 担保方式细分
        String guarMode = "";
        // 贷款类别 贷款类别
        String loanType = "";
        // 期限 借据放款期限
        String loanTerm = "";

        // 获取贷款出账数据
        // 业务类型（产品ID）
        bizTypeSub = prdId;
        // 涉农贷款投向
        farmDirection = pvpLoanApp.getAgriLoanTer();
        // 贷款投向编号
        if (pvpLoanApp.getLoanTer() != null && !"".equals(pvpLoanApp.getLoanTer())) {
            directionOption = pvpLoanApp.getLoanTer().substring(0, 1);
        }
        // 担保方式细分
        guarMode = pvpLoanApp.getGuarMode();
        // 贷款类别
        loanType = pvpLoanApp.getLoanTypeDetail();
        // 借据放款期限
        loanTerm = pvpLoanApp.getLoanTerm();
        CfgAccountClassChooseDto cfgAccountClassChooseDto = new CfgAccountClassChooseDto();

        // 查询客户类型
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());
        publicPerson = cusBaseClientDto.getCusCatalog();
        log.info("科目计算，获取客户基本信息数据:{}", cusBaseClientDto.toString());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId()).getData();
            log.info("科目计算，获取个人客户基本信息数据:{}", cusCorpDto.toString());
            cusType = cusCorpDto.getCusType();
            cityVillage = cusCorpDto.getCityType();
            factoryType = cusCorpDto.getConType();
            mainDeal = StringUtils.isBlank(cusCorpDto.getTradeClass()) ? "" : cusCorpDto.getTradeClass().substring(0, 1);
        } else {
            // 如果是个人客户
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(pvpLoanApp.getCusId());
            log.info("科目计算，获取个人客户基本信息数据:{}", cusIndivDto.toString());
            cusType = cusIndivDto.getData().getCusType();
            farmFlag = cusIndivDto.getData().getAgriFlg();
        }
        // 期限转换
        if (Integer.parseInt(loanTerm) <= 12) {
            loanTerm = "0";// 短期
        } else {
            loanTerm = "1";// 中长期
        }

        cfgAccountClassChooseDto.setPublicPerson(publicPerson);
        cfgAccountClassChooseDto.setBizTypeSub(bizTypeSub);
        cfgAccountClassChooseDto.setCusType(cusType);
        cfgAccountClassChooseDto.setCityVillage(cityVillage);
        cfgAccountClassChooseDto.setFactoryType(factoryType);
        cfgAccountClassChooseDto.setMainDeal(mainDeal);
        cfgAccountClassChooseDto.setFarmFlag(farmFlag);
        cfgAccountClassChooseDto.setFarmDirection(farmDirection);
        cfgAccountClassChooseDto.setDirectionOption(directionOption);
        cfgAccountClassChooseDto.setGuarMode(guarMode);
        cfgAccountClassChooseDto.setLoanType(loanType);
        cfgAccountClassChooseDto.setLoanTerm(loanTerm);
        System.out.println(cfgAccountClassChooseDto.toString());
        log.info("科目计算，科目计算传参为:{}", cfgAccountClassChooseDto);
        cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByProps(cfgAccountClassChooseDto).getData();
        log.info("科目计算，科目计算回参为:{}", cfgAccountClassChooseDto.toString());
        if (!EcfEnum.ECF020000.key.equals(cfgAccountClassChooseDto.getRtnCode())) {
            throw BizException.error(null, cfgAccountClassChooseDto.getRtnCode(), cfgAccountClassChooseDto.getRtnMsg());
        }
        log.info("出账申请流水号【{}】计算科目号结束,产品代码【{}】、产品属性【{}】、映射信贷科目【{}】、映射核心科目【{}】！", pvpLoanApp.getPvpSerno(), cfgPrdBasicinfoDto.getPrdId(),
                cfgPrdBasicinfoDto.getPrdType(), cfgAccountClassChooseDto.getAccountClass(), cfgAccountClassChooseDto.getHxAccountClass());
        return cfgAccountClassChooseDto;
    }

    /**
     * @param pvpLoanApp, prdId
     * @return cn.com.yusys.yusp.dto.CfgAccountClassChooseDto
     * @author 王玉坤
     * @date 2021/11/19 11:35
     * @version 1.0.0
     * @desc 线上产品科目映射
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto getClassSubNoForOnline(PvpLoanApp pvpLoanApp, String prdId) {
        // 计算科目
        String hxAccountClass = "";
        // 客户类别 1对公，2对私
        String publicPerson = "";
        // 业务类型（产品ID）
        String bizTypeSub = "";
        // 客户类型
        String cusType = "";
        // 城乡类型
        String cityVillage = "";
        // 企业类型 对公客户才有的字段
        String factoryType = "";
        // 主营业务 行业分类
        String mainDeal = "";
        // 涉农标识 个人客户中"是否农户"字段
        String farmFlag = "";
        // 涉农投向 涉农贷款投向
        String farmDirection = "";
        // 投向行业 贷款投向编号
        String directionOption = "";
        // 担保方式 担保方式细分
        String guarMode = "";
        // 贷款类别 贷款类别
        String loanType = "";
        // 期限 借据放款期限
        String loanTerm = "";

        // 获取贷款出账数据
        // 业务类型（产品ID）
        bizTypeSub = prdId;
        // 涉农贷款投向
        farmDirection = pvpLoanApp.getAgriLoanTer();
        // 贷款投向编号
        if (pvpLoanApp.getLoanTer() != null && !"".equals(pvpLoanApp.getLoanTer())) {
            directionOption = pvpLoanApp.getLoanTer().substring(0, 1);
        }
        // 担保方式细分
        guarMode = pvpLoanApp.getGuarMode();
        // 贷款类别
        loanType = pvpLoanApp.getLoanTypeDetail();
        // 借据放款期限
        loanTerm = pvpLoanApp.getLoanTerm();
        CfgAccountClassChooseDto cfgAccountClassChooseDto = new CfgAccountClassChooseDto();

        // 查询客户类型
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());
        publicPerson = cusBaseClientDto.getCusCatalog();
        log.info("科目计算，获取客户基本信息数据:{}", cusBaseClientDto.toString());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId()).getData();
            log.info("科目计算，获取个人客户基本信息数据:{}", cusCorpDto.toString());
            cusType = cusCorpDto.getCusType();
            cityVillage = cusCorpDto.getCityType();
            factoryType = cusCorpDto.getConType();
            mainDeal = StringUtils.isBlank(cusCorpDto.getTradeClass()) ? "" : cusCorpDto.getTradeClass().substring(0, 1);
        } else {
            // 如果是个人客户
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(pvpLoanApp.getCusId());
            log.info("科目计算，获取个人客户基本信息数据:{}", cusIndivDto.toString());
            cusType = cusIndivDto.getData().getCusType();
            farmFlag = cusIndivDto.getData().getAgriFlg();
        }
        // 期限转换
        if (Integer.parseInt(loanTerm) <= 12) {
            loanTerm = "0";// 短期
        } else {
            loanTerm = "1";// 中长期
        }

        cfgAccountClassChooseDto.setPublicPerson(publicPerson);
        cfgAccountClassChooseDto.setBizTypeSub(bizTypeSub);
        cfgAccountClassChooseDto.setCusType(cusType);
        cfgAccountClassChooseDto.setCityVillage(cityVillage);
        cfgAccountClassChooseDto.setFactoryType(factoryType);
        cfgAccountClassChooseDto.setMainDeal(mainDeal);
        cfgAccountClassChooseDto.setFarmFlag(farmFlag);
        cfgAccountClassChooseDto.setFarmDirection(farmDirection);
        cfgAccountClassChooseDto.setDirectionOption(directionOption);
        cfgAccountClassChooseDto.setGuarMode(guarMode);
        cfgAccountClassChooseDto.setLoanType(loanType);
        cfgAccountClassChooseDto.setLoanTerm(loanTerm);
        System.out.println(cfgAccountClassChooseDto.toString());
        log.info("科目计算，科目计算传参为:{}", cfgAccountClassChooseDto);
        cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByProps(cfgAccountClassChooseDto).getData();
        log.info("科目计算，科目计算回参为:{}", cfgAccountClassChooseDto.toString());
        if (!EcfEnum.ECF020000.key.equals(cfgAccountClassChooseDto.getRtnCode())) {
            throw BizException.error(null, cfgAccountClassChooseDto.getRtnCode(), cfgAccountClassChooseDto.getRtnMsg());
        }
        log.info("出账申请流水号【{}】计算科目号结束,产品代码【{}】、映射信贷科目【{}】、映射核心科目【{}】！", pvpLoanApp.getPvpSerno(), prdId,
                cfgAccountClassChooseDto.getAccountClass(), cfgAccountClassChooseDto.getHxAccountClass());
        return cfgAccountClassChooseDto;
    }

    /**
     * @方法名称: calHxkuaijilb
     * @方法描述: 通过科目号查询对应的核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 王玉坤
     * @创建时间: 2021-06-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto calHxkuaijilb(String subJectNo) {
        CfgAccountClassChooseDto cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByAcccountClass(subJectNo).getData();
        if (!EcfEnum.ECF020000.key.equals(cfgAccountClassChooseDto.getRtnCode())) {
            throw BizException.error(null, cfgAccountClassChooseDto.getRtnCode(), cfgAccountClassChooseDto.getRtnMsg());
        }
        return cfgAccountClassChooseDto;
    }

    /**
     * @param pvpLoanApp
     * @return
     * @author 徐盛
     * @date 2021/6/24 14:36
     * @version 1.0.0
     * @desc 档案归档任务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void docArchiveTaskForAccLoan(PvpLoanApp pvpLoanApp) {
        // 生成归档任务
        log.info("开始系统生成档案归档信息");
        try {
            String cusId = pvpLoanApp.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setArchiveMode("02");
            docArchiveClientDto.setDocClass("03");
            if ("01".equals(pvpLoanApp.getBelgLine())) {//小微条线
                docArchiveClientDto.setDocType("22");// 22:小微贷款业务
                docArchiveClientDto.setBizSerno(pvpLoanApp.getPvpSerno());
            } else if ("02".equals(pvpLoanApp.getBelgLine())) {//零售条线
                docArchiveClientDto.setDocType("17");// 17:零售消费类业务
                docArchiveClientDto.setBizSerno(pvpLoanApp.getIqpSerno());
            } else {
                docArchiveClientDto.setDocType("05");// 05:对公及个人经营性贷款
                docArchiveClientDto.setDocBizType("08");// 普通贷款
                docArchiveClientDto.setBizSerno(pvpLoanApp.getPvpSerno());
            }
            String prdTypeProp = pvpLoanApp.getPrdTypeProp();
            // 产品类型属性 STD_PRD_TYPE_PROP P011:省心快贷;P034:房抵e点贷
            if ("P011".equals(prdTypeProp) || "P034".equals(prdTypeProp)) {
                docArchiveClientDto.setDocType("19");// 19 省心快贷/房抵e点贷合同
            }
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(pvpLoanApp.getManagerId());
            docArchiveClientDto.setManagerBrId(pvpLoanApp.getManagerBrId());
            docArchiveClientDto.setInputId(pvpLoanApp.getInputId());
            docArchiveClientDto.setInputBrId(pvpLoanApp.getInputBrId());
            docArchiveClientDto.setContNo(pvpLoanApp.getContNo());
            docArchiveClientDto.setBillNo(pvpLoanApp.getBillNo());
            docArchiveClientDto.setLoanAmt(pvpLoanApp.getPvpAmt());
            docArchiveClientDto.setStartDate(pvpLoanApp.getLoanStartDate());
            docArchiveClientDto.setEndDate(pvpLoanApp.getLoanEndDate());
            docArchiveClientDto.setPrdId(pvpLoanApp.getPrdId());
            docArchiveClientDto.setPrdName(pvpLoanApp.getPrdName());
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if (num < 1) {
                log.info("系统生成档案归档信息失败");
            }
        } catch (Exception e) {
            log.error("生成档案归档任务失败！");
            log.error(e.getMessage(), e);
        }
    }

    /**
     * @param pvpEntrustLoanApp
     * @return
     * @author 徐盛
     * @date 2021/6/24 14:36
     * @version 1.0.0
     * @desc 档案归档任务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void docArchiveTaskForAccEntrustLoan(PvpEntrustLoanApp pvpEntrustLoanApp, String billNo) {
        // 生成归档任务
        log.info("开始系统生成档案归档信息");
        try {
            String cusId = pvpEntrustLoanApp.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setArchiveMode("02");
            docArchiveClientDto.setDocClass("03");
            docArchiveClientDto.setDocType("06");
            docArchiveClientDto.setBizSerno(pvpEntrustLoanApp.getPvpSerno());
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(pvpEntrustLoanApp.getManagerId());
            docArchiveClientDto.setManagerBrId(pvpEntrustLoanApp.getManagerBrId());
            docArchiveClientDto.setInputId(pvpEntrustLoanApp.getInputId());
            docArchiveClientDto.setInputBrId(pvpEntrustLoanApp.getInputBrId());
            docArchiveClientDto.setContNo(pvpEntrustLoanApp.getContNo());
            docArchiveClientDto.setBillNo(billNo);
            docArchiveClientDto.setLoanAmt(pvpEntrustLoanApp.getPvpAmt());
            docArchiveClientDto.setStartDate(pvpEntrustLoanApp.getStartDate());
            docArchiveClientDto.setEndDate(pvpEntrustLoanApp.getEndDate());
            docArchiveClientDto.setPrdId(pvpEntrustLoanApp.getPrdId());
            docArchiveClientDto.setPrdName(pvpEntrustLoanApp.getPrdName());
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if (num < 1) {
                log.info("系统生成档案归档信息失败");
            }
        } catch (Exception e) {
            log.error("生成档案归档任务失败！");
            log.error(e.getMessage(), e);
        }
    }

    /**
     * @方法名称: selectPvpAuthorizeList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpAuthorize> selectPvpAuthorizeList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpAuthorize> list = pvpAuthorizeMapper.selectPvpAuthorizeList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 马顺
     * @date 2021/6/11 14:38
     * @version 1.0.0
     * @desc 委托放款交易处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto sendAuthToCoreForXdWTDK(PvpAuthorize pvpAuthorize) {
        log.info("委托出账申请交易开始，出账流水号【{}】", pvpAuthorize.getTranSerno());
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
        String rtnMsg = "";
        // 出账授权临时对象信息
        PvpAuthorize pvpAuthorizeTemp = null;
        // 台账对象信息
        AccEntrustLoan accEntrustLoan = null;
        // 合同对象信息
        CtrEntrustLoanCont ctrEntrustLoanCont = null;
        // 出账申请对象
        PvpEntrustLoanApp pvpEntrustLoanApp = null;
        // Ln3005返回对象
        Ln3005RespDto ln3005RespDto = null;
        // Ln3020请求对象
        Ln3020ReqDto ln3020ReqDto = null;
        // Ln3020返回对象
        Ln3020RespDto ln3020RespDto = null;
        try {
            // 1、查询出账通知信息
            log.info("根据出账流水号【{}】查询出账通知信息开始", pvpAuthorize.getTranSerno());
            pvpAuthorizeTemp = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
            if (pvpAuthorizeTemp == null) {
                throw BizException.error(null, rtnCode, "未查询到出账通知信息");
            }
            log.info("根据出账流水号【{}】查询出账通知信息结束", pvpAuthorize.getTranSerno());

            // 1.1、查询台账信息
            log.info("根据借据号【{}】查询台账信息开始", pvpAuthorize.getBillNo());
            accEntrustLoan = accEntrustLoanService.queryByBillNo(pvpAuthorize.getBillNo());
            if (accEntrustLoan == null) {
                throw BizException.error(null, rtnCode, "未查询到台账信息");
            }
            log.info("根据借据号【{}】查询台账信息结束", pvpAuthorize.getBillNo());

            // 1.2、查询合同信息
            log.info("根据合同号【{}】查询合同信息开始", pvpAuthorize.getContNo());
            ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(pvpAuthorize.getContNo());
            if (ctrEntrustLoanCont == null) {
                throw BizException.error(null, rtnCode, "未查询到合同信息");
            }
            log.info("根据合同号【{}】查询合同信息结束", pvpAuthorize.getContNo());

            // 1.3、查询出账申请信息
            log.info("根据放款流水号【{}】查询放款信息开始", pvpAuthorize.getPvpSerno());
            pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpAuthorize.getPvpSerno());
            if (pvpEntrustLoanApp == null) {
                throw BizException.error(null, rtnCode, "未查询到放款申请信息");
            }
            log.info("根据放款流水号【{}】查询放款信息结束", pvpAuthorize.getPvpSerno());

            // 3、请求核心接口放款
            // 3.1、拼装请求参数
            ln3020ReqDto = this.buildLn3020ReqBodyForWTDK(pvpAuthorizeTemp, accEntrustLoan, ctrEntrustLoanCont, pvpEntrustLoanApp);
            log.info("请求核心接口放款3020请求报文是：【{}】", JSON.toJSONString(ln3020ReqDto));
            // 3.2 调用核心出账接口开始
            ln3020RespDto = ln3020Service.ln3020(ln3020ReqDto);
            log.info("请求核心接口放款3020响应报文是：【{}】", JSON.toJSONString(ln3020ReqDto));
            // 如果3020接口响应报文不为空，并且受托支付信息也不空，则进行二代受托支付
            if (Objects.nonNull(ln3020RespDto) && CollectionUtils.nonEmpty(ln3020RespDto.getLstdkstzf())) {
                // 根据授权信息表创建受托支付登记簿名单
                toppAccPayDetailService.createToppAccPayDetail(pvpAuthorizeTemp, ln3020RespDto.getLstdkstzf());
            }

            // 4、更新授权通知信息、台账表状态
            // 4.1、台账信息更新
            accEntrustLoan.setAccStatus("1");
            accEntrustLoan.setLoanBalance(accEntrustLoan.getLoanAmt());
            int accEntrustLoanCount = accEntrustLoanService.updateSelective(accEntrustLoan);

            // 4.2、出账授权表信息更新
            // 获取营业日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            pvpAuthorize.setTranDate(openday);
            // 2--发送核心成功
            pvpAuthorize.setTradeStatus("2");

            pvpAuthorize.setAuthStatus("2");
            // 核心交易日期
            pvpAuthorize.setCoreTranDate(ln3020RespDto.getJiaoyirq());
            // 交易流水
            pvpAuthorize.setCoreTranSerno(ln3020RespDto.getJiaoyils());

            int pvpCount = this.updateSelective(pvpAuthorize);
            if (pvpCount > 0) {
                this.docArchiveTaskForAccEntrustLoan(pvpEntrustLoanApp, pvpAuthorize.getBillNo());
                rtnCode = "0000";
            }

            //调额度出账通知接口
            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
            cmisLmt0029ReqDto.setDealBizNo(pvpEntrustLoanApp.getPvpSerno());//交易流水号
            cmisLmt0029ReqDto.setOriginAccNo(pvpEntrustLoanApp.getBillNo());//原台账编号
            cmisLmt0029ReqDto.setNewAccNo(pvpEntrustLoanApp.getBillNo());//新台账编号
            cmisLmt0029ReqDto.setStartDate("");//合同起始日
            cmisLmt0029ReqDto.setEndDate("");//合同到期日
            cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
            log.info("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "，前往额度系统通知出账开始");
            ResultDto<CmisLmt0029RespDto> resultDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
            log.info("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "，前往额度系统通知出账结束");
            if (!"0".equals(resultDto.getCode())) {
                log.error("贷款出账调额度出账通知" + pvpAuthorize.getBillNo() + "前往额度系统通知出账异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDto.getData().getErrorCode();
            if (!EcbEnum.ECB010000.key.equals(code)) {
                log.error("贷款出账调额度出账通知异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }


        } catch (BizException e) {
            // 更新授权信息开始
            pvpAuthorize.setReturnDesc(e.getMessage());
            pvpAuthorize.setReturnCode(e.getErrorCode());
            pvpAuthorize.setAuthStatus("3");
            this.updateSelective(pvpAuthorize);
            LOGGER.error(e.getMessage(), e);
            rtnMsg = "出账失败：" + e.getMessage();
        } catch (Exception e) {
            // 更新授权信息开始
            pvpAuthorize.setReturnDesc(e.getMessage());
            pvpAuthorize.setAuthStatus("3");
            this.updateSelective(pvpAuthorize);
            LOGGER.error(e.getMessage(), e);
            rtnMsg = "出账失败：" + e.getMessage();
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return new ResultDto(result);
    }


    /***
     * @param pvpAuthorize, accEntrustLoan
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Ln3020ReqDto
     * @author mashun
     * @date 2021/6/11 20:25
     * @version 1.0.0
     * @desc 拼组核心请求报文
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Ln3020ReqDto buildLn3020ReqBodyForWTDK(PvpAuthorize pvpAuthorize, AccEntrustLoan accEntrustLoan, CtrEntrustLoanCont ctrEntrustLoanCont, PvpEntrustLoanApp pvpEntrustLoanApp) {
        Ln3020ReqDto ln3020ReqDto = null;
        try {

            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(accEntrustLoan.getPrdId());
            String catalogId;
            if (CfgPrdBasicinfoDto.getData() != null && !"".equals(CfgPrdBasicinfoDto.getData().getCorePrdId())) {
                catalogId = CfgPrdBasicinfoDto.getData().getCorePrdId();
            } else {
                //  抛出错误异常
                throw BizException.error(null, "9999", "未查询到对应核心产品映射");
            }

            // 2.1、请求核心获取产品信息
            ResultDto<Ln3005RespDto> resultDtoLn3005 = this.sendLn3005(accEntrustLoan.getCurType(), catalogId);
            // 2.2、获取返回结构体
            Ln3005RespDto ln3005RespDto = resultDtoLn3005.getData();

            ln3020ReqDto = new Ln3020ReqDto();
            // 贷款借据号
            ln3020ReqDto.setDkjiejuh(pvpAuthorize.getBillNo());
            // 营业机构
            ln3020ReqDto.setYngyjigo(accEntrustLoan.getFinaBrId());
            // 合同编号
            ln3020ReqDto.setHetongbh(pvpAuthorize.getContNo());
            // 账务机构
            ln3020ReqDto.setZhngjigo(accEntrustLoan.getFinaBrId());
            // 货币代号
            ln3020ReqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + pvpAuthorize.getCurType()));
            // 贷款科目
            ln3020ReqDto.setKuaijilb("LNHS135");
            // 产品代码
            ln3020ReqDto.setChanpdma("710003");

            // 本次放款金额
            ln3020ReqDto.setBencfkje(pvpAuthorize.getTranAmt());
            // 客户号
            ln3020ReqDto.setKehuhaoo(pvpAuthorize.getCusId());
            // 客户名称
            ln3020ReqDto.setKehmingc(pvpAuthorize.getCusName());
            // 贷款出账号
            ln3020ReqDto.setDkczhzhh(pvpAuthorize.getBillNo());
            // 贷款入账账号 老信贷根据“是否监管账户”、“连云港机构” 区分 TODO
            ln3020ReqDto.setDkrzhzhh(accEntrustLoan.getLoanPayoutAccno());
            // 贷款入账账号子序号 TODO
            ln3020ReqDto.setDkrzhzxh(accEntrustLoan.getLoanPayoutSubNo());
            // 还款账号 市民贷022097、惠民贷022099、大家e贷022100 取Fldvalue03
            ln3020ReqDto.setHuankzhh(accEntrustLoan.getRepayAccno());
            // 开户操作标志1--录入、2--修改、3--复核、4--直通
            ln3020ReqDto.setDkkhczbz("4");
            // 起息日期
            ln3020ReqDto.setQixiriqi(pvpAuthorize.getTranDate().replace("-", ""));
            // 贷款期限(月)
            ln3020ReqDto.setDkqixian("");
            // 贷款天数
            ln3020ReqDto.setDktiansh("");

            // 利率浮动方式判断 若为浮动利率，目前仅支持LPR加点
            // 利率期限靠档方式判断
            //  02--浮动利率 01--固定利率
            if ("02".equals(pvpEntrustLoanApp.getRateAdjMode())) {
                // 00--LPR加点 委托贷款无此字段
                //if ("00".equals(pvpEntrustLoanApp.getIrFloatType())) {
                    // 利率期限靠档方式1--贷款期限、2--利率期限、3--剩余期限
                    ln3020ReqDto.setLlqxkdfs("2");
                    if ("A1".equals(pvpEntrustLoanApp.getLprRateIntval())) {// 一年期
                        ln3020ReqDto.setLilvqixx("1Y");// 利率期限
                    } else if (("A2".equals(pvpEntrustLoanApp.getLprRateIntval()))) {// 五年期
                        ln3020ReqDto.setLilvqixx("5Y");// 利率期限
                    } else {
                        throw BizException.error("", "9999", "利率期限值有误，请核查！");
                    }
                //} else {
                    //throw BizException.error("", "9999", "利率浮动方式仅支持LPR加点方式，请核查！");
                //}
            } else {
                // 利率期限靠档方式1--贷款期限、2--利率期限、3--剩余期限
                ln3020ReqDto.setLlqxkdfs("1");
                // 利率期限
                ln3020ReqDto.setLilvqixx("");
            }
            // 年/月利率标识:D--日利率\M--月利率\Y--年利率
            ln3020ReqDto.setNyuelilv("Y");
            // 合同利率
            ln3020ReqDto.setHetongll(null);
            // 还款账号子序号
            ln3020ReqDto.setHkzhhzxh(accEntrustLoan.getRepaySubAccno());
            // 备注信息
            ln3020ReqDto.setBeizhuuu("");
            // 合同金额
            ln3020ReqDto.setHetongje(ctrEntrustLoanCont.getContAmt());
            // 贷款账号
            ln3020ReqDto.setDkzhangh(accEntrustLoan.getLoanPayoutAccno());
            // 借据金额
            ln3020ReqDto.setJiejuuje(accEntrustLoan.getLoanAmt());
            // 到期日期
            ln3020ReqDto.setDaoqriqi(accEntrustLoan.getLoanEndDate().replace("-", ""));
            // 正常利率
            DecimalFormat df = new DecimalFormat("0.000000");
            df.setRoundingMode(RoundingMode.HALF_UP);
            ln3020ReqDto.setZhchlilv(new BigDecimal(df.format(accEntrustLoan.getExecRateYear().multiply(new BigDecimal(100)))));
            // 贷款担保方式
            ln3020ReqDto.setDkdbfshi(DicTranEnum.lookup("GUAR_MODE_XDTOHX_" + accEntrustLoan.getGuarMode()));
            // 委托存款业务编码
            ln3020ReqDto.setWtckywbm("");
            // 委托人存款账号
            ln3020ReqDto.setWtrckuzh(accEntrustLoan.getConsignorIdSettlAccno());
            // 委托人存款账号子序号
            ln3020ReqDto.setWtrckzxh("");
            // 委托人客户号
            ln3020ReqDto.setWtrkehuh(accEntrustLoan.getConsignorCusId());
            // 委托人名称
            ln3020ReqDto.setWtrmingc(accEntrustLoan.getConsignorCusName());
            // 委托存款账号
            ln3020ReqDto.setWtckzhao("");
            // 委托存款账号子序号
            ln3020ReqDto.setWtckzixh("");

            // 以下字段来源于LN3005接口返回
            // 贷款对象
            ln3020ReqDto.setDaikduix(ln3005RespDto.getDaikduix());
            // 贷款对象细分
            ln3020ReqDto.setDaikdxxf(ln3005RespDto.getDaikdxxf());
            // 业务分类
            ln3020ReqDto.setYewufenl(ln3005RespDto.getYewufenl());
            // 补贴贷款
            ln3020ReqDto.setButidaik(ln3005RespDto.getButidaik());
            // 循环贷款
            ln3020ReqDto.setXunhdaik(ln3005RespDto.getXunhdaik());
            // 承诺贷款
            ln3020ReqDto.setChendaik(ln3005RespDto.getChendaik());
            // 承诺可循环标志
            ln3020ReqDto.setCnkxhbzh("");
            // 表外产品
            ln3020ReqDto.setBwchapbz(ln3005RespDto.getBwchapbz());
            // 核算方式
            ln3020ReqDto.setHesuanfs(ln3005RespDto.getHesuanfs());
            // 按应计非应计核算
            ln3020ReqDto.setYjfyjhes(ln3005RespDto.getYiyldhes());
            // 按一逾两呆核算
            ln3020ReqDto.setYiyldhes(ln3005RespDto.getYiyldhes());
            // 形态分科目核算
            ln3020ReqDto.setDkxtkmhs(ln3005RespDto.getDkxtkmhs());
            // 允许贷款展期
            ln3020ReqDto.setYunxdkzq(ln3005RespDto.getYunxdkzq());
            // 展期最大次数
            ln3020ReqDto.setZhqzdcsh(ln3005RespDto.getZhqzdcsh().toString());

            // 可售产品代码
            ln3020ReqDto.setKshchpdm(accEntrustLoan.getPrdId());
            // 可售产品名称
            ln3020ReqDto.setKshchpmc(accEntrustLoan.getPrdName());
            // 贷款用途 TODO
            ln3020ReqDto.setDkyongtu("");
            // 表外核1码 TODO
            ln3020ReqDto.setBwhesdma("");
            // 放款记账方式：0--不过借款人存款户 、1--过借款人存款户
            ln3020ReqDto.setFkjzhfsh("1");
            // 允许对行内同名账户放款
            ln3020ReqDto.setHntmifku("");
            // 允许对行内非同名账户放款
            ln3020ReqDto.setHnftmfku("");
            // 允许对内部账户放款
            ln3020ReqDto.setNeibufku("");
            // 放款类型：1--多次放款 、2--单次放款
            ln3020ReqDto.setFangkulx("2");
            // 放款借据管理模式
            ln3020ReqDto.setZdfkjjms("");
            // 周期性放款标志
            ln3020ReqDto.setZhqifkbz("");
            // 放款周期
            ln3020ReqDto.setFkzhouqi("");
            // 放款金额方式
            ln3020ReqDto.setFkfangsh("");
            // 每次放款金额或比例
            ln3020ReqDto.setMcfkjebl(null);
            // 借新还旧控制
            ln3020ReqDto.setJxhjdkkz("");
            // 借新还旧还款控制
            ln3020ReqDto.setJxhjhkkz("");
            // 贸融ABS贷款类型
            ln3020ReqDto.setAbsdkulx("");

            // 贷款形式 1新增贷款 2收回再贷 3借新还旧 4资产重组 5转入 6无还本续贷 8小企业无还本续贷 9其他
            // TODO 缺少优转续贷
            String loanModal = accEntrustLoan.getLoanModal();
            //如果贷款形式为3-借新还旧，需查询借新还旧信息  （贷款形式 ：1-新增、3-借新还旧、4-无缝对接）
            // 核心还旧类型:0--借新还旧、1--无本续贷、2--优转续贷
            ln3020ReqDto.setHjiuleix(DicTranEnum.lookup("LOAN_MODAL_XDTOHX_" + loanModal));

            // 委托贷款为空即可
            List<Lstydkjjh> lstydkjjhList = new ArrayList<>();
            Lstydkjjh lstydkjjh = new Lstydkjjh();
            // 借新还本金
            lstydkjjh.setJxhuanbj(null);
            // 原贷款借据号
            lstydkjjh.setYdkjiejh("");
            // 自还利息
            lstydkjjh.setZihuanlx(null);
            // 自还本金
            lstydkjjh.setZihuanbj(null);
            lstydkjjhList.add(lstydkjjh);
            ln3020ReqDto.setLstydkjjh(lstydkjjhList);

            // 定制还款计划 1--是、0--否
            ln3020ReqDto.setDzhifkjh("");
            List<Lstdkfkjh> lstdkfkjhList = new ArrayList<>();
            Lstdkfkjh lstdkfkjh = new Lstdkfkjh();
            // 放款日期
            lstdkfkjh.setFkriqiii("");
            // 放款金额
            lstdkfkjh.setFkjineee(null);
            // 贷款入账账号
            lstdkfkjh.setDkrzhzhh("");
            // 贷款入账账号子序号
            lstdkfkjh.setDkrzhzxh("");

            lstdkfkjhList.add(lstdkfkjh);
            ln3020ReqDto.setLstdkfkjh(lstdkfkjhList);

            // 允许特殊放款标志；0--不允许、1--允许
            ln3020ReqDto.setYxtsfkbz("");
            // 正常本金
            ln3020ReqDto.setZhchbjin(BigDecimal.ZERO);
            // 逾期本金
            ln3020ReqDto.setYuqibjin(BigDecimal.ZERO);
            // 呆滞本金
            ln3020ReqDto.setDzhibjin(BigDecimal.ZERO);
            // 呆账本金
            ln3020ReqDto.setDaizbjin(BigDecimal.ZERO);
            // 应收应计利息
            ln3020ReqDto.setYsyjlixi(BigDecimal.ZERO);
            // 催收应计利息
            ln3020ReqDto.setCsyjlixi(BigDecimal.ZERO);
            // 应收欠息
            ln3020ReqDto.setYsqianxi(BigDecimal.ZERO);
            // 催收欠息
            ln3020ReqDto.setCsqianxi(BigDecimal.ZERO);
            // 应收应计罚息
            ln3020ReqDto.setYsyjfaxi(BigDecimal.ZERO);
            // 催收应计罚息
            ln3020ReqDto.setCsyjfaxi(BigDecimal.ZERO);
            // 应收罚息
            ln3020ReqDto.setYshofaxi(BigDecimal.ZERO);
            // 催收罚息
            ln3020ReqDto.setCshofaxi(BigDecimal.ZERO);
            // 应计复息
            ln3020ReqDto.setYingjifx(BigDecimal.ZERO);
            // 复息
            ln3020ReqDto.setFuxiiiii(BigDecimal.ZERO);
            // 应计贴息
            ln3020ReqDto.setYingjitx(BigDecimal.ZERO);
            // 应收贴息
            ln3020ReqDto.setYingshtx(BigDecimal.ZERO);
            // 应收费用
            ln3020ReqDto.setYingshfy(BigDecimal.ZERO);
            // 应收罚金
            ln3020ReqDto.setYingshfj(BigDecimal.ZERO);
            // 合作方编号
            ln3020ReqDto.setHezuofbh("");
            // 合作方名称
            ln3020ReqDto.setHezuofmc("");
            // 受托支付业务编码
            ln3020ReqDto.setShtzfhxm("");

            /*******************贷款受托支付列表开始**********************/
            // 受托支付标识 1-是 0-否 光伏贷默认是022098
            // todo 个人一手住房按揭贷款 常熟个人一手住房按揭贷款（资金托管） 是否我行资金监管
            String isBeEntrustedPay = accEntrustLoan.getIsBeEntrustedPay();
            if ("1".equals(isBeEntrustedPay)) {
                //放款资金处理方式0--自主支付、1--转卖方、2--转待销账、3--临时冻结、4--受托支付
                ln3020ReqDto.setFkzjclfs("4");

                // 查询受托支付账户信息
                log.info("根据放款流水号【{}】查询交易对手信息开始", accEntrustLoan.getPvpSerno());
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("bizSerno", accEntrustLoan.getPvpSerno());
                List<ToppAcctSub> toppAcctSubs = toppAcctSubService.selectAll(queryModel);
                log.info("根据放款流水号【{}】查询交易对手信息结束", accEntrustLoan.getPvpSerno());

                // 贷款受托支付列表
                List<Lstdkstzf> lstdkstzflist = new ArrayList<>();
                if (toppAcctSubs != null && toppAcctSubs.size() > 0) {
                    log.info("根据放款流水号【{}】查询交易对手信息结束，拼装交易对手信息开始", accEntrustLoan.getPvpSerno());

                    // 拼装交易对手列表信息
                    toppAcctSubs.stream().forEach(s -> {
                        // 交易对手单条信息
                        Lstdkstzf lstdkstzf = new Lstdkstzf();

                        // 发布系统/渠道  MBL(个人网银)：表示线上 NNT：表示线下
                        if ("1".equals(s.getIsOnline())) {
                            lstdkstzf.setQudaohao("NET");
                        } else {
                            lstdkstzf.setQudaohao("NNT");
                        }
                        // 受托金额
                        lstdkstzf.setStzfjine(s.getToppAmt());
                        // 资金来源账号
                        lstdkstzf.setZjlyzhao("");
                        // 资金来源账号子序号
                        lstdkstzf.setZjlyzzxh("");
                        // 资金来源账号名称
                        lstdkstzf.setZjlyzhmc("");
                        // 资金转入账号
                        lstdkstzf.setZjzrzhao("");
                        // 资金转入账号子序号
                        lstdkstzf.setZjzrzzxh("");
                        // 资金转入账号名称
                        lstdkstzf.setZjzrzhmc("");
                        // 是否本行账户 核心字典项 1--本行账号 2--他行账户
                        lstdkstzf.setDfzhhzhl("1".equals(s.getIsBankAcct()) ? "1" : "2");
                        // 对方账号开户行
                        lstdkstzf.setDfzhhkhh(s.getAcctsvcrNo());
                        // 对方账号开户行名
                        lstdkstzf.setDfzhkhhm(s.getAcctsvcrName());
                        // 对方账号
                        lstdkstzf.setDfzhangh(s.getToppAcctNo());

                        // 根据是否本行账户查询子账户信息,若为对公账户获取子账户序号
                        if ("1".equals(s.getIsBankAcct())) { // 本行
                            // 查询子账户信息
                            Dp2021RespDto dp2021RespDto = this.sendDp2021(s.getToppAcctNo());
                            // 遍历返回信息
                            List<Lstacctinfo> lstacctinfoList = dp2021RespDto.getLstacctinfo();
                            // 核心对公一般只有一条，对私可能会有多条， 直接取 对应币种的活期子户 的第一条数据
                            if (lstacctinfoList != null && lstacctinfoList.size() > 0) {
                                // 子账户序号
                                String zzhxh = "";
                                // 遍历子账户列表，获取序号为00001的序号
                                for (Lstacctinfo lstacctinfo : lstacctinfoList) {
                                    // 账户状态为A--正常 且客户账户类型为0--对公账户，则
                                    if ("A".equals(lstacctinfo.getZhhuztai()) && "0".equals(lstacctinfo.getKehuzhlx())) {
                                        // 账户序号为”00001“
                                        if ("00001".equals(lstacctinfo.getZhhaoxuh())) {
                                            zzhxh = lstacctinfo.getZhhaoxuh();
                                            break;
                                        }
                                    }

                                    // 账户状态为J--控制 且客户账户类型为0--对公账户，则
                                    if ("".equals(zzhxh)) {
                                        if ("J".equals(lstacctinfo.getZhhuztai()) && "0".equals(lstacctinfo.getKehuzhlx())) {
                                            zzhxh = lstacctinfo.getZhhaoxuh();
                                        }
                                    }
                                }

                                // TODO 存量房专用账户到中间业务查 目前缺少个人二手房资金托管新增信息表 Pvp_Esftuoguan_Info
                                if ("801000006838588".equals(s.getToppAcctNo())) {
//                                    KeyedCollection kc = dao.queryFirst("PvpEsftuoguanInfo", null, " WHERE pvp_serno = '" + pa.getSerno() + "'", connection);
//                                    KeyedCollection reqPkgclf = new KeyedCollection();
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "prcscd","clfxcx");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servtp","XDG");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "userid","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "brchno","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "datasq",UUID.randomUUID().toString().replaceAll("-", ""));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servsq",UUID.randomUUID().toString().replaceAll("-", ""));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servdt",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "servti",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "ipaddr","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "mac","");
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "xybhsq", KeyedCollectionUtil.getDataValueAsString(kc, "tgxy_no"));//协议号
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "fkzjh", KeyedCollectionUtil.getDataValueAsString(kc, "buyer_certcode"));//证件号
//                                    KeyedCollectionUtil.setDataValue(reqPkgclf, "fkmc", KeyedCollectionUtil.getDataValueAsString(kc, "buyer_name"));//买方名称
//                                    KeyedCollection res = tradeComponent.call8HeadFormate("clfxcx", reqPkgclf, TradePubConstant.toEsb);
//
//                                    zxh=KeyedCollectionUtil.getDataValueAsString(res, "zzhxh");
//                                    if(zxh==null||"".equals(zxh)){
//                                        throw new EMPException("存量房专用账户发送中间业务查询子账户失败："+ KeyedCollectionUtil.getDataValueAsString(res, "erortx"));
//                                    }
                                }
                                // 对方账号子序号
                                lstdkstzf.setDfzhhzxh(zzhxh);
                            } else {
                                // 若没有查询到对方账户子账户序号，则默认空
                                lstdkstzf.setDfzhhzxh("");
                            }
                        } else {
                            // 非本行账户，对方账号子序号默认为空
                            lstdkstzf.setDfzhhzxh("");
                        }
                        // 对方账户名称
                        lstdkstzf.setDfzhhmch(s.getToppName());
                        // 受托支付方式：1--手工支付、2--自动支付、3--立即支付
                        // 本行账号立即支付，它行账号手工支付
                        lstdkstzf.setStzffshi("1".equals(s.getIsBankAcct()) ? "3" : "1");
                        // 受托支付日期
                        lstdkstzf.setStzfriqi("");
                        // 受托支付处理状态
                        lstdkstzf.setStzfclzt("");
                        // 备注
                        lstdkstzf.setBeizhuxx("");

                        // 小贷塞放贷机构到备注，结尾改为1
                        if ("016001".equals(pvpAuthorize.getDisbOrgNo()) || pvpAuthorize.getDisbOrgNo().endsWith("6")) {
                            // 获取放贷机构
                            String fdjg = pvpAuthorize.getFldvalue41();
                            if ((fdjg != null) && (fdjg.length() > 0)) {
                                lstdkstzf.setBeizhuxx(fdjg.substring(0, 5).concat("1"));
                            }
                        }

                        // 受托比例
                        lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                        // TODO 若是 我行监管账户 而且机构为连云港营业部管理部 目前缺少字段：是否我行监管账户
//                        if(islygysf){
//                            KeyedCollectionUtil.setDataValue(lstdkstzfCall, "dfzhhzxh", "00001");// 对方账号子序号
//                            KeyedCollectionUtil.setDataValue(lstdkstzfCall, "dfzhhmch", "连云港市房地产交易管理服务有限公司");// 对方账号名称
//                            KeyedCollectionUtil.setDataValue(lstdkstzfCall, "dfzhangh", "802000050093188");// 对方账号名称
//                        }
                        lstdkstzflist.add(lstdkstzf);
                    });

                    //  TODO 未查询到对方账户子序号信息还是没有交易对手信息走这段 这段逻辑是不是有问题
                    if (lstdkstzflist == null || lstdkstzflist.size() == 0) {
                        Lstdkstzf lstdkstzf = new Lstdkstzf();
                        // 发布系统/渠道
                        lstdkstzf.setQudaohao("");
                        // 受托金额
                        lstdkstzf.setStzfjine(BigDecimal.ZERO);
                        // 资金来源账号
                        lstdkstzf.setZjlyzhao("");
                        // 资金来源账号子序号
                        lstdkstzf.setZjlyzzxh("");
                        // 资金来源账号名称
                        lstdkstzf.setZjlyzhmc("");
                        // 资金转入账号
                        lstdkstzf.setZjzrzhao("");
                        // 资金转入账号子序号
                        lstdkstzf.setZjzrzzxh("");
                        // 资金转入账号名称
                        lstdkstzf.setZjzrzhmc("");
                        // 对方账号种类
                        lstdkstzf.setDfzhhzhl("");
                        // 对方账号开户行
                        lstdkstzf.setDfzhhkhh("");
                        // 对方账号开户行名
                        lstdkstzf.setDfzhkhhm("");
                        // 对方账号
                        lstdkstzf.setDfzhangh("");
                        // 对方账号子序号
                        lstdkstzf.setDfzhhzxh("");
                        // 对方账号名称
                        lstdkstzf.setDfzhhmch("");
                        // 受托支付方式
                        lstdkstzf.setStzffshi("");
                        // 受托支付日期
                        lstdkstzf.setStzfriqi("");
                        // 受托支付处理状态
                        lstdkstzf.setStzfclzt("");
                        // 备注
                        lstdkstzf.setBeizhuxx("");
                        // 受托比例
                        lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                        lstdkstzflist.add(lstdkstzf);
                    }
                }
                ln3020ReqDto.setLstdkstzf(lstdkstzflist);
            } else {// 非受托支付处理方式
                ln3020ReqDto.setFkzjclfs("0");// 放款资金处理方式

                Lstdkstzf lstdkstzf = new Lstdkstzf();
                List<Lstdkstzf> lstdkstzflist = new LinkedList<>();
                // 发布系统/渠道
                lstdkstzf.setQudaohao("");
                // 受托金额
                lstdkstzf.setStzfjine(BigDecimal.ZERO);
                // 资金来源账号
                lstdkstzf.setZjlyzhao("");
                //资金来源账号子序号
                lstdkstzf.setZjlyzzxh("");
                // 资金来源账号名称
                lstdkstzf.setZjlyzhmc("");
                // 资金转入账号
                lstdkstzf.setZjzrzhao("");
                // 资金转入账号子序号
                lstdkstzf.setZjzrzzxh("");
                // 资金转入账号名称
                lstdkstzf.setZjzrzhmc("");
                // 对方账号种类
                lstdkstzf.setDfzhhzhl("");
                // 对方账号开户行
                lstdkstzf.setDfzhhkhh("");
                // 对方账号开户行名
                lstdkstzf.setDfzhkhhm("");
                // 对方账号
                lstdkstzf.setDfzhangh("");
                // 对方账号子序号
                lstdkstzf.setDfzhhzxh("");
                // 对方账号名称
                lstdkstzf.setDfzhhmch("");
                // 受托支付方式
                lstdkstzf.setStzffshi("");
                // 受托支付日期
                lstdkstzf.setStzfriqi("");
                // 受托支付处理状态
                lstdkstzf.setStzfclzt("");
                // 备注
                lstdkstzf.setBeizhuxx("");
                // 受托比例
                lstdkstzf.setShtzfbli(BigDecimal.ZERO);
                lstdkstzflist.add(lstdkstzf);
                ln3020ReqDto.setLstdkstzf(lstdkstzflist);
            }
            /*******************贷款受托支付列表结束**********************/

            //计息本金规则
            ln3020ReqDto.setJixibjgz("");
            //利息计算方法
            ln3020ReqDto.setLixijsff("");
            //计息头尾规则
            ln3020ReqDto.setJixitwgz("");
            //计息最小金额
            ln3020ReqDto.setJixizxje(new BigDecimal("0"));
            //计息舍入规则
            ln3020ReqDto.setJixisrgz("");
            //舍入最小单位
            ln3020ReqDto.setSrzxdanw("");
            //分段计息标志
            ln3020ReqDto.setFdjixibz("");
            //早起息标志
            ln3020ReqDto.setZaoqixbz("");
            //晚起息标志
            ln3020ReqDto.setWanqixbz("");
            //起息天数
            ln3020ReqDto.setQixitshu("");

            // 利率类型 核心字典项1--PBOC、2--SHIBOR、3--HIBOR、4--LIBOR、5--SIBOR、6--固定利率
            if (StringUtils.isEmpty(accEntrustLoan.getRateAdjMode())) {
                throw BizException.error("", "9999", "利率调整方式不能为空！");
            }
            ln3020ReqDto.setLilvleix(DicTranEnum.lookup("RATE_ADJ_MODE_XDTOHX_" + accEntrustLoan.getRateAdjMode()));
            // 利率调整周期
            ln3020ReqDto.setLilvtzzq("");

            // 根据利率类型填入利率相关参数 1--浮动利率 6--固定利率
            if ("1".equals(ln3020ReqDto.getLilvleix())) { // 非固定利率
                // 根据利率浮动方式填入参数
                //if ("00".equals(pvpEntrustLoanApp.getIrFloatType())) {// LPR加点
                    // 利率浮动方式 1-按值浮动
                    ln3020ReqDto.setLilvfdfs("1");
                    // 正常利率编号
                    ln3020ReqDto.setZclilvbh("LPR1");
                    // 利率浮动值
                    ln3020ReqDto.setLilvfdzh(accEntrustLoan.getRateFloatPoint());
                //} else {
                    //log.info("利率浮动方式错误:" + pvpEntrustLoanApp.getIrFloatType());
                    //throw BizException.error("", "", "该笔出账业务的利率浮动方式错误,请检查!");
                //}

                if ("IMM".equals(accEntrustLoan.getRateAdjType())) {// 立即调整
                    log.info("利率调整模式【{}】", "立即调整");
                    ln3020ReqDto.setLilvtzfs("4");
                } else if ("DDA".equals(accEntrustLoan.getRateAdjType())) {// 满一年调整
                    log.info("利率调整模式【{}】", "满一年调整");
                    ln3020ReqDto.setLilvtzfs("1");
                } else if ("NYF".equals(accEntrustLoan.getRateAdjType())) {// 每年1.1调整
                    log.info("利率调整模式【{}】", "每年1.1调整");
                    ln3020ReqDto.setLilvtzfs("2");
                    // 利率调整周期
                    ln3020ReqDto.setLilvtzzq("1YAF");
                } else if ("FIX".equals(accEntrustLoan.getRateAdjType())) {// 固定日
                    ln3020ReqDto.setLilvtzfs("2");
                    // 下一次利率调整间隔单位
                    String unit = accEntrustLoan.getNextRateAdjUnit();
                    // 下一次利率调整间隔
                    String freq = accEntrustLoan.getNextRateAdjInterval();
                    // 第一次调整日
                    String frd = accEntrustLoan.getFirstAdjDate();

                    // 计算调整日期
                    // 按月
                    if ("M".equals(unit)) {
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(8, 10));
                    } else if ("Q".equals(unit)) {// 按季
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(8, 10) + "E");
                    } else if ("Y".equals(unit)) {// 按年
                        ln3020ReqDto.setLilvtzzq(freq + unit + "A" + frd.substring(5, 7) + frd.substring(8, 10));
                    }
                } else if ("FIA".equals(accEntrustLoan.getRateAdjType())) {//按月
                    ln3020ReqDto.setLilvtzfs("2");
                    ln3020ReqDto.setLilvtzzq("1MA01");
                } else {
                    ln3020ReqDto.setLilvtzfs("");
                    log.info("利率调整选项错误:" + pvpEntrustLoanApp.getRateAdjType());
                    throw BizException.error("", "", "暂不支持该利率调整选项,请检查!");
                }
            } else if ("6".equals(ln3020ReqDto.getLilvleix())) {// 固定利率填充 1--浮动利率 6--固定利率
                // 正常利率编号
                ln3020ReqDto.setZclilvbh("LPR1");
                //利率类型选择固定利率时，利率浮动方式必须选择不浮动
                ln3020ReqDto.setLilvfdfs("0");
                //利率类型选择固定利率时，利率调整方式必须选择不调整
                // 利率调整方式:0--不调整、1--按对月对日调整、2--按指定周期调整、3--按次月对日调整、4--即时调整、5--次月发放日调整
                ln3020ReqDto.setLilvtzfs("0");
                // 利率浮动值
                ln3020ReqDto.setLilvfdzh(accEntrustLoan.getRateFloatPoint());
            } else {
                throw BizException.error("", "9999", "利率调整方式不符合规范，请核查！");
            }

            // 核心基本信息 取基准利率参数之一，利率类型为非6--固定利率时必输
            if ("1".equals(ln3020ReqDto.getLilvleix())) {
                // 逾期利率编号
                ln3020ReqDto.setYuqillbh("LPR1");
                // 逾期罚息浮动方式0-不浮动、1-按值浮动、2-按比例浮动
                ln3020ReqDto.setYqfxfdfs("2");
                // 逾期利率调整方式 与正常利率保持一致
                ln3020ReqDto.setYuqitzfs(ln3020ReqDto.getLilvtzfs());

                // 复利利率编号
                ln3020ReqDto.setFulilvbh("LPR1");
                // 复利利率浮动方式
                ln3020ReqDto.setFulifdfs("2");
                // 复利利率调整方式 与正常利率保持一致
                ln3020ReqDto.setFulitzfs(ln3020ReqDto.getLilvtzfs());
            } else if ("6".equals(ln3020ReqDto.getLilvleix())) { // 固定利率填充
                // 逾期利率编号
                ln3020ReqDto.setYuqillbh("LPR1");
                // 逾期罚息浮动方式 2--按比例浮动
                ln3020ReqDto.setYqfxfdfs("2");
                // 复利利率编号
                ln3020ReqDto.setFulilvbh("LPR1");
                // 复利利率浮动方式 2--按比例浮动
                ln3020ReqDto.setFulifdfs("2");
                // 逾期利率调整方式 0--不调整
                ln3020ReqDto.setYuqitzfs("0");
                // 复利利率调整方式 0--不调整
                ln3020ReqDto.setFulitzfs("0");
            } else {
                throw BizException.error("", "9999", "利率调整方式不符合规范，请核查！");
            }

            // 逾期利率参考类型 1--基准利率、2--正常利率、3--指定利率
            ln3020ReqDto.setYqllcklx("2");
            // 逾期罚息浮动值--逾期利率浮动比例
            ln3020ReqDto.setYqfxfdzh(new BigDecimal(df.format(pvpEntrustLoanApp.getOverdueRatePefloat().multiply(new BigDecimal("100")))));
            // 逾期利率
            ln3020ReqDto.setYuqililv(new BigDecimal(df.format(pvpEntrustLoanApp.getOverdueExecRate().multiply(new BigDecimal("100")))));
            // 逾期利率调整周期
            ln3020ReqDto.setYuqitzzq(ln3020ReqDto.getLilvtzzq());
            // 逾期年月利标识
            ln3020ReqDto.setYuqinyll("");

            // 复利利率参考类型 1--基准利率、2--正常利率、3--指定利率
            ln3020ReqDto.setFlllcklx("2");
            //复利利率浮动值--取复息利率浮动比例
            ln3020ReqDto.setFulifdzh(new BigDecimal(df.format(pvpEntrustLoanApp.getCiRatePefloat().multiply(new BigDecimal("100")))));
            // 复利利率
            ln3020ReqDto.setFulililv(new BigDecimal(df.format(pvpEntrustLoanApp.getCiExecRate().multiply(new BigDecimal("100")))));
            // 复利利率调整周期
            ln3020ReqDto.setFulitzzq(ln3020ReqDto.getLilvtzzq());
            // 复利月利率标识
            ln3020ReqDto.setFulilvny("");

            // 计息规则
            ln3020ReqDto.setJixiguiz("");
            // 零头计息规则
            ln3020ReqDto.setLtjixigz("");
            // 计息标志 0--不计息 1--计息
            ln3020ReqDto.setJixibzhi("");
            // 计复息标识
            ln3020ReqDto.setJfxibzhi("");
            // 复息计息标志
            ln3020ReqDto.setFxjxbzhi("");
            // 计提规则
            ln3020ReqDto.setJitiguiz("");
            // 预收息方式
            ln3020ReqDto.setYushxfsh("");
            // 预收息总额
            ln3020ReqDto.setYushxize(new BigDecimal("0"));
            // 预收息扣息来源账号
            ln3020ReqDto.setYsxlyzhh("");
            // 预收息扣息账号序号
            ln3020ReqDto.setYsxlyzxh("");
            // 利息摊销周期
            ln3020ReqDto.setLixitxzq("");
            // 每次摊销方式
            ln3020ReqDto.setMeictxfs("");
            // 每次摊销比例
            ln3020ReqDto.setMeictxbl(new BigDecimal("0"));
            // 贴息标志
            ln3020ReqDto.setTiexibzh("");
            // 补贴金额计算方式
            ln3020ReqDto.setButijejs("");
            // 首次还款日模式
            ln3020ReqDto.setScihkrbz("");

            // 借e点特殊判断
            if ("022101".equals(accEntrustLoan.getPrdId())) {
                if (Objects.equals("A002", accEntrustLoan.getRepayMode())) {// 等额本息
                    ln3020ReqDto.setScihkrbz("2"); //首次还款日模式
                } else {// 其余还款方式
                    ln3020ReqDto.setScihkrbz("1"); //首次还款日模式
                }
            }
            // 还款相关参数配置
            // 小贷特殊还款方式
            if ("016000".equals(accEntrustLoan.getManagerBrId())) {
                if ("A022".equals(accEntrustLoan.getRepayMode())) {//10年期等额本息
                    //是否特殊贷款标志 1--是 0--否
                    ln3020ReqDto.setSftsdkbz("1");
                    // 特殊贷款计息总期数
                    ln3020ReqDto.setTsdkjxqs("240");
                }
            } else {
                //是否特殊贷款标志 1--是 0--否
                ln3020ReqDto.setSftsdkbz("0");
                // 特殊贷款计息总期数
                ln3020ReqDto.setTsdkjxqs("");
            }

            /******************贷款利率分段计息参数******************/
            // TODO 缺少台账免息信息表记录免息信息，需与老信贷人员确认 小微公众号项目免息需求,苏宁智能营销 白领贷免息
            // 1、先获取“是否免息”、“免息天数信息”
            // 是否免息天数
            boolean isLilvfend = false;
            // 免息天数
            int freeDays = 0;
            if ("016000".equals(accEntrustLoan.getManagerBrId()) || "022100".equals(accEntrustLoan.getPrdId()) || "022028".equals(accEntrustLoan.getPrdId())) {
                // 获取免息标识

                // 获取免息天数

            }

            // TODO 2、获取是否分段计息标识  零售需求暂时没有
            String lilvfend = accEntrustLoan.getIsSegInterest();
            // 分段计息列表信息
            List<Lstdkllfd> lstdkllfdList = new ArrayList<>();
            // 根据分段计息标识、是否免息判断
            if ("1".equals(lilvfend)) {// 分段计息标识为“是”
                // 利率分段计息标识
                ln3020ReqDto.setLilvfend(lilvfend);

                // 分段计息查询结果进行取值
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("loanPvpSeq", accEntrustLoan.getPvpSerno());
                List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubService.querySegInterstSub(queryModel);
                if (list != null && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub = list.get(i);
                        Lstdkllfd lstdkllfd = new Lstdkllfd();
                        lstdkllfd.setLilvtzzq("");
                        String idMode = toConverRate(pvpLoanAppSegInterstSub.getRateAdjMode());
                        lstdkllfd.setXuhaoooo(i + 1);
                        lstdkllfd.setLilvfdfs("0");// 利率浮动方式
                        lstdkllfd.setLilvleix("6");// 利率类型
                        lstdkllfd.setLilvtzfs("0");// 利率调整方式
                        lstdkllfd.setQishriqi(pvpLoanAppSegInterstSub.getSegStartdate().replace("-", ""));// 起始日期
                        lstdkllfd.setDaoqriqi(pvpLoanAppSegInterstSub.getSegEnddate().replace("-", ""));// 到期日期
                        lstdkllfd.setLilvfdzh(pvpLoanAppSegInterstSub.getRateFloatPoint());// 利率浮动值--取利率浮动比例
                        lstdkllfd.setZhchlilv(pvpLoanAppSegInterstSub.getExecRateYear());// 正常利率
                        lstdkllfd.setLilvtzzq("");// 利率调整周期
                        lstdkllfd.setZclilvbh("");// 正常利率编号
                        lstdkllfdList.add(lstdkllfd);
                    }
                    ln3020ReqDto.setLstdkllfd(lstdkllfdList);
                }
            } else if (isLilvfend) { // 若免息
                // 利率分段计息标识 1--是 0--否
                ln3020ReqDto.setLilvfend("1");

                // 贷款利率分段信息
                Lstdkllfd lstdkllfdFirst = new Lstdkllfd();
                // 序号
                lstdkllfdFirst.setXuhaoooo(1);
                // 起始日期
                lstdkllfdFirst.setQishriqi(pvpAuthorize.getTranDate().replace("-", ""));
                LocalDate freeEndDate = LocalDate.parse(pvpAuthorize.getTranDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).plusDays(freeDays);
                log.info("借据号【{}】,根据免息天数【{}】，出账日期【{}】，计算免息到期日结束，到期日【{}】", pvpAuthorize.getBillNo(),
                        freeDays, pvpAuthorize.getTranDate(), freeEndDate.toString());
                // 判断免息截止日期是否大于借据截止日
                boolean secordFlag = true;
                if (freeEndDate.compareTo(LocalDate.parse(accEntrustLoan.getLoanEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"))) >= 0) {
                    secordFlag = false;
                    // 到期日期 若免息截止日期大于借据截止日 ，取借据截止日期，反之取免息截止日
                    lstdkllfdFirst.setDaoqriqi(accEntrustLoan.getLoanEndDate().replace("-", ""));
                } else {
                    // 到期日期
                    lstdkllfdFirst.setDaoqriqi(freeEndDate.toString().replace("-", ""));
                }
                // 利率浮动方式 0--不浮动
                lstdkllfdFirst.setLilvfdfs("0");
                // 利率类型 6--固定利率
                lstdkllfdFirst.setLilvleix("6");
                // 利率调整方式 0--不调整
                lstdkllfdFirst.setLilvtzfs("0");
                // 利率浮动值
                lstdkllfdFirst.setLilvfdzh(BigDecimal.ZERO);
                // 正常利率
                lstdkllfdFirst.setZhchlilv(BigDecimal.ZERO);
                // 利率调整周期
                lstdkllfdFirst.setLilvtzzq("");
                // 正常利率编号
                lstdkllfdFirst.setZclilvbh("");

                lstdkllfdList.add(lstdkllfdFirst);

                // 免息截止日期在贷款到期日之内 分两段计息
                if (secordFlag) {
                    // 第二段免息贷款利率分段
                    Lstdkllfd lstdkllfdSecond = new Lstdkllfd();
                    // 序号
                    lstdkllfdSecond.setXuhaoooo(2);
                    // 起始日期 为免息到期日期
                    lstdkllfdSecond.setQishriqi(freeEndDate.toString().replace("-", ""));
                    // 到期日期 为借据到期日期
                    lstdkllfdSecond.setDaoqriqi(accEntrustLoan.getLoanEndDate().replace("-", ""));
                    // 利率浮动方式 0--不浮动
                    lstdkllfdSecond.setLilvfdfs("0");
                    // 利率类型 6--固定利率
                    lstdkllfdSecond.setLilvleix(ln3020ReqDto.getLilvleix());
                    // 利率调整方式 0--不调整
                    lstdkllfdSecond.setLilvtzfs("0");
                    // 利率浮动值
                    lstdkllfdSecond.setLilvfdzh(BigDecimal.ZERO);
                    // 正常利率
                    lstdkllfdSecond.setZhchlilv(ln3020ReqDto.getZhchlilv());
                    // 利率调整周期
                    lstdkllfdSecond.setLilvtzzq("");
                    // 正常利率编号
                    lstdkllfdSecond.setZclilvbh("");

                    lstdkllfdList.add(lstdkllfdSecond);
                }
                ln3020ReqDto.setLstdkllfd(lstdkllfdList);
            } else { // 无分段计息与免息
                // 利率分段计息标识 1--是 0--否
                ln3020ReqDto.setLilvfend("0");
                // 第二段免息贷款利率分段
                Lstdkllfd lstdkllfd = new Lstdkllfd();
                // 序号
                lstdkllfd.setXuhaoooo(null);
                // 起始日期 为免息到期日期
                lstdkllfd.setQishriqi("");
                // 到期日期 为借据到期日期
                lstdkllfd.setDaoqriqi("");
                // 利率浮动方式 0--不浮动
                lstdkllfd.setLilvfdfs("");
                // 利率类型 6--固定利率
                lstdkllfd.setLilvleix("");
                // 利率调整方式 0--不调整
                lstdkllfd.setLilvtzfs("");
                // 利率浮动值
                lstdkllfd.setLilvfdzh(null);
                // 正常利率
                lstdkllfd.setZhchlilv(null);
                // 利率调整周期
                lstdkllfd.setLilvtzzq("");
                // 正常利率编号
                lstdkllfd.setZclilvbh("");

                lstdkllfdList.add(lstdkllfd);
            }
            ln3020ReqDto.setLstdkllfd(lstdkllfdList);
            /******************分段计息处理逻辑结束**********************/

            /****************贷款多贴息账户**************/
            List<Lstdktxzh> lstdktxzhList = new ArrayList<>();
            Lstdktxzh lstdktxzh = new Lstdktxzh();
            // 贴息比率
            lstdktxzh.setTiexibil(null);
            // 生效日期
            lstdktxzh.setShengxrq("");
            // 到期日期
            lstdktxzh.setDaoqriqi("");
            // 贴息账号
            lstdktxzh.setTiexizhh("");
            // 贴息账号子序号
            lstdktxzh.setTxzhhzxh("");
            // 贴息账户名称
            lstdktxzh.setTxzhhmch("");
            // 贴息记复利标识
            lstdktxzh.setFxjxbzhi("");
            // 自动扣款标识
            lstdktxzh.setZdkoukbz("");

            lstdktxzhList.add(lstdktxzh);
            ln3020ReqDto.setLstdktxzh(lstdktxzhList);
            /****************贷款多贴息账户**************/

            /****************还款方式信息转换**************/
            Ln3235RespDto ln3235RespDto = null;
            // 获取还款方式
            String repayMode = DicTranEnum.lookup("REPAY_MODE_XDTOHX_" + accEntrustLoan.getRepayMode());

            if ("".equals(repayMode)) { // 若转换后还款方式为空则表示特殊还款方式
                // 调用核心还款方式转换接口Ln3235
                ln3235RespDto = this.sendLn3235(pvpAuthorize, null, accEntrustLoan);

                // 根据Ln3235接口返回数据填充请求报文
                // 还款方式
                ln3020ReqDto.setHuankfsh(ln3235RespDto.getHuankfsh());
                // 定制还款计划:1--是、0--否
                ln3020ReqDto.setDzhhkjih(ln3235RespDto.getDzhhkjih());
                // 期限变更调整计划
                ln3020ReqDto.setQxbgtzjh(ln3235RespDto.getQxbgtzjh());
                // 利率变更调整计划
                ln3020ReqDto.setLlbgtzjh(ln3235RespDto.getLlbgtzjh());
                // 多次放款调整计划
                ln3020ReqDto.setDcfktzjh(ln3235RespDto.getDcfktzjh());
                // 提前还款调整计划
                ln3020ReqDto.setTqhktzjh(ln3235RespDto.getTqhktzjh());
                // 本金分段
                ln3020ReqDto.setBenjinfd(ln3235RespDto.getBenjinfd());
                // 首次还款日
                ln3020ReqDto.setScihkrbz(ln3235RespDto.getScihkrbz());

                // 贷款还本计划
                List<Lsdkhbjh> lsdkhbjhList = ln3235RespDto.getLsdkhbjh();
                if (CollectionUtils.nonEmpty(lsdkhbjhList)) {
                    List<Lstdkhbjh> lstdkhbjhList = lsdkhbjhList.parallelStream().map(lsdkhbjh -> {
                        Lstdkhbjh lstdkhbjh = new Lstdkhbjh();
                        BeanUtils.copyProperties(lsdkhbjh, lstdkhbjh);
                        return lstdkhbjh;
                    }).collect(Collectors.toList());
                    ln3020ReqDto.setLstdkhbjh(lstdkhbjhList);
                }

                // 本金分段计划
                List<Lsdkbjfd> lsdkbjfdList = ln3235RespDto.getLsdkbjfd();
                if (CollectionUtils.nonEmpty(lsdkbjfdList)) {
                    List<Lstdkbjfd> lstdkhbjhList = lsdkbjfdList.parallelStream().map(lsdkbjfd -> {
                        Lstdkbjfd lstdkbjfd = new Lstdkbjfd();
                        BeanUtils.copyProperties(lsdkbjfd, lstdkbjfd);
                        return lstdkbjfd;
                    }).collect(Collectors.toList());
                    ln3020ReqDto.setLstdkbjfd(lstdkhbjhList);
                }

                // 还款周期
                ln3020ReqDto.setHkzhouqi("");
                // 还本周期
                ln3020ReqDto.setHuanbzhq("");
                // 逾期还款周期
                ln3020ReqDto.setYuqhkzhq("");
            } else {// 若非特殊还款方式，按如下逻辑处理
                // 小微特殊还款方式判断 A023--按月付息，定期还本 经与老信贷确认，此种还款方式小贷已不使用
                if ("016000".equals(accEntrustLoan.getManagerBrId()) && "A023".equals(accEntrustLoan.getRepayMode())) {
                    log.info("小贷已暂不支持A023--按月付息，定期还本还款方式");
                } else {
                    // 定制还款计划:1--是、0--否
                    ln3020ReqDto.setDzhhkjih("0");
                }

                // 还款方式：1--利随本清、2--多次还息一次还本、3--等额本息、4--等额本金、5--等比累进、6--等额累进、7--定制还款
                ln3020ReqDto.setHuankfsh(repayMode);
                // 结息间隔周期
                String eiIntervalCycle = accEntrustLoan.getEiIntervalCycle();
                // 结息周期单位
                String eiIntervalUnit = accEntrustLoan.getEiIntervalUnit();
                // 还款日
                String repayDay = accEntrustLoan.getDeductDay();
                // 计算还款日
                if (!StringUtils.isEmpty(repayDay)) {
                    repayDay = repayDay.length() == 1 ? "0".concat(repayDay) : repayDay;
                }

                // 标识位 对公客户号以8开头的
                String sy_flag = "A";
                // 节假日顺延标志位,对公客户默认展示1
                String isDelay = pvpEntrustLoanApp.getIsHolidayDelay();

                if (pvpEntrustLoanApp.getCusId() != null && pvpEntrustLoanApp.getCusId().startsWith("8") && "1".equals(isDelay)) {
                    sy_flag = "N";
                }
                // 根据还款周期单位赋值
                String temp = "";
                if ("M".equals(eiIntervalUnit)) {// 按月
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay;
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else if ("Q".equals(eiIntervalUnit)) {// 按季
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay + "E";
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else if ("Y".equals(eiIntervalUnit)) {// 按年
                    temp = eiIntervalCycle + eiIntervalUnit + sy_flag + "12" + repayDay;
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi(temp);
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq(temp);
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq(temp);
                } else {
                    // 还款周期
                    ln3020ReqDto.setHkzhouqi("");
                    // 还本周期
                    ln3020ReqDto.setHuanbzhq("");
                    // 逾期还款周期
                    ln3020ReqDto.setYuqhkzhq("");
                }
                // 期限变更调整计划
                ln3020ReqDto.setQxbgtzjh("");
                // 利率变更调整计划
                ln3020ReqDto.setLlbgtzjh("");
                // 多次放款调整计划
                ln3020ReqDto.setDcfktzjh("");
                // 提前还款调整计划
                ln3020ReqDto.setTqhktzjh("");
                // 本金分段
                ln3020ReqDto.setBenjinfd("");
            }
            // 等额处理规则
            // 还款方式 3--等额本息 4--等额本金
            if ("3".equals(repayMode) || ("4".equals(repayMode) && "022001".equals(accEntrustLoan.getPrdId()) || "022002".equals(accEntrustLoan.getPrdId()) ||
                    "022020".equals(accEntrustLoan.getPrdId()) || "022021".equals(accEntrustLoan.getPrdId()) || "022024".equals(accEntrustLoan.getPrdId()) ||
                    "022031".equals(accEntrustLoan.getPrdId()) || "022040".equals(accEntrustLoan.getPrdId()) || "022051".equals(accEntrustLoan.getPrdId()) ||
                    "022052".equals(accEntrustLoan.getPrdId()) || "022053".equals(accEntrustLoan.getPrdId()) || "022055".equals(accEntrustLoan.getPrdId()) ||
                    "022056".equals(accEntrustLoan.getPrdId()) || "022054".equals(accEntrustLoan.getPrdId()))) {
                // 20190218修改 覃明贵确定还款方式为等额本息，计息规则传01，其余传空
                ln3020ReqDto.setJixiguiz("01");
                // 核心吴高科 说等额本息时，等额处理规则=2--标准   2019-11-22焦广福要求修改为1 0--不适用、1--标准+差额、2--标准、3--标准/实际
                ln3020ReqDto.setDechligz("1");
            } else {
                ln3020ReqDto.setJixiguiz("");
                ln3020ReqDto.setDechligz("");
            }

            // 还款方式为1--利随本清 不计复利 其余为空
            if ("1".equals(repayMode)) {
                ln3020ReqDto.setJfxibzhi("0");
                ln3020ReqDto.setJixiguiz("10");//2021-8-23 与唐瑜确认
            } else {
                ln3020ReqDto.setJfxibzhi("");
            }

            // 贷款还本计划
            if (Objects.isNull(ln3020ReqDto.getLstdkhbjh())) {
                List<Lstdkhbjh> lsdkhbjhList = new ArrayList<>();
                Lstdkhbjh lstdkhbjh = new Lstdkhbjh();
                // 定制还款种类
                lstdkhbjh.setDzhhkzhl("");
                // 需足额提前还款
                lstdkhbjh.setDzhhkzhl("");
                // 定制还款日期
                lstdkhbjh.setDzhkriqi("");
                // 还本金额
                lstdkhbjh.setHuanbjee(null);
                // 还款账号
                lstdkhbjh.setHuankzhh("");
                // 还款账号子序号
                lstdkhbjh.setHkzhhzxh("");
                // 还息方式
                lstdkhbjh.setTqhkhxfs("");
                // 还款遇假日规则
                lstdkhbjh.setHkyujrgz("");
                // 是否有宽限期
                lstdkhbjh.setSfyxkuxq("");
                // 宽限期天数
                lstdkhbjh.setKuanxqts(null);
                // 宽限期节假日规则
                lstdkhbjh.setKxqjjrgz("");
                lsdkhbjhList.add(lstdkhbjh);
                ln3020ReqDto.setLstdkhbjh(lsdkhbjhList);
            }
            // 本金分段登记
            if (Objects.isNull(ln3020ReqDto.getLstdkbjfd())) {
                List<Lstdkbjfd> lstdkbjfdList = new ArrayList<>();
                Lstdkbjfd lstdkbjfd = new Lstdkbjfd();
                // 累进值
                lstdkbjfd.setLeijinzh(null);
                // 起始日期
                lstdkbjfd.setQishriqi("");
                // 本阶段还款方式
                lstdkbjfd.setBjdhkfsh("");
                // 还款周期
                lstdkbjfd.setHkzhouqi("");
                // 本阶段还款期数
                lstdkbjfd.setBjdhkqsh(null);
                // 累进区间期数
                lstdkbjfd.setLeijqjsh("");
                // 序号
                lstdkbjfd.setXuhaoooo(null);
                // 计息规则
                lstdkbjfd.setJixiguiz("");
                // 等额处理规则
                lstdkbjfd.setDechligz("");
                // 到期日期
                lstdkbjfd.setDaoqriqi("");
                // 本阶段还本金额
                lstdkbjfd.setBjdhbjee(null);
            }
            /****************还款方式信息转换结束**************/

            // 累进区间期数
            ln3020ReqDto.setLeijqjsh("");
            // 累进值
            ln3020ReqDto.setLeijinzh(null);
            // 累进首段期数
            ln3020ReqDto.setLjsxqish("");
            // 每期还本比例
            ln3020ReqDto.setMqhbbili(null);
            // 每期还款总额
            ln3020ReqDto.setMeiqhkze(null);
            // 每期还本金额
            ln3020ReqDto.setMeiqhbje(null);
            // 保留金额
            ln3020ReqDto.setBaoliuje(null);
            // 还款顺序编号
            ln3020ReqDto.setHkshxubh("");
            // 还款期限(月)
            ln3020ReqDto.setHkqixian("");
            // 节假日利息顺延标志
            ln3020ReqDto.setJrlxsybz("");
            // 频率还款标志
            ln3020ReqDto.setDuophkbz("");
            // 下一次还款日
            ln3020ReqDto.setXycihkrq("");
            // 末期还款方式
            ln3020ReqDto.setMqihkfsh("");
            // 不足额扣款方式
            ln3020ReqDto.setBzuekkfs("");

            // 扣款方式判断 核心字典项1--自动扣款
            if ("AUTO".equals(accEntrustLoan.getDeductType())) {// AUTO--自动
                // 自动扣款标志
                ln3020ReqDto.setZdkoukbz("1");
            } else if ("MANL".equals(accEntrustLoan.getDeductType())) {// MANL--手动
                // 自动扣款标志
                ln3020ReqDto.setZdkoukbz("0");
            } else {
                throw BizException.error("","9999","扣款方式不符合规范，请核查！");
            }

            // 指定文件批量扣款标识
            ln3020ReqDto.setZdplkkbz("");
            // 自动结清贷款标志
            ln3020ReqDto.setZdjqdkbz("");
            // 贷款到期假日规则
            ln3020ReqDto.setZbjrsygz("");
            // 签约循环贷款标志
            ln3020ReqDto.setQyxhdkbz("");
            // 签约账号
            ln3020ReqDto.setXhdkqyzh("");
            // 签约账号子序号
            ln3020ReqDto.setXhdkzhxh("");
            // 允许提前还款
            ln3020ReqDto.setYunxtqhk("");
            // 提前还款锁定期
            ln3020ReqDto.setTiqhksdq("");

            // 多还款账户标志 1--是 0--否
            // 022097--市民贷 022099--惠民贷 022100--大家e贷 && 替补还款账号不为空
            if (("022097".equals(accEntrustLoan.getPrdId()) || "022099".equals(accEntrustLoan.getPrdId()) || "022100".equals(accEntrustLoan.getPrdId())) && StringUtil.isNotEmpty(pvpAuthorize.getFldvalue04())) {
                // 多还款账户标志
                ln3020ReqDto.setDhkzhhbz("1");

                // 贷款多还款账户列表信息
                List<Lstdkhkzh> lstdkhkzhList = new ArrayList<>();
                // 贷款多还款账户信息
                Lstdkhkzh lstdkhkzh = new Lstdkhkzh();
                // 账户信息
                String cusAccNo = pvpAuthorize.getFldvalue04();
                // 还款基数种类 1--本金 2--利息 3--本息
                lstdkhkzh.setHkjshzhl("3");
                // 还款账户规则 1--专用账户 2--普通账户
                lstdkhkzh.setHkzhhgze("2");

                // 还款账户种类 1--本人账户 2--他人账户
                lstdkhkzh.setHkzhhzhl("1");
                // 还款比例
                lstdkhkzh.setHuankbli(new BigDecimal("100"));
                // 生效日期
                lstdkhkzh.setShengxrq(accEntrustLoan.getLoanStartDate().replace("-", ""));
                // 到期日期
                lstdkhkzh.setDaoqriqi("29991231");
                // 优先级
                lstdkhkzh.setYouxianj("2");

                // 查询账号子序号信息
                Ib1253RespDto ib1253RespDto = this.sendIb1253(cusAccNo);

                // 还款账号
                lstdkhkzh.setHuankzhh(cusAccNo);
                // 还款账户子序号
                lstdkhkzh.setHkzhhzxh(ib1253RespDto.getZhaoxhao());
                // 还款账户名称
                lstdkhkzh.setHkzhhmch(ib1253RespDto.getZhhuzwmc());

                lstdkhkzhList.add(lstdkhkzh);
                ln3020ReqDto.setLstdkhkzh(lstdkhkzhList);

            } else {
                // 多还款账户标志
                ln3020ReqDto.setDhkzhhbz("");

                // 贷款多还款账户列表信息
                List<Lstdkhkzh> lstdkhkzhList = new ArrayList<>();
                // 贷款多还款账户信息
                Lstdkhkzh lstdkhkzh = new Lstdkhkzh();

                // 还款基数种类 1--本金 2--利息 3--本息
                lstdkhkzh.setHkjshzhl("");
                // 还款账户规则 1--专用账户 2--普通账户
                lstdkhkzh.setHkzhhgze("");
                // 还款账户种类 1--本人账户 2--他人账户
                lstdkhkzh.setHkzhhzhl("");
                // 还款比例
                lstdkhkzh.setHuankbli(null);
                // 生效日期
                lstdkhkzh.setShengxrq("");
                // 到期日期
                lstdkhkzh.setDaoqriqi("");
                // 优先级
                lstdkhkzh.setYouxianj("");
                // 还款账号
                lstdkhkzh.setHuankzhh("");
                // 还款账户子序号
                lstdkhkzh.setHkzhhzxh("");
                // 还款账户名称
                lstdkhkzh.setHkzhhmch("");

                lstdkhkzhList.add(lstdkhkzh);
                ln3020ReqDto.setLstdkhkzh(lstdkhkzhList);
            }

            // 多笔贷款扣款顺序
            ln3020ReqDto.setDbdkkksx("");

            /*************贷款定制期供计划开始**************/
            List<Lstdzqgjh> lstdzqgjhList = new ArrayList<>();
            // 7--定制还款
            if ("7".equals(repayMode)) {
                // 与业务商定还本计划暂时不发核心，后续如需要，放开即可
//                // 期供生成方式:1--还款规则、2--还款计划书
//                ln3020ReqDto.setQigscfsh("2");
//                // 查询定制还款计划
//                QueryModel queryModel = new QueryModel();
//                queryModel.addCondition("serno",pvpAuthorize.getPvpSerno());
//                queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
//                queryModel.setSort("repayDate asc");
//                List<RepayCapPlan> list = repayCapPlanService.selectAll(queryModel);
//                for (int i = 0; i < list.size(); i++) {
//                    RepayCapPlan repayCapPlan = list.get(i);
//                    // 起始日期
//                    String qishriqi = "";
//                    // 到期日期
//                    String daoqriqi = "";
//                    if(i == 0){
//                        // 第一期
//                        qishriqi = accLoan.getLoanStartDate();
//                        daoqriqi = repayCapPlan.getRepayDate();
//                    }else{
//                        /*
//                         * 第二期到第n期
//                         * 1.先取上一期的还款日期转化为date类型，格式转化为"yyyy-MM-dd"
//                         * 2.把处理后的日期加一天(核心的规则不需要加一天且最后一期的到期日期等于贷款到期日，先按照核心的规则)
//                         * 3.再把日期转换为字符串
//                         */
//                        RepayCapPlan repayCapPlanOther = list.get(i-1);
////                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
////                        Date qishriqiDate = sdf.parse(repayCapPlanOther.getRepayDate());
////
////                        Date fianlQishriqi = DateUtils.addDay(qishriqiDate,1);
////
////                        qishriqi = sdf.format(fianlQishriqi);
//                        if(i == list.size()-1){
//                            qishriqi = repayCapPlanOther.getRepayDate();
//                            daoqriqi = accLoan.getLoanEndDate();
//                        }else{
//                            qishriqi = repayCapPlanOther.getRepayDate();
//                            daoqriqi = repayCapPlan.getRepayDate();
//                        }
//                    }
//                    Lstdzqgjh lstdzqgjh = new Lstdzqgjh();
//                    // TODO 为何没有借据号了
//                    //lstdzqgjh.setDkjiejuh(pvpAuthorize.getBillNo());// 贷款借据号
//                    // 本期期数
//                    lstdzqgjh.setBenqqish(String.valueOf(i+1));
//                    // 起始日期
//                    lstdzqgjh.setQishriqi(qishriqi.replace("-", ""));
//                    // 到期日期
//                    lstdzqgjh.setDaoqriqi(daoqriqi.replace("-", ""));
//                    // 还本金额
//                    lstdzqgjh.setHuanbjee(repayCapPlan.getRepayAmt());
//                    // 还息金额默认0
//                    lstdzqgjh.setHxijinee(new BigDecimal("0.0"));
//                    // 最晚还款日
//                    lstdzqgjh.setZwhkriqi("");
//                    lstdzqgjhList.add(lstdzqgjh);
//                }
            } else {
                // 期供生成方式:1--还款规则、2--还款计划书
                ln3020ReqDto.setQigscfsh("");
                Lstdzqgjh lstdzqgjh = new Lstdzqgjh();

                // 本期期数
                lstdzqgjh.setBenqqish("");
                // 起始日期
                lstdzqgjh.setQishriqi("");
                // 到期日期
                lstdzqgjh.setDaoqriqi("");
                // 还本金额
                lstdzqgjh.setHuanbjee(null);
                // 还息金额默认0
                lstdzqgjh.setHxijinee(new BigDecimal("0.0"));
                // 最晚还款日
                lstdzqgjh.setZwhkriqi("");
                lstdzqgjhList.add(lstdzqgjh);
            }
            ln3020ReqDto.setLstdzqgjh(lstdzqgjhList);
            /*************贷款定制期供计划结束**************/

            // 期供利息类型 1--贷款本金利息 2--本期本金利息 3--指定本金利息
            ln3020ReqDto.setQglxleix("7".equals(repayMode) ? "1" : "");

            // 允许行内同名账户还款
            ln3020ReqDto.setHntmihku("");
            // 允许行内非同名帐户还款
            ln3020ReqDto.setHnftmhku("");
            // 允许内部账户还款
            ln3020ReqDto.setNbuzhhku("");
            // 允许缩期
            ln3020ReqDto.setYunxsuoq("");
            // 缩期最大次数
            ln3020ReqDto.setSqizdcsh("");
            // 逾期不足额扣款方式
            ln3020ReqDto.setYqbzkkfs("");
            // 逾期自动追缴标志
            ln3020ReqDto.setYqzdzjbz("");
            // 允许调整还款方式
            ln3020ReqDto.setTiaozhkf("");
            // 是否有宽限期
            ln3020ReqDto.setSfyxkuxq("");
            // 宽限期天数
            ln3020ReqDto.setKuanxqts("");
            // 宽限期计息方式
            ln3020ReqDto.setKxqjixgz("");
            // 宽限期后计息规则
            ln3020ReqDto.setKxqhjxgz("");
            // 宽限期转逾期规则
            ln3020ReqDto.setKxqzyqgz("");
            // 宽限期收息规则
            ln3020ReqDto.setKxqshxgz("");
            // 宽限期节假日规则
            ln3020ReqDto.setKxqjjrgz("");
            // 宽限期最大次数
            ln3020ReqDto.setKxqzdcsh("");
            // 展期规则编号
            ln3020ReqDto.setZqgzbhao("");
            // 展期需足额扣款
            ln3020ReqDto.setZhqxzekk("");
            // 展期最长期限(月)
            ln3020ReqDto.setZhqizcqx("");
            // 自动形态转移
            ln3020ReqDto.setZidxtzhy("");
            // 利息转出规则
            ln3020ReqDto.setLixizcgz("");
            // 利息转回规则
            ln3020ReqDto.setLixizhgz("");
            // 使用贷款承诺
            ln3020ReqDto.setSydkcnuo("");
            // 承诺贷款借据号
            ln3020ReqDto.setCndkjjho("");
            // 使用额度标志
            ln3020ReqDto.setShynedbz("");
            // 额度编号
            ln3020ReqDto.setEdbiahao("");
            // 额度币种规则
            ln3020ReqDto.setEdbizhgz("");
            // 额度指定币种
            ln3020ReqDto.setEdzdbizh("");
            // 保证人担保标志
            ln3020ReqDto.setBzhrdbbz("");

            // 贷款保证人信息
            List<Lstdkzhbz> lstdkzhbzList = new ArrayList<>();
            Lstdkzhbz lstdkzhbz = new Lstdkzhbz();
            // 保证方式
            lstdkzhbz.setBaozhfsh("");
            // 保证人客户号
            lstdkzhbz.setBzrkehuh("");
            // 保证金额
            lstdkzhbz.setBaozjine(null);
            // 客户名称
            lstdkzhbz.setKehmingc("");
            // 担保账号
            lstdkzhbz.setDanbzhao("");
            // 担保账号子序号
            lstdkzhbz.setDbzhzxuh("");
            // 备注信息
            lstdkzhbz.setBeizhuuu("");
            lstdkzhbzList.add(lstdkzhbz);
            ln3020ReqDto.setLstdkzhbz(lstdkzhbzList);

            // 代理核算方式
            ln3020ReqDto.setDlhesfsh("");
            // 代理信息指定规则
            ln3020ReqDto.setDlxxzdgz("");
            // 代理信息取值规则
            ln3020ReqDto.setDlxxqzgz("");
            // 代理序号
            ln3020ReqDto.setDailixuh("");
            // 代理描述
            ln3020ReqDto.setDailimsh("");
            // 本金归还入账账号
            ln3020ReqDto.setBjghrzzh("");
            // 本金归还入账账号子序号
            ln3020ReqDto.setBjghrzxh("");
            // 利息归还入账账号
            ln3020ReqDto.setLxghrzzh("");
            // 利息归还入账账号子序号
            ln3020ReqDto.setLxghrzxh("");
            // 多委托人标志
            ln3020ReqDto.setDuowtrbz("");

            // 贷款多委托人账户
            List<Lstdkwtxx> lstdkwtxxlist = new ArrayList<>();
            Lstdkwtxx lstdkwtxx = new Lstdkwtxx();
            // 本行出资标志
            lstdkwtxx.setBhchzibz("");
            // 本行借据号
            lstdkwtxx.setBhjiejuh("");
            // 本行账号标志
            lstdkwtxx.setBhzhaobz("");
            // 资金归集标志
            lstdkwtxx.setZjingjbz("");
            // 还款资金划转方式
            lstdkwtxx.setHkzjhzfs("");
            // 账户开户行行号
            lstdkwtxx.setZhkaihhh("");
            // 账户开户行行名
            lstdkwtxx.setZhkaihhm("");
            // 委托人客户号
            lstdkwtxx.setWtrkehuh("");
            // 委托人存款账号
            lstdkwtxx.setWtrckuzh("");
            // 委托人存款账号子序号
            lstdkwtxx.setWtrckzxh("");
            // 委托存款账号
            lstdkwtxx.setWtckzhao("");
            // 委托存款账号子序号
            lstdkwtxx.setWtckzixh("");
            // 本金归还入账账号
            lstdkwtxx.setBjghrzzh("");
            // 利息归还入账账号
            lstdkwtxx.setLxghrzzh("");
            // 本金归还入账账号子序号
            lstdkwtxx.setBjghrzxh("");
            // 利息归还入账账号子序号
            lstdkwtxx.setLxghrzxh("");
            // 委托金额
            lstdkwtxx.setWeituoje(null);
            //委托人名称
            lstdkwtxx.setWtrmingc("");
            lstdkwtxxlist.add(lstdkwtxx);
            ln3020ReqDto.setLstdkwtxx(lstdkwtxxlist);

            //联名贷款标志
            ln3020ReqDto.setLmdkbzhi("");

            // 贷款账户联名
            List<Lstdkzhlm> lstdkzhlmList = new ArrayList<>();
            Lstdkzhlm lstdkzhlm = new Lstdkzhlm();
            // 客户关系类型
            lstdkzhlm.setKehugxlx("");
            // 客户号
            lstdkzhlm.setKehuhaoo("");
            // 贷款账号
            lstdkzhlm.setDkzhangh("");
            lstdkzhlmList.add(lstdkzhlm);
            ln3020ReqDto.setLstdkzhlm(lstdkzhlmList);

            // 联合贷款
            List<Lstdklhmx> lstdklhmxlist = new ArrayList<>();
            Lstdklhmx lstdklhmx = new Lstdklhmx();

            // 参与方代码
            lstdklhmx.setCanyfdma("");
            // 参与方户名
            lstdklhmx.setCanyfhum("");
            // 联合方式
            lstdklhmx.setLianhfsh("");
            // 联合贷款类型
            lstdklhmx.setLhdkleix("");
            // 参与金额
            lstdklhmx.setCanyjine(null);
            // 参与比例
            lstdklhmx.setCanybili(null);
            // 资金来源账号
            lstdklhmx.setZjlyzhao("");
            lstdklhmx.setZjlyzzxh("");//资金来源账号子序号
            lstdklhmx.setZjzrzhao("");//资金转入账号
            lstdklhmx.setZjzrzzxh("");//资金转入账号子序号
            lstdklhmx.setDailfeil(new BigDecimal("0"));//代理费率
            lstdklhmx.setCanyjjha("");//参与借据号
            lstdklhmxlist.add(lstdklhmx);
            ln3020ReqDto.setLstdklhmx(lstdklhmxlist);

            // 提前还款罚金编号
            ln3020ReqDto.setTqhkfjbh("");
            // 提前还款罚金名称
            ln3020ReqDto.setTqhkfjmc("");
            //提前还款附加罚金金额
            ln3020ReqDto.setTqhkfjfj(null);

            // 贷款收费事件
            List<Lstdksfsj> lstdksfsjlist = new LinkedList<Lstdksfsj>();
            Lstdksfsj lstdksfsj = new Lstdksfsj();

            // 收费种类
            lstdksfsj.setShoufzhl("");
            // 收费金额/比例
            lstdksfsj.setShoufjee(null);
            // 付费账号
            lstdksfsj.setFufeizhh("");
            // 收费事件
            lstdksfsj.setShoufshj("");
            // 收费代码
            lstdksfsj.setShoufdma("");
            // 收费事件名称
            lstdksfsj.setShfshjmc("");
            // 收费代码名称
            lstdksfsj.setShfdmamc("");
            // 付费账号子序号
            lstdksfsj.setFfzhhzxh("");
            // 收费入账账号
            lstdksfsj.setSfrzhzhh("");
            // 收费入账账号子序号
            lstdksfsj.setSfrzhzxh("");
            lstdksfsjlist.add(lstdksfsj);
            ln3020ReqDto.setLstdksfsj(lstdksfsjlist);

            // 收费类型
            ln3020ReqDto.setShfleixi("1");
            // 收费频率 一次性收取
            ln3020ReqDto.setSfpinlvv("3");
            // 收费周期
            ln3020ReqDto.setSfzhouqi("");
            // 收费种类 覃明贵确定传1
            ln3020ReqDto.setShoufzhl("1");
            // 摊销周期
            ln3020ReqDto.setTxzhouqi("");
            // 收费代码
            ln3020ReqDto.setShoufdma("H1100139");
            // 收费代码名称
            ln3020ReqDto.setShfdmamc("委托贷款手续费");
            // 收费金额/比例
            ln3020ReqDto.setShoufjee(pvpAuthorize.getFldvalue34() != null ? new BigDecimal(pvpAuthorize.getFldvalue34()) : BigDecimal.ZERO);
            // 付费账号
            ln3020ReqDto.setFufeizhh(pvpAuthorize.getFldvalue07());
            // 付费账号子序号
            ln3020ReqDto.setFfzhhzxh(pvpAuthorize.getFldvalue28());
            // 付费账号名称
            ln3020ReqDto.setFfzhhmch(pvpAuthorize.getFldvalue41());
            // 收费入账账号
            ln3020ReqDto.setSfrzhzhh("");
            // 收费入账账号子序号
            ln3020ReqDto.setSfrzhzxh("");
            // 末期已结清是否收费
            ln3020ReqDto.setMqjqsfbz("");
            // 费用首期是否包括当前期
            ln3020ReqDto.setFybkdqbz("");
            // 是否立即收费
            ln3020ReqDto.setSfljsfei("1");
            // 费用是否摊销
            ln3020ReqDto.setShiftanx("");


            // 起始日期
            ln3020ReqDto.setQishriqi("");
            // 终止日期
            ln3020ReqDto.setZhzhriqi("");
            // 正常提前通知
            ln3020ReqDto.setZhchtqtz("");
            // 逾期催收通知
            ln3020ReqDto.setYqcshtzh("");
            // 利率变更通知
            ln3020ReqDto.setLilvbgtz("");
            // 余额变更通知
            ln3020ReqDto.setYuebgtzh("");
            // 通知提前天数
            ln3020ReqDto.setTzhtqtsh("");
            // 通知间隔天数
            ln3020ReqDto.setTzhjgtsh("");
            // 五级分类标志
            ln3020ReqDto.setWujiflbz("");
            // 五级分类日期
            ln3020ReqDto.setWujiflrq("");
            // 贷款管理机构
            ln3020ReqDto.setDkgljgou("");
            // 管理机构类别
            ln3020ReqDto.setGljgleib("");
            // 复核机构
            ln3020ReqDto.setFuhejgou("");
            // 客户经理
            ln3020ReqDto.setKhjingli(accEntrustLoan.getManagerId());
            // 凭证批号
            ln3020ReqDto.setPingzhma("");
            // 凭证序号
            ln3020ReqDto.setPngzxhao("");
            // 是否缴纳印花税标志
            ln3020ReqDto.setSfjnyhsh("");
            // 印花税率
            ln3020ReqDto.setYinhshlv(null);
            // 印花税金额
            ln3020ReqDto.setYinhshje(null);
            // 联系人名称
            ln3020ReqDto.setLxirenmc("");
            // 联系人电话
            ln3020ReqDto.setLxirendh("");
            // 是否关联抵质押物标志
            ln3020ReqDto.setSfgldzyw("");

            //抵押关联信息
            List<Lstdkdygl> lstdkdygllist = new ArrayList<>();
            Lstdkdygl lstdkdygl = new Lstdkdygl();

            // 贷款借据号
            lstdkdygl.setDkjiejuh("");
            // 关联业务种类
            lstdkdygl.setGlyewuzl("");
            // 担保方式
            lstdkdygl.setDbaofshi("");
            // 抵质押物编号
            lstdkdygl.setDzywbhao("");
            // 担保控制种类
            lstdkdygl.setDbaokzzl("");
            // 担保比例
            lstdkdygl.setDbaobili(null);
            // 联动担保控制
            lstdkdygl.setLddbkzhi("");
            // 担保币种
            lstdkdygl.setDbaobizh("");
            // 担保金额
            lstdkdygl.setDbaojine(null);
            // 借据币种
            lstdkdygl.setJijubizh("");
            // 借据金额
            lstdkdygl.setJiejuuje(null);
            // 折算汇率
            lstdkdygl.setZhshuilv(null);
            // 关联状态
            lstdkdygl.setGuanlzht("");
            // 明细序号
            lstdkdygl.setMingxixh("");
            lstdkdygllist.add(lstdkdygl);
            ln3020ReqDto.setLstdkdygl(lstdkdygllist);

            /******************账户质押信息开始*******************/
            // 是否账户质押标志
            ln3020ReqDto.setSfglzhzy("0");

            // TODO 根据合同编号查询合同下，我行本币存单和我行外币存单送存单相关信息

            /******************账户质押信息结束*******************/


        } catch (BizException bizException) {
            bizException.printStackTrace();
            throw BizException.error(null, bizException.getErrorCode(), bizException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        log.info("放款流水号【{}】放款报文组装结束", pvpEntrustLoanApp.getPvpSerno());
        return ln3020ReqDto;
    }
    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/8/26 11:12
     * @version 1.0.0
     * @desc 冲正申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto czsq(PvpLoanApp pvpLoanApp) {
        // 1、根据放款信息查询出账信息
        PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpLoanApp.getPvpSerno());
        //[{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
        // {"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},
        // {"key":"5","value":"已记帐已撤销"},{"key":"6","value":"抹账被核心拒绝"},
        // {"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},{"key":"9","value":"已发送核心冲正成功"}]
        if (pvpAuthorize == null) {
            return new ResultDto(null).message("未查询到出账授权数据");
        }

        // 若暂未出账，不允许冲正申请 2--已出账
        if (!"2".equals(pvpAuthorize.getTradeStatus())) {
            return new ResultDto(null).message("出账状态不为已出账，拒绝冲正申请！");
        }
        pvpAuthorize.setAuthStatus("A"); // A--冲正申请

        int i = pvpAuthorizeMapper.updateByPrimaryKey(pvpAuthorize);
        if (i != 1) {
            return new ResultDto(null).message("冲正申请失败");
        }
        // 作废等待入库的归档任务
        docArchiveInfoService.invalidByBizSerno(pvpLoanApp.getPvpSerno(), pvpLoanApp.getBillNo());

        return new ResultDto(pvpAuthorize).message("申请成功");
    }

    public ResultDto getAuthStatus(String pvpSerNo) {
        PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerNo);
        if (pvpAuthorize == null) {
            return new ResultDto(null).message("未查询到相关放款数据");
        } else {
            return new ResultDto(pvpAuthorize.getAuthStatus()).message(null);
        }
    }

    /**
     * @param pvpSerNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 尚智勇
     * @date 2021/9/03 10:15
     * @version 1.0.0
     * @desc 查询交易流水号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto getTranSerno(String pvpSerNo) {
        PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerNo);
        if (pvpAuthorize == null && pvpAuthorize.getTranSerno() == null) {
            return new ResultDto(null).message("未查询到相关放款数据");
        } else {
            return new ResultDto(pvpAuthorize.getTranSerno()).message(null);
        }
    }

    /**
     * @param pvpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/9/6 14:04
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto annulPvpAuthorize(String pvpSerno) {
        log.info("************小微出账授权撤销开始，放款流水号：【{}】**********", pvpSerno);
        try {
            PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            AccLoan accloan = accLoanService.selectByAccLoanPvpSerno(pvpLoanApp.getPvpSerno());
            String authStatus = pvpAuthorize.getAuthStatus();
            if ("2".equals(authStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "已发送核心校验成功，不可撤销！");
            }
            if (!"6".equals(accloan.getAccStatus())) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销尚未出账的放款申请！");
            }
            if (!"3".equals(authStatus) && !"0".equals(authStatus)) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "只可撤销尚未出账的放款申请！");
            }
            //台账额度恢复
            pvpLoanAppService.retailRestore(pvpSerno);
            pvpLoanApp.setApproveStatus("996");//自行退出
            pvpLoanAppService.updateSelective(pvpLoanApp);
            pvpAuthorize.setAuthStatus("4");//出账已撤销
            pvpAuthorizeMapper.updateByPrimaryKeySelective(pvpAuthorize);
            accloan.setOprType("02");//台账逻辑删除
            accLoanService.updateSelective(accloan);
        } catch (Exception e) {
            log.info("************小微出账授权撤销异常，放款流水号：【{}】，异常信息：【{}】**********", pvpSerno, e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("************小微出账授权撤销结束，放款流水号：【{}】**********", pvpSerno);
        return new ResultDto();
    }

    /**
     * @param pvpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-7 16:18:18
     * @version 1.0.0
     * @desc 判断账户是否为正常
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto sendPvpCheckRetail(String pvpSerno) {
        log.info("************零售手工出账校验开始，放款流水号：【{}】**********", pvpSerno);
        try {
            PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            AccLoan accLoan = accLoanService.selectByAccLoanPvpSerno(pvpLoanApp.getPvpSerno());
            String dkzhhzht = "";
            Ln3108ReqDto ln3108ReqDto = GenericBuilder.of(Ln3108ReqDto::new)//请求DTO:贷款展期查询
                    .with(Ln3108ReqDto::setDkjiejuh, pvpLoanApp.getBillNo()) //贷款借据号
                    .with(Ln3108ReqDto::setDkzhangh, org.apache.commons.lang.StringUtils.EMPTY) //贷款账号
                    .with(Ln3108ReqDto::setJiejuuje, null)//借据金额
                    .with(Ln3108ReqDto::setHetongbh, org.apache.commons.lang.StringUtils.EMPTY)//合同编号
                    .with(Ln3108ReqDto::setKehuhaoo, org.apache.commons.lang.StringUtils.EMPTY)//客户名
                    .with(Ln3108ReqDto::setChaxfanw, org.apache.commons.lang.StringUtils.EMPTY)//查询范围标志
                    .with(Ln3108ReqDto::setYngyjigo, org.apache.commons.lang.StringUtils.EMPTY)//营业机构
                    .with(Ln3108ReqDto::setChanpdma, org.apache.commons.lang.StringUtils.EMPTY)//产品代码
                    .with(Ln3108ReqDto::setKaihriqi, org.apache.commons.lang.StringUtils.EMPTY)//开户日期
                    .with(Ln3108ReqDto::setZhzhriqi, org.apache.commons.lang.StringUtils.EMPTY)//终止日期
                    .with(Ln3108ReqDto::setQishriqi, org.apache.commons.lang.StringUtils.EMPTY)//起始日期
                    .with(Ln3108ReqDto::setDaoqriqi, org.apache.commons.lang.StringUtils.EMPTY)//到期日期
                    .with(Ln3108ReqDto::setXiaohurq, org.apache.commons.lang.StringUtils.EMPTY)//销户日期
                    .with(Ln3108ReqDto::setDjdqriqi, org.apache.commons.lang.StringUtils.EMPTY)//冻结到期日期
                    .with(Ln3108ReqDto::setZhcwjyrq, org.apache.commons.lang.StringUtils.EMPTY)//最后财务交易日
                    .with(Ln3108ReqDto::setScjyriqi, org.apache.commons.lang.StringUtils.EMPTY)//上次交易日期
                    .with(Ln3108ReqDto::setDaikxtai, org.apache.commons.lang.StringUtils.EMPTY)//贷款形态
                    .with(Ln3108ReqDto::setYjfyjzht, org.apache.commons.lang.StringUtils.EMPTY)//应计非应计状态
                    .with(Ln3108ReqDto::setDkzhhzht, org.apache.commons.lang.StringUtils.EMPTY)//贷款账户状态
                    .with(Ln3108ReqDto::setHuobdhao, org.apache.commons.lang.StringUtils.EMPTY)//货币代号
                    .with(Ln3108ReqDto::setHuankzhh, org.apache.commons.lang.StringUtils.EMPTY)//还款账号
                    .with(Ln3108ReqDto::setKaihujig, org.apache.commons.lang.StringUtils.EMPTY)//开户机构
                    .with(Ln3108ReqDto::setQishibis, null)//起始笔数
                    .with(Ln3108ReqDto::setChxunbis, null)//查询笔数
                    .with(Ln3108ReqDto::setDaikduix, org.apache.commons.lang.StringUtils.EMPTY)//贷款对象
                    .with(Ln3108ReqDto::setXhdkqyzh, org.apache.commons.lang.StringUtils.EMPTY)//循环贷款签约账号
                    .with(Ln3108ReqDto::setDkrzhzhh, org.apache.commons.lang.StringUtils.EMPTY)//贷款入账账号
                    .with(Ln3108ReqDto::setCndkjjho, org.apache.commons.lang.StringUtils.EMPTY)//承诺贷款借据号
                    .with(Ln3108ReqDto::setKehmingc, org.apache.commons.lang.StringUtils.EMPTY)//客户名称
                    .with(Ln3108ReqDto::setHuankfsh, org.apache.commons.lang.StringUtils.EMPTY)//还款方式
                    .with(Ln3108ReqDto::setDzhhkjih, org.apache.commons.lang.StringUtils.EMPTY)//定制还款计划
                    .with(Ln3108ReqDto::setCxqkbizi, org.apache.commons.lang.StringUtils.EMPTY)//查询欠款标志
                    .with(Ln3108ReqDto::setKhjingli, org.apache.commons.lang.StringUtils.EMPTY)//客户经理
                    .build();


            //发送接口
            ResultDto<Ln3108RespDto> ln3108RespDto = dscms2CoreLnClientService.ln3108(ln3108ReqDto);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3108RespDto.getCode())) {
                List<cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb> ln3108Lstdkzhzb = ln3108RespDto.getData().getLstdkzhzb();
                if (CollectionUtils.nonEmpty(ln3108Lstdkzhzb)) {
                    Optional<cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb> lstdkzhzbOptional = ln3108Lstdkzhzb.parallelStream().findFirst();
                    cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb lstdkzhzb = lstdkzhzbOptional.get();
                    dkzhhzht = lstdkzhzb.getDkzhhzht();// 贷款账户状态
                }
            }

            if ("0".equals(dkzhhzht)) {
                // 更新授权通知信息、台账表状态
                log.info("更新授权通知信息、台账表状态【{}】", pvpAuthorize.getPvpSerno());
                // 4.1、台账信息更新
                // 台账状态 1--正常
                accLoan.setAccStatus("1");
                // 贷款余额
                accLoan.setLoanBalance(accLoan.getLoanAmt());
                // 人民币余额
                accLoan.setExchangeRmbBal(accLoan.getLoanAmt());

                if (accLoanService.updateSelective(accLoan) == 0) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "台账更新失败");
                }

                // 4.2、出账授权表信息更新
                //pvpAuthorize.setTranDate(openday);
                // 2--发送核心成功
                pvpAuthorize.setTradeStatus("2");
                pvpAuthorize.setAuthStatus("2");
                // 核心交易日期
                //pvpAuthorize.setCoreTranDate(ln3020RespDto.getJiaoyirq());
                // 交易流水
                //pvpAuthorize.setCoreTranSerno(ln3020RespDto.getJiaoyils());
                this.updateSelective(pvpAuthorize);

                if (this.updateSelective(pvpAuthorize) == 0) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "出账授权更新失败");
                }

                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该笔借据已生效不可重复出账");

            }

        } catch (Exception e) {
            log.info("************小微出账授权撤销异常，放款流水号：【{}】，异常信息：【{}】**********", pvpSerno, e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("************零售手工出账校验结束，放款流水号：【{}】**********", pvpSerno);
        return new ResultDto();
    }

    /**
     * @param pvpLoanApp
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstydkjjh>
     * @author 王玉坤
     * @date 2021/9/12 21:33
     * @version 1.0.0
     * @desc 查询借新还旧信息开始
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Lstydkjjh> queryPvpJxhjRepayLoan(PvpLoanApp pvpLoanApp) throws Exception {
        List<Lstydkjjh> lstydkjjhList = new ArrayList<>();
        try {
            // 如果贷款形式为3-借新还旧，需查询借新还旧信息  （贷款形式 ：1-新增、3-借新还旧、4-无缝对接）
            String v_loan_form = pvpLoanApp.getLoanModal();
            if ("3".equals(v_loan_form) || "6".equals(v_loan_form) || "8".equals(v_loan_form)) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno", pvpLoanApp.getPvpSerno());
                List<PvpJxhjRepayLoan> pvpJxhjRepayLoanList = pvpJxhjRepayLoanService.selectByModel(queryModel);
                if (CollectionUtils.nonEmpty(pvpJxhjRepayLoanList)) {
                    for (int i = 0; i < pvpJxhjRepayLoanList.size(); i++) {
                        PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanList.get(i);
                        Lstydkjjh lstydkjjh = new Lstydkjjh();
                        // 原贷款借据号
                        lstydkjjh.setYdkjiejh(pvpJxhjRepayLoan.getBillNo());
                        // 借新还本金
                        lstydkjjh.setJxhuanbj(new BigDecimal(pvpJxhjRepayLoan.getNewLoanAmt()));
                        // 自还本金
                        lstydkjjh.setZihuanbj(new BigDecimal(pvpJxhjRepayLoan.getSelfPrcp()));
                        // 自还利息
                        lstydkjjh.setZihuanlx(new BigDecimal(pvpJxhjRepayLoan.getSelfInt()));

                        lstydkjjhList.add(lstydkjjh);
                    }
                }
            }

            if (lstydkjjhList == null || lstydkjjhList.size() == 0) {
                Lstydkjjh lstydkjjh = new Lstydkjjh();
                // 原贷款借据号
                lstydkjjh.setYdkjiejh("");
                // 借新还本金
                lstydkjjh.setJxhuanbj(null);
                // 自还本金
                lstydkjjh.setZihuanbj(null);
                // 自还利息
                lstydkjjh.setZihuanlx(null);

                lstydkjjhList.add(lstydkjjh);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("查询借新还旧信息 错误," + e.getMessage());
        }
        return lstydkjjhList;
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-9-29 23:10:35
     * @注释 零售冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto retailCz(PvpAuthorize pvpAuthorize) {

        log.info("出账通知冲正处理开始，交易流水号【{}】", pvpAuthorize.getTranSerno());

        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        //[{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
        // {"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},
        // {"key":"5","value":"已记帐已撤销"},{"key":"6","value":"抹账被核心拒绝"},
        // {"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},{"key":"9","value":"已发送核心冲正成功"}]
        if (!"2".equals(pvpAuthorize1.getAuthStatus())) {
            log.info("仅可对‘已发送核心校验成功’的出账业务进行冲正");
            return new ResultDto(null).message("仅可对‘已发送核心校验成功’的出账业务进行冲正").code("9999");
        }

        //如何判断受托支付失败 核心自行判断
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String tranDate = pvpAuthorize1.getTranDate();
        if (!tranDate.equals(openday)) {
            log.info("只能对当天的出账且受托支付失败的业务进行冲");
            throw BizException.error(null, "9999", "只能对当天的出账且受托支付失败的业务进行冲正");
        }


        try {
            // 调用核心冲正交易
            sendIb1241(pvpAuthorize1);

            // 已发送核心冲正成功
            pvpAuthorize1.setAuthStatus("9");
            int i = pvpAuthorizeMapper.updateByPrimaryKeySelective(pvpAuthorize1);
            if (i != 1) {
                log.info("冲正处理失败");
                throw BizException.error(null, "9999", "冲正处理失败");
            }

            //更新台账状态
            AccLoan accLoan = accLoanService.selectByBillNo(pvpAuthorize1.getBillNo());
            accLoan.setAccStatus("0");//已关闭
            accLoanService.updateSelective(accLoan);

            //台账额度恢复
            pvpLoanAppService.retailRestore(pvpAuthorize1.getPvpSerno());

            //更新出账申请
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpAuthorize1.getPvpSerno());
            pvpLoanApp.setApproveStatus("996");//自行退出
            pvpLoanAppService.updateSelective(pvpLoanApp);

            // 作废等待入库的归档任务
            if (Objects.equals(pvpLoanApp.getBelgLine(), "02")) {
                // 02零售
                docArchiveInfoService.invalidByBizSerno(pvpLoanApp.getIqpSerno(), pvpLoanApp.getBillNo());
            } else {
                docArchiveInfoService.invalidByBizSerno(pvpLoanApp.getPvpSerno(), pvpLoanApp.getBillNo());
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw BizException.error(null, "9999", "冲正失败：" + e.getMessage());
        }
        log.info("出账通知冲正处理结束，交易流水号【{}】", pvpAuthorize.getTranSerno());

        return new ResultDto(null).message("冲正处理成功");
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:10:35
     * @注释 出账通知手工作废
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto handInvaild(PvpAuthorize pvpAuthorize) {
        //
        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        if (!CmisCommonConstants.STD_AUTH_STATUS_0.equals(pvpAuthorize1.getAuthStatus()) && !CmisCommonConstants.STD_AUTH_STATUS_3.equals(pvpAuthorize1.getAuthStatus())) {
            log.info("仅可作废【未出帐】和【已发送核心校验失败】的出账业务");
            return new ResultDto(null).message("仅可作废【未出帐】和【已发送核心校验失败】的出账业务").code("9999");
        }
        //更新台账状态
        AccLoan accLoan = accLoanService.selectByBillNo(pvpAuthorize1.getBillNo());
        accLoan.setAccStatus(CmisCommonConstants.ACC_STATUS_7);//作废
        accLoanService.updateSelective(accLoan);

        // 更新授权状态
        pvpAuthorize1.setAuthStatus(CmisCommonConstants.STD_AUTH_STATUS_4);
        this.updateSelective(pvpAuthorize1);

        //台账额度恢复
        pvpLoanAppService.retailRestore(pvpAuthorize1.getPvpSerno());

        //仅省心快贷提款审核的出账通知数据，更新省心快贷提款审核状态为作废
        SxkdDrawCheck sxkdDrawCheck = sxkdDrawCheckMapper.selectByPvpSerno(pvpAuthorize.getPvpSerno());
        if (Objects.nonNull(sxkdDrawCheck)) {
            sxkdDrawCheck.setPvpStatus("3");
            sxkdDrawCheckMapper.updateByPrimaryKey(sxkdDrawCheck);
        }

        return new ResultDto(null).message("作废成功");
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:10:35
     * @注释 委托贷款出账通知手动作废
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto handInvaildEntrust(PvpAuthorize pvpAuthorize) {
        //
        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        if (!CmisCommonConstants.STD_AUTH_STATUS_0.equals(pvpAuthorize1.getAuthStatus()) && !CmisCommonConstants.STD_AUTH_STATUS_3.equals(pvpAuthorize1.getAuthStatus())) {
            log.info("仅可作废【未出帐】和【已发送核心校验失败】的出账业务");
            return new ResultDto(null).message("仅可作废【未出帐】和【已发送核心校验失败】的出账业务").code("9999");
        }
        //更新台账状态
        AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(pvpAuthorize1.getBillNo());
        accEntrustLoan.setAccStatus(CmisCommonConstants.ACC_STATUS_7);//作废
        accEntrustLoanService.updateSelective(accEntrustLoan);

        // 更新授权状态
        pvpAuthorize1.setAuthStatus(CmisCommonConstants.STD_AUTH_STATUS_4);
        this.updateSelective(pvpAuthorize1);
        //台账额度恢复
        pvpEntrustLoanAppService.retailRestore(pvpAuthorize1.getPvpSerno());
        return new ResultDto(null).message("作废成功");
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:10:35
     * @注释 委托贷款出账通知冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto retailCzEntrust(PvpAuthorize pvpAuthorize) {

        log.info("委托贷款冲正处理开始，交易流水号【{}】", pvpAuthorize.getTranSerno());

        PvpAuthorize pvpAuthorize1 = pvpAuthorizeMapper.selectByPrimaryKey(pvpAuthorize.getTranSerno());
        //[{"key":"0","value":"未出帐"},{"key":"1","value":"已发送核心【未知】"},{"key":"2","value":"已发送核心校验成功"},
        // {"key":"3","value":"已发送核心校验失败"},{"key":"4","value":"出账已撤销"},
        // {"key":"5","value":"已记帐已撤销"},{"key":"6","value":"抹账被核心拒绝"},
        // {"key":"7","value":"抹帐"},{"key":"8","value":"已发送核心未冲正"},{"key":"9","value":"已发送核心冲正成功"}]
        if (!"2".equals(pvpAuthorize1.getAuthStatus())) {
            log.info("仅可对【已发送核心校验成功】的出账业务进行冲正");
            return new ResultDto(null).message("仅可对【已发送核心校验成功】的出账业务进行冲正").code("9999");
        }

        //TODO 如何判断受托支付失败
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String tranDate = pvpAuthorize1.getTranDate();
        if (!tranDate.equals(openday)) {
            return new ResultDto(null).message("只能对当天的出账且受托支付失败的业务进行冲正").code("9999");
        }


        try {
            // 调用核心冲正交易
            sendIb1241(pvpAuthorize1);

            // 已发送核心冲正成功
            pvpAuthorize1.setAuthStatus("9");
            int i = pvpAuthorizeMapper.updateByPrimaryKeySelective(pvpAuthorize1);
            if (i != 1) {
                log.info("冲正处理失败");
                throw BizException.error(null, "9999", "冲正处理失败");
            }

            //更新台账状态
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(pvpAuthorize1.getBillNo());
            accEntrustLoan.setAccStatus("0");//已关闭
            accEntrustLoanService.updateSelective(accEntrustLoan);

            //台账额度恢复
            pvpEntrustLoanAppService.retailRestore(pvpAuthorize1.getPvpSerno());

            //更新出账申请
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpAuthorize1.getPvpSerno());
            pvpEntrustLoanApp.setApproveStatus("996");//自行退出
            pvpEntrustLoanAppService.updateSelective(pvpEntrustLoanApp);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw BizException.error(null, "9999", "冲正失败：" + e.getMessage());
        }
        log.info("委托贷款冲正处理结束，交易流水号【{}】", pvpAuthorize.getTranSerno());

        return new ResultDto(null).message("冲正处理成功");
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-10-12 22:17:57
     * @注释 个人二手住房、二手商用房(资金托管)-买方资金存入
     */
    public void mfzjcr(PvpAuthorize pvpAuthorize) {

        try {
            if ("022040".equals(pvpAuthorize.getPrdId()) || "022051".equals(pvpAuthorize.getPrdId())) {//个人二手住房、二手商用房(资金托管)
                //买方资金存入
                log.info("买方资金存入处理开始，交易流水号【{}】", pvpAuthorize.getTranSerno());
                PvpSehandEsInfo pvpSehandEsInfo = pvpSehandEsInfoService.selectByPrimaryKey(pvpAuthorize.getPvpSerno());
                if (pvpSehandEsInfo != null) {
                    MfzjcrReqDto mfzjcrReqDto = new MfzjcrReqDto();
                    mfzjcrReqDto.setIpaddr("");//请求方IP
                    mfzjcrReqDto.setMac("");//请求方MAC
                    mfzjcrReqDto.setQydm("");//区域代码
                    mfzjcrReqDto.setXybhsq(pvpSehandEsInfo.getTgxyNo());//协议编号
                    mfzjcrReqDto.setTranam(pvpAuthorize.getTranAmt());//存款金额
                    mfzjcrReqDto.setZjxz("0");//资金性质
                    mfzjcrReqDto.setCkrq(pvpAuthorize.getTranDate().replace("-", ""));//存款日期
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("bizSerno", pvpAuthorize.getPvpSerno());
                    List<ToppAcctSub> toppAcctSubList = toppAcctSubMapper.selectByModel(queryModel);
                    if (toppAcctSubList != null && toppAcctSubList.size() > 0) {
                        ToppAcctSub toppAcctSub = toppAcctSubList.get(0);
                        mfzjcrReqDto.setAcctba(toppAcctSub.getToppAcctNo());//资金托管账号
                        mfzjcrReqDto.setDataid(toppAcctSub.getToppName());//资金托管账户名
                    }
                    log.info("买方资金存入请求报文：", mfzjcrReqDto.toString());
                    mfzjcrService.mfzjcr(mfzjcrReqDto);
                }
                log.info("买方资金存入处理结束，交易流水号【{}】", pvpAuthorize.getTranSerno());
            }
        } catch (Exception e) {
            log.info("买方资金存入处理异常: " + e.getMessage());
        }


    }

    /**
     * @创建人 shenli
     * @创建时间 2021-10-12 22:17:57
     * @注释 连云港-资金缴存通知
     */
    public void cljctz(PvpAuthorize pvpAuthorize) {

        try {
            if (("022053".equals(pvpAuthorize.getPrdId()) || "022054".equals(pvpAuthorize.getPrdId()))) {
                log.info("连云港-资金缴存通知处理开始，交易流水号【{}】", pvpAuthorize.getTranSerno());
                PvpSehandEsInfo pvpSehandEsInfo = pvpSehandEsInfoService.selectByPrimaryKey(pvpAuthorize.getPvpSerno());
                if (pvpSehandEsInfo != null) {
                    CljctzReqDto cljctzReqDto = new CljctzReqDto();
                    cljctzReqDto.setSavvou(pvpSehandEsInfo.getTgxyNo());     //监管协议号（缴款凭证编号）
                    cljctzReqDto.setAcctno(pvpSehandEsInfo.getAcctNo());     //监管账号
                    cljctzReqDto.setAcctna(pvpSehandEsInfo.getAcctName());     //监管账号名称
                    cljctzReqDto.setTranam(pvpAuthorize.getTranAmt());     //本次缴款金额
                    cljctzReqDto.setTrantp("40");//缴款类型 "10首付款 30一次性付款 40商业贷款 50公积金贷款 60支付退款
                    cljctzReqDto.setSavsrc("TR");//缴存资金来源 TR 转账 CS现金
                    cljctzReqDto.setSernum(pvpAuthorize.getTranSerno());//流水号
                    cljctzReqDto.setSavtim(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//缴款时间
                    cljctzReqDto.setByidna(pvpSehandEsInfo.getBuyerName());//买方名称
                    cljctzReqDto.setByidno(pvpSehandEsInfo.getBuyerCertCode());//买方证件号
                    cljctzReqDto.setContno(pvpSehandEsInfo.getContNo());     //合同编号
                    log.info("连云港-资金缴存通知请求报文：", cljctzReqDto.toString());
                    cljctzService.cljctz(cljctzReqDto);
                }

                log.info("连云港-资金缴存通知处理结束，交易流水号【{}】", pvpAuthorize.getTranSerno());
            }


        } catch (Exception e) {
            log.info("连云港-资金缴存通知处理异常: " + e.getMessage());
        }

    }
}