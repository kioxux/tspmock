package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpEntrustLoanApp
 * @类描述: pvp_entrust_loan_app数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-19 16:56:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpEntrustLoanAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 放款流水号 **/
	private String serno;
	
	/** 主键 **/
	private String pkId;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 出账模式 **/
	private String pvpMode;
	
	/** 合同影像是否审核 **/
	private String isContImageAudit;
	
	/** 合同金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 贷款发放币种 **/
	private String contCurType;
	
	/** 本合同项下最高可用金额 **/
	private java.math.BigDecimal highAvlAmt;
	
	/** 出账金额 **/
	private java.math.BigDecimal pvpAmt;
	
	/** 合同起始日 **/
	private String startDate;
	
	/** 合同到期日 **/
	private String endDate;
	
	/** 贷款起始日期 **/
	private String loadStartdate;
	
	/** 贷款到期日期 **/
	private String loadEnddate;
	
	/** 贷款期限 **/
	private String loanTerm;
	
	/** 贷款期限单位 **/
	private String loanTermUnit;
	
	/** 利率调整方式 **/
	private String rateAdjMode;
	
	/** 是否分段计息 **/
	private String isSegInterest;
	
	/** LPR授信利率区间 **/
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	private java.math.BigDecimal curtLprRate;
	
	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;
	
	/** 执行利率(年) **/
	private java.math.BigDecimal execRateYear;
	
	/** 逾期利率浮动比 **/
	private java.math.BigDecimal overdueRatePefloat;
	
	/** 逾期执行利率(年利率) **/
	private java.math.BigDecimal overdueExecRate;
	
	/** 复息利率浮动比 **/
	private java.math.BigDecimal ciRatePefloat;
	
	/** 复息执行利率(年利率) **/
	private java.math.BigDecimal ciExecRate;
	
	/** 利率调整选项 **/
	private String rateAdjType;
	
	/** 下一次利率调整间隔 **/
	private String nextRateAdjInterval;
	
	/** 下一次利率调整间隔单位 **/
	private String nextRateAdjUnit;
	
	/** 第一次调整日 **/
	private String firstAdjDate;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 结息间隔周期 **/
	private String eiIntervalCycle;
	
	/** 结息间隔周期单位 **/
	private String eiIntervalUnit;
	
	/** 扣款方式 **/
	private String deductType;
	
	/** 扣款日 **/
	private String deductDay;
	
	/** 贷款发放账号 **/
	private String loanPayoutAccno;
	
	/** 贷款发放账号子序号 **/
	private String loanPayoutSubNo;
	
	/** 发放账号名称 **/
	private String payoutAcctName;
	
	/** 是否受托支付 **/
	private String isBeEntrustedPay;
	
	/** 还款计划账号 **/
	private String repayAccno;
	
	/** 贷款还款账户子序号 **/
	private String repaySubAccno;
	
	/** 还款账户名称 **/
	private String repayAcctName;
	
	/** 是否贴息 **/
	private String isSbsy;
	
	/** 贴息人存款账号 **/
	private String sbsyDepAccno;
	
	/** 贴息比例 **/
	private java.math.BigDecimal sbsyPerc;
	
	/** 贴息到期日 **/
	private String sbysEnddate;
	
	/** 是否使用授信额度 **/
	private String isUseLmtAmt;
	
	/** 授信额度编号 **/
	private String lmtAccNo;
	
	/** 批复编号 **/
	private String replyNo;
	
	/** 委托人客户编号 **/
	private String consignorIdCusNo;
	
	/** 委托人名称 **/
	private String consignorIdName;
	
	/** 委托人结算账号 **/
	private String consignorIdSettlAccno;
	
	/** 委托贷款手续费收取方式 **/
	private String csgnLoanChrgCollectType;
	
	/** 委托贷款手续费比例 **/
	private java.math.BigDecimal csgnLoanChrgRate;
	
	/** 委托贷款手续费金额 **/
	private java.math.BigDecimal csgnLoanChrgAmt;
	
	/** 账务机构编号 **/
	private String finaBrId;
	
	/** 账务机构名称 **/
	private String finaBrIdName;
	
	/** 放款机构编号 **/
	private String disbOrgNo;
	
	/** 放款机构名称 **/
	private String disbOrgName;
	
	/** 资料全否 **/
	private String fileSufFlag;
	
	/** 审批状态 **/
	private String apprStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param pvpMode
	 */
	public void setPvpMode(String pvpMode) {
		this.pvpMode = pvpMode == null ? null : pvpMode.trim();
	}
	
    /**
     * @return PvpMode
     */	
	public String getPvpMode() {
		return this.pvpMode;
	}
	
	/**
	 * @param isContImageAudit
	 */
	public void setIsContImageAudit(String isContImageAudit) {
		this.isContImageAudit = isContImageAudit == null ? null : isContImageAudit.trim();
	}
	
    /**
     * @return IsContImageAudit
     */	
	public String getIsContImageAudit() {
		return this.isContImageAudit;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contCurType
	 */
	public void setContCurType(String contCurType) {
		this.contCurType = contCurType == null ? null : contCurType.trim();
	}
	
    /**
     * @return ContCurType
     */	
	public String getContCurType() {
		return this.contCurType;
	}
	
	/**
	 * @param highAvlAmt
	 */
	public void setHighAvlAmt(java.math.BigDecimal highAvlAmt) {
		this.highAvlAmt = highAvlAmt;
	}
	
    /**
     * @return HighAvlAmt
     */	
	public java.math.BigDecimal getHighAvlAmt() {
		return this.highAvlAmt;
	}
	
	/**
	 * @param pvpAmt
	 */
	public void setPvpAmt(java.math.BigDecimal pvpAmt) {
		this.pvpAmt = pvpAmt;
	}
	
    /**
     * @return PvpAmt
     */	
	public java.math.BigDecimal getPvpAmt() {
		return this.pvpAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param loadStartdate
	 */
	public void setLoadStartdate(String loadStartdate) {
		this.loadStartdate = loadStartdate == null ? null : loadStartdate.trim();
	}
	
    /**
     * @return LoadStartdate
     */	
	public String getLoadStartdate() {
		return this.loadStartdate;
	}
	
	/**
	 * @param loadEnddate
	 */
	public void setLoadEnddate(String loadEnddate) {
		this.loadEnddate = loadEnddate == null ? null : loadEnddate.trim();
	}
	
    /**
     * @return LoadEnddate
     */	
	public String getLoadEnddate() {
		return this.loadEnddate;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm == null ? null : loanTerm.trim();
	}
	
    /**
     * @return LoanTerm
     */	
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param loanTermUnit
	 */
	public void setLoanTermUnit(String loanTermUnit) {
		this.loanTermUnit = loanTermUnit == null ? null : loanTermUnit.trim();
	}
	
    /**
     * @return LoanTermUnit
     */	
	public String getLoanTermUnit() {
		return this.loanTermUnit;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode == null ? null : rateAdjMode.trim();
	}
	
    /**
     * @return RateAdjMode
     */	
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param isSegInterest
	 */
	public void setIsSegInterest(String isSegInterest) {
		this.isSegInterest = isSegInterest == null ? null : isSegInterest.trim();
	}
	
    /**
     * @return IsSegInterest
     */	
	public String getIsSegInterest() {
		return this.isSegInterest;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval == null ? null : lprRateIntval.trim();
	}
	
    /**
     * @return LprRateIntval
     */	
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return CurtLprRate
     */	
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return RateFloatPoint
     */	
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return ExecRateYear
     */	
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}
	
    /**
     * @return OverdueRatePefloat
     */	
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}
	
	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}
	
    /**
     * @return OverdueExecRate
     */	
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}
	
	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}
	
    /**
     * @return CiRatePefloat
     */	
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}
	
	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}
	
    /**
     * @return CiExecRate
     */	
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType == null ? null : rateAdjType.trim();
	}
	
    /**
     * @return RateAdjType
     */	
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval == null ? null : nextRateAdjInterval.trim();
	}
	
    /**
     * @return NextRateAdjInterval
     */	
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit == null ? null : nextRateAdjUnit.trim();
	}
	
    /**
     * @return NextRateAdjUnit
     */	
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate == null ? null : firstAdjDate.trim();
	}
	
    /**
     * @return FirstAdjDate
     */	
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle == null ? null : eiIntervalCycle.trim();
	}
	
    /**
     * @return EiIntervalCycle
     */	
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}
	
	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit == null ? null : eiIntervalUnit.trim();
	}
	
    /**
     * @return EiIntervalUnit
     */	
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}
	
	/**
	 * @param deductType
	 */
	public void setDeductType(String deductType) {
		this.deductType = deductType == null ? null : deductType.trim();
	}
	
    /**
     * @return DeductType
     */	
	public String getDeductType() {
		return this.deductType;
	}
	
	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay == null ? null : deductDay.trim();
	}
	
    /**
     * @return DeductDay
     */	
	public String getDeductDay() {
		return this.deductDay;
	}
	
	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno == null ? null : loanPayoutAccno.trim();
	}
	
    /**
     * @return LoanPayoutAccno
     */	
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}
	
	/**
	 * @param loanPayoutSubNo
	 */
	public void setLoanPayoutSubNo(String loanPayoutSubNo) {
		this.loanPayoutSubNo = loanPayoutSubNo == null ? null : loanPayoutSubNo.trim();
	}
	
    /**
     * @return LoanPayoutSubNo
     */	
	public String getLoanPayoutSubNo() {
		return this.loanPayoutSubNo;
	}
	
	/**
	 * @param payoutAcctName
	 */
	public void setPayoutAcctName(String payoutAcctName) {
		this.payoutAcctName = payoutAcctName == null ? null : payoutAcctName.trim();
	}
	
    /**
     * @return PayoutAcctName
     */	
	public String getPayoutAcctName() {
		return this.payoutAcctName;
	}
	
	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay == null ? null : isBeEntrustedPay.trim();
	}
	
    /**
     * @return IsBeEntrustedPay
     */	
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}
	
	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno == null ? null : repayAccno.trim();
	}
	
    /**
     * @return RepayAccno
     */	
	public String getRepayAccno() {
		return this.repayAccno;
	}
	
	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno == null ? null : repaySubAccno.trim();
	}
	
    /**
     * @return RepaySubAccno
     */	
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName == null ? null : repayAcctName.trim();
	}
	
    /**
     * @return RepayAcctName
     */	
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param isSbsy
	 */
	public void setIsSbsy(String isSbsy) {
		this.isSbsy = isSbsy == null ? null : isSbsy.trim();
	}
	
    /**
     * @return IsSbsy
     */	
	public String getIsSbsy() {
		return this.isSbsy;
	}
	
	/**
	 * @param sbsyDepAccno
	 */
	public void setSbsyDepAccno(String sbsyDepAccno) {
		this.sbsyDepAccno = sbsyDepAccno == null ? null : sbsyDepAccno.trim();
	}
	
    /**
     * @return SbsyDepAccno
     */	
	public String getSbsyDepAccno() {
		return this.sbsyDepAccno;
	}
	
	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}
	
    /**
     * @return SbsyPerc
     */	
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}
	
	/**
	 * @param sbysEnddate
	 */
	public void setSbysEnddate(String sbysEnddate) {
		this.sbysEnddate = sbysEnddate == null ? null : sbysEnddate.trim();
	}
	
    /**
     * @return SbysEnddate
     */	
	public String getSbysEnddate() {
		return this.sbysEnddate;
	}
	
	/**
	 * @param isUseLmtAmt
	 */
	public void setIsUseLmtAmt(String isUseLmtAmt) {
		this.isUseLmtAmt = isUseLmtAmt == null ? null : isUseLmtAmt.trim();
	}
	
    /**
     * @return IsUseLmtAmt
     */	
	public String getIsUseLmtAmt() {
		return this.isUseLmtAmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}
	
    /**
     * @return LmtAccNo
     */	
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}
	
    /**
     * @return ReplyNo
     */	
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param consignorIdCusNo
	 */
	public void setConsignorIdCusNo(String consignorIdCusNo) {
		this.consignorIdCusNo = consignorIdCusNo == null ? null : consignorIdCusNo.trim();
	}
	
    /**
     * @return ConsignorIdCusNo
     */	
	public String getConsignorIdCusNo() {
		return this.consignorIdCusNo;
	}
	
	/**
	 * @param consignorIdName
	 */
	public void setConsignorIdName(String consignorIdName) {
		this.consignorIdName = consignorIdName == null ? null : consignorIdName.trim();
	}
	
    /**
     * @return ConsignorIdName
     */	
	public String getConsignorIdName() {
		return this.consignorIdName;
	}
	
	/**
	 * @param consignorIdSettlAccno
	 */
	public void setConsignorIdSettlAccno(String consignorIdSettlAccno) {
		this.consignorIdSettlAccno = consignorIdSettlAccno == null ? null : consignorIdSettlAccno.trim();
	}
	
    /**
     * @return ConsignorIdSettlAccno
     */	
	public String getConsignorIdSettlAccno() {
		return this.consignorIdSettlAccno;
	}
	
	/**
	 * @param csgnLoanChrgCollectType
	 */
	public void setCsgnLoanChrgCollectType(String csgnLoanChrgCollectType) {
		this.csgnLoanChrgCollectType = csgnLoanChrgCollectType == null ? null : csgnLoanChrgCollectType.trim();
	}
	
    /**
     * @return CsgnLoanChrgCollectType
     */	
	public String getCsgnLoanChrgCollectType() {
		return this.csgnLoanChrgCollectType;
	}
	
	/**
	 * @param csgnLoanChrgRate
	 */
	public void setCsgnLoanChrgRate(java.math.BigDecimal csgnLoanChrgRate) {
		this.csgnLoanChrgRate = csgnLoanChrgRate;
	}
	
    /**
     * @return CsgnLoanChrgRate
     */	
	public java.math.BigDecimal getCsgnLoanChrgRate() {
		return this.csgnLoanChrgRate;
	}
	
	/**
	 * @param csgnLoanChrgAmt
	 */
	public void setCsgnLoanChrgAmt(java.math.BigDecimal csgnLoanChrgAmt) {
		this.csgnLoanChrgAmt = csgnLoanChrgAmt;
	}
	
    /**
     * @return CsgnLoanChrgAmt
     */	
	public java.math.BigDecimal getCsgnLoanChrgAmt() {
		return this.csgnLoanChrgAmt;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName == null ? null : finaBrIdName.trim();
	}
	
    /**
     * @return FinaBrIdName
     */	
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo == null ? null : disbOrgNo.trim();
	}
	
    /**
     * @return DisbOrgNo
     */	
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}
	
	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName == null ? null : disbOrgName.trim();
	}
	
    /**
     * @return DisbOrgName
     */	
	public String getDisbOrgName() {
		return this.disbOrgName;
	}
	
	/**
	 * @param fileSufFlag
	 */
	public void setFileSufFlag(String fileSufFlag) {
		this.fileSufFlag = fileSufFlag == null ? null : fileSufFlag.trim();
	}
	
    /**
     * @return FileSufFlag
     */	
	public String getFileSufFlag() {
		return this.fileSufFlag;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}