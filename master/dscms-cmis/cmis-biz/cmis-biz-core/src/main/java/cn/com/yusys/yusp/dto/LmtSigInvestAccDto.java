package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestAcc
 * @类描述: lmt_sig_invest_acc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 15:26:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestAccDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 台账编号 **/
	private String accNo;
	
	/** 批复流水号 **/
	private String replySerno;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 资产编号 **/
	private String assetNo;
	
	/** 底层资产类型 **/
	private String basicAssetType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户大类 **/
	private String cusCatalog;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 授信品种编号 **/
	private String lmtBizType;
	
	/** 授信品种名称 **/
	private String lmtBizTypeName;
	
	/** 授信产品 **/
	private String lmtPrd;
	
	/** 是否大额授信 **/
	private String isLargeLmt;
	
	/** 是否需报备董事长 **/
	private String isReportChairman;
	
	/** 大额主责任人 **/
	private String largeLmtMainManager;
	
	/** 底层资产标准值 **/
	private String basicAssetNormal;
	
	/** 资管计划申请业务类型 **/
	private String assetPlanAppBusiType;
	
	/** 资管计划产品类型 **/
	private String assetManaPrdType;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 币种 **/
	private String curType;
	
	/** 利率 **/
	private java.math.BigDecimal rate;
	
	/** 项目投资期限(月) **/
	private Integer proInvestTerm;
	
	/** 最高授信投资剩余期限（月） **/
	private Integer highLmtInvestSurplusTerm;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 授信期限 **/
	private Integer lmtTerm;
	
	/** 授信金额 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 自营金额 **/
	private java.math.BigDecimal sobsAmt;
	
	/** 资管金额 **/
	private java.math.BigDecimal assetManaAmt;
	
	/** 拟/实际发行规模（原币） **/
	private java.math.BigDecimal intendActualIssuedScale;
	
	/** 拟/实际发行时间 **/
	private String intendActualIssuedTime;
	
	/** 发行币种 **/
	private String issuedCurType;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 债项评级结果 **/
	private String debtEvalResult;
	
	/** 债项评级日期 **/
	private String debtEvalDate;
	
	/** 评级机构名称 **/
	private String debtEvalOrgName;
	
	/** 终审机构 **/
	private String finalApprBrId;
	
	/** 审批结论 **/
	private String apprResult;
	
	/** 审批模式 **/
	private String apprMode;
	
	/** 台账状态 **/
	private String accStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 投资类型(STD_ZB_NORM_INVEST_TYPE) **/
	private String investType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo == null ? null : accNo.trim();
	}
	
    /**
     * @return AccNo
     */	
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno == null ? null : replySerno.trim();
	}
	
    /**
     * @return ReplySerno
     */	
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo == null ? null : assetNo.trim();
	}
	
    /**
     * @return AssetNo
     */	
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType == null ? null : basicAssetType.trim();
	}
	
    /**
     * @return BasicAssetType
     */	
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog == null ? null : cusCatalog.trim();
	}
	
    /**
     * @return CusCatalog
     */	
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType == null ? null : lmtBizType.trim();
	}
	
    /**
     * @return LmtBizType
     */	
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName == null ? null : lmtBizTypeName.trim();
	}
	
    /**
     * @return LmtBizTypeName
     */	
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param lmtPrd
	 */
	public void setLmtPrd(String lmtPrd) {
		this.lmtPrd = lmtPrd == null ? null : lmtPrd.trim();
	}
	
    /**
     * @return LmtPrd
     */	
	public String getLmtPrd() {
		return this.lmtPrd;
	}
	
	/**
	 * @param isLargeLmt
	 */
	public void setIsLargeLmt(String isLargeLmt) {
		this.isLargeLmt = isLargeLmt == null ? null : isLargeLmt.trim();
	}
	
    /**
     * @return IsLargeLmt
     */	
	public String getIsLargeLmt() {
		return this.isLargeLmt;
	}
	
	/**
	 * @param isReportChairman
	 */
	public void setIsReportChairman(String isReportChairman) {
		this.isReportChairman = isReportChairman == null ? null : isReportChairman.trim();
	}
	
    /**
     * @return IsReportChairman
     */	
	public String getIsReportChairman() {
		return this.isReportChairman;
	}
	
	/**
	 * @param largeLmtMainManager
	 */
	public void setLargeLmtMainManager(String largeLmtMainManager) {
		this.largeLmtMainManager = largeLmtMainManager == null ? null : largeLmtMainManager.trim();
	}
	
    /**
     * @return LargeLmtMainManager
     */	
	public String getLargeLmtMainManager() {
		return this.largeLmtMainManager;
	}
	
	/**
	 * @param basicAssetNormal
	 */
	public void setBasicAssetNormal(String basicAssetNormal) {
		this.basicAssetNormal = basicAssetNormal == null ? null : basicAssetNormal.trim();
	}
	
    /**
     * @return BasicAssetNormal
     */	
	public String getBasicAssetNormal() {
		return this.basicAssetNormal;
	}
	
	/**
	 * @param assetPlanAppBusiType
	 */
	public void setAssetPlanAppBusiType(String assetPlanAppBusiType) {
		this.assetPlanAppBusiType = assetPlanAppBusiType == null ? null : assetPlanAppBusiType.trim();
	}
	
    /**
     * @return AssetPlanAppBusiType
     */	
	public String getAssetPlanAppBusiType() {
		return this.assetPlanAppBusiType;
	}
	
	/**
	 * @param assetManaPrdType
	 */
	public void setAssetManaPrdType(String assetManaPrdType) {
		this.assetManaPrdType = assetManaPrdType == null ? null : assetManaPrdType.trim();
	}
	
    /**
     * @return AssetManaPrdType
     */	
	public String getAssetManaPrdType() {
		return this.assetManaPrdType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return Rate
     */	
	public java.math.BigDecimal getRate() {
		return this.rate;
	}
	
	/**
	 * @param proInvestTerm
	 */
	public void setProInvestTerm(Integer proInvestTerm) {
		this.proInvestTerm = proInvestTerm;
	}
	
    /**
     * @return ProInvestTerm
     */	
	public Integer getProInvestTerm() {
		return this.proInvestTerm;
	}
	
	/**
	 * @param highLmtInvestSurplusTerm
	 */
	public void setHighLmtInvestSurplusTerm(Integer highLmtInvestSurplusTerm) {
		this.highLmtInvestSurplusTerm = highLmtInvestSurplusTerm;
	}
	
    /**
     * @return HighLmtInvestSurplusTerm
     */	
	public Integer getHighLmtInvestSurplusTerm() {
		return this.highLmtInvestSurplusTerm;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param sobsAmt
	 */
	public void setSobsAmt(java.math.BigDecimal sobsAmt) {
		this.sobsAmt = sobsAmt;
	}
	
    /**
     * @return SobsAmt
     */	
	public java.math.BigDecimal getSobsAmt() {
		return this.sobsAmt;
	}
	
	/**
	 * @param assetManaAmt
	 */
	public void setAssetManaAmt(java.math.BigDecimal assetManaAmt) {
		this.assetManaAmt = assetManaAmt;
	}
	
    /**
     * @return AssetManaAmt
     */	
	public java.math.BigDecimal getAssetManaAmt() {
		return this.assetManaAmt;
	}
	
	/**
	 * @param intendActualIssuedScale
	 */
	public void setIntendActualIssuedScale(java.math.BigDecimal intendActualIssuedScale) {
		this.intendActualIssuedScale = intendActualIssuedScale;
	}
	
    /**
     * @return IntendActualIssuedScale
     */	
	public java.math.BigDecimal getIntendActualIssuedScale() {
		return this.intendActualIssuedScale;
	}
	
	/**
	 * @param intendActualIssuedTime
	 */
	public void setIntendActualIssuedTime(String intendActualIssuedTime) {
		this.intendActualIssuedTime = intendActualIssuedTime == null ? null : intendActualIssuedTime.trim();
	}
	
    /**
     * @return IntendActualIssuedTime
     */	
	public String getIntendActualIssuedTime() {
		return this.intendActualIssuedTime;
	}
	
	/**
	 * @param issuedCurType
	 */
	public void setIssuedCurType(String issuedCurType) {
		this.issuedCurType = issuedCurType == null ? null : issuedCurType.trim();
	}
	
    /**
     * @return IssuedCurType
     */	
	public String getIssuedCurType() {
		return this.issuedCurType;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param debtEvalResult
	 */
	public void setDebtEvalResult(String debtEvalResult) {
		this.debtEvalResult = debtEvalResult == null ? null : debtEvalResult.trim();
	}
	
    /**
     * @return DebtEvalResult
     */	
	public String getDebtEvalResult() {
		return this.debtEvalResult;
	}
	
	/**
	 * @param debtEvalDate
	 */
	public void setDebtEvalDate(String debtEvalDate) {
		this.debtEvalDate = debtEvalDate == null ? null : debtEvalDate.trim();
	}
	
    /**
     * @return DebtEvalDate
     */	
	public String getDebtEvalDate() {
		return this.debtEvalDate;
	}
	
	/**
	 * @param debtEvalOrgName
	 */
	public void setDebtEvalOrgName(String debtEvalOrgName) {
		this.debtEvalOrgName = debtEvalOrgName == null ? null : debtEvalOrgName.trim();
	}
	
    /**
     * @return DebtEvalOrgName
     */	
	public String getDebtEvalOrgName() {
		return this.debtEvalOrgName;
	}
	
	/**
	 * @param finalApprBrId
	 */
	public void setFinalApprBrId(String finalApprBrId) {
		this.finalApprBrId = finalApprBrId == null ? null : finalApprBrId.trim();
	}
	
    /**
     * @return FinalApprBrId
     */	
	public String getFinalApprBrId() {
		return this.finalApprBrId;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult == null ? null : apprResult.trim();
	}
	
    /**
     * @return ApprResult
     */	
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode == null ? null : apprMode.trim();
	}
	
    /**
     * @return ApprMode
     */	
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param investType
	 */
	public void setInvestType(String investType) {
		this.investType = investType == null ? null : investType.trim();
	}
	
    /**
     * @return InvestType
     */	
	public String getInvestType() {
		return this.investType;
	}


}