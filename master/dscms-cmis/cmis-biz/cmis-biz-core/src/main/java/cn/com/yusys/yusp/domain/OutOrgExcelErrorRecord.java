package cn.com.yusys.yusp.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 用于记录外部评估机构Excel导入时发生错误的数量、位置和原因
 */
@Component
public class OutOrgExcelErrorRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Excel导入时错误的数量
     */
    private int num = 0;

    /**
     * Excel导入时错误的行
     */
    private int line = 0;

    /**
     * Excel导入错误的原因
     */
    private String reason = "";

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
