/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BailAccInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.req.Fb1166ReqDto;
import cn.com.yusys.yusp.service.VisaXdRiskService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: VisaXdRiskResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:50:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/visaxdrisk")
public class VisaXdRiskResource {
    @Autowired
    private VisaXdRiskService visaXdRiskService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<VisaXdRisk>> query() {
        QueryModel queryModel = new QueryModel();
        List<VisaXdRisk> list = visaXdRiskService.selectAll(queryModel);
        return new ResultDto<List<VisaXdRisk>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<VisaXdRisk>> index(QueryModel queryModel) {
        List<VisaXdRisk> list = visaXdRiskService.selectByModel(queryModel);
        return new ResultDto<List<VisaXdRisk>>(list);
    }

    /**
     * @函数名称:queryVisaXdRiskList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建者：zhangliang15
     */
    @ApiOperation(value = "房抵e点贷面签信息列表查询")
    @PostMapping("/queryVisaXdRiskList")
    protected ResultDto<List<VisaXdRisk>> queryVisaXdRiskList(@RequestBody QueryModel queryModel) {
        List<VisaXdRisk> list = visaXdRiskService.selectByModel(queryModel);
        return new ResultDto<List<VisaXdRisk>>(list);
    }

    /**
     * @函数名称:querySfVisaList
     * @函数描述:房抵e点贷尽调结果新增pop获取面签信息列表选择页面
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建者：zhangliang15
     */
    @ApiOperation(value = "房抵e点贷尽调结果面签信息列表选择页面")
    @PostMapping("/querySfVisaList")
    protected ResultDto<List<VisaXdRisk>> querySfVisaList(@RequestBody QueryModel queryModel) {
        List<VisaXdRisk> list = visaXdRiskService.querySfVisaList(queryModel);
        return new ResultDto<List<VisaXdRisk>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        VisaXdRisk visaXdRisk = visaXdRiskService.selectBySerno((String) params.get("serno"));
        if (visaXdRisk != null) {
            resultDto.setCode(200);
            resultDto.setData(visaXdRisk);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<VisaXdRisk> show(@PathVariable("serno") String serno) {
        VisaXdRisk visaXdRisk = visaXdRiskService.selectByPrimaryKey(serno);
        return new ResultDto<VisaXdRisk>(visaXdRisk);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<VisaXdRisk> create(@RequestBody VisaXdRisk visaXdRisk) throws URISyntaxException {
        visaXdRiskService.insert(visaXdRisk);
        return new ResultDto<VisaXdRisk>(visaXdRisk);
    }

    /**
     * @函数名称:updateVisaXdRise
     * @函数描述:面签信息修改将信息同步发送风控 调用fb1166
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("面签信息修改将信息同步发送风控")
    @PostMapping("/updateVisaXdRise")
    protected ResultDto<String> updateVisaXdRise(@RequestBody VisaXdRisk visaXdRisk) {
        return  visaXdRiskService.updateVisaXdRise(visaXdRisk);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody VisaXdRisk visaXdRisk) throws URISyntaxException {
        int result = visaXdRiskService.update(visaXdRisk);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = visaXdRiskService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = visaXdRiskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
