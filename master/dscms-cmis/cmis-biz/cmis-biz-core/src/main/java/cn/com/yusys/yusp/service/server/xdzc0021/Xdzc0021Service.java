package cn.com.yusys.yusp.service.server.xdzc0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.dto.server.xdzc0021.req.Xdzc0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.resp.Xdzc0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AccAccpService;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:融资汇总
 *
 * @Author xs
 * @Date 2021/21/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0021Service {

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AccAccpService accAccpService;

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0021Service.class);

    /**
     * 交易码：xdzc0021
     * 交易描述:
     * 资产汇总查询
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0021DataRespDto xdzc0021Service(Xdzc0021DataReqDto xdzc0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value);
        Xdzc0021DataRespDto xdzc0021DataRespDto = new Xdzc0021DataRespDto();
        String cusId = xdzc0021DataReqDto.getCusId();//客户号
        try {
            // 资产池总额
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            if(ctrAsplDetails!=null){
                String contNo = ctrAsplDetails.getContNo();
                // 资产池保证金账户余额
                BigDecimal assetPoolBailAmt = asplAssetsListService.getAssetPoolBailAmt(ctrAsplDetails);
                // 资产池融资额度
                BigDecimal assetPoolFinAmt = asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails,assetPoolBailAmt);
                // 可用池融资额度
                BigDecimal assetPoolAvaAmt = asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails,assetPoolFinAmt,assetPoolBailAmt);

                // 获取该用户下所有的资产池协议编号
                List<String> contNoList = ctrAsplDetailsService.selectContNoListByCusId(cusId);
                // ∑低风险池下借据总额
                BigDecimal lowRiskAmt = BigDecimal.ZERO;
                // 超短贷用信总金额
                BigDecimal loanSumAmt = BigDecimal.ZERO;
                // 电票用信总金额
                BigDecimal drftTotalSumAmt = BigDecimal.ZERO;
                // 省心e付用信金额
                BigDecimal eloanSumAmt = BigDecimal.ZERO;
                // 省心e票用信金额
                BigDecimal edrftTotalSumAmt = BigDecimal.ZERO;
                for(String contNoNow : contNoList){
                    /**
                     * 累加所有的贷款台账的贷款余额 （只计算未结清的贷款）
                     * * 贷款台账的 贷款余额 1，2，3，4，6
                     */
                    BigDecimal lowRiskLoanAmtn = Optional.ofNullable(accLoanService.selectUnClearByContNo(contNoNow)).orElse(BigDecimal.ZERO);
                    logger.info("资产池协议："+contNo+", 普通贷款总额："+ Objects.toString(lowRiskLoanAmtn));
                    loanSumAmt = loanSumAmt.add(lowRiskLoanAmtn);
                    /**
                     * 银承台账票据总额（未结清） 0，1，4，5
                     * 银承台账票据明细（根据 银承台账借据编号 查询分项）
                     */
                    BigDecimal lowRiskBillAmtn = Optional.ofNullable(accAccpService.selectSumAmtByContNo(contNoNow)).orElse(BigDecimal.ZERO);
                    logger.info("资产池协议："+contNo+", 银承台账银票总额："+Objects.toString(lowRiskBillAmtn));
                    drftTotalSumAmt = drftTotalSumAmt.add(lowRiskBillAmtn);

                    lowRiskAmt = lowRiskAmt.add(lowRiskLoanAmtn).add(lowRiskBillAmtn);
                }

                xdzc0021DataRespDto.setAssetSumAmt(assetPoolFinAmt);// 资产池总额
                xdzc0021DataRespDto.setSupshAgrAmt(loanSumAmt);// 超短贷用信金额
                xdzc0021DataRespDto.setEBillAmt(drftTotalSumAmt);// 电票用信金额
                xdzc0021DataRespDto.setSxepyxje(edrftTotalSumAmt);// 省心e票用信金额
                xdzc0021DataRespDto.setSxefyxje(eloanSumAmt);// 省心e付用信金额
                xdzc0021DataRespDto.setNowAmt(assetPoolAvaAmt);// 剩余贷款总余额
                xdzc0021DataRespDto.setSupshAgrPer(loanSumAmt.divide(assetPoolFinAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 超短贷用信金额百分比
                xdzc0021DataRespDto.setEBillPer(drftTotalSumAmt.divide(assetPoolFinAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 电票用信金额百分比
                xdzc0021DataRespDto.setSxepyxjePer(edrftTotalSumAmt.divide(assetPoolFinAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 省心e票用信金额百分比
                xdzc0021DataRespDto.setSxefyxjePer(eloanSumAmt.divide(assetPoolFinAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 省心e付用信金额百分比
                xdzc0021DataRespDto.setNowAmtPer(assetPoolAvaAmt.divide(assetPoolFinAmt,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")));// 可用融资额度百分比
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value);
        return xdzc0021DataRespDto;
    }
}