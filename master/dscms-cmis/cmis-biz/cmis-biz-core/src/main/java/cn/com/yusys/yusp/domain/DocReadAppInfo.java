/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadAppInfo
 * @类描述: doc_read_app_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 17:00:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_read_app_info")
public class DocReadAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调阅流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DRAI_SERNO")
	private String draiSerno;
	
	/** 实际调阅人 **/
	@Column(name = "READ_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="readName")
	private String readId;
	
	/** 实际调阅机构 **/
	@Column(name = "READ_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="readOrgName")
	private String readOrg;
	
	/** 调阅形式 **/
	@Column(name = "READ_MODE", unique = false, nullable = true, length = 5)
	private String readMode;
	/** 调阅类型 **/
	@Column(name = "READ_TYPE", unique = false, nullable = true, length = 5)
	private String readType;

	/** 归还日期 **/
	@Column(name = "BACK_DATE", unique = false, nullable = true, length = 10)
	private String backDate;
	
	/** 调阅申请人 **/
	@Column(name = "READ_RQSTR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="readRqstrName")
	private String readRqstrId;
	
	/** 调阅申请机构 **/
	@Column(name = "READ_RQSTR_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="readRqstrOrgName" )
	private String readRqstrOrg;
	
	/** 调阅申请日期 **/
	@Column(name = "READ_RQSTR_DATE", unique = false, nullable = true, length = 10)
	private String readRqstrDate;

	/** 调阅用途说明 **/
	@Column(name = "READ_PURPOSE_DESC", unique = false, nullable = true, length = 2000)
	private String readPurposeDesc;
	
	/** 调阅原因 **/
	@Column(name = "READ_REASON", unique = false, nullable = true, length = 5)
	private String readReason;
	
	/** 原因描述 **/
	@Column(name = "REASON_MOME", unique = false, nullable = true, length = 2000)
	private String reasonMome;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param draiSerno
	 */
	public void setDraiSerno(String draiSerno) {
		this.draiSerno = draiSerno;
	}
	
    /**
     * @return draiSerno
     */
	public String getDraiSerno() {
		return this.draiSerno;
	}
	
	/**
	 * @param readId
	 */
	public void setReadId(String readId) {
		this.readId = readId;
	}
	
    /**
     * @return readId
     */
	public String getReadId() {
		return this.readId;
	}
	
	/**
	 * @param readOrg
	 */
	public void setReadOrg(String readOrg) {
		this.readOrg = readOrg;
	}
	
    /**
     * @return readOrg
     */
	public String getReadOrg() {
		return this.readOrg;
	}
	
	/**
	 * @param readMode
	 */
	public void setReadMode(String readMode) {
		this.readMode = readMode;
	}
	
    /**
     * @return readMode
     */
	public String getReadMode() {
		return this.readMode;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}
	
    /**
     * @return backDate
     */
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param readRqstrId
	 */
	public void setReadRqstrId(String readRqstrId) {
		this.readRqstrId = readRqstrId;
	}
	
    /**
     * @return readRqstrId
     */
	public String getReadRqstrId() {
		return this.readRqstrId;
	}
	
	/**
	 * @param readRqstrOrg
	 */
	public void setReadRqstrOrg(String readRqstrOrg) {
		this.readRqstrOrg = readRqstrOrg;
	}
	
    /**
     * @return readRqstrOrg
     */
	public String getReadRqstrOrg() {
		return this.readRqstrOrg;
	}
	
	/**
	 * @param readRqstrDate
	 */
	public void setReadRqstrDate(String readRqstrDate) {
		this.readRqstrDate = readRqstrDate;
	}
	
    /**
     * @return readRqstrDate
     */
	public String getReadRqstrDate() {
		return this.readRqstrDate;
	}
	
	/**
	 * @param readReason
	 */
	public void setReadReason(String readReason) {
		this.readReason = readReason;
	}
	
    /**
     * @return readReason
     */
	public String getReadReason() {
		return this.readReason;
	}
	
	/**
	 * @param reasonMome
	 */
	public void setReasonMome(String reasonMome) {
		this.reasonMome = reasonMome;
	}
	
    /**
     * @return reasonMome
     */
	public String getReasonMome() {
		return this.reasonMome;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getReadPurposeDesc() {
		return readPurposeDesc;
	}

	public void setReadPurposeDesc(String readPurposeDesc) {
		this.readPurposeDesc = readPurposeDesc;
	}

	public String getReadType() {
		return readType;
	}

	public void setReadType(String readType) {
		this.readType = readType;
	}
}