/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.SfResultInfoDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.req.Fb1174ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.resp.Fb1174RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.CusHouseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.EntlQyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.SfResultInfoMapper;
import cn.com.yusys.yusp.repository.mapper.VisaXdRiskMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SfResultInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:30:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class SfResultInfoService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private SfResultInfoMapper sfResultInfoMapper;
    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;
    @Autowired
    private CusHouseInfoMapper cusHouseInfoMapper;
    @Autowired
    private EntlQyInfoMapper entlQyInfoMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public SfResultInfo selectByPrimaryKey(String serno) {
        return sfResultInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<SfResultInfo> selectAll(QueryModel model) {
        List<SfResultInfo> records = (List<SfResultInfo>) sfResultInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<SfResultInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<SfResultInfo> list = sfResultInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public SfResultInfo selectBySerno(String serno) {
        return sfResultInfoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: sfResultInfolist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（尽调结果录入）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<SfResultInfo> sfResultInfolist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return sfResultInfoMapper.selectByModel(model);
    }
    /**

     * @方法名称: sfResultInfoHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（尽调结果录入）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<SfResultInfo> sfResultInfoHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return sfResultInfoMapper.selectByModel(model);
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(SfResultInfo record) {
        return sfResultInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSfResultInfo
     * @方法描述: 房抵e点贷尽调结果录入选择面签信息下一步落表校验
     * 老信贷落标前校验逻辑：1.校验选中的面签信息中面签时间，面签地址是否录入
     *                   2.校验已婚客户配偶信息是否录入
     *                   3.校验客户信息中账户登记薄信息是否完善
     * @参数与返回说明:
     * @创建人: zhangliang15
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map insertSfResultInfo(SfResultInfo sfResultInfo) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        String cusId = "";
        String signatureTime = ""; // 面签时间
        String signatureAddr = ""; // 面签地址
        try {
            // TODO 保存前校验 待补充
            serno = sfResultInfo.getSerno();
            cusId = sfResultInfo.getCusId();
            // 获取面签信息
            VisaXdRisk visaXdRisk = visaXdRiskMapper.selectByPrimaryKey(serno);
            signatureTime = visaXdRisk.getSignatureTime();
            signatureAddr = visaXdRisk.getSignatureTime();
            // 1.校验选中的面签信息中面签时间，面签地址是否录入
            if (signatureTime == null || signatureTime.isEmpty())  {
                rtnCode = EcbEnum.ECB020018.key;
                rtnMsg = EcbEnum.ECB020018.value;
                return rtnData;
            }
            if (signatureAddr == null || signatureAddr.isEmpty())  {
                rtnCode = EcbEnum.ECB020019.key;
                rtnMsg = EcbEnum.ECB020019.value;
                return rtnData;
            }
            // 获取个人客户基本信息
            CusIndivDto cusIndivDto = new CusIndivDto();
            log.info("根据客户号获取个人客户基本信息请求：" + cusId);
            cusIndivDto = cmisCusClientService.queryCusindivByCusid(cusId).getData();
            log.info("根据客户号获取个人客户基本信息返回：" + cusIndivDto);
            if (Objects.isNull(cusIndivDto)) {
                throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
            }
            //  2.校验已婚客户配偶信息是否录入 已婚 :20
            if (Objects.equals("20", cusIndivDto.getMarStatus())) {
                //根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号
                log.info("根据证件编号查询是否有配偶请求：" + sfResultInfo.getCertCode());
                ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(sfResultInfo.getCertCode());
                log.info("根据证件编号查询是否有配偶返回：" + cmisCus0013RespDtoResultDto);
                if (Objects.isNull(cmisCus0013RespDtoResultDto) || Objects.isNull(cmisCus0013RespDtoResultDto.getData())) {
                    rtnCode = EcbEnum.ECB020027.key;
                    rtnMsg = EcbEnum.ECB020027.value;
                    return rtnData;
                }
            }
            // 3.校验客户信息中账户登记薄信息是否完善 -- todo
            // 获取创建，修改日期
            sfResultInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            sfResultInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

            // 新增落表
            int count = sfResultInfoMapper.insert(sfResultInfo);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷尽点录入新增失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存房抵e点贷数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateSfResultInfo
     * @方法描述: 房抵e点贷尽调结果录入校验并保存
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public Map submitSfResultInfo(SfResultInfo record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            // 校验房产抵押物或企业经营情况异常 (01:正常 02:异常) 经营企业近2年无逾期贷款或经营企业无风险分类后三类贷款为否 (1：是 0：否),此校验是做提示，不做拦截
            if("02".equals(record.getHousePldCase())||"02".equals(record.getOperCase())||"0".equals(record.getIsCorpOverdueLoan())||"0".equals(record.getIsCorpThreeLoan())){
                rtnCode = EcbEnum.ECB020026.key;
                rtnMsg = EcbEnum.ECB020026.value;
            }
            // 校验借款人是否有配偶 或 配偶近2年无逾期经营性贷款 或 配偶近2年房贷、消费贷逾期不超过5次 或 配偶无风险分类后三类贷款 (1：是 0：否)，此校验是做提示，不做拦截
            if("1".equals(record.getIsCusSpouse())&&("0".equals(record.getIsSpouseOperLoan())||"0".equals(record.getIsSpouseOverdueTimes())||"0".equals(record.getIsSpouseThreeLoan()))){
                rtnCode = EcbEnum.ECB020026.key;
                rtnMsg = EcbEnum.ECB020026.value;
            }
            // 老信贷代码校验逻辑 UpdateSFResultInfoRecordOp -- todo
            // 请先在征信分析中添加征信报告
            // 请先在征信分析中添加有效的企业和配偶的征信报告
            // 征信报告已过期，请添加有效的企业或配偶征信报告

            // 获取营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 获取提交时间
            record.setSubmitDate(openDay);
            // 当是否予以业务准入为否时,直接更新审批状态为否决；
            if ("0".equals(record.getIsAdmit())){
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            }
            // 落表
            int count = sfResultInfoMapper.updateByPrimaryKey(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷尽调结果录入保存失败！");
            }
            // 不允许准入时调用 fb1174接口
            if("0".equals(record.getIsAdmit())){
                // 定义是否准入
                String is_zr = "";
                log.info("获取面签基本信息,流水号【{}】", record.getSerno());
                // 根据业务流水获取面签信息
                VisaXdRisk visaXdRisk = visaXdRiskMapper.selectByPrimaryKey(record.getSerno());
                log.info("获取面签基本信息：", visaXdRisk);
                // 调用风控fb1174接口,当返回成功时，且不允许准入时，删除当前面签信息表，房屋信息表，企业信息表（真删除假删除待确认）
                log.info("调用房抵e点贷尽调结果通知推送风控开始,流水号【{}】", record.getSerno());
                Fb1174ReqDto fb1174ReqDto = new Fb1174ReqDto();
                if("1".equals(record.getIsAdmit())){
                    is_zr = "Y";
                }else if("0".equals(record.getIsAdmit())){
                    is_zr = "N";
                }
                fb1174ReqDto.setIs_admint(is_zr); //是否予以准入
                fb1174ReqDto.setApp_no(visaXdRisk.getCrpSerno()); //客户信息流水号,与风控交互唯一流水
                fb1174ReqDto.setCust_core_id (record.getCusId()); //核心客户号
                fb1174ReqDto.setCust_name(record.getCusName()); //客户名称
                fb1174ReqDto.setCert_code(record.getCertCode()); //证件号
                log.info("调用房抵e点贷尽调结果通知推送风控请求：" + fb1174ReqDto);
                ResultDto<Fb1174RespDto> fb1174RespResultDto = dscms2CircpClientService.fb1174(fb1174ReqDto);
                log.info("调用房抵e点贷尽调结果通知推送风控返回：" + fb1174RespResultDto);
                String fb1174Code = Optional.ofNullable(fb1174RespResultDto.getCode()).orElse(StringUtils.EMPTY);
                String fb1174Meesage = Optional.ofNullable(fb1174RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
                log.info("调用房抵e点贷尽调结果通知推送风控返回信息：" + fb1174Meesage);
                if (Objects.equals(fb1174Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    // 删除该笔数据对应的签信息表，房屋信息表，企业信息表三表
                    if("N".equals(is_zr)){
                        // 根据业务流水删除面签信息
                        visaXdRiskMapper.deleteByPrimaryKey(record.getSerno());
                        // 根据面签表客户信息流水号删除企业，房产信息表
                        cusHouseInfoMapper.deleteByCrpSerno(visaXdRisk.getCrpSerno());
                        entlQyInfoMapper.deleteByPrimaryKey(visaXdRisk.getCrpSerno());
                    }
                } else {
                    rtnCode = fb1174Code;
                    rtnMsg = fb1174Meesage;
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("房抵e点贷尽调结果提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("serno", record.getSerno());
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(SfResultInfo record) {
        return sfResultInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(SfResultInfo record) {
        return sfResultInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(SfResultInfo record) {
        record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return sfResultInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return sfResultInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: sfResultInfodelete
     * @方法描述: 根据主键做逻辑删除（房抵e点贷）
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map sfResultInfodelete(SfResultInfo sfResultInfo) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String serno = sfResultInfo.getSerno();
            log.info(String.format("根据主键%s对房抵e点贷尽调结果信息进行逻辑删除", serno));
            // 如果是打回则自行退出，不是打回自行删除
            if(Objects.equals(CmisCommonConstants.WF_STATUS_992,sfResultInfo.getApproveStatus())){
                // 修改状态为自行退出
                sfResultInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                sfResultInfoMapper.updateByPrimaryKeySelective(sfResultInfo);
                workflowCoreClient.deleteByBizId(sfResultInfo.getSerno());
            }else{
                sfResultInfoMapper.deleteByPrimaryKey(serno);
            }
            log.info(String.format("根据主键%s更新企业信息状态", serno));
            entlQyInfoMapper.updateCrpStatusBySerno(serno);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除对房抵e点贷尽调结果信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return sfResultInfoMapper.deleteByIds(ids);
    }
}
