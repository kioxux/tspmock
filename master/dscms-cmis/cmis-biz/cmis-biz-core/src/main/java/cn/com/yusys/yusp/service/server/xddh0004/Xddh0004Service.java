package cn.com.yusys.yusp.service.server.xddh0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.IqpPrePaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/3 20:38
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xddh0004Service {

    private static final Logger logger = LoggerFactory.getLogger(Xddh0004Service.class);

    @Autowired
    private IqpPrePaymentMapper iqpPrePaymentMapper;

    /**
     * @param billNo:台账编号
     * @Description:根据billNo查询在途的还款申请数量
     * @Author: YX-WJ
     * @Date: 2021/6/3 20:57
     * @return: cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto
     **/
    public Xddh0004DataRespDto xddh0004(String billNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value);
        Xddh0004DataRespDto xddh0004DataRespDto = new Xddh0004DataRespDto();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, billNo);
            //查询在途还款申请数量
            int count = iqpPrePaymentMapper.selecOnTheWayCountByBillNo(billNo);
            if (count == 0) {
                xddh0004DataRespDto.setIsOnTheWay("N");
            } else {
                //如果存在在途还款申请，将【在途金额】和【在途合同号】返回 todo(在途金额字段 与新信贷的表结构匹配不上，现在选择的金额字段其实不对，后期确认后记得修改sql求和使用的字段)
                BigDecimal amt = iqpPrePaymentMapper.selectOnTheWayAmtByBillNo(billNo);
                String contNo = iqpPrePaymentMapper.selectOnTheWayContNoByBillNo(billNo);
                xddh0004DataRespDto.setIsOnTheWay("Y");
                xddh0004DataRespDto.setOnTheWayAmt(amt);
                xddh0004DataRespDto.setOnTheWayContNo(contNo);
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, billNo);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value);
        return xddh0004DataRespDto;
    }
}
