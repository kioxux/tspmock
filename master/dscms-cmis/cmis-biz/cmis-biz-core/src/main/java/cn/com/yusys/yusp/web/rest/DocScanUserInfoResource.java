/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocScanUserInfo;
import cn.com.yusys.yusp.service.DocScanUserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocScanUserInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docscanuserinfo")
public class DocScanUserInfoResource {
    @Autowired
    private DocScanUserInfoService docScanUserInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DocScanUserInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocScanUserInfo> list = docScanUserInfoService.selectAll(queryModel);
        return new ResultDto<List<DocScanUserInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<DocScanUserInfo>> index(@RequestBody QueryModel queryModel) {
        List<DocScanUserInfo> list = docScanUserInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocScanUserInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{dsuiSerno}")
    protected ResultDto<DocScanUserInfo> show(@PathVariable("dsuiSerno") String dsuiSerno) {
        DocScanUserInfo docScanUserInfo = docScanUserInfoService.selectByPrimaryKey(dsuiSerno);
        return new ResultDto<DocScanUserInfo>(docScanUserInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DocScanUserInfo> create(@RequestBody DocScanUserInfo docScanUserInfo) throws URISyntaxException {
        docScanUserInfoService.insert(docScanUserInfo);
        return new ResultDto<DocScanUserInfo>(docScanUserInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocScanUserInfo docScanUserInfo) throws URISyntaxException {
        int result = docScanUserInfoService.update(docScanUserInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{dsuiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("dsuiSerno") String dsuiSerno) {
        int result = docScanUserInfoService.deleteByPrimaryKey(dsuiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docScanUserInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据档案流水号获取扫描人
     *
     * @author jijian_yx
     * @date 2021/6/21 16:01
     **/
    @PostMapping("/querybydocserno")
    @ApiOperation("根据档案流水号获取扫描人")
    protected ResultDto<List<DocScanUserInfo>> queryByDocSerno(@RequestBody QueryModel queryModel) {
        List<DocScanUserInfo> list = docScanUserInfoService.queryByDocSerno(queryModel);
        return new ResultDto<List<DocScanUserInfo>>(list);
    }
}
