package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 授信复议次数校验
 */
@Service
public class RiskItem0041Service {

    // 系统参数配置服务
    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;
    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0041Service.class);

    /**
     * @方法名称: riskItem0041
     * @方法描述: 授信复议次数校验
     * @参数与返回说明:
     * @算法描述:发起授信复议次数大于设置参数，则拦截
     * @创建人: yfs
     * @创建时间: 2021-07-13 10:47:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0041(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString(); // 获取流水号
        String bizType =  queryModel.getCondition().get("bizType").toString(); // 获取流程编号类型
        log.info("发起授信复议次数大于设置参数校验开始*******************业务流水号：【{}】",serno);
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_SX004,bizType) ||
                Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_SX027,bizType) ||
                Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_SX011,bizType) ) {
            // 初始化系统复议次数（默认是3）
            int allowFuyiCount = 3;
            String fuiyiCount = getSysParameterByName("DGSX_FY_COUNT");
            if (StringUtils.nonEmpty(fuiyiCount)) {
                allowFuyiCount = Integer.parseInt(fuiyiCount);
            }
            //授信复议可发起天数
            int allowDelayDays = 0;
/*            String allowDelayDaysSys = getSysParameterByName("DGSX_FY_DAYS");
            if (StringUtils.nonEmpty(allowDelayDaysSys)) {
                allowDelayDays = Integer.parseInt(allowDelayDaysSys);
            }
            //0天 不允许发起复议
            if (allowDelayDays == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04105);
                return riskResultDto;
            }*/
            riskResultDto = checkChangeIsExceedTimes(serno,allowFuyiCount,allowDelayDays,bizType);
            if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }
        log.info("发起授信复议次数大于设置参数校验开始*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 判断复议申请是否超过
     * @param origSerno
     * @param allowFuyiCount
     * @param allowDelayDays
     * @return
     * @throws Exception
     */
    public RiskResultDto checkChangeIsExceedTimes(String origSerno,int allowFuyiCount,int allowDelayDays,String bizType) {
        RiskResultDto riskResultDto = new RiskResultDto();
/*        //6.授信复议不能超过天数计算
        Date date = DateUtils.addDay(DateUtils.getCurrDate(), allowDelayDays);
        //复议最后日期
        int deadLine = Integer.parseInt(DateUtils.formatDate(date, "yyyyMMdd"));
        //当前日期
        int curDay = Integer.parseInt(DateUtils.formatDate("yyyyMMdd"));
        if (deadLine < curDay) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04106);
            return riskResultDto;
        }*/
        //授信复议次数
        int fuyiCount = 0;
        try {
            if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_SX004,bizType) ||
                    Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_SX027,bizType)) {
                fuyiCount = getFyCount(origSerno);
            } else {
                fuyiCount = getJTFyCount(origSerno);
            }
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04102);
            return riskResultDto;
        }
        if (fuyiCount >= allowFuyiCount) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04101);
            return riskResultDto;
        }
        return riskResultDto;

    }

    /**
     * 获取系统参数
     * @param configName
     * @return
     */
    public String getSysParameterByName(String configName){
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(configName);
            return adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        } catch (Exception e) {
            log.error("获取系统参数{}失败{}",configName,e);
            return "";
        }
    }

    /**
     * 获取发起复议次数
     * add by zhangjw 20210724
     */
    public Integer getFyCount(String serno) throws Exception {
        Integer count = 0;
        Map params = new HashMap();
        params.put("serno", serno);
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApp> list = lmtAppService.selectByParams(params);
        if(CollectionUtils.isEmpty(list) || list.size() != 1) {
            throw new Exception("授信批复数据获取异常");
        }
        if(StringUtils.nonEmpty(list.get(0).getOrigiLmtReplySerno())) {
            LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(list.get(0).getOrigiLmtReplySerno());
            while(Objects.nonNull(lmtReply)) {
                if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(lmtReply.getLmtType())) {
                    count++;
                    String origiLmtReplySerno = lmtReply.getOrigiLmtReplySerno();
                    if (StringUtils.nonEmpty(origiLmtReplySerno)) {
                        lmtReply = lmtReplyService.queryLmtReplyBySerno(origiLmtReplySerno);
                    }else {
                        lmtReply = null;
                    }
                }else {
                    lmtReply = null;
                }
            }
        }
        return count;
    }

    /**
     * 获取发起复议次数
     * add by zhangjw 20210724
     */
    public Integer getJTFyCount(String serno) throws Exception {
        Integer count = 0;
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
        if(Objects.isNull(lmtGrpApp)) {
            throw new Exception("授信批复数据获取异常");
        }
        if(StringUtils.nonEmpty(lmtGrpApp.getOrigiLmtReplySerno())) {
            LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(lmtGrpApp.getOrigiLmtReplySerno());
            while(Objects.nonNull(lmtGrpReply)) {
                if (CmisBizConstants.STD_SX_LMT_TYPE_05.equals(lmtGrpReply.getLmtType())) {
                    count++;
                    String origiLmtReplySerno = lmtGrpReply.getOrigiGrpReplySerno();
                    if (StringUtils.nonEmpty(origiLmtReplySerno)) {
                        lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(origiLmtReplySerno);
                    }else {
                        lmtGrpReply = null;
                    }
                }else {
                    lmtGrpReply = null;
                }
            }
        }
        return count;
    }
}
