/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppApprMode
 * @类描述: iqp_loan_app_appr_mode数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-03 15:22:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_app_appr_mode")
public class IqpLoanAppApprMode extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;

	/** 业务优先级 **/
	@Column(name = "BIZ_PRI", unique = false, nullable = true, length = 5)
	private String bizPri;

	/** 是否绿色通道 **/
	@Column(name = "IS_GREEN_TYPE", unique = false, nullable = true, length = 5)
	private String isGreenType;

	/** 是否特例审批 STD_ZB_YES_NO **/
	@Column(name = "IS_SP_APPV", unique = false, nullable = true, length = 5)
	private String isSpAppv;

	/** 分行审批模式 **/
	@Column(name = "F_APPV_TYPE", unique = false, nullable = true, length = 5)
	private String fAppvType;

	/** 总行审批模式 **/
	@Column(name = "Z_APPV_TYPE", unique = false, nullable = true, length = 5)
	private String zAppvType;

	/** 当前节点编号 **/
	@Column(name = "CURR_NODE_ID", unique = false, nullable = true, length = 10)
	private String currNodeId;

	/** 是否需电核 STD_ZB_YES_NO **/
	@Column(name = "IS_CALL", unique = false, nullable = true, length = 5)
	private String isCall;

	/** 全流程状态 STD_ZB_ALLWF_STATUS **/
	@Column(name = "WF_STATUS", unique = false, nullable = true, length = 5)
	private String wfStatus;

	/** 房贷类型 STD_ZB_APPL_TYPE **/
	@Column(name = "APPL_TYPE", unique = false, nullable = true, length = 5)
	private String applType;

	/** 操作类型 OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;

	public String getfAppvType() {
		return fAppvType;
	}

	public void setfAppvType(String fAppvType) {
		this.fAppvType = fAppvType;
	}

	public String getzAppvType() {
		return zAppvType;
	}

	public void setzAppvType(String zAppvType) {
		this.zAppvType = zAppvType;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param bizPri
	 */
	public void setBizPri(String bizPri) {
		this.bizPri = bizPri;
	}

    /**
     * @return bizPri
     */
	public String getBizPri() {
		return this.bizPri;
	}

	/**
	 * @param isGreenType
	 */
	public void setIsGreenType(String isGreenType) {
		this.isGreenType = isGreenType;
	}

    /**
     * @return isGreenType
     */
	public String getIsGreenType() {
		return this.isGreenType;
	}

	/**
	 * @param isSpAppv
	 */
	public void setIsSpAppv(String isSpAppv) {
		this.isSpAppv = isSpAppv;
	}

    /**
     * @return isSpAppv
     */
	public String getIsSpAppv() {
		return this.isSpAppv;
	}

	/**
	 * @param fAppvType
	 */
	public void setFAppvType(String fAppvType) {
		this.fAppvType = fAppvType;
	}

    /**
     * @return fAppvType
     */
	public String getFAppvType() {
		return this.fAppvType;
	}

	/**
	 * @param zAppvType
	 */
	public void setZAppvType(String zAppvType) {
		this.zAppvType = zAppvType;
	}

    /**
     * @return zAppvType
     */
	public String getZAppvType() {
		return this.zAppvType;
	}

	/**
	 * @param currNodeId
	 */
	public void setCurrNodeId(String currNodeId) {
		this.currNodeId = currNodeId;
	}

    /**
     * @return currNodeId
     */
	public String getCurrNodeId() {
		return this.currNodeId;
	}

	/**
	 * @param isCall
	 */
	public void setIsCall(String isCall) {
		this.isCall = isCall;
	}

    /**
     * @return isCall
     */
	public String getIsCall() {
		return this.isCall;
	}

	/**
	 * @param wfStatus
	 */
	public void setWfStatus(String wfStatus) {
		this.wfStatus = wfStatus;
	}

    /**
     * @return wfStatus
     */
	public String getWfStatus() {
		return this.wfStatus;
	}

	/**
	 * @param applType
	 */
	public void setApplType(String applType) {
		this.applType = applType;
	}

    /**
     * @return applType
     */
	public String getApplType() {
		return this.applType;
	}

	public IqpLoanAppApprMode(String iqpSerno, String bizPri, String isGreenType, String isSpAppv, String fAppvType, String zAppvType, String currNodeId, String isCall, String wfStatus, String applType) {
		this.iqpSerno = iqpSerno;
		this.bizPri = bizPri;
		this.isGreenType = isGreenType;
		this.isSpAppv = isSpAppv;
		this.fAppvType = fAppvType;
		this.zAppvType = zAppvType;
		this.currNodeId = currNodeId;
		this.isCall = isCall;
		this.wfStatus = wfStatus;
		this.applType = applType;
	}

	public IqpLoanAppApprMode() {
	}
}
