/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.dto.AsplTotlIncomeDto;
import cn.com.yusys.yusp.dto.InPoolAssetListDto;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AsplTotlIncome;
import cn.com.yusys.yusp.service.AsplTotlIncomeService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplTotlIncomeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产池业务总体收益")
@RequestMapping("/api/aspltotlincome")
public class AsplTotlIncomeResource {
    @Autowired
    private AsplTotlIncomeService asplTotlIncomeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplTotlIncome>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplTotlIncome> list = asplTotlIncomeService.selectAll(queryModel);
        return new ResultDto<List<AsplTotlIncome>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplTotlIncome>> index(QueryModel queryModel) {
        List<AsplTotlIncome> list = asplTotlIncomeService.selectByModel(queryModel);
        return new ResultDto<List<AsplTotlIncome>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplTotlIncome> show(@PathVariable("pkId") String pkId) {
        AsplTotlIncome asplTotlIncome = asplTotlIncomeService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplTotlIncome>(asplTotlIncome);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplTotlIncome> create(@RequestBody AsplTotlIncome asplTotlIncome) throws URISyntaxException {
        asplTotlIncomeService.insert(asplTotlIncome);
        return new ResultDto<AsplTotlIncome>(asplTotlIncome);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplTotlIncome asplTotlIncome) throws URISyntaxException {
        int result = asplTotlIncomeService.update(asplTotlIncome);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplTotlIncomeService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplTotlIncomeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:池收益对比分析表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("池收益对比分析表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<AsplTotlIncomeDto>> toSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<AsplTotlIncomeDto> list = asplTotlIncomeService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplTotlIncomeDto>>(list);
    }

    /**
     * @函数名称:queryCtrAsplDetailsDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/queryaspltotlincomedatabyparams")
    protected ResultDto<List<AsplTotlIncome>> queryAsplTotlIncomeDataByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("contNo",(String)map.get("contNo"));
        queryData.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<AsplTotlIncome> list = asplTotlIncomeService.queryAsplTotlIncomeDataByParams(queryData);
        return new ResultDto<List<AsplTotlIncome>>(list);
    }
}
