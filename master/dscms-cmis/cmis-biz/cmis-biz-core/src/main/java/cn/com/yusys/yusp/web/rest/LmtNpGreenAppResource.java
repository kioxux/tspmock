/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtNpGreenApp;
import cn.com.yusys.yusp.service.LmtNpGreenAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtNpGreenAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-30 16:45:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtnpgreenapp")
public class LmtNpGreenAppResource {
    @Autowired
    private LmtNpGreenAppService lmtNpGreenAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtNpGreenApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtNpGreenApp> list = lmtNpGreenAppService.selectAll(queryModel);
        return new ResultDto<List<LmtNpGreenApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtNpGreenApp>> index(QueryModel queryModel) {
        List<LmtNpGreenApp> list = lmtNpGreenAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtNpGreenApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtNpGreenApp> show(@PathVariable("serno") String serno) {
        LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
        return new ResultDto<LmtNpGreenApp>(lmtNpGreenApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtNpGreenApp> create(@RequestBody LmtNpGreenApp lmtNpGreenApp) throws URISyntaxException {
        lmtNpGreenAppService.insert(lmtNpGreenApp);
        return new ResultDto<LmtNpGreenApp>(lmtNpGreenApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtNpGreenApp lmtNpGreenApp) throws URISyntaxException {
        int result = lmtNpGreenAppService.update(lmtNpGreenApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtNpGreenAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtNpGreenAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtNpGreenApp>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<LmtNpGreenApp>>(lmtNpGreenAppService.selectByModel(model));
    }

    /**
     * 获取内评低准入例外审批信息
     * @param model
     * @return
     */
    @PostMapping("/getLmtGreenApp")
    protected ResultDto<List<LmtNpGreenApp>> getLmtGreenApp(@RequestBody QueryModel model){
        return new ResultDto<List<LmtNpGreenApp>>(lmtNpGreenAppService.getLmtGreenApp(model));
    }

    /**
     * 获取内评低准入例外审批历史信息
     * @param model
     * @return
     */
    @PostMapping("/getLmtGreenAppHis")
    protected ResultDto<List<LmtNpGreenApp>> getLmtGreenAppHis(@RequestBody QueryModel model){
        return new ResultDto<List<LmtNpGreenApp>>(lmtNpGreenAppService.getLmtGreenAppHis(model));
    }

    /**
     * 新增方法
     * @param lmtNpGreenApp
     * @return
     */
    @PostMapping("/insertNpGreenApp")
    protected ResultDto<Map> insertNpGreenApp(@RequestBody LmtNpGreenApp lmtNpGreenApp){
        return new ResultDto<Map>(lmtNpGreenAppService.insertNpGreenApp(lmtNpGreenApp));
    }

    /**
     * 根据授信流水号查询单笔信息
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtNpGreenApp> selectBySerno(@RequestBody String serno){
        return new ResultDto<LmtNpGreenApp>(lmtNpGreenAppService.selectByPrimaryKey(serno));
    }

    /**
     * 保存方法
     * @param lmtNpGreenApp
     * @return
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody LmtNpGreenApp lmtNpGreenApp){
        return  new ResultDto<Integer>(lmtNpGreenAppService.save(lmtNpGreenApp));
    }

    /**
     * 逻辑删除
     * @param lmtNpGreenApp
     * @return
     */
    @PostMapping("/logicDelete")
    protected ResultDto<Integer> logicDelete(@RequestBody LmtNpGreenApp lmtNpGreenApp){
        return new ResultDto<Integer>(lmtNpGreenAppService.logicDelete(lmtNpGreenApp));
    }
}
