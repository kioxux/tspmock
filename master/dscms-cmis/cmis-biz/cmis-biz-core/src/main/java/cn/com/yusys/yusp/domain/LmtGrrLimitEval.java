/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrrLimitEval
 * @类描述: lmt_grr_limit_eval数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-31 17:50:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grr_limit_eval")
public class LmtGrrLimitEval extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 担保人客户编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 担保人客户名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String guarCusName;
	
	/** 企业规模 **/
	@Column(name = "CORP_SCALE", unique = false, nullable = true, length = 5)
	private String corpScale;
	
	/** 可担保额度 **/
	@Column(name = "EVAL_GUAR_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalGuarLmtAmt;
	
	/** 净资产值 **/
	@Column(name = "PURE_ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pureAssetValue;
	
	/** 放大倍数 **/
	@Column(name = "ENALRGE_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal enalrgeTimes;
	
	/** 已存在的对外保证担保 **/
	@Column(name = "CNTG_DEBT_GUAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cntgDebtGuar;
	
	/** 其它可确认的或有负债 **/
	@Column(name = "CNTG_DEBT_OTHER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cntgDebtOther;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale;
	}
	
    /**
     * @return corpScale
     */
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param evalGuarLmtAmt
	 */
	public void setEvalGuarLmtAmt(java.math.BigDecimal evalGuarLmtAmt) {
		this.evalGuarLmtAmt = evalGuarLmtAmt;
	}
	
    /**
     * @return evalGuarLmtAmt
     */
	public java.math.BigDecimal getEvalGuarLmtAmt() {
		return this.evalGuarLmtAmt;
	}
	
	/**
	 * @param pureAssetValue
	 */
	public void setPureAssetValue(java.math.BigDecimal pureAssetValue) {
		this.pureAssetValue = pureAssetValue;
	}
	
    /**
     * @return pureAssetValue
     */
	public java.math.BigDecimal getPureAssetValue() {
		return this.pureAssetValue;
	}
	
	/**
	 * @param enalrgeTimes
	 */
	public void setEnalrgeTimes(java.math.BigDecimal enalrgeTimes) {
		this.enalrgeTimes = enalrgeTimes;
	}
	
    /**
     * @return enalrgeTimes
     */
	public java.math.BigDecimal getEnalrgeTimes() {
		return this.enalrgeTimes;
	}
	
	/**
	 * @param cntgDebtGuar
	 */
	public void setCntgDebtGuar(java.math.BigDecimal cntgDebtGuar) {
		this.cntgDebtGuar = cntgDebtGuar;
	}
	
    /**
     * @return cntgDebtGuar
     */
	public java.math.BigDecimal getCntgDebtGuar() {
		return this.cntgDebtGuar;
	}
	
	/**
	 * @param cntgDebtOther
	 */
	public void setCntgDebtOther(java.math.BigDecimal cntgDebtOther) {
		this.cntgDebtOther = cntgDebtOther;
	}
	
    /**
     * @return cntgDebtOther
     */
	public java.math.BigDecimal getCntgDebtOther() {
		return this.cntgDebtOther;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}