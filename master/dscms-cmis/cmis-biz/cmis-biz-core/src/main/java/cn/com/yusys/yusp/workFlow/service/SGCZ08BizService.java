package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.RetailPrimeRateApp;
import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import cn.com.yusys.yusp.domain.RetailPrimeRateReplyInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.RetailPrimeRateAppService;
import cn.com.yusys.yusp.service.RetailPrimeRateApprService;
import cn.com.yusys.yusp.service.RetailPrimeRateReplyInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 零售优惠利率申请审批流程业务处理类 --村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class SGCZ08BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(SGCZ08BizService.class);

    @Autowired
    private BGYW01BizService bgyw01BizService;
    @Autowired
    private RetailPrimeRateAppService retailPrimeRateAppService;
    @Autowired
    private RetailPrimeRateApprService retailPrimeRateApprService;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private RetailPrimeRateReplyInfoService retailPrimeRateReplyInfoService;
    @Autowired
    private BGYW01BizService bGYW01BizService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //  SGF02 利率定价-二手房按揭贷款 村镇
        if(CmisFlowConstants.FLOW_TYPE_TYPE_SGF02.equals(bizType)){
            this.iRetailPrimeRateBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_SGH02.equals(bizType) ){
            bGYW01BizService.iqpRateChgAppBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void iRetailPrimeRateBizApp(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppService.selectByPrimaryKey(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("serno",iqpSerno);
                List<RetailPrimeRateAppr> retailPrimeRateApprs = retailPrimeRateApprService.selectAll(queryModel1);
                RetailPrimeRateAppr retailPrimeRateAppr = retailPrimeRateApprs.get(0);
                //TODO 将申请表和审批表中的数据保存在复批表中
                RetailPrimeRateReplyInfo retailPrimeRateReplyInfo = new RetailPrimeRateReplyInfo();
                BeanUtils.copyProperties(retailPrimeRateApp, retailPrimeRateReplyInfo);
                String replySerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
                retailPrimeRateReplyInfo.setReplySerno(replySerno);
                retailPrimeRateReplyInfo.setReplyRate(retailPrimeRateAppr.getReplyRate());
                retailPrimeRateReplyInfoService.updateSelective(retailPrimeRateReplyInfo);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回操作:" + instanceInfo);
                    retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    retailPrimeRateAppService.updateSelective(retailPrimeRateApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                retailPrimeRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                retailPrimeRateAppService.updateSelective(retailPrimeRateApp);

                log.info("-------业务否决：-- ----" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ08.equals(flowCode);
    }
}
