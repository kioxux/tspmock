package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.DoubleViewDto;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DoubleViewServices
 * @类描述: #服务类 双录信息查询
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DoubleViewServices {
    private static final Logger logger = LoggerFactory.getLogger(DoubleViewServices.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: query
     * @方法描述: 根据条件查询双录信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DoubleViewDto> querymessage(DoubleViewDto doubleViewDto) {
        logger.info("**********DoubleViewServices**双录信息查询开始,查询参数为:{}", JSON.toJSONString(doubleViewDto));
        //查询条件
        String bizStageType = doubleViewDto.getBizStageType();//业务阶段
        String managerId = doubleViewDto.getManagerId();//客户经理
        String serno = doubleViewDto.getBizSerno();
        String cusId = doubleViewDto.getCusId();
        String cusName = doubleViewDto.getCusName();
        String prdName = doubleViewDto.getPrdName();
        String pageNum = doubleViewDto.getStartPageNum();
        String pageSize = doubleViewDto.getPageSize();
        //分页查询
        int startPageNum = 1;//页码
        int pageSizeNum = 10;//每页条数
        if (StringUtil.isNotEmpty(pageSize) && StringUtil.isNotEmpty(pageNum)) {
            startPageNum = Integer.parseInt(pageNum);
            pageSizeNum = Integer.parseInt(pageSize);
        }
        //查询条件
        Map map = new HashMap<>();
        map.put("managerId", managerId);
        map.put("cusId", cusId);
        map.put("cusName", cusName);
        map.put("prdName", prdName);
        //查询
        java.util.List<DoubleViewDto> resList = new ArrayList<>();
        if (StringUtil.isEmpty(managerId) && StringUtil.isEmpty(cusName)) {//客户经理,产品名称，客户名称不能同为空
            return resList;
        }
        if ("1".equals(bizStageType)) {//授信阶段
            //查询小微授信调查主表
            PageHelper.startPage(startPageNum, pageSizeNum);
            resList = lmtSurveyReportMainInfoMapper.selectDoubleViewByModel(map);
            PageHelper.clearPage();
            //对公信息
            resList = resList.parallelStream().map(ret -> {
                DoubleViewDto temp = new DoubleViewDto();
                BeanUtils.copyProperties(ret, temp);
                //获取客户经理名称
               String managerName = findManagerName(ret.getManagerBrId());
               temp.setManagerName(managerName);
                //获取机构名称
                String managerBrName = findManagerBrName(ret.getManagerBrId());
                temp.setManagerBrName(managerBrName);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            //列表相加
        } else if ("2".equals(bizStageType)) {//合同查询
            PageHelper.startPage(startPageNum, pageSizeNum);
            resList = ctrLoanContMapper.selectDoubleViewFromCtrLoanCont(map);
            PageHelper.clearPage();
            //列表转化
            resList = resList.parallelStream().map(ret -> {
                DoubleViewDto temp = new DoubleViewDto();
                BeanUtils.copyProperties(ret, temp);
                //获取客户经理名称
                String managerName = findManagerName(ret.getManagerBrId());
                temp.setManagerName(managerName);
                //获取机构名称
                String managerBrName = findManagerBrName(ret.getManagerBrId());
                temp.setManagerBrName(managerBrName);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
        }
        logger.info("**********DoubleViewServices**双录信息查询结束,返回参数为:{}", JSON.toJSONString(resList));
        return resList;
    }

    /***
     * 列表转化公共方法
     * **/
    public java.util.List<DoubleViewDto> changeList(List list, String type) {
        java.util.List<DoubleViewDto> resList = new ArrayList<>();
        return resList;
    }

    /***
     * 获取责任人名称
     * **/
    public String  findManagerName(String managerId){
        String managerName = "";
        if(StringUtil.isNotEmpty(managerId)){
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                managerName = adminSmUserDto.getUserName();
            }
        }
        return managerName;
    }

    /***
     * 获取责任机构名称
     * **/
    public String  findManagerBrName(String managerBrId){
        String managerName = "";
        if(StringUtil.isNotEmpty(managerBrId)){
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(managerBrId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                managerName = adminSmOrgDto.getOrgName();
            }
        }
        return managerName;
    }

}
