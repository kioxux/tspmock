package cn.com.yusys.yusp.web.server.xdxw0027;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0027.req.Xdxw0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0027.resp.Xdxw0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0027.Xdxw0027Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:客户调查信息详情查看
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0027:客户调查信息详情查看")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0027Resource.class);

    @Autowired
    private Xdxw0027Service xdxw0027Service;
    /**
     * 交易码：xdxw0027
     * 交易描述：客户调查信息详情查看
     *
     * @param xdxw0027DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户调查信息详情查看")
    @PostMapping("/xdxw0027")
    protected @ResponseBody
    ResultDto<Xdxw0027DataRespDto> xdxw0027(@Validated @RequestBody Xdxw0027DataReqDto xdxw0027DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataReqDto));
        Xdxw0027DataRespDto xdxw0027DataRespDto = new Xdxw0027DataRespDto();// 响应Dto:客户调查信息详情查看
        ResultDto<Xdxw0027DataRespDto> xdxw0027DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataReqDto));
            xdxw0027DataRespDto = xdxw0027Service.getXdxw0027(xdxw0027DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataRespDto));
            // 封装xdxw0027DataResultDto中正确的返回码和返回信息
            xdxw0027DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0027DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, e.getMessage());
            xdxw0027DataResultDto.setCode(e.getErrorCode());
            xdxw0027DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, e.getMessage());
            // 封装xdxw0027DataResultDto中异常返回码和返回信息
            xdxw0027DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0027DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0027DataRespDto到xdxw0027DataResultDto中
        xdxw0027DataResultDto.setData(xdxw0027DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataResultDto));
        return xdxw0027DataResultDto;
    }
}
