package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarCont
 * @类描述: grt_guar_cont数据实体类
 * @功能描述:
 * @创建人: 刘权
 * @创建时间: 2021-05-05 18:50:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GrtGuarContDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 担保合同流水号  **/
	private String guarPkId;

	/** 担保合同编号  **/
	private String guarContNo;

	/** 担保合同中文合同编号 **/
	private String guarContCnNo;

	/** 客户编号 **/
	private String cusId;

	/** 是否追加担保  STD_ZB_YES_NO **/
	private String isSuperaddGuar;

	/** 担保合同类型 STD_ZB_GRT_TYP **/
	private String guarContType;

	/** 担保合同担保方式 STD_ZB_GUAR_WAY **/
	private String guarWay;

	/** 保证形式 STD_ZB_ASSURE_MODAL **/
	private String assureModal;

	/** 保证方式 **/
	private String assureWay;

	/** 纸质合同编号 **/
	private String paperContNo;

	/** 是否在线抵押 **/
	private String isOnlinePld;

	/** 抵质押合同类型 **/
	private String pldContType;

	/** 是否授信项下 STD_ZB_YES_NO **/
	private String isUnderLmt;

	/** 抵押顺位 **/
	private String pldOrder;

	/** 是否浮动抵押 **/
	private String isFloatPld;

	/** 借款人编号 **/
	private String borrowerId;

	/** 贷款卡号 **/
	private String lnCardNo;

	/** 币种 **/
	private String curType;

	/** 是否合格担保 STD_ZB_YES_NO **/
	private String isQualifiedGuar;

	/** 担保金额 **/
	private java.math.BigDecimal guarAmt;

	/** 期限类型 STD_ZB_TERM_TYP **/
	private String termType;

	/** 担保期限 **/
	private Integer guarTerm;

	/** 担保起始日 **/
	private String guarStartDate;

	/** 担保终止日 **/
	private String guarEndDate;

	/** 签订日期 **/
	private String signDate;

	/** 质押类型 STD_ZB_IMN_TYPE **/
	private String imnType;

	/** 担保合同状态 STD_ZB_GRT_ST **/
	private String guarContState;

	/** 担保双录编号 **/
	private String guarIserchNo;

	/** 备注 **/
	private String remark;

	/** 争议借据方式选项 **/
	private String billDispupeOpt;

	/** 法院所在地 **/
	private String courtAddr;

	/** 仲裁委员会地点 **/
	private String arbitrateAddr;

	/** 其他方式 **/
	private String otherOpt;

	/** 仲裁机构 **/
	private String arbitrateBch;

	/** 合同份数 **/
	private Integer contQnt;

	/** 甲方执合同份数 **/
	private Integer contQntOwner;

	/** 乙方执合同份数 **/
	private Integer contQntPartyB;

	/** 丙方执合同份数 **/
	private Integer contQntPartyC;

	/** 丁方执合同份数 **/
	private Integer contQntPartyD;

	/** 戊方执合同份数 **/
	private Integer contQntPartyE;

	/** 其他主合同 **/
	private String otherMainCont;

	/** 确认最高债权额方式 STD_ZB_MAX_CLAIM_TP **/
	private String maxClaimTp;

	/** 最高债权额 **/
	private java.math.BigDecimal maxClaimAmt;

	/** 其他约定事项 **/
	private String otherAgreedEvent;

	/** 合同打印次数 **/
	private Integer contPrintNum;

	/** 申请类型 STD_ZB_GRT_APP_TYP **/
	private String approveType;

	/** 注销日期 **/
	private String logoutDate;

	/** 主办人 **/
	private String managerId;

	/** 主办机构 **/
	private String managerBrId;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 担保人编号 **/
	private String guarantorId;

	/** 担保人名称 **/
	private String assureName;

	/** 担保人证件类型 **/
	private String assureCertType;

	/** 业务条线 **/
	private String bizLine;

	/** 担保人证件号码 **/
	private String assureCertCode;

	/** 客户名称 **/
	private String cusName;

	/** 是否将存量债务纳入担保范围 **/
	private String isDebtGuar;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 批复编号 **/
	private String replyNo;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/**担保合同和业务关系表**/
	private GrtGuarBizRstRel grtGuarBizRstRel;

	/** 业务流水号  **/
	private String serno;

	/** 授信分享流水号 (用于关联业务与押品关系表) **/
	private String subSerno;

	public String getSubSerno() {
		return subSerno;
	}

	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}

	public String getSerno(){ return serno;};
	public void setSerno(String serno){this.serno = serno;}


	public GrtGuarBizRstRel getGrtGuarBizRstRel() {
		return grtGuarBizRstRel;
	}

	public void setGrtGuarBizRstRel(GrtGuarBizRstRel grtGuarBizRstRel) {
		this.grtGuarBizRstRel = grtGuarBizRstRel;
	}

	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId == null ? null : guarPkId.trim();
	}

	/**
	 * @return GuarPkId
	 */
	public String getGuarPkId() {
		return this.guarPkId;
	}

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}

	/**
	 * @return GuarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	/**
	 * @param guarContCnNo
	 */
	public void setGuarContCnNo(String guarContCnNo) {
		this.guarContCnNo = guarContCnNo == null ? null : guarContCnNo.trim();
	}

	/**
	 * @return GuarContCnNo
	 */
	public String getGuarContCnNo() {
		return this.guarContCnNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

	/**
	 * @return CusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param isSuperaddGuar
	 */
	public void setIsSuperaddGuar(String isSuperaddGuar) {
		this.isSuperaddGuar = isSuperaddGuar == null ? null : isSuperaddGuar.trim();
	}

	/**
	 * @return IsSuperaddGuar
	 */
	public String getIsSuperaddGuar() {
		return this.isSuperaddGuar;
	}

	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType == null ? null : guarContType.trim();
	}

	/**
	 * @return GuarContType
	 */
	public String getGuarContType() {
		return this.guarContType;
	}

	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}

	/**
	 * @return GuarWay
	 */
	public String getGuarWay() {
		return this.guarWay;
	}

	/**
	 * @param assureModal
	 */
	public void setAssureModal(String assureModal) {
		this.assureModal = assureModal == null ? null : assureModal.trim();
	}

	/**
	 * @return AssureModal
	 */
	public String getAssureModal() {
		return this.assureModal;
	}

	/**
	 * @param assureWay
	 */
	public void setAssureWay(String assureWay) {
		this.assureWay = assureWay == null ? null : assureWay.trim();
	}

	/**
	 * @return AssureWay
	 */
	public String getAssureWay() {
		return this.assureWay;
	}

	/**
	 * @param paperContNo
	 */
	public void setPaperContNo(String paperContNo) {
		this.paperContNo = paperContNo == null ? null : paperContNo.trim();
	}

	/**
	 * @return PaperContNo
	 */
	public String getPaperContNo() {
		return this.paperContNo;
	}

	/**
	 * @param isOnlinePld
	 */
	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld == null ? null : isOnlinePld.trim();
	}

	/**
	 * @return IsOnlinePld
	 */
	public String getIsOnlinePld() {
		return this.isOnlinePld;
	}

	/**
	 * @param pldContType
	 */
	public void setPldContType(String pldContType) {
		this.pldContType = pldContType == null ? null : pldContType.trim();
	}

	/**
	 * @return PldContType
	 */
	public String getPldContType() {
		return this.pldContType;
	}

	/**
	 * @param isUnderLmt
	 */
	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt == null ? null : isUnderLmt.trim();
	}

	/**
	 * @return IsUnderLmt
	 */
	public String getIsUnderLmt() {
		return this.isUnderLmt;
	}

	/**
	 * @param pldOrder
	 */
	public void setPldOrder(String pldOrder) {
		this.pldOrder = pldOrder == null ? null : pldOrder.trim();
	}

	/**
	 * @return PldOrder
	 */
	public String getPldOrder() {
		return this.pldOrder;
	}

	/**
	 * @param isFloatPld
	 */
	public void setIsFloatPld(String isFloatPld) {
		this.isFloatPld = isFloatPld == null ? null : isFloatPld.trim();
	}

	/**
	 * @return IsFloatPld
	 */
	public String getIsFloatPld() {
		return this.isFloatPld;
	}

	/**
	 * @param borrowerId
	 */
	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId == null ? null : borrowerId.trim();
	}

	/**
	 * @return BorrowerId
	 */
	public String getBorrowerId() {
		return this.borrowerId;
	}

	/**
	 * @param lnCardNo
	 */
	public void setLnCardNo(String lnCardNo) {
		this.lnCardNo = lnCardNo == null ? null : lnCardNo.trim();
	}

	/**
	 * @return LnCardNo
	 */
	public String getLnCardNo() {
		return this.lnCardNo;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}

	/**
	 * @return CurType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param isQualifiedGuar
	 */
	public void setIsQualifiedGuar(String isQualifiedGuar) {
		this.isQualifiedGuar = isQualifiedGuar == null ? null : isQualifiedGuar.trim();
	}

	/**
	 * @return IsQualifiedGuar
	 */
	public String getIsQualifiedGuar() {
		return this.isQualifiedGuar;
	}

	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}

	/**
	 * @return GuarAmt
	 */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}

	/**
	 * @return TermType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param guarTerm
	 */
	public void setGuarTerm(Integer guarTerm) {
		this.guarTerm = guarTerm;
	}

	/**
	 * @return GuarTerm
	 */
	public Integer getGuarTerm() {
		return this.guarTerm;
	}

	/**
	 * @param guarStartDate
	 */
	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate == null ? null : guarStartDate.trim();
	}

	/**
	 * @return GuarStartDate
	 */
	public String getGuarStartDate() {
		return this.guarStartDate;
	}

	/**
	 * @param guarEndDate
	 */
	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate == null ? null : guarEndDate.trim();
	}

	/**
	 * @return GuarEndDate
	 */
	public String getGuarEndDate() {
		return this.guarEndDate;
	}

	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate == null ? null : signDate.trim();
	}

	/**
	 * @return SignDate
	 */
	public String getSignDate() {
		return this.signDate;
	}

	/**
	 * @param imnType
	 */
	public void setImnType(String imnType) {
		this.imnType = imnType == null ? null : imnType.trim();
	}

	/**
	 * @return ImnType
	 */
	public String getImnType() {
		return this.imnType;
	}

	/**
	 * @param guarContState
	 */
	public void setGuarContState(String guarContState) {
		this.guarContState = guarContState == null ? null : guarContState.trim();
	}

	/**
	 * @return GuarContState
	 */
	public String getGuarContState() {
		return this.guarContState;
	}

	/**
	 * @param guarIserchNo
	 */
	public void setGuarIserchNo(String guarIserchNo) {
		this.guarIserchNo = guarIserchNo == null ? null : guarIserchNo.trim();
	}

	/**
	 * @return GuarIserchNo
	 */
	public String getGuarIserchNo() {
		return this.guarIserchNo;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	/**
	 * @return Remark
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param billDispupeOpt
	 */
	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt == null ? null : billDispupeOpt.trim();
	}

	/**
	 * @return BillDispupeOpt
	 */
	public String getBillDispupeOpt() {
		return this.billDispupeOpt;
	}

	/**
	 * @param courtAddr
	 */
	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr == null ? null : courtAddr.trim();
	}

	/**
	 * @return CourtAddr
	 */
	public String getCourtAddr() {
		return this.courtAddr;
	}

	/**
	 * @param arbitrateAddr
	 */
	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr == null ? null : arbitrateAddr.trim();
	}

	/**
	 * @return ArbitrateAddr
	 */
	public String getArbitrateAddr() {
		return this.arbitrateAddr;
	}

	/**
	 * @param otherOpt
	 */
	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt == null ? null : otherOpt.trim();
	}

	/**
	 * @return OtherOpt
	 */
	public String getOtherOpt() {
		return this.otherOpt;
	}

	/**
	 * @param arbitrateBch
	 */
	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch == null ? null : arbitrateBch.trim();
	}

	/**
	 * @return ArbitrateBch
	 */
	public String getArbitrateBch() {
		return this.arbitrateBch;
	}

	/**
	 * @param contQnt
	 */
	public void setContQnt(Integer contQnt) {
		this.contQnt = contQnt;
	}

	/**
	 * @return ContQnt
	 */
	public Integer getContQnt() {
		return this.contQnt;
	}

	/**
	 * @param contQntOwner
	 */
	public void setContQntOwner(Integer contQntOwner) {
		this.contQntOwner = contQntOwner;
	}

	/**
	 * @return ContQntOwner
	 */
	public Integer getContQntOwner() {
		return this.contQntOwner;
	}

	/**
	 * @param contQntPartyB
	 */
	public void setContQntPartyB(Integer contQntPartyB) {
		this.contQntPartyB = contQntPartyB;
	}

	/**
	 * @return ContQntPartyB
	 */
	public Integer getContQntPartyB() {
		return this.contQntPartyB;
	}

	/**
	 * @param contQntPartyC
	 */
	public void setContQntPartyC(Integer contQntPartyC) {
		this.contQntPartyC = contQntPartyC;
	}

	/**
	 * @return ContQntPartyC
	 */
	public Integer getContQntPartyC() {
		return this.contQntPartyC;
	}

	/**
	 * @param contQntPartyD
	 */
	public void setContQntPartyD(Integer contQntPartyD) {
		this.contQntPartyD = contQntPartyD;
	}

	/**
	 * @return ContQntPartyD
	 */
	public Integer getContQntPartyD() {
		return this.contQntPartyD;
	}

	/**
	 * @param contQntPartyE
	 */
	public void setContQntPartyE(Integer contQntPartyE) {
		this.contQntPartyE = contQntPartyE;
	}

	/**
	 * @return ContQntPartyE
	 */
	public Integer getContQntPartyE() {
		return this.contQntPartyE;
	}

	/**
	 * @param otherMainCont
	 */
	public void setOtherMainCont(String otherMainCont) {
		this.otherMainCont = otherMainCont == null ? null : otherMainCont.trim();
	}

	/**
	 * @return OtherMainCont
	 */
	public String getOtherMainCont() {
		return this.otherMainCont;
	}

	/**
	 * @param maxClaimTp
	 */
	public void setMaxClaimTp(String maxClaimTp) {
		this.maxClaimTp = maxClaimTp == null ? null : maxClaimTp.trim();
	}

	/**
	 * @return MaxClaimTp
	 */
	public String getMaxClaimTp() {
		return this.maxClaimTp;
	}

	/**
	 * @param maxClaimAmt
	 */
	public void setMaxClaimAmt(java.math.BigDecimal maxClaimAmt) {
		this.maxClaimAmt = maxClaimAmt;
	}

	/**
	 * @return MaxClaimAmt
	 */
	public java.math.BigDecimal getMaxClaimAmt() {
		return this.maxClaimAmt;
	}

	/**
	 * @param otherAgreedEvent
	 */
	public void setOtherAgreedEvent(String otherAgreedEvent) {
		this.otherAgreedEvent = otherAgreedEvent == null ? null : otherAgreedEvent.trim();
	}

	/**
	 * @return OtherAgreedEvent
	 */
	public String getOtherAgreedEvent() {
		return this.otherAgreedEvent;
	}

	/**
	 * @param contPrintNum
	 */
	public void setContPrintNum(Integer contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	/**
	 * @return ContPrintNum
	 */
	public Integer getContPrintNum() {
		return this.contPrintNum;
	}

	/**
	 * @param approveType
	 */
	public void setApproveType(String approveType) {
		this.approveType = approveType == null ? null : approveType.trim();
	}

	/**
	 * @return ApproveType
	 */
	public String getApproveType() {
		return this.approveType;
	}

	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate == null ? null : logoutDate.trim();
	}

	/**
	 * @return LogoutDate
	 */
	public String getLogoutDate() {
		return this.logoutDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param guarantorId
	 */
	public void setGuarantorId(String guarantorId) {
		this.guarantorId = guarantorId == null ? null : guarantorId.trim();
	}

	/**
	 * @return GuarantorId
	 */
	public String getGuarantorId() {
		return this.guarantorId;
	}

	/**
	 * @param assureName
	 */
	public void setAssureName(String assureName) {
		this.assureName = assureName == null ? null : assureName.trim();
	}

	/**
	 * @return AssureName
	 */
	public String getAssureName() {
		return this.assureName;
	}

	/**
	 * @param assureCertType
	 */
	public void setAssureCertType(String assureCertType) {
		this.assureCertType = assureCertType == null ? null : assureCertType.trim();
	}

	/**
	 * @return AssureCertType
	 */
	public String getAssureCertType() {
		return this.assureCertType;
	}

	/**
	 * @param bizLine
	 */
	public void setBizLine(String bizLine) {
		this.bizLine = bizLine == null ? null : bizLine.trim();
	}

	/**
	 * @return BizLine
	 */
	public String getBizLine() {
		return this.bizLine;
	}

	/**
	 * @param assureCertCode
	 */
	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode == null ? null : assureCertCode.trim();
	}

	/**
	 * @return AssureCertCode
	 */
	public String getAssureCertCode() {
		return this.assureCertCode;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

	/**
	 * @return CusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param isDebtGuar
	 */
	public void setIsDebtGuar(String isDebtGuar) {
		this.isDebtGuar = isDebtGuar == null ? null : isDebtGuar.trim();
	}

	/**
	 * @return IsDebtGuar
	 */
	public String getIsDebtGuar() {
		return this.isDebtGuar;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}

	/**
	 * @return LmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}

	/**
	 * @return ReplyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
