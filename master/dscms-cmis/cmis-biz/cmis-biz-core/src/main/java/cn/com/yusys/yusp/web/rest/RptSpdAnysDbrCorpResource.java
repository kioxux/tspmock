/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysDbrCorp;
import cn.com.yusys.yusp.service.RptSpdAnysDbrCorpService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysDbrCorpResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-24 16:51:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysdbrcorp")
public class RptSpdAnysDbrCorpResource {
    @Autowired
    private RptSpdAnysDbrCorpService rptSpdAnysDbrCorpService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysDbrCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysDbrCorp> list = rptSpdAnysDbrCorpService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysDbrCorp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysDbrCorp>> index(QueryModel queryModel) {
        List<RptSpdAnysDbrCorp> list = rptSpdAnysDbrCorpService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysDbrCorp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysDbrCorp> show(@PathVariable("serno") String serno) {
        RptSpdAnysDbrCorp rptSpdAnysDbrCorp = rptSpdAnysDbrCorpService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysDbrCorp>(rptSpdAnysDbrCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysDbrCorp> create(@RequestBody RptSpdAnysDbrCorp rptSpdAnysDbrCorp) throws URISyntaxException {
        rptSpdAnysDbrCorpService.insert(rptSpdAnysDbrCorp);
        return new ResultDto<RptSpdAnysDbrCorp>(rptSpdAnysDbrCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysDbrCorp rptSpdAnysDbrCorp) throws URISyntaxException {
        int result = rptSpdAnysDbrCorpService.update(rptSpdAnysDbrCorp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysDbrCorpService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysDbrCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询 财务情况资产负债近一期自制和税务报表分析
     * @param map
     * @return RptSpdAnysDbrCorp rptSpdAnysDbrCorp
     */
    @ApiOperation("根据流水号查看担保融（企业类）的信息")
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysDbrCorp> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptSpdAnysDbrCorp>(rptSpdAnysDbrCorpService.selectByPrimaryKey(serno));
    }

    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptSpdAnysDbrCorp rptSpdAnysDbrCorp){
        return  new ResultDto<Integer>(rptSpdAnysDbrCorpService.save(rptSpdAnysDbrCorp));
    }
}
