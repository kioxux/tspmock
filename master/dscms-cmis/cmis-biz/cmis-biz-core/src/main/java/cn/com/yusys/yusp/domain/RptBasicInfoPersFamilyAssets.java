/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoPersFamilyAssets
 * @类描述: rpt_basic_info_pers_family_assets数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 17:26:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_pers_family_assets")
public class RptBasicInfoPersFamilyAssets extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 资产类型 **/
	@Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String assetType;
	
	/** 与企业关系 **/
	@Column(name = "ENTERPRISES_RELATION", unique = false, nullable = true, length = 20)
	private String enterprisesRelation;
	
	/** 资产范围 **/
	@Column(name = "ASSET_RANGE", unique = false, nullable = true, length = 40)
	private String assetRange;
	
	/** 所有权人 **/
	@Column(name = "ASSET_OWNER", unique = false, nullable = true, length = 40)
	private String assetOwner;
	
	/** 地理位置/被投资公司/理财等 **/
	@Column(name = "GERPC_POSI_INVEST_CASE", unique = false, nullable = true, length = 40)
	private String gerpcPosiInvestCase;
	
	/** 面积 **/
	@Column(name = "SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal squ;
	
	/** 投入原值 **/
	@Column(name = "ORIGI_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiValue;
	
	/** 市场价值（估值） **/
	@Column(name = "MARKET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal marketValue;
	
	/** 是否抵质押 **/
	@Column(name = "GAUR_IND", unique = false, nullable = true, length = 5)
	private String gaurInd;
	
	/** 已抵押金额 **/
	@Column(name = "HAS_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasGuarAmt;
	
	/** 未抵押金额 **/
	@Column(name = "LAST_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastGuarAmt;
	
	/** 资产所有人 **/
	@Column(name = "ASSET_OWNER_PERSON", unique = false, nullable = true, length = 40)
	private String assetOwnerPerson;
	
	/** 是否二押 **/
	@Column(name = "SECOND_GUAR_IND", unique = false, nullable = true, length = 5)
	private String secondGuarInd;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 出租自用标志 **/
	@Column(name = "RENT_SELFUSE_FLAG", unique = false, nullable = true, length = 5)
	private String rentSelfuseFlag;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param assetType
	 */
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	
    /**
     * @return assetType
     */
	public String getAssetType() {
		return this.assetType;
	}
	
	/**
	 * @param enterprisesRelation
	 */
	public void setEnterprisesRelation(String enterprisesRelation) {
		this.enterprisesRelation = enterprisesRelation;
	}
	
    /**
     * @return enterprisesRelation
     */
	public String getEnterprisesRelation() {
		return this.enterprisesRelation;
	}
	
	/**
	 * @param assetRange
	 */
	public void setAssetRange(String assetRange) {
		this.assetRange = assetRange;
	}
	
    /**
     * @return assetRange
     */
	public String getAssetRange() {
		return this.assetRange;
	}
	
	/**
	 * @param assetOwner
	 */
	public void setAssetOwner(String assetOwner) {
		this.assetOwner = assetOwner;
	}
	
    /**
     * @return assetOwner
     */
	public String getAssetOwner() {
		return this.assetOwner;
	}
	
	/**
	 * @param gerpcPosiInvestCase
	 */
	public void setGerpcPosiInvestCase(String gerpcPosiInvestCase) {
		this.gerpcPosiInvestCase = gerpcPosiInvestCase;
	}
	
    /**
     * @return gerpcPosiInvestCase
     */
	public String getGerpcPosiInvestCase() {
		return this.gerpcPosiInvestCase;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return squ
     */
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param origiValue
	 */
	public void setOrigiValue(java.math.BigDecimal origiValue) {
		this.origiValue = origiValue;
	}
	
    /**
     * @return origiValue
     */
	public java.math.BigDecimal getOrigiValue() {
		return this.origiValue;
	}
	
	/**
	 * @param marketValue
	 */
	public void setMarketValue(java.math.BigDecimal marketValue) {
		this.marketValue = marketValue;
	}
	
    /**
     * @return marketValue
     */
	public java.math.BigDecimal getMarketValue() {
		return this.marketValue;
	}
	
	/**
	 * @param gaurInd
	 */
	public void setGaurInd(String gaurInd) {
		this.gaurInd = gaurInd;
	}
	
    /**
     * @return gaurInd
     */
	public String getGaurInd() {
		return this.gaurInd;
	}
	
	/**
	 * @param hasGuarAmt
	 */
	public void setHasGuarAmt(java.math.BigDecimal hasGuarAmt) {
		this.hasGuarAmt = hasGuarAmt;
	}
	
    /**
     * @return hasGuarAmt
     */
	public java.math.BigDecimal getHasGuarAmt() {
		return this.hasGuarAmt;
	}
	
	/**
	 * @param lastGuarAmt
	 */
	public void setLastGuarAmt(java.math.BigDecimal lastGuarAmt) {
		this.lastGuarAmt = lastGuarAmt;
	}
	
    /**
     * @return lastGuarAmt
     */
	public java.math.BigDecimal getLastGuarAmt() {
		return this.lastGuarAmt;
	}
	
	/**
	 * @param assetOwnerPerson
	 */
	public void setAssetOwnerPerson(String assetOwnerPerson) {
		this.assetOwnerPerson = assetOwnerPerson;
	}
	
    /**
     * @return assetOwnerPerson
     */
	public String getAssetOwnerPerson() {
		return this.assetOwnerPerson;
	}
	
	/**
	 * @param secondGuarInd
	 */
	public void setSecondGuarInd(String secondGuarInd) {
		this.secondGuarInd = secondGuarInd;
	}
	
    /**
     * @return secondGuarInd
     */
	public String getSecondGuarInd() {
		return this.secondGuarInd;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param rentSelfuseFlag
	 */
	public void setRentSelfuseFlag(String rentSelfuseFlag) {
		this.rentSelfuseFlag = rentSelfuseFlag;
	}
	
    /**
     * @return rentSelfuseFlag
     */
	public String getRentSelfuseFlag() {
		return this.rentSelfuseFlag;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}