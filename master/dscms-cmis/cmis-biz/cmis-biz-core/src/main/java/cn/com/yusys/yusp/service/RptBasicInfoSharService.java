/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasicInfoShar;
import cn.com.yusys.yusp.repository.mapper.RptBasicInfoSharMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoSharService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 15:46:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicInfoSharService {

    @Autowired
    private RptBasicInfoSharMapper rptBasicInfoSharMapper;

    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptBasicInfoShar selectByPrimaryKey(String pkId) {
        return rptBasicInfoSharMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptBasicInfoShar> selectAll(QueryModel model) {
        List<RptBasicInfoShar> records = (List<RptBasicInfoShar>) rptBasicInfoSharMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptBasicInfoShar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasicInfoShar> list = rptBasicInfoSharMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptBasicInfoShar record) {
        return rptBasicInfoSharMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptBasicInfoShar record) {
        return rptBasicInfoSharMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptBasicInfoShar record) {
        return rptBasicInfoSharMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptBasicInfoShar record) {
        return rptBasicInfoSharMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptBasicInfoSharMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicInfoSharMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询股东信息
     *
     * @param serno
     * @return
     */
    public List<RptBasicInfoShar> queryStockHolder(String serno) {
        return rptBasicInfoSharMapper.queryStockHolder(serno);
    }

    /**
     * 初始化股东信息
     * @param model
     * @return
     */
    public List<RptBasicInfoShar> initShar(QueryModel model) {
        String cusId = model.getCondition().get("cusId").toString();
        String serno = model.getCondition().get("serno").toString();
        List<RptBasicInfoShar> resultData = new ArrayList<>();
        List<RptBasicInfoShar> rptBasicInfoSharList = queryStockHolder(serno);
        if (CollectionUtils.nonEmpty(rptBasicInfoSharList)) {
            return rptBasicInfoSharList;
        } else {
            ResultDto<List<CusCorpApitalDto>> cusCorpApitalDtoResultDto = iCusClientService.selectByCusIdRel(cusId);
            if (cusCorpApitalDtoResultDto != null && cusCorpApitalDtoResultDto.getData() != null) {
                List<CusCorpApitalDto> CusCorpApitalList = cusCorpApitalDtoResultDto.getData();
                if (CollectionUtils.nonEmpty(CusCorpApitalList)) {
                    for (CusCorpApitalDto cusCorpApitalDto : CusCorpApitalList) {
                        RptBasicInfoShar rptBasicInfoShar = new RptBasicInfoShar();
                        rptBasicInfoShar.setPkId(StringUtils.getUUID());
                        rptBasicInfoShar.setSerno(serno);
                        rptBasicInfoShar.setShdCusId(cusCorpApitalDto.getCusId());
                        rptBasicInfoShar.setShdCusName(cusCorpApitalDto.getInvtName());
                        rptBasicInfoShar.setPerc(cusCorpApitalDto.getInvtPerc());
                        rptBasicInfoShar.setPaidCap(cusCorpApitalDto.getInvtAmt());
                        rptBasicInfoShar.setInvApp(cusCorpApitalDto.getInvtType());
                        int count = insertSelective(rptBasicInfoShar);
                        if (count > 0) {
                            resultData.add(rptBasicInfoShar);
                        }
                    }
                }
            }
        }

        return resultData;
    }
    public int save(RptBasicInfoShar rptBasicInfoShar){
        String pkId = rptBasicInfoShar.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return rptBasicInfoSharMapper.updateByPrimaryKey(rptBasicInfoShar);
        }else {
            rptBasicInfoShar.setPkId(StringUtils.getUUID());
            return rptBasicInfoSharMapper.insert(rptBasicInfoShar);
        }
    }
}
