/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateLoanAppSub
 * @类描述: other_cny_rate_loan_app_sub数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-08 10:01:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_loan_app_sub")
public class OtherCnyRateLoanAppSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = false, length = 40)
	private String subSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 统计类型 **/
	@Column(name = "STATIS_TYPE", unique = false, nullable = false, length = 5)
	private String statisType;
	
	/** 计算金额 **/
	@Column(name = "CAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal calAmt;
	
	/** 具体品种 **/
	@Column(name = "CONC_BREED", unique = false, nullable = true, length = 80)
	private String concBreed;
	
	/** 收益率 **/
	@Column(name = "EARN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal earnRate;
	
	/** 存期对应FTP价格 **/
	@Column(name = "MATURITY_FTP_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maturityFtpPrice;
	
	/** 执行利率 **/
	@Column(name = "REALITY_IR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realityIr;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param statisType
	 */
	public void setStatisType(String statisType) {
		this.statisType = statisType;
	}
	
    /**
     * @return statisType
     */
	public String getStatisType() {
		return this.statisType;
	}
	
	/**
	 * @param calAmt
	 */
	public void setCalAmt(java.math.BigDecimal calAmt) {
		this.calAmt = calAmt;
	}
	
    /**
     * @return calAmt
     */
	public java.math.BigDecimal getCalAmt() {
		return this.calAmt;
	}
	
	/**
	 * @param concBreed
	 */
	public void setConcBreed(String concBreed) {
		this.concBreed = concBreed;
	}
	
    /**
     * @return concBreed
     */
	public String getConcBreed() {
		return this.concBreed;
	}
	
	/**
	 * @param earnRate
	 */
	public void setEarnRate(java.math.BigDecimal earnRate) {
		this.earnRate = earnRate;
	}
	
    /**
     * @return earnRate
     */
	public java.math.BigDecimal getEarnRate() {
		return this.earnRate;
	}
	
	/**
	 * @param maturityFtpPrice
	 */
	public void setMaturityFtpPrice(java.math.BigDecimal maturityFtpPrice) {
		this.maturityFtpPrice = maturityFtpPrice;
	}
	
    /**
     * @return maturityFtpPrice
     */
	public java.math.BigDecimal getMaturityFtpPrice() {
		return this.maturityFtpPrice;
	}
	
	/**
	 * @param realityIr
	 */
	public void setRealityIr(java.math.BigDecimal realityIr) {
		this.realityIr = realityIr;
	}
	
    /**
     * @return realityIr
     */
	public java.math.BigDecimal getRealityIr() {
		return this.realityIr;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}