/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcdOper
 * @类描述: rpt_spd_anys_mcd_oper数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-22 22:58:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_mcd_oper")
public class RptSpdAnysMcdOper extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 木材品种 **/
	@Column(name = "WOOD_TYPE", unique = false, nullable = true, length = 80)
	private String woodType;
	
	/** 入库数量 **/
	@Column(name = "INVENTORY_NUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inventoryNum;
	
	/** 采购金额 **/
	@Column(name = "BUY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal buyAmt;
	
	/** 出库数量 **/
	@Column(name = "DELIVERY_NUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal deliveryNum;
	
	/** 销售金额 **/
	@Column(name = "SALES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal salesAmt;
	
	/** 当年库存数量 **/
	@Column(name = "CURR_INVENTORY_NUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currInventoryNum;
	
	/** 当年库存金额 **/
	@Column(name = "CURR_INVENTORY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currInventoryAmt;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param woodType
	 */
	public void setWoodType(String woodType) {
		this.woodType = woodType;
	}
	
    /**
     * @return woodType
     */
	public String getWoodType() {
		return this.woodType;
	}
	
	/**
	 * @param inventoryNum
	 */
	public void setInventoryNum(java.math.BigDecimal inventoryNum) {
		this.inventoryNum = inventoryNum;
	}
	
    /**
     * @return inventoryNum
     */
	public java.math.BigDecimal getInventoryNum() {
		return this.inventoryNum;
	}
	
	/**
	 * @param buyAmt
	 */
	public void setBuyAmt(java.math.BigDecimal buyAmt) {
		this.buyAmt = buyAmt;
	}
	
    /**
     * @return buyAmt
     */
	public java.math.BigDecimal getBuyAmt() {
		return this.buyAmt;
	}
	
	/**
	 * @param deliveryNum
	 */
	public void setDeliveryNum(java.math.BigDecimal deliveryNum) {
		this.deliveryNum = deliveryNum;
	}
	
    /**
     * @return deliveryNum
     */
	public java.math.BigDecimal getDeliveryNum() {
		return this.deliveryNum;
	}
	
	/**
	 * @param salesAmt
	 */
	public void setSalesAmt(java.math.BigDecimal salesAmt) {
		this.salesAmt = salesAmt;
	}
	
    /**
     * @return salesAmt
     */
	public java.math.BigDecimal getSalesAmt() {
		return this.salesAmt;
	}
	
	/**
	 * @param currInventoryNum
	 */
	public void setCurrInventoryNum(java.math.BigDecimal currInventoryNum) {
		this.currInventoryNum = currInventoryNum;
	}
	
    /**
     * @return currInventoryNum
     */
	public java.math.BigDecimal getCurrInventoryNum() {
		return this.currInventoryNum;
	}
	
	/**
	 * @param currInventoryAmt
	 */
	public void setCurrInventoryAmt(java.math.BigDecimal currInventoryAmt) {
		this.currInventoryAmt = currInventoryAmt;
	}
	
    /**
     * @return currInventoryAmt
     */
	public java.math.BigDecimal getCurrInventoryAmt() {
		return this.currInventoryAmt;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}