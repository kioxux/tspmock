/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.req.CmisLmt0027ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarSpeCorp;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysGuarSpeCorpMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarSpeCorpService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:40:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysGuarSpeCorpService {
    private static final Logger log = LoggerFactory.getLogger(RptLmtRepayAnysGuarSpeCorpService.class);
    @Autowired
    private RptLmtRepayAnysGuarSpeCorpMapper rptLmtRepayAnysGuarSpeCorpMapper;
    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLmtRepayAnysGuarSpeCorp selectByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarSpeCorpMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLmtRepayAnysGuarSpeCorp> selectAll(QueryModel model) {
        List<RptLmtRepayAnysGuarSpeCorp> records = (List<RptLmtRepayAnysGuarSpeCorp>) rptLmtRepayAnysGuarSpeCorpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLmtRepayAnysGuarSpeCorp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnysGuarSpeCorp> list = rptLmtRepayAnysGuarSpeCorpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnysGuarSpeCorp record) {
        return rptLmtRepayAnysGuarSpeCorpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnysGuarSpeCorp record) {
        return rptLmtRepayAnysGuarSpeCorpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnysGuarSpeCorp record) {
        return rptLmtRepayAnysGuarSpeCorpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnysGuarSpeCorp record) {
        return rptLmtRepayAnysGuarSpeCorpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarSpeCorpMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptLmtRepayAnysGuarSpeCorpMapper.deleteByIds(ids);
    }

    /**
     * 保存信息
     *
     * @param rptLmtRepayAnysGuarSpeCorp
     * @return
     */
    public int saveGuarSpecorp(RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp) {
        String pkId = rptLmtRepayAnysGuarSpeCorp.getPkId();
        RptLmtRepayAnysGuarSpeCorp temp = rptLmtRepayAnysGuarSpeCorpMapper.selectByPrimaryKey(pkId);
        int count = 0;
        if (temp != null) {
            count = rptLmtRepayAnysGuarSpeCorpMapper.updateByPrimaryKeySelective(rptLmtRepayAnysGuarSpeCorp);
        } else {
            count = rptLmtRepayAnysGuarSpeCorpMapper.insertSelective(rptLmtRepayAnysGuarSpeCorp);
        }
        return count;
    }

    public RptLmtRepayAnysGuarSpeCorp selectBySerno(String serno) {
        return rptLmtRepayAnysGuarSpeCorpMapper.selectBySerno(serno);
    }

    public RptLmtRepayAnysGuarSpeCorp initGuarSpecorp(Map map) {
        String serno = map.get("serno").toString();
        RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp = rptLmtRepayAnysGuarSpeCorpMapper.selectBySerno(serno);
        if (Objects.nonNull(rptLmtRepayAnysGuarSpeCorp)) {
            return rptLmtRepayAnysGuarSpeCorp;
        } else {
            RptLmtRepayAnysGuarSpeCorp result = new RptLmtRepayAnysGuarSpeCorp();
            result.setPkId(StringUtils.getUUID());
            result.setSerno(serno);
            String partnerNo = map.get("partnerNo").toString();
            CoopPlanAccInfo coopPlanAccInfo = coopPlanAccInfoService.selectByPartnerNo(partnerNo);
            if (Objects.nonNull(coopPlanAccInfo)) {
                result.setGuarComName(coopPlanAccInfo.getPartnerName());
                result.setCoopTotalAmt(coopPlanAccInfo.getTotlCoopLmtAmt());
                result.setSingAccoLmt(coopPlanAccInfo.getSingleCoopQuota());
                result.setSingBusiCoopLmt(coopPlanAccInfo.getSigBusiCoopQuota());
                result.setComGraPed(coopPlanAccInfo.getSubpayGraper());
                result.setPropDefaultRisk(coopPlanAccInfo.getSubpayPerc());
                result.setCoopStartDate(coopPlanAccInfo.getCoopStartDate());
                result.setCoopEndDate(coopPlanAccInfo.getCoopEndDate());
                String coopPlanStatus = coopPlanAccInfo.getCoopPlanStatus();
                if ("1".equals(coopPlanStatus)) {
                    result.setPartnerStatus("正常");
                } else if ("2".equals(coopPlanStatus)) {
                    result.setPartnerStatus("终止");
                } else if ("3".equals(coopPlanStatus)) {
                    result.setPartnerStatus("退出");
                }
                User userInfo = SessionUtils.getUserInformation();
                String instuCode = CmisCommonUtils.getInstucde(userInfo.getOrg().getCode());
                CmisLmt0027ReqDto dto = new CmisLmt0027ReqDto();
                dto.setCusId(partnerNo);
                dto.setInstuCde(instuCode);
                log.info("获取合作方可用额度和已用额度,请求报文:" + JSON.toJSONString(dto));
                ResultDto<CmisLmt0027RespDto> cmisLmt0027RespDtoResultDto = cmisLmtClientService.cmislmt0027(dto);
                log.info("获取合作方可用额度和已用额度,响应报文:" + JSON.toJSONString(cmisLmt0027RespDtoResultDto));
                if (cmisLmt0027RespDtoResultDto != null && cmisLmt0027RespDtoResultDto.getData() != null) {
                    CmisLmt0027RespDto data = cmisLmt0027RespDtoResultDto.getData();
                    result.setUsedCoopAmt(data.getOutstandAmt());
                    result.setAvailCoopAmt(data.getValAmt());
                }
            }
            insert(result);
            return result;
        }
    }
}
