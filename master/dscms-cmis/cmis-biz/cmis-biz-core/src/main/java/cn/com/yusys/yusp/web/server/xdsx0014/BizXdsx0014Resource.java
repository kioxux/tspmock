package cn.com.yusys.yusp.web.server.xdsx0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0014.req.Xdsx0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.resp.Xdsx0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0014.Xdsx0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:风控发信贷进行授信作废接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0014:风控发信贷进行授信作废接口")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0014Resource.class);

    @Autowired
    private Xdsx0014Service xdsx0014Service;

    /**
     * 交易码：xdsx0014
     * 交易描述：风控发信贷进行授信作废接口
     *
     * @param xdsx0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发信贷进行授信作废接口")
    @PostMapping("/xdsx0014")
    protected @ResponseBody
    ResultDto<Xdsx0014DataRespDto> xdsx0014(@Validated @RequestBody Xdsx0014DataReqDto xdsx0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014DataReqDto));
        Xdsx0014DataRespDto xdsx0014DataRespDto = new Xdsx0014DataRespDto();// 响应Dto:风控发信贷进行授信作废接口
        ResultDto<Xdsx0014DataRespDto> xdsx0014DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0014DataReqDto获取业务值进行业务逻辑处理
            xdsx0014DataRespDto = xdsx0014Service.xdsx0014(xdsx0014DataReqDto);
            // 封装xdsx0014DataResultDto中正确的返回码和返回信息
            xdsx0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
            // 封装xdsx0014DataResultDto中异常返回码和返回信息
            xdsx0014DataResultDto.setCode(e.getErrorCode());
            xdsx0014DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
            // 封装xdsx0014DataResultDto中异常返回码和返回信息
            xdsx0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0014DataRespDto到xdsx0014DataResultDto中
        xdsx0014DataResultDto.setData(xdsx0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014DataResultDto));
        return xdsx0014DataResultDto;
    }
}
