package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanCont
 * @类描述: ctr_loan_cont数据实体类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-09-04 21:10:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrLoanContDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 合同编号 **/
	private String contNo;

	/** 中文合同编号 **/
	private String contCnNo;

	/** 调查编号 **/
	private String surveySerno;

	/** 批复编号 **/
	private String replyNo;

	/** 业务申请流水号 **/
	private String serno;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 产品类型属性 **/
	private String prdTypeProp;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 手机号码 **/
	private String phone;

	/** 业务类型 **/
	private String bizType;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 特殊业务类型 **/
	private String especBizType;

	/** 贷款用途 **/
	private String loanPurp;

	/** 其他贷款用途描述 **/
	private String loanPurpDesc;

	/** 贷款形式 **/
	private String loanModal;

	/** 贷款性质 **/
	private String loanCha;

	/** 是否曾被拒绝 **/
	private String isHasRefused;

	/** 主担保方式 **/
	private String guarWay;

	/** 是否共同申请人 **/
	private String isCommonRqstr;

	/** 是否确认支付方式 **/
	private String isCfirmPayWay;

	/** 支付方式 **/
	private String payMode;

	/** 币种 **/
	private String curType;

	/** 合同类型 **/
	private String contType;

	/** 合同期限 **/
	private Integer contTerm;

	/** 合同起始日期 **/
	private String contStartDate;

	/** 合同到期日期 **/
	private String contEndDate;

	/** 合同状态 **/
	private String contStatus;

	/** 合同金额 **/
	private java.math.BigDecimal contAmt;

	/** 合同余额 **/
	private java.math.BigDecimal contBalance;

	/** 合同汇率 **/
	private java.math.BigDecimal contRate;

	/** 纸质合同签订日期 **/
	private String paperContSignDate;

	/** 结息方式 **/
	private String eiMode;

	/** 结息具体说明 **/
	private String eiModeExpl;

	/** 地址 **/
	private String addr;

	/** 传真 **/
	private String fax;

	/** 联系人 **/
	private String linkman;

	/** 邮箱 **/
	private String email;

	/** QQ **/
	private String qq;

	/** 微信 **/
	private String wechat;

	/** 本行角色 **/
	private String bankRole;

	/** 银团总金额 **/
	private java.math.BigDecimal bksyndicTotlAmt;

	/** 银团纸质合同编号 **/
	private String bksyndicPaperContNo;

	/** 还款顺序 **/
	private String repaySeq;

	/** 其他通讯方式及账号 **/
	private String otherPhone;

	/** 签约方式 **/
	private String signMode;

	/** 签约状态 **/
	private String signState;

	/** 签约渠道 **/
	private String signChannel;

	/** 保证金来源 **/
	private String bailSour;

	/** 保证金汇率 **/
	private java.math.BigDecimal bailExchangeRate;

	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;

	/** 保证金币种 **/
	private String bailCurType;

	/** 保证金金额 **/
	private java.math.BigDecimal bailAmt;

	/** 保证金折算人民币金额 **/
	private java.math.BigDecimal bailCvtCnyAmt;

	/** 折算人民币金额 **/
	private java.math.BigDecimal cvtCnyAmt;

	/** 期限类型 **/
	private String termType;

	/** 申请期限 **/
	private java.math.BigDecimal appTerm;

	/** 利率依据方式 **/
	private String irAccordType;

	/** 利率种类 **/
	private String irType;

	/** 基准利率（年） **/
	private java.math.BigDecimal rulingIr;

	/** 对应基准利率(月) **/
	private java.math.BigDecimal rulingIrM;

	/** 计息方式 **/
	private String loanRatType;

	/** 利率调整类型 **/
	private String irAdjustType;

	/** 利率调整周期(月) **/
	private java.math.BigDecimal irAdjustTerm;

	/** 调息方式 **/
	private String praType;

	/** 利率形式 **/
	private String rateType;

	/** LPR授信利率区间 **/
	private String lprRateIntval;

	/** 当前LPR利率 **/
	private java.math.BigDecimal curtLprRate;

	/** 正常利率浮动方式 **/
	private String irFloatType;

	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;

	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;

	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;

	/** 执行利率(月) **/
	private java.math.BigDecimal realityIrM;

	/** 逾期利率浮动比 **/
	private java.math.BigDecimal overdueRatePefloat;

	/** 逾期执行利率(年利率) **/
	private java.math.BigDecimal overdueExecRate;

	/** 复息利率浮动比 **/
	private java.math.BigDecimal ciRatePefloat;

	/** 复息执行利率(年利率) **/
	private java.math.BigDecimal ciExecRate;

	/** 违约利率浮动百分比 **/
	private java.math.BigDecimal defaultRate;

	/** 违约利率（年） **/
	private java.math.BigDecimal defaultRateY;

	/** 风险敞口金额 **/
	private java.math.BigDecimal riskOpenAmt;

	/** 还款方式 **/
	private String repayMode;

	/** 停本付息期间 **/
	private String stopPintTerm;

	/** 还款间隔周期 **/
	private String repayTerm;

	/** 还款间隔 **/
	private String repaySpace;

	/** 还款日确定规则 **/
	private String repayRule;

	/** 还款日类型 **/
	private String repayDtType;

	/** 还款日 **/
	private String repayDate;

	/** 本金宽限方式 **/
	private String capGraperType;

	/** 本金宽限天数 **/
	private String capGraperDay;

	/** 利息宽限方式 **/
	private String intGraperType;

	/** 利息宽限天数 **/
	private String intGraperDay;

	/** 扣款扣息方式 **/
	private String deductDeduType;

	/** 还款频率类型 **/
	private String repayFreType;

	/** 本息还款频率 **/
	private String repayFre;

	/** 提前还款违约金免除时间(月) **/
	private Integer liquFreeTime;

	/** 分段方式 **/
	private String subType;

	/** 保留期限 **/
	private Integer reserveTerm;

	/** 计算期限 **/
	private Integer calTerm;

	/** 保留金额 **/
	private java.math.BigDecimal reserveAmt;

	/** 第一阶段还款期数 **/
	private Integer repayTermOne;

	/** 第一阶段还款本金 **/
	private java.math.BigDecimal repayAmtOne;

	/** 第二阶段还款期数 **/
	private Integer repayTermTwo;

	/** 第二阶段还款本金 **/
	private java.math.BigDecimal repayAmtTwo;

	/** 利率选取日期种类 **/
	private String rateSelType;

	/** 贴息方式 **/
	private String sbsyMode;

	/** 贴息比例 **/
	private java.math.BigDecimal sbsyPerc;

	/** 贴息金额 **/
	private java.math.BigDecimal sbsyAmt;

	/** 贴息单位名称 **/
	private String sbsyUnitName;

	/** 贴息方账户 **/
	private String sbsyAcct;

	/** 贴息方账户户名 **/
	private String sbsyAcctName;

	/** 五级分类 **/
	private String fiveClass;

	/** 贷款投向 **/
	private String loanTer;

	/** 工业转型升级标识 **/
	private String comUpIndtify;

	/** 战略新兴产业类型 **/
	private String strategyNewLoan;

	/** 是否文化产业 **/
	private String isCulEstate;

	/** 贷款种类 **/
	private String loanType;

	/** 产业结构调整类型 **/
	private String estateAdjustType;

	/** 新兴产业贷款 **/
	private String newPrdLoan;

	/** 还款来源 **/
	private String repaySour;

	/** 是否委托人办理 **/
	private String isAuthorize;

	/** 委托人姓名 **/
	private String authedName;

	/** 委托人证件类型 **/
	private String consignorCertType;

	/** 委托人证件号 **/
	private String consignorCertCode;

	/** 委托人联系方式 **/
	private String authedTelNo;

	/** 签订日期 **/
	private String signDate;

	/** 注销日期 **/
	private String logoutDate;

	/** 渠道来源 **/
	private String chnlSour;

	/** 争议解决方式 **/
	private String billDispupeOpt;

	/** 法院所在地 **/
	private String courtAddr;

	/** 仲裁委员会 **/
	private String arbitrateBch;

	/** 仲裁委员会地点 **/
	private String arbitrateAddr;

	/** 用途分析 **/
	private String purpAnaly;

	/** 交叉核验详细分析 **/
	private String crossChkDetailAnaly;

	/** 调查人结论 **/
	private String inveConclu;

	/** 其他方式 **/
	private String otherOpt;

	/** 合同份数 **/
	private java.math.BigDecimal contQnt;

	/** 公积金贷款合同编号 **/
	private String pundContNo;

	/** 补充条款 **/
	private String spplClause;

	/** 签约地点 **/
	private String signAddr;

	/** 营业网点 **/
	private String busiNetwork;

	/** 主要营业场所地址 **/
	private String mainBusiPalce;

	/** 合同模板 **/
	private String contTemplate;

	/** 合同打印次数 **/
	private java.math.BigDecimal contPrintNum;

	/** 签章审批状态 **/
	private String signApproveStatus;

	/** 所属团队 **/
	private String team;

	/** 其他约定 **/
	private String otherAgreed;

	/** 开户行名称 **/
	private String acctsvcrName;

	/** 贷款发放账号 **/
	private String loanPayoutAccno;

	/** 贷款发放账号名称 **/
	private String loanPayoutAccName;

	/** 合同模式 **/
	private String contMode;

	/** 是否线上提款 **/
	private String isOnlineDraw;

	/** 线上合同启用标识 **/
	private String ctrBeginFlag;

	/** 申请状态 **/
	private String approveStatus;

	/** 本合同项下最高可用信金额 **/
	private java.math.BigDecimal highAvlAmt;

	/** 是否续签 **/
	private String isRenew;

	/** 原合同编号 **/
	private String origiContNo;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 是否在线抵押 **/
	private String isOlPld;

	/** 是否先放款后抵押 **/
	private String beforehandInd;

	/** 是否电子用印 **/
	private String isESeal;

	/** 双录编号 **/
	private String doubleRecordNo;

	/** 借款利率调整日 **/
	private String loanRateAdjDay;

	/** 提款方式 **/
	private String drawMode;

	/** 提款天数限制 **/
	private String dayLimit;

	/** 所属条线 **/
	private String belgLine;

	/** 债项等级 **/
	private String debtLevel;

	/** 违约损失率LGD **/
	private java.math.BigDecimal lgd;

	/** 违约风险暴露EAD **/
	private java.math.BigDecimal ead;

	/** 其他借款用途 **/
	private String otherLoanPurp;

	/** 主管客户经理 **/
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="managerIdName")
	private String managerId;

	/** 主管机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="managerBrIdName" )
	private String managerBrId;

	/** 操作类型 **/
	private String oprType;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;


	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}

	/**
	 * @return ContNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo == null ? null : contCnNo.trim();
	}

	/**
	 * @return ContCnNo
	 */
	public String getContCnNo() {
		return this.contCnNo;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}

	/**
	 * @return SurveySerno
	 */
	public String getSurveySerno() {
		return this.surveySerno;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}

	/**
	 * @return ReplyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

	/**
	 * @return CusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

	/**
	 * @return CusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}

	/**
	 * @return PrdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

	/**
	 * @return PrdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp == null ? null : prdTypeProp.trim();
	}

	/**
	 * @return PrdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}

	/**
	 * @return CertType
	 */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

	/**
	 * @return CertCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}

	/**
	 * @return Phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType == null ? null : bizType.trim();
	}

	/**
	 * @return BizType
	 */
	public String getBizType() {
		return this.bizType;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}

	/**
	 * @return LmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param especBizType
	 */
	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType == null ? null : especBizType.trim();
	}

	/**
	 * @return EspecBizType
	 */
	public String getEspecBizType() {
		return this.especBizType;
	}

	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}

	/**
	 * @return LoanPurp
	 */
	public String getLoanPurp() {
		return this.loanPurp;
	}

	/**
	 * @param loanPurpDesc
	 */
	public void setLoanPurpDesc(String loanPurpDesc) {
		this.loanPurpDesc = loanPurpDesc == null ? null : loanPurpDesc.trim();
	}

	/**
	 * @return LoanPurpDesc
	 */
	public String getLoanPurpDesc() {
		return this.loanPurpDesc;
	}

	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal == null ? null : loanModal.trim();
	}

	/**
	 * @return LoanModal
	 */
	public String getLoanModal() {
		return this.loanModal;
	}

	/**
	 * @param loanCha
	 */
	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha == null ? null : loanCha.trim();
	}

	/**
	 * @return LoanCha
	 */
	public String getLoanCha() {
		return this.loanCha;
	}

	/**
	 * @param isHasRefused
	 */
	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused == null ? null : isHasRefused.trim();
	}

	/**
	 * @return IsHasRefused
	 */
	public String getIsHasRefused() {
		return this.isHasRefused;
	}

	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}

	/**
	 * @return GuarWay
	 */
	public String getGuarWay() {
		return this.guarWay;
	}

	/**
	 * @param isCommonRqstr
	 */
	public void setIsCommonRqstr(String isCommonRqstr) {
		this.isCommonRqstr = isCommonRqstr == null ? null : isCommonRqstr.trim();
	}

	/**
	 * @return IsCommonRqstr
	 */
	public String getIsCommonRqstr() {
		return this.isCommonRqstr;
	}

	/**
	 * @param isCfirmPayWay
	 */
	public void setIsCfirmPayWay(String isCfirmPayWay) {
		this.isCfirmPayWay = isCfirmPayWay == null ? null : isCfirmPayWay.trim();
	}

	/**
	 * @return IsCfirmPayWay
	 */
	public String getIsCfirmPayWay() {
		return this.isCfirmPayWay;
	}

	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode == null ? null : payMode.trim();
	}

	/**
	 * @return PayMode
	 */
	public String getPayMode() {
		return this.payMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}

	/**
	 * @return CurType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType == null ? null : contType.trim();
	}

	/**
	 * @return ContType
	 */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}

	/**
	 * @return ContTerm
	 */
	public Integer getContTerm() {
		return this.contTerm;
	}

	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate == null ? null : contStartDate.trim();
	}

	/**
	 * @return ContStartDate
	 */
	public String getContStartDate() {
		return this.contStartDate;
	}

	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate == null ? null : contEndDate.trim();
	}

	/**
	 * @return ContEndDate
	 */
	public String getContEndDate() {
		return this.contEndDate;
	}

	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus == null ? null : contStatus.trim();
	}

	/**
	 * @return ContStatus
	 */
	public String getContStatus() {
		return this.contStatus;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return ContAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contBalance
	 */
	public void setContBalance(java.math.BigDecimal contBalance) {
		this.contBalance = contBalance;
	}

	/**
	 * @return ContBalance
	 */
	public java.math.BigDecimal getContBalance() {
		return this.contBalance;
	}

	/**
	 * @param contRate
	 */
	public void setContRate(java.math.BigDecimal contRate) {
		this.contRate = contRate;
	}

	/**
	 * @return ContRate
	 */
	public java.math.BigDecimal getContRate() {
		return this.contRate;
	}

	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate == null ? null : paperContSignDate.trim();
	}

	/**
	 * @return PaperContSignDate
	 */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}

	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode == null ? null : eiMode.trim();
	}

	/**
	 * @return EiMode
	 */
	public String getEiMode() {
		return this.eiMode;
	}

	/**
	 * @param eiModeExpl
	 */
	public void setEiModeExpl(String eiModeExpl) {
		this.eiModeExpl = eiModeExpl == null ? null : eiModeExpl.trim();
	}

	/**
	 * @return EiModeExpl
	 */
	public String getEiModeExpl() {
		return this.eiModeExpl;
	}

	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr == null ? null : addr.trim();
	}

	/**
	 * @return Addr
	 */
	public String getAddr() {
		return this.addr;
	}

	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax == null ? null : fax.trim();
	}

	/**
	 * @return Fax
	 */
	public String getFax() {
		return this.fax;
	}

	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman == null ? null : linkman.trim();
	}

	/**
	 * @return Linkman
	 */
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * @return Email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq == null ? null : qq.trim();
	}

	/**
	 * @return Qq
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat == null ? null : wechat.trim();
	}

	/**
	 * @return Wechat
	 */
	public String getWechat() {
		return this.wechat;
	}

	/**
	 * @param bankRole
	 */
	public void setBankRole(String bankRole) {
		this.bankRole = bankRole == null ? null : bankRole.trim();
	}

	/**
	 * @return BankRole
	 */
	public String getBankRole() {
		return this.bankRole;
	}

	/**
	 * @param bksyndicTotlAmt
	 */
	public void setBksyndicTotlAmt(java.math.BigDecimal bksyndicTotlAmt) {
		this.bksyndicTotlAmt = bksyndicTotlAmt;
	}

	/**
	 * @return BksyndicTotlAmt
	 */
	public java.math.BigDecimal getBksyndicTotlAmt() {
		return this.bksyndicTotlAmt;
	}

	/**
	 * @param bksyndicPaperContNo
	 */
	public void setBksyndicPaperContNo(String bksyndicPaperContNo) {
		this.bksyndicPaperContNo = bksyndicPaperContNo == null ? null : bksyndicPaperContNo.trim();
	}

	/**
	 * @return BksyndicPaperContNo
	 */
	public String getBksyndicPaperContNo() {
		return this.bksyndicPaperContNo;
	}

	/**
	 * @param repaySeq
	 */
	public void setRepaySeq(String repaySeq) {
		this.repaySeq = repaySeq == null ? null : repaySeq.trim();
	}

	/**
	 * @return RepaySeq
	 */
	public String getRepaySeq() {
		return this.repaySeq;
	}

	/**
	 * @param otherPhone
	 */
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone == null ? null : otherPhone.trim();
	}

	/**
	 * @return OtherPhone
	 */
	public String getOtherPhone() {
		return this.otherPhone;
	}

	/**
	 * @param signMode
	 */
	public void setSignMode(String signMode) {
		this.signMode = signMode == null ? null : signMode.trim();
	}

	/**
	 * @return SignMode
	 */
	public String getSignMode() {
		return this.signMode;
	}

	/**
	 * @param signState
	 */
	public void setSignState(String signState) {
		this.signState = signState == null ? null : signState.trim();
	}

	/**
	 * @return SignState
	 */
	public String getSignState() {
		return this.signState;
	}

	/**
	 * @param signChannel
	 */
	public void setSignChannel(String signChannel) {
		this.signChannel = signChannel == null ? null : signChannel.trim();
	}

	/**
	 * @return SignChannel
	 */
	public String getSignChannel() {
		return this.signChannel;
	}

	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour == null ? null : bailSour.trim();
	}

	/**
	 * @return BailSour
	 */
	public String getBailSour() {
		return this.bailSour;
	}

	/**
	 * @param bailExchangeRate
	 */
	public void setBailExchangeRate(java.math.BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}

	/**
	 * @return BailExchangeRate
	 */
	public java.math.BigDecimal getBailExchangeRate() {
		return this.bailExchangeRate;
	}

	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}

	/**
	 * @return BailPerc
	 */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}

	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType == null ? null : bailCurType.trim();
	}

	/**
	 * @return BailCurType
	 */
	public String getBailCurType() {
		return this.bailCurType;
	}

	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	/**
	 * @return BailAmt
	 */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}

	/**
	 * @param bailCvtCnyAmt
	 */
	public void setBailCvtCnyAmt(java.math.BigDecimal bailCvtCnyAmt) {
		this.bailCvtCnyAmt = bailCvtCnyAmt;
	}

	/**
	 * @return BailCvtCnyAmt
	 */
	public java.math.BigDecimal getBailCvtCnyAmt() {
		return this.bailCvtCnyAmt;
	}

	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}

	/**
	 * @return CvtCnyAmt
	 */
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}

	/**
	 * @return TermType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param appTerm
	 */
	public void setAppTerm(java.math.BigDecimal appTerm) {
		this.appTerm = appTerm;
	}

	/**
	 * @return AppTerm
	 */
	public java.math.BigDecimal getAppTerm() {
		return this.appTerm;
	}

	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType == null ? null : irAccordType.trim();
	}

	/**
	 * @return IrAccordType
	 */
	public String getIrAccordType() {
		return this.irAccordType;
	}

	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType == null ? null : irType.trim();
	}

	/**
	 * @return IrType
	 */
	public String getIrType() {
		return this.irType;
	}

	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

	/**
	 * @return RulingIr
	 */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}

	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}

	/**
	 * @return RulingIrM
	 */
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}

	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType == null ? null : loanRatType.trim();
	}

	/**
	 * @return LoanRatType
	 */
	public String getLoanRatType() {
		return this.loanRatType;
	}

	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType == null ? null : irAdjustType.trim();
	}

	/**
	 * @return IrAdjustType
	 */
	public String getIrAdjustType() {
		return this.irAdjustType;
	}

	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(java.math.BigDecimal irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}

	/**
	 * @return IrAdjustTerm
	 */
	public java.math.BigDecimal getIrAdjustTerm() {
		return this.irAdjustTerm;
	}

	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType == null ? null : praType.trim();
	}

	/**
	 * @return PraType
	 */
	public String getPraType() {
		return this.praType;
	}

	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType == null ? null : rateType.trim();
	}

	/**
	 * @return RateType
	 */
	public String getRateType() {
		return this.rateType;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval == null ? null : lprRateIntval.trim();
	}

	/**
	 * @return LprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	/**
	 * @return CurtLprRate
	 */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType == null ? null : irFloatType.trim();
	}

	/**
	 * @return IrFloatType
	 */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	/**
	 * @return IrFloatRate
	 */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return RateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return ExecRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param realityIrM
	 */
	public void setRealityIrM(java.math.BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}

	/**
	 * @return RealityIrM
	 */
	public java.math.BigDecimal getRealityIrM() {
		return this.realityIrM;
	}

	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}

	/**
	 * @return OverdueRatePefloat
	 */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}

	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}

	/**
	 * @return OverdueExecRate
	 */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}

	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}

	/**
	 * @return CiRatePefloat
	 */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}

	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}

	/**
	 * @return CiExecRate
	 */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}

	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

	/**
	 * @return DefaultRate
	 */
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}

	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}

	/**
	 * @return DefaultRateY
	 */
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}

	/**
	 * @param riskOpenAmt
	 */
	public void setRiskOpenAmt(java.math.BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}

	/**
	 * @return RiskOpenAmt
	 */
	public java.math.BigDecimal getRiskOpenAmt() {
		return this.riskOpenAmt;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}

	/**
	 * @return RepayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param stopPintTerm
	 */
	public void setStopPintTerm(String stopPintTerm) {
		this.stopPintTerm = stopPintTerm == null ? null : stopPintTerm.trim();
	}

	/**
	 * @return StopPintTerm
	 */
	public String getStopPintTerm() {
		return this.stopPintTerm;
	}

	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm == null ? null : repayTerm.trim();
	}

	/**
	 * @return RepayTerm
	 */
	public String getRepayTerm() {
		return this.repayTerm;
	}

	/**
	 * @param repaySpace
	 */
	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace == null ? null : repaySpace.trim();
	}

	/**
	 * @return RepaySpace
	 */
	public String getRepaySpace() {
		return this.repaySpace;
	}

	/**
	 * @param repayRule
	 */
	public void setRepayRule(String repayRule) {
		this.repayRule = repayRule == null ? null : repayRule.trim();
	}

	/**
	 * @return RepayRule
	 */
	public String getRepayRule() {
		return this.repayRule;
	}

	/**
	 * @param repayDtType
	 */
	public void setRepayDtType(String repayDtType) {
		this.repayDtType = repayDtType == null ? null : repayDtType.trim();
	}

	/**
	 * @return RepayDtType
	 */
	public String getRepayDtType() {
		return this.repayDtType;
	}

	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate == null ? null : repayDate.trim();
	}

	/**
	 * @return RepayDate
	 */
	public String getRepayDate() {
		return this.repayDate;
	}

	/**
	 * @param capGraperType
	 */
	public void setCapGraperType(String capGraperType) {
		this.capGraperType = capGraperType == null ? null : capGraperType.trim();
	}

	/**
	 * @return CapGraperType
	 */
	public String getCapGraperType() {
		return this.capGraperType;
	}

	/**
	 * @param capGraperDay
	 */
	public void setCapGraperDay(String capGraperDay) {
		this.capGraperDay = capGraperDay == null ? null : capGraperDay.trim();
	}

	/**
	 * @return CapGraperDay
	 */
	public String getCapGraperDay() {
		return this.capGraperDay;
	}

	/**
	 * @param intGraperType
	 */
	public void setIntGraperType(String intGraperType) {
		this.intGraperType = intGraperType == null ? null : intGraperType.trim();
	}

	/**
	 * @return IntGraperType
	 */
	public String getIntGraperType() {
		return this.intGraperType;
	}

	/**
	 * @param intGraperDay
	 */
	public void setIntGraperDay(String intGraperDay) {
		this.intGraperDay = intGraperDay == null ? null : intGraperDay.trim();
	}

	/**
	 * @return IntGraperDay
	 */
	public String getIntGraperDay() {
		return this.intGraperDay;
	}

	/**
	 * @param deductDeduType
	 */
	public void setDeductDeduType(String deductDeduType) {
		this.deductDeduType = deductDeduType == null ? null : deductDeduType.trim();
	}

	/**
	 * @return DeductDeduType
	 */
	public String getDeductDeduType() {
		return this.deductDeduType;
	}

	/**
	 * @param repayFreType
	 */
	public void setRepayFreType(String repayFreType) {
		this.repayFreType = repayFreType == null ? null : repayFreType.trim();
	}

	/**
	 * @return RepayFreType
	 */
	public String getRepayFreType() {
		return this.repayFreType;
	}

	/**
	 * @param repayFre
	 */
	public void setRepayFre(String repayFre) {
		this.repayFre = repayFre == null ? null : repayFre.trim();
	}

	/**
	 * @return RepayFre
	 */
	public String getRepayFre() {
		return this.repayFre;
	}

	/**
	 * @param liquFreeTime
	 */
	public void setLiquFreeTime(Integer liquFreeTime) {
		this.liquFreeTime = liquFreeTime;
	}

	/**
	 * @return LiquFreeTime
	 */
	public Integer getLiquFreeTime() {
		return this.liquFreeTime;
	}

	/**
	 * @param subType
	 */
	public void setSubType(String subType) {
		this.subType = subType == null ? null : subType.trim();
	}

	/**
	 * @return SubType
	 */
	public String getSubType() {
		return this.subType;
	}

	/**
	 * @param reserveTerm
	 */
	public void setReserveTerm(Integer reserveTerm) {
		this.reserveTerm = reserveTerm;
	}

	/**
	 * @return ReserveTerm
	 */
	public Integer getReserveTerm() {
		return this.reserveTerm;
	}

	/**
	 * @param calTerm
	 */
	public void setCalTerm(Integer calTerm) {
		this.calTerm = calTerm;
	}

	/**
	 * @return CalTerm
	 */
	public Integer getCalTerm() {
		return this.calTerm;
	}

	/**
	 * @param reserveAmt
	 */
	public void setReserveAmt(java.math.BigDecimal reserveAmt) {
		this.reserveAmt = reserveAmt;
	}

	/**
	 * @return ReserveAmt
	 */
	public java.math.BigDecimal getReserveAmt() {
		return this.reserveAmt;
	}

	/**
	 * @param repayTermOne
	 */
	public void setRepayTermOne(Integer repayTermOne) {
		this.repayTermOne = repayTermOne;
	}

	/**
	 * @return RepayTermOne
	 */
	public Integer getRepayTermOne() {
		return this.repayTermOne;
	}

	/**
	 * @param repayAmtOne
	 */
	public void setRepayAmtOne(java.math.BigDecimal repayAmtOne) {
		this.repayAmtOne = repayAmtOne;
	}

	/**
	 * @return RepayAmtOne
	 */
	public java.math.BigDecimal getRepayAmtOne() {
		return this.repayAmtOne;
	}

	/**
	 * @param repayTermTwo
	 */
	public void setRepayTermTwo(Integer repayTermTwo) {
		this.repayTermTwo = repayTermTwo;
	}

	/**
	 * @return RepayTermTwo
	 */
	public Integer getRepayTermTwo() {
		return this.repayTermTwo;
	}

	/**
	 * @param repayAmtTwo
	 */
	public void setRepayAmtTwo(java.math.BigDecimal repayAmtTwo) {
		this.repayAmtTwo = repayAmtTwo;
	}

	/**
	 * @return RepayAmtTwo
	 */
	public java.math.BigDecimal getRepayAmtTwo() {
		return this.repayAmtTwo;
	}

	/**
	 * @param rateSelType
	 */
	public void setRateSelType(String rateSelType) {
		this.rateSelType = rateSelType == null ? null : rateSelType.trim();
	}

	/**
	 * @return RateSelType
	 */
	public String getRateSelType() {
		return this.rateSelType;
	}

	/**
	 * @param sbsyMode
	 */
	public void setSbsyMode(String sbsyMode) {
		this.sbsyMode = sbsyMode == null ? null : sbsyMode.trim();
	}

	/**
	 * @return SbsyMode
	 */
	public String getSbsyMode() {
		return this.sbsyMode;
	}

	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

	/**
	 * @return SbsyPerc
	 */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}

	/**
	 * @param sbsyAmt
	 */
	public void setSbsyAmt(java.math.BigDecimal sbsyAmt) {
		this.sbsyAmt = sbsyAmt;
	}

	/**
	 * @return SbsyAmt
	 */
	public java.math.BigDecimal getSbsyAmt() {
		return this.sbsyAmt;
	}

	/**
	 * @param sbsyUnitName
	 */
	public void setSbsyUnitName(String sbsyUnitName) {
		this.sbsyUnitName = sbsyUnitName == null ? null : sbsyUnitName.trim();
	}

	/**
	 * @return SbsyUnitName
	 */
	public String getSbsyUnitName() {
		return this.sbsyUnitName;
	}

	/**
	 * @param sbsyAcct
	 */
	public void setSbsyAcct(String sbsyAcct) {
		this.sbsyAcct = sbsyAcct == null ? null : sbsyAcct.trim();
	}

	/**
	 * @return SbsyAcct
	 */
	public String getSbsyAcct() {
		return this.sbsyAcct;
	}

	/**
	 * @param sbsyAcctName
	 */
	public void setSbsyAcctName(String sbsyAcctName) {
		this.sbsyAcctName = sbsyAcctName == null ? null : sbsyAcctName.trim();
	}

	/**
	 * @return SbsyAcctName
	 */
	public String getSbsyAcctName() {
		return this.sbsyAcctName;
	}

	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass == null ? null : fiveClass.trim();
	}

	/**
	 * @return FiveClass
	 */
	public String getFiveClass() {
		return this.fiveClass;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer == null ? null : loanTer.trim();
	}

	/**
	 * @return LoanTer
	 */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param comUpIndtify
	 */
	public void setComUpIndtify(String comUpIndtify) {
		this.comUpIndtify = comUpIndtify == null ? null : comUpIndtify.trim();
	}

	/**
	 * @return ComUpIndtify
	 */
	public String getComUpIndtify() {
		return this.comUpIndtify;
	}

	/**
	 * @param strategyNewLoan
	 */
	public void setStrategyNewLoan(String strategyNewLoan) {
		this.strategyNewLoan = strategyNewLoan == null ? null : strategyNewLoan.trim();
	}

	/**
	 * @return StrategyNewLoan
	 */
	public String getStrategyNewLoan() {
		return this.strategyNewLoan;
	}

	/**
	 * @param isCulEstate
	 */
	public void setIsCulEstate(String isCulEstate) {
		this.isCulEstate = isCulEstate == null ? null : isCulEstate.trim();
	}

	/**
	 * @return IsCulEstate
	 */
	public String getIsCulEstate() {
		return this.isCulEstate;
	}

	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType == null ? null : loanType.trim();
	}

	/**
	 * @return LoanType
	 */
	public String getLoanType() {
		return this.loanType;
	}

	/**
	 * @param estateAdjustType
	 */
	public void setEstateAdjustType(String estateAdjustType) {
		this.estateAdjustType = estateAdjustType == null ? null : estateAdjustType.trim();
	}

	/**
	 * @return EstateAdjustType
	 */
	public String getEstateAdjustType() {
		return this.estateAdjustType;
	}

	/**
	 * @param newPrdLoan
	 */
	public void setNewPrdLoan(String newPrdLoan) {
		this.newPrdLoan = newPrdLoan == null ? null : newPrdLoan.trim();
	}

	/**
	 * @return NewPrdLoan
	 */
	public String getNewPrdLoan() {
		return this.newPrdLoan;
	}

	/**
	 * @param repaySour
	 */
	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour == null ? null : repaySour.trim();
	}

	/**
	 * @return RepaySour
	 */
	public String getRepaySour() {
		return this.repaySour;
	}

	/**
	 * @param isAuthorize
	 */
	public void setIsAuthorize(String isAuthorize) {
		this.isAuthorize = isAuthorize == null ? null : isAuthorize.trim();
	}

	/**
	 * @return IsAuthorize
	 */
	public String getIsAuthorize() {
		return this.isAuthorize;
	}

	/**
	 * @param authedName
	 */
	public void setAuthedName(String authedName) {
		this.authedName = authedName == null ? null : authedName.trim();
	}

	/**
	 * @return AuthedName
	 */
	public String getAuthedName() {
		return this.authedName;
	}

	/**
	 * @param consignorCertType
	 */
	public void setConsignorCertType(String consignorCertType) {
		this.consignorCertType = consignorCertType == null ? null : consignorCertType.trim();
	}

	/**
	 * @return ConsignorCertType
	 */
	public String getConsignorCertType() {
		return this.consignorCertType;
	}

	/**
	 * @param consignorCertCode
	 */
	public void setConsignorCertCode(String consignorCertCode) {
		this.consignorCertCode = consignorCertCode == null ? null : consignorCertCode.trim();
	}

	/**
	 * @return ConsignorCertCode
	 */
	public String getConsignorCertCode() {
		return this.consignorCertCode;
	}

	/**
	 * @param authedTelNo
	 */
	public void setAuthedTelNo(String authedTelNo) {
		this.authedTelNo = authedTelNo == null ? null : authedTelNo.trim();
	}

	/**
	 * @return AuthedTelNo
	 */
	public String getAuthedTelNo() {
		return this.authedTelNo;
	}

	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate == null ? null : signDate.trim();
	}

	/**
	 * @return SignDate
	 */
	public String getSignDate() {
		return this.signDate;
	}

	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate == null ? null : logoutDate.trim();
	}

	/**
	 * @return LogoutDate
	 */
	public String getLogoutDate() {
		return this.logoutDate;
	}

	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour == null ? null : chnlSour.trim();
	}

	/**
	 * @return ChnlSour
	 */
	public String getChnlSour() {
		return this.chnlSour;
	}

	/**
	 * @param billDispupeOpt
	 */
	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt == null ? null : billDispupeOpt.trim();
	}

	/**
	 * @return BillDispupeOpt
	 */
	public String getBillDispupeOpt() {
		return this.billDispupeOpt;
	}

	/**
	 * @param courtAddr
	 */
	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr == null ? null : courtAddr.trim();
	}

	/**
	 * @return CourtAddr
	 */
	public String getCourtAddr() {
		return this.courtAddr;
	}

	/**
	 * @param arbitrateBch
	 */
	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch == null ? null : arbitrateBch.trim();
	}

	/**
	 * @return ArbitrateBch
	 */
	public String getArbitrateBch() {
		return this.arbitrateBch;
	}

	/**
	 * @param arbitrateAddr
	 */
	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr == null ? null : arbitrateAddr.trim();
	}

	/**
	 * @return ArbitrateAddr
	 */
	public String getArbitrateAddr() {
		return this.arbitrateAddr;
	}

	/**
	 * @param purpAnaly
	 */
	public void setPurpAnaly(String purpAnaly) {
		this.purpAnaly = purpAnaly == null ? null : purpAnaly.trim();
	}

	/**
	 * @return PurpAnaly
	 */
	public String getPurpAnaly() {
		return this.purpAnaly;
	}

	/**
	 * @param crossChkDetailAnaly
	 */
	public void setCrossChkDetailAnaly(String crossChkDetailAnaly) {
		this.crossChkDetailAnaly = crossChkDetailAnaly == null ? null : crossChkDetailAnaly.trim();
	}

	/**
	 * @return CrossChkDetailAnaly
	 */
	public String getCrossChkDetailAnaly() {
		return this.crossChkDetailAnaly;
	}

	/**
	 * @param inveConclu
	 */
	public void setInveConclu(String inveConclu) {
		this.inveConclu = inveConclu == null ? null : inveConclu.trim();
	}

	/**
	 * @return InveConclu
	 */
	public String getInveConclu() {
		return this.inveConclu;
	}

	/**
	 * @param otherOpt
	 */
	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt == null ? null : otherOpt.trim();
	}

	/**
	 * @return OtherOpt
	 */
	public String getOtherOpt() {
		return this.otherOpt;
	}

	/**
	 * @param contQnt
	 */
	public void setContQnt(java.math.BigDecimal contQnt) {
		this.contQnt = contQnt;
	}

	/**
	 * @return ContQnt
	 */
	public java.math.BigDecimal getContQnt() {
		return this.contQnt;
	}

	/**
	 * @param pundContNo
	 */
	public void setPundContNo(String pundContNo) {
		this.pundContNo = pundContNo == null ? null : pundContNo.trim();
	}

	/**
	 * @return PundContNo
	 */
	public String getPundContNo() {
		return this.pundContNo;
	}

	/**
	 * @param spplClause
	 */
	public void setSpplClause(String spplClause) {
		this.spplClause = spplClause == null ? null : spplClause.trim();
	}

	/**
	 * @return SpplClause
	 */
	public String getSpplClause() {
		return this.spplClause;
	}

	/**
	 * @param signAddr
	 */
	public void setSignAddr(String signAddr) {
		this.signAddr = signAddr == null ? null : signAddr.trim();
	}

	/**
	 * @return SignAddr
	 */
	public String getSignAddr() {
		return this.signAddr;
	}

	/**
	 * @param busiNetwork
	 */
	public void setBusiNetwork(String busiNetwork) {
		this.busiNetwork = busiNetwork == null ? null : busiNetwork.trim();
	}

	/**
	 * @return BusiNetwork
	 */
	public String getBusiNetwork() {
		return this.busiNetwork;
	}

	/**
	 * @param mainBusiPalce
	 */
	public void setMainBusiPalce(String mainBusiPalce) {
		this.mainBusiPalce = mainBusiPalce == null ? null : mainBusiPalce.trim();
	}

	/**
	 * @return MainBusiPalce
	 */
	public String getMainBusiPalce() {
		return this.mainBusiPalce;
	}

	/**
	 * @param contTemplate
	 */
	public void setContTemplate(String contTemplate) {
		this.contTemplate = contTemplate == null ? null : contTemplate.trim();
	}

	/**
	 * @return ContTemplate
	 */
	public String getContTemplate() {
		return this.contTemplate;
	}

	/**
	 * @param contPrintNum
	 */
	public void setContPrintNum(java.math.BigDecimal contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	/**
	 * @return ContPrintNum
	 */
	public java.math.BigDecimal getContPrintNum() {
		return this.contPrintNum;
	}

	/**
	 * @param signApproveStatus
	 */
	public void setSignApproveStatus(String signApproveStatus) {
		this.signApproveStatus = signApproveStatus == null ? null : signApproveStatus.trim();
	}

	/**
	 * @return SignApproveStatus
	 */
	public String getSignApproveStatus() {
		return this.signApproveStatus;
	}

	/**
	 * @param team
	 */
	public void setTeam(String team) {
		this.team = team == null ? null : team.trim();
	}

	/**
	 * @return Team
	 */
	public String getTeam() {
		return this.team;
	}

	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed == null ? null : otherAgreed.trim();
	}

	/**
	 * @return OtherAgreed
	 */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}

	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName == null ? null : acctsvcrName.trim();
	}

	/**
	 * @return AcctsvcrName
	 */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}

	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno == null ? null : loanPayoutAccno.trim();
	}

	/**
	 * @return LoanPayoutAccno
	 */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}

	/**
	 * @param loanPayoutAccName
	 */
	public void setLoanPayoutAccName(String loanPayoutAccName) {
		this.loanPayoutAccName = loanPayoutAccName == null ? null : loanPayoutAccName.trim();
	}

	/**
	 * @return LoanPayoutAccName
	 */
	public String getLoanPayoutAccName() {
		return this.loanPayoutAccName;
	}

	/**
	 * @param contMode
	 */
	public void setContMode(String contMode) {
		this.contMode = contMode == null ? null : contMode.trim();
	}

	/**
	 * @return ContMode
	 */
	public String getContMode() {
		return this.contMode;
	}

	/**
	 * @param isOnlineDraw
	 */
	public void setIsOnlineDraw(String isOnlineDraw) {
		this.isOnlineDraw = isOnlineDraw == null ? null : isOnlineDraw.trim();
	}

	/**
	 * @return IsOnlineDraw
	 */
	public String getIsOnlineDraw() {
		return this.isOnlineDraw;
	}

	/**
	 * @param ctrBeginFlag
	 */
	public void setCtrBeginFlag(String ctrBeginFlag) {
		this.ctrBeginFlag = ctrBeginFlag == null ? null : ctrBeginFlag.trim();
	}

	/**
	 * @return CtrBeginFlag
	 */
	public String getCtrBeginFlag() {
		return this.ctrBeginFlag;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}

	/**
	 * @return ApproveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param highAvlAmt
	 */
	public void setHighAvlAmt(java.math.BigDecimal highAvlAmt) {
		this.highAvlAmt = highAvlAmt;
	}

	/**
	 * @return HighAvlAmt
	 */
	public java.math.BigDecimal getHighAvlAmt() {
		return this.highAvlAmt;
	}

	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew == null ? null : isRenew.trim();
	}

	/**
	 * @return IsRenew
	 */
	public String getIsRenew() {
		return this.isRenew;
	}

	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo == null ? null : origiContNo.trim();
	}

	/**
	 * @return OrigiContNo
	 */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt == null ? null : isUtilLmt.trim();
	}

	/**
	 * @return IsUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld == null ? null : isOlPld.trim();
	}

	/**
	 * @return IsOlPld
	 */
	public String getIsOlPld() {
		return this.isOlPld;
	}

	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd == null ? null : beforehandInd.trim();
	}

	/**
	 * @return BeforehandInd
	 */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}

	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal == null ? null : isESeal.trim();
	}

	/**
	 * @return IsESeal
	 */
	public String getIsESeal() {
		return this.isESeal;
	}

	/**
	 * @param doubleRecordNo
	 */
	public void setDoubleRecordNo(String doubleRecordNo) {
		this.doubleRecordNo = doubleRecordNo == null ? null : doubleRecordNo.trim();
	}

	/**
	 * @return DoubleRecordNo
	 */
	public String getDoubleRecordNo() {
		return this.doubleRecordNo;
	}

	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay == null ? null : loanRateAdjDay.trim();
	}

	/**
	 * @return LoanRateAdjDay
	 */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}

	/**
	 * @param drawMode
	 */
	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode == null ? null : drawMode.trim();
	}

	/**
	 * @return DrawMode
	 */
	public String getDrawMode() {
		return this.drawMode;
	}

	/**
	 * @param dayLimit
	 */
	public void setDayLimit(String dayLimit) {
		this.dayLimit = dayLimit == null ? null : dayLimit.trim();
	}

	/**
	 * @return DayLimit
	 */
	public String getDayLimit() {
		return this.dayLimit;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}

	/**
	 * @return BelgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel == null ? null : debtLevel.trim();
	}

	/**
	 * @return DebtLevel
	 */
	public String getDebtLevel() {
		return this.debtLevel;
	}

	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}

	/**
	 * @return Lgd
	 */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}

	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}

	/**
	 * @return Ead
	 */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}

	/**
	 * @param otherLoanPurp
	 */
	public void setOtherLoanPurp(String otherLoanPurp) {
		this.otherLoanPurp = otherLoanPurp == null ? null : otherLoanPurp.trim();
	}

	/**
	 * @return OtherLoanPurp
	 */
	public String getOtherLoanPurp() {
		return this.otherLoanPurp;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return CreateTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return UpdateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}
