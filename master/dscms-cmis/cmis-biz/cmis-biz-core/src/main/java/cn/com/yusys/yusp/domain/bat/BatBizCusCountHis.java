/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizCusCountHis
 * @类描述: bat_biz_cus_count_his数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_biz_cus_count_his")
public class BatBizCusCountHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = false, length = 30)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 30)
	private String managerBrId;
	
	/** 当前管户融资户数 **/
	@Column(name = "TOTAL_CUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer totalCusCount;
	
	/** 其中：公司类融资户数 **/
	@Column(name = "CUS_COM_COUNT", unique = false, nullable = true, length = 10)
	private Integer cusComCount;
	
	/** 其中：小企业类融资户数 **/
	@Column(name = "SM_CUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer smCusCount;
	
	/** 其中：贷款客户数 **/
	@Column(name = "LOAN_COUNT", unique = false, nullable = true, length = 10)
	private Integer loanCount;
	
	/** 其中：非贷款客户数 **/
	@Column(name = "OHTER_LOAN_COUNT", unique = false, nullable = true, length = 10)
	private Integer ohterLoanCount;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param totalCusCount
	 */
	public void setTotalCusCount(Integer totalCusCount) {
		this.totalCusCount = totalCusCount;
	}
	
    /**
     * @return totalCusCount
     */
	public Integer getTotalCusCount() {
		return this.totalCusCount;
	}
	
	/**
	 * @param cusComCount
	 */
	public void setCusComCount(Integer cusComCount) {
		this.cusComCount = cusComCount;
	}
	
    /**
     * @return cusComCount
     */
	public Integer getCusComCount() {
		return this.cusComCount;
	}
	
	/**
	 * @param smCusCount
	 */
	public void setSmCusCount(Integer smCusCount) {
		this.smCusCount = smCusCount;
	}
	
    /**
     * @return smCusCount
     */
	public Integer getSmCusCount() {
		return this.smCusCount;
	}
	
	/**
	 * @param loanCount
	 */
	public void setLoanCount(Integer loanCount) {
		this.loanCount = loanCount;
	}
	
    /**
     * @return loanCount
     */
	public Integer getLoanCount() {
		return this.loanCount;
	}
	
	/**
	 * @param ohterLoanCount
	 */
	public void setOhterLoanCount(Integer ohterLoanCount) {
		this.ohterLoanCount = ohterLoanCount;
	}
	
    /**
     * @return ohterLoanCount
     */
	public Integer getOhterLoanCount() {
		return this.ohterLoanCount;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}