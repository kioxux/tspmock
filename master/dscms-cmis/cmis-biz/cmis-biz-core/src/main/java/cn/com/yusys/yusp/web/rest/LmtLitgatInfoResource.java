/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.dto.YqdLigatAndDebitDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtLitgatInfo;
import cn.com.yusys.yusp.service.LmtLitgatInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtLitgatInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-17 22:48:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "涉诉信息")
@RequestMapping("/api/lmtlitgatinfo")
public class LmtLitgatInfoResource {
    @Autowired
    private LmtLitgatInfoService lmtLitgatInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtLitgatInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtLitgatInfo> list = lmtLitgatInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtLitgatInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtLitgatInfo>> index(QueryModel queryModel) {
        List<LmtLitgatInfo> list = lmtLitgatInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtLitgatInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtLitgatInfo> show(@PathVariable("pkId") String pkId) {
        LmtLitgatInfo lmtLitgatInfo = lmtLitgatInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtLitgatInfo>(lmtLitgatInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtLitgatInfo> create(@RequestBody LmtLitgatInfo lmtLitgatInfo) throws URISyntaxException {
        lmtLitgatInfoService.insert(lmtLitgatInfo);
        return new ResultDto<LmtLitgatInfo>(lmtLitgatInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtLitgatInfo lmtLitgatInfo) throws URISyntaxException {
        int result = lmtLitgatInfoService.update(lmtLitgatInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtLitgatInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtLitgatInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtLitgatInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/22 15:55
     * @version 1.0.0
     * @desc  查询黑名单信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查询黑名单信息")
    @PostMapping("/hhmdkh")
    protected ResultDto<YqdLigatAndDebitDto> hhmdkh(@RequestBody LmtLitgatInfo lmtLitgatInfo) {
        return  lmtLitgatInfoService.hhmdkh(lmtLitgatInfo.getSurveySerno());
    }

    /**
     * @author hubp
     * @date 2021/5/25 15:20
     * @version 1.0.0
     * @desc    查询涉诉信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查询涉诉信息")
    @PostMapping("/qyssxx")
    protected ResultDto<List> qyssxx(@RequestBody LmtLitgatInfo lmtLitgatInfo) {
        List list = lmtLitgatInfoService.qyssxx(lmtLitgatInfo.getSurveySerno());
        return new ResultDto<List>(list);
    }
}
