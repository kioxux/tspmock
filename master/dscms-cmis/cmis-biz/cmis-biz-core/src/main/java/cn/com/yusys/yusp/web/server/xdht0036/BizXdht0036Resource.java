package cn.com.yusys.yusp.web.server.xdht0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0036.req.Xdht0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0036.resp.Xdht0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0036.Xdht0036Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0036:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0036Resource.class);

    @Resource
    private Xdht0036Service xdht0036Service;

    /**
     * 交易码：xdht0036
     * 交易描述：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
     *
     * @param xdht0036DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览")
    @PostMapping("/xdht0036")
    protected @ResponseBody
    ResultDto<Xdht0036DataRespDto> xdht0036(@Validated @RequestBody Xdht0036DataReqDto xdht0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataReqDto));
        Xdht0036DataRespDto xdht0036DataRespDto = new Xdht0036DataRespDto();// 响应Dto:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
        ResultDto<Xdht0036DataRespDto> xdht0036DataResultDto = new ResultDto<>();
        String cusId = xdht0036DataReqDto.getCusId();//客户号
        String loanStartDate = xdht0036DataReqDto.getLoanStartDate();//贷款起始日
        String managerId = xdht0036DataReqDto.getManagerId();//管户客户经理编号
        try {
            // 从xdht0036DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataReqDto));
            xdht0036DataRespDto = xdht0036Service.queryContStatusByCertCode(xdht0036DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataRespDto));
            // 封装xdht0036DataResultDto中正确的返回码和返回信息
            xdht0036DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0036DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, e.getMessage());
            // 封装xdht0036DataResultDto中异常返回码和返回信息
            xdht0036DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0036DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0036DataRespDto到xdht0036DataResultDto中
        xdht0036DataResultDto.setData(xdht0036DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataResultDto));
        return xdht0036DataResultDto;
    }
}
