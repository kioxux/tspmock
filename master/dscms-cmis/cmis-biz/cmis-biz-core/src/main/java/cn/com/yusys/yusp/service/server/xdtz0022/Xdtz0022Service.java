package cn.com.yusys.yusp.service.server.xdtz0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.dto.server.xdtz0022.req.Xdtz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.resp.Xdtz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.repository.mapper.AccTfLocMapper;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class Xdtz0022Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0022Service.class);

    @Autowired
    private AccTfLocMapper accTfLocMapper;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;


    /**
     * 交易码：Xdtz0022
     * 交易描述：保证金台账入账
     *
     * @param xdtz0022DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0022DataRespDto xdtz0022(Xdtz0022DataReqDto xdtz0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataReqDto));
        //定义返回信息
        Xdtz0022DataRespDto xdtz0022DataRespDto = new Xdtz0022DataRespDto();
        //得到请求字段
        String billNo = xdtz0022DataReqDto.getBillNo();//借据号
        String contNo = xdtz0022DataReqDto.getContNo();//合同号
        String busiCurType = xdtz0022DataReqDto.getBusiCurType();//业务币种
        BigDecimal busiBal = xdtz0022DataReqDto.getBusiBal().compareTo(BigDecimal.ZERO) > 0 ? xdtz0022DataReqDto.getBusiBal() : BigDecimal.ZERO;//业务余额
        BigDecimal applyAmt = xdtz0022DataReqDto.getApplyAmt();//申请金额
        BigDecimal bailAmt = xdtz0022DataReqDto.getBailAmt();//保证金金额
        BigDecimal exchgRate = xdtz0022DataReqDto.getExchgRate();//汇率
        String bizType = xdtz0022DataReqDto.getBizType();//业务品种
        //业务处理开始
        try {
            //设置查询参数
            Map queryMap = new HashMap();
            queryMap.put("exchgRate", exchgRate);//汇率
            queryMap.put("busiCurType", busiCurType);//业务币种
            queryMap.put("applyAmt", applyAmt);//申请金额
            queryMap.put("busiBal", busiBal);//业务余额
            queryMap.put("bailAmt", bailAmt);//保证金金额
            queryMap.put("billNo", billNo);//借据号
            //信用证台账
            if("1".equals(bizType)){
                //根据借据编号更新保证金额度

                AccTfLoc accTfLoc = accTfLocMapper.selectByBillNo(billNo);
                if (Objects.isNull(accTfLoc)) {
                    xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0022DataRespDto.setOpMsg("根据借据编号未查到台账信息！");
                } else {
                    BigDecimal cvtCnyAmt = busiBal.multiply(exchgRate).setScale(2, BigDecimal.ROUND_HALF_UP);//折算人民币金额
                    accTfLoc.setOcerAmt(applyAmt);//开证金额
                    accTfLoc.setCreditBal(busiBal);//开证余额
                    accTfLoc.setCvtCnyAmt(cvtCnyAmt);//折算人民币金额
                    if (Objects.equals("21", accTfLoc.getGuarMode()) || Objects.equals("60", accTfLoc.getGuarMode())) {
                        //低风险为0
                        accTfLoc.setSpacBal(BigDecimal.ZERO);//敞口余额
                        accTfLoc.setCvtCnySpac(BigDecimal.ZERO);//折算人民币敞口
                    } else {
                        BigDecimal spacBal = busiBal.subtract(bailAmt.divide(exchgRate, 2, BigDecimal.ROUND_HALF_UP));//敞口余额
                        BigDecimal cvtCnySpac = cvtCnyAmt.subtract(bailAmt);//折算人民币敞口
                        accTfLoc.setSpacBal(spacBal);//敞口余额
                        accTfLoc.setCvtCnySpac(cvtCnySpac.compareTo(BigDecimal.ZERO) > 0 ? cvtCnySpac : BigDecimal.ZERO);//折算人民币敞口
                    }
                    int i = accTfLocMapper.updateByPrimaryKeySelective(accTfLoc);
                    if(i>0){
                        xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
                        xdtz0022DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
                    }else{
                        xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0022DataRespDto.setOpMsg("数据未更新成功，借据对应台账信息不存在");
                    }
                }
                //保函
            }else if("2".equals(bizType)){
                BigDecimal cvtCnyAmt = busiBal.multiply(exchgRate).setScale(2, BigDecimal.ROUND_HALF_UP);//折算人民币金额
                AccCvrs accCvrs = accCvrsMapper.selectByBillno(billNo);
                if(Objects.isNull(accCvrs)){
                    xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0022DataRespDto.setOpMsg("根据借据编号未查到台账信息！");
                }else{
                    //根据借据编号更新保证金额度
                    accCvrs.setGuarantAmt(busiBal);//保函余额
                    if (Objects.equals("21", accCvrs.getGuarMode()) || Objects.equals("60", accCvrs.getGuarMode())) {
                        //低风险为0
                        accCvrs.setSpacBal(BigDecimal.ZERO);//敞口余额
                        accCvrs.setExchangeRmbSpac(BigDecimal.ZERO);//折算人民币敞口
                    } else {
                        BigDecimal spacBal = busiBal.subtract(bailAmt.divide(exchgRate, 2, BigDecimal.ROUND_HALF_UP));//敞口余额
                        BigDecimal cvtCnySpac = cvtCnyAmt.subtract(bailAmt);//折算人民币敞口
                        accCvrs.setSpacBal(spacBal);//敞口余额
                        accCvrs.setExchangeRmbSpac(cvtCnySpac.compareTo(BigDecimal.ZERO) > 0 ? cvtCnySpac : BigDecimal.ZERO);//折算人民币敞口
                    }
                    accCvrs.setBailCvtCnyAmt(bailAmt);//保证金折算人民币金额
                    int i = accCvrsMapper.updateByPrimaryKeySelective(accCvrs);
                    if(i>0){
                        xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
                        xdtz0022DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
                    }else{
                        xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0022DataRespDto.setOpMsg("数据未更新成功，借据对应台账信息不存在");
                    }
                }

            }else{
                xdtz0022DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                xdtz0022DataRespDto.setOpMsg("非信用证与保函！");
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataRespDto));
        return xdtz0022DataRespDto;
    }
}