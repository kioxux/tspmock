package cn.com.yusys.yusp.web.server.xdtz0037;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0037.req.Xdtz0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.resp.Xdtz0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0037.Xdtz0037Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:无还本续贷额度状态更新
 *
 * @author zhangpeng
 * @version 1.0
 */
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0037Resource.class);

    @Autowired
    private Xdtz0037Service xdtz0037Service;

    /**
     * 交易码：xdtz0037
     * 交易描述：无还本续贷额度状态更新
     *
     * @param xdtz0037DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdtz0037")
    protected @ResponseBody
    ResultDto<Xdtz0037DataRespDto> xdtz0037(@Validated @RequestBody Xdtz0037DataReqDto xdtz0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataReqDto));
        Xdtz0037DataRespDto xdtz0037DataRespDto = new Xdtz0037DataRespDto();// 响应Dto:无还本续贷额度状态更新
        ResultDto<Xdtz0037DataRespDto> xdtz0037DataResultDto = new ResultDto<>();
        // 从xdtz0037DataReqDto获取业务值进行业务逻辑处理
        try {
            String surveySerno = xdtz0037DataReqDto.getSurveySerno();//客户调查主键
            //请求字段校验
            if (StringUtil.isNotEmpty(surveySerno)) {
                // 调用XXXXXService层开始
                logger.info("**************************调用Xddb0037Service*开始*START*************************");
                xdtz0037DataRespDto = xdtz0037Service.queryLoanCont(xdtz0037DataReqDto);
                // 调用XXXXXService层结束
                // 封装xdtz0037DataResultDto中正确的返回码和返回信息
                xdtz0037DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdtz0037DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {//请求字段为空
                logger.info("***********************请求字段为空,校验失败***********************************");
                xdtz0037DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdtz0037DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, e.getMessage());
            // 封装xdtz0037DataResultDto中异常返回码和返回信息
            xdtz0037DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0037DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0037DataRespDto到xdtz0037DataResultDto中
        xdtz0037DataResultDto.setData(xdtz0037DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataResultDto));
        return xdtz0037DataResultDto;
    }
}
