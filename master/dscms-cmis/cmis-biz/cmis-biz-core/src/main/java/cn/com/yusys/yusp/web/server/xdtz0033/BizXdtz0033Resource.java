package cn.com.yusys.yusp.web.server.xdtz0033;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0033.req.Xdtz0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.resp.Xdtz0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0033.Xdtz0033Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0033:查询客户所担保的行内贷款五级分类非正常状态件数")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0033Resource.class);

    @Autowired
    private Xdtz0033Service xdtz0033Service;

    /**
     * 交易码：xdtz0033
     * 交易描述：查询客户所担保的行内贷款五级分类非正常状态件数
     *
     * @param xdtz0033DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0033:查询客户所担保的行内贷款五级分类非正常状态件数")
    @PostMapping("/xdtz0033")
    protected @ResponseBody
    ResultDto<Xdtz0033DataRespDto> xdtz0033(@Validated @RequestBody Xdtz0033DataReqDto xdtz0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033DataReqDto));
        Xdtz0033DataRespDto xdtz0033DataRespDto = new Xdtz0033DataRespDto();// 响应Dto:查询客户所担保的行内贷款五级分类非正常状态件数
        ResultDto<Xdtz0033DataRespDto> xdtz0033DataResultDto = new ResultDto<>();
        // 从xdtz0033DataReqDto获取业务值进行业务逻辑处理
        try {
            String cusId = xdtz0033DataReqDto.getCusId();//客户编号
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0033DataReqDto));
            xdtz0033DataRespDto = xdtz0033Service.xdtz0033(xdtz0033DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0033DataRespDto));
            // 封装xdtz0033DataResultDto中正确的返回码和返回信息
            xdtz0033DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0033DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, e.getMessage());
            // 封装xdtz0033DataResultDto中异常返回码和返回信息
            xdtz0033DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0033DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0033DataRespDto到xdtz0033DataResultDto中
        xdtz0033DataResultDto.setData(xdtz0033DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033DataResultDto));
        return xdtz0033DataResultDto;
    }
}
