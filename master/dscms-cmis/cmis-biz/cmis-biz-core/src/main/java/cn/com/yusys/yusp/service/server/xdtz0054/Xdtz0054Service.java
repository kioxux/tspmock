package cn.com.yusys.yusp.service.server.xdtz0054;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.IqpChgTrupayAcctApp;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.req.Xdtz0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.PvpInfoList;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.Xdtz0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 接口处理类:渠道将要修改的受托支付账号记录发给信贷，信贷落表，通知客户经理去处理
 *
 * @author YX-YD
 * @version 1.0
 */
@Service
public class Xdtz0054Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0054Service.class);

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private ToppAcctSubMapper toppAcctSubMapper;

    @Autowired
    private IqpChgTrupayAcctAppMapper iqpChgTrupayAcctAppMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;
    /**
     * @Description:渠道将要修改的受托支付账号记录发给信贷，信贷落表，通知客户经理去处理
     * @Author: YX-YD
     * @Date: 2021/6/4 20:26
     * @param xdtz0054DataReqDto
     * @return: xdtz0054DataRespDto
     **/
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0054DataRespDto xdtz0054(Xdtz0054DataReqDto xdtz0054DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataReqDto));
        // 定义返回信息
        Xdtz0054DataRespDto xdtz0054DataRespDto = new Xdtz0054DataRespDto();
        // 借据号
        String billNo = xdtz0054DataReqDto.getBillNo();
        // 流水号
        String serno = xdtz0054DataReqDto.getSerno();
        // 交易对手账号
        String toppAcctNo = xdtz0054DataReqDto.getToppAcctNo();
        // 交易对手名称
        String toppName = xdtz0054DataReqDto.getToppName();
        // 交易对手金额
        BigDecimal toppAmt = xdtz0054DataReqDto.getToppAmt();
        try {
            String msg = "success";
            if(StringUtil.isEmpty(billNo)){
                // 请求参数不存在
                xdtz0054DataRespDto.setOpFlag(StringUtils.EMPTY);
                xdtz0054DataRespDto.setOpMsg(StringUtils.EMPTY);
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }else{
                // 根据借据编号查询历史记录
                logger.info("根据根据借据编号关联出账表查询对手信息开始,查询参数为:{}", JSON.toJSONString(billNo));
                // 根据借据编号关联出账表查询对手信息
                List<ToppAcctSub>  toppAcctSubList = toppAcctSubMapper.queryToppAcctSubByBillNo(billNo);
                logger.info("根据根据借据编号关联出账表查询对手信息结束,返回参数为:{}", JSON.toJSONString(toppAcctSubList));
                if(toppAcctSubList.size()<1 || toppAcctSubList==null){
                    return xdtz0054DataRespDto;
                }else{
                    String isXs="";
                    String khhNo="";
                    String khhName="";
                    String contNo="";
                    String czSerno = "";//出账时候的流水
                    String managerId = "";
                    String managerBrId = "";
                    String cusId = "";
                    String cusName = "";
                    String telnum = "";
                    String isCus = "";
                    // 查询参数
                    Map queryMap = new HashMap<String,String>();
                    // 借据编号
                    queryMap.put("billNo", billNo);
                    // 流水号
                    queryMap.put("cusId", serno);
                    // 根据借据编号查询历史记录
                    logger.info("根据根据借据编号关联出账表查询对手信息开始,查询参数为:{}", JSON.toJSONString(queryMap));
                    // 根据借据编号关联出账表查询对手信息
                    int count = iqpChgTrupayAcctAppMapper.countIqpChgTrupayAcctAppBySerno(queryMap);
                    logger.info("根据根据借据编号关联出账表查询对手信息结束,返回参数为:{}", JSON.toJSONString(count));
                    if(count>0){
                        msg="已经存在该笔退回记录,请勿重复退回!";
                    }
                    if("success".equals(msg)){
                        queryMap.put("toppAcctNo", toppAcctNo);
                        // 根据借据编号查询历史记录
                        logger.info("根据根据借据编号关联出账表查询对手信息开始,查询参数为:{}", JSON.toJSONString(queryMap));
                        // 根据借据编号关联出账表查询对手信息
                        List<PvpInfoList> pvpInfoList = pvpLoanAppMapper.selectPvpInfoListByParam(queryMap);
                        logger.info("根据根据借据编号关联出账表查询对手信息结束,返回参数为:{}", JSON.toJSONString(pvpInfoList));
                        if(pvpInfoList.size()<1 || pvpInfoList==null){
                            msg = "根据借据号"+billNo+"找不到出账信息！";
                        }else{
                            PvpInfoList pvp = pvpInfoList.get(0);
                            managerId = pvp.getManagerId();
                            billNo = pvp.getBillNo();
                            contNo = pvp.getContNo();
                            czSerno = pvp.getPvpSerno();
                            cusId = pvp.getCusId();
                            cusName = pvp.getCusName();
                            managerId = pvp.getManagerId();
                            managerBrId = pvp.getManagerBrId();
                            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                            telnum = resultDto.getData().getUserMobilephone();
                            isXs = pvp.getIsOnline();
                            khhNo = pvp.getAcctsvcrNo();
                            khhName = pvp.getAcctsvcrName();

                            // 根据借据编号查询台账信息
                            String bizType =  accLoanMapper.selectPrdIdByBillNo(billNo);
                            // TODO PRODUCT 公司特色产品是否和PRD_ID 一个意思
                            //String product = ctrLoanContMapper.selectProductByBillNo(billNo);
                            /*if("022066".equals(bizType) || product == "1"){
                                isCus = "1";
                            }else{
                                isCus = "2";
                            }*/
                            isCus = "1";
                            // 生成流水号
                            String sqSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>());
                            // 将编辑号的信息插入IQP_CHG_TRUPAY_ACCT_APP
                            // 初始化IqpChgTrupayAcctApp
                            IqpChgTrupayAcctApp iqpChgTrupayAcctApp = new IqpChgTrupayAcctApp();
                            iqpChgTrupayAcctApp.setPkId(sqSerno);
                            iqpChgTrupayAcctApp.setSerno(serno);
                            iqpChgTrupayAcctApp.setBillNo(billNo);
                            iqpChgTrupayAcctApp.setToppAccno(toppAcctNo);
                            iqpChgTrupayAcctApp.setToppName(toppName);
                            iqpChgTrupayAcctApp.setToppAmt(toppAmt);
                            iqpChgTrupayAcctApp.setOrigiToppAccno(toppAcctNo);
                            iqpChgTrupayAcctApp.setOrigiToppName(toppName);
                            iqpChgTrupayAcctApp.setOrigiToppAmt(toppAmt);
                            iqpChgTrupayAcctApp.setInputId(managerId);
                            iqpChgTrupayAcctApp.setInputBrId(managerBrId);
                            iqpChgTrupayAcctApp.setHxSerno(contNo);
                            iqpChgTrupayAcctApp.setContNo(contNo);
                            iqpChgTrupayAcctApp.setApproveStatus("000");
                            iqpChgTrupayAcctApp.setUpdDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            iqpChgTrupayAcctApp.setToppBankNo(khhNo);
                            iqpChgTrupayAcctApp.setToppBankName(khhName);
                            iqpChgTrupayAcctApp.setOrigiToppBankno(khhNo);
                            iqpChgTrupayAcctApp.setIsOwner(isCus);
                            iqpChgTrupayAcctApp.setOrigiToppBankname(khhName);
                            int iqpChgTrupayAcctAppCnt = iqpChgTrupayAcctAppMapper.insert(iqpChgTrupayAcctApp);
                            if (iqpChgTrupayAcctAppCnt < 1) {
                                throw new Exception("插入对手交易表时出错");
                            }
                            try {
                                if (telnum != null && !"".equals(telnum)) {
                                    String content = "客户"+ cusName +"的借据"+ billNo +"中受托支付账号需要修改，请及时登录信贷管理系统处理!";
                                    sendMsg(content, telnum);
                                }
                            } catch (Exception e) {
                                throw new Exception("发送短信失败！");
                            }
                        }
                        if("success".equals(msg)){
                            xdtz0054DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                            xdtz0054DataRespDto.setOpMsg(msg);
                        }else{
                            xdtz0054DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                            xdtz0054DataRespDto.setOpMsg(msg);
                        }
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataRespDto));
        return xdtz0054DataRespDto;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
            ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

            String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            SenddxRespDto senddxRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                //  获取相关的值并解析
                senddxRespDto = senddxResultDto.getData();
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.info("errmsg：" + e.getMessage());
            throw new Exception("发送短信失败！");
        }
    }
}
