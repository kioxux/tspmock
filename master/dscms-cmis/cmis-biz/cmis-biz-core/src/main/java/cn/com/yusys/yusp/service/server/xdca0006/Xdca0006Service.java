package cn.com.yusys.yusp.service.server.xdca0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditCardAmtInfo;
import cn.com.yusys.yusp.dto.server.xdca0006.req.Xdca0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0006.resp.Xdca0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditCardAmtInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:审批结果结果反馈接口
 *
 * @author wzy
 * @version 1.0
 */
@Service
public class Xdca0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0006Service.class);


    @Autowired
    private CreditCardAmtInfoMapper creditCardAmtInfoMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0006DataRespDto xdzx0006(Xdca0006DataReqDto xdca0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataReqDto));
        Xdca0006DataRespDto xdca0006DataRespDto = new Xdca0006DataRespDto();
        try {
            Map  param = new HashMap();
            param.put("serno", xdca0006DataReqDto.getAPPNO_EXTERNAL());
            param.put("cardPrd", xdca0006DataReqDto.getPRODUCT_CD());
            CreditCardAmtInfo creditCardAmtInfo = creditCardAmtInfoMapper.queryBySernoAndPrd(param);
            logger.info(DscmsEnum.TRADE_CODE_XDCA0006.key,"返回结果为："+creditCardAmtInfo);
            if(creditCardAmtInfo !=null){
                xdca0006DataRespDto.setAccLmt(creditCardAmtInfo.getAprroveAmt());
                xdca0006DataRespDto.setRemark3("");
                xdca0006DataRespDto.setRiskResult(creditCardAmtInfo.getOprType());
            }else{
                throw BizException.error(null, EpbEnum.EPB090004.key, "未查询到审批结果信息！");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataRespDto));
        return xdca0006DataRespDto;
    }
}
