/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtFrUfrAccRel;
import cn.com.yusys.yusp.domain.LmtFrUfrApp;
import cn.com.yusys.yusp.dto.LmtFrUfrAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtFrUfrAppMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:38:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtFrUfrAppService {
	private static final Logger log = LoggerFactory.getLogger(LmtFrUfrAppService.class);
	
	@Autowired
	private LmtFrUfrAppMapper lmtFrUfrAppMapper;
	@Autowired
	private LmtFrUfrAccRelService lmtFrUfrAccRelService;
	
	/**
	 * @方法名称: selectByPrimaryKey
	 * @方法描述: 根据主键查询
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public LmtFrUfrApp selectByPrimaryKey(String frUfrSerno) {
		return lmtFrUfrAppMapper.selectByPrimaryKey(frUfrSerno);
	}
	
	/**
	 * @方法名称: selectAll
	 * @方法描述: 查询所有数据
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	@Transactional(readOnly = true)
	public List<LmtFrUfrApp> selectAll(QueryModel model) {
		List<LmtFrUfrApp> records = (List<LmtFrUfrApp>) lmtFrUfrAppMapper.selectByModel(model);
		return records;
	}
	
	/**
	 * @方法名称: selectByModel
	 * @方法描述: 条件查询 - 查询进行分页
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public List<LmtFrUfrApp> selectByModel(QueryModel model) {
		PageHelper.startPage(model.getPage(), model.getSize());
		List<LmtFrUfrApp> list = lmtFrUfrAppMapper.selectByModel(model);
		PageHelper.clearPage();
		return list;
	}
	
	/**
	 * @方法名称: insert
	 * @方法描述: 插入
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int insert(LmtFrUfrApp record) {
		return lmtFrUfrAppMapper.insert(record);
	}
	
	/**
	 * @方法名称: insertSelective
	 * @方法描述: 插入 - 只插入非空字段
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int insertSelective(LmtFrUfrApp record) {
		return lmtFrUfrAppMapper.insertSelective(record);
	}
	
	/**
	 * @方法名称: update
	 * @方法描述: 根据主键更新
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int update(LmtFrUfrApp record) {
		return lmtFrUfrAppMapper.updateByPrimaryKey(record);
	}
	
	/**
	 * @方法名称: updateSelective
	 * @方法描述: 根据主键更新 - 只更新非空字段
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int updateSelective(LmtFrUfrApp record) {
		return lmtFrUfrAppMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
	 * @方法名称: deleteByPrimaryKey
	 * @方法描述: 根据主键删除
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int deleteByPrimaryKey(String serno) {
		return lmtFrUfrAppMapper.deleteByPrimaryKey(serno);
	}
	
	/**
	 * @方法名称: deleteByIds
	 * @方法描述: 根据多个主键删除
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	
	public int deleteByIds(String ids) {
		return lmtFrUfrAppMapper.deleteByIds(ids);
	}
	
	/**
	 * 通过入参查询是否存在申请数据
	 *
	 * @param queryMap
	 * @return
	 */
	public List<LmtFrUfrApp> selectByParams(Map queryMap) {
		return lmtFrUfrAppMapper.selectByParams(queryMap);
	}
	
	/**
	 * @方法名称: saveLmtFrUrfApp
	 * @方法描述: 保存额度冻结/解冻申请表
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	@Transactional(rollbackFor = Exception.class)
	public int saveLmtFrUrfApp(Map params) throws Exception {
		int rtData = 0;
		String frUfrSerno = (String) params.get("frUfrSerno");
		String LmtCtrNo = (String) params.get("lmtCtrNo");
		String opeType = (String) params.get("opeType");
		String opType = (String) params.get("opType");
		String appType = (String) params.get("appType");

		return rtData;
	}
	
	/**
	 * @方法名称: updateApproveStatus
	 * @方法描述: 流程申请状态
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int updateApproveStatus(LmtFrUfrApp lmtFrUfrApp) {
		return lmtFrUfrAppMapper.updateApproveStatus(lmtFrUfrApp);
	}
	
	/**
	 * 审批通过后进行的业务操作
	 * 1、更新额度冻结/解冻申请状态由审批中111 -> 审批通过 997
	 * 2、更新授信协议、额度台账信息
	 *
	 * @param frUfrSerno
	 */
	@Transactional(rollbackFor = Exception.class)
	public void handleBusinessDataAfterEnd(String frUfrSerno) {
		try {

			log.info("审批通过更新额度冻结解冻主表信息" + frUfrSerno);
			LmtFrUfrApp lmtFrUfrApp = lmtFrUfrAppMapper.selectByPrimaryKey(frUfrSerno);
			if (lmtFrUfrApp == null) {
				throw new YuspException(EcbEnum.AFTEREND_LMTFRURFAPP_EXCEPTION.key, EcbEnum.AFTEREND_LMTFRURFAPP_EXCEPTION.value);
			}
			log.info("额度冻结/解冻申请" + frUfrSerno + "更新额度冻结/解冻申请申请状态");
			lmtFrUfrApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
			lmtFrUfrAppMapper.updateApproveStatus(lmtFrUfrApp);

			Map lmtCtrMap = new HashMap();
			BigDecimal lmtFrozeAmt = lmtFrUfrApp.getLmtFrozeAmt();
			BigDecimal lmtUnfrozeAmt = lmtFrUfrApp.getLmtUnfrozeAmt();
			lmtCtrMap.put("lmtCtrNo", lmtFrUfrApp.getLmtCtrNo());
			lmtCtrMap.put("appType", lmtFrUfrApp.getAppType());//操作类型（冻结/解冻）
			lmtCtrMap.put("lmtFrozeAmt", lmtFrozeAmt);//本次冻结金额
			lmtCtrMap.put("lmtUnfrozeAmt", lmtUnfrozeAmt);//本次解冻金额
			lmtCtrMap.put("updId", lmtFrUfrApp.getUpdId());//最后修改人
			lmtCtrMap.put("updBrId", lmtFrUfrApp.getUpdBrId());//最后修改机构
			lmtCtrMap.put("updDate", lmtFrUfrApp.getUpdDate());//最后修改时间
			log.info("额度冻结/解冻申请" + frUfrSerno + "获取授信协议项下引用的额度台账编号");
			List<LmtFrUfrAccRel> lmtFrUfrAccRelList = lmtFrUfrAccRelService.selectByFrUfrSerno(frUfrSerno);
			if (CollectionUtils.isEmpty(lmtFrUfrAccRelList) || lmtFrUfrAccRelList.size() == 0) {
				throw new YuspException(EcbEnum.LMT_FR_URF_APP_REL_NOTIMPROT_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_REL_NOTIMPROT_EXCEPTION.value);
			}
			List<Map> lmtAccMapList = new ArrayList<>();
			for (LmtFrUfrAccRel lmtFrUfrAccRel : lmtFrUfrAccRelList) {
				Map lmtFrUfrAccMap = JSONObject.parseObject(JSON.toJSONString(lmtFrUfrAccRel, SerializerFeature.WriteMapNullValue), Map.class);
				if (CollectionUtils.isEmpty(lmtFrUfrAccMap)) {
					throw new YuspException(EcbEnum.LMT_FR_URF_APP_REL_NOTIMPROT_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_REL_NOTIMPROT_EXCEPTION.value);
				}
				lmtAccMapList.add(lmtFrUfrAccMap);
			}
			

		} catch (YuspException e) {
			throw e;
			
		} catch (Exception e) {
			log.error("流程审批通过业务处理发生异常！", e);
			throw new YuspException(EcbEnum.E_LMT_FR_URF_APP_EXCEPTION.key, EcbEnum.E_LMT_FR_URF_APP_EXCEPTION.value);
		}
	}
	
	/**
	 * 额度冻结解冻初始化
	 */
	public LmtFrUfrAppDto getLmtFrozeAmt(String frUfrSerno) {
		LmtFrUfrApp lmtFrUfrApp = lmtFrUfrAppMapper.selectByPrimaryKey(frUfrSerno);
		if (lmtFrUfrApp == null) {
			throw new YuspException(EcbEnum.GET_LMT_FR_URF_APP_SERNO_EXCEPTION.key, EcbEnum.GET_LMT_FR_URF_APP_SERNO_EXCEPTION.value);
		}
		String lmtCtrNo = lmtFrUfrApp.getLmtCtrNo();
		String appType = lmtFrUfrApp.getAppType();
		LmtFrUfrAppDto lmtFrUfrAppDto = new LmtFrUfrAppDto();
		
		return lmtFrUfrAppDto;
	}
}
