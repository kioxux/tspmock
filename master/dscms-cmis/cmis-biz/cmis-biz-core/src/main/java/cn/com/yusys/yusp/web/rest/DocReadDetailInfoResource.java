/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DocReadDetailInfo;
import cn.com.yusys.yusp.service.DocReadDetailInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDetailInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 18:57:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/docreaddetailinfo")
public class DocReadDetailInfoResource {
    @Autowired
    private DocReadDetailInfoService docReadDetailInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<DocReadDetailInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocReadDetailInfo> list = docReadDetailInfoService.selectAll(queryModel);
        return new ResultDto<List<DocReadDetailInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<DocReadDetailInfo>> index(@RequestBody QueryModel queryModel) {
        List<DocReadDetailInfo> list = docReadDetailInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocReadDetailInfo>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:保存新增的档案记录
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savelist")
    protected ResultDto<Object> saveList(@RequestBody List<DocReadDetailInfo> docReadDetailInfo) throws URISyntaxException {
        int count = docReadDetailInfoService.saveList(docReadDetailInfo);
        return new ResultDto<Object>(count);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明: 主键ID
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody DocReadDetailInfo Info) {
        int result = docReadDetailInfoService.deleteByPrimaryKey(Info.getDrdiSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明: 多个组件ID，逗号隔开
     * @算法描述:
     */
    @PostMapping("/batchdelete")
    protected ResultDto<Integer> deletes(@RequestBody DocReadDetailInfo Info) {
        int result = docReadDetailInfoService.deleteByIds(Info.getDraiSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateDocStsBack
     * @函数描述:归还的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @param drdiSerno
     * @算法描述:
     */
    @PostMapping("/updateDocStsBack")
    protected ResultDto<Object> updateDocStsBack(@RequestBody String drdiSerno) {
        Map<String,String> res = docReadDetailInfoService.updateDocStsBack(drdiSerno);
        return new ResultDto<Object>(res.get("code"),res.get("msg"));
    }

    /**
     * @函数名称:updateDocStsBack
     * @函数描述:接受档案，表示档案已出库成功
     * @参数与返回说明:
     * @param drdiSerno
     * @算法描述:
     */
    @PostMapping("/updateDocStsByReceive")
    protected ResultDto<Object> updateDocStsByReceive(@RequestBody String drdiSerno) {
        Map<String,String> res = docReadDetailInfoService.updateDocStsByReceive(drdiSerno);
        return new ResultDto<Object>(res.get("code"),res.get("msg"));
    }
}
