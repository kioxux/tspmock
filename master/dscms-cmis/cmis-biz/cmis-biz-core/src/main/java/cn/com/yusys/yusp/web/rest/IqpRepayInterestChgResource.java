package cn.com.yusys.yusp.web.rest;
import cn.com.yusys.yusp.domain.IqpRepayInterestChg;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.IqpRepayInterestChgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayInterestChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: fangzhen
 * @创建时间: 2021-01-24  14:12:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/IqpRepayInterestChg")
public class IqpRepayInterestChgResource {
    @Autowired
    private IqpRepayInterestChgService  iqpRepayInterestChgService;

    /**
     * 添加利息减免   变更申请  初始化审核状态为带发起   操作类型为新增
     * @param iqpRepayInterestChg
     * @return
     */
    @PostMapping("/save")
    public ResultDto<Integer> save(@RequestBody IqpRepayInterestChg iqpRepayInterestChg) {
        int result = this.iqpRepayInterestChgService.save(iqpRepayInterestChg);
        return new ResultDto<Integer>(result);
    }
    /**
     *    提交流程之前的权限校验逻辑  判断该条借据是否存在其他在途的流程审批节点
     * @param iqpRepayInterestChg
     * @return
     */
    @PostMapping("/checkIsExistIqpRepayWayChgBizByBillNo")
    protected ResultDto<Integer> checkIsExistIqpRepayWayChgBizByBillNo(@RequestBody IqpRepayInterestChg iqpRepayInterestChg) throws URISyntaxException {
        int  result=this.iqpRepayInterestChgService.checkIsExistChgBizByBillNo(iqpRepayInterestChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 首页点击修改按钮   再点击保存按钮保存数据
     * @param iqpRepayInterestChg
     * @return
     */
    @PostMapping("/updateCommit")
    public ResultDto<Integer> updateCommit(@RequestBody IqpRepayInterestChg iqpRepayInterestChg) {
        int result = this.iqpRepayInterestChgService.updateCommit(iqpRepayInterestChg);
        return new ResultDto<Integer>(result);
    }
}
