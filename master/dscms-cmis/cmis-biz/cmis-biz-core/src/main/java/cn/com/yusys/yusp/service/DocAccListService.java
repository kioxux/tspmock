/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.DocAccList;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import cn.com.yusys.yusp.repository.mapper.DocAccListMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.DocAccListVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAccListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 17:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocAccListService {
    private final Logger log = LoggerFactory.getLogger(DocAccListService.class);

    @Autowired
    private DocAccListMapper docAccListMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private CmisPspClientService cmisPspClientService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private AccCvrsService accCvrsService;
    @Autowired
    private AccTfLocService accTfLocService;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocAccList selectByPrimaryKey(String docSerno) {
        return docAccListMapper.selectByPrimaryKey(docSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocAccList> selectAll(QueryModel model) {
        List<DocAccList> records = (List<DocAccList>) docAccListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: docDestoryPageList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocAccList> docDestroyPageList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        // 档案状态（已入库）
        model.addCondition("docStauts", "03");
        model.addCondition("isNeedDestroy", "true");
        List<DocAccList> list = docAccListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocAccList> selectByModel(QueryModel model) {
        model.setSort("inputDate desc");
        // 根据角色控制数据权限
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                String roleCode = roles.get(i).getCode();
                if ("R1034".equals(roleCode) || "R1036".equals(roleCode)) {
                    // R1034 集中作业档案岗；R1036 集中作业管理人员
                    model.addCondition("localFlag", "true");
                    break;
                } else if ("R1054".equals(roleCode)) {
                    // R1054 信贷管理部档案管理人员
                    model.addCondition("daFlag", "true");
                    break;
                } else if ("R0020".equals(roleCode) || "R0050".equals(roleCode) || "R0010".equals(roleCode)
                        || "R0030".equals(roleCode) || "RSG01".equals(roleCode) || "RDH01".equals(roleCode)) {
                    // 综合客户经理 R0020  小企业客户经理 R0050  小微客户经理 R0010
                    // 零售客户经理 R0030   客户经理（寿光） RSG01  客户经理（东海） RDH01
                    model.addCondition("khFlag", "true");
                    model.addCondition("kfId", loginUser.getLoginCode());
                }
            }
        }
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocAccList> list = docAccListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocAccList record) {
        return docAccListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocAccList record) {
        return docAccListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocAccList record) {
        return docAccListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocAccList record) {
        return docAccListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String docSerno) {
        return docAccListMapper.deleteByPrimaryKey(docSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docAccListMapper.deleteByIds(ids);
    }


    /**
     * 台账状态：更新为已销毁
     *
     * @param list
     * @param status
     * @return
     */
    public int updatedocAccStatus(List<String> list, String status) {
        return docAccListMapper.updatedocAccStatus(list, status);

    }


    /**
     * @方法名称: selectByDocNo
     * @方法描述: 条件查询 - 根据档案编号号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocAccList> selectByDocNo(String docNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("docNo", docNo);
        List<DocAccList> list = docAccListMapper.selectByModel(queryModel);
        return list;
    }

    /**
     * 导出档案台长列表
     *
     * @param model
     * @return
     */
    public ProgressDto exportDocAccList(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("inputDate desc");
            String apiUrl = "/api/docAccList/exportDocAccList";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return exportDocAccListData(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(DocAccListVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    private List<DocAccListVo> exportDocAccListData(QueryModel model) {
        List<DocAccListVo> docAccLists = new ArrayList<>();
        List<DocAccList> list = selectByModel(model);
        for (DocAccList docAccList : list) {
            DocAccListVo docAccListVo = new DocAccListVo();
            String managerIdName = OcaTranslatorUtils.getUserName(docAccList.getManagerId());// 责任人
            String managerBrIdName = OcaTranslatorUtils.getOrgName(docAccList.getManagerBrId());// 责任机构
            String optUsrName = OcaTranslatorUtils.getUserName(docAccList.getOptUsr());// 入库操作人
            String optOrgName = OcaTranslatorUtils.getOrgName(docAccList.getOptOrg());// 入库操作机构
            org.springframework.beans.BeanUtils.copyProperties(docAccList, docAccListVo);
            docAccListVo.setManagerIdName(managerIdName);
            docAccListVo.setManagerBrIdName(managerBrIdName);
            docAccListVo.setOptUsrName(optUsrName);
            docAccListVo.setOptOrgName(optOrgName);
            docAccLists.add(docAccListVo);
        }
        return docAccLists;
    }

    /**
     * 同步档案状态
     *
     * @param docNo
     */
    public void synDocAccStatus(String docNo, String status) {
        // 申请人，申请机构，创建时间
        String updId = "", updBrId = "", updDate = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
            // 申请时间
            updDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        // 更新档案台账状态
        List<DocAccList> docAccLists = selectByDocNo(docNo);
        if (Objects.nonNull(docAccLists) && !docAccLists.isEmpty()) {
            DocAccList docAccList = docAccLists.get(0);
            docAccList.setDocStauts(status);
            docAccList.setUpdId(updId);
            docAccList.setUpdBrId(updBrId);
            docAccList.setUpdDate(updDate);
            updateSelective(docAccList);
        }
    }

    /**
     * 获取各类型档案目录年份数据
     *
     * @author jijian_yx
     * @date 2021/7/9 20:07
     **/
    public Map<String, List<String>> getAccTimeList(String cusId) {
        Map<String, List<String>> map = new HashMap<>();
        // 档案目录

        // 1.基础信息 base
        List<String> yearList1 = cmisCusClientService.getAccTimeList(cusId).getData();
        map.put("base", yearList1);

        // 2.授信信息 lmt
        List<String> yearList2 = docAccListMapper.getAccTimeList("lmt", cusId);
        map.put("lmt", yearList2);

        // 3.放款信息 amt
        List<String> yearList3 = docAccListMapper.getAccTimeList("amt", cusId);
        map.put("amt", yearList3);

        // 4.贷后检查信息 check
        List<String> yearList4 = cmisPspClientService.getAccTimeList(cusId).getData();
        map.put("check", yearList4);

        // 5.其他合同信息 other
        List<String> yearList5 = docAccListMapper.getAccTimeList("other", cusId);
        map.put("other", yearList5);
        return map;
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     *
     * @author jijian_yx
     * @date 2021/7/10 14:15
     **/
    public List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel) {
        Object type = queryModel.getCondition().get("type");
        List<DocAccSearchDto> list = new ArrayList<>();
        if (Objects.nonNull(type)) {
            if (Objects.equals(type.toString(), "base")) {
                list = cmisCusClientService.queryAccListByDocTypeAndYear(queryModel).getData();
            } else if (Objects.equals(type.toString(), "check")) {
                list = cmisPspClientService.queryAccListByDocTypeAndYear(queryModel).getData();
            } else {
                PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
                list = docAccListMapper.queryAccListByDocTypeAndYear(queryModel);
                PageHelper.clearPage();
            }
        }
        return list;
    }

    /**
     * 获取序列号
     *
     * @author jijian_yx
     * @date 2021/8/13 14:56
     **/
    public String getSequences() {
        String daSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_GD_SEQ, new HashMap<>());
        System.out.println("档案流水号:" + daSerno);
        return daSerno;
    }

    /**
     * 更新客户名下未结清的银承、保函、开证、委托贷款台账五十级分类结果
     *
     * @author jijian_yx
     * @date 2021/10/25 23:02
     **/
    public int updateOtherLoanByCusId(Map<String, String> map) {
        int result = 0;
        try {
            int result1 = accAccpService.updateLoanFiveAndTenClassByCusId(map);
            log.info("风险分类审批结束更新客户未结清银承台账五十级分类结果[{}]", result1);
            int result2 = accCvrsService.updateLoanFiveAndTenClassByCusId(map);
            log.info("风险分类审批结束更新客户未结清保函台账五十级分类结果[{}]", result2);
            int result3 = accTfLocService.updateLoanFiveAndTenClassByCusId(map);
            log.info("风险分类审批结束更新客户未结清开证台账五十级分类结果[{}]", result3);
            int result4 = accEntrustLoanService.updateLoanFiveAndTenClassByCusId(map);
            log.info("风险分类审批结束更新客户未结清委托贷款台账五十级分类结果[{}]", result4);
            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("风险分类审批结束更新客户未结清银承、保函、开证、委托贷款台账五十级分类异常[{}]", e.getMessage());
        }
        return result;
    }
}
