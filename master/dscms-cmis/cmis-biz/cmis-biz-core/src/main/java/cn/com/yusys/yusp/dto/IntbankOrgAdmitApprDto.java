package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAppr
 * @类描述: intbank_org_admit_appr数据实体类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-05-11 13:48:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IntbankOrgAdmitApprDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 申请类型 **/
	private String appType;
	
	/** 原批复流水号 **/
	private String origiReplySerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 同业机构准入 **/
	private String intbankOrgAdmit;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 期限 **/
	private String term;
	
	/** 综合分析 **/
	private String inteAnaly;
	
	/** 评审结论 **/
	private String reviewResult;
	
	/** 结论性描述 **/
	private String restDesc;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType == null ? null : appType.trim();
	}
	
    /**
     * @return AppType
     */	
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param origiReplySerno
	 */
	public void setOrigiReplySerno(String origiReplySerno) {
		this.origiReplySerno = origiReplySerno == null ? null : origiReplySerno.trim();
	}
	
    /**
     * @return OrigiReplySerno
     */	
	public String getOrigiReplySerno() {
		return this.origiReplySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param intbankOrgAdmit
	 */
	public void setIntbankOrgAdmit(String intbankOrgAdmit) {
		this.intbankOrgAdmit = intbankOrgAdmit == null ? null : intbankOrgAdmit.trim();
	}
	
    /**
     * @return IntbankOrgAdmit
     */	
	public String getIntbankOrgAdmit() {
		return this.intbankOrgAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(String term) {
		this.term = term == null ? null : term.trim();
	}
	
    /**
     * @return Term
     */	
	public String getTerm() {
		return this.term;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly == null ? null : inteAnaly.trim();
	}
	
    /**
     * @return InteAnaly
     */	
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param reviewResult
	 */
	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult == null ? null : reviewResult.trim();
	}
	
    /**
     * @return ReviewResult
     */	
	public String getReviewResult() {
		return this.reviewResult;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc == null ? null : restDesc.trim();
	}
	
    /**
     * @return RestDesc
     */	
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}