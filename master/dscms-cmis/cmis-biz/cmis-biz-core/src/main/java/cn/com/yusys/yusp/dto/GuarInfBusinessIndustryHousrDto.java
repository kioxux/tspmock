package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBusinessIndustryHousr
 * @类描述: guar_inf_business_industry_housr数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfBusinessIndustryHousrDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 一手/二手标识 STD_ZB_IS_USED **/
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	private String twocard2oneInd;
	
	/** 产权证号 **/
	private String houseLandNo;
	
	/** 该产证是否全部抵押 STD_ZB_YES_NO **/
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	private String businessHouseNo;
	
	/** 购买日期 **/
	private String purchaseDate;
	
	/** 购买价格（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	private String buildArea;
	
	/** 竣工日期 **/
	private String endDate;
	
	/** 建成年份 **/
	private String activateYears;
	
	/** 楼龄 **/
	private String floorAge;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 门牌号/弄号 **/
	private String houseNo;
	
	/** 产权地址 **/
	private String pocAddr;
	
	/** 层次（标的楼层） **/
	private String bdlc;
	
	/** 层数（标的楼高） **/
	private String bdgd;
	
	/** 房地产所在地段情况 STD_ZB_HOUSE_PLACE_INFO **/
	private String housePlaceInfo;
	
	/** 土地证号 **/
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_USE_QUAL **/
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_USE_WAY **/
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LAND_PURP **/
	private String landPurp;
	
	/** 土地说明 **/
	private String landExplain;
	
	/** 土地使用权面积 **/
	private String landUseArea;
	
	/** 土地使用权单位 **/
	private String landUseUnit;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	private String houseUseType;
	
	/** 承租人名称 **/
	private String lesseeName;
	
	/** 年租金（元） **/
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	private String lease;
	
	/** 剩余租期（月） **/
	private String leftLease;
	
	/** 修改次数 **/
	private Integer modifyNum;
	
	/** 房产取得方式 STD_ZB_ESTATE_ACQUIRE_WAY **/
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	private String inUseYear;
	
	/** 物业类型 STD_ZB_PROPERTY_TYPE **/
	private String houseType;
	
	/** 发证日期 **/
	private String issueCertiDate;
	
	/** 工业房地产开发模式 STD_ZB_DECELOP_MODEL **/
	private String industryDecelopModel;
	
	/** 工业园区等级 **/
	private String industryZoneLevel;
	
	/** 盈利模式 STD_ZB_PROFIT_MODEL **/
	private String profitModel;
	
	/** 基础设施情况描述 **/
	private String bascFaciDesc;
	
	/** 是否满足该行业厂房设计标准 STD_ZB_YES_NO **/
	private String isIndustryDesignPlant;
	
	/** 物业情况 **/
	private String propertyCase;
	
	/** 房产使用情况 **/
	private String houseProperty;
	
	/** 所属地理位置 **/
	private String belongArea;
	
	/** 楼号 **/
	private String buildingRoomNum;
	
	/** 室号 **/
	private String roomNum;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUCTURE **/
	private String houseStructure;
	
	/** 市场租金 **/
	private java.math.BigDecimal marketRent;
	
	/** 租赁合同生效日 **/
	private String leaseConEftDt;
	
	/** 租赁合同到期日 **/
	private String leaseConEndDt;
	
	/** 租赁形式 STD_ZB_LEASE_TYPE **/
	private String leaseType;
	
	/** 楼盘名称 **/
	private String communityName;
	
	/** 商业繁华程度 STD_ZB_BUSTLING_LEVEL **/
	private String bustlingLevel;
	
	/** 交通便捷程度 STD_ZB_TRAFFIC_CONVENIENT_LEVEL **/
	private String trafficConvenientLevel;
	
	/** 外界环境 STD_ZB_ENVIRONMENT **/
	private String environment;
	
	/** 空置率 **/
	private java.math.BigDecimal vacancyRate;
	
	/** 临街情况 STD_ZB_STREET_SITUATION **/
	private String streetSituation;
	
	/** 是否位于城市地带 STD_ZB_YES_NO **/
	private String isUrbanZone;
	
	/** 建筑外观与公共装修 STD_ZB_BUILD_PUB_FIT **/
	private String buildPubFit;
	
	/** 物业管理资质等级 STD_ZB_NATUR_LEVEL **/
	private String realtyManaNaturLevel;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed == null ? null : isUsed.trim();
	}
	
    /**
     * @return IsUsed
     */	
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd == null ? null : twocard2oneInd.trim();
	}
	
    /**
     * @return Twocard2oneInd
     */	
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo == null ? null : houseLandNo.trim();
	}
	
    /**
     * @return HouseLandNo
     */	
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd == null ? null : houseAllPledgeInd.trim();
	}
	
    /**
     * @return HouseAllPledgeInd
     */	
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc == null ? null : partRegPositionDesc.trim();
	}
	
    /**
     * @return PartRegPositionDesc
     */	
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo == null ? null : businessHouseNo.trim();
	}
	
    /**
     * @return BusinessHouseNo
     */	
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(String buildArea) {
		this.buildArea = buildArea == null ? null : buildArea.trim();
	}
	
    /**
     * @return BuildArea
     */	
	public String getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears == null ? null : activateYears.trim();
	}
	
    /**
     * @return ActivateYears
     */	
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge == null ? null : floorAge.trim();
	}
	
    /**
     * @return FloorAge
     */	
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo == null ? null : houseNo.trim();
	}
	
    /**
     * @return HouseNo
     */	
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr == null ? null : pocAddr.trim();
	}
	
    /**
     * @return PocAddr
     */	
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc == null ? null : bdlc.trim();
	}
	
    /**
     * @return Bdlc
     */	
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd == null ? null : bdgd.trim();
	}
	
    /**
     * @return Bdgd
     */	
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo == null ? null : housePlaceInfo.trim();
	}
	
    /**
     * @return HousePlaceInfo
     */	
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo == null ? null : landNo.trim();
	}
	
    /**
     * @return LandNo
     */	
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual == null ? null : landUseQual.trim();
	}
	
    /**
     * @return LandUseQual
     */	
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate == null ? null : landUseBeginDate.trim();
	}
	
    /**
     * @return LandUseBeginDate
     */	
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate == null ? null : landUseEndDate.trim();
	}
	
    /**
     * @return LandUseEndDate
     */	
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears == null ? null : landUseYears.trim();
	}
	
    /**
     * @return LandUseYears
     */	
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp == null ? null : landPurp.trim();
	}
	
    /**
     * @return LandPurp
     */	
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain == null ? null : landExplain.trim();
	}
	
    /**
     * @return LandExplain
     */	
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea == null ? null : landUseArea.trim();
	}
	
    /**
     * @return LandUseArea
     */	
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setLandUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit == null ? null : landUseUnit.trim();
	}
	
    /**
     * @return LandUseUnit
     */	
	public String getLandUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType == null ? null : houseUseType.trim();
	}
	
    /**
     * @return HouseUseType
     */	
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName == null ? null : lesseeName.trim();
	}
	
    /**
     * @return LesseeName
     */	
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return AnnualRent
     */	
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease == null ? null : lease.trim();
	}
	
    /**
     * @return Lease
     */	
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease == null ? null : leftLease.trim();
	}
	
    /**
     * @return LeftLease
     */	
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param modifyNum
	 */
	public void setModifyNum(Integer modifyNum) {
		this.modifyNum = modifyNum;
	}
	
    /**
     * @return ModifyNum
     */	
	public Integer getModifyNum() {
		return this.modifyNum;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay == null ? null : estateAcquireWay.trim();
	}
	
    /**
     * @return EstateAcquireWay
     */	
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear == null ? null : inUseYear.trim();
	}
	
    /**
     * @return InUseYear
     */	
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param issueCertiDate
	 */
	public void setIssueCertiDate(String issueCertiDate) {
		this.issueCertiDate = issueCertiDate == null ? null : issueCertiDate.trim();
	}
	
    /**
     * @return IssueCertiDate
     */	
	public String getIssueCertiDate() {
		return this.issueCertiDate;
	}
	
	/**
	 * @param industryDecelopModel
	 */
	public void setIndustryDecelopModel(String industryDecelopModel) {
		this.industryDecelopModel = industryDecelopModel == null ? null : industryDecelopModel.trim();
	}
	
    /**
     * @return IndustryDecelopModel
     */	
	public String getIndustryDecelopModel() {
		return this.industryDecelopModel;
	}
	
	/**
	 * @param industryZoneLevel
	 */
	public void setIndustryZoneLevel(String industryZoneLevel) {
		this.industryZoneLevel = industryZoneLevel == null ? null : industryZoneLevel.trim();
	}
	
    /**
     * @return IndustryZoneLevel
     */	
	public String getIndustryZoneLevel() {
		return this.industryZoneLevel;
	}
	
	/**
	 * @param profitModel
	 */
	public void setProfitModel(String profitModel) {
		this.profitModel = profitModel == null ? null : profitModel.trim();
	}
	
    /**
     * @return ProfitModel
     */	
	public String getProfitModel() {
		return this.profitModel;
	}
	
	/**
	 * @param bascFaciDesc
	 */
	public void setBascFaciDesc(String bascFaciDesc) {
		this.bascFaciDesc = bascFaciDesc == null ? null : bascFaciDesc.trim();
	}
	
    /**
     * @return BascFaciDesc
     */	
	public String getBascFaciDesc() {
		return this.bascFaciDesc;
	}
	
	/**
	 * @param isIndustryDesignPlant
	 */
	public void setIsIndustryDesignPlant(String isIndustryDesignPlant) {
		this.isIndustryDesignPlant = isIndustryDesignPlant == null ? null : isIndustryDesignPlant.trim();
	}
	
    /**
     * @return IsIndustryDesignPlant
     */	
	public String getIsIndustryDesignPlant() {
		return this.isIndustryDesignPlant;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase == null ? null : propertyCase.trim();
	}
	
    /**
     * @return PropertyCase
     */	
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty == null ? null : houseProperty.trim();
	}
	
    /**
     * @return HouseProperty
     */	
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea == null ? null : belongArea.trim();
	}
	
    /**
     * @return BelongArea
     */	
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum == null ? null : buildingRoomNum.trim();
	}
	
    /**
     * @return BuildingRoomNum
     */	
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum == null ? null : roomNum.trim();
	}
	
    /**
     * @return RoomNum
     */	
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure == null ? null : houseStructure.trim();
	}
	
    /**
     * @return HouseStructure
     */	
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param marketRent
	 */
	public void setMarketRent(java.math.BigDecimal marketRent) {
		this.marketRent = marketRent;
	}
	
    /**
     * @return MarketRent
     */	
	public java.math.BigDecimal getMarketRent() {
		return this.marketRent;
	}
	
	/**
	 * @param leaseConEftDt
	 */
	public void setLeaseConEftDt(String leaseConEftDt) {
		this.leaseConEftDt = leaseConEftDt == null ? null : leaseConEftDt.trim();
	}
	
    /**
     * @return LeaseConEftDt
     */	
	public String getLeaseConEftDt() {
		return this.leaseConEftDt;
	}
	
	/**
	 * @param leaseConEndDt
	 */
	public void setLeaseConEndDt(String leaseConEndDt) {
		this.leaseConEndDt = leaseConEndDt == null ? null : leaseConEndDt.trim();
	}
	
    /**
     * @return LeaseConEndDt
     */	
	public String getLeaseConEndDt() {
		return this.leaseConEndDt;
	}
	
	/**
	 * @param leaseType
	 */
	public void setLeaseType(String leaseType) {
		this.leaseType = leaseType == null ? null : leaseType.trim();
	}
	
    /**
     * @return LeaseType
     */	
	public String getLeaseType() {
		return this.leaseType;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName == null ? null : communityName.trim();
	}
	
    /**
     * @return CommunityName
     */	
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bustlingLevel
	 */
	public void setBustlingLevel(String bustlingLevel) {
		this.bustlingLevel = bustlingLevel == null ? null : bustlingLevel.trim();
	}
	
    /**
     * @return BustlingLevel
     */	
	public String getBustlingLevel() {
		return this.bustlingLevel;
	}
	
	/**
	 * @param trafficConvenientLevel
	 */
	public void setTrafficConvenientLevel(String trafficConvenientLevel) {
		this.trafficConvenientLevel = trafficConvenientLevel == null ? null : trafficConvenientLevel.trim();
	}
	
    /**
     * @return TrafficConvenientLevel
     */	
	public String getTrafficConvenientLevel() {
		return this.trafficConvenientLevel;
	}
	
	/**
	 * @param environment
	 */
	public void setEnvironment(String environment) {
		this.environment = environment == null ? null : environment.trim();
	}
	
    /**
     * @return Environment
     */	
	public String getEnvironment() {
		return this.environment;
	}
	
	/**
	 * @param vacancyRate
	 */
	public void setVacancyRate(java.math.BigDecimal vacancyRate) {
		this.vacancyRate = vacancyRate;
	}
	
    /**
     * @return VacancyRate
     */	
	public java.math.BigDecimal getVacancyRate() {
		return this.vacancyRate;
	}
	
	/**
	 * @param streetSituation
	 */
	public void setStreetSituation(String streetSituation) {
		this.streetSituation = streetSituation == null ? null : streetSituation.trim();
	}
	
    /**
     * @return StreetSituation
     */	
	public String getStreetSituation() {
		return this.streetSituation;
	}
	
	/**
	 * @param isUrbanZone
	 */
	public void setIsUrbanZone(String isUrbanZone) {
		this.isUrbanZone = isUrbanZone == null ? null : isUrbanZone.trim();
	}
	
    /**
     * @return IsUrbanZone
     */	
	public String getIsUrbanZone() {
		return this.isUrbanZone;
	}
	
	/**
	 * @param buildPubFit
	 */
	public void setBuildPubFit(String buildPubFit) {
		this.buildPubFit = buildPubFit == null ? null : buildPubFit.trim();
	}
	
    /**
     * @return BuildPubFit
     */	
	public String getBuildPubFit() {
		return this.buildPubFit;
	}
	
	/**
	 * @param realtyManaNaturLevel
	 */
	public void setRealtyManaNaturLevel(String realtyManaNaturLevel) {
		this.realtyManaNaturLevel = realtyManaNaturLevel == null ? null : realtyManaNaturLevel.trim();
	}
	
    /**
     * @return RealtyManaNaturLevel
     */	
	public String getRealtyManaNaturLevel() {
		return this.realtyManaNaturLevel;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}