/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.DocImageSpplInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocImageSpplInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocImageSpplInfoService {

    private static final Logger logger = LoggerFactory.getLogger(DocImageSpplInfoService.class);

    @Autowired
    private DocImageSpplInfoMapper docImageSpplInfoMapper;
    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;
    @Autowired
    private LmtAppService lmtAppService;//授信
    @Autowired
    private PvpLoanAppService pvpLoanAppService;//放款申请
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private BusinessInformationService businessInformationService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private IqpContExtService iqpContExtService;
    @Autowired
    private IqpGuarChgAppService iqpGuarChgAppService;
    @Autowired
    private BusiImageRelInfoService busiImageRelInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private SendMessageForDgService sendMessageForDgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocImageSpplInfo selectByPrimaryKey(String disiSerno) {
        return docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocImageSpplInfo> selectAll(QueryModel model) {
        List<DocImageSpplInfo> records = (List<DocImageSpplInfo>) docImageSpplInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocImageSpplInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocImageSpplInfo> list = docImageSpplInfoMapper.selectByModel(model);
        list.stream().forEach(docImageSppl -> {
            // 存量数据业务补扫类型处理,涉及影像目录的调阅
            if (StringUtils.isBlank(docImageSppl.getTaskFlag())) {
                docImageSppl.setTaskFlag("01");
            }
            String spplType = docImageSppl.getSpplType();
            String spplBizType = docImageSppl.getSpplBizType();
            if (Objects.equals(spplType, "01") && StringUtils.isBlank(spplBizType)) {
                String belg_line = docImageSpplInfoMapper.getBelgLineByBizSerno(docImageSppl.getBizSerno());
                if (Objects.nonNull(belg_line)) {
                    if (Objects.equals(belg_line, "01")) {
                        docImageSppl.setSpplBizType("04");// 小微
                    } else if (Objects.equals(belg_line, "02")) {
                        docImageSppl.setSpplBizType("03");// 零售
                    } else if (Objects.equals(belg_line, "03")) {
                        docImageSppl.setSpplBizType("01");// 对公
                    }
                    docImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSppl);
                } else {
                    // 是否银承放款
                    int yc = docImageSpplInfoMapper.isYCAmtByBizSerno(docImageSppl.getBizSerno());
                    if (yc > 0) {
                        docImageSppl.setSpplBizType("02");
                        docImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSppl);
                    }
                    // 是否委托贷款出账
                    int wtdk = docImageSpplInfoMapper.isWTDKAmtByBizSerno(docImageSppl.getBizSerno());
                    if (wtdk > 0) {
                        docImageSppl.setSpplBizType("08");
                        docImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSppl);
                    }
                }
            } else if (Objects.equals(spplType, "03") && StringUtils.isBlank(spplBizType)) {
                if (docImageSpplInfoMapper.isDHIqpByBizSerno(docImageSppl.getBizSerno()) > 0) {
                    // 05	单户授信
                    docImageSppl.setSpplBizType("05");
                } else if (docImageSpplInfoMapper.isJTIqpByBizSerno(docImageSppl.getBizSerno()) > 0) {
                    // 06	集团授信
                    docImageSppl.setSpplBizType("06");
                } else if (docImageSpplInfoMapper.isLSIqpByBizSerno(docImageSppl.getBizSerno()) > 0) {
                    // 07	零售业务授信
                    docImageSppl.setSpplBizType("07");
                }
                docImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSppl);
            }
        });
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocImageSpplInfo record) {
        return docImageSpplInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocImageSpplInfo record) {
        return docImageSpplInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocImageSpplInfo record) {
        return docImageSpplInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocImageSpplInfo record) {
        return docImageSpplInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String disiSerno) {
        int result = 0;
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        if (null != docImageSpplInfo) {
            if (Objects.equals(docImageSpplInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_992)) {
                docImageSpplInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                docImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSpplInfo);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(disiSerno);
                result = 1;
            } else if (Objects.equals(docImageSpplInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_000)) {
                result = docImageSpplInfoMapper.deleteByPrimaryKey(disiSerno);
            }
        }
        return result;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docImageSpplInfoMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:selectSernoByCusId
     * @函数描述:根据客户编号查询业务流水号--档案管理/影像补扫
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public List<DocImageSpplInfoPoDto> selectSernoByCusId(QueryModel queryModel) {
        List<DocImageSpplInfoPoDto> docImageSpplInfoPoDtoList = new ArrayList<DocImageSpplInfoPoDto>();
        String spplType = queryModel.getCondition().get("spplType").toString();
        boolean flag = false;

        // 角色控制数据权限
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                // 综合客户经理 R0020; 零售客户经理 R0030; 小企业客户经理 R0050
                // 小微客户经理 R0010；客户经理（寿光） RSG01；客户经理（东海） RDH01
                if ("R0020".equals(roles.get(i).getCode()) || "R0030".equals(roles.get(i).getCode()) || "R0050".equals(roles.get(i).getCode())
                        || "R0010".equals(roles.get(i).getCode()) || "RSG01".equals(roles.get(i).getCode()) || "RDH01".equals(roles.get(i).getCode())) {
                    flag = true;
                    break;
                }
            }
        }
        if (flag) {
            queryModel.addCondition("managerId", loginUser.getLoginCode());
        }
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        if (!StringUtils.isEmpty(spplType)) {
            docImageSpplInfoPoDtoList = docImageSpplInfoMapper.selectSernoByCusId(queryModel);
        }
        PageHelper.clearPage();
        return docImageSpplInfoPoDtoList;

    }

    /**
     * 影像补扫申请新增
     *
     * @author cainingbo_yx
     * @date 2021/6/18
     **/
    public ResultDto<String> saveDoImageSpplInfo(DocImageSpplInfo record) {
        ResultDto<String> resultDto = new ResultDto<>();

        // 有系统生成的补扫任务不允许再发起人工任务
        // 判断该笔业务流水号是否已生成在途系统生成的影像补扫任务
        Map<String, String> map = new HashMap<>();
        map.put("bizSerno", record.getBizSerno());// 业务流水号
        map.put("spplType", record.getSpplType());// 影像补扫类型
        map.put("taskFlag", "01");// 任务标识 01系统发起;02人工发起
        map.put("existsAppFlag", "true");// 标识位 在途
        int existsCount = docImageSpplInfoMapper.selectByBizSerno(map);
        if (existsCount > 0) {
            resultDto.setCode(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_02.key);
            resultDto.setMessage(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_02.value);
            return resultDto;
        }
        // 无系统发起的补扫任务,再判断是否重复发起
        // 判断该笔业务流水号是否已生成在途人工生成的影像补扫任务
        Map<String, String> map2 = new HashMap<>();
        map2.put("bizSerno", record.getBizSerno());// 业务流水号
        map2.put("spplType", record.getSpplType());// 影像补扫类型
        map2.put("taskFlag", "02");// 任务标识 01系统发起;02人工发起
        map2.put("existsAppFlag", "true");// 标识位 在途
        int existsCount2 = docImageSpplInfoMapper.selectByBizSerno(map2);
        if (existsCount2 > 0) {
            resultDto.setCode(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_03.key);
            resultDto.setMessage(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_03.value);
            return resultDto;
        }

        String disiSerno = "";
        String updId = "";//登记人
        String updBrId = "";//登记人机构
        if (null == record.getDisiSerno()) {
            disiSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_BS_SEQ, new HashMap<>());
            record.setDisiSerno(disiSerno);
        }
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
        }
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        record.setInputId(updId);//登记人ID
        record.setInputBrId(updBrId);//登记人机构
        record.setInputDate(openDay);
        record.setCreateTime(DateUtils.parseDateByDef(openDay));
        record.setUpdDate(openDay);
        record.setUpdateTime(DateUtils.parseDateByDef(openDay));
        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//申请状态为待发起
        int count = docImageSpplInfoMapper.insertSelective(record);
        if (count > 0) {
            resultDto.setData(disiSerno);
        } else {
            resultDto.setCode(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_01.key);
            resultDto.setMessage(EcbEnum.E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_01.value);
        }
        return resultDto;
    }

    /**
     * @函数名称:updateDoImageSpplInfo
     * @函数描述:实体类创建
     * @参数与返回说明:
     * @算法描述:
     * @author :cainingbo_yx
     */
    public Map updateDocImageSpplInfo(DocImageSpplInfo record) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.DISI_SUCCESS_DEF.key;
        String rtnMsg = "";
        String updId = "";//修改人
        String updBrId = "";//修改机构
        String updDate = "";
        if (null != record.getDisiSerno()) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 修改人
                updId = userInfo.getLoginCode();
                // 修改机构
                updBrId = userInfo.getOrg().getCode();
                // 修改时间
                updDate = openDay;
            }
            Date createTime = DateUtils.parseDateByDef(openDay);// 修改时间
            record.setUpdId(updId);
            record.setUpdBrId(updBrId);
            record.setUpdDate(updDate);//修改日期
            record.setUpdateTime(createTime);//修改时间
            int count = docImageSpplInfoMapper.updateByPrimaryKeySelective(record);
            if (count > 0) {
                result.put("rtnCode", rtnCode);
                result.put("rtnMsg", rtnMsg);
            } else {
                result.put("rtnCode", EcbEnum.E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_01.key);
                result.put("rtnMsg", EcbEnum.E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_01.value);
            }
        } else {
            result.put("rtnCode", EcbEnum.E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_02.key);
            result.put("rtnMsg", EcbEnum.E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_02.value);
        }
        return result;
    }

    /**
     * @函数名称:selectDetailByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公授信
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public DocImageSpplInfoPoDto selectDetailByBizSerno(String bizSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", bizSerno);
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = new DocImageSpplInfoPoDto();
        List<LmtApp> lmtAppList = lmtAppService.selectByModel(queryModel);
        if (lmtAppList != null && lmtAppList.size() != 0) {
            docImageSpplInfoPoDto.setIsCom("Y");
        } else {
            docImageSpplInfoPoDto.setIsCom("N");
        }
        return docImageSpplInfoPoDto;
    }

    /**
     * @函数名称:selectDetailHTByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公合同
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public DocImageSpplInfoPoDto selectDetailHTByBizSerno(String disiSerno) {

        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        String bizSerno = docImageSpplInfo.getBizSerno();
//        String spplBizType = docImageSpplInfo.getSpplBizType();

        QueryModel queryModel = new QueryModel();
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = new DocImageSpplInfoPoDto();
//        if (Objects.equals(spplBizType, "09")) {
//            // 09 合同影像补扫
//            queryModel.addCondition("iqpSerno", bizSerno);
//            List<CtrLoanCont> ctrLoanContList = ctrLoanContService.selectByModel(queryModel);
//            if (null != ctrLoanContList && ctrLoanContList.size() > 0) {
//                String belgLine = ctrLoanContList.get(0).getBelgLine();
//                if ("02".equals(belgLine)) {
//                    docImageSpplInfoPoDto.setIsCom("N");
//                } else {
//                    docImageSpplInfoPoDto.setIsCom("Y");
//                }
//            }
//        } else {
        // 08 合同影像审核
        queryModel.addCondition("serno", bizSerno);
        List<CtrContImageAuditApp> ctrContImageAuditAppList = ctrContImageAuditAppService.selectByModel(queryModel);
        if (null != ctrContImageAuditAppList && ctrContImageAuditAppList.size() > 0) {
            String belgLine = ctrContImageAuditAppList.get(0).getBelgLine();
            if ("02".equals(belgLine)) {
                docImageSpplInfoPoDto.setIsCom("N");
            } else {
                docImageSpplInfoPoDto.setIsCom("Y");
            }
        }
//        }
        return docImageSpplInfoPoDto;
    }


    /**
     * @函数名称:selectDetailFKByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公放款
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public DocImageSpplInfoPoDto selectDetailFKByBizSerno(String disiSerno) {
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = new DocImageSpplInfoPoDto();
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        if (Objects.equals(docImageSpplInfo.getSpplBizType(), CmisCommonConstants.STD_SPPL_BIZ_TYPE_02)
                || Objects.equals(docImageSpplInfo.getSpplBizType(), CmisCommonConstants.STD_SPPL_BIZ_TYPE_08)) {
            docImageSpplInfoPoDto.setIsCom("Y");
            return docImageSpplInfoPoDto;
        }
        String bizSerno = docImageSpplInfo.getBizSerno();
        if (Objects.equals(docImageSpplInfo.getSpplBizType(), CmisCommonConstants.STD_SPPL_BIZ_TYPE_03)) {
            String pvpSerno = docImageSpplInfoMapper.selectPvpSernoByBillNo(docImageSpplInfo.getBillNo());
            bizSerno = Optional.ofNullable(pvpSerno).orElse(bizSerno);
        }
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pvpSerno", bizSerno);
        List<PvpLoanApp> pvpLoanAppList = pvpLoanAppService.selectByModel(queryModel);
        for (PvpLoanApp pvpLoanApps : pvpLoanAppList) {
            String belgLine = pvpLoanApps.getBelgLine();
            if ("02".equals(belgLine)) {
                docImageSpplInfoPoDto.setIsCom("N");
            } else {
                docImageSpplInfoPoDto.setIsCom("Y");
            }
        }
        return docImageSpplInfoPoDto;
    }

    /**
     * @函数名称:selectDetailHTByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公展期
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public DocImageSpplInfoPoDto selectDetailZQByBizSerno(String bizSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("iqpSerno", bizSerno);
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = new DocImageSpplInfoPoDto();
        List<IqpContExt> iqpContExtList = iqpContExtService.selectByModel(queryModel);
        for (IqpContExt iqpContExt : iqpContExtList) {
            String belgLine = iqpContExt.getBelgLine();
            if ("02".equals(belgLine)) {
                docImageSpplInfoPoDto.setIsCom("N");
            } else {
                docImageSpplInfoPoDto.setIsCom("Y");
            }
        }
        return docImageSpplInfoPoDto;
    }

    /**
     * @函数名称:selectDetailHTByBizSerno
     * @函数描述:当流程提交时查询单个对象判断是否为对公担保变更
     * @参数与返回说明:
     * @算法描述:
     * @author: cainingbo_yx
     */
    public DocImageSpplInfoPoDto selectDetailDBBGByBizSerno(String bizSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("iqpSerno", bizSerno);
        DocImageSpplInfoPoDto docImageSpplInfoPoDto = new DocImageSpplInfoPoDto();
        List<IqpGuarChgApp> iqpGuarChgAppList = iqpGuarChgAppService.selectByModel(queryModel);
        for (IqpGuarChgApp iqpGuarChgApp : iqpGuarChgAppList) {
            String belgLine = iqpGuarChgApp.getBelgLine();
            if ("02".equals(belgLine)) {
                docImageSpplInfoPoDto.setIsCom("N");
            } else {
                docImageSpplInfoPoDto.setIsCom("Y");
            }
        }
        return docImageSpplInfoPoDto;
    }

    /**
     * 系统生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/6/30 10:52
     **/
    public int createDocImageSpplBySys(DocImageSpplClientDto docImageSpplClientDto) {
        if (StringUtils.isEmpty(docImageSpplClientDto.getBizSerno())) {
            // 未传入业务流水号
            return 0;
        }
        // 判断该笔业务流水号是否已生成在途影像补扫任务
        Map<String, String> map = new HashMap<>();
        map.put("bizSerno", docImageSpplClientDto.getBizSerno());// 业务流水号
        map.put("spplType", docImageSpplClientDto.getSpplType());// 补扫类型 STD_SPPL_TYPE
        map.put("taskFlag", "01");// 任务标识 01系统发起;02人工发起
        map.put("existsAppFlag", "true");// 标识位 在途
        int count = docImageSpplInfoMapper.selectByBizSerno(map);
        if (count > 0) {
            // 已生成,防止重复生成
            return 1;
        } else {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 生成影像补扫任务
            DocImageSpplInfo docImageSpplInfo = new DocImageSpplInfo();
            BeanUtils.copyProperties(docImageSpplClientDto, docImageSpplInfo);
            docImageSpplInfo.setDisiSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_BS_SEQ, new HashMap<>()));// 流水号
            docImageSpplInfo.setTaskFlag("01");// 任务标识 01系统发起;02人工发起
            docImageSpplInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);// 审批状态 待发起
            docImageSpplInfo.setInputDate(openDay);
            docImageSpplInfo.setCreateTime(DateUtils.parseDateByDef(openDay));
            return docImageSpplInfoMapper.insertSelective(docImageSpplInfo);
        }
    }

    /**
     * 补扫完成更新原业务资料齐全
     *
     * @author jijian_yx
     * @date 2021/8/28 22:13
     **/
    public void updateBusinessInfo(String bizSerno) {
        BusinessInformation businessInformation = businessInformationService.selectByBizId(bizSerno);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            businessInformation.setComplete("1");
            businessInformationService.updateSelective(businessInformation);
        }
    }

    /**
     * 影像补扫流程结束，人工发起的补扫任务，
     * 如果没有在途的归档任务,生成一笔新的档案归档任务
     *
     * @author jijian_yx
     * @date 2021/8/30 10:38
     **/
    public void insertNewDocTask(DocImageSpplInfo spplInfo) {
        // 人工发起的补扫任务
        logger.info("人工补扫任务结束生成归档任务开始，关联业务流水号[{}]", spplInfo.getBizSerno());
        if (Objects.equals("02", spplInfo.getTaskFlag())) {
            if (Objects.equals(spplInfo.getSpplType(), CmisCommonConstants.STD_SPPL_TYPE_03)
                    && Objects.equals(spplInfo.getSpplBizType(), CmisCommonConstants.STD_SPPL_BIZ_TYPE_07)) {
                // 零售业务授信不生成归档任务
                return;
            }
            int count = docArchiveInfoService.queryDocTaskInWayCount(spplInfo.getBizSerno());
            if (count <= 0) {
                // 无在途归档任务,新生成一笔
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                Map<String, String> docMap = changeSpplToDoc(spplInfo);
                String docType = docMap.get("docType");// 档案类型
                String docClass = docMap.get("docClass");// 档案分类
                String docBizType = docMap.get("docBizType");// 档案业务品种

                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(spplInfo.getInputBrId());
                String archiveMode = "03";
                if (null != resultDto && null != resultDto.getData()) {
                    String orgType = resultDto.getData().getOrgType();
                    if ("1".equals(orgType) || "2".equals(orgType) || "3".equals(orgType)) {
                        archiveMode = "01";
                    } else {
                        List<String> nodeNames = commonService.getInstanceNodeNameByBizId(spplInfo.getBizSerno());
                        if (null != nodeNames && nodeNames.size() > 0) {
                            for (String nodeName : nodeNames) {
                                if (nodeName.contains("集中作业")) {
                                    archiveMode = "02";// 本地集中归档
                                    break;
                                }
                            }
                        }
                    }
                }

                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(spplInfo.getCusId());
                String certType = "";
                String certCode = "";
                if (null != cusBaseClientDto) {
                    certType = cusBaseClientDto.getCertType();
                    certCode = cusBaseClientDto.getCertCode();
                }
                docArchiveClientDto.setDocType(docType);
                docArchiveClientDto.setDocClass(docClass);
                docArchiveClientDto.setArchiveMode(archiveMode);
                docArchiveClientDto.setDocBizType(docBizType);
                docArchiveClientDto.setBizSerno(spplInfo.getBizSerno());
                docArchiveClientDto.setCusId(spplInfo.getCusId());
                docArchiveClientDto.setCusName(spplInfo.getCusName());
                docArchiveClientDto.setCertType(certType);
                docArchiveClientDto.setCertCode(certCode);
                docArchiveClientDto.setManagerId(spplInfo.getInputId());
                docArchiveClientDto.setManagerBrId(spplInfo.getInputBrId());
                docArchiveClientDto.setInputId(spplInfo.getInputId());
                docArchiveClientDto.setInputBrId(spplInfo.getInputBrId());
                docArchiveClientDto.setContNo(spplInfo.getContNo());
                docArchiveClientDto.setBillNo(spplInfo.getBillNo());
                docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            }
        }
    }

    /**
     * 根据影像补扫类型转换对应档案类型、档案分类
     *
     * @author jijian_yx
     * @date 2021/8/30 14:11
     **/
    private Map<String, String> changeSpplToDoc(DocImageSpplInfo spplInfo) {
        /**
         * 影像补扫类型 STD_SPPL_TYPE
         * 补扫业务品种 STD_SPPL_BIZ_TYPE
         *
         * 01:放款影像补扫
         *      01	一般贷款出账
         *      02	银承出账
         *      03	零售放款
         *      04	小微放款
         *      08  委托贷款出账
         * 02:合同审核影像补扫
         * 03:授信类型资料补录
         *      05	单户授信
         *      06	集团授信
         *      07	零售业务授信
         * 04:合作方准入影像补扫
         * 05:合作方协议签订影像补扫
         * 06:征信影像补扫
         * 07:展期申请影像补扫
         * 08:担保变更申请影像补扫
         * 09:展期协议影像补扫
         * 10:担保变更协议影像补扫
         **/
        String spplType = spplInfo.getSpplType();// 补扫类型
        String spplBizType = spplInfo.getSpplBizType();// 补扫业务品种
        String bizSerno = spplInfo.getBizSerno();// 关联业务流水号
        String docType = "";// 档案类型
        String docBizType = "";// 档案业务品种
        String docClass = "";// 档案分类
        BigDecimal loanAmt = BigDecimal.ZERO;// 金额
        String startDate = "";// 起始时间
        String endDate = "";// 到期时间
        switch (spplType) {
            case "01":// 放款影像补扫
                String pvpSerno = bizSerno;
                if (Objects.equals("03", spplBizType)) {
                    pvpSerno = selectPvpSernoByBillNo(spplInfo.getBillNo());
                }
                PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                if (null != pvpLoanApp) {
                    loanAmt = Optional.ofNullable(pvpLoanApp.getPvpAmt()).orElse(BigDecimal.ZERO);
                    startDate = pvpLoanApp.getStartDate();
                    endDate = pvpLoanApp.getEndDate();
                }
                if (Objects.equals("01", spplBizType)) {// 01一般贷款出账
                    docType = "05";
                    docClass = "03";
                } else if (Objects.equals("02", spplBizType)) {// 02银承出账
                    docType = "14";
                    docClass = "03";
                } else if (Objects.equals("03", spplBizType)) {// 03零售放款
                    docType = "17";
                    docClass = "03";
                } else if (Objects.equals("04", spplBizType)) {// 04小微放款
                    docType = "22";
                    docClass = "03";
                } else if (Objects.equals("08", spplBizType)) {// 08委托贷款出账
                    docType = "06";
                    docClass = "03";
                }
                break;
            case "02":// 合同审核影像补扫
                String bizType = "";
                CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(bizSerno);
                if (null != ctrContImageAuditApp) {
                    loanAmt = Optional.ofNullable(ctrContImageAuditApp.getContAmt()).orElse(BigDecimal.ZERO);
                    bizType = Optional.ofNullable(ctrContImageAuditApp.getBizType()).orElse("");
                }
                if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_01, bizType)) {
                    docType = "05";
                    docBizType = "09";// 最高额授信协议
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_02, bizType)) {
                    docType = "05";
                    docBizType = "08";// 普通贷款
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_03, bizType)) {
                    docType = "09";// 贴现
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_04, bizType)) {
                    docType = "15";// 国际业务
                    docBizType = "07";// 贸易融资
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_05, bizType)) {
                    docType = "15";// 国际业务
                    docBizType = "05";// 福费廷
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_06, bizType)) {
                    docType = "15";// 国际业务
                    docBizType = "06";// 开证
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_07, bizType)) {
                    docType = "14";// 承兑业务
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_08, bizType)) {
                    docType = "13";// 保函
                    docClass = "03";
                } else if (Objects.equals(CmisCommonConstants.STD_BUSI_TYPE_09, bizType)) {
                    docType = "06";// 委托贷款
                    docClass = "03";
                }
                break;
            case "03":// 授信类型资料补录
                if (Objects.equals("05", spplBizType)) {// 05单户授信
                    docType = "04";
                    docBizType = "01";
                    docClass = "02";
                } else if (Objects.equals("06", spplBizType)) {// 06集团授信
                    docType = "04";
                    docBizType = "02";
                    docClass = "02";
                } else if (Objects.equals("07", spplBizType)) {// 07零售业务授信
                    docType = "03";
                    docClass = "02";
                }
                break;
            case "04":// 合作方准入影像补扫
                docType = "10";
                docClass = "03";
                break;
            case "05":// 合作方协议签订影像补扫
                docType = "10";
                docClass = "03";
                break;
            case "06":// 征信影像补扫
                docType = "02";
                docClass = "01";
                break;
            case "07":// 展期申请影像补扫
                docType = "12";
                docBizType = "10";
                docClass = "03";
                break;
            case "08":// 担保变更申请影像补扫
                docType = "24";
                docBizType = "12";
                docClass = "03";
                break;
            case "09":// 展期协议影像补扫
                docType = "12";
                docBizType = "11";
                docClass = "03";
                break;
            case "10":// 担保变更协议影像补扫
                docType = "24";
                docBizType = "13";
                docClass = "03";
                break;
            default:
                break;
        }
        Map<String, String> map = new HashMap<>();
        map.put("docType", docType);
        map.put("docBizType", docBizType);
        map.put("docClass", docClass);
        map.put("loanAmt", loanAmt.toPlainString());
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        return map;
    }

    /**
     * 查询客户大类
     *
     * @author jijian_yx
     * @date 2021/8/30 15:05
     **/
    public String queryCusCatalog(String disiSerno) {
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        String cusCatalog = "";//客户大类 STD_ZB_CUS_CATALOG 1:对私;2:公司;3:同业;4:集团
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(docImageSpplInfo.getCusId());
        if (null != cusBaseClientDto) {
            cusCatalog = cusBaseClientDto.getCusCatalog();
        }
        return cusCatalog;
    }

    /**
     * 根据业务流水号和审批状态获取影像补扫任务数
     *
     * @author jijian_yx
     * @date 2021/9/3 20:58
     **/
    public int selectByBizSerno(Map<String, String> map) {
        return docImageSpplInfoMapper.selectByBizSerno(map);
    }

    /**
     * 获取合作方准入/协议影像目录
     *
     * @author jijian_yx
     * @date 2021/9/8 14:35
     **/
    public String getCoopImageCode(String disiSerno) {
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        String bizSerno = Optional.ofNullable(docImageSpplInfo.getBizSerno()).orElse("");
        return docImageSpplInfoMapper.getCoopImageCode(bizSerno);
    }

    /**
     * 获取合作方准入/协议影像目录
     *
     * @author jijian_yx
     * @date 2021/9/8 14:35
     **/
    public String getCoopImageCodeByBizSerno(String bizSerno) {
        return docImageSpplInfoMapper.getCoopImageCode(bizSerno);
    }

    /**
     * 根据借据号获取出账流水号
     *
     * @author jijian_yx
     * @date 2021/9/16 2:02
     **/
    public String selectPvpSernoByBillNo(String billNo) {
        return docImageSpplInfoMapper.selectPvpSernoByBillNo(billNo);
    }

    /**
     * 推送影像审批信息
     *
     * @author jijian_yx
     * @date 2021/10/6 21:33
     **/
    public void sendImage(ResultInstanceDto resultInstanceDto, DocImageSpplInfo docImageSpplInfo) {
        logger.info("影像补扫流程影像审核开始,补扫申请流水号[{}]", docImageSpplInfo.getBizSerno());
        /**
         * 影像补扫类型
         * 01	放款影像补扫
         * 02	合同影像补扫
         * 03	授信类型资料补录
         * 04	合作方准入影像补扫
         * 05	合作方协议签订影像补扫
         * 06	征信影像补扫
         * 07	展期申请影像补扫
         * 08	担保变更申请影像补扫
         * 09	展期协议影像补扫
         * 10	担保变更协议影像补扫
         **/
        String spplType = docImageSpplInfo.getSpplType();
        /**
         * 补扫业务品种
         * 01	一般贷款出账 （01	放款影像补扫）
         * 02	银承出账    （01	放款影像补扫）
         * 03	零售放款    （01	放款影像补扫）
         * 04	小微放款    （01	放款影像补扫）
         * 05	单户授信    （03	授信类型资料补录）
         * 06	集团授信    （03	授信类型资料补录）
         * 07	零售业务授信 （03	授信类型资料补录）
         * 08   委托贷款出账 （01	放款影像补扫）
         **/
        String spplBizType = docImageSpplInfo.getSpplBizType();
        if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_04) || Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_05)) {
            // 合作方协议影像参数动态获取
            QueryModel model = new QueryModel();
            model.addCondition("serno", docImageSpplInfo.getBizSerno());
            List<BusiImageRelInfo> list = busiImageRelInfoService.selectByModel(model);
            if (null != list && list.size() > 0) {
                for (BusiImageRelInfo busiImageRelInfo : list) {
                    String imageNo = busiImageRelInfo.getImageNo();// 任务编号
                    String topOutsystemCode = busiImageRelInfo.getTopOutsystemCode();// 影像根级目录
                    String[] arr = topOutsystemCode.split(";");
                    for (int i = 0; i < arr.length; i++) {
                        ImageApprDto imageApprDto = new ImageApprDto();
                        imageApprDto.setDocId(imageNo);//任务编号
                        imageApprDto.setApproval("同意");//审批意见
                        imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                        imageApprDto.setOutcode(arr[i]);//文件类型根节点
                        imageApprDto.setOpercode(resultInstanceDto.getCurrentUserId());//审批人员
                        sendMessageForDgService.sendImage(imageApprDto);
                    }
                }
            }
        } else {
            String outcode = "";// 影像根级目录
            String docId = "";// 任务编号
            if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_01)) {
                if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_01)) {
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_02)) {
                    outcode = "CDHPCZJB;CDHPDY;CDHPZY;CDHPBZDB;CDHPDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_03)) {
                    outcode = "GRXFDKCZJB;GRXFDKCZDY;GRXFDKCZZY;GRXFDKCZBZDB;GRXFDKCZDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_04)) {
                    outcode = "XDCZZYFLWB;XDCZDZY;XDCZDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_08)) {
                    outcode = "WTDKCZJB;WTDKDYHT;WTDKZYHT;WTDKBZDBHT;WTDKDCCZ";
                }
                docId = docImageSpplInfo.getContNo();
            } else if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_02)) {
                String bizType = getCtrContImageBizType(docImageSpplInfo.getDisiSerno());
                if (Objects.equals(bizType, "01")) {// 最高额授信协议
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                } else if (Objects.equals(bizType, "02")) {// 普通贷款合同
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                } else if (Objects.equals(bizType, "03")) {// 贴现协议
                    outcode = "DGYX;XDTXYWYX";
                } else if (Objects.equals(bizType, "04")) {// 贸易融资合同
                    outcode = "GJYWHTJB;GJYWDY;GJYWZY;GJYWBZDB;GJYWMYRZ;GJYWMYRZ;GJYWBH;GJYWDCCZ;GJYWSQSQT";
                } else if (Objects.equals(bizType, "05")) {// 福费廷合同
                    outcode = "GJYWHTJB;GJYWDY;GJYWZY;GJYWBZDB;GJYWMYRZ;GJYWMYRZ;GJYWBH;GJYWDCCZ;GJYWSQSQT";
                } else if (Objects.equals(bizType, "06")) {// 开证合同
                    outcode = "GJYWHTJB;GJYWDY;GJYWZY;GJYWBZDB;GJYWXYZ;GJYWDCCZ;GJYWSQSQT";
                } else if (Objects.equals(bizType, "07")) {// 银承合同
                    outcode = "CDHPCZJB;CDHPDY;CDHPZY;CDHPBZDB;CDHPDCCZ";
                } else if (Objects.equals(bizType, "08")) {// 保函合同
                    outcode = "BHCZJB;BHDY;BHZY;BHBZDB";
                } else if (Objects.equals(bizType, "09")) {// 委托贷款合同
                    outcode = "WTDKCZJB;WTDKDYHT;WTDKZYHT;WTDKBZDBHT;WTDKDCCZ";
                } else if (Objects.equals(bizType, "LS")) {// 零售线上提款启用
                    outcode = "GRXFDKCZJB;GRXFDKCZDY;GRXFDKCZZY;GRXFDKCZBZDB";
                } else {
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                }
                docId = docImageSpplInfo.getContNo();
            } else if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_03)) {
                if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_05)) {
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_06)) {
                    outcode = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ";
                } else if (Objects.equals(spplBizType, CmisCommonConstants.STD_SPPL_BIZ_TYPE_07)) {
                    outcode = "GRXFDKSX;GRXFDKCZDY";
                }
                docId = docImageSpplInfo.getBizSerno();
            } else if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_06)) {
                String cusCatalog = queryCusCatalog(docImageSpplInfo.getDisiSerno());// 客户大类
                if (Objects.equals(cusCatalog, "1")) {
                    outcode = "ZXCXSQZLGR";// 1对私
                } else {
                    outcode = "ZXCXSQZLQY";// 2对公 3同业 4集团
                }
                docId = docImageSpplInfo.getBizSerno();
            } else if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_07) || Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_09)) {
                outcode = "ZQSQYX;ZQCZ";
                docId = docImageSpplInfo.getBizSerno();
            } else if (Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_08) || Objects.equals(spplType, CmisCommonConstants.STD_SPPL_TYPE_10)) {
                outcode = "XXD_YWBGSQ";
                docId = docImageSpplInfo.getBizSerno();
            }
            String[] arr = outcode.split(";");
            for (int i = 0; i < arr.length; i++) {
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(docId);//任务编号
                imageApprDto.setApproval("同意");//审批意见
                imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                imageApprDto.setOutcode(arr[i]);//文件类型根节点
                imageApprDto.setOpercode(resultInstanceDto.getCurrentUserId());//审批人员
                sendMessageForDgService.sendImage(imageApprDto);
            }
        }
        logger.info("影像补扫流程影像审核结束");
    }

    /**
     * 获取合同影像审核业务类型
     *
     * @author jijian_yx
     * @date 2021/10/31 13:57
     **/
    public String getCtrContImageBizType(String disiSerno) {
        String bizType = "";
        DocImageSpplInfo docImageSpplInfo = docImageSpplInfoMapper.selectByPrimaryKey(disiSerno);
        if (null != docImageSpplInfo) {
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(docImageSpplInfo.getBizSerno());
            if (null != ctrContImageAuditApp) {
                String belg_line = ctrContImageAuditApp.getBelgLine();
                if (Objects.equals(belg_line, CmisCommonConstants.STD_BELG_LINE_02)) {
                    // 零售线上提款启用
                    bizType = "LS";
                } else if (Objects.equals(belg_line, CmisCommonConstants.STD_BELG_LINE_03)) {
                    bizType = ctrContImageAuditApp.getBizType();
                }
            }
        }
        return bizType;
    }
}