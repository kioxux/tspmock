/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydGuarCont
 * @类描述: tmp_wyd_guar_cont数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_guar_cont_tmp")
public class TmpWydGuarContTmp {
	
	/** 担保合同编号 **/
	@Id
	@Column(name = "GUAR_CONTRACT_NO")
	private String guarContractNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 主合同类型 **/
	@Column(name = "MAIN_CONTRACT_TYPE", unique = false, nullable = true, length = 10)
	private String mainContractType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 20)
	private String guarAmt;
	
	/** 币种 **/
	@Column(name = "CCY_CD", unique = false, nullable = true, length = 10)
	private String ccyCd;
	
	/** 签约日期 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 10)
	private String signDate;
	
	/** 到期日期 **/
	@Column(name = "MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String maturityDate;
	
	/** 保证人或抵质押品的权属人客户号 **/
	@Column(name = "GUAR_CUSTNO", unique = false, nullable = true, length = 30)
	private String guarCustno;
	
	/** 保证人或抵质押品的权属人客户名称 **/
	@Column(name = "GUAR_CUST_NAME", unique = false, nullable = true, length = 60)
	private String guarCustName;
	
	/** 保证人或抵质押品的权属人客户类型 **/
	@Column(name = "GUAR_CUST_TYPE", unique = false, nullable = true, length = 32)
	private String guarCustType;
	
	/** 保证人或抵质押品的权属人客户证件类型 **/
	@Column(name = "GUAR_CUST_IDTYPE", unique = false, nullable = true, length = 20)
	private String guarCustIdtype;
	
	/** 保证人或抵质押品的权属人客户证件号码 **/
	@Column(name = "GUAR_CUST_IDNO", unique = false, nullable = true, length = 30)
	private String guarCustIdno;
	
	/** 担保合同类型 **/
	@Column(name = "GUAR_CONTRACT_TYPE", unique = false, nullable = true, length = 10)
	private String guarContractType;
	
	/** 担保方式 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 10)
	private String guarType;
	
	/** 地方政府融资平台担保分类 **/
	@Column(name = "LOCAL_GOV_GUAR_TYPE", unique = false, nullable = true, length = 10)
	private String localGovGuarType;
	
	/** 担保起始日期 **/
	@Column(name = "GUAR_START_DATE", unique = false, nullable = true, length = 10)
	private String guarStartDate;
	
	/** 担保到期日期 **/
	@Column(name = "GUAR_END_DATE", unique = false, nullable = true, length = 10)
	private String guarEndDate;
	
	/** 担保合同失效日期 **/
	@Column(name = "EXPIRE_DATE", unique = false, nullable = true, length = 10)
	private String expireDate;
	
	/** 担保合同状态 **/
	@Column(name = "GUAR_CONTRACT_STS", unique = false, nullable = true, length = 3)
	private String guarContractSts;
	
	/** 经办员工号 **/
	@Column(name = "OPERATOR", unique = false, nullable = true, length = 20)
	private String operator;
	
	/** 保证人类型 **/
	@Column(name = "GUARANTOR_TYPE", unique = false, nullable = true, length = 10)
	private String guarantorType;
	
	/** 主合同号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 64)
	private String contractNo;
	
	/** 单位id **/
	@Column(name = "MERCHANT_ID", unique = false, nullable = true, length = 32)
	private String merchantId;
	
	/** 是否是平台批量担保 **/
	@Column(name = "IS_PLATFORM_GUAR", unique = false, nullable = true, length = 1)
	private String isPlatformGuar;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param guarContractNo
	 */
	public void setGuarContractNo(String guarContractNo) {
		this.guarContractNo = guarContractNo;
	}
	
    /**
     * @return guarContractNo
     */
	public String getGuarContractNo() {
		return this.guarContractNo;
	}
	
	/**
	 * @param mainContractType
	 */
	public void setMainContractType(String mainContractType) {
		this.mainContractType = mainContractType;
	}
	
    /**
     * @return mainContractType
     */
	public String getMainContractType() {
		return this.mainContractType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(String guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public String getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param ccyCd
	 */
	public void setCcyCd(String ccyCd) {
		this.ccyCd = ccyCd;
	}
	
    /**
     * @return ccyCd
     */
	public String getCcyCd() {
		return this.ccyCd;
	}
	
	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}
	
    /**
     * @return signDate
     */
	public String getSignDate() {
		return this.signDate;
	}
	
	/**
	 * @param maturityDate
	 */
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
    /**
     * @return maturityDate
     */
	public String getMaturityDate() {
		return this.maturityDate;
	}
	
	/**
	 * @param guarCustno
	 */
	public void setGuarCustno(String guarCustno) {
		this.guarCustno = guarCustno;
	}
	
    /**
     * @return guarCustno
     */
	public String getGuarCustno() {
		return this.guarCustno;
	}
	
	/**
	 * @param guarCustName
	 */
	public void setGuarCustName(String guarCustName) {
		this.guarCustName = guarCustName;
	}
	
    /**
     * @return guarCustName
     */
	public String getGuarCustName() {
		return this.guarCustName;
	}
	
	/**
	 * @param guarCustType
	 */
	public void setGuarCustType(String guarCustType) {
		this.guarCustType = guarCustType;
	}
	
    /**
     * @return guarCustType
     */
	public String getGuarCustType() {
		return this.guarCustType;
	}
	
	/**
	 * @param guarCustIdtype
	 */
	public void setGuarCustIdtype(String guarCustIdtype) {
		this.guarCustIdtype = guarCustIdtype;
	}
	
    /**
     * @return guarCustIdtype
     */
	public String getGuarCustIdtype() {
		return this.guarCustIdtype;
	}
	
	/**
	 * @param guarCustIdno
	 */
	public void setGuarCustIdno(String guarCustIdno) {
		this.guarCustIdno = guarCustIdno;
	}
	
    /**
     * @return guarCustIdno
     */
	public String getGuarCustIdno() {
		return this.guarCustIdno;
	}
	
	/**
	 * @param guarContractType
	 */
	public void setGuarContractType(String guarContractType) {
		this.guarContractType = guarContractType;
	}
	
    /**
     * @return guarContractType
     */
	public String getGuarContractType() {
		return this.guarContractType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param localGovGuarType
	 */
	public void setLocalGovGuarType(String localGovGuarType) {
		this.localGovGuarType = localGovGuarType;
	}
	
    /**
     * @return localGovGuarType
     */
	public String getLocalGovGuarType() {
		return this.localGovGuarType;
	}
	
	/**
	 * @param guarStartDate
	 */
	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate;
	}
	
    /**
     * @return guarStartDate
     */
	public String getGuarStartDate() {
		return this.guarStartDate;
	}
	
	/**
	 * @param guarEndDate
	 */
	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate;
	}
	
    /**
     * @return guarEndDate
     */
	public String getGuarEndDate() {
		return this.guarEndDate;
	}
	
	/**
	 * @param expireDate
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	
    /**
     * @return expireDate
     */
	public String getExpireDate() {
		return this.expireDate;
	}
	
	/**
	 * @param guarContractSts
	 */
	public void setGuarContractSts(String guarContractSts) {
		this.guarContractSts = guarContractSts;
	}
	
    /**
     * @return guarContractSts
     */
	public String getGuarContractSts() {
		return this.guarContractSts;
	}
	
	/**
	 * @param operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
    /**
     * @return operator
     */
	public String getOperator() {
		return this.operator;
	}
	
	/**
	 * @param guarantorType
	 */
	public void setGuarantorType(String guarantorType) {
		this.guarantorType = guarantorType;
	}
	
    /**
     * @return guarantorType
     */
	public String getGuarantorType() {
		return this.guarantorType;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param merchantId
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
    /**
     * @return merchantId
     */
	public String getMerchantId() {
		return this.merchantId;
	}
	
	/**
	 * @param isPlatformGuar
	 */
	public void setIsPlatformGuar(String isPlatformGuar) {
		this.isPlatformGuar = isPlatformGuar;
	}
	
    /**
     * @return isPlatformGuar
     */
	public String getIsPlatformGuar() {
		return this.isPlatformGuar;
	}


}