/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.CfgAccountClassChooseDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.PvpAuthorizeService;
import cn.com.yusys.yusp.service.risk.RiskItem0104Service;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorizeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-01-08 20:46:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvpauthorize")
public class PvpAuthorizeResource {
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;


    @Autowired
    private RiskItem0104Service riskItem0104Service;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpAuthorize>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpAuthorize> list = pvpAuthorizeService.selectAll(queryModel);
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpAuthorize>> index(QueryModel queryModel) {
        List<PvpAuthorize> list = pvpAuthorizeService.selectByModel(queryModel);
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{tranSerno}")
    protected ResultDto<PvpAuthorize> show(@PathVariable("tranSerno") String tranSerno) {
        PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectByPrimaryKey(tranSerno);
        return new ResultDto<PvpAuthorize>(pvpAuthorize);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpAuthorize> create(@RequestBody PvpAuthorize pvpAuthorize) throws URISyntaxException {
        pvpAuthorizeService.insert(pvpAuthorize);
        return new ResultDto<PvpAuthorize>(pvpAuthorize);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpAuthorize pvpAuthorize) throws URISyntaxException {
        int result = pvpAuthorizeService.update(pvpAuthorize);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{tranSerno}")
    protected ResultDto<Integer> delete(@PathVariable("tranSerno") String tranSerno) {
        int result = pvpAuthorizeService.deleteByPrimaryKey(tranSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpAuthorizeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }



    /**
     * @函数名称:toSignEnstrustlist-委托贷款出账zxz
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tosignentrustlist")
    protected ResultDto<List<PvpAuthorize>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.toSignEntrustlist(queryModel);
        ResultDto<List<PvpAuthorize>> resultDto = new ResultDto<List<PvpAuthorize>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:doneSignEnstrustlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/donesignentrustlist")
    protected ResultDto<List<PvpAuthorize>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.doneSignEntrustlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:sendcore
     * @函数描述:发送核心进行出账
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendcore")
    protected  ResultDto sendCore(@RequestBody PvpAuthorize pvpAuthorize) {
        ResultDto resultDto = pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
        return resultDto;
    }

    /**
     * @函数名称:toSignAccpList
     * @函数描述:银承贷款待出账列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("银承贷款待出账列表")
    @PostMapping("/tosignaccplist")
    protected ResultDto<List<PvpAuthorize>> toSignAccpList(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("tranId", CmisCommonConstants.YCCZ);
        queryModel.addCondition("tradeStatus", CmisCommonConstants.TRADE_STATUS_1);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.toSignAccpList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:doneSignAccpList
     * @函数描述:银承贷款出账历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("银承贷款出账历史列表")
    @PostMapping("/donesignaccplist")
    protected ResultDto<List<PvpAuthorize>> doneSignAccpList(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("tranId", CmisCommonConstants.YCCZ);
        queryModel.addCondition("tradeStatus", CmisCommonConstants.TRADE_STATUS_2);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.doneSignAccpList(queryModel);
        ResultDto<List<PvpAuthorize>> resultDto = new ResultDto<List<PvpAuthorize>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:sendBill
     * @函数描述:发送票据系统进行出账
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("发送票据系统进行出账")
    @PostMapping("/sendbill")
    protected  ResultDto<Map> sendBill(@RequestBody PvpAuthorize pvpAuthorize) {
        Map rtnData =  pvpAuthorizeService.sendBill(pvpAuthorize);
        return new ResultDto<>(rtnData);
    }

    /**
     * @author zlf
     * @date 2021/5/8 17:26
     * @version 1.0.0
     * @desc    零售发送核心出账授权
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/sendcorels")
    protected  ResultDto<PvpAuthorize> sendCorels(@RequestBody PvpAuthorize pvpAuthorize) {
        return pvpAuthorizeService.sendCorels(pvpAuthorize);
    }
    
    

    /**
     * @函数名称:toAccPvpAuthorize
     * @函数描述:贷款出账通知
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贷款出账通知")
    @PostMapping("/toAccPvpAuthorize")
    protected ResultDto<List<PvpAuthorize>> toAccPvpAuthorize(@RequestBody QueryModel queryModel) {
        queryModel.setSort("tranDate desc");
        queryModel.addCondition("tranId", CmisCommonConstants.DKCZ);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.toAccPvpAuthorize(queryModel);
        ResultDto<List<PvpAuthorize>> resultDto = new ResultDto<List<PvpAuthorize>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:doneSignAccpList
     * @函数描述:银承贷款出账历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("贷款出账通知历史列表")
    @PostMapping("/doneAccPvpAuthorize")
    protected ResultDto<List<PvpAuthorize>> doneAccPvpAuthorize(@RequestBody QueryModel queryModel) {
        queryModel.setSort("tranDate desc");
        queryModel.addCondition("tranId", CmisCommonConstants.DKCZ);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAuthorize> list = pvpAuthorizeService.doneAccPvpAuthorize(queryModel);
        ResultDto<List<PvpAuthorize>> resultDto = new ResultDto<List<PvpAuthorize>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @函数名称:params
     * @函数描述:出账通知查看
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("出账通知查看")
    @PostMapping("/showdetialsub")
    protected ResultDto<Object> showDetialSub(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpAuthorize temp = new PvpAuthorize();
        PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectByPrimaryKey((String)params.get(("tranSerno")));
        if (pvpAuthorize != null) {
            resultDto.setCode(200);
            resultDto.setData(pvpAuthorize);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 18:17
     * @注释 分页条件查询界面
     */
    @ApiOperation("分页条件查询,多余筛选条件,所属条线,操作类型,申请状态")
    @PostMapping("/selectbymodel")
    protected ResultDto<List<PvpAuthorize>> selectbymodel(@RequestBody  QueryModel queryModel) {
        List<PvpAuthorize> list = pvpAuthorizeService.selectByModel(queryModel);
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:27
     * @注释 通知核心出账
     */
    @PostMapping("/sendAuthToCoreForXd")
    protected ResultDto sendAuthToCoreForXd( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);

    }
    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:27
     * @注释 冲正申请
     */
    @PostMapping("/jyczsq")
    protected ResultDto jyczsq( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.jyczsq(pvpAuthorize);

    }
/*

    */
/**
     * @创建人 李帅
     * @创建时间 2021/8/9 9:27
     * @注释 冲正申请
     */

    @PostMapping("/czsq")
    protected ResultDto czsq( @RequestBody PvpLoanApp pvpLoanApp) {
        return pvpAuthorizeService.czsq(pvpLoanApp);

    }


/**
     * @创建人 李帅
     * @创建时间 2021/8/9 14:10
     * @注释 查询授权状态
     */

    @GetMapping("/getAuthStatus/{pvpSerNo}")
    protected ResultDto getAuthStatus(@PathVariable("pvpSerNo") String pvpSerNo) {
        return pvpAuthorizeService.getAuthStatus(pvpSerNo);

    }
    /**
     * @创建人 尚智勇
     * @创建时间 2021/9/3 10:13
     * @注释 查询交易流水号
     */
    @PostMapping("/getTranSerno/{pvpSerNo}")
    protected ResultDto getTranSerno(@PathVariable("pvpSerNo") String pvpSerNo) {
        return pvpAuthorizeService.getTranSerno(pvpSerNo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:27
     * @注释 冲正处理
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.ib1241(pvpAuthorize);

    }

    /**
     * @author shenli
     * @date 2021-7-2
     * @version 1.0.0
     * @desc     出账收授权列表查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/selectpvpauthorizelist")
    protected ResultDto<List<PvpAuthorize>> selectPvpAuthorizeList(@RequestBody QueryModel queryModel) {
        List<PvpAuthorize> list = pvpAuthorizeService.selectPvpAuthorizeList(queryModel);
        return new ResultDto<List<PvpAuthorize>>(list);
    }

    /**
     * @author shenli
     * @date 2021-7-2
     * @version 1.0.0
     * @desc     出账收授权列表查询
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/selectbyprimarykey")
    protected ResultDto<PvpAuthorize> selectbyprimarykey(@RequestBody String tranSerno) {
        PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectByPrimaryKey(tranSerno);
        return new ResultDto<PvpAuthorize>(pvpAuthorize);
    }

    /**
     * @函数名称:sendcoreforwtdk
     * @函数描述:委托贷款发送核心进行出账
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendcoreforwtdk")
    protected  ResultDto sendCoreForWTDK(@RequestBody PvpAuthorize pvpAuthorize) {
        ResultDto resultDto = pvpAuthorizeService.sendAuthToCoreForXdWTDK(pvpAuthorize);
        return resultDto;
    }

    /**
     * @函数名称:riskItem0104
     * @函数描述:零售-手动发送前第三方保证金校验
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/riskitem0104")
    protected  ResultDto<RiskResultDto> riskItem0104(@RequestBody PvpAuthorize pvpAuthorize) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("bizId",pvpAuthorize.getPvpSerno());
        return new ResultDto<RiskResultDto>(riskItem0104Service.riskItem0104(queryModel));
    }

    /**
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map>
     * @author hubp
     * @date 2021/9/6 14:01
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/annulpvpauthorize")
    protected ResultDto annulPvpAuthorize(@RequestBody PvpAuthorize pvpAuthorize) {
        return pvpAuthorizeService.annulPvpAuthorize(pvpAuthorize.getPvpSerno());
    }


    /**
     * @方法名称: calHxAccountClass
     * @方法描述: 测算核心科目编号
     * @参数与返回说明: 根据12个科目属性调用科目查询的接口查询返回科目数据
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/calHxAccountClass")
    protected ResultDto<CfgAccountClassChooseDto>  calHxAccountClass(@RequestBody String pvpSerno){
        return new ResultDto<CfgAccountClassChooseDto>(pvpAuthorizeService.calHxAccountClass(pvpSerno));
    }

    /**
     * @param pvpAuthorize
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map>
     * @author shenli
     * @date 2021/9/6 14:01
     * @version 1.0.0
     * @desc 判断账户是否为正常
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/sendpvpcheckretail")
    protected ResultDto sendPvpCheckRetail(@RequestBody PvpAuthorize pvpAuthorize) {
        return pvpAuthorizeService.sendPvpCheckRetail(pvpAuthorize.getPvpSerno());
    }


    /**
     * @创建人 shenl
     * @创建时间 2021-9-29 23:11:38
     * @注释 零售冲正处理
     */
    @PostMapping("/retailcz")
    protected ResultDto retailCz( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.retailCz(pvpAuthorize);
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:11:38
     * @注释 出账通知手动作废
     */
    @PostMapping("/handinvaild")
    protected ResultDto handInvaild( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.handInvaild(pvpAuthorize);
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:11:38
     * @注释 委托贷款出账通知手动作废
     */
    @PostMapping("/handinvaildentrust")
    protected ResultDto handInvaildEntrust( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.handInvaildEntrust(pvpAuthorize);
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-29 23:11:38
     * @注释 委托贷款出账通知冲正处理
     */
    @PostMapping("/retailczentrust")
    protected ResultDto retailCzEntrust( @RequestBody PvpAuthorize pvpAuthorize) {
        return  pvpAuthorizeService.retailCzEntrust(pvpAuthorize);
    }
}