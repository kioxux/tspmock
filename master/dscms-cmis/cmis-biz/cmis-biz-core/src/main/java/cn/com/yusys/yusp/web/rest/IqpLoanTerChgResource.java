/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanTerChg;
import cn.com.yusys.yusp.service.IqpLoanTerChgService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * 贷款投向调整resource
 */
@RestController
@RequestMapping("/api/iqploanterchg")
public class IqpLoanTerChgResource {
    @Autowired
    private IqpLoanTerChgService iqpLoanTerChgService;

    private static final Logger log = LoggerFactory.getLogger(IqpLoanTerChgService.class);

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanTerChg>> index(QueryModel queryModel) {
        List<IqpLoanTerChg> list = iqpLoanTerChgService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanTerChg>>(list);
    }

    /**
     * 新增页面保存贷款投向调整申请数据
     *
     * @param params
     * @return
     */
    @PostMapping("/saveIqpLoanTerInfo")
    protected ResultDto<Map> saveIqpLoanTerInfo(@RequestBody Map params) {
        Map result = iqpLoanTerChgService.saveIqpLoanTerChg(params);
        System.out.println(result.toString());
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称: checkIsExistWFByContNo
     * @函数描述: 根据借据编号查询是否存在在途的变更业务
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkIsExistWFBizByContNo")
    protected ResultDto<Integer> checkIsExistWFByBillNo(@RequestBody IqpLoanTerChg iqpLoanTerChg) throws  URISyntaxException{
        log.info("新增还款方式申请表数据【{}】", JSONObject.toJSON(iqpLoanTerChg));
        int result = iqpLoanTerChgService.checkIsExistWFByBillNo(iqpLoanTerChg);
        return new ResultDto<Integer>(result);
    }


}
