/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCar
 * @类描述: iqp_car数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:32:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_car")
public class IqpCar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 汽车类型 STD_ZB_CAR_TYP **/
	@Column(name = "CAR_TYPE", unique = false, nullable = true, length = 5)
	private String carType;
	
	/** 所购车辆品牌型号 **/
	@Column(name = "CAR_BRAND_MODEL", unique = false, nullable = true, length = 40)
	private String carBrandModel;
	
	/** 车辆总价 **/
	@Column(name = "CAR_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal carTotal;
	
	/** 首付金额 **/
	@Column(name = "FIRSTPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstpayAmt;
	
	/** 销售商名称 **/
	@Column(name = "SALE_NAME", unique = false, nullable = true, length = 80)
	private String saleName;
	
	/** 是否进口车型 **/
	@Column(name = "IS_INLET_CAR", unique = false, nullable = true, length = 5)
	private String isInletCar;
	
	/** 汽车品牌名称 **/
	@Column(name = "CAR_BRAND_NAME", unique = false, nullable = true, length = 80)
	private String carBrandName;
	
	/** 车架号 **/
	@Column(name = "VIN", unique = false, nullable = true, length = 40)
	private String vin;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param carType
	 */
	public void setCarType(String carType) {
		this.carType = carType;
	}
	
    /**
     * @return carType
     */
	public String getCarType() {
		return this.carType;
	}
	
	/**
	 * @param carBrandModel
	 */
	public void setCarBrandModel(String carBrandModel) {
		this.carBrandModel = carBrandModel;
	}
	
    /**
     * @return carBrandModel
     */
	public String getCarBrandModel() {
		return this.carBrandModel;
	}
	
	/**
	 * @param carTotal
	 */
	public void setCarTotal(java.math.BigDecimal carTotal) {
		this.carTotal = carTotal;
	}
	
    /**
     * @return carTotal
     */
	public java.math.BigDecimal getCarTotal() {
		return this.carTotal;
	}
	
	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}
	
    /**
     * @return firstpayAmt
     */
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}
	
	/**
	 * @param saleName
	 */
	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}
	
    /**
     * @return saleName
     */
	public String getSaleName() {
		return this.saleName;
	}
	
	/**
	 * @param isInletCar
	 */
	public void setIsInletCar(String isInletCar) {
		this.isInletCar = isInletCar;
	}
	
    /**
     * @return isInletCar
     */
	public String getIsInletCar() {
		return this.isInletCar;
	}
	
	/**
	 * @param carBrandName
	 */
	public void setCarBrandName(String carBrandName) {
		this.carBrandName = carBrandName;
	}
	
    /**
     * @return carBrandName
     */
	public String getCarBrandName() {
		return this.carBrandName;
	}
	
	/**
	 * @param vin
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}
	
    /**
     * @return vin
     */
	public String getVin() {
		return this.vin;
	}


}