package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0115Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0115Resource
 * @类描述: 合作方(集群贷市场方、专业担保公司)保证金校验
 * @功能描述:
 * @创建人: yfs
 * @创建时间: 2021-09-01 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0115合作方(集群贷市场方、专业担保公司)保证金校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0115")
public class RiskItem0115Resource {

    @Autowired
    private RiskItem0115Service riskItem0115Service;

    /**
     * @方法名称: riskItem0115
     * @方法描述: 合作方(集群贷市场方、专业担保公司)保证金校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yfs
     * @创建时间: 2021-09-07 10:30:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "合作方(集群贷市场方、专业担保公司)保证金校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0115(@RequestBody QueryModel queryModel) {
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0115Service.riskItem0115(queryModel));
    }
}
