package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req.Yx0003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp.Yx0003RespDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.Yky001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.req.CmisLmt0057ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcfEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.service.client.bsp.core.dp2021.Dp2021Service;
import cn.com.yusys.yusp.service.client.bsp.xwh.xwh003.Xwh003Service;
import cn.com.yusys.yusp.service.client.bsp.xwh.yx0003.Yx0003Service;
import cn.com.yusys.yusp.service.client.bsp.yk.yky001.Yky001Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0001.CmisLmt0001Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0009.CmisLmt0009Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0010.CmisLmt0010Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0011.CmisLmt0011Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0013.CmisLmt0013Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0029.CmisLmt0029Service;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0057.CmisLmt0057Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CmisBizXwCommonService
 * @类描述: 小微公共处理类
 * @功能描述: 小微公共处理方法
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisBizXwCommonService {
    private Logger logger = LoggerFactory.getLogger(CmisBizXwCommonService.class);

    @Autowired
    private CmisLmt0001Service cmisLmt0001Service;

    @Autowired
    private CmisLmt0009Service cmisLmt0009Service;

    @Autowired
    private CmisLmt0010Service cmisLmt0010Service;

    @Autowired
    private CmisLmt0011Service cmisLmt0011Service;

    @Autowired
    private CmisLmt0013Service cmisLmt0013Service;

    @Autowired
    private CmisLmt0029Service cmisLmt0029Service;

    @Autowired
    private CmisLmt0057Service cmisLmt0057Service;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AreaOrgService areaOrgService;

    @Autowired
    private AreaUserService areaUserService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private Yky001Service yky001Service;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AreaManagerService areaManagerService;

    @Autowired
    private AreaAdminUserService areaAdminUserService;

    @Autowired
    private FbManagerService fbManagerService;

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;

    @Autowired
    private AccLoanFreeInterService accLoanFreeInterService;

    @Autowired
    private DiscCouponBusinRelService discCouponBusinRelService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private Yx0003Service yx0003Service;

    @Autowired
    private Xwh003Service xwh003Service;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private Dp2021Service dp2021Service;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * @param lmtCrdReplyInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/8/25 19:05
     * @version 1.0.0
     * @desc 前往额度系统同步额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0001RespDto sendCmisLmt0001(LmtCrdReplyInfo lmtCrdReplyInfo) {
        logger.info("授信批复 - 前往额度系统占用额度开始【{}】", lmtCrdReplyInfo.getSurveySerno());
        CmisLmt0001ReqDto cmisLmt0001ReqDto = null;
        CmisLmt0001RespDto cmisLmt0001RespDto = null;
        try {
            // 1、前往额度系统建立额度
            // 组装额度系统报文
            cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
            cmisLmt0001ReqDto.setInstuCde(getInstuCde(lmtCrdReplyInfo.getManagerBrId()));
            cmisLmt0001ReqDto.setAccNo(lmtCrdReplyInfo.getSurveySerno());//批复台账编号
            cmisLmt0001ReqDto.setCurType(lmtCrdReplyInfo.getCurType());//币种
            cmisLmt0001ReqDto.setCusId(lmtCrdReplyInfo.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtCrdReplyInfo.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("0");//是否生成新批复台账
            cmisLmt0001ReqDto.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());//授信金额
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);//批复台账状态
            cmisLmt0001ReqDto.setTerm(lmtCrdReplyInfo.getAppTerm());//授信期限
            cmisLmt0001ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());//起始日
            cmisLmt0001ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());//到期日
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);//批复台账状态
            cmisLmt0001ReqDto.setManagerId(lmtCrdReplyInfo.getManagerId());//责任人
            cmisLmt0001ReqDto.setManagerBrId(lmtCrdReplyInfo.getManagerBrId());//责任机构
            cmisLmt0001ReqDto.setInputId(lmtCrdReplyInfo.getInputId());//登记人
            cmisLmt0001ReqDto.setInputBrId(lmtCrdReplyInfo.getInputBrId());//登记机构
            cmisLmt0001ReqDto.setInputDate(lmtCrdReplyInfo.getInputDate());//登记日期
            /* 额度分项*/
            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());//授信分项编号
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo("");//原授信分项编号
            cmisLmt0001LmtSubListReqDto.setParentId("");//父节点
            // 额度品种编号
            String limitSubNo = "";
            ResultDto<List<LmtSubPrdMappConfDto>> listResultDto = cmisLmtClientService.selectLimitSubNoByPrdId(lmtCrdReplyInfo.getPrdId());
            if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode()) && CollectionUtils.nonEmpty(listResultDto.getData())) {
                limitSubNo = listResultDto.getData().get(0).getLimitSubNo();
            } else {
                limitSubNo = lmtCrdReplyInfo.getPrdId();
            }
            cmisLmt0001LmtSubListReqDto.setLimitSubNo(limitSubNo);// 授信品种编号
            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtCrdReplyInfo.getPrdName()); //授信品种名称
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02); //操作类型
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt()); // 授信金额
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType()); // 币种
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());// 授信期限
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate()); // 额度起始日
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate()); // 额度到期日
            //cmisLmt0001LmtSubListReqDto.setBailPreRate();//批复保证金比例
            cmisLmt0001LmtSubListReqDto.setRateYear(lmtCrdReplyInfo.getExecRateYear()); //年利率
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtCrdReplyInfo.getGuarMode()); // 担保方式
            cmisLmt0001LmtSubListReqDto.setLmtGraper(lmtCrdReplyInfo.getLmtGraper()); //宽限期
            // 01--一次性额度
            cmisLmt0001LmtSubListReqDto.setIsRevolv("01".equals(lmtCrdReplyInfo.getLimitType()) ? "0" : "1"); // 是否可循环
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0"); // 是否预授信
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0"); // 是否低风险
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01); // 批复分项状态
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD); // 操作类型
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);

            // 调用额度接口
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-建立额度请求报文：" + cmisLmt0001ReqDto.toString());
            cmisLmt0001RespDto = cmisLmt0001Service.cmisLmt0001(cmisLmt0001ReqDto);
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-建立额度返回报文：" + cmisLmt0001RespDto.toString());
        } catch (Exception e) {
            logger.error("小微授信批复额度同步发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("小微授信批复额度同步结束【{}】", lmtCrdReplyInfo.getSurveySerno());
        }
        return cmisLmt0001RespDto;
    }

    /**
     * @param lmtCrdReplyInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/8/25 19:05
     * @version 1.0.0
     * @desc 前往额度系统替换额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0001RespDto sendCmisLmt0001ForReplace(LmtCrdReplyInfo lmtCrdReplyInfo, String oldSurveySerno, String oldReplyNO) {
        logger.info("授信批复 - 前往额度系统替换额度开始，新批复台账【{}】、新分项编号【{}】、原批复台账【{}】、原分项编号【{}】", lmtCrdReplyInfo.getSurveySerno(),
                lmtCrdReplyInfo.getReplySerno(), oldSurveySerno, oldReplyNO);
        CmisLmt0001ReqDto cmisLmt0001ReqDto = null;
        CmisLmt0001RespDto cmisLmt0001RespDto = null;
        try {
            // 1、前往额度系统建立额度
            // 组装额度系统报文
            cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
            cmisLmt0001ReqDto.setInstuCde(getInstuCde(lmtCrdReplyInfo.getManagerBrId()));
            cmisLmt0001ReqDto.setAccNo(lmtCrdReplyInfo.getSurveySerno());//批复台账编号
            cmisLmt0001ReqDto.setOrigiAccNo(oldSurveySerno);// 批复原台账编号
            cmisLmt0001ReqDto.setCurType(lmtCrdReplyInfo.getCurType());//币种
            cmisLmt0001ReqDto.setCusId(lmtCrdReplyInfo.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtCrdReplyInfo.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("1");//是否生成新批复台账 1-- 覆盖
            cmisLmt0001ReqDto.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());//授信金额
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);//批复台账状态
            cmisLmt0001ReqDto.setTerm(lmtCrdReplyInfo.getAppTerm());//授信期限
            cmisLmt0001ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());//起始日
            cmisLmt0001ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());//到期日
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);//批复台账状态
            cmisLmt0001ReqDto.setManagerId(lmtCrdReplyInfo.getManagerId());//责任人
            cmisLmt0001ReqDto.setManagerBrId(lmtCrdReplyInfo.getManagerBrId());//责任机构
            cmisLmt0001ReqDto.setInputId(lmtCrdReplyInfo.getInputId());//登记人
            cmisLmt0001ReqDto.setInputBrId(lmtCrdReplyInfo.getInputBrId());//登记机构
            cmisLmt0001ReqDto.setInputDate(lmtCrdReplyInfo.getInputDate());//登记日期
            /* 额度分项*/
            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());//授信分项编号
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo(oldReplyNO);//原授信分项编号
            cmisLmt0001LmtSubListReqDto.setParentId("");//父节点
            cmisLmt0001LmtSubListReqDto.setLimitSubNo(lmtCrdReplyInfo.getPrdId());// 授信品种编号
            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtCrdReplyInfo.getPrdName()); //授信品种名称
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02); //操作类型
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt()); // 授信金额
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType()); // 币种
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());// 授信期限
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate()); // 额度起始日
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate()); // 额度到期日
            //cmisLmt0001LmtSubListReqDto.setBailPreRate();//批复保证金比例
            cmisLmt0001LmtSubListReqDto.setRateYear(lmtCrdReplyInfo.getExecRateYear()); //年利率
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtCrdReplyInfo.getGuarMode()); // 担保方式
            cmisLmt0001LmtSubListReqDto.setLmtGraper(lmtCrdReplyInfo.getLmtGraper()); //宽限期
            // 01--一次性额度
            cmisLmt0001LmtSubListReqDto.setIsRevolv("01".equals(lmtCrdReplyInfo.getLimitType()) ? "0" : "1"); // 是否可循环
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0"); // 是否预授信
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0"); // 是否低风险
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01); // 批复分项状态
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD); // 操作类型
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);

            // 调用额度接口
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-替换额度请求报文：" + cmisLmt0001ReqDto.toString());
            cmisLmt0001RespDto = cmisLmt0001Service.cmisLmt0001(cmisLmt0001ReqDto);
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-替换额度返回报文：" + cmisLmt0001RespDto.toString());
        } catch (Exception e) {
            logger.error("小微授信批复额度同步发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("小微授信批复额度同步结束【{}】", lmtCrdReplyInfo.getSurveySerno());
        }
        return cmisLmt0001RespDto;
    }


    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 合同校验接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0009RespDto sendCmisLmt0009(CtrLoanCont ctrLoanCont) {
        logger.info("合同校验接口 - 开始【{}】", ctrLoanCont.getContNo());
        CmisLmt0009RespDto cmisLmt0009RespDto = null;
        try {

            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0009ReqDto.setInstuCde(getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0009ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0009ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0009ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0009ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0009ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0009ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
            cmisLmt0009ReqDto.setOrigiDealBizNo(ctrLoanCont.getIqpSerno());//原交易业务编号
            cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
            cmisLmt0009ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
            cmisLmt0009ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0009ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0009ReqDto.setBelgLine(CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04);//零售
            cmisLmt0009ReqDto.setInputId(ctrLoanCont.getInputId());
            cmisLmt0009ReqDto.setInputBrId(ctrLoanCont.getInputBrId());
            cmisLmt0009ReqDto.setInputDate(ctrLoanCont.getInputDate());
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDto.setLmtType("01");//额度类型
            cmisLmt0009OccRelListReqDto.setLmtSubNo(ctrLoanCont.getReplyNo());//额度分项编号
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(ctrLoanCont.getContAmt());//占用总额(折人民币)
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
            cmisLmt0009ReqDto.setBussStageType("02");//合同阶段

            // 调用额度接口
            logger.info("根据业务合同编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
            cmisLmt0009RespDto = cmisLmt0009Service.cmisLmt0009(cmisLmt0009ReqDto);
            logger.info("根据业务合同编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());

        } catch (Exception e) {
            logger.error("合同校验接口发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("合同校验接口结束【{}】", ctrLoanCont.getContNo());
        }
        return cmisLmt0009RespDto;
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 合同占用接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0011RespDto sendCmisLmt0011(CtrLoanCont ctrLoanCont) {
        logger.info("合同占用接口 - 开始【{}】", ctrLoanCont.getContNo());
        CmisLmt0011RespDto cmisLmt0011RespDto = null;
        try {

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(ctrLoanCont.getIqpSerno());//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiRecoverType("1");//原交易属性
            cmisLmt0011ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0011ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
            cmisLmt0011ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
            cmisLmt0011ReqDto.setBelgLine("04");//零售条线
            cmisLmt0011ReqDto.setBussStageType("02");//合同阶段


            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType("01");//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(ctrLoanCont.getReplyNo());//额度分项编号
            cmisLmt0011OccRelListDto.setBizTotalAmt(ctrLoanCont.getContAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmt(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            // 调用额度接口
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-合同占用请求报文：" + cmisLmt0011ReqDto.toString());
            cmisLmt0011RespDto = cmisLmt0011Service.cmisLmt0011(cmisLmt0011ReqDto);
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-合同占用请求报文：" + cmisLmt0011RespDto.toString());

        } catch (Exception e) {
            logger.error("合同占用接口接口发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("合同占用接口接口结束【{}】", ctrLoanCont.getContNo());
        }
        return cmisLmt0011RespDto;
    }


    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 台账校验接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0010RespDto sendCmisLmt0010(PvpLoanApp pvpLoanApp) {
        logger.info("台账校验接口 - 开始【{}】", pvpLoanApp.getPvpSerno());
        CmisLmt0010RespDto cmisLmt0010RespDto = null;
        try {

            //1. 向额度系统发送接口：校验额度
            CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
            cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0010ReqDto.setInstuCde(getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0010ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号

            CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
            cmisLmt0010ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());
            cmisLmt0010ReqDealBizListDto.setIsFollowBiz("0");
            //cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo(pvpLoanApp.getBillNo());
            //cmisLmt0010ReqDealBizListDto.setOrigiRecoverType("06");
            //cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus("200");
            //cmisLmt0010ReqDealBizListDto.setOrigiBizAttr("2");
            cmisLmt0010ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0010ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0010ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0010ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0010ReqDealBizListDto.setDealBizAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
            cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);

            // 调用额度接口
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验请求报文：" + cmisLmt0010ReqDto.toString());
            cmisLmt0010RespDto = cmisLmt0010Service.cmisLmt0010(cmisLmt0010ReqDto);
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验返回报文：" + cmisLmt0010RespDto.toString());

        } catch (Exception e) {
            logger.error("台账校验接口发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("台账校验接口结束【{}】", pvpLoanApp.getPvpSerno());
        }
        return cmisLmt0010RespDto;
    }


    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 台账占用接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0013RespDto sendCmisLmt0013(PvpLoanApp pvpLoanApp) {
        logger.info("台账占用接口 - 开始【{}】", pvpLoanApp.getPvpSerno());
        CmisLmt0013RespDto cmisLmt0013RespDto = null;
        try {

            CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
            cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0013ReqDto.setInstuCde(getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0013ReqDto.setSerno(pvpLoanApp.getPvpSerno());
            cmisLmt0013ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisLmt0013ReqDto.setInputDate(pvpLoanApp.getInputDate());
            cmisLmt0013ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisLmt0013ReqDto.setBizNo(pvpLoanApp.getContNo());

            List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();

            CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
            cmisLmt0013ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
            cmisLmt0013ReqDealBizListDto.setIsFollowBiz("0");//是否无缝衔接
            //cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo(pvpLoanApp.getBillNo());//原交易业务编号
            //cmisLmt0013ReqDealBizListDto.setDealBizStatus("200");//原交易业务状态
            //cmisLmt0013ReqDealBizListDto.setOrigiRecoverType("06");//原交易业务恢复类型
            //cmisLmt0013ReqDealBizListDto.setOrigiBizAttr("2");//原交易属性
            cmisLmt0013ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0013ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0013ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0013ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0013ReqDealBizListDto.setPrdTypeProp(pvpLoanApp.getPrdTypeProp());//产品类型属性
            cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setStartDate(pvpLoanApp.getLoanStartDate());
            cmisLmt0013ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            cmisLmt0013ReqDealBizListDto.setDealBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);
            cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
            cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);

            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账占用请求报文：" + cmisLmt0013ReqDto.toString());
            cmisLmt0013RespDto = cmisLmt0013Service.cmisLmt0013(cmisLmt0013ReqDto);
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账占用返回报文：" + cmisLmt0013RespDto.toString());
        } catch (Exception e) {
            logger.error("合同占用接口接口发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("合同占用接口接口结束【{}】", pvpLoanApp.getPvpSerno());
        }
        return cmisLmt0013RespDto;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 更新台账编号接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0029RespDto sendCmisLmt0029(PvpLoanApp pvpLoanApp) {
        logger.info("贷款出账调额 - 开始【{}】", pvpLoanApp.getPvpSerno());
        CmisLmt0029RespDto cmisLmt0029RespDto = null;
        try {

            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
            cmisLmt0029ReqDto.setDealBizNo(pvpLoanApp.getPvpSerno());//交易流水号
            cmisLmt0029ReqDto.setOriginAccNo(pvpLoanApp.getBillNo());//原台账编号
            cmisLmt0029ReqDto.setNewAccNo(pvpLoanApp.getBillNo());//新台账编号
            cmisLmt0029ReqDto.setStartDate("");//合同起始日
            cmisLmt0029ReqDto.setEndDate("");//合同到期日
            cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
            logger.info("贷款出账调额度出账通知" + pvpLoanApp.getPvpSerno() + "，前往额度系统通知出账开始");
            cmisLmt0029RespDto = cmisLmt0029Service.cmisLmt0029(cmisLmt0029ReqDto);
            logger.info("贷款出账调额度出账通知" + pvpLoanApp.getPvpSerno() + "，前往额度系统通知出账结束");

        } catch (Exception e) {
            logger.error("贷款出账调额发生异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("贷款出账调额结束【{}】", pvpLoanApp.getPvpSerno());
        }
        return cmisLmt0029RespDto;
    }

    /**
     * @param ctrLoanCont,pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-6 23:09:32
     * @version 1.0.0
     * @desc 线上产品自动生成合同台账
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CmisLmt0057RespDto sendCmisLmt0057(CtrLoanCont ctrLoanCont, PvpLoanApp pvpLoanApp, LmtCrdReplyInfo lmtCrdReplyInfo) {
        CmisLmt0057RespDto cmisLmt0057RespDto = null;
        try {
            CmisLmt0057ReqDto cmisLmt0057ReqDto = new CmisLmt0057ReqDto();
            cmisLmt0057ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0057ReqDto.setInstuCde(getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0057ReqDto.setBelgLine(ctrLoanCont.getBelgLine());//所属条线
            cmisLmt0057ReqDto.setSerno(UUID.randomUUID().toString());//交易流水号
            cmisLmt0057ReqDto.setAccNo(lmtCrdReplyInfo.getSurveySerno());//批复台账编号
            cmisLmt0057ReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());//批复分项编号
            cmisLmt0057ReqDto.setIsRevolv("01".equals(lmtCrdReplyInfo.getLimitType()) ? "0" : "1");//是否循环
            cmisLmt0057ReqDto.setSuitGuarWay(ctrLoanCont.getGuarWay());//担保方式
            cmisLmt0057ReqDto.setAvlAmt(ctrLoanCont.getContAmt());//授信金额
            cmisLmt0057ReqDto.setLmtStartDate(lmtCrdReplyInfo.getReplyStartDate());//额度起始日期
            cmisLmt0057ReqDto.setLmtEndDate(lmtCrdReplyInfo.getReplyEndDate());//额度到期日期
            cmisLmt0057ReqDto.setBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0057ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型 1 一般合同 2 最高额合同
            cmisLmt0057ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            cmisLmt0057ReqDto.setBizStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0057ReqDto.setBizEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0057ReqDto.setInputId(pvpLoanApp.getInputId());//登记人
            cmisLmt0057ReqDto.setInputBrId(pvpLoanApp.getInputBrId());//登记机构
            cmisLmt0057ReqDto.setInputDate(pvpLoanApp.getInputDate());//登记日期
            cmisLmt0057ReqDto.setDealBizNo(pvpLoanApp.getBillNo());//业务台账编号
            cmisLmt0057ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0057ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0057ReqDto.setPrdNo(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0057ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0057ReqDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0057ReqDto.setDealBizAmtCny(pvpLoanApp.getPvpAmt());//台账占用总额(人民币)
            cmisLmt0057ReqDto.setDealBizSpacAmtCny(pvpLoanApp.getPvpAmt());//台账占用敞口(人民币)
            cmisLmt0057ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
            cmisLmt0057ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
            cmisLmt0057ReqDto.setAccStartDate(pvpLoanApp.getLoanStartDate());//台账起始日
            cmisLmt0057ReqDto.setAccEndDate(pvpLoanApp.getLoanEndDate());//台账到期日
            cmisLmt0057ReqDto.setLimitSubNo("");//授信品种编号
            cmisLmt0057ReqDto.setLimitSubName("");//授信品种名称
            cmisLmt0057ReqDto.setIsLriskLmt("0");//是否低风险
            //发送额度服务
            logger.info("线上产品自动生成合同台账【{}】前往额度系统通知出账开始", JSON.toJSONString(cmisLmt0057ReqDto));
            cmisLmt0057RespDto = cmisLmt0057Service.cmisLmt0057(cmisLmt0057ReqDto);
            logger.info("线上产品自动生成合同台账前往额度系统通知出账结束，返回【{}】", JSON.toJSONString(cmisLmt0057RespDto));
        } catch (Exception e) {
            logger.error("线上产品自动生成合同台账异常【{}】", e.getMessage());
            throw e;
        } finally {
            logger.info("线上产品自动生成合同台账结束【{}】", pvpLoanApp.getPvpSerno());
        }
        return cmisLmt0057RespDto;
    }

    /**
     * @param managerId
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/9/2 11:16
     * @version 1.0.0
     * @desc 根据客户经理查询分中心负责人信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<AdminSmUserDto> getCenterManagerIdInfo(String managerId) {
        logger.info("根据客户经理【{}】查询,分中心负责人开始！", managerId);

        // 客户经理用户对象
        AdminSmUserDto adminSmUserDto = null;
        // 分中心用户对象
        AdminSmUserDto adminSmUserDtoCenter = null;

        // 客户经理所在机构
        String orgId = "";
        // 分中心负责人工号
        String managerIdCenter = "";
        try {
            // 1、根据客户经理查询岗位信息
            // 调用oca接口查询机构
            adminSmUserDto = commonService.getByLoginCode(managerId);
            // 校验非空
            if (Objects.isNull(adminSmUserDto)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到客户经理用户信息！");
            }
            // 获取所在机构
            orgId = adminSmUserDto.getOrgId();

            // 2、根据客户经理获取机构信息,根据机构信息查询区域信息
            logger.info("根据机构号【{}】查询区域信息开始", orgId);
            List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
            if (CollectionUtils.isEmpty(areaOrgDtos)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户机构所属区域信息！");
            }
            logger.info("根据机构号【{}】查询区域信息结束,区域编号【{}】", orgId, areaOrgDtos.get(0).getAreaNo());

            // 3、一个机构只属于一个区域，取第一条即可，根据区域编号查询区域负责人信息
            logger.info("根据区域编号【{}】查询区域负责人信息开始", areaOrgDtos.get(0).getAreaNo());
            List<AreaUser> areaUsers = areaUserService.selectByAreaNo(areaOrgDtos.get(0).getAreaNo());
            if (CollectionUtils.isEmpty(areaUsers)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到区域负责人信息！");
            }
            logger.info("区域编号【{}】查询区域负责人信息结束,负责人工号【{}】", orgId, areaUsers.get(0).getUserNo());
            managerIdCenter = areaUsers.get(0).getUserNo();

            // 调用oca接口查询机构
            adminSmUserDtoCenter = commonService.getByLoginCode(managerIdCenter);
            // 校验非空
            if (Objects.isNull(adminSmUserDtoCenter)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到分中心负责人用户信息！");
            }
        } catch (BizException e) {
            e.printStackTrace();
        }
        return new ResultDto<AdminSmUserDto>(adminSmUserDtoCenter);
    }


    /**
     * @param orgId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.String>
     * @author 王玉坤
     * @date 2021/9/7 16:40
     * @version 1.0.0
     * @desc 根据分中心负责人返回其名下小微客户经理信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> getmanagerAreaName(String orgId) {
        String managerArea = StringUtils.EMPTY;
        try {
            // 2、根据客户经理获取机构信息,根据机构信息查询区域信息
            logger.info("根据机构号【{}】查询区域信息开始", orgId);
            List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
            if (CollectionUtils.isEmpty(areaOrgDtos)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户机构所属区域信息！");
            }
            logger.info("根据机构号【{}】查询区域信息结束，返回信息为：【{}】", orgId, JSON.toJSON(areaOrgDtos.get(0)));
            AreaManager areaManager = areaManagerService.selectByPrimaryKey(areaOrgDtos.get(0).getAreaNo());
            managerArea = areaManager.getAreaName();
        } catch (BizException e) {
            logger.error("根据机构号【{}】查询区域信息异常,异常信息：【{}】", orgId, JSON.toJSON(e));
            new ResultDto<String>(StringUtils.EMPTY);
        }
        return new ResultDto<String>(managerArea);
    }

    /**
     * 获取 小微分中心/小微分部部长下的客户经理
     *
     * @return
     * @创建人：周茂伟
     */
    public Map<String, List<String>> queryManagerByXwRole(String userNo) {
        Map<String, List<String>> map = new HashMap<>();
        QueryModel model = new QueryModel();
        model.addCondition("userNo", userNo);
        // 1、查询用户是哪个分中心负责人
        List<AreaUser> areaUsers = areaUserService.selectAll(model);
        for (AreaUser areaUser : areaUsers) {
            List<String> tempList = null;
            if (map.containsKey(areaUser.getUserNo())) {
                tempList = map.get(areaUser.getUserNo());
            } else {
                tempList = new ArrayList<>();
                map.put(areaUser.getUserNo(), tempList);
            }
            // 根据区域查询下属机构信息
            List<AreaOrg> areaOrgList = areaOrgService.selectByAreaNo(areaUser.getAreaNo());
            // 根据机构信息遍历机构下用户信息，一个区域可能关联多个机构
            // 根据机构信息查询用户信息
            List<AreaAdminUser> areaAdminUserList = areaAdminUserService.selectByOrgCodes(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            List<String> temp2List = areaAdminUserList.stream().map(AreaAdminUser::getUserNo).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(temp2List)) {
                tempList.addAll(temp2List);
            }
        }
        // 2、查询用户是哪个分部部长
        List<FbManager> fbList = fbManagerService.selectAll(model);
        for (FbManager fbManager : fbList) {
            String bzUserCode = fbManager.getMinisterId();
            List<String> tempList = null;
            if (map.containsKey(bzUserCode)) {
                tempList = map.get(bzUserCode);
            } else {
                tempList = new ArrayList<>();
                map.put(bzUserCode, tempList);
            }
            // 获取分部对应的所管辖所有机构
            List<AreaOrg> areaOrgList = areaOrgService.selectByFbCode(fbManager.getFbCode());
            // 根据机构信息查询用户信息
            List<AreaAdminUser> areaAdminUserList = areaAdminUserService.selectByOrgCodes(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            List<String> temp2List = areaAdminUserList.stream().map(AreaAdminUser::getUserNo).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(temp2List)) {
                tempList.addAll(temp2List);
            }
        }
        return map;
    }

    /**
     * @param extSerno, bizType, iqpLoanApp
     * @return void
     * @author 王玉坤
     * @date 2021/9/2 15:45
     * @version 1.0.0
     * @desc 处理是否用印任务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendYKTask(String extSerno, String bizType, String userId, String cusName) {
        logger.info("处理是否用印任务开始！");
        // 查询审批结果
        BusinessInformation businessInformation = null;
        try {
            businessInformation = businessInformationService.selectByPrimaryKey(extSerno, bizType);
            if (null != businessInformation) {
                // 是否电子用印 1--是
                if ("1".equals(businessInformation.getSignet())) {
                    // 前往用印系统推送用印任务
                    this.sendYk(userId, extSerno, cusName);
                }

            } else {
                logger.info("流水号【{}】，未查询到用印任务！", extSerno);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param userId, iqpLoanApp
     * @return void
     * @author 王玉坤
     * @date 2021/9/2 23:54
     * @version 1.0.0
     * @desc 发送印控系统
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void sendYk(String userId, String extSerno, String cusName) {
        logger.info("流水号【{}】,前往印控系统请求用印开始,用印人【{}】", extSerno, cusName);
        // 印控系统请求报文
        Yky001ReqDto yky001ReqDto = null;
        // 印控系统返回报文
        Yky001RespDto yky001RespDto = null;

        // 用印人员用户信息
        AdminSmUserDto adminSmUserDto = null;
        try {
            // 1、查询用印人信息
            adminSmUserDto = commonService.getByLoginCode(userId);
            // 校验非空
            if (Objects.isNull(adminSmUserDto)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到客户经理用户信息！");
            }
            yky001ReqDto = new Yky001ReqDto();
            yky001ReqDto.setOaId(extSerno);
            yky001ReqDto.setOperatorCode(userId);
            yky001ReqDto.setOperatorName(adminSmUserDto.getUserName());
            yky001ReqDto.setOrgNo("001023011");
            yky001ReqDto.setOrgName("信贷集中作业中心");
            yky001ReqDto.setTitle(cusName);
            yky001ReqDto.setAccessory("/dscms/");
            yky001ReqDto.setApplyTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            yky001ReqDto.setStampEmpOrg("001023011");

            List<cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List> listList = new ArrayList<>();
            cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List list1 = new cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List();

            list1.setTradeCode("00002");
            list1.setTradeCodeName("信贷合同专用章");
            list1.setApplyNum("100");
            listList.add(list1);

            cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List list2 = new cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List();
            list2.setTradeCode("00003");
            list2.setTradeCodeName("抵押登记专用章");
            list2.setApplyNum("100");
            listList.add(list2);

            yky001ReqDto.setList(listList);

            // 调用印控系统
            yky001RespDto = yky001Service.yky001(yky001ReqDto);
        } catch (Exception e) {
            logger.info("流水号【{}】,前往印控系统请求异常,用印人【{}】", extSerno, cusName);
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("流水号【{}】,前往印控系统请求结束,用印人【{}】", extSerno, cusName);
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/9/15 19:03
     * @version 1.0.0
     * @desc 小微合同校验与占用
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto contLmtForXw(CtrLoanCont ctrLoanCont) {
        CmisLmt0011RespDto cmisLmt0011RespDto = null;
        // 批复信息
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        try {
            // 1、判断该笔合同是否为无还本续贷合同
            // 判断是否无缝衔接
            String isFollowBiz = "0";
            // 原交易业务编号
            String origiDealBizNo = "";
            // 原交易业务状态
            String origiDealBizStatus = "";
            // 原交易恢复类型
            String origiRecoverType = "";
            // 原交易属性
            String origiBizAttr = "";
            // 分项编号
            String lmtSubNo = "";
            logger.info("根据批复编号【{}】获取批复信息开始！", ctrLoanCont.getReplyNo());
            lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(ctrLoanCont.getReplyNo());
            logger.info("根据批复编号【{}】获取批复信息结束！查询结果【{}】", ctrLoanCont.getReplyNo(), Objects.nonNull(lmtCrdReplyInfo));
            if (Objects.isNull(lmtCrdReplyInfo)) {
                throw BizException.error(null, null, " 未查询到批复信息！");
            }

            // 额度分项编号 无论无还本续贷、还本续贷、普通贷款，一律占用新批复额度
            lmtSubNo = ctrLoanCont.getReplyNo();
            if ("1".equals(lmtCrdReplyInfo.getIsWxbxd())) {
                isFollowBiz = "1";
                // 获取原合同编号，根据原合同编号查询原批复编号
                String oldContNo = lmtCrdReplyInfo.getXdOrigiContNo();
                logger.info("根据原合同编号【{}】查询原批复编号开始！", oldContNo);
                CtrLoanCont olsCtrLoanCont = ctrLoanContService.selectByPrimaryKey(oldContNo);
                logger.info("根据原合同编号【{}】查询原批复编号结束！", oldContNo);

                if (Objects.isNull(lmtCrdReplyInfo) || Objects.equals(org.apache.commons.lang.StringUtils.EMPTY, lmtCrdReplyInfo.getReplySerno())) {
                    throw BizException.error(null, null, " 根据原合同号未查询到原批复信息！");
                }
                logger.info("根据原合同编号【{}】查询到原批复编号【{}】", oldContNo, lmtCrdReplyInfo.getReplySerno());
            }


            //1. 向额度系统发送接口：额度校验
            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0009ReqDto.setInstuCde(getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0009ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0009ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0009ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0009ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0009ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0009ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0009ReqDto.setIsFollowBiz(isFollowBiz);//是否无缝衔接
            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
            cmisLmt0009ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0009ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0009ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0009ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性
            cmisLmt0009ReqDto.setDealBizAmt(ctrLoanCont.getHighAvlAmt());//交易业务金额
            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
            cmisLmt0009ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0009ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0009OccRelListReqDto.setLmtSubNo(lmtSubNo);//额度分项编号STD_ZB_LMT_TYPE_01
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(ctrLoanCont.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)


            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
            CmisLmt0009RespDto cmisLmt0009RespDto = cmisLmt0009Service.cmisLmt0009(cmisLmt0009ReqDto);
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());

            //2. 向额度系统发送接口：占用额度
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsFollowBiz(isFollowBiz);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性
            cmisLmt0011ReqDto.setDealBizAmt(ctrLoanCont.getHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0011ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态  TODO 王浩 2021年8月2日16:42:22 这里必须传200  不然会发生后续其他问题 谨记
            cmisLmt0011ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(ctrLoanCont.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(lmtSubNo);//额度分项编号
            cmisLmt0011OccRelListDto.setBizTotalAmt(ctrLoanCont.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmt(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(ctrLoanCont.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)


            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011ReqDto);
            cmisLmt0011RespDto = cmisLmt0011Service.cmisLmt0011(cmisLmt0011ReqDto);
            logger.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度占用返回报文：" + cmisLmt0011RespDto.toString());
        } catch (Exception e) {
            logger.error("小微-合同额度占用异常异常：", e);
            throw e;
        }
        return new ResultDto().message("合同额度占用成功");
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author 王玉坤
     * @date 2021/9/21 23:21
     * @version 1.0.0
     * @desc 小微台账占用、校验额度信息开始
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto loanLmtForXw(PvpLoanApp pvpLoanApp) {
        CmisLmt0011RespDto cmisLmt0011RespDto = null;
        try {
            // 1、判断该笔合同是否为无还本续贷合同
            // 判断是否无缝衔接
            String isFollowBiz = "0";
            // 原交易业务编号
            String origiDealBizNo = "";
            // 原交易业务状态
            String origiDealBizStatus = "";
            // 原交易恢复类型
            String origiRecoverType = "";
            // 原交易属性
            String origiBizAttr = "";

            // 判断是否是无还本续贷信息 3--借新还旧 6--无还本续贷
            if ("6".equals(pvpLoanApp.getLoanModal()) || "3".equals(pvpLoanApp.getLoanModal())) {
                isFollowBiz = "1";
            }


            //1. 向额度系统发送接口：校验额度
            CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
            cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0010ReqDto.setInstuCde(getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0010ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号
            cmisLmt0010ReqDto.setBelgLine("01");// 小微业务条线

            CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
            cmisLmt0010ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());
            cmisLmt0010ReqDealBizListDto.setIsFollowBiz(isFollowBiz);
            cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo(origiDealBizNo);
            cmisLmt0010ReqDealBizListDto.setOrigiRecoverType(origiRecoverType);
            cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus(origiDealBizStatus);
            cmisLmt0010ReqDealBizListDto.setOrigiBizAttr(origiBizAttr);
            cmisLmt0010ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0010ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0010ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0010ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0010ReqDealBizListDto.setDealBizAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(pvpLoanApp.getPvpAmt());
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0010ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
            cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);

            // 调用额度接口
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验请求报文：" + cmisLmt0010ReqDto.toString());
            CmisLmt0010RespDto cmisLmt0010RespDto = cmisLmt0010Service.cmisLmt0010(cmisLmt0010ReqDto);
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账校验返回报文：" + cmisLmt0010RespDto.toString());

            //2. 向额度系统发送接口：占用额度
            CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
            cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0013ReqDto.setInstuCde(getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0013ReqDto.setSerno(pvpLoanApp.getPvpSerno());
            cmisLmt0013ReqDto.setInputBrId(pvpLoanApp.getInputBrId());
            cmisLmt0013ReqDto.setInputDate(pvpLoanApp.getInputDate());
            cmisLmt0013ReqDto.setInputId(pvpLoanApp.getInputId());
            cmisLmt0013ReqDto.setBizNo(pvpLoanApp.getContNo());
            cmisLmt0013ReqDto.setBelgLine("01"); // 小微业务条线

            List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();

            CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
            cmisLmt0013ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());//台账编号
            cmisLmt0013ReqDealBizListDto.setIsFollowBiz(isFollowBiz);
            cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo(origiDealBizNo);
            cmisLmt0013ReqDealBizListDto.setOrigiRecoverType(origiRecoverType);
            cmisLmt0013ReqDealBizListDto.setOrigiDealBizStatus(origiDealBizStatus);
            cmisLmt0013ReqDealBizListDto.setOrigiBizAttr(origiBizAttr);
            cmisLmt0013ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
            cmisLmt0013ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
            cmisLmt0013ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
            cmisLmt0013ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
            cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(pvpLoanApp.getPvpAmt());
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setStartDate(pvpLoanApp.getLoanStartDate());
            cmisLmt0013ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
            cmisLmt0013ReqDealBizListDto.setDealBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);
            cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
            cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);

            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账占用请求报文：" + cmisLmt0013ReqDto.toString());
            CmisLmt0013RespDto cmisLmt0013RespDto = cmisLmt0013Service.cmisLmt0013(cmisLmt0013ReqDto);
            logger.info("根据业务申请编号【" + pvpLoanApp.getPvpSerno() + "】前往额度系统-台账占用返回报文：" + cmisLmt0013RespDto.toString());
        } catch (Exception e) {
            logger.error("小微-合同额度占用异常异常：", e);
            throw e;
        }
        return new ResultDto().message("合同额度占用成功");
    }


    /**
     * @param orgCode
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-9-16 22:05:40
     * @version 1.0.0
     * @desc 根据机构返回金融机构代码
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getInstuCde(String orgCode) {
        String instuCde = CmisCommonConstants.INSTUCDE_001;//C1115632000023	张家港农村商业银行

        if (orgCode.startsWith("80")) {//寿光
            instuCde = CmisCommonConstants.INSTUCDE_002;// C1102137000013	寿光村镇银行
        } else if (orgCode.startsWith("81")) {//东海
            instuCde = CmisCommonConstants.INSTUCDE_003;//C1100832000011	东海村镇银行
        }

        return instuCde;
    }

    /**
     * @param startDate, endDate
     * @return int
     * @author 王玉坤
     * @date 2021/9/19 0:18
     * @version 1.0.0
     * @desc 根据日期计算相差月份 yyyy-MM-dd
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int getBetweenMonth(String startDate, String endDate) throws Exception {
        return CmisCommonUtils.getBetweenMonth(startDate, endDate);
    }

    /**
     * @param imageApprDto
     * @return void
     * @author 王玉坤
     * @date 2021/9/29 11:39
     * @version 1.0.0
     * @desc 推送影像审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> sendImage(ImageApprDto imageApprDto) {
        logger.info("推送影像审批信息交易开始,影像流水号【{}】", imageApprDto.getDocId());
        ResultDto<String> responseEntityResultDto = null;
        try {
            responseEntityResultDto = dscms2ImageClientService.imageappr(imageApprDto);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, responseEntityResultDto.getCode()) && responseEntityResultDto.getData() != null) {
                if (Objects.equals("true", responseEntityResultDto.getData())) {
                    logger.info("推送影像审批信息交易结束,交易成功，影像流水号【{}】", imageApprDto.getDocId());
                } else {
                    logger.info("推送影像审批信息交易结束,交易失败，影像流水号【{}】", imageApprDto.getDocId());
                }
            } else {
                logger.info("推送影像审批信息交易结束,交易异常，影像流水号【{}】", imageApprDto.getDocId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        logger.info("推送影像审批信息交易结束,影像流水号【{}】", imageApprDto.getDocId());
        return responseEntityResultDto;
    }

    /**
     * @param billNo，isFreeIntere，freeIntereDays
     * @return void
     * @author 王玉坤
     * @date 2021/10/10 17:28
     * @version 1.0.0
     * @desc 保存免息信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void saveAccLoanFreeInter(String billNo, String isFreeIntere, int freeIntereDays) {
        logger.info("借据号【{}】，保存免息信息开始--------------------", billNo);
        logger.info("借据号【{}】，查询免息信息开始--------------------");
        AccLoanFreeInter accLoanFreeInter = accLoanFreeInterService.selectByPrimaryKey(billNo);
        logger.info("借据号【{}】，查询免息信息结束--------------------", billNo);

        // 有则更新、无则插入
        if (Objects.isNull(accLoanFreeInter)) {
            logger.info("保存免息信息开始，借据号【{}】------------------", billNo);
            accLoanFreeInter = new AccLoanFreeInter();
            accLoanFreeInter.setBillNo(billNo);
            accLoanFreeInter.setIsFreeInter(isFreeIntere);
            accLoanFreeInter.setFreeInterDay(freeIntereDays);

            accLoanFreeInterService.insert(accLoanFreeInter);
            logger.info("保存免息信息结束，借据号【{}】------------------", billNo);
        } else {
            logger.info("更新免息信息开始，借据号【{}】------------------", billNo);
            accLoanFreeInter.setIsFreeInter(isFreeIntere);
            accLoanFreeInter.setFreeInterDay(freeIntereDays);
            accLoanFreeInter.setUpdateTime(DateUtils.getCurrDate());

            accLoanFreeInterService.update(accLoanFreeInter);
            logger.info("更新免息信息结束，借据号【{}】------------------", billNo);
        }
    }

    /**
     * @param discountCode，businessNo，businessType businessType 01：合同 02：借据（当签订合同时传01；当生成借据号时传02；）
     * @return void
     * @author 王玉坤
     * @date 2021/10/10 17:28
     * @version 1.0.0
     * @desc 保存优惠券信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void saveDiscountCoupon(String discountCode, String businessNo, String businessType) {
        logger.info("业务编号【{}】，操作类型【{}】，优惠券码值【{}】 保存优惠券信息开始--------------------", businessNo, businessType, discountCode);
        DiscCouponBusinRel discCouponBusinRel = new DiscCouponBusinRel();
        discCouponBusinRel.setPkId(UUID.randomUUID().toString());
        discCouponBusinRel.setDiscountCode(discountCode);
        discCouponBusinRel.setSerno(businessNo);
        discCouponBusinRel.setBusiType(businessType);
        discCouponBusinRelService.insert(discCouponBusinRel);
        logger.info("业务编号【{}】，操作类型【{}】，优惠券码值【{}】 保存优惠券信息结束--------------------", businessNo, businessType, discountCode);

    }

    /**
     * 市民贷优惠券核销锁定
     *
     * @param discountCode 优惠券编号
     * @param businessNo   业务编号
     * @param businessType 业务类型 1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
     * @param cusId        客户号
     * @return Yx0003RespDto
     * @author 王玉坤
     */
    public Yx0003RespDto cancelAfterVerificaDiscount(String discountCode, String businessNo, String businessType, String cusId, String phone) {
        logger.info("yx0003--市民贷2.0优惠券核销锁定开始，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
        // 请求对象
        Yx0003ReqDto yx0003ReqDto = new Yx0003ReqDto();
        // 返回对象
        Yx0003RespDto yx0003RespDto = null;
        try {
            // 查询客户信息
            logger.info("根据客户号【{}】查询客户联系信息开始-----------", cusId);
            CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(cusId);
            logger.info("根据客户号【{}】查询客户联系信息结束-----------", cusId);
            if (Objects.isNull(cusIndivContactDto)) {
                logger.info("根据客户号【{}】未查询客户联系信息-----------", cusId);
            } else {
                phone = cusIndivContactDto.getMobileNo();
                logger.info("根据客户号【{}】查询客户联系信息,电话号码【{}】-----------", cusId, phone);
            }

            // 拼装报文
            // 唯一码
            yx0003ReqDto.setCardSecret(discountCode);
            // 电话号码
            yx0003ReqDto.setPhone(phone);
            // 状态 业务类型 1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
            yx0003ReqDto.setStatus(businessType);

            // 1：锁定
            if ("1".equals(businessType)) {
                yx0003ReqDto.setLoanContNo(businessNo);// 合同号
                yx0003ReqDto.setBillNo("");// 借据号
            } else if ("2".equals(businessType)) {// 2：核销
                yx0003ReqDto.setLoanContNo("");// 合同号
                yx0003ReqDto.setBillNo(businessNo);// 借据号
            }

            yx0003RespDto = yx0003Service.Yx0003(yx0003ReqDto);

        } catch (Exception e) {
            logger.info("市民贷2.0优惠券核销锁定异常，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        }
        logger.info("yx0003--市民贷2.0优惠券核销锁定结束，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
        return yx0003RespDto;
    }

    /**
     * 优惠券核销锁定
     *
     * @param discountCode 优惠券编号
     * @param businessNo   业务编号
     * @param businessType 业务类型 1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
     * @param cusId        客户号
     * @return Xwh003RespDto
     * @author 王玉坤
     */
    public Xwh003RespDto cancelAfterVerificaDiscountCoupon(String discountCode, String businessNo, String businessType, String cusId) {
        logger.info("Xwh003--优惠券核销锁定开始，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
        // 请求对象
        Xwh003ReqDto xwh003ReqDto = new Xwh003ReqDto();
        // 返回对象
        Xwh003RespDto xwh003RespDto = null;
        try {
            // 查询客户信息
            String certCode = "";
            logger.info("根据客户号【{}】查询客户联系信息开始-----------", cusId);
            ResultDto<CusBaseDto> cusBaseDto = iCusClientService.queryCusBaseByCusId(cusId);
            logger.info("根据客户号【{}】查询客户联系信息结束-----------", cusId);
            if (Objects.isNull(cusBaseDto) || Objects.isNull(cusBaseDto.getData())) {
                logger.info("根据客户号【{}】未查询客户联系信息-----------", cusId);
            } else {
                certCode = cusBaseDto.getData().getCertCode();
            }
            // 拼装报文
            // 唯一码
            xwh003ReqDto.setCardSecret(discountCode);
            // 客户号
            xwh003ReqDto.setCusId(cusId);
            // 状态 业务类型 1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
            xwh003ReqDto.setStatus(businessType);
            // 证件号码
            xwh003ReqDto.setIdCard(certCode);

            // 1：锁定
            if ("1".equals(businessType)) {
                xwh003ReqDto.setLoanContNo(businessNo);// 合同号
                xwh003ReqDto.setBillNo("");// 借据号
            } else if ("2".equals(businessType)) {// 2：核销
                xwh003ReqDto.setLoanContNo("");// 合同号
                xwh003ReqDto.setBillNo(businessNo);// 借据号
            }

            xwh003RespDto = xwh003Service.xwh003(xwh003ReqDto);

        } catch (Exception e) {
            logger.info("优惠券核销锁定异常，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        }
        logger.info("Xwh003--优惠券核销锁定结束，优惠券【{}】, 操作类型【{}】, 业务流水号【{}】--------", discountCode, businessType, businessNo);
        return xwh003RespDto;
    }


    /**
     * @param orgCode
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-10-11 10:19:38
     * @version 1.0.0
     * @desc 根据机构返回对应的普通贷款序列
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getDKSeq(String orgCode) {
        String dkSeq = "";
        if (orgCode.startsWith("80")) {//寿光普通贷款序列
            dkSeq = SeqConstant.SGCDK_SEQ;
        } else if (orgCode.startsWith("81")) {//东海普通贷款序列
            dkSeq = SeqConstant.DHCDK_SEQ;
        } else {//张家港普通贷款序列
            dkSeq = SeqConstant.ZRCDK_SEQ;
        }
        return dkSeq;
    }


    /**
     * @param contNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-10-11 16:14:27
     * @version 1.0.0
     * @desc 生成借据编号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getBillNo(String contNo) {
        String billNo = "";
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("contNo", contNo);
        List<PvpLoanApp> pvpLoanAppList1 = pvpLoanAppMapper.selectByModel(queryModel1);
        int pvpLoanAppCount1 = pvpLoanAppList1.size();
        boolean bool = true;
        while (bool) {
            pvpLoanAppCount1 += 1;
            billNo = contNo + String.format("%04d", pvpLoanAppCount1);
            QueryModel queryModel2 = new QueryModel();
            queryModel2.addCondition("billNo", billNo);
            List<PvpLoanApp> pvpLoanAppList2 = pvpLoanAppMapper.selectByModel(queryModel2);
            int pvpLoanAppCount2 = pvpLoanAppList2.size();
            if (pvpLoanAppCount2 == 0) {
                bool = false;
            }
        }

        return billNo;
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo>
     * @author 王玉坤
     * @date 2021/10/17 15:22
     * @version 1.0.0
     * @desc 根据客户编号查询结算账户信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<LstacctinfoDto> getAccNoListBycusId(QueryModel queryModel) {
        Dp2021ReqDto dp2021ReqDto = new Dp2021ReqDto();
        dp2021ReqDto.setChxunbis(100);
        dp2021ReqDto.setKehuhaoo(queryModel.getCondition().get("cusId").toString());
        dp2021ReqDto.setQishibis(1);
        Dp2021RespDto dp2021RespDto = Optional.ofNullable(dp2021Service.dp2021(dp2021ReqDto)).orElse(new Dp2021RespDto());
        List<Lstacctinfo> lstacctinfo = Optional.ofNullable(dp2021RespDto.getLstacctinfo()).orElse(new ArrayList<>());

        // 过滤结算账户
        lstacctinfo = lstacctinfo.stream().filter(s -> ("1".equals(s.getShfojshu()))).collect(Collectors.toList());
        List<LstacctinfoDto> list = new ArrayList<>();
        for (int i = 0; i < lstacctinfo.size(); i++) {
            Lstacctinfo record = lstacctinfo.get(i);
            LstacctinfoDto lstacctinfoDto = new LstacctinfoDto();
            BeanUtils.beanCopy(record, lstacctinfoDto);
            if (record.getKaihjigo().startsWith("80")) {
                lstacctinfoDto.setKaihjigomc("寿光村镇银行");
            } else if (record.getKaihjigo().startsWith("81")) {
                lstacctinfoDto.setKaihjigomc("东海村镇银行");
            } else {
                lstacctinfoDto.setKaihjigomc("张家港农村商业银行");
            }
            list.add(lstacctinfoDto);
        }
        // 筛选结算账户
        return list;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.dto.CfgAccountClassChooseDto
     * @author 王玉坤
     * @date 2021/10/23 20:49
     * @version 1.0.0
     * @desc 计算科目号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto caluAccountClass(PvpLoanApp pvpLoanApp) {
        String hxAccountClass = "";
        // 客户类别 1对公，2对私
        String publicPerson = "";
        // 业务类型（产品ID）
        String bizTypeSub = "";
        // 客户类型
        String cusType = "";
        // 城乡类型
        String cityVillage = "";
        // 企业类型 对公客户才有的字段
        String factoryType = "";
        // 主营业务 行业分类
        String mainDeal = "";
        // 涉农标识 个人客户中"是否农户"字段
        String farmFlag = "";
        // 涉农投向 涉农贷款投向
        String farmDirection = "";
        // 投向行业 贷款投向编号
        String directionOption = "";
        // 担保方式 担保方式细分
        String guarMode = "";
        // 贷款类别 贷款类别
        String loanType = "";
        // 期限 借据放款期限
        String loanTerm = "";

        // 首先查询出账申请记录，如果查询不到查询委托申请记录
        logger.info("科目计算，获取出账申请数据:{}", pvpLoanApp.toString());
        // 获取贷款出账数据
        // 业务类型（产品ID）
        bizTypeSub = pvpLoanApp.getPrdId();
        // 涉农贷款投向
        farmDirection = pvpLoanApp.getAgriLoanTer();
        // 贷款投向编号
        if (pvpLoanApp.getLoanTer() != null && !"".equals(pvpLoanApp.getLoanTer())) {
            directionOption = pvpLoanApp.getLoanTer().substring(0, 1);
        }
        // 担保方式细分
        guarMode = pvpLoanApp.getGuarMode();
        // 贷款类别
        loanType = pvpLoanApp.getLoanTypeDetail();
        // 借据放款期限
        loanTerm = pvpLoanApp.getLoanTerm();
        CfgAccountClassChooseDto cfgAccountClassChooseDto = new CfgAccountClassChooseDto();

        // 查询客户类型
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpLoanApp.getCusId());
        publicPerson = cusBaseClientDto.getCusCatalog();
        logger.info("科目计算，获取客户基本信息数据:{}", cusBaseClientDto.toString());
        if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(pvpLoanApp.getCusId()).getData();
            logger.info("科目计算，获取个人客户基本信息数据:{}", cusCorpDto.toString());
            cusType = cusCorpDto.getCusType();
            cityVillage = cusCorpDto.getCityType();
            factoryType = cusCorpDto.getConType();
            mainDeal = StringUtils.isBlank(cusCorpDto.getTradeClass()) ? "" : cusCorpDto.getTradeClass().substring(0, 1);
        } else {
            // 如果是个人客户
            ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(pvpLoanApp.getCusId());
            logger.info("科目计算，获取个人客户基本信息数据:{}", cusIndivDto.toString());
            cusType = cusIndivDto.getData().getCusType();
            farmFlag = cusIndivDto.getData().getAgriFlg();
        }
        // 期限转换
        if (Integer.parseInt(loanTerm) <= 12) {
            loanTerm = "0";// 短期
        } else {
            loanTerm = "1";// 中长期
        }

        cfgAccountClassChooseDto.setPublicPerson(publicPerson);
        cfgAccountClassChooseDto.setBizTypeSub(bizTypeSub);
        cfgAccountClassChooseDto.setCusType(cusType);
        cfgAccountClassChooseDto.setCityVillage(cityVillage);
        cfgAccountClassChooseDto.setFactoryType(factoryType);
        cfgAccountClassChooseDto.setMainDeal(mainDeal);
        cfgAccountClassChooseDto.setFarmFlag(farmFlag);
        cfgAccountClassChooseDto.setFarmDirection(farmDirection);
        cfgAccountClassChooseDto.setDirectionOption(directionOption);
        cfgAccountClassChooseDto.setGuarMode(guarMode);
        cfgAccountClassChooseDto.setLoanType(loanType);
        cfgAccountClassChooseDto.setLoanTerm(loanTerm);
        System.out.println(cfgAccountClassChooseDto.toString());
        logger.info("科目计算，科目计算传参为:{}", cfgAccountClassChooseDto);
        cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByProps(cfgAccountClassChooseDto).getData();
        logger.info("科目计算，科目计算回参为:{}", cfgAccountClassChooseDto.toString());
        if (!EcfEnum.ECF020000.key.equals(cfgAccountClassChooseDto.getRtnCode())) {
            throw BizException.error(null, cfgAccountClassChooseDto.getRtnCode(), cfgAccountClassChooseDto.getRtnMsg());
        }
        return cfgAccountClassChooseDto;
    }
}
