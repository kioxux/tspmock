package cn.com.yusys.yusp.service.server.xdht0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdht0045.resp.Xdht0045DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0045.req.Xdht0045DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class Xdht0045Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0045Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    
    @Transactional
    public Xdht0045DataRespDto xdht0045(Xdht0045DataReqDto xdht0045DataReqDto) throws Exception {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045DataReqDto));
        Xdht0045DataRespDto xdht0045DataRespDto = new Xdht0045DataRespDto();

        try{
            String custId = xdht0045DataReqDto.getCustId();
            String certTp = xdht0045DataReqDto.getCertTp();
            String certNo = xdht0045DataReqDto.getCertNo();

            if(StringUtils.isEmpty(custId)) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("certType", certTp);
                queryModel.addCondition("certCode", certNo);
                ResultDto<List<CusIndivDto>> resultDto = cmisCusClientService.queryCusIndiv(queryModel);
                if(CollectionUtils.nonEmpty(resultDto.getData())) {
                    if(resultDto.getData().get(0) instanceof CusIndivDto) {
                        custId = resultDto.getData().get(0).getCusId();
                    }else {
                        //UserAndOrgNameAspect.listTrans()会将CusIndivDto转换成LinkedHashMap
                        custId = (String)((Map)resultDto.getData().get(0)).get("cusId");
                    }
                }
            }

            if(StringUtils.nonEmpty(custId)) {
                QueryModel qMd = new QueryModel();
                qMd.addCondition("prdTypeProp", "P034");//房抵e点贷
                qMd.addCondition("cusId", custId);
                List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByModel(qMd);

                ArrayList list = new ArrayList<>();
                if(CollectionUtils.nonEmpty(ctrLoanContList)) {
                    for(int i=0; i<ctrLoanContList.size(); i++) {
                        CtrLoanCont ctrLoanCont = ctrLoanContList.get(i);
                        cn.com.yusys.yusp.dto.server.xdht0045.resp.List lst = new cn.com.yusys.yusp.dto.server.xdht0045.resp.List();
                        lst.setContNo(ctrLoanCont.getContNo());
                        lst.setContSt(ctrLoanCont.getContStatus());
                        list.add(lst);
                    }
                }
                xdht0045DataRespDto.setList(list);
            } else {
                throw BizException.error(null, EpbEnum.EPB099999.key, "信贷系统无法查询到该客户");
            }
        }catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key,
                    DscmsEnum.TRADE_CODE_XDHT0045.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key,
                    DscmsEnum.TRADE_CODE_XDHT0045.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value);
        return xdht0045DataRespDto;
    }
}
