package cn.com.yusys.yusp.web.server.xddb0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0010.req.Xddb0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.Xddb0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.YpList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0010.Xddb0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * 接口处理类:信贷押品列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDB0010:信贷押品列表查询")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0010Resource.class);

    @Autowired
    private Xddb0010Service xddb0010Service;

    /**
     * 交易码：xddb0010
     * 交易描述：信贷押品列表查询
     *
     * @param xddb0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷押品列表查询")
    @PostMapping("/xddb0010")
    protected @ResponseBody
    ResultDto<Xddb0010DataRespDto> xddb0010(@Validated @RequestBody Xddb0010DataReqDto xddb0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010DataReqDto));
        Xddb0010DataRespDto xddb0010DataRespDto = new Xddb0010DataRespDto();// 响应Dto:信贷押品列表查询
        ResultDto<Xddb0010DataRespDto> xddb0010DataResultDto = new ResultDto<>();
        try {
			String contNo = xddb0010DataReqDto.getCont_no();//合同号
            // 从xddb0010DataReqDto获取业务值进行业务逻辑处理
            List<YpList> contListByContNo = xddb0010Service.getContListByContNo(contNo);
            if (!CollectionUtils.isEmpty(contListByContNo)) {
                xddb0010DataRespDto.setYpList(contListByContNo);
            } else {
                YpList ypList = new YpList();
                ypList.set_ypserno(StringUtils.EMPTY);// 押品统一编号
                ypList.set_ypstatus(StringUtils.EMPTY);// 入库状态
                ypList.setAreacode(StringUtils.EMPTY);// 所在区域编号
                xddb0010DataRespDto.setYpList(Arrays.asList(ypList));
            }
            // 封装xddb0010DataResultDto中正确的返回码和返回信息
            xddb0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, e.getMessage());
            // 封装xddb0010DataResultDto中异常返回码和返回信息
            xddb0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0010DataRespDto到xddb0010DataResultDto中
        xddb0010DataResultDto.setData(xddb0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010DataRespDto));
        return xddb0010DataResultDto;
    }
}
