package cn.com.yusys.yusp.web.server.xdxw0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0015.req.Xdxw0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0015.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0015.resp.Xdxw0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0015.Xdxw0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 接口处理类:查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0015:查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0015Resource.class);
		@Resource
    private Xdxw0015Service xdxw0015Service;
    /**
     * 交易码：xdxw0015
     * 交易描述：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
     *
     * @param xdxw0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”")
    @PostMapping("/xdxw0015")
    protected @ResponseBody
    ResultDto<Xdxw0015DataRespDto> xdxw0015(@Validated @RequestBody Xdxw0015DataReqDto xdxw0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataReqDto));
        Xdxw0015DataRespDto xdxw0015DataRespDto = new Xdxw0015DataRespDto();// 响应Dto:查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
        ResultDto<Xdxw0015DataRespDto> xdxw0015DataResultDto = new ResultDto<>();
        String cusId = xdxw0015DataReqDto.getCusId();//客户号
        try {
            // 从xdxw0015DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0015DataReqDto));
            xdxw0015DataRespDto = xdxw0015Service.xdxw0015(xdxw0015DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0015DataRespDto));
            // TODO 调用XXXXXService层结束


            // 封装xdxw0015DataResultDto中正确的返回码和返回信息
            xdxw0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, e.getMessage());
            // 封装xdxw0015DataResultDto中异常返回码和返回信息
            xdxw0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0015DataRespDto到xdxw0015DataResultDto中
        xdxw0015DataResultDto.setData(xdxw0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataResultDto));
        return xdxw0015DataResultDto;
    }
}
