/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppCorreShd;
import cn.com.yusys.yusp.service.LmtAppCorreShdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppCorreShdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:36:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtappcorreshd")
public class LmtAppCorreShdResource {
    @Autowired
    private LmtAppCorreShdService lmtAppCorreShdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppCorreShd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppCorreShd> list = lmtAppCorreShdService.selectAll(queryModel);
        return new ResultDto<List<LmtAppCorreShd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtAppCorreShd>> index(@RequestBody QueryModel queryModel) {
        List<LmtAppCorreShd> list = lmtAppCorreShdService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppCorreShd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppCorreShd> show(@PathVariable("pkId") String pkId) {
        LmtAppCorreShd lmtAppCorreShd = lmtAppCorreShdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppCorreShd>(lmtAppCorreShd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppCorreShd> create(@RequestBody LmtAppCorreShd lmtAppCorreShd) throws URISyntaxException {
        lmtAppCorreShdService.insert(lmtAppCorreShd);
        return new ResultDto<LmtAppCorreShd>(lmtAppCorreShd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppCorreShd lmtAppCorreShd) throws URISyntaxException {
        int result = lmtAppCorreShdService.update(lmtAppCorreShd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppCorreShdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppCorreShdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteLogicByPkId")
    protected ResultDto<Integer> deleteLogicByPkId(@RequestBody Map condition) {
        String pkId = (String) condition.get("pkId");
        int result = lmtAppCorreShdService.deleteLogicByPkId(pkId);
        return new ResultDto<Integer>(result);
    }

}
