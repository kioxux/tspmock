package cn.com.yusys.yusp.service.server.xdsx0013;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CfgPrdTypePropertiesDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.req.Xdsx0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.resp.Xdsx0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList;
import cn.com.yusys.yusp.enums.online.DscmsBizSxEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0013Service
 * @类描述: #服务类
 * @功能描述:用于对公智能风控平台查询客户房抵E点贷授信及用信情况 1、根据传过来的客户号查询是否为存量客户；
 * 2、根据传过来的客户号查询该客户是否存在有效的授信协议编号；
 * 3、查找该客户是否存在房抵e点贷的业务；
 * 4、查找出该客户是否存在生效的房抵e点贷最高额合同；
 * 5、查找出该客户是否存在未签订的房抵e点贷最高额合同；
 * 6、查找出该客户作为实际控制人、法人代表、主要实际控制人关联的企业是否存在省心快贷业务；
 * 7、查询合同申请表中是否存在在途的线上房抵e点申请；
 * 8、查询客户是否存在生效的非消费类贷款的合同；
 * 9、查询客户是否存在是否有未关闭的非消费类贷款的借据信息；
 * 10、查询客户判断是否存在在途的授信申请；
 * 11、查询客户判断是否存在非房抵e点贷授信申请记录；
 * 12、加工授信状态：
 * 1）当2为是且3为否时，则授信状态为01存量客户在我行有授信业务，但不包含房抵e点授信的；
 * 2）当2为是且3为是且8为是时，则授信状态为04存量客户个人在我行已授信房抵e点贷业务，并签订了有效的is_xs=1的最高额借款合同；
 * 3）当2为是且3为是且（5为是或者7为是）时，则授信状态为03-存量客户个人在我行已授信房抵e点贷业务，但未签订有效的is_xs=1的最高额借款合同；
 * 4）当6为是时，则授信状态为05-存量客户关联企业在我行已授信省心快贷业务；
 * 5）当11为是且2为否时，则授信状态为07-存量客户：存在授信申请记录，但授信未生效；
 * 6）当10为否且8为否且9为否时，则授信状态为02-存量客户在我行未有过授信或授信已结束；
 * 7）当1为否时，则授信状态为06-非存量客户；
 * 13、根据客户号查询有效的授信协议信息以及授信品种分项信息进行返回；
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0013Service.class);

    @Autowired
    private CommonService commonService;//公共查询
    @Autowired
    private LmtReplyMapper lmtReplyMapper;//授信批复
    @Resource
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;//授信批复台账分项适用品种明细表
    @Autowired
    private LmtReplyAccMapper lmtReplyAccMapper;//授信台账表
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;//合同表
    @Autowired
    private AccLoanMapper accLoanMapper;//台账表
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;//业务主表
    @Autowired
    private LmtAppMapper lmtAppMapper;//授信申请表
    @Autowired
    private AdminSmUserService adminSmUserService;//用户
    @Autowired
    private AdminSmOrgService adminSmOrgService;//机构
    @Autowired
    private CmisCusClientService cmisCusClientService;//客户信息模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;//额度信息模块
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;//配置信息模块

    @Autowired
    private LmtReplySubMapper lmtReplySubMapper;

    /**
     * 交易码：Xdsx0013
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param xdht0034DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0013DataRespDto xdsx0013(Xdsx0013DataReqDto xdht0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdht0034DataReqDto));
        //定义返回信息
        Xdsx0013DataRespDto xdsx0013DataRespDto = new Xdsx0013DataRespDto();
        try {
            //获取请求字段
            String cusId = xdht0034DataReqDto.getCus_id();//客户号
            String certCode = xdht0034DataReqDto.getCredit_no();//证件号
            //获取系统当前日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            //返回字段
            String manager_id = "";
            String manager_br_id = "";
            String crd_status = "";

            //根据传过来的客户号查询是否为存量客户
            CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);
            if (cusBaseDto != null) {//存量客户
                String managerId = cusBaseDto.getManagerId();//获取管户经理
//                /*************根据产品标识P034(房抵E贷)查询出所属所有产品编号*****************/
//                List<String> prdIds = new ArrayList<>();
//                ResultDto<List<CfgPrdTypePropertiesDto>> resultDtos = iCmisCfgClientService.queryCfgPrdTypePropertiesByProrNo("P034");
//                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDtos.getCode())) {
//                    List<CfgPrdTypePropertiesDto> cfgPrdTypePropertiesDtos = resultDtos.getData();
//                    if(CollectionUtils.nonEmpty(cfgPrdTypePropertiesDtos)){
//                        cfgPrdTypePropertiesDtos= JSON.parseObject(JSON.toJSONString(cfgPrdTypePropertiesDtos), new TypeReference< List<CfgPrdTypePropertiesDto>>(){});
//                        prdIds = cfgPrdTypePropertiesDtos.parallelStream()
//                                .map(cfgPrdTypePropertiesDto -> cfgPrdTypePropertiesDto.getPrdId())
//                                .collect(Collectors.toList());// 转换成List
//                    }
//                }
                //根据条件判断并返回相关信息给风控
                //查找出该客户是否存在有效的授信协议
                HashMap<String, String> paramMap = new HashMap<String, String>();
                paramMap.put("cusId", cusId);
                paramMap.put("openDay", openDay);
                logger.info("根据客户号查找出该客户是否存在有效的授信协议开始,查询参数为:{}", JSON.toJSONString(paramMap));
                int lmtcount = lmtReplyAccMapper.selectCountLmtReplayAccBycusId(paramMap);
                logger.info("根据客户号查找出该客户是否存在有效的授信协议结束,返回结果为:{}", JSON.toJSONString(lmtcount));

                //查找该客户是否存在房抵e点贷的业务
                logger.info("根据客户号查找该客户是否存在有效房抵e点贷的授信开始,查询参数为:{}", JSON.toJSONString(paramMap));
                int lmtHouseCount = lmtReplyAccSubPrdMapper.queryCountLmtReplayHouseBycusId(paramMap);
                logger.info("根据客户号查找该客户是否存在房抵e点贷的业务结束,返回结果为:{}", JSON.toJSONString(lmtHouseCount));

                HashMap queryAccLoanMap = new HashMap();
                queryAccLoanMap.put("cusId", cusId);
//                queryAccLoanMap.put("prdIds", prdIds);//产品编号
                //查找出该客户是存在生效的房抵e点贷最高额合同
                logger.info("根据客户号查找出该客户是存在生效的房抵e点贷最高额合同开始,查询参数为:{}", JSON.toJSONString(queryAccLoanMap));
                int contNum = ctrLoanContMapper.querySxFdeddCtrloanConNumberBycusId(queryAccLoanMap);
                logger.info("根据客户号查找出该客户是存在生效的房抵e点贷最高额合同结束,返回结果为:{}", JSON.toJSONString(contNum));
                //查找出该客户是存在未签订的最高额合同
                int contRecord = 0;
                if (contNum > 0) {
                    logger.info("根据客户号查找出该客户是存在未签订的房抵e点贷最高额合同开始,查询参数为:{}", JSON.toJSONString(queryAccLoanMap));
                    contRecord = ctrLoanContMapper.queryWqdFdeddCtrloanConNumberBycusId(queryAccLoanMap);
                    logger.info("根据客户号查找出该客户是存在未签订的房抵e点贷最高额合同结束,返回结果为:{}", JSON.toJSONString(contRecord));
                }

                //通过证件号查询到借款人作为高管的关联企业客户号
                logger.info("通过证件号查询到借款人作为高管的关联企业客户号,查询参数为:{}", JSON.toJSONString(cusId));
                CmisCus0012ReqDto cmisCus0012ReqDto = new CmisCus0012ReqDto();
                cmisCus0012ReqDto.setCusId(cusId);
                ResultDto<CmisCus0012RespDto> magResultDto = cmisCusClientService.cmiscus0012(cmisCus0012ReqDto);
                //查询客户号
                List<String> cuscomIds = new ArrayList<>();
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, magResultDto.getCode())) {
                    CmisCus0012RespDto comResultDtos = magResultDto.getData();
                    java.util.List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> listResult = comResultDtos.getList();
                    if (CollectionUtils.nonEmpty(listResult)) {
                        for (int i = 0; i < listResult.size(); i++) {
                            cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List cusCorpMgrlist = listResult.get(i);
                            String corpMgrid = cusCorpMgrlist.getCusIdRel();//关联企业客户号
                            cuscomIds.add(corpMgrid);
                        }
                    }
                }
                logger.info("通过证件号查询到借款人作为高管的关联企业客户号,返回结果为:{}", JSON.toJSONString(cuscomIds));

                //根据客户号查找出该客户关联的企业是否存在生效合同或未结清贷款业务
                int accCount = 0;
                Map queryMap = new HashMap();
                queryMap.put("cuscomIds", cuscomIds);//客户号
                logger.info("根据客户号查找出该客户关联的企业是否存在生效合同或未结清贷款业务开始,查询参数为:{}", JSON.toJSONString(queryMap));
                if (cuscomIds != null && cuscomIds.size() > 0) {
                    accCount = accLoanMapper.selectAccLoancountByRelCusComIdList(queryMap);
                }
                logger.info("根据客户号查找出该客户关联的企业是否存在生效合同或未结清贷款业务结束,返回结果为:{}", JSON.toJSONString(accCount));

                //查询合同申请表中是否存在在途的线上房抵e点贷申请
                logger.info("查询合同申请表中是否存在在途的线上房抵e点贷请开始,查询参数为:{}", JSON.toJSONString(paramMap));
                int iqpCount = iqpLoanAppMapper.selectFdeddIqpLoanAppCountByCusId(paramMap);
                logger.info("查询合同申请表中是否存在在途的线上房抵e点贷申请结束,返回结果为:{}", JSON.toJSONString(iqpCount));


//                /*************根据产品层级C0007查询出所属所有产品编号*****************/
//                List<String> cfgPrdIds = new ArrayList<>();
//                ResultDto<List<CfgPrdBasicinfoDto>> cfgResultDtos = iCmisCfgClientService.queryBasicInfoByCatalogId("C0007");
//                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDtos.getCode())) {
//                    List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = cfgResultDtos.getData();
//                    if(CollectionUtils.nonEmpty(cfgPrdBasicinfoDtos)){
//                        cfgPrdBasicinfoDtos= JSON.parseObject(JSON.toJSONString(cfgPrdBasicinfoDtos), new TypeReference< List<CfgPrdBasicinfoDto>>(){});
//                        cfgPrdIds = cfgPrdBasicinfoDtos.parallelStream()
//                                .map(cfgPrdBasicinfoDto -> cfgPrdBasicinfoDto.getPrdId())
//                                .collect(Collectors.toList());// 转换成List
//                    }
//                }
//
//                //添加产品编号
//                queryAccLoanMap.put("cfgPrdIds", cfgPrdIds);//产品编号
                //是否存在非消费类生效的贷款合同
                //老信贷排除的消费贷产品都是非对公产品，故仅查询对公线条
                logger.info("根据客户号查询是否存在非消费类生效的贷款合同开始,查询参数为:{}", JSON.toJSONString(queryAccLoanMap));
                int ctrNum = ctrLoanContMapper.selectSxNotXfCtrLoanContCountByCusId(queryAccLoanMap);
                logger.info("根据客户号查询是否存在非消费类生效的贷款合同结束,返回结果为:{}", JSON.toJSONString(ctrNum));

                //查询是否有未关闭的借据信息
                //老信贷排除的消费贷产品都是非对公产品，故仅查询对公线条
                logger.info("根据客户号查询是否有未关闭的非消费类借据信息,查询参数为:{}", JSON.toJSONString(queryAccLoanMap));
                int accNum = accLoanMapper.selectNotColseAccLoanCountByCusId(queryAccLoanMap);
                logger.info("根据客户号查询是否有未关闭的非消费类借据信息结束,返回结果为:{}", JSON.toJSONString(accNum));

                //判断是否存在在途的授信申请
                logger.info("判断是否存在在途的授信申请开始,查询参数为:{}", JSON.toJSONString(paramMap));
                int lmtCount = lmtAppMapper.selectOnloadLmtApplyCountByCusNo(paramMap);
                logger.info("判断是否存在在途的授信申请结束,返回结果为:{}", JSON.toJSONString(lmtCount));

                //判断是否存在非房抵e点贷授信申请记录
                logger.info("判断是否存在非房抵e点贷授信申请记录开始,查询参数为:{}", JSON.toJSONString(paramMap));
                int lmtSubCount = lmtAppMapper.selectNotFdeddLmtSubApplyCountByCusNo(paramMap);
                logger.info("判断是否存在非房抵e点贷授信申请记录结束,返回结果为:{}", JSON.toJSONString(lmtSubCount));

                if (lmtcount > 0 && lmtHouseCount == 0) {//(1)存量客户在我行有授信业务，但不包含房抵e点授信的
                    crd_status = DscmsBizSxEnum.CRD_STATUS_01.key;
                } else if (lmtcount > 0 && lmtHouseCount > 0 && contNum > 0) {//04-存量客户个人在我行已授信房抵e点贷业务，并签订了有效的is_xs=1的最高额借款合同
                    crd_status = DscmsBizSxEnum.CRD_STATUS_04.key;
                } else if (lmtcount > 0 && lmtHouseCount > 0 && (contRecord > 0 || iqpCount > 0)) {//03-存量客户个人在我行已授信房抵e点贷业务，但未签订有效的is_xs=1的最高额借款合同
                    crd_status = DscmsBizSxEnum.CRD_STATUS_03.key;
                } else if (accCount > 0) {////05-存量客户关联企业在我行已授信省心快贷业务
                    crd_status = DscmsBizSxEnum.CRD_STATUS_05.key;
                } else if (lmtSubCount > 0 && lmtcount == 0) {//07-存量客户：存在授信申请记录，但授信未生效
                    crd_status = DscmsBizSxEnum.CRD_STATUS_07.key;
                } else if (lmtCount == 0 && (ctrNum == 0 && (accNum == 0))) {//02-存量客户在我行未有过授信或授信已结束
                    crd_status = DscmsBizSxEnum.CRD_STATUS_02.key;
                }
                //处理返回状态
                xdsx0013DataRespDto.setCrd_status(crd_status);
                //获取用户;用户名；机构；机构名称
                // 根据工号获取用户表中的联系电话
                String managerName = StringUtils.EMPTY;
                String orgId = StringUtils.EMPTY;
                String managerBrIdName = StringUtils.EMPTY;
                logger.info("***********调用AdminSmUserService;adminSmOrgService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    managerName = adminSmUserDto.getUserName();
                    orgId = adminSmUserDto.getOrgId();
                }
                ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(orgId);
                String codeNum = resultOrgDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(codeNum) && CmisBizConstants.NUM_ZERO.equals(codeNum)) {
                    AdminSmOrgDto adminorgDto = resultOrgDto.getData();
                    managerBrIdName = adminorgDto.getOrgName();
                }
                logger.info("***********调用AdminSmUserService;adminSmOrgService用户信息查询服务结束*END*****************");
                //处理返回
                xdsx0013DataRespDto.setManager_id(managerId);
                xdsx0013DataRespDto.setManager_name(managerName);
                xdsx0013DataRespDto.setManager_org_id(orgId);
                xdsx0013DataRespDto.setManager_org_name(managerBrIdName);

                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("cusId", cusId);
                logger.info("根据客户号查询查找出该客户是否存在授信协议开始,查询参数为:{}", JSON.toJSONString(queryModel));
                LmtReply lmtReply = lmtReplyMapper.selectUsedReply(queryModel);
                logger.info("根据客户号查询查找出该客户是否存在授信协议结束,返回结果为:{}", JSON.toJSONString(lmtReply));
                if (Objects.nonNull(lmtReply)) {
                    //返回list信息
                    xdsx0013DataRespDto.setSerno(lmtReply.getSerno());
                    xdsx0013DataRespDto.setSx_start_date(lmtReply.getReplyInureDate());
                    LocalDate start = LocalDate.parse(lmtReply.getReplyInureDate());
                    LocalDate end = start.plusMonths(lmtReply.getLmtTerm());
                    xdsx0013DataRespDto.setSx_end_date(end.toString());
                    String serno = lmtReply.getSerno();
                    // 查询授信分项信息
                    java.util.List<cn.com.yusys.yusp.dto.server.xdsx0013.resp.List> lists = lmtReplySubMapper.querySubInfoBySerno(serno);
                    if (CollectionUtils.nonEmpty(lists)) {
                        xdsx0013DataRespDto.setList(lists);
                        xdsx0013DataRespDto.setLmt_total_amt(lists.stream().map(e -> new BigDecimal(e.getFx_amt())).reduce(BigDecimal.ZERO, BigDecimal::add).toString());
                    }
                }

            } else {
                crd_status = DscmsBizSxEnum.CRD_STATUS_06.key;//06-非存量客户
                xdsx0013DataRespDto.setCrd_status(crd_status);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013DataRespDto));
        return xdsx0013DataRespDto;
    }
}
