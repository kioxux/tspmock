///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.service;
//
//import java.util.HashMap;
//import java.util.List;
//
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.*;
//import cn.com.yusys.yusp.constants.CmisCommonConstants;
//import cn.com.yusys.yusp.domain.ModelApprResultInfo;
//import cn.com.yusys.yusp.domain.SurveyReportBasicAndCom;
//import cn.com.yusys.yusp.domain.SurveyReportComInfo;
//import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.github.pagehelper.PageHelper;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.repository.mapper.SurveyReportBasicInfoMapper;
//
///**
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyReportBasicInfoService
// * @类描述: #服务类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 13:55:08
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@Service
//@Transactional
//public class SurveyReportBasicInfoService {
//
//    @Autowired
//    private SurveyReportBasicInfoMapper surveyReportBasicInfoMapper;
//
//    @Autowired
//    private SurveyReportComInfoService surveyReportComInfoService;
//
//    @Autowired
//    private ModelApprResultInfoService modelApprResultInfoService;
//    @Autowired
//    private SurveyReportMainInfoService surveyReportMainInfoService;
//    // 流水号生成服务
//    @Autowired
//    private SequenceTemplateService sequenceTemplateClient;
//    /**
//     * @方法名称: selectByPrimaryKey
//     * @方法描述: 根据主键查询
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public SurveyReportBasicInfo selectByPrimaryKey(String surveyNo) {
//        return surveyReportBasicInfoMapper.selectByPrimaryKey(surveyNo);
//    }
//
//	/**
//     * @方法名称: selectAll
//     * @方法描述: 查询所有数据
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    @Transactional(readOnly=true)
//    public List<SurveyReportBasicInfo> selectAll(QueryModel model) {
//        List<SurveyReportBasicInfo> records = (List<SurveyReportBasicInfo>) surveyReportBasicInfoMapper.selectByModel(model);
//        return records;
//    }
//
//    /**
//     * @方法名称: selectByModel
//     * @方法描述: 条件查询 - 查询进行分页
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public List<SurveyReportBasicInfo> selectByModel(QueryModel model) {
//        PageHelper.startPage(model.getPage(), model.getSize());
//        List<SurveyReportBasicInfo> list = surveyReportBasicInfoMapper.selectByModel(model);
//        PageHelper.clearPage();
//        return list;
//    }
//
//    /**
//     * @方法名称: insert
//     * @方法描述: 插入
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int insert(SurveyReportBasicInfo record) {
//        return surveyReportBasicInfoMapper.insert(record);
//    }
//
//    /**
//     * @方法名称: insertSelective
//     * @方法描述: 插入 - 只插入非空字段
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int insertSelective(SurveyReportBasicInfo record) {
//        return surveyReportBasicInfoMapper.insertSelective(record);
//    }
//
//    /**
//     * @方法名称: update
//     * @方法描述: 根据主键更新
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int update(SurveyReportBasicInfo record) {
//        return surveyReportBasicInfoMapper.updateByPrimaryKey(record);
//    }
//
//    /**
//     * @方法名称: updateSelective
//     * @方法描述: 根据主键更新 - 只更新非空字段
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int updateSelective(SurveyReportBasicInfo record) {
//        return surveyReportBasicInfoMapper.updateByPrimaryKeySelective(record);
//    }
//
//    /**
//     * @方法名称: deleteByPrimaryKey
//     * @方法描述: 根据主键删除
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int deleteByPrimaryKey(String surveyNo) {
//        return surveyReportBasicInfoMapper.deleteByPrimaryKey(surveyNo);
//    }
//
//    /**
//     * @方法名称: deleteByIds
//     * @方法描述: 根据多个主键删除
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int deleteByIds(String ids) {
//        return surveyReportBasicInfoMapper.deleteByIds(ids);
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 15:57 2021-04-12
//     * @return 新增 修改  基本信息 企业信息逻辑
//     **/
//    @Transactional(rollbackFor = Exception.class)
//    public int savebasicandcom(SurveyReportBasicAndCom surveyReportBasicAndCom) {
//        LmtSurveyReportBasicInfo surveyReportBasicInfo = surveyReportBasicAndCom.getSurveyReportBasicInfo();
//        LmtSurveyReportComInfo surveyReportComInfo = surveyReportBasicAndCom.getSurveyReportComInfo();
//        //基本信息有无创建
//        int result;
//        if (this.selectByPrimaryKey(surveyReportBasicInfo.getSurveyNo())==null){
//            //无基本信息
//            result = this.insertSelective(surveyReportBasicInfo);
//        }else {
//            result =  this.updateSelective(surveyReportBasicInfo);
//        }
//        if (surveyReportComInfoService.selectByPrimaryKey(surveyReportComInfo.getSurveyNo())==null){
//            //无基本信息
//        }else {
//            result =surveyReportComInfoService.updateSelective(surveyReportComInfo);
//        }
//        //企业信息有无创建
//        return  result ;
//    }
//   /**
//    * @param
//    * @return
//    * @author WH
//    * @date 2021-04-16 15:23
//    * @version 1.0.0
//    * @desc
//    * @修改历史: 修改时间    修改人员    修改原因
//    */
//    @Transactional(rollbackFor = Exception.class)
//    public int modelapprove(SurveyReportBasicAndCom surveyReportBasicAndCom) {
//        //保存
//        int savebasicandcom = this.savebasicandcom(surveyReportBasicAndCom);
//        //先查30天内征信
//        String surveyNo = surveyReportBasicAndCom.getSurveyReportBasicInfo().getSurveyNo();
//        //风控模型结果
//        //根据风控结果 插入数据
////        int insert=0;
//        ModelApprResultInfo modelApprResultInfo = modelApprResultInfoService.selectByPrimaryKey(surveyNo);
//       if (modelApprResultInfo==null){
//           ModelApprResultInfo model=new ModelApprResultInfo();
//           model.setSurveyNo(surveyNo);
//           model.setModelRstStatus("003");
//           savebasicandcom = modelApprResultInfoService.insert(model);
//       }else {
//           savebasicandcom= modelApprResultInfoService.updateSelective(modelApprResultInfo);
//       }
//        return savebasicandcom;
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 2021-04-16 18:16
//     * @注释 查询单挑数据 并返回给前端该条数据的审批状态
//     */
//    public ResultDto<SurveyReportBasicAndCom> selectbasicandcom(String surveyNo) {
//        SurveyReportBasicInfo surveyReportBasicInfo = new SurveyReportBasicInfo();
//        SurveyReportComInfo surveyReportComInfo = new SurveyReportComInfo();
//        SurveyReportBasicInfo basic = surveyReportBasicInfoMapper.selectByPrimaryKey(surveyNo);
//        if (basic==null){
//            //没有查询到对应的值 此时去主表拿数据补全
//            SurveyReportMainInfo surveyReportMainInfo = surveyReportMainInfoService.selectByPrimaryKey(surveyNo);
//            surveyReportBasicInfo.setSurveyNo(surveyReportMainInfo.getSurveyNo());
//            surveyReportBasicInfo.setCusId(surveyReportMainInfo.getCusNo());
//            surveyReportBasicInfo.setCusName(surveyReportMainInfo.getCusName());
//            surveyReportBasicInfo.setCertNo(surveyReportMainInfo.getCertNo());
//            surveyReportBasicInfo.setCertType(surveyReportMainInfo.getCertType());
//        }else {
//            surveyReportBasicInfo=basic;
//        }
//        SurveyReportComInfo com = surveyReportComInfoService.selectByPrimaryKey(surveyNo);
//        if (com==null){
//            surveyReportComInfo.setSurveyNo(surveyNo);
//        }else {
//            surveyReportComInfo=com;
//        }
//        SurveyReportBasicAndCom surveyReportBasicAndCom = new SurveyReportBasicAndCom();
//        surveyReportBasicAndCom.setSurveyReportBasicInfo(surveyReportBasicInfo);
//        surveyReportBasicAndCom.setSurveyReportComInfo(surveyReportComInfo);
//        //现在去查询其他复杂条件 比如 是否模型审批通过 是否利率审批通过  查询完成的话 可以在
//        ModelApprResultInfo modelApprResultInfo = modelApprResultInfoService.selectByPrimaryKey(surveyNo);
//        if (modelApprResultInfo==null){
//            //没有进行模型审批
//            surveyReportBasicAndCom.setI(1);
//        }else if ("000".equals(modelApprResultInfo.getModelRstStatus())){
//            //模型审批待发起
//            surveyReportBasicAndCom.setI(2);
//        }else if ("003".equals(modelApprResultInfo.getModelRstStatus())){
//            //模型审批通过
//            surveyReportBasicAndCom.setI(3);
//        }
//
//        return new ResultDto(surveyReportBasicAndCom);
//    }
//
//
//
//    /**
//     * @创建人 wzy
//     * @创建时间 15:57 2021-04-12
//     * @return 新增 修改  基本信息 企业信息逻辑
//     **/
//    @Transactional(rollbackFor = Exception.class)
//    public int savebasic(SurveyReportBasicInfo surveyReportBasicInfo) {
//        //基本信息有无创建
//        int result;
//        if (this.selectByPrimaryKey(surveyReportBasicInfo.getSurveyNo())==null){
//            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
//            surveyReportBasicInfo.setSurveyNo(serno);
//            //无基本信息
//            result = this.insertSelective(surveyReportBasicInfo);
//        }else {
//            result =  this.updateSelective(surveyReportBasicInfo);
//        }
//        //企业信息有无创建
//        return  result ;
//    }
//}
