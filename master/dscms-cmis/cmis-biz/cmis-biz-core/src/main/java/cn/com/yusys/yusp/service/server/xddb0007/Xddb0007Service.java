package cn.com.yusys.yusp.service.server.xddb0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.dto.server.xddb0007.req.Xddb0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0007.resp.Xddb0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 接口处理类:电票质押请求
 *
 * @author code-generator
 * @version 1.0
 */
@Service
public class Xddb0007Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xddb0007.Xddb0007Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Resource
    private GrtGuarContMapper grtGuarContMapper;
    @Resource
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;
    @Autowired
    private AsplIoPoolService asplIoPoolService;
    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private ICusClientService iCusClientService ;
    @Autowired
    private AsplAssetsListService asplAssetsListService;
    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;
    @Autowired
    private AsplAorgListService asplAorgListService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;


    @Transactional
    public Xddb0007DataRespDto xddb0007(Xddb0007DataReqDto xddb0007DataReqDto){

        //参数初始化
        String opType = xddb0007DataReqDto.getOpType();//操作类型
        String eBillNo = xddb0007DataReqDto.geteBillNo();//电票号码
        String imnManCusId = xddb0007DataReqDto.getImnManCusId();//质押人客户号
        String imnCusName = xddb0007DataReqDto.getImnCusName();//质押客户名称
        String collName = xddb0007DataReqDto.getCollName();//质押品名称
        String ownCertNo = xddb0007DataReqDto.getOwnCertNo();//权属证号
        BigDecimal drftAmt = xddb0007DataReqDto.getDrftAmt();//票面金额
        String eBillCurType = xddb0007DataReqDto.geteBillCurType();//电票币种
        String isseDate = xddb0007DataReqDto.getIsseDate();//出票日
        String endDate = xddb0007DataReqDto.getEndDate();//到期日
        String pyeeName = xddb0007DataReqDto.getPyeeName();//收款人名称
        String pyeeAcctbNm = xddb0007DataReqDto.getPyeeAcctbNm();//收款人开户行行名
        String pyeeAcctbNo = xddb0007DataReqDto.getPyeeAcctbNo();//收款人开户行行号
        String pyeeAcctNo = xddb0007DataReqDto.getPyeeAcctNo();//收款人账号
        String drwrCertNo = xddb0007DataReqDto.getDrwrCertNo();//出票人证件号码
        String drwrName = xddb0007DataReqDto.getDrwrName();//出票人名称
        String drwrAcctbNo = xddb0007DataReqDto.getDrwrAcctbNo();//出票人开户行行号
        String drwrAcctbNm = xddb0007DataReqDto.getDrwrAcctbNm();//出票人开户行行名
        String drwrAcctbAcctNo = xddb0007DataReqDto.getDrwrAcctbAcctNo();//出票人开户行帐号
        String managerId = xddb0007DataReqDto.getManagerId();//主管客户经理
        String finaBrId = xddb0007DataReqDto.getFinaBrId();//账务机构
        String managerBrId = xddb0007DataReqDto.getManagerBrId();//管理机构
        String aorgType = xddb0007DataReqDto.getAorgType();//承兑行类型
        String aorgNo = xddb0007DataReqDto.getAorgNo();//承兑行行号
        String accptr = xddb0007DataReqDto.getAccptr();//承兑人
        String evalType = xddb0007DataReqDto.getEvalType();//评估方式
        BigDecimal evalAmt = xddb0007DataReqDto.getEvalAmt();//评估金额（元）
        String evalDate = xddb0007DataReqDto.getEvalDate();//评估日期
        String evalOrg = xddb0007DataReqDto.getEvalOrg();//评估机构
        String evalOrgCmonNo = xddb0007DataReqDto.getEvalOrgCmonNo();//评估机构组织机构代码
        BigDecimal claimAmt = xddb0007DataReqDto.getClaimAmt();//权利金额(元)
        String drfpoImnFlag = xddb0007DataReqDto.getDrfpoImnFlag();//票据池质押标记
        String cjxtyp = "";
        if("016000".equals(managerBrId)){
            cjxtyp = "02";
        }else{
            cjxtyp = "01";
        }
        String grpNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>()); // 生成核心担保编号 老信贷（权证编号）
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());  // 生成出入库流水号
        String cardType="";
        if("电子银行承兑汇票".equals(collName)){
            cardType = "19";
        }else{
            cardType = "20";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatEnum.DEFAULT.getValue());

        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();
        try{
            //生成GuarBaseInfo对象
            GuarBaseInfo guarBaseInfo = getGuarBaseInfoObj(xddb0007DataReqDto,cjxtyp);
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            //    1.质押时:
            // 1）按权证编号查询押品在质押表中是否存在登记人为DP0000，且状态非10008、10013的押品
            //     若存在且票据池日期为空，则返回错误“已存在该笔票号的信息,请手工入池”
            //     若存在且票据池日期不为空，则返回错误“该笔票号已入池”
            // 2) 调用客户端接口billyp发送押品，创建质押品并返回押品编号
            //    插入质押品表grt_p_basic_info以及票据详情表grt_p_bill（这表没有）
            // 3) 若非票据池业务，则插入GUAR_WARRANT_IN_APP、GUAR_REGISTER_DETAIL
            //    调用客户端接口co3200发送核心完整自动入库：更新GUAR_WARRANT_IN_APP中押品状态；
            if("1".equals(opType)){//质押
                //查询是否存在的押品记录是否存在登记人为DP0000，抵押物状态为非（正常出库和部分出库）
                Boolean flag = checkIfExit(guarBaseInfo,xddb0007DataRespDto);
                // 校验承兑行总行配置分类是否存在
                asplAssetsListService.getHeadBankInfo(xddb0007DataReqDto.getAorgNo());

                if(flag == true){
                    xddb0007DataRespDto = zYOption(xddb0007DataReqDto,cjxtyp,grpNo,serno,guarBaseInfo,openDay);
                }else{
                    return xddb0007DataRespDto;
                }
            }

            //   2.撤销质押时：
            // 1）按权证编号查询押品在质押表中是否存在登记人为DP0000的押品，
            // 若状态非10001则返回错误“只有未入库才能撤销质押请求”--原代码逻辑有问题，这种情况不可能。若不存在记录则返回错误“不存在未入库的该票据信息”；
            // 2）关联权证编号删除质押品表grt_p_basic_info以及票据详情表grt_p_bill中记录（目前没有grt_p_bill表）；

            else if("2".equals(opType)){//2：撤销质押
                Boolean flag = this.checkIfDelete(guarBaseInfo , xddb0007DataRespDto);
                if(flag == true){
                    xddb0007DataRespDto = this.deletePbasicAndPbil(guarBaseInfo);
                }else{
                    return xddb0007DataRespDto;
                }
            }

            //   3.解质押时：
            //1）按权证编号,查询押品在抵质押基本表中是否存在登记人为DP0000的押品，若状态不是（已入库10006），则返回错误“只有已入库状态才能出库”，若不存在记录则返回错误"不存在已入库的票据信息"
            //2）按权证编号,查询状态为未生效或生效的贷款合同信息，若存在则返回错误信息"该押品所关联的担保合同未注销，不能出库"
            //3）按权证编号,查询对应押品的入库授权编号以及押品编号
            //4）调用客户端接口co3202发送核心完成押品出库，发送成功，更新Grt_Gp_Inout_Info中状态以及交易流水等
            //   更新押品信息状态为出库已记账、入池状态为2、出池日期
            else if("3".equals(opType)){
                Boolean flag = this.checkIfCancel(guarBaseInfo , xddb0007DataRespDto);
                if(flag == true){
                    xddb0007DataRespDto = this.SendMsgPbasicAndPbil(xddb0007DataReqDto,guarBaseInfo ,"chuku");
                }else{
                    return xddb0007DataRespDto;
                }
            }

            //   4.撤销解质押时：
            // 1）按权证编号关联查询对应押品的入库授权编号以及押品编号；
            // 2）调用客户端接口ib1241发送核心完成押品撤销出库，发送成功：
            //     更新授权出库、台账状态为入库已记账，
            //     更新押品基本信息表状态为入库已记账
            else if("4".equals(opType)){
                xddb0007DataRespDto =this.SendMsgPbasicAndPbil(xddb0007DataReqDto,guarBaseInfo ,"chexiaochuku");
            }else{
                return xddb0007DataRespDto;
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value);
        return xddb0007DataRespDto;
    }

    /**
    * 质押操作
    * 1、获取对应数据
    * 2、发送押品接口billyp同步数据
    * 3、获取接口返回统一押品编号，插入押品基本信息表
    * 4、发送核心入库接口co3200，获取核心押品编号
    * 5、插入权证入库申请数据
    * 6、更新押品状态
    * */
    public Xddb0007DataRespDto zYOption(Xddb0007DataReqDto xddb0007DataReqDto,String cjxtyp,String grpNo,String serno,GuarBaseInfo guarBaseInfo,String openDay){
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();
        xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
        try{

            logger.info("票据信息发送给押品系统");
            //封装billypReqDto票据信息
            BillypReqDto billypReqDto = getBillypReqDto(xddb0007DataReqDto,cjxtyp,openDay);
            // 票据信息发送给押品接口，返回押品编号
            ResultDto<BillypRespDto> billypResultDto = dscms2YpxtClientService.billyp(billypReqDto);
            BillypRespDto billypRespDto = billypResultDto.getData();
            String erorcd = billypResultDto.getCode();//响应码
            String yptybh = billypRespDto.getYptybh();//押品统一编号
            //封装guarBaseInfo押品基本信息
            if("0".equals(erorcd) && !"".equals(yptybh) && yptybh!=null){
                //票据信息同步成功后添加押品统一编号
                guarBaseInfo.setSerno(yptybh);
                guarBaseInfo.setGuarNo(yptybh);
                guarBaseInfo.setCoreGuarantyNo(grpNo);
                // 插入押品基本信息表
                guarBaseInfoMapper.insertSelective(guarBaseInfo);

            }else if(!"0001".equals(erorcd)){
                xddb0007DataRespDto.setOpFlag("F");
                xddb0007DataRespDto.setOpMsg(billypResultDto.getMessage());
                return xddb0007DataRespDto;
            }

            // 如果是资产池(入池质押)
            if("0".equals(xddb0007DataReqDto.getDrfpoImnFlag())){
                // 校验额度承兑行名单 额度占用(只有资产池才会占用)
                sendCmisLmt0024(xddb0007DataReqDto);
                // 质押人客户号(根据资产池客户编号查询资产池信息)
                String cusId = xddb0007DataReqDto.getImnManCusId();
                CtrAsplDetails ctrAsplDetailist = ctrAsplDetailsService.selectInfoByCusId(cusId);
                // 一个客户只有一个有效的资产池协议编号
                if(Objects.isNull(ctrAsplDetailist)){
                    xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xddb0007DataRespDto.setOpMsg("未查询到客户"+cusId+"下存在有效的资产池协议");
                    return xddb0007DataRespDto;
                }
                // 获取资产池协议
                CtrAsplDetails ctrAsplDetails = ctrAsplDetailist;
                // 获取该协议下流水号
                QueryModel model = new QueryModel();
                // 根据流水号查询
                model.addCondition("serno",ctrAsplDetails.getSerno());
                java.util.List<GrtGuarBizRstRel> GrtGuarBizRstRelList = grtGuarBizRstRelService.selectByModel(model);
                GrtGuarBizRstRel grtGuarBizRstRel = null;
                // 资产池关联的担保合同只有一个
                if(CollectionUtils.nonEmpty(GrtGuarBizRstRelList)){
                    grtGuarBizRstRel = GrtGuarBizRstRelList.get(0);
                }else{
                    logger.info("未查询到对应的业务与担保合同的关系，业务流水："+ctrAsplDetails.getSerno());
                    xddb0007DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xddb0007DataRespDto.setOpMsg("未查询到对应的业务与担保合同的关系，业务流水："+ctrAsplDetails.getSerno());
                    return xddb0007DataRespDto;//终止foreach的循环
                }
                // 担保合同编号
                String guarContNo = grtGuarBizRstRel.getGuarContNo();
                logger.info("核心入库开始");
                // 押品开户接口组装
                Co3200ReqDto co3200ReqDto =  getCo3200ReqDto(xddb0007DataReqDto,guarBaseInfo,grpNo);
                // 发送押品核心入库接口
                ResultDto<Co3200RespDto> co3200ResultDto = dscms2CoreCoClientService.co3200(co3200ReqDto);
                Co3200RespDto co3200RespDto = null;
                // 判断核心入库是否成功，更新担保权证入库申请
                if("0".equals(co3200ResultDto.getCode())){
                    co3200RespDto = co3200ResultDto.getData();
                    // 权证新增
                    insertGuarWarrantInfo(xddb0007DataReqDto,grpNo,serno,co3200RespDto,openDay);
                    // 权证 担保合同 押品关系表
                    insertGuarContRelWarrant(grpNo,serno,guarBaseInfo,guarContNo);
                }else{
                    xddb0007DataRespDto.setOpFlag("F");
                    xddb0007DataRespDto.setOpMsg(co3200ResultDto.getMessage());
                    return xddb0007DataRespDto;
                }
                xddb0007DataRespDto = zYPoolOption(xddb0007DataReqDto,grpNo,yptybh,ctrAsplDetails,grtGuarBizRstRel);
            }
            return xddb0007DataRespDto;
        }catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }catch (Exception e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }
        return xddb0007DataRespDto;
    }

    private int insertGuarContRelWarrant(String grpNo, String serno,GuarBaseInfo guarBaseInfo,String guarContNo) {
        logger.info("新增权证关系表");
        GuarContRelWarrant guarContRelWarrant = new GuarContRelWarrant();
        guarContRelWarrant.setSerno(serno);
        guarContRelWarrant.setGuarContNo(guarContNo);
        guarContRelWarrant.setCoreGuarantyNo(grpNo);
        guarContRelWarrant.setGuarNo(guarBaseInfo.getGuarNo());
        guarContRelWarrant.setPldimnMemo(guarBaseInfo.getPldimnMemo());
        guarContRelWarrant.setGuarType(guarBaseInfo.getGuarType());
        guarContRelWarrant.setGuarCusId(guarBaseInfo.getGuarCusId());
        guarContRelWarrant.setGuarCusName(guarBaseInfo.getGuarCusName());
        guarContRelWarrant.setOprType(CmisBizConstants.OPR_TYPE_01);
        return guarContRelWarrantService.insert(guarContRelWarrant);
    }

    /**
     * 撤销质押
     * */
    public boolean checkIfDelete(GuarBaseInfo guarBaseInfo,Xddb0007DataRespDto xddb0007DataRespDto){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("rightCertNo",guarBaseInfo.getRightCertNo());//权属证件号
        queryModel.addCondition("inputId",guarBaseInfo.getInputId());//登记人
        //通过权证编号，查询是否有未入库的登记人为DP0000的押品
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.queryExistsWrk(queryModel);
        if(null == guarBaseInfoList){
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg("不存在未入库的该票据信息！");
            return false;
        }
        return true;
    }

    /**
     * 解质押校验规则：1.校验已入库的押品是否存在
     *                2.校验该权证编号对应的押品，是否是入库已记账状态。如果不是，则提示只有已入库才能出库。
     *                3.校验该押品关联的担保合同未注销，不能出库。
     * */
    public boolean checkIfCancel(GuarBaseInfo guarBaseInfo,Xddb0007DataRespDto xddb0007DataRespDto){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("rightCertNo",guarBaseInfo.getRightCertNo());//权属证件号
        queryModel.addCondition("inputId",guarBaseInfo.getInputId());//登记人
        //通过权证编号，查询登记人为DP0000是否有已入库的的押品
        String guarState = guarBaseInfoMapper.queryExistsYrk(queryModel);
        if(null == guarState || "".equals(guarState)){
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg("不存在已入库的票据信息！！");
            return false;
        }

        guarState = guarBaseInfoMapper.queryYpStatus(queryModel);
        //押品状态不是已入库
        if(!"".equals(guarState) && !"10006".equals(guarState)){
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg("只有已入库状态才能出库！！");
            return false;
        }

        //通过权证编号，查询登记人为DP0000，并且已入库的押品所关联的生效或者未生效的合同状态的担保合同是否存在(未注销)
       List<GrtGuarCont> grtGuarContList = grtGuarContMapper.queryExistsWzx(queryModel);
       if(null!=grtGuarContList && grtGuarContList.size()>0){
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(" 该押品所关联的担保合同未注销，不能出库!");
            return false;
        }
        return true;
    }
    /**
     * 解质押与撤销解质押（公共方法）--发送核心
     * */
    public Xddb0007DataRespDto SendMsgPbasicAndPbil(Xddb0007DataReqDto xddb0007DataReqDto,GuarBaseInfo guarBaseInfo,String ckFlag){
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();
        xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
        try{
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("rightCertNo",guarBaseInfo.getRightCertNo());//权属证件号
            queryModel.addCondition("inputId",guarBaseInfo.getInputId());//登记人
            queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);

            //根据权证编号，获取押品编号【非（未入库、正常出库、部分出库）】
            String  guarNo = guarBaseInfoMapper.queryExistsNork(queryModel);
            if(null != guarNo && !"".equals(guarNo)){
                //根据押品编号，查询权证入库时的出入库流水号
                queryModel.addCondition("guarNo",guarNo);//押品编号

                List<GuarBaseInfo> guarBaseInfos = guarBaseInfoService.selectAll(queryModel);

                String  bookSerno = guarBaseInfos.get(0).getCoreGuarantyNo();

                QueryModel model = new QueryModel();
                model.addCondition("coreGuarantyNo",bookSerno);
                model.addCondition("warrantAppType",CmisBizConstants.STD_WARRANT_APP_TYPE_02);
                model.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_997);
                model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);

                //根据核心担保编号查询该笔权证出库对应的权证入库记录
                GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectAll(queryModel).get(0);

                GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(bookSerno);

                //通过出入库流水号，查询押品所有人编号、押品所有人名称、抵质押物名称
                List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.queryYpnoYpnameByBookSerno(bookSerno);
                String cusIdList="";
                String cusNameList = "";
                String guarNoList = "";
                String  gageNameList = "";
                if(null!=guarBaseInfoList && guarBaseInfoList.size()>0){
                    for(int i=0 ;i<guarBaseInfoList.size();i++){
                        GuarBaseInfo guarBaseInfo1 = guarBaseInfoList.get(i);
                        cusIdList += ","+guarBaseInfo1.getGuarCusId();
                        cusNameList+= ","+guarBaseInfo1.getGuarCusName();
                        guarNoList += ","+guarBaseInfo1.getGuarNo();
                        gageNameList+= ","+guarBaseInfo1.getPldimnMemo();
                    }
                }
                // 核心不支持多个客户号拼凑字符串，暂时只发第一个客户号
                String cusIdFirst = "";
                if (cusIdList != null) {
                    if (cusIdList.length() >= 10) {
                        cusIdFirst = cusIdList.substring(0, 10);
                    } else {
                        cusIdFirst = cusIdList;
                    }
                }
                if("chuku".equals(ckFlag)){
                    this.getReqPkgInfo(guarBaseInfo,guarWarrantManageApp,guarWarrantInfo,cusIdFirst,cusNameList,guarNoList,gageNameList,ckFlag);
                }else if("chexiaochuku".equals(ckFlag)){
                    this.getReqPkgInfo(guarBaseInfo,guarWarrantManageApp,guarWarrantInfo,cusIdFirst,cusNameList,guarNoList,gageNameList,ckFlag);
                }
            }
            return xddb0007DataRespDto;
        }catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }catch (Exception e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }
        return xddb0007DataRespDto;
    }

    /**
    *  生成BillypReqDto对象
    * */
    public BillypReqDto getBillypReqDto(Xddb0007DataReqDto xddb0007DataReqDto,String cjxtyp,String openDay){
        BillypReqDto billypReqDto = new BillypReqDto();
        billypReqDto.setCustno(xddb0007DataReqDto.getImnManCusId());//押品所有人编号
        billypReqDto.setCustnm(xddb0007DataReqDto.getImnCusName());//押品所有人名称
        String imnManCusId = xddb0007DataReqDto.getImnManCusId();
        // 根据质押人编号
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(imnManCusId);
        logger.info("获取押品客户信息(客户主信息)："+cusBaseClientDto);
        ResultDto<CusCorpDto> CusCorpResultDto= iCusClientService.queryCusCropDtoByCusId(imnManCusId);
        logger.info("获取押品客户信息(客户对公信息)："+cusBaseClientDto);
        CusCorpDto cusCorpDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, CusCorpResultDto.getCode())) {
            //  获取相关的值并解析
            cusCorpDto = CusCorpResultDto.getData();
        } else{
            throw new BizException(null,"",null,"为查询到该客户信息，客户编号："+imnManCusId);
        }
        billypReqDto.setCustlx(cusCorpDto.getCusType());//押品所有人类型
        billypReqDto.setZjlxyp(changeCertTypeFromEcif(cusBaseClientDto.getCertType()));//押品所有人证件类型 20 组织机构代码
        billypReqDto.setZjhmyp(xddb0007DataReqDto.getDrwrCertNo());//押品所有人证件号码
        billypReqDto.setDzypnm(xddb0007DataReqDto.getCollName());//质押品名称
        billypReqDto.setCjxtyp(cjxtyp);//创建系统
        billypReqDto.setGhridy(cusBaseClientDto.getManagerId());//管户人
        billypReqDto.setZhxgry(cusBaseClientDto.getManagerId());//最后修改人
        billypReqDto.setZhxgsj(openDay);//最后更新时间
        billypReqDto.setZhxgjg(xddb0007DataReqDto.getManagerBrId());//最后修改人机构
        billypReqDto.setPjhmyp(xddb0007DataReqDto.geteBillNo());//票据号码
        billypReqDto.setCprorg(StringUtils.EMPTY);//出票人组织机构代码
        billypReqDto.setCprnmy(xddb0007DataReqDto.getDrwrName());//出票人名称
        billypReqDto.setCprlxy(StringUtils.EMPTY);//出票人类型
        billypReqDto.setCprhnm(xddb0007DataReqDto.getDrwrAcctbNm());//出票人开户行名称
        billypReqDto.setCprhno(xddb0007DataReqDto.getDrwrAcctbNo());//出票人开户行行号
        billypReqDto.setCprzhy(xddb0007DataReqDto.getDrwrAcctbAcctNo());//出票人账号
        billypReqDto.setCdrorg(StringUtils.EMPTY);//承兑人组织机构代码
        billypReqDto.setCdrhno(xddb0007DataReqDto.getAorgNo());//承兑行行号
        billypReqDto.setCdrhnm(xddb0007DataReqDto.getAccptr());//承兑人名称
        billypReqDto.setCdrlxy(StringUtils.EMPTY);//承兑人类型
        billypReqDto.setSkrorg(StringUtils.EMPTY);//收款人组织机构代码
        billypReqDto.setSkrhnm(xddb0007DataReqDto.getPyeeAcctbNm());//收款人开户行行名
        billypReqDto.setSkrhno(xddb0007DataReqDto.getPyeeAcctbNo());//收款人开户行行号
        billypReqDto.setSkrzhy(xddb0007DataReqDto.getPyeeAcctNo());//收款人账号
        billypReqDto.setSkrnmy(xddb0007DataReqDto.getPyeeName());//收款人名称
        billypReqDto.setSkrlxy(StringUtils.EMPTY);//收款人类型
        billypReqDto.setIspjqs("0");//是否有票据前手
        billypReqDto.setPjqsjg(StringUtils.EMPTY);//票据前手组织机构代码
        billypReqDto.setPjqsnm(StringUtils.EMPTY);//票据前手名称
        billypReqDto.setPjqslx(StringUtils.EMPTY);//票据前手类型
        billypReqDto.setPmjeyp(xddb0007DataReqDto.getDrftAmt());//票面金额
        billypReqDto.setBzypxt(xddb0007DataReqDto.geteBillCurType());//币种
        billypReqDto.setLlypxt(null);//利率
        billypReqDto.setCprqyp(xddb0007DataReqDto.getIsseDate());//出票日期
        billypReqDto.setPjdqrq(xddb0007DataReqDto.getEndDate());//票据到期日期
        billypReqDto.setCxcfqk(StringUtils.EMPTY);//查询查复情况
        billypReqDto.setCxcfrq(StringUtils.EMPTY);//查询查复日期
        billypReqDto.setAssetNo(xddb0007DataReqDto.geteBillNo());// 资产编号等同于票据编号
        return billypReqDto;
    }
    /**
     *  生成GuarBaseInfo对象
     * */
    public GuarBaseInfo getGuarBaseInfoObj(Xddb0007DataReqDto xddb0007DataReqDto,String cjxtyp){
        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);// 审批状态
            guarBaseInfo.setOprType(CmisBizConstants.OPR_TYPE_01);// 操作类型
            guarBaseInfo.setGuarCusId(xddb0007DataReqDto.getImnManCusId());// 押品所有人编号
            guarBaseInfo.setGuarCusName(xddb0007DataReqDto.getImnCusName());// 押品所有人名称
            guarBaseInfo.setPldimnMemo(xddb0007DataReqDto.getCollName());// 抵质押物名称（抵质押物类型名称）
            guarBaseInfo.setRightCertNo(xddb0007DataReqDto.getOwnCertNo());//权属证件号
            guarBaseInfo.setRightCertTypeCode("20006");//权属证件类型：查询不到20006
            guarBaseInfo.setEvalType("10004");//评估方式：其他
            // TODO evalAmt 评估金额
            guarBaseInfo.setEvalAmt(xddb0007DataReqDto.getDrftAmt());// 评估金额（元）
            guarBaseInfo.setEvalOrg(xddb0007DataReqDto.getEvalOrg());// 评估机构
            guarBaseInfo.setEvalDate(xddb0007DataReqDto.getEvalDate());// 评估日期
            guarBaseInfo.setEvalOrgInsCode(xddb0007DataReqDto.getEvalOrgCmonNo());// 评估机构组织机构代码
            guarBaseInfo.setConfirmAmt(xddb0007DataReqDto.getDrftAmt());//押品认定价值 = 票面金额
            guarBaseInfo.setRcDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));// 入池时间
            // 账务机构
            guarBaseInfo.setEvalDate(stringRedisTemplate.opsForValue().get("openDay"));//评估日期stringRedisTemplate.opsForValue().get("openDay")
            guarBaseInfo.setCreateSys(cjxtyp);// 创建系统/来源系统 STD_DATA_SOURCE
            guarBaseInfo.setAccountManager(xddb0007DataReqDto.getManagerId());// 管户人
            guarBaseInfo.setUpdId(xddb0007DataReqDto.getManagerId());// 最后修改人
            guarBaseInfo.setCurType("CNY");// 一期默认 人民币
            guarBaseInfo.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));// 最后修改日期
            guarBaseInfo.setUpdBrId(xddb0007DataReqDto.getManagerBrId());// 最后修改人机构
            guarBaseInfo.setFinaBrId(xddb0007DataReqDto.getFinaBrId());// 账务机构
            guarBaseInfo.setIsPjc(xddb0007DataReqDto.getDrfpoImnFlag());// 是否票据池
            guarBaseInfo.setInputId(xddb0007DataReqDto.getManagerId());// 登记人
            guarBaseInfo.setInputBrId(xddb0007DataReqDto.getManagerBrId());// 登记机构
            guarBaseInfo.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));// 登记日期
            guarBaseInfo.setGuarTypeCd("ZY0301005");// 担保分类代码
            guarBaseInfo.setGuarType("10014");// 抵质押分类
            guarBaseInfo.setMortagageRate(new BigDecimal("100"));//设定质押率（%）
            guarBaseInfo.setCommonAssetsInd("2");//是否共同财产 Y是N否
            guarBaseInfo.setGrtFlag("02");// 质押物类型
            guarBaseInfo.setManagerId(xddb0007DataReqDto.getManagerId());
            guarBaseInfo.setManagerBrId(xddb0007DataReqDto.getManagerBrId());
        }catch (Exception e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
        }
        return guarBaseInfo;
    }
    /**
     *  生成Co3200ReqDto对象
     * */
    public Co3200ReqDto getCo3200ReqDto(Xddb0007DataReqDto xddb0007DataReqDto,GuarBaseInfo guarBaseInfo,String grpNo){
        Co3200ReqDto co3200ReqDto = new Co3200ReqDto();
        String yewusx01 = PUBUtilTools.changeloanGlRoleCde("10014");//转换抵质押品类型对应核算分类代码
        String CurType = PUBUtilTools.currencyType(guarBaseInfo.getCurType());
        co3200ReqDto.setDkkhczbz("4");//开户操作标志1--录入、2--修改、3--复核、4--直通
        co3200ReqDto.setDzywbhao(grpNo);// 核心担保编号
        co3200ReqDto.setDzywminc(xddb0007DataReqDto.getCollName());//抵质押物名称
        co3200ReqDto.setYngyjigo(xddb0007DataReqDto.getManagerBrId());//营业机构
        co3200ReqDto.setDzywzlei("14");//抵质押物种类
        co3200ReqDto.setDizyfshi("2");//抵质押方式1--抵押 2--质押
        co3200ReqDto.setSyrkhhao(xddb0007DataReqDto.getImnManCusId());//受益人客户号
        co3200ReqDto.setSyrkhmin(xddb0007DataReqDto.getImnCusName());//受益人客户名
        co3200ReqDto.setSyqrkehh(xddb0007DataReqDto.getImnManCusId());//所有权人客户号
        co3200ReqDto.setSyqrkehm(xddb0007DataReqDto.getImnCusName());//所有权人客户名
        co3200ReqDto.setHuobdhao(CurType);//货币代号
        co3200ReqDto.setMinyjiaz(xddb0007DataReqDto.getDrftAmt());//名义价值
        co3200ReqDto.setShijjiaz(xddb0007DataReqDto.getDrftAmt());//实际价值
        co3200ReqDto.setPingjiaz(xddb0007DataReqDto.getDrftAmt());//评估价值
        co3200ReqDto.setShengxrq(xddb0007DataReqDto.getIsseDate().replace("-",""));//生效日期
        co3200ReqDto.setDaoqriqi(xddb0007DataReqDto.getEndDate().replace("-",""));//到期日期
        co3200ReqDto.setGlywbhao(xddb0007DataReqDto.geteBillNo());//关联业务编号  --传权证号(理财传理财产品代码)
        co3200ReqDto.setYewusx01(yewusx01);//记账余额属性
        return co3200ReqDto;
    }
    /**
    * 获取guarWarrantInfo对象,并插入表中
    * */
    public int insertGuarWarrantInfo(Xddb0007DataReqDto xddb0007DataReqDto,String grpNo,String serno, Co3200RespDto co3200RespDto,String openDay){
        logger.info("新增权证记录");
        GuarWarrantInfo guarWarrantInfo = new GuarWarrantInfo();
        guarWarrantInfo.setPkId(UUID.randomUUID().toString().replace("-",""));//PK
//        guarWarrantInfo.setGuarCusId(co3200RespDto.getSyqrkehh());//押品所有人编号
//        guarWarrantInfo.setGuarCusName(co3200RespDto.getSyqrkehm());//押品所有人名称
        guarWarrantInfo.setGrtFlag("02");//押品类型:01抵押  02质押
        guarWarrantInfo.setWarrantNo(xddb0007DataReqDto.getOwnCertNo());// 权证编号 = 资产编号 = 票号
        guarWarrantInfo.setCoreGuarantyNo(grpNo);//核心担保编号
        guarWarrantInfo.setCoreGuarantySeq("14");//核心担保品序号(抵质押物种类)
        guarWarrantInfo.setCertiRecordId(grpNo);//权利凭证号
        guarWarrantInfo.setCertiCatalog(StringUtils.EMPTY);//权证类别
        guarWarrantInfo.setCertiTypeCd(StringUtils.EMPTY);//权证类型
        guarWarrantInfo.setIsEWarrant("1");//是否电子权证 0否 1是
        guarWarrantInfo.setWarrantName(co3200RespDto.getDzywminc());//权证名称
        guarWarrantInfo.setCertiOrgName(StringUtils.EMPTY);//权证发证机关名称
        guarWarrantInfo.setCertiStartDate(StringUtils.EMPTY);//权证发证日期
        guarWarrantInfo.setCertiEndDate(StringUtils.EMPTY);//权证到期日期
        guarWarrantInfo.setWarrantInputDate(StringUtils.EMPTY);//权证登记日期
        guarWarrantInfo.setInDate(openDay);//权证入库日期
        guarWarrantInfo.setOutDate(StringUtils.EMPTY);//权证正常出库日期
        guarWarrantInfo.setCertiAmt(xddb0007DataReqDto.getClaimAmt());//权利金额
        guarWarrantInfo.setMortOrderFlag(StringUtils.EMPTY);//押品顺位标识
        guarWarrantInfo.setDepositOrgNo(StringUtils.EMPTY);//管存机关
        guarWarrantInfo.setFinaBrId(xddb0007DataReqDto.getFinaBrId());//账务机构
        guarWarrantInfo.setCertiComment(StringUtils.EMPTY);//权证备注信息
        guarWarrantInfo.setTempBorrowerName(StringUtils.EMPTY);//权证临时借用人名称
        guarWarrantInfo.setTempOutDate(StringUtils.EMPTY);//权证临时出库日期
        guarWarrantInfo.setPreBackDate(StringUtils.EMPTY);//权证预计归还时间
        guarWarrantInfo.setRealBackDate(StringUtils.EMPTY);//权证实际归还日期
        guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_04);//权证状态
        guarWarrantInfo.setRemark(StringUtils.EMPTY);//备注
        guarWarrantInfo.setInputId(StringUtils.EMPTY);//登记人
        guarWarrantInfo.setInputBrId(StringUtils.EMPTY);//登记机构
        guarWarrantInfo.setInputDate(openDay);//登记日期
        guarWarrantInfo.setUpdId(xddb0007DataReqDto.getManagerBrId());//最后修改人
        guarWarrantInfo.setUpdBrId(xddb0007DataReqDto.getManagerBrId());//最后修改机构
        guarWarrantInfo.setUpdDate(openDay);//最后修改日期
        guarWarrantInfo.setOprType("01");//操作类型
        guarWarrantInfo.setManagerId(xddb0007DataReqDto.getManagerId());//主管客户经理
        guarWarrantInfo.setManagerBrId(xddb0007DataReqDto.getManagerBrId());//主管机构

        int result = guarWarrantInfoMapper.insertSelective(guarWarrantInfo);
        return result;
    }
    /**
     * 根据权属证件号查询记录是否存在登记人为DP0000，抵押物状态为非（正常出库和部分出库）
    **/
    public boolean checkIfExit(GuarBaseInfo guarBaseInfo,Xddb0007DataRespDto xddb0007DataRespDto){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("rightCertNo",guarBaseInfo.getRightCertNo());//权属证件号
        queryModel.addCondition("inputId",guarBaseInfo.getInputId());//登记人
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.queryExistsNoCK(queryModel);
        if(guarBaseInfoList!=null && guarBaseInfoList.size()>0 ){
            if(guarBaseInfoList.get(0).getRcDate() == null || "".equals(guarBaseInfoList.get(0).getRcDate())){
                xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0007DataRespDto.setOpMsg("已存在该笔票号的信息,请手工入池！");
                return false;
            }
        }else if(guarBaseInfoList!=null && guarBaseInfoList.size()>0 ){
            if(guarBaseInfoList.get(0).getRcDate()!=null || !"".equals(guarBaseInfoList.get(0).getRcDate())){
                xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0007DataRespDto.setOpMsg("该笔票号已入池！");
                return false;
            }
        }
        return true;
    }
    /**
    * 初始化参数
    * */
    public void init(Xddb0007DataReqDto xddb0007DataReqDto){

    }
    /**
     * 撤销质押
     * */
    public Xddb0007DataRespDto  deletePbasicAndPbil(GuarBaseInfo guarBaseInfo ){
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();
        xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);

        try{
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("rightCertNo",guarBaseInfo.getRightCertNo());//权属证件号
            queryModel.addCondition("inputId",guarBaseInfo.getInputId());//登记人
            int result = guarBaseInfoMapper.deleteGuarBaseInfoByRightCertNo(guarBaseInfo);
            return xddb0007DataRespDto;
        }catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }catch (Exception e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        }
        return xddb0007DataRespDto;
    }

    /**
     * 封装发送核心报文（chuku）
     * @param guarBaseInfo
     * @param guarWarrantInfo
     * @param cusIdFirst
     * @param cusNameList
     * @param gageNameList
     * @param ckFlag
     * @return
     */
    public void getReqPkgInfo(GuarBaseInfo guarBaseInfo,GuarWarrantManageApp guarWarrantManageApp, GuarWarrantInfo guarWarrantInfo,String cusIdFirst, String cusNameList,String guarNoList, String gageNameList, String ckFlag){
        // 币种
        String curType = guarBaseInfo.getCurType();
        // 币种转换核心币种
        String hxbz = toConverCurrency(curType);
        if("chuku".equals(ckFlag)){
            Co3202ReqDto co3202ReqDto = getCo3202ReqDto(guarBaseInfo,guarWarrantInfo,cusIdFirst, guarNoList,cusNameList,gageNameList,hxbz);
            ResultDto<Co3202RespDto> co3202ResultDto = dscms2CoreCoClientService.co3202(co3202ReqDto);
            // 判断核心 出库是否成功，更新担保权证出库申请
            if("0".equals(co3202ResultDto.getCode())){
                // 发送押品核心
                updateZywStatus(co3202ResultDto,guarWarrantManageApp,guarWarrantInfo,gageNameList);
            }
        }else{
            //撤销出库
            Ib1241ReqDto ib1241ReqDto = getIb1241ReqDto(guarWarrantManageApp);
            logger.info("流水号【"+guarWarrantManageApp.getSerno()+"】，发核心系统进行冲正 开始："+ ib1241ReqDto);
            ResultDto<Ib1241RespDto> ib1241ResultDto = dscms2CoreIbClientService.ib1241(ib1241ReqDto);
            logger.info("流水号【"+guarWarrantManageApp.getSerno()+"】，发核心系统进行冲正 结束："+ ib1241ResultDto);
            if("0".equals(ib1241ResultDto.getCode())){
                //更新权证出库申请、权证台账的状态为10006已入库
                guarWarrantInfo.setCertiState("04");//入库已记账
                guarWarrantInfoMapper.updateByPrimaryKeySelective(guarWarrantInfo);
            }
        }
    }
    /**
     * 发送核心后，chuku 更改权证出库申请表的权证状态和出库日期
     * */
    public void  updateZywStatus( ResultDto<Co3202RespDto> co3202ResultDto,GuarWarrantManageApp guarWarrantManageApp,GuarWarrantInfo guarWarrantInfo,String gageNameList){
        //核心返回数据
        Co3202RespDto co3202RespDto = co3202ResultDto.getData();
        String jiaoyils = co3202RespDto.getJiaoyils();//交易流水
        String jiaoyirq = co3202RespDto.getJiaoyirq();//交易日期

        guarWarrantManageApp.setHxDate(jiaoyirq);
        guarWarrantManageApp.setHxSerno(jiaoyils);
        guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);

        //2.根据入库流水号，更新权证台账的权证状态和权证出库时间。
        guarWarrantInfo.setCertiState("10");//出库已记账
        guarWarrantInfo.setOutDate(jiaoyirq);//权证正常出库日期
        guarWarrantInfoMapper.updateByPrimaryKeySelective(guarWarrantInfo);

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("ccDate",stringRedisTemplate.opsForValue().get("openDay"));//出池日期
        queryModel.addCondition("coreGrtNo",guarWarrantManageApp.getCoreGuarantyNo());
        //3.通过入库流水号，更新抵质押基本信息表的押品状态为出库正常10008 ，票据池状态为出池2，出池日期。
        guarBaseInfoMapper.updateYpstatusByGuarNo(queryModel);
        //------------缺少表部分start-------------------
        /*String statusCode="";
        String operateType_liushui="";
        if("票据【贴现】".equals(gageNameList.trim())){
            //贴现的质押品信息直接入库
            statusCode = "10001";
        }else{
            statusCode = "10010";//出库将押品状态恢复到未入库
        }*/
        //如果是建账时间段，状态直接改未入库状态.************************ 缺少PubInterfaceParamPO********************
       /* if("1".equals(po.getIsTurnDay())){
            statusCode="10001";
        }*/
        //operateType_liushui="3";//出入库历史表需要
       //**************************************************************缺少GRT_GP_INOUT_HIS出入库流水表**************
        //------------缺少表部分end---------------------

        //4.出入库状态同步押品系统
        CetinfReqDto cetinfReqDto =  getCetinfReqDto(guarWarrantManageApp);
        dscms2YpxtClientService.cetinf(cetinfReqDto);
    }

    /**
     *  生成发送押品的对象(解质押-Cetinf对象)
     * */
    public CetinfReqDto getCetinfReqDto(GuarWarrantManageApp guarWarrantManageApp){
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        //通过核心担保编号，查询权证台账信息。
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);
        CetinfReqDto cetinfReqDto = new CetinfReqDto();

        cetinfReqDto.setDbhtbh(guarWarrantManageApp.getGuarContNo());//担保合同编号
        cetinfReqDto.setDjjgyp(guarWarrantManageApp.getInputBrId());//登记机构
        cetinfReqDto.setDjrmyp(guarWarrantManageApp.getInputId());//登记人
        cetinfReqDto.setDjrqyp(guarWarrantManageApp.getInputDate());// 登记日期
        cetinfReqDto.setDyswbs(guarWarrantInfo.getMortOrderFlag());//抵押顺位标识
        cetinfReqDto.setOperat("01");//操作
        cetinfReqDto.setQljeyp(guarWarrantInfo.getCertiAmt()+"");//权利金额
        cetinfReqDto.setQlpzhm(guarWarrantInfo.getWarrantNo());//权利凭证号
        cetinfReqDto.setQzbzxx("");//权证备注信息
        cetinfReqDto.setQzdqrq(guarWarrantInfo.getCertiEndDate());//权证到期日期
        cetinfReqDto.setQzffrq(guarWarrantInfo.getCertiStartDate()); //权证发证日期
        cetinfReqDto.setQzfzjg(guarWarrantInfo.getCertiOrgName());//权证发证机关名称
        cetinfReqDto.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(guarWarrantInfo.getCertiTypeCd()));//权证类型
        cetinfReqDto.setQzztyp(guarWarrantInfo.getCertiState());// 权证状态
        cetinfReqDto.setSernoy(coreGuarantyNo);//核心担保编号

        List<CetinfListInfo> list = new ArrayList<>();

        QueryModel model = new QueryModel();
        model.addCondition("coreGuarantyNo",coreGuarantyNo);
        List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectAll(model);

        if (CollectionUtils.nonEmpty(guarContRelWarrants)){
            for (GuarContRelWarrant guarContRelWarrant : guarContRelWarrants) {
                String guarNo = guarContRelWarrant.getGuarNo();
                CetinfListInfo cetinfInfo = new CetinfListInfo();
                cetinfInfo.setYptybh(guarNo);//押品编号
                list.add(cetinfInfo);
            }
        }
        cetinfReqDto.setList(list);
        return cetinfReqDto;
    }

    /**
     *  生成Co3202ReqDto对象(解质押-chuku)
     * */
    public Co3202ReqDto getCo3202ReqDto(GuarBaseInfo guarBaseInfo,GuarWarrantInfo guarWarrantInfo,String cusIdFirst, String cusNameList,String guarNoList, String gageNameList,String hxbz){
        //是否票据池业务
        String sfPjc = guarBaseInfo.getIsPjc();
        Co3202ReqDto co3202ReqDto = new Co3202ReqDto();
        if("1".equals(sfPjc)){
            co3202ReqDto.setDaikczbz("3");//业务操作标志
        }else{
            co3202ReqDto.setDaikczbz("1");//业务操作标志
        }
        co3202ReqDto.setDzywbhao(guarNoList);//抵质押物编号
        co3202ReqDto.setDzywminc(gageNameList);//抵质押物名称
        co3202ReqDto.setDizyfshi("2");//抵质押方式
        co3202ReqDto.setChrkleix("2");//出入库类型
        co3202ReqDto.setSyqrkehh(cusIdFirst);//所有权人客户号
        co3202ReqDto.setSyqrkehm(cusNameList);//所有权人客户名
        co3202ReqDto.setRuzjigou(guarWarrantInfo.getFinaBrId());//入账机构
        co3202ReqDto.setHuobdhao(hxbz);//货币代号
        co3202ReqDto.setMinyjiaz(guarWarrantInfo.getCertiAmt());//名义价值
        co3202ReqDto.setShijjiaz(guarWarrantInfo.getCertiAmt());//实际价值
        co3202ReqDto.setDizybilv(null); //抵质押比率
        co3202ReqDto.setKeyongje(null);//可用金额
        co3202ReqDto.setShengxrq(guarWarrantInfo.getCertiStartDate().replace("-", "").substring(0,8));//生效日期
        co3202ReqDto.setDaoqriqi(guarWarrantInfo.getCertiEndDate().replace("-", "").substring(0,8));// 到期日期
        co3202ReqDto.setDzywztai("2");//抵质押物状态
        co3202ReqDto.setZhaiyoms(""); //摘要
        return co3202ReqDto;
    }
    /**
     *  生成Ib1241ReqDto对象
     * */
    public Ib1241ReqDto getIb1241ReqDto(GuarWarrantManageApp guarWarrantManageApp){
        String hxDate =  guarWarrantManageApp.getHxDate();//权证出库日期
        String hxSerno = guarWarrantManageApp.getHxSerno();//出入库流水号
        Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
        ib1241ReqDto.setYjiaoyrq(hxDate);//原交易日期
        ib1241ReqDto.setYgyliush(hxSerno);//原交易流水
        ib1241ReqDto.setYqzhriqi("");//原主机日期（原核心交易日期）
        ib1241ReqDto.setYqzhlshu("");//原主机流水（原核心柜员流水）
        ib1241ReqDto.setQianzhrq("");//前置日期
        ib1241ReqDto.setQianzhls("");//前置流水
        return ib1241ReqDto;
    }

    /*
     * 新核心改造 币种映射 信贷--->核心
     * @param xdbz 信贷币种码值
     * @return hxbz 核心币种码值
     */
    public static String toConverCurrency(String xdbz){
        String hxbz = "";
        if("CNY".equals(xdbz)){
            hxbz = "01";
        }else if("MOP".equals(xdbz)){//澳门币
            hxbz = "81";
        }else if("CAD".equals(xdbz)){//加元
            hxbz = "28";
        }else if("CHF".equals(xdbz)){//瑞士法郎
            hxbz = "15";
        }else if("JPY".equals(xdbz)){//日元
            hxbz = "27";
        }else if("EUR".equals(xdbz)){//欧元
            hxbz = "38";
        }else if("GBP".equals(xdbz)){//英镑
            hxbz = "12";
        }else if("HKD".equals(xdbz)){//港币
            hxbz = "13";
        }else if("AUD".equals(xdbz)){//澳元
            hxbz = "29";
        }else if("USD".equals(xdbz)){//美元
            hxbz = "14";
        }else if("SGD".equals(xdbz)){//新加坡元
            hxbz = "18";
        }else if("SEK".equals(xdbz)){//瑞典克郎
            hxbz = "21";
        }else if("DKK".equals(xdbz)){//丹麦克朗
            hxbz = "22";
        }else if("NOK".equals(xdbz)){//挪威克朗
            hxbz = "23";
        }else{
            hxbz = xdbz;//未匹配到的币种发信贷的币种过去（DEM 德国马克;MSD 克鲁赛罗;NLG 荷兰盾;BEF 比利时法郎;ITL 意大利里拉;FRF 法国法郎;ATS 奥地利先令;FIM 芬兰马克）
        }
        return hxbz;
    }
    /**
     *  信贷证件类型转核心证件类型
     * @param ecifCertType
     * @return
     */
    public static String changeCertTypeFromEcif(String ecifCertType){
        String certType ="";
        if(ecifCertType ==null||"".equals(ecifCertType)){
            return certType;
        }
        if(ecifCertType.equals("A")){
            certType="10";
        }else if(ecifCertType.equals("B")){
            certType="12";
        }else if(ecifCertType.equals("C")){
            certType="11";
        }else if(ecifCertType.equals("D")){
            certType="15";
        }else if(ecifCertType.equals("E")){
            certType="16";
        }else if(ecifCertType.equals("G")){
            certType="13";
        }else if(ecifCertType.equals("H")){
            certType="14";
        }else if(ecifCertType.equals("S")){
            certType="18";
        }else if(ecifCertType.equals("T")||ecifCertType.equals("F")||ecifCertType.equals("I")||ecifCertType.equals("J")||ecifCertType.equals("K")||ecifCertType.equals("L")){
            certType="28";
        }else if(ecifCertType.equals("Q")){
            certType="20";
        }else if(ecifCertType.equals("V")){
            certType="22";
        }else if(ecifCertType.equals("U")){
            certType="23";
        }else if(ecifCertType.equals("M")){
            certType="24";
        }else if(ecifCertType.equals("R")){
            certType="25";
        }else if(ecifCertType.equals("N")){
            certType="26";
        }else if(ecifCertType.equals("P")||ecifCertType.equals("O")||ecifCertType.equals("U")){
            certType="27";
        }else if(ecifCertType.equals("W")){
            certType="29";
        }
        return certType;
    }
    /**
     * 资产池银票入池质押
     * @param xddb0007DataReqDto
     * @return
     */
    private Xddb0007DataRespDto zYPoolOption(Xddb0007DataReqDto xddb0007DataReqDto,String grpNo,String yptybh,CtrAsplDetails ctrAsplDetails,GrtGuarBizRstRel grtGuarBizRstRel) {
        logger.info("资产池银票入池质押开始");
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();
        // 申请流水号 网银流水 serno
        String serno = xddb0007DataReqDto.getSerno();
        String contNo = ctrAsplDetails.getContNo();
        // 资产出入池 记录 判断有无 出入池批次流水，有的话就更新，没就新增
        AsplIoPool asplIoPool = asplIoPoolService.queryAsplIoPoolBySerno(serno);
        // 营业时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if (Objects.isNull(asplIoPool)){
            asplIoPool = new AsplIoPool();
            asplIoPool.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
            asplIoPool.setSerno(serno);//业务流水号
            asplIoPool.setContNo(ctrAsplDetails.getContNo());//合同编号
            asplIoPool.setInoutType(CmisCommonConstants.INOUT_TYPE_1);// 出入类型
            asplIoPool.setCusId(ctrAsplDetails.getCusId());// 客户编号
            asplIoPool.setCusName(ctrAsplDetails.getCusName());// 客户名称
            asplIoPool.setLmtAmt(xddb0007DataReqDto.getDrftAmt());//批次总额 (批次金额)
            asplIoPool.setOutstndAmt(BigDecimal.ZERO);//todo 已用额度
            asplIoPool.setStartDate(ctrAsplDetails.getStartDate());//起始日期
            asplIoPool.setEndDate(ctrAsplDetails.getEndDate());//到期日期
            asplIoPool.setResn(StringUtils.EMPTY);//原因
            asplIoPool.setApprStatus(CmisCommonConstants.WF_STATUS_997);//审批状态(入池不需要走审批，默认通过)
            asplIoPool.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            asplIoPool.setInputId(ctrAsplDetails.getInputId());//登记人
            asplIoPool.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
            asplIoPool.setInputDate(openDay);//登记日期
            asplIoPool.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
            asplIoPool.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
            asplIoPool.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//最近修改日期
            asplIoPool.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
            asplIoPool.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
            asplIoPool.setCreateTime(DateUtils.getCurrTimestamp());//创建时间
            asplIoPool.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
            logger.info("资产出入池表记录新增开始");
            asplIoPoolService.insertSelective(asplIoPool);
        }else{
            // 批次金额的累加
            BigDecimal lmtAmt =  asplIoPool.getLmtAmt();
            asplIoPool.setLmtAmt(lmtAmt.add(xddb0007DataReqDto.getDrftAmt()).setScale(4,BigDecimal.ROUND_DOWN));
            asplIoPool.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//最近修改日期
            logger.info("资产出入池表记录更新开始");
            asplIoPoolService.updateSelective(asplIoPool);
        }
        // 资产出入池明细表
        AsplIoPoolDetails asplIoPoolDetails = new AsplIoPoolDetails();
        asplIoPoolDetails.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
        asplIoPoolDetails.setSerno(serno);//业务流水号
        asplIoPoolDetails.setContNo(contNo);//合同编号
        asplIoPoolDetails.setAssetNo(xddb0007DataReqDto.getOwnCertNo());//资产编号
        asplIoPoolDetails.setAssetType("01");//资产类型 01银行承兑汇票（电子）
        asplIoPoolDetails.setAssetValue(xddb0007DataReqDto.getDrftAmt());//资产价值
        asplIoPoolDetails.setAssetEndDate(xddb0007DataReqDto.getEndDate());//资产到期日
        asplIoPoolDetails.setAssetStatus(StringUtil.EMPTY_STRING);// TODO 资产状态待确定
        asplIoPoolDetails.setIsPool(CmisCommonConstants.INOUT_TYPE_1);//是否入池
        asplIoPoolDetails.setIsPledge(CmisCommonConstants.INOUT_TYPE_1);//是否质押（目前只考虑入池质押）
        asplIoPoolDetails.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
        asplIoPoolDetails.setInputId(ctrAsplDetails.getInputId());//登记人
        asplIoPoolDetails.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
        asplIoPoolDetails.setInputDate(openDay);//登记日期
        asplIoPoolDetails.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
        asplIoPoolDetails.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
        asplIoPoolDetails.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
        asplIoPoolDetails.setUpdateTime(ctrAsplDetails.getUpdateTime());//修改时间
        // 资产出入池表 资产出入池明细表 中新增该记录
        asplIoPoolDetailsService.insertSelective(asplIoPoolDetails);
        // 资产清单表
        // 判断是否存在原资产清单（可能会出现重复出入池业务）
        logger.info("资产池内清单更新开始");
        AsplAssetsList asplAssetsList = asplAssetsListService.selectByAssetNo(contNo,xddb0007DataReqDto.getOwnCertNo());
        if (Objects.isNull(asplAssetsList)){
            AsplAssetsList assetsList = new AsplAssetsList();
            assetsList.setPkId(UUID.randomUUID().toString().replace("-",""));
            assetsList.setCusId(ctrAsplDetails.getCusId());//客户编号
            assetsList.setContNo(contNo);//合同编号
            assetsList.setAssetNo(xddb0007DataReqDto.getOwnCertNo());//资产编号
            assetsList.setAssetType("01");//资产类型
            assetsList.setAssetValue(xddb0007DataReqDto.getDrftAmt());//资产价值
            assetsList.setAssetEndDate(xddb0007DataReqDto.getEndDate());//资产到期日
            assetsList.setAssetStatus("");// TODO 资产状态待确定
            assetsList.setAssteSour(StringUtil.EMPTY_STRING);//资产来源
            assetsList.setIsPool(CmisCommonConstants.INOUT_TYPE_1);//是否入池（默认入池）
            assetsList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            assetsList.setIsPledge("1");//是否入池质押
            assetsList.setInpTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//入池时间
            assetsList.setAorgNo(xddb0007DataReqDto.getAorgNo());//行号
            assetsList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            assetsList.setInputId(ctrAsplDetails.getInputId());//登记人
            assetsList.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
            assetsList.setInputDate(openDay);//登记日期
            assetsList.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
            assetsList.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
            assetsList.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
            assetsList.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
            assetsList.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
            assetsList.setCreateTime(ctrAsplDetails.getCreateTime());//创建时间
            assetsList.setUpdateTime(ctrAsplDetails.getUpdateTime());//修改时间
            assetsList.setGuarNo(yptybh);// todo统一押品编号
            asplAssetsListService.insertSelective(assetsList);
        }else{
            // 更新客户资产清单
            asplAssetsListService.updateInpPollAsplAssets(asplAssetsList.getAssetNo(),contNo);
        }

        // 担保合同编号
        String guarContNo = grtGuarBizRstRel.getGuarContNo();
        // 担保合同挂押品
        GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
        grtGuarContRel.setPkId(UUID.randomUUID().toString().replace("-", ""));
        grtGuarContRel.setGuarContNo(guarContNo);// 担保合同编号
        grtGuarContRel.setGuarNo(yptybh);// 押品统一编号
        grtGuarContRel.setContNo(contNo);// 合同编号
        grtGuarContRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
        grtGuarContRel.setInputId(ctrAsplDetails.getInputId());//登记人
        grtGuarContRel.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
        grtGuarContRel.setInputDate(openDay);//登记日期
        grtGuarContRel.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
        grtGuarContRel.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
        grtGuarContRel.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
        grtGuarContRel.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
        grtGuarContRel.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
        grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        if(grtGuarContRelService.insertSelective(grtGuarContRel)<1){//小于500条没关系
            xddb0007DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xddb0007DataRespDto.setOpMsg("入池质押引入押品关系失败");
            return xddb0007DataRespDto;//终止foreach的循环
        }
        // 入库 权证新增(cetinf) 押品入库(certis)
        // 1、权证新增(cetinf)
        logger.info(" 权证新增(cetinf)");
        CetinfReqDto cetinfReqDto = new CetinfReqDto();
        java.util.List<CetinfListInfo> cetinfList = new ArrayList<CetinfListInfo>();
        CetinfListInfo cetinfListInfo = new CetinfListInfo();
        cetinfListInfo.setYptybh(yptybh);
        // 银票出票单张出票
        cetinfList.add(cetinfListInfo);
        cetinfReqDto.setList(cetinfList); // 押品统一编号
        cetinfReqDto.setDbhtbh(guarContNo);// 担保合同编号
        cetinfReqDto.setSernoy(grpNo);// 核心担保编号(自己生成的)
        cetinfReqDto.setDyswbs("01");// 抵押顺位标识
        cetinfReqDto.setQzlxyp("19");// 权证类型(银行承兑)
        // TODO
        cetinfReqDto.setQlpzhm(grpNo);// 权利凭证号
        cetinfReqDto.setQzfzjg("苏州不动产登记中心");// 权证发证机关名称
        cetinfReqDto.setQzffrq(openDay);// 权证发证日期
        cetinfReqDto.setQzdqrq(xddb0007DataReqDto.getEndDate());// 权证到期日期
        cetinfReqDto.setQljeyp(String.valueOf(xddb0007DataReqDto.getDrftAmt()));// 权利金额
        cetinfReqDto.setQzztyp("10006");// 权证状态
        cetinfReqDto.setQzbzxx("资产池银票入池质押");// 权证备注信息
        cetinfReqDto.setDjrmyp(ctrAsplDetails.getInputId());// 登记人
        cetinfReqDto.setDjjgyp(ctrAsplDetails.getInputId());// 登记机构
        cetinfReqDto.setDjrqyp(openDay);// 登记日期
        cetinfReqDto.setOperat("05"); // 操作
        // 发送cetinf接口
        ResultDto<CetinfRespDto> cetinfResultDto = dscms2YpxtClientService.cetinf(cetinfReqDto);
        if(!"0".equals(cetinfResultDto.getCode())){
            //失败直接返回
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(cetinfResultDto.getMessage());
            return xddb0007DataRespDto;
        }

        // 2、押品入库(certis)
        logger.info(" 押品入库(certis)");
        CertisReqDto certisReqDto = new CertisReqDto();
        List<CertisListInfo> certisListInfoList = new ArrayList<CertisListInfo>();
        CertisListInfo certisListInfo = new CertisListInfo();
        certisListInfo.setSernoy(grpNo);//核心担保编号
        certisListInfo.setQlpzhm(grpNo);//权利凭证号 (统一押品编号)
        certisListInfo.setQzlxyp("19");// 权证类型(银行承兑)
        certisListInfo.setQzztyp("10006");//权证状态
        certisListInfo.setQzrkrq(openDay);//权证入库日期  状态为10006-已入库时必输
        certisListInfo.setQzckrq(StringUtil.EMPTY_STRING);//权证正常出库日期（ 核心时间）      状态为10008-正常出库时必输
        certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称             状态为10019-已借阅时，必输
        certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期                  状态为10019-已借阅时，必输
        certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期                   状态为10019-已借阅时，必输
        certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期               状态为10020-外借归还是，必输
        certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因                   状态为10019-已借阅时，必输
        certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
        certisListInfoList.add(certisListInfo);

        certisReqDto.setList(certisListInfoList);
        ResultDto<CertisRespDto> certisResultDto =  dscms2YpxtClientService.certis(certisReqDto);
        if(!"0".equals(certisResultDto.getCode())){
            xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
            xddb0007DataRespDto.setOpMsg(certisResultDto.getMessage());
            return xddb0007DataRespDto;
        }
        xddb0007DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0007DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
        return xddb0007DataRespDto;
    }

    /**
     * 承兑行白名单额度占用
     * @param
     * @return
     */
    public void sendCmisLmt0024(Xddb0007DataReqDto xddb0007DataReqDto){
        // 额度接口已经转换过行号
//        // 承兑行总行号
//        String superBankNo = "";
//        // 获取总行号 CfgBankInfo
//        ResultDto<CfgBankInfoDto> cfgBankInfoResult = iCmisCfgClientService.selectbybankno(xddb0007DataReqDto.getAorgNo());
//        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,cfgBankInfoResult .getCode())) {
//            if(Objects.nonNull(cfgBankInfoResult.getData())){
//                // 总行行号
//                superBankNo = cfgBankInfoResult.getData().getSuperBankNo();
//            }else{
//                throw BizException.error(null, "9999", "未查询到该承兑行信息，请联系信贷管理员！");
//            }
//        }else{
//            throw BizException.error(null, "9999", "调用CFG配置查询失败，请联系信贷管理员！");
//        }

        logger.info("承兑行白名单额度占用-额度系统：");
        List<CmisLmt0024OccRelListReqDto> occRelList = new ArrayList<CmisLmt0024OccRelListReqDto>();
        // 判断是否是资产池银票出票
        CmisLmt0024OccRelListReqDto dealBiz = new CmisLmt0024OccRelListReqDto();
        dealBiz.setDealBizNo(xddb0007DataReqDto.geteBillNo());//交易业务编号
        dealBiz.setCusId(xddb0007DataReqDto.getImnManCusId());
        dealBiz.setCusName(xddb0007DataReqDto.getImnCusName());
        dealBiz.setPrdNo("YPZYRC");//产品编号
        dealBiz.setPrdName("银票质押入池");//产品名称
        dealBiz.setOrigiDealBizNo("");//原交易业务编号
        dealBiz.setOrigiRecoverType("");//原交易业务恢复类型
        dealBiz.setStartDate(xddb0007DataReqDto.getIsseDate());//起始日(出票日)
        dealBiz.setEndDate(xddb0007DataReqDto .getEndDate());//到期日(到期日)
        dealBiz.setOrgCusId(xddb0007DataReqDto.getAorgNo());//承兑行客户号
        dealBiz.setBizTotalAmt(xddb0007DataReqDto.getDrftAmt());//占用金额
        occRelList.add(dealBiz);

        CmisLmt0024ReqDto cmisLmt0024ReqDto = new CmisLmt0024ReqDto();
        cmisLmt0024ReqDto.setOccRelList(occRelList);
        cmisLmt0024ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);// 系统编号
        cmisLmt0024ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);// 金融机构代码
        cmisLmt0024ReqDto.setInputId(xddb0007DataReqDto.getManagerId());// 合同编号
        cmisLmt0024ReqDto.setInputBrId(xddb0007DataReqDto.getManagerBrId());
        cmisLmt0024ReqDto.setInputDate(DateUtils.getCurrDateStr());

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(cmisLmt0024ReqDto));
        ResultDto<CmisLmt0024RespDto> cmisLmt0024RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0024(cmisLmt0024ReqDto)).orElse(new ResultDto<>());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(cmisLmt0024RespDtoResultDto));
        if (Objects.equals(cmisLmt0024RespDtoResultDto.getCode(), "0")) {
            if(!Objects.equals(cmisLmt0024RespDtoResultDto.getData().getErrorCode(), "0000")){
                throw BizException.error(null, "9999","额度系统【cmisLmt0024】"+cmisLmt0024RespDtoResultDto.getData().getErrorMsg());
            }
        }else{
            throw BizException.error(null, "9999","额度系统【cmisLmt0024】"+cmisLmt0024RespDtoResultDto.getMessage());
        }
    }
}
