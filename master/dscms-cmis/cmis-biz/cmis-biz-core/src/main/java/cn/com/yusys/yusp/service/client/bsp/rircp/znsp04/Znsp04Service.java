package cn.com.yusys.yusp.service.client.bsp.rircp.znsp04;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;

import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import cn.com.yusys.yusp.service.Dscms2ZnwdspxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：惠享贷规则审批申请接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Znsp04Service {
    private static final Logger logger = LoggerFactory.getLogger(Znsp04Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2ZnwdspxtClientService dscms2ZnwdspxtClientService;

    /**
     * 业务逻辑处理方法：惠享贷规则审批申请接口
     *
     * @param znsp04ReqDto
     * @return
     */
    @Transactional
    public Znsp04RespDto znsp04(Znsp04ReqDto znsp04ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value, JSON.toJSONString(znsp04ReqDto));
        ResultDto<Znsp04RespDto> znsp04ResultDto = dscms2ZnwdspxtClientService.znsp04(znsp04ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value, JSON.toJSONString(znsp04ResultDto));
        String znsp04Code = Optional.ofNullable(znsp04ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String znsp04Meesage = Optional.ofNullable(znsp04ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Znsp04RespDto znsp04RespDto = new Znsp04RespDto();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, znsp04ResultDto.getCode())) {
            //  获取相关的值并解析
            znsp04RespDto = znsp04ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(znsp04Code, znsp04Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value);
        return znsp04RespDto;
    }


}
