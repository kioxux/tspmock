/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpTermChg;
import cn.com.yusys.yusp.service.IqpRepayWayChgService;
import cn.com.yusys.yusp.service.IqpTermChgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpTermChgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xuchao
 * @创建时间: 2021-01-19 21:19:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqptermchg")
public class IqpTermChgResource {
    @Autowired
    private IqpTermChgService iqpTermChgService;

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    private static final Logger log = LoggerFactory.getLogger(IqpTermChgResource.class);
	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpTermChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpTermChg> list = iqpTermChgService.selectAll(queryModel);
        return new ResultDto<List<IqpTermChg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpTermChg>> index(QueryModel queryModel) {
        List<IqpTermChg> list = iqpTermChgService.selectByModel(queryModel);
        return new ResultDto<List<IqpTermChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpTermChg> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpTermChg iqpTermChg = iqpTermChgService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpTermChg>(iqpTermChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpTermChg> create(@RequestBody IqpTermChg iqpTermChg) throws URISyntaxException {
        iqpTermChgService.insert(iqpTermChg);
        return new ResultDto<IqpTermChg>(iqpTermChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpTermChg iqpTermChg) throws URISyntaxException {
        int result = iqpTermChgService.update(iqpTermChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpTermChgService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpTermChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 期限调整申请引导页数据保存
     * @param iqpTermChg
     * @return
     */
    @PostMapping("/iqpTermChgApp/saveLeadIqpTermChgInfo")
    protected ResultDto<Integer> saveLeadIqpSpecialInfo(@RequestBody IqpTermChg iqpTermChg) throws Exception {
        int rtnData = 0;
        try{
            rtnData = iqpTermChgService.insertIqpRateChgAppInfo(iqpTermChg);
        }catch (Exception e){
            log.error(e.getMessage() ,e);
        }
        return new ResultDto<Integer>(rtnData);
    }

    /**
     * @函数名称: checkIsExistIqpTermChgBizByBillNo
     * @函数描述: 查询借据编号是否存在在途的变更业务
     * @参数与返回说明:
     * @算法描述:
     * @param iqpTermChg
     */
    @PostMapping("/iqpTermChgApp/checkIsExistIqpTermChgBizByBillNo")
    protected ResultDto<Integer> checkIsExistIqpTermChgBizByBillNo(@RequestBody IqpTermChg iqpTermChg) throws  URISyntaxException{
        String iqpSerno = iqpTermChg.getIqpSerno();
        String billNo = iqpTermChg.getBillNo();
        log.info("校验借据编号是否存在在途的变更业务开始，流水号：", iqpSerno,"，借据号："+billNo);
        int result = iqpTermChgService.checkIsExistChgBizByIqpSerno(iqpSerno,billNo);
        log.info("校验借据编号是否存在在途的变更业务结束",result);
        return new ResultDto<Integer>(result);
    }
}
