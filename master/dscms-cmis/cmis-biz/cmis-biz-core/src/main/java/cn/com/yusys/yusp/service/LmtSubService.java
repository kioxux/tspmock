/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.IqpGuarBizRelApp;
import cn.com.yusys.yusp.domain.LmtSub;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSubMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-23 10:19:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSubService {
    private static final Logger log = LoggerFactory.getLogger(LmtSubService.class);
    @Autowired
    private LmtSubMapper lmtSubMapper;
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;//业务与担保合同关系数据
    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同服务
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSub selectByPrimaryKey(String lmtLimitNo) {
        return lmtSubMapper.selectByPrimaryKey(lmtLimitNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSub> selectAll(QueryModel model) {
        List<LmtSub> records = (List<LmtSub>) lmtSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSub> list = lmtSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSub record) {
        return lmtSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSub record) {
        return lmtSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSub record) {
        return lmtSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSub record) {
        return lmtSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String lmtLimitNo) {
        return lmtSubMapper.deleteByPrimaryKey(lmtLimitNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSubMapper.deleteByIds(ids);
    }

    /**
     * 批量添加额度分项数据
     * @param lmtSubList
     * @return
     */
    public int insertBatch(List<LmtSub> lmtSubList){
        return lmtSubMapper.insertBatch(lmtSubList);
    }

    /**
     * 1、校验引入的担保合同总额是否能够覆盖授信金额
     * 2、保存额度分项数据
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateLmtSub(Map params) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "修改成功！";
        try {
            //获取授信额度以及引入的有效的担保合同关系数据中本次担保金额总和的大小
            String lmtAmtString = (String) params.get("lmtAmt");
            if (StringUtils.isBlank(lmtAmtString)) {
                rtnCode = EcbEnum.LS_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LS_UPDATE_PARAMS_EXCEPTION.value + "授信额度为空！";
                return rtnData;
            }
            //获取额度分项的额度编号
            String lmtLimitNo = (String) params.get("lmtLimitNo");
            String guarWay = (String) params.get("guarWay");
            String logPrefix = "额度分项" + lmtLimitNo + "-";
            //非信用类的担保方式，需要校验担保合同的担保总金额是否能够覆盖授信金额
            if (!guarWay.equals(CmisBizConstants.GUAR_WAY_400)) {
                log.info(logPrefix + "获取引入的担保合同数据");
                Map igMap = new HashMap();
                igMap.put("iqpSerno", lmtLimitNo);
                igMap.put("rType", CmisBizConstants.R_TYPE_1);//关联类型为"与授信关联"
                igMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);//操作类型为新增的
                igMap.put("unSecure", "true");//非解除查询条件标志位
                List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectByParams(igMap);
                if (CollectionUtils.isEmpty(iqpGuarBizRelAppList) || iqpGuarBizRelAppList.size() == 0) {
                    rtnCode = EcbEnum.LS_UPDATE_GETGUARLISTNULL_EXCEPTION.key;
                    rtnMsg = EcbEnum.LS_UPDATE_GETGUARLISTNULL_EXCEPTION.value;
                    return rtnData;
                }

                log.info(logPrefix + "获取引入的担保合同数据本次担保金额总额");
                BigDecimal totalThisGuarAmt = new BigDecimal("0");
                String guarContNos = "";//担保合同数据
                for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
                    totalThisGuarAmt = totalThisGuarAmt.add(iqpGuarBizRelApp.getThisGuarAmt());
                    guarContNos += iqpGuarBizRelApp.getGuarContNo() + ",";
                }

                log.info(logPrefix + "比较授信额度与本次担保金额总额大小");
                if (totalThisGuarAmt.compareTo(new BigDecimal(lmtAmtString)) < 0) {
                    rtnCode = EcbEnum.LS_UPDATE_GUARAMTSMALL_EXCEPTION.key;
                    rtnMsg = EcbEnum.LS_UPDATE_GUARAMTSMALL_EXCEPTION.value;
                    return rtnData;
                }
                //20210202 因为引入担保合同时校验期限是通过实时的额度分项获取的，引入之后存在授信期限修改的可能，因此额度分项数据保存时，需要校验额度分项的授信期限是否小于引入的担保合同的担保到期日
                this.checkGuarTermByGuarContNo(logPrefix, guarContNos, params);
            }

            log.info("额度分项" + lmtLimitNo + "保存额度分项数据");
            LmtSub lmtSub = new LmtSub();
            lmtSub = JSONObject.parseObject(JSON.toJSONString(params), LmtSub.class);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return rtnData;
            }

            lmtSub.setUpdId(userInfo.getLoginCode());
            lmtSub.setUpdBrId(userInfo.getOrg().getCode());
            lmtSub.setUpdDate(openday);

            int lmtSubCount = lmtSubMapper.updateByPrimaryKeySelective(lmtSub);

            if (lmtSubCount < 0) {
                throw new YuspException(EcbEnum.LS_UPDATE_UPDATELSDATA_EXCEPTION.key, EcbEnum.LS_UPDATE_UPDATELSDATA_EXCEPTION.value);
            }
        }catch(Exception e){
            log.error("修改额度分项数据异常！",e);
            throw e;
        }finally {
            rtnData.put("rtnCode",rtnCode);
            rtnData.put("rtnMsg",rtnMsg);
        }

        return rtnData;
    }

    /**
     * 校验担保合同的到期日是否能够覆盖额度分项的授信到期日
     * @param logPrefix
     * @param guarContNos
     * @param params
     */
    private void checkGuarTermByGuarContNo(String logPrefix,String guarContNos, Map params) {
        log.info(logPrefix+"校验额度分项引入的担保合同到期日是否能够覆盖额度分项的授信到期日");

        log.info(logPrefix+"计算额度分项额度到期日");
        String lmtEndDate = "";
        String oldLmtStarDate = (String)params.get("lmtStartDate");
        String oldLmtEndDate = (String)params.get("lmtEndDate");
        String isAdjTerm = (String)params.get("isAdjTerm");
        String lmtType = (String)params.get("lmtType");
        String lmtTermType = (String)params.get("lmtTermType");
        int lmtTerm = Integer.parseInt((String)params.get("lmtTerm"));

        String lmtSubStarDate = "";//额度分项起始日
        try{
            if (CmisBizConstants.LMT_TYPE_01.equals(lmtType) || CmisBizConstants.LMT_TYPE_03.equals(lmtType)) {
                lmtSubStarDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                lmtEndDate = this.calDateByTermTypeAndTerm(lmtSubStarDate, lmtTermType, lmtTerm);
            } else if (CmisBizConstants.LMT_TYPE_02.equals(lmtType) || CmisBizConstants.LMT_TYPE_04.equals(lmtType)) {
                lmtSubStarDate = oldLmtStarDate;
                if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isAdjTerm)) {//是否调整期限为是时，计算额度到期日
                    lmtEndDate = this.calDateByTermTypeAndTerm(lmtSubStarDate, lmtTermType, lmtTerm);
                } else {
                    lmtEndDate = oldLmtEndDate;
                }
            }
        } catch (Exception e) {
            log.error("额度分项数据获取异常！请检查额度分项数据是否录入完整！", e);
            throw new YuspException(EcbEnum.LS_UPDATE_GUARCONTNULL_EXCEPTION.key
                    , EcbEnum.LS_UPDATE_GUARCONTNULL_EXCEPTION.value);
        }


        log.info(logPrefix + "获取额度分项下引入的担保合同数据");
        Map queryMap = new HashMap();
        queryMap.put("guarContNos", guarContNos);
        List<GrtGuarCont> grtGuarContList = grtGuarContService.queryByParams(queryMap);
        if (CollectionUtils.isEmpty(grtGuarContList) || grtGuarContList.size() == 0) {
            throw new YuspException(EcbEnum.LS_UPDATE_GUARCONTNULL_EXCEPTION.key
                    , EcbEnum.LS_UPDATE_GUARCONTNULL_EXCEPTION.value);
        }

        log.info(logPrefix + "比较担保合同到期日与额度分项授信到期日");
        for (GrtGuarCont grtGuarCont : grtGuarContList) {
            String guarEndDate = grtGuarCont.getGuarEndDate();
            if (DateUtils.getDaysByTwoDatesDef(lmtEndDate, guarEndDate) < 0) {
                throw new YuspException(EcbEnum.LS_UPDATE_GUARCONTTERMSMALL_EXCEPTION.key
                        , EcbEnum.LS_UPDATE_GUARCONTTERMSMALL_EXCEPTION.value + "担保合同号：" + grtGuarCont.getGuarContNo());
            }
        }
    }

    /**
     * 通过期限类型，期限计算日期
     * @param dateStr
     * @param termType
     * @param term
     */
    private String calDateByTermTypeAndTerm(String dateStr,String termType,int term) {
        String calDate = "";
        if (CmisBizConstants.LMT_TERM_TYPE_001.equals(termType)) {//期限类型为年
            calDate = DateUtils.addYear(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_002.equals(termType)) {//期限类型为月
            calDate = DateUtils.addMonth(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_003.equals(termType)) {//期限类型为日
            calDate = DateUtils.addDay(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        }

        return calDate;
    }

    /**
     * 1、通过额度编号更新担保合同关系的操作类型为删除->opr_type=02
     * 2、更新额度分项主表的操作类型为删除->opr_type=02
     * @param lmtLimitNo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map deleteLmtSub(String lmtLimitNo) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "删除成功！";
        try {
            int delCount = 0;
            Map delMap = new HashMap();
            delMap.put("iqpSerno", lmtLimitNo);
            delMap.put("oprType", CmisCommonConstants.OPR_TYPE_DELETE);
            delCount = iqpGuarBizRelAppService.updateByParams(delMap);

            if (delCount < 0) {
                throw new YuspException(EcbEnum.LS_DELETE_DELGUAR_EXCEPTION.key, EcbEnum.LS_DELETE_DELGUAR_EXCEPTION.value);
            }

            LmtSub lmtSub = new LmtSub();
            lmtSub.setLmtLimitNo(lmtLimitNo);
            lmtSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            delCount = lmtSubMapper.updateByPrimaryKeySelective(lmtSub);
            if(delCount<=0){
                throw new YuspException(EcbEnum.LS_DELETE_DELLS_EXCEPTION.key, EcbEnum.LS_DELETE_DELLS_EXCEPTION.value);
            }
        }catch(Exception e){
            log.error("删除额度分项数据异常！",e);
            throw e;
        }finally {
            rtnData.put("rtnCode",rtnCode);
            rtnData.put("rtnMsg",rtnMsg);
        }

        return rtnData;
    }

    /**
     * 通过入参更新额度分项数据
     * @param delMap
     * @return
     */
    public int updateLmtSubByParams(Map delMap) {
        return lmtSubMapper.updateLmtSubByParams(delMap);
    }
}
