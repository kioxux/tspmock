/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpContExt
 * @类描述: iqp_cont_ext数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-25 11:19:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpContExtDto extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	private String iqpSerno;

	/** 合同编号 **/
	private String contNo;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 合同金额 **/
	private java.math.BigDecimal contAmt;

	/** 合同类型 **/
	private String contType;

	/** 合同余额 **/
	private java.math.BigDecimal contBalance;

	/** 合同起始日期 **/
	private String contStartDate;

	/** 合同到期日期 **/
	private String contEndDate;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 批复编号 **/
	private String replyNo;

	/** 申请原因 **/
	private String appResn;

	/** 所属条线 **/
	private String belgLine;

	/** 展期协议编号 **/
	private String argNo;

	/** 签订日期 **/
	private String signDate;

	/** 合同状态    **/
	private String contStatus;

	/** 审批状态 **/
	private String approveStatus;

	/** 合同审批状态  **/
	private String contApproveStatus;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 主管客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String managerBrId;

	/** 操作类型 **/
	private String oprType;

	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;

	/** 借据编号 **/
	private String billNo;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;

	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;

	/** 贷款起始日 **/
	private String loanStartDate;

	/** 贷款到期日 **/
	private String loanEndDate;

	/** 展期金额 **/
	private java.math.BigDecimal extAmt;

	/** 展期到期日期 **/
	private String extEndDate;

	/** 展期次数 **/
	private String extTimes;


	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}

    /**
     * @return contType
     */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param contBalance
	 */
	public void setContBalance(java.math.BigDecimal contBalance) {
		this.contBalance = contBalance;
	}

    /**
     * @return contBalance
     */
	public java.math.BigDecimal getContBalance() {
		return this.contBalance;
	}

	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}

    /**
     * @return contStartDate
     */
	public String getContStartDate() {
		return this.contStartDate;
	}

	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate;
	}

    /**
     * @return contEndDate
     */
	public String getContEndDate() {
		return this.contEndDate;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}

    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param argNo
	 */
	public void setArgNo(String argNo) {
		this.argNo = argNo;
	}

    /**
     * @return argNo
     */
	public String getArgNo() {
		return this.argNo;
	}

	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

    /**
     * @return signDate
     */
	public String getSignDate() {
		return this.signDate;
	}

	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

    /**
     * @return contStatus
     */
	public String getContStatus() {
		return this.contStatus;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param contApproveStatus
	 */
	public void setContApproveStatus(String contApproveStatus) {
		this.contApproveStatus = contApproveStatus;
	}

    /**
     * @return contApproveStatus
     */
	public String getContApproveStatus() {
		return this.contApproveStatus;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public String getLoanStartDate() {
		return loanStartDate;
	}

	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	public String getLoanEndDate() {
		return loanEndDate;
	}

	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}

	public BigDecimal getExtAmt() {
		return extAmt;
	}

	public void setExtAmt(BigDecimal extAmt) {
		this.extAmt = extAmt;
	}

	public String getExtEndDate() {
		return extEndDate;
	}

	public void setExtEndDate(String extEndDate) {
		this.extEndDate = extEndDate;
	}

	public String getExtTimes() {
		return extTimes;
	}

	public void setExtTimes(String extTimes) {
		this.extTimes = extTimes;
	}
}