/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardTelvPerDetail;
import cn.com.yusys.yusp.repository.mapper.CreditCardTelvPerDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvPerDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardTelvPerDetailService {
    private static final Logger log = LoggerFactory.getLogger(CreditCardTelvPerDetailService.class);
    @Autowired
    private CreditCardTelvPerDetailMapper creditCardTelvPerDetailMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardTelvPerDetail selectByPrimaryKey(String pkId) {
        return creditCardTelvPerDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardTelvPerDetail> selectAll(QueryModel model) {
        List<CreditCardTelvPerDetail> records = (List<CreditCardTelvPerDetail>) creditCardTelvPerDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardTelvPerDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardTelvPerDetail> list = creditCardTelvPerDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardTelvPerDetail record) {
        return creditCardTelvPerDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardTelvPerDetail record) {
        return creditCardTelvPerDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardTelvPerDetail record) {
        return creditCardTelvPerDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardTelvPerDetail record) {
        return creditCardTelvPerDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardTelvPerDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardTelvPerDetailMapper.deleteByIds(ids);
    }

    /**
     * @param list
     * @return int
     * @author wzy
     * @date 2021/5/25 21:25
     * @version 1.0.0
     * @desc  电话调查可编辑列表保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int saveTelList(List<CreditCardTelvPerDetail> list) {
        int result = 0;
        try {
            for (int i = 0;i<list.size();i++){
                CreditCardTelvPerDetail creditCardTelvPerDetail = list.get(i);
                String pkId = creditCardTelvPerDetail.getPkId();
                if(pkId ==null || "".equals(pkId)){
                    creditCardTelvPerDetail.setPkId(UUID.randomUUID().toString());
                    result = this.insertSelective(creditCardTelvPerDetail);
                }else{
                    result = this.updateSelective(creditCardTelvPerDetail);
                }
                if(result !=1){
                    throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "新增电话调查数据异常！");
                }
            }
        }catch (Exception e){
            log.error("新增电话调查数据异常！",e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return result;
    }

    /**
     * @param serno
     * @return
     * @author wzy
     * @date 2021/6/5 15:36
     * @version 1.0.0
     * @desc  根据业务流水号查询电话调查信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CreditCardTelvPerDetail> selectBySerno(String  serno) {
        List<CreditCardTelvPerDetail> records = (List<CreditCardTelvPerDetail>) creditCardTelvPerDetailMapper.selectBySerno(serno);
        return records;
    }
}
