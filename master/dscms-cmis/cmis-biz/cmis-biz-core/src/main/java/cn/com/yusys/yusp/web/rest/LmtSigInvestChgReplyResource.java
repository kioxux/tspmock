/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.compatible.dto.Obj;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.service.LmtSigInvestAccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestChgReply;
import cn.com.yusys.yusp.service.LmtSigInvestChgReplyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestChgReplyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-31 09:41:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestchgreply")
public class LmtSigInvestChgReplyResource {
    @Autowired
    private LmtSigInvestChgReplyService lmtSigInvestChgReplyService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestChgReply>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestChgReply> list = lmtSigInvestChgReplyService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestChgReply>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestChgReply>> index(QueryModel queryModel) {
        List<LmtSigInvestChgReply> list = lmtSigInvestChgReplyService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestChgReply>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtSigInvestChgReply> show(@PathVariable("serno") String serno) {
        LmtSigInvestChgReply lmtSigInvestChgReply = lmtSigInvestChgReplyService.selectByPrimaryKey(serno);
        return new ResultDto<LmtSigInvestChgReply>(lmtSigInvestChgReply);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestChgReply> create(@RequestBody LmtSigInvestChgReply lmtSigInvestChgReply) throws URISyntaxException {
        lmtSigInvestChgReplyService.insert(lmtSigInvestChgReply);
        return new ResultDto<LmtSigInvestChgReply>(lmtSigInvestChgReply);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestChgReply lmtSigInvestChgReply) throws URISyntaxException {
        int result = lmtSigInvestChgReplyService.update(lmtSigInvestChgReply);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtSigInvestChgReplyService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestChgReplyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 查询批复变更信息
     * @param model
     * @return
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<LmtSigInvestChgReply>> selectByCondition(@RequestBody QueryModel model){
        if (StringUtils.isBlank(model.getSort())){
            model.setSort(" serno desc");
        }
        return new ResultDto<List<LmtSigInvestChgReply>>(lmtSigInvestChgReplyService.selectByCondition(model));
    }

    /**
     * 查询批复变更历史信息
     * @param model
     * @return
     */
    @PostMapping("/selectHisByCondition")
    protected ResultDto<List<LmtSigInvestChgReply>> selectHisByCondition(@RequestBody QueryModel model){
        if (StringUtils.isBlank(model.getSort())){
            model.setSort(" serno desc");
        }
        return new ResultDto<List<LmtSigInvestChgReply>>(lmtSigInvestChgReplyService.selectHisByCondition(model));
    }

    /**
     * 新增批复变更
     * @param map
     * @return
     */
    @PostMapping("/insertReplyChg")
    protected ResultDto<Map<String, Object>> insertReplyChg(@RequestBody Map<String,Object> map){
        return new ResultDto<Map<String, Object>>(lmtSigInvestChgReplyService.insertReplyChg(map));
    }

    /**
     * 逻辑删除
     * @param lmtSigInvestChgReply
     * @return
     */
    @PostMapping("/logicalDelete")
    protected  ResultDto<Integer> logicalDelete(@RequestBody LmtSigInvestChgReply lmtSigInvestChgReply){
        return new ResultDto<Integer>(lmtSigInvestChgReplyService.logicalDelete(lmtSigInvestChgReply));
    }

    /**
     *
     * 修改批复变更信息
     * @param lmtSigInvestChgReply
     * @return
     */
    @PostMapping("/updateReplyChg")
    protected  ResultDto<Integer> updateReplyChg(@RequestBody LmtSigInvestChgReply lmtSigInvestChgReply){
        return new ResultDto<Integer>(lmtSigInvestChgReplyService.updateSelective(lmtSigInvestChgReply));
    }

    /**
     * 根据流水号查询
     * @param map
     * @return
     */
    @PostMapping("/selectBySerno")
    protected  ResultDto<LmtSigInvestChgReply> selectBySerno(@RequestBody Map<String,Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<LmtSigInvestChgReply>(lmtSigInvestChgReplyService.selectByPrimaryKey(serno));
    }
}
