/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.service.AccEntrustLoanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccEntrustLoanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 21:23:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accentrustloan")
public class AccEntrustLoanResource {
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccEntrustLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccEntrustLoan> list = accEntrustLoanService.selectAll(queryModel);
        return new ResultDto<List<AccEntrustLoan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccEntrustLoan>> index(QueryModel queryModel) {
        List<AccEntrustLoan> list = accEntrustLoanService.selectByModel(queryModel);
        return new ResultDto<List<AccEntrustLoan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccEntrustLoan> show(@PathVariable("pkId") String pkId) {
        AccEntrustLoan accEntrustLoan = accEntrustLoanService.selectByPrimaryKey(pkId);
        return new ResultDto<AccEntrustLoan>(accEntrustLoan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccEntrustLoan> create(@RequestBody AccEntrustLoan accEntrustLoan) throws URISyntaxException {
        accEntrustLoanService.insert(accEntrustLoan);
        return new ResultDto<AccEntrustLoan>(accEntrustLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccEntrustLoan accEntrustLoan) throws URISyntaxException {
        int result = accEntrustLoanService.update(accEntrustLoan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accEntrustLoanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accEntrustLoanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("合同下关联贷款台账页查询")
    @PostMapping("/selectaccloanshow")
    protected ResultDto<List<AccEntrustLoan>> selectAccLoanShow(@RequestBody QueryModel queryModel) {
        List<AccEntrustLoan> list = accEntrustLoanService.selectAccLoanShow(queryModel);
        return new ResultDto<List<AccEntrustLoan>>(list);
    }

    /**
     * @函数名称:queryAll
     * @函数描述: 分页查询委托贷款台账列表信息
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccEntrustLoan>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccEntrustLoan> list = accEntrustLoanService.selectByModel(queryModel);
        return new ResultDto<List<AccEntrustLoan>>(list);
    }

    /**
     * @函数名称: queryByBillNo
     * @函数描述: 根据借据编号查询委托贷款台账数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByBillNo")
    protected ResultDto<AccEntrustLoan> queryByBillNo(@RequestBody String billNo) {
        AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(billNo);
        return new ResultDto<AccEntrustLoan>(accEntrustLoan);
    }

    /**
     * 异步下载委托贷款台账列表
     */
    @PostMapping("/exportAccEntrustLoan")
    public ResultDto<ProgressDto> asyncExportAccEntrustLoan(@RequestBody QueryModel model) {
        ProgressDto progressDto = accEntrustLoanService.asyncExportAccEntrustLoan(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:querymodelByCondition
     * @函数描述:
     * @参数与返回说明:
     * @param queryModel
     *            根据查询条件查询台账信息并返回
     * @算法描述:
     */
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccEntrustLoan>> querymodelByCondition(@RequestBody QueryModel queryModel) {
        List<AccEntrustLoan> list = accEntrustLoanService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccEntrustLoan>>(list);
    }

}
