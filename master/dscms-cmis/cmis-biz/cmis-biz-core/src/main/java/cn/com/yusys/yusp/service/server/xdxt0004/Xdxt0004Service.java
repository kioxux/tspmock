package cn.com.yusys.yusp.service.server.xdxt0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.req.Xdxt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.AreaList;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.Xdxt0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AreaOrgMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 业务逻辑类:根据工号获取所辖区域
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxt0004Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0004Service.class);

    @Resource
    private AdminSmUserService adminSmUserService;

    @Resource
    private AreaOrgMapper areaOrgMapper;
    /**
     * 根据工号获取所辖区域
     * @param xdxt0004DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0004DataRespDto getXdxt0004(Xdxt0004DataReqDto xdxt0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataReqDto));
        Xdxt0004DataRespDto xdxt0004DataRespDto = new Xdxt0004DataRespDto();
        try {
            //客户经理编号
            String managerId =xdxt0004DataReqDto.getManagerId();
            //机构号
            String orgId = "";
            ResultDto<AdminSmUserDto> adminSmUserResultDto = adminSmUserService.getByLoginCode(managerId);
            if(ResultDto.success().getCode().equals(adminSmUserResultDto.getCode())){
                AdminSmUserDto adminSmUserDto = adminSmUserResultDto.getData();
                if(!Objects.isNull(adminSmUserDto)){
                    orgId = adminSmUserDto.getOrgId();
                }
            }
            java.util.List<AreaList> areaList = new ArrayList<>();
            if(!StringUtils.isEmpty(orgId)){
                areaList = areaOrgMapper.selectAreaNameByOrgNo(orgId);
            }
            xdxt0004DataRespDto.setAreaList(areaList);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataRespDto));
        return xdxt0004DataRespDto;
    }

}
