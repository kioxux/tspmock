/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.dto.BizGuarExchangeDto;
import cn.com.yusys.yusp.dto.GrtGuarContRelDto;
import cn.com.yusys.yusp.dto.GuarEvalInfoClientDto;
import cn.com.yusys.yusp.dto.GuarGrtContRelClientDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContRelMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-19 14:00:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class GrtGuarContRelService {
    private static final Logger log = LoggerFactory.getLogger(GrtGuarContRelService.class);

    @Autowired
    private GrtGuarContRelMapper grtGuarContRelMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateService;//序列号服务接口
    @Autowired
    private IqpCertiInoutRelService iqpCertiInoutRelService;
    @Autowired
    private GrtGuarContService grtGuarContService;
    // @Autowired
    // private IGuarClientService iGuarClientService;//押品端服务接口

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public GrtGuarContRel selectByPrimaryKey(String pkId) {
        return grtGuarContRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<GrtGuarContRel> selectAll(QueryModel model) {
        List<GrtGuarContRel> records = (List<GrtGuarContRel>) grtGuarContRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GrtGuarContRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarContRel> list = grtGuarContRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GrtGuarContRel record) {
        return grtGuarContRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GrtGuarContRel record) {
        return grtGuarContRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GrtGuarContRel record) {
        return grtGuarContRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GrtGuarContRel record) {
        return grtGuarContRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return grtGuarContRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return grtGuarContRelMapper.deleteByIds(ids);
    }

    /**
     * 外部接口调用担保合同与押品关系落地服务
     *
     * @param bizGuarExchangeDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public BizGuarExchangeDto saveGrtGuarContRelInter(BizGuarExchangeDto bizGuarExchangeDto) {
        log.info("外部接口调用担保合同与押品关系落地服务开始");

        String rtnCode = EcbEnum.GGCR_DEFAULT_SUCCESS.key;
        String rtnMsg = EcbEnum.GGCR_DEFAULT_SUCCESS.value;
        try {
            if (null == bizGuarExchangeDto || StringUtils.isBlank(bizGuarExchangeDto.getGuarContNo())
                    || StringUtils.isBlank(bizGuarExchangeDto.getGuarNo())) {
                throw new YuspException(EcbEnum.GGCR_INSERTOUT_PARAMS_EXCEPTION.key, EcbEnum.GGCR_INSERTOUT_PARAMS_EXCEPTION.value);
            }

            //校验押品与担保品关系是否存在
            String guarContNo = bizGuarExchangeDto.getGuarContNo();
            String guarNo = bizGuarExchangeDto.getGuarNo();

            log.info("校验担保合同" + guarContNo + "与押品" + guarNo + "是否存在生效的关系数据！");
            GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
            grtGuarContRel.setGuarContNo(guarContNo);
            grtGuarContRel.setGuarNo(guarNo);
            grtGuarContRel.setOprType(CmisBizConstants.GGCR_OPRTYPE_ADD);
            grtGuarContRel = queryGrtGuarContRelInfo(grtGuarContRel);

            if (grtGuarContRel != null) {
                throw new YuspException(EcbEnum.GGCR_INSERTOUT_EXISTS_EXCEPTION.key, EcbEnum.GGCR_INSERTOUT_EXISTS_EXCEPTION.value);
            }

            log.info("保存担保合同" + guarContNo + "与押品" + guarNo + "关系数据！");
            GrtGuarContRel insertGrtGuarContRel = new GrtGuarContRel();
            BeanUtils.copyProperties(bizGuarExchangeDto, insertGrtGuarContRel);
            insertGrtGuarContRel.setPkId(StringUtils.uuid(true));
            insertGrtGuarContRel.setStatus(CmisBizConstants.GGCR_STATUS_EFFCT);
            insertGrtGuarContRel.setOprType(CmisBizConstants.GGCR_OPRTYPE_ADD);
            int insertCount = grtGuarContRelMapper.insertSelective(insertGrtGuarContRel);
            if (insertCount < 0) {
                throw new YuspException(EcbEnum.GGCR_INSERTOUT_INSERTFAIL_EXCEPTION.key, EcbEnum.GGCR_INSERTOUT_INSERTFAIL_EXCEPTION.value);
            }

            log.info("保存担保合同" + guarContNo + "与押品" + guarNo + "关系数据成功");
        } catch (YuspException e) {
            log.error("保存担保合同与押品关系数据异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
            throw e;
        } catch (Exception e) {
            log.error("保存担保合同与押品关系数据异常！", e);
            rtnCode = EcbEnum.GGCR_DEFAULT_EXCEPTION.key;
            rtnMsg = EcbEnum.GGCR_DEFAULT_EXCEPTION.value + "异常类型：" + e.getClass().getSimpleName() + "；异常原因：" + e.getMessage();
            throw new YuspException(rtnCode, rtnMsg);
        } finally {
            if(null != bizGuarExchangeDto){
                bizGuarExchangeDto.setRtnCode(rtnCode);
                bizGuarExchangeDto.setRtnMsg(rtnMsg);
            }
        }

        return bizGuarExchangeDto;
    }

    /**
     * 通过入参查询关系数据是否存在
     *
     * @param bizGuarExchangeDto
     * @return
     */
    public BizGuarExchangeDto queryGrtGuarContRelInfo(BizGuarExchangeDto bizGuarExchangeDto) {
        log.info("外部接口查询关系数据是否存在开始");
        String rtnCode = EcbEnum.GGCR_DEFAULT_SUCCESS.key;
        String rtnMsg = EcbEnum.GGCR_DEFAULT_SUCCESS.value;
        boolean existsFlag = false;//定义存在标识位
        try {
            if (bizGuarExchangeDto == null || StringUtils.isBlank(bizGuarExchangeDto.getGuarContNo())
                    || StringUtils.isBlank(bizGuarExchangeDto.getGuarNo())) {
                throw new YuspException(EcbEnum.GGCR_INSERTOUT_PARAMS_EXCEPTION.key, EcbEnum.GGCR_INSERTOUT_PARAMS_EXCEPTION.value);
            }

            String guarContNo = bizGuarExchangeDto.getGuarContNo();
            String guarNo = bizGuarExchangeDto.getGuarNo();

            log.info("校验担保合同" + guarContNo + "与押品" + guarNo + "是否存在生效的关系数据！");
            GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
            grtGuarContRel.setGuarContNo(guarContNo);
            grtGuarContRel.setGuarNo(guarNo);
            grtGuarContRel.setOprType(CmisBizConstants.GGCR_OPRTYPE_ADD);

            grtGuarContRel = queryGrtGuarContRelInfo(grtGuarContRel);

            if (grtGuarContRel != null) {
                log.info("担保合同" + guarContNo + "与押品" + guarNo + "存在生效的关系数据！");
                existsFlag = true;
            }
        } catch (YuspException e) {
            log.error("校验数据是否存在异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            //针对入参为空的场景，需要new一个对象防止后续空指针异常
            if (bizGuarExchangeDto == null) bizGuarExchangeDto = new BizGuarExchangeDto();
            bizGuarExchangeDto.setRtnCode(rtnCode);
            bizGuarExchangeDto.setRtnMsg(rtnMsg);
            bizGuarExchangeDto.setExistsFlag(existsFlag);
        }
        return bizGuarExchangeDto;
    }

    /**
     * 通过入参查询数据是否存在
     *
     * @param grtGuarContRel
     * @return
     */
    public GrtGuarContRel queryGrtGuarContRelInfo(GrtGuarContRel grtGuarContRel) {
        return grtGuarContRelMapper.selectByGrtGuarContRel(grtGuarContRel);
    }

    /**
     * 通过合同编号获取所有押品价值数据
     * 1、通过入参的担保合同编号(可多个入参，以,分隔) 查询grt_guar_cont_rel 获取所有的押品列表数据
     * 2、调用押品服务端，入参 押品(可多个入参，以,分割) 返回押品编号+押品评估金额信息
     * 3、累加押品返回的评估金额
     *
     * @param guarContNos 入参的担保合同编号，可多个，以【,】进行分割
     * @param exGuarNo    针对最高额担保合同，解除押品的引用时，计算押品总金额需要将待解除的押品编号排开
     * @return
     */
    public Map getGuarTotalAmt(String guarContNos, String exGuarNo) {
        Map guarTotalAmtMap = new HashMap();
        String rtnCode = EcbEnum.GGCR_DEFAULT_SUCCESS.key;
        String rtnMsg = EcbEnum.GGCR_DEFAULT_SUCCESS.value;
        BigDecimal guarTotalAmt = new BigDecimal(0);
        try {
            if (StringUtils.isBlank(guarContNos)) {
                throw new YuspException(EcbEnum.GGCR_GETGUARAMT_PARAM_EXCEPTION.key, EcbEnum.GGCR_GETGUARAMT_PARAM_EXCEPTION.value);
            }
            log.info("获取担保合同" + guarContNos + "下的押品信息开始");
            Map guarMap = new HashMap();
            guarMap.put("guarContNo", guarContNos);
            List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelMapper.selectGGCRList(guarMap);
            if (CollectionUtils.isEmpty(grtGuarContRelList)) {
                throw new YuspException(EcbEnum.GGCR_GETGUARAMT_GUARNOTEXISTS_EXCEPTION.key, EcbEnum.GGCR_GETGUARAMT_GUARNOTEXISTS_EXCEPTION.value);
            }

            //总的押品编号
            String guarNos = "";
            for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
                if (StringUtils.isBlank(exGuarNo)
                        || (!StringUtils.isBlank(exGuarNo) && !exGuarNo.equals(grtGuarContRel.getGuarNo()))) {
                    guarNos += grtGuarContRel.getGuarNo() + ",";
                }
            }
            if (guarNos.contains(",")) {
                guarNos = guarNos.substring(0, guarNos.length() - 1);
            }
            log.info("担保合同" + guarContNos + "下的押品信息为：" + guarNos);

            log.info("担保合同" + guarContNos + "获取押品总金额信息-调用接口开始");
            //定义交互的dto
            String[] guarNoAry = guarNos.split(",");
            List<Map> list = new ArrayList<>();
            for (int j = 0; j < guarNoAry.length; j++) {
                String guarNo = guarNoAry[j];
                //将json字符串转为map对象
                Map map = new HashMap();
                map.put("guarNo", guarNo);
                list.add(map);
            }
            GuarGrtContRelClientDto guarGrtContRelClientDto = new GuarGrtContRelClientDto();
            guarGrtContRelClientDto.setList(list);
            guarGrtContRelClientDto = null;//iGuarClientService.getGuarAmtInfoByGuarNos(guarGrtContRelClientDto);
            log.info("担保合同" + guarContNos + "获取押品总金额信息-调用接口结束");

            if (guarGrtContRelClientDto == null) {
                throw new YuspException(EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.key, EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.value);
            }

//            rtnCode = guarGrtContRelClientDto.key;
            if (StringUtils.isBlank(rtnCode)) {
                throw new YuspException(EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.key, EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.value);
            }

            if (!EcbEnum.GGCR_DEFAULT_SUCCESS.key.equals(rtnCode)) {
//                rtnMsg = guarGrtContRelClientDto.value;
                return guarTotalAmtMap;
            }

            List<GuarEvalInfoClientDto> guarGrtContRelBaseDtoList = guarGrtContRelClientDto.getGuarEvalInfoClientDtos();
            if (CollectionUtils.isEmpty(guarGrtContRelBaseDtoList)) {
                throw new YuspException(EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.key, EcbEnum.GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION.value);
            }

            log.info("担保合同" + guarContNos + "获取押品总金额信息-计算累加金额");
            for (GuarEvalInfoClientDto guarGrtContRelBaseDto : guarGrtContRelBaseDtoList) {
                if (guarGrtContRelBaseDto.getGuarValueAmt() == null) {
                    log.info("押品" + guarGrtContRelBaseDto.getGuarNo() + "评估加值为null");
                    continue;
                }
                guarTotalAmt = guarTotalAmt.add(guarGrtContRelBaseDto.getGuarValueAmt());
            }

            log.info("担保合同" + guarContNos + "获取押品总金额为" + guarTotalAmt.toString());
        } catch (YuspException e) {
            log.error("获取押品总价值信息异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            guarTotalAmtMap.put("guarTotalAmt", guarTotalAmt);
            guarTotalAmtMap.put("rtnCode", rtnCode);
            guarTotalAmtMap.put("rtnMsg", rtnMsg);
        }
        return guarTotalAmtMap;
    }

    /**
     * 通过入参查询符合条件的权证出入库申请
     *
     * @param params
     * @return
     */

    public List<GrtGuarContRel> selectGrtGuarCont(Map params) {
        return grtGuarContRelMapper.selectByBorrowerId(params);
    }

    /**
     * 查询担保合同与押品关系数据
     *
     * @param guarMap
     * @return
     */
    public List<GrtGuarContRel> selectGrtGuarContRelByGuarNo(Map guarMap) {
        return grtGuarContRelMapper.selectGGCRList(guarMap);
    }

    /**
     * 查询所有引用担保的押品
     *
     * @param
     * @return
     */
    public List<GrtGuarContRel> getGrtGuarContRel(String oprType) {
        return grtGuarContRelMapper.getGrtGuarContRel(oprType);
    }

    /**
     * @方法名称：getByGuarContNo
     * @方法描述：根据担保合同编号查询担保合同和担保人、物的关系
     * @创建人：zhangming12
     * @创建时间：2021/5/18 13:48
     * @修改记录：修改时间 修改人员 修改时间
    */
    public List<GrtGuarContRel> getByGuarContNo(String guarContNo){
        return grtGuarContRelMapper.getByGuarContNo(guarContNo);
    }

    /**
     * @函数名称:selectGrtGuarContRelLinkGuarBaseInfo
     * @函数描述:查询担保合同和押品基本信息列表通过关系表关联
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GrtGuarContRelDto> selectGrtGuarContRelLinkGuarBaseInfo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GrtGuarContRelDto> grtGuarContRelDtos = grtGuarContRelMapper.selectGrtGuarContRelLinkGuarBaseInfo(queryModel);
        PageHelper.clearPage();
        return grtGuarContRelDtos;
    }
    /**
     * @函数名称:selectDetailByGuarContNo
     * @函数描述:通过担保合同编号查询担保合同和押品基本信息列表通过关系表关联
     * @参数与返回说明:
     * @算法描述:
     */
    public GrtGuarContRel selectDetailByGuarContNo(String guarContNo) {
        return grtGuarContRelMapper.selectDetailByGuarContNo(guarContNo);
    }

    public List<GrtGuarContRel> selectForXw(String serno) {
        return grtGuarContRelMapper.selectForXw(serno);
    }

    public List<String> selectByGuarCopyNoList(List<String> guarCopyNoList) {
        return grtGuarContRelMapper.selectByGuarCopyNoList(guarCopyNoList);
    }

    public int insertGrtGuarContRelList(List<GrtGuarContRel> grtGuarContRelList) {
        return grtGuarContRelMapper.insertGrtGuarContRelList(grtGuarContRelList);
    }

    /**
     * 根据押品编号查询关联的担保合同不是解除状态的记录数
     * @param guarNo
     * @return
     */
    public int selectNoRelieveRecordsByGuarNo(String guarNo){
        return grtGuarContRelMapper.selectNoRelieveRecordsByGuarNo(guarNo);
    }

    public int deleteByAssetNo(String assetNo) {
        return grtGuarContRelMapper.deleteByAssetNo(assetNo);
    }


    public String selectGuarNosByGuarContNo(String guarContNo){
        return grtGuarContRelMapper.selectGuarNosByGuarContNo(guarContNo);
    }

    public int updateCertiAmt(QueryModel queryModel){
        return grtGuarContRelMapper.updateCertiAmt(queryModel);
    }

    public String selectGuarNosByContNos(String contNos){
        return grtGuarContRelMapper.selectGuarNosByContNos(contNos);
    }

    /**
     * 根据保证人id查询关联的担保合同编号
     * @param guarantyId
     * @return
     */
    public String selectGuarContNosByGuarantyId(String guarantyId){
        return grtGuarContRelMapper.selectGuarContNosByGuarantyId(guarantyId);
    }


    public String selectAorgInfoByGuarContNo(String guarContNo){
        return grtGuarContRelMapper.selectAorgInfoByGuarContNo(guarContNo);
    }
    /**
     * 根据担保合同编号删除押品关系
     * @param guarCont
     * @return
     */
    public int deleteByGuarCont(String guarCont) {
        return grtGuarContRelMapper.deleteGrtGuarContRelByGuarContNo(guarCont);
    }
}
