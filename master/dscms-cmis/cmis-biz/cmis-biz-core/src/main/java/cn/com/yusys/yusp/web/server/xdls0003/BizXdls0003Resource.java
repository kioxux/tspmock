package cn.com.yusys.yusp.web.server.xdls0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0003.req.Xdls0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0003.resp.Xdls0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:信贷授信协议合同金额查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDLS0003:信贷授信协议合同金额查询")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0003Resource.class);

    /**
     * 交易码：xdls0003
     * 交易描述：信贷授信协议合同金额查询
     *
     * @param xdls0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷授信协议合同金额查询")
    @PostMapping("/xdls0003")
    protected @ResponseBody
    ResultDto<Xdls0003DataRespDto> xdls0003(@Validated @RequestBody Xdls0003DataReqDto xdls0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003DataReqDto));
        Xdls0003DataRespDto xdls0003DataRespDto = new Xdls0003DataRespDto();// 响应Dto:信贷授信协议合同金额查询
        ResultDto<Xdls0003DataRespDto> xdls0003DataResultDto = new ResultDto<>();
        try {
            String cusId = xdls0003DataReqDto.getCusId();//核心客户号
            String curtDate = xdls0003DataReqDto.getCurtDate();//当前日期
            // 从xdls0003DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdls0003DataRespDto对象开始
            xdls0003DataRespDto.setCrdLmtTotal(StringUtils.EMPTY);// 授信协议合同金额合计
            // TODO 封装xdls0003DataRespDto对象结束
            // 封装xdls0003DataResultDto中正确的返回码和返回信息
            xdls0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, e.getMessage());
            // 封装xdls0003DataResultDto中异常返回码和返回信息
            // TODO EpbEnum.EPB099999 待调整 开始
            xdls0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EpbEnum.EPB099999 待调整  结束
        }
        // 封装xdls0003DataRespDto到xdls0003DataResultDto中
        xdls0003DataResultDto.setData(xdls0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003DataRespDto));
        return xdls0003DataResultDto;
    }
}
