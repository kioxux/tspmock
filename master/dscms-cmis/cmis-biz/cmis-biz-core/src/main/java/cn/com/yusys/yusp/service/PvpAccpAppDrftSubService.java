/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpAccpAppDrftSub;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppDrftSubMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppDrftSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-26 09:07:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpAccpAppDrftSubService {

    @Autowired
    private PvpAccpAppDrftSubMapper pvpAccpAppDrftSubMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpAccpAppDrftSub selectByPrimaryKey(String pkId) {
        return pvpAccpAppDrftSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpAccpAppDrftSub> selectAll(QueryModel model) {
        List<PvpAccpAppDrftSub> records = (List<PvpAccpAppDrftSub>) pvpAccpAppDrftSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpAccpAppDrftSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpAccpAppDrftSub> list = pvpAccpAppDrftSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpAccpAppDrftSub record) {
        return pvpAccpAppDrftSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpAccpAppDrftSub record) {
        return pvpAccpAppDrftSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpAccpAppDrftSub record) {
        return pvpAccpAppDrftSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpAccpAppDrftSub record) {
        return pvpAccpAppDrftSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pvpAccpAppDrftSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpAccpAppDrftSubMapper.deleteByIds(ids);
    }

    /**
     * 获取票据明细
     *
     * @param serno
     * @return
     */
    public List<PvpAccpAppDrftSub> selectBySerno(String serno) {
        return pvpAccpAppDrftSubMapper.selectBySerno(serno);
    }


    /**
     * 票据明细批量插入
     * @param pvpAccpAppDrftSubList
     * @return
     */
    public boolean insertList(List<PvpAccpAppDrftSub> pvpAccpAppDrftSubList) {
        return pvpAccpAppDrftSubMapper.insertList(pvpAccpAppDrftSubList)>0?true:false;
    }

    /**
     * 获取票据明细
     *
     * @param accpPvpSeq
     * @return
     */
    public List<PvpAccpAppDrftSub> queryByAccpPvpSeq(String accpPvpSeq) {
        return pvpAccpAppDrftSubMapper.queryByAccpPvpSeq(accpPvpSeq);
    }

    /**
     * 删除票据明细
     *
     * @param pcSerno
     * @return
     */
    public int deleteByPcSerno(String pcSerno) {
        return pvpAccpAppDrftSubMapper.deleteByPcSerno(pcSerno);
    }
}
