package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CreditCtrLoanCont;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.CreditCtrLoanContService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className SingleBatchApp
 * @Description 业务展期
 * @Date 2020/12/21 : 10:48
 */
@Service
public class XKYW06BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(XKYW06BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CreditCtrLoanContService creditCtrLoanContService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String contNo = instanceInfo.getBizId();
        CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContService.selectByPrimaryKey(contNo);
        String extSerno = creditCtrLoanCont.getSerno();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
                updateContStatus(extSerno,"100");
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                creditCtrLoanContService.handleBusinessDataAfterEnd(extSerno,"1");
                //推送影像信息
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(extSerno);
                imageApprDto.setIsApproved("1");
                imageApprDto.setOutcode("XXD_ZXFQ");
                imageApprDto.setOpercode(instanceInfo.getCurrentUserId());
                cmisBizXwCommonService.sendImage(imageApprDto);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                    updateContStatus(extSerno,"700");
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 以流程标识-进入流程
     * @param instanceInfo
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.XKYW06.equals(flowCode);
    }

    //更新审批状态
    public void updateStatus(String serno,String state){
        CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContService.selectBySerno(serno);
        creditCtrLoanCont.setApproveStatus(state);
        creditCtrLoanContService.updateSelective(creditCtrLoanCont);
    }

    //更新状态
    public void updateContStatus(String serno,String state){
        CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContService.selectBySerno(serno);
        creditCtrLoanCont.setContStatus(state);
        creditCtrLoanContService.updateSelective(creditCtrLoanCont);
    }
}
