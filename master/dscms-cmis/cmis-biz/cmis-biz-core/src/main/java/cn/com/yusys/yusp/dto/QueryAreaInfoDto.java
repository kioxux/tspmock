package cn.com.yusys.yusp.dto;

public class QueryAreaInfoDto {
    //客户经理所属分中心
    private String areaName;
    //分中心负责人工号
    private String userNo;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNO) {
        this.userNo = userNO;
    }
}
