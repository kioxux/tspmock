/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-dto模块
 * @类名称: RptFncSituBsAnys
 * @类描述: rpt_fnc_situ_bs_anys数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-13 15:46:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_fnc_situ_bs_anys")
public class RptFncSituBsAnys extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 流水号
     **/
    @Id
    @Column(name = "SERNO")
    private String serno;

    /**
     * 科目类型
     **/
    @Column(name = "SUBJECT_TYPE", unique = false, nullable = true, length = 5)
    private String subjectType;

    /**
     * 科目名称
     **/
    @Column(name = "SUBJECT_NAME", unique = false, nullable = true, length = 40)
    private String subjectName;

    /**
     * 金额
     **/
    @Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lmtAmt;

    /**
     * 简要说明或分析
     **/
    @Column(name = "BRIEF_DESCRIPTION", unique = false, nullable = true, length = 65535)
    private String briefDescription;

    /**
     * 最近两年末金额
     **/
    @Column(name = "LAST_TWO_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lastTwoYear;

    /**
     * 最近一年末金额
     **/
    @Column(name = "LAST_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lastYear;

    /**
     * 当年月末金额
     **/
    @Column(name = "CUR_YEAR_LAST_MONTH", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal curYearLastMonth;

    /**
     * 自制报表金额
     **/
    @Column(name = "AMT_SR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal amtSr;

    /**
     * 税务报表金额
     **/
    @Column(name = "AMT_TR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal amtTr;

    /**
     * 实际金额
     **/
    @Column(name = "AMT_DIFF", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal amtDiff;

    /**
     * 报表类型标识
     **/
    @Column(name = "FNC_FLAG", unique = false, nullable = true, length = 5)
    private String fncFlag;

    /**
     * 排序标识
     **/
    @Column(name = "SORT", unique = false, nullable = true, length = 11)
    private Integer sort;
    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;


    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param subjectType
     */
    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    /**
     * @return subjectType
     */
    public String getSubjectType() {
        return this.subjectType;
    }

    /**
     * @param subjectName
     */
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * @return subjectName
     */
    public String getSubjectName() {
        return this.subjectName;
    }

    /**
     * @param lmtAmt
     */
    public void setLmtAmt(java.math.BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    /**
     * @return lmtAmt
     */
    public java.math.BigDecimal getLmtAmt() {
        return this.lmtAmt;
    }

    /**
     * @param briefDescription
     */
    public void setBriefDescription(String briefDescription) {
        this.briefDescription = briefDescription;
    }

    /**
     * @return briefDescription
     */
    public String getBriefDescription() {
        return this.briefDescription;
    }

    /**
     * @param lastTwoYear
     */
    public void setLastTwoYear(java.math.BigDecimal lastTwoYear) {
        this.lastTwoYear = lastTwoYear;
    }

    /**
     * @return lastTwoYear
     */
    public java.math.BigDecimal getLastTwoYear() {
        return this.lastTwoYear;
    }

    /**
     * @param lastYear
     */
    public void setLastYear(java.math.BigDecimal lastYear) {
        this.lastYear = lastYear;
    }

    /**
     * @return lastYear
     */
    public java.math.BigDecimal getLastYear() {
        return this.lastYear;
    }

    /**
     * @param curYearLastMonth
     */
    public void setCurYearLastMonth(java.math.BigDecimal curYearLastMonth) {
        this.curYearLastMonth = curYearLastMonth;
    }

    /**
     * @return curYearLastMonth
     */
    public java.math.BigDecimal getCurYearLastMonth() {
        return this.curYearLastMonth;
    }

    /**
     * @param amtSr
     */
    public void setAmtSr(java.math.BigDecimal amtSr) {
        this.amtSr = amtSr;
    }

    /**
     * @return amtSr
     */
    public java.math.BigDecimal getAmtSr() {
        return this.amtSr;
    }

    /**
     * @param amtTr
     */
    public void setAmtTr(java.math.BigDecimal amtTr) {
        this.amtTr = amtTr;
    }

    /**
     * @return amtTr
     */
    public java.math.BigDecimal getAmtTr() {
        return this.amtTr;
    }

    /**
     * @param amtDiff
     */
    public void setAmtDiff(java.math.BigDecimal amtDiff) {
        this.amtDiff = amtDiff;
    }

    /**
     * @return amtDiff
     */
    public java.math.BigDecimal getAmtDiff() {
        return this.amtDiff;
    }

    /**
     * @param fncFlag
     */
    public void setFncFlag(String fncFlag) {
        this.fncFlag = fncFlag;
    }

    /**
     * @return fncFlag
     */
    public String getFncFlag() {
        return this.fncFlag;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}