/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import cn.com.yusys.yusp.domain.DiscountBp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DiscountBpMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zy
 * @创建时间: 2021-06-05 14:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DiscountBpMapper {

    /**
     * @方法名称: queryDiscountBpByCusIdAndDate
     * @方法描述: 根据客户号和系统日期查询优惠点数信息
     * @参数与返回说明: 
     * @算法描述: 无
     */
    DiscountBp queryDiscountBpByCusIdAndDate(QueryModel model);
    /**
     * @方法名称:queryContHisDiscountBpConStr
     * @方法描述: 根据客户号、合同号查询历史优惠次数信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal queryContHisDiscountBpConStr(QueryModel model);
   
}