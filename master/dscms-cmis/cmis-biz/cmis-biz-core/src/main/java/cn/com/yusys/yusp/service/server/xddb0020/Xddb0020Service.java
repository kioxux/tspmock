package cn.com.yusys.yusp.service.server.xddb0020;


import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0020.req.Xddb0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0020.resp.Xddb0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.service.client.common.SmLookupItemService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:根据客户名查询抵押物类型
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xddb0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0020Service.class);

    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private SmLookupItemService smLookupItemService;

    /**
     * 根据客户名查询抵押物类型
     *
     * @param xddb0020DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0020DataRespDto xddb0020(Xddb0020DataReqDto xddb0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value);
        Xddb0020DataRespDto xddb0020DataRespDto = new Xddb0020DataRespDto();

        try {
            String guarCusNames = xddb0020DataReqDto.getCus_name();//押品编号（支持多个，逗号分隔：）

            List<String> guarCusNameList = new ArrayList<>();
            if (guarCusNames.contains(",")) {
                String[] cusNameAry = guarCusNames.split(",");
                for (int i = 0; i < cusNameAry.length; i++) {
                    String guarCusName = cusNameAry[i];
                    guarCusNameList.add(guarCusName);
                }
            } else {
                guarCusNameList.add(guarCusNames);
            }

            //查询押品信息
            Map map = new HashMap();
            map.put("guarCusNames", guarCusNameList);
            logger.info("***********XDDB0020查询押品信息开始,查询参数为:{}", JSON.toJSONString(map));
            List<cn.com.yusys.yusp.dto.server.xddb0020.resp.List> retLists = guarBaseInfoMapper.selectGuarBaseInfoByCusName(map);
            logger.info("***********XDDB0021根据管户人号查询客户经理结束,返回结果为:{}", JSON.toJSONString(retLists));

            //字典翻译 STD_ZB_GUAR_STATE 押品状态名
            //字典翻译 STD_ZB_GAGE_TYPE 抵押物类型名
            //字典翻译 STD_ZB_RIGHT_CERT_T 抵押物权属证件类型名
            //字典翻译 STD_ZB_EVAL_TYPE 评估方式名
            //查询对应的字典
            logger.info("从Redis中获取字典项缓存开始");
            retLists = Optional.ofNullable(retLists).orElseThrow(() -> new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value));
            retLists = retLists.parallelStream().map(ret -> {
                cn.com.yusys.yusp.dto.server.xddb0020.resp.List temp = new cn.com.yusys.yusp.dto.server.xddb0020.resp.List();
                BeanUtils.copyProperties(ret, temp);
                //押品状态名码值转换
                String status_name = temp.getStatus_name();//参数值
                String statusName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_STATE", status_name);
                temp.setStatus_name(statusName);
                //抵押物权属证件类型名码值转换
                String right_cert_type_name = temp.getRight_cert_type_name();
                String rightCertTypeName = DictTranslatorUtils.findValueByDictKey("STD_RIGHT_CERT_TYPE_CODE", right_cert_type_name);
                temp.setRight_cert_type_name(rightCertTypeName);
                //评估方式名码值转换
                String eval_type_name = temp.getEval_type_name();
                String evalTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_EVAL_TYPE", eval_type_name);
                temp.setEval_type_name(evalTypeName);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            logger.info("从Redis中获取字典项缓存结束");


            //返回信息
            xddb0020DataRespDto.setList(retLists);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value);
        return xddb0020DataRespDto;
    }
}
