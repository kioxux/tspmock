package cn.com.yusys.yusp.web.server.xdzc0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0004.req.Xdzc0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.resp.Xdzc0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0004.Xdzc0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户资产清单查询接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0004:客户资产清单查询接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0004Resource.class);

    @Autowired
    private Xdzc0004Service xdzc0004Service;
    /**
     * 交易码：xdzc0004
     * 交易描述：客户资产清单查询接口
     *
     * @param xdzc0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产清单查询接口")
    @PostMapping("/xdzc0004")
    protected @ResponseBody
    ResultDto<Xdzc0004DataRespDto> xdzc0004(@Validated @RequestBody Xdzc0004DataReqDto xdzc0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004DataReqDto));
        Xdzc0004DataRespDto xdzc0004DataRespDto = new Xdzc0004DataRespDto();// 响应Dto:客户资产池下资产清单列表查询（出池时查询）
        ResultDto<Xdzc0004DataRespDto> xdzc0004DataResultDto = new ResultDto<>();
        try {
            // 从xdzc0004DataReqDto获取业务值进行业务逻辑处理
            xdzc0004DataRespDto = xdzc0004Service.xdzc0004Service(xdzc0004DataReqDto);
            // 封装xdzc0004DataResultDto中正确的返回码和返回信息
            xdzc0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, e.getMessage());
            // 封装xdzc0004DataResultDto中异常返回码和返回信息
            xdzc0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0004DataRespDto到xdzc0004DataResultDto中
        xdzc0004DataResultDto.setData(xdzc0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004DataResultDto));
        return xdzc0004DataResultDto;
    }
}
