/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccOperApp
 * @类描述: lmt_grp_reply_acc_oper_app数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:42:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_reply_acc_oper_app")
public class LmtGrpReplyAccOperApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 集团申请流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;

	/** 额度操作类型 **/
	@Column(name = "LMT_ACC_OPER_TYPE", unique = false, nullable = true, length = 5)
	private String lmtAccOperType;

	/** 集团授信台账编号 **/
	@Column(name = "GRP_ACC_NO", unique = false, nullable = true, length = 40)
	private String grpAccNo;

	/** 集团授信台账编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;

	/** 集团授信台账编号 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;

	/** 原因 **/
	@Column(name = "RESN", unique = false, nullable = true, length = 4000)
	private String resn;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	//页面需要展示的其他字段
	private String cusId;
	private String cusName;
	private String cusType;
	private String openLmtAmt;
	private String lowRiskLmtAmt;
	private String isAdjustFlag;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	/**
	 * @return grpSerno
	 */
	public String getGrpSerno() {
		return this.grpSerno;
	}

	/**
	 * @param lmtAccOperType
	 */
	public void setLmtAccOperType(String lmtAccOperType) {
		this.lmtAccOperType = lmtAccOperType;
	}

	/**
	 * @return lmtAccOperType
	 */
	public String getLmtAccOperType() {
		return this.lmtAccOperType;
	}

	/**
	 * @param grpAccNo
	 */
	public void setGrpAccNo(String grpAccNo) {
		this.grpAccNo = grpAccNo;
	}

	/**
	 * @return grpAccNo
	 */
	public String getGrpAccNo() {
		return this.grpAccNo;
	}

	public String getGrpCusId() {
		return grpCusId;
	}

	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}

	public String getGrpCusName() {
		return grpCusName;
	}

	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}

	/**
	 * @param resn
	 */
	public void setResn(String resn) {
		this.resn = resn;
	}

	/**
	 * @return resn
	 */
	public String getResn() {
		return this.resn;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getOpenLmtAmt() {
		return openLmtAmt;
	}

	public void setOpenLmtAmt(String openLmtAmt) {
		this.openLmtAmt = openLmtAmt;
	}

	public String getLowRiskLmtAmt() {
		return lowRiskLmtAmt;
	}

	public void setLowRiskLmtAmt(String lowRiskLmtAmt) {
		this.lowRiskLmtAmt = lowRiskLmtAmt;
	}

	public String getIsAdjustFlag() {
		return isAdjustFlag;
	}

	public void setIsAdjustFlag(String isAdjustFlag) {
		this.isAdjustFlag = isAdjustFlag;
	}
}