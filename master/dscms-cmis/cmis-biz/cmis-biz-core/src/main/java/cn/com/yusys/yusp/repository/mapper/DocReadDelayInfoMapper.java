/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.vo.DocReadDelayInfoVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.DocReadDelayInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDelayInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 09:36:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocReadDelayInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocReadDelayInfo selectByPrimaryKey(@Param("drdiSerno") String drdiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocReadDelayInfoVo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocReadDelayInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocReadDelayInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocReadDelayInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocReadDelayInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("drdiSerno") String drdiSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: updateDocStsDelay
     * @方法描述: 调阅延期申请通过的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateDocStsDelay(Map<String,Object> parm);
    /**
     * @方法名称: updateAccessBackDateDelay
     * @方法描述: 调阅延期申请通过的时候更新档案调阅申请表中的归还时间
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateAccessBackDateDelay(Map<String,Object> parm);

    /**
     * 获取最新延期归还日期
     * @author jijian_yx
     * @date 2021/10/14 2:43
     **/
    String getDelayBackDate(@Param("draiSerno") String draiSerno);

    /**
     * 更新调阅明细状态
     * @author jijian_yx
     * @date 2021/10/31 20:56
     **/
    int updateDocStsRead(Map<String, Object> param);
}