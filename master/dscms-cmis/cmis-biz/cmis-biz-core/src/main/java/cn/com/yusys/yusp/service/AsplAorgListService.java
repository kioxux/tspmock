/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.AsplAorgList;
import cn.com.yusys.yusp.domain.AsplAorgListOutPort;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AsplAorgListMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AsplAorgListService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplAorgListService {

    @Resource
    private AsplAorgListMapper asplAorgListMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplAorgList selectByPrimaryKey(String pkId) {
        return asplAorgListMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplAorgList> selectAll(QueryModel model) {
        List<AsplAorgList> records = (List<AsplAorgList>) asplAorgListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplAorgList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplAorgList> list = asplAorgListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplAorgList record) {
        return asplAorgListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplAorgList record) {
        return asplAorgListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplAorgList record) {
        return asplAorgListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplAorgList record) {
        return asplAorgListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplAorgListMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplAorgListMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: exportAsplAorgList
     * @方法描述: 导出
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ProgressDto exportAsplAorgList(AsplAorgList asplAorgList) {
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("headBankNo", asplAorgList.getHeadBankNo());
            queryModel.getCondition().put("intBankName", asplAorgList.getIntbankName());
            queryModel.getCondition().put("oprType", CmisCommonConstants.ADD_OPR);
            List<AsplAorgList> listTest = selectByModel(queryModel);
            DataAcquisition dataAcquisition = (page, size, object) -> {
                QueryModel queryModeTemp = (QueryModel)object;
                queryModeTemp.setPage(page);
                queryModeTemp.setSize(size);
                return selectByModel(queryModeTemp);
            };
            ExportContext exportContext = ExportContext.of(AsplAorgListOutPort.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
            return ExcelUtils.asyncExport(exportContext);
        } catch (Exception e) {
            throw new YuspException(EcbEnum.E_OUTPORTEXCEL_EXCEPTION_01.key,EcbEnum.E_OUTPORTEXCEL_EXCEPTION_01.value);
        }

    }

    /**
     * 批量插入承兑行白名单
     *
     * @param asplAorgListOutPortList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int importAsplAorgList(List<AsplAorgListOutPort> asplAorgListOutPortList) {
        List<AsplAorgList> asplAorgList = (List<AsplAorgList>) BeanUtils.beansCopy(asplAorgListOutPortList, AsplAorgList.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            for (AsplAorgList asplAorgListExcel : asplAorgList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("headBankNo", asplAorgListExcel.getHeadBankNo());
                query.addCondition("ecidNo", asplAorgListExcel.getEcifNo());
                query.addCondition("intBankName", asplAorgListExcel.getIntbankName());
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                List<AsplAorgList> asplAorgListData = asplAorgListMapper.selectByModel(query) ;
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(asplAorgListData)){
                    AsplAorgList asplAorgListUpdate = asplAorgListData.get(0);
                    // 总行行号
                    asplAorgListUpdate.setHeadBankNo(asplAorgListExcel.getHeadBankNo());
                    // ECIF编号
                    asplAorgListUpdate.setEcifNo(asplAorgListExcel.getEcifNo());
                    // 同业名称
                    asplAorgListUpdate.setIntbankName(asplAorgListExcel.getIntbankName());
                    // 信用等级
                    asplAorgListUpdate.setCreditLevel(asplAorgListExcel.getCreditLevel());
                    // 质押率
                    asplAorgListUpdate.setPldimnRate(asplAorgListExcel.getPldimnRate());
                    // 生效状态
                    asplAorgListUpdate.setStatus(CommonConstance.STATUS_1);
                    //最近修改人
                    asplAorgListUpdate.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    asplAorgListUpdate.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    asplAorgListUpdate.setUpdDate(sf.format(new Date()));
                    //修改时间
                    asplAorgListUpdate.setUpdateTime(new Date());
                    asplAorgListMapper.updateByPrimaryKeySelective(asplAorgListUpdate);

                    //新增名单信息日志记录

                }else{//数据库中不存在，新增信息
                    //生成主键
                    Map paramMap= new HashMap<>();
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    asplAorgListExcel.setPkId(pkValue);
                    // 生效状态
                    asplAorgListExcel.setStatus(CommonConstance.STATUS_1);
                    //操作类型
                    asplAorgListExcel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //登记人
                    asplAorgListExcel.setInputId(userInfo.getLoginCode());
                    //登记机构
                    asplAorgListExcel.setInputBrId(userInfo.getOrg().getCode());
                    //登记日期
                    asplAorgListExcel.setInputDate(sf.format(new Date()));
                    //管户经理
                    asplAorgListExcel.setManagerId(userInfo.getLoginCode());
                    //管户机构
                    asplAorgListExcel.setManagerBrId(userInfo.getOrg().getCode());
                    //最近修改人
                    asplAorgListExcel.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    asplAorgListExcel.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    asplAorgListExcel.setUpdDate(sf.format(new Date()));
                    //创建时间
                    asplAorgListExcel.setCreateTime(new Date());
                    asplAorgListMapper.insertSelective(asplAorgListExcel);

                    //新增名单信息日志记录
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
            sqlSession.close();
        }
        return asplAorgListOutPortList.size();
    }

    /**
     * 异步下载资产池承兑行名单模板
     * @return 导出进度信息
     */
    public ProgressDto exportAsplAorgListModel() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(AsplAorgListOutPort.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 承兑行校验(是否存在，是否生效) 返回有效名单
     * @param asplAorgList
     * @return
     */
    public List<String> isAsplAorgList(List<String> asplAorgList){
        if(CollectionUtils.isEmpty(asplAorgList)){
            return null;
        }
        return asplAorgListMapper.isAsplAorgList(asplAorgList);
    }

    public AsplAorgList selectByHeadBankNo(String superBankNo) {
        return asplAorgListMapper.selectByHeadBankNo(superBankNo);
    }

    public int deleteByHeadBankNo(String aorgNo) {
        return asplAorgListMapper.deleteByHeadBankNo(aorgNo);
    }

    public int updateByHeadBankNo(AsplAorgList asplAorgList) {
        return asplAorgListMapper.updateByHeadBankNo(asplAorgList);
    }
}
