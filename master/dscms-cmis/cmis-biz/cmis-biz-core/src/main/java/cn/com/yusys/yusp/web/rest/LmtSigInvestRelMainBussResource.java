/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestRelMainBuss;
import cn.com.yusys.yusp.service.LmtSigInvestRelMainBussService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelMainBussResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 11:18:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestrelmainbuss")
public class LmtSigInvestRelMainBussResource {
    @Autowired
    private LmtSigInvestRelMainBussService lmtSigInvestRelMainBussService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestRelMainBuss>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestRelMainBuss> list = lmtSigInvestRelMainBussService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestRelMainBuss>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestRelMainBuss>> index(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestRelMainBuss> list = lmtSigInvestRelMainBussService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestRelMainBuss>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestRelMainBuss> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss = lmtSigInvestRelMainBussService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestRelMainBuss>(lmtSigInvestRelMainBuss);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<LmtSigInvestRelMainBuss> create(@RequestBody LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss) throws URISyntaxException {
        lmtSigInvestRelMainBussService.insert(lmtSigInvestRelMainBuss);
        return new ResultDto<LmtSigInvestRelMainBuss>(lmtSigInvestRelMainBuss);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss) throws URISyntaxException {
        int result = lmtSigInvestRelMainBussService.update(lmtSigInvestRelMainBuss);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestRelMainBussService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestRelMainBussService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 单个对象逻辑删除
     * @param condition
     * @return
     */
    @PostMapping("/deleteLogicByPkId")
    public ResultDto<Integer> deleteLogicByPkId(@RequestBody Map condition){
        String pkId = (String) condition.get("pkId");
        int result = lmtSigInvestRelMainBussService.deleteLogicByPkId(pkId);
        return new ResultDto<>(result);
    }
}
