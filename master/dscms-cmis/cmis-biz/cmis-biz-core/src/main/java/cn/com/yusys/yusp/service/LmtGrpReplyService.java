/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtGrpReplyMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-20 11:06:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpReplyService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    @Resource
    private LmtGrpReplyMapper lmtGrpReplyMapper;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtReplyChgService lmtReplyChgService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtGrpReply selectByPrimaryKey(String pkId) {
        return lmtGrpReplyMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpReply> selectAll(QueryModel model) {
        List<LmtGrpReply> records = (List<LmtGrpReply>) lmtGrpReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtGrpReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpReply> list = lmtGrpReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtGrpReply record) {
        record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        return lmtGrpReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpReply record) {
        return lmtGrpReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtGrpReply record) {
        return lmtGrpReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpReply record) {
        return lmtGrpReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpReplyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpReplyMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：getOriginReply
     * @方法描述：台账里的批复号取原批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-04-20 上午 11:17
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReply> getOriginReply(String grpReplyNo) {
        List<LmtGrpReply> lmtReplies = new ArrayList<>();
        LmtGrpReply lmtGrpReply = new LmtGrpReply();
        try {
            if (StringUtils.isBlank(grpReplyNo)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //现在台账对应的批复
            lmtGrpReply = lmtGrpReplyMapper.getReply(grpReplyNo);
            if (lmtGrpReply == null) {
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String originReplySerno = lmtGrpReply.getOrigiGrpReplySerno();
            lmtReplies.add(lmtGrpReply);
            int count = 0;
            while (!(originReplySerno == null || "".equals(originReplySerno) || grpReplyNo.equals(originReplySerno))) {
                lmtGrpReply = lmtGrpReplyMapper.getOriginReply(originReplySerno);
                if (lmtGrpReply == null) {
                    break;
                }
                if (count > 100){
                    break;
                }
                lmtReplies.add(lmtGrpReply);
                originReplySerno = lmtGrpReply.getOrigiGrpReplySerno();
                count ++;
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("查询批复历史沿革！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return lmtReplies;
    }

    /**
     * @方法名称：qryReplyNo
     * @方法描述：查询可变更的批复号
     * @参数与返回说明：
     * @算法描述：当前客户经理下的所有批复，再经过所有合同申请表过滤，剩下的是可变更的批复号
     * @创建人：ywl
     * @创建时间：2021-04-17 下午 4:46
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReply> qryChangeableReply(QueryModel queryModel) {
        List<LmtGrpReply> lmtGrpReplies = lmtGrpReplyMapper.selectByModel(queryModel);
        return lmtGrpReplies;
    }

    /**
     * @方法名称: 生成批复
     * @方法描述: 通过授信审批数据生成授信批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */

    @Transactional
    public void generateLmtGrpReplyHandleByLmtGrpAppr(LmtGrpAppr lmtGrpAppr, String currentUserId, String currentOrgId) throws Exception {
        if(!CmisCommonConstants.LMT_TYPE_01.equals(lmtGrpAppr.getLmtType()) && !CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpAppr.getLmtType())){
            // 其他类型先终止原来批复，生成新的批复。
            invalidatedOldLmtGrpReply(lmtGrpAppr);
        }
        // 生成新的批复
        generateLmtGrpReplyInfoByLmtGrpAppr(lmtGrpAppr, currentUserId, currentOrgId);
    }

    /**
     * @方法名称:
     * @方法描述: 生成集团子成员批复
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpReplyInfoByLmtGrpAppr(LmtGrpAppr lmtGrpAppr, String currentUserId, String currentOrgId) throws Exception {
        String lmtGrpReplySerno = generateLmtGrpReply(lmtGrpAppr, currentUserId, currentOrgId);
        lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpReplyByLmtGrpAppr(currentUserId, currentOrgId, lmtGrpAppr.getGrpApproveSerno(), lmtGrpReplySerno);
    }

    /**
     * @方法名称:
     * @方法描述: 生成集团批复
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    private String generateLmtGrpReply(LmtGrpAppr lmtGrpAppr, String currentUserId, String currentOrgId) {
        LmtGrpReply lmtGrpReply = new LmtGrpReply();
        BeanUtils.copyProperties(lmtGrpAppr, lmtGrpReply);
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(lmtGrpAppr.getGrpSerno());
        String repaySerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("生成新的集团授信批复流水号:" + repaySerno);
        lmtGrpReply.setPkId(UUID.randomUUID().toString());
        lmtGrpReply.setGrpReplySerno(repaySerno);
        lmtGrpReply.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        lmtGrpReply.setReplyInureDate(openday);
        lmtGrpReply.setInputId(lmtGrpApp.getManagerId());
        lmtGrpReply.setInputBrId(lmtGrpApp.getManagerBrId());
        lmtGrpReply.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtGrpReply.setUpdId(currentUserId);
        lmtGrpReply.setUpdBrId(currentOrgId);
        lmtGrpReply.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtGrpReply.setManagerId(lmtGrpApp.getManagerId());
        lmtGrpReply.setManagerBrId(lmtGrpApp.getManagerBrId());
        lmtGrpReply.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtGrpReply.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        // 批复状态
        lmtGrpReply.setReplyStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        // 原批复编号
        lmtGrpReply.setOrigiGrpReplySerno(lmtGrpAppr.getOrigiLmtReplySerno());
        // 用信审核方式
        lmtGrpReply.setLoanApprMode(lmtGrpAppr.getLoanApprMode());
        // 审核模式
        lmtGrpReply.setApprMode(lmtGrpAppr.getApprMode());
        // 终审机构类型  现默认为 总行处理
        lmtGrpReply.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        // 审批结论  通过当前授信申请流水号 查询授信申请的当前审批状态
        HashMap map = new HashMap();
        map.put("oprType", CommonConstance.OPR_TYPE_ADD);
        map.put("grpSerno", lmtGrpAppr.getGrpSerno());
        log.info("通过当前授信申请流水号:"+lmtGrpAppr.getGrpSerno()+"查询授信申请的当前审批状态");
        List<LmtGrpApp> lmtGrpAppList = lmtGrpAppService.selectLmtGrpAppByParams(map);
        if(!lmtGrpAppList.isEmpty() && lmtGrpAppList.size()>0) {
            lmtGrpReply.setApprResult(lmtGrpAppList.get(0).getApproveStatus());
        }
        this.insert(lmtGrpReply);
        log.info("生成新的集团授信批复:" + lmtGrpReply.getGrpReplySerno());
        return repaySerno;
    }

    /**
     * @方法名称:
     * @方法描述: 终止集团批复
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void invalidatedOldLmtGrpReply(LmtGrpAppr lmtGrpAppr) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("grpReplySerno", lmtGrpAppr.getOrigiLmtReplySerno());
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpReply> lmtGrpReplyList = this.queryLmtGrpReplyByParams(queryMap);
        if(lmtGrpReplyList != null && lmtGrpReplyList.size() == 1){
            LmtGrpReply oldLmtGrpReply = lmtGrpReplyList.get(0);
            oldLmtGrpReply.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_03);
            this.update(oldLmtGrpReply);
            // 集团成员批复终止
            lmtReplyService.invalidatedOldLmtReplyForLmtGrpReply(lmtGrpAppr.getOrigiLmtReplySerno());
        }else{
            throw new Exception("查询原批复数据异常！");
        }
    }

    /**
     * @方法名称:queryLmtGrpReplyByParams
     * @方法描述:通过条件查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtGrpReply> queryLmtGrpReplyByParams(HashMap<String, String> queryMap) {
        return lmtGrpReplyMapper.queryLmtReplyDataByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtGrpReplyByGrpReplySerno
     * @方法描述: 通过集团授信申请流水号查询集团批复
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReply queryLmtGrpReplyByGrpReplySerno(String grpReplySerno)  {
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("grpReplySerno", grpReplySerno);
        lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 生效日期倒排
        List<LmtGrpReply> lmtGrpReplyList = queryLmtGrpReplyByParams(lmtReplyQueryMap);
        LmtGrpReply lmtGrpReply = null;
        if (lmtGrpReplyList != null && lmtGrpReplyList.size() == 1) {
            lmtGrpReply = lmtGrpReplyList.get(0);
        } else {
            BizException.error(null, EcbEnum.ECB010071.key, EcbEnum.ECB010071.value);
        }
        return lmtGrpReply;
    }

    /**
     * @方法名称: queryLmtGrpReplyByGrpSerno
     * @方法描述: 通过集团授信申请流水号查询集团批复
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReply queryLmtGrpReplyByGrpSerno(String grpSerno) throws Exception {
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("grpSerno", grpSerno);
        lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 生效日期倒排
        List<LmtGrpReply> lmtGrpReplyList = queryLmtGrpReplyByParams(lmtReplyQueryMap);
        LmtGrpReply lmtGrpReply = null;
        if (lmtGrpReplyList != null && lmtGrpReplyList.size() == 1) {
            lmtGrpReply = lmtGrpReplyList.get(0);
        } else {
            throw new Exception("授信批复数据获取异常");
        }
        return lmtGrpReply;
    }

    /**
     * @方法名称：queryUnchangeableReply
     * @方法描述：查询不可变更的批复号
     * @参数与返回说明：
     * @算法描述：当前客户经理下的所有批复，再经过所有合同申请表过滤，剩下的是不可变更的批复号
     * @创建人：qw
     * @创建时间：2021-05-21 下午 2:37
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReply> queryUnchangeableReply(QueryModel queryModel) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        queryModel.addCondition("managerId",managerId);
        List<LmtGrpReply> lmtGrpReplies = lmtGrpReplyMapper.queryUnchangeableReply(queryModel);
        return lmtGrpReplies;
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：获取当前登录人下的批复信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-04-29 上午 9:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReply> queryByManagerId() {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("managerId", managerId);
        return lmtGrpReplyMapper.queryLmtReplyDataByParams(paramMap);
    }

    /**
     * @方法名称：viewLmtGrpReplyByGrpReplySerno
     * @方法描述：集团授信原批复查看
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-07-5 下午 9:07
     * @修改记录：修改时间   修改人员  修改原因
     */

    public LmtGrpReply viewLmtGrpReplyByGrpReplySerno(String grpReplySerno) {
        return lmtGrpReplyMapper.viewLmtGrpReplyByGrpReplySerno(grpReplySerno);
    }

    /**
     * @方法名称:
     * @方法描述: 更新授信批复
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void updateLmtGrpReplyHandleByLmtGrpReplyChg(LmtGrpReplyChg lmtGrpReplyChg, String currentUserId, String currentOrgId) throws Exception {
        // 通过集团授信批复变更更新集团授信批复
        this.updateLmtGrpReplyByLmtGrpReplyChg(lmtGrpReplyChg, currentUserId, currentOrgId);
        // 获取到所有集团子成员变更的数据
        log.info("根据集团申请流水号从集团授信成员关系表获取集团子成员的申请流水号开始");
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.selectLmtGrpReplyByGrpSerno(lmtGrpReplyChg.getGrpSerno());
        for (LmtGrpMemRel lmtGrpMemRel: lmtGrpMemRelList) {
            LmtReplyChg lmtReplyChg = lmtReplyChgService.queryLmtReplyChgBySerno(lmtGrpMemRel.getSingleSerno());
            lmtReplyService.updateLmtReplyHandleByLmtReplyChg(lmtReplyChg, currentUserId, currentOrgId);
        }
        log.info("根据集团申请流水号从集团授信成员关系表获取集团子成员的申请流水号结束");
    }

    /**
     * @方法名称: generateNewLmtReplyByLmtReplyChg
     * @方法描述: 通过授信审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtGrpReplyByLmtGrpReplyChg(LmtGrpReplyChg lmtGrpReplyChg, String currentUserId, String currentOrgId) throws Exception {
        LmtGrpReply lmtGrpReply = this.queryLmtGrpReplyByReplySerno(lmtGrpReplyChg.getGrpReplySerno());
        //拷贝一个对象 使用该对象进行更新
        LmtGrpReply updateLmtGrpReply = (LmtGrpReply) SerializationUtils.clone(lmtGrpReply);
        BeanUtils.copyProperties(lmtGrpReplyChg, updateLmtGrpReply);
        log.info("将集团授信批复变更记录里的信息拷贝到新建的集团授信批复对象updateLmtGrpReply里，updateLmtGrpReply："+updateLmtGrpReply);
        updateLmtGrpReply.setPkId(lmtGrpReply.getPkId());
        //集团申请流水号
        updateLmtGrpReply.setGrpSerno(lmtGrpReply.getGrpSerno());
        updateLmtGrpReply.setGrpReplySerno(lmtGrpReply.getGrpReplySerno());
        updateLmtGrpReply.setUpdId(currentUserId);
        updateLmtGrpReply.setUpdBrId(currentOrgId);
        updateLmtGrpReply.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        updateLmtGrpReply.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        log.info("新建的集团授信批复对象updateLmtGrpReply："+updateLmtGrpReply);
        this.update(updateLmtGrpReply);
    }

    /**
     * @方法名称:
     * @方法描述: 通过集团申请流水号查询集团批复数据
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReply queryLmtGrpReplyByReplySerno(String lmtGrpReplySerno) throws Exception {
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("grpReplySerno", lmtGrpReplySerno);
        lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 生效日期倒排
        log.info("根据【集团批复流水号】"+lmtGrpReplySerno+"查询集团授信批复表记录开始");
        List<LmtGrpReply> lmtGrpReplyList = queryLmtGrpReplyByParams(lmtReplyQueryMap);
        LmtGrpReply lmtGrpReply = null;
        if (lmtGrpReplyList != null && lmtGrpReplyList.size() == 1) {
            lmtGrpReply = lmtGrpReplyList.get(0);
        } else {
            log.info("根据【集团批复流水号】"+lmtGrpReplySerno+"查询集团授信批复表记录为空");
            throw new Exception("授信批复数据获取异常");
        }
        log.info("根据【集团批复流水号】"+lmtGrpReplySerno+"查询集团授信批复表记录结束，记录内容："+lmtGrpReply);
        return lmtGrpReply;
    }

    /**
     * @方法名称：selectOrigiLmtGrpReplyDataByGrpSerno
     * @方法描述：根据授信申请流水号查询批复新
     * @创建人：css
     * @创建时间：2021-10-29 下午 15:07
     * @修改记录：修改时间 修改人员  修改原因
     */

    public LmtGrpReply selectOrigiLmtGrpReplyDataByGrpSerno(String grpSerno) {
        LmtGrpReply lmtGrpReply = new LmtGrpReply();
        try {
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(grpSerno);
            log.info("根据申请流水号[{}]查询授信申请数据[{}]",grpSerno, JSON.toJSONString(lmtGrpApp));
            String origiLmtReplySerno = lmtGrpApp.getOrigiLmtReplySerno();
            lmtGrpReply = this.queryLmtGrpReplyByReplySerno(origiLmtReplySerno);
            log.info("根据申请流水号[{}]查询批复数据[{}]",grpSerno, JSON.toJSONString(lmtGrpReply));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lmtGrpReply;
    }

    /**
     * 根据单一台账编号查集团批复
     * @param singleSerno
     * @return
     */
    public LmtGrpReply getBySingleSerno(String singleSerno) {
        LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(singleSerno);
        return lmtGrpReplyMapper.getReply(lmtGrpMemRel.getGrpSerno());
    }
}
