package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EntlQyInfo
 * @类描述: entl_qy_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:43:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class EntlQyInfoQueryVo extends PageQuery{
	
	/** 流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户所在区域 **/
	private String cusArea;
	
	/** 营业执照号 **/
	private String regCde;
	
	/** 经营范围 **/
	private String operRange;
	
	/** 年销售收入 **/
	private java.math.BigDecimal yearSaleIncome;
	
	/** 企业信息状态 **/
	private String crpStatus;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusArea
	 */
	public void setCusArea(String cusArea) {
		this.cusArea = cusArea == null ? null : cusArea.trim();
	}
	
    /**
     * @return CusArea
     */	
	public String getCusArea() {
		return this.cusArea;
	}
	
	/**
	 * @param regCde
	 */
	public void setRegCde(String regCde) {
		this.regCde = regCde == null ? null : regCde.trim();
	}
	
    /**
     * @return RegCde
     */	
	public String getRegCde() {
		return this.regCde;
	}
	
	/**
	 * @param operRange
	 */
	public void setOperRange(String operRange) {
		this.operRange = operRange == null ? null : operRange.trim();
	}
	
    /**
     * @return OperRange
     */	
	public String getOperRange() {
		return this.operRange;
	}
	
	/**
	 * @param yearSaleIncome
	 */
	public void setYearSaleIncome(java.math.BigDecimal yearSaleIncome) {
		this.yearSaleIncome = yearSaleIncome;
	}
	
    /**
     * @return YearSaleIncome
     */	
	public java.math.BigDecimal getYearSaleIncome() {
		return this.yearSaleIncome;
	}
	
	/**
	 * @param crpStatus
	 */
	public void setCrpStatus(String crpStatus) {
		this.crpStatus = crpStatus == null ? null : crpStatus.trim();
	}
	
    /**
     * @return CrpStatus
     */	
	public String getCrpStatus() {
		return this.crpStatus;
	}


}