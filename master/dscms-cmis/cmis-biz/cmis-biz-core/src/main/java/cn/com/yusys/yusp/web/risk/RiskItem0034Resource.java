package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0034Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0034Resource
 * @类描述: 征信校验
 * @功能描述:
 * @创建人: hubp
 * @创建时间: 2021年7月13日15:49:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0034住房按揭放款申请征信报告校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0034")
public class RiskItem0034Resource {

    @Autowired
    private RiskItem0034Service riskItem0034Service;

    /**
     * @方法名称: riskItem0034
     * @方法描述: 住房按揭放款申请征信报告校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: hubp
     * @创建时间: 2021年7月13日15:49:17
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "住房按揭放款申请征信报告校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0033(@RequestBody QueryModel queryModel) {
        // TODO 判断实现逻辑
        return ResultDto.success(riskItem0034Service.riskItem0034(queryModel));
    }
}
