package cn.com.yusys.yusp.service.server.xdtz0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.dto.server.xdtz0020.req.Xdtz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.resp.Xdtz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.service.AccCvrsService;
import cn.com.yusys.yusp.service.CmisPspClientService;
import cn.com.yusys.yusp.service.CtrCvrgContService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 	 * 场景：国结系统推送保函台账操作交易：01-登记保函台账；02-保函台账冲正；03-保函台账保函金额、余额、台账状态同步
 * 	 * 接口说明：
 * 	 * 01-登记入账：先根据接口入参【借据编号BILL_NO】查询ACC_CVRG是否已存在，如存在做更新操作；
 * 	 * 	不存在ACC_CVRG则根据接口入参【合同编号CONT_NO】查询保函协议CTR_CVRG_CONT获取业务数据，生成有效(ACCOUNT_STATUS='1')保函台账ACC_CVRG；
 * 	 * 	生成台账后，更新五级、十级分类
 * 	 *
 * 	 * 02-保函台账冲正：根据接口入参【借据 编号BILL_NO】删除保函台账ACC_CVRG记录
 * 	 *
 * 	 * 03-保函台账保函金额、余额、台账状态同步：根据接口入参【借据编号BILL_NO】直接更新ACC_CVRG.guarantee_amount、ACC_CVRG.guarantee_balance、ACC_CVRG.account_status等于对应接口入参值
 */
@Service
public class Xdtz0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0020Service.class);
    /**
     *1.操作类型'01'：将传入参数保存至保函台账表中，保存成功后修改五十级分类。
     * 2.操作类型'02'：通过借据号删除此条保函台账信息。
     * 3.操作类型'03'：(1)台账贷款余额+入参借据金额<0 返回错误信息借据金额不能<0,反之，则更新贷款余额为入参借据金额。
     * 4./************修改五十级分类******
     * */
    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0020DataRespDto xdtz0020(Xdtz0020DataReqDto xdtz0020DataReqDto) throws Exception {
        String msg = "交易成功";
        Xdtz0020DataRespDto xdtz0020DataRespDto = new Xdtz0020DataRespDto();
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value);
        try {
            AccCvrs accCvrs = BeanUtils.beanCopy(xdtz0020DataReqDto, AccCvrs.class);
            String operType = xdtz0020DataReqDto.getOprtype();
            AccCvrs accCvrs1 = accCvrsService.selectByBillno(accCvrs.getBillNo());
            if ("01".equals(operType)) {
                if (accCvrs1 != null) {
                    //如果存在则更新台账状态
                    accCvrsService.updateByBillNoForStatus(accCvrs);
                } else {
                    String exchgRate = xdtz0020DataReqDto.getExchgRate();
                    Map param = new HashMap<>();
                    param.put("contNo", xdtz0020DataReqDto.getContNo());
                    CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.getCvrgContByParam(param);
                    //不存在则插入新台账
                    accCvrs.setChrgRate(xdtz0020DataReqDto.getChrgRate());//手续费率
                    accCvrs.setChrgAmt(xdtz0020DataReqDto.getChrgAmt());//手续费金额
                    accCvrs.setPrdId(xdtz0020DataReqDto.getPrdId());//产品编号
                    accCvrs.setPrdName(xdtz0020DataReqDto.getPrdName());//产品名称
                    accCvrs.setExchangeRate(xdtz0020DataReqDto.getCretQuotationExchgRate());//汇率
                    accCvrs.setGuarantMode(xdtz0020DataReqDto.getGuarantTypeName());//保函种类名称
                    accCvrs.setGuarMode(ctrCvrgCont.getGuarMode());//担保方式
                    accCvrs.setCurType(xdtz0020DataReqDto.getGuarantAmtCurType());//保函金额币种
                    accCvrs.setInureDate(xdtz0020DataReqDto.getStartDate());//生效日期
                    accCvrs.setInvlDate(xdtz0020DataReqDto.getEndDate());//失效日期
                    accCvrs.setOverdueRate(xdtz0020DataReqDto.getOverduePadYearRate());//逾期垫款年利率
                    accCvrs.setguarantPayMode(xdtz0020DataReqDto.getGuarantPayType());//保函付款方式
                    accCvrs.setBeneficiarName(xdtz0020DataReqDto.getBeneficiar());//受益人
                    accCvrs.setSettlAccno(xdtz0020DataReqDto.getSettlAcctNo());//受益人账号
                    accCvrs.setSettlAcctName(xdtz0020DataReqDto.getSettlAcctNoName());//受益人开户名称
                    accCvrs.setPvpSerno(xdtz0020DataReqDto.getPvpSerno());//交易编号
                    accCvrs.setPkId(UUID.randomUUID().toString());
                    accCvrs.setContNo(xdtz0020DataReqDto.getContNo());//合同号
                    accCvrs.setBailExchangeRate(xdtz0020DataReqDto.getCretQuotationExchgRate());//保证金汇率
                    accCvrs.setBailPerc(ctrCvrgCont.getBailPerc());//保证金比例
                    accCvrs.setBailCurType(xdtz0020DataReqDto.getGuarantAmtCurType());//保证金币种
                    accCvrs.setExchangeRmbAmt(xdtz0020DataReqDto.getGuarantAmt().multiply(accCvrs.getExchangeRate()).setScale(2, BigDecimal.ROUND_HALF_UP));//折合人民币金额
                    accCvrs.setBailAmt(BigDecimal.ZERO);//保证金金额
                    accCvrs.setBailCvtCnyAmt(BigDecimal.ZERO);//保证金折算人民币金额
                    accCvrs.setManagerId(ctrCvrgCont.getManagerId());//责任人
                    accCvrs.setManagerBrId(ctrCvrgCont.getManagerBrId());//责任机构
                    accCvrs.setInputId(ctrCvrgCont.getManagerId());//登记人
                    accCvrs.setInputBrId(ctrCvrgCont.getManagerBrId());//登记机构
                    accCvrs.setUpdId(ctrCvrgCont.getManagerId());//修改人
                    accCvrs.setUpdBrId(ctrCvrgCont.getManagerBrId());//修改机构
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    accCvrs.setInputDate(openDay);//登记日期
                    accCvrs.setUpdDate(openDay);//修改日期
                    Date date = new Date();
                    accCvrs.setCreateTime(date);//创建时间
                    accCvrs.setUpdateTime(date);//更新时间

                    if (Objects.equals("21", ctrCvrgCont.getGuarMode()) || Objects.equals("60", ctrCvrgCont.getGuarMode())) {
                        //低风险为0
                        accCvrs.setOrigiOpenAmt(BigDecimal.ZERO);//原始敞口金额
                        accCvrs.setSpacBal(BigDecimal.ZERO);//敞口余额
                        accCvrs.setExchangeRmbSpac(BigDecimal.ZERO);//折算人民币敞口
                    } else {
                        BigDecimal spac = xdtz0020DataReqDto.getGuarantBal();//保函金额
                        BigDecimal spacCny = spac.multiply(StringUtils.isBlank(exchgRate) ? new BigDecimal("1") :
                                new BigDecimal(exchgRate)).setScale(2, BigDecimal.ROUND_HALF_UP);//保函金额折算人民币金额
                        BigDecimal bailAmt = new BigDecimal(StringUtils.isBlank(xdtz0020DataReqDto.getBailAmt()) ? "0" : xdtz0020DataReqDto.getBailAmt());//保证金金额(人民币)
                        BigDecimal origiOpenAmt = spac.subtract(bailAmt.divide(StringUtils.isBlank(exchgRate) ? new BigDecimal("1") :
                                new BigDecimal(exchgRate), 2, BigDecimal.ROUND_HALF_UP));//原始敞口金额
                        accCvrs.setOrigiOpenAmt(origiOpenAmt);//原始敞口金额
                        accCvrs.setSpacBal(origiOpenAmt);//敞口余额
                        accCvrs.setExchangeRmbSpac(spacCny.subtract(bailAmt));//折算人民币敞口
                    }
                    accCvrs.setTenClass("11");//十级分类
                    accCvrs.setFiveClass("10");//五级分类
                    accCvrs.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//分类时间
                    accCvrs.setOprType(CmisCommonConstants.OP_TYPE_01);//操作类型
                    accCvrs.setAccStatus("1");//台账状态
                    accCvrsService.insert(accCvrs);
                }
            } else if ("02".equals(operType)) {
                //通过借据号删除对应台账
                accCvrsService.deleteByBillNo(accCvrs.getBillNo());
            } else if ("03".equals(operType)) {
                //这里取的是保函金额,是否有问题
                if (Objects.isNull(accCvrs1)) {
                    xdtz0020DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0020DataRespDto.setOpMsg("台账不存在，请先登记入账！");
                    return xdtz0020DataRespDto;
                } else {
                    BigDecimal sumAmt = accCvrs.getGuarantAmt().add(Optional.ofNullable(accCvrs1.getGuarantAmt()).orElse(BigDecimal.ZERO));
                    if (sumAmt.compareTo(new BigDecimal("0")) < 0) {
                        xdtz0020DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0020DataRespDto.setOpMsg("恢复额度后，借据金额不能小于0！");
                        return xdtz0020DataRespDto;
                    }else {
                        accCvrs.setAccStatus("0");//闭卷
                        accCvrsService.updateByBillNoForamt(accCvrs);
                    }
                }

            }
            xdtz0020DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
            xdtz0020DataRespDto.setOpMsg(msg);
        }catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        return xdtz0020DataRespDto;
    }

}
