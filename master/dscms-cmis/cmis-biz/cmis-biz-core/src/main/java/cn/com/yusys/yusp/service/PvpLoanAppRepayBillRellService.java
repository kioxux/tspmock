/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import cn.com.yusys.yusp.dto.RepayBillRellDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRellMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRellService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-15 14:34:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpLoanAppRepayBillRellService {
    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppRepayBillRellService.class);

    @Autowired
    private PvpLoanAppRepayBillRellMapper pvpLoanAppRepayBillRellMapper;

    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private AccLoanService accLoanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpLoanAppRepayBillRell selectByPrimaryKey(String pkId, String serno) {
        return pvpLoanAppRepayBillRellMapper.selectByPrimaryKey(pkId, serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpLoanAppRepayBillRell> selectAll(QueryModel model) {
        List<PvpLoanAppRepayBillRell> records = (List<PvpLoanAppRepayBillRell>) pvpLoanAppRepayBillRellMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpLoanAppRepayBillRell> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpLoanAppRepayBillRell> list = pvpLoanAppRepayBillRellMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpLoanAppRepayBillRell record) {
        return pvpLoanAppRepayBillRellMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PvpLoanAppRepayBillRell record) {
        return pvpLoanAppRepayBillRellMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpLoanAppRepayBillRell record) {
        return pvpLoanAppRepayBillRellMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PvpLoanAppRepayBillRell record) {
        return pvpLoanAppRepayBillRellMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return pvpLoanAppRepayBillRellMapper.deleteByPrimaryKey(pkId, serno);
    }

    /**
     * @方法名称: deleteByBillNo
     * @方法描述: 根据借据号删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int deleteByBillNo(String billNo) {
        return pvpLoanAppRepayBillRellMapper.deleteByBillNo(billNo);
    }

    /**
     * @方法名称: queryRepayBillRel
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<RepayBillRellDto> queryRepayBillRell(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RepayBillRellDto> ctrList = pvpLoanAppRepayBillRellMapper.selectRepayBillRellByParams(model);
        PageHelper.clearPage();
        return ctrList;
    }

    /**
     * @方法名称: queryRepayBillRel
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<RepayBillRellDto> queryRepayBillRellForXw(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RepayBillRellDto> ctrList = pvpLoanAppRepayBillRellMapper.selectRepayBillRellByParams(model);
        PageHelper.clearPage();
        return ctrList;
    }

    public List<RepayBillRellDto> queryAllRepayBillRell(QueryModel model) {
        List<RepayBillRellDto> ctrList = pvpLoanAppRepayBillRellMapper.selectRepayBillRellByParams(model);
        return ctrList;
    }

    /**
     *偿还借据,引入台账
     * @param queryMap
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map leadInAccLoan(HashMap queryMap) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = new PvpLoanAppRepayBillRell();
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpLoanAppRepayBillRell.setInputId(userInfo.getLoginCode());
                pvpLoanAppRepayBillRell.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanAppRepayBillRell.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanAppRepayBillRell.setUpdId(userInfo.getLoginCode());
                pvpLoanAppRepayBillRell.setUpdBrId(userInfo.getOrg().getCode());
                pvpLoanAppRepayBillRell.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanAppRepayBillRell.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                pvpLoanAppRepayBillRell.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            String pkId = StringUtils.uuid(true);
            pvpLoanAppRepayBillRell.setPkId(pkId);
            pvpLoanAppRepayBillRell.setBillNo((String) queryMap.get("billNo"));//将贷款台账表acc_loan中的“借据编号”赋值给偿还借据关联表中的“借据编号”
            pvpLoanAppRepayBillRell.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            pvpLoanAppRepayBillRell.setSerno((String)queryMap.get("pvpSerno"));//将贷款台账表acc_loan中的“放款流水号”赋值给偿还借据关联表中的“流水号”，queryMap.get("pvpSerno")是Object类型
            //pvpLoanAppRepayBillRel.setSerno();//流水号
            int insertCount = pvpLoanAppRepayBillRellMapper.insertSelective(pvpLoanAppRepayBillRell);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo((String)queryMap.get("billNo"));
            if(Objects.isNull(accLoan)){
                log.error("根据借据编号【{}】未找到对应的借据信息！",(String)queryMap.get("billNo"));
                throw BizException.error(null,EcbEnum.ECB010017.key,EcbEnum.ECB010017.value);
            }
            // 给新出账申请打上借新还旧标志
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey((String)queryMap.get("pvpSerno"));
            if(Objects.isNull(accLoan)){
                log.error("根据出账流水号【{}】未找到对应的出账申请信息！",(String)queryMap.get("pvpSerno"));
                throw BizException.error(null,EcbEnum.ECB010017.key,EcbEnum.ECB010017.value);
            }
            pvpLoanApp.setRefinancingFlag(accLoan.getRefinancingFlag());
            int updateCount = pvpLoanAppService.updateSelective(pvpLoanApp);
            if(updateCount<=0){
                log.error("修改出账申请【{}】失败！",(String)queryMap.get("pvpSerno"));
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            result.put("pkId",pkId);
            log.info("偿还借据引入台账"+pkId+"-保存成功！");
        }catch(YuspException e){
            log.error("偿还借据引入台账异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("偿还借据引入台账异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账申请流水号查询关联的原借据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpLoanAppRepayBillRell selectByPvpSerno(String pvpSerno) {
        return pvpLoanAppRepayBillRellMapper.selectByPvpSerno(pvpSerno);
    }

    /**
     * @方法名称: deleteRepaybillrellAndCalculateResult
     * @方法描述: 根据借据号删除偿还借据信息和关联的测算信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteRepaybillrellAndCalculateResult(Map map) {
        int deleteCount = 0;
        PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = this.selectByPvpSerno((String) map.get("serno"));
        if(Objects.isNull(pvpLoanAppRepayBillRell)){
            log.error("根据流水号【{}】查询偿还借据信息异常！",map.get("serno"));
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        deleteCount = this.deleteByBillNo((String) map.get("billNo"));
        if(deleteCount<=0){
            log.error("根据借据编号【{}】删除偿还借据信息异常！",map.get("billNo"));
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        HashMap queryMap = new HashMap();
        queryMap.put("serno",(String) map.get("serno"));
        PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanService.queryPvpJxhjRepayLoanDataByParams(queryMap);
        deleteCount = pvpJxhjRepayLoanService.deleteByPrimaryKey(pvpJxhjRepayLoan.getPkId());
        if(deleteCount<=0){
            log.error("删除测算信息【{}】异常！",pvpJxhjRepayLoan.getSerno());
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        return deleteCount;
    }
}
