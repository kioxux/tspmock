/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import cn.com.yusys.yusp.repository.mapper.RetailPrimeRateApprMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:20:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RetailPrimeRateApprService {

    @Autowired
    private RetailPrimeRateApprMapper retailPrimeRateApprMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RetailPrimeRateAppr selectByPrimaryKey(String pkId, String serno) {
        return retailPrimeRateApprMapper.selectByPrimaryKey(pkId, serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RetailPrimeRateAppr> selectAll(QueryModel model) {
        List<RetailPrimeRateAppr> records = (List<RetailPrimeRateAppr>) retailPrimeRateApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RetailPrimeRateAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RetailPrimeRateAppr> list = retailPrimeRateApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RetailPrimeRateAppr record) {
        return retailPrimeRateApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RetailPrimeRateAppr record) {
        return retailPrimeRateApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RetailPrimeRateAppr record) {
        return retailPrimeRateApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RetailPrimeRateAppr record) {
        return retailPrimeRateApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return retailPrimeRateApprMapper.deleteByPrimaryKey(pkId, serno);
    }

    /**
     * @param serno
     * @return cn.com.yusys.yusp.domain.RetailPrimeRateAppr
     * @author 王玉坤
     * @date 2021/9/10 14:23
     * @version 1.0.0
     * @desc 根据流水号查询最新一笔审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RetailPrimeRateAppr selectBySernoOrderByDate(String serno) {
        return retailPrimeRateApprMapper.selectBySernoOrderByDate(serno);
    }
    /**
     * @param retailPrimeRateAppr
     * @return
     * @author wzy
     * @date 2021/9/11 17:50
     * @version 1.0.0
     * @desc  审批信息修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
	 public RetailPrimeRateAppr selectBySernoNew(RetailPrimeRateAppr retailPrimeRateAppr) {
        return retailPrimeRateApprMapper.selectBySernoNew(retailPrimeRateAppr);
    }
    /**
     * @param retailPrimeRateAppr
     * @return
     * @author wzy
     * @date 2021/9/11 17:50
     * @version 1.0.0
     * @desc  审批信息修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RetailPrimeRateAppr saveInfo(RetailPrimeRateAppr retailPrimeRateAppr) {
        String serno = retailPrimeRateAppr.getSerno();
        String approvePost = retailPrimeRateAppr.getApprovePost();
        Map param = new HashMap();
        param.put("serno",serno);
        param.put("approvePost",approvePost);
        RetailPrimeRateAppr record = retailPrimeRateApprMapper.selectBySernoAndNode(param);
        if(record == null){
            retailPrimeRateAppr.setPkId(UUID.randomUUID().toString());
            int result = retailPrimeRateApprMapper.insertSelective(retailPrimeRateAppr);
            if(result != 1){
                throw new YuspException("999999","新增审批数据异常！");
            }
        }else{
            retailPrimeRateAppr.setPkId(record.getPkId());
            int result =  retailPrimeRateApprMapper.updateByPrimaryKeySelective(retailPrimeRateAppr);
            if(result != 1){
                throw new YuspException("999999","新增审批数据异常！");
            }
        }
        return retailPrimeRateAppr;
    }

    public RetailPrimeRateAppr selectbySernoAndNode(RetailPrimeRateAppr retailPrimeRateAppr) {
        Map param = new HashMap();
        param.put("serno",retailPrimeRateAppr.getSerno());
        param.put("approvePost",retailPrimeRateAppr.getApprovePost());
        return   retailPrimeRateApprMapper.selectBySernoAndNode(param);
    }
}
