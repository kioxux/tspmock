package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.TmpWydHkPlan;
import cn.com.yusys.yusp.service.WydRepayPlanService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: WydBatRecordResource
 * @类描述: #微业贷还款计划
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:50:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wydrepayplan")
public class WydRepayPlanResource {
    @Resource
    private WydRepayPlanService wydRepayPlanService;

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：zl
     */
    @ApiOperation(value = "微业贷批处理记录分页查询")
    @PostMapping("/selectByModel")
    protected ResultDto<List<TmpWydHkPlan>> selectByModel(@RequestBody QueryModel queryModel) {
        List<TmpWydHkPlan> list = wydRepayPlanService.selectByModel(queryModel);
        return new ResultDto<List<TmpWydHkPlan>>(list);
    }

}
