/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpGuarChgAppRel;
import cn.com.yusys.yusp.repository.mapper.IqpGuarChgAppRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarChgAppRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-21 14:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpGuarChgAppRelService {

    @Autowired
    private IqpGuarChgAppRelMapper iqpGuarChgAppRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpGuarChgAppRel selectByPrimaryKey(String igcarSerno) {
        return iqpGuarChgAppRelMapper.selectByPrimaryKey(igcarSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpGuarChgAppRel> selectAll(QueryModel model) {
        List<IqpGuarChgAppRel> records = (List<IqpGuarChgAppRel>) iqpGuarChgAppRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpGuarChgAppRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpGuarChgAppRel> list = iqpGuarChgAppRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param model
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author tangxun
     * @date 2021/5/18 10:54
     * @version 1.0.0
     * @desc 查询担保合同部分信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String,Object>> selectGuarContByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String,Object>> list = iqpGuarChgAppRelMapper.selectGuarContByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpGuarChgAppRel record) {
        return iqpGuarChgAppRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpGuarChgAppRel record) {
        return iqpGuarChgAppRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpGuarChgAppRel record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpGuarChgAppRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpGuarChgAppRel record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpGuarChgAppRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String igcarSerno) {
        return iqpGuarChgAppRelMapper.deleteByPrimaryKey(igcarSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpGuarChgAppRelMapper.deleteByIds(ids);
    }
}
