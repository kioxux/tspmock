package cn.com.yusys.yusp.service.server.xdzc0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.OpRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.req.Xdzc0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.resp.Xdzc0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 接口处理类:资产池出票校验
 *
 * @Author xs
 * @Date 2021/08/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0008Service {

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0008.Xdzc0008Service.class);

    /**
     * 交易码：xdzc0008
     * 交易描述：
     * 开立银行承兑汇票
     * 资产池协议状态为有效；
     * 100 未生效  200 生效   500 中止 600 注销  700 撤回  800作废
     * 本次开票金额*（1 - 保证金留存比例）<= 资产池可用融资额度；
     * 到期日<=资产池协议到期日 + 授信批复宽限期。
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0008DataRespDto xdzc0008Service(Xdzc0008DataReqDto xdzc0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value);
        Xdzc0008DataRespDto xdzc0008DataRespDto = new Xdzc0008DataRespDto();
        xdzc0008DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0008DataRespDto.setOpMsg("出票校验逻辑报错");// 描述信息
        String contNo = xdzc0008DataReqDto.getContNo();
        BigDecimal billAmt = xdzc0008DataReqDto.getBillAmt();
        try {
            OpRespDto opRespDto = ctrAsplDetailsService.isPvpAccpApp(contNo,billAmt);
            xdzc0008DataRespDto.setOpFlag(opRespDto.getOpFlag());// 成功失败标志
            xdzc0008DataRespDto.setOpMsg(opRespDto.getOpMsg());// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value);
        return xdzc0008DataRespDto;
    }
}