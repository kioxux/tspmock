/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoRelatedGrpCustomers;
import cn.com.yusys.yusp.service.RptBasicInfoRelatedGrpCustomersService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRelatedGrpCustomersResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:30:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinforelatedgrpcustomers")
public class RptBasicInfoRelatedGrpCustomersResource {
    @Autowired
    private RptBasicInfoRelatedGrpCustomersService rptBasicInfoRelatedGrpCustomersService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoRelatedGrpCustomers>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoRelatedGrpCustomers> list = rptBasicInfoRelatedGrpCustomersService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoRelatedGrpCustomers>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoRelatedGrpCustomers>> index(QueryModel queryModel) {
        List<RptBasicInfoRelatedGrpCustomers> list = rptBasicInfoRelatedGrpCustomersService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoRelatedGrpCustomers>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptBasicInfoRelatedGrpCustomers> show(@PathVariable("serno") String serno) {
        RptBasicInfoRelatedGrpCustomers rptBasicInfoRelatedGrpCustomers = rptBasicInfoRelatedGrpCustomersService.selectByPrimaryKey(serno);
        return new ResultDto<RptBasicInfoRelatedGrpCustomers>(rptBasicInfoRelatedGrpCustomers);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoRelatedGrpCustomers> create(@RequestBody RptBasicInfoRelatedGrpCustomers rptBasicInfoRelatedGrpCustomers) throws URISyntaxException {
        rptBasicInfoRelatedGrpCustomersService.insert(rptBasicInfoRelatedGrpCustomers);
        return new ResultDto<RptBasicInfoRelatedGrpCustomers>(rptBasicInfoRelatedGrpCustomers);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoRelatedGrpCustomers rptBasicInfoRelatedGrpCustomers) throws URISyntaxException {
        int result = rptBasicInfoRelatedGrpCustomersService.update(rptBasicInfoRelatedGrpCustomers);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptBasicInfoRelatedGrpCustomersService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoRelatedGrpCustomersService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptBasicInfoRelatedGrpCustomers> selectBySerno(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        return new ResultDto<RptBasicInfoRelatedGrpCustomers>(rptBasicInfoRelatedGrpCustomersService.selectByPrimaryKey(serno));
    }

    @PostMapping("/updateGrpCustomers")
    protected ResultDto<Integer> updateGrpCustomers(@RequestBody RptBasicInfoRelatedGrpCustomers rptBasicInfoRelatedGrpCustomers) {
        return new ResultDto<Integer>(rptBasicInfoRelatedGrpCustomersService.updateSelective(rptBasicInfoRelatedGrpCustomers));
    }

    /**
     * 引入集团核心客户数据
     */
    @PostMapping("/initGrpAsso")
    protected ResultDto<Integer> initGrpAsso(@RequestBody Map params){
        return new ResultDto<Integer>(rptBasicInfoRelatedGrpCustomersService.initGrpAsso(params));
    }
}
