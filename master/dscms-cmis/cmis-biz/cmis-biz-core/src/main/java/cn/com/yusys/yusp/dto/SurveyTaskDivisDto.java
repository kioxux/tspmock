package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyTaskDivis
 * @类描述: survey_task_divis数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-10 18:41:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class SurveyTaskDivisDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	private String serno;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 客户编号 **/
	private String cusNo;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certNo;
	
	/** 所属条线 **/
	private String belgLine;
	
	/** 手机号码 **/
	private String phone;
	
	/** 工作 **/
	private String work;
	
	/** 申请渠道 **/
	private String appChnl;
	
	/** 贷款类别 **/
	private String loanCls;
	
	/** 贷款用途 **/
	private String loanUse;
	
	/** 客户经理名称 **/
	private String managerName;
	
	/** 客户经理编号 **/
	private String managerId;
	
	/** 客户经理片区 **/
	private String managerArea;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;
	
	/** 进件时间 **/
	private String intoTime;
	
	/** 业务状态 **/
	private String busiStatus;
	
	/** 确认状态 **/
	private String cfirmStatus;
	
	/** 分配时间 **/
	private String divisTime;
	
	/** 处理人 **/
	private String prcId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusNo
	 */
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo == null ? null : cusNo.trim();
	}
	
    /**
     * @return CusNo
     */	
	public String getCusNo() {
		return this.cusNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo == null ? null : certNo.trim();
	}
	
    /**
     * @return CertNo
     */	
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}
	
    /**
     * @return BelgLine
     */	
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param work
	 */
	public void setWork(String work) {
		this.work = work == null ? null : work.trim();
	}
	
    /**
     * @return Work
     */	
	public String getWork() {
		return this.work;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl == null ? null : appChnl.trim();
	}
	
    /**
     * @return AppChnl
     */	
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param loanCls
	 */
	public void setLoanCls(String loanCls) {
		this.loanCls = loanCls == null ? null : loanCls.trim();
	}
	
    /**
     * @return LoanCls
     */	
	public String getLoanCls() {
		return this.loanCls;
	}
	
	/**
	 * @param loanUse
	 */
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse == null ? null : loanUse.trim();
	}
	
    /**
     * @return LoanUse
     */	
	public String getLoanUse() {
		return this.loanUse;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerArea
	 */
	public void setManagerArea(String managerArea) {
		this.managerArea = managerArea == null ? null : managerArea.trim();
	}
	
    /**
     * @return ManagerArea
     */	
	public String getManagerArea() {
		return this.managerArea;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime == null ? null : intoTime.trim();
	}
	
    /**
     * @return IntoTime
     */	
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param busiStatus
	 */
	public void setBusiStatus(String busiStatus) {
		this.busiStatus = busiStatus == null ? null : busiStatus.trim();
	}
	
    /**
     * @return BusiStatus
     */	
	public String getBusiStatus() {
		return this.busiStatus;
	}
	
	/**
	 * @param cfirmStatus
	 */
	public void setCfirmStatus(String cfirmStatus) {
		this.cfirmStatus = cfirmStatus == null ? null : cfirmStatus.trim();
	}
	
    /**
     * @return CfirmStatus
     */	
	public String getCfirmStatus() {
		return this.cfirmStatus;
	}
	
	/**
	 * @param divisTime
	 */
	public void setDivisTime(String divisTime) {
		this.divisTime = divisTime == null ? null : divisTime.trim();
	}
	
    /**
     * @return DivisTime
     */	
	public String getDivisTime() {
		return this.divisTime;
	}
	
	/**
	 * @param prcId
	 */
	public void setPrcId(String prcId) {
		this.prcId = prcId == null ? null : prcId.trim();
	}
	
    /**
     * @return PrcId
     */	
	public String getPrcId() {
		return this.prcId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}