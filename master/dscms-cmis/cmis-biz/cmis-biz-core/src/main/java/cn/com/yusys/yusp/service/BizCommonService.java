/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CmisLmt0053ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CusListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.resp.CmisLmt0053RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.req.CmisLmt0064ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.resp.CmisLmt0064RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BizCommonService
 * @类描述: #服务类
 * @功能描述: BIZ公共服务
 * @创建人: zhangjw
 * @创建时间: 2021-09-12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizCommonService {
    private static final Logger logger = LoggerFactory.getLogger(BizCommonService.class);
    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtSubPrdLowGuarRelService lmtSubPrdLowGuarRelService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private LmtIntbankReplyService lmtIntbankReplyService ;

    @Autowired
    private LmtIntbankAccService lmtIntbankAccService;

    @Autowired
    private LmtSigInvestAccService lmtSigInvestAccService;

    @Autowired
    private LmtSigInvestRstService  lmtSigInvestRstService;

    @Autowired
    private SendMessageForDgService sendMessageForDgService;

    /**
     * 校验客户是否关联方 判断
     * @param cusId
     * @return  1-是   0-否
     */
    public String checkCusIsGlf(String cusId){
        String isGLAppr = CmisCommonConstants.STD_ZB_YES_NO_0;

        CusLstGlfDto cusLstGlfDto = iCusClientService.queryCusLstGlfByCusId(cusId).getData();
        logger.info("根据客户号【{}】查询客户是否在关联客户名单，返回为{}", cusId, cusLstGlfDto);

        if (cusLstGlfDto != null && !StringUtils.isBlank(cusLstGlfDto.getCusId())) {
            isGLAppr = CmisCommonConstants.STD_ZB_YES_NO_1;
        }
        return isGLAppr;
    }

    /**
     * 计算关联交易类型
     * @param cusId
     * @return  1-是   0-否
     */
    public Map<String,Object> calGljyType(String cusId,BigDecimal openTotalLmtAmt,BigDecimal lowRiskTotalLmtAmt,BigDecimal lowRiskNoRelAmt,String instuCde){
        Map<String,Object> resultMap = new HashMap<>();
        BigDecimal origiOpenTotalLmtAmt = openTotalLmtAmt;//保留传入的敞口金额 BUG15794修改

        // 获取本行资本净额
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.BANK_CAPITAL_NET);
        String bankCapitalNet = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        BigDecimal bankCapitalNetAmt = new BigDecimal(bankCapitalNet);
        logger.info("计算关联交易类型：----->本行资本净额 bankCapitalNetAmt {}", bankCapitalNetAmt);
        // 获取本行资本净值
        AdminSmPropQueryDto adminSmPropQueryDto2 = new AdminSmPropQueryDto();
        adminSmPropQueryDto2.setPropName(CmisCommonConstants.BANK_CAPITAL_VALUE);
        String bankCapitalValue = adminSmPropService.getPropValue(adminSmPropQueryDto2).getData().getPropValue();
        BigDecimal bankCapitalValueAmt = new BigDecimal(bankCapitalValue);
        logger.info("计算关联交易类型：----->本行资本净值 bankCapitalValueAmt {}", bankCapitalValueAmt);
        // 是否对公客户
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);

        // 本行资本净额于与行资本净值较低值
        BigDecimal minBankCapitalAmt = bankCapitalNetAmt.compareTo(bankCapitalValueAmt) > 0 ? bankCapitalNetAmt : bankCapitalValueAmt;
        logger.info("计算关联交易类型：----->本行资本净额于与行资本净值较低值 minBankCapitalAmt {}", minBankCapitalAmt);
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog()) || CmisCusConstants.STD_ZB_CUS_CATALOG_3.equals(cusBaseClientDto.getCusCatalog())){ // 单一对公客户 || 同业客户

            /**
             * 客户申报对公综合授信时，授信敞口=本笔对公授信申报敞口+资金业务版块授信敞口；
             * 客户申报资金业务授信时，授信敞口=本笔资金授信申报金额+存量资金授信金额（剔除本笔）+对公授信版块授信敞口
             * 对公综合授信版块取授信敞口:1.在途授信、生效或冻结授信，取授信敞口 2.授信到期未结清，取用信余额 3.扣除低风险质押：国债、存单、全额保证金  4.扣除委托贷款
             * 资金业务授信版块取授信金额:1.在途授信、生效或冻结授信，取授信金额 2.授信到期未结清，取用信余额 3.资金业务授信中扣除承销类产品
             */
            BigDecimal lmtBalanceAmt = this.queryLmtBalanceGljy(cusId,instuCde);

            openTotalLmtAmt = openTotalLmtAmt.add(lmtBalanceAmt);
            logger.info("客户【"+cusId+"】 关联交易判断 授信金额 + 客户除承销/委贷外授信余额 --------> openTotalLmtAmt 【"+ openTotalLmtAmt +"】");

            //累加客户 除本笔外在途授信金额
            BigDecimal ztLmtAmt = this.queryZtLmtAmtGljy(cusId,instuCde,cusBaseClientDto.getCusCatalog());
            openTotalLmtAmt = openTotalLmtAmt.add(ztLmtAmt).subtract(origiOpenTotalLmtAmt);//剔除本笔，所以减了一下
            logger.info("客户【"+cusId+"】 关联交易判断 授信金额 + 在途授信金额 --------> openTotalLmtAmt 【"+ openTotalLmtAmt +"】");

            // 关联交易名单中关联方预计额度
            BigDecimal relatedPartyForeLmt = BigDecimal.ZERO;
            // 是否超过关联方预计额度
            String isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_0;
            // 申报金额比例
            BigDecimal newLmtRelRate = BigDecimal.ZERO;
            // 存量敞口金额比例
            BigDecimal allLmtRelRate = BigDecimal.ZERO;
            // 是否超过关联方预计额度
            CusLstGlfGljyyjedDto cusLstGlfGljyyjedDto = iCusClientService.queryCusLstGlfGljyyjedDtoByCusId(cusId).getData();
            logger.info("计算关联交易类型：----->根据客户号【{}】查询客户关联客户额度，返回为{}", cusId, cusLstGlfGljyyjedDto);
            if (cusLstGlfGljyyjedDto != null && !StringUtils.isBlank(cusLstGlfGljyyjedDto.getRelatedPartyForeLmt())) {
                relatedPartyForeLmt = new BigDecimal(cusLstGlfGljyyjedDto.getRelatedPartyForeLmt());
            }
            // 申报金额比较金额 【授信金额】 - 【除去 全额保证金、国债质押、存单质押的金额】 - 【关联交易名单中关联方预计额度】
            BigDecimal newCompareAmt = openTotalLmtAmt.add(lowRiskTotalLmtAmt).subtract(lowRiskNoRelAmt).subtract(relatedPartyForeLmt);
            logger.info("计算关联交易类型：----->是否超过关联方预计额度 openTotalLmtAmt {}  ，lowRiskTotalLmtAmt {} ，lowRiskNoRelAmt {} ，relatedPartyForeLmt  {}  ", openTotalLmtAmt,lowRiskTotalLmtAmt,lowRiskNoRelAmt,relatedPartyForeLmt);
            newCompareAmt = newCompareAmt.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : newCompareAmt;
            logger.info("计算关联交易类型：----->申报金额比较金额 【授信金额】 - 【除去 全额保证金、国债质押、存单质押的金额】 - 【关联交易名单中关联方预计额度】 newCompareAmt {}", newCompareAmt);

            BigDecimal openBalance = BigDecimal.ZERO;
            CmisLmt0052ReqDto cmisLmt0052ReqDto = new CmisLmt0052ReqDto();
            cmisLmt0052ReqDto.setCusId(cusId);
            cmisLmt0052ReqDto.setDealBizNo(UUID.randomUUID().toString());
            CmisLmt0052RespDto cmisLmt0052RespDto = cmisLmtClientService.cmislmt0052(cmisLmt0052ReqDto).getData();
            logger.info("计算关联交易类型：----->根据客户号【{}】查询单一客户存量敞口余额，返回为{}", cusId, cmisLmt0052RespDto);
            if (cmisLmt0052RespDto != null && cmisLmt0052RespDto.getSpcaBalanceAmt() != null) {
                openBalance = cmisLmt0052RespDto.getSpcaBalanceAmt();
            }

            // 单笔拟申报金额比例
            newLmtRelRate = newCompareAmt.divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);
            logger.info("计算关联交易类型：----->单笔拟申报金额比例 newLmtRelRate {}", newLmtRelRate);
            resultMap.put("newLmtRelRate", newLmtRelRate);
            // 拟申报金额 + 存量敞口 金额比例
            allLmtRelRate = newCompareAmt.add(openBalance).divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);
            /*logger.info("计算关联交易类型：----->拟申报金额 + 存量敞口 占资本净额金额比例 allLmtRelRate {}", allLmtRelRate);*/
            resultMap.put("allLmtRelRate", allLmtRelRate);
            // 是否超过关联方预计额度
            if(newCompareAmt.compareTo(BigDecimal.ZERO) > 0){
                isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_1;
                logger.info("计算关联交易类型：----->是否超过关联方预计额度 isSupGLFLmtAmt {}", isSupGLFLmtAmt);
            }
            resultMap.put("isSupGLFLmtAmt", isSupGLFLmtAmt);
        } else {
            // 申报金额比例
            BigDecimal newLmtRelRate = BigDecimal.ZERO;
            // 存量敞口金额比例
            BigDecimal allLmtRelRate = BigDecimal.ZERO;
            // 是否超过关联方预计额度
            String isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_0;
            // 个人客户
            List<CusLstGlfDto> csLstGlfDtoList =  cmisCusClientService.queryCusLstGlfForIndiv();
            logger.info("计算关联交易类型：----->获取关联方所有自然人名单 cusIndivDtoList {}", csLstGlfDtoList);

            BigDecimal natureOpenTotalAmt = BigDecimal.ZERO;
            BigDecimal natureOpenTotalBalance = BigDecimal.ZERO;

            List<CusListDto> glfIndivCusIdList = new ArrayList<>();

            if (csLstGlfDtoList!=null && csLstGlfDtoList.size()>0) {
                for(CusLstGlfDto cusLstGlfDto: csLstGlfDtoList){
                    String glfCusId = cusLstGlfDto.getCusId();
                    CusListDto cusListDto = new CusListDto();
                    cusListDto.setCusId(glfCusId);
                    glfIndivCusIdList.add(cusListDto);
                }

                //累加个人关联客户 除本笔外在途授信金额
                BigDecimal ztLmtAmt = this.queryZtLmtAmtGljyForPer(glfIndivCusIdList, instuCde);
                openTotalLmtAmt = openTotalLmtAmt.add(ztLmtAmt).subtract(origiOpenTotalLmtAmt);//剔除本笔，所以减了一下
                logger.info("计算关联交易类型：----->客户【"+cusId+"】 关联交易判断 授信金额 + 在途授信金额 --------> openTotalLmtAmt 【"+ openTotalLmtAmt +"】");

                //累加个人关联客户 ：
                //1、如果授信为循环类且为生效或冻结状态则取授信敞口
                //2、如果授信为循环类且为到期未结清状态则取用信余额
                //3、如果授信为非循环类则取用信余额

                CmisLmt0064ReqDto cmisLmt0064ReqDto = new CmisLmt0064ReqDto();
                cmisLmt0064ReqDto.setCusId(cusId);
                cmisLmt0064ReqDto.setInstuCde(instuCde);
                logger.error("计算关联交易类型：----->获取关联交易计算个人客户余额，请求信息: cmisLmt0064ReqDto -------------------------->" + cmisLmt0064ReqDto.toString() );
                ResultDto<CmisLmt0064RespDto> cmisLmt0064RespDto = cmisLmtClientService.cmislmt0064(cmisLmt0064ReqDto);
                logger.error("计算关联交易类型：----->获取关联交易计算个人客户余额，返回信息: cmisLmt0064RespDto -------------------------->" + cmisLmt0064RespDto.toString() );
                String cmisLmt0064Code = cmisLmt0064RespDto.getData().getErrorCode();
                if (!"0000".equals(cmisLmt0064Code)) {
                    logger.error("获取获取关联交易计算个人客户余额异常！");
                    throw new YuspException(EcbEnum.ECB019999.key, "获取获取关联交易计算个人客户余额异常！");
                }
                BigDecimal lmtAmt = cmisLmt0064RespDto.getData().getLmtAmt() == null ? BigDecimal.ZERO : cmisLmt0064RespDto.getData().getLmtAmt() ;


                openTotalLmtAmt = openTotalLmtAmt.add(lmtAmt);//累加客户存量额度
                logger.info("计算关联交易类型：----->客户【"+cusId+"】 关联交易判断 授信金额 + 存量授信余额 --------> openTotalLmtAmt 【"+ openTotalLmtAmt +"】");

                CmisLmt0053ReqDto cmisLmt0053ReqDto = new CmisLmt0053ReqDto();
                cmisLmt0053ReqDto.setDealBizNo(UUID.randomUUID().toString());
                cmisLmt0053ReqDto.setInstuCde(instuCde);
                cmisLmt0053ReqDto.setCusListDtoList(glfIndivCusIdList);
                logger.error("获取所有自然人关联方授信敞口额度，请求信息: cmisLmt0053ReqDto -------------------------->" + cmisLmt0053ReqDto.toString() );
                ResultDto<CmisLmt0053RespDto> resultDto = cmisLmtClientService.cmislmt0053(cmisLmt0053ReqDto);
                logger.error("获取所有自然人关联方授信敞口额度，返回信息: resultDto -------------------------->" + resultDto.toString() );
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    logger.error("获取所有自然人关联方授信敞口额度异常！");
                    throw new YuspException(EcbEnum.ECB019999.key, "获取所有自然人关联方授信敞口额度异常！");
                }
                CmisLmt0053RespDto cmisLmt0053RespDto = resultDto.getData();
                natureOpenTotalAmt = cmisLmt0053RespDto.getSpacLmtAmt() == null || "".equals(cmisLmt0053RespDto.getSpacLmtAmt()) ? BigDecimal.ZERO : cmisLmt0053RespDto.getSpacLmtAmt()  ;//敞口金额
                natureOpenTotalBalance = cmisLmt0053RespDto.getSpacBalanceAmt()  == null || "".equals(cmisLmt0053RespDto.getSpacBalanceAmt()) ? BigDecimal.ZERO : cmisLmt0053RespDto.getSpacBalanceAmt() ;//敞口余额
                logger.error("获取所有自然人关联方授信敞口额度: natureOpenTotalAmt ------------------------->" + natureOpenTotalAmt );
                logger.error("获取所有自然人关联方授信敞口额度: natureOpenTotalBalance --------------------->" + natureOpenTotalBalance );
            }

            // 自然人关联交易预计总额度上限
            AdminSmPropQueryDto adminSmPropQueryDto3 = new AdminSmPropQueryDto();
            adminSmPropQueryDto3.setPropName(CmisCommonConstants.NATURE_REL_TOTAL_AMT);
            String natureRelTotal = adminSmPropService.getPropValue(adminSmPropQueryDto3).getData().getPropValue();
            BigDecimal natureRelTotalAmt = new BigDecimal(natureRelTotal);

            // 【申报金额比较金额】  【授信金额】 - 【除去 全额保证金、国债质押、存单质押的金额】 + 【关联方名单中所有的自然人授信敞口之和】 - 【自然人关联交易预计总额度上限】
            BigDecimal newCompareAmt = openTotalLmtAmt.add(lowRiskTotalLmtAmt).subtract(lowRiskNoRelAmt).add(natureOpenTotalAmt).subtract(natureRelTotalAmt);
            newCompareAmt = newCompareAmt.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : newCompareAmt;

            // 单笔拟申报金额比例 【申报金额比较金额】/【本行资本净额于与行资本净值较低值】
            newLmtRelRate = newCompareAmt.divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);
            resultMap.put("newLmtRelRate", newLmtRelRate);
            // 存量敞口金额比例   (【申报金额比较金额】 + 【关联方名单中所有的自然人存量敞口余额之和】) / 【本行资本净额于与行资本净值较低值】
            /*allLmtRelRate = newCompareAmt.add(natureOpenTotalBalance).divide(minBankCapitalAmt, 9, BigDecimal.ROUND_HALF_UP);*/
            resultMap.put("allLmtRelRate", allLmtRelRate);
            // 是否超过关联方预计额度
            if(newCompareAmt.compareTo(BigDecimal.ZERO) > 0){
                isSupGLFLmtAmt = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            resultMap.put("isSupGLFLmtAmt", isSupGLFLmtAmt);
        }

        logger.info("关联交易比例计算结果: resultMap【"+resultMap.toString()+"】");
        return resultMap;
    }


    /**
     * 计算关联交易类型
     * @param map
     * @return   01 一般关联交易   02 重大关联交易   03 特大关联交易
     */
    public String getGLType(Map<String,Object> map ) {
        String gLType = "";

        /**
         * 关联交易类型  判断规则：（资本净额取的是取资本净额、资产净值中较低者）
         * 资产净值0.5%以下，需报关联交易审批委员会备案。如资产净值在0.5%-1%需对外披露
         *
         *    01 一般关联交易：本次单笔拟授信申报金额 占最近一期我行资本净额   1%（含）以下
         *                      或者
         *                    该关联方贷款余额（存量敞口）+拟申报授信金额之和占我行资本净额  5%（含）以下
         *    02 重大关联交易：本次单笔拟授信申报金额（如上）占我行资本净额1%（不含）以上
         *                      或者
         *                    我行与一个关联方发生交易后，与该关联方的交易余额（该关联方贷款余额（如上）+拟申报授信金额之和）占我行资本净额5%（不含）
         *    03 特大关联交易:单笔授信大于基数的5%
         *                     或者
         *                   授信总额大于基数的10%时，此为特大关联交易
         */
        BigDecimal newLmtRelRate = (BigDecimal) map.get("newLmtRelRate"); //本次单笔拟授信申报金额 占最近一期我行资本净额
        BigDecimal allLmtRelRate = (BigDecimal) map.get("allLmtRelRate"); // 拟申报金额 + 存量敞口 金额比例

        logger.info("判断关联交易类型：请求参数map【"+map.toString()+"】");
        //单笔授信大于基数的5%  或者  授信总额大于基数的10%
        if ((newLmtRelRate.compareTo(new BigDecimal("0.05")) > 0 || allLmtRelRate.compareTo(new BigDecimal("0.1")) > 0)) {
            gLType = CmisFlowConstants.STD_GL_TYPE_03;
        }
        //单笔授信大于基数的1%  或者  授信总额大于基数的5%
        else if((newLmtRelRate.compareTo(new BigDecimal("0.01")) > 0 || allLmtRelRate.compareTo(new BigDecimal("0.05")) > 0)){
            gLType = CmisFlowConstants.STD_GL_TYPE_02;
        }
        //单笔授信小于等于基数的1%  或者  授信总额小于等于基数的5%
        else if((newLmtRelRate.compareTo(new BigDecimal("0.01")) <= 0 || allLmtRelRate.compareTo(new BigDecimal("0.05")) <= 0)){
            gLType = CmisFlowConstants.STD_GL_TYPE_01;
        }

        logger.info("判断关联交易类型：判断结果gLType【"+gLType+"】");
        return gLType;
    }

    /**
     * 检查借款合同下的押品是否已全部入库
     * @param contNo
     * @return
     */
    public String checkWarrantIsInstoreByContNo(String contNo,String bizType){
        String result = "success";
        //1.查询借款合同名下的担保合同列表
        String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNo(contNo);

        if (!StringUtils.isEmpty(guarContNos)) {
            String[] guarContNoArr = guarContNos.split(",");

            for (String guarContNo : guarContNoArr) {
                //2.遍历担保合同列表，查询担保合同关联的押品列表
                String guarNos = grtGuarContRelService.selectGuarNosByGuarContNo(guarContNo);

                if (!StringUtils.isEmpty(guarNos)) {
                    String[] guarNoArr = guarNos.split(",");

                    for (String guarNo : guarNoArr) {
                        //3.遍历押品列表，查询担保合同和押品对应的权证是否入库
                        QueryModel model = new QueryModel();
                        model.addCondition("guarContNo", guarContNo);
                        model.addCondition("guarNo", guarNo);

                        int record = guarWarrantInfoService.countWarrantInRecordsByGuarNoAndGuarContNo(model);
                        logger.info("查询到已入库数量为：【{}】" , record);
                        if (record == 0) {
                            if(CmisFlowConstants.FLOW_TYPE_TYPE_XW002.contains(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType)
                                    || CmisFlowConstants.FLOW_TYPE_TYPE_LS008.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType)
                                    || CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_SGE04.equals(bizType)
                                    || CmisFlowConstants.FLOW_TYPE_TYPE_DHE04.equals(bizType)){
                                //小微和零售业务
                                result = "担保合同【" + guarContNo + "】关联的押品【" + guarNo + "】未入库！";
                                return result;
                            }else{
                                //检查押品和授信的关系是否已解除
                                QueryModel queryModel = new QueryModel();
                                queryModel.addCondition("guarNo",guarNo);
                                queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                                List<GuarBizRel> guarBizRels = guarBizRelService.selectAll(queryModel);

                                if (CollectionUtils.isNotEmpty(guarBizRels)){
                                    logger.info("押品【"+guarNo+"】的授信关系未解除！");
                                    //权证未入库
                                    result = "担保合同【" + guarContNo + "】关联的押品【" + guarNo + "】未入库！";
                                    return result;
                                }else{
                                    GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);

                                    if(CmisBizConstants.STD_ZB_GUAR_WAY_21.equals(grtGuarCont.getGuarWay())){
                                        logger.info("押品【"+guarNo+"】关联的担保合同【"+guarContNo+"】的担保方式是21--低风险质押！");
                                        //权证未入库
                                        result = "担保合同【" + guarContNo + "】关联的押品【" + guarNo + "】未入库！";
                                        return result;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 根据管理机构查询对应的账务机构
     * @param managerBrId
     * @return
     */
    public String getFinaBrNoByManagerBrId(String managerBrId){
        String finaBrNo = "";
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("managerBrNo",managerBrId);
        ResultDto<List<CfgSorgFinaDto>> restltList = iCmisCfgClientService.selecSorgFina(queryModel);
        String jsonData = JSONObject.toJSONString(restltList.getData());
        List<CfgSorgFinaDto>  tempList = JSON.parseArray(jsonData, CfgSorgFinaDto.class);

        if(CollectionUtils.isNotEmpty(tempList)){
            if (tempList.size()==1){
                finaBrNo = tempList.get(0).getFinaBrNo();
            }else{
                //如果查出来多笔，将管理机构的末尾数字改成1作为账务机构
                finaBrNo = managerBrId.substring(0, managerBrId.length()-1) +"1";
            }
        }

        return finaBrNo;
    }

    /**
     * 推送影像审批信息
     * @param docId 任务编号
     * @param topOutSystemCode 影像根目录
     * @param approveUserIds 审批人员
     */
    public void sendImage(String docId,String topOutSystemCode,String approveUserIds) {
        logger.info("docId:"+docId+";topOutSystemCode:"+topOutSystemCode+";approveUserIds:"+approveUserIds);
        String[] outCodeArr = topOutSystemCode.split(";");

        for (String outCode : outCodeArr) {
            logger.info("开始 outCode:"+outCode );
            ImageApprDto imageApprDto = new ImageApprDto();
            imageApprDto.setDocId(docId);//任务编号
            imageApprDto.setApproval("同意");//审批意见
            imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
            imageApprDto.setOutcode(outCode);//文件类型根节点
            imageApprDto.setOpercode(approveUserIds);//审批人员
            sendMessageForDgService.sendImage(imageApprDto);
            logger.info("结束 outCode:"+outCode );
        }
    }

    /**
     * 删除对公用信关联的担保合同及担保合同与押品关系表
     * @param serno
     */
    public void logicDeleteGrtGuarCont(String serno){
        List<GrtGuarBizRstRel> grtGuarBizRstRels = grtGuarBizRstRelService.selectByLmtSerno(serno);
        logger.info("流水号【"+serno+"】,grtGuarBizRstRels:"+grtGuarBizRstRels);

        if (CollectionUtils.isNotEmpty(grtGuarBizRstRels)){
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRels) {
                logger.info("流水号【"+serno+"】,逻辑删除grtGuarBizRstRel："+grtGuarBizRstRel);
                grtGuarBizRstRel.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                grtGuarBizRstRelMapper.updateByPrimaryKey(grtGuarBizRstRel);

                GrtGuarCont grtGuarCont = grtGuarContService.selectByPrimaryKey(grtGuarBizRstRel.getGuarPkId());

                if (grtGuarCont!=null && StringUtils.nonEmpty(grtGuarBizRstRel.getGuarPkId()) && !CmisBizConstants.GUAR_CONT_STATE_01.equals(grtGuarCont.getGuarContState())){
                    logger.info("流水号【"+serno+"】,逻辑删除grtGuarCont："+grtGuarCont);
                    grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                    grtGuarContMapper.updateByPrimaryKey(grtGuarCont);
                }
            }
        }
    }

    /**
     * 获取客户授信余额（除承销额度、 委贷额度）
     * @param cusId
     */
    public BigDecimal queryLmtBalanceGljy(String cusId,String instuCde){
        CmisLmt0032ReqDto reqDto = new CmisLmt0032ReqDto();
        reqDto.setCusId(cusId);
        reqDto.setInstuCde(instuCde);
        reqDto.setIsQuryGrp(CmisLmtConstants.YES_NO_N);
        reqDto.setIsQueryCth(CmisLmtConstants.YES_NO_Y);
        reqDto.setIsQueryDfx(CmisLmtConstants.YES_NO_Y);
        reqDto.setIsQueryWt(CmisLmtConstants.YES_NO_N);

        logger.info("根据客户号【{}】获取客户 除承销、委托贷款外的授信余额 cmislmt0032 请求报文--------> start 【{}】", cusId, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0032RespDto> cmisLmt0032RespDto = cmisLmtClientService.cmislmt0032(reqDto);
        logger.info("根据客户号【{}】获取客户 除承销、委托贷款外的授信余额 cmislmt0032 响应结果--------> end 【{}】", cusId, JSON.toJSONString(cmisLmt0032RespDto));
        String code = cmisLmt0032RespDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            logger.info("根据客户号【{}】获取客户及其集团成员的单笔投资业务授信余额失败！", cusId);
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0032RespDto.getMessage());
        }
        BigDecimal lmtBalanceAmt = cmisLmt0032RespDto.getData().getLmtBalanceAmt();
        return lmtBalanceAmt;
    }
    /**
     * 获取客户在途授信金额
     * @param cusId
     */
    public BigDecimal queryZtLmtAmtGljy(String cusId,String instuCde,String cusCatalog){
        BigDecimal ztLmtAmt = BigDecimal.ZERO;

        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){//对公
            //获取对公综合授信 在途授信金额
            LmtApp lmtApp = lmtAppService.selectOnWayByCusid(cusId);
            if (lmtApp != null){
                logger.info("计算关联交易类型：----->在途授信金额  对公综合授信 openTotalLmtAmt {}", lmtApp.getOpenTotalLmtAmt());
                BigDecimal lowRiskNoRelAmt = BigDecimal.ZERO;
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtApp.getSerno());
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    // 除去低风险之外的品种
                    if (CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) { // 低风险分项
                        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            // 如果为低风险担保类型细分为：全额保证金、国债质押、存单质押则不计算在关联方金额内
                            List<LmtSubPrdLowGuarRel> lmtSubPrdLowGuarRels = lmtSubPrdLowGuarRelService.queryLmtSubPrdLowGuarRelListByPrdSerno(lmtAppSubPrd.getSubPrdSerno());
                            for (LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel : lmtSubPrdLowGuarRels) {
                                if ("10".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                        || "11".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())
                                        || "13".equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())) {
                                    lowRiskNoRelAmt = lowRiskNoRelAmt.add(lmtSubPrdLowGuarRel.getLmtAmt());
                                }
                            }
                        }
                    }
                }
                ztLmtAmt = ztLmtAmt.add(lmtApp.getOpenTotalLmtAmt()).add(lmtApp.getLowRiskTotalLmtAmt()).subtract(lowRiskNoRelAmt);
            }else{
                logger.info("计算关联交易类型：----->在途授信金额  对公综合授信 openTotalLmtAmt {}", BigDecimal.ZERO);
            }
            logger.info("计算关联交易类型：----->在途授信金额  对公综合授信 ztLmtAmt {}", ztLmtAmt);
        }else if(CmisCusConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)){//同业
            //获取同业客户综合授信  在途授信金额
            BigDecimal ztIntBankLmtAmt = lmtIntbankAppService.getIntbankZtAmt(cusId);
            logger.info("计算关联交易类型：----->在途授信金额  同业客户综合授信 ztIntBankLmtAmt {}", ztIntBankLmtAmt);
            ztLmtAmt = ztLmtAmt.add(ztIntBankLmtAmt);
            logger.info("计算关联交易类型：-----> 累加 同业客户综合授信  后 ztLmtAmt {}", ztLmtAmt);
        }

        //获取客户单笔投资业务  在途授信金额（除承销额度）
        BigDecimal ztInvestLmtAmt = lmtSigInvestAppService.getSigInvestZtAmt(cusId);
        logger.info("计算关联交易类型：----->在途授信金额  单笔投资类型 ztInvestLmtAmt {}", ztInvestLmtAmt);

        ztLmtAmt = ztLmtAmt.add(ztInvestLmtAmt);
        logger.info("计算关联交易类型：----->累加 单笔投资类型  后 ztLmtAmt {}", ztLmtAmt);
        return ztLmtAmt;
    }

    /**
     * 获取客户在途授信金额
     * @param glfIndivCusIdList
     */
    public BigDecimal queryZtLmtAmtGljyForPer(List<CusListDto> glfIndivCusIdList, String instuCde){
        BigDecimal ztLmtAmt = BigDecimal.ZERO;
        for (CusListDto cusListDto : glfIndivCusIdList) {
            //获取对公综合授信 在途授信金额
            LmtApp lmtApp = lmtAppService.selectOnWayByCusid(cusListDto.getCusId());
            if(lmtApp != null && !StringUtils.isBlank(lmtApp.getSerno())){
                logger.info("计算关联交易类型：----->在途授信金额  关联客户【{}】 对公综合授信 openTotalLmtAmt {}", lmtApp.getOpenTotalLmtAmt());
                ztLmtAmt = ztLmtAmt.add(lmtApp.getOpenTotalLmtAmt() == null ? BigDecimal.ZERO : lmtApp.getOpenTotalLmtAmt());
                logger.info("计算关联交易类型：----->在途授信金额 关联客户组 对公综合授信 ztLmtAmt {}", ztLmtAmt);
            }else{
                logger.info("计算关联交易类型：----->在途授信金额 关联客户组 客户【{}】，无授信", cusListDto.getCusId());
            }
        }
        logger.info("计算关联交易类型：----->累加 关联客户组  ztLmtAmt {}", ztLmtAmt);
        return ztLmtAmt;
    }

    /**
     * 用于授信复议：获取真正的授信处理类型
     */
    public String getLmtIntbankRelLmtType(LmtIntbankApp lmtIntbankApp) throws Exception {
        String lmtType = lmtIntbankApp.getLmtType() ;

        //如果是授信复议，则获取真正的授信类型
        if(CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(lmtType)){
            logger.info("获取真正的授信类型lmtType  本笔申请为授信复议：------->"+lmtIntbankApp.getSerno());
            String origiLmtReplySerno = lmtIntbankApp.getOrigiLmtReplySerno(); //原批复编号
            if(StringUtils.isBlank(origiLmtReplySerno)){
                throw new Exception(EclEnum.ECL070117.value + "：" + lmtIntbankApp.getSerno()) ;
            }

            //获取原批复数据
            LmtIntbankReply origilmtIntbankReply = lmtIntbankReplyService.selectByReplySerno(lmtIntbankApp.getOrigiLmtReplySerno());

            if(origilmtIntbankReply!=null){
                //获取原批复申请类型
                String origiLmtType = origilmtIntbankReply.getLmtType();
                logger.info("获取真正的授信类型lmtType  本笔申请为授信复议，对应的原批复申请类型为：------->"+origiLmtType + " ; 对应原批复申请流水号为：【"+origilmtIntbankReply.getSerno()+"】  ");
                LmtIntbankApp origiLmtIntbankApp = lmtIntbankAppService.selectBySerno(origilmtIntbankReply.getSerno());
                if(CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(origiLmtType)){
                    logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断------->");

                    logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为------->" +origiLmtIntbankApp.getApproveStatus());
                    //判断是否为审批通过的复议，若是，则以该笔数据为准，做授信变更操作
                    if(CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtIntbankApp.getApproveStatus())){
                        logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为【997】，则认定 真正的授信类型为 【授信变更】------->" );
                        lmtType =  CmisLmtConstants.STD_SX_LMT_TYPE_02;
                    }else{
                        logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为【998】，继续向上找， origiLmtIntbankApp  ------->"  + origiLmtIntbankApp.getSerno() );
                        //不为审批通过，则继续向上找
                        lmtType = getLmtIntbankRelLmtType(origiLmtIntbankApp);
                    }
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_01.equals(origiLmtType) && CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtIntbankApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信新增  且  审批状态为 通过 ，则认定 真正的授信类型为 【授信变更】------->origiLmtIntbankApp.Serno = " + origiLmtIntbankApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_02;
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_01.equals(origiLmtType) && !CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtIntbankApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信新增 且  审批状态为 否决 ，则认定 真正的授信类型为 【授信新增】------->origiLmtIntbankApp.Serno = " + origiLmtIntbankApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_01;
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(origiLmtType) && CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtIntbankApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信续作 且  审批状态为 通过 ，则认定 真正的授信类型为 【授信变更】------->origiLmtIntbankApp.Serno = " + origiLmtIntbankApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_02;
                }else{
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信续作 且  审批状态为 否决 ，则认定 真正的授信类型为 【授信续作】------->origiLmtIntbankApp.Serno = " + origiLmtIntbankApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_03;
                }
            }else{
                throw new Exception("获取授信批复异常");
            }
        }

        logger.info("获取真正的授信类型lmtType  结果：------->"+lmtType);
        return lmtType;
    }

    /**
     * 用于授信复议：获取原批复流水号  对应的  授信台账编号
     */
    public String getLmtIntbankRelAccNo(String origiLmtReplySerno) throws Exception {
        logger.info(this.getClass().getName() + "获取原批复流水号{}---star", origiLmtReplySerno);
        String accNo = "";

        LmtIntbankAcc origiLmtIntbankAcc = lmtIntbankAccService.selectByReplySerno(origiLmtReplySerno);

        if(origiLmtIntbankAcc!=null && !StringUtils.isBlank(origiLmtIntbankAcc.getAccNo())){
            accNo = origiLmtIntbankAcc.getAccNo();
        }else{//根据批复编号未获取到台账编号  -- 否决情况，继续向上找
            LmtIntbankReply origiLmtIntbankReply = lmtIntbankReplyService.selectByReplySerno(origiLmtReplySerno);
            String origiSerno = origiLmtIntbankReply.getSerno();//原申请流水号
            if(StringUtils.isBlank(origiSerno)){
                throw new Exception("未根据原批复流水号["+origiLmtReplySerno+"]获取到原申请流水号");
            }
            LmtIntbankApp origiLmtIntbankApp = lmtIntbankAppService.selectBySerno(origiSerno);
            if(origiLmtIntbankApp==null || StringUtils.isBlank(origiLmtIntbankApp.getSerno())){
                throw new Exception("未根据原申请流水号["+origiSerno+"]获取到原申请流水信息");
            }

            origiLmtReplySerno = origiLmtIntbankApp.getOrigiLmtReplySerno();
            accNo = getLmtIntbankRelAccNo(origiLmtReplySerno);// 继续向上找
        }
        logger.info(this.getClass().getName() + "获取原批复流水号{}-----end", accNo);
        return accNo;
    }

    /**
     * 用于授信复议：获取真正的授信处理类型 单笔投资业务
     */
    public String getLmtSigInvestRelLmtType(LmtSigInvestApp lmtSigInvestApp) throws Exception {
        String lmtType = lmtSigInvestApp.getAppType();

        //如果是授信复议，则获取真正的授信类型
        if(CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(lmtType)){
            logger.info("获取真正的授信类型lmtType  本笔申请为授信复议：------->"+lmtSigInvestApp.getSerno());
            String origiLmtReplySerno = lmtSigInvestApp.getOrigiLmtReplySerno(); //原批复编号
            if(StringUtils.isBlank(origiLmtReplySerno)){
                throw new Exception(EclEnum.ECL070117.value + "：" + lmtSigInvestApp.getSerno()) ;
            }

            //获取原批复数据
            LmtSigInvestRst origiLmtSigInvestReply = lmtSigInvestRstService.selectByReplySerno(lmtSigInvestApp.getOrigiLmtReplySerno());

            if(origiLmtSigInvestReply!=null){
                //获取原批复申请类型
                String origiLmtType = origiLmtSigInvestReply.getAppType();
                logger.info("获取真正的授信类型lmtType  本笔申请为授信复议，对应的原批复申请类型为：------->"+origiLmtType + " ; 对应原批复申请流水号为：【"+origiLmtSigInvestReply.getSerno()+"】  ");
                LmtSigInvestApp origiLmtSigInvestApp = lmtSigInvestAppService.selectBySerno(origiLmtSigInvestReply.getSerno());
                if(CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(origiLmtType)){
                    logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断------->");

                    logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为------->" +origiLmtSigInvestApp.getApproveStatus());
                    //判断是否为审批通过的复议，若是，则以该笔数据为准，做授信变更操作
                    if(CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtSigInvestApp.getApproveStatus())){
                        logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为【997】，则认定 真正的授信类型为 【授信变更】------->" );
                        lmtType =  CmisLmtConstants.STD_SX_LMT_TYPE_02;
                    }else{
                        logger.info("获取真正的授信类型lmtType  原批复申请类型为授信复议时：判断  原批复审批状态为【998】，继续向上找， origiLmtSigInvestApp  ------->"  + origiLmtSigInvestApp.getSerno() );
                        //不为审批通过，则继续向上找
                        lmtType = getLmtSigInvestRelLmtType(origiLmtSigInvestApp);
                    }
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_01.equals(origiLmtType) && CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtSigInvestApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信新增  且  审批状态为 通过 ，则认定 真正的授信类型为 【授信变更】------->origiLmtSigInvestApp.Serno = " + origiLmtSigInvestApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_02;
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_01.equals(origiLmtType) && !CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtSigInvestApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信新增 且  审批状态为 否决 ，则认定 真正的授信类型为 【授信新增】------->origiLmtSigInvestApp.Serno = " + origiLmtSigInvestApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_01;
                }else if(CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(origiLmtType) && CmisBizConstants.APPLY_STATE_PASS.equals(origiLmtSigInvestApp.getApproveStatus())){
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信续作 且  审批状态为 通过 ，则认定 真正的授信类型为 【授信变更】------->origiLmtSigInvestApp.Serno = " + origiLmtSigInvestApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_02;
                }else{
                    logger.info("获取真正的授信类型lmtType  本笔申请为授信续作 且  审批状态为 否决 ，则认定 真正的授信类型为 【授信续作】------->origiLmtSigInvestApp.Serno = " + origiLmtSigInvestApp.getSerno());
                    lmtType = CmisLmtConstants.STD_SX_LMT_TYPE_03;
                }
            }else{
                throw new Exception("获取授信批复异常");
            }
        }
        logger.info("获取真正的授信类型lmtType  结果：------->"+lmtType);
        return lmtType;
    }

    /**
     * 用于授信复议：获取原批复流水号  对应的  授信台账编号  单笔投资业务
     */
    public String getLmtSigInvestRelAccNo(String origiLmtReplySerno) throws Exception {
        logger.info(this.getClass().getName() + "获取原批复流水号{}---star", origiLmtReplySerno);
        String accNo = "";

        LmtSigInvestAcc origiLmtSigInvestAcc = lmtSigInvestAccService.selectByReplySerno(origiLmtReplySerno);

        if(origiLmtSigInvestAcc!=null && !StringUtils.isBlank(origiLmtSigInvestAcc.getAccNo())){
            accNo = origiLmtSigInvestAcc.getAccNo();
        }else{//根据批复编号未获取到台账编号  -- 否决情况，继续向上找
            LmtSigInvestRst origiLmtSigInvestRst = lmtSigInvestRstService.selectByReplySerno(origiLmtReplySerno);
            String origiSerno = origiLmtSigInvestRst.getSerno();//原申请流水号
            if(StringUtils.isBlank(origiSerno)){
                throw new Exception("未根据原批复流水号["+origiLmtReplySerno+"]获取到原申请流水号");
            }
            LmtSigInvestApp origilmtSigInvestApp = lmtSigInvestAppService.selectBySerno(origiSerno);
            if(origilmtSigInvestApp==null || StringUtils.isBlank(origilmtSigInvestApp.getSerno())){
                throw new Exception("未根据原申请流水号["+origiSerno+"]获取到原申请流水信息");
            }

            origiLmtReplySerno = origilmtSigInvestApp.getOrigiLmtReplySerno();
            accNo = getLmtIntbankRelAccNo(origiLmtReplySerno);// 继续向上找
        }
        logger.info(this.getClass().getName() + "获取原批复流水号{}-----end", accNo);
        return accNo;
    }
}