package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.IntbankOrgAdmitAppService;
import cn.com.yusys.yusp.service.IntbankOrgAdmitApprService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 同业机构准入申请流程
 * by lixy
 */
@Service
public class TYSX01BizService implements ClientBizInterface {

    private final Logger logger = LoggerFactory.getLogger(TYSX01BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService;

    @Autowired
    private IntbankOrgAdmitApprService intbankOrgAdmitApprService ;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();

        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();

        logger.info("后业务处理类型{}", currentOpType);
        IntbankOrgAdmitApp intbankOrgAdmitApp = null;
        try {
            intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(serno);

            //加载审批路由
            put2VarParam(resultInstanceDto,serno);

            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
                //判断当前节点是否出具审查报告、授信批复节点
                String issueReportType = "";
                //获取下一审批节点信息
                String nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
                if(resultInstanceDto.getNextNodeInfos()!=null && resultInstanceDto.getNextNodeInfos().size()>0){
                    nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
                }
                //判断下一审批节点是否包含出具审查报告、出具批复页面
                if(CmisBizConstants.TYSX01_01.contains(nextNodeId+",")){
                    issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_01;
                }else if(CmisBizConstants.TYSX01_03.contains(nextNodeId+",")){
                    issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_03;
                }
                //如果当前节点是发起节点，则从申请表copy数据到审批表；如果当前节点非发起节点，则copy审批表中最新的数据至审批表
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                if(CmisBizConstants.TYSX01_START.equals(currNodeId)){
                    //从申请表生成对应的审批表信息
                    intbankOrgAdmitAppService.generateIntbankOrgAdmitApprInfo(intbankOrgAdmitApp,currentUserId,currentOrgId,issueReportType);
                }else{
                    String cur_next_id = currNodeId+";"+nextNodeId;
                    //如果当前节点非发起节点，则copy审批表中最新的数据至审批表
                    intbankOrgAdmitApprService.generateIntbankOrgAdmitApprService(intbankOrgAdmitApp,currentUserId,currentOrgId,issueReportType,CmisBizConstants.APPLY_STATE_APP,cur_next_id);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                intbankOrgAdmitAppService.handleAfterEnd(intbankOrgAdmitApp,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_PASS);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedBack(intbankOrgAdmitApp);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedBack(intbankOrgAdmitApp);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("同业机构准入申请启用【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("同业机构准入申请启用【{}】，流程否决操作，流程参数【{}】", serno, resultInstanceDto);
                intbankOrgAdmitAppService.handleAfterEnd(intbankOrgAdmitApp,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_REFUSE);
            } else {
                logger.warn("同业机构准入申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("同业机构准入申请审批后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称：extractedBack
     * @方法描述：执行流程打回或退回操作
     * @param intbankOrgAdmitApp    流程准入申请信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void extractedBack(IntbankOrgAdmitApp intbankOrgAdmitApp) {
        //审批打回至客户经理处
        intbankOrgAdmitApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
        intbankOrgAdmitAppService.update(intbankOrgAdmitApp);
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.TYSX01).equals(flowCode);
    }
    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数-同业授信
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: zhangjw 20210719
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = intbankOrgAdmitAppService.getRouterMapResult(resultInstanceDto,serno);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        return params;
    }

}
