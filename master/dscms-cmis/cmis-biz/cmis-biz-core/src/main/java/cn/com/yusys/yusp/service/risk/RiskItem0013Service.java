package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtLadEval;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * 单一客户限额校验
 */
@Service
public class RiskItem0013Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0013Service.class);

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtLadEvalService lmtLadEvalService;

    //LMT_APP_SUB_PRD
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    //LMT_APP_SUB
    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * @方法名称: riskItem0013
     * @方法描述: 单一客户限额校验
     * @参数与返回说明:
     * @算法描述: 本次授信敞口大于单一客户限额，则提示。
     * ---解释说明：
     * （1）本次授信敞口=授信总额-低风险额度-专业贷款额度
     * （2）单一客户限额：客户经理人工点击测算按钮，调用非零内评测算。
     * （3）专业贷款：产品上有标识
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0013(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = (String) queryModel.getCondition().get("bizId");
        String bizType = (String) queryModel.getCondition().get("bizType");
        log.info("本次授信敞口大于单一客户限额开始*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        if (StringUtils.isEmpty(serno) || StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
        }

        /**
         * 单一客户授信业务场景编号
         */
        if (CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType)) {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            if (Objects.nonNull(lmtApp)) {
                List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(serno);
                if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                    long count = lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                            && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                            && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                    if (count == 0) {
                        // 授信新增仅申报的授信分项中 只有低风险，不进行校验
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                        return riskResultDto;
                    }
                }
                // 获取客户模块中的客户基本信息
                ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId());
                if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
                    return riskResultDto;
                }
                CusBaseDto cusBaseDtoDo = resultDto.getData();
                String cusCatalog = cusBaseDtoDo.getCusCatalog();
                // 借款人必须是企业（客户大类 2-对公 1-对私）
                if (Objects.equals("2", cusCatalog)) {
                    //敞口额度合计
                    BigDecimal openTotalLmtAmt = Objects.nonNull(lmtApp.getOpenTotalLmtAmt()) ? lmtApp.getOpenTotalLmtAmt() : BigDecimal.ZERO;
                    riskResultDto = checkIsLmtAmt(openTotalLmtAmt, serno);
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        log.info("本次授信敞口大于单一客户限额结束*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param openTotalLmtAmt
     * @param serno
     * @return
     */
    private RiskResultDto checkIsLmtAmt(BigDecimal openTotalLmtAmt, String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
/*        Map<String,Object> subMap  = lmtAppSubService.riskItemCommonAppSubCheck(serno);
        riskResultDto = (RiskResultDto) subMap.get("riskResultDto");
        if(Objects.nonNull(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 初始化专业贷款额度
        BigDecimal proLmtAmt = BigDecimal.ZERO;
        List<LmtAppSubPrd> subPrdList = (List<LmtAppSubPrd>) subMap.get("subPrdList");
        if(CollectionUtils.nonEmpty(subPrdList)) {
            for (LmtAppSubPrd lmtAppSubPrd : subPrdList) {
                if (lmtAppSubPrd.getLmtBizType().indexOf(CmisBizConstants.ProfessionalLoan_Prefix_1401)==0
                        ||lmtAppSubPrd.getLmtBizType().indexOf(CmisBizConstants.ProfessionalLoan_Prefix_1402)==0){
                    proLmtAmt.add(Objects.nonNull(lmtAppSubPrd.getLmtAmt()) ? lmtAppSubPrd.getLmtAmt() : BigDecimal.ZERO);
                }
            }
        }*/
        //测算最高流动资金贷款额度(单一客户限额) -- 单一客户限额：客户经理人工点击测算按钮，调用非零内评测算。
        //单一客户限额 SINGLE_EVAL_LIMIT
        BigDecimal singleEvalLimit = BigDecimal.ZERO;
        //LMT_LAD_EVAL
        LmtLadEval lmtLadEval = lmtLadEvalService.selectSingleBySerno(serno);
        if(Objects.isNull(lmtLadEval)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10501);
            return riskResultDto;
        }
        singleEvalLimit=Objects.nonNull(lmtLadEval.getSingleEvalLimit()) ? lmtLadEval.getSingleEvalLimit() : BigDecimal.ZERO;
        //本次授信敞口大于单一客户限额，则提示。
        if (openTotalLmtAmt.compareTo(singleEvalLimit) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01301);
            return riskResultDto;
        }
        return riskResultDto;
    }
}
