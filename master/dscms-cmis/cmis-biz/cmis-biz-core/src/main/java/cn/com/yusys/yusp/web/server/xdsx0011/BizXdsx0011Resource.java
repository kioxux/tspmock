package cn.com.yusys.yusp.web.server.xdsx0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0011.req.Xdsx0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.resp.Xdsx0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdsx0011.Xdsx0011Service;
import cn.com.yusys.yusp.service.server.xdsx0016.Xdsx0016Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询优惠（省心E付）
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0011:查询优惠（省心E付）")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0011Resource.class);
    @Autowired
    private Xdsx0011Service xdsx0011Service;
    /**
     * 交易码：xdsx0011
     * 交易描述：查询优惠（省心E付）
     *
     * @param xdsx0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优惠（省心E付）")
    @PostMapping("/xdsx0011")
    protected @ResponseBody
    ResultDto<Xdsx0011DataRespDto> xdsx0011(@Validated @RequestBody Xdsx0011DataReqDto xdsx0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011DataReqDto));
        Xdsx0011DataRespDto xdsx0011DataRespDto = new Xdsx0011DataRespDto();// 响应Dto:查询优惠（省心E付）
        ResultDto<Xdsx0011DataRespDto> xdsx0011DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0011DataReqDto获取业务值进行业务逻辑处理
            // 调用Xdsx0011Service层开始
            xdsx0011DataRespDto = xdsx0011Service.xdsx0011(xdsx0011DataReqDto);
            //调用Xdsx0011Service层结束
            // 封装xdsx0011DataResultDto中正确的返回码和返回信息
            xdsx0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            // 封装xdsx0011DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdsx0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdsx0011DataRespDto到xdsx0011DataResultDto中
        xdsx0011DataResultDto.setData(xdsx0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011DataRespDto));
        return xdsx0011DataResultDto;
    }
}
