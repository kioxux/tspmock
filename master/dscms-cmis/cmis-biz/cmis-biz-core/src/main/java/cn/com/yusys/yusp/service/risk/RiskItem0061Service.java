package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0061Service
 * @类描述: 贷款期限校验
 * @功能描述: 展期期限不得小于原到期日
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0061Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0061Service.class);

    @Autowired
    private IqpContExtBillService iqpContExtBillService;

    /**
     * @方法名称: riskItem0061
     * @方法描述: 展期期限不得小于原到期日
     * @参数与返回说明:
     * @算法描述:
     * @创建人: macm
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0061(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("展期期限日期校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        //  根据流水号获取展期申请中借据列表
        QueryModel queryMap = new QueryModel();
        queryMap.addCondition("iqpSerno",serno);
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryMap);
        if(CollectionUtils.isEmpty(list)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06101);
            return riskResultDto;
        }
        for(IqpContExtBill iqpContExtBill :list) {
            // 获取贷款到期日
            String loanEndDate = iqpContExtBill.getLoanEndDate();
            if(StringUtils.isBlank(loanEndDate)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06102);
                return riskResultDto;
            }
            // 获取展期后到期日
            String extEndDate = iqpContExtBill.getExtEndDate();
            if(StringUtils.isBlank(extEndDate)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06103);
                return riskResultDto;
            }
            // 展期到期日小于贷款到期日 则拦截
            if(extEndDate.compareTo(loanEndDate) < 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06104);
                return riskResultDto;
            }
        }
        log.info("展期期限日期校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
