package cn.com.yusys.yusp.web.server.xdcz0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0020.req.Xdcz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.resp.Xdcz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0020.Xdcz0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:小贷用途承诺书文本生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0020:小贷用途承诺书文本生成pdf")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0020Resource.class);

    @Autowired
    private Xdcz0020Service xdcz0020Service;
    /**
     * 交易码：xdcz0020
     * 交易描述：小贷用途承诺书文本生成pdf
     *
     * @param xdcz0020DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("小贷用途承诺书文本生成pdf")
    @PostMapping("/xdcz0020")
    protected @ResponseBody
    ResultDto<Xdcz0020DataRespDto>  xdcz0020(@Validated @RequestBody Xdcz0020DataReqDto xdcz0020DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataReqDto));
        Xdcz0020DataRespDto  xdcz0020DataRespDto  = new Xdcz0020DataRespDto();// 响应Dto:小贷用途承诺书文本生成pdf
        ResultDto<Xdcz0020DataRespDto> xdcz0020DataResultDto = new ResultDto<>();
        try {
            String cusName = xdcz0020DataReqDto.getCusName();//客户名称
            String certCode = xdcz0020DataReqDto.getCertCode();//身份证件号码
            String loanUseType = xdcz0020DataReqDto.getLoanUseType();//借款用途
            String applyDate = xdcz0020DataReqDto.getApplyDate();//申请日期
            //必输字段校验
            if (StringUtil.isEmpty(cusName)) {
                xdcz0020DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0020DataResultDto.setMessage("客户名称【cusName】不能为空！");
                return xdcz0020DataResultDto;
            }
            if (StringUtil.isEmpty(certCode)) {
                xdcz0020DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0020DataResultDto.setMessage("证件号码【certCode】不能为空！");
                return xdcz0020DataResultDto;
            }
            if (StringUtil.isEmpty(applyDate)) {
                xdcz0020DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0020DataResultDto.setMessage("申请日期【applyDate】不能为空！");
                return xdcz0020DataResultDto;
            }

            // 调用xdcz0020Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataReqDto));
            xdcz0020DataRespDto = xdcz0020Service.getXdcz0020(xdcz0020DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataReqDto));
            // 封装xdcz0020DataResultDto中正确的返回码和返回信息
            xdcz0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value,e.getMessage());
            // 封装xdcz0020DataResultDto中异常返回码和返回信息
            xdcz0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0020DataRespDto到xdcz0020DataResultDto中
        xdcz0020DataResultDto.setData(xdcz0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataRespDto));
        return xdcz0020DataResultDto;
    }
}
