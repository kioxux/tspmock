/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.CreditReportLoanFinaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditReportLoanFina;
import cn.com.yusys.yusp.service.CreditReportLoanFinaService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportLoanFinaResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-07 16:49:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditreportloanfina")
public class CreditReportLoanFinaResource {
    @Autowired
    private CreditReportLoanFinaService creditReportLoanFinaService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditReportLoanFina>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditReportLoanFina> list = creditReportLoanFinaService.selectAll(queryModel);
        return new ResultDto<List<CreditReportLoanFina>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CreditReportLoanFina>> index(@RequestBody QueryModel queryModel) {
        List<CreditReportLoanFina> list = creditReportLoanFinaService.selectByModel(queryModel);
        return new ResultDto<List<CreditReportLoanFina>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{crlfSerno}")
    protected ResultDto<CreditReportLoanFina> show(@PathVariable("crlfSerno") String crlfSerno) {
        CreditReportLoanFina creditReportLoanFina = creditReportLoanFinaService.selectByPrimaryKey(crlfSerno);
        return new ResultDto<CreditReportLoanFina>(creditReportLoanFina);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CreditReportLoanFina> create(@RequestBody CreditReportLoanFina creditReportLoanFina) throws URISyntaxException {
        creditReportLoanFinaService.insert(creditReportLoanFina);
        return new ResultDto<CreditReportLoanFina>(creditReportLoanFina);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditReportLoanFina creditReportLoanFina) throws URISyntaxException {
        int result = creditReportLoanFinaService.update(creditReportLoanFina);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{crlfSerno}")
    protected ResultDto<Integer> delete(@PathVariable("crlfSerno") String crlfSerno) {
        int result = creditReportLoanFinaService.deleteByPrimaryKey(crlfSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditReportLoanFinaService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:createCreditAndLoan
     * @函数描述:移动OAM征信查询信息保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createcreditandloan")
    protected ResultDto<Integer> createCreditAndLoan(@RequestBody CreditReportLoanFinaDto creditReportLoanFinaDto) {
        int count = creditReportLoanFinaService.createCreditAndLoan(creditReportLoanFinaDto);
        return new ResultDto<>(count);
    }

    /**
     * @函数名称:queryByCrqlSerno
     * @函数描述:通过征信流水号查询贷款融资信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querybycrqlserno")
    protected ResultDto<CreditReportLoanFina> queryByCrqlSerno(@RequestBody String crqlSerno) {
        CreditReportLoanFina creditReportLoanFina = creditReportLoanFinaService.queryByCrqlSerno(crqlSerno);
        return new ResultDto<CreditReportLoanFina>(creditReportLoanFina);
    }

    /**
     * @函数名称:selectByCrqlSerno
     * @函数描述:通过征信流水号查询完整信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbycrqlserno")
    protected ResultDto<CreditReportLoanFinaDto> selectByCrqlSerno(@RequestBody Map map) {
        String crqlSerno = (String) map.get("crqlSerno");
        CreditReportLoanFinaDto creditReportLoanFinaDto = creditReportLoanFinaService.selectByCrqlSerno(crqlSerno);
        return new ResultDto<CreditReportLoanFinaDto>(creditReportLoanFinaDto);
    }

    /**
     * @函数名称:getSequences
     * @函数描述:用于获取序列号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querysequences")
    public ResultDto<String> querySequences() {
        String taskNo = creditReportLoanFinaService.querySequences();
        return new ResultDto<String>(taskNo);
    }
}
