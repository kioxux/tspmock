package cn.com.yusys.yusp.web.server.xdht0027;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0027.req.Xdht0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0027.resp.Xdht0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0027.Xdht0027Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:根据客户调查表编号取得贷款合同主表的合同状态
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0027:根据客户调查表编号取得贷款合同主表的合同状态")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0027Resource.class);
    @Resource
    private Xdht0027Service xdht0027Service;
    /**
     * 交易码：xdht0027
     * 交易描述：根据客户调查表编号取得贷款合同主表的合同状态
     *
     * @param xdht0027DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户调查表编号取得贷款合同主表的合同状态")
    @PostMapping("/xdht0027")
    protected @ResponseBody
    ResultDto<Xdht0027DataRespDto> xdht0027(@Validated @RequestBody Xdht0027DataReqDto xdht0027DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataReqDto));
        Xdht0027DataRespDto xdht0027DataRespDto = new Xdht0027DataRespDto();// 响应Dto:根据客户调查表编号取得贷款合同主表的合同状态
        ResultDto<Xdht0027DataRespDto> xdht0027DataResultDto = new ResultDto<>();
        try {
            // 从xdht0027DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataReqDto));
            xdht0027DataRespDto = xdht0027Service.queryContStatusByCertCode(xdht0027DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataRespDto));
            // 封装xdht0027DataResultDto中正确的返回码和返回信息
            xdht0027DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0027DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, e.getMessage());
            // 封装xdht0027DataResultDto中异常返回码和返回信息
            //  EpbEnum.EPB099999  开始
            xdht0027DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0027DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EpbEnum.EPB099999   结束
        }
        // 封装xdht0027DataRespDto到xdht0027DataResultDto中
        xdht0027DataResultDto.setData(xdht0027DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataRespDto));
        return xdht0027DataResultDto;
    }
}
