package cn.com.yusys.yusp.service.server.xdtz0057;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.req.Xdtz0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0057.resp.Xdtz0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdtz0057Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-18 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0057Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0057Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;

    @Resource
    private AdminSmUserService adminSmUserService;

    @Resource
    private CommonService commonService;

    /**
     * 根据流水号查询客户调查的放款信息（在途需求）
     *
     * @param xdtz0057DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0057DataRespDto getXdtz0057(Xdtz0057DataReqDto xdtz0057DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataReqDto));
        Xdtz0057DataRespDto xdtz0057DataRespDto = new Xdtz0057DataRespDto();
        try {
            String surveySerno = xdtz0057DataReqDto.getSurveySerno();
            java.util.List<List> list = new ArrayList<>();
            if (StringUtils.isEmpty(surveySerno)) {
                throw BizException.error(null, EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
            } else {
                list = accLoanMapper.getAccInfoBySurveySerno(surveySerno);
                for (List item : list) {
                    //业务负责人
                    String accMgr = item.getAccMgr();
                    //客户编号-用于查询管护人
                    String cusMgr = item.getCusMgr();
                    CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusMgr);
                    String managerId = cusBaseDto.getManagerId();
                    java.util.List cusIds = new ArrayList();
                    cusIds.add(accMgr);
                    cusIds.add(managerId);
                    ResultDto<java.util.List<AdminSmUserDto>> resultDto = adminSmUserService.getByLoginCodeList(cusIds);
                    if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                        java.util.List<AdminSmUserDto> adminSmUserDtos = resultDto.getData();
                        if (CollectionUtils.nonEmpty(adminSmUserDtos)) {
                            for (AdminSmUserDto adminSmUserDto : adminSmUserDtos) {
                                if (Objects.equals(accMgr, adminSmUserDto.getLoginCode())) {
                                    accMgr = adminSmUserDto.getUserName();
                                }
                                if (Objects.equals(managerId, adminSmUserDto.getLoginCode())) {
                                    managerId = adminSmUserDto.getUserName();
                                }
                            }
                        }
                    }
                    item.setAccMgr(accMgr);
                    item.setCusMgr(managerId);
                }
                xdtz0057DataRespDto.setList(list);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, e.getMessage());
            throw new Exception(EcbEnum.ECB019999.value);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataRespDto));
        return xdtz0057DataRespDto;
    }
}
