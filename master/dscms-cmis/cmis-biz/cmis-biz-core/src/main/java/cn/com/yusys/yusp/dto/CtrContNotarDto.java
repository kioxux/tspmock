package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContNotar
 * @类描述: ctr_cont_notar数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-30 19:17:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrContNotarDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 是否公证 STD_ZB_IS_NOTAR **/
	private String isNotar;
	
	/** 公证员 **/
	private String greffierName;
	
	/** 公证合同编号 **/
	private String notaryContCode;
	
	/** 公证单位注册号 **/
	private String notaryUnitNum;
	
	/** 公证单位名称 **/
	private String notaryUnitName;
	
	/** 公证书编号 **/
	private String notarizationCode;
	
	/** 公证日期 **/
	private String notaryDate;
	
	/** 公证合同类型 **/
	private String notaryContType;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param isNotar
	 */
	public void setIsNotar(String isNotar) {
		this.isNotar = isNotar == null ? null : isNotar.trim();
	}
	
    /**
     * @return IsNotar
     */	
	public String getIsNotar() {
		return this.isNotar;
	}
	
	/**
	 * @param greffierName
	 */
	public void setGreffierName(String greffierName) {
		this.greffierName = greffierName == null ? null : greffierName.trim();
	}
	
    /**
     * @return GreffierName
     */	
	public String getGreffierName() {
		return this.greffierName;
	}
	
	/**
	 * @param notaryContCode
	 */
	public void setNotaryContCode(String notaryContCode) {
		this.notaryContCode = notaryContCode == null ? null : notaryContCode.trim();
	}
	
    /**
     * @return NotaryContCode
     */	
	public String getNotaryContCode() {
		return this.notaryContCode;
	}
	
	/**
	 * @param notaryUnitNum
	 */
	public void setNotaryUnitNum(String notaryUnitNum) {
		this.notaryUnitNum = notaryUnitNum == null ? null : notaryUnitNum.trim();
	}
	
    /**
     * @return NotaryUnitNum
     */	
	public String getNotaryUnitNum() {
		return this.notaryUnitNum;
	}
	
	/**
	 * @param notaryUnitName
	 */
	public void setNotaryUnitName(String notaryUnitName) {
		this.notaryUnitName = notaryUnitName == null ? null : notaryUnitName.trim();
	}
	
    /**
     * @return NotaryUnitName
     */	
	public String getNotaryUnitName() {
		return this.notaryUnitName;
	}
	
	/**
	 * @param notarizationCode
	 */
	public void setNotarizationCode(String notarizationCode) {
		this.notarizationCode = notarizationCode == null ? null : notarizationCode.trim();
	}
	
    /**
     * @return NotarizationCode
     */	
	public String getNotarizationCode() {
		return this.notarizationCode;
	}
	
	/**
	 * @param notaryDate
	 */
	public void setNotaryDate(String notaryDate) {
		this.notaryDate = notaryDate == null ? null : notaryDate.trim();
	}
	
    /**
     * @return NotaryDate
     */	
	public String getNotaryDate() {
		return this.notaryDate;
	}
	
	/**
	 * @param notaryContType
	 */
	public void setNotaryContType(String notaryContType) {
		this.notaryContType = notaryContType == null ? null : notaryContType.trim();
	}
	
    /**
     * @return NotaryContType
     */	
	public String getNotaryContType() {
		return this.notaryContType;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}