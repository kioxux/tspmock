package cn.com.yusys.yusp.service.server.xdxw0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtGuareCloestInfo;
import cn.com.yusys.yusp.dto.server.xdxw0001.req.Xdxw0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0001.resp.Xdxw0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareCloestInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * 房屋估价信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxw0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0001Service.class);

    @Resource
    private LmtGuareCloestInfoMapper lmtGuareCloestInfoMapper;

    /**
     * 房屋估价信息同步
     *
     * @param xdxw0001DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0001DataRespDto getXdxw0001(Xdxw0001DataReqDto xdxw0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataReqDto));
        Xdxw0001DataRespDto xdxw0001DataRespDto = new Xdxw0001DataRespDto();
        try {
            LmtGuareCloestInfo lmtGuareCloestInfo = new LmtGuareCloestInfo();
            BeanUtils.copyProperties(xdxw0001DataReqDto, lmtGuareCloestInfo);
            //生成UUID主键
            String pkId = UUID.randomUUID().toString().replace("-", "");
            lmtGuareCloestInfo.setPkId(pkId);
            //日期
            if (StringUtils.nonEmpty(xdxw0001DataReqDto.getTranDate())) {
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                lmtGuareCloestInfo.setCreateTime(sf.parse(xdxw0001DataReqDto.getTranDate()));
            }
            lmtGuareCloestInfo.setBuildingName(xdxw0001DataReqDto.getBuildings());
            lmtGuareCloestInfo.setAssEvaAmt(xdxw0001DataReqDto.getAssetAmt());
            lmtGuareCloestInfo.setTotalAmt(xdxw0001DataReqDto.getTotalAmt());
            lmtGuareCloestInfo.setAddr(xdxw0001DataReqDto.getAddr());
            lmtGuareCloestInfo.setQryUser(xdxw0001DataReqDto.getQryName());
            Integer squ = xdxw0001DataReqDto.getSqu();//面积
            if (squ != null) {
                lmtGuareCloestInfo.setSqu(squ.toString());
            }
            logger.info("***************插入lmtGuareCloestInfo信息开始,插入参数为:{}", JSON.toJSONString(lmtGuareCloestInfo));
            int flag = lmtGuareCloestInfoMapper.insertSelective(lmtGuareCloestInfo);
            logger.info("***************插入lmtGuareCloestInfo信息结束,返回结果为:{}", JSON.toJSONString(flag));
            if (flag < 1) {
                xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
                xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
                throw BizException.error(null, null, CommonConstance.OP_MSG_F);
            } else {
                xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
                xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, e.getMessage());
            xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, e.getMessage());
            xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataRespDto));
        return xdxw0001DataRespDto;
    }

}
