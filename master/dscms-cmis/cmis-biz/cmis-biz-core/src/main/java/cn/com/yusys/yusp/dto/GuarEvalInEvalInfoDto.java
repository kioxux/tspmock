package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalInEvalInfo
 * @类描述: guar_eval_in_eval_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-29 09:51:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalInEvalInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 内评流水号 **/
	private String inEvalSerno;
	
	/** 申请流水号 **/
	private String evalApplySerno;
	
	/** 评估方法 **/
	private String evalEnname;
	
	/** 内部评估价值 **/
	private java.math.BigDecimal evalInAmt;
	
	/** 内部评估价值币种 **/
	private String evalInCurrency;
	
	/** 内部评估价值时间 **/
	private String evalInDate;
	
	/** 评估方法选择理由 **/
	private String evalTypeResn;
	
	/** 评估类型 **/
	private String evalType;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param inEvalSerno
	 */
	public void setInEvalSerno(String inEvalSerno) {
		this.inEvalSerno = inEvalSerno == null ? null : inEvalSerno.trim();
	}
	
    /**
     * @return InEvalSerno
     */	
	public String getInEvalSerno() {
		return this.inEvalSerno;
	}
	
	/**
	 * @param evalApplySerno
	 */
	public void setEvalApplySerno(String evalApplySerno) {
		this.evalApplySerno = evalApplySerno == null ? null : evalApplySerno.trim();
	}
	
    /**
     * @return EvalApplySerno
     */	
	public String getEvalApplySerno() {
		return this.evalApplySerno;
	}
	
	/**
	 * @param evalEnname
	 */
	public void setEvalEnname(String evalEnname) {
		this.evalEnname = evalEnname == null ? null : evalEnname.trim();
	}
	
    /**
     * @return EvalEnname
     */	
	public String getEvalEnname() {
		return this.evalEnname;
	}
	
	/**
	 * @param evalInAmt
	 */
	public void setEvalInAmt(java.math.BigDecimal evalInAmt) {
		this.evalInAmt = evalInAmt;
	}
	
    /**
     * @return EvalInAmt
     */	
	public java.math.BigDecimal getEvalInAmt() {
		return this.evalInAmt;
	}
	
	/**
	 * @param evalInCurrency
	 */
	public void setEvalInCurrency(String evalInCurrency) {
		this.evalInCurrency = evalInCurrency == null ? null : evalInCurrency.trim();
	}
	
    /**
     * @return EvalInCurrency
     */	
	public String getEvalInCurrency() {
		return this.evalInCurrency;
	}
	
	/**
	 * @param evalInDate
	 */
	public void setEvalInDate(String evalInDate) {
		this.evalInDate = evalInDate == null ? null : evalInDate.trim();
	}
	
    /**
     * @return EvalInDate
     */	
	public String getEvalInDate() {
		return this.evalInDate;
	}
	
	/**
	 * @param evalTypeResn
	 */
	public void setEvalTypeResn(String evalTypeResn) {
		this.evalTypeResn = evalTypeResn == null ? null : evalTypeResn.trim();
	}
	
    /**
     * @return EvalTypeResn
     */	
	public String getEvalTypeResn() {
		return this.evalTypeResn;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType == null ? null : evalType.trim();
	}
	
    /**
     * @return EvalType
     */	
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}