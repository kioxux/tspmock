/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignOfBocApp;
import cn.com.yusys.yusp.service.OtherRecordAccpSignOfBocAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignOfBocAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-15 14:20:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherrecordaccpsignofbocapp")
public class OtherRecordAccpSignOfBocAppResource {
    private static final Logger log = LoggerFactory.getLogger(OtherRecordAccpSignOfBocAppResource.class);

    @Autowired
    private OtherRecordAccpSignOfBocAppService otherRecordAccpSignOfBocAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherRecordAccpSignOfBocApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherRecordAccpSignOfBocApp> list = otherRecordAccpSignOfBocAppService.selectAll(queryModel);
        return new ResultDto<List<OtherRecordAccpSignOfBocApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<OtherRecordAccpSignOfBocApp>> index(QueryModel queryModel) {
        List<OtherRecordAccpSignOfBocApp> list = otherRecordAccpSignOfBocAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordAccpSignOfBocApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherRecordAccpSignOfBocApp> show(@PathVariable("serno") String serno) {
        OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp = otherRecordAccpSignOfBocAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherRecordAccpSignOfBocApp>(otherRecordAccpSignOfBocApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<OtherRecordAccpSignOfBocApp> create(@RequestBody OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp) throws URISyntaxException {
        otherRecordAccpSignOfBocAppService.insert(otherRecordAccpSignOfBocApp);
        return new ResultDto<OtherRecordAccpSignOfBocApp>(otherRecordAccpSignOfBocApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp) throws URISyntaxException {
        //否决、打回s
        if(CmisBizConstants.OPR_TYPE_02.equals(otherRecordAccpSignOfBocApp.getOprType())
                &&CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherRecordAccpSignOfBocApp.getApproveStatus())){
            otherRecordAccpSignOfBocApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherRecordAccpSignOfBocApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherRecordAccpSignOfBocApp.getSerno());
            workflowCoreClient.deleteByBizId(otherRecordAccpSignOfBocApp.getSerno());
        }
        int result = otherRecordAccpSignOfBocAppService.update(otherRecordAccpSignOfBocApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateOprTypeUnderLmt
     * @函数描述:授信场景下，逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) throws URISyntaxException {
        OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp =  otherRecordAccpSignOfBocAppService.selectByPrimaryKey(serno);

        int result = otherRecordAccpSignOfBocAppService.update(otherRecordAccpSignOfBocApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherRecordAccpSignOfBocAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherRecordAccpSignOfBocAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    
    /**
     * @方法名称: getotherrecordaccpsignofbocapp
     * @方法描述: 根据入参获取当前客户经理名下中行代签电票申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherrecordaccpsignofbocapp")
    protected ResultDto<List<OtherRecordAccpSignOfBocApp>> getOtherAccpPerferFeeApp(@RequestBody QueryModel model) {
        List<OtherRecordAccpSignOfBocApp> list = otherRecordAccpSignOfBocAppService.selectByModel(model);
        return new ResultDto<List<OtherRecordAccpSignOfBocApp>>(list);
    }

    /**
     * @方法名称: upIsUpperApprAuth
     * @方法描述:  上调权限处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/upIsUpperApprAuth")
    protected ResultDto<Integer> upIsUpperApprAuth(@RequestBody Map<String,String> params) {
        int result = 0;
        // 获取流水号
        String serno =  params.get("serno");
        // 是否上调权限
        String  isUpperApprAuth = params.get("isUpperApprAuth");
        if(!"".equals(serno) && null != serno){
            OtherRecordAccpSignOfBocApp otherRecordAccpSignOfBocApp =  otherRecordAccpSignOfBocAppService.selectByPrimaryKey(serno);
            if(null != otherRecordAccpSignOfBocApp){
                otherRecordAccpSignOfBocApp.setIsUpperApprAuth(isUpperApprAuth);
                result = otherRecordAccpSignOfBocAppService.update(otherRecordAccpSignOfBocApp);
            }
        }
        return new ResultDto<Integer>(result);
    }


}
