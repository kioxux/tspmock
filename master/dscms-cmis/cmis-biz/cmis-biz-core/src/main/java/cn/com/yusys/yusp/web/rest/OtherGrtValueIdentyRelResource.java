/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.OtherForRateApp;
import cn.com.yusys.yusp.dto.OtherGrtDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherGrtValueIdentyRel;
import cn.com.yusys.yusp.service.OtherGrtValueIdentyRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueIdentyRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 20:39:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othergrtvalueidentyrel")
public class OtherGrtValueIdentyRelResource {
    @Autowired
    private OtherGrtValueIdentyRelService otherGrtValueIdentyRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherGrtValueIdentyRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherGrtValueIdentyRel> list = otherGrtValueIdentyRelService.selectAll(queryModel);
        return new ResultDto<List<OtherGrtValueIdentyRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherGrtValueIdentyRel>> index(QueryModel queryModel) {
        List<OtherGrtValueIdentyRel> list = otherGrtValueIdentyRelService.selectByModel(queryModel);
        return new ResultDto<List<OtherGrtValueIdentyRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<OtherGrtValueIdentyRel> show(@PathVariable("pkId") String pkId) {
        OtherGrtValueIdentyRel otherGrtValueIdentyRel = otherGrtValueIdentyRelService.selectByPrimaryKey(pkId);
        return new ResultDto<OtherGrtValueIdentyRel>(otherGrtValueIdentyRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherGrtValueIdentyRel> create(@RequestBody OtherGrtValueIdentyRel otherGrtValueIdentyRel) throws URISyntaxException {
        otherGrtValueIdentyRelService.insert(otherGrtValueIdentyRel);
        return new ResultDto<OtherGrtValueIdentyRel>(otherGrtValueIdentyRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherGrtValueIdentyRel otherGrtValueIdentyRel) throws URISyntaxException {
        int result = otherGrtValueIdentyRelService.update(otherGrtValueIdentyRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = otherGrtValueIdentyRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherGrtValueIdentyRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: addothergrtvalueidentyrel
     * @方法描述: 新增抵押物信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addothergrtvalueidentyrel")
    protected ResultDto addothergrtvalueidentyrel(@RequestBody OtherGrtDto otherGrtDto) {
        return otherGrtValueIdentyRelService.addothergrtvalueidentyrel(otherGrtDto);
    }

    /**
     * @方法名称: updateothergrtvalueidentyrel
     * @方法描述: 修改抵押物信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateothergrtvalueidentyrel")
    protected ResultDto updateothergrtvalueidentyrel(@RequestBody OtherGrtDto otherGrtDto) {
        return otherGrtValueIdentyRelService.updateothergrtvalueidentyrel(otherGrtDto);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{pkId}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("pkId") String pkId) {
        int result = otherGrtValueIdentyRelService.deleteInfo(pkId);
        return new ResultDto<Integer>(result);
    }

}
