package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Service
public class RiskItem0117Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0117Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private CtrHighAmtAgrContService CtrHighAmtAgrContService;

    /**
     * @方法名称: riskItem0117
     * @方法描述: 出账申请合同影像审核状态检验
     * @参数与返回说明:
     * @算法描述:
     * 对公用信-出账申请-贷款出账申请，如果判断房抵e点贷、(最高额合同)省心快贷出账申请前，未发起合同影像审核，提示报错：出账前需发起并审核通过合同影像审核
     * @创建人: yfs
     * @创建时间: 2021-09-16 15:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public  RiskResultDto riskItem0117(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("出账申请合同影像审核状态检验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        if(Objects.isNull(pvpLoanApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
            return riskResultDto;
        }
        // 获取合同信息
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpLoanApp.getContNo());
        //4、房抵e点贷和省心快贷：房抵e点贷、(最高额合同)省心快贷出账申请前，未发起合同影像审核，提示报错：出账前需发起并审核通过合同影像审核；
        //   房抵e点贷 P034 省心快贷 P011 STD_CONT_TYPE:2 最高额合同
        if(Objects.equals("P034",pvpLoanApp.getPrdTypeProp()) || (Objects.equals("P011",pvpLoanApp.getPrdTypeProp()) && Objects.equals("2",ctrLoanCont.getContType()))) {
            if(Objects.isNull(ctrLoanCont)){
                CtrHighAmtAgrCont ctrHighAmtAgrCont = CtrHighAmtAgrContService.selectDataByContNo(pvpLoanApp.getContNo());
                if(Objects.isNull(ctrHighAmtAgrCont)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_011701);
                    return riskResultDto;
                }
                if(CmisCommonConstants.STD_ZB_YES_NO_0.equals(ctrHighAmtAgrCont.getCtrBeginFlag()) || "".equals(ctrHighAmtAgrCont.getCtrBeginFlag())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02624);
                    return riskResultDto;
                }
            }else{
                if("P034".equals(ctrLoanCont.getPrdTypeProp())){
                    CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByContNo(ctrLoanCont.getContNo());
                    if(!CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrContImageAuditApp.getIsFk())){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02624);
                        return riskResultDto;
                    }
                }
                if(CmisCommonConstants.STD_ZB_YES_NO_0.equals(ctrLoanCont.getCtrBeginFlag()) || "".equals(ctrLoanCont.getCtrBeginFlag())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02624);
                    return riskResultDto;
                }
            }
        }
        log.info("出账申请合同影像审核状态检验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}