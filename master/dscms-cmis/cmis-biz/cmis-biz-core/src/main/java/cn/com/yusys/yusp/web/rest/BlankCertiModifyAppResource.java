/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.BlankCertiModifyAppDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BlankCertiModifyApp;
import cn.com.yusys.yusp.service.BlankCertiModifyAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BlankCertiModifyAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:59:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/blankcertimodifyapp")
public class BlankCertiModifyAppResource {
    @Autowired
    private BlankCertiModifyAppService blankCertiModifyAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BlankCertiModifyApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BlankCertiModifyApp> list = blankCertiModifyAppService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BlankCertiModifyApp>> index(QueryModel queryModel) {
        List<BlankCertiModifyApp> list = blankCertiModifyAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BlankCertiModifyApp> create(@RequestBody BlankCertiModifyApp blankCertiModifyApp) throws URISyntaxException {
        blankCertiModifyAppService.insert(blankCertiModifyApp);
        return new ResultDto<>(blankCertiModifyApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BlankCertiModifyApp blankCertiModifyApp) throws URISyntaxException {
        int result = blankCertiModifyAppService.update(blankCertiModifyApp);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId) {
        int result = blankCertiModifyAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象逻辑删除删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteonlogic")
    protected ResultDto<Integer> deleteOnLogic(@RequestBody BlankCertiModifyApp record) {
        int result = blankCertiModifyAppService.deleteOnLogic(record);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querylist")
    protected ResultDto<List<BlankCertiModifyApp>> queryList(@RequestBody QueryModel queryModel) {
        List<BlankCertiModifyApp> list = blankCertiModifyAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:根据空白凭证变更对象暂存(新增/修改)数据
     * @param blankCertiModifyAppDto
     * @return
     */
    @ApiOperation("根据空白凭证变更对象暂存(新增/修改)数据")
    @PostMapping("/tempsave")
    protected ResultDto<Integer> tempSave(@RequestBody BlankCertiModifyAppDto blankCertiModifyAppDto){
        int result  = blankCertiModifyAppService.tempSave(blankCertiModifyAppDto);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:checkOnTheWaySernoByCertino
     * @函数描述:检查该凭证编号是否有在途的申请
     * @param certiNo
     * @return
     */
    @ApiOperation("检查该凭证编号是否有在途的申请")
    @GetMapping("/checkOnTheWaySernoByCertino/{certiNo}")
    protected ResultDto<String> checkOnTheWaySernoByCertiNo(@PathVariable("certiNo") String certiNo){
        String result  = blankCertiModifyAppService.selectOnTheWaySernoByCertiNo(certiNo);
        return new ResultDto<>(result);
    }
}