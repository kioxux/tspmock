/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanCont
 * @类描述: ctr_loan_cont数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-30 17:17:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_loan_cont")
public class CtrLoanCont extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 合同编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CONT_NO")
	private String contNo;

	/** 中文合同编号 **/
	@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 60)
	private String contCnNo;

	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;

	/** 是否占用第三方额度 **/
	@Column(name = "IS_OUTSTND_TRD_LMT_AMT", unique = false, nullable = true, length = 5)
	private String isOutstndTrdLmtAmt;

	/** 第三方合同协议编号 **/
	@Column(name = "TDP_AGR_NO", unique = false, nullable = true, length = 40)
	private String tdpAgrNo;

	/** 合作方客户编号 **/
	@Column(name = "COOP_CUS_ID", unique = false, nullable = true, length = 20)
	private String coopCusId;

	/** 合作方客户名称 **/
	@Column(name = "COOP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String coopCusName;

	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;

	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;

	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;

	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;

	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 特殊业务类型 **/
	@Column(name = "ESPEC_BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String especBizType;

	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 2000)
	private String loanPurp;

	/** 其他贷款用途描述 **/
	@Column(name = "LOAN_PURP_DESC", unique = false, nullable = true, length = 500)
	private String loanPurpDesc;

	/** 贷款形式 **/
	@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
	private String loanModal;

	/** 贷款性质 **/
	@Column(name = "LOAN_CHA", unique = false, nullable = true, length = 5)
	private String loanCha;

	/** 是否曾被拒绝 **/
	@Column(name = "IS_HAS_REFUSED", unique = false, nullable = true, length = 5)
	private String isHasRefused;

	/** 主担保方式 **/
	@Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
	private String guarWay;

	/** 是否共同申请人 **/
	@Column(name = "IS_COMMON_RQSTR", unique = false, nullable = true, length = 5)
	private String isCommonRqstr;

	/** 是否确认支付方式 **/
	@Column(name = "IS_CFIRM_PAY_WAY", unique = false, nullable = true, length = 5)
	private String isCfirmPayWay;

	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
	private String payMode;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 合同类型 **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;

	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private Integer contTerm;

	/** 合同起始日期 **/
	@Column(name = "CONT_START_DATE", unique = false, nullable = true, length = 10)
	private String contStartDate;

	/** 合同到期日期 **/
	@Column(name = "CONT_END_DATE", unique = false, nullable = true, length = 10)
	private String contEndDate;

	/** 合同状态 **/
	@Column(name = "CONT_STATUS", unique = false, nullable = true, length = 5)
	private String contStatus;

	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;

	/** 合同余额 **/
	@Column(name = "CONT_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contBalance;

	/** 合同汇率 **/
	@Column(name = "CONT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contRate;

	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;

	/** 结息方式 **/
	@Column(name = "EI_MODE", unique = false, nullable = true, length = 5)
	private String eiMode;

	/** 结息具体说明 **/
	@Column(name = "EI_MODE_EXPL", unique = false, nullable = true, length = 500)
	private String eiModeExpl;

	/** 地址 **/
	@Column(name = "ADDR", unique = false, nullable = true, length = 500)
	private String addr;

	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 40)
	private String fax;

	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;

	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 50)
	private String email;

	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;

	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 20)
	private String wechat;

	/** 本行角色 **/
	@Column(name = "BANK_ROLE", unique = false, nullable = true, length = 40)
	private String bankRole;

	/** 银团总金额 **/
	@Column(name = "BKSYNDIC_TOTL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bksyndicTotlAmt;

	/** 银团纸质合同编号 **/
	@Column(name = "BKSYNDIC_PAPER_CONT_NO", unique = false, nullable = true, length = 40)
	private String bksyndicPaperContNo;

	/** 还款顺序 **/
	@Column(name = "REPAY_SEQ", unique = false, nullable = true, length = 40)
	private String repaySeq;

	/** 其他通讯方式及账号 **/
	@Column(name = "OTHER_PHONE", unique = false, nullable = true, length = 50)
	private String otherPhone;

	/** 签约方式 **/
	@Column(name = "SIGN_MODE", unique = false, nullable = true, length = 5)
	private String signMode;

	/** 签约状态 **/
	@Column(name = "SIGN_STATE", unique = false, nullable = true, length = 5)
	private String signState;

	/** 签约渠道 **/
	@Column(name = "SIGN_CHANNEL", unique = false, nullable = true, length = 5)
	private String signChannel;

	/** 保证金来源 **/
	@Column(name = "BAIL_SOUR", unique = false, nullable = true, length = 5)
	private String bailSour;

	/** 保证金汇率 **/
	@Column(name = "BAIL_EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailExchangeRate;

	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;

	/** 保证金币种 **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;

	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;

	/** 保证金折算人民币金额 **/
	@Column(name = "BAIL_CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailCvtCnyAmt;

	/** 折算人民币金额 **/
	@Column(name = "CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cvtCnyAmt;

	/** 期限类型 **/
	@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
	private String termType;

	/** 申请期限 **/
	@Column(name = "APP_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal appTerm;

	/** 利率依据方式 **/
	@Column(name = "IR_ACCORD_TYPE", unique = false, nullable = true, length = 5)
	private String irAccordType;

	/** 利率种类 **/
	@Column(name = "IR_TYPE", unique = false, nullable = true, length = 6)
	private String irType;

	/** 基准利率（年） **/
	@Column(name = "RULING_IR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIr;

	/** 对应基准利率(月) **/
	@Column(name = "RULING_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIrM;

	/** 计息方式 **/
	@Column(name = "LOAN_RAT_TYPE", unique = false, nullable = true, length = 5)
	private String loanRatType;

	/** 利率调整类型 **/
	@Column(name = "IR_ADJUST_TYPE", unique = false, nullable = true, length = 5)
	private String irAdjustType;

	/** 利率调整周期(月) **/
	@Column(name = "IR_ADJUST_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal irAdjustTerm;

	/** 调息方式 **/
	@Column(name = "PRA_TYPE", unique = false, nullable = true, length = 5)
	private String praType;

	/** 利率形式 **/
	@Column(name = "RATE_TYPE", unique = false, nullable = true, length = 5)
	private String rateType;

	/** LPR授信利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;

	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;

	/** 正常利率浮动方式 **/
	@Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String irFloatType;

	/** 利率浮动百分比 **/
	@Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal irFloatRate;

	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;

	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;

	/** 执行利率(月) **/
	@Column(name = "REALITY_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realityIrM;

	/** 逾期利率浮动比 **/
	@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRatePefloat;

	/** 逾期执行利率(年利率) **/
	@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueExecRate;

	/** 复息利率浮动比 **/
	@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciRatePefloat;

	/** 复息执行利率(年利率) **/
	@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciExecRate;

	/** 违约利率浮动百分比 **/
	@Column(name = "DEFAULT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRate;

	/** 违约利率(年) **/
	@Column(name = "DEFAULT_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRateY;

	/** 风险敞口金额 **/
	@Column(name = "RISK_OPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal riskOpenAmt;

	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;

	/** 停本付息期间 **/
	@Column(name = "STOP_PINT_TERM", unique = false, nullable = true, length = 5)
	private String stopPintTerm;

	/** 还款间隔周期 **/
	@Column(name = "REPAY_TERM", unique = false, nullable = true, length = 5)
	private String repayTerm;

	/** 还款间隔 **/
	@Column(name = "REPAY_SPACE", unique = false, nullable = true, length = 5)
	private String repaySpace;

	/** 还款日确定规则 **/
	@Column(name = "REPAY_RULE", unique = false, nullable = true, length = 5)
	private String repayRule;

	/** 还款日类型 **/
	@Column(name = "REPAY_DT_TYPE", unique = false, nullable = true, length = 5)
	private String repayDtType;

	/** 还款日 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 10)
	private String repayDate;

	/** 本金宽限方式 **/
	@Column(name = "CAP_GRAPER_TYPE", unique = false, nullable = true, length = 5)
	private String capGraperType;

	/** 本金宽限天数 **/
	@Column(name = "CAP_GRAPER_DAY", unique = false, nullable = true, length = 5)
	private String capGraperDay;

	/** 利息宽限方式 **/
	@Column(name = "INT_GRAPER_TYPE", unique = false, nullable = true, length = 5)
	private String intGraperType;

	/** 利息宽限天数 **/
	@Column(name = "INT_GRAPER_DAY", unique = false, nullable = true, length = 5)
	private String intGraperDay;

	/** 扣款扣息方式 **/
	@Column(name = "DEDUCT_DEDU_TYPE", unique = false, nullable = true, length = 5)
	private String deductDeduType;

	/** 还款频率类型 **/
	@Column(name = "REPAY_FRE_TYPE", unique = false, nullable = true, length = 5)
	private String repayFreType;

	/** 本息还款频率 **/
	@Column(name = "REPAY_FRE", unique = false, nullable = true, length = 10)
	private String repayFre;

	/** 提前还款违约金免除时间(月) **/
	@Column(name = "LIQU_FREE_TIME", unique = false, nullable = true, length = 10)
	private Integer liquFreeTime;

	/** 分段方式 **/
	@Column(name = "SUB_TYPE", unique = false, nullable = true, length = 5)
	private String subType;

	/** 保留期限 **/
	@Column(name = "RESERVE_TERM", unique = false, nullable = true, length = 10)
	private Integer reserveTerm;

	/** 计算期限 **/
	@Column(name = "CAL_TERM", unique = false, nullable = true, length = 10)
	private Integer calTerm;

	/** 保留金额 **/
	@Column(name = "RESERVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal reserveAmt;

	/** 第一阶段还款期数 **/
	@Column(name = "REPAY_TERM_ONE", unique = false, nullable = true, length = 10)
	private Integer repayTermOne;

	/** 第一阶段还款本金 **/
	@Column(name = "REPAY_AMT_ONE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmtOne;

	/** 第二阶段还款期数 **/
	@Column(name = "REPAY_TERM_TWO", unique = false, nullable = true, length = 10)
	private Integer repayTermTwo;

	/** 第二阶段还款本金 **/
	@Column(name = "REPAY_AMT_TWO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmtTwo;

	/** 利率选取日期种类 **/
	@Column(name = "RATE_SEL_TYPE", unique = false, nullable = true, length = 5)
	private String rateSelType;

	/** 贴息方式 **/
	@Column(name = "SBSY_MODE", unique = false, nullable = true, length = 5)
	private String sbsyMode;

	/** 贴息比例 **/
	@Column(name = "SBSY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyPerc;

	/** 贴息金额 **/
	@Column(name = "SBSY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyAmt;

	/** 贴息单位名称 **/
	@Column(name = "SBSY_UNIT_NAME", unique = false, nullable = true, length = 80)
	private String sbsyUnitName;

	/** 贴息方账户 **/
	@Column(name = "SBSY_ACCT", unique = false, nullable = true, length = 40)
	private String sbsyAcct;

	/** 贴息方账户户名 **/
	@Column(name = "SBSY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String sbsyAcctName;

	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;

	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 100)
	private String loanTer;

	/** 工业转型升级标识 **/
	@Column(name = "COM_UP_INDTIFY", unique = false, nullable = true, length = 5)
	private String comUpIndtify;

	/** 战略新兴产业类型 **/
	@Column(name = "STRATEGY_NEW_LOAN", unique = false, nullable = true, length = 5)
	private String strategyNewLoan;

	/** 是否文化产业 **/
	@Column(name = "IS_CUL_ESTATE", unique = false, nullable = true, length = 5)
	private String isCulEstate;

	/** 贷款种类 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 20)
	private String loanType;

	/** 产业结构调整类型 **/
	@Column(name = "ESTATE_ADJUST_TYPE", unique = false, nullable = true, length = 5)
	private String estateAdjustType;

	/** 新兴产业贷款 **/
	@Column(name = "NEW_PRD_LOAN", unique = false, nullable = true, length = 5)
	private String newPrdLoan;

	/** 还款来源 **/
	@Column(name = "REPAY_SOUR", unique = false, nullable = true, length = 2000)
	private String repaySour;

	/** 是否委托人办理 **/
	@Column(name = "IS_AUTHORIZE", unique = false, nullable = true, length = 5)
	private String isAuthorize;

	/** 委托人姓名 **/
	@Column(name = "AUTHED_NAME", unique = false, nullable = true, length = 30)
	private String authedName;

	/** 委托人证件类型 **/
	@Column(name = "CONSIGNOR_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String consignorCertType;

	/** 委托人证件号 **/
	@Column(name = "CONSIGNOR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String consignorCertCode;

	/** 委托人联系方式 **/
	@Column(name = "AUTHED_TEL_NO", unique = false, nullable = true, length = 20)
	private String authedTelNo;

	/** 签订日期 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 10)
	private String signDate;

	/** 注销日期 **/
	@Column(name = "LOGOUT_DATE", unique = false, nullable = true, length = 10)
	private String logoutDate;

	/** 渠道来源 **/
	@Column(name = "CHNL_SOUR", unique = false, nullable = true, length = 5)
	private String chnlSour;

	/** 争议解决方式 **/
	@Column(name = "BILL_DISPUPE_OPT", unique = false, nullable = true, length = 5)
	private String billDispupeOpt;

	/** 法院所在地 **/
	@Column(name = "COURT_ADDR", unique = false, nullable = true, length = 200)
	private String courtAddr;

	/** 仲裁委员会 **/
	@Column(name = "ARBITRATE_BCH", unique = false, nullable = true, length = 400)
	private String arbitrateBch;

	/** 仲裁委员会地点 **/
	@Column(name = "ARBITRATE_ADDR", unique = false, nullable = true, length = 200)
	private String arbitrateAddr;

	/** 用途分析 **/
	@Column(name = "PURP_ANALY", unique = false, nullable = true, length = 2000)
	private String purpAnaly;

	/** 交叉核验详细分析 **/
	@Column(name = "CROSS_CHK_DETAIL_ANALY", unique = false, nullable = true, length = 500)
	private String crossChkDetailAnaly;

	/** 调查人结论 **/
	@Column(name = "INVE_CONCLU", unique = false, nullable = true, length = 2000)
	private String inveConclu;

	/** 其他方式 **/
	@Column(name = "OTHER_OPT", unique = false, nullable = true, length = 200)
	private String otherOpt;

	/** 合同份数 **/
	@Column(name = "CONT_QNT", unique = false, nullable = true, length = 2)
	private java.math.BigDecimal contQnt;

	/** 公积金贷款合同编号 **/
	@Column(name = "PUND_CONT_NO", unique = false, nullable = true, length = 40)
	private String pundContNo;

	/** 补充条款 **/
	@Column(name = "SPPL_CLAUSE", unique = false, nullable = true, length = 2000)
	private String spplClause;

	/** 签约地点 **/
	@Column(name = "SIGN_ADDR", unique = false, nullable = true, length = 200)
	private String signAddr;

	/** 营业网点 **/
	@Column(name = "BUSI_NETWORK", unique = false, nullable = true, length = 5)
	private String busiNetwork;

	/** 主要营业场所地址 **/
	@Column(name = "MAIN_BUSI_PALCE", unique = false, nullable = true, length = 200)
	private String mainBusiPalce;

	/** 合同模板 **/
	@Column(name = "CONT_TEMPLATE", unique = false, nullable = true, length = 5)
	private String contTemplate;

	/** 合同打印次数 **/
	@Column(name = "CONT_PRINT_NUM", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal contPrintNum;

	/** 签章审批状态 **/
	@Column(name = "SIGN_APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String signApproveStatus;

	/** 所属团队 **/
	@Column(name = "TEAM", unique = false, nullable = true, length = 10)
	private String team;

	/** 其他约定 **/
	@Column(name = "OTHER_AGREED", unique = false, nullable = true, length = 2000)
	private String otherAgreed;

	/** 开户行名称 **/
	@Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 20)
	private String acctsvcrName;

	/** 贷款发放账号 **/
	@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 40)
	private String loanPayoutAccno;

	/** 贷款发放账号名称 **/
	@Column(name = "LOAN_PAYOUT_ACC_NAME", unique = false, nullable = true, length = 80)
	private String loanPayoutAccName;

	/** 合同模式 **/
	@Column(name = "CONT_MODE", unique = false, nullable = true, length = 5)
	private String contMode;

	/** 是否线上提款 **/
	@Column(name = "IS_ONLINE_DRAW", unique = false, nullable = true, length = 1)
	private String isOnlineDraw;

	/** 线上合同启用标识 **/
	@Column(name = "CTR_BEGIN_FLAG", unique = false, nullable = true, length = 2)
	private String ctrBeginFlag;

	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 本合同项下最高可用信金额 **/
	@Column(name = "HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal highAvlAmt;

	/** 是否续签 **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;

	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;

	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;

	/** 是否在线抵押 **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 5)
	private String isOlPld;

	/** 是否先放款后抵押 **/
	@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
	private String beforehandInd;

	/** 是否电子用印 **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;

	/** 是否无缝对接 **/
	@Column(name = "IS_SEAJNT", unique = false, nullable = true, length = 5)
	private String isSeajnt;

	/** 双录编号 **/
	@Column(name = "DOUBLE_RECORD_NO", unique = false, nullable = true, length = 40)
	private String doubleRecordNo;

	/** 借款利率调整日 **/
	@Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 5)
	private String loanRateAdjDay;

	/** 提款方式 **/
	@Column(name = "DRAW_MODE", unique = false, nullable = true, length = 10)
	private String drawMode;

	/** 提款天数限制 **/
	@Column(name = "DAY_LIMIT", unique = false, nullable = true, length = 10)
	private String dayLimit;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 40)
	private String belgLine;

	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;

	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;

	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;

	/** 其他借款用途 **/
	@Column(name = "OTHER_LOAN_PURP", unique = false, nullable = true, length = 200)
	private String otherLoanPurp;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="managerIdName")
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="managerBrIdName" )
	private String managerBrId;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 2)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="inputIdName")
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="inputBrIdName" )
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}

	/**
	 * @return contCnNo
	 */
	public String getContCnNo() {
		return this.contCnNo;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}

	/**
	 * @return surveySerno
	 */
	public String getSurveySerno() {
		return this.surveySerno;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param isOutstndTrdLmtAmt
	 */
	public void setIsOutstndTrdLmtAmt(String isOutstndTrdLmtAmt) {
		this.isOutstndTrdLmtAmt = isOutstndTrdLmtAmt;
	}

	/**
	 * @return isOutstndTrdLmtAmt
	 */
	public String getIsOutstndTrdLmtAmt() {
		return this.isOutstndTrdLmtAmt;
	}

	/**
	 * @param tdpAgrNo
	 */
	public void setTdpAgrNo(String tdpAgrNo) {
		this.tdpAgrNo = tdpAgrNo;
	}

	/**
	 * @return tdpAgrNo
	 */
	public String getTdpAgrNo() {
		return this.tdpAgrNo;
	}

	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId;
	}

	/**
	 * @return coopCusId
	 */
	public String getCoopCusId() {
		return this.coopCusId;
	}

	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName;
	}

	/**
	 * @return coopCusName
	 */
	public String getCoopCusName() {
		return this.coopCusName;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	/**
	 * @return iqpSerno
	 */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}

	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}

	/**
	 * @return certType
	 */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	/**
	 * @return certCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param especBizType
	 */
	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType;
	}

	/**
	 * @return especBizType
	 */
	public String getEspecBizType() {
		return this.especBizType;
	}

	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}

	/**
	 * @return loanPurp
	 */
	public String getLoanPurp() {
		return this.loanPurp;
	}

	/**
	 * @param loanPurpDesc
	 */
	public void setLoanPurpDesc(String loanPurpDesc) {
		this.loanPurpDesc = loanPurpDesc;
	}

	/**
	 * @return loanPurpDesc
	 */
	public String getLoanPurpDesc() {
		return this.loanPurpDesc;
	}

	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	/**
	 * @return loanModal
	 */
	public String getLoanModal() {
		return this.loanModal;
	}

	/**
	 * @param loanCha
	 */
	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha;
	}

	/**
	 * @return loanCha
	 */
	public String getLoanCha() {
		return this.loanCha;
	}

	/**
	 * @param isHasRefused
	 */
	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused;
	}

	/**
	 * @return isHasRefused
	 */
	public String getIsHasRefused() {
		return this.isHasRefused;
	}

	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}

	/**
	 * @return guarWay
	 */
	public String getGuarWay() {
		return this.guarWay;
	}

	/**
	 * @param isCommonRqstr
	 */
	public void setIsCommonRqstr(String isCommonRqstr) {
		this.isCommonRqstr = isCommonRqstr;
	}

	/**
	 * @return isCommonRqstr
	 */
	public String getIsCommonRqstr() {
		return this.isCommonRqstr;
	}

	/**
	 * @param isCfirmPayWay
	 */
	public void setIsCfirmPayWay(String isCfirmPayWay) {
		this.isCfirmPayWay = isCfirmPayWay;
	}

	/**
	 * @return isCfirmPayWay
	 */
	public String getIsCfirmPayWay() {
		return this.isCfirmPayWay;
	}

	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	/**
	 * @return payMode
	 */
	public String getPayMode() {
		return this.payMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}

	/**
	 * @return contType
	 */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}

	/**
	 * @return contTerm
	 */
	public Integer getContTerm() {
		return this.contTerm;
	}

	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}

	/**
	 * @return contStartDate
	 */
	public String getContStartDate() {
		return this.contStartDate;
	}

	/**
	 * @param contEndDate
	 */
	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate;
	}

	/**
	 * @return contEndDate
	 */
	public String getContEndDate() {
		return this.contEndDate;
	}

	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	/**
	 * @return contStatus
	 */
	public String getContStatus() {
		return this.contStatus;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return contAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contBalance
	 */
	public void setContBalance(java.math.BigDecimal contBalance) {
		this.contBalance = contBalance;
	}

	/**
	 * @return contBalance
	 */
	public java.math.BigDecimal getContBalance() {
		return this.contBalance;
	}

	/**
	 * @param contRate
	 */
	public void setContRate(java.math.BigDecimal contRate) {
		this.contRate = contRate;
	}

	/**
	 * @return contRate
	 */
	public java.math.BigDecimal getContRate() {
		return this.contRate;
	}

	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}

	/**
	 * @return paperContSignDate
	 */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}

	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode;
	}

	/**
	 * @return eiMode
	 */
	public String getEiMode() {
		return this.eiMode;
	}

	/**
	 * @param eiModeExpl
	 */
	public void setEiModeExpl(String eiModeExpl) {
		this.eiModeExpl = eiModeExpl;
	}

	/**
	 * @return eiModeExpl
	 */
	public String getEiModeExpl() {
		return this.eiModeExpl;
	}

	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}

	/**
	 * @return addr
	 */
	public String getAddr() {
		return this.addr;
	}

	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return fax
	 */
	public String getFax() {
		return this.fax;
	}

	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	/**
	 * @return linkman
	 */
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * @return qq
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	/**
	 * @return wechat
	 */
	public String getWechat() {
		return this.wechat;
	}

	/**
	 * @param bankRole
	 */
	public void setBankRole(String bankRole) {
		this.bankRole = bankRole;
	}

	/**
	 * @return bankRole
	 */
	public String getBankRole() {
		return this.bankRole;
	}

	/**
	 * @param bksyndicTotlAmt
	 */
	public void setBksyndicTotlAmt(java.math.BigDecimal bksyndicTotlAmt) {
		this.bksyndicTotlAmt = bksyndicTotlAmt;
	}

	/**
	 * @return bksyndicTotlAmt
	 */
	public java.math.BigDecimal getBksyndicTotlAmt() {
		return this.bksyndicTotlAmt;
	}

	/**
	 * @param bksyndicPaperContNo
	 */
	public void setBksyndicPaperContNo(String bksyndicPaperContNo) {
		this.bksyndicPaperContNo = bksyndicPaperContNo;
	}

	/**
	 * @return bksyndicPaperContNo
	 */
	public String getBksyndicPaperContNo() {
		return this.bksyndicPaperContNo;
	}

	/**
	 * @param repaySeq
	 */
	public void setRepaySeq(String repaySeq) {
		this.repaySeq = repaySeq;
	}

	/**
	 * @return repaySeq
	 */
	public String getRepaySeq() {
		return this.repaySeq;
	}

	/**
	 * @param otherPhone
	 */
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	/**
	 * @return otherPhone
	 */
	public String getOtherPhone() {
		return this.otherPhone;
	}

	/**
	 * @param signMode
	 */
	public void setSignMode(String signMode) {
		this.signMode = signMode;
	}

	/**
	 * @return signMode
	 */
	public String getSignMode() {
		return this.signMode;
	}

	/**
	 * @param signState
	 */
	public void setSignState(String signState) {
		this.signState = signState;
	}

	/**
	 * @return signState
	 */
	public String getSignState() {
		return this.signState;
	}

	/**
	 * @param signChannel
	 */
	public void setSignChannel(String signChannel) {
		this.signChannel = signChannel;
	}

	/**
	 * @return signChannel
	 */
	public String getSignChannel() {
		return this.signChannel;
	}

	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour;
	}

	/**
	 * @return bailSour
	 */
	public String getBailSour() {
		return this.bailSour;
	}

	/**
	 * @param bailExchangeRate
	 */
	public void setBailExchangeRate(java.math.BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}

	/**
	 * @return bailExchangeRate
	 */
	public java.math.BigDecimal getBailExchangeRate() {
		return this.bailExchangeRate;
	}

	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}

	/**
	 * @return bailPerc
	 */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}

	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}

	/**
	 * @return bailCurType
	 */
	public String getBailCurType() {
		return this.bailCurType;
	}

	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	/**
	 * @return bailAmt
	 */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}

	/**
	 * @param bailCvtCnyAmt
	 */
	public void setBailCvtCnyAmt(java.math.BigDecimal bailCvtCnyAmt) {
		this.bailCvtCnyAmt = bailCvtCnyAmt;
	}

	/**
	 * @return bailCvtCnyAmt
	 */
	public java.math.BigDecimal getBailCvtCnyAmt() {
		return this.bailCvtCnyAmt;
	}

	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}

	/**
	 * @return cvtCnyAmt
	 */
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}

	/**
	 * @return termType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param appTerm
	 */
	public void setAppTerm(java.math.BigDecimal appTerm) {
		this.appTerm = appTerm;
	}

	/**
	 * @return appTerm
	 */
	public java.math.BigDecimal getAppTerm() {
		return this.appTerm;
	}

	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType;
	}

	/**
	 * @return irAccordType
	 */
	public String getIrAccordType() {
		return this.irAccordType;
	}

	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType;
	}

	/**
	 * @return irType
	 */
	public String getIrType() {
		return this.irType;
	}

	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

	/**
	 * @return rulingIr
	 */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}

	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}

	/**
	 * @return rulingIrM
	 */
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}

	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType;
	}

	/**
	 * @return loanRatType
	 */
	public String getLoanRatType() {
		return this.loanRatType;
	}

	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType;
	}

	/**
	 * @return irAdjustType
	 */
	public String getIrAdjustType() {
		return this.irAdjustType;
	}

	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(java.math.BigDecimal irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}

	/**
	 * @return irAdjustTerm
	 */
	public java.math.BigDecimal getIrAdjustTerm() {
		return this.irAdjustTerm;
	}

	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType;
	}

	/**
	 * @return praType
	 */
	public String getPraType() {
		return this.praType;
	}

	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	/**
	 * @return rateType
	 */
	public String getRateType() {
		return this.rateType;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}

	/**
	 * @return lprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	/**
	 * @return curtLprRate
	 */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}

	/**
	 * @return irFloatType
	 */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	/**
	 * @return irFloatRate
	 */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return rateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return execRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param realityIrM
	 */
	public void setRealityIrM(java.math.BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}

	/**
	 * @return realityIrM
	 */
	public java.math.BigDecimal getRealityIrM() {
		return this.realityIrM;
	}

	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}

	/**
	 * @return overdueRatePefloat
	 */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}

	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}

	/**
	 * @return overdueExecRate
	 */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}

	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}

	/**
	 * @return ciRatePefloat
	 */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}

	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}

	/**
	 * @return ciExecRate
	 */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}

	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

	/**
	 * @return defaultRate
	 */
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}

	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}

	/**
	 * @return defaultRateY
	 */
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}

	/**
	 * @param riskOpenAmt
	 */
	public void setRiskOpenAmt(java.math.BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}

	/**
	 * @return riskOpenAmt
	 */
	public java.math.BigDecimal getRiskOpenAmt() {
		return this.riskOpenAmt;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}

	/**
	 * @return repayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param stopPintTerm
	 */
	public void setStopPintTerm(String stopPintTerm) {
		this.stopPintTerm = stopPintTerm;
	}

	/**
	 * @return stopPintTerm
	 */
	public String getStopPintTerm() {
		return this.stopPintTerm;
	}

	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm;
	}

	/**
	 * @return repayTerm
	 */
	public String getRepayTerm() {
		return this.repayTerm;
	}

	/**
	 * @param repaySpace
	 */
	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace;
	}

	/**
	 * @return repaySpace
	 */
	public String getRepaySpace() {
		return this.repaySpace;
	}

	/**
	 * @param repayRule
	 */
	public void setRepayRule(String repayRule) {
		this.repayRule = repayRule;
	}

	/**
	 * @return repayRule
	 */
	public String getRepayRule() {
		return this.repayRule;
	}

	/**
	 * @param repayDtType
	 */
	public void setRepayDtType(String repayDtType) {
		this.repayDtType = repayDtType;
	}

	/**
	 * @return repayDtType
	 */
	public String getRepayDtType() {
		return this.repayDtType;
	}

	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}

	/**
	 * @return repayDate
	 */
	public String getRepayDate() {
		return this.repayDate;
	}

	/**
	 * @param capGraperType
	 */
	public void setCapGraperType(String capGraperType) {
		this.capGraperType = capGraperType;
	}

	/**
	 * @return capGraperType
	 */
	public String getCapGraperType() {
		return this.capGraperType;
	}

	/**
	 * @param capGraperDay
	 */
	public void setCapGraperDay(String capGraperDay) {
		this.capGraperDay = capGraperDay;
	}

	/**
	 * @return capGraperDay
	 */
	public String getCapGraperDay() {
		return this.capGraperDay;
	}

	/**
	 * @param intGraperType
	 */
	public void setIntGraperType(String intGraperType) {
		this.intGraperType = intGraperType;
	}

	/**
	 * @return intGraperType
	 */
	public String getIntGraperType() {
		return this.intGraperType;
	}

	/**
	 * @param intGraperDay
	 */
	public void setIntGraperDay(String intGraperDay) {
		this.intGraperDay = intGraperDay;
	}

	/**
	 * @return intGraperDay
	 */
	public String getIntGraperDay() {
		return this.intGraperDay;
	}

	/**
	 * @param deductDeduType
	 */
	public void setDeductDeduType(String deductDeduType) {
		this.deductDeduType = deductDeduType;
	}

	/**
	 * @return deductDeduType
	 */
	public String getDeductDeduType() {
		return this.deductDeduType;
	}

	/**
	 * @param repayFreType
	 */
	public void setRepayFreType(String repayFreType) {
		this.repayFreType = repayFreType;
	}

	/**
	 * @return repayFreType
	 */
	public String getRepayFreType() {
		return this.repayFreType;
	}

	/**
	 * @param repayFre
	 */
	public void setRepayFre(String repayFre) {
		this.repayFre = repayFre;
	}

	/**
	 * @return repayFre
	 */
	public String getRepayFre() {
		return this.repayFre;
	}

	/**
	 * @param liquFreeTime
	 */
	public void setLiquFreeTime(Integer liquFreeTime) {
		this.liquFreeTime = liquFreeTime;
	}

	/**
	 * @return liquFreeTime
	 */
	public Integer getLiquFreeTime() {
		return this.liquFreeTime;
	}

	/**
	 * @param subType
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return subType
	 */
	public String getSubType() {
		return this.subType;
	}

	/**
	 * @param reserveTerm
	 */
	public void setReserveTerm(Integer reserveTerm) {
		this.reserveTerm = reserveTerm;
	}

	/**
	 * @return reserveTerm
	 */
	public Integer getReserveTerm() {
		return this.reserveTerm;
	}

	/**
	 * @param calTerm
	 */
	public void setCalTerm(Integer calTerm) {
		this.calTerm = calTerm;
	}

	/**
	 * @return calTerm
	 */
	public Integer getCalTerm() {
		return this.calTerm;
	}

	/**
	 * @param reserveAmt
	 */
	public void setReserveAmt(java.math.BigDecimal reserveAmt) {
		this.reserveAmt = reserveAmt;
	}

	/**
	 * @return reserveAmt
	 */
	public java.math.BigDecimal getReserveAmt() {
		return this.reserveAmt;
	}

	/**
	 * @param repayTermOne
	 */
	public void setRepayTermOne(Integer repayTermOne) {
		this.repayTermOne = repayTermOne;
	}

	/**
	 * @return repayTermOne
	 */
	public Integer getRepayTermOne() {
		return this.repayTermOne;
	}

	/**
	 * @param repayAmtOne
	 */
	public void setRepayAmtOne(java.math.BigDecimal repayAmtOne) {
		this.repayAmtOne = repayAmtOne;
	}

	/**
	 * @return repayAmtOne
	 */
	public java.math.BigDecimal getRepayAmtOne() {
		return this.repayAmtOne;
	}

	/**
	 * @param repayTermTwo
	 */
	public void setRepayTermTwo(Integer repayTermTwo) {
		this.repayTermTwo = repayTermTwo;
	}

	/**
	 * @return repayTermTwo
	 */
	public Integer getRepayTermTwo() {
		return this.repayTermTwo;
	}

	/**
	 * @param repayAmtTwo
	 */
	public void setRepayAmtTwo(java.math.BigDecimal repayAmtTwo) {
		this.repayAmtTwo = repayAmtTwo;
	}

	/**
	 * @return repayAmtTwo
	 */
	public java.math.BigDecimal getRepayAmtTwo() {
		return this.repayAmtTwo;
	}

	/**
	 * @param rateSelType
	 */
	public void setRateSelType(String rateSelType) {
		this.rateSelType = rateSelType;
	}

	/**
	 * @return rateSelType
	 */
	public String getRateSelType() {
		return this.rateSelType;
	}

	/**
	 * @param sbsyMode
	 */
	public void setSbsyMode(String sbsyMode) {
		this.sbsyMode = sbsyMode;
	}

	/**
	 * @return sbsyMode
	 */
	public String getSbsyMode() {
		return this.sbsyMode;
	}

	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

	/**
	 * @return sbsyPerc
	 */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}

	/**
	 * @param sbsyAmt
	 */
	public void setSbsyAmt(java.math.BigDecimal sbsyAmt) {
		this.sbsyAmt = sbsyAmt;
	}

	/**
	 * @return sbsyAmt
	 */
	public java.math.BigDecimal getSbsyAmt() {
		return this.sbsyAmt;
	}

	/**
	 * @param sbsyUnitName
	 */
	public void setSbsyUnitName(String sbsyUnitName) {
		this.sbsyUnitName = sbsyUnitName;
	}

	/**
	 * @return sbsyUnitName
	 */
	public String getSbsyUnitName() {
		return this.sbsyUnitName;
	}

	/**
	 * @param sbsyAcct
	 */
	public void setSbsyAcct(String sbsyAcct) {
		this.sbsyAcct = sbsyAcct;
	}

	/**
	 * @return sbsyAcct
	 */
	public String getSbsyAcct() {
		return this.sbsyAcct;
	}

	/**
	 * @param sbsyAcctName
	 */
	public void setSbsyAcctName(String sbsyAcctName) {
		this.sbsyAcctName = sbsyAcctName;
	}

	/**
	 * @return sbsyAcctName
	 */
	public String getSbsyAcctName() {
		return this.sbsyAcctName;
	}

	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	/**
	 * @return fiveClass
	 */
	public String getFiveClass() {
		return this.fiveClass;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}

	/**
	 * @return loanTer
	 */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param comUpIndtify
	 */
	public void setComUpIndtify(String comUpIndtify) {
		this.comUpIndtify = comUpIndtify;
	}

	/**
	 * @return comUpIndtify
	 */
	public String getComUpIndtify() {
		return this.comUpIndtify;
	}

	/**
	 * @param strategyNewLoan
	 */
	public void setStrategyNewLoan(String strategyNewLoan) {
		this.strategyNewLoan = strategyNewLoan;
	}

	/**
	 * @return strategyNewLoan
	 */
	public String getStrategyNewLoan() {
		return this.strategyNewLoan;
	}

	/**
	 * @param isCulEstate
	 */
	public void setIsCulEstate(String isCulEstate) {
		this.isCulEstate = isCulEstate;
	}

	/**
	 * @return isCulEstate
	 */
	public String getIsCulEstate() {
		return this.isCulEstate;
	}

	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	/**
	 * @return loanType
	 */
	public String getLoanType() {
		return this.loanType;
	}

	/**
	 * @param estateAdjustType
	 */
	public void setEstateAdjustType(String estateAdjustType) {
		this.estateAdjustType = estateAdjustType;
	}

	/**
	 * @return estateAdjustType
	 */
	public String getEstateAdjustType() {
		return this.estateAdjustType;
	}

	/**
	 * @param newPrdLoan
	 */
	public void setNewPrdLoan(String newPrdLoan) {
		this.newPrdLoan = newPrdLoan;
	}

	/**
	 * @return newPrdLoan
	 */
	public String getNewPrdLoan() {
		return this.newPrdLoan;
	}

	/**
	 * @param repaySour
	 */
	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour;
	}

	/**
	 * @return repaySour
	 */
	public String getRepaySour() {
		return this.repaySour;
	}

	/**
	 * @param isAuthorize
	 */
	public void setIsAuthorize(String isAuthorize) {
		this.isAuthorize = isAuthorize;
	}

	/**
	 * @return isAuthorize
	 */
	public String getIsAuthorize() {
		return this.isAuthorize;
	}

	/**
	 * @param authedName
	 */
	public void setAuthedName(String authedName) {
		this.authedName = authedName;
	}

	/**
	 * @return authedName
	 */
	public String getAuthedName() {
		return this.authedName;
	}

	/**
	 * @param consignorCertType
	 */
	public void setConsignorCertType(String consignorCertType) {
		this.consignorCertType = consignorCertType;
	}

	/**
	 * @return consignorCertType
	 */
	public String getConsignorCertType() {
		return this.consignorCertType;
	}

	/**
	 * @param consignorCertCode
	 */
	public void setConsignorCertCode(String consignorCertCode) {
		this.consignorCertCode = consignorCertCode;
	}

	/**
	 * @return consignorCertCode
	 */
	public String getConsignorCertCode() {
		return this.consignorCertCode;
	}

	/**
	 * @param authedTelNo
	 */
	public void setAuthedTelNo(String authedTelNo) {
		this.authedTelNo = authedTelNo;
	}

	/**
	 * @return authedTelNo
	 */
	public String getAuthedTelNo() {
		return this.authedTelNo;
	}

	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	/**
	 * @return signDate
	 */
	public String getSignDate() {
		return this.signDate;
	}

	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	/**
	 * @return logoutDate
	 */
	public String getLogoutDate() {
		return this.logoutDate;
	}

	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour;
	}

	/**
	 * @return chnlSour
	 */
	public String getChnlSour() {
		return this.chnlSour;
	}

	/**
	 * @param billDispupeOpt
	 */
	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt;
	}

	/**
	 * @return billDispupeOpt
	 */
	public String getBillDispupeOpt() {
		return this.billDispupeOpt;
	}

	/**
	 * @param courtAddr
	 */
	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr;
	}

	/**
	 * @return courtAddr
	 */
	public String getCourtAddr() {
		return this.courtAddr;
	}

	/**
	 * @param arbitrateBch
	 */
	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch;
	}

	/**
	 * @return arbitrateBch
	 */
	public String getArbitrateBch() {
		return this.arbitrateBch;
	}

	/**
	 * @param arbitrateAddr
	 */
	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr;
	}

	/**
	 * @return arbitrateAddr
	 */
	public String getArbitrateAddr() {
		return this.arbitrateAddr;
	}

	/**
	 * @param purpAnaly
	 */
	public void setPurpAnaly(String purpAnaly) {
		this.purpAnaly = purpAnaly;
	}

	/**
	 * @return purpAnaly
	 */
	public String getPurpAnaly() {
		return this.purpAnaly;
	}

	/**
	 * @param crossChkDetailAnaly
	 */
	public void setCrossChkDetailAnaly(String crossChkDetailAnaly) {
		this.crossChkDetailAnaly = crossChkDetailAnaly;
	}

	/**
	 * @return crossChkDetailAnaly
	 */
	public String getCrossChkDetailAnaly() {
		return this.crossChkDetailAnaly;
	}

	/**
	 * @param inveConclu
	 */
	public void setInveConclu(String inveConclu) {
		this.inveConclu = inveConclu;
	}

	/**
	 * @return inveConclu
	 */
	public String getInveConclu() {
		return this.inveConclu;
	}

	/**
	 * @param otherOpt
	 */
	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt;
	}

	/**
	 * @return otherOpt
	 */
	public String getOtherOpt() {
		return this.otherOpt;
	}

	/**
	 * @param contQnt
	 */
	public void setContQnt(java.math.BigDecimal contQnt) {
		this.contQnt = contQnt;
	}

	/**
	 * @return contQnt
	 */
	public java.math.BigDecimal getContQnt() {
		return this.contQnt;
	}

	/**
	 * @param pundContNo
	 */
	public void setPundContNo(String pundContNo) {
		this.pundContNo = pundContNo;
	}

	/**
	 * @return pundContNo
	 */
	public String getPundContNo() {
		return this.pundContNo;
	}

	/**
	 * @param spplClause
	 */
	public void setSpplClause(String spplClause) {
		this.spplClause = spplClause;
	}

	/**
	 * @return spplClause
	 */
	public String getSpplClause() {
		return this.spplClause;
	}

	/**
	 * @param signAddr
	 */
	public void setSignAddr(String signAddr) {
		this.signAddr = signAddr;
	}

	/**
	 * @return signAddr
	 */
	public String getSignAddr() {
		return this.signAddr;
	}

	/**
	 * @param busiNetwork
	 */
	public void setBusiNetwork(String busiNetwork) {
		this.busiNetwork = busiNetwork;
	}

	/**
	 * @return busiNetwork
	 */
	public String getBusiNetwork() {
		return this.busiNetwork;
	}

	/**
	 * @param mainBusiPalce
	 */
	public void setMainBusiPalce(String mainBusiPalce) {
		this.mainBusiPalce = mainBusiPalce;
	}

	/**
	 * @return mainBusiPalce
	 */
	public String getMainBusiPalce() {
		return this.mainBusiPalce;
	}

	/**
	 * @param contTemplate
	 */
	public void setContTemplate(String contTemplate) {
		this.contTemplate = contTemplate;
	}

	/**
	 * @return contTemplate
	 */
	public String getContTemplate() {
		return this.contTemplate;
	}

	/**
	 * @param contPrintNum
	 */
	public void setContPrintNum(java.math.BigDecimal contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	/**
	 * @return contPrintNum
	 */
	public java.math.BigDecimal getContPrintNum() {
		return this.contPrintNum;
	}

	/**
	 * @param signApproveStatus
	 */
	public void setSignApproveStatus(String signApproveStatus) {
		this.signApproveStatus = signApproveStatus;
	}

	/**
	 * @return signApproveStatus
	 */
	public String getSignApproveStatus() {
		return this.signApproveStatus;
	}

	/**
	 * @param team
	 */
	public void setTeam(String team) {
		this.team = team;
	}

	/**
	 * @return team
	 */
	public String getTeam() {
		return this.team;
	}

	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}

	/**
	 * @return otherAgreed
	 */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}

	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}

	/**
	 * @return acctsvcrName
	 */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}

	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}

	/**
	 * @return loanPayoutAccno
	 */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}

	/**
	 * @param loanPayoutAccName
	 */
	public void setLoanPayoutAccName(String loanPayoutAccName) {
		this.loanPayoutAccName = loanPayoutAccName;
	}

	/**
	 * @return loanPayoutAccName
	 */
	public String getLoanPayoutAccName() {
		return this.loanPayoutAccName;
	}

	/**
	 * @param contMode
	 */
	public void setContMode(String contMode) {
		this.contMode = contMode;
	}

	/**
	 * @return contMode
	 */
	public String getContMode() {
		return this.contMode;
	}

	/**
	 * @param isOnlineDraw
	 */
	public void setIsOnlineDraw(String isOnlineDraw) {
		this.isOnlineDraw = isOnlineDraw;
	}

	/**
	 * @return isOnlineDraw
	 */
	public String getIsOnlineDraw() {
		return this.isOnlineDraw;
	}

	/**
	 * @param ctrBeginFlag
	 */
	public void setCtrBeginFlag(String ctrBeginFlag) {
		this.ctrBeginFlag = ctrBeginFlag;
	}

	/**
	 * @return ctrBeginFlag
	 */
	public String getCtrBeginFlag() {
		return this.ctrBeginFlag;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param highAvlAmt
	 */
	public void setHighAvlAmt(java.math.BigDecimal highAvlAmt) {
		this.highAvlAmt = highAvlAmt;
	}

	/**
	 * @return highAvlAmt
	 */
	public java.math.BigDecimal getHighAvlAmt() {
		return this.highAvlAmt;
	}

	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}

	/**
	 * @return isRenew
	 */
	public String getIsRenew() {
		return this.isRenew;
	}

	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}

	/**
	 * @return origiContNo
	 */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}

	/**
	 * @return isOlPld
	 */
	public String getIsOlPld() {
		return this.isOlPld;
	}

	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}

	/**
	 * @return beforehandInd
	 */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}

	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}

	/**
	 * @return isESeal
	 */
	public String getIsESeal() {
		return this.isESeal;
	}

	/**
	 * @param isSeajnt
	 */
	public void setIsSeajnt(String isSeajnt) {
		this.isSeajnt = isSeajnt;
	}

	/**
	 * @return isSeajnt
	 */
	public String getIsSeajnt() {
		return this.isSeajnt;
	}

	/**
	 * @param doubleRecordNo
	 */
	public void setDoubleRecordNo(String doubleRecordNo) {
		this.doubleRecordNo = doubleRecordNo;
	}

	/**
	 * @return doubleRecordNo
	 */
	public String getDoubleRecordNo() {
		return this.doubleRecordNo;
	}

	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay;
	}

	/**
	 * @return loanRateAdjDay
	 */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}

	/**
	 * @param drawMode
	 */
	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode;
	}

	/**
	 * @return drawMode
	 */
	public String getDrawMode() {
		return this.drawMode;
	}

	/**
	 * @param dayLimit
	 */
	public void setDayLimit(String dayLimit) {
		this.dayLimit = dayLimit;
	}

	/**
	 * @return dayLimit
	 */
	public String getDayLimit() {
		return this.dayLimit;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}

	/**
	 * @return debtLevel
	 */
	public String getDebtLevel() {
		return this.debtLevel;
	}

	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}

	/**
	 * @return lgd
	 */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}

	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}

	/**
	 * @return ead
	 */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}

	/**
	 * @param otherLoanPurp
	 */
	public void setOtherLoanPurp(String otherLoanPurp) {
		this.otherLoanPurp = otherLoanPurp;
	}

	/**
	 * @return otherLoanPurp
	 */
	public String getOtherLoanPurp() {
		return this.otherLoanPurp;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}