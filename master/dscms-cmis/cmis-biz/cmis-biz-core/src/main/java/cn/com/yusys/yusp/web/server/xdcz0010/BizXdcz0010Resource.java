package cn.com.yusys.yusp.web.server.xdcz0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0010.req.List;
import cn.com.yusys.yusp.dto.server.xdcz0010.req.Xdcz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0010.resp.Xdcz0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:企业网银省心E付放款
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0010:企业网银省心E付放款")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0010.BizXdcz0010Resource.class);
    /**
     * 交易码：xdcz0010
     * 交易描述：企业网银省心E付放款
     *
     * @param xdcz0010DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("企业网银省心E付放款")
    @PostMapping("/xdcz0010")
    protected @ResponseBody
    ResultDto<Xdcz0010DataRespDto>  xdcz0010(@Validated @RequestBody Xdcz0010DataReqDto xdcz0010DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010DataReqDto));
        Xdcz0010DataRespDto  xdcz0010DataRespDto  = new Xdcz0010DataRespDto();// 响应Dto:企业网银省心E付放款
        ResultDto<Xdcz0010DataRespDto>xdcz0010DataResultDto = new ResultDto<>();

        String contNo = xdcz0010DataReqDto.getContNo();//合同号
        String cusId = xdcz0010DataReqDto.getCusId();//客户号
        String cusName = xdcz0010DataReqDto.getCusName();//客户名
        String billNo = xdcz0010DataReqDto.getBillNo();//借据名
        String loanEndDate = xdcz0010DataReqDto.getLoanEndDate();//借款到期日
        String loanUseType = xdcz0010DataReqDto.getLoanUseType();//借款用途
        BigDecimal contAmt = xdcz0010DataReqDto.getContAmt();//合同金额
        String preferSurplusTimes = xdcz0010DataReqDto.getPreferSurplusTimes();//优惠剩余次数
        String preferPoints = xdcz0010DataReqDto.getPreferPoints();//优惠点数
        BigDecimal loanAmt = xdcz0010DataReqDto.getLoanAmt();//借据金额
        String repayAcctNo = xdcz0010DataReqDto.getRepayAcctNo();//还款账号
        String acctName = xdcz0010DataReqDto.getAcctName();//账户名称
        String replacePayoutMon = xdcz0010DataReqDto.getReplacePayoutMon();//代发月份
        BigDecimal indivSalAMt = xdcz0010DataReqDto.getIndivSalAMt();//工资总金额
        BigDecimal realityIrY = xdcz0010DataReqDto.getRealityIrY();//执行利率
        String amtUcase = xdcz0010DataReqDto.getAmtUcase();//金额大写
        String uploadFile = xdcz0010DataReqDto.getUploadFile();//上传文件
        java.util.List<List> list = xdcz0010DataReqDto.getList();

        try {
            // 从xdcz0010DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0010DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdcz0010DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xdcz0010DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdcz0010DataRespDto对象结束
            // 封装xdcz0010DataResultDto中正确的返回码和返回信息
            xdcz0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value,e.getMessage());
            // 封装xdcz0010DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0010DataRespDto到xdcz0010DataResultDto中
        xdcz0010DataResultDto.setData(xdcz0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010DataRespDto));
        return xdcz0010DataResultDto;
    }
}
