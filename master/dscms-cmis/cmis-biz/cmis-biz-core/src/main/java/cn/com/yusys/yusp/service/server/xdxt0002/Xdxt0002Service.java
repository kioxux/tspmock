package cn.com.yusys.yusp.service.server.xdxt0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.req.Xdxt0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.List;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.Xdxt0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AreaAdminUserMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * 业务逻辑类:根据直营团队类型查询客户经理工号
 *
 * @author 徐超
 * @version 1.0
 */
@Service
public class Xdxt0002Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxt0002Service.class);

    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Transactional
    public Xdxt0002DataRespDto getXdxt0002(Xdxt0002DataReqDto xdxt0002DataReqDto)throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0002DataReqDto));
        Xdxt0002DataRespDto xdxt0002DataRespDto = new Xdxt0002DataRespDto();
        try {
            //团队类型
            String teamType = xdxt0002DataReqDto.getTeamType();
            logger.info("**********XDXT0002**根据团队类型查询所在机构信息开始,查询参数为:{}", JSON.toJSONString(teamType));
            String orgId = areaAdminUserMapper.getOrgIdByTeamType(teamType);
            logger.info("**********XDXT0002**根据团队类型查询所在机构信息结束,查询结果为:{}", JSON.toJSONString(orgId));
            java.util.List<cn.com.yusys.yusp.dto.server.xdxt0002.resp.List> lists = new ArrayList<>();
            if(StringUtils.nonEmpty(orgId)){
                logger.info("**********XDXT0002**根据机构号查询客户经理工号开始,查询参数为:{}", JSON.toJSONString(orgId));
                ResultDto<java.util.List<AdminSmUserDto>> resultDto = adminSmUserService.getByName(orgId);
                logger.info("**********XDXT0002**根据机构号查询客户经理工号结束,查询结果为:{}", JSON.toJSONString(resultDto));
                if(ResultDto.success().getCode().equals(resultDto.getCode())){
                    java.util.List<AdminSmUserDto> adminSmUserDtos = resultDto.getData();
                    lists = adminSmUserDtos.stream().map(e->{
                        cn.com.yusys.yusp.dto.server.xdxt0002.resp.List list = new cn.com.yusys.yusp.dto.server.xdxt0002.resp.List();
                        list.setManagerId(e.getLoginCode());
                        return list;
                    }).collect(Collectors.toList());
                }
            }
            xdxt0002DataRespDto.setList(lists);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0002DataRespDto));
        return xdxt0002DataRespDto;
    }

}
