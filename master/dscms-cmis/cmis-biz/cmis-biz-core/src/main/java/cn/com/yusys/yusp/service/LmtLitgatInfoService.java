package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtCobInfo;
import cn.com.yusys.yusp.domain.LmtLitgatInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.R007Dto;
import cn.com.yusys.yusp.dto.YqdLigatAndDebitDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases;
import cn.com.yusys.yusp.repository.mapper.LmtLitgatInfoMapper;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credzb.CredzbService;
import cn.com.yusys.yusp.service.client.bsp.fxyjxt.hhmdkh.HhmdkhService;
import cn.com.yusys.yusp.service.client.bsp.outerdata.qyssxx.QyssxxService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtLitgatInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-17 22:48:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtLitgatInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtLitgatInfoService.class);

    @Autowired
    private LmtLitgatInfoMapper lmtLitgatInfoMapper;

    @Autowired
    private HhmdkhService hhmdkhService;

    @Autowired
    private LmtSurveyReportBasicInfoService basicInfoService;

    @Autowired
    private LmtSurveyReportComInfoService comInfoService;

    @Autowired
    private LmtCobInfoService cobInfoService;

    @Autowired
    private QyssxxService qyssxxService;

    @Autowired
    private CredzbService credzbService;

    @Autowired
    private CreditReportQryLstService CreditReportQryLstService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtLitgatInfo selectByPrimaryKey(String pkId) {
        return lmtLitgatInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtLitgatInfo> selectAll(QueryModel model) {
        List<LmtLitgatInfo> records = (List<LmtLitgatInfo>) lmtLitgatInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtLitgatInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtLitgatInfo> list = lmtLitgatInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtLitgatInfo record) {
        return lmtLitgatInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtLitgatInfo record) {
        return lmtLitgatInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtLitgatInfo record) {
        return lmtLitgatInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtLitgatInfo record) {
        return lmtLitgatInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtLitgatInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtLitgatInfoMapper.deleteByIds(ids);
    }

    /**
     * @param surveySerno
     * @return ResultDto
     * @author hubp
     * @date 2021/5/22 15:58
     * @version 1.0.0
     * @desc 查询黑名单信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<YqdLigatAndDebitDto> hhmdkh(String surveySerno) {
        logger.info("查询黑名单信息开始..................");
        YqdLigatAndDebitDto yqdLigatAndDebitDto = new YqdLigatAndDebitDto();
        try {
            LmtSurveyReportBasicInfo basicInfo = basicInfoService.selectByPrimaryKey(surveySerno);
            LmtSurveyReportComInfo ComInfo = comInfoService.selectByPrimaryKey(surveySerno);
            List<LmtCobInfo> cobInfoList = cobInfoService.selectByIqpSerno(surveySerno);
            List<HhmdkhRespDto> hhmdkhList = new ArrayList<>();
            //借款人黑名单信息
            if (null != basicInfo) {
                logger.info("开始查找借款人黑名单信息---借款人姓名：【{}】,借款人证件号：【{}】", basicInfo.getCusName(), basicInfo.getCertCode());
                HhmdkhReqDto hhmdkhReqDto = new HhmdkhReqDto();
                //塞入名称
                hhmdkhReqDto.setCustnm(basicInfo.getCusName());
                HhmdkhRespDto hhmdkhRespDto = hhmdkhService.hhmdkh(hhmdkhReqDto);
                hhmdkhList.add(hhmdkhRespDto);
                logger.info("查找借款人黑名单信息结束---借款人姓名：【{}】,借款人证件号：【{}】", basicInfo.getCusName(), basicInfo.getCertCode());
            }
            //共借人黑名单信息
            if (cobInfoList.size() > 0) {
                for (LmtCobInfo lmtCobInfo : cobInfoList) {
                    logger.info("开始查找共借人黑名单信息---共借人姓名：【{}】,共借人证件号：【{}】", lmtCobInfo.getCommonDebitName(), lmtCobInfo.getCommonDebitCertCode());
                    HhmdkhReqDto hhmdkhReqDto = new HhmdkhReqDto();
                    //塞入名称
                    hhmdkhReqDto.setCustnm(lmtCobInfo.getCommonDebitName());
                    HhmdkhRespDto hhmdkhRespDto = hhmdkhService.hhmdkh(hhmdkhReqDto);
                    hhmdkhList.add(hhmdkhRespDto);
                    logger.info("查找共借人黑名单信息结束---共借人姓名：【{}】,共借人证件号：【{}】", lmtCobInfo.getCommonDebitName(), lmtCobInfo.getCommonDebitCertCode());
                }
            }
            yqdLigatAndDebitDto.setHhmdkhList(hhmdkhList);
            List<R007Dto> list = this.selectDebit(surveySerno);
            if (list.size() > 0) {
                yqdLigatAndDebitDto.setR007Dto(list);
            }
        } catch (YuspException e) {
            logger.info("查询黑名单信息失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("查询黑名单信息结束..................");
        }
        return new ResultDto(yqdLigatAndDebitDto);
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto
     * @author hubp
     * @date 2021/7/2 10:53
     * @version 1.0.0
     * @desc 根据征信风险指标查询信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<R007Dto> selectDebit(String surveySerno) {
        logger.info("根据征信风险指标查询信息开始..................");
        CredzbRespDto credzbRespDto = null;
        List<R007Dto> list = new ArrayList<>();
        try {
            CredzbReqDto credzbReqDto = new CredzbReqDto();
            //根据流水号查询相关信息
            LmtSurveyReportBasicInfo basicInfo = basicInfoService.selectByPrimaryKey(surveySerno);
            credzbReqDto.setCustomName(basicInfo.getCusName());
            credzbReqDto.setCertificateNum(basicInfo.getCertCode());
            credzbReqDto.setRuleCode("R007");
            credzbReqDto.setBrchno(basicInfo.getManagerBrId());
            // 2021年10月25日14:43:29 hubp  BUG15211 查询征信获取征信编号进行填充
            QueryModel model = new QueryModel();
            model.addCondition("bizSerno" , surveySerno);
            model.addCondition("certCode" , basicInfo.getCertCode());
            model.setSort("reportCreateTime desc");
            List<CreditReportQryLstDto> reportList = CreditReportQryLstService.selectCreditReportQryLstByCrqlSerno(model);
            if(reportList.size() > 0){
                credzbReqDto.setReportId(reportList.get(0).getReportNo());
            } else {
                throw BizException.error(null, "9999","未查到到借款人征信报告编号！");
            }
            credzbRespDto = credzbService.credzb(credzbReqDto);
            // 开始处理指标对象
            String result = credzbRespDto.getResult();
            Map<String, Integer> condition = null;
            if (null != result && result.length() != 0) {
                condition = (Map<String, Integer>) JSON.parse(result);
            }
            if (null == condition) {
                throw new RuntimeException("未查询到指标数据！");
            }
            condition.forEach((x, y) -> {
                R007Dto r007Dto = new R007Dto();
                r007Dto.setName(basicInfo.getCusName());
                if ("OVERDUEACCOUNTNUM".equals(x)) {
                    r007Dto.setFxzb("OVERDUEACCOUNTNUM");
                    r007Dto.setItem("当前逾期账户数");
                } else if ("OVERDUEM3ACCOUNTNUM".equals(x)) {
                    r007Dto.setFxzb("OVERDUEM3ACCOUNTNUM");
                    r007Dto.setItem("逾期M3及以上账户数");
                } else if ("GUARANTEEACCOUNTNUM".equals(x)) {
                    r007Dto.setFxzb("GUARANTEEACCOUNTNUM");
                    r007Dto.setItem("对外担保账户数");
                } else if ("CREDITCARD".equals(x)) {
                    r007Dto.setFxzb("CREDITCARD");
                    r007Dto.setItem("近半年信用卡审批次数");
                } else if ("LOAN".equals(x)) {
                    r007Dto.setFxzb("LOAN");
                    r007Dto.setItem("近半年贷款审批次数");
                } else if ("BEFOREGUARANTEE".equals(x)) {
                    r007Dto.setFxzb("BEFOREGUARANTEE");
                    r007Dto.setItem("近半年保前审查次数");
                } else if ("GUARANTEE".equals(x)) {
                    r007Dto.setFxzb("GUARANTEE");
                    r007Dto.setItem("近半年担保资格审查次数");
                } else if ("ABNORMALACCOUNT".equals(x)) {
                    r007Dto.setFxzb("ABNORMALACCOUNT");
                    r007Dto.setItem("五级分类非正常贷款或信用卡账户数");
                }
                r007Dto.setResult(y);
                list.add(r007Dto);
            });
        } catch (BizException e) {
            logger.info("根据征信风险指标查询信息失败..................");
            throw BizException.error(null, "9999", e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("根据征信风险指标查询信息结束..................");
        }

        return list;
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto
     * @author hubp
     * @date 2021/5/24 17:01
     * @version 1.0.0
     * @desc 查询涉诉信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List qyssxx(String surveySerno) {
        logger.info("*************查询涉诉信息开始**********【{}】", surveySerno);
        List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> cases = new ArrayList<>();
        try {
            QyssxxRespDto qyssxxRespDto = null;
            QyssxxReqDto qyssxxReqDto = new QyssxxReqDto();
            LmtSurveyReportBasicInfo basicInfo = basicInfoService.selectByPrimaryKey(surveySerno);
            LmtSurveyReportComInfo ComInfo = comInfoService.selectByPrimaryKey(surveySerno);
            List<LmtCobInfo> cobInfoList = cobInfoService.selectByIqpSerno(surveySerno);
            //借款人涉诉信息
            if (null != basicInfo) {
                logger.info("开始查找借款人涉诉信息---借款人姓名：【{}】,借款人证件号：【{}】", basicInfo.getCusName(), basicInfo.getCertCode());
                //查询类型->0：查询自然人， 1：查询组织机构
                qyssxxReqDto.setType("0");
                //姓名/企业名称
                qyssxxReqDto.setName(basicInfo.getCusName());
                //身份证号/组织机构代码
                qyssxxReqDto.setQyid(basicInfo.getCertCode());
                //查找借款人涉诉信息
                qyssxxRespDto = qyssxxService.qyssxx(qyssxxReqDto);
                //取出借款人涉诉信息
                List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> casesBasic = qyssxxRespDto.getData().getCivil().getCases();
                if (null != casesBasic && casesBasic.size() > 0) {
                    cases.addAll(casesBasic);
                    this.addLmtLitgatInfo(casesBasic, surveySerno);
                }
                logger.info("查找借款人涉诉信息借宿---借款人姓名：【{}】,借款人证件号：【{}】", basicInfo.getCusName(), basicInfo.getCertCode());
            }
            //企业涉诉信息
            if (null != ComInfo) {
                logger.info("开始查找企业涉诉信息---企业名：【{}】", ComInfo.getConName());
                //查询类型->0：查询自然人， 1：查询组织机构
                qyssxxReqDto.setType("1");
                //姓名/企业名称
                qyssxxReqDto.setName(ComInfo.getConName());
                //身份证号/组织机构代码
                qyssxxReqDto.setQyid("");
                //查找企业涉诉信息
                qyssxxRespDto = qyssxxService.qyssxx(qyssxxReqDto);
                //取出企业涉诉信息
                List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> casesCom = qyssxxRespDto.getData().getCivil().getCases();
                if (null != casesCom && casesCom.size() > 0) {
                    cases.addAll(casesCom);
                    this.addLmtLitgatInfo(casesCom, surveySerno);
                }
                logger.info("查找企业涉诉信息结束---企业名：【{}】", ComInfo.getConName());
            }
            //共借人涉诉信息
            if (cobInfoList.size() > 0) {
                cobInfoList.forEach(cobInfo -> {
                    //查询类型->0：查询自然人， 1：查询组织机构
                    logger.info("开始查找共借人涉诉信息---共借人姓名：【{}】,共借人证件号：【{}】", cobInfo.getCommonDebitName(), cobInfo.getCommonDebitCertCode());
                    qyssxxReqDto.setType("0");
                    //姓名/企业名称
                    qyssxxReqDto.setName(cobInfo.getCommonDebitName());
                    //身份证号/组织机构代码
                    qyssxxReqDto.setQyid(cobInfo.getCommonDebitCertCode());
                    //查找共借人涉诉信息
                    QyssxxRespDto qyssxxRespDtoCob = qyssxxService.qyssxx(qyssxxReqDto);
                    //取出共借人涉诉信息
                    List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> casesCob = qyssxxRespDtoCob.getData().getCivil().getCases();
                    if (null != casesCob && casesCob.size() > 0) {
                        cases.addAll(casesCob);
                        this.addLmtLitgatInfo(casesCob, surveySerno);
                    }
                    logger.info("查找共借人涉诉信息结束---共借人姓名：【{}】,共借人证件号：【{}】", cobInfo.getCommonDebitName(), cobInfo.getCommonDebitCertCode());
                });
            }
        } catch (YuspException e) {
            logger.info("***********查询涉诉信息异常*************");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("******************查询涉诉信息结束************");
        }
        return cases;
    }

    /**
     * @param casesCob
     * @return void
     * @author hubp
     * @date 2021/8/31 9:42
     * @version 1.0.0
     * @desc 私有方法，将查询到的涉诉信息，落到自己的表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void addLmtLitgatInfo(List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> casesCob, String surveySerno) {
        for (Cases cases : casesCob) {
            LmtLitgatInfo lmtLitgatInfo = new LmtLitgatInfo();
            lmtLitgatInfo.setSurveySerno(surveySerno);
            lmtLitgatInfo.setCusName(cases.getC_gkws_dsr());
            lmtLitgatInfo.setCaseno(cases.getC_ah());
            lmtLitgatInfo.setLawsuitStats(cases.getN_ssdw());
            lmtLitgatInfo.setPkId(UUID.randomUUID().toString());
            lmtLitgatInfoMapper.insert(lmtLitgatInfo);
        }
    }
}
