/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiscApp
 * @类描述: iqp_disc_app数据实体类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:03:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_disc_app")
public class IqpDiscApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 10)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 业务类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;
	
	/** 合同类型 STD_ZB_CONT_TYPE **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 担保方式 STD_ZB_ASSURE_MEANS **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 合同币种 STD_ZX_CUR_TYPE **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 本合同项下最高可用信金额 **/
	@Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighAvlAmt;
	
	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private Integer contTerm;
	
	/** 起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;
	
	/** 是否续签  STD_ZB_YES_NO **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;
	
	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;
	
	/** 是否使用授信额度 STD_ZB_YES_NO **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 是否电子用印 STD_ZB_YES_NO **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;
	
	/** 是否在线抵押 STD_ZB_YES_NO **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 5)
	private String isOlPld;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 5)
	private String belgLine;
	
	/** 债项等级 DEBT_GRADE **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;
	
	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;
	
	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;
	
	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;
	
	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 20)
	private String fax;
	
	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;
	
	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 40)
	private String wechat;
	
	/** 送达地址 **/
	@Column(name = "DELIVERY_ADDR", unique = false, nullable = true, length = 500)
	private String deliveryAddr;
	
	/** 审批状态 WF_STATUS **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 贴现协议类型   STD_ZB_DISC_CONT_TYPE **/
	@Column(name = "DISC_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String discContType;
	
	/** 票据种类   STD_ZB_DRFT_TYPE **/
	@Column(name = "DRFT_TYPE", unique = false, nullable = true, length = 5)
	private String drftType;
	
	/** 是否电子票据  STD_ZB_YES_NO **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;
	
	/** 申请人账号 **/
	@Column(name = "RQSTR_ACC_NO", unique = false, nullable = true, length = 40)
	private String rqstrAccNo;
	
	/** 申请人账户名称 **/
	@Column(name = "RQSTR_ACC_NAME", unique = false, nullable = true, length = 80)
	private String rqstrAccName;
	
	/** 买入类型  STD_ML_DISC_CHECK **/
	@Column(name = "PUR_TYPE", unique = false, nullable = true, length = 5)
	private String purType;
	
	/** 贴现币种  STD_ZX_CUR_TYPE **/
	@Column(name = "DISC_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String discCurType;
	
	/** 票面总金额 **/
	@Column(name = "DRFT_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftTotalAmt;
	
	/** 是否先贴后查  STD_ZB_YES_NO **/
	@Column(name = "IS_ATCF", unique = false, nullable = true, length = 5)
	private String isAtcf;
	
	/** 付息方式  STD_ZB_PAY_INTEREST **/
	@Column(name = "PINT_MODE", unique = false, nullable = true, length = 5)
	private String pintMode;
	
	/** 承兑企业授信额度编号 **/
	@Column(name = "ACPT_CRP_LMT_NO", unique = false, nullable = true, length = 40)
	private String acptCrpLmtNo;
	
	/** 承兑企业批复编号 **/
	@Column(name = "ACPT_CRP_REPLY_NO", unique = false, nullable = true, length = 40)
	private String acptCrpReplyNo;
	
	/** 承兑企业客户编号 **/
	@Column(name = "ACPT_CRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String acptCrpCusId;
	
	/** 承兑企业客户名称 **/
	@Column(name = "ACPT_CRP_CUS_NAME", unique = false, nullable = true, length = 40)
	private String acptCrpCusName;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}
	
    /**
     * @return contType
     */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(java.math.BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}
	
    /**
     * @return contHighAvlAmt
     */
	public java.math.BigDecimal getContHighAvlAmt() {
		return this.contHighAvlAmt;
	}
	
	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}
	
    /**
     * @return contTerm
     */
	public Integer getContTerm() {
		return this.contTerm;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}
	
    /**
     * @return paperContSignDate
     */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}
	
	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}
	
    /**
     * @return isRenew
     */
	public String getIsRenew() {
		return this.isRenew;
	}
	
	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}
	
    /**
     * @return origiContNo
     */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

    /**
     * @param isUtilLmt
     */
    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    /**
     * @return isUtilLmt
     */
    public String getIsUtilLmt() {
        return this.isUtilLmt;
    }
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}
	
    /**
     * @return isESeal
     */
	public String getIsESeal() {
		return this.isESeal;
	}
	
	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}
	
    /**
     * @return isOlPld
     */
	public String getIsOlPld() {
		return this.isOlPld;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}
	
    /**
     * @return debtLevel
     */
	public String getDebtLevel() {
		return this.debtLevel;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	
    /**
     * @return linkman
     */
	public String getLinkman() {
		return this.linkman;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
    /**
     * @return wechat
     */
	public String getWechat() {
		return this.wechat;
	}
	
	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}
	
    /**
     * @return deliveryAddr
     */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param discContType
	 */
	public void setDiscContType(String discContType) {
		this.discContType = discContType;
	}
	
    /**
     * @return discContType
     */
	public String getDiscContType() {
		return this.discContType;
	}
	
	/**
	 * @param drftType
	 */
	public void setDrftType(String drftType) {
		this.drftType = drftType;
	}
	
    /**
     * @return drftType
     */
	public String getDrftType() {
		return this.drftType;
	}
	
	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}
	
    /**
     * @return isEDrft
     */
	public String getIsEDrft() {
		return this.isEDrft;
	}
	
	/**
	 * @param rqstrAccNo
	 */
	public void setRqstrAccNo(String rqstrAccNo) {
		this.rqstrAccNo = rqstrAccNo;
	}
	
    /**
     * @return rqstrAccNo
     */
	public String getRqstrAccNo() {
		return this.rqstrAccNo;
	}
	
	/**
	 * @param rqstrAccName
	 */
	public void setRqstrAccName(String rqstrAccName) {
		this.rqstrAccName = rqstrAccName;
	}
	
    /**
     * @return rqstrAccName
     */
	public String getRqstrAccName() {
		return this.rqstrAccName;
	}
	
	/**
	 * @param purType
	 */
	public void setPurType(String purType) {
		this.purType = purType;
	}
	
    /**
     * @return purType
     */
	public String getPurType() {
		return this.purType;
	}
	
	/**
	 * @param discCurType
	 */
	public void setDiscCurType(String discCurType) {
		this.discCurType = discCurType;
	}
	
    /**
     * @return discCurType
     */
	public String getDiscCurType() {
		return this.discCurType;
	}
	
	/**
	 * @param drftTotalAmt
	 */
	public void setDrftTotalAmt(java.math.BigDecimal drftTotalAmt) {
		this.drftTotalAmt = drftTotalAmt;
	}
	
    /**
     * @return drftTotalAmt
     */
	public java.math.BigDecimal getDrftTotalAmt() {
		return this.drftTotalAmt;
	}
	
	/**
	 * @param isAtcf
	 */
	public void setIsAtcf(String isAtcf) {
		this.isAtcf = isAtcf;
	}
	
    /**
     * @return isAtcf
     */
	public String getIsAtcf() {
		return this.isAtcf;
	}
	
	/**
	 * @param pintMode
	 */
	public void setPintMode(String pintMode) {
		this.pintMode = pintMode;
	}
	
    /**
     * @return pintMode
     */
	public String getPintMode() {
		return this.pintMode;
	}

	
	/**
	 * @param acptCrpLmtNo
	 */
	public void setAcptCrpLmtNo(String acptCrpLmtNo) {
		this.acptCrpLmtNo = acptCrpLmtNo;
	}
	
    /**
     * @return acptCrpLmtNo
     */
	public String getAcptCrpLmtNo() {
		return this.acptCrpLmtNo;
	}
	
	/**
	 * @param acptCrpReplyNo
	 */
	public void setAcptCrpReplyNo(String acptCrpReplyNo) {
		this.acptCrpReplyNo = acptCrpReplyNo;
	}
	
    /**
     * @return acptCrpReplyNo
     */
	public String getAcptCrpReplyNo() {
		return this.acptCrpReplyNo;
	}
	
	/**
	 * @param acptCrpCusId
	 */
	public void setAcptCrpCusId(String acptCrpCusId) {
		this.acptCrpCusId = acptCrpCusId;
	}
	
    /**
     * @return acptCrpCusId
     */
	public String getAcptCrpCusId() {
		return this.acptCrpCusId;
	}
	
	/**
	 * @param acptCrpCusName
	 */
	public void setAcptCrpCusName(String acptCrpCusName) {
		this.acptCrpCusName = acptCrpCusName;
	}
	
    /**
     * @return acptCrpCusName
     */
	public String getAcptCrpCusName() {
		return this.acptCrpCusName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}