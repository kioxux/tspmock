/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitCreditInfo
 * @类描述: lmt_debit_credit_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 16:15:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_debit_credit_info")
public class LmtDebitCreditInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "survey_serno")
	private String surveySerno;
	
	/** 借款人未结清贷款笔数 **/
	@Column(name = "loan_unpaid_nums", unique = false, nullable = true, length = 10)
	private String loanUnpaidNums;
	
	/** 借款人未销户信用卡授信总额 **/
	@Column(name = "credit_crdamt_close", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal creditCrdamtClose;
	
	/** 借款人未结清贷款余额 **/
	@Column(name = "loan_bal_unpaid", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalUnpaid;
	
	/** 借款人近6个月信用卡平均支用比例 **/
	@Column(name = "credit_6m_ave_defray_perc", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal credit6mAveDefrayPerc;
	
	/** 借款人近2年贷款审批查询机构数 **/
	@Column(name = "loan_2y_qryorg_nums", unique = false, nullable = true, length = 10)
	private String loan2yQryorgNums;
	
	/** 借款人未结清消费金融公司贷款笔数 **/
	@Column(name = "loan_consumecom_unpaid_nums", unique = false, nullable = true, length = 10)
	private String loanConsumecomUnpaidNums;
	
	/** 借款人未结清对外担保贷款笔数 **/
	@Column(name = "loan_outguar_unpaid_nums", unique = false, nullable = true, length = 10)
	private String loanOutguarUnpaidNums;
	
	/** 企业未结清业务机构数 **/
	@Column(name = "com_unpaid_busi_org", unique = false, nullable = true, length = 10)
	private String comUnpaidBusiOrg;
	
	/** 企业未结清信贷余额(元) **/
	@Column(name = "loan_bal_com_unpaid", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalComUnpaid;
	
	/** 创建时间 **/
	@Column(name = "create_time", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "update_time", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param loanUnpaidNums
	 */
	public void setLoanUnpaidNums(String loanUnpaidNums) {
		this.loanUnpaidNums = loanUnpaidNums;
	}
	
    /**
     * @return loanUnpaidNums
     */
	public String getLoanUnpaidNums() {
		return this.loanUnpaidNums;
	}
	
	/**
	 * @param creditCrdamtClose
	 */
	public void setCreditCrdamtClose(java.math.BigDecimal creditCrdamtClose) {
		this.creditCrdamtClose = creditCrdamtClose;
	}
	
    /**
     * @return creditCrdamtClose
     */
	public java.math.BigDecimal getCreditCrdamtClose() {
		return this.creditCrdamtClose;
	}
	
	/**
	 * @param loanBalUnpaid
	 */
	public void setLoanBalUnpaid(java.math.BigDecimal loanBalUnpaid) {
		this.loanBalUnpaid = loanBalUnpaid;
	}
	
    /**
     * @return loanBalUnpaid
     */
	public java.math.BigDecimal getLoanBalUnpaid() {
		return this.loanBalUnpaid;
	}
	
	/**
	 * @param credit6mAveDefrayPerc
	 */
	public void setCredit6mAveDefrayPerc(java.math.BigDecimal credit6mAveDefrayPerc) {
		this.credit6mAveDefrayPerc = credit6mAveDefrayPerc;
	}
	
    /**
     * @return credit6mAveDefrayPerc
     */
	public java.math.BigDecimal getCredit6mAveDefrayPerc() {
		return this.credit6mAveDefrayPerc;
	}
	
	/**
	 * @param loan2yQryorgNums
	 */
	public void setLoan2yQryorgNums(String loan2yQryorgNums) {
		this.loan2yQryorgNums = loan2yQryorgNums;
	}
	
    /**
     * @return loan2yQryorgNums
     */
	public String getLoan2yQryorgNums() {
		return this.loan2yQryorgNums;
	}
	
	/**
	 * @param loanConsumecomUnpaidNums
	 */
	public void setLoanConsumecomUnpaidNums(String loanConsumecomUnpaidNums) {
		this.loanConsumecomUnpaidNums = loanConsumecomUnpaidNums;
	}
	
    /**
     * @return loanConsumecomUnpaidNums
     */
	public String getLoanConsumecomUnpaidNums() {
		return this.loanConsumecomUnpaidNums;
	}
	
	/**
	 * @param loanOutguarUnpaidNums
	 */
	public void setLoanOutguarUnpaidNums(String loanOutguarUnpaidNums) {
		this.loanOutguarUnpaidNums = loanOutguarUnpaidNums;
	}
	
    /**
     * @return loanOutguarUnpaidNums
     */
	public String getLoanOutguarUnpaidNums() {
		return this.loanOutguarUnpaidNums;
	}
	
	/**
	 * @param comUnpaidBusiOrg
	 */
	public void setComUnpaidBusiOrg(String comUnpaidBusiOrg) {
		this.comUnpaidBusiOrg = comUnpaidBusiOrg;
	}
	
    /**
     * @return comUnpaidBusiOrg
     */
	public String getComUnpaidBusiOrg() {
		return this.comUnpaidBusiOrg;
	}
	
	/**
	 * @param loanBalComUnpaid
	 */
	public void setLoanBalComUnpaid(java.math.BigDecimal loanBalComUnpaid) {
		this.loanBalComUnpaid = loanBalComUnpaid;
	}
	
    /**
     * @return loanBalComUnpaid
     */
	public java.math.BigDecimal getLoanBalComUnpaid() {
		return this.loanBalComUnpaid;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}