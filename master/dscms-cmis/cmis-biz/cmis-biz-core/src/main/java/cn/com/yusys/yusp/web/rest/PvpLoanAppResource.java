/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccountDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.service.CtrLoanContService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.BizPvpLoanAppDto;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import org.springframework.beans.BeanUtils;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-01-05 10:57:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "贷款出账申请")
@RequestMapping("/api/pvploanapp")
public class PvpLoanAppResource {
    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpLoanApp> list = pvpLoanAppService.selectAll(queryModel);
        return new ResultDto<List<PvpLoanApp>>(list);
    }


    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpLoanApp>> index(QueryModel queryModel) {
        List<PvpLoanApp> list = pvpLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pvpSerno}")
    protected ResultDto<PvpLoanApp> show(@PathVariable("pvpSerno") String pvpSerno) {
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        return new ResultDto<PvpLoanApp>(pvpLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpLoanApp> create(@RequestBody PvpLoanApp pvpLoanApp) throws URISyntaxException {
        pvpLoanAppService.insert(pvpLoanApp);
        return new ResultDto<PvpLoanApp>(pvpLoanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpLoanApp pvpLoanApp) throws URISyntaxException {
        int result = pvpLoanAppService.update(pvpLoanApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pvpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pvpSerno") String pvpSerno) {
        int result = pvpLoanAppService.deleteByPrimaryKey(pvpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/8 10:38
     * @注释 放款保存 小微
     */
    @PostMapping("/saveandupdate")
    protected ResultDto<Integer> saveandupdate(@RequestBody PvpLoanApp pvpLoanApp) throws URISyntaxException {

        return   pvpLoanAppService.saveandupdate(pvpLoanApp);

    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-06 9:45
     * @注释 根据合同新增数据 小微
     */
    @ApiOperation("根据合同新增数据 小微")
    @PostMapping("/addpvploanappxw")
    protected ResultDto addpvploanappxw(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.addpvploanappxw(pvpLoanApp);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-06 9:45
     * @注释 根据合同新增数据 零售
     */
    @ApiOperation("根据合同新增数据 零售")
    @PostMapping("/addpvploanappls")
    protected ResultDto addpvploanappls(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.addpvploanappls(pvpLoanApp);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-17 16:42
     * @注释 查询放款申请基本信息页面
     */
    @ApiOperation("查询放款申请基本信息页面")
    @PostMapping("/selectbasicpage/{ids}")
    protected ResultDto selectbasicpage(@PathVariable String ids) {
        return pvpLoanAppService.selectbasicpage(ids);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-17 16:42
     * @注释 根据放款单查询合同信息
     */
    @ApiOperation("根据放款单查询合同信息")
    @PostMapping("/selectbyids/{ids}")
    protected ResultDto selectbyids(@PathVariable String ids) {
        return pvpLoanAppService.selectbyids(ids);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-17 19:18
     * @注释 根据传入的对象进行修改操作
     */
    @ApiOperation("根据传入的对象进行修改操作")
    @PostMapping("/updateonedata")
    protected ResultDto updateonedata(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.updateonedata(pvpLoanApp);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:04
     * @注释 还款测算
     */
    @ApiOperation("还款测算")
    @PostMapping("/selectcalculate")
    protected ResultDto selectcalculate(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        return pvpLoanAppService.selectcalculate(lmtSurveyReportDto);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:04
     * @注释 删除/作废放款申请单 申请状态 ->200
     */
    @ApiOperation("删除/作废放款申请单 申请状态 ->200")
    @PostMapping("/invalidpvploanapp")
    protected ResultDto invalidpvploanapp(@RequestBody PvpLoanApp pvpLoanApp ) {
        return pvpLoanAppService.invalidpvploanapp(pvpLoanApp.getPvpSerno());
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:05
     * @注释 撤销放款申请单 申请状态->000
     */
    @ApiOperation("撤销放款申请单,申请状态->000")
    @PostMapping("/revokepvploanapp")
    protected ResultDto revokepvploanapp(@RequestBody String pvpSerno) {
        return pvpLoanAppService.revokepvploanapp(pvpSerno);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:29
     * @注释 借据 打印接口
     */
    @ApiOperation("借据,打印接口")
    @PostMapping("/pvploanprint")
    protected ResultDto pvploanprint(@RequestBody String pvpSerno) {
        return pvpLoanAppService.pvploanprint(pvpSerno);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:30
     * @注释 额度 申请书 打印
     */
    @ApiOperation("额度,申请书,打印")
    @PostMapping("/applybookprint")
    protected ResultDto applybookprint(@RequestBody String pvpSerno) {
        return pvpLoanAppService.applybookprint(pvpSerno);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-24 15:33
     * @注释 手动Manual 发送send
     */
    @PostMapping("/manualsend")
    protected ResultDto manualsend(@RequestBody String pvpSerno) {
        return pvpLoanAppService.manualsend(pvpSerno);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("贷款出账申请待发起")
    @PostMapping("/tosignlist")
    protected ResultDto<List<PvpLoanApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        queryModel.getCondition().put("belgLine", CmisBizConstants.BELGLINE_DG_03);
        List<PvpLoanApp> list = pvpLoanAppService.signlist(queryModel);
        return new ResultDto<List<PvpLoanApp>>(list);
    }

    /**
     * @函数名称:solvedSignlist
     * @函数描述:贷款出账申请已处理
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("贷款出账申请已处理")
    @PostMapping("/solvedsignlist")
    protected ResultDto<List<PvpLoanApp>> solvedSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        queryModel.getCondition().put("belgLine", CmisBizConstants.BELGLINE_DG_03);
        List<PvpLoanApp> list = pvpLoanAppService.solvedSignlist(queryModel);
        return new ResultDto<List<PvpLoanApp>>(list);
    }

    /**
     * @函数名称:savePvploanapp
     * @函数描述:贷款出账申请新增
     * @参数与返回说明:pvpLoanApp
     * @创建人:zhanyb
     */
    @ApiOperation("贷款出账申请新增")
    @PostMapping("/savepvploanapp")
    protected ResultDto<Map> savePvploanapp(@RequestBody PvpLoanApp pvpLoanApp) throws URISyntaxException {
        ResultDto<PvpLoanApp> resultDto = new ResultDto<PvpLoanApp>();
        Map result = pvpLoanAppService.savePvploanapp(pvpLoanApp);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("贷款出账申请查看")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpLoanApp temp = new PvpLoanApp();
        PvpLoanApp studyDemo = pvpLoanAppService.selectByPvpLoanSernoKey((String)params.get(("pvpSerno")));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:delete
     * @函数描述:贷款出账申请手工作废
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhanyb
     */
    @ApiOperation("贷款出账申请手工作废")
    @PostMapping("/handinvaild/{pvpSerno}")
    public ResultDto<Integer> handInvaild(@PathVariable("pvpSerno") String pvpSerno) {
        return new ResultDto<Integer>(pvpLoanAppService.onCancel(pvpSerno));
    }
    /**
     * 贷款出账申请手工作废
     * @param pvpLoanApp
     * @函数名称:updatesavepvploanapp
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhanyb
     */
    @ApiOperation("贷款出账申请保存")
    @PostMapping("/updatesavepvploanapp")
    public ResultDto<Map> updateSavePvpLoanApp(@RequestBody PvpLoanApp pvpLoanApp){
        Map rtnData = pvpLoanAppService.updateSavePvpLoanApp(pvpLoanApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-27 19:37
     * @注释 通过传入调查编号去找借据编号
     */
    @ApiOperation("通过传入调查编号去找借据编号")
    @PostMapping("/selectbilllist")
    public ResultDto<List<AccLoan>> selectbilllist(@RequestBody PvpLoanApp pvpLoanApp){
        return  pvpLoanAppService.selectbilllist(pvpLoanApp.getSurveySerno());
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 9:36
     * @注释 分页条件查询 多余筛选条件  所属条线 操作类型  申请状态
     */
    @ApiOperation("分页条件查询,多余筛选条件,所属条线,操作类型,申请状态")
    @PostMapping("/selectDataByModel")
    protected ResultDto selectDataByModel(@RequestBody QueryModel queryModel) {
        List<PvpLoanApp> list = pvpLoanAppService.selectDataByModel(queryModel);
        return new ResultDto(list);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-08 10:26
     * @注释 放款审批通过调接口
     */
    @ApiOperation("放款审批通过调接口")
    @PostMapping("/reviewend")
    protected ResultDto reviewend(@RequestBody PvpLoanApp pvpLoanApp){
        ResultDto reviewend = pvpLoanAppService.Xwyw02end(pvpLoanApp);
        return reviewend;
    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:11
     * @注释 放款单是否可以提交审批
     */
    @ApiOperation("放款单是否可以提交审批")
    @PostMapping("/submityesorno")
    protected ResultDto submityesorno(@RequestBody PvpLoanApp pvpLoanApp){
        return pvpLoanAppService.submityesorno(pvpLoanApp);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/6/9 10:31
     * @注释 校验放款单是否可以提交 for小微
     */
    @ApiOperation("放款单是否可以提交审批")
    @PostMapping("/submityesornoxw")
    protected ResultDto submityesornoxw(@RequestBody PvpLoanApp pvpLoanApp){
        return pvpLoanAppService.submityesornoxw(pvpLoanApp);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:26
     * @注释 查询单条对象
     */
    @ApiOperation("查询单条对象")
    @PostMapping("/selectbypvpsernoforxd")
    protected ResultDto selectByPvpSernoForXd(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.selectByPvpSernoForXd(pvpLoanApp.getPvpSerno());
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-08 16:26
     * @注释 查询单条对象
     */
    @ApiOperation("查询单条数据对象")
    @PostMapping("/selectbypvpserno")
    protected ResultDto selectByPvpSernoForLs(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.selectByPvpSernoForLs(pvpLoanApp.getPvpSerno());
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhanyb
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("贷款出账申请流程结束业务处理")
    @PostMapping("/handleBusinessAfterEnd")
    protected  ResultDto<Object> handleBusinessAfterEnd(@RequestBody Map map) {
        ResultDto<Object> result = new ResultDto<>();
        String pvpSerno = (String)map.get("pvpSerno");
        pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
        return  result;
    }

    /**
     * @author shenli
     * @date 2021-6-4
     * @version 1.0.0
     * @desc     根据账号查询帐户信息
     * @修改历史  修改时间 修改人员 修改原因
     */
    @PostMapping("/getOpanOrgName")
    protected ResultDto getOpanOrgName(@RequestBody IqpLoanApp iqpLoanApp) {
        ResultDto rusult = ctrLoanContService.getOpanOrgName(iqpLoanApp.getLoanPayoutAccno());
        return rusult;
    }
    /**
     * @创建人 WH
     * @创建时间 2021/8/2 19:31
     * @注释 调用cmisLmt0010 和cmisLmt0013  cmisLmt0013ForXw
     */
    @PostMapping("/cmisLmt0013ForXw")
    protected ResultDto cmisLmt0013ForXw(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.cmisLmt0013ForXw(pvpLoanApp.getPvpSerno());

    }

    /**
     * @创建人 shenli
     * @创建时间 2021-8-16 10:28:43
     * @注释 零售出账申请撤消
     */
    @PostMapping("/pvploanapprevocationretail")
    protected ResultDto<Integer> pvpLoanAppRevocationRetail(@RequestBody PvpLoanApp pvpLoanApp) {
        return new ResultDto<Integer>(pvpLoanAppService.pvpLoanAppRevocationRetail(pvpLoanApp.getPvpSerno()));
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-8-16 10:28:43
     * @注释 零售出账申请撤消
     */
    @ApiOperation("生成影像补录任务")
    @PostMapping("/createimage")
    protected ResultDto<Integer> createimage(@RequestBody ResultInstanceDto resultInstanceDto) {
        pvpLoanAppService.createImageSpplInfo(resultInstanceDto.getBizId(),resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(), CmisCommonConstants.STD_SPPL_BIZ_TYPE_03,"");
        return new ResultDto<Integer>();
    }
    /**
     * @方法名称：sendctrcont
     * @方法描述：最高额授信协议做出账时，判断选择的产品是否存在于合同所选的分项下
     * @参数与返回说明：
     * @创建人：qw
     * @创建时间：2021-08-27 上午 9:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/isprdexist")
    protected ResultDto<Boolean> isPrdExist(@RequestBody Map map) {
        return pvpLoanAppService.isPrdExist(map);
    }

    // TODO: 2021/8/27 出账金额不允许大于最高可永信金额添加风险拦截

    /**
     * @创建人 qw
     * @创建时间 2021-9-2 15:35:43
     * @注释 出账申请时，判断是否满足小企业无还本续贷业务要求
     */
    @PostMapping("/isrepaycapitalreloan")
    protected ResultDto<Boolean> isRepayCapitalReloan(@RequestBody Map map) throws Exception {
        return pvpLoanAppService.isRepayCapitalReloan(map);
    }

    /**
     * @创建人 qw
     * @创建时间 2021-9-2 15:35:43
     * @注释 小企业无还本续贷调智能风控
     */
    @PostMapping("/pvprepaycapitalreloan")
    protected ResultDto<Map> pvpRepayCapitalReloan(@RequestBody Map map) {
        Map result = pvpLoanAppService.pvpRepayCapitalReloan(map);
        return new ResultDto<>(result);
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.Map>
     * @author hubp
     * @date 2021/9/6 14:01
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/annulpvploanapp")
    protected ResultDto annulPvpLoanApp(@RequestBody PvpLoanApp pvpLoanApp) {
        return pvpLoanAppService.annulPvpLoanApp(pvpLoanApp.getPvpSerno());
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<String>
     * @author shangzy
     * @date 2021/9/30 15:23
     * @version 1.0.0
     * @desc 获取
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getotherviewurl")
    protected ResultDto<String> getOtherViewUrl(@RequestBody PvpLoanApp pvpLoanApp) throws UnsupportedEncodingException {
        return new ResultDto<String>(pvpLoanAppService.getOtherViewUrl(pvpLoanApp));
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<String>
     * @author shangzy
     * @date 2021/9/30 15:23
     * @version 1.0.0
     * @desc 获取
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getreportviewurl")
    protected ResultDto<String> getReportViewUrl(@RequestBody PvpLoanApp pvpLoanApp) throws UnsupportedEncodingException {
        return new ResultDto<String>(pvpLoanAppService.getReportViewUrl(pvpLoanApp));
    }

    /**
     * @方法名称：getAccountList
     * @方法描述：获取结算账户信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("结算账户获取方法")
    @PostMapping("/getaccountdtodata")
    protected  ResultDto<List<AccountDto>> getAccountDtoData(@RequestBody QueryModel queryModel) {
        List<AccountDto> accountDto = pvpLoanAppService.getAccountDtoData(queryModel);
        return new ResultDto<List<AccountDto>>(accountDto);
    }


    /**
     * @方法名称：getAccount
     * @方法描述：获取账户信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("账户获取方法")
    @PostMapping("/getAccount")
    protected  ResultDto<List<AccountDto>> getAccount(@RequestBody QueryModel queryModel) {
        List<AccountDto> accountDto = pvpLoanAppService.getAccount(queryModel);
        return new ResultDto<List<AccountDto>>(accountDto);
    }

    /**
     * @方法名称：getAccountList
     * @方法描述：获取结算账户信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("根据合同号查询记录")
    @PostMapping("/selectbycontnoalive")
    protected  ResultDto<List<PvpLoanApp>> selectByContNo(@RequestBody String contNo) {
        List<PvpLoanApp> pvpLoanApp = pvpLoanAppService.selectByContNoAlive(contNo);
        return new ResultDto<List<PvpLoanApp>>(pvpLoanApp);
    }

    /**
     * @方法名称：getAccountList
     * @方法描述：获取结算账户信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("根据合同号查询审批中记录")
    @PostMapping("/selectbycontnorun")
    protected  ResultDto<PvpLoanApp> selectRunByContNo(@RequestBody String contNo) {
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectRunByContNo(contNo);
        return new ResultDto<PvpLoanApp>(pvpLoanApp);
    }

    /**
     * @方法名称：selectContByContNo
     * @方法描述：根据合同编号查询合同信息
     * @参数与返回说明： 最高额合同 普通贷款合同 资产池协议
     * @算法描述：
     * @创建人：xs
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("根据合同编号查询合同信息")
    @PostMapping("/selectContByContNo")
    protected  ResultDto<Map<String,String>> selectContByContNo(@RequestBody Map map) {
        String contNo = (String)map.get("contNo");
        return pvpLoanAppService.selectContByContNo(contNo);
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo>
     * @author qw
     * @date 2021/10/28 23:26
     * @version 1.0.0
     * @desc 根据客户编号查询结算账户信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping(value = "/getaccnolistbycusid")
    public ResultDto<List<LstacctinfoDto>> getAccNoListBycusId(@RequestBody QueryModel queryModel) {
        return new ResultDto<List<LstacctinfoDto>>(pvpLoanAppService.getAccNoListBycusId(queryModel)) ;
    }
}