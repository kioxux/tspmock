package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustRel
 * @类描述: lmt_grp_sub_prd_adjust_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:25:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGrpSubPrdAdjustRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信分项流水号 **/
	private String lmtSubSerno;
	
	/** 成员客户编号 **/
	private String cusId;
	
	/** 成员客户名称 **/
	private String cusName;
	
	/** 敞口额度 **/
	private java.math.BigDecimal openLmtAmt;
	
	/** 调剂后敞口额度 **/
	private java.math.BigDecimal adjustOpenLmtAmt;
	
	/** 低风险额度 **/
	private java.math.BigDecimal lowRiskLmtAmt;
	
	/** 调剂后低风险额度 **/
	private java.math.BigDecimal adjustLowRiskLmtAmt;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSubSerno
	 */
	public void setLmtSubSerno(String lmtSubSerno) {
		this.lmtSubSerno = lmtSubSerno == null ? null : lmtSubSerno.trim();
	}
	
    /**
     * @return LmtSubSerno
     */	
	public String getLmtSubSerno() {
		return this.lmtSubSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param openLmtAmt
	 */
	public void setOpenLmtAmt(java.math.BigDecimal openLmtAmt) {
		this.openLmtAmt = openLmtAmt;
	}
	
    /**
     * @return OpenLmtAmt
     */	
	public java.math.BigDecimal getOpenLmtAmt() {
		return this.openLmtAmt;
	}
	
	/**
	 * @param adjustOpenLmtAmt
	 */
	public void setAdjustOpenLmtAmt(java.math.BigDecimal adjustOpenLmtAmt) {
		this.adjustOpenLmtAmt = adjustOpenLmtAmt;
	}
	
    /**
     * @return AdjustOpenLmtAmt
     */	
	public java.math.BigDecimal getAdjustOpenLmtAmt() {
		return this.adjustOpenLmtAmt;
	}
	
	/**
	 * @param lowRiskLmtAmt
	 */
	public void setLowRiskLmtAmt(java.math.BigDecimal lowRiskLmtAmt) {
		this.lowRiskLmtAmt = lowRiskLmtAmt;
	}
	
    /**
     * @return LowRiskLmtAmt
     */	
	public java.math.BigDecimal getLowRiskLmtAmt() {
		return this.lowRiskLmtAmt;
	}
	
	/**
	 * @param adjustLowRiskLmtAmt
	 */
	public void setAdjustLowRiskLmtAmt(java.math.BigDecimal adjustLowRiskLmtAmt) {
		this.adjustLowRiskLmtAmt = adjustLowRiskLmtAmt;
	}
	
    /**
     * @return AdjustLowRiskLmtAmt
     */	
	public java.math.BigDecimal getAdjustLowRiskLmtAmt() {
		return this.adjustLowRiskLmtAmt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}