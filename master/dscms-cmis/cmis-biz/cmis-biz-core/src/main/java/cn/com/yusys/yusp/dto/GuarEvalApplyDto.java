package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalApply
 * @类描述: guar_eval_apply数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-26 15:21:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalApplyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String evalApplySerno;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 评估方式 **/
	private String evalWay;
	
	/** 认定价值币种 **/
	private String identyValueCurType;
	
	/** 认定价值 **/
	private java.math.BigDecimal identyValue;
	
	/** 折算人民币价值 **/
	private java.math.BigDecimal identyValueCny;
	
	/** 申请状态 **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;

	/** 原申请流水号 **/
	private String oldEvalApplySerno;

	/** 估值申请生效状态 **/
	private String evalApplyStatus;
	
	
	/**
	 * @param evalApplySerno
	 */
	public void setEvalApplySerno(String evalApplySerno) {
		this.evalApplySerno = evalApplySerno == null ? null : evalApplySerno.trim();
	}
	
    /**
     * @return EvalApplySerno
     */	
	public String getEvalApplySerno() {
		return this.evalApplySerno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param evalWay
	 */
	public void setEvalWay(String evalWay) {
		this.evalWay = evalWay == null ? null : evalWay.trim();
	}
	
    /**
     * @return EvalWay
     */	
	public String getEvalWay() {
		return this.evalWay;
	}
	
	/**
	 * @param identyValueCurType
	 */
	public void setIdentyValueCurType(String identyValueCurType) {
		this.identyValueCurType = identyValueCurType == null ? null : identyValueCurType.trim();
	}
	
    /**
     * @return IdentyValueCurType
     */	
	public String getIdentyValueCurType() {
		return this.identyValueCurType;
	}
	
	/**
	 * @param identyValue
	 */
	public void setIdentyValue(java.math.BigDecimal identyValue) {
		this.identyValue = identyValue;
	}
	
    /**
     * @return IdentyValue
     */	
	public java.math.BigDecimal getIdentyValue() {
		return this.identyValue;
	}
	
	/**
	 * @param identyValueCny
	 */
	public void setIdentyValueCny(java.math.BigDecimal identyValueCny) {
		this.identyValueCny = identyValueCny;
	}
	
    /**
     * @return IdentyValueCny
     */	
	public java.math.BigDecimal getIdentyValueCny() {
		return this.identyValueCny;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oldEvalApplySerno
	 */
	public void setOldEvalApplySerno(String oldEvalApplySerno) {
		this.oldEvalApplySerno = oldEvalApplySerno;
	}

	/**
	 * @return oldEvalApplySerno
	 */
	public String getOldEvalApplySerno() {
		return this.oldEvalApplySerno;
	}

	/**
	 * @param evalApplyStatus
	 */
	public void setEvalApplyStatus(String evalApplyStatus) {
		this.evalApplyStatus = evalApplyStatus;
	}

	/**
	 * @return evalApplyStatus
	 */
	public String getEvalApplyStatus() {
		return this.evalApplyStatus;
	}


}