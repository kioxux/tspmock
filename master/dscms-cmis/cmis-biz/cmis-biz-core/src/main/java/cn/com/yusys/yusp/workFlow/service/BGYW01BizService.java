package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpRateChgApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.req.Ln3055ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.resp.Ln3055RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2119:37
 * @desc 利率变更（小微）
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(BGYW01BizService.class);//定义log


    @Autowired
    private IqpRateChgAppService iqpRateChgAppService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //申请（小微）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG001.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG002.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG003.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG004.equals(bizType)) {
            iqpRateChgAppBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG032.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG033.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG040.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH04.equals(bizType)
                | CmisFlowConstants.FLOW_TYPE_TYPE_DHH04.equals(bizType)) {
            //协议
            iqpRateChgAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author tangxun
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 利率变更申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void iqpRateChgAppBizApp(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpRateChgApp iqpRateChgApp = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                iqpRateChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpRateChgAppService.updateSelective(iqpRateChgApp);
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                iqpRateChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);

                // 生成归档任务
                BigDecimal oldExecRateYear = iqpRateChgApp.getOldExecRateYear();//原执行年利率
                BigDecimal execRateYear = iqpRateChgApp.getExecRateYear();//变更后执行年利率
                //利率上调需要签订协议
                if(execRateYear.compareTo(oldExecRateYear) > 0 ) {
                    iqpRateChgApp.setIsSignAgr("Y");
                }else{
                    //利率下调不签协议，直接发送核心
                    String flag = this.sendHxToChangeRate(iqpSerno);
                    if(!"success".equals(flag)){
                        throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "核心返回："+flag);
                    }else{
                        this.changeAccLoan(iqpSerno);
                    }
                }
                iqpRateChgAppService.updateSelective(iqpRateChgApp);

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回操作:" + instanceInfo);
                    iqpRateChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpRateChgAppService.updateSelective(iqpRateChgApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpRateChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpRateChgAppService.updateSelective(iqpRateChgApp);
                log.info("-------业务否决：-- ----" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    // 集团客户处理
    private void iqpRateChgAppBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpRateChgApp iqpRateChgApp = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                //档案
                String bizType = instanceInfo.getBizType();
                String nodeId = instanceInfo.getNodeId();
                if ((CmisFlowConstants.FLOW_TYPE_TYPE_BG032.equals(bizType) && "243_5".equals(nodeId)) ||
                        (CmisFlowConstants.FLOW_TYPE_TYPE_BG033.equals(bizType))){
                    this.createCentralFileTask(bizType,iqpSerno,instanceInfo);
                }
                iqpRateChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpRateChgAppService.updateSelective(iqpRateChgApp);
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);

                iqpRateChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpRateChgAppService.updateSelective(iqpRateChgApp);
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                String flag = this.sendHxToChangeRate(iqpSerno);
                if(!"success".equals(flag)){
                    throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "核心返回："+flag);
                }else{
                    this.changeAccLoan(iqpSerno);
                }
                //根据需求 只有利率上调才需要签订协议
                log.info("开始系统生成档案归档信息");
                String cusId = iqpRateChgApp.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                docArchiveClientDto.setDocType("25");// 25 业务变更-利率变更
                docArchiveClientDto.setBizSerno(iqpSerno);
                docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(iqpRateChgApp.getManagerId());
                docArchiveClientDto.setManagerBrId(iqpRateChgApp.getManagerBrId());
                docArchiveClientDto.setInputId(iqpRateChgApp.getInputId());
                docArchiveClientDto.setInputBrId(iqpRateChgApp.getInputBrId());
                docArchiveClientDto.setContNo(iqpRateChgApp.getContNo());
                docArchiveClientDto.setBillNo(iqpRateChgApp.getBillNo());
                docArchiveClientDto.setLoanAmt(iqpRateChgApp.getLoanAmt());
                docArchiveClientDto.setStartDate(iqpRateChgApp.getLoanStartDate());
                docArchiveClientDto.setEndDate(iqpRateChgApp.getLoanEndDate());
                docArchiveClientDto.setPrdId(iqpRateChgApp.getPrdId());
                docArchiveClientDto.setPrdName(iqpRateChgApp.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成档案归档信息失败");
                }
                log.info("结束操作完成:" + instanceInfo);
                // 推送用印系统
                try {
                    cmisBizXwCommonService.sendYk(currentUserId,iqpSerno,iqpRateChgApp.getCusName());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)){
                    log.info("打回操作:" + instanceInfo);
                    iqpRateChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpRateChgAppService.updateSelective(iqpRateChgApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                log.info("否决操作结束:" + instanceInfo);
                iqpRateChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpRateChgAppService.updateSelective(iqpRateChgApp);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    public String sendHxToChangeRate(String iqpSerno){
        /*
         *************************************************************************
         * 调用核心利率变更接口
         * *************************************************************************/
        //查询数据
        IqpRateChgApp vo = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);
        //组装报文
        Ln3055ReqDto ln3055ReqDto = new Ln3055ReqDto();
        ln3055ReqDto.setDkjiejuh(vo.getBillNo());//贷款借据号
        ln3055ReqDto.setDkzhangh("");//贷款账号
        ln3055ReqDto.setKehuhaoo(vo.getCusId());//客户号
        ln3055ReqDto.setKehmingc(vo.getCusName());//客户名称
//                ln3055ReqDto.setQixiriqi(vo.getLoanStartDate());//起息日期
//                ln3055ReqDto.setDaoqriqi(vo.getLoanEndDate());//到期日期
        ln3055ReqDto.setDkqixian("");//贷款期限(月)
        ln3055ReqDto.setHuobdhao("01");//货币代号
//                ln3055ReqDto.setHetongje("");//合同金额
        ln3055ReqDto.setJiejuuje(vo.getLoanAmt());//借据金额
//                ln3055ReqDto.setLilvleix("");//利率类型
//                ln3055ReqDto.setNyuelilv("");//年/月利率标识
        ln3055ReqDto.setZhchlilv(vo.getExecRateYear().multiply(new BigDecimal(100)));//正常利率

        String nextRateAdjInterval = vo.getNextRateAdjInterval();//下一次利率调整间隔
        String nextRateAdjUnit = vo.getNextRateAdjUnit();//下一次利率调整间隔单位
        String newInureDate = vo.getNewInureDate();//利率生效日
        //
        String sy_flag = "";
        //利率调整方式
        if("01".equals(vo.getRateAdjMode())){//固定利率
            ln3055ReqDto.setLilvfdfs("0");//利率浮动方式  0--不浮动
            ln3055ReqDto.setLilvfdzh(new BigDecimal(0));//利率浮动值
        }else if("02".equals(vo.getRateAdjMode())){//浮动利率
            ln3055ReqDto.setLilvfdfs("1");//利率浮动方式  1--按值浮动
            ln3055ReqDto.setLilvfdzh(vo.getRateFloatPoint());//利率浮动值
            if("IMM".equals(vo.getRateAdjType())) {//立即调整
                ln3055ReqDto.setLilvtzfs("4");//即时调整
                ln3055ReqDto.setLilvtzzq("");//利率调整周期
            }else if("DDA".equals(vo.getRateAdjMode())) {//满一年调整
                ln3055ReqDto.setLilvtzfs("2");//按指定周期调整
                ln3055ReqDto.setLilvtzzq("");//利率调整周期
            }else if("NYF".equals(vo.getRateAdjMode())) {//每年1月1日调整
                ln3055ReqDto.setLilvtzfs("2");//按指定周期调整
                ln3055ReqDto.setLilvtzzq("");//利率调整周期
            }else if("FIX".equals(vo.getRateAdjMode())) {//固定日调整
                ln3055ReqDto.setLilvtzfs("2");//按指定周期调整
                ln3055ReqDto.setLilvtzzq("");//利率调整周期
            }else if("FIA".equals(vo.getRateAdjMode())) {//按月调整
                ln3055ReqDto.setLilvtzfs("2");//按指定周期调整
                PvpLoanApp pvpLoan = pvpLoanAppService.queryPvpLoanAppDataByBillNo(vo.getBillNo());
                String syFlag = "N";//  N是顺延   A不顺延
                if(Objects.nonNull(pvpLoan)){
                    if("0".equals(pvpLoan.getIsHolidayDelay())){// 1是 0否
                        syFlag = "A";
                    }
                }
                ln3055ReqDto.setLilvtzzq(nextRateAdjInterval + nextRateAdjUnit +syFlag + sy_flag + newInureDate );//利率调整周期
            }
        }
//
//
//                ln3055ReqDto.setZclilvbh("");//正常利率编号
//                ln3055ReqDto.setLlqxkdfs("");//利率期限靠档方式
//                ln3055ReqDto.setLilvqixx("");//利率期限
//                ln3055ReqDto.setYqllcklx("");//逾期利率参考类型
//                ln3055ReqDto.setLilvfend("");//利率分段
//                ln3055ReqDto.setYuqillbh("");//逾期利率编号
//                ln3055ReqDto.setYuqinyll("");//逾期年月利率
        ln3055ReqDto.setYuqililv(vo.getOverdueExecRate().multiply(new BigDecimal(100)));//逾期利率
//                ln3055ReqDto.setYuqitzfs("");//逾期利率调整方式
//                ln3055ReqDto.setYuqitzzq("");//逾期利率调整周期
//                ln3055ReqDto.setYqfxfdfs("");//逾期罚息浮动方式
        ln3055ReqDto.setYqfxfdzh(new BigDecimal("50"));//逾期罚息浮动值
//                ln3055ReqDto.setFulilvbh("");//复利利率编号
//                ln3055ReqDto.setFulilvny("");//复利利率年月标识
        ln3055ReqDto.setFulililv(vo.getCiExecRate().multiply(new BigDecimal(100)));//复利利率
//                ln3055ReqDto.setFulitzfs("");//复利利率调整方式
//                ln3055ReqDto.setFulitzzq("");//复利利率调整周期
//                ln3055ReqDto.setFulifdfs("");//复利利率浮动方式
        ln3055ReqDto.setFulifdzh(new BigDecimal("50"));//复利利率浮动值
//                ln3055ReqDto.setFlllcklx("");//复利利率参考类型
        ResultDto<Ln3055RespDto> ln3055ResultDto = dscms2CoreLnClientService.ln3055(ln3055ReqDto);
        String ln3055Code = Optional.ofNullable(ln3055ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3055Meesage = Optional.ofNullable(ln3055ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        log.info("交易返回{}", ln3055ResultDto);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3055Code)) {
            //  获取相关的值并解析
            return "success";
        } else {
            return ln3055Meesage;
        }
    }

    public Integer changeAccLoan(String iqpSerno){
        //查询数据
        IqpRateChgApp vo = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);
        String bill_no = vo.getBillNo();
        AccLoan accLoan = accLoanService.selectByBillNo(bill_no);
        String rateAdjMode = vo.getRateAdjMode();
        //1、利率调整方式
        accLoan.setRateAdjMode(rateAdjMode);
        //2、LPR授信利率期间
        accLoan.setLprRateIntval(vo.getLprRateIntval());
        //3、LPR利率
        accLoan.setCurtLprRate(vo.getCurtLprRate());
        //4、浮动点数 （固定利率没有）
        accLoan.setRateFloatPoint("01".equals(rateAdjMode) ? new BigDecimal(0) :vo.getRateFloatPoint());
        //5、执行利率(年)
        accLoan.setExecRateYear(vo.getExecRateYear());
        //6、逾期利率浮动比
        accLoan.setOverdueRatePefloat(vo.getOverdueRatePefloat());
        //7、逾期执行利率(年利率)
        accLoan.setOverdueExecRate(vo.getOverdueExecRate());
        //8、复息利率浮动比
        accLoan.setCiRatePefloat(vo.getCiRatePefloat());
        //9、复息执行利率(年利率)
        accLoan.setCiExecRate(vo.getCiExecRate());
        //10、利率调整选项（固定利率没有）
        accLoan.setRateAdjType( "01".equals(rateAdjMode) ? "" : vo.getRateAdjType());
        //11、下一次利率调整间隔 、单位
        accLoan.setNextRateAdjInterval(vo.getNextRateAdjInterval());
        accLoan.setNextRateAdjUnit(vo.getNextRateAdjUnit());
        //12、第一次调整日
        if(null != vo.getFirstAdjDate() && !"".equals(vo.getFirstAdjDate())){
            accLoan.setFirstAdjDate(vo.getFirstAdjDate().substring(8,10));
        }
        //13、调整后利率生效日 TODO

        return accLoanService.update(accLoan);
    }

    public void createCentralFileTask(String bizType , String iqpSerno,ResultInstanceDto instanceInfo){
        IqpRateChgApp iqpRateChgApp = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);
        try {
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpRateChgApp.getInputBrId());
            String orgType = resultDto.getData().getOrgType();
            //        0-总行部室
            //        1-异地支行（有分行）
            //        2-异地支行（无分行）
            //        3-异地分行
            //        4-中心支行
            //        5-综合支行
            //        6-对公支行
            //        7-零售支行
            if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType)){
                //新增临时档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(iqpRateChgApp.getIqpSerno());
                centralFileTaskdto.setCusId(iqpRateChgApp.getCusId());
                centralFileTaskdto.setCusName(iqpRateChgApp.getCusName());
                centralFileTaskdto.setBizType(bizType); //
                centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
                centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setInputId(iqpRateChgApp.getInputId());
                centralFileTaskdto.setInputBrId(iqpRateChgApp.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setTaskType("02"); // 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);
            }
        }catch (Exception e){
            log.info("利率变更审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW01.equals(flowCode);
    }
}
