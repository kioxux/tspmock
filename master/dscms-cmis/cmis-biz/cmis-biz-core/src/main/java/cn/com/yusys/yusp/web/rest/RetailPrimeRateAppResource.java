/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RetailPrimeRateApp;
import cn.com.yusys.yusp.service.RetailPrimeRateAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:18:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/retailprimerateapp")
public class RetailPrimeRateAppResource {
    @Autowired
    private RetailPrimeRateAppService retailPrimeRateAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RetailPrimeRateApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<RetailPrimeRateApp> list = retailPrimeRateAppService.selectAll(queryModel);
        return new ResultDto<List<RetailPrimeRateApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RetailPrimeRateApp>> index(QueryModel queryModel) {
        List<RetailPrimeRateApp> list = retailPrimeRateAppService.selectByModel(queryModel);
        return new ResultDto<List<RetailPrimeRateApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RetailPrimeRateApp> show(@PathVariable("serno") String serno) {
        RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<RetailPrimeRateApp>(retailPrimeRateApp);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<RetailPrimeRateApp> showRetailPrimeRate(@PathVariable("serno") String serno) {
        RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<RetailPrimeRateApp>(retailPrimeRateApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RetailPrimeRateApp> create(@RequestBody RetailPrimeRateApp retailPrimeRateApp) throws URISyntaxException {
        retailPrimeRateAppService.insert(retailPrimeRateApp);
        return new ResultDto<RetailPrimeRateApp>(retailPrimeRateApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RetailPrimeRateApp retailPrimeRateApp) throws URISyntaxException {
        int result = retailPrimeRateAppService.update(retailPrimeRateApp);
        return new ResultDto<Integer>(result);
    }


    @PostMapping("/repair")
    protected ResultDto<Integer> repair(@RequestBody RetailPrimeRateAppr RetailPrimeRateAppr ) throws URISyntaxException {
        int result = retailPrimeRateAppService.repair(RetailPrimeRateAppr);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = retailPrimeRateAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteiqpserno/{iqpSerno}")
    protected ResultDto<Integer> deleteiqpserno(@PathVariable("iqpSerno") String iqpSerno) {
        int result = retailPrimeRateAppService.deleteiqpserno(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = retailPrimeRateAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<RetailPrimeRateApp>> selectByModel(@RequestBody  QueryModel queryModel) {
        List<RetailPrimeRateApp> list = retailPrimeRateAppService.selectByModel(queryModel);
        return new ResultDto<List<RetailPrimeRateApp>>(list);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateretail")
    protected ResultDto<Integer> updateRetail(@RequestBody RetailPrimeRateApp retailPrimeRateApp) throws URISyntaxException {
        int result = retailPrimeRateAppService.updateRetail(retailPrimeRateApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectByIqpSerno
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyiqpserno")
    protected ResultDto<RetailPrimeRateApp> selectByModel(@RequestBody String iqpSerno) {
        RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppService.selectByIqpSerno(iqpSerno);
        return new ResultDto<RetailPrimeRateApp>(retailPrimeRateApp);
    }

    @PostMapping("/updateflowparam")
    protected ResultDto<String> updateflowparam(@RequestBody ResultInstanceDto instanceInfo) {
        return retailPrimeRateAppService.updateflowparam(instanceInfo);
    }
    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/9/16 10:49
     * @version 1.0.0
     * @desc 新增优惠利率申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addretailapp")
    protected ResultDto<RetailPrimeRateApp> addRetailApp(@RequestBody RetailPrimeRateApp retailPrimeRateApp) {
        RetailPrimeRateApp record =  retailPrimeRateAppService.addRetailApp(retailPrimeRateApp);
        return new ResultDto<>(record);
    }


    /**
     * @param
     * @return
     * @author shenli
     * @date 2021-9-26 16:13:46
     * @version 1.0.0
     * @desc 校验关联授信信息是否正确
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/checkretailprimerateapp")
    protected ResultDto<Integer> CheckRetailPrimeRateApp(@RequestBody RetailPrimeRateApp retailPrimeRateApp) {
        return new ResultDto<Integer>(retailPrimeRateAppService.CheckRetailPrimeRateApp(retailPrimeRateApp));
    }
}
