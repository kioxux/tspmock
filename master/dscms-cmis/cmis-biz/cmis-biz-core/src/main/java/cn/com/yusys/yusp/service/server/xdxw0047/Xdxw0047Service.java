package cn.com.yusys.yusp.service.server.xdxw0047;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.dto.server.xdxw0047.req.Xdxw0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0047.resp.Xdxw0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.xdxw0020.Xdxw0020Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @className Xdxw0047Service
 * @date 2021/10/7 20:36
 * @desc 风控推送增享贷白名单模型A审批信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Xdxw0047Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0020Service.class);

    @Autowired
    private CusLstZxdService cusLstZxdService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private LmtModelApprResultInfoService lmtModelApprResultInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    /**
     * @param xdxw0047DataReqDto
     * @return cn.com.yusys.yusp.dto.server.xdxw0047.resp.Xdxw0047DataRespDto
     * @author 王玉坤
     * @date 2021/10/7 20:37
     * @version 1.0.0
     * @desc 风控推送增享贷白名单模型A审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0047DataRespDto xdxw0047(Xdxw0047DataReqDto xdxw0047DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataReqDto));
        Xdxw0047DataRespDto xdxw0047DataRespDto = new Xdxw0047DataRespDto();
        CusLstZxdDto cusLstZxdDto = new CusLstZxdDto();
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
        try {
            // 1、必输项校验
            if (StringUtils.isNotBlank(xdxw0047DataReqDto.getReqtype())) {
                if (StringUtils.isBlank(xdxw0047DataReqDto.getCus_id())) {
                    throw new BizException(null, null, null, "客户号不能为空！");
                }
                //查询白名单信息
                cusLstZxdDto.setCusId(xdxw0047DataReqDto.getCus_id());
                // 默认报名单办理状态为--00 初始化
                cusLstZxdDto.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_00.key);
                //查询白名单
                cusLstZxdDto = cusLstZxdService.selectzxdbycusid(cusLstZxdDto);
                //判断非空
                if (cusLstZxdDto == null || StringUtils.isBlank(cusLstZxdDto.getSerno())) {
                    //如果流水号不是空 那么就是查到了
                    throw new BizException(null, "90009", null, "未查询到白名单信息！");
                }
                //判断非空
                if (StringUtils.isBlank(cusLstZxdDto.getContNo())) {
                    //如果流水号不是空 那么就是查到了
                    throw new BizException(null, "90009", null, "未查询到白名单合同编号！");
                }
                // 2021年11月17日22:18:28 hubp 问题编号：20211117-00082  获取合同信息，填充管护信息
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(cusLstZxdDto.getContNo());
                //判断非空
                if (Objects.isNull(ctrLoanCont)) {
                    //如果流水号不是空 那么就是查到了
                    throw new BizException(null, "90009", null, "未查询到白名单所关联的合同信息！");
                }
                // 模型审批通过
                if ("1".equals(xdxw0047DataReqDto.getReqtype())) {
                    if (StringUtils.isBlank(xdxw0047DataReqDto.getTask_id())) {
                        throw new BizException(null, "90009", null, "任务流水号不能为空！");
                    }
                    if (Objects.isNull(xdxw0047DataReqDto.getModel_amount())) {
                        throw new BizException(null, "90009", null, "模型建议金额不能为空！");
                    }
                    if (Objects.isNull(xdxw0047DataReqDto.getModel_rate())) {
                        throw new BizException(null, "90009", null, "模型建议利率不能为空！");
                    }
                    // 生成调查表信息
                    String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                    String openday = stringRedisTemplate.opsForValue().get("openDay");
                    lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
                    lmtSurveyReportMainInfo.setSurveySerno(serno);
                    lmtSurveyReportMainInfo.setCusId(xdxw0047DataReqDto.getCus_id());
                    lmtSurveyReportMainInfo.setCusName(cusLstZxdDto.getCusName());
                    lmtSurveyReportMainInfo.setCertCode(cusLstZxdDto.getCertCode());
                    lmtSurveyReportMainInfo.setListSerno(cusLstZxdDto.getSerno());
                    lmtSurveyReportMainInfo.setBizSerno(xdxw0047DataReqDto.getTask_id());
                    lmtSurveyReportMainInfo.setCertType("A");
                    lmtSurveyReportMainInfo.setInputDate(openday);
                    lmtSurveyReportMainInfo.setIntoTime(openday);
                    lmtSurveyReportMainInfo.setApproveStatus("000");
                    lmtSurveyReportMainInfo.setDataSource("00");
                    lmtSurveyReportMainInfo.setIsAutodivis("0");
                    lmtSurveyReportMainInfo.setIsStopOffline("0");
                    lmtSurveyReportMainInfo.setPrdId("SC010014");
                    lmtSurveyReportMainInfo.setPrdName("增享贷");
                    lmtSurveyReportMainInfo.setSurveyType("10");
                    lmtSurveyReportMainInfo.setOprType("01");
                    lmtSurveyReportMainInfo.setManagerId("");
                    lmtSurveyReportMainInfo.setManagerBrId("");
                    lmtSurveyReportMainInfo.setInputId(ctrLoanCont.getManagerId());
                    lmtSurveyReportMainInfo.setInputBrId(ctrLoanCont.getManagerBrId());
                    lmtSurveyReportMainInfoService.insertSelective(lmtSurveyReportMainInfo);

                    // 插入调查报告基本信息
                    logger.info("*********************XDXW0047:插入调查报告基本信息**********************");
                    lmtSurveyReportBasicInfo.setSurveySerno(serno);
                    lmtSurveyReportBasicInfo.setCusId(xdxw0047DataReqDto.getCus_id());
                    lmtSurveyReportBasicInfo.setCusName(cusLstZxdDto.getCusName());
                    lmtSurveyReportBasicInfo.setCertCode(cusLstZxdDto.getCertCode());
                    lmtSurveyReportBasicInfo.setPhone(cusLstZxdDto.getPhone());
                    lmtSurveyReportBasicInfo.setSpouseCusId(cusLstZxdDto.getSpouseCusId());
                    lmtSurveyReportBasicInfo.setSpouseName(cusLstZxdDto.getSpouseCusName());
                    lmtSurveyReportBasicInfo.setSpouseCertCode(cusLstZxdDto.getSpouseCertCode());
                    lmtSurveyReportBasicInfo.setSpousePhone(cusLstZxdDto.getSpousePhone());
                    lmtSurveyReportBasicInfo.setGuarMode("00");//增享贷担保方式
                    lmtSurveyReportBasicInfo.setModelAdviceAmt(xdxw0047DataReqDto.getModel_amount());
                    lmtSurveyReportBasicInfo.setModelAdviceRate(xdxw0047DataReqDto.getModel_rate());
                    lmtSurveyReportBasicInfo.setManagerId("");
                    lmtSurveyReportBasicInfo.setManagerBrId("");
                    lmtSurveyReportBasicInfo.setInputId(ctrLoanCont.getManagerId());
                    lmtSurveyReportBasicInfo.setInputBrId(ctrLoanCont.getManagerBrId());
                    logger.info("**********XDXW0047**插入调查报告基本信息开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                    lmtSurveyReportBasicInfoService.insertSelective(lmtSurveyReportBasicInfo);

                    // 模型审批结果信息
                    logger.info("*********************XDXW0047:模型审批结果信息**********************");
                    LmtModelApprResultInfo lmtModelApprResultInfo = new LmtModelApprResultInfo();
                    lmtModelApprResultInfo.setSurveySerno(serno);
                    lmtModelApprResultInfo.setModelApprTime(openday);
                    lmtModelApprResultInfo.setModelAmt(xdxw0047DataReqDto.getModel_amount());
                    lmtModelApprResultInfo.setModelRate(xdxw0047DataReqDto.getModel_rate());
                    lmtModelApprResultInfo.setModelRstStatus(DscmsBizXwEnum.XW_MODEL_RST_STATUS_10.key);
                    lmtModelApprResultInfo.setModelAdvice(DscmsBizXwEnum.XW_MODEL_RST_STATUS_10.value);
                    lmtModelApprResultInfo.setBizUniqueNo(xdxw0047DataReqDto.getTask_id());

                    logger.info("**********XDXW0047**插入模型审批结果信息开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                    lmtModelApprResultInfoService.insert(lmtModelApprResultInfo);
                    cusLstZxdDto.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_02.key);
                    //修改白名单表状态
                    CusLstZxd cusLstZxd = new CusLstZxd();
                    BeanUtils.copyProperties(cusLstZxdDto, cusLstZxd);

                    this.cusLstZxdService.updateSelective(cusLstZxd);

                    xdxw0047DataRespDto.setOpFlag("S");
                    xdxw0047DataRespDto.setOpMsg("调用接口成功");
                    logger.info("更新增享贷白名单成功");
                    logger.info("*****XDXW0047:调用接口成功**********");
                } else if ("2".equals(xdxw0047DataReqDto.getReqtype())) { // 模型拒绝
                    // 更新CUS_LST_ZXD的APPLY_STATUS状态
                    logger.info("*****XDXW0047:模型拒绝,更新CUS_LST_ZXD的APPLY_STATUS状态**********");
                    cusLstZxdDto.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_01.key);
                    CusLstZxd cusLstZxd = new CusLstZxd();
                    BeanUtils.copyProperties(cusLstZxdDto, cusLstZxd);

                    this.cusLstZxdService.updateSelective(cusLstZxd);

                    logger.info("更新增享贷白名单成功");
                    xdxw0047DataRespDto.setOpFlag("S");
                    xdxw0047DataRespDto.setOpMsg("调用接口成功");
                }
            } else {
                throw BizException.error(null, null, "请求类型不能为空!");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataReqDto));
        return xdxw0047DataRespDto;
    }
}
