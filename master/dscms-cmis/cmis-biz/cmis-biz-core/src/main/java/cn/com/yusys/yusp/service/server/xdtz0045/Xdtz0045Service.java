package cn.com.yusys.yusp.service.server.xdtz0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.req.Xdtz0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0045.resp.Xdtz0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 业务逻辑类:商贷分户实时查询
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdtz0045Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0045Service.class);
    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * 交易码：xdtz0045
     * 交易描述：商贷分户实时查询
     *
     * @param xdtz0045DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0045DataRespDto xdtz0045(Xdtz0045DataReqDto xdtz0045DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataReqDto));
        Xdtz0045DataRespDto xdtz0045DataRespDto = new Xdtz0045DataRespDto();
        try {
            // 从xdtz0045DataReqDto获取业务值进行业务逻辑处理
            String tranDate = xdtz0045DataReqDto.getTranDate();//交易日期
            String cusName = xdtz0045DataReqDto.getCusName();//客户名称
            String certNo = xdtz0045DataReqDto.getCertNo();//证件号码
            String loanAccount = xdtz0045DataReqDto.getLoanAccount();//贷款账号
            String reqPageNum = xdtz0045DataReqDto.getReqPageNum();//请求页码
            String totlRecordQnt = xdtz0045DataReqDto.getTotlRecordQnt();//总记录数
            String totlPageNum = xdtz0045DataReqDto.getTotlPageNum();//总页数

            //通过客户证件号查询客户信息
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCusByCertCode(certNo);
            if (Objects.isNull(cusBaseClientDto) || StringUtils.isBlank(cusBaseClientDto.getCusId())) {
                throw BizException.error(null, null, "未查询到客户信息！");
            }

            QueryModel queryModel = new QueryModel();
            //借据编号
            queryModel.addCondition("bill_no", loanAccount);
            // 总记录数
            queryModel.setSize(Integer.valueOf(totlRecordQnt));
            // 总页数
            queryModel.setPage(Integer.valueOf(totlPageNum));
            // 根据借据号查询本行其他贷款拖欠情况
            java.util.List<List> xdtz0045DataRespList = pvpLoanAppService.querySd0007LoanInfo(queryModel);


            if (CollectionUtils.nonEmpty(xdtz0045DataRespList)) {
                xdtz0045DataRespList.stream().forEach(s -> {
                    s.setLoanType("003");
                    s.setCertType(cusBaseClientDto.getCertType());
                    s.setCertNo(cusBaseClientDto.getCertCode());
                });
            }
            xdtz0045DataRespDto.setList(xdtz0045DataRespList);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, e.getMessage());
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, e.getMessage());
            logger.error(e.getMessage(), e);
            throw e;
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataRespDto));
        return xdtz0045DataRespDto;
    }
}
