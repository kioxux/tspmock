/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtRediDetail;
import cn.com.yusys.yusp.service.LmtRediDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRediDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-31 21:00:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtredidetail")
public class LmtRediDetailResource {
    @Autowired
    private LmtRediDetailService lmtRediDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRediDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRediDetail> list = lmtRediDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtRediDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRediDetail>> index(QueryModel queryModel) {
        List<LmtRediDetail> list = lmtRediDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtRediDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByLmtSerno")
    protected ResultDto<LmtRediDetail> selectByLmtSerno(@RequestBody Map condition) {
        String serno = (String) condition.get("lmtSerno");
        LmtRediDetail lmtRediDetail = lmtRediDetailService.selectByLmtSerno(serno);
        return new ResultDto<LmtRediDetail>(lmtRediDetail);
    }

    /**
     * 根据pkId更新授信复议申请书
     * @param lmtRediDetail
     * @return
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRediDetail lmtRediDetail){
        int result = lmtRediDetailService.update(lmtRediDetail);
        return new ResultDto<>(result);
    }


    /**
     * 根据pkId更新授信复议申请书
     * @param lmtRediDetail
     * @return
     */
    @PostMapping("/updateFysqb")
    protected ResultDto<Integer> updateFysqb(@RequestBody LmtRediDetail lmtRediDetail){
        int result = lmtRediDetailService.updateFysqb(lmtRediDetail);
        return new ResultDto<>(result);
    }

}
