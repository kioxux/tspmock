/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAcc
 * @类描述: intbank_org_admit_acc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 17:21:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "intbank_org_admit_acc")
public class IntbankOrgAdmitAcc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 台帐号 **/
	@Id
	@Column(name = "ACC_NO")
	private String accNo;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String replySerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 同业机构准入 **/
	@Column(name = "INTBANK_ORG_ADMIT", unique = false, nullable = true, length = 2000)
	private String intbankOrgAdmit;
	
	/** 经营情况和财务情况 **/
	@Column(name = "OPER_FINA_SITU", unique = false, nullable = true, length = 65535)
	private String operFinaSitu;
	
	/** 经营情况和财务情况图片路径 **/
	@Column(name = "OPER_FINA_SITU_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String operFinaSituPicturePath;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 2000)
	private String indgtResult;
	
	/** 期限 **/
	@Column(name = "TERM", unique = false, nullable = true, length = 10)
	private Integer term;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	
    /**
     * @return accNo
     */
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param intbankOrgAdmit
	 */
	public void setIntbankOrgAdmit(String intbankOrgAdmit) {
		this.intbankOrgAdmit = intbankOrgAdmit;
	}
	
    /**
     * @return intbankOrgAdmit
     */
	public String getIntbankOrgAdmit() {
		return this.intbankOrgAdmit;
	}
	
	/**
	 * @param operFinaSitu
	 */
	public void setOperFinaSitu(String operFinaSitu) {
		this.operFinaSitu = operFinaSitu;
	}
	
    /**
     * @return operFinaSitu
     */
	public String getOperFinaSitu() {
		return this.operFinaSitu;
	}
	
	/**
	 * @param operFinaSituPicturePath
	 */
	public void setOperFinaSituPicturePath(String operFinaSituPicturePath) {
		this.operFinaSituPicturePath = operFinaSituPicturePath;
	}
	
    /**
     * @return operFinaSituPicturePath
     */
	public String getOperFinaSituPicturePath() {
		return this.operFinaSituPicturePath;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}