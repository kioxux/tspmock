package cn.com.yusys.yusp.service.client.lmt.cmislmt0001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @class CmisLmt0001Service
 * @author 王玉坤
 * @date 2021/8/25 19:10
 * @version 1.0.0
 * @desc 占用额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0001Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0001Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param [cmisLmt0001ReqDto]
     * @return cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 占用额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0001RespDto cmisLmt0001(CmisLmt0001ReqDto cmisLmt0001ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001ReqDto));
        ResultDto<CmisLmt0001RespDto> cmisLmt0001ResultDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001ResultDto));

        String cmisLmt0001Code = Optional.ofNullable(cmisLmt0001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0001Meesage = Optional.ofNullable(cmisLmt0001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0001RespDto cmisLmt0001RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0001ResultDto.getCode()) && cmisLmt0001ResultDto.getData() != null
        && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0001ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0001RespDto = cmisLmt0001ResultDto.getData();
        } else {
            if (cmisLmt0001ResultDto.getData() != null) {
                cmisLmt0001Code = cmisLmt0001ResultDto.getData().getErrorCode();
                cmisLmt0001Meesage = cmisLmt0001ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0001Code = EpbEnum.EPB099999.key;
                cmisLmt0001Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0001Code, cmisLmt0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value);
        return cmisLmt0001RespDto;
    }
}
