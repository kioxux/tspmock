/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.service.CoopPlanAccInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAccInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 16:26:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplanaccinfo")
public class CoopPlanAccInfoResource {
    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanAccInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanAccInfo> list = coopPlanAccInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanAccInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanAccInfo>> index(QueryModel queryModel) {
        List<CoopPlanAccInfo> list = coopPlanAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanAccInfo>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPlanAccInfo>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPlanAccInfo> list = coopPlanAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanAccInfo>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query4widget")
    protected ResultDto<List<CoopPlanAccInfo>> widgetQuery(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPlanAccInfo> list = coopPlanAccInfoService.selectByModel4Widget(queryModel);
        return new ResultDto<List<CoopPlanAccInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CoopPlanAccInfo> show(@PathVariable("pkId") String pkId) {
        CoopPlanAccInfo coopPlanAccInfo = coopPlanAccInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<CoopPlanAccInfo>(coopPlanAccInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPlanAccInfo> create(@RequestBody CoopPlanAccInfo coopPlanAccInfo) throws URISyntaxException {
        coopPlanAccInfoService.insert(coopPlanAccInfo);
        return new ResultDto<CoopPlanAccInfo>(coopPlanAccInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPlanAccInfo coopPlanAccInfo) throws URISyntaxException {
        int result = coopPlanAccInfoService.update(coopPlanAccInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = coopPlanAccInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanAccInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryListByPartnerNo
     * @函数描述:根据合作方编号查询特色关联对公授信
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryListByPartnerNo")
    protected ResultDto<List<Map<String, Object>>> queryListByPartnerNo(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = coopPlanAccInfoService.queryListByPartnerNo(queryModel);
        return new ResultDto<>(list);
    }
    /**
     * @函数名称:queryLmtAppSubGuar
     * @函数描述:根据合作方编号查询一般担保关联对公授信
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryLmtAppSubGuar")
    protected ResultDto<List<Map<String, Object>>> queryLmtAppSubGuar(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = coopPlanAccInfoService.queryLmtAppSubGuar(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:byProNo
     * @函数描述:根据分项编号查询关联零售授信
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/byProNo")
    protected ResultDto<List<Map<String, Object>>> byProNo(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = coopPlanAccInfoService.byProNo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:getSumByLmtAmt
     * @函数描述:根据合作方编号计算特色关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSumByLmtAmt")
    protected ResultDto<Map<String, Object>> getSumByLmtAmt(@RequestBody QueryModel queryModel) {
        Map<String, Object> map = coopPlanAccInfoService.getSumByLmtAmt(queryModel);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getSumByLmtAmt
     * @函数描述:根据合作方编号查询一般担保关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSumByLmtAmtGuar")
    protected ResultDto<Map<String, Object>> getSumByLmtAmtGuar(@RequestBody QueryModel queryModel) {
        Map<String, Object> map = coopPlanAccInfoService.getSumByLmtAmtGuar(queryModel);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getSumByLmtAmt
     * @函数描述:计算申请额度之和
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSumByAppAmt")
    protected ResultDto<Map<String, Object>> getSumByAppAmt(@RequestBody QueryModel queryModel) {
        Map<String, Object> map = coopPlanAccInfoService.getSumByAppAmt(queryModel);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:queryDetail
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryDetail")
    protected ResultDto<List<CoopPlanAccInfo>> queryDetail(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPlanAccInfo> list = coopPlanAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanAccInfo>>(list);
    }
}
