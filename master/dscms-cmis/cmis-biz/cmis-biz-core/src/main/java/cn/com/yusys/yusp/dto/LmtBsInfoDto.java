package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.LmtBsInfo;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/6/22 20:46
 * @desc 专为移动OA封装DTO
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class LmtBsInfoDto {

    private List<LmtBsInfo> list;

    public List<LmtBsInfo> getList() {
        return list;
    }

    public void setList(List<LmtBsInfo> list) {
        this.list = list;
    }
}
