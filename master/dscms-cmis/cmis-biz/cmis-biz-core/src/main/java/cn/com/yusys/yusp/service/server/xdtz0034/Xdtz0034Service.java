package cn.com.yusys.yusp.service.server.xdtz0034;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.server.xdtz0034.req.Xdtz0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.resp.Xdtz0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0002.CmisBatch0002Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 接口处理类:根据客户号查询申请人是否有行内信用记录
 *
 * @author qx
 * @version 1.0
 */
@Service
public class Xdtz0034Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0034Service.class);
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private CmisBatch0002Service cmisBatch0002Service;//查询[核心系统-历史表-贷款账户主表]信息

    /**
     * 交易码：Xdtz0034
     * 交易描述：根据客户号查询申请人是否有行内信用记录
     *
     * @param xdtz0034DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0034DataRespDto xdtz0034(Xdtz0034DataReqDto xdtz0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataReqDto));
        //定义返回信息
        Xdtz0034DataRespDto xdtz0034DataRespDto = new Xdtz0034DataRespDto();
        try {
            //请求字段
            String cusId = xdtz0034DataReqDto.getCusId();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);
            List<AccLoan> accLoanList = accLoanService.selectByModel(queryModel);
            //根据客户号查询申请人是否有行内信用记录
            AtomicInteger count = new AtomicInteger();
            if (CollectionUtils.nonEmpty(accLoanList)) {
                accLoanList.stream().forEach(accLoan -> {
                    String billNo = accLoan.getBillNo();//贷款借据号
                    Cmisbatch0002ReqDto cmisbatch0002ReqDto = new Cmisbatch0002ReqDto();
                    cmisbatch0002ReqDto.setDkjiejuh(billNo);//贷款借据号
                    cmisbatch0002ReqDto.setKehuhaoo(cusId);//客户号
                    Cmisbatch0002RespDto cmisbatch0002RespDto = cmisBatch0002Service.cmisbatch0002(cmisbatch0002ReqDto);
                    count.addAndGet(cmisbatch0002RespDto.getCmisbatch0002List().size());
                });
            }
            if (count.get() > 0) {
                xdtz0034DataRespDto.setIsHavingCdtRecord(String.valueOf(count.get()));
            } else {
                xdtz0034DataRespDto.setIsHavingCdtRecord(SuccessEnum.CMIS_SUCCSESS.key);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataRespDto));
        return xdtz0034DataRespDto;
    }
}
