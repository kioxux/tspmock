package cn.com.yusys.yusp.service.server.xdxw0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0015.req.Xdxw0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0015.resp.*;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdxw0015Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人:
 * @创建时间: 2021-06-03 21:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0015Service.class);
    //用户信息模块
    @Autowired
    private AdminSmUserService adminSmUserService;
    // 机构信息模块
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    //客户信息模块
    @Autowired
    private CmisCusClientService cmisCusClientService;
    //个人额度查询
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private LmtAppMapper lmtAppMapper;
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;
    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Resource
    private LmtReplyAccMapper lmtReplyAccMapper;

    @Transactional
    public Xdxw0015DataRespDto xdxw0015(Xdxw0015DataReqDto xdxw0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataReqDto));
        Xdxw0015DataRespDto xdxw0015DataRespDto = null;
        // 客户列表
        List<String> cusIdList = new ArrayList<String>();
        try {
            String cusId = xdxw0015DataReqDto.getCusId();
            String certCode = xdxw0015DataReqDto.getCertNo();

            if (StringUtils.isNotEmpty(certCode)) {
                //通过客户证件号查询客户信息
                logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certCode));
                CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certCode)).orElse(new CusBaseClientDto());
                logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certCode), JSON.toJSONString(cusBaseClientDto));
                // 根据证件号查询客户编号
                cusId = cusBaseClientDto.getCusId();
            }
            cusIdList.add(cusId);
            CmisCus0012ReqDto cmisCus0012ReqDto = new CmisCus0012ReqDto();
            cmisCus0012ReqDto.setCusId(cusId);
            cmisCus0012ReqDto.setCertCode(xdxw0015DataReqDto.getCertNo());
            cmisCus0012ReqDto.setMrgCertType(xdxw0015DataReqDto.getCertType());
            // 法人代表为200400;实际控制人201200
            cmisCus0012ReqDto.setMrgType("200400,201200");
            cmisCus0012ReqDto.setQueryType("1");//多高管类别查询
            // 1、查询担任法人的企业客户号
            logger.info("***********调用客户服务cmiscus0012查询担任法人的企业客户号开始,查询参数为:{}", JSON.toJSONString(cmisCus0012ReqDto));
            ResultDto<CmisCus0012RespDto> cmisCus0012RespDtoResultDto = cmisCusClientService.cmiscus0012(cmisCus0012ReqDto);
            logger.info("***********调用客户服务cmiscus0012查询担任法人的企业客户号结束*END,返回结果为:{}", JSON.toJSONString(cmisCus0012RespDtoResultDto));
            if (cmisCus0012RespDtoResultDto != null) {
                CmisCus0012RespDto cmisCus0012RespDto = cmisCus0012RespDtoResultDto.getData();
                String code = cmisCus0012RespDtoResultDto.getCode();
                if (StringUtils.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    if (cmisCus0012RespDto != null) {
                        List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> list = cmisCus0012RespDto.getList();
                        if (list != null && list.size() > 0) {
                            List<String> cusIdRelList = list.stream().map(cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List::getCusIdRel).collect(Collectors.toList());
                            cusIdList.addAll(cusIdRelList);
                        }
                    }
                }
            }
            xdxw0015DataRespDto = this.getXdxw0015DataRespDto(cusIdList);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataRespDto));
        return xdxw0015DataRespDto;
    }

    /**
     * 获取返回信息
     *
     * @param cusIdList
     * @return
     */
    private Xdxw0015DataRespDto getXdxw0015DataRespDto(List<String> cusIdList) {
        logger.info("***********XDXW0015需要查询的客户信息包含如下:{}", JSON.toJSONString(cusIdList));
        Xdxw0015DataRespDto xdxw0015DataRespDto = new Xdxw0015DataRespDto();
        // 贷款信息列表
        List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLoan> listLoanList = new ArrayList<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLoan>();
        // 额度列表
        List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLmt> listLmtList = new ArrayList<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLmt>();
        // 合同列表
        List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont> listContList = new ArrayList<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont>();
        // 申请列表
        List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> listApplyList = new ArrayList<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply>();

        /*****************************查询出所有经营性贷款的产品编号***********************************/
        String prdType = "01,02,03,04,05,06,07,08";//对公贷款+小微经营性贷款
        String prdId = "";
        logger.info("***********调用iCmisCfgClientService查询产品类别*START**************产品类型" + prdType);
        ResultDto<List<CfgPrdBasicinfoDto>> resultDto = iCmisCfgClientService.queryCfgPrdBasicInfoByPrdType(prdType);
        String prdCode = resultDto.getCode();//返回结果
        if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
            List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoList = JSON.parseObject(JSON.toJSONString(resultDto.getData()), new TypeReference<List<CfgPrdBasicinfoDto>>() {
            });
            for (int i = 0; i < cfgPrdBasicinfoDtoList.size(); i++) {
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoList.get(i);
                prdId += cfgPrdBasicinfoDto.getPrdId();
                if (i < cfgPrdBasicinfoDtoList.size() - 1) {
                    prdId += "','";
                }
            }
        }
        logger.info("***********调用iCmisCfgClientService查询产品类别*END**************产品号" + prdId);
        /*****************************查询出所有经营性贷款的产品编号结束***********************************/
        //
        if (cusIdList != null && cusIdList.size() > 0) {
            for (int i = 0; i < cusIdList.size(); i++) {
                String cusId = cusIdList.get(i);
                // 贷款信息
                logger.info("***********XDXW0015贷款（经营贷）信息开始,查询参数为:{}", JSON.toJSONString(cusId));
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLoan> loanList = accLoanMapper.selectListLoanByCusId(cusId, prdId);
                logger.info("***********XDXW0015贷款（经营贷）信息结束*END,返回结果为:{}", JSON.toJSONString(loanList));
                if (loanList != null && loanList.size() > 0) {
                    for (ListLoan loanInfo : loanList) {
                        String managerId = loanInfo.getManagerId();
                        String orgNo = loanInfo.getOrgNo();
                        String orgName = this.getOrgName(orgNo);
                        String managerName = this.getManagerName(managerId);
                        loanInfo.setManagerName(managerName);
                        loanInfo.setOrgName(orgName);
                        // 获取直营团队类型
                        String teamType = getTeamType(managerId);
                        loanInfo.setTeamType(teamType);
                    }
                }
                // 插入贷款信息列表
                listLoanList.addAll(loanList);
                // 额度列表
                CmisLmt0023ReqDto cmisLmt0023ReqDto = new CmisLmt0023ReqDto();
                cmisLmt0023ReqDto.setCusId(cusId);
                logger.info("***********XDXW0015调用额度服务cmislmt0023查询开始,查询参数为:{}", JSON.toJSONString(cmisLmt0023ReqDto));
                ResultDto<CmisLmt0023RespDto> cmisLmt0023RespDtoResultDto = cmisLmtClientService.cmislmt0023(cmisLmt0023ReqDto);
                logger.info("***********XDXW0015调用额度服务cmislmt0023查询结束*END,返回结果为:{}", JSON.toJSONString(cmisLmt0023RespDtoResultDto));
                if (cmisLmt0023RespDtoResultDto != null) {
                    String cmisLmt0023RespDtoResultDtoCode = cmisLmt0023RespDtoResultDto.getCode();
                    CmisLmt0023RespDto cmisLmt0023RespDto = cmisLmt0023RespDtoResultDto.getData();
                    if (StringUtils.isNotEmpty(cmisLmt0023RespDtoResultDtoCode) && CmisBizConstants.NUM_ZERO.equals(cmisLmt0023RespDtoResultDtoCode)) {
                        if (cmisLmt0023RespDto != null) {
                            List<CmisLmt0023LmtListRespDto> cmisLmt0023LmtList = cmisLmt0023RespDto.getLmtList();
                            if (cmisLmt0023LmtList != null && cmisLmt0023LmtList.size() > 0) {
                                for (CmisLmt0023LmtListRespDto cmisLmt0023LmtListRespDto : cmisLmt0023LmtList) {
                                    if (cmisLmt0023LmtListRespDto == null) {
                                        continue;
                                    }
                                    //授信品种编号 20开头为经营性贷款 21开头为消费性贷款
                                    String lmtBizType = cmisLmt0023LmtListRespDto.getLmtBizType();
                                    if(StringUtil.isNotEmpty(lmtBizType)&&lmtBizType.startsWith("20")){//经营性贷款
                                        // 获取经理号返回的dto暂无该字段
                                        String managerId = cmisLmt0023LmtListRespDto.getManagerId();
                                        // 获取机构号返回的dto暂无该字段
                                        String orgNo = cmisLmt0023LmtListRespDto.getManagerBrId();
                                        // 获取机构名称
                                        String orgName = this.getOrgName(orgNo);
                                        // 获取经理名称
                                        String managerName = this.getManagerName(managerId);
                                        // 获取返回批复台账流水号
                                        String apprSeqno = cmisLmt0023LmtListRespDto.getApprSerno();
                                        // 授信总额
                                        BigDecimal avlAmt = cmisLmt0023LmtListRespDto.getAvlAmt();
                                        // 贷款类型 外部调用接口暂未返回
                                        String loanType = null;
                                        // 获取调查编号
                                        String surveySerno = null;
                                        String isMicroDept = "N";
                                        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectByPrimaryKey(apprSeqno);
                                        if (lmtCrdReplyInfo != null) {
                                            surveySerno = lmtCrdReplyInfo.getSurveySerno();
                                            String belgLine = lmtCrdReplyInfo.getBelgLine();
                                            if (StringUtils.isNotEmpty(belgLine) && "01".equals(belgLine)) {
                                                isMicroDept = "Y";
                                            }
                                        }
                                        ListLmt lmtInfo = new ListLmt();
                                        lmtInfo.setSerno(surveySerno);
                                        lmtInfo.setCusId(cusId);
                                        lmtInfo.setLoanType(loanType);
                                        lmtInfo.setIsMicroDept(isMicroDept);
                                        lmtInfo.setLmtAmt(avlAmt);
                                        lmtInfo.setManagerId(managerId);
                                        lmtInfo.setManagerName(managerName);
                                        lmtInfo.setOrgNo(orgNo);
                                        lmtInfo.setOrgName(orgName);
                                        // 获取直营团队类型
                                        String teamType = getTeamType(managerId);
                                        lmtInfo.setTeamType(teamType);
                                        listLmtList.add(lmtInfo);
                                    }
                                }
                            }
                        }
                    }
                }
                // 合同列表
                logger.info("***************根据客户号【{}】查询合同列表", cusId);
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont> contList = ctrLoanContMapper.selectListContByCusId(cusId, prdId);
                logger.info("***************根据客户号【{}】查询合同申请信息结束,查询结果信息【{}】", cusId, JSON.toJSONString(contList));
                if (CollectionUtils.nonNull(contList) && contList.size() > 0) {
                    for (int j = 0; j < contList.size(); j++) {
                        cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont listCont = contList.get(j);
                        //翻译客户经理名称
                        listCont.setManagerName(getManagerName(listCont.getManagerId()));
                        //翻译机构名称
                        listCont.setOrgName(getOrgName(listCont.getOrgNo()));
                        // 获取直营团队类型
                        String teamType = getTeamType(listCont.getManagerId());
                        listCont.setTeamType(teamType);
                    }
                    listContList.addAll(contList);
                }
                // 申请列表
                // 小贷申请
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> smlnApplyList = lmtSurveyReportMainInfoMapper.findSmlnApplyListByCusId(cusId);
                // 对公申请
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> comApplyList = lmtAppMapper.findComApplyListByCusId(cusId);
                //对公批复
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> lmtReplyAccList = lmtReplyAccMapper.findlmtReplyAccListByCusId(cusId);
                // 零售申请
                List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> preApplyList = iqpLoanAppMapper.findPreApplyList(cusId);
                // 插入申请列表
                if (smlnApplyList != null && smlnApplyList.size() > 0) {
                    listApplyList.addAll(smlnApplyList);
                }
                if (comApplyList != null && comApplyList.size() > 0) {
                    listApplyList.addAll(comApplyList);
                }
                if (lmtReplyAccList != null && lmtReplyAccList.size() > 0) {
                    listApplyList.addAll(lmtReplyAccList);
                }
                if (preApplyList != null && preApplyList.size() > 0) {
                    listApplyList.addAll(preApplyList);
                }
                logger.info("***************根据客户号【{}】查询合同申请信息结束,查询结果信息【{}】", cusId, JSON.toJSONString(listApplyList));
                if (CollectionUtils.nonNull(listApplyList) && listApplyList.size() > 0) {
                    listApplyList = listApplyList.parallelStream().map(ret -> {
                        cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply temp = new cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply();
                        BeanUtils.copyProperties(ret, temp);
                        //翻译客户经理名称
                        String managerId = temp.getManagerId();//客户经理号
                        temp.setManagerName(getManagerName(managerId));
                        //翻译翻译机构名称
                        String orgNo = temp.getOrgNo();//机构号
                        temp.setOrgName(getOrgName(orgNo));
                        //是否属于小微部门
                        String IsMicroDept = getIsMicroDept(managerId);
                        temp.setIsMicroDept(IsMicroDept);
                        // 获取直营团队类型
                        String teamType = getTeamType(managerId);
                        temp.setTeamType(teamType);
                        //返回列表
                        return temp;
                    }).collect(Collectors.toList());
                }
            }
        }
        xdxw0015DataRespDto.setListLoan(listLoanList);
        xdxw0015DataRespDto.setListLmt(listLmtList);
        xdxw0015DataRespDto.setListCont(listContList);
        xdxw0015DataRespDto.setListApply(listApplyList);
        return xdxw0015DataRespDto;
    }


    /**
     * 是否属于小微部门
     *
     * @param managerId
     * @return
     */
    private String getIsMicroDept(String managerId) {
        String IsMicroDept = "N";
        if (StringUtils.isNotBlank(managerId)) {//客户经理不能为空
            logger.info("************XDXW0015*第一步根据客户经理编号【{}】查询客户经理信息开始", managerId);
            ResultDto<GetIsXwUserDto> getIsXwUserDtoResultDto = adminSmUserService.getIsXWUserByLoginCode(managerId);
            logger.info("************XDXW0015*第一步根据客户经理编号【{}】查询客户经理信息结束,客户信息【{}】", managerId, JSON.toJSONString(getIsXwUserDtoResultDto));
            if (ResultDto.success().getCode().equals(getIsXwUserDtoResultDto.getCode())) {
                GetIsXwUserDto getIsXwUserDto = getIsXwUserDtoResultDto.getData();
                if (Objects.nonNull(getIsXwUserDto)) {
                    IsMicroDept = getIsXwUserDto.getIsXWUser();
                }
            }
        }
        return IsMicroDept;
    }

    /**
     * 获取直营团队类型
     *
     * @param managerId
     * @return
     */
    private String getTeamType(String managerId) {
        String teamType = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(managerId)) {
            logger.info("**************XDXW0015*根据客户经理号【{}】获取直营团队类型", managerId);
            AreaAdminUser areaAdminUser = areaAdminUserMapper.selectByPrimaryKey(managerId);
            logger.info("**************XDXW0015*根据客户经理号【{}】获取直营团队类型结束,查询结果信息【{}】", managerId, JSON.toJSONString(areaAdminUser));
            if (areaAdminUser != null) {
                teamType = areaAdminUser.getTeamType();
            }
        }
        return teamType;
    }


    /**
     * 获取客户经理名称
     *
     * @param managerId
     * @return
     */
    private String getManagerName(String managerId) {
        String managerName = null;
        if (StringUtils.isNotEmpty(managerId)) {
            ResultDto<cn.com.yusys.yusp.dto.AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
            if (adminSmUserDtoResultDto != null) {
                String adminSmUserCode = adminSmUserDtoResultDto.getCode();
                AdminSmUserDto adminSmUserDto = adminSmUserDtoResultDto.getData();
                if (StringUtils.isNotEmpty(adminSmUserCode) && CmisBizConstants.NUM_ZERO.equals(adminSmUserCode)) {
                    managerName = adminSmUserDto.getUserName();
                }
            }
        }
        return managerName;
    }

    /**
     * 获取机构名称
     *
     * @param orgNo
     * @return
     */
    private String getOrgName(String orgNo) {
        String orgName = null;
        // 获取经理号名称，机构号名称
        if (StringUtils.isNotEmpty(orgNo)) {
            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(orgNo);
            AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
            String adminSmOrgCode = adminSmOrgDtoResultDto.getCode();
            if (StringUtils.isNotEmpty(adminSmOrgCode) && CmisBizConstants.NUM_ZERO.equals(adminSmOrgCode)) {
                orgName = adminSmOrgDto.getOrgName();
            }
        }
        return orgName;
    }
}
