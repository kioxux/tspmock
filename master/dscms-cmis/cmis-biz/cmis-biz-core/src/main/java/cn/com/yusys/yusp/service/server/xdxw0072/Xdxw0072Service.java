package cn.com.yusys.yusp.service.server.xdxw0072;

import cn.com.yusys.yusp.domain.LmtGuareInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author xusheng
 * @Date 2021/5/10 16:20
 * @Version 1.0
 */
@Service
@Transactional
public class Xdxw0072Service {

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    public String getManagerIdBySurveyNo(@Param("surveyNo") String surveyNo){
        return lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(surveyNo).getManagerId();
    }
}
