/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.AccDisc;
import cn.com.yusys.yusp.domain.AccDiscClassificationDto;
import cn.com.yusys.yusp.dto.AccLoanInfoDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.AccDiscClassificationVo;
import cn.com.yusys.yusp.vo.AccDiscVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccDiscService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:42:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccDiscService {

    @Autowired
    private AccDiscMapper accDiscMapper;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccDisc selectByPrimaryKey(String pkId) {
        return accDiscMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccDisc> selectAll(QueryModel model) {
        List<AccDisc> records = (List<AccDisc>) accDiscMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccDisc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccDisc> list = accDiscMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccDisc record) {
        return accDiscMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccDisc record) {
        return accDiscMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccDisc record) {
        return accDiscMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccDisc record) {
        return accDiscMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accDiscMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accDiscMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selectForAccLoanInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 14:59
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccDisc> selectForAccLoanInfo(String cusId) {
        return accDiscMapper.selectforAccDiscInfo(cusId);
    }


    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 16:00
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccDisc> selectByContNo(String contNo) {
        return accDiscMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询贴现台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccDisc selectByBillNo(String billNo) {
        return accDiscMapper.selectByBillNo(billNo);
    }

    /**
     * 异步导出贴现台账列表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccDisc(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("draftStartDate desc");
            String apiUrl = "/api/accdisc/exportAccDisc";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return exportAccDiscData(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccDiscVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    private List<AccDiscVo> exportAccDiscData(QueryModel model) {
        List<AccDiscVo> accDiscVoList = new ArrayList<>();
        List<AccDisc> accDiscList = querymodelByCondition(model);
        for (AccDisc accDisc : accDiscList) {
            AccDiscVo accDiscVo = new AccDiscVo();
            BeanUtils.copyProperties(accDisc, accDiscVo);
            String managerIdName = OcaTranslatorUtils.getUserName(accDisc.getManagerId());// 客户经理名称
            String managerBrIdName = OcaTranslatorUtils.getOrgName(accDisc.getManagerBrId());// 责任机构名称
            accDiscVo.setManagerIdName(managerIdName);
            accDiscVo.setManagerBrIdName(managerBrIdName);
            accDiscVoList.add(accDiscVo);
        }
        return accDiscVoList;
    }

    /**
     * @param model 分页查询类
     * @函数名称:
     * @函数描述:综合查询（贷款风险分类支行汇总表（实时））
     * @参数与返回说明:
     * @算法描述:
     */
    public List<AccDiscClassificationDto> selectClassificationSubBranchSummary(QueryModel model) {
        List<AccDiscClassificationDto> accDiscClassificationDtoList = new ArrayList<>();
        List<String> orgCodes = new ArrayList<>();
        if (!StringUtils.isBlank((String) model.getCondition().get("managerBrId"))) {
            orgCodes.add((String) model.getCondition().get("managerBrId"));
        } else {
            User userinfo = SessionUtils.getUserInformation();
            String orgCode = userinfo.getOrg().getCode();
            // 总行部室
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(orgCode);
            AdminSmOrgDto adminSmOrgDto= new AdminSmOrgDto();
            if (null != resultDto && null != resultDto.getData()) {
                adminSmOrgDto= resultDto.getData();
            }
            String  orgType= "";
            if(adminSmOrgDto != null){
                orgType = adminSmOrgDto.getOrgType();
            }
            // 总行部室特殊处理
            if(StringUtils.isNotBlank(orgType) && "0".equals(orgType)){
                orgCode="000000";// 总行部室查全部
            }
            // 权限范围登陆机构及其下级机构
            orgCodes = commonService.getLowerOrgId(orgCode);
        }
        if (CollectionUtils.nonEmpty(orgCodes)) {
            orgCodes.stream().forEach(orgCode -> {
                PageHelper.startPage(model.getPage(), model.getSize());
                AccDiscClassificationDto accDiscClassificationDtos = accLoanService.selectClassificationSubBranchSummary(orgCode);
                if (null != accDiscClassificationDtos && StringUtils.isNotBlank(accDiscClassificationDtos.getManagerBrId())) {
                    accDiscClassificationDtoList.add(accDiscClassificationDtos);
                }
                PageHelper.clearPage();
            });
        }
        if (CollectionUtils.nonEmpty(accDiscClassificationDtoList)) {
            for (AccDiscClassificationDto accDiscClassificationDtos : accDiscClassificationDtoList) {
                //对公十级分类小计
                BigDecimal subtotal = accDiscClassificationDtos.getNormalCount1()//正常1类贷款余额
                        .add(accDiscClassificationDtos.getNormalCount2())//正常2类贷款余额
                        .add(accDiscClassificationDtos.getNormalCount3())//正常3类贷款余额
                        .add(accDiscClassificationDtos.getFollowCount1())//关注1类贷款余额
                        .add(accDiscClassificationDtos.getFollowCount2())//关注2类贷款余额
                        .add(accDiscClassificationDtos.getFollowCount3())//关注3类贷款余额
                        .add(accDiscClassificationDtos.getInferiorityCount1())//次级1类贷款余额
                        .add(accDiscClassificationDtos.getInferiorityCount2())//次级2类贷款余额
                        .add(accDiscClassificationDtos.getSuspiciousCount1())//可疑类
                        .add(accDiscClassificationDtos.getDamageCount1());//损失类
                //对私五级分类小计
                BigDecimal subtotal1 = accDiscClassificationDtos.getNormalCount4()//正常类贷款余额
                        .add(accDiscClassificationDtos.getFollowCount4())//关注类贷款余额
                        .add(accDiscClassificationDtos.getInferiorityCount4())//次级类贷款余额
                        .add(accDiscClassificationDtos.getSuspiciousCount4())//可疑类贷款余额
                        .add(accDiscClassificationDtos.getDamageCount4());//损失类贷款余额
                accDiscClassificationDtos.setSubtotal(subtotal);
                accDiscClassificationDtos.setSubtotal1(subtotal1);
                accDiscClassificationDtos.setManagerBrIdName(OcaTranslatorUtils.getOrgName(accDiscClassificationDtos.getManagerBrId()));
            }
        }
        return accDiscClassificationDtoList;
    }

    /**
     * 异步下载综合查询（贷款风险分类支行汇总表（实时））
     */
    public ProgressDto exportClassificationSubBranchSummary(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
//            String apiUrl = "/api/accdisc/exportClassificationSubBranchSummary";
//            String dataAuth = commonService.setDataAuthority(apiUrl);
//            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(dataAuth)) {
//                queryModeTemp.setDataAuth(dataAuth);
//            }
            return selectClassificationSubBranchSummary(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccDiscClassificationVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: querymodelByCondition
     * @方法描述: 根据查询条件查询台账信息并返回
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccDisc> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccDisc> list = accDiscMapper.querymodelByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据客户编号获取台账信息
     *
     * @param cusId
     * @return
     */
    public List<AccLoanInfoDto> getAllBusAccInfo(String cusId) {
        return accDiscMapper.getAllBusAccInfo(cusId);
    }
}
