package cn.com.yusys.yusp.service.server.Xdht0031;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务逻辑类:根据核心客户号查询经营性贷款合同额度
 * @author rzx
 * @version 1.0
 */
@Service
public class Xdht0031Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0031Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易码：xdht0031
     * 交易描述：根据核心客户号查询经营性贷款合同额度
     * @param
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0031DataRespDto xdht0031(String cusId) throws Exception {
        //BigDecimal applyAmt = ctrLoanContMapper.queryOperatingLoanContAmt(cusId);
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, cusId);
        Xdht0031DataRespDto xdht0031DataRespDto = ctrLoanContMapper.queryOperatingLoanContAmt(cusId);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataRespDto));
        return ctrLoanContMapper.queryOperatingLoanContAmt(cusId);
    }
}
