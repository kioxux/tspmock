/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpStpUnstpIntApp;
import cn.com.yusys.yusp.service.IqpStpUnstpIntAppService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpStpUnstpIntAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-13 18:08:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpstpunstpintapp")
public class IqpStpUnstpIntAppResource {
    @Autowired
    private IqpStpUnstpIntAppService iqpStpUnstpIntAppService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpStpUnstpIntApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpStpUnstpIntApp> list = iqpStpUnstpIntAppService.selectAll(queryModel);
        return new ResultDto<List<IqpStpUnstpIntApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:  queryModel QueryModel
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpStpUnstpIntApp>> index(QueryModel queryModel) {
        List<IqpStpUnstpIntApp> list = iqpStpUnstpIntAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpStpUnstpIntApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpStpUnstpIntApp> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpStpUnstpIntApp iqpStpUnstpIntApp = iqpStpUnstpIntAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpStpUnstpIntApp>(iqpStpUnstpIntApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpStpUnstpIntApp> create(@RequestBody IqpStpUnstpIntApp iqpStpUnstpIntApp) throws URISyntaxException {
        iqpStpUnstpIntAppService.insert(iqpStpUnstpIntApp);
        return new ResultDto<IqpStpUnstpIntApp>(iqpStpUnstpIntApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpStpUnstpIntApp iqpStpUnstpIntApp) throws URISyntaxException {
        int result = iqpStpUnstpIntAppService.update(iqpStpUnstpIntApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpStpUnstpIntAppService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpStpUnstpIntAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveIqpStpIntAppLeadInfo
     * @函数描述:停息向导页面保存数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveIqpStpIntAppLeadInfo/{billNo}")
    protected ResultDto<Map> saveIqpStpIntAppLeadInfo(@PathVariable String billNo){
        Map map = iqpStpUnstpIntAppService.saveIqpStpIntAppLeadInfo(billNo);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:saveIqpUnStpIntAppLeadInfo
     * @函数描述:恢复计息向导页面保存数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveIqpUnStpIntAppLeadInfo/{billNo}")
    protected ResultDto<Map> saveIqpUnStpIntAppLeadInfo(@PathVariable String billNo){
        Map map = iqpStpUnstpIntAppService.saveIqpUnStpIntAppLeadInfo(billNo);
        return new ResultDto<Map>(map);
    }
}
