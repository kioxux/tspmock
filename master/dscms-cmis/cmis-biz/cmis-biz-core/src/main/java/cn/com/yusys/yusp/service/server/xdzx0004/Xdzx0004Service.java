package cn.com.yusys.yusp.service.server.xdzx0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.web.server.xdzx0004.BizXdzx0004Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdzx0004Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdzx0004Service.class);

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdzx0004DataRespDto xdzx0004(Xdzx0004DataReqDto xdzx0004DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataReqDto));
        Xdzx0004DataRespDto xdzx0004DataRespDto = new Xdzx0004DataRespDto();
        try {
            xdzx0004DataRespDto = creditReportQryLstMapper.queryApproveStatusBySerno(xdzx0004DataReqDto);
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, e.getMessage());
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, e);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataRespDto));
        return xdzx0004DataRespDto;
    }
}
