package cn.com.yusys.yusp.service.server.xdzx0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.server.xdzx0003.req.Xdzx0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.resp.Xdzx0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 接口处理类:征信查询授权状态同步
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdzx0003Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzx0003Service.class);

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    /**
     * 征信查询授权状态同步
     *
     * @param xdzx0003DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdzx0003DataRespDto syncApproveStatus(Xdzx0003DataReqDto xdzx0003DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataReqDto));
        Xdzx0003DataRespDto xdzx0003DataRespDto = new Xdzx0003DataRespDto();
        String approveStatus = xdzx0003DataReqDto.getApproveStatus();
        String crqlSerno = xdzx0003DataReqDto.getCrqlSerno();
        String ImageNo = xdzx0003DataReqDto.getIamgeNo();
        try {
            creditReportQryLstMapper.updateApproveStatusforzx(crqlSerno, approveStatus, ImageNo);

            if (Objects.equals("997", approveStatus)) {
                ResultInstanceDto resultInstanceDto = new ResultInstanceDto();
                resultInstanceDto.setCurrentUserId("71013782");
                creditReportQryLstService.handleBusinessDataAfterEnd(crqlSerno, resultInstanceDto);
                //小微客户经理发起的征信查询调用新微贷贷款申请接口
                try {
                    creditReportQryLstService.xwd001(crqlSerno);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            xdzx0003DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdzx0003DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, e.getMessage());
            xdzx0003DataRespDto.setOpFlag(EpbEnum.EPB099999.key);
            xdzx0003DataRespDto.setOpFlag(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, e.getMessage());
            xdzx0003DataRespDto.setOpFlag(EpbEnum.EPB099999.key);
            xdzx0003DataRespDto.setOpFlag(e.getMessage());
            throw e;
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataRespDto));
        return xdzx0003DataRespDto;
    }
}
