package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "对私贷款实时报表", fileType = ExcelCsv.ExportFileType.XLS)
public class AccLoanCusVo {

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
   借据编号
    */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    产品名称
     */
    @ExcelField(title = "产品名称", viewLength = 20)
    private String prdName;

    /*
    币种
     */
    @ExcelField(title = "币种", viewLength = 20)
    private String curType;

    /*
    贷款余额
     */
    @ExcelField(title = "贷款余额", viewLength = 20)
    private java.math.BigDecimal loanBalance;

    /*
    贷款起始日
     */
    @ExcelField(title = "贷款起始日", viewLength = 20)
    private String loanStartDate;

    /*
    贷款到期日
     */
    @ExcelField(title = "贷款到期日", viewLength = 20)
    private String loanEndDate;

    /*
    科目编号
     */
    @ExcelField(title = "科目编号", viewLength = 20)
    private String subjectNo;

    /*
    科目名称
     */
    @ExcelField(title = "科目名称", viewLength = 20)
    private String subjectName;

    /*
    客户经理员工号
     */
    @ExcelField(title = "客户经理员工号", viewLength = 20)
    private String managerId;

    /*
    客户经理姓名
     */
    @ExcelField(title = "客户经理姓名", viewLength = 20)
    private String managerIdName;

    /*
    责任机构号
     */
    @ExcelField(title = "责任机构号", viewLength = 20)
    private String managerBrId;

    /*
    责任机构名称
     */
    @ExcelField(title = "责任机构名称", viewLength = 20)
    private String managerBrIdName;

    /*
    账户机构号
     */
    @ExcelField(title = "账户机构号", viewLength = 20)
    private String finaBrId;

    /*
    账务机构名称
     */
    @ExcelField(title = "账务机构名称", viewLength = 20)
    private String finaBrIdName;

    /*
    五级分类
     */
    @ExcelField(title = "五级分类", viewLength = 20)
    private String fiveClass;

    /*
    十级分类
     */
    @ExcelField(title = "十级分类", viewLength = 20)
    private String tenClass;



    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(String subjectNo) {
        this.subjectNo = subjectNo;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getTenClass() {
        return tenClass;
    }

    public void setTenClass(String tenClass) {
        this.tenClass = tenClass;
    }

}
