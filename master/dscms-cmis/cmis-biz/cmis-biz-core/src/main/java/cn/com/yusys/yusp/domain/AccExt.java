/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccExt
 * @类描述: acc_ext数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-23 16:31:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_ext")
public class AccExt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 展期借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "EXT_BILL_NO")
	private String extBillNo;

	/** 展期协议编号 **/
	@Column(name = "EXT_CTR_NO", unique = false, nullable = true, length = 40)
	private String extCtrNo;

	/** 原借据编号 **/
	@Column(name = "OLD_BILL_NO", unique = false, nullable = true, length = 40)
	private String oldBillNo;

	/** 原合同编号 **/
	@Column(name = "OLD_CONT_NO", unique = false, nullable = true, length = 40)
	private String oldContNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 原币种 STD_ZB_CUR_TYP **/
	@Column(name = "FOUNT_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String fountCurType;

	/** 原贷款金额 **/
	@Column(name = "FOUNT_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fountLoanAmt;

	/** 原贷款余额 **/
	@Column(name = "FOUNT_LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fountLoanBalance;

	/** 原起贷日期 **/
	@Column(name = "FOUNT_START_DATE", unique = false, nullable = true, length = 10)
	private String fountStartDate;

	/** 原止贷日期 **/
	@Column(name = "FOUNT_END_DATE", unique = false, nullable = true, length = 10)
	private String fountEndDate;

	/** 原执行利率（年） **/
	@Column(name = "OLD_REALITY_IR_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldRealityIrY;

	/** 原执行利率(月) **/
	@Column(name = "OLD_REALITY_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldRealityIrM;

	/** 原逾期利率（年） **/
	@Column(name = "OLD_OVERDUE_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldOverdueRateY;

	/** 原违约利率（年） **/
	@Column(name = "OLD_DEFAULT_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldDefaultRateY;

	/** 展期金额 **/
	@Column(name = "EXT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extAmt;

	/** 展期到期日期 **/
	@Column(name = "EXT_END_DATE", unique = false, nullable = true, length = 10)
	private String extEndDate;

	/** 利率依据方式 STD_ZB_IR_WAY **/
	@Column(name = "IR_ACCORD_TYPE", unique = false, nullable = true, length = 5)
	private String irAccordType;

	/** 利率种类 STD_ZB_IR_TYP **/
	@Column(name = "IR_TYPE", unique = false, nullable = true, length = 6)
	private String irType;

	/** 基准利率（年） **/
	@Column(name = "RULING_IR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIr;

	/** 对应基准利率(月) **/
	@Column(name = "RULING_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIrM;

	/** 计息方式 STD_ZB_LOAN_RAT_TYPE **/
	@Column(name = "LOAN_RAT_TYPE", unique = false, nullable = true, length = 5)
	private String loanRatType;

	/** 利率调整方式 STD_ZB_RADJ_TYP **/
	@Column(name = "IR_ADJUST_TYPE", unique = false, nullable = true, length = 5)
	private String irAdjustType;

	/** 利率调整周期(月) **/
	@Column(name = "IR_ADJUST_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal irAdjustTerm;

	/** 调息方式 STD_ZB_PRA_MODE **/
	@Column(name = "PRA_TYPE", unique = false, nullable = true, length = 5)
	private String praType;

	/** 利率形式 STD_ZB_RATE_TYPE **/
	@Column(name = "RATE_TYPE", unique = false, nullable = true, length = 5)
	private String rateType;

	/** 正常利率浮动方式 STD_ZB_RFLOAT_TYP **/
	@Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String irFloatType;

	/** 利率浮动百分比 **/
	@Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal irFloatRate;

	/** 固定加点值 **/
	@Column(name = "IR_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal irFloatPoint;

	/** 展期执行利率（年） **/
	@Column(name = "EXT_REALITY_IR_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extRealityIrY;

	/** 展期执行利率(月) **/
	@Column(name = "EXT_REALITY_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extRealityIrM;

	/** 逾期利率浮动方式 STD_ZB_RFLOAT_TYP **/
	@Column(name = "OVERDUE_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String overdueFloatType;

	/** 逾期利率浮动加点值 **/
	@Column(name = "OVERDUE_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overduePoint;

	/** 逾期利率浮动百分比 **/
	@Column(name = "OVERDUE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRate;

	/** 逾期利率（年） **/
	@Column(name = "OVERDUE_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRateY;

	/** 违约利率浮动方式 STD_ZB_RFLOAT_TYP **/
	@Column(name = "DEFAULT_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String defaultFloatType;

	/** 违约利率浮动加点值 **/
	@Column(name = "DEFAULT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultPoint;

	/** 违约利率浮动百分比 **/
	@Column(name = "DEFAULT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRate;

	/** 违约利率（年） **/
	@Column(name = "DEFAULT_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRateY;

	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;

	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 台账状态 STD_ZB_ACC_TYP **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;


	/**
	 * @param extBillNo
	 */
	public void setExtBillNo(String extBillNo) {
		this.extBillNo = extBillNo;
	}

    /**
     * @return extBillNo
     */
	public String getExtBillNo() {
		return this.extBillNo;
	}

	/**
	 * @param extCtrNo
	 */
	public void setExtCtrNo(String extCtrNo) {
		this.extCtrNo = extCtrNo;
	}

    /**
     * @return extCtrNo
     */
	public String getExtCtrNo() {
		return this.extCtrNo;
	}

	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo;
	}

    /**
     * @return oldBillNo
     */
	public String getOldBillNo() {
		return this.oldBillNo;
	}

	/**
	 * @param oldContNo
	 */
	public void setOldContNo(String oldContNo) {
		this.oldContNo = oldContNo;
	}

    /**
     * @return oldContNo
     */
	public String getOldContNo() {
		return this.oldContNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param fountCurType
	 */
	public void setFountCurType(String fountCurType) {
		this.fountCurType = fountCurType;
	}

    /**
     * @return fountCurType
     */
	public String getFountCurType() {
		return this.fountCurType;
	}

	/**
	 * @param fountLoanAmt
	 */
	public void setFountLoanAmt(java.math.BigDecimal fountLoanAmt) {
		this.fountLoanAmt = fountLoanAmt;
	}

    /**
     * @return fountLoanAmt
     */
	public java.math.BigDecimal getFountLoanAmt() {
		return this.fountLoanAmt;
	}

	/**
	 * @param fountLoanBalance
	 */
	public void setFountLoanBalance(java.math.BigDecimal fountLoanBalance) {
		this.fountLoanBalance = fountLoanBalance;
	}

    /**
     * @return fountLoanBalance
     */
	public java.math.BigDecimal getFountLoanBalance() {
		return this.fountLoanBalance;
	}

	/**
	 * @param fountStartDate
	 */
	public void setFountStartDate(String fountStartDate) {
		this.fountStartDate = fountStartDate;
	}

    /**
     * @return fountStartDate
     */
	public String getFountStartDate() {
		return this.fountStartDate;
	}

	/**
	 * @param fountEndDate
	 */
	public void setFountEndDate(String fountEndDate) {
		this.fountEndDate = fountEndDate;
	}

    /**
     * @return fountEndDate
     */
	public String getFountEndDate() {
		return this.fountEndDate;
	}

	/**
	 * @param oldRealityIrY
	 */
	public void setOldRealityIrY(java.math.BigDecimal oldRealityIrY) {
		this.oldRealityIrY = oldRealityIrY;
	}

    /**
     * @return oldRealityIrY
     */
	public java.math.BigDecimal getOldRealityIrY() {
		return this.oldRealityIrY;
	}

	/**
	 * @param oldRealityIrM
	 */
	public void setOldRealityIrM(java.math.BigDecimal oldRealityIrM) {
		this.oldRealityIrM = oldRealityIrM;
	}

    /**
     * @return oldRealityIrM
     */
	public java.math.BigDecimal getOldRealityIrM() {
		return this.oldRealityIrM;
	}

	/**
	 * @param oldOverdueRateY
	 */
	public void setOldOverdueRateY(java.math.BigDecimal oldOverdueRateY) {
		this.oldOverdueRateY = oldOverdueRateY;
	}

    /**
     * @return oldOverdueRateY
     */
	public java.math.BigDecimal getOldOverdueRateY() {
		return this.oldOverdueRateY;
	}

	/**
	 * @param oldDefaultRateY
	 */
	public void setOldDefaultRateY(java.math.BigDecimal oldDefaultRateY) {
		this.oldDefaultRateY = oldDefaultRateY;
	}

    /**
     * @return oldDefaultRateY
     */
	public java.math.BigDecimal getOldDefaultRateY() {
		return this.oldDefaultRateY;
	}

	/**
	 * @param extAmt
	 */
	public void setExtAmt(java.math.BigDecimal extAmt) {
		this.extAmt = extAmt;
	}

    /**
     * @return extAmt
     */
	public java.math.BigDecimal getExtAmt() {
		return this.extAmt;
	}

	/**
	 * @param extEndDate
	 */
	public void setExtEndDate(String extEndDate) {
		this.extEndDate = extEndDate;
	}

    /**
     * @return extEndDate
     */
	public String getExtEndDate() {
		return this.extEndDate;
	}

	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType;
	}

    /**
     * @return irAccordType
     */
	public String getIrAccordType() {
		return this.irAccordType;
	}

	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType;
	}

    /**
     * @return irType
     */
	public String getIrType() {
		return this.irType;
	}

	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

    /**
     * @return rulingIr
     */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}

	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}

    /**
     * @return rulingIrM
     */
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}

	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType;
	}

    /**
     * @return loanRatType
     */
	public String getLoanRatType() {
		return this.loanRatType;
	}

	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType;
	}

    /**
     * @return irAdjustType
     */
	public String getIrAdjustType() {
		return this.irAdjustType;
	}

	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(java.math.BigDecimal irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}

    /**
     * @return irAdjustTerm
     */
	public java.math.BigDecimal getIrAdjustTerm() {
		return this.irAdjustTerm;
	}

	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType;
	}

    /**
     * @return praType
     */
	public String getPraType() {
		return this.praType;
	}

	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

    /**
     * @return rateType
     */
	public String getRateType() {
		return this.rateType;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}

    /**
     * @return irFloatType
     */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

    /**
     * @return irFloatRate
     */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param irFloatPoint
	 */
	public void setIrFloatPoint(java.math.BigDecimal irFloatPoint) {
		this.irFloatPoint = irFloatPoint;
	}

    /**
     * @return irFloatPoint
     */
	public java.math.BigDecimal getIrFloatPoint() {
		return this.irFloatPoint;
	}

	/**
	 * @param extRealityIrY
	 */
	public void setExtRealityIrY(java.math.BigDecimal extRealityIrY) {
		this.extRealityIrY = extRealityIrY;
	}

    /**
     * @return extRealityIrY
     */
	public java.math.BigDecimal getExtRealityIrY() {
		return this.extRealityIrY;
	}

	/**
	 * @param extRealityIrM
	 */
	public void setExtRealityIrM(java.math.BigDecimal extRealityIrM) {
		this.extRealityIrM = extRealityIrM;
	}

    /**
     * @return extRealityIrM
     */
	public java.math.BigDecimal getExtRealityIrM() {
		return this.extRealityIrM;
	}

	/**
	 * @param overdueFloatType
	 */
	public void setOverdueFloatType(String overdueFloatType) {
		this.overdueFloatType = overdueFloatType;
	}

    /**
     * @return overdueFloatType
     */
	public String getOverdueFloatType() {
		return this.overdueFloatType;
	}

	/**
	 * @param overduePoint
	 */
	public void setOverduePoint(java.math.BigDecimal overduePoint) {
		this.overduePoint = overduePoint;
	}

    /**
     * @return overduePoint
     */
	public java.math.BigDecimal getOverduePoint() {
		return this.overduePoint;
	}

	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}

    /**
     * @return overdueRate
     */
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}

	/**
	 * @param overdueRateY
	 */
	public void setOverdueRateY(java.math.BigDecimal overdueRateY) {
		this.overdueRateY = overdueRateY;
	}

    /**
     * @return overdueRateY
     */
	public java.math.BigDecimal getOverdueRateY() {
		return this.overdueRateY;
	}

	/**
	 * @param defaultFloatType
	 */
	public void setDefaultFloatType(String defaultFloatType) {
		this.defaultFloatType = defaultFloatType;
	}

    /**
     * @return defaultFloatType
     */
	public String getDefaultFloatType() {
		return this.defaultFloatType;
	}

	/**
	 * @param defaultPoint
	 */
	public void setDefaultPoint(java.math.BigDecimal defaultPoint) {
		this.defaultPoint = defaultPoint;
	}

    /**
     * @return defaultPoint
     */
	public java.math.BigDecimal getDefaultPoint() {
		return this.defaultPoint;
	}

	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

    /**
     * @return defaultRate
     */
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}

	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}

    /**
     * @return defaultRateY
     */
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}


}
