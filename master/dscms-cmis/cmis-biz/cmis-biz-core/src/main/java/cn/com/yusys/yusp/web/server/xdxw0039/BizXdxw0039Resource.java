package cn.com.yusys.yusp.web.server.xdxw0039;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0039.req.Xdxw0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0039.resp.Xdxw0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0039.Xdxw0039Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:提交决议
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0039:提交决议")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0039Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0039Resource.class);

    @Autowired
    private Xdxw0039Service xdxw0039Service;

    /**
     * 交易码：xdxw0039
     * 交易描述：提交决议
     *
     * @param xdxw0039DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提交决议")
    @PostMapping("/xdxw0039")
    protected @ResponseBody
    ResultDto<Xdxw0039DataRespDto> xdxw0039(@Validated @RequestBody Xdxw0039DataReqDto xdxw0039DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataReqDto));
        Xdxw0039DataRespDto xdxw0039DataRespDto = new Xdxw0039DataRespDto();// 响应Dto:提交决议
        ResultDto<Xdxw0039DataRespDto> xdxw0039DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0039DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataReqDto));
            xdxw0039DataRespDto = xdxw0039Service.xdxw0039(xdxw0039DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataRespDto));
            // 封装xdxw0039DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0039DataRespDto.getOpFlag();
            String opMessage = xdxw0039DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0039DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0039DataResultDto.setMessage(opMessage);
            } else {
                xdxw0039DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0039DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0039DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0039DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0039DataResultDto.setCode(e.getErrorCode());
            xdxw0039DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, e.getMessage());
            xdxw0039DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0039DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0039DataResultDto中异常返回码和返回信息
            xdxw0039DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0039DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0039DataRespDto到xdxw0039DataResultDto中
        xdxw0039DataResultDto.setData(xdxw0039DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataResultDto));
        return xdxw0039DataResultDto;
    }
}
