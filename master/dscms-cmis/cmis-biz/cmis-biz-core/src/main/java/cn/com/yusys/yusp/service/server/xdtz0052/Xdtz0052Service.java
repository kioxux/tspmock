package cn.com.yusys.yusp.service.server.xdtz0052;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0052.req.Xdtz0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.resp.Xdtz0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0004.CmisBatch0004Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:根据借据号查询申请人行内还款（利息、本金）该笔借据次数
 *
 * @author YX-YD
 * @version 1.0
 */
@Service
public class Xdtz0052Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0052Service.class);
    @Autowired
    private CmisBatch0004Service cmisBatch0004Service;


    /**
     * @param xdtz0052DataReqDto
     * @Description:根据借据号查询申请人行内还款（利息、本金）该笔借据次数
     * @Author: YX-YD
     * @Date: 2021/6/5 18:26
     * @return: xdtz0052DataRespDto
     **/
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0052DataRespDto xdtz0052(Xdtz0052DataReqDto xdtz0052DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0052DataReqDto));
        //定义返回信息
        Xdtz0052DataRespDto xdtz0052DataRespDto = new Xdtz0052DataRespDto();
        try {
            // 请求字段
            String billNo = xdtz0052DataReqDto.getBillNo();
            /**
             * 查询类型:
             * 10:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
             * 05:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
             * 0:无查询条件
             */
            String queryType = null;//查询类型
            Integer countNum = null;
            // 必输字段校验
            if (StringUtil.isNotEmpty(billNo)) {
                logger.info("根据借据号查询申请人行内还款（利息、本金）该笔借据次数开始,查询参数为:{}", JSON.toJSONString(billNo));
                Cmisbatch0004ReqDto cmisbatch0004ReqDto = new Cmisbatch0004ReqDto();
                cmisbatch0004ReqDto.setDkjiejuh(billNo);//贷款借据号
                cmisbatch0004ReqDto.setQueryType("0");
                Cmisbatch0004RespDto cmisbatch0004RespDto = cmisBatch0004Service.cmisbatch0004(cmisbatch0004ReqDto);
                countNum = cmisbatch0004RespDto.getCountNum();
                String repayThisBillTimes = countNum.toString();
                logger.info("根据借据号查询申请人行内还款（利息、本金）该笔借据次数结束,返回参数为:{}", JSON.toJSONString(repayThisBillTimes));
                // 根据借据编号前往信贷查询当房贷未结清时，近24个月内逾期记录条数
                if (StringUtil.isNotEmpty(repayThisBillTimes)) {
                    // 行内还款（利息、本金）该笔借据次数
                    xdtz0052DataRespDto.setRepayThisBillTimes(repayThisBillTimes);
                } else {
                    // 行内还款（利息、本金）该笔借据次数
                    xdtz0052DataRespDto.setRepayThisBillTimes(SuccessEnum.CMIS_SUCCSESS.key);
                }
            } else {
                // 请求参数不存在
                xdtz0052DataRespDto.setRepayThisBillTimes(StringUtils.EMPTY);
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataRespDto));
        return xdtz0052DataRespDto;
    }
}
